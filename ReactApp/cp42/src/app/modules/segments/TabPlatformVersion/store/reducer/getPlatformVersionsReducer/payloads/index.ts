import { SelectDataMetadataResponseDto } from 'app/web/common';
import { PlatformVersion1CDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabPlatformVersionApiProxy';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения списка данных по версии платформы 1С
 */
export type GetPlatformVersionsActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения списка данных по версии платформы 1С
 */
export type GetPlatformVersionsActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения списка данных по версии платформы 1С
 */
export type GetPlatformVersionsActionSuccessPayload = ReducerActionSuccessPayload & SelectDataMetadataResponseDto<PlatformVersion1CDataModel>;

/**
 * Все возможные Action при получении списка данных по версии платформы 1С
 */
export type GetPlatformVersionsActionAnyPayload =
    | GetPlatformVersionsActionStartPayload
    | GetPlatformVersionsActionFailedPayload
    | GetPlatformVersionsActionSuccessPayload;