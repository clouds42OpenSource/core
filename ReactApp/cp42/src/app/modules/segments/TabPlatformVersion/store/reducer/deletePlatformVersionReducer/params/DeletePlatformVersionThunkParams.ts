import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для удаления версии платформы 1С
 */
export type DeletePlatformVersionThunkParams = IForceThunkParam & {
    /**
     * Версия платформы 1C
     */
    platformVersion: string
};