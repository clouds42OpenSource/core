import { PlatformVersion1CFilterParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabPlatformVersionApiProxy';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для получения списка версии платформы 1С
 */
export type GetPlatformVersionsThunkParams = IForceThunkParam & PlatformVersion1CFilterParams;