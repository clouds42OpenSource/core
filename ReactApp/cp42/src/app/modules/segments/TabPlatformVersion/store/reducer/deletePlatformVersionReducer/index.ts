import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabPlatformVersionProcessId } from 'app/modules/segments/TabPlatformVersion';
import { TabPlatformVersionActions } from 'app/modules/segments/TabPlatformVersion/store/actions';
import { TabPlatformVersionState } from 'app/modules/segments/TabPlatformVersion/store/reducer';
import { DeletePlatformVersionActionAnyPayload, DeletePlatformVersionActionSuccessPayload } from 'app/modules/segments/TabPlatformVersion/store/reducer/deletePlatformVersionReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для удаления версии платформы 1С
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const deletePlatformVersionReducer: TPartialReducerObject<TabPlatformVersionState, DeletePlatformVersionActionAnyPayload> =
    (state, action) => {
        const processId = TabPlatformVersionProcessId.DeletePlatformVersion;
        const actions = TabPlatformVersionActions.DeletePlatformVersion;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as DeletePlatformVersionActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, payload);
            }
        });
    };