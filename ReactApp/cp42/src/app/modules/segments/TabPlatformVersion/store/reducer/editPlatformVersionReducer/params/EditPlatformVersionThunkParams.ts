import { PlatformVersion1CDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabPlatformVersionApiProxy';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для редактирования версии платформы 1С
 */
export type EditPlatformVersionThunkParams = IForceThunkParam & PlatformVersion1CDataModel;