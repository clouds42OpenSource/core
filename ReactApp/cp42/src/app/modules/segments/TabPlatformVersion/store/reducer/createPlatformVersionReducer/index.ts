import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabPlatformVersionProcessId } from 'app/modules/segments/TabPlatformVersion';
import { TabPlatformVersionActions } from 'app/modules/segments/TabPlatformVersion/store/actions';
import { TabPlatformVersionState } from 'app/modules/segments/TabPlatformVersion/store/reducer';
import { CreatePlatformVersionActionAnyPayload, CreatePlatformVersionActionSuccessPayload } from 'app/modules/segments/TabPlatformVersion/store/reducer/createPlatformVersionReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для создания версии платформы 1С
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const createPlatformVersionReducer: TPartialReducerObject<TabPlatformVersionState, CreatePlatformVersionActionAnyPayload> =
    (state, action) => {
        const processId = TabPlatformVersionProcessId.CreatePlatformVersion;
        const actions = TabPlatformVersionActions.CreatePlatformVersion;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as CreatePlatformVersionActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, payload);
            }
        });
    };