import { partialFillOf } from 'app/common/functions';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabPlatformVersionProcessId } from 'app/modules/segments/TabPlatformVersion';
import { TabPlatformVersionActions } from 'app/modules/segments/TabPlatformVersion/store/actions';
import { TabPlatformVersionState } from 'app/modules/segments/TabPlatformVersion/store/reducer';
import { GetPlatformVersionActionAnyPayload, GetPlatformVersionActionSuccessPayload } from 'app/modules/segments/TabPlatformVersion/store/reducer/getPlatformVersionReducer/payloads';
import { PlatformVersion1CDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabPlatformVersionApiProxy';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения версии платформы 1С
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const getPlatformVersionReducer: TPartialReducerObject<TabPlatformVersionState, GetPlatformVersionActionAnyPayload> =
    (state, action) => {
        const processId = TabPlatformVersionProcessId.GetPlatformVersion;
        const actions = TabPlatformVersionActions.GetPlatformVersion;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onStartProcessState: () => {
                return reducerStateProcessStart(state, processId, {
                    platformVersion: partialFillOf<PlatformVersion1CDataModel>()
                });
            },
            onSuccessProcessState: () => {
                const payload = action.payload as GetPlatformVersionActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    platformVersion: payload
                });
            }
        });
    };