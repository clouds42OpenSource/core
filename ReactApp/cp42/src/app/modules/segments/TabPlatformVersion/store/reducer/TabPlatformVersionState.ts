import { TabPlatformVersionProcessId } from 'app/modules/segments/TabPlatformVersion';
import { SelectDataMetadataResponseDto } from 'app/web/common';
import { PlatformVersion1CDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabPlatformVersionApiProxy';
import { IReducerState } from 'core/redux/interfaces';

/**
 * Состояние редюсера для работы с процесами Segments -> Платформа 1С
 */
export type TabPlatformVersionState = IReducerState<TabPlatformVersionProcessId> & {
    /**
     * Массив версии платформы 1С
     */
    platformVersions: SelectDataMetadataResponseDto<PlatformVersion1CDataModel>,

    /**
     * Модель версии платформы 1С
     */
    platformVersion: PlatformVersion1CDataModel,

    /**
     * содержит ярлыки для фиксации загрузок данных, была ли загрузка или ещё нет
     */
    hasSuccessFor: {
        /**
         * Если true, то версии платформы 1С получены
         */
        hasPlatformVersionsReceived: boolean
    };
};