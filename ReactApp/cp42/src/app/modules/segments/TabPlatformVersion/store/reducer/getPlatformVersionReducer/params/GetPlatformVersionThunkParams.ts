import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для получения платформы 1С
 */
export type GetPlatformVersionThunkParams = IForceThunkParam & {
    /**
     * Версия платформы 1C
     */
    platformVersion: string
};