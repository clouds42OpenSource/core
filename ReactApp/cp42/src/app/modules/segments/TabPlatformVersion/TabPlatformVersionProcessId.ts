/**
 * Процесы Segments -> "Платформа 1С"
 */
export enum TabPlatformVersionProcessId {
    /**
     * Процесс на получение списка данных по версиям платформы 1С
     */
    GetPlatformVersions = 'GET_PLATFORM_VERSIONS',

    /**
     * Процесс на создание версии платформы 1С
     */
    CreatePlatformVersion = 'CREATE_PLATFORM_VERSION',

    /**
     * Процесс на редактирование версии платформы 1С
     */
    EditPlatformVersion = 'EDIT_PLATFORM_VERSION',

    /**
     * Процесс на получение версии платформы 1С по ID
     */
    GetPlatformVersion = 'GET_PLATFORM_VERSION',

    /**
     * Процесс на удаление версии платформы 1С по ID
     */
    DeletePlatformVersion = 'DELETE_PLATFORM_VERSION'
}