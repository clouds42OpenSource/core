/**
 * Константы для модуля Segments -> Таб Платформа 1С
 */
export const TabPlatformVersionConstants = {
    /**
     * Название редюсера для модуля Segments -> Таб Платформа 1С
     */
    reducerName: 'TAB_PLATFORM_VERSION'
};