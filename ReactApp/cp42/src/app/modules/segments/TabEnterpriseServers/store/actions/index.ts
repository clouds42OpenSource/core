import { TabEnterpriseServersProcessId } from 'app/modules/segments/TabEnterpriseServers';
import { TabEnterpriseServersConsts } from 'app/modules/segments/TabEnterpriseServers/constants';
import { createReducerActions } from 'core/redux/functions';

/**
 * Все доступные actions для редюсера Segments -> "Сервера 1С"
 */
export const TabEnterpriseServersActions = {
    /**
     * Набор action на получение списка данных по серверам 1С:Предприятие
     */
    GetEnterpriseServers: createReducerActions(TabEnterpriseServersConsts.reducerName, TabEnterpriseServersProcessId.GetEnterpriseServers),

    /**
     * Набор action на создание сервера 1С:Предприятие
     */
    CreateEnterpriseServer: createReducerActions(TabEnterpriseServersConsts.reducerName, TabEnterpriseServersProcessId.CreateEnterpriseServer),

    /**
     * Набор action на редактирование сервера 1С:Предприятие
     */
    EditEnterpriseServer: createReducerActions(TabEnterpriseServersConsts.reducerName, TabEnterpriseServersProcessId.EditEnterpriseServer),

    /**
     * Набор action на получение сервера 1С:Предприятие по ID
     */
    GetEnterpriseServer: createReducerActions(TabEnterpriseServersConsts.reducerName, TabEnterpriseServersProcessId.GetEnterpriseServer),

    /**
     * Набор action на удаление сервера 1С:Предприятие по ID
     */
    DeleteEnterpriseServer: createReducerActions(TabEnterpriseServersConsts.reducerName, TabEnterpriseServersProcessId.DeleteEnterpriseServer),
};