import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabEnterpriseServersProcessId } from 'app/modules/segments/TabEnterpriseServers';
import { TabEnterpriseServersActions } from 'app/modules/segments/TabEnterpriseServers/store/actions';
import { TabEnterpriseServersState } from 'app/modules/segments/TabEnterpriseServers/store/reducer';
import { CreateEnterpriseServerActionAnyPayload, CreateEnterpriseServerActionSuccessPayload } from 'app/modules/segments/TabEnterpriseServers/store/reducer/createEnterpriseServerReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для создания сервера 1С:Предприятия
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const createEnterpriseServerReducer: TPartialReducerObject<TabEnterpriseServersState, CreateEnterpriseServerActionAnyPayload> =
    (state, action) => {
        const processId = TabEnterpriseServersProcessId.CreateEnterpriseServer;
        const actions = TabEnterpriseServersActions.CreateEnterpriseServer;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as CreateEnterpriseServerActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, payload);
            }
        });
    };