import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения редактирования сервера 1C:Предприятия
 */
export type EditEnterpriseServerActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении редактирования сервера 1C:Предприятия
 */
export type EditEnterpriseServerActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении редактирования сервера 1C:Предприятия
 */
export type EditEnterpriseServerActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при редактирования сервера 1C:Предприятия
 */
export type EditEnterpriseServerActionAnyPayload =
    | EditEnterpriseServerActionStartPayload
    | EditEnterpriseServerActionFailedPayload
    | EditEnterpriseServerActionSuccessPayload;