import { EnterpriseServerParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для создания сервера 1С:Предприятия
 */
export type CreateEnterpriseServerThunkParams = IForceThunkParam & EnterpriseServerParams;