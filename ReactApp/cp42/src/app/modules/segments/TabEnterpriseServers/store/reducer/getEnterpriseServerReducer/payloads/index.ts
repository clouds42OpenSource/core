import { EnterpriseServerDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabEnterpriseServersApiProxy';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения данных по серверам 1С:Предприятие по ID
 */
export type GetEnterpriseServerActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения данных по серверам 1С:Предприятие по ID
 */
export type GetEnterpriseServerActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения данных по серверам 1С:Предприятие по ID
 */
export type GetEnterpriseServerActionSuccessPayload = ReducerActionSuccessPayload & EnterpriseServerDataModel;

/**
 * Все возможные Action при получении данных по серверам 1С:Предприятие по ID
 */
export type GetEnterpriseServerActionAnyPayload =
    | GetEnterpriseServerActionStartPayload
    | GetEnterpriseServerActionFailedPayload
    | GetEnterpriseServerActionSuccessPayload;