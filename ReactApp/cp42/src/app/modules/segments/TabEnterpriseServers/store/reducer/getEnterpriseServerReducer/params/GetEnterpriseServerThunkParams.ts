import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для получения сервера 1С:Предприятия по ID
 */
export type GetEnterpriseServerThunkParams = IForceThunkParam & {
    /**
     * ID сервера 1С:Предприятия
     */
    enterpriseServerId: string;
};