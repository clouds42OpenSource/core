import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabEnterpriseServersProcessId } from 'app/modules/segments/TabEnterpriseServers';
import { TabEnterpriseServersActions } from 'app/modules/segments/TabEnterpriseServers/store/actions';
import { TabEnterpriseServersState } from 'app/modules/segments/TabEnterpriseServers/store/reducer';
import { GetEnterpriseServersActionAnyPayload, GetEnterpriseServersActionSuccessPayload } from 'app/modules/segments/TabEnterpriseServers/store/reducer/getEnterpriseServersReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения списка данных по серверам 1С:Предприятие
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const getEnterpriseServersReducer: TPartialReducerObject<TabEnterpriseServersState, GetEnterpriseServersActionAnyPayload> =
    (state, action) => {
        const processId = TabEnterpriseServersProcessId.GetEnterpriseServers;
        const actions = TabEnterpriseServersActions.GetEnterpriseServers;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as GetEnterpriseServersActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    enterpriseServers: payload,
                    hasSuccessFor: {
                        ...state,
                        hasEnterpriseServersReceived: true
                    }
                });
            }
        });
    };