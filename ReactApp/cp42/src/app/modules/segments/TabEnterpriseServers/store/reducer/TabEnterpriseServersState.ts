import { TabEnterpriseServersProcessId } from 'app/modules/segments/TabEnterpriseServers';
import { SelectDataMetadataResponseDto } from 'app/web/common';
import { EnterpriseServerDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabEnterpriseServersApiProxy';
import { IReducerState } from 'core/redux/interfaces';

/**
 * Состояние редюсера для работы с процесами Segments -> Сервера 1С
 */
export type TabEnterpriseServersState = IReducerState<TabEnterpriseServersProcessId> & {
    /**
     * Массив cервера 1С:Предприятие
     */
    enterpriseServers: SelectDataMetadataResponseDto<EnterpriseServerDataModel>,

    /**
     * Модель сервера 1С:Предприятие
     */
    enterpriseServer: EnterpriseServerDataModel,

    /**
     * содержит ярлыки для фиксации загрузок данных, была ли загрузка или ещё нет
     */
    hasSuccessFor: {
        /**
         * Если true, то cервера 1С:Предприятие получены
         */
        hasEnterpriseServersReceived: boolean
    };
};