import { SelectDataMetadataResponseDto } from 'app/web/common';
import { EnterpriseServerDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabEnterpriseServersApiProxy';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения списка данных по серверам 1С:Предприятие
 */
export type GetEnterpriseServersActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения списка данных по серверам 1С:Предприятие
 */
export type GetEnterpriseServersActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения списка данных по серверам 1С:Предприятие
 */
export type GetEnterpriseServersActionSuccessPayload = ReducerActionSuccessPayload & SelectDataMetadataResponseDto<EnterpriseServerDataModel>;

/**
 * Все возможные Action при получении списка данных по серверам 1С:Предприятие
 */
export type GetEnterpriseServersActionAnyPayload =
    | GetEnterpriseServersActionStartPayload
    | GetEnterpriseServersActionFailedPayload
    | GetEnterpriseServersActionSuccessPayload;