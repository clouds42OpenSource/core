import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabEnterpriseServersProcessId } from 'app/modules/segments/TabEnterpriseServers';
import { TabEnterpriseServersActions } from 'app/modules/segments/TabEnterpriseServers/store/actions';
import { TabEnterpriseServersState } from 'app/modules/segments/TabEnterpriseServers/store/reducer';
import { EditEnterpriseServerActionAnyPayload, EditEnterpriseServerActionSuccessPayload } from 'app/modules/segments/TabEnterpriseServers/store/reducer/editEnterpriseServerReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для редактирования сервера 1С:Предприятия
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const editEnterpriseServerReducer: TPartialReducerObject<TabEnterpriseServersState, EditEnterpriseServerActionAnyPayload> =
    (state, action) => {
        const processId = TabEnterpriseServersProcessId.EditEnterpriseServer;
        const actions = TabEnterpriseServersActions.EditEnterpriseServer;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as EditEnterpriseServerActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, payload);
            }
        });
    };