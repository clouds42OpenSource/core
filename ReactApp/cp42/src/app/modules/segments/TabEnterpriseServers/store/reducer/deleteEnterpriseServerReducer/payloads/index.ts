import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения удаления данных по серверам 1С:Предприятие по ID
 */
export type DeleteEnterpriseServerActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении удаления данных по серверам 1С:Предприятие по ID
 */
export type DeleteEnterpriseServerActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении удаления данных по серверам 1С:Предприятие по ID
 */
export type DeleteEnterpriseServerActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при удалении данных по серверам 1С:Предприятие по ID
 */
export type DeleteEnterpriseServerActionAnyPayload =
    | DeleteEnterpriseServerActionStartPayload
    | DeleteEnterpriseServerActionFailedPayload
    | DeleteEnterpriseServerActionSuccessPayload;