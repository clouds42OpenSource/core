import { EnterpriseServerFilterParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabEnterpriseServersApiProxy';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для получения списка данных по серверам 1С:Предприятие
 */
export type GetEnterpriseServersThunkParams = IForceThunkParam & EnterpriseServerFilterParams;