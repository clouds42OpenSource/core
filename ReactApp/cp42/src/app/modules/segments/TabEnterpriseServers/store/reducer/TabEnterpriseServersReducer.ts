import { getInitialMetadata, partialFillOf } from 'app/common/functions';
import { TabEnterpriseServersConsts } from 'app/modules/segments/TabEnterpriseServers/constants';
import { createEnterpriseServerReducer } from 'app/modules/segments/TabEnterpriseServers/store/reducer/createEnterpriseServerReducer';
import { deleteEnterpriseServerReducer } from 'app/modules/segments/TabEnterpriseServers/store/reducer/deleteEnterpriseServerReducer';
import { editEnterpriseServerReducer } from 'app/modules/segments/TabEnterpriseServers/store/reducer/editEnterpriseServerReducer';
import { getEnterpriseServerReducer } from 'app/modules/segments/TabEnterpriseServers/store/reducer/getEnterpriseServerReducer';
import { getEnterpriseServersReducer } from 'app/modules/segments/TabEnterpriseServers/store/reducer/getEnterpriseServersReducer';
import { TabEnterpriseServersState } from 'app/modules/segments/TabEnterpriseServers/store/reducer/TabEnterpriseServersState';
import { EnterpriseServerDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabEnterpriseServersApiProxy';
import { createReducer, initReducerState } from 'core/redux/functions';

/**
 * Начальное состояние редюсера Segments -> Сервера 1С
 */
const initialState = initReducerState<TabEnterpriseServersState>(
    TabEnterpriseServersConsts.reducerName,
    {
        enterpriseServers: {
            metadata: getInitialMetadata(),
            records: []
        },
        enterpriseServer: partialFillOf<EnterpriseServerDataModel>(),
        hasSuccessFor: {
            hasEnterpriseServersReceived: false
        }
    }
);

/**
 * Объединённые части редюсера для всего состояния Segments -> Сервера 1С
 */
const partialReducers = [
    getEnterpriseServersReducer,
    createEnterpriseServerReducer,
    getEnterpriseServerReducer,
    editEnterpriseServerReducer,
    deleteEnterpriseServerReducer
];

/**
 * Редюсер Segments -> Сервера 1С
 */
export const TabEnterpriseServersReducer = createReducer(initialState, partialReducers);