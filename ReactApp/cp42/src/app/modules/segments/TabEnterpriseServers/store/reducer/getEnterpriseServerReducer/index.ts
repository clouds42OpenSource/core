import { partialFillOf } from 'app/common/functions';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabEnterpriseServersProcessId } from 'app/modules/segments/TabEnterpriseServers';
import { TabEnterpriseServersActions } from 'app/modules/segments/TabEnterpriseServers/store/actions';
import { TabEnterpriseServersState } from 'app/modules/segments/TabEnterpriseServers/store/reducer';
import { GetEnterpriseServerActionAnyPayload, GetEnterpriseServerActionSuccessPayload } from 'app/modules/segments/TabEnterpriseServers/store/reducer/getEnterpriseServerReducer/payloads';
import { EnterpriseServerDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabEnterpriseServersApiProxy';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения данных по серверу 1С:Предприятие по ID
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const getEnterpriseServerReducer: TPartialReducerObject<TabEnterpriseServersState, GetEnterpriseServerActionAnyPayload> =
    (state, action) => {
        const processId = TabEnterpriseServersProcessId.GetEnterpriseServer;
        const actions = TabEnterpriseServersActions.GetEnterpriseServer;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onStartProcessState: () => {
                return reducerStateProcessStart(state, processId, {
                    enterpriseServer: partialFillOf<EnterpriseServerDataModel>()
                });
            },
            onSuccessProcessState: () => {
                const payload = action.payload as GetEnterpriseServerActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    enterpriseServer: payload
                });
            }
        });
    };