import { TabEnterpriseServersActions } from 'app/modules/segments/TabEnterpriseServers/store/actions';
import { EditEnterpriseServerThunkParams } from 'app/modules/segments/TabEnterpriseServers/store/reducer/editEnterpriseServerReducer/params';
import { EditEnterpriseServerActionFailedPayload, EditEnterpriseServerActionStartPayload, EditEnterpriseServerActionSuccessPayload } from 'app/modules/segments/TabEnterpriseServers/store/reducer/editEnterpriseServerReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = EditEnterpriseServerActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = EditEnterpriseServerActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = EditEnterpriseServerActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = EditEnterpriseServerThunkParams;

const { EditEnterpriseServer: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = TabEnterpriseServersActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для редактирования сервера 1С:Предприятия
 */
export class EditEnterpriseServerThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new EditEnterpriseServerThunk().execute(args, args?.showLoadingProgress);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        return {
            condition: args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().TabEnterpriseServersState
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        const api = InterlayerApiProxy.getSegmentsApiProxy().getTabEnterpriseServersApiProxyMethods();

        try {
            await api.editEnterpriseServer(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, args.inputParams!);
            args.success();
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}