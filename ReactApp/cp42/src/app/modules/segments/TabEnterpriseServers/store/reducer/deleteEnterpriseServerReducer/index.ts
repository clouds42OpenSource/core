import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabEnterpriseServersProcessId } from 'app/modules/segments/TabEnterpriseServers';
import { TabEnterpriseServersActions } from 'app/modules/segments/TabEnterpriseServers/store/actions';
import { TabEnterpriseServersState } from 'app/modules/segments/TabEnterpriseServers/store/reducer';
import { DeleteEnterpriseServerActionAnyPayload, DeleteEnterpriseServerActionSuccessPayload } from 'app/modules/segments/TabEnterpriseServers/store/reducer/deleteEnterpriseServerReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для удаления данных по серверу 1С:Предприятие по ID
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const deleteEnterpriseServerReducer: TPartialReducerObject<TabEnterpriseServersState, DeleteEnterpriseServerActionAnyPayload> =
    (state, action) => {
        const processId = TabEnterpriseServersProcessId.DeleteEnterpriseServer;
        const actions = TabEnterpriseServersActions.DeleteEnterpriseServer;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as DeleteEnterpriseServerActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, payload);
            }
        });
    };