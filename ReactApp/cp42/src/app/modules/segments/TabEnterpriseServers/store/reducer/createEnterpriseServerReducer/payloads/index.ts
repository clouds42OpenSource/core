import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения создания сервера 1C:Предприятия
 */
export type CreateEnterpriseServerActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении создания сервера 1C:Предприятия
 */
export type CreateEnterpriseServerActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении создания сервера 1C:Предприятия
 */
export type CreateEnterpriseServerActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при создания сервера 1C:Предприятия
 */
export type CreateEnterpriseServerActionAnyPayload =
    | CreateEnterpriseServerActionStartPayload
    | CreateEnterpriseServerActionFailedPayload
    | CreateEnterpriseServerActionSuccessPayload;