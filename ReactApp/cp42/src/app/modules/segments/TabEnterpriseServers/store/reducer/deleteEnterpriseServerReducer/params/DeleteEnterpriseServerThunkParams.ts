import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для удаления сервера 1С:Предприятия
 */
export type DeleteEnterpriseServerThunkParams = IForceThunkParam & {
    /**
     * ID сервера 1С:Предприятия
     */
    enterpriseServerId: string;
};