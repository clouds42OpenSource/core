/**
 * Константы для модуля Segments -> Таб Сервера 1С
 */
export const TabEnterpriseServersConsts = {
    /**
     * Название редюсера для модуля Segments -> Таб Сервера 1С
     */
    reducerName: 'TAB_ENTERPRISE_SERVERS'
};