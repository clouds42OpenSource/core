/**
 * Процесы Segments -> "Сервера 1С"
 */
export enum TabEnterpriseServersProcessId {
    /**
     * Процесс на получение списка данных по серверам 1С:Предприятие
     */
    GetEnterpriseServers = 'GET_ENTERPRISE_SERVERS',

    /**
     * Процесс на создание сервера 1С:Предприятие
     */
    CreateEnterpriseServer = 'CREATE_ENTERPRISE_SERVER',

    /**
     * Процесс на редактирование сервера 1С:Предприятие
     */
    EditEnterpriseServer = 'EDIT_ENTERPRISE_SERVER',

    /**
     * Процесс на получение сервера 1С:Предприятие по ID
     */
    GetEnterpriseServer = 'GET_ENTERPRISE_SERVER',

    /**
     * Процесс на удаление сервера 1С:Предприятие по ID
     */
    DeleteEnterpriseServer = 'DELETE_ENTERPRISE_SERVER'
}