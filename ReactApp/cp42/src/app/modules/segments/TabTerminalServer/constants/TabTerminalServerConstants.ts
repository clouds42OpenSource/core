/**
 * Константы для модуля Segments -> Таб Терм. сервера
 */
export const TabTerminalServerConstants = {
    /**
     * Название редюсера для модуля Segments -> Таб Терм. сервера
     */
    reducerName: 'TAB_TERMINAL_SERVER'
};