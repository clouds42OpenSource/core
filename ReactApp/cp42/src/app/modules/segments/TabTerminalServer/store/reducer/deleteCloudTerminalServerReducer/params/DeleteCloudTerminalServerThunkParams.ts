import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для удаления терминального сервера
 */
export type DeleteCloudTerminalServerThunkParams = IForceThunkParam & {
    /**
     * ID терминального сервера
     */
    terminalServerId: string
};