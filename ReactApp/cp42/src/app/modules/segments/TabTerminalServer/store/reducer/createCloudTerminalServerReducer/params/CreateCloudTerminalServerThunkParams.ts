import { CommonCreateSegmentParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для создания терминального сервера
 */
export type CreateCloudTerminalServerThunkParams = IForceThunkParam & CommonCreateSegmentParams;