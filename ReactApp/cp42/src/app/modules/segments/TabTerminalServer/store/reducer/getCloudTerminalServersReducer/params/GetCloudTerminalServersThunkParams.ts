import { CommonSegmentFilterParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для получения списка терминальных серверов
 */
export type GetCloudTerminalServersThunkParams = IForceThunkParam & CommonSegmentFilterParams;