import { TabTerminalServerProcessId } from 'app/modules/segments/TabTerminalServer';
import { SelectDataMetadataResponseDto } from 'app/web/common';
import { CommonSegmentDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import { IReducerState } from 'core/redux/interfaces';

/**
 * Состояние редюсера для работы с процесами Segments -> Терм. сервера
 */
export type TabTerminalServerState = IReducerState<TabTerminalServerProcessId> & {
    /**
     * Массив терминального сервера
     */
    cloudTerminalServers: SelectDataMetadataResponseDto<CommonSegmentDataModel>,

    /**
     * Модель терминального сервера
     */
    cloudTerminalServer: CommonSegmentDataModel,

    /**
     * содержит ярлыки для фиксации загрузок данных, была ли загрузка или ещё нет
     */
    hasSuccessFor: {
        /**
         * Если true, то терминальные сервера получены
         */
        hasCloudTerminalServersReceived: boolean
    };
};