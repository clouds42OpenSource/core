import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabTerminalServerProcessId } from 'app/modules/segments/TabTerminalServer';
import { TabTerminalServerActions } from 'app/modules/segments/TabTerminalServer/store/actions';
import { TabTerminalServerState } from 'app/modules/segments/TabTerminalServer/store/reducer';
import { GetTerminalServersActionAnyPayload, GetTerminalServersActionSuccessPayload } from 'app/modules/segments/TabTerminalServer/store/reducer/getCloudTerminalServersReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения списка данных по терминальным серверам
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const getCloudTerminalServersReducer: TPartialReducerObject<TabTerminalServerState, GetTerminalServersActionAnyPayload> =
    (state, action) => {
        const processId = TabTerminalServerProcessId.GetTerminalServers;
        const actions = TabTerminalServerActions.GetTerminalServers;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as GetTerminalServersActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    cloudTerminalServers: payload,
                    hasSuccessFor: {
                        ...state,
                        hasCloudTerminalServersReceived: true
                    }
                });
            }
        });
    };