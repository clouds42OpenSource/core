import { TabTerminalServerProcessId } from 'app/modules/segments/TabTerminalServer';
import { TabTerminalServerConstants } from 'app/modules/segments/TabTerminalServer/constants';
import { createReducerActions } from 'core/redux/functions';

/**
 * Все доступные actions для редюсера Segments -> "Терм. сервера"
 */
export const TabTerminalServerActions = {
    /**
     * Набор action на получение списка данных по терминальным серверам
     */
    GetTerminalServers: createReducerActions(TabTerminalServerConstants.reducerName, TabTerminalServerProcessId.GetTerminalServers),

    /**
     * Набор action на создание терминального сервера
     */
    CreateTerminalServer: createReducerActions(TabTerminalServerConstants.reducerName, TabTerminalServerProcessId.CreateTerminalServer),

    /**
     * Набор action на редактирование терминального сервера
     */
    EditTerminalServer: createReducerActions(TabTerminalServerConstants.reducerName, TabTerminalServerProcessId.EditTerminalServer),

    /**
     * Набор action на получение терминального сервера по ID
     */
    GetTerminalServer: createReducerActions(TabTerminalServerConstants.reducerName, TabTerminalServerProcessId.GetTerminalServer),

    /**
     * Набор action на удаление терминального сервера по ID
     */
    DeleteTerminalServer: createReducerActions(TabTerminalServerConstants.reducerName, TabTerminalServerProcessId.DeleteTerminalServer)
};