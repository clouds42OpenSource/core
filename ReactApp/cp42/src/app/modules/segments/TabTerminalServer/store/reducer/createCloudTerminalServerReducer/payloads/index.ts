import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения создания терминального сервера
 */
export type CreateTerminalServerActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении создания терминального сервера
 */
export type CreateTerminalServerActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении создания терминального сервера
 */
export type CreateTerminalServerActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при создании терминального сервера
 */
export type CreateTerminalServerActionAnyPayload =
    | CreateTerminalServerActionStartPayload
    | CreateTerminalServerActionFailedPayload
    | CreateTerminalServerActionSuccessPayload;