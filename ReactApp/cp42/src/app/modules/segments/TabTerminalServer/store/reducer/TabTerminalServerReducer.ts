import { getInitialMetadata, partialFillOf } from 'app/common/functions';
import { TabTerminalServerConstants } from 'app/modules/segments/TabTerminalServer/constants';
import { TabTerminalServerState } from 'app/modules/segments/TabTerminalServer/store/reducer';
import { createCloudTerminalServerReducer } from 'app/modules/segments/TabTerminalServer/store/reducer/createCloudTerminalServerReducer';
import { deleteCloudTerminalServerReducer } from 'app/modules/segments/TabTerminalServer/store/reducer/deleteCloudTerminalServerReducer';
import { editCloudTerminalServerReducer } from 'app/modules/segments/TabTerminalServer/store/reducer/editCloudTerminalServerReducer';
import { getCloudTerminalServerReducer } from 'app/modules/segments/TabTerminalServer/store/reducer/getCloudTerminalServerReducer';
import { getCloudTerminalServersReducer } from 'app/modules/segments/TabTerminalServer/store/reducer/getCloudTerminalServersReducer';
import { CommonSegmentDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import { createReducer, initReducerState } from 'core/redux/functions';

/**
 * Начальное состояние редюсера Segments -> Терм. сервера
 */
const initialState = initReducerState<TabTerminalServerState>(
    TabTerminalServerConstants.reducerName,
    {
        cloudTerminalServers: {
            metadata: getInitialMetadata(),
            records: []
        },
        cloudTerminalServer: partialFillOf<CommonSegmentDataModel>(),
        hasSuccessFor: {
            hasCloudTerminalServersReceived: false
        }
    }
);

/**
 * Объединённые части редюсера для всего состояния Segments -> Терм. сервера
 */
const partialReducers = [
    getCloudTerminalServersReducer,
    getCloudTerminalServerReducer,
    deleteCloudTerminalServerReducer,
    createCloudTerminalServerReducer,
    editCloudTerminalServerReducer
];

/**
 * Редюсер Segments -> Терм. сервера
 */
export const TabTerminalServerReducer = createReducer(initialState, partialReducers);