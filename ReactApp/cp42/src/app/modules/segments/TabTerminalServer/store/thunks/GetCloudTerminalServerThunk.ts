import { TabTerminalServerActions } from 'app/modules/segments/TabTerminalServer/store/actions';
import { GetCloudTerminalServerThunkParams } from 'app/modules/segments/TabTerminalServer/store/reducer/getCloudTerminalServerReducer/params';
import { GetTerminalServerActionFailedPayload, GetTerminalServerActionStartPayload, GetTerminalServerActionSuccessPayload } from 'app/modules/segments/TabTerminalServer/store/reducer/getCloudTerminalServerReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = GetTerminalServerActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = GetTerminalServerActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = GetTerminalServerActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = GetCloudTerminalServerThunkParams;

const { GetTerminalServer: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = TabTerminalServerActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для получения терминального сервера
 */
export class GetCloudTerminalServerThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new GetCloudTerminalServerThunk().execute(args, args?.showLoadingProgress);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        return {
            condition: args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().TabTerminalServerState
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        const api = InterlayerApiProxy.getSegmentsApiProxy().getTerminalServerTabApiProxyMethods();

        try {
            const response = await api.getCloudTerminalServer(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, args.inputParams!.terminalServerId);
            args.success(response);
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}