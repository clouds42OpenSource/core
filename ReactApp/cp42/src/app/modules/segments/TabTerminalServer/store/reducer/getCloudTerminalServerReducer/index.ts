import { partialFillOf } from 'app/common/functions';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabTerminalServerProcessId } from 'app/modules/segments/TabTerminalServer';
import { TabTerminalServerActions } from 'app/modules/segments/TabTerminalServer/store/actions';
import { TabTerminalServerState } from 'app/modules/segments/TabTerminalServer/store/reducer';
import { GetTerminalServerActionAnyPayload, GetTerminalServerActionSuccessPayload } from 'app/modules/segments/TabTerminalServer/store/reducer/getCloudTerminalServerReducer/payloads';
import { CommonSegmentDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения терминального сервера
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const getCloudTerminalServerReducer: TPartialReducerObject<TabTerminalServerState, GetTerminalServerActionAnyPayload> =
    (state, action) => {
        const processId = TabTerminalServerProcessId.GetTerminalServer;
        const actions = TabTerminalServerActions.GetTerminalServer;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onStartProcessState: () => {
                return reducerStateProcessStart(state, processId, {
                    cloudTerminalServer: partialFillOf<CommonSegmentDataModel>()
                });
            },
            onSuccessProcessState: () => {
                const payload = action.payload as GetTerminalServerActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    cloudTerminalServer: payload
                });
            }
        });
    };