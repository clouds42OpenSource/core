import { SelectDataMetadataResponseDto } from 'app/web/common';
import { CommonSegmentDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения списка данных по терминальным серверам
 */
export type GetTerminalServersActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения списка данных по терминальным серверам
 */
export type GetTerminalServersActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения списка данных по терминальным серверам
 */
export type GetTerminalServersActionSuccessPayload = ReducerActionSuccessPayload & SelectDataMetadataResponseDto<CommonSegmentDataModel>;

/**
 * Все возможные Action при получении списка данных по терминальным серверам
 */
export type GetTerminalServersActionAnyPayload =
    | GetTerminalServersActionStartPayload
    | GetTerminalServersActionFailedPayload
    | GetTerminalServersActionSuccessPayload;