import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabTerminalServerProcessId } from 'app/modules/segments/TabTerminalServer';
import { TabTerminalServerActions } from 'app/modules/segments/TabTerminalServer/store/actions';
import { TabTerminalServerState } from 'app/modules/segments/TabTerminalServer/store/reducer';
import { CreateTerminalServerActionAnyPayload, CreateTerminalServerActionSuccessPayload } from 'app/modules/segments/TabTerminalServer/store/reducer/createCloudTerminalServerReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для создания терминального сервера
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const createCloudTerminalServerReducer: TPartialReducerObject<TabTerminalServerState, CreateTerminalServerActionAnyPayload> =
    (state, action) => {
        const processId = TabTerminalServerProcessId.CreateTerminalServer;
        const actions = TabTerminalServerActions.CreateTerminalServer;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as CreateTerminalServerActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, payload);
            }
        });
    };