import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabTerminalServerProcessId } from 'app/modules/segments/TabTerminalServer';
import { TabTerminalServerActions } from 'app/modules/segments/TabTerminalServer/store/actions';
import { TabTerminalServerState } from 'app/modules/segments/TabTerminalServer/store/reducer';
import { DeleteTerminalServerActionAnyPayload, DeleteTerminalServerActionSuccessPayload } from 'app/modules/segments/TabTerminalServer/store/reducer/deleteCloudTerminalServerReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для удаления терминального сервера
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const deleteCloudTerminalServerReducer: TPartialReducerObject<TabTerminalServerState, DeleteTerminalServerActionAnyPayload> =
    (state, action) => {
        const processId = TabTerminalServerProcessId.DeleteTerminalServer;
        const actions = TabTerminalServerActions.DeleteTerminalServer;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as DeleteTerminalServerActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, payload);
            }
        });
    };