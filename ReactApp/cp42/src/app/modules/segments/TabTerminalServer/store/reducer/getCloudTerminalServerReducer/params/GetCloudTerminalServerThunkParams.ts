import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для получения терминального сервера
 */
export type GetCloudTerminalServerThunkParams = IForceThunkParam & {
    /**
     * ID терминального сервера
     */
    terminalServerId: string
};