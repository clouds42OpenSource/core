import { CommonSegmentDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения терминального сервера
 */
export type GetTerminalServerActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения терминального сервера
 */
export type GetTerminalServerActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения терминального сервера
 */
export type GetTerminalServerActionSuccessPayload = ReducerActionSuccessPayload & CommonSegmentDataModel;

/**
 * Все возможные Action при получении терминального сервера
 */
export type GetTerminalServerActionAnyPayload =
    | GetTerminalServerActionStartPayload
    | GetTerminalServerActionFailedPayload
    | GetTerminalServerActionSuccessPayload;