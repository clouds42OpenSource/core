import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения удаления терминального сервера
 */
export type DeleteTerminalServerActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении удаления терминального сервера
 */
export type DeleteTerminalServerActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении удаления терминального сервера
 */
export type DeleteTerminalServerActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при удалении терминального сервера
 */
export type DeleteTerminalServerActionAnyPayload =
    | DeleteTerminalServerActionStartPayload
    | DeleteTerminalServerActionFailedPayload
    | DeleteTerminalServerActionSuccessPayload;