import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabTerminalServerProcessId } from 'app/modules/segments/TabTerminalServer';
import { TabTerminalServerActions } from 'app/modules/segments/TabTerminalServer/store/actions';
import { TabTerminalServerState } from 'app/modules/segments/TabTerminalServer/store/reducer';
import { EditTerminalServerActionAnyPayload, EditTerminalServerActionSuccessPayload } from 'app/modules/segments/TabTerminalServer/store/reducer/editCloudTerminalServerReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для редактирования терминального сервера
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const editCloudTerminalServerReducer: TPartialReducerObject<TabTerminalServerState, EditTerminalServerActionAnyPayload> =
    (state, action) => {
        const processId = TabTerminalServerProcessId.EditTerminalServer;
        const actions = TabTerminalServerActions.EditTerminalServer;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as EditTerminalServerActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, payload);
            }
        });
    };