import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения редактирования терминального сервера
 */
export type EditTerminalServerActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении редактирования терминального сервера
 */
export type EditTerminalServerActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении редактирования терминального сервера
 */
export type EditTerminalServerActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при редактировании терминального сервера
 */
export type EditTerminalServerActionAnyPayload =
    | EditTerminalServerActionStartPayload
    | EditTerminalServerActionFailedPayload
    | EditTerminalServerActionSuccessPayload;