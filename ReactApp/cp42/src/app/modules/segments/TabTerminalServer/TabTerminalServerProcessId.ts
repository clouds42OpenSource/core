/**
 * Процесы Segments -> "Терм. сервера"
 */
export enum TabTerminalServerProcessId {
    /**
     * Процесс на получение списка данных по терминальным серверам
     */
    GetTerminalServers = 'GET_TERMINAL_SERVERS',

    /**
     * Процесс на создание терминального сервера
     */
    CreateTerminalServer = 'CREATE_TERMINAL_SERVER',

    /**
     * Процесс на редактирование терминального сервера
     */
    EditTerminalServer = 'EDIT_TERMINAL_SERVER',

    /**
     * Процесс на получение терминального сервера по ID
     */
    GetTerminalServer = 'GET_TERMINAL_SERVER',

    /**
     * Процесс на удаление терминального сервера по ID
     */
    DeleteTerminalServer = 'DELETE_TERMINAL_SERVER'
}