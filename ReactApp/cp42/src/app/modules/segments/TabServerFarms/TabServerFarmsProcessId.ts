/**
 * Процесы Segments -> "Ферма серверов"
 */
export enum TabServerFarmsProcessId {
    /**
     * Процесс на получение списка данных по ферме сервера
     */
    GetServerFarms = 'GET_SERVER_FARMS',

    /**
     * Процесс на создание фермы сервера
     */
    CreateServerFarm = 'CREATE_SERVER_FARM',

    /**
     * Процесс на редактирование фермы сервера
     */
    EditServerFarm = 'EDIT_SERVER_FARM',

    /**
     * Процесс на получение фермы сервера по ID
     */
    GetServerFarm = 'GET_SERVER_FARM',

    /**
     * Процесс на удаление фермы сервера по ID
     */
    DeleteServerFarm = 'DELETE_SERVER_FARM',

    /**
     * Процесс на получение всех доступных нод
     */
    GetAllPublishNodes = 'GET_ALL_PUBLISH_NODES'
}