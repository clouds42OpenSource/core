/**
 * Константы для модуля Segments -> Ферма серверов
 */
export const TabServerFarmsConstants = {
    /**
     * Название редюсера для модуля Segments -> Ферма серверов
     */
    reducerName: 'TAB_SERVER_FARMS'
};