import { ServerFarmFilterParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabServerFarmsApiProxy';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для получения списка фермы серверов
 */
export type GetServerFarmsThunkParams = IForceThunkParam & ServerFarmFilterParams;