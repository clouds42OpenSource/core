import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения создания фермы сервера
 */
export type CreateServerFarmActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении создания фермы сервера
 */
export type CreateServerFarmActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении создания фермы сервера
 */
export type CreateServerFarmActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при создании фермы сервера
 */
export type CreateServerFarmActionAnyPayload =
    | CreateServerFarmActionStartPayload
    | CreateServerFarmActionFailedPayload
    | CreateServerFarmActionSuccessPayload;