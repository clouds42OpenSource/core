import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения удаления фермы сервера
 */
export type DeleteServerFarmActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении удаления фермы сервера
 */
export type DeleteServerFarmActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении удаления фермы сервера
 */
export type DeleteServerFarmActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при удалении фермы сервера
 */
export type DeleteServerFarmActionAnyPayload =
    | DeleteServerFarmActionStartPayload
    | DeleteServerFarmActionFailedPayload
    | DeleteServerFarmActionSuccessPayload;