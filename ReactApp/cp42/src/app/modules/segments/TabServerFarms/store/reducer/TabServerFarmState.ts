import { TabServerFarmsProcessId } from 'app/modules/segments/TabServerFarms';
import { SelectDataMetadataResponseDto } from 'app/web/common';
import { PublishNodeDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import { ServerFarmDataModel, ServerFarmModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabServerFarmsApiProxy';
import { IReducerState } from 'core/redux/interfaces';

/**
 * Состояние редюсера для работы с процесами Segments -> Ферма серверов
 */
export type TabServerFarmState = IReducerState<TabServerFarmsProcessId> & {
    /**
     * Массив фермы сервера
     */
    serverFarms: SelectDataMetadataResponseDto<ServerFarmDataModel>,

    /**
     * Модель фермы сервера
     */
    serverFarm: ServerFarmModel,

    /**
     * Массив нода публикаций
     */
    availableNodes: PublishNodeDataModel[],

    /**
     * содержит ярлыки для фиксации загрузок данных, была ли загрузка или ещё нет
     */
    hasSuccessFor: {
        /**
         * Если true, то массив фермы сервера получены
         */
        hasServerFarmsReceived: boolean
    };
};