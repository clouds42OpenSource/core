import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabServerFarmsProcessId } from 'app/modules/segments/TabServerFarms';
import { TabServerFarmsActions } from 'app/modules/segments/TabServerFarms/store/actions';
import { TabServerFarmState } from 'app/modules/segments/TabServerFarms/store/reducer';
import { GetServerFarmsActionAnyPayload, GetServerFarmsActionSuccessPayload } from 'app/modules/segments/TabServerFarms/store/reducer/getServerFarmsReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения списка данных фермы серверов
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const getServerFarmsReducer: TPartialReducerObject<TabServerFarmState, GetServerFarmsActionAnyPayload> =
    (state, action) => {
        const processId = TabServerFarmsProcessId.GetServerFarms;
        const actions = TabServerFarmsActions.GetServerFarms;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as GetServerFarmsActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    serverFarms: payload,
                    hasSuccessFor: {
                        ...state,
                        hasServerFarmsReceived: true
                    }
                });
            }
        });
    };