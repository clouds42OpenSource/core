import { SelectDataMetadataResponseDto } from 'app/web/common';
import { ServerFarmDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabServerFarmsApiProxy';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения списка фермы серверов
 */
export type GetServerFarmsActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения списка фермы серверов
 */
export type GetServerFarmsActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения списка фермы серверов
 */
export type GetServerFarmsActionSuccessPayload = ReducerActionSuccessPayload & SelectDataMetadataResponseDto<ServerFarmDataModel>;

/**
 * Все возможные Action при получении списка фермы серверов
 */
export type GetServerFarmsActionAnyPayload =
    | GetServerFarmsActionStartPayload
    | GetServerFarmsActionFailedPayload
    | GetServerFarmsActionSuccessPayload;