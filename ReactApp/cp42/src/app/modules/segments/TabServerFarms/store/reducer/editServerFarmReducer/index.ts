import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabServerFarmsProcessId } from 'app/modules/segments/TabServerFarms';
import { TabServerFarmsActions } from 'app/modules/segments/TabServerFarms/store/actions';
import { TabServerFarmState } from 'app/modules/segments/TabServerFarms/store/reducer';
import { EditServerFarmActionAnyPayload, EditServerFarmActionSuccessPayload } from 'app/modules/segments/TabServerFarms/store/reducer/editServerFarmReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для редактирования фермы сервера
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const editServerFarmReducer: TPartialReducerObject<TabServerFarmState, EditServerFarmActionAnyPayload> =
    (state, action) => {
        const processId = TabServerFarmsProcessId.EditServerFarm;
        const actions = TabServerFarmsActions.EditServerFarm;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as EditServerFarmActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, payload);
            }
        });
    };