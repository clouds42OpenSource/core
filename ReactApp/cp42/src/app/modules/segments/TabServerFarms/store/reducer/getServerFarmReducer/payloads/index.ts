import { ServerFarmModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabServerFarmsApiProxy';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения фермы сервера
 */
export type GetServerFarmActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения фермы сервера
 */
export type GetServerFarmActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения фермы сервера
 */
export type GetServerFarmActionSuccessPayload = ReducerActionSuccessPayload & ServerFarmModel;

/**
 * Все возможные Action при получении фермы сервера
 */
export type GetServerFarmActionAnyPayload =
    | GetServerFarmActionStartPayload
    | GetServerFarmActionFailedPayload
    | GetServerFarmActionSuccessPayload;