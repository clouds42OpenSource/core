import { TabServerFarmsActions } from 'app/modules/segments/TabServerFarms/store/actions';
import { GetAllPublishNodesActionFailedPayload, GetAllPublishNodesActionStartPayload, GetAllPublishNodesActionSuccessPayload } from 'app/modules/segments/TabServerFarms/store/reducer/getAllPublishNodesReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = GetAllPublishNodesActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = GetAllPublishNodesActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = GetAllPublishNodesActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = IForceThunkParam;

const { GetAllPublishNodes: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = TabServerFarmsActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для получения всех доступных нодов
 */
export class GetAllPublishNodesThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new GetAllPublishNodesThunk().execute(args, args?.showLoadingProgress);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        return {
            condition: args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().TabServerFarmState
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        const api = InterlayerApiProxy.getSegmentsApiProxy().getServerFarmsTabApiProxyMethods();

        try {
            const response = await api.getAllPublishNodes(RequestKind.SEND_BY_USER_SYNCHRONOUSLY);
            args.success(response);
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}