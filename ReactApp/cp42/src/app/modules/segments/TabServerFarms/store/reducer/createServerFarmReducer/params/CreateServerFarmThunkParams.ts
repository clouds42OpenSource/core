import { ServerFarmParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabServerFarmsApiProxy';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для создания фермы сервера
 */
export type CreateServerFarmThunkParams = IForceThunkParam & ServerFarmParams;