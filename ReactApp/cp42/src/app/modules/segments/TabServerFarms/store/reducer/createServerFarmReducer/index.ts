import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabServerFarmsProcessId } from 'app/modules/segments/TabServerFarms';
import { TabServerFarmsActions } from 'app/modules/segments/TabServerFarms/store/actions';
import { TabServerFarmState } from 'app/modules/segments/TabServerFarms/store/reducer';
import { CreateServerFarmActionAnyPayload, CreateServerFarmActionSuccessPayload } from 'app/modules/segments/TabServerFarms/store/reducer/createServerFarmReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для создания фермы сервера
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const createServerFarmReducer: TPartialReducerObject<TabServerFarmState, CreateServerFarmActionAnyPayload> =
    (state, action) => {
        const processId = TabServerFarmsProcessId.CreateServerFarm;
        const actions = TabServerFarmsActions.CreateServerFarm;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as CreateServerFarmActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, payload);
            }
        });
    };