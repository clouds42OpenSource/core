import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для получения фермы сервера
 */
export type GetServerFarmThunkParams = IForceThunkParam & {
    /**
     * ID фермы сервера
     */
    serverFarmId: string
};