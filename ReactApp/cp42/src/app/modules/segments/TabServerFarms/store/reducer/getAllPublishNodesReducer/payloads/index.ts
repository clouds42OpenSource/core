import { PublishNodeDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения массива доступных нодов
 */
export type GetAllPublishNodesActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения массива доступных нодов
 */
export type GetAllPublishNodesActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения массива доступных нодов
 */
export type GetAllPublishNodesActionSuccessPayload = ReducerActionSuccessPayload & PublishNodeDataModel[];

/**
 * Все возможные Action при получении массива доступных нодов
 */
export type GetAllPublishNodesActionAnyPayload =
    | GetAllPublishNodesActionStartPayload
    | GetAllPublishNodesActionFailedPayload
    | GetAllPublishNodesActionSuccessPayload;