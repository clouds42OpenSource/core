import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabServerFarmsProcessId } from 'app/modules/segments/TabServerFarms';
import { TabServerFarmsActions } from 'app/modules/segments/TabServerFarms/store/actions';
import { TabServerFarmState } from 'app/modules/segments/TabServerFarms/store/reducer';
import { DeleteServerFarmActionAnyPayload, DeleteServerFarmActionSuccessPayload } from 'app/modules/segments/TabServerFarms/store/reducer/deleteServerFarmReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для удаления фермы сервера
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const deleteServerFarmReducer: TPartialReducerObject<TabServerFarmState, DeleteServerFarmActionAnyPayload> =
    (state, action) => {
        const processId = TabServerFarmsProcessId.DeleteServerFarm;
        const actions = TabServerFarmsActions.DeleteServerFarm;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as DeleteServerFarmActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, payload);
            }
        });
    };