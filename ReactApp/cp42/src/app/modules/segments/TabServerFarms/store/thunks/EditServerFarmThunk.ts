import { TabServerFarmsActions } from 'app/modules/segments/TabServerFarms/store/actions';
import { EditServerFarmThunkParams } from 'app/modules/segments/TabServerFarms/store/reducer/editServerFarmReducer/params';
import { EditServerFarmActionFailedPayload, EditServerFarmActionStartPayload, EditServerFarmActionSuccessPayload } from 'app/modules/segments/TabServerFarms/store/reducer/editServerFarmReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = EditServerFarmActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = EditServerFarmActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = EditServerFarmActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = EditServerFarmThunkParams;

const { EditServerFarm: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = TabServerFarmsActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для редактирования фермы сервера
 */
export class EditServerFarmThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new EditServerFarmThunk().execute(args, args?.showLoadingProgress);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        return {
            condition: args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().TabServerFarmState
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        const api = InterlayerApiProxy.getSegmentsApiProxy().getServerFarmsTabApiProxyMethods();

        try {
            await api.editServerFarm(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, args.inputParams!);
            args.success();
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}