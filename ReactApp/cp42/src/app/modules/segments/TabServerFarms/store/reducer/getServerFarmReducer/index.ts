import { partialFillOf } from 'app/common/functions';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabServerFarmsProcessId } from 'app/modules/segments/TabServerFarms';
import { TabServerFarmsActions } from 'app/modules/segments/TabServerFarms/store/actions';
import { TabServerFarmState } from 'app/modules/segments/TabServerFarms/store/reducer';
import { GetServerFarmActionAnyPayload, GetServerFarmActionSuccessPayload } from 'app/modules/segments/TabServerFarms/store/reducer/getServerFarmReducer/payloads';
import { ServerFarmModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabServerFarmsApiProxy';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения фермы сервера
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const getServerFarmReducer: TPartialReducerObject<TabServerFarmState, GetServerFarmActionAnyPayload> =
    (state, action) => {
        const processId = TabServerFarmsProcessId.GetServerFarm;
        const actions = TabServerFarmsActions.GetServerFarm;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onStartProcessState: () => {
                return reducerStateProcessStart(state, processId, {
                    serverFarm: partialFillOf<ServerFarmModel>()
                });
            },
            onSuccessProcessState: () => {
                const payload = action.payload as GetServerFarmActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    serverFarm: payload
                });
            }
        });
    };