import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabServerFarmsProcessId } from 'app/modules/segments/TabServerFarms';
import { TabServerFarmsActions } from 'app/modules/segments/TabServerFarms/store/actions';
import { TabServerFarmState } from 'app/modules/segments/TabServerFarms/store/reducer';
import { GetAllPublishNodesActionAnyPayload, GetAllPublishNodesActionSuccessPayload } from 'app/modules/segments/TabServerFarms/store/reducer/getAllPublishNodesReducer/payloads';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения всех доступных нодов
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const getAllPublishNodesReducer: TPartialReducerObject<TabServerFarmState, GetAllPublishNodesActionAnyPayload> =
    (state, action) => {
        const processId = TabServerFarmsProcessId.GetAllPublishNodes;
        const actions = TabServerFarmsActions.GetAllPublishNodes;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onStartProcessState: () => {
                return reducerStateProcessStart(state, processId, {
                    availableNodes: []
                });
            },
            onSuccessProcessState: () => {
                const payload = action.payload as GetAllPublishNodesActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    availableNodes: payload
                });
            }
        });
    };