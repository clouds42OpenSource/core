import { TabServerFarmsProcessId } from 'app/modules/segments/TabServerFarms';
import { TabServerFarmsConstants } from 'app/modules/segments/TabServerFarms/constants';
import { createReducerActions } from 'core/redux/functions';

/**
 * Все доступные actions для редюсера Segments -> "Ферма серверов"
 */
export const TabServerFarmsActions = {
    /**
     * Набор action на получение списка данных по ферме сервера
     */
    GetServerFarms: createReducerActions(TabServerFarmsConstants.reducerName, TabServerFarmsProcessId.GetServerFarms),

    /**
     * Набор action на создание фермы сервера
     */
    CreateServerFarm: createReducerActions(TabServerFarmsConstants.reducerName, TabServerFarmsProcessId.CreateServerFarm),

    /**
     * Набор action на редактирование фермы сервера
     */
    EditServerFarm: createReducerActions(TabServerFarmsConstants.reducerName, TabServerFarmsProcessId.EditServerFarm),

    /**
     * Набор action на получение фермы сервера по ID
     */
    GetServerFarm: createReducerActions(TabServerFarmsConstants.reducerName, TabServerFarmsProcessId.GetServerFarm),

    /**
     * Набор action на удаление фермы сервера по ID
     */
    DeleteServerFarm: createReducerActions(TabServerFarmsConstants.reducerName, TabServerFarmsProcessId.DeleteServerFarm),

    /**
     * Набор action на получение всех доступных нод
     */
    GetAllPublishNodes: createReducerActions(TabServerFarmsConstants.reducerName, TabServerFarmsProcessId.GetAllPublishNodes)
};