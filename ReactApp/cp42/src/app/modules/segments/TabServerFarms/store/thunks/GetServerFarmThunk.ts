import { TabServerFarmsActions } from 'app/modules/segments/TabServerFarms/store/actions';
import { GetServerFarmThunkParams } from 'app/modules/segments/TabServerFarms/store/reducer/getServerFarmReducer/params';
import { GetServerFarmActionFailedPayload, GetServerFarmActionStartPayload, GetServerFarmActionSuccessPayload } from 'app/modules/segments/TabServerFarms/store/reducer/getServerFarmReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = GetServerFarmActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = GetServerFarmActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = GetServerFarmActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = GetServerFarmThunkParams;

const { GetServerFarm: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = TabServerFarmsActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для получения фермы сервера
 */
export class GetServerFarmThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new GetServerFarmThunk().execute(args, args?.showLoadingProgress);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        return {
            condition: args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().TabServerFarmState
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        const api = InterlayerApiProxy.getSegmentsApiProxy().getServerFarmsTabApiProxyMethods();

        try {
            const response = await api.getServerFarm(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, args.inputParams!.serverFarmId);
            args.success(response);
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}