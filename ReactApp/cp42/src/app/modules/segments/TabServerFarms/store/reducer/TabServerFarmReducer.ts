import { getInitialMetadata, partialFillOf } from 'app/common/functions';
import { TabServerFarmsConstants } from 'app/modules/segments/TabServerFarms/constants';
import { TabServerFarmState } from 'app/modules/segments/TabServerFarms/store/reducer';
import { createServerFarmReducer } from 'app/modules/segments/TabServerFarms/store/reducer/createServerFarmReducer';
import { deleteServerFarmReducer } from 'app/modules/segments/TabServerFarms/store/reducer/deleteServerFarmReducer';
import { editServerFarmReducer } from 'app/modules/segments/TabServerFarms/store/reducer/editServerFarmReducer';
import { getAllPublishNodesReducer } from 'app/modules/segments/TabServerFarms/store/reducer/getAllPublishNodesReducer';
import { getServerFarmReducer } from 'app/modules/segments/TabServerFarms/store/reducer/getServerFarmReducer';
import { getServerFarmsReducer } from 'app/modules/segments/TabServerFarms/store/reducer/getServerFarmsReducer';
import { ServerFarmModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabServerFarmsApiProxy';
import { createReducer, initReducerState } from 'core/redux/functions';

/**
 * Начальное состояние редюсера Segments -> Ферма серверов
 */
const initialState = initReducerState<TabServerFarmState>(
    TabServerFarmsConstants.reducerName,
    {
        serverFarms: {
            metadata: getInitialMetadata(),
            records: []
        },
        serverFarm: partialFillOf<ServerFarmModel>(),
        availableNodes: [],
        hasSuccessFor: {
            hasServerFarmsReceived: false
        }
    }
);

/**
 * Объединённые части редюсера для всего состояния Segments -> Ферма серверов
 */
const partialReducers = [
    getServerFarmsReducer,
    deleteServerFarmReducer,
    getServerFarmReducer,
    getAllPublishNodesReducer,
    createServerFarmReducer,
    editServerFarmReducer
];

/**
 * Редюсер Segments -> Ферма серверов
 */
export const TabServerFarmReducer = createReducer(initialState, partialReducers);