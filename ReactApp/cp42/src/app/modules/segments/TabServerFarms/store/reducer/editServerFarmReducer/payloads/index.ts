import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения редактирования фермы сервера
 */
export type EditServerFarmActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении редактирования фермы сервера
 */
export type EditServerFarmActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении редактирования фермы сервера
 */
export type EditServerFarmActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при редактировании фермы сервера
 */
export type EditServerFarmActionAnyPayload =
    | EditServerFarmActionStartPayload
    | EditServerFarmActionFailedPayload
    | EditServerFarmActionSuccessPayload;