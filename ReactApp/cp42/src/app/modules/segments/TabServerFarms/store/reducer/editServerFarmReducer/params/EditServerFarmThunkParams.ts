import { ServerFarmParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabServerFarmsApiProxy';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для редактирования фермы сервера
 */
export type EditServerFarmThunkParams = IForceThunkParam & ServerFarmParams;