import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры на удаление фермы сервера
 */
export type DeleteServerFarmThunkParams = IForceThunkParam & {
    /**
     * ID фермы сервера
     */
    serverFarmId: string
};