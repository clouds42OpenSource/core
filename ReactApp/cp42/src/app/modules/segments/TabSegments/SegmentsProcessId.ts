/**
 * Процесы Segments
 */
export enum SegmentsProcessId {
    /**
     * Процесс на получение списка сегментов
     */
    GetCloudSegments = 'GET_CLOUD_SEGMENTS',

    /**
     * Процесс на получение элементов сегмента
     */
    GetSegmentElements = 'GET_SEGMENT_ELEMENTS',

    /**
     * Процесс на создание сегмента
     */
    CreateSegment = 'CREATE_SEGMENT',

    /**
     * Процесс на получение сегмента по ID
     */
    GetSegment = 'GET_SEGMENT',

    /**
     * Процесс на получение данные по аккаунтам сегмента
     */
    GetSegmentAccounts = 'GET_SEGMENT_ACCOUNTS',

    /**
     * Процесс на удаление сегмента
     */
    DeleteSegment = 'DeleteSegment',

    /**
     * Процесс на редактирование сегмента
     */
    EditSegment = 'EDIT_SEGMENT',

    /**
     * Процесс для получения кратких данных по сегментам
     */
    ReceiveSegmentsShortData = 'RECEIVE_SEGMENTS_SHORT_DATA',

    /**
     * Процесс для проверки возможности удаления файлового хранилища из доступных
     */
    CheckAbilityToDeleteFileStorage = 'CHECK_ABILITY_TO_DELETE_FILE_STORAGE',

    getSegmentDropdownList = 'GET_SEGMENT_DROPDOWN_LIST'
}