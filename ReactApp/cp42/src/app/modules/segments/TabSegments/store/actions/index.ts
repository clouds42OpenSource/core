import { SegmentsProcessId } from 'app/modules/segments/TabSegments';
import { SegmentsConsts } from 'app/modules/segments/TabSegments/constants';
import { createReducerActions } from 'core/redux/functions';

/**
 * Все доступные actions для редюсера Segments
 */
export const SegmentsActions = {
    /**
     * Набор action на получение списка сегментов
     */
    GetCloudSegments: createReducerActions(SegmentsConsts.reducerName, SegmentsProcessId.GetCloudSegments),

    /**
     * Набор action на получение элементов сегмента
     */
    GetSegmentElements: createReducerActions(SegmentsConsts.reducerName, SegmentsProcessId.GetSegmentElements),

    /**
     * Набор action на создание сегмента
     */
    CreateSegment: createReducerActions(SegmentsConsts.reducerName, SegmentsProcessId.CreateSegment),

    /**
     * Набор action на получение сегмента по ID
     */
    GetSegment: createReducerActions(SegmentsConsts.reducerName, SegmentsProcessId.GetSegment),

    /**
     * Набор action на получение данные по аккаунтам сегмента
     */
    GetSegmentAccounts: createReducerActions(SegmentsConsts.reducerName, SegmentsProcessId.GetSegmentAccounts),

    /**
     * Набор action на удаление сегмента
     */
    DeleteSegment: createReducerActions(SegmentsConsts.reducerName, SegmentsProcessId.DeleteSegment),

    /**
     * Набор action на редактирование сегмента
     */
    EditSegment: createReducerActions(SegmentsConsts.reducerName, SegmentsProcessId.EditSegment),

    /**
     * Действие для получения кратких данных по сегментам
     */
    ReceiveSegmentsShortData: createReducerActions(SegmentsConsts.reducerName, SegmentsProcessId.ReceiveSegmentsShortData),

    /**
     * Действие для проверки возможности удаления файлового хранилища из доступных
     */
    CheckAbilityToDeleteFileStorage: createReducerActions(SegmentsConsts.reducerName, SegmentsProcessId.CheckAbilityToDeleteFileStorage),

    //  получения списка для поиска

    GetSegmentsDropdownList: createReducerActions(SegmentsConsts.reducerName, SegmentsProcessId.getSegmentDropdownList),

};