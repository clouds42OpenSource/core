import { SegmentElementsDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabSegmentsApiProxy';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения элементов сегмента
 */
export type GetSegmentElementsActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения элементов сегмента
 */
export type GetSegmentElementsActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения элементов сегмента
 */
export type GetSegmentElementsActionSuccessPayload = ReducerActionSuccessPayload & SegmentElementsDataModel;

/**
 * Все возможные Action при получении элементов сегмента
 */
export type GetSegmentElementsActionAnyPayload =
    | GetSegmentElementsActionStartPayload
    | GetSegmentElementsActionFailedPayload
    | GetSegmentElementsActionSuccessPayload;