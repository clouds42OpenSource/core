import { SegmentAccountFilterParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabSegmentsApiProxy';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры на получение данных по аккаунтам сегмента
 */
export type GetSegmentAccountsThunkParams = IForceThunkParam & SegmentAccountFilterParams;