import { CloudServicesSegmentDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabSegmentsApiProxy/getSegment/data-models/CloudServicesSegmentDataModel';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения сегмента по ID
 */
export type GetSegmentActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения сегмента по ID
 */
export type GetSegmentActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения сегмента по ID
 */
export type GetSegmentActionSuccessPayload = ReducerActionSuccessPayload & CloudServicesSegmentDataModel;

/**
 * Все возможные Action при получении сегмента по ID
 */
export type GetSegmentActionAnyPayload =
    | GetSegmentActionStartPayload
    | GetSegmentActionFailedPayload
    | GetSegmentActionSuccessPayload;