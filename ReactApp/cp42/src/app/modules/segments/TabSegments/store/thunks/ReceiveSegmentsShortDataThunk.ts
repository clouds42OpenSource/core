import { SegmentsActions } from 'app/modules/segments/TabSegments/store/actions';
import { ReceiveSegmentsShortDataActionSuccessPayload } from 'app/modules/segments/TabSegments/store/reducers/receiveSegmentsShortDataReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { ReducerActionFailedPayload, ReducerActionStartPayload } from 'core/redux/interfaces';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, ReducerActionStartPayload, ReceiveSegmentsShortDataActionSuccessPayload, ReducerActionFailedPayload, IForceThunkParam>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<ReducerActionStartPayload, ReducerActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, ReducerActionStartPayload, ReceiveSegmentsShortDataActionSuccessPayload, ReducerActionFailedPayload, IForceThunkParam>;

const { ReceiveSegmentsShortData: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = SegmentsActions;

/**
 * Thunk получения кратких данных по сегментам
 */
export class ReceiveSegmentsShortDataThunk extends BaseReduxThunkObject<AppReduxStoreState, ReducerActionStartPayload, ReceiveSegmentsShortDataActionSuccessPayload, ReducerActionFailedPayload, IForceThunkParam> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: IForceThunkParam) {
        return new ReceiveSegmentsShortDataThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        const reducerState = args.getStore().SegmentsState;
        return {
            condition: !reducerState.hasSuccessFor.hasSegmentsShortDataReceived || args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().SegmentsState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        try {
            const segmentsApiProxy = InterlayerApiProxy.getSegmentsApiProxy().getTabSegmentsApiProxyMethods();
            const response = await segmentsApiProxy.receiveSegmentsShortData(RequestKind.SEND_BY_ROBOT);
            args.success(response);
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}