import { getInitialMetadata, partialFillOf } from 'app/common/functions';
import { SegmentsConsts } from 'app/modules/segments/TabSegments/constants';
import { SegmentsState } from 'app/modules/segments/TabSegments/store/reducers';
import { checkAbilityToDeleteFileStorageReducer } from 'app/modules/segments/TabSegments/store/reducers/checkAbilityToDeleteFileStorageReducer';
import { createSegmentReducer } from 'app/modules/segments/TabSegments/store/reducers/createSegmentReducer';
import { deleteSegmentReducer } from 'app/modules/segments/TabSegments/store/reducers/deleteSegmentReducer';
import { editSegmentReducer } from 'app/modules/segments/TabSegments/store/reducers/editSegmentReducer';
import { getCloudSegmentsReducer } from 'app/modules/segments/TabSegments/store/reducers/getCloudSegmentsReducer';
import { getSegmentAccountsReducer } from 'app/modules/segments/TabSegments/store/reducers/getSegmentAccountsReducer';
import { getCloudSegmentDropdownListReducer } from 'app/modules/segments/TabSegments/store/reducers/getSegmentDropdownListReducer';
import { getSegmentElementsReducer } from 'app/modules/segments/TabSegments/store/reducers/getSegmentElementsReducer';
import { getSegmentReducer } from 'app/modules/segments/TabSegments/store/reducers/getSegmentReducer';
import { receiveSegmentsShortDataReducer } from 'app/modules/segments/TabSegments/store/reducers/receiveSegmentsShortDataReducer';
import { CloudServicesSegmentDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabSegmentsApiProxy/getSegment/data-models/CloudServicesSegmentDataModel';
import { KeyValueStringLowerType } from 'app/web/api/SegmentsProxy';
import { createReducer, initReducerState } from 'core/redux/functions';

/**
 * Начальное состояние редюсера Segments
 */
const initialState = initReducerState<SegmentsState>(
    SegmentsConsts.reducerName,
    {
        segments: {
            metadata: getInitialMetadata(),
            records: []
        },
        segmentElements: {
            alpha83Versions: [],
            backupStorages: [],
            contentServers: [],
            coreHostingList: [],
            enterprise82Servers: [],
            enterprise83Servers: [],
            fileStorageServers: [],
            gatewayTerminals: [],
            sqlServers: [],
            stable82Versions: [],
            stable83Versions: [],
            terminalFarms: []
        },
        segmentDropdownList: {
            backupStorages: [
                partialFillOf<KeyValueStringLowerType>()
            ],
            fileStorageServers: [
                partialFillOf<KeyValueStringLowerType>()
            ],
            enterprise82Servers: [
                partialFillOf<KeyValueStringLowerType>()
            ],
            enterprise83Servers: [
                partialFillOf<KeyValueStringLowerType>()
            ],
            contentServers: [
                partialFillOf<KeyValueStringLowerType>()
            ],
            sqlServers: [
                partialFillOf<KeyValueStringLowerType>()
            ],
            terminalFarms: [
                partialFillOf<KeyValueStringLowerType>()
            ],
            gatewayTerminals: [
                partialFillOf<KeyValueStringLowerType>()
            ],
            stable82Versions: [''],
            stable83Versions: [''],
            alpha83Versions: [''],
            coreHostingList: [
                partialFillOf<KeyValueStringLowerType>()
            ],
            autoUpdateNodeList: [
                partialFillOf<KeyValueStringLowerType>()
            ]
        },
        segment: partialFillOf<CloudServicesSegmentDataModel>(),
        segmentAccounts: {
            metadata: getInitialMetadata(),
            records: []
        },
        segmentsShortData: {
            items: []
        },
        isCheckAbilityToDeleteFileStorage: false,
        hasSuccessFor: {
            hasSegmentsReceived: false,
            hasSegmentsShortDataReceived: false
        }
    }
);

/**
 * Объединённые части редюсера для всего состояния Segments
 */
const partialReducers = [
    getCloudSegmentsReducer,
    getSegmentElementsReducer,
    createSegmentReducer,
    getSegmentReducer,
    getSegmentAccountsReducer,
    deleteSegmentReducer,
    editSegmentReducer,
    receiveSegmentsShortDataReducer,
    checkAbilityToDeleteFileStorageReducer,
    getCloudSegmentDropdownListReducer
];

/**
 * Редюсер Segments
 */
export const SegmentsReducer = createReducer(initialState, partialReducers);