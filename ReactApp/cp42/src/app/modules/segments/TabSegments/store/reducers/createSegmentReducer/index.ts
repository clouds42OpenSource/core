import { reducerActionState } from 'app/common/functions/reducerActionState';
import { SegmentsProcessId } from 'app/modules/segments/TabSegments';
import { SegmentsActions } from 'app/modules/segments/TabSegments/store/actions';
import { SegmentsState } from 'app/modules/segments/TabSegments/store/reducers';
import { CreateSegmentActionAnyPayload, CreateSegmentActionSuccessPayload } from 'app/modules/segments/TabSegments/store/reducers/createSegmentReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для создания сегмента
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const createSegmentReducer: TPartialReducerObject<SegmentsState, CreateSegmentActionAnyPayload> =
    (state, action) => {
        const processId = SegmentsProcessId.CreateSegment;
        const actions = SegmentsActions.CreateSegment;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as CreateSegmentActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, payload);
            }
        });
    };