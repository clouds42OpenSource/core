import { reducerActionState } from 'app/common/functions/reducerActionState';
import { SegmentsProcessId } from 'app/modules/segments/TabSegments';
import { SegmentsActions } from 'app/modules/segments/TabSegments/store/actions';
import { SegmentsState } from 'app/modules/segments/TabSegments/store/reducers';
import { ReceiveSegmentsShortDataActionAnyPayload, ReceiveSegmentsShortDataActionSuccessPayload } from 'app/modules/segments/TabSegments/store/reducers/receiveSegmentsShortDataReducer/payloads';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер получения кратких данных по сегментам
 * @param state состояние
 * @param action действие
 */
export const receiveSegmentsShortDataReducer: TPartialReducerObject<SegmentsState, ReceiveSegmentsShortDataActionAnyPayload> =
    (state, action) => {
        const processId = SegmentsProcessId.ReceiveSegmentsShortData;
        const actions = SegmentsActions.ReceiveSegmentsShortData;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onStartProcessState: () => {
                return reducerStateProcessStart(state, processId, {
                    segmentsShortData: {
                        items: []
                    },
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasSegmentsShortDataReceived: false
                    }
                });
            },
            onSuccessProcessState: () => {
                const payload = action.payload as ReceiveSegmentsShortDataActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    segmentsShortData: {
                        items: [...payload.items]
                    },
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasSegmentsShortDataReceived: true
                    }
                });
            }
        });
    };