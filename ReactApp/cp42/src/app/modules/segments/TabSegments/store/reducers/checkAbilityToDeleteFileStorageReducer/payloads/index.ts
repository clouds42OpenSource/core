import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при успешном выполнении проверки возможности удаления файлового хранилища из доступных
 */
export type CheckAbilityToDeleteFileStorageActionSuccessPayload = ReducerActionSuccessPayload & boolean;

/**
 * Все возможные Action при проверки возможности удаления файлового хранилища из доступных
 */
export type CheckAbilityToDeleteFileStorageActionAnyPayload =
    | ReducerActionStartPayload
    | ReducerActionFailedPayload
    | CheckAbilityToDeleteFileStorageActionSuccessPayload;