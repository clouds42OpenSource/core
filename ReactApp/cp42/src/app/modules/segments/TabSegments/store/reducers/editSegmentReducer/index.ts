import { reducerActionState } from 'app/common/functions/reducerActionState';
import { SegmentsProcessId } from 'app/modules/segments/TabSegments';
import { SegmentsActions } from 'app/modules/segments/TabSegments/store/actions';
import { SegmentsState } from 'app/modules/segments/TabSegments/store/reducers';
import { EditSegmentActionAnyPayload, EditSegmentActionSuccessPayload } from 'app/modules/segments/TabSegments/store/reducers/editSegmentReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для редактирования сегмента
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const editSegmentReducer: TPartialReducerObject<SegmentsState, EditSegmentActionAnyPayload> =
    (state, action) => {
        const processId = SegmentsProcessId.EditSegment;
        const actions = SegmentsActions.EditSegment;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as EditSegmentActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, payload);
            }
        });
    };