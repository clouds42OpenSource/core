import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения создания сегмента
 */
export type CreateSegmentActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении создания сегмента
 */
export type CreateSegmentActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении создания сегмента
 */
export type CreateSegmentActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при создании сегмента
 */
export type CreateSegmentActionAnyPayload =
    | CreateSegmentActionStartPayload
    | CreateSegmentActionFailedPayload
    | CreateSegmentActionSuccessPayload;