import { CreateCloudServicesSegmentParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabSegmentsApiProxy/createSegment';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметр для создания сегмента
 */
export type CreateSegmentThunkParams = IForceThunkParam & CreateCloudServicesSegmentParams;