import { templateErrorText } from 'app/api';
import { SegmentsActions } from 'app/modules/segments/TabSegments/store/actions';
import { EditSegmentThunkParams } from 'app/modules/segments/TabSegments/store/reducers/editSegmentReducer/params';
import { EditSegmentActionFailedPayload, EditSegmentActionStartPayload, EditSegmentActionSuccessPayload } from 'app/modules/segments/TabSegments/store/reducers/editSegmentReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = EditSegmentActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = EditSegmentActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = EditSegmentActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = EditSegmentThunkParams;

const { EditSegment: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = SegmentsActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для редактирования сегмента
 */
export class EditSegmentThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new EditSegmentThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        return {
            condition: args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().SegmentsState
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        const api = InterlayerApiProxy.getSegmentsApiProxy().getTabSegmentsApiProxyMethods();

        if (args.inputParams && args.inputParams.alpha83VersionId === '') {
            args.inputParams.alpha83VersionId = null;
        }

        try {
            const response = await api.editSegment(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, args.inputParams!);
            if (response?.success === false) {
                throw new Error(response.message ?? templateErrorText);
            }
            args.success();
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}