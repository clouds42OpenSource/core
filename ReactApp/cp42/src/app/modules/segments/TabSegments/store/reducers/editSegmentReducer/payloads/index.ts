import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения редактирования сегмента
 */
export type EditSegmentActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении редактирования сегмента
 */
export type EditSegmentActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении редактирования сегмента
 */
export type EditSegmentActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при редактировании сегмента
 */
export type EditSegmentActionAnyPayload =
    | EditSegmentActionStartPayload
    | EditSegmentActionFailedPayload
    | EditSegmentActionSuccessPayload;