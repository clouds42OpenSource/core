import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения удаления сегмента
 */
export type DeleteSegmentActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении удаления сегмента
 */
export type DeleteSegmentActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении удаления сегмента
 */
export type DeleteSegmentActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при удалении сегмента
 */
export type DeleteSegmentActionAnyPayload =
    | DeleteSegmentActionStartPayload
    | DeleteSegmentActionFailedPayload
    | DeleteSegmentActionSuccessPayload;