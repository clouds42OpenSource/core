import { reducerActionState } from 'app/common/functions/reducerActionState';
import { SegmentsProcessId } from 'app/modules/segments/TabSegments';
import { SegmentsActions } from 'app/modules/segments/TabSegments/store/actions';
import { SegmentsState } from 'app/modules/segments/TabSegments/store/reducers';
import { GetSegmentAccountsActionAnyPayload, GetSegmentAccountsActionSuccessPayload } from 'app/modules/segments/TabSegments/store/reducers/getSegmentAccountsReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения данных по аккаунтам сегмента
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const getSegmentAccountsReducer: TPartialReducerObject<SegmentsState, GetSegmentAccountsActionAnyPayload> =
    (state, action) => {
        const processId = SegmentsProcessId.GetSegmentAccounts;
        const actions = SegmentsActions.GetSegmentAccounts;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as GetSegmentAccountsActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    segmentAccounts: payload
                });
            }
        });
    };