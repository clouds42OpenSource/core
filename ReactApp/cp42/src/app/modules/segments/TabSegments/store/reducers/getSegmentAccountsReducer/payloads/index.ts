import { SelectDataResultMetadataModel } from 'app/web/common/data-models';
import { SegmentAccountDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabSegmentsApiProxy';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения данных по аккаунтам сегмента
 */
export type GetSegmentAccountsActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения данных по аккаунтам сегмента
 */
export type GetSegmentAccountsActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения данных по аккаунтам сегмента
 */
export type GetSegmentAccountsActionSuccessPayload = ReducerActionSuccessPayload & SelectDataResultMetadataModel<SegmentAccountDataModel>;

/**
 * Все возможные Action при получении данных по аккаунтам сегмента
 */
export type GetSegmentAccountsActionAnyPayload =
    | GetSegmentAccountsActionStartPayload
    | GetSegmentAccountsActionFailedPayload
    | GetSegmentAccountsActionSuccessPayload;