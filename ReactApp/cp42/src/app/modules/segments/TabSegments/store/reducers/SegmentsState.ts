import { SegmentsProcessId } from 'app/modules/segments/TabSegments';
import { SegmentDropdownListResponseDto } from 'app/web/api/SegmentsProxy';
import { SelectDataMetadataResponseDto } from 'app/web/common';
import { KeyValueDataModel, SelectDataResultMetadataModel } from 'app/web/common/data-models';
import { CloudSegmentDataModel, SegmentAccountDataModel, SegmentElementsDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabSegmentsApiProxy';
import { CloudServicesSegmentDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabSegmentsApiProxy/getSegment/data-models/CloudServicesSegmentDataModel';
import { IReducerState } from 'core/redux/interfaces';

export type SegmentsState = IReducerState<SegmentsProcessId> & {
    segments: SelectDataResultMetadataModel<CloudSegmentDataModel>,
    segmentElements: SegmentElementsDataModel,
    segmentDropdownList: SegmentDropdownListResponseDto,
    segment: CloudServicesSegmentDataModel,
    segmentAccounts: SelectDataMetadataResponseDto<SegmentAccountDataModel>,
    segmentsShortData: {
        items: Array<KeyValueDataModel<string, string>>
    },
    isCheckAbilityToDeleteFileStorage: boolean,
    hasSuccessFor: {
        hasSegmentsReceived: boolean,
        hasSegmentsShortDataReceived: boolean
    }
};