import { reducerActionState } from 'app/common/functions/reducerActionState';
import { SegmentsProcessId } from 'app/modules/segments/TabSegments';
import { SegmentsActions } from 'app/modules/segments/TabSegments/store/actions';
import { SegmentsState } from 'app/modules/segments/TabSegments/store/reducers';
import { CheckAbilityToDeleteFileStorageActionAnyPayload, CheckAbilityToDeleteFileStorageActionSuccessPayload } from 'app/modules/segments/TabSegments/store/reducers/checkAbilityToDeleteFileStorageReducer/payloads';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для проверки возможности удаления файлового хранилища из доступных
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const checkAbilityToDeleteFileStorageReducer: TPartialReducerObject<SegmentsState, CheckAbilityToDeleteFileStorageActionAnyPayload> =
    (state, action) => {
        const processId = SegmentsProcessId.CheckAbilityToDeleteFileStorage;
        const actions = SegmentsActions.CheckAbilityToDeleteFileStorage;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onStartProcessState: () => {
                return reducerStateProcessStart(state, processId, {
                    isCheckAbilityToDeleteFileStorage: false
                });
            },
            onSuccessProcessState: () => {
                const payload = action.payload as CheckAbilityToDeleteFileStorageActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    isCheckAbilityToDeleteFileStorage: payload
                });
            }
        });
    };