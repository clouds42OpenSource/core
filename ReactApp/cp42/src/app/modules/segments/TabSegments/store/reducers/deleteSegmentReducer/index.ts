import { reducerActionState } from 'app/common/functions/reducerActionState';
import { SegmentsProcessId } from 'app/modules/segments/TabSegments';
import { SegmentsActions } from 'app/modules/segments/TabSegments/store/actions';
import { SegmentsState } from 'app/modules/segments/TabSegments/store/reducers';
import { DeleteSegmentActionAnyPayload, DeleteSegmentActionSuccessPayload } from 'app/modules/segments/TabSegments/store/reducers/deleteSegmentReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для удаления сегмента
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const deleteSegmentReducer: TPartialReducerObject<SegmentsState, DeleteSegmentActionAnyPayload> =
    (state, action) => {
        const processId = SegmentsProcessId.DeleteSegment;
        const actions = SegmentsActions.DeleteSegment;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as DeleteSegmentActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, payload);
            }
        });
    };