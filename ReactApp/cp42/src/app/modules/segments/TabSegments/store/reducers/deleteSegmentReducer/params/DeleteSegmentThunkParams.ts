import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для удаления сегмента
 */
export type DeleteSegmentThunkParams = IForceThunkParam & {
    /**
     * ID сегмента
     */
    segmentId: string
};