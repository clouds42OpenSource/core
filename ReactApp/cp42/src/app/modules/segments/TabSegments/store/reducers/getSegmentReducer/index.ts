import { partialFillOf } from 'app/common/functions';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { SegmentsProcessId } from 'app/modules/segments/TabSegments';
import { SegmentsActions } from 'app/modules/segments/TabSegments/store/actions';
import { SegmentsState } from 'app/modules/segments/TabSegments/store/reducers';
import { GetSegmentActionAnyPayload, GetSegmentActionSuccessPayload } from 'app/modules/segments/TabSegments/store/reducers/getSegmentReducer/payloads';
import { CloudServicesSegmentDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabSegmentsApiProxy/getSegment/data-models/CloudServicesSegmentDataModel';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения сегмента по ID
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const getSegmentReducer: TPartialReducerObject<SegmentsState, GetSegmentActionAnyPayload> =
    (state, action) => {
        const processId = SegmentsProcessId.GetSegment;
        const actions = SegmentsActions.GetSegment;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onStartProcessState: () => {
                return reducerStateProcessStart(state, processId, {
                    segment: partialFillOf<CloudServicesSegmentDataModel>()
                });
            },
            onSuccessProcessState: () => {
                const payload = action.payload as GetSegmentActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    segment: payload
                });
            }
        });
    };