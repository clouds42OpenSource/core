import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Параметры thunk для получения списка сегментов
 */
export type getSegmentsDropdownListThunkParams = IForceThunkParam;