import { SelectDataResultMetadataModel } from 'app/web/common/data-models';
import { CloudSegmentDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabSegmentsApiProxy';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения списка сегментов
 */
export type GetSegmentsActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения списка сегментов
 */
export type GetSegmentsActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения списка сегментов
 */
export type GetSegmentsActionSuccessPayload = ReducerActionSuccessPayload & {
    segments: SelectDataResultMetadataModel<CloudSegmentDataModel>;
    orderBy?: string;
};

/**
 * Все возможные Action при получении списка сегментов
 */
export type GetSegmentsActionAnyPayload =
    | GetSegmentsActionStartPayload
    | GetSegmentsActionFailedPayload
    | GetSegmentsActionSuccessPayload;