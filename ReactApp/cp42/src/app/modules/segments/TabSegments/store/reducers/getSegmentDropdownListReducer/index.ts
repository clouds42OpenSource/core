import { partialFillOf } from 'app/common/functions';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { SegmentsProcessId } from 'app/modules/segments/TabSegments';
import { SegmentsActions } from 'app/modules/segments/TabSegments/store/actions';
import { SegmentsState } from 'app/modules/segments/TabSegments/store/reducers';
import { GetSegmentsActionAnyPayload, GetSegmentsDropdownListActionSuccessPayload } from 'app/modules/segments/TabSegments/store/reducers/getSegmentDropdownListReducer/payloads';
import { KeyValueStringLowerType } from 'app/web/api/SegmentsProxy';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения списка сегментов
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const getCloudSegmentDropdownListReducer: TPartialReducerObject<SegmentsState, GetSegmentsActionAnyPayload> =
    (state, action) => {
        const processId = SegmentsProcessId.getSegmentDropdownList;
        const actions = SegmentsActions.GetSegmentsDropdownList;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onStartProcessState: () => {
                return reducerStateProcessStart(state, processId, {
                    segmentDropdownList: {
                        backupStorages: [
                            partialFillOf<KeyValueStringLowerType>()
                        ],
                        fileStorageServers: [
                            partialFillOf<KeyValueStringLowerType>()
                        ],
                        enterprise82Servers: [
                            partialFillOf<KeyValueStringLowerType>()
                        ],
                        enterprise83Servers: [
                            partialFillOf<KeyValueStringLowerType>()
                        ],
                        contentServers: [
                            partialFillOf<KeyValueStringLowerType>()
                        ],
                        sqlServers: [
                            partialFillOf<KeyValueStringLowerType>()
                        ],
                        terminalFarms: [
                            partialFillOf<KeyValueStringLowerType>()
                        ],
                        gatewayTerminals: [
                            partialFillOf<KeyValueStringLowerType>()
                        ],
                        stable82Versions: [''],
                        stable83Versions: [''],
                        alpha83Versions: [''],
                        coreHostingList: [
                            partialFillOf<KeyValueStringLowerType>()
                        ],
                        autoUpdateNodeList: [
                            partialFillOf<KeyValueStringLowerType>()
                        ]
                    }
                });
            },
            onSuccessProcessState: () => {
                const payload = action.payload as GetSegmentsDropdownListActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    segmentDropdownList: payload,
                });
            }
        });
    };