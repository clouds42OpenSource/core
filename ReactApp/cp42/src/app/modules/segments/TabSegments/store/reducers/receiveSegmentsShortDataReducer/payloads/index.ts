import { KeyValueDataModel } from 'app/web/common/data-models';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при успешном выполнении получения кратких данных по сегментам
 */
export type ReceiveSegmentsShortDataActionSuccessPayload = ReducerActionSuccessPayload & {
    /**
     * Массив записей
     */
    items: Array<KeyValueDataModel<string, string>>;
};

/**
 * Все возможные Action при получении кратких данных по сегментам
 */
export type ReceiveSegmentsShortDataActionAnyPayload =
    | ReducerActionStartPayload
    | ReceiveSegmentsShortDataActionSuccessPayload
    | ReducerActionFailedPayload;