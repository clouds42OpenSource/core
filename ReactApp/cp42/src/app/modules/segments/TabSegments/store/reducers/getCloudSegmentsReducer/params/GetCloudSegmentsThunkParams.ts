import { CloudSegmentFilterParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabSegmentsApiProxy';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Параметры thunk для получения списка сегментов
 */
export type GetCloudSegmentsThunkParams = IForceThunkParam & CloudSegmentFilterParams;