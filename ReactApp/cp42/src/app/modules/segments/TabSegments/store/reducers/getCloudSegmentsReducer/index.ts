import { reducerActionState } from 'app/common/functions/reducerActionState';
import { SegmentsProcessId } from 'app/modules/segments/TabSegments';
import { SegmentsActions } from 'app/modules/segments/TabSegments/store/actions';
import { SegmentsState } from 'app/modules/segments/TabSegments/store/reducers';
import { GetSegmentsActionAnyPayload, GetSegmentsActionSuccessPayload } from 'app/modules/segments/TabSegments/store/reducers/getCloudSegmentsReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения списка сегментов
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const getCloudSegmentsReducer: TPartialReducerObject<SegmentsState, GetSegmentsActionAnyPayload> =
    (state, action) => {
        const processId = SegmentsProcessId.GetCloudSegments;
        const actions = SegmentsActions.GetCloudSegments;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as GetSegmentsActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    segments: {
                        ...payload.segments,
                        orderBy: payload.orderBy
                    },
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasSegmentsReceived: true
                    }
                });
            }
        });
    };