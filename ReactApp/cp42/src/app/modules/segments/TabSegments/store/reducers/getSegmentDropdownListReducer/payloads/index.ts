import { SegmentDropdownListResponseDto } from 'app/web/api/SegmentsProxy';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения списка сегментов
 */
export type GetSegmentsDropdownListActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения списка сегментов
 */
export type GetSegmentsDropdownListActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения списка сегментов
 */
export type GetSegmentsDropdownListActionSuccessPayload = ReducerActionSuccessPayload & SegmentDropdownListResponseDto;

/**
 * Все возможные Action при получении списка сегментов
 */
export type GetSegmentsActionAnyPayload =
    | GetSegmentsDropdownListActionStartPayload
    | GetSegmentsDropdownListActionFailedPayload
    | GetSegmentsDropdownListActionSuccessPayload;