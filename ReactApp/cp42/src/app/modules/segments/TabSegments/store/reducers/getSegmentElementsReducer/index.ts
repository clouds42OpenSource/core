import { reducerActionState } from 'app/common/functions/reducerActionState';
import { SegmentsProcessId } from 'app/modules/segments/TabSegments';
import { SegmentsActions } from 'app/modules/segments/TabSegments/store/actions';
import { SegmentsState } from 'app/modules/segments/TabSegments/store/reducers';
import { GetSegmentElementsActionAnyPayload, GetSegmentElementsActionSuccessPayload } from 'app/modules/segments/TabSegments/store/reducers/getSegmentElementsReducer/payloads';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения элементов сегмента
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const getSegmentElementsReducer: TPartialReducerObject<SegmentsState, GetSegmentElementsActionAnyPayload> =
    (state, action) => {
        const processId = SegmentsProcessId.GetSegmentElements;
        const actions = SegmentsActions.GetSegmentElements;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onStartProcessState: () => {
                return reducerStateProcessStart(state, processId, {
                    segmentElements: {
                        alpha83Versions: [],
                        backupStorages: [],
                        contentServers: [],
                        coreHostingList: [],
                        enterprise82Servers: [],
                        enterprise83Servers: [],
                        fileStorageServers: [],
                        gatewayTerminals: [],
                        sqlServers: [],
                        stable82Versions: [],
                        stable83Versions: [],
                        terminalFarms: []
                    }
                });
            },
            onSuccessProcessState: () => {
                const payload = action.payload as GetSegmentElementsActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    segmentElements: payload
                });
            }
        });
    };