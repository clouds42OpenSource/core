import { EditCloudServicesSegmentParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabSegmentsApiProxy';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для редактирования сегмента
 */
export type EditSegmentThunkParams = IForceThunkParam & EditCloudServicesSegmentParams;