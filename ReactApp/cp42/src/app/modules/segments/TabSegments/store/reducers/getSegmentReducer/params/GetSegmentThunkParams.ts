import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметр для получения сегмента по ID
 */
export type GetSegmentThunkParams = IForceThunkParam & {
    /**
     * ID сегмента
     */
    segmentId: string
};