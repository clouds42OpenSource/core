import { CheckAbilityToDeleteFileStorageParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabSegmentsApiProxy/сheckAbilityToDeleteFileStorage';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для проверки возможности удаления файлового хранилища из доступных
 */
export type CheckAbilityToDeleteFileStorageThunkParams = IForceThunkParam & CheckAbilityToDeleteFileStorageParams;