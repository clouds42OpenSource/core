/**
 * Константы для модуля Segments -> Таб сегменты
 */
export const SegmentsConsts = {
    /**
     * Название редюсера для модуля Segments -> Таб сегменты
     */
    reducerName: 'TAB_SEGMENTS'
};