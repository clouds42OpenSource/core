/**
 * Константы для модуля Segments -> Хостинг
 */
export const TabHostingConstants = {
    /**
     * Название редюсера для модуля Segments -> Хостинг
     */
    reducerName: 'TAB_HOSTING_CONSTANTS'
};