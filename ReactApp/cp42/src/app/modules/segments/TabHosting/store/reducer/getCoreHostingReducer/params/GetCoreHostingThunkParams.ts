import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для получения хостинга облака
 */
export type GetCoreHostingThunkParams = IForceThunkParam & {
    /**
     * ID хостинга облака
     */
    coreHostingId: string
};