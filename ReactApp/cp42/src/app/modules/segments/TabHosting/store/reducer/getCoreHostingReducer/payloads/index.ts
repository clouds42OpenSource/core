import { CoreHostingDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabHostinApiProxy';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения хостинга
 */
export type GetHostingActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения хостинга
 */
export type GetHostingActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения хостинга
 */
export type GetHostingActionSuccessPayload = ReducerActionSuccessPayload & CoreHostingDataModel;

/**
 * Все возможные Action при получении хостинга
 */
export type GetHostingActionAnyPayload =
    | GetHostingActionStartPayload
    | GetHostingActionFailedPayload
    | GetHostingActionSuccessPayload;