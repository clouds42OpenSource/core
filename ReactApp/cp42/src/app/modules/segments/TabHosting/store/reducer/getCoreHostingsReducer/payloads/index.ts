import { SelectDataMetadataResponseDto } from 'app/web/common';
import { CoreHostingDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabHostinApiProxy';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения списка хостинга облака
 */
export type GetHostingsActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения списка хостинга облака
 */
export type GetHostingsActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения списка хостинга облака
 */
export type GetHostingsActionSuccessPayload = ReducerActionSuccessPayload & SelectDataMetadataResponseDto<CoreHostingDataModel>;

/**
 * Все возможные Action при получении списка хостинга облака
 */
export type GetHostingsActionAnyPayload =
    | GetHostingsActionStartPayload
    | GetHostingsActionFailedPayload
    | GetHostingsActionSuccessPayload;