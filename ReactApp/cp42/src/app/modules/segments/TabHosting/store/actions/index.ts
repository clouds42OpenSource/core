import { TabHostingProcessId } from 'app/modules/segments/TabHosting';
import { TabHostingConstants } from 'app/modules/segments/TabHosting/constants';
import { createReducerActions } from 'core/redux/functions';

/**
 * Все доступные actions для редюсера Segments -> "Хостинг"
 */
export const TabHostingActions = {
    /**
     * Набор action на получение списка данных по xостингу облака
     */
    GetHostings: createReducerActions(TabHostingConstants.reducerName, TabHostingProcessId.GetHostings),

    /**
     * Набор action на создание хостинга
     */
    CreateHosting: createReducerActions(TabHostingConstants.reducerName, TabHostingProcessId.CreateHosting),

    /**
     * Набор action на редактирование хостинга
     */
    EditHosting: createReducerActions(TabHostingConstants.reducerName, TabHostingProcessId.EditHosting),

    /**
     * Набор action на получение хостинга по ID
     */
    GetHosting: createReducerActions(TabHostingConstants.reducerName, TabHostingProcessId.GetHosting),

    /**
     * Набор action на удаление хостинга по ID
     */
    DeleteHosting: createReducerActions(TabHostingConstants.reducerName, TabHostingProcessId.DeleteHosting)
};