import { CoreHostingDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabHostinApiProxy';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для редактирования хостинга облака
 */
export type EditCoreHostingThunkParams = IForceThunkParam & CoreHostingDataModel;