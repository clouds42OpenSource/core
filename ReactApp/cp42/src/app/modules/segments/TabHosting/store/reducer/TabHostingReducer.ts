import { getInitialMetadata, partialFillOf } from 'app/common/functions';
import { TabHostingConstants } from 'app/modules/segments/TabHosting/constants';
import { TabHostingState } from 'app/modules/segments/TabHosting/store/reducer';
import { createCoreHostingReducer } from 'app/modules/segments/TabHosting/store/reducer/createCoreHostingReducer';
import { deleteCoreHostingReducer } from 'app/modules/segments/TabHosting/store/reducer/deleteCoreHostingReducer';
import { editCoreHostingReducer } from 'app/modules/segments/TabHosting/store/reducer/editCoreHostingReducer';
import { getCoreHostingReducer } from 'app/modules/segments/TabHosting/store/reducer/getCoreHostingReducer';
import { getCoreHostingsReducer } from 'app/modules/segments/TabHosting/store/reducer/getCoreHostingsReducer';
import { CoreHostingDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabHostinApiProxy';
import { createReducer, initReducerState } from 'core/redux/functions';

/**
 * Начальное состояние редюсера Segments -> Хостинг
 */
const initialState = initReducerState<TabHostingState>(
    TabHostingConstants.reducerName,
    {
        cloudHostings: {
            metadata: getInitialMetadata(),
            records: []
        },
        cloudHosting: partialFillOf<CoreHostingDataModel>(),
        hasSuccessFor: {
            hasCloudHostingsReceived: false
        }
    }
);

/**
 * Объединённые части редюсера для всего состояния Segments -> Хостинг
 */
const partialReducers = [
    getCoreHostingsReducer,
    deleteCoreHostingReducer,
    getCoreHostingReducer,
    createCoreHostingReducer,
    editCoreHostingReducer
];

/**
 * Редюсер Segments -> Хостинг
 */
export const TabHostingReducer = createReducer(initialState, partialReducers);