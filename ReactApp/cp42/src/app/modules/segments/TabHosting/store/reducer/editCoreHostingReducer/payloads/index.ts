import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения редактирования хостинга
 */
export type EditHostingActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении редактирования хостинга
 */
export type EditHostingActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении редактирования хостинга
 */
export type EditHostingActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при редактировании хостинга
 */
export type EditHostingActionAnyPayload =
    | EditHostingActionStartPayload
    | EditHostingActionFailedPayload
    | EditHostingActionSuccessPayload;