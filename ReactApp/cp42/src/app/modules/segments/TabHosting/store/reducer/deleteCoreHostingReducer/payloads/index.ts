import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения удаления хостинга
 */
export type DeleteHostingActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении удаления хостинга
 */
export type DeleteHostingActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении удаления хостинга
 */
export type DeleteHostingActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при удалении хостинга
 */
export type DeleteHostingActionAnyPayload =
    | DeleteHostingActionStartPayload
    | DeleteHostingActionFailedPayload
    | DeleteHostingActionSuccessPayload;