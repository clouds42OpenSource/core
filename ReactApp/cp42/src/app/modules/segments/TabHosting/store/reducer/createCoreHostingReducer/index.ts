import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabHostingProcessId } from 'app/modules/segments/TabHosting';
import { TabHostingActions } from 'app/modules/segments/TabHosting/store/actions';
import { TabHostingState } from 'app/modules/segments/TabHosting/store/reducer';
import { CreateHostingActionAnyPayload, CreateHostingActionSuccessPayload } from 'app/modules/segments/TabHosting/store/reducer/createCoreHostingReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для создания хостинга облака
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const createCoreHostingReducer: TPartialReducerObject<TabHostingState, CreateHostingActionAnyPayload> =
    (state, action) => {
        const processId = TabHostingProcessId.CreateHosting;
        const actions = TabHostingActions.CreateHosting;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as CreateHostingActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, payload);
            }
        });
    };