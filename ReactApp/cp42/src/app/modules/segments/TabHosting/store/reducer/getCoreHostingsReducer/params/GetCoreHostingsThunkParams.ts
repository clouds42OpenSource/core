import { CoreHostingFilterParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabHostinApiProxy';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для получения списка хостинга облака
 */
export type GetCoreHostingsThunkParams = IForceThunkParam & CoreHostingFilterParams;