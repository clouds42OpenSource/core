import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabHostingProcessId } from 'app/modules/segments/TabHosting';
import { TabHostingActions } from 'app/modules/segments/TabHosting/store/actions';
import { TabHostingState } from 'app/modules/segments/TabHosting/store/reducer';
import { EditHostingActionAnyPayload, EditHostingActionSuccessPayload } from 'app/modules/segments/TabHosting/store/reducer/editCoreHostingReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для редактирования хостинга облака
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const editCoreHostingReducer: TPartialReducerObject<TabHostingState, EditHostingActionAnyPayload> =
    (state, action) => {
        const processId = TabHostingProcessId.EditHosting;
        const actions = TabHostingActions.EditHosting;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as EditHostingActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, payload);
            }
        });
    };