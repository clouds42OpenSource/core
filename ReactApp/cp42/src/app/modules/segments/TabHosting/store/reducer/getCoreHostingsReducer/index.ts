import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabHostingProcessId } from 'app/modules/segments/TabHosting';
import { TabHostingActions } from 'app/modules/segments/TabHosting/store/actions';
import { TabHostingState } from 'app/modules/segments/TabHosting/store/reducer';
import { GetHostingsActionAnyPayload, GetHostingsActionSuccessPayload } from 'app/modules/segments/TabHosting/store/reducer/getCoreHostingsReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения списка данных по хостингу облака
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const getCoreHostingsReducer: TPartialReducerObject<TabHostingState, GetHostingsActionAnyPayload> =
    (state, action) => {
        const processId = TabHostingProcessId.GetHostings;
        const actions = TabHostingActions.GetHostings;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as GetHostingsActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    cloudHostings: payload,
                    hasSuccessFor: {
                        ...state,
                        hasCloudHostingsReceived: true
                    }
                });
            }
        });
    };