import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения создания хостинга
 */
export type CreateHostingActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении создания хостинга
 */
export type CreateHostingActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении создания хостинга
 */
export type CreateHostingActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при создании хостинга
 */
export type CreateHostingActionAnyPayload =
    | CreateHostingActionStartPayload
    | CreateHostingActionFailedPayload
    | CreateHostingActionSuccessPayload;