import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для удаления хостинга
 */
export type DeleteCoreHostingThunkParams = IForceThunkParam & {
    /**
     * ID хостинга облака
     */
    coreHostingId: string
};