import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabHostingProcessId } from 'app/modules/segments/TabHosting';
import { TabHostingActions } from 'app/modules/segments/TabHosting/store/actions';
import { TabHostingState } from 'app/modules/segments/TabHosting/store/reducer';
import { DeleteHostingActionAnyPayload, DeleteHostingActionSuccessPayload } from 'app/modules/segments/TabHosting/store/reducer/deleteCoreHostingReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для удаления хостинга облака
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const deleteCoreHostingReducer: TPartialReducerObject<TabHostingState, DeleteHostingActionAnyPayload> =
    (state, action) => {
        const processId = TabHostingProcessId.DeleteHosting;
        const actions = TabHostingActions.DeleteHosting;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as DeleteHostingActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, payload);
            }
        });
    };