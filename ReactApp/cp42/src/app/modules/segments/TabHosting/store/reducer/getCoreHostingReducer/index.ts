import { partialFillOf } from 'app/common/functions';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabHostingProcessId } from 'app/modules/segments/TabHosting';
import { TabHostingActions } from 'app/modules/segments/TabHosting/store/actions';
import { TabHostingState } from 'app/modules/segments/TabHosting/store/reducer';
import { GetHostingActionAnyPayload, GetHostingActionSuccessPayload } from 'app/modules/segments/TabHosting/store/reducer/getCoreHostingReducer/payloads';
import { CoreHostingDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabHostinApiProxy';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения хостинга облака
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const getCoreHostingReducer: TPartialReducerObject<TabHostingState, GetHostingActionAnyPayload> =
    (state, action) => {
        const processId = TabHostingProcessId.GetHosting;
        const actions = TabHostingActions.GetHosting;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onStartProcessState: () => {
                return reducerStateProcessStart(state, processId, {
                    cloudHosting: partialFillOf<CoreHostingDataModel>()
                });
            },
            onSuccessProcessState: () => {
                const payload = action.payload as GetHostingActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    cloudHosting: payload
                });
            }
        });
    };