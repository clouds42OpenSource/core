import { CreateCoreHostingParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabHostinApiProxy';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для создания хостинга облака
 */
export type CreateCoreHostingThunkParams = IForceThunkParam & CreateCoreHostingParams;