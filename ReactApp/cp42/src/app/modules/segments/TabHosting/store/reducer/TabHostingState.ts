import { TabHostingProcessId } from 'app/modules/segments/TabHosting';
import { SelectDataMetadataResponseDto } from 'app/web/common';
import { CoreHostingDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabHostinApiProxy';
import { IReducerState } from 'core/redux/interfaces';

/**
 * Состояние редюсера для работы с процесами Segments -> Хостинг
 */
export type TabHostingState = IReducerState<TabHostingProcessId> & {
    /**
     * Массив хостинга
     */
    cloudHostings: SelectDataMetadataResponseDto<CoreHostingDataModel>,

    /**
     * Модель хостинга
     */
    cloudHosting: CoreHostingDataModel,

    /**
     * содержит ярлыки для фиксации загрузок данных, была ли загрузка или ещё нет
     */
    hasSuccessFor: {
        /**
         * Если true, то массив хостинга получены
         */
        hasCloudHostingsReceived: boolean
    };
};