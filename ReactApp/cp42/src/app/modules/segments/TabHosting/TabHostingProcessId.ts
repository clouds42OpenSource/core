/**
 * Процесы Segments -> "Хостинг"
 */
export enum TabHostingProcessId {
    /**
     * Процесс на получение списка данных по хостингу облака
     */
    GetHostings = 'GET_HOSTINGS',

    /**
     * Процесс на создание хостинга
     */
    CreateHosting = 'CREATE_HOSTING',

    /**
     * Процесс на редактирование хостинга
     */
    EditHosting = 'EDIT_HOSTING',

    /**
     * Процесс на получение хостинга по ID
     */
    GetHosting = 'GET_HOSTING',

    /**
     * Процесс на удаление хостинга по ID
     */
    DeleteHosting = 'DELETE_HOSTING'
}