/**
 * Процесы DatabaseCard
 */
export enum CloudServicesProcessId {
    /**
     * ID процеса для получения всех записей справочника CloudService
     */
    ReceiveCloudServices = 'RECEIVE_CLOUD_SERVICES',

    /**
     * ID процеса для применения изменений выбранной записи из справочника CloudService
     */
    UpdateCloudService = 'UPDATE_CLOUD_SERVICE',

    /**
     * ID процеса для применения добавления записи в справочник CloudService
     */
    AddCloudService = 'ADD_CLOUD_SERVICE'

}