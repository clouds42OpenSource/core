import { CloudServicesProcessId } from 'app/modules/cloudServices/CloudServicesProcessId';
import { CloudServicesConstants } from 'app/modules/cloudServices/consts';
import { createReducerActions } from 'core/redux/functions';

/**
 * Все доступные actions дле редюсера CloudServices
 */
export const CloudServicesActions = {
    /**
     * Набор action на получения записей из справочника CloudService
     */
    receiveCloudServices: createReducerActions(CloudServicesConstants.reducerName, CloudServicesProcessId.ReceiveCloudServices),

    /**
     * Набор action для обновления записи в справочнике CloudService
     */
    updateCloudService: createReducerActions(CloudServicesConstants.reducerName, CloudServicesProcessId.UpdateCloudService),

    /**
     * Набор action для добавления записи в справочнике CloudService
     */
    addCloudService: createReducerActions(CloudServicesConstants.reducerName, CloudServicesProcessId.AddCloudService),
};