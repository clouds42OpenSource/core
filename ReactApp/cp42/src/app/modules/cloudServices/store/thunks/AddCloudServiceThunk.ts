import { CloudServicesActions } from 'app/modules/cloudServices/store/actions';
import { AddCloudServiceThunkParams } from 'app/modules/cloudServices/store/reducers/addCloudServiceReducer/params';
import { AddCloudServiceActionFailedPayload, AddCloudServiceActionStartPayload, AddCloudServiceActionSuccessPayload } from 'app/modules/cloudServices/store/reducers/addCloudServiceReducer/payloads';
import { ReceiveCloudServicesThunk } from 'app/modules/cloudServices/store/thunks/ReceiveCloudServicesThunk';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = AddCloudServiceActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = AddCloudServiceActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = AddCloudServiceActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = AddCloudServiceThunkParams;

const { addCloudService: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = CloudServicesActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для запроса обновления записи справочника CloudServices
 */
export class AddCloudServiceThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new AddCloudServiceThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        return {
            condition: args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().CloudServicesState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        try {
            const requestArgs = args.inputParams!;
            const cloudServicesApi = InterlayerApiProxy.getCloudServicesApi();

            const response = await cloudServicesApi.addCloudService(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, {
                cloudServiceId: requestArgs.cloudServiceId,
                jsonWebToken: requestArgs.jsonWebToken,
                serviceCaption: requestArgs.serviceCaption
            });

            if (typeof response === 'string') {
                args.failed({
                    error: new Error(response)
                });
            } else {
                args.thunkDispatch(new ReceiveCloudServicesThunk(), { force: true });

                requestArgs.id = response.id;

                args.success({
                    cloudService: requestArgs
                });
            }
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}