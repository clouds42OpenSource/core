import { CloudServicesActions } from 'app/modules/cloudServices/store/actions';
import { ReceiveCloudServicesThunkParams } from 'app/modules/cloudServices/store/reducers/receiveCloudServicesReducer/params';
import { ReceiveCloudServicesActionFailedPayload, ReceiveCloudServicesActionStartPayload, ReceiveCloudServicesActionSuccessPayload } from 'app/modules/cloudServices/store/reducers/receiveCloudServicesReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = ReceiveCloudServicesActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = ReceiveCloudServicesActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = ReceiveCloudServicesActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = ReceiveCloudServicesThunkParams;

const { receiveCloudServices: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = CloudServicesActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для запроса списка записей справочника CloudServices
 */
export class ReceiveCloudServicesThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new ReceiveCloudServicesThunk().execute(args);
    }

    protected get startActionValue(): string { return START_ACTION; }

    protected get successActionValue(): string { return SUCCESS_ACTION; }

    protected get failedActionValue(): string { return FAILED_ACTION; }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        const cloudServicesState = args.getStore().CloudServicesState;
        return {
            condition: !cloudServicesState.hasSuccessFor.hasCloudServicesReceived || args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().CloudServicesState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        try {
            const cloudServicesApi = InterlayerApiProxy.getCloudServicesApi();

            const cloudServices = await cloudServicesApi.receiveCloudServices(RequestKind.SEND_BY_USER_SYNCHRONOUSLY);

            args.success({
                cloudServices
            });
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}