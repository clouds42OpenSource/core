import { CloudServiceItemDataModel } from 'app/web/InterlayerApiProxy/CloudServicesApiProxy/receiveCloudServices';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте получения справочника CloudServices
 */
export type ReceiveCloudServicesActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при ошибки получения справочника CloudServices
 */
export type ReceiveCloudServicesActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном получении справочника CloudServices
 */
export type ReceiveCloudServicesActionSuccessPayload = ReducerActionSuccessPayload & {
    cloudServices: Array<CloudServiceItemDataModel>;
};

/**
 * Все возможные Action при получении списка записей справочника CloudServices
 */
export type ReceiveCloudServicesActionAnyPayload =
    | ReceiveCloudServicesActionStartPayload
    | ReceiveCloudServicesActionSuccessPayload
    | ReceiveCloudServicesActionFailedPayload;