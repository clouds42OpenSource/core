import { CloudServiceItemDataModel } from 'app/web/InterlayerApiProxy/CloudServicesApiProxy/receiveCloudServices';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте добавления записи справочника CloudService
 */
export type AddCloudServiceActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при ошибки добавления записи справочника CloudService
 */
export type AddCloudServiceActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном добавлении записи справочника CloudService
 */
export type AddCloudServiceActionSuccessPayload = ReducerActionSuccessPayload & {
    cloudService: CloudServiceItemDataModel;
};

/**
 * Все возможные Action при добавлении записи справочника CloudService
 */
export type AddCloudServiceActionAnyPayload =
    | AddCloudServiceActionStartPayload
    | AddCloudServiceActionSuccessPayload
    | AddCloudServiceActionFailedPayload;