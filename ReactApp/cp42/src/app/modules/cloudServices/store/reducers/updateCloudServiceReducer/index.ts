import { reducerActionState } from 'app/common/functions/reducerActionState';
import { CloudServicesProcessId } from 'app/modules/cloudServices';
import { CloudServicesActions } from 'app/modules/cloudServices/store/actions';
import { CloudServicesState } from 'app/modules/cloudServices/store/reducers/CloudServicesState';
import { UpdateCloudServiceActionAnyPayload, UpdateCloudServiceActionSuccessPayload } from 'app/modules/cloudServices/store/reducers/updateCloudServiceReducer/payloads';
import { reducerStateProcessFail, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для обновления записи справочника CloudServices
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const updateCloudServiceReducer: TPartialReducerObject<CloudServicesState, UpdateCloudServiceActionAnyPayload> =
    (state, action) => {
        const processId = CloudServicesProcessId.UpdateCloudService;
        const actions = CloudServicesActions.updateCloudService;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as UpdateCloudServiceActionSuccessPayload;

                const foundCloudServiceIndex = state.cloudServices.findIndex(item => item.id === payload.cloudService.id);

                if (Number.isNaN(foundCloudServiceIndex)) {
                    return reducerStateProcessFail(state, processId, new Error(`Обновляемый CloudService(${ payload.cloudService.id }) не найден.`));
                }

                const cloudServices = [...state.cloudServices];
                cloudServices[foundCloudServiceIndex] = { ...payload.cloudService };

                return reducerStateProcessSuccess(state, processId, {
                    cloudServices
                });
            }
        });
    };