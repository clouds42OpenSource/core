import { reducerActionState } from 'app/common/functions/reducerActionState';
import { CloudServicesProcessId } from 'app/modules/cloudServices';
import { CloudServicesActions } from 'app/modules/cloudServices/store/actions';
import { AddCloudServiceActionAnyPayload } from 'app/modules/cloudServices/store/reducers/addCloudServiceReducer/payloads';
import { CloudServicesState } from 'app/modules/cloudServices/store/reducers/CloudServicesState';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для добавления записи в справочник CloudServices
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const addCloudServiceReducer: TPartialReducerObject<CloudServicesState, AddCloudServiceActionAnyPayload> =
    (state, action) => {
        const processId = CloudServicesProcessId.AddCloudService;
        const actions = CloudServicesActions.addCloudService;

        return reducerActionState({
            action,
            actions,
            processId,
            state
        });
    };