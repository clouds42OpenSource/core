import { CloudServicesConstants } from 'app/modules/cloudServices/consts';
import { addCloudServiceReducer } from 'app/modules/cloudServices/store/reducers/addCloudServiceReducer';
import { CloudServicesState } from 'app/modules/cloudServices/store/reducers/CloudServicesState';
import { receiveCloudServicesReducer } from 'app/modules/cloudServices/store/reducers/receiveCloudServicesReducer';
import { updateCloudServiceReducer } from 'app/modules/cloudServices/store/reducers/updateCloudServiceReducer';
import { createReducer, initReducerState } from 'core/redux/functions';

/**
 * Начальное состояние редюсера CloudServices
 */
const initialState = initReducerState<CloudServicesState>(
    CloudServicesConstants.reducerName,
    {
        cloudServices: [],
        error: null,
        hasSuccessFor: {
            hasCloudServicesReceived: false
        }
    }
);

/**
 * Объединённые части редюсера для всего состояния CloudServices
 */
const partialReducers = [
    receiveCloudServicesReducer,
    updateCloudServiceReducer,
    addCloudServiceReducer
];

/**
 * Редюсер CloudServices
 */
export const cloudServicesReducer = createReducer(initialState, partialReducers);