import { CloudServiceItemDataModel } from 'app/web/InterlayerApiProxy/CloudServicesApiProxy/receiveCloudServices';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте обновления записи справочника CloudService
 */
export type UpdateCloudServiceActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при ошибки обновления записи справочника CloudService
 */
export type UpdateCloudServiceActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном обновлении записи справочника CloudService
 */
export type UpdateCloudServiceActionSuccessPayload = ReducerActionSuccessPayload & {
    cloudService: CloudServiceItemDataModel;
};

/**
 * Все возможные Action при обновлении записи справочника CloudService
 */
export type UpdateCloudServiceActionAnyPayload =
    | UpdateCloudServiceActionStartPayload
    | UpdateCloudServiceActionSuccessPayload
    | UpdateCloudServiceActionFailedPayload;