import { CloudServiceItemDataModel } from 'app/web/InterlayerApiProxy/CloudServicesApiProxy/receiveCloudServices';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для добавления записи справочника CloudService
 */
export type AddCloudServiceThunkParams = IForceThunkParam & CloudServiceItemDataModel;