import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для загрузки записей справочника CloudService
 */
export type ReceiveCloudServicesThunkParams = IForceThunkParam;