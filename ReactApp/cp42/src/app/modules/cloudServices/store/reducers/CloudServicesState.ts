import { CloudServicesProcessId } from 'app/modules/cloudServices/CloudServicesProcessId';
import { CloudServiceItemDataModel } from 'app/web/InterlayerApiProxy/CloudServicesApiProxy/receiveCloudServices';
import { IReducerState } from 'core/redux/interfaces';

/**
 * Состояние редюсера для справочника CloudServices
 */
export type CloudServicesState = IReducerState<CloudServicesProcessId> & {

    /**
     * Записи справочника CloudServices
     */
    cloudServices: Array<CloudServiceItemDataModel>;
    error: string | null;
    /**
     * содержит ярлыки для фиксации загрузок данных, была ли загрузка или ещё нет
     */
    hasSuccessFor: {
        /**
         * Если true, то справочник CloudService уже был загружён
         */
        hasCloudServicesReceived: boolean

    };
};