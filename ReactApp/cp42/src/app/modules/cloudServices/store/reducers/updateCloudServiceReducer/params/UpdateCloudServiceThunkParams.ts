import { CloudServiceItemDataModel } from 'app/web/InterlayerApiProxy/CloudServicesApiProxy/receiveCloudServices';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для обновления записи справочника CloudService
 */
export type UpdateCloudServiceThunkParams = IForceThunkParam & CloudServiceItemDataModel;