import { reducerActionState } from 'app/common/functions/reducerActionState';
import { CloudServicesProcessId } from 'app/modules/cloudServices';
import { CloudServicesActions } from 'app/modules/cloudServices/store/actions';
import { CloudServicesState } from 'app/modules/cloudServices/store/reducers/CloudServicesState';
import { ReceiveCloudServicesActionAnyPayload, ReceiveCloudServicesActionSuccessPayload } from 'app/modules/cloudServices/store/reducers/receiveCloudServicesReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения справочника CloudServices
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const receiveCloudServicesReducer: TPartialReducerObject<CloudServicesState, ReceiveCloudServicesActionAnyPayload> =
    (state, action) => {
        const processId = CloudServicesProcessId.ReceiveCloudServices;
        const actions = CloudServicesActions.receiveCloudServices;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as ReceiveCloudServicesActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    cloudServices: [
                        ...payload.cloudServices,
                    ],
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasCloudServicesReceived: true
                    }
                });
            }
        });
    };