import { createReducerActions } from 'core/redux/functions';
import { MyCompanyConstants } from 'app/modules/myCompany/consts';
import { MyCompanyProcessId } from 'app/modules/myCompany/MyCompanyProcessId';

/**
 * Все действия для редюсера MyCompany
 */
export const MyCompanyAction = {
    /**
     * Действие для получения данных о компании
     */
    ReceiveMyCompanyData: createReducerActions(MyCompanyConstants.reducerName, MyCompanyProcessId.ReceiveMyCompanyData),

    /**
     * Действия для редактирования данных компании
     */
    EditMyCompanyData: createReducerActions(MyCompanyConstants.reducerName, MyCompanyProcessId.EditMyCompanyData),

    /**
     * Действия для получения сегмента компании
     */
    ReceiveMyCompanySegment: createReducerActions(MyCompanyConstants.reducerName, MyCompanyProcessId.ReceiveMyCompanySegmentData),

    /**
     * Действия для смены сегмента компании
     */
    ChangeMyCompanySegment: createReducerActions(MyCompanyConstants.reducerName, MyCompanyProcessId.ChangeMyCompanySegmentData),

    /**
     * Действия для получения БД для миграции
     */
    ReceiveDatabaseMigration: createReducerActions(MyCompanyConstants.reducerName, MyCompanyProcessId.ReceiveDatabaseMigration),

    /**
     * Действия для миграции БД
     */
    ChangeDatabaseMigration: createReducerActions(MyCompanyConstants.reducerName, MyCompanyProcessId.ChangeDatabaseMigration)
};