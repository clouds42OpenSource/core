import { MyCompanyAction } from 'app/modules/myCompany/store/actions';
import { ChangeDatabaseMigrationThunkParams } from 'app/modules/myCompany/store/reducers/changeDatabaseMigration/params';
import { ChangeDatabaseMigrationActionFailedPayload, ChangeDatabaseMigrationActionStartPayload, ChangeDatabaseMigrationActionSuccessPayload } from 'app/modules/myCompany/store/reducers/changeDatabaseMigration/payload';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсере при вызове действия START_ACTION
 */
type TActionStartPayload = ChangeDatabaseMigrationActionStartPayload;

/**
 * Тип данных в редюсере при вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = ChangeDatabaseMigrationActionSuccessPayload;

/**
 * Тип данных в редюсере при вызове действия FAILED_ACTION
 */
type TActionFailedPayload = ChangeDatabaseMigrationActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParam = ChangeDatabaseMigrationThunkParams;

const { ChangeDatabaseMigration: { START_ACTION, FAILED_ACTION, SUCCESS_ACTION } } = MyCompanyAction;

/**
 * Тип параметров Thunk-и при старте выполнения
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParam>;

/**
 * Тип результата после подготовки выполнения Think-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParam>;

/**
 * Thunk для получения сегментов компании
 */
export class ChangeDatabaseMigrationThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParam> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args: TInputParam) {
        return new ChangeDatabaseMigrationThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        return {
            condition: args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().MyCompanyState
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        try {
            const requestArgs = args.inputParams!;
            const myCompanyApiProxy = InterlayerApiProxy.getDatabaseMigrationApiProxy();

            await myCompanyApiProxy.changeDatabaseMigration(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, {
                databases: requestArgs.databases,
                storage: requestArgs.storage
            });

            args.success();
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}