import { MyCompanyAction } from 'app/modules/myCompany/store/actions';
import { ReceiveDatabaseMigrationThunkParams } from 'app/modules/myCompany/store/reducers/receiveDatabaseMigration/params';
import { ReceiveDatabaseMigrationActionFailedPayload, ReceiveDatabaseMigrationActionStartPayload, ReceiveDatabaseMigrationActionSuccessPayload } from 'app/modules/myCompany/store/reducers/receiveDatabaseMigration/payload';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = ReceiveDatabaseMigrationActionStartPayload;

/**
 * Тип данных в редюсер при вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = ReceiveDatabaseMigrationActionSuccessPayload;

/**
 * Тип данных в редюсер при вызове действия FAILED_ACTION
 */
type TActionFailedPayload = ReceiveDatabaseMigrationActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParam = ReceiveDatabaseMigrationThunkParams;

const { ReceiveDatabaseMigration: { START_ACTION, FAILED_ACTION, SUCCESS_ACTION } } = MyCompanyAction;

/**
 * Тип параметров Thunk-и при старте выполнения
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParam>;

/**
 * Тип результата после подготовки выполнения Think-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParam>;

/**
 * Thunk для получения БД для миграции
 */
export class ReceiveDatabaseMigrationThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParam> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParam) {
        return new ReceiveDatabaseMigrationThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        const reducerState = args.getStore().MyCompanyState;
        return {
            condition: !reducerState.hasSuccessFor.hasDatabaseMigrationReceived || args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().MyCompanyState
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        try {
            const api = InterlayerApiProxy.getDatabaseMigrationApiProxy();
            const response = await api.receiveDatabaseMigration(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, args.inputParams!);

            args.success({
                databaseMigration: response
            });
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}