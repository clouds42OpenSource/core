import { MyCompanyAction } from 'app/modules/myCompany/store/actions';
import { ChangeMyCompanySegmentThunkParams } from 'app/modules/myCompany/store/reducers/changeMyCompanySegmentData/params';
import { ChangeMyCompanySegmentActionFailedPayload, ChangeMyCompanySegmentActionStartPayload, ChangeMyCompanySegmentActionSuccessPayload } from 'app/modules/myCompany/store/reducers/changeMyCompanySegmentData/payload';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсере при вызове действия START_ACTION
 */
type TActionStartPayload = ChangeMyCompanySegmentActionStartPayload;

/**
 * Тип данных в редюсере при вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = ChangeMyCompanySegmentActionSuccessPayload;

/**
 * Тип данных в редюсере при вызове действия FAILED_ACTION
 */
type TActionFailedPayload = ChangeMyCompanySegmentActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParam = ChangeMyCompanySegmentThunkParams;

const { ChangeMyCompanySegment: { START_ACTION, FAILED_ACTION, SUCCESS_ACTION } } = MyCompanyAction;

/**
 * Тип параметров Thunk-и при старте выполнения
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParam>;

/**
 * Тип результата после подготовки выполнения Think-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParam>;

/**
 * Thunk для получения сегментов компании
 */
export class ChangeMyCompanySegmentThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParam> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParam) {
        return new ChangeMyCompanySegmentThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        return {
            condition: args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().MyCompanyState
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        try {
            const requestArgs = args.inputParams!;
            const myCompanyApiProxy = InterlayerApiProxy.getMyCompanySegmentApiProxy();

            await myCompanyApiProxy.changeMyCompanySegment(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, {
                accountId: requestArgs.accountId,
                segmentId: requestArgs.segmentId
            });

            args.success();
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}