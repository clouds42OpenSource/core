import { MyCompanyAction } from 'app/modules/myCompany/store/actions';
import { ReceiveMyCompanyThunkParams } from 'app/modules/myCompany/store/reducers/receiveMyCompanyData/params';
import { ReceiveMyCompanyActionSuccessPayload } from 'app/modules/myCompany/store/reducers/receiveMyCompanyData/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { ReducerActionFailedPayload, ReducerActionStartPayload } from 'core/redux/interfaces';
import { RequestKind } from 'core/requestSender/enums';

const { ReceiveMyCompanyData: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = MyCompanyAction;

/**
 * Тип параметров Thunk-и при старте выполнения
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, ReducerActionStartPayload, ReceiveMyCompanyActionSuccessPayload, ReducerActionFailedPayload, ReceiveMyCompanyThunkParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<ReducerActionStartPayload, ReducerActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, ReducerActionStartPayload, ReceiveMyCompanyActionSuccessPayload, ReducerActionFailedPayload, ReceiveMyCompanyThunkParams>;

/**
 * Thunk получения данных о компании
 */
export class ReceiveMyCompanyThunk extends BaseReduxThunkObject<AppReduxStoreState, ReducerActionStartPayload, ReceiveMyCompanyActionSuccessPayload, ReducerActionFailedPayload, ReceiveMyCompanyThunkParams> {
    /**
     * Запуск Thunk-u
     * @param args Параметр запуска Thunk-u
     */
    public static invoke(args?: ReceiveMyCompanyThunkParams) {
        return new ReceiveMyCompanyThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        const reducerState = args.getStore().MyCompanyState;
        return {
            condition: !reducerState.hasSuccessFor.hasMyCompanyReceived || args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().LocalesState
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        try {
            const api = InterlayerApiProxy.getMyCompanyApiProxy();
            const response = await api.receiveMyCompany(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY);

            args.success({
                myCompany: response,
            });
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}