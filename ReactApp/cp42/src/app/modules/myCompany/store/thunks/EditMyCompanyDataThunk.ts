import { MyCompanyAction } from 'app/modules/myCompany/store/actions';
import { EditMyCompanyDataThunkParams } from 'app/modules/myCompany/store/reducers/editMyCompanyData/params';
import { EditMyCompanyDataActionFailedPayload, EditMyCompanyDataActionStartPayload, EditMyCompanyDataActionSuccessPayload } from 'app/modules/myCompany/store/reducers/editMyCompanyData/payload';
import { ReceiveMyCompanyThunk } from 'app/modules/myCompany/store/thunks/ReceiveMyCompanyThunk';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсере при вызове действия START_ACTION
 */
type TActionStartPayload = EditMyCompanyDataActionStartPayload;

/**
 * Тип данных в редюсере при вызове действия FAILED_ACTION
 */
type TActionFailedPayload = EditMyCompanyDataActionFailedPayload;

/**
 * Тип данных в редюсере при вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = EditMyCompanyDataActionSuccessPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParam = EditMyCompanyDataThunkParams;

const { EditMyCompanyData: { START_ACTION, FAILED_ACTION, SUCCESS_ACTION } } = MyCompanyAction;

/**
 * Тип параметров Thunk-и при старте выполнения
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParam>;

/**
 * Тип результата после подготовки выполнения Think-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParam>;

/**
 * Thunk для изменения данных о компании
 */
export class EditMyCompanyDataThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParam> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParam) {
        return new EditMyCompanyDataThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        return {
            condition: args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().MyCompanyState
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        try {
            const requestArgs = args.inputParams!;
            const myCompanyProxy = InterlayerApiProxy.getMyCompanyApiProxy();

            await myCompanyProxy.editMyCompany(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, {
                accountId: requestArgs.accountId,
                accountDescription: requestArgs.accountDescription,
                accountCaption: requestArgs.accountCaption,
                accountInn: requestArgs.accountInn,
                isVip: requestArgs.isVip,
                localeId: requestArgs.localeId,
                emails: requestArgs.emails,
                createClusterDatabase: requestArgs.createClusterDatabase,
                departments: requestArgs.departments,
            });
            args.thunkDispatch(new ReceiveMyCompanyThunk(), {
                force: true
            });

            args.success();
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}