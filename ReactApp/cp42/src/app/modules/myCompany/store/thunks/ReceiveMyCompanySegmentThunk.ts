import { MyCompanyAction } from 'app/modules/myCompany/store/actions';
import { ReceiveMyCompanySegmentThunkParams } from 'app/modules/myCompany/store/reducers/receiveMyCompanySegmentData/params';
import { ReceiveMyCompanySegmentActionFailedPayload, ReceiveMyCompanySegmentActionStartPayload, ReceiveMyCompanySegmentActionSuccessPayload } from 'app/modules/myCompany/store/reducers/receiveMyCompanySegmentData/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = ReceiveMyCompanySegmentActionStartPayload;

/**
 * Тип данных в редюсер при вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = ReceiveMyCompanySegmentActionSuccessPayload;

/**
 * Тип данных в редюсер при вызове действия FAILED_ACTION
 */
type TActionFailedPayload = ReceiveMyCompanySegmentActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParam = ReceiveMyCompanySegmentThunkParams;

const { ReceiveMyCompanySegment: { START_ACTION, FAILED_ACTION, SUCCESS_ACTION } } = MyCompanyAction;

/**
 * Тип параметров Thunk-и при старте выполнения
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParam>;

/**
 * Тип результата после подготовки выполнения Think-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParam>;

/**
 * Thunk для получения сегментов компании
 */
export class ReceiveMyCompanySegmentThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParam> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParam) {
        return new ReceiveMyCompanySegmentThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        const reducerState = args.getStore().MyCompanyState;
        return {
            condition: !reducerState.hasSuccessFor.hasMyCompanySegmentReceived || args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().MyCompanyState
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        try {
            const api = InterlayerApiProxy.getMyCompanySegmentApiProxy();
            const response = await api.receiveMyCompanySegment(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY);

            args.success({
                segment: response
            });
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}