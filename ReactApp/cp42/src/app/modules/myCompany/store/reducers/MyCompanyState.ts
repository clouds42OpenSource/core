import { IReducerState } from 'core/redux/interfaces';
import { MyCompanyProcessId } from 'app/modules/myCompany/MyCompanyProcessId';
import { MyCompanyDataModel } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/receiveMyCompany/data-models';
import { MyCompanySegmentDataModel } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/receiveMyCompanySegment/data-models';
import { EditMyCompanyDataModel } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/editMyCompany/data-models';
import { ChangeMyCompanySegmentDataModel } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/changeMyCompanySegment/data-models';
import { DatabaseMigrationDataModel } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/receiveDatabaseMigration/data-model';
import { ChangeDatabaseMigrationDataModel } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/changeDatabaseMigration/data-models';

/**
 * Состояние редюсера для данных о компании
 */
export type MyCompanyState = IReducerState<MyCompanyProcessId> & {
    /**
     * Данные о компании
     */
    myCompany: MyCompanyDataModel,
    editMyCompanyData: EditMyCompanyDataModel,
    /**
     * Сегменты компании
     */
    segment: MyCompanySegmentDataModel[],
    /**
     * Параметры изменения сегмента компании
     */
    changeSegmentData: ChangeMyCompanySegmentDataModel,
    databaseMigration: DatabaseMigrationDataModel,
    changeDatabaseMigration: ChangeDatabaseMigrationDataModel,
    hasSuccessFor: {
        /**
         * Если true, то данные компании получены
         */
        hasMyCompanyReceived: boolean,
        /**
         * Если true, то сегменты компании получены
         */
        hasMyCompanySegmentReceived: boolean,
        /**
         * Если true, то БД для миграции получены
         */
        hasDatabaseMigrationReceived: boolean
    }
};