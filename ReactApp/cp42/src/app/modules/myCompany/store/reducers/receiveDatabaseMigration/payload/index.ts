import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';
import { DatabaseMigrationDataModel } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/receiveDatabaseMigration/data-model';

/**
 * Данные в редюсере при старте получении БД для миграции
 */
export type ReceiveDatabaseMigrationActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсере при неуспешном получении БД для миграции
 */
export type ReceiveDatabaseMigrationActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсере при успешном получении БД для миграции
 */
export type ReceiveDatabaseMigrationActionSuccessPayload = ReducerActionSuccessPayload & {
    /**
     * БД для миграции
     */
    databaseMigration: DatabaseMigrationDataModel
};

/**
 * Все возможные экшены при получении БД для миграции
 */
export type ReceiveDatabaseMigrationActionAnyPayload =
    ReceiveDatabaseMigrationActionStartPayload |
    ReceiveDatabaseMigrationActionFailedPayload |
    ReceiveDatabaseMigrationActionSuccessPayload;