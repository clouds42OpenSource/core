import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';
import { MyCompanyDataModel } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/receiveMyCompany/data-models';

/**
 * Данные в редюсере при успешном выполнении получения данных о компании
 */
export type ReceiveMyCompanyActionSuccessPayload = ReducerActionSuccessPayload & {
    myCompany: MyCompanyDataModel;
};

/**
 * Все возможные экшены при получении данных о компании
 */
export type ReceiveMyCompanyActionAnyPayload =
    ReducerActionStartPayload |
    ReducerActionFailedPayload |
    ReceiveMyCompanyActionSuccessPayload;