import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров на получение данных о компании
 */
export type ReceiveMyCompanyThunkParams = IForceThunkParam;