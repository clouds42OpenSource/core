import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';
import { ReceiveDatabaseMigrationInputParam } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/receiveDatabaseMigration/input-params';

/**
 * Модель параметров на получение БД для миграции
 */
export type ReceiveDatabaseMigrationThunkParams = IForceThunkParam & ReceiveDatabaseMigrationInputParam;