import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';
import { ChangeMyCompanySegmentDataModel } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/changeMyCompanySegment/data-models';

/**
 * Данные в редюсере при старте изменения сегмента компонента
 */
export type ChangeMyCompanySegmentActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсере при неуспешном изменении сегмента компании
 */
export type ChangeMyCompanySegmentActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсере при успешном изменении сегмента компании
 */
export type ChangeMyCompanySegmentActionSuccessPayload = ReducerActionSuccessPayload & ChangeMyCompanySegmentDataModel;

/**
 * Все возможные экшены при изменении сегмента компании
 */
export type ChangeMyCompanySegmentActionAnyPayload =
    ChangeMyCompanySegmentActionStartPayload |
    ChangeMyCompanySegmentActionSuccessPayload |
    ChangeMyCompanySegmentActionFailedPayload;