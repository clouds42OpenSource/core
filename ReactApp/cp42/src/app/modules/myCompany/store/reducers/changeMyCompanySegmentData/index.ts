import { reducerActionState } from 'app/common/functions/reducerActionState';
import { MyCompanyProcessId } from 'app/modules/myCompany/MyCompanyProcessId';
import { TPartialReducerObject } from 'core/redux/types';
import { MyCompanyState } from 'app/modules/myCompany/store/reducers/MyCompanyState';
import { ChangeMyCompanySegmentActionAnyPayload, ChangeMyCompanySegmentActionSuccessPayload } from 'app/modules/myCompany/store/reducers/changeMyCompanySegmentData/payload';
import { MyCompanyAction } from 'app/modules/myCompany/store/actions';
import { reducerStateProcessSuccess } from 'core/redux/functions';

/**
 * Редюсер для изменения сегмента компании
 * @param state состояние
 * @param action действие
 */
export const changeMyCompanySegmentDataReducer: TPartialReducerObject<MyCompanyState, ChangeMyCompanySegmentActionAnyPayload> =
    (state, action) => {
        const processId = MyCompanyProcessId.ChangeMyCompanySegmentData;
        const actions = MyCompanyAction.ChangeMyCompanySegment;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as ChangeMyCompanySegmentActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    changeSegmentData: payload,
                });
            }
        });
    };