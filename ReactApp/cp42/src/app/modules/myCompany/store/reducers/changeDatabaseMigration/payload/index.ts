import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';
import { ChangeDatabaseMigrationDataModel } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/changeDatabaseMigration/data-models';

/**
 * Данные в редюсере при старте миграции БД
 */
export type ChangeDatabaseMigrationActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсере при неуспешной миграции БД
 */
export type ChangeDatabaseMigrationActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсере при успешной миграции БД
 */
export type ChangeDatabaseMigrationActionSuccessPayload = ReducerActionSuccessPayload & ChangeDatabaseMigrationDataModel;

/**
 * Все возможны экшены при миграции БД
 */
export type ChangeDatabaseMigrationActionAnyPayload =
    ChangeDatabaseMigrationActionStartPayload |
    ChangeDatabaseMigrationActionFailedPayload |
    ChangeDatabaseMigrationActionSuccessPayload;