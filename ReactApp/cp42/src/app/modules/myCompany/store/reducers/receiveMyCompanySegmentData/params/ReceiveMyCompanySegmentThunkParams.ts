import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров на получение сегмента компании
 */
export type ReceiveMyCompanySegmentThunkParams = IForceThunkParam;