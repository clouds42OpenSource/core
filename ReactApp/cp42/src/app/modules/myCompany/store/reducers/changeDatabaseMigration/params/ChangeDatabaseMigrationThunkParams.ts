import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';
import { ChangeDatabaseMigrationDataModel } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/changeDatabaseMigration/data-models';

/**
 * Модель параметров на миграции БД
 */
export type ChangeDatabaseMigrationThunkParams = IForceThunkParam & ChangeDatabaseMigrationDataModel;