import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';
import { EditMyCompanyDataModel } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/editMyCompany/data-models';

/**
 * Модель параметров на редактирование данных о компании
 */
export type EditMyCompanyDataThunkParams = IForceThunkParam & EditMyCompanyDataModel;