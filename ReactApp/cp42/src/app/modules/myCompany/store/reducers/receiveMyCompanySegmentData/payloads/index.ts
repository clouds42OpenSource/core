import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';
import { MyCompanySegmentDataModel } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/receiveMyCompanySegment/data-models';

/**
 * Данные в редюсере при старте получения сегментов компонента
 */
export type ReceiveMyCompanySegmentActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсере при неуспешном получении сегментов компании
 */
export type ReceiveMyCompanySegmentActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные редюсера при успешном получении сегментов компании
 */
export type ReceiveMyCompanySegmentActionSuccessPayload = ReducerActionSuccessPayload & {
    /**
     * Массив сегментов
     */
    segment: MyCompanySegmentDataModel[]
};

/**
 * Все возможные экшены при получении данных о компании
 */
export type ReceiveMyCompanySegmentActionAnyPayload =
    ReducerActionStartPayload |
    ReducerActionFailedPayload |
    ReducerActionSuccessPayload;