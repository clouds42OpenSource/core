import { TPartialReducerObject } from 'core/redux/types';
import { MyCompanyState } from 'app/modules/myCompany/store/reducers/MyCompanyState';
import { ReceiveDatabaseMigrationActionAnyPayload, ReceiveDatabaseMigrationActionSuccessPayload } from 'app/modules/myCompany/store/reducers/receiveDatabaseMigration/payload';
import { MyCompanyProcessId } from 'app/modules/myCompany/MyCompanyProcessId';
import { MyCompanyAction } from 'app/modules/myCompany/store/actions';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { reducerStateProcessSuccess } from 'core/redux/functions';

/**
 * Редюсер для получения БД для миграции
 * @param state состояние
 * @param action действие
 */
export const receiveDatabaseMigrationDataReducer: TPartialReducerObject<MyCompanyState, ReceiveDatabaseMigrationActionAnyPayload> = (state, action) => {
    const processId = MyCompanyProcessId.ReceiveDatabaseMigration;
    const actions = MyCompanyAction.ReceiveDatabaseMigration;

    return reducerActionState({
        action,
        actions,
        processId,
        state,
        onSuccessProcessState: () => {
            const payload = action.payload as ReceiveDatabaseMigrationActionSuccessPayload;
            return reducerStateProcessSuccess(state, processId, {
                databaseMigration: payload.databaseMigration,
                hasSuccessFor: {
                    ...state.hasSuccessFor,
                    hasDatabaseMigrationReceived: true
                }
            });
        }
    });
};