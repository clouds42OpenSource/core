import { TPartialReducerObject } from 'core/redux/types';
import { MyCompanyState } from 'app/modules/myCompany/store/reducers/MyCompanyState';
import { ReceiveMyCompanyActionAnyPayload, ReceiveMyCompanyActionSuccessPayload } from 'app/modules/myCompany/store/reducers/receiveMyCompanyData/payloads';
import { MyCompanyProcessId } from 'app/modules/myCompany/MyCompanyProcessId';
import { MyCompanyAction } from 'app/modules/myCompany/store/actions';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { reducerStateProcessSuccess } from 'core/redux/functions';

/**
 * Редюсер для получения данных о компании
 * @param state состояние
 * @param action действие
 */
export const receiveMyCompanyDataReducer: TPartialReducerObject<MyCompanyState, ReceiveMyCompanyActionAnyPayload> = (state, action) => {
    const processId = MyCompanyProcessId.ReceiveMyCompanyData;
    const actions = MyCompanyAction.ReceiveMyCompanyData;

    return reducerActionState({
        action,
        actions,
        processId,
        state,
        onSuccessProcessState: () => {
            const payload = action.payload as ReceiveMyCompanyActionSuccessPayload;
            return reducerStateProcessSuccess(state, processId, {
                myCompany: payload.myCompany,
                hasSuccessFor: {
                    ...state.hasSuccessFor,
                    hasMyCompanyReceived: true
                }
            });
        }
    });
};