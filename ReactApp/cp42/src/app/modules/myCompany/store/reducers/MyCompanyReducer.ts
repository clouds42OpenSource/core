import { EVerificationStatus } from 'app/api/endpoints/accounts/enum';
import { getInitialMetadata, partialFillOf } from 'app/common/functions';
import { MyCompanyConstants } from 'app/modules/myCompany/consts';
import { changeDatabaseMigrationReducer } from 'app/modules/myCompany/store/reducers/changeDatabaseMigration';
import { changeMyCompanySegmentDataReducer } from 'app/modules/myCompany/store/reducers/changeMyCompanySegmentData';
import { editMyCompanyDataReducer } from 'app/modules/myCompany/store/reducers/editMyCompanyData';
import { MyCompanyState } from 'app/modules/myCompany/store/reducers/MyCompanyState';
import { receiveDatabaseMigrationDataReducer } from 'app/modules/myCompany/store/reducers/receiveDatabaseMigration';
import { receiveMyCompanyDataReducer } from 'app/modules/myCompany/store/reducers/receiveMyCompanyData';
import { receiveMyCompanySegmentDataReducer } from 'app/modules/myCompany/store/reducers/receiveMyCompanySegmentData';
import { ChangeDatabaseMigrationDataModel } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/changeDatabaseMigration/data-models';
import { ChangeMyCompanySegmentDataModel } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/changeMyCompanySegment/data-models';
import { EditMyCompanyDataModel } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/editMyCompany/data-models';
import { createReducer, initReducerState } from 'core/redux/functions';

/**
 * Начальное состояние редюсера MyCompany
 */
const initialState = initReducerState<MyCompanyState>(
    MyCompanyConstants.reducerName,
    {
        myCompany: {
            createClusterDatabase: false,
            accountId: '',
            accountCaption: '',
            indexNumber: 0,
            accountInn: '',
            isVip: false,
            accountDescription: '',
            segmentId: '',
            localeId: '',
            locales: [],
            registrationDate: '',
            localeCurrency: '',
            cloudStorageWebLink: '',
            userSource: '',
            accountType: '',
            usedSizeOnDisk: 0,
            accountSaleManagerCaption: '',
            emails: [],
            segmentName: '',
            balance: 0,
            lastActivity: '',
            serviceTotalSum: 0,
            canChangeLocale: false,
            additionalCompanies: [],
            verificationStatus: EVerificationStatus.underReview,
            departments: [],
        },
        editMyCompanyData: partialFillOf<EditMyCompanyDataModel>(),
        segment: [],
        changeSegmentData: partialFillOf<ChangeMyCompanySegmentDataModel>(),
        databaseMigration: {
            fileStorageAccountDatabasesSummary: [],
            databases: {
                records: [],
                metadata: getInitialMetadata()
            },
            avalableFileStorages: [],
            availableStoragePath: [],
            numberOfDatabasesToMigrate: 0
        },
        changeDatabaseMigration: partialFillOf<ChangeDatabaseMigrationDataModel>(),
        hasSuccessFor: {
            hasMyCompanyReceived: false,
            hasMyCompanySegmentReceived: false,
            hasDatabaseMigrationReceived: false
        }
    }
);

const partialReducers = [
    receiveMyCompanyDataReducer,
    editMyCompanyDataReducer,
    receiveMyCompanySegmentDataReducer,
    changeMyCompanySegmentDataReducer,
    receiveDatabaseMigrationDataReducer,
    changeDatabaseMigrationReducer
];

export const myCompanyReducer = createReducer(initialState, partialReducers);