import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';
import { EditMyCompanyDataModel } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/editMyCompany/data-models';

/**
 * Данные в редюсере при старте изменения данных о компании
 */
export type EditMyCompanyDataActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсере при неуспешном изменении данных о компании
 */
export type EditMyCompanyDataActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсере при успешном изменении данных о компании
 */
export type EditMyCompanyDataActionSuccessPayload = ReducerActionSuccessPayload & EditMyCompanyDataModel;

/**
 * Все возможные экшены при изменении данных о компании
 */
export type EditMyCompanyDataActionAnyPayload =
    EditMyCompanyDataActionStartPayload |
    EditMyCompanyDataActionFailedPayload |
    EditMyCompanyDataActionSuccessPayload;