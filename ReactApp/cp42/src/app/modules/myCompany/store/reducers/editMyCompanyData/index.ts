import { reducerActionState } from 'app/common/functions/reducerActionState';
import { MyCompanyProcessId } from 'app/modules/myCompany/MyCompanyProcessId';
import { TPartialReducerObject } from 'core/redux/types';
import { MyCompanyState } from 'app/modules/myCompany/store/reducers/MyCompanyState';
import { EditMyCompanyDataActionAnyPayload, EditMyCompanyDataActionSuccessPayload } from 'app/modules/myCompany/store/reducers/editMyCompanyData/payload';
import { MyCompanyAction } from 'app/modules/myCompany/store/actions';
import { reducerStateProcessSuccess } from 'core/redux/functions';

/**
 * Редюсер для изменения данных о компании
 * @param state состояние
 * @param action действие
 */
export const editMyCompanyDataReducer: TPartialReducerObject<MyCompanyState, EditMyCompanyDataActionAnyPayload> =
    (state, action) => {
        const processId = MyCompanyProcessId.EditMyCompanyData;
        const actions = MyCompanyAction.EditMyCompanyData;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as EditMyCompanyDataActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    editMyCompanyData: payload
                });
            }
        });
    };