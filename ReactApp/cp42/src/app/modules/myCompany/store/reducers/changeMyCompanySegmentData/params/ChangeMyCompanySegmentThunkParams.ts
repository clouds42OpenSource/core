import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';
import { ChangeMyCompanySegmentDataModel } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/changeMyCompanySegment/data-models';

/**
 * Модель параметров на изменение сегмента компании
 */
export type ChangeMyCompanySegmentThunkParams = IForceThunkParam & ChangeMyCompanySegmentDataModel;