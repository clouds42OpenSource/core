import { reducerActionState } from 'app/common/functions/reducerActionState';
import { MyCompanyProcessId } from 'app/modules/myCompany/MyCompanyProcessId';
import { TPartialReducerObject } from 'core/redux/types';
import { MyCompanyState } from 'app/modules/myCompany/store/reducers/MyCompanyState';
import { ChangeDatabaseMigrationActionAnyPayload, ChangeDatabaseMigrationActionSuccessPayload } from 'app/modules/myCompany/store/reducers/changeDatabaseMigration/payload';
import { MyCompanyAction } from 'app/modules/myCompany/store/actions';
import { reducerStateProcessSuccess } from 'core/redux/functions';

/**
 * Редюсер для миграции БД
 * @param state состояние
 * @param action действие
 */
export const changeDatabaseMigrationReducer: TPartialReducerObject<MyCompanyState, ChangeDatabaseMigrationActionAnyPayload> =
    (state, action) => {
        const processId = MyCompanyProcessId.ChangeDatabaseMigration;
        const actions = MyCompanyAction.ChangeDatabaseMigration;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as ChangeDatabaseMigrationActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    changeDatabaseMigration: payload
                });
            }
        });
    };