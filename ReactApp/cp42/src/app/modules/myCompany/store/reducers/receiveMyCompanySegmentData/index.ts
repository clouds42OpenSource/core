import { TPartialReducerObject } from 'core/redux/types';
import { MyCompanyState } from 'app/modules/myCompany/store/reducers/MyCompanyState';
import { ReceiveMyCompanySegmentActionAnyPayload, ReceiveMyCompanySegmentActionSuccessPayload } from 'app/modules/myCompany/store/reducers/receiveMyCompanySegmentData/payloads';
import { MyCompanyProcessId } from 'app/modules/myCompany/MyCompanyProcessId';
import { MyCompanyAction } from 'app/modules/myCompany/store/actions';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { reducerStateProcessSuccess } from 'core/redux/functions';

/**
 * Редюсер для получения сегментов
 * @param state состояние
 * @param action действие
 */
export const receiveMyCompanySegmentDataReducer: TPartialReducerObject<MyCompanyState, ReceiveMyCompanySegmentActionAnyPayload> = (state, action) => {
    const processId = MyCompanyProcessId.ReceiveMyCompanySegmentData;
    const actions = MyCompanyAction.ReceiveMyCompanySegment;

    return reducerActionState({
        action,
        actions,
        processId,
        state,
        onSuccessProcessState: () => {
            const payload = action.payload as ReceiveMyCompanySegmentActionSuccessPayload;
            return reducerStateProcessSuccess(state, processId, {
                segment: [...payload.segment],
                hasSuccessFor: {
                    ...state.hasSuccessFor,
                    hasMyCompanySegmentReceived: true
                }
            });
        }
    });
};