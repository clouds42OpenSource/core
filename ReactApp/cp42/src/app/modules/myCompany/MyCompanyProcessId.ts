/**
 * Процессы для данных о компании
 */
export enum MyCompanyProcessId {
    /**
     * Процесс для получения данных о компании
     */
    ReceiveMyCompanyData = 'RECEIVE_MY_COMPANY_DATA',

    /**
     * Процесс редактирования данных о компании
     */
    EditMyCompanyData = 'EDIT_MY_COMPANY_DATA',

    /**
     * Процесс для получения сегмента компании
     */
    ReceiveMyCompanySegmentData = 'RECEIVE_MY_COMPANY_SEGMENT_DATA',

    /**
     * Процесс для изменения сегмента компании
     */
    ChangeMyCompanySegmentData = 'CHANGE_MY_COMPANY_SEGMENT_DATA',

    /**
     * Процесс для получения БД для миграции
     */
    ReceiveDatabaseMigration = 'RECEIVE_DATABASE_MIGRATION',

    /**
     * Процесс для миграции БД
     */
    ChangeDatabaseMigration = 'CHANGE_DATABASE_MIGRATION'
}