/**
 * Процесы поставщиков
 */
export enum SuppliersProcessId {
    /**
     * Процесс на получение коллекцию поставщиков с пагинацией
     */
    GetSuppliersWithPagination = 'GET_SUPPLIERS_WITH_PAGINATION',

    /**
     * Процесс на получение элементов комбобокса
     * при инициализации страницы создания/редактирования поставщика
     */
    GetInitCreateEditPageItems = 'GET_INIT_CREATE_EDIT_PAGE_ITEMS',

    /**
     * Процесс на создание поставщика
     */
    CreateSupplier = 'CREATE_SUPPLIER',

    /**
     * Процесс на редактирования поставщика
     */
    EditSupplier = 'EDIT_SUPPLIER',

    /**
     * Процесс на получения поставщика по ID
     */
    GetSupplierById = 'GET_SUPPLIER_BY_ID',

    /**
     * Процесс на удаление поставщика
     */
    DeleteSupplier = 'DELETE_SUPPLIER',

    /**
     * Процесс на получение реферальных аккаунтов по ID поставщика
     */
    GetReferralAccountsBySupplierId = 'GET_REFERRAL_ACCOUNTS_BY_SUPPLIER_ID',

    /**
     * Процесс на получение реферальных аккаунтов по значению поиска
     */
    GetReferralAccountsBySearchValue = 'GET_REFERRAL_ACCOUNTS_BY_SEARCH_VALUE',

    /**
     * Процесс на удаление реферального аккаунта у поставщика
     */
    DeleteSupplierReferralAccount = 'DELETE_SUPPLIER_REFERRAL_ACCOUNT',

    /**
     * Процесс на добавление реферального аккаунта к поставщику
     */
    CreateSupplierReferralAccount = 'CREATE_SUPPLIER_REFERRAL_ACCOUNT'
}