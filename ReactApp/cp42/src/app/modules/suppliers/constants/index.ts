/**
 * Константы для модуля Suppliers
 */
export const SuppliersConsts = {
    /**
     * Название редюсера для модуля Suppliers
     */
    reducerName: 'SUPPLIERS'
};