import { SuppliersProcessId } from 'app/modules/suppliers';
import { InitSelectListItemsDataModel } from 'app/web/InterlayerApiProxy/SuppliersApiProxy/getInitItemsPage';
import { SupplierReferralAccountsDataModel } from 'app/web/InterlayerApiProxy/SuppliersApiProxy/getReferralAccountsBySupplierId';
import { SupplierByIdDataModel } from 'app/web/InterlayerApiProxy/SuppliersApiProxy/getSupplierById';
import { SuppliersPaginationDataModel } from 'app/web/InterlayerApiProxy/SuppliersApiProxy/getSuppliersWithPagination';
import { IReducerState } from 'core/redux/interfaces';

/**
 * Состояние редюсера для работы с поставщиками
 */
export type SuppliersState = IReducerState<SuppliersProcessId> & {
    /**
     * Коллекция поставщиков с пагинацией
     */
    suppliers: SuppliersPaginationDataModel,

    /**
     * Модель для получения элементов комбобокса
     * при инциализации страниц создания/редактирования поставщика
     */
    initItemsCreateEditPage: InitSelectListItemsDataModel,

    /**
     * Модель поставщика
     */
    supplier: SupplierByIdDataModel,

    /**
     * Модель словаря для реферальных аккаунтов поставщика
     * Key - ID реферального аккаунта
     * Value - название реферального аккаунта
     */
    supplierReferralAccounts: SupplierReferralAccountsDataModel,

    /**
     * Модель словаря для реферальных аккаунтов поставщика по значению поиска
     * Key - ID реферального аккаунта
     * Value - название реферального аккаунта
     */
    supplierReferralAccountsOptions: SupplierReferralAccountsDataModel,

    /**
     * содержит ярлыки для фиксации загрузок данных, была ли загрузка или ещё нет
     */
    hasSuccessFor: {
        /**
         * Если true, то данные поставщиков полученны
         */
        hasSuppliersReceived: boolean,

        /**
         * Если true, то данные для инициализации страницы создания/редактирования поставщика полученны
         */
        hasInitItemsReceived: boolean
    };
};