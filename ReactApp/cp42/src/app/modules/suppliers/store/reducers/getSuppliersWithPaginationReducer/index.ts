import { reducerActionState } from 'app/common/functions/reducerActionState';
import { SuppliersActions } from 'app/modules/suppliers/store/actions';
import { GetSuppliersWithPaginationActionAnyPayload, GetSuppliersWithPaginationActionSuccessPayload } from 'app/modules/suppliers/store/reducers/getSuppliersWithPaginationReducer/payloads';
import { SuppliersState } from 'app/modules/suppliers/store/reducers/SuppliersState';
import { SuppliersProcessId } from 'app/modules/suppliers/SuppliersProcessId';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения поставщиков с пагинацией
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const getSuppliersWithPaginationReducer: TPartialReducerObject<SuppliersState, GetSuppliersWithPaginationActionAnyPayload> =
    (state, action) => {
        const processId = SuppliersProcessId.GetSuppliersWithPagination;
        const actions = SuppliersActions.GetSuppliersWithPagination;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as GetSuppliersWithPaginationActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    suppliers: payload,
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasSuppliersReceived: true
                    }
                });
            }
        });
    };