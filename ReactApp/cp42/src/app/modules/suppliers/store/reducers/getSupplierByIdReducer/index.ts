import { partialFillOf } from 'app/common/functions';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { SuppliersActions } from 'app/modules/suppliers/store/actions';
import { GetSupplierByIdActionAnyPayload, GetSupplierByIdActionSuccessPayload } from 'app/modules/suppliers/store/reducers/getSupplierByIdReducer/payloads';
import { SuppliersState } from 'app/modules/suppliers/store/reducers/SuppliersState';
import { SuppliersProcessId } from 'app/modules/suppliers/SuppliersProcessId';
import { SupplierByIdDataModel } from 'app/web/InterlayerApiProxy/SuppliersApiProxy/getSupplierById';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения поставщика по ID
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const getSupplierByIdReducer: TPartialReducerObject<SuppliersState, GetSupplierByIdActionAnyPayload> =
    (state, action) => {
        const processId = SuppliersProcessId.GetSupplierById;
        const actions = SuppliersActions.GetSupplierById;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onStartProcessState: () => {
                return reducerStateProcessStart(state, processId, {
                    supplier: partialFillOf<SupplierByIdDataModel>()
                });
            },
            onSuccessProcessState: () => {
                const payload = action.payload as GetSupplierByIdActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    supplier: payload
                });
            }
        });
    };