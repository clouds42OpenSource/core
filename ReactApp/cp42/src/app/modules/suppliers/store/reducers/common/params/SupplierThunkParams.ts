import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметра для передачи ID поставщика
 */
export type SupplierThunkParams = IForceThunkParam & {
    /**
     * ID поставщика
     */
    supplierId: string
}