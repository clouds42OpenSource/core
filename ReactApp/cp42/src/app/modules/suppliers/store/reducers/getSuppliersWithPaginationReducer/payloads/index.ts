import { SuppliersPaginationDataModel } from 'app/web/InterlayerApiProxy/SuppliersApiProxy/getSuppliersWithPagination';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения поставщиков
 */
export type GetSuppliersWithPaginationActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения поставщиков
 */
export type GetSuppliersWithPaginationActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения поставщиков
 */
export type GetSuppliersWithPaginationActionSuccessPayload = ReducerActionSuccessPayload & SuppliersPaginationDataModel;

/**
 * Все возможные Action при получении поставщиков
 */
export type GetSuppliersWithPaginationActionAnyPayload =
    | GetSuppliersWithPaginationActionStartPayload
    | GetSuppliersWithPaginationActionFailedPayload
    | GetSuppliersWithPaginationActionSuccessPayload;