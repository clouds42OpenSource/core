import { reducerActionState } from 'app/common/functions/reducerActionState';
import { SuppliersActions } from 'app/modules/suppliers/store/actions';
import { GetReferralAccountsBySupplierIdActionAnyPayload, GetReferralAccountsBySupplierIdActionSuccessPayload } from 'app/modules/suppliers/store/reducers/getReferralAccountsBySupplierIdReducer/payloads';
import { SuppliersState } from 'app/modules/suppliers/store/reducers/SuppliersState';
import { SuppliersProcessId } from 'app/modules/suppliers/SuppliersProcessId';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения реферальных аккаунтов поставщика
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const getReferralAccountsBySupplierIdReducer: TPartialReducerObject<SuppliersState, GetReferralAccountsBySupplierIdActionAnyPayload> =
    (state, action) => {
        const processId = SuppliersProcessId.GetReferralAccountsBySupplierId;
        const actions = SuppliersActions.GetReferralAccountsBySupplierId;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onStartProcessState: () => {
                return reducerStateProcessStart(state, processId, {
                    supplierReferralAccounts: []
                });
            },
            onSuccessProcessState: () => {
                const payload = action.payload as GetReferralAccountsBySupplierIdActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    supplierReferralAccounts: payload
                });
            }
        });
    };