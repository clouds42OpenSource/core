import { SupplierReferralAccountsDataModel } from 'app/web/InterlayerApiProxy/SuppliersApiProxy/getReferralAccountsBySupplierId';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения реферальных аккаунтов поставщика по значению поиска
 */
export type GetReferralAccountsBySearchValueActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения реферальных аккаунтов поставщика по значению поиска
 */
export type GetReferralAccountsBySearchValueActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения реферальных аккаунтов поставщика по значению поиска
 */
export type GetReferralAccountsBySearchValueActionSuccessPayload = ReducerActionSuccessPayload & SupplierReferralAccountsDataModel;

/**
 * Все возможные Action при получении реферальных аккаунтов поставщика по значению поиска
 */
export type GetReferralAccountsBySearchValueActionAnyPayload =
    | GetReferralAccountsBySearchValueActionStartPayload
    | GetReferralAccountsBySearchValueActionFailedPayload
    | GetReferralAccountsBySearchValueActionSuccessPayload;