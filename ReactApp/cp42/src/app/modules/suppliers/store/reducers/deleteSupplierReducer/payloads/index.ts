import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения удаления поставщика
 */
export type DeleteSupplierActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении удаления поставщика
 */
export type DeleteSupplierActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении удаления поставщика
 */
export type DeleteSupplierActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при удалении поставщика
 */
export type DeleteSupplierActionAnyPayload =
    | DeleteSupplierActionStartPayload
    | DeleteSupplierActionFailedPayload
    | DeleteSupplierActionSuccessPayload;