import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения добавления реферального аккаунта поставщика
 */
export type CreateSupplierReferralAccountActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении добавления реферального аккаунта поставщика
 */
export type CreateSupplierReferralAccountActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении добавления реферального аккаунта поставщика
 */
export type CreateSupplierReferralAccountActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при добавлении реферального аккаунта поставщика
 */
export type CreateSupplierReferralAccountActionAnyPayload =
    | CreateSupplierReferralAccountActionStartPayload
    | CreateSupplierReferralAccountActionFailedPayload
    | CreateSupplierReferralAccountActionSuccessPayload;