import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения редактирования поставщика
 */
export type EditSupplierActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении редактирования поставщика
 */
export type EditSupplierActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении редактирования поставщика
 */
export type EditSupplierActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при редактировании поставщика
 */
export type EditSupplierActionAnyPayload =
    | EditSupplierActionStartPayload
    | EditSupplierActionFailedPayload
    | EditSupplierActionSuccessPayload;