import { SupplierReferralAccountsParams } from 'app/web/InterlayerApiProxy/SuppliersApiProxy/common';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметра для thunk при добавления/удаления реферального аккаунта
 */
export type ReferralAccountThunkParams = IForceThunkParam & SupplierReferralAccountsParams;