import { reducerActionState } from 'app/common/functions/reducerActionState';
import { SuppliersActions } from 'app/modules/suppliers/store/actions';
import { DeleteSupplierActionAnyPayload } from 'app/modules/suppliers/store/reducers/deleteSupplierReducer/payloads';
import { SuppliersState } from 'app/modules/suppliers/store/reducers/SuppliersState';
import { SuppliersProcessId } from 'app/modules/suppliers/SuppliersProcessId';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для удалении поставщика
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const deleteSupplierReducer: TPartialReducerObject<SuppliersState, DeleteSupplierActionAnyPayload> =
    (state, action) => {
        const processId = SuppliersProcessId.DeleteSupplier;
        const actions = SuppliersActions.DeleteSupplier;

        return reducerActionState({
            action,
            actions,
            processId,
            state
        });
    };