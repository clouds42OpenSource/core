import { reducerActionState } from 'app/common/functions/reducerActionState';
import { SuppliersActions } from 'app/modules/suppliers/store/actions';
import { GetInitCreateEditPageItemsActionAnyPayload, GetInitCreateEditPageItemsActionSuccessPayload } from 'app/modules/suppliers/store/reducers/getInitCreateEditPageItemsReducer/payloads';
import { SuppliersState } from 'app/modules/suppliers/store/reducers/SuppliersState';
import { SuppliersProcessId } from 'app/modules/suppliers/SuppliersProcessId';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения элементов комбобокса
 * при инициализации страницы создания/редактирования поставщика
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const getInitCreateEditPageItemsReducer: TPartialReducerObject<SuppliersState, GetInitCreateEditPageItemsActionAnyPayload> =
    (state, action) => {
        const processId = SuppliersProcessId.GetInitCreateEditPageItems;
        const actions = SuppliersActions.GetInitCreateEditPageItems;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onStartProcessState: () => {
                return reducerStateProcessStart(state, processId, {
                    initItemsCreateEditPage: {
                        locales: [],
                        printedHtmlForms: []
                    },
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasInitItemsReceived: false
                    }
                });
            },
            onSuccessProcessState: () => {
                const payload = action.payload as GetInitCreateEditPageItemsActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    initItemsCreateEditPage: payload,
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasInitItemsReceived: true
                    }
                });
            }
        });
    };