import { SupplierReferralAccountsDataModel } from 'app/web/InterlayerApiProxy/SuppliersApiProxy/getReferralAccountsBySupplierId';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения реферальных аккаунтов поставщика
 */
export type GetReferralAccountsBySupplierIdActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения реферальных аккаунтов поставщика
 */
export type GetReferralAccountsBySupplierIdActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения реферальных аккаунтов поставщика
 */
export type GetReferralAccountsBySupplierIdActionSuccessPayload = ReducerActionSuccessPayload & SupplierReferralAccountsDataModel;

/**
 * Все возможные Action при получении реферальных аккаунтов поставщика
 */
export type GetReferralAccountsBySupplierIdActionAnyPayload =
    | GetReferralAccountsBySupplierIdActionStartPayload
    | GetReferralAccountsBySupplierIdActionFailedPayload
    | GetReferralAccountsBySupplierIdActionSuccessPayload;