import { getInitialMetadata, partialFillOf } from 'app/common/functions';
import { SuppliersState } from 'app/modules/suppliers/store/reducers';
import { createSupplierReducer } from 'app/modules/suppliers/store/reducers/createSupplierReducer';
import { deleteSupplierReducer } from 'app/modules/suppliers/store/reducers/deleteSupplierReducer';
import { deleteSupplierReferralAccountReducer } from 'app/modules/suppliers/store/reducers/deleteSupplierReferralAccountReducer';
import { editSupplierReducer } from 'app/modules/suppliers/store/reducers/editSupplierReducer';
import { getInitCreateEditPageItemsReducer } from 'app/modules/suppliers/store/reducers/getInitCreateEditPageItemsReducer';
import { getReferralAccountsBySearchValueReducer } from 'app/modules/suppliers/store/reducers/getReferralAccountsBySearchValueReducer';
import { getReferralAccountsBySupplierIdReducer } from 'app/modules/suppliers/store/reducers/getReferralAccountsBySupplierIdReducer';
import { getSupplierByIdReducer } from 'app/modules/suppliers/store/reducers/getSupplierByIdReducer';
import { getSuppliersWithPaginationReducer } from 'app/modules/suppliers/store/reducers/getSuppliersWithPaginationReducer';
import { createSupplierReferralAccountReducer } from 'app/modules/suppliers/store/reducers/сreateSupplierReferralAccountReducer';
import { SupplierByIdDataModel } from 'app/web/InterlayerApiProxy/SuppliersApiProxy/getSupplierById';
import { createReducer, initReducerState } from 'core/redux/functions';

/**
 * Начальное состояние редюсера SuppliersReducer
 */
const initialState = initReducerState<SuppliersState>(
    'SUPPLIERS',
    {
        /**
         * Коллекция поставщиков с пагинацией
         */
        suppliers: {
            records: [],
            metadata: getInitialMetadata()
        },

        /**
         * Модель для получения элементов комбобокса
         * при инциализации страниц создания/редактирования поставщика
         */
        initItemsCreateEditPage: {
            locales: [],
            printedHtmlForms: []
        },

        /**
         * Модель поставщика
         */
        supplier: partialFillOf<SupplierByIdDataModel>(),

        /**
         * Модель словаря для реферальных аккаунтов поставщика
         * Key - ID реферального аккаунта
         * Value - название реферального аккаунта
         */
        supplierReferralAccounts: [],

        /**
         * Модель словаря для реферальных аккаунтов поставщика по значению поиска
         * Key - ID реферального аккаунта
         * Value - название реферального аккаунта
         */
        supplierReferralAccountsOptions: [],

        /**
         * содержит ярлыки для фиксации загрузок данных, была ли загрузка или ещё нет
         */
        hasSuccessFor: {
            /**
             * Если true, то данные поставщиков полученны
             */
            hasSuppliersReceived: false,

            /**
             * Если true, то данные для инициализации страницы создания/редактирования поставщика полученны
             */
            hasInitItemsReceived: false
        }
    }
);

/**
 * Объединённые части редюсера для всего состояния SuppliersReducer
 */
const partialReducers = [
    getSuppliersWithPaginationReducer,
    getInitCreateEditPageItemsReducer,
    createSupplierReducer,
    editSupplierReducer,
    getSupplierByIdReducer,
    deleteSupplierReducer,
    getReferralAccountsBySupplierIdReducer,
    getReferralAccountsBySearchValueReducer,
    deleteSupplierReferralAccountReducer,
    createSupplierReferralAccountReducer
];

/**
 * Редюсер SuppliersReducer
 */
export const SuppliersReducer = createReducer(initialState, partialReducers);