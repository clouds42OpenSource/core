import { GetSuppliersWithPaginationParams } from 'app/web/InterlayerApiProxy/SuppliersApiProxy/getSuppliersWithPagination';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для получения поставщиков с пагинацией
 */
export type GetSuppliersWithPaginationThunkParams = IForceThunkParam & GetSuppliersWithPaginationParams;