import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения удаления реферального аккаунта поставщика
 */
export type DeleteSupplierReferralAccountActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении удаления реферального аккаунта поставщика
 */
export type DeleteSupplierReferralAccountActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении удаления реферального аккаунта поставщика
 */
export type DeleteSupplierReferralAccountActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при удалении реферального аккаунта поставщика
 */
export type DeleteSupplierReferralAccountActionAnyPayload =
    | DeleteSupplierReferralAccountActionStartPayload
    | DeleteSupplierReferralAccountActionFailedPayload
    | DeleteSupplierReferralAccountActionSuccessPayload;