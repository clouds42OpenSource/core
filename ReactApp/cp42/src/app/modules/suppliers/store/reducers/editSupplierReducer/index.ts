import { reducerActionState } from 'app/common/functions/reducerActionState';
import { SuppliersActions } from 'app/modules/suppliers/store/actions';
import { EditSupplierActionAnyPayload } from 'app/modules/suppliers/store/reducers/editSupplierReducer/payloads';
import { SuppliersState } from 'app/modules/suppliers/store/reducers/SuppliersState';
import { SuppliersProcessId } from 'app/modules/suppliers/SuppliersProcessId';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для редактирования поставщика
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const editSupplierReducer: TPartialReducerObject<SuppliersState, EditSupplierActionAnyPayload> =
    (state, action) => {
        const processId = SuppliersProcessId.EditSupplier;
        const actions = SuppliersActions.EditSupplier;

        return reducerActionState({
            action,
            actions,
            processId,
            state
        });
    };