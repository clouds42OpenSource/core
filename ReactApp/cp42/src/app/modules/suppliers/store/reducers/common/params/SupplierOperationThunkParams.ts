import { SupplierParams } from 'app/web/InterlayerApiProxy/SuppliersApiProxy/common';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметра для thunk при создании/редактировании поставщика
 */
export type SupplierOperationThunkParams = IForceThunkParam & SupplierParams;