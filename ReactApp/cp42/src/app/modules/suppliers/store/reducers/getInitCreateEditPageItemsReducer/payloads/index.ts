import { InitSelectListItemsDataModel } from 'app/web/InterlayerApiProxy/SuppliersApiProxy/getInitItemsPage';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения элементов комбобокса
 * при инициализации страницы создания/редактирования поставщика
 */
export type GetInitCreateEditPageItemsActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения элементов комбобокса
 * при инициализации страницы создания/редактирования поставщика
 */
export type GetInitCreateEditPageItemsActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения элементов комбобокса
 * при инициализации страницы создания/редактирования поставщика
 */
export type GetInitCreateEditPageItemsActionSuccessPayload = ReducerActionSuccessPayload & InitSelectListItemsDataModel;

/**
 * Все возможные Action при получении элементов комбобокса
 * при инициализации страницы создания/редактирования поставщика
 */
export type GetInitCreateEditPageItemsActionAnyPayload =
    | GetInitCreateEditPageItemsActionStartPayload
    | GetInitCreateEditPageItemsActionFailedPayload
    | GetInitCreateEditPageItemsActionSuccessPayload;