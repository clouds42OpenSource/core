import { reducerActionState } from 'app/common/functions/reducerActionState';
import { SuppliersActions } from 'app/modules/suppliers/store/actions';
import { SuppliersState } from 'app/modules/suppliers/store/reducers/SuppliersState';
import { CreateSupplierReferralAccountActionAnyPayload } from 'app/modules/suppliers/store/reducers/сreateSupplierReferralAccountReducer/payloads';
import { SuppliersProcessId } from 'app/modules/suppliers/SuppliersProcessId';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для добавления реферального аккаунта поставщика
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const createSupplierReferralAccountReducer: TPartialReducerObject<SuppliersState, CreateSupplierReferralAccountActionAnyPayload> =
    (state, action) => {
        const processId = SuppliersProcessId.CreateSupplierReferralAccount;
        const actions = SuppliersActions.CreateSupplierReferralAccount;

        return reducerActionState({
            action,
            actions,
            processId,
            state
        });
    };