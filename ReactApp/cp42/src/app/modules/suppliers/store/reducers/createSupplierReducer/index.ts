import { reducerActionState } from 'app/common/functions/reducerActionState';
import { SuppliersActions } from 'app/modules/suppliers/store/actions';
import { CreateSupplierActionAnyPayload } from 'app/modules/suppliers/store/reducers/createSupplierReducer/payloads';
import { SuppliersState } from 'app/modules/suppliers/store/reducers/SuppliersState';
import { SuppliersProcessId } from 'app/modules/suppliers/SuppliersProcessId';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для создания поставщика
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const createSupplierReducer: TPartialReducerObject<SuppliersState, CreateSupplierActionAnyPayload> =
    (state, action) => {
        const processId = SuppliersProcessId.CreateSupplier;
        const actions = SuppliersActions.CreateSupplier;

        return reducerActionState({
            action,
            actions,
            processId,
            state
        });
    };