import { SupplierByIdDataModel } from 'app/web/InterlayerApiProxy/SuppliersApiProxy/getSupplierById';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения поставщика по ID
 */
export type GetSupplierByIdActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения поставщика по ID
 */
export type GetSupplierByIdActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения поставщика по ID
 */
export type GetSupplierByIdActionSuccessPayload = ReducerActionSuccessPayload & SupplierByIdDataModel;

/**
 * Все возможные Action при получении поставщика по ID
 */
export type GetSupplierByIdActionAnyPayload =
    | GetSupplierByIdActionStartPayload
    | GetSupplierByIdActionFailedPayload
    | GetSupplierByIdActionSuccessPayload;