import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для получения реферальных аккаунтов по значению поисковика
 */
export type GetReferralBySearchValueThunkParams = IForceThunkParam & {
    /**
     * Значение поиска для получения реферальных аккаунтов
     */
    SearchValue: string
}