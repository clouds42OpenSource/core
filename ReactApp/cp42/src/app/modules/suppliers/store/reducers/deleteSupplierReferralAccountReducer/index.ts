import { reducerActionState } from 'app/common/functions/reducerActionState';
import { SuppliersActions } from 'app/modules/suppliers/store/actions';
import { DeleteSupplierReferralAccountActionAnyPayload } from 'app/modules/suppliers/store/reducers/deleteSupplierReferralAccountReducer/payloads';
import { SuppliersState } from 'app/modules/suppliers/store/reducers/SuppliersState';
import { SuppliersProcessId } from 'app/modules/suppliers/SuppliersProcessId';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для удалении реферального аккаунта поставщика
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const deleteSupplierReferralAccountReducer: TPartialReducerObject<SuppliersState, DeleteSupplierReferralAccountActionAnyPayload> =
    (state, action) => {
        const processId = SuppliersProcessId.DeleteSupplierReferralAccount;
        const actions = SuppliersActions.DeleteSupplierReferralAccount;

        return reducerActionState({
            action,
            actions,
            processId,
            state
        });
    };