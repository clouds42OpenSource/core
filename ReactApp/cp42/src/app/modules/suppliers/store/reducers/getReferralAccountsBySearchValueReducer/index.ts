import { reducerActionState } from 'app/common/functions/reducerActionState';
import { SuppliersActions } from 'app/modules/suppliers/store/actions';
import { GetReferralAccountsBySearchValueActionAnyPayload, GetReferralAccountsBySearchValueActionSuccessPayload } from 'app/modules/suppliers/store/reducers/getReferralAccountsBySearchValueReducer/payloads';
import { SuppliersState } from 'app/modules/suppliers/store/reducers/SuppliersState';
import { SuppliersProcessId } from 'app/modules/suppliers/SuppliersProcessId';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения реферальных аккаунтов поставщика по значению поиска
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const getReferralAccountsBySearchValueReducer: TPartialReducerObject<SuppliersState, GetReferralAccountsBySearchValueActionAnyPayload> =
    (state, action) => {
        const processId = SuppliersProcessId.GetReferralAccountsBySearchValue;
        const actions = SuppliersActions.GetReferralAccountsBySearchValue;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onStartProcessState: () => {
                return reducerStateProcessStart(state, processId, {
                    supplierReferralAccountsOptions: []
                });
            },
            onSuccessProcessState: () => {
                const payload = action.payload as GetReferralAccountsBySearchValueActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    supplierReferralAccountsOptions: payload
                });
            }
        });
    };