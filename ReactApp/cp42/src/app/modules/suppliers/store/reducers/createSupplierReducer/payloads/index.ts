import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения создания поставщика
 */
export type CreateSupplierActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении создания поставщика
 */
export type CreateSupplierActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении создания поставщика
 */
export type CreateSupplierActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при создании поставщика
 */
export type CreateSupplierActionAnyPayload =
    | CreateSupplierActionStartPayload
    | CreateSupplierActionFailedPayload
    | CreateSupplierActionSuccessPayload;