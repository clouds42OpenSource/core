import { SuppliersActions } from 'app/modules/suppliers/store/actions';
import { GetReferralBySearchValueThunkParams } from 'app/modules/suppliers/store/reducers/getReferralAccountsBySearchValueReducer/params';
import {
    GetReferralAccountsBySearchValueActionFailedPayload,
    GetReferralAccountsBySearchValueActionStartPayload,
    GetReferralAccountsBySearchValueActionSuccessPayload
} from 'app/modules/suppliers/store/reducers/getReferralAccountsBySearchValueReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = GetReferralAccountsBySearchValueActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = GetReferralAccountsBySearchValueActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = GetReferralAccountsBySearchValueActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = GetReferralBySearchValueThunkParams;

const { GetReferralAccountsBySearchValue: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = SuppliersActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для запроса получения реферальных аккаунтов поставщика по значению поиска
 */
export class GetReferralAccountsBySearchValueThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new GetReferralAccountsBySearchValueThunk().execute(args, args?.showLoadingProgress);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        return {
            condition: args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().SuppliersState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        try {
            const api = InterlayerApiProxy.getSuppliersApiProxy();
            const response = await api.getReferralAccountsBySearchValue(RequestKind.SEND_BY_ROBOT, args.inputParams!.SearchValue);
            args.success(response);
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}