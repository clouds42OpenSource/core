import { SuppliersActions } from 'app/modules/suppliers/store/actions';
import { GetSuppliersWithPaginationThunkParams } from 'app/modules/suppliers/store/reducers/getSuppliersWithPaginationReducer/params';
import { GetSuppliersWithPaginationActionFailedPayload, GetSuppliersWithPaginationActionStartPayload, GetSuppliersWithPaginationActionSuccessPayload } from 'app/modules/suppliers/store/reducers/getSuppliersWithPaginationReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = GetSuppliersWithPaginationActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = GetSuppliersWithPaginationActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = GetSuppliersWithPaginationActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = GetSuppliersWithPaginationThunkParams;

const { GetSuppliersWithPagination: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = SuppliersActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для запроса списка поставщиков
 */
export class GetSuppliersWithPaginationThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new GetSuppliersWithPaginationThunk().execute(args);
    }

    protected get startActionValue(): string { return START_ACTION; }

    protected get successActionValue(): string { return SUCCESS_ACTION; }

    protected get failedActionValue(): string { return FAILED_ACTION; }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        const processFlowState = args.getStore().SuppliersState;

        return {
            condition: !processFlowState.hasSuccessFor.hasSuppliersReceived || args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().SuppliersState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        try {
            const api = InterlayerApiProxy.getSuppliersApiProxy();
            const response = await api.getSuppliersWithPagination(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, args.inputParams!);
            args.success(response);
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}