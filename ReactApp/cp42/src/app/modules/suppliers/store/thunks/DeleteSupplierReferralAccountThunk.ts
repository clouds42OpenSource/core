import { SuppliersActions } from 'app/modules/suppliers/store/actions';
import { ReferralAccountThunkParams } from 'app/modules/suppliers/store/reducers/common/params';
import {
    DeleteSupplierReferralAccountActionFailedPayload,
    DeleteSupplierReferralAccountActionStartPayload,
    DeleteSupplierReferralAccountActionSuccessPayload
} from 'app/modules/suppliers/store/reducers/deleteSupplierReferralAccountReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = DeleteSupplierReferralAccountActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = DeleteSupplierReferralAccountActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = DeleteSupplierReferralAccountActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = ReferralAccountThunkParams;

const { DeleteSupplierReferralAccount: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = SuppliersActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для удаления реферального аккаунта поставщика
 */
export class DeleteSupplierReferralAccountThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new DeleteSupplierReferralAccountThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        return {
            condition: args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().SuppliersState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        try {
            await InterlayerApiProxy.getSuppliersApiProxy().deleteSupplierReferralAccount(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, args.inputParams!);
            args.success();
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}