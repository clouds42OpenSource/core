import { SuppliersConsts } from 'app/modules/suppliers/constants';
import { SuppliersProcessId } from 'app/modules/suppliers/SuppliersProcessId';
import { createReducerActions } from 'core/redux/functions';

/**
 * Все доступные actions для поставщиков
 */
export const SuppliersActions = {
    /**
     * Набор action на получение коллекцию поставщиков с пагинацией
     */
    GetSuppliersWithPagination: createReducerActions(SuppliersConsts.reducerName, SuppliersProcessId.GetSuppliersWithPagination),

    /**
     * Набор action на получение элементов комбобокса
     * при инициализации страницы создания/редактирования поставщика
     */
    GetInitCreateEditPageItems: createReducerActions(SuppliersConsts.reducerName, SuppliersProcessId.GetInitCreateEditPageItems),

    /**
     * Набор action на создание поставщика
     */
    CreateSupplier: createReducerActions(SuppliersConsts.reducerName, SuppliersProcessId.CreateSupplier),

    /**
     * Набор action на редактирования поставщика
     */
    EditSupplier: createReducerActions(SuppliersConsts.reducerName, SuppliersProcessId.EditSupplier),

    /**
     * Набор action на получения поставщика по ID
     */
    GetSupplierById: createReducerActions(SuppliersConsts.reducerName, SuppliersProcessId.GetSupplierById),

    /**
     * Набор action на удаление поставщика
     */
    DeleteSupplier: createReducerActions(SuppliersConsts.reducerName, SuppliersProcessId.DeleteSupplier),

    /**
     * Набор action на получение реферальных аккаунтов по ID поставщика
     */
    GetReferralAccountsBySupplierId: createReducerActions(SuppliersConsts.reducerName, SuppliersProcessId.GetReferralAccountsBySupplierId),

    /**
     * Набор action на получение реферальных аккаунтов по значению поиска
     */
    GetReferralAccountsBySearchValue: createReducerActions(SuppliersConsts.reducerName, SuppliersProcessId.GetReferralAccountsBySearchValue),

    /**
     * Набор action на удаление реферального аккаунта у поставщика
     */
    DeleteSupplierReferralAccount: createReducerActions(SuppliersConsts.reducerName, SuppliersProcessId.DeleteSupplierReferralAccount),

    /**
     * Набор action на добавление реферального аккаунта к поставщику
     */
    CreateSupplierReferralAccount: createReducerActions(SuppliersConsts.reducerName, SuppliersProcessId.CreateSupplierReferralAccount)
};