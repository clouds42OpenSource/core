import { combineReducers } from 'redux';
import usersSlice from './usersSlice';
import usersFilter from './usersFilter';
import userCard from './userCard';

export const UsersPageReducer = combineReducers({
    usersSlice,
    usersFilter,
    userCard
});