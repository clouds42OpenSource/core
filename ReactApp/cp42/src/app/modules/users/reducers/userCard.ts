import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { TPostNewUserResponse } from 'app/api/endpoints/users/response';
import { TResponseLowerCase } from 'app/api/types';
import { USERS_PAGE_SLICE_NAME } from 'app/modules/users/constants';

type TInitialStateUserCard = {
    id: string;
    showUserCard: boolean;
    isNewUser?: boolean;
    lastCreateUser?: TResponseLowerCase<TPostNewUserResponse>;
}

const initialState: TInitialStateUserCard = {
    id: '',
    showUserCard: false
};

export const userCard = createSlice({
    name: USERS_PAGE_SLICE_NAME.userCard,
    initialState,
    reducers: {
        changeStateUserCard(state, { payload: { id, showUserCard, isNewUser } }: PayloadAction<TInitialStateUserCard>) {
            state.id = id;
            state.showUserCard = showUserCard;
            state.isNewUser = isNewUser;
        },
        createNewUser(state) {
            state.showUserCard = true;
            state.isNewUser = true;
        },
        toInitialState(state) {
            state.id = initialState.id;
            state.showUserCard = initialState.showUserCard;
            state.isNewUser = false;
        },
        setLastCreateUser(state, action: PayloadAction<TResponseLowerCase<TPostNewUserResponse> | undefined>) {
            state.lastCreateUser = action.payload;
        }
    }
});

export default userCard.reducer;