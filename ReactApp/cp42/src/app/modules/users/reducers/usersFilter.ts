import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { TGetUsersParamsState } from 'app/api/endpoints/users/request';
import { USERS_PAGE_SLICE_NAME } from 'app/modules/users/constants';

export type TChangeParams = Omit<TGetUsersParamsState, 'accountId'>;
export type TChangeFilterParams = Omit<TGetUsersParamsState, 'accountId' | 'pageNumber'>;
export type TActionChangeFilter = { formName: keyof TChangeFilterParams, value?: string };

const initialState: TChangeParams = {
    pageNumber: 1
};

export const usersFilter = createSlice({
    name: USERS_PAGE_SLICE_NAME.usersFilter,
    initialState,
    reducers: {
        onChangeFilter(state, action: PayloadAction<TActionChangeFilter>) {
            state[action.payload.formName] = action.payload.value;
        },
        onChangePage(state, action: PayloadAction<number>) {
            state.pageNumber = action.payload;
        },
        resetFilters(state) {
            state.pageNumber = 1;
            state.group = undefined;
            state.orderBy = undefined;
            state.searchLine = undefined;
        }
    }
});

export default usersFilter.reducer;