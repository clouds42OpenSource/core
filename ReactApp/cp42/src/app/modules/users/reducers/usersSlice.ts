import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { TGetUsers } from 'app/api/endpoints/users/response';
import { TDataLightReturn } from 'app/api/types';
import { USERS_PAGE_SLICE_NAME } from 'app/modules/users/constants';

const initialState: TDataLightReturn<TGetUsers> = {
    data: null,
    error: null,
    isLoading: false
};

export const usersSlice = createSlice({
    name: USERS_PAGE_SLICE_NAME.users,
    initialState,
    reducers: {
        loading(state) {
            state.isLoading = true;
        },
        get(state, action: PayloadAction<TGetUsers>) {
            state.data = action.payload;
            state.isLoading = false;
        },
        error(state, action: PayloadAction<string>) {
            state.data = null;
            state.error = action.payload;
            state.isLoading = false;
        }
    }
});

export default usersSlice.reducer;