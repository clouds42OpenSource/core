export const USERS_PAGE_SLICE_NAME = {
    users: 'users',
    usersFilter: 'usersFilter',
    userCard: 'userCard'
} as const;