/**
 * Процесы InfoDbCard
 */
export enum InfoDbCardProcessId {
    /**
     * Процесс получения карточки базы
     */
    ReceiveInfoDbCard = 'RECEIVE_INFODB_CARD',
    /**
     * Процесс получения общей вкладки карточки базы
     */
    ReceiveCommonInfoDbCard = 'RECEIVE_COMMON_INFODB_CARD',

    /**
     * Процесс получения прав для базы
     */
    ReceiveRightsInfoDbCard = 'RECEIVE_RIGHTS_INFODB_CARD',
    /**
     * Процесс получения данных авторизации
     */
    ReceiveHasSupportCard = 'RECEIVE_HAS_SUPPORT_CARD',
    /**
     * Процесс получения данные настроек доступа
     */
    ReceiveAccessCard = 'RECEIVE_ACCESS_CARD',

    /**
     * Процесс получения данных настроек доступа
     */
    ReceiveAccessCostCard = 'RECEIVE_ACCESS_COST_CARD',

    /**
     * Процесс получения данных сервисов
     */
    ReceiveServicesCard = 'RECEIVE_SERVICES_CARD',

    /**
     * Процесс получения статуса сервиса
     */
    ReceiveStatusServiceCard = 'RECEIVE_STATUS_SERVICE_CARD',

    /**
     * Процесс получения данных бэкапов
     */
    ReceiveBackupsCard = 'RECEIVE_BACKUPS_CARD',

}