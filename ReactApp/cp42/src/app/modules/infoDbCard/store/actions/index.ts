import { InfoDbCardConsts } from 'app/modules/infoDbCard/constants';
import { InfoDbCardProcessId } from 'app/modules/infoDbCard/InfoDbCardProcessId';
import { createReducerActions } from 'core/redux/functions';

/**
 * Все доступные actions дле редюсера Logging
 */
export const InfoDbCardActions = {
    /**
     * Набор action на получение всей карточки информационной базы
     */
    ReceiveInfoDbCard: createReducerActions(InfoDbCardConsts.reducerName, InfoDbCardProcessId.ReceiveInfoDbCard),
    /**
     * Набор action на получение вкладки Общие карточки информационной базы
     */
    ReceiveCommonInfoDbCard: createReducerActions(InfoDbCardConsts.reducerName, InfoDbCardProcessId.ReceiveCommonInfoDbCard),
    /**
     * Набор action на получение прав карточки информационной базы
     */
    ReceiveRightsInfoDbCard: createReducerActions(InfoDbCardConsts.reducerName, InfoDbCardProcessId.ReceiveRightsInfoDbCard),
    /**
     * Набор action на данные авторизации карточки информационной базы
     */
    ReceiveHasSupportCard: createReducerActions(InfoDbCardConsts.reducerName, InfoDbCardProcessId.ReceiveHasSupportCard),
    /**
     * Набор action на данные доступов карточки информационной базы
     */
    ReceiveAccessCard: createReducerActions(InfoDbCardConsts.reducerName, InfoDbCardProcessId.ReceiveAccessCard),
    /**
     * Набор action на стоимость доступов карточки информационной базы
     */
    ReceiveAccessCostCard: createReducerActions(InfoDbCardConsts.reducerName, InfoDbCardProcessId.ReceiveAccessCostCard),
    /**
     * Набор action на данных сервисов карточки информационной базы
     */
    ReceiveServicesCard: createReducerActions(InfoDbCardConsts.reducerName, InfoDbCardProcessId.ReceiveServicesCard),
    /**
     * Набор action на получение данных статуса сервиса карточки информационной базы
     */
    ReceiveStatusServiceCard: createReducerActions(InfoDbCardConsts.reducerName, InfoDbCardProcessId.ReceiveStatusServiceCard),
    /**
     * Набор action на получение бэкапов карточки информационной базы
     */
    ReceiveBackupsCard: createReducerActions(InfoDbCardConsts.reducerName, InfoDbCardProcessId.ReceiveBackupsCard)
};