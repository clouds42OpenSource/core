import { BuckupInfoDbDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/BuckupInfoDbDataModel';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте получения вкладки Архивные копии карточки информациооной базы
 */
export type ReceiveBackupsActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при ошибки получения вкладки Архивные копии карточки информациооной базы
 */
export type ReceiveBackupsActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном получении вкладки Архивные копии карточки информациооной базы
 */
export type ReceiveBackupsActionSuccessPayload = ReducerActionSuccessPayload & {
    backupsInfoDb: BuckupInfoDbDataModel
};

/**
 * Все возможные Action при получении вкладки Архивные копии карточки информациооной базы
 */
export type ReceiveBackupsActionAnyPayload =
    | ReceiveBackupsActionStartPayload
    | ReceiveBackupsActionSuccessPayload
    | ReceiveBackupsActionFailedPayload;