import { reducerActionState } from 'app/common/functions/reducerActionState';
import { InfoDbCardProcessId } from 'app/modules/infoDbCard';
import { InfoDbCardActions } from 'app/modules/infoDbCard/store/actions';
import { InfoDbCardReducerState } from 'app/modules/infoDbCard/store/reducers/InfoDbCardReducerState';
import { ReceiveInfoDbCardActionAnyPayload, ReceiveInfoDbCardActionSuccessPayload } from 'app/modules/infoDbCard/store/reducers/receiveDatabaseCardReducer/payloads';
import { InfoDbCardAccountDatabaseInfoDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/InfoDbCardAccountDatabaseInfoDataModel';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения карточки информационной базы
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const receiveInfoDbCardReducer: TPartialReducerObject<InfoDbCardReducerState, ReceiveInfoDbCardActionAnyPayload> =
  (state, action) => {
      const processId = InfoDbCardProcessId.ReceiveInfoDbCard;
      const actions = InfoDbCardActions.ReceiveInfoDbCard;

      return reducerActionState({
          action,
          actions,
          processId,
          state,
          onStartProcessState: () => {
              return reducerStateProcessStart(state, processId, {
                  commonDetails: {} as InfoDbCardAccountDatabaseInfoDataModel,
                  hasSuccessFor: {
                      ...state.hasSuccessFor,
                      hasDatabaseCardReceived: false
                  }
              });
          },
          onSuccessProcessState: () => {
              const payload = action.payload as ReceiveInfoDbCardActionSuccessPayload;
              return reducerStateProcessSuccess(state, processId, {
                  commonDetails: payload.commonDetails ? { ...payload.commonDetails } : null,
                  hasSupportInfoDb: { ...payload.hasSupportInfoDb },
                  databaseAccesses: payload.databaseAccesses,
                  servicesInfoDb: payload.servicesInfoDb,
                  backupsInfoDb: { ...payload.backupsInfoDb },
                  currency: payload.currency,
                  clientServerAccessCost: payload.clientServerAccessCost,
                  rateData: payload.rateData,
                  hasSuccessFor: {
                      ...state.hasSuccessFor,
                      hasDatabaseCardReceived: true,

                  }
              });
          }
      });
  };