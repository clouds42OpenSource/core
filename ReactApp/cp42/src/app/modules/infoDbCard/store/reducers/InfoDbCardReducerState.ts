import { InfoDbCardProcessId } from 'app/modules/infoDbCard';
import { DatabaseAccessesDataModel, RateDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/AccessInfoDbDataModel';
import { BuckupInfoDbDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/BuckupInfoDbDataModel';
import { CalculateAccessInfoDbDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/CalculateAccessInfoDbDataModel';
import { HasSupportInfoDbDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/HasSupportInfoDbDataModel';
import { InfoDbCardAccountDatabaseInfoDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/InfoDbCardAccountDatabaseInfoDataModel';
import { ServicesInfoDbDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/ServicesInfoDbDataModel';
import { RightsInfoDbCardDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getRightsInfoDbCard/data-models/InfoDbCardAccountDatabaseInfoDataModel';
import { IReducerState } from 'core/redux/interfaces';

/**
 * Состояние редюсера для работы с карточкой информационной базы
 */
export type InfoDbCardReducerState = IReducerState<InfoDbCardProcessId> & {
    /**
     * Вкладка Общие
     */
    commonDetails: InfoDbCardAccountDatabaseInfoDataModel | null;
    /**
     * Права к базе
     */
    rightsInfoDb: RightsInfoDbCardDataModel;
    /**
     * Вкладка Авторизация
     */
    hasSupportInfoDb: HasSupportInfoDbDataModel;
    /**
     * Вкладка Настройки доступов
     */
    databaseAccesses: DatabaseAccessesDataModel[];
    /**
     * Вкладка Настройки доступов, стоимость доступов
     */
    databaseAccessesCost: CalculateAccessInfoDbDataModel;
    /**
     * Вкладка Сервисы
     */
    servicesInfoDb: ServicesInfoDbDataModel[];
    /**
     * Вкладка Архивные копии
     */
    backupsInfoDb: BuckupInfoDbDataModel;
    currency: string;
    clientServerAccessCost: number;
    rateData: RateDataModel;
    serverDadabaseServiceTypeId: string;
    /**
     * содержит ярлыки для фиксации загрузок данных, была ли загрузка или ещё нет
     */
    hasSuccessFor: {
        /**
         * Если true, то карточка информационной базы уже получена
         */
        hasDatabaseCardRightsReceived?: boolean,
        hasDatabaseCardReceived?: boolean,
        hasHasSupportReceived?: boolean,
        hasServicesReceived?: boolean,
        hasAccessReceived?: boolean,
        hasAccessCostReceived?: boolean,
        hasBackupsReceived?: boolean,
        hasCommonInfoDbCardReceived?: boolean,
    };
};