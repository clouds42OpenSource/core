import { reducerActionState } from 'app/common/functions/reducerActionState';
import { InfoDbCardProcessId } from 'app/modules/infoDbCard';
import { InfoDbCardActions } from 'app/modules/infoDbCard/store/actions';
import { InfoDbCardReducerState } from 'app/modules/infoDbCard/store/reducers/InfoDbCardReducerState';
import { ReceiveRightsInfoDbCardActionAnyPayload, ReceiveRightsInfoDbCardActionSuccessPayload } from 'app/modules/infoDbCard/store/reducers/receiveRightsInfoDbCardReducer/payloads';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения прав для информационной базы
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const receiveRightsInfoDbCardReducer: TPartialReducerObject<InfoDbCardReducerState, ReceiveRightsInfoDbCardActionAnyPayload> =
    (state, action) => {
        const processId = InfoDbCardProcessId.ReceiveRightsInfoDbCard;
        const actions = InfoDbCardActions.ReceiveRightsInfoDbCard;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onStartProcessState: () => {
                return reducerStateProcessStart(state, processId, {
                    rightsInfoDb: [] as string[],
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasDatabaseCardRightsReceived: false
                    }
                });
            },
            onSuccessProcessState: () => {
                const payload = action.payload as ReceiveRightsInfoDbCardActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    rightsInfoDb: payload.rightsInfoDb,
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasDatabaseCardRightsReceived: true
                    }
                });
            }
        });
    };