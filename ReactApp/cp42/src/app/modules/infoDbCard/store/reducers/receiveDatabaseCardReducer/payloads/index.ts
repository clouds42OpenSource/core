import { DatabaseAccessesDataModel, RateDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/AccessInfoDbDataModel';
import { BuckupInfoDbDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/BuckupInfoDbDataModel';
import { HasSupportInfoDbDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/HasSupportInfoDbDataModel';
import { InfoDbCardAccountDatabaseInfoDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/InfoDbCardAccountDatabaseInfoDataModel';
import { ServicesInfoDbDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/ServicesInfoDbDataModel';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте получения карточки информациооной базы
 */
export type ReceiveInfoDbCardActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при ошибки получения карточки информациооной базы
 */
export type ReceiveInfoDbCardActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном получении карточки информациооной базы
 */
export type ReceiveInfoDbCardActionSuccessPayload = ReducerActionSuccessPayload & {

    /**
     * Информация о базе данных
     */
    commonDetails: InfoDbCardAccountDatabaseInfoDataModel | null,
    /**
     * Информация об авторизации
     */
    hasSupportInfoDb: HasSupportInfoDbDataModel,
    /**
     * Информация о доступах к базе
     */
    databaseAccesses: DatabaseAccessesDataModel[],
    /**
     * Информация о сервисах
     */
    servicesInfoDb: ServicesInfoDbDataModel[],
    /**
     * Информация об бэкапах
     */
    backupsInfoDb: BuckupInfoDbDataModel
    currency: string;
    rateData: RateDataModel;
    clientServerAccessCost: number;
};

/**
 * Все возможные Action при получении карточки информационной базы
 */
export type ReceiveInfoDbCardActionAnyPayload =
    | ReceiveInfoDbCardActionStartPayload
    | ReceiveInfoDbCardActionSuccessPayload
    | ReceiveInfoDbCardActionFailedPayload;