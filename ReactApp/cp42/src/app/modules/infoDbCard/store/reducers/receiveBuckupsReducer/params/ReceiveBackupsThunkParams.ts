import { InputBuckupsInfoDbCard } from 'app/web/api/InfoDbCardProxy/request-dto/InputBuckupsInfoDbCard';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель на запрос получения вкладки Архивные копии карточки информационной базы
 */
export type ReceiveBackupsThunkParams = IForceThunkParam & InputBuckupsInfoDbCard;