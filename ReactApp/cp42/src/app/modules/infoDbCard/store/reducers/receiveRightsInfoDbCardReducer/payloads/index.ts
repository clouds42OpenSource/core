import { RightsInfoDbCardDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getRightsInfoDbCard/data-models/InfoDbCardAccountDatabaseInfoDataModel';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте получения прав для информациооной базы
 */
export type ReceiveRightsInfoDbCardActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при ошибки получения прав для информациооной базы
 */
export type ReceiveRightsInfoDbCardActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном получении прав для информациооной базы
 */
export type ReceiveRightsInfoDbCardActionSuccessPayload = ReducerActionSuccessPayload & {

  rightsInfoDb: RightsInfoDbCardDataModel
};

/**
 * Все возможные Action при получении прав для информациооной базы
 */
export type ReceiveRightsInfoDbCardActionAnyPayload =
  | ReceiveRightsInfoDbCardActionStartPayload
  | ReceiveRightsInfoDbCardActionSuccessPayload
  | ReceiveRightsInfoDbCardActionFailedPayload;