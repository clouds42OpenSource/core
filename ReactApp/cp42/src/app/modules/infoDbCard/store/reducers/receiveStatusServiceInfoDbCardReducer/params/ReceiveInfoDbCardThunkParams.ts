import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель на запрос получения статуса сервиса карточки информационной базы
 */
export type ReceiveStatusServiceThunkParams = IForceThunkParam & {

     serviceId: string,

     accountDatabaseId: string
};