import { DatabaseAccessesDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/AccessInfoDbDataModel';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте получения вкладки Настройки доступов карточки информациооной базы
 */
export type ReceiveAccessActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при ошибки получения вкладки Настройки доступов карточки информациооной базы
 */
export type ReceiveAccessActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном получении вкладки Настройки доступов карточки информациооной базы
 */
export type ReceiveAccessActionSuccessPayload = ReducerActionSuccessPayload & {

    databaseAccesses: DatabaseAccessesDataModel[]
};

/**
 * Все возможные Action при получении вкладки Настройки доступов карточки информациооной базы
 */
export type ReceiveAccessActionAnyPayload =
    | ReceiveAccessActionStartPayload
    | ReceiveAccessActionSuccessPayload
    | ReceiveAccessActionFailedPayload;