import { StatusServiceInfoDbDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/StatusServiceInfoDbDataModel';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте получения статуса сервиса карточки информациооной базы
 */
export type ReceiveStatusServiceActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при ошибки получения статуса сервиса карточки информациооной базы
 */
export type ReceiveStatusServiceActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном получении статуса сервиса карточки информациооной базы
 */
export type ReceiveStatusServiceActionSuccessPayload = ReducerActionSuccessPayload & {

   statusService: StatusServiceInfoDbDataModel
};

/**
 * Все возможные Action при получении статуса сервиса карточки информациооной базы
 */
export type ReceiveStatusServiceActionAnyPayload =
   | ReceiveStatusServiceActionStartPayload
   | ReceiveStatusServiceActionSuccessPayload
   | ReceiveStatusServiceActionFailedPayload;