import { ServicesInfoDbDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/ServicesInfoDbDataModel';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте получения вкладки Сервисы карточки информациооной базы
 */
export type ReceiveServicesActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при ошибки получения вкладки Сервисы карточки информациооной базы
 */
export type ReceiveServicesActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном получении вкладки Сервисы карточки информациооной базы
 */
export type ReceiveServicesActionSuccessPayload = ReducerActionSuccessPayload & {

    servicesInfoDb: ServicesInfoDbDataModel[]
};

/**
 * Все возможные Action при получении вкладки Сервисы карточки информациооной базы
 */
export type ReceiveServicesActionAnyPayload =
    | ReceiveServicesActionStartPayload
    | ReceiveServicesActionSuccessPayload
    | ReceiveServicesActionFailedPayload;