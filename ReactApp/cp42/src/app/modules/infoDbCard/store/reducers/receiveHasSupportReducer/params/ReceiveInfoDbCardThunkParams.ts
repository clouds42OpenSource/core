import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель на запрос получения вкладки Авторизация карточки информационной базы
 */
export type ReceiveHasSupportThunkParams = IForceThunkParam & {

    databaseId: string;

};