import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель на запрос получения карточки информационной базы
 */
export type ReceiveInfoDbCardThunkParams = IForceThunkParam & {
    accountId: string;
    databaseId: string;

    isDbOnDelimiters?: boolean;
};