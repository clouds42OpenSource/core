import { reducerActionState } from 'app/common/functions/reducerActionState';
import { InfoDbCardProcessId } from 'app/modules/infoDbCard';
import { InfoDbCardActions } from 'app/modules/infoDbCard/store/actions';
import { InfoDbCardReducerState } from 'app/modules/infoDbCard/store/reducers/InfoDbCardReducerState';
import { ReceiveAccessActionAnyPayload, ReceiveAccessActionSuccessPayload } from 'app/modules/infoDbCard/store/reducers/receiveAccessInfoDbCardReducer/payloads';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения вкладки Настройки доступов карточки информационной базы
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const receiveAccessReducer: TPartialReducerObject<InfoDbCardReducerState, ReceiveAccessActionAnyPayload> =

    (state, action) => {
        const processId = InfoDbCardProcessId.ReceiveAccessCard;
        const actions = InfoDbCardActions.ReceiveAccessCard;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onStartProcessState: () => {
                return reducerStateProcessStart(state, processId, {
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasAccessReceived: false,
                        hasAccessCostReceived: false
                    }
                });
            },
            onSuccessProcessState: () => {
                const payload = action.payload as ReceiveAccessActionSuccessPayload;

                return reducerStateProcessSuccess(state, processId, {
                    databaseAccesses: payload.databaseAccesses,
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasAccessReceived: true,
                        hasAccessCostReceived: true
                    }
                });
            },

        });
    };