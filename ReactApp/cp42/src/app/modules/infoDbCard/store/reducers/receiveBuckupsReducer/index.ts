import { reducerActionState } from 'app/common/functions/reducerActionState';
import { InfoDbCardProcessId } from 'app/modules/infoDbCard';
import { InfoDbCardActions } from 'app/modules/infoDbCard/store/actions';
import { InfoDbCardReducerState } from 'app/modules/infoDbCard/store/reducers/InfoDbCardReducerState';
import { ReceiveBackupsActionAnyPayload, ReceiveBackupsActionSuccessPayload } from 'app/modules/infoDbCard/store/reducers/receiveBuckupsReducer/payloads';
import { BuckupInfoDbDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/BuckupInfoDbDataModel';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения вкладки Архивные копии карточки информационной базы
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const receiveBuckapsReducer: TPartialReducerObject<InfoDbCardReducerState, ReceiveBackupsActionAnyPayload> =

    (state, action) => {
        const processId = InfoDbCardProcessId.ReceiveBackupsCard;
        const actions = InfoDbCardActions.ReceiveBackupsCard;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onStartProcessState: () => {
                return reducerStateProcessStart(state, processId, {
                    backupsInfoDb: {} as BuckupInfoDbDataModel,
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasBackupsReceived: false
                    }
                });
            },
            onSuccessProcessState: () => {
                const payload = action.payload as ReceiveBackupsActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    backupsInfoDb: { ...payload.backupsInfoDb },

                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasBackupsReceived: true
                    }

                });
            }
        });
    };