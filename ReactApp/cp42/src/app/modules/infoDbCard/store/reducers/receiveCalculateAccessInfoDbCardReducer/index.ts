import { reducerActionState } from 'app/common/functions/reducerActionState';
import { InfoDbCardProcessId } from 'app/modules/infoDbCard';
import { InfoDbCardActions } from 'app/modules/infoDbCard/store/actions';
import { InfoDbCardReducerState } from 'app/modules/infoDbCard/store/reducers/InfoDbCardReducerState';
import { ReceiveAccessCostActionAnyPayload, ReceiveAccessCostActionSuccessPayload } from 'app/modules/infoDbCard/store/reducers/receiveCalculateAccessInfoDbCardReducer/payloads';
import { CalculateAccessInfoDbDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/CalculateAccessInfoDbDataModel';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения подсчета стоимости доступов к информационной базе
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const receiveAccessCostReducer: TPartialReducerObject<InfoDbCardReducerState, ReceiveAccessCostActionAnyPayload> =

  (state, action) => {
      const processId = InfoDbCardProcessId.ReceiveAccessCostCard;
      const actions = InfoDbCardActions.ReceiveAccessCostCard;

      return reducerActionState({
          action,
          actions,
          processId,
          state,
          onStartProcessState: () => {
              return reducerStateProcessStart(state, processId, {
                  databaseAccessesCost: [] as CalculateAccessInfoDbDataModel,
                  hasSuccessFor: {
                      ...state.hasSuccessFor,
                      hasAccessCostReceived: false
                  }
              });
          },
          onSuccessProcessState: () => {
              const payload = action.payload as ReceiveAccessCostActionSuccessPayload;
              return reducerStateProcessSuccess(state, processId, {
                  databaseAccessesCost: [...payload.databaseAccessesCost],
                  hasSuccessFor: {
                      ...state.hasSuccessFor,
                      hasAccessCostReceived: true
                  }
              });
          }
      });
  };