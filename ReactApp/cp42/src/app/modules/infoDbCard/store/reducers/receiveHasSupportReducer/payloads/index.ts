import { HasSupportInfoDbDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/HasSupportInfoDbDataModel';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте получения вкладки Авторизация карточки информациооной базы
 */
export type ReceiveHasSupportActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при ошибки получения вкладки Авторизация карточки информациооной базы
 */
export type ReceiveHasSupportActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном получении вкладки Авторизация карточки информациооной базы
 */
export type ReceiveHasSupportActionSuccessPayload = ReducerActionSuccessPayload & {

   hasSupportInfoDb: HasSupportInfoDbDataModel
};

/**
 * Все возможные Action при получении вкладки Авторизация карточки информациооной базы
 */
export type ReceiveHasSupportActionAnyPayload =
   | ReceiveHasSupportActionStartPayload
   | ReceiveHasSupportActionSuccessPayload
   | ReceiveHasSupportActionFailedPayload;