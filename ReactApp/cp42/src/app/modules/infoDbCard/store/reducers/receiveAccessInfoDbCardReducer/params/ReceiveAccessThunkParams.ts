import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель на запрос получения вкладки Настройки доступов карточки информационной базы
 */

export type ReceiveAccessThunkParams = IForceThunkParam & {
    databaseId: string;
    searchString: string
    usersByAccessFilterType: number;
};