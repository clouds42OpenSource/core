import { InfoDbCardAccountDatabaseInfoDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/InfoDbCardAccountDatabaseInfoDataModel';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте получения карточки информациооной базы
 */
export type ReceiveCommonInfoDbCardActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при ошибки получения карточки информациооной базы
 */
export type ReceiveCommonInfoDbCardActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном получении карточки информациооной базы
 */
export type ReceiveCommonInfoDbCardActionSuccessPayload = ReducerActionSuccessPayload & {

    /**
     * Информация о базе данных
     */
    commonDetails: InfoDbCardAccountDatabaseInfoDataModel | null,

};

/**
 * Все возможные Action при получении карточки информациооной базы
 */
export type ReceiveCommonInfoDbCardActionAnyPayload =
    | ReceiveCommonInfoDbCardActionStartPayload
    | ReceiveCommonInfoDbCardActionSuccessPayload
    | ReceiveCommonInfoDbCardActionFailedPayload;