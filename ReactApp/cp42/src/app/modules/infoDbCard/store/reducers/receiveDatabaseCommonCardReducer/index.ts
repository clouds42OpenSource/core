import { reducerActionState } from 'app/common/functions/reducerActionState';
import { InfoDbCardProcessId } from 'app/modules/infoDbCard';
import { InfoDbCardActions } from 'app/modules/infoDbCard/store/actions';
import { InfoDbCardReducerState } from 'app/modules/infoDbCard/store/reducers/InfoDbCardReducerState';
import { ReceiveCommonInfoDbCardActionAnyPayload, ReceiveCommonInfoDbCardActionSuccessPayload } from 'app/modules/infoDbCard/store/reducers/receiveDatabaseCommonCardReducer/payloads';
import { InfoDbCardAccountDatabaseInfoDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/InfoDbCardAccountDatabaseInfoDataModel';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения вкладки Общие карточки информационной базы
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const receiveCommonInfoDbCardReducer: TPartialReducerObject<InfoDbCardReducerState, ReceiveCommonInfoDbCardActionAnyPayload> =
  (state, action) => {
      const processId = InfoDbCardProcessId.ReceiveCommonInfoDbCard;
      const actions = InfoDbCardActions.ReceiveCommonInfoDbCard;

      return reducerActionState({
          action,
          actions,
          processId,
          state,
          onStartProcessState: () => {
              return reducerStateProcessStart(state, processId, {
                  commonDetails: {} as InfoDbCardAccountDatabaseInfoDataModel,
                  hasSuccessFor: {
                      ...state.hasSuccessFor,
                      hasCommonInfoDbCardReceived: false,
                      hasDatabaseCardReceived: false
                  }
              });
          },
          onSuccessProcessState: () => {
              const payload = action.payload as ReceiveCommonInfoDbCardActionSuccessPayload;
              return reducerStateProcessSuccess(state, processId, {
                  commonDetails: payload.commonDetails ? { ...payload.commonDetails } : null,
                  hasSuccessFor: {
                      ...state.hasSuccessFor,
                      hasCommonInfoDbCardReceived: true,
                      hasDatabaseCardReceived: true,
                  }
              });
          }
      });
  };