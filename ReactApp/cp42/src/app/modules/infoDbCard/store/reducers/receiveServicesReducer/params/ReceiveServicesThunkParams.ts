import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель на запрос получения вкладки Сервисы карточки информационной базы
 */
export type ReceiveServicesThunkParams = IForceThunkParam & {

    AccountId: string,
    ServiceName: string,
    ServiceExtensionsForDatabaseType: number,
    AccountDatabaseId: string

};