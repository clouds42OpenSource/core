import { reducerActionState } from 'app/common/functions/reducerActionState';
import { InfoDbCardProcessId } from 'app/modules/infoDbCard';
import { InfoDbCardActions } from 'app/modules/infoDbCard/store/actions';
import { InfoDbCardReducerState } from 'app/modules/infoDbCard/store/reducers/InfoDbCardReducerState';
import { ReceiveHasSupportActionAnyPayload, ReceiveHasSupportActionSuccessPayload } from 'app/modules/infoDbCard/store/reducers/receiveHasSupportReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения вкладки Авторизация карточки информационной базы
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const receiveHasSupportReducer: TPartialReducerObject<InfoDbCardReducerState, ReceiveHasSupportActionAnyPayload> =

  (state, action) => {
      const processId = InfoDbCardProcessId.ReceiveHasSupportCard;
      const actions = InfoDbCardActions.ReceiveHasSupportCard;

      return reducerActionState({
          action,
          actions,
          processId,
          state,

          onSuccessProcessState: () => {
              const payload = action.payload as ReceiveHasSupportActionSuccessPayload;
              return reducerStateProcessSuccess(state, processId, {
                  hasSupportInfoDb: { ...payload.hasSupportInfoDb },
                  hasSuccessFor: {
                      ...state.hasSuccessFor,
                      hasHasSupportReceived: true
                  }

              });
          }
      });
  };