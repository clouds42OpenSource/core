import { CalculateAccessInfoDbDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/CalculateAccessInfoDbDataModel';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте получения стоимости доступов ки информациооной базе
 */
export type ReceiveAccessCostActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при ошибки получения стоимости доступов к информациооной базе
 */
export type ReceiveAccessCostActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном получении стоимости доступов к информациооной базе
 */
export type ReceiveAccessCostActionSuccessPayload = ReducerActionSuccessPayload & {

    databaseAccessesCost: CalculateAccessInfoDbDataModel
};

/**
 * Все возможные Action при получении стоимости доступов к информациооной базе
 */
export type ReceiveAccessCostActionAnyPayload =
    | ReceiveAccessCostActionStartPayload
    | ReceiveAccessCostActionSuccessPayload
    | ReceiveAccessCostActionFailedPayload;