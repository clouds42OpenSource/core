import { reducerActionState } from 'app/common/functions/reducerActionState';
import { InfoDbCardProcessId } from 'app/modules/infoDbCard';
import { InfoDbCardActions } from 'app/modules/infoDbCard/store/actions';
import { InfoDbCardReducerState } from 'app/modules/infoDbCard/store/reducers/InfoDbCardReducerState';
import { ReceiveStatusServiceActionAnyPayload, ReceiveStatusServiceActionSuccessPayload } from 'app/modules/infoDbCard/store/reducers/receiveStatusServiceInfoDbCardReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения статуса сервиса карточки информационной базы
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const receiveStatusServiceReducer: TPartialReducerObject<InfoDbCardReducerState, ReceiveStatusServiceActionAnyPayload> =

  (state, action) => {
      const processId = InfoDbCardProcessId.ReceiveStatusServiceCard;
      const actions = InfoDbCardActions.ReceiveStatusServiceCard;

      return reducerActionState({
          action,
          actions,
          processId,
          state,
          onSuccessProcessState: () => {
              const payload = action.payload as ReceiveStatusServiceActionSuccessPayload;

              const result = state.servicesInfoDb.map(i => {
                  if (payload.statusService.serviceId === i.id) {
                      return { ...i, extensionState: payload.statusService.extensionDatabaseStatus };
                  }
                  return i;
              });

              return reducerStateProcessSuccess(state, processId, {
                  servicesInfoDb: [...result],

              });
          }
      });
  };