import { InputCalculateAccesses } from 'app/web/api/InfoDbCardProxy/request-dto/InputCalculateAccesses';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель на запрос получения карточки информационной базы
 */
export type ReceiveAccessCostThunkParams = IForceThunkParam & InputCalculateAccesses