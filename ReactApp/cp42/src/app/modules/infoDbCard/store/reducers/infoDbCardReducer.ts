import { partialFillOf } from 'app/common/functions/fillObjectWithUndefined';
import { InfoDbCardConsts } from 'app/modules/infoDbCard/constants';
import { receiveAccessReducer } from 'app/modules/infoDbCard/store/reducers/receiveAccessInfoDbCardReducer';
import { receiveBuckapsReducer } from 'app/modules/infoDbCard/store/reducers/receiveBuckupsReducer';
import { receiveAccessCostReducer } from 'app/modules/infoDbCard/store/reducers/receiveCalculateAccessInfoDbCardReducer';
import { receiveInfoDbCardReducer } from 'app/modules/infoDbCard/store/reducers/receiveDatabaseCardReducer';
import { receiveCommonInfoDbCardReducer } from 'app/modules/infoDbCard/store/reducers/receiveDatabaseCommonCardReducer';
import { receiveHasSupportReducer } from 'app/modules/infoDbCard/store/reducers/receiveHasSupportReducer';
import { receiveRightsInfoDbCardReducer } from 'app/modules/infoDbCard/store/reducers/receiveRightsInfoDbCardReducer';
import { receiveServicesReducer } from 'app/modules/infoDbCard/store/reducers/receiveServicesReducer';
import { receiveStatusServiceReducer } from 'app/modules/infoDbCard/store/reducers/receiveStatusServiceInfoDbCardReducer';
import { RateDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/AccessInfoDbDataModel';
import { HasSupportInfoDbDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/HasSupportInfoDbDataModel';
import { InfoDbCardAccountDatabaseInfoDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/InfoDbCardAccountDatabaseInfoDataModel';
import { createReducer, initReducerState } from 'core/redux/functions';

/**
 * Начальное состояние редюсера InfoDbCard
 */
const initialState = initReducerState<any>(
    InfoDbCardConsts.reducerName,
    {
        commonDetails: partialFillOf<InfoDbCardAccountDatabaseInfoDataModel>(),
        hasSupportInfoDb: partialFillOf<HasSupportInfoDbDataModel>(),
        rightsInfoDb: [],
        databaseAccesses: [],
        databaseAccessesCost: [],
        servicesInfoDb: [],
        backupsInfoDb: {
            databaseBackups: [],
            dbBackupsCount: 0
        },
        currency: '',
        clientServerAccessCost: 0,
        rateData: partialFillOf<RateDataModel>(),
        hasSuccessFor: {
            hasDatabaseCardReceived: false,
            hasCommonInfoDbCardReceived: false,
            hasHasSupportReceived: false,
            hasServicesReceived: false,
            hasAccessReceived: false,
            hasAccessCostReceived: false,
            hasBackupsReceived: false,
            hasDatabaseCardRightsReceived: false
        }
    }
);

/**
 * Объединённые части редюсера для всего состояния InfoDbCard
 */
const partialReducers = [
    receiveInfoDbCardReducer,
    receiveCommonInfoDbCardReducer,
    receiveRightsInfoDbCardReducer,
    receiveHasSupportReducer,
    receiveAccessReducer,
    receiveAccessCostReducer,
    receiveServicesReducer,
    receiveStatusServiceReducer,
    receiveBuckapsReducer,

];

/**
 * Редюсер InfoDbCard
 */
export const infoDbCardReducer = createReducer(initialState, partialReducers);