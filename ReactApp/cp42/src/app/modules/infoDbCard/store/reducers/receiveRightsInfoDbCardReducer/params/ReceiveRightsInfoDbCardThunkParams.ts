import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель на запрос получения прав для информационной базы
 */
export type ReceiveRightsInfoDbCardThunkParams = IForceThunkParam & {

    databaseId: string;

    objectActions: string;
};