import { reducerActionState } from 'app/common/functions/reducerActionState';
import { InfoDbCardProcessId } from 'app/modules/infoDbCard';
import { InfoDbCardActions } from 'app/modules/infoDbCard/store/actions';
import { InfoDbCardReducerState } from 'app/modules/infoDbCard/store/reducers/InfoDbCardReducerState';
import { ReceiveServicesActionAnyPayload, ReceiveServicesActionSuccessPayload } from 'app/modules/infoDbCard/store/reducers/receiveServicesReducer/payloads';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения вкладки Сервисы карточки информационной базы
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const receiveServicesReducer: TPartialReducerObject<InfoDbCardReducerState, ReceiveServicesActionAnyPayload> =

  (state, action) => {
      const processId = InfoDbCardProcessId.ReceiveServicesCard;
      const actions = InfoDbCardActions.ReceiveServicesCard;

      return reducerActionState({
          action,
          actions,
          processId,
          state,
          onStartProcessState: () => {
              return reducerStateProcessStart(state, processId, {
                  hasSuccessFor: {
                      ...state.hasSuccessFor,
                      hasServicesReceived: false
                  }
              });
          },
          onSuccessProcessState: () => {
              const payload = action.payload as ReceiveServicesActionSuccessPayload;
              return reducerStateProcessSuccess(state, processId, {

                  servicesInfoDb: payload.servicesInfoDb,
                  hasSuccessFor: {
                      ...state.hasSuccessFor,
                      hasServicesReceived: true
                  }

              });
          }
      });
  };