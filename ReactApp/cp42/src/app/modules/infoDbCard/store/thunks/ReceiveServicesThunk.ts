import { InfoDbCardActions } from 'app/modules/infoDbCard/store/actions';
import { ReceiveServicesThunkParams } from 'app/modules/infoDbCard/store/reducers/receiveServicesReducer/params';
import { ReceiveServicesActionFailedPayload, ReceiveServicesActionStartPayload, ReceiveServicesActionSuccessPayload } from 'app/modules/infoDbCard/store/reducers/receiveServicesReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = ReceiveServicesActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = ReceiveServicesActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = ReceiveServicesActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = ReceiveServicesThunkParams;

const { ReceiveServicesCard: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = InfoDbCardActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для запроса списка сервисов карточки ИБ
 */
export class ReceiveServicesInfoDbCardThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new ReceiveServicesInfoDbCardThunk().execute(args, args?.showLoadingProgress);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        return {
            condition: args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().InfoDbCardState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        const requestArgs = args.inputParams!;
        const infoDbCardApi = InterlayerApiProxy.getInfoDbCardApi();

        try {
            const servicesCardResponse = await infoDbCardApi.GetServicesInfoDbCard(RequestKind.SEND_BY_ROBOT, {
                AccountDatabaseId: requestArgs.AccountDatabaseId,
                AccountId: requestArgs.AccountId,
                ServiceExtensionsForDatabaseType: requestArgs.ServiceExtensionsForDatabaseType,
                ServiceName: requestArgs.ServiceName
            });

            args.success({
                servicesInfoDb: servicesCardResponse
            });
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}