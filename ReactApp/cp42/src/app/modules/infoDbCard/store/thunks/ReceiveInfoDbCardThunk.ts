import { accountId, contextAccountId } from 'app/api';
import { AccountUserGroup } from 'app/common/enums';
import { ReceiveDatabaseCardActionFailedPayload, ReceiveDatabaseCardActionStartPayload } from 'app/modules/databaseCard/store/reducers/receiveDatabaseCardReducer/payloads';
import { InfoDbCardActions } from 'app/modules/infoDbCard/store/actions';
import { ReceiveAccessThunkParams } from 'app/modules/infoDbCard/store/reducers/receiveAccessInfoDbCardReducer/params';
import { ReceiveBackupsThunkParams } from 'app/modules/infoDbCard/store/reducers/receiveBuckupsReducer/params';
import { ReceiveInfoDbCardThunkParams } from 'app/modules/infoDbCard/store/reducers/receiveDatabaseCardReducer/params';
import { ReceiveInfoDbCardActionSuccessPayload } from 'app/modules/infoDbCard/store/reducers/receiveDatabaseCardReducer/payloads';
import { ReceiveServicesThunkParams } from 'app/modules/infoDbCard/store/reducers/receiveServicesReducer/params';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { BuckupInfoDbDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/BuckupInfoDbDataModel';
import { HasSupportInfoDbDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/HasSupportInfoDbDataModel';
import { ServicesInfoDbDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/ServicesInfoDbDataModel';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = ReceiveDatabaseCardActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = ReceiveInfoDbCardActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = ReceiveDatabaseCardActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = IForceThunkParam
    & ReceiveInfoDbCardThunkParams
    & Partial<ReceiveAccessThunkParams>
    & ReceiveServicesThunkParams
    & ReceiveBackupsThunkParams &
    { isOther?: boolean };

const { ReceiveInfoDbCard: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = InfoDbCardActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для запроса всей карточки ИБ
 */
export class ReceiveInfoDbCardThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new ReceiveInfoDbCardThunk().execute(args, args?.showLoadingProgress);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        const infoDbCardState = args.getStore().InfoDbCardState;
        return {
            condition: !infoDbCardState.hasSuccessFor.hasDatabaseCardReceived || args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().InfoDbCardState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        const requestArgs = args.inputParams!;
        const infoDbCardApi = InterlayerApiProxy.getInfoDbCardApi();
        const store = args.getStore();
        const { currentUserGroups } = store.Global.getCurrentSessionSettingsReducer.settings.currentContextInfo;

        try {
            let supportDataCardResponse;
            let servicesCardResponse;

            const databaseCardResponse = await infoDbCardApi.getInfoDbCard(RequestKind.SEND_BY_ROBOT, {
                accountId: requestArgs.accountId,
                databaseId: requestArgs.databaseId
            });

            if (typeof databaseCardResponse === 'string') {
                args.failed({
                    error: databaseCardResponse as unknown as ErrorObject
                });
                return;
            }

            if (
                !requestArgs.isOther &&
                !(currentUserGroups.includes(AccountUserGroup.AccountUser) && currentUserGroups.length === 1)
            ) {
                supportDataCardResponse = await infoDbCardApi.getSupportDataCard(RequestKind.SEND_BY_ROBOT, {
                    ...args.inputParams!
                });
            } else {
                supportDataCardResponse = {} as HasSupportInfoDbDataModel;
            }

            const { rateData, databaseAccesses, currency, clientServerAccessCost } = await infoDbCardApi.GetAccessInfoDbCard(RequestKind.SEND_BY_ROBOT, {
                DatabaseId: requestArgs.databaseId,
                SearchString: requestArgs.searchString || '',
                UsersByAccessFilterType: requestArgs.usersByAccessFilterType || 0,
                showLoadingProgress: false
            });

            if (args.inputParams?.isDbOnDelimiters) {
                servicesCardResponse = await infoDbCardApi.GetServicesInfoDbCard(RequestKind.SEND_BY_ROBOT, {
                    AccountDatabaseId: requestArgs.AccountDatabaseId,
                    AccountId: requestArgs.AccountId,
                    ServiceExtensionsForDatabaseType: requestArgs.ServiceExtensionsForDatabaseType,
                    ServiceName: requestArgs.ServiceName
                });
            } else {
                servicesCardResponse = [] as ServicesInfoDbDataModel[];
            }

            let backupCardResponse: BuckupInfoDbDataModel = {
                databaseBackups: [],
                dbBackupsCount: 0
            };

            if (!requestArgs.isOther) {
                backupCardResponse = await infoDbCardApi.GetBackupsInfoDbCard(RequestKind.SEND_BY_ROBOT, {
                    DatabaseId: requestArgs.DatabaseId,
                    CreationDateFrom: requestArgs.CreationDateFrom,
                    CreationDateTo: requestArgs.CreationDateTo
                });
            }

            args.success({
                commonDetails: databaseCardResponse,
                hasSupportInfoDb: supportDataCardResponse,
                databaseAccesses,
                servicesInfoDb: servicesCardResponse,
                backupsInfoDb: backupCardResponse,
                currency,
                rateData,
                clientServerAccessCost
            });
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}