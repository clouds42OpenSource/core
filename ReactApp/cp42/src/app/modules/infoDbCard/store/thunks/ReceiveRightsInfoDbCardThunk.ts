import { InfoDbCardActions } from 'app/modules/infoDbCard/store/actions';
import { ReceiveRightsInfoDbCardThunkParams } from 'app/modules/infoDbCard/store/reducers/receiveRightsInfoDbCardReducer/params';
import { ReceiveRightsInfoDbCardActionFailedPayload, ReceiveRightsInfoDbCardActionStartPayload, ReceiveRightsInfoDbCardActionSuccessPayload } from 'app/modules/infoDbCard/store/reducers/receiveRightsInfoDbCardReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = ReceiveRightsInfoDbCardActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = ReceiveRightsInfoDbCardActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = ReceiveRightsInfoDbCardActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = ReceiveRightsInfoDbCardThunkParams;

const { ReceiveRightsInfoDbCard: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = InfoDbCardActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для запроса списка прав к ИБ
 */
export class ReceiveRightsInfoDbCardThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new ReceiveRightsInfoDbCardThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        const infoDbCardState = args.getStore().InfoDbCardState;
        return {
            condition: !infoDbCardState.hasSuccessFor.hasDatabaseCardRightsReceived || args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().InfoDbCardState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        const requestArgs = args.inputParams!;
        const infoDbCardApi = InterlayerApiProxy.getInfoDbCardApi();

        try {
            const infoDbRightsResponse = await infoDbCardApi.getRightsInfoDbCard(RequestKind.SEND_BY_ROBOT, {

                databaseId: requestArgs.databaseId,
                objectActions: requestArgs.objectActions

            });
            args.success({
                rightsInfoDb: infoDbRightsResponse
            });
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}