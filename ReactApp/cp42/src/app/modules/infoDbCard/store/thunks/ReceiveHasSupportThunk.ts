import { InfoDbCardActions } from 'app/modules/infoDbCard/store/actions';
import { ReceiveHasSupportThunkParams } from 'app/modules/infoDbCard/store/reducers/receiveHasSupportReducer/params';
import { ReceiveHasSupportActionFailedPayload, ReceiveHasSupportActionStartPayload, ReceiveHasSupportActionSuccessPayload } from 'app/modules/infoDbCard/store/reducers/receiveHasSupportReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = ReceiveHasSupportActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = ReceiveHasSupportActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = ReceiveHasSupportActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = ReceiveHasSupportThunkParams;

const { ReceiveHasSupportCard: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = InfoDbCardActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для запроса списка записей вкладки Авторизации карточки ИБ
 */
export class ReceiveHasSupportThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new ReceiveHasSupportThunk().execute(args, args?.showLoadingProgress);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        const infoDbCardState = args.getStore().InfoDbCardState;
        return {
            condition: !infoDbCardState.hasSuccessFor.hasHasSupportReceived || args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().InfoDbCardState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        const infoDbCardApi = InterlayerApiProxy.getInfoDbCardApi();

        try {
            const supportDataCardResponse = await infoDbCardApi.getSupportDataCard(RequestKind.SEND_BY_ROBOT, {
                ...args.inputParams!
            });

            args.success({
                hasSupportInfoDb: supportDataCardResponse

            });
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}