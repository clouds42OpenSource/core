import { InfoDbCardActions } from 'app/modules/infoDbCard/store/actions';
import { ReceiveAccessCostThunkParams } from 'app/modules/infoDbCard/store/reducers/receiveCalculateAccessInfoDbCardReducer/params';
import { ReceiveAccessCostActionFailedPayload, ReceiveAccessCostActionStartPayload, ReceiveAccessCostActionSuccessPayload } from 'app/modules/infoDbCard/store/reducers/receiveCalculateAccessInfoDbCardReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = ReceiveAccessCostActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = ReceiveAccessCostActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = ReceiveAccessCostActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = ReceiveAccessCostThunkParams;

const { ReceiveAccessCostCard: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = InfoDbCardActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для запроса списка записей стоимости доступов к ИБ
 */
export class ReceiveAccessCostThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new ReceiveAccessCostThunk().execute(args, args?.showLoadingProgress);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        const infoDbCardState = args.getStore().InfoDbCardState;
        return {
            condition: !infoDbCardState.hasSuccessFor.hasHasSupportReceived || args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().InfoDbCardState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        const requestArgs = args.inputParams!;
        const infoDbCardApi = InterlayerApiProxy.getInfoDbCardApi();

        try {
            const accessCostDataCardResponse = await infoDbCardApi.CalculateAccesses(RequestKind.SEND_BY_ROBOT, {
                accountUserDataModelsList: requestArgs.accountUserDataModelsList,
                serviceTypesIdsList: requestArgs.serviceTypesIdsList,

            });

            args.success({
                databaseAccessesCost: accessCostDataCardResponse
            });
        } catch (er) {
            args.success({
                databaseAccessesCost: []
            });
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}