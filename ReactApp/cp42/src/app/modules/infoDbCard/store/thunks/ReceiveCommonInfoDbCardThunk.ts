import { InfoDbCardActions } from 'app/modules/infoDbCard/store/actions';
import { ReceiveInfoDbCardThunkParams } from 'app/modules/infoDbCard/store/reducers/receiveDatabaseCardReducer/params';
import { ReceiveCommonInfoDbCardActionFailedPayload, ReceiveCommonInfoDbCardActionStartPayload, ReceiveCommonInfoDbCardActionSuccessPayload } from 'app/modules/infoDbCard/store/reducers/receiveDatabaseCommonCardReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = ReceiveCommonInfoDbCardActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = ReceiveCommonInfoDbCardActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = ReceiveCommonInfoDbCardActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = IForceThunkParam
    & ReceiveInfoDbCardThunkParams

const { ReceiveCommonInfoDbCard: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = InfoDbCardActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для запроса списка записей вкладки Общие карточки ИБ
 */
export class ReceiveCommonInfoDbCardThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new ReceiveCommonInfoDbCardThunk().execute(args, args?.showLoadingProgress);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        const infoDbCardState = args.getStore().InfoDbCardState;
        return {
            condition: !infoDbCardState.hasSuccessFor.hasDatabaseCardReceived || args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().InfoDbCardState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        const requestArgs = args.inputParams!;
        const infoDbCardApi = InterlayerApiProxy.getInfoDbCardApi();

        try {
            const infoDbCardResponse = await infoDbCardApi.getInfoDbCard(RequestKind.SEND_BY_ROBOT, {
                accountId: requestArgs.accountId,
                databaseId: requestArgs.databaseId
            });

            if (typeof infoDbCardResponse !== 'string') {
                args.success({
                    commonDetails: infoDbCardResponse,
                });
            } else {
                args.failed({
                    error: infoDbCardResponse as unknown as ErrorObject
                });
            }
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}