/**
 * Константы для модуля InfoDbCard
 */
export const InfoDbCardConsts = {
    /**
     * Название редюсера для модуля InfoDbCard
     */
    reducerName: 'INFODB_CARD',

};