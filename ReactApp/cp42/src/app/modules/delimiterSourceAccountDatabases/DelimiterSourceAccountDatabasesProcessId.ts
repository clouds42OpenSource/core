/**
 * Процесы DelimiterSourceAccountDatabases
 */
export enum DelimiterSourceAccountDatabasesProcessId {
    /**
     * Процесс получить материнские базы разделителей
     */
    ReceiveDelimiterSourceAccountDatabases = 'RECEIVE_DELIMITER_SOURCE_ACCOUNT_DATABASES',

    /**
     * Процесс обновления материнской базы разделителей
     */
    UpdateDelimiterSourceAccountDatabase = 'UPDATE_DELIMITER_SOURCE_ACCOUNT_DATABASE',

    /**
     * Процесс добавления записи материнской базы разделителей
     */
    InsertDelimiterSourceAccountDatabase = 'INSERT_DELIMITER_SOURCE_ACCOUNT_DATABASE',

    /**
     * Процесс удаления записи материнской базы разделителей
     */
    DeleteDelimiterSourceAccountDatabase = 'DELETE_DELIMITER_SOURCE_ACCOUNT_DATABASE'
}