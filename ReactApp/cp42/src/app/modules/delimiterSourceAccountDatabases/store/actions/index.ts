import { DelimiterSourceAccountDatabasesConsts } from 'app/modules/delimiterSourceAccountDatabases/constants';
import { DelimiterSourceAccountDatabasesProcessId } from 'app/modules/delimiterSourceAccountDatabases/DelimiterSourceAccountDatabasesProcessId';
import { createReducerActions } from 'core/redux/functions';

/**
 * Все доступные actions дле редюсера DelimiterSourceAccountDatabases
 */
export const DelimiterSourceAccountDatabasesActions = {
    /**
     * Набор action на получение материнских баз разделителей
     */
    ReceiveDelimiterSourceAccountDatabases: createReducerActions(DelimiterSourceAccountDatabasesConsts.reducerName, DelimiterSourceAccountDatabasesProcessId.ReceiveDelimiterSourceAccountDatabases),
    /**
     * Набор action на обновление записи материнской базы разделителей
     */
    UpdateDelimiterSourceAccountDatabase: createReducerActions(DelimiterSourceAccountDatabasesConsts.reducerName, DelimiterSourceAccountDatabasesProcessId.UpdateDelimiterSourceAccountDatabase),
    /**
     * Набор action на добавлениезаписи материнской базы разделителей
     */
    InsertDelimiterSourceAccountDatabase: createReducerActions(DelimiterSourceAccountDatabasesConsts.reducerName, DelimiterSourceAccountDatabasesProcessId.InsertDelimiterSourceAccountDatabase),

    /**
     * Набор action на удаление материнской базы разделителей
     */
    DeleteDelimiterSourceAccountDatabase: createReducerActions(DelimiterSourceAccountDatabasesConsts.reducerName, DelimiterSourceAccountDatabasesProcessId.DeleteDelimiterSourceAccountDatabase)

};