import { DelimiterSourceAccountDatabasesActions } from 'app/modules/delimiterSourceAccountDatabases/store/actions';
import { ReceiveDelimiterSourceAccountDatabasesThunkParams } from 'app/modules/delimiterSourceAccountDatabases/store/reducers/receiveDelimiterSourceAccountDatabasesReducer/params';
import {
    ReceiveDelimiterSourceAccountDatabasesActionFailedPayload,
    ReceiveDelimiterSourceAccountDatabasesActionStartPayload,
    ReceiveDelimiterSourceAccountDatabasesActionSuccessPayload
} from 'app/modules/delimiterSourceAccountDatabases/store/reducers/receiveDelimiterSourceAccountDatabasesReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = ReceiveDelimiterSourceAccountDatabasesActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = ReceiveDelimiterSourceAccountDatabasesActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = ReceiveDelimiterSourceAccountDatabasesActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = ReceiveDelimiterSourceAccountDatabasesThunkParams;

const { ReceiveDelimiterSourceAccountDatabases: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = DelimiterSourceAccountDatabasesActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для запроса списка действий облака для логирования
 */
export class ReceiveDelimiterSourceAccountDatabasesThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new ReceiveDelimiterSourceAccountDatabasesThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        const processFlowState = args.getStore().DelimiterSourceAccountDatabasesState;
        return {
            condition: !processFlowState.hasSuccessFor.hasDelimiterSourceAccountDatabasesReceived || args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().DelimiterSourceAccountDatabasesState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        try {
            const api = InterlayerApiProxy.getDelimiterSourceAccountDatabasesApi();
            const response = await api.receiveDelimiterSourceAccountDatabases(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, args.inputParams!);

            args.success({
                delimiterSourceAccountDatabases: response
            });
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}