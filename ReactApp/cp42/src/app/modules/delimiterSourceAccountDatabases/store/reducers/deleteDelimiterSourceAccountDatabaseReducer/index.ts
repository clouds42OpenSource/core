import { reducerActionState } from 'app/common/functions/reducerActionState';
import { DelimiterSourceAccountDatabasesProcessId } from 'app/modules/delimiterSourceAccountDatabases';
import { DelimiterSourceAccountDatabasesActions } from 'app/modules/delimiterSourceAccountDatabases/store/actions';
import { DelimiterSourceAccountDatabasesReducerState } from 'app/modules/delimiterSourceAccountDatabases/store/reducers';
import {
    DeleteDelimiterSourceAccountDatabaseActionAnyPayload,
    DeleteDelimiterSourceAccountDatabaseActionSuccessPayload
} from 'app/modules/delimiterSourceAccountDatabases/store/reducers/deleteDelimiterSourceAccountDatabaseReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для удаления записи в DelimiterSourceAccountDatabase
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const deleteDelimiterSourceAccountDatabaseReducer: TPartialReducerObject<DelimiterSourceAccountDatabasesReducerState, DeleteDelimiterSourceAccountDatabaseActionAnyPayload> =
    (state, action) => {
        const processId = DelimiterSourceAccountDatabasesProcessId.DeleteDelimiterSourceAccountDatabase;
        const actions = DelimiterSourceAccountDatabasesActions.DeleteDelimiterSourceAccountDatabase;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as DeleteDelimiterSourceAccountDatabaseActionSuccessPayload;

                const newRecords = state.delimiterSourceAccountDatabases.records.filter(item => !(item.accountDatabaseId === payload.accountDatabaseId && item.dbTemplateDelimiterCode === payload.dbTemplateDelimiterCode));

                return reducerStateProcessSuccess(state, processId, {
                    delimiterSourceAccountDatabases: {
                        ...state.delimiterSourceAccountDatabases,
                        records: newRecords,
                        metadata: {
                            ...state.delimiterSourceAccountDatabases.metadata,
                            pageSize: state.delimiterSourceAccountDatabases.metadata.pageSize - 1,
                            totalItemCount: state.delimiterSourceAccountDatabases.metadata.totalItemCount - 1
                        }
                    }
                });
            }
        });
    };