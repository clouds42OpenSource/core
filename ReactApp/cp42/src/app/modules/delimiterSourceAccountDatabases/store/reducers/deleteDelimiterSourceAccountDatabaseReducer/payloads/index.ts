import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения удаления записи материнской базы разделителей
 */
export type DeleteDelimiterSourceAccountDatabaseActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении удаления записи материнской базы разделителей
 */
export type DeleteDelimiterSourceAccountDatabaseActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении удаления записи материнской базы разделителей
 */
export type DeleteDelimiterSourceAccountDatabaseActionSuccessPayload = ReducerActionSuccessPayload & {

    /**
     * Id информационной базы
     */
    accountDatabaseId: string;

    /**
     * Код конфигурации базы на разделителях
     */
    dbTemplateDelimiterCode: string;
};

/**
 * Все возможные Action при удалении записи материнской базы разделителей
 */
export type DeleteDelimiterSourceAccountDatabaseActionAnyPayload =
    | DeleteDelimiterSourceAccountDatabaseActionStartPayload
    | DeleteDelimiterSourceAccountDatabaseActionFailedPayload
    | DeleteDelimiterSourceAccountDatabaseActionSuccessPayload;