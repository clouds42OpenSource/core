import { reducerActionState } from 'app/common/functions/reducerActionState';
import { DelimiterSourceAccountDatabasesProcessId } from 'app/modules/delimiterSourceAccountDatabases';
import { DelimiterSourceAccountDatabasesActions } from 'app/modules/delimiterSourceAccountDatabases/store/actions';
import { DelimiterSourceAccountDatabasesReducerState } from 'app/modules/delimiterSourceAccountDatabases/store/reducers';
import {
    UpdateDelimiterSourceAccountDatabaseActionAnyPayload,
    UpdateDelimiterSourceAccountDatabaseActionSuccessPayload
} from 'app/modules/delimiterSourceAccountDatabases/store/reducers/updateDelimiterSourceAccountDatabaseReducer/payloads';
import { reducerStateProcessFail, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для редактирования записи в DelimiterSourceAccountDatabase
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const updateDelimiterSourceAccountDatabaseReducer: TPartialReducerObject<DelimiterSourceAccountDatabasesReducerState, UpdateDelimiterSourceAccountDatabaseActionAnyPayload> =
    (state, action) => {
        const processId = DelimiterSourceAccountDatabasesProcessId.UpdateDelimiterSourceAccountDatabase;
        const actions = DelimiterSourceAccountDatabasesActions.UpdateDelimiterSourceAccountDatabase;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as UpdateDelimiterSourceAccountDatabaseActionSuccessPayload;

                const foundIndex = state.delimiterSourceAccountDatabases.records.findIndex(item => item.id === payload.updatedItem.id);

                if (foundIndex < 0) {
                    return reducerStateProcessFail(state, processId, new Error(`Запись с ID: ${ payload.updatedItem.id } не найдена.`));
                }

                const newRecords = [...state.delimiterSourceAccountDatabases.records];

                newRecords[foundIndex] = {
                    ...payload.updatedItem
                };

                return reducerStateProcessSuccess(state, processId, {
                    delimiterSourceAccountDatabases: {
                        ...state.delimiterSourceAccountDatabases,
                        records: newRecords
                    }
                });
            }
        });
    };