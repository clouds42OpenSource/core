import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для удаления записи материнской базы разделителей
 */
export type DeleteDelimiterSourceAccountDatabaseThunkParams = IForceThunkParam & {
    /**
     * Id информационной базы
     */
    accountDatabaseId: string;

    /**
     * Код конфигурации базы на разделителях
     */
    dbTemplateDelimiterCode: string;
};