import { DelimiterSourceAccountDatabaseItemDataModel } from 'app/web/InterlayerApiProxy/DelimiterSourceAccountDatabasesApiProxy/receiveDelimiterSourceAccountDatabases';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения обновления записи материнской базы разделителей
 */
export type UpdateDelimiterSourceAccountDatabaseActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении обновления записи материнской базы разделителей
 */
export type UpdateDelimiterSourceAccountDatabaseActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении обновления записи материнской базы разделителей
 */
export type UpdateDelimiterSourceAccountDatabaseActionSuccessPayload = ReducerActionSuccessPayload & {

    /**
     * Обновлённые поля записи материнской базы разделителей
     */
    updatedItem: DelimiterSourceAccountDatabaseItemDataModel;
};

/**
 * Все возможные Action при обновлении записи материнской базы разделителей
 */
export type UpdateDelimiterSourceAccountDatabaseActionAnyPayload =
    | UpdateDelimiterSourceAccountDatabaseActionStartPayload
    | UpdateDelimiterSourceAccountDatabaseActionFailedPayload
    | UpdateDelimiterSourceAccountDatabaseActionSuccessPayload;