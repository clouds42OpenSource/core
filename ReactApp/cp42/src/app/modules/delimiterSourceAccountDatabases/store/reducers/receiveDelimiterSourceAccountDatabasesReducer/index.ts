import { reducerActionState } from 'app/common/functions/reducerActionState';
import { DelimiterSourceAccountDatabasesProcessId } from 'app/modules/delimiterSourceAccountDatabases';
import { DelimiterSourceAccountDatabasesActions } from 'app/modules/delimiterSourceAccountDatabases/store/actions';
import { DelimiterSourceAccountDatabasesReducerState } from 'app/modules/delimiterSourceAccountDatabases/store/reducers';
import {
    ReceiveDelimiterSourceAccountDatabasesActionAnyPayload,
    ReceiveDelimiterSourceAccountDatabasesActionSuccessPayload
} from 'app/modules/delimiterSourceAccountDatabases/store/reducers/receiveDelimiterSourceAccountDatabasesReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения записи в DelimiterSourceAccountDatabase
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const receiveDelimiterSourceAccountDatabasesReducer: TPartialReducerObject<DelimiterSourceAccountDatabasesReducerState, ReceiveDelimiterSourceAccountDatabasesActionAnyPayload> =
    (state, action) => {
        const processId = DelimiterSourceAccountDatabasesProcessId.ReceiveDelimiterSourceAccountDatabases;
        const actions = DelimiterSourceAccountDatabasesActions.ReceiveDelimiterSourceAccountDatabases;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as ReceiveDelimiterSourceAccountDatabasesActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    delimiterSourceAccountDatabases: { ...payload.delimiterSourceAccountDatabases },
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasDelimiterSourceAccountDatabasesReceived: true
                    }
                });
            }
        });
    };