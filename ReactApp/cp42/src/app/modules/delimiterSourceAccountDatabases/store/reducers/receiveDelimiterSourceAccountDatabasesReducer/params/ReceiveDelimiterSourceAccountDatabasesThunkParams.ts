import { ReceiveDelimiterSourceAccountDatabasesParams } from 'app/web/InterlayerApiProxy/DelimiterSourceAccountDatabasesApiProxy/receiveDelimiterSourceAccountDatabases/input-params';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для получения материнских баз разделителей
 */
export type ReceiveDelimiterSourceAccountDatabasesThunkParams = IForceThunkParam & ReceiveDelimiterSourceAccountDatabasesParams;