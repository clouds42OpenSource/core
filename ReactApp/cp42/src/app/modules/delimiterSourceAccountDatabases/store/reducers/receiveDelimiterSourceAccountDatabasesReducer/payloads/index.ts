import { DelimiterSourceAccountDatabaseResultDataModel } from 'app/web/InterlayerApiProxy/DelimiterSourceAccountDatabasesApiProxy/receiveDelimiterSourceAccountDatabases';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения материнских баз разделителей
 */
export type ReceiveDelimiterSourceAccountDatabasesActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения материнских баз разделителей
 */
export type ReceiveDelimiterSourceAccountDatabasesActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения материнских баз разделителей
 */
export type ReceiveDelimiterSourceAccountDatabasesActionSuccessPayload = ReducerActionSuccessPayload & {

    /**
     * Материнские базы разделителей
     */
    delimiterSourceAccountDatabases: DelimiterSourceAccountDatabaseResultDataModel
};

/**
 * Все возможные Action при получении материнских баз разделителей
 */
export type ReceiveDelimiterSourceAccountDatabasesActionAnyPayload =
    | ReceiveDelimiterSourceAccountDatabasesActionStartPayload
    | ReceiveDelimiterSourceAccountDatabasesActionFailedPayload
    | ReceiveDelimiterSourceAccountDatabasesActionSuccessPayload;