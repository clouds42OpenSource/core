import { DelimiterSourceAccountDatabaseItemDataModel } from 'app/web/InterlayerApiProxy/DelimiterSourceAccountDatabasesApiProxy/receiveDelimiterSourceAccountDatabases';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения добавления новой записи материнской базы разделителей
 */
export type InsertDelimiterSourceAccountDatabaseActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении добавления новой записи материнской базы разделителей
 */
export type InsertDelimiterSourceAccountDatabaseActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении добавления новой записи материнской базы разделителей
 */
export type InsertDelimiterSourceAccountDatabaseActionSuccessPayload = ReducerActionSuccessPayload & {

    /**
     * Новая запись материнской базы разделителей
     */
    newItem: DelimiterSourceAccountDatabaseItemDataModel;
};

/**
 * Все возможные Action при добавлении новой записи материнской базы разделителей
 */
export type InsertDelimiterSourceAccountDatabaseActionAnyPayload =
    | InsertDelimiterSourceAccountDatabaseActionStartPayload
    | InsertDelimiterSourceAccountDatabaseActionFailedPayload
    | InsertDelimiterSourceAccountDatabaseActionSuccessPayload;