import { DelimiterSourceAccountDatabaseItemDataModel } from 'app/web/InterlayerApiProxy/DelimiterSourceAccountDatabasesApiProxy/receiveDelimiterSourceAccountDatabases';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для добавления новой записи материнской базы разделителей
 */
export type InsertDelimiterSourceAccountDatabaseThunkParams = IForceThunkParam & DelimiterSourceAccountDatabaseItemDataModel;