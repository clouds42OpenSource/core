import { DelimiterSourceAccountDatabasesProcessId } from 'app/modules/delimiterSourceAccountDatabases';
import { DelimiterSourceAccountDatabaseResultDataModel } from 'app/web/InterlayerApiProxy/DelimiterSourceAccountDatabasesApiProxy/receiveDelimiterSourceAccountDatabases';
import { IReducerState } from 'core/redux/interfaces';

/**
 * Состояние редюсера для работы с материнскими базами разделителей
 */
export type DelimiterSourceAccountDatabasesReducerState = IReducerState<DelimiterSourceAccountDatabasesProcessId> & {

    /**
     * Материнские базы разделителей
     */
    delimiterSourceAccountDatabases: DelimiterSourceAccountDatabaseResultDataModel
    hasSuccessFor: {
        /**
         * Если true, то Материнские базы разделителей получены
         */
        hasDelimiterSourceAccountDatabasesReceived: boolean,
    };
};