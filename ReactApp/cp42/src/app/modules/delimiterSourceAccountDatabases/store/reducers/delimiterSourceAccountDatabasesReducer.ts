import { getInitialMetadata } from 'app/common/functions';
import { DelimiterSourceAccountDatabasesConsts } from 'app/modules/delimiterSourceAccountDatabases/constants';
import { DelimiterSourceAccountDatabasesReducerState } from 'app/modules/delimiterSourceAccountDatabases/store/reducers';
import { deleteDelimiterSourceAccountDatabaseReducer } from 'app/modules/delimiterSourceAccountDatabases/store/reducers/deleteDelimiterSourceAccountDatabaseReducer';
import { insertDelimiterSourceAccountDatabaseReducer } from 'app/modules/delimiterSourceAccountDatabases/store/reducers/insertDelimiterSourceAccountDatabaseReducer';
import { receiveDelimiterSourceAccountDatabasesReducer } from 'app/modules/delimiterSourceAccountDatabases/store/reducers/receiveDelimiterSourceAccountDatabasesReducer';
import { updateDelimiterSourceAccountDatabaseReducer } from 'app/modules/delimiterSourceAccountDatabases/store/reducers/updateDelimiterSourceAccountDatabaseReducer';
import { createReducer, initReducerState } from 'core/redux/functions';

/**
 * Начальное состояние редюсера ReceiveDelimiterSourceAccountDatabases
 */
const initialState = initReducerState<DelimiterSourceAccountDatabasesReducerState>(
    DelimiterSourceAccountDatabasesConsts.reducerName,
    {
        delimiterSourceAccountDatabases: {
            records: [],
            metadata: getInitialMetadata()
        },
        hasSuccessFor: {
            hasDelimiterSourceAccountDatabasesReceived: false
        }
    }
);

/**
 * Объединённые части редюсера для всего состояния ReceiveDelimiterSourceAccountDatabases
 */
const partialReducers = [
    receiveDelimiterSourceAccountDatabasesReducer,
    updateDelimiterSourceAccountDatabaseReducer,
    insertDelimiterSourceAccountDatabaseReducer,
    deleteDelimiterSourceAccountDatabaseReducer
];

/**
 * Редюсер ReceiveDelimiterSourceAccountDatabases
 */
export const delimiterSourceAccountDatabasesReducer = createReducer(initialState, partialReducers);