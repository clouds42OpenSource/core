import { DelimiterSourceAccountDatabaseItemDataModel } from 'app/web/InterlayerApiProxy/DelimiterSourceAccountDatabasesApiProxy/receiveDelimiterSourceAccountDatabases';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для обновления записи материнской базы разделителей
 */
export type UpdateDelimiterSourceAccountDatabaseThunkParams = DelimiterSourceAccountDatabaseItemDataModel & IForceThunkParam;