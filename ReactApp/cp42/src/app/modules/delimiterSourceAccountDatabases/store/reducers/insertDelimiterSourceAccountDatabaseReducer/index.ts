import { reducerActionState } from 'app/common/functions/reducerActionState';
import { DelimiterSourceAccountDatabasesProcessId } from 'app/modules/delimiterSourceAccountDatabases';
import { DelimiterSourceAccountDatabasesActions } from 'app/modules/delimiterSourceAccountDatabases/store/actions';
import { DelimiterSourceAccountDatabasesReducerState } from 'app/modules/delimiterSourceAccountDatabases/store/reducers';
import {
    InsertDelimiterSourceAccountDatabaseActionAnyPayload,
    InsertDelimiterSourceAccountDatabaseActionSuccessPayload
} from 'app/modules/delimiterSourceAccountDatabases/store/reducers/insertDelimiterSourceAccountDatabaseReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для добавления записи в DelimiterSourceAccountDatabase
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const insertDelimiterSourceAccountDatabaseReducer: TPartialReducerObject<DelimiterSourceAccountDatabasesReducerState, InsertDelimiterSourceAccountDatabaseActionAnyPayload> =
    (state, action) => {
        const processId = DelimiterSourceAccountDatabasesProcessId.InsertDelimiterSourceAccountDatabase;
        const actions = DelimiterSourceAccountDatabasesActions.InsertDelimiterSourceAccountDatabase;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as InsertDelimiterSourceAccountDatabaseActionSuccessPayload;

                const records = [
                    payload.newItem,
                    ...state.delimiterSourceAccountDatabases.records
                ];

                return reducerStateProcessSuccess(state, processId, {
                    delimiterSourceAccountDatabases: {
                        ...state.delimiterSourceAccountDatabases,
                        records,
                        metadata: {
                            ...state.delimiterSourceAccountDatabases.metadata,
                            pageSize: state.delimiterSourceAccountDatabases.metadata.pageSize + 1,
                            totalItemCount: state.delimiterSourceAccountDatabases.metadata.totalItemCount + 1
                        }
                    }
                });
            }
        });
    };