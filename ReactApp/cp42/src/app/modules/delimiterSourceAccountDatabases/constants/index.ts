/**
 * Константы для модуля DelimiterSourceAccountDatabases
 */
export const DelimiterSourceAccountDatabasesConsts = {
    /**
     * Название редюсера для модуля DelimiterSourceAccountDatabases
     */
    reducerName: 'DELIMITER_SOURCE_ACCOUNT_DATABASES'
};