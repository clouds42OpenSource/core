import { TData, TDataReturn } from 'app/api/types';
import { TServicesListResponse } from 'app/api/endpoints/partners/response';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { PARTNER_SLICE_NAMES } from 'app/modules/partners/constants';

const initialState: TDataReturn<TServicesListResponse> = {
    data: null,
    error: null,
    isLoading: false
};

export const getServicesListSlice = createSlice({
    name: PARTNER_SLICE_NAMES.getServicesList,
    initialState,
    reducers: {
        loading(state) {
            state.isLoading = true;
        },
        empty(state) {
            state.isLoading = false;
            state.data = null;
            state.error = null;
        },
        success(state, action: PayloadAction<TData<TServicesListResponse>>) {
            state.data = action.payload;
        },
        error(state, action: PayloadAction<string>) {
            state.error = action.payload;
        }
    }
});

export default getServicesListSlice.reducer;