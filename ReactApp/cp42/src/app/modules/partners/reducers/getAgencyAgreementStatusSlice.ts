import { TData, TDataReturn } from 'app/api/types';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { PARTNER_SLICE_NAMES } from 'app/modules/partners/constants';

const initialState: TDataReturn<boolean> = {
    data: null,
    error: null,
    isLoading: false
};

export const getAgencyAgreementStatusSlice = createSlice({
    name: PARTNER_SLICE_NAMES.getAgencyAgreementStatusSlice,
    initialState,
    reducers: {
        loading(state) {
            state.isLoading = true;
            state.error = null;
            state.data = null;
        },
        success(state, action: PayloadAction<TData<boolean>>) {
            state.isLoading = false;
            state.error = null;
            state.data = action.payload;
        },
        error(state, action: PayloadAction<string>) {
            state.isLoading = false;
            state.error = action.payload;
            state.data = null;
        }
    }
});

export default getAgencyAgreementStatusSlice.reducer;