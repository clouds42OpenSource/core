import { TData, TDataReturn } from 'app/api/types';
import { TSummaryDataResponse } from 'app/api/endpoints/partners/response/TSummaryData';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { PARTNER_SLICE_NAMES } from 'app/modules/partners/constants';

const initialState: TDataReturn<TSummaryDataResponse> = {
    data: null,
    error: null,
    isLoading: false
};

export const getSummaryDataSlice = createSlice({
    name: PARTNER_SLICE_NAMES.getSummaryData,
    initialState,
    reducers: {
        loading(state) {
            state.isLoading = true;
            state.error = null;
        },
        success(state, action: PayloadAction<TData<TSummaryDataResponse>>) {
            state.isLoading = false;
            state.error = null;
            state.data = action.payload;
        },
        setMonthlyCharge(state, acton: PayloadAction<number>) {
            if (state.data && state.data.rawData) {
                state.data.rawData.monthlyCharge = acton.payload;
            }
        },
        error(state, action: PayloadAction<string>) {
            state.isLoading = false;
            state.error = action.payload;
            state.data = null;
        }
    }
});

export default getSummaryDataSlice.reducer;