import { combineReducers } from 'redux';

import summaryDataReducer from './getSummaryDataSlice';
import clientListReducer from './getClientListSlice';
import servicesListReducer from './getServicesListSlice';
import agentRequisitesListReducer from './getAgentRequisitesListSlice';
import agentPaymentListReducer from './getAgentPaymentListSlice';
import agentCashOutRequestListReducer from './getAgentCashOutRequestListSlice';
import agencyAgreementStatusReducer from './getAgencyAgreementStatusSlice';
import recalculationServiceCostListReducer from './getRecalculationServiceCostListSlice';

export const PartnersReducer = combineReducers({
    summaryDataReducer,
    clientListReducer,
    servicesListReducer,
    agentRequisitesListReducer,
    agentPaymentListReducer,
    agentCashOutRequestListReducer,
    agencyAgreementStatusReducer,
    recalculationServiceCostListReducer
});