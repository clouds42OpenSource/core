import { TData, TDataReturn } from 'app/api/types';
import { TAgentRequisitesListResponse } from 'app/api/endpoints/partners/response';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { PARTNER_SLICE_NAMES } from 'app/modules/partners/constants';

const initialState: TDataReturn<TAgentRequisitesListResponse> = {
    data: null,
    error: null,
    isLoading: false
};

export const getAgentRequisitesListSlice = createSlice({
    name: PARTNER_SLICE_NAMES.getAgentRequisitesList,
    initialState,
    reducers: {
        loading(state) {
            state.isLoading = true;
        },
        empty(state) {
            state.isLoading = false;
            state.data = null;
            state.error = null;
        },
        success(state, action: PayloadAction<TData<TAgentRequisitesListResponse>>) {
            state.data = action.payload;
        },
        error(state, action: PayloadAction<string>) {
            state.error = action.payload;
        }
    }
});

export default getAgentRequisitesListSlice.reducer;