export const PARTNER_SLICE_NAMES = {
    getSummaryData: 'getSummaryData',
    getClientList: 'getClientList',
    getServicesList: 'getServicesList',
    getAgentRequisitesList: 'getAgentRequisitesList',
    getAgentPaymentList: 'getAgentPaymentList',
    getAgentCashOutRequestList: 'getAgentCashOutRequestList',
    getAgencyAgreementStatusSlice: 'getAgencyAgreementStatusSlice',
    getRecalculationServiceCostList: 'getRecalculationServiceCostList'
} as const;