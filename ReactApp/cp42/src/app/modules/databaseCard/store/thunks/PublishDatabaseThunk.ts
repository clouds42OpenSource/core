import { DatabaseCardActions } from 'app/modules/databaseCard/store/actions';
import { PublishDatabaseActionFailedPayload, PublishDatabaseActionStartPayload, PublishDatabaseActionSuccessPayload } from 'app/modules/databaseCard/store/reducers/publishDatabaseReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { PublishDatabaseParams } from 'app/web/InterlayerApiProxy/DatabaseApiProxy/publishDatabase';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = PublishDatabaseActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = PublishDatabaseActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = PublishDatabaseActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = PublishDatabaseParams;

const { PublishDatabase: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = DatabaseCardActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для запроса списка записей логирования запуска баз и открытия RDP
 */
export class PublishDatabaseThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new PublishDatabaseThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        return {
            condition: args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().DatabaseCardState
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        const requestArgs = args.inputParams!;
        const databaseApi = InterlayerApiProxy.getDatabaseApi();

        try {
            await databaseApi.publishDatabase(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, {
                databaseId: requestArgs.databaseId
            });

            args.success({
                isDatabasePublishingInfoVisible: true,
                isPublishDatabaseCommandVisible: false
            });
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}