import { DatabaseCardActions } from 'app/modules/databaseCard/store/actions';
import { ReceiveDatabaseCardThunkParams } from 'app/modules/databaseCard/store/reducers/receiveDatabaseCardReducer/params';
import { ReceiveDatabaseCardActionFailedPayload, ReceiveDatabaseCardActionStartPayload, ReceiveDatabaseCardActionSuccessPayload } from 'app/modules/databaseCard/store/reducers/receiveDatabaseCardReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = ReceiveDatabaseCardActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = ReceiveDatabaseCardActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = ReceiveDatabaseCardActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = ReceiveDatabaseCardThunkParams;

const { ReceiveDatabaseCard: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = DatabaseCardActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для запроса списка записей логирования запуска баз и открытия RDP
 */
export class ReceiveDatabaseCardThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new ReceiveDatabaseCardThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        const databaseCardState = args.getStore().DatabaseCardState;
        return {
            condition: !databaseCardState.hasSuccessFor.hasDatabaseCardReceived || args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().DatabaseCardState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        const requestArgs = args.inputParams!;
        const databaseCardApi = InterlayerApiProxy.getDatabaseCardApi();

        try {
            const databaseCardResponse = await databaseCardApi.receiveDatabaseCard(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, {
                accountDatabaseNumber: requestArgs.accountDatabaseNumber
            });
            if (databaseCardResponse.data && databaseCardResponse.success) {
                args.success({
                    backups: databaseCardResponse.data.backups,
                    launchPublishedDbUrl: databaseCardResponse.data.launchPublishedDbUrl,
                    commandVisibility: databaseCardResponse.data.commandVisibility,
                    commonDetails: databaseCardResponse.data.database,
                    tabVisibility: databaseCardResponse.data.tabVisibility,
                    databaseCardReadModeFieldAccessInfo: databaseCardResponse.data.fieldsAccessReadModeInfo,
                    tehSupportInfo: databaseCardResponse.data.tehSupportInfo,
                    accountCardEditModeDictionaries: databaseCardResponse.data.accountCardEditModeDictionaries,
                    canEditDatabase: databaseCardResponse.data.canEditDatabase
                });
            } else {
                args.failed({
                    error: new Error(databaseCardResponse.message ?? 'Непредвиденная ошибка получения базы')
                });
            }
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}