import { DatabaseCardActions } from 'app/modules/databaseCard/store/actions';
import { EditDatabaseDataThunkParams } from 'app/modules/databaseCard/store/reducers/editDatabaseDataReducer/params';
import { EditDatabaseDataActionFailedPayload, EditDatabaseDataActionStartPayload, EditDatabaseDataActionSuccessPayload } from 'app/modules/databaseCard/store/reducers/editDatabaseDataReducer/payloads';
import { ReceiveDatabaseListThunk } from 'app/modules/databaseList/store/thunks/ReceiveDatabaseListThunk';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = EditDatabaseDataActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = EditDatabaseDataActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = EditDatabaseDataActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = EditDatabaseDataThunkParams;

const { EditDatabaseData: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = DatabaseCardActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для запроса списка записей логирования запуска баз и открытия RDP
 */
export class EditDatabaseDataThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new EditDatabaseDataThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        return {
            condition: args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().DatabaseCardState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        const requestArgs = args.inputParams!;
        const databaseApi = InterlayerApiProxy.getDatabaseApi();

        try {
            await databaseApi.editDatabaseData(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, {
                ...requestArgs
            });
            const { databaseList } = args.getStore().DatabaseListState;
            args.thunkDispatch(new ReceiveDatabaseListThunk(), {
                force: true,
                pageNumber: databaseList.metadata.pageNumber,
                filter: { searchLine: databaseList.searchQuery ? databaseList.searchQuery : '' },
                orderBy: databaseList.orderBy

            });

            args.success({
                changedFields: requestArgs
            });
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}