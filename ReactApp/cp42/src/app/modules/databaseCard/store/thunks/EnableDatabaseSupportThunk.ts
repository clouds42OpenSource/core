import { DatabaseCardActions } from 'app/modules/databaseCard/store/actions';
import { EnableDatabaseSupportThunkParams } from 'app/modules/databaseCard/store/reducers/enableDatabaseSupportReducer/params';
import { EnableDatabaseSupportActionFailedPayload, EnableDatabaseSupportActionStartPayload, EnableDatabaseSupportActionSuccessPayload } from 'app/modules/databaseCard/store/reducers/enableDatabaseSupportReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = EnableDatabaseSupportActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = EnableDatabaseSupportActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = EnableDatabaseSupportActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = EnableDatabaseSupportThunkParams;

const { EnableDatabaseSupport: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = DatabaseCardActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для запроса списка записей логирования запуска баз и открытия RDP
 */
export class EnableDatabaseSupportThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new EnableDatabaseSupportThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        return {
            condition: args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().DatabaseCardState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        const requestArgs = args.inputParams!;
        const databaseApi = InterlayerApiProxy.getDatabaseApi();

        try {
            const result = await databaseApi.enableDatabaseSupport(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, {
                databaseId: requestArgs.databaseId,
                login: requestArgs.login,
                password: requestArgs.password
            });

            if (!result.success) {
                args.failed({
                    error: new Error(result.message ?? 'Произошла непредвиденная ошибка')
                });
            } else {
                args.success({
                    result: result.data!
                });
            }
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}