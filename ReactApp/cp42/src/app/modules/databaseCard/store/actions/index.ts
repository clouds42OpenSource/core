import { DatabaseCardConsts } from 'app/modules/databaseCard/constants';
import { DatabaseCardProcessId } from 'app/modules/databaseCard/DatabaseCardProcessId';
import { createReducerActions } from 'core/redux/functions';

/**
 * Все доступные actions дле редюсера Logging
 */
export const DatabaseCardActions = {
    /**
     * Набор action на получения карточки информационной базы
     */
    ReceiveDatabaseCard: createReducerActions(DatabaseCardConsts.reducerName, DatabaseCardProcessId.ReceiveDatabaseCard),
    /**
     * Набор action на отключение информационной базы от тех поддержки
     */
    DisableDatabaseSupport: createReducerActions(DatabaseCardConsts.reducerName, DatabaseCardProcessId.DisableDatabaseSupport),
    /**
     * Набор action на подключение информационной базы на тех поддержку
     */
    EnableDatabaseSupport: createReducerActions(DatabaseCardConsts.reducerName, DatabaseCardProcessId.EnableDatabaseSupport),
    /**
     * Набор action на редактирование данных базы
     */
    EditDatabaseData: createReducerActions(DatabaseCardConsts.reducerName, DatabaseCardProcessId.EditDatabaseData),
    /**
     * Набор action на публикацию базы
     */
    PublishDatabase: createReducerActions(DatabaseCardConsts.reducerName, DatabaseCardProcessId.PublishDatabase),
    /**
     * Набор action на снятия публикации с базы
     */
    UnpublishDatabase: createReducerActions(DatabaseCardConsts.reducerName, DatabaseCardProcessId.UnpublishDatabase)
};