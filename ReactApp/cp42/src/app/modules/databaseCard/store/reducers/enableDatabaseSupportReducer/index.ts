import { reducerActionState } from 'app/common/functions/reducerActionState';
import { DatabaseCardProcessId } from 'app/modules/databaseCard';
import { DatabaseCardActions } from 'app/modules/databaseCard/store/actions';
import { DatabaseCardReducerState } from 'app/modules/databaseCard/store/reducers';
import { EnableDatabaseSupportActionAnyPayload, EnableDatabaseSupportActionSuccessPayload } from 'app/modules/databaseCard/store/reducers/enableDatabaseSupportReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для подключения информационной базы на техподдержки
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const enableDatabaseSupportReducer: TPartialReducerObject<DatabaseCardReducerState, EnableDatabaseSupportActionAnyPayload> =
    (state, action) => {
        const processId = DatabaseCardProcessId.EnableDatabaseSupport;
        const actions = DatabaseCardActions.EnableDatabaseSupport;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as EnableDatabaseSupportActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    tehSupportInfo: {
                        ...state.tehSupportInfo,
                        isInConnectingState: payload.result.isInConnectingState,
                        supportStateDescription: payload.result.supportStateDescription,
                        supportState: payload.result.supportState,
                        lastHistoryDate: payload.result.lastHistoryDate
                    }
                });
            }
        });
    };