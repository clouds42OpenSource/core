import { EditDatabaseDataParams } from 'app/web/InterlayerApiProxy/DatabaseApiProxy/editDatabaseData';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте получения карточки информациооной базы
 */
export type EditDatabaseDataActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при ошибки получения карточки информациооной базы
 */
export type EditDatabaseDataActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном получении карточки информациооной базы
 */
export type EditDatabaseDataActionSuccessPayload = ReducerActionSuccessPayload & {
    changedFields: EditDatabaseDataParams;
};

/**
 * Все возможные Action при получении карточки информациооной базы
 */
export type EditDatabaseDataActionAnyPayload =
    | EditDatabaseDataActionStartPayload
    | EditDatabaseDataActionSuccessPayload
    | EditDatabaseDataActionFailedPayload;