import { partialFillOf } from 'app/common/functions/fillObjectWithUndefined';
import { DatabaseCardConsts } from 'app/modules/databaseCard/constants';
import { DatabaseCardReducerState } from 'app/modules/databaseCard/store/reducers/DatabaseCardReducerState';
import { disableDatabaseSupportReducer } from 'app/modules/databaseCard/store/reducers/disableDatabaseSupportReducer';
import { editDatabaseDataReducer } from 'app/modules/databaseCard/store/reducers/editDatabaseDataReducer';
import { enableDatabaseSupportReducer } from 'app/modules/databaseCard/store/reducers/enableDatabaseSupportReducer';
import { publishDatabaseReducer } from 'app/modules/databaseCard/store/reducers/publishDatabaseReducer';
import { receiveDatabaseCardReducer } from 'app/modules/databaseCard/store/reducers/receiveDatabaseCardReducer';
import { unpublishDatabaseReducer } from 'app/modules/databaseCard/store/reducers/unpublishDatabaseReducer';
import { DatabaseCardAccountDatabaseInfoDataModel, DatabaseCardEditModeDictionariesDataModel, DatabaseCardReadModeFieldAccessDataModel } from 'app/web/InterlayerApiProxy/DatabaseCardApiProxy/receiveDatabaseCard/data-models';
import { createReducer, initReducerState } from 'core/redux/functions';

/**
 * Начальное состояние редюсера DatabaseCard
 */
const initialState = initReducerState<DatabaseCardReducerState>(
    DatabaseCardConsts.reducerName,
    {
        commonDetails: partialFillOf<DatabaseCardAccountDatabaseInfoDataModel>(),
        launchPublishedDbUrl: '',
        databaseCardReadModeFieldAccessInfo: partialFillOf<DatabaseCardReadModeFieldAccessDataModel>(),
        backups: [],
        tehSupportInfo: null,
        tabVisibility: {
            isBackupTabVisible: false,
            isTehSupportTabVisible: false
        },
        commandVisibility: {
            isCanacelingDatabasePublishingInfoVisible: false,
            isCancelPublishDatabaseCommandVisible: false,
            isDatabasePublishingInfoVisible: false,
            isDeleteDatabaseCommandVisible: false,
            isPublishDatabaseCommandVisible: false
        },
        accountCardEditModeDictionaries: partialFillOf<DatabaseCardEditModeDictionariesDataModel>(),
        canEditDatabase: false,
        hasSuccessFor: {
            hasDatabaseCardReceived: false
        }
    }
);

/**
 * Объединённые части редюсера для всего состояния DatabaseCard
 */
const partialReducers = [
    receiveDatabaseCardReducer,
    disableDatabaseSupportReducer,
    enableDatabaseSupportReducer,
    editDatabaseDataReducer,
    publishDatabaseReducer,
    unpublishDatabaseReducer
];

/**
 * Редюсер DatabaseCard
 */
export const databaseCardReducer = createReducer(initialState, partialReducers);