import { reducerActionState } from 'app/common/functions/reducerActionState';
import { DatabaseCardProcessId } from 'app/modules/databaseCard';
import { DatabaseCardActions } from 'app/modules/databaseCard/store/actions';
import { DatabaseCardReducerState } from 'app/modules/databaseCard/store/reducers';
import { DisableDatabaseSupportActionAnyPayload, DisableDatabaseSupportActionSuccessPayload } from 'app/modules/databaseCard/store/reducers/disableDatabaseSupportReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для отключения информационной базы от техподдержки
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const disableDatabaseSupportReducer: TPartialReducerObject<DatabaseCardReducerState, DisableDatabaseSupportActionAnyPayload> =
    (state, action) => {
        const processId = DatabaseCardProcessId.DisableDatabaseSupport;
        const actions = DatabaseCardActions.DisableDatabaseSupport;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as DisableDatabaseSupportActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    tehSupportInfo: {
                        ...state.tehSupportInfo,
                        isInConnectingState: payload.result.isInConnectingState,
                        supportStateDescription: payload.result.supportStateDescription,
                        supportState: payload.result.supportState,
                        lastHistoryDate: payload.result.lastHistoryDate
                    }
                });
            }
        });
    };