import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель на запрос получения карточки информационной базы
 */
export type EnableDatabaseSupportThunkParams = IForceThunkParam & {
    /**
     * Id информационной базы
     */
    databaseId: string;

    /**
     * Логин администратора в базу
     */
    login: string;

    /**
     * Пароль администратора в базу
     */
    password: string;
};