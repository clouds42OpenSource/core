import { DatabaseCardProcessId } from 'app/modules/databaseCard';
import {
    DatabaseCardAccountDatabaseInfoDataModel,
    DatabaseCardBackupInfoDataModel,
    DatabaseCardCommandVisibilityDataModel,
    DatabaseCardReadModeFieldAccessDataModel,
    DatabaseCardTabVisibilityDataModel,
    DatabaseCardTehSupportInfoDataModel
} from 'app/web/InterlayerApiProxy/DatabaseCardApiProxy/receiveDatabaseCard/data-models';
import { DatabaseCardEditModeDictionariesDataModel } from 'app/web/InterlayerApiProxy/DatabaseCardApiProxy/receiveDatabaseCard/data-models/DatabaseCardEditModeDictionariesDataModel';
import { IReducerState } from 'core/redux/interfaces';

/**
 * Состояние редюсера для работы с карточкой информационной базы
 */
export type DatabaseCardReducerState = IReducerState<DatabaseCardProcessId> & {
    /**
     * Информация о базе
     */
    commonDetails: DatabaseCardAccountDatabaseInfoDataModel;
    /**
     * Url для запуска опубликованной базы
     */
    launchPublishedDbUrl: string;
    /**
     * Информация о видемости полей в режиме просмотра
     */
    databaseCardReadModeFieldAccessInfo: DatabaseCardReadModeFieldAccessDataModel;
    /**
     * Информация о бекапах информационной базы
     */
    backups: DatabaseCardBackupInfoDataModel[];
    /**
     * Информация о техподержке информационной базы
     */
    tehSupportInfo: DatabaseCardTehSupportInfoDataModel | null;
    /**
     * Видимость табов
     */
    tabVisibility: DatabaseCardTabVisibilityDataModel;
    /**
     * Команды с базой
     */
    commandVisibility: DatabaseCardCommandVisibilityDataModel;
    /**
     * Видимости полей и справочники для карточки информационной базы в режиме редактирования
     */
    accountCardEditModeDictionaries: DatabaseCardEditModeDictionariesDataModel;
    /**
     * Если true, то можно редактировать данные в карточке базы
     */
    canEditDatabase: boolean;

    /**
     * содержит ярлыки для фиксации загрузок данных, была ли загрузка или ещё нет
     */
    hasSuccessFor: {
        /**
         * Если true, то карточка информационной базы уже получена
         */
        hasDatabaseCardReceived: boolean,
    };
};