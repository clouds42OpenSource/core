import { reducerActionState } from 'app/common/functions/reducerActionState';
import { DatabaseCardProcessId } from 'app/modules/databaseCard';
import { DatabaseCardActions } from 'app/modules/databaseCard/store/actions';
import { DatabaseCardReducerState } from 'app/modules/databaseCard/store/reducers';
import { ReceiveDatabaseCardActionAnyPayload, ReceiveDatabaseCardActionSuccessPayload } from 'app/modules/databaseCard/store/reducers/receiveDatabaseCardReducer/payloads';
import { DatabaseCardAccountDatabaseInfoDataModel, DatabaseCardCommandVisibilityDataModel } from 'app/web/InterlayerApiProxy/DatabaseCardApiProxy/receiveDatabaseCard';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения карточки информационной базы
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const receiveDatabaseCardReducer: TPartialReducerObject<DatabaseCardReducerState, ReceiveDatabaseCardActionAnyPayload> =
    (state, action) => {
        const processId = DatabaseCardProcessId.ReceiveDatabaseCard;
        const actions = DatabaseCardActions.ReceiveDatabaseCard;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onStartProcessState: () => {
                return reducerStateProcessStart(state, processId, {
                    commonDetails: {} as DatabaseCardAccountDatabaseInfoDataModel,
                    commandVisibility: {} as DatabaseCardCommandVisibilityDataModel,
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasDatabaseCardReceived: false
                    }
                });
            },
            onSuccessProcessState: () => {
                const payload = action.payload as ReceiveDatabaseCardActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    launchPublishedDbUrl: payload.launchPublishedDbUrl,
                    commonDetails: { ...payload.commonDetails },
                    databaseCardReadModeFieldAccessInfo: { ...payload.databaseCardReadModeFieldAccessInfo },
                    backups: [...payload.backups],
                    tehSupportInfo: payload.tehSupportInfo ? { ...payload.tehSupportInfo } : null,
                    tabVisibility: { ...payload.tabVisibility },
                    commandVisibility: { ...payload.commandVisibility },
                    accountCardEditModeDictionaries: { ...payload.accountCardEditModeDictionaries },
                    canEditDatabase: payload.canEditDatabase,
                    hasSuccessFor: {
                        hasDatabaseCardReceived: true
                    }
                });
            }
        });
    };