import {
    DatabaseCardAccountDatabaseInfoDataModel,
    DatabaseCardBackupInfoDataModel,
    DatabaseCardCommandVisibilityDataModel,
    DatabaseCardReadModeFieldAccessDataModel,
    DatabaseCardTabVisibilityDataModel,
    DatabaseCardTehSupportInfoDataModel
} from 'app/web/InterlayerApiProxy/DatabaseCardApiProxy/receiveDatabaseCard/data-models';
import { DatabaseCardEditModeDictionariesDataModel } from 'app/web/InterlayerApiProxy/DatabaseCardApiProxy/receiveDatabaseCard/data-models/DatabaseCardEditModeDictionariesDataModel';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте получения карточки информациооной базы
 */
export type ReceiveDatabaseCardActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при ошибки получения карточки информациооной базы
 */
export type ReceiveDatabaseCardActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном получении карточки информациооной базы
 */
export type ReceiveDatabaseCardActionSuccessPayload = ReducerActionSuccessPayload & {

    /**
     * Информация о базе данных
     */
    commonDetails: DatabaseCardAccountDatabaseInfoDataModel;
    /**
     * Url для запуска опубликованной базы
     */
    launchPublishedDbUrl: string;
    /**
     * Настройки по показу полей для чтения в карточке информационной базы
     */
    databaseCardReadModeFieldAccessInfo: DatabaseCardReadModeFieldAccessDataModel;
    /**
     * Бекапы информационной базы
     */
    backups: DatabaseCardBackupInfoDataModel[];
    /**
     * Информация о подключении информационной базы к ТиИ
     */
    tehSupportInfo: DatabaseCardTehSupportInfoDataModel | null;
    /**
     * Информация о видимости табах в карточке информационной базы
     */
    tabVisibility: DatabaseCardTabVisibilityDataModel;
    /**
     * Информация о видимости комманд с базой
     */
    commandVisibility: DatabaseCardCommandVisibilityDataModel;
    /**
     * Видимость полей и справочники для карточки информационной базы в режиме редактирования
     */
    accountCardEditModeDictionaries: DatabaseCardEditModeDictionariesDataModel;

    /**
     * Если true, то можно редактировать данные в карточке базы
     */
    canEditDatabase: boolean;
};

/**
 * Все возможные Action при получении карточки информациооной базы
 */
export type ReceiveDatabaseCardActionAnyPayload =
    | ReceiveDatabaseCardActionStartPayload
    | ReceiveDatabaseCardActionSuccessPayload
    | ReceiveDatabaseCardActionFailedPayload;