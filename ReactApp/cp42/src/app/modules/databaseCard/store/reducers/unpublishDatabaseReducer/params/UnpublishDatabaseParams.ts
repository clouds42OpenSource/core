import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель на запрос снфтие публикации информационной базы
 */
export type UnpublishDatabaseThunkParams = IForceThunkParam & {
    /**
     * Id информационной базы для снятия публикации
     */
    databaseId: string;
};