import { reducerActionState } from 'app/common/functions/reducerActionState';
import { DatabaseCardProcessId } from 'app/modules/databaseCard';
import { DatabaseCardActions } from 'app/modules/databaseCard/store/actions';
import { DatabaseCardReducerState } from 'app/modules/databaseCard/store/reducers';
import { EditDatabaseDataActionAnyPayload, EditDatabaseDataActionSuccessPayload } from 'app/modules/databaseCard/store/reducers/editDatabaseDataReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения карточки информационной базы
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const editDatabaseDataReducer: TPartialReducerObject<DatabaseCardReducerState, EditDatabaseDataActionAnyPayload> =
    (state, action) => {
        const processId = DatabaseCardProcessId.EditDatabaseData;
        const actions = DatabaseCardActions.EditDatabaseData;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as EditDatabaseDataActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    commonDetails: {
                        ...state.commonDetails,
                        v82Name: payload.changedFields.v82Name,
                        fileStorage: payload.changedFields.fileStorageId,
                        caption: payload.changedFields.databaseCaption,
                        databaseState: payload.changedFields.databaseState,
                        dbTemplate: payload.changedFields.databaseTemplateId,
                        platformType: payload.changedFields.platformType,
                        distributionType: payload.changedFields.distributionType,
                        usedWebServices: payload.changedFields.usedWebServices,
                    }
                });
            }
        });
    };