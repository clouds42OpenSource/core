import { DisableSupportDatabaseDataModel } from 'app/web/InterlayerApiProxy/DatabaseApiProxy/disableDatabaseSupport/data-models/DisableSupportDatabaseDataModel';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте отключения информационной базы от техподдержки
 */
export type DisableDatabaseSupportActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при ошибки отключения информационной базы от техподдержки
 */
export type DisableDatabaseSupportActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном отключении информационной базы от техподдержки
 */
export type DisableDatabaseSupportActionSuccessPayload = ReducerActionSuccessPayload & {
    result: DisableSupportDatabaseDataModel;
};

/**
 * Все возможные Action при отключении информационной базы от техподдержки
 */
export type DisableDatabaseSupportActionAnyPayload =
    | DisableDatabaseSupportActionStartPayload
    | DisableDatabaseSupportActionSuccessPayload
    | DisableDatabaseSupportActionFailedPayload;