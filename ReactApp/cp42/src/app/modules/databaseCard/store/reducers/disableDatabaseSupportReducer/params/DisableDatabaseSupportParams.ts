import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель на запрос получения карточки информационной базы
 */
export type DisableDatabaseSupportThunkParams = IForceThunkParam & {
    /**
     * Id информационной базы
     */
    databaseId: string;
};