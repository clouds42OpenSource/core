import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте снятия публикации информационной базы
 */
export type UnpublishDatabaseActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при ошибки снятия публикации информационной базы
 */
export type UnpublishDatabaseActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном снятия публикации информационной базы
 */
export type UnpublishDatabaseActionSuccessPayload = ReducerActionSuccessPayload & {
    /**
     * Флаг видимости о том что идёт отмена публикации базы
     */
    isCanacelingDatabasePublishingInfoVisible: boolean;
    /**
     * Флаг видимости кнопки для отмены публикации базы
     */
    isCancelPublishDatabaseCommandVisible: boolean;
};

/**
 * Все возможные Action при снятия публикации информационной базы
 */
export type UnpublishDatabaseActionAnyPayload =
    | UnpublishDatabaseActionStartPayload
    | UnpublishDatabaseActionSuccessPayload
    | UnpublishDatabaseActionFailedPayload;