import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте публикации информационной базы
 */
export type PublishDatabaseActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при ошибки публикации информационной базы
 */
export type PublishDatabaseActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешной публикации информационной базы
 */
export type PublishDatabaseActionSuccessPayload = ReducerActionSuccessPayload & {
    /**
     * Флаг видимости о том что идёт публикации базы
     */
    isDatabasePublishingInfoVisible: boolean;
    /**
     * Флаг видимости кнопки для публикации базы
     */
    isPublishDatabaseCommandVisible: boolean;
};

/**
 * Все возможные Action при публикации информационной базы
 */
export type PublishDatabaseActionAnyPayload =
    | PublishDatabaseActionStartPayload
    | PublishDatabaseActionSuccessPayload
    | PublishDatabaseActionFailedPayload;