import { EditDatabaseDataParams } from 'app/web/InterlayerApiProxy/DatabaseApiProxy/editDatabaseData';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель на запрос обновления данных базы
 */
export type EditDatabaseDataThunkParams = IForceThunkParam & EditDatabaseDataParams;