import { reducerActionState } from 'app/common/functions/reducerActionState';
import { DatabaseCardProcessId } from 'app/modules/databaseCard';
import { DatabaseCardActions } from 'app/modules/databaseCard/store/actions';
import { DatabaseCardReducerState } from 'app/modules/databaseCard/store/reducers';
import { UnpublishDatabaseActionAnyPayload, UnpublishDatabaseActionSuccessPayload } from 'app/modules/databaseCard/store/reducers/unpublishDatabaseReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для подключения информационной базы на техподдержки
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const unpublishDatabaseReducer: TPartialReducerObject<DatabaseCardReducerState, UnpublishDatabaseActionAnyPayload> =
    (state, action) => {
        const processId = DatabaseCardProcessId.UnpublishDatabase;
        const actions = DatabaseCardActions.UnpublishDatabase;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as UnpublishDatabaseActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    commandVisibility: {
                        ...state.commandVisibility,
                        isCanacelingDatabasePublishingInfoVisible: payload.isCanacelingDatabasePublishingInfoVisible,
                        isCancelPublishDatabaseCommandVisible: payload.isCancelPublishDatabaseCommandVisible
                    }
                });
            }
        });
    };