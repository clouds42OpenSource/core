import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель на запрос получения карточки информационной базы
 */
export type ReceiveDatabaseCardThunkParams = IForceThunkParam & {
    /**
     * Номер информационной базы
     */
    accountDatabaseNumber: string;
};