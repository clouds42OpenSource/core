import { reducerActionState } from 'app/common/functions/reducerActionState';
import { DatabaseCardProcessId } from 'app/modules/databaseCard';
import { DatabaseCardActions } from 'app/modules/databaseCard/store/actions';
import { DatabaseCardReducerState } from 'app/modules/databaseCard/store/reducers';
import { PublishDatabaseActionAnyPayload, PublishDatabaseActionSuccessPayload } from 'app/modules/databaseCard/store/reducers/publishDatabaseReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для подключения информационной базы на техподдержки
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const publishDatabaseReducer: TPartialReducerObject<DatabaseCardReducerState, PublishDatabaseActionAnyPayload> =
    (state, action) => {
        const processId = DatabaseCardProcessId.PublishDatabase;
        const actions = DatabaseCardActions.PublishDatabase;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as PublishDatabaseActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    commandVisibility: {
                        ...state.commandVisibility,
                        isDatabasePublishingInfoVisible: payload.isDatabasePublishingInfoVisible,
                        isPublishDatabaseCommandVisible: payload.isPublishDatabaseCommandVisible
                    }
                });
            }
        });
    };