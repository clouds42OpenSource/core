import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель на запрос публикации информационной базы
 */
export type PublishDatabaseThunkParams = IForceThunkParam & {
    /**
     * Id информационной базы для публикации
     */
    databaseId: string;
};