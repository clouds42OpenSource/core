import { EnableSupportDatabaseDataModel } from 'app/web/InterlayerApiProxy/DatabaseApiProxy/enableDatabaseSupport';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте отключения информационной базы от техподдержки
 */
export type EnableDatabaseSupportActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при ошибки отключения информационной базы от техподдержки
 */
export type EnableDatabaseSupportActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном отключении информационной базы от техподдержки
 */
export type EnableDatabaseSupportActionSuccessPayload = ReducerActionSuccessPayload & {
    result: EnableSupportDatabaseDataModel;
};

/**
 * Все возможные Action при отключении информационной базы от техподдержки
 */
export type EnableDatabaseSupportActionAnyPayload =
    | EnableDatabaseSupportActionStartPayload
    | EnableDatabaseSupportActionSuccessPayload
    | EnableDatabaseSupportActionFailedPayload;