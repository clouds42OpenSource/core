/**
 * Константы для модуля DatabaseCard
 */
export const DatabaseCardConsts = {
    /**
     * Название редюсера для модуля DatabaseCard
     */
    reducerName: 'DATABASE_CARD'
};