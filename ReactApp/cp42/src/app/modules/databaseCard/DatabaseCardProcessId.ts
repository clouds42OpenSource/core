/**
 * Процесы DatabaseCard
 */
export enum DatabaseCardProcessId {
    /**
     * Процесс получить карточку базы
     */
    ReceiveDatabaseCard = 'RECEIVE_DATABASE_CARD',

    /**
     * Процесс на отключение информационной базы от тех поддержки
     */
    DisableDatabaseSupport = 'DISABLE_DATABASE_SUPPORT',

    /**
     * Процесс подключения информационной базы к тех поддержки
     */
    EnableDatabaseSupport = 'ENABLE_DATABASE_SUPPORT',

    /**
     * Процесс редактирования данных базы
     */
    EditDatabaseData = 'EDIT_DATABASE_DATA',

    /**
     * Процесс публикации базы
     */
    PublishDatabase = 'PUBLISH_DATABASE',

    /**
     * Процесс снятия публикации базы
     */
    UnpublishDatabase = 'UNPUBLISH_DATABASE'
}