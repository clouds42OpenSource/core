/**
 * Константы для модуля AgencyAgreement
 */
export const AgencyAgreementConsts = {
    /**
     * Название редюсера для модуля AgencyAgreement
     */
    reducerName: 'AGENCYAGREEMENT'
};