import { AgencyAgreementActions } from 'app/modules/AgencyAgreement/store/actions';
import { ReceiveAgencyAgreementsParamsThunkParams } from 'app/modules/AgencyAgreement/store/reducers/receiveAgencyAgreementsReducer/params';
import { ReceiveAgencyAgreementsActionFailedPayload, ReceiveAgencyAgreementsActionStartPayload, ReceiveAgencyAgreementsActionSuccessPayload } from 'app/modules/AgencyAgreement/store/reducers/receiveAgencyAgreementsReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = ReceiveAgencyAgreementsActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = ReceiveAgencyAgreementsActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = ReceiveAgencyAgreementsActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = ReceiveAgencyAgreementsParamsThunkParams;

const { ReceiveAgencyAgreements: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = AgencyAgreementActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для запроса списка действий облака для логирования
 */
export class ReceiveAgencyAgreementsThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new ReceiveAgencyAgreementsThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        const processFlowState = args.getStore().AgencyAgreementState;
        return {
            condition: !processFlowState.hasSuccessFor.hasAgencyAgreementsReceived || args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().AgencyAgreementState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        try {
            const api = InterlayerApiProxy.getAgencyAgreementApiProxy();
            const response = await api.receiveAgencyAgreements(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, args.inputParams!);

            args.success({
                agencyAgreements: response
            });
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}