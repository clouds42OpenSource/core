import { AgencyAgreementProcessId } from 'app/modules/AgencyAgreement/AgencyAgreementProcessId';
import { AgencyAgreementConsts } from 'app/modules/AgencyAgreement/constants';
import { createReducerActions } from 'core/redux/functions';

/**
 * Все доступные actions дле редюсера AgencyAgreement
 */
export const AgencyAgreementActions = {
    /**
     * Набор action на получение агентских соглашений
     */
    ReceiveAgencyAgreements: createReducerActions(AgencyAgreementConsts.reducerName, AgencyAgreementProcessId.ReceiveAgencyAgreements),

    /**
     * Набор action на добавление агентского соглашения
     */
    AddAgencyAgreement: createReducerActions(AgencyAgreementConsts.reducerName, AgencyAgreementProcessId.AddAgencyAgreement)
};