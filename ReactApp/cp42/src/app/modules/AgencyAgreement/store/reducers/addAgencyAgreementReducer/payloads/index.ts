import { AgencyAgreementItemDataModel } from 'app/web/InterlayerApiProxy/AgencyAgreementApiProxy/receiveAgencyAgreements/data-models/AgencyAgreementItemDataModel';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения добавления агентского соглашения
 */
export type AddAgencyAgreementActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении добавления агентского соглашения
 */
export type AddAgencyAgreementActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении добавления агентского соглашения
 */
export type AddAgencyAgreementActionSuccessPayload = ReducerActionSuccessPayload & {
    /**
     * агентское соглашение для добавления
     */
    agencyAgreementToAdd: AgencyAgreementItemDataModel;
};

/**
 * Все возможные Action при обновлении агентского соглашения
 */
export type AddAgencyAgreementActionAnyPayload =
    | AddAgencyAgreementActionStartPayload
    | AddAgencyAgreementActionFailedPayload
    | AddAgencyAgreementActionSuccessPayload;