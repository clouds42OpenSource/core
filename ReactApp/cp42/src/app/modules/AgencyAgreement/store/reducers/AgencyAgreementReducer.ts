import { getInitialMetadata } from 'app/common/functions';
import { AgencyAgreementConsts } from 'app/modules/AgencyAgreement/constants';
import { addAgencyAgreementReducer } from 'app/modules/AgencyAgreement/store/reducers/addAgencyAgreementReducer';
import { AgencyAgreementReducerState } from 'app/modules/AgencyAgreement/store/reducers/AgencyAgreementReducerState';
import { receiveAgencyAgreementsReducer } from 'app/modules/AgencyAgreement/store/reducers/receiveAgencyAgreementsReducer';
import { createReducer, initReducerState } from 'core/redux/functions';

/**
 * Начальное состояние редюсера AgencyAgreement
 */
const initialState = initReducerState<AgencyAgreementReducerState>(
    AgencyAgreementConsts.reducerName,
    {
        agencyAgreements: {
            records: [],
            metadata: getInitialMetadata()
        },
        hasSuccessFor: {
            hasAgencyAgreementsReceived: false
        }
    }
);

/**
 * Объединённые части редюсера для всего состояния AgencyAgreement
 */
const partialReducers = [
    receiveAgencyAgreementsReducer,
    addAgencyAgreementReducer
];

/**
 * Редюсер AgencyAgreement
 */
export const agencyAgreementReducer = createReducer(initialState, partialReducers);