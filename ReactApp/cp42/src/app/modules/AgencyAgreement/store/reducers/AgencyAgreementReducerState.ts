import { AgencyAgreementProcessId } from 'app/modules/AgencyAgreement/AgencyAgreementProcessId';
import { GetAgencyAgreementResultDataModel } from 'app/web/InterlayerApiProxy/AgencyAgreementApiProxy/receiveAgencyAgreements/data-models/GetAgencyAgreementResultDataModel';
import { IReducerState } from 'core/redux/interfaces';

/**
 * Состояние редюсера для работы с агентскими соглашениями
 */
export type AgencyAgreementReducerState = IReducerState<AgencyAgreementProcessId> & {

    /**
     * Конфигурации 1С
     */
    agencyAgreements: GetAgencyAgreementResultDataModel;
    hasSuccessFor: {
        /**
         * Если true, то агентские соглашения получены
         */
        hasAgencyAgreementsReceived: boolean;
    };
};