import { AddAgencyAgreementParams } from 'app/web/InterlayerApiProxy/AgencyAgreementApiProxy/addAgencyAgreement/input-params';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для добавления агентского соглашения
 */
export type AddAgencyAgreementThunkParams = IForceThunkParam & AddAgencyAgreementParams;