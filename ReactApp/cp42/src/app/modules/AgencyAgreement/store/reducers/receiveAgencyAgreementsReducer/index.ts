import { reducerActionState } from 'app/common/functions/reducerActionState';
import { AgencyAgreementProcessId } from 'app/modules/AgencyAgreement/AgencyAgreementProcessId';
import { AgencyAgreementActions } from 'app/modules/AgencyAgreement/store/actions';
import { AgencyAgreementReducerState } from 'app/modules/AgencyAgreement/store/reducers/AgencyAgreementReducerState';
import { ReceiveAgencyAgreementsActionAnyPayload, ReceiveAgencyAgreementsActionSuccessPayload } from 'app/modules/AgencyAgreement/store/reducers/receiveAgencyAgreementsReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

export const receiveAgencyAgreementsReducer: TPartialReducerObject<AgencyAgreementReducerState, ReceiveAgencyAgreementsActionAnyPayload> =
    (state, action) => {
        const processId = AgencyAgreementProcessId.ReceiveAgencyAgreements;
        const actions = AgencyAgreementActions.ReceiveAgencyAgreements;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as ReceiveAgencyAgreementsActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    agencyAgreements: { ...payload.agencyAgreements },
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasAgencyAgreementsReceived: true
                    }
                });
            }
        });
    };