import { GetAgencyAgreementResultDataModel } from 'app/web/InterlayerApiProxy/AgencyAgreementApiProxy/receiveAgencyAgreements/data-models/GetAgencyAgreementResultDataModel';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения агентских соглашений
 */
export type ReceiveAgencyAgreementsActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения агентских соглашений
 */
export type ReceiveAgencyAgreementsActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения агентских соглашений
 */
export type ReceiveAgencyAgreementsActionSuccessPayload = ReducerActionSuccessPayload & {

    /**
     * Агентские соглашения
     */
    agencyAgreements: GetAgencyAgreementResultDataModel
};

/**
 * Все возможные Action при получении агентских соглашений
 */
export type ReceiveAgencyAgreementsActionAnyPayload =
    | ReceiveAgencyAgreementsActionStartPayload
    | ReceiveAgencyAgreementsActionFailedPayload
    | ReceiveAgencyAgreementsActionSuccessPayload;