import { ReceiveAgencyAgreementsParams } from 'app/web/InterlayerApiProxy/AgencyAgreementApiProxy/receiveAgencyAgreements/input-params';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для получения агентских соглашений
 */
export type ReceiveAgencyAgreementsParamsThunkParams = IForceThunkParam & ReceiveAgencyAgreementsParams;