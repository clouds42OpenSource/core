import { reducerActionState } from 'app/common/functions/reducerActionState';
import { AgencyAgreementProcessId } from 'app/modules/AgencyAgreement/AgencyAgreementProcessId';
import { AgencyAgreementActions } from 'app/modules/AgencyAgreement/store/actions';
import { AgencyAgreementReducerState } from 'app/modules/AgencyAgreement/store/reducers';
import { AddAgencyAgreementActionAnyPayload, AddAgencyAgreementActionSuccessPayload } from 'app/modules/AgencyAgreement/store/reducers/addAgencyAgreementReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

export const addAgencyAgreementReducer: TPartialReducerObject<AgencyAgreementReducerState, AddAgencyAgreementActionAnyPayload> =
    (state, action) => {
        const processId = AgencyAgreementProcessId.AddAgencyAgreement;
        const actions = AgencyAgreementActions.AddAgencyAgreement;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as AddAgencyAgreementActionSuccessPayload;

                const newRecords = [
                    payload.agencyAgreementToAdd,
                    ...state.agencyAgreements.records
                ];

                return reducerStateProcessSuccess(state, processId, {
                    agencyAgreements: {
                        ...state.agencyAgreements,
                        records: newRecords,
                        metadata: {
                            ...state.agencyAgreements.metadata,
                            pageSize: state.agencyAgreements.metadata.pageSize + 1,
                            totalItemCount: state.agencyAgreements.metadata.totalItemCount + 1
                        }
                    }
                });
            }
        });
    };