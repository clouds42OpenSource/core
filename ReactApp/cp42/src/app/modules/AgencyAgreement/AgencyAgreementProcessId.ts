/**
 * Процесы AgencyAgreement
 */
export enum AgencyAgreementProcessId {
    /**
     * Процесс получить агентских соглашений
     */
    ReceiveAgencyAgreements = 'RECEIVE_AGENCY_AGREEMENTS',

    /**
     * Процесс добавления агентское соглашение
     */
    AddAgencyAgreement = 'ADD_AGENCY_AGREEMENT',
}