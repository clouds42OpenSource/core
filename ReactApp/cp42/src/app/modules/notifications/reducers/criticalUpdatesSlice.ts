import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { EStateNotification } from 'app/api/endpoints/notification/response';
import { NOTIFICATIONS } from 'app/modules/notifications/constants';
import { TCriticalUpdate } from 'app/modules/notifications/types';

const initialState: TCriticalUpdate = {
    id: '',
    isOpenNotification: false,
    content: '',
    date: '',
    state: EStateNotification.normal
};

export const criticalUpdatesSlice = createSlice({
    name: NOTIFICATIONS.criticalUpdates,
    initialState,
    reducers: {
        open(state, { payload: { date, content, id, state: stateNotification } }: PayloadAction<Omit<TCriticalUpdate, 'isOpenNotification'>>) {
            state.isOpenNotification = true;
            state.id = id;
            state.content = content;
            state.date = date;
            state.state = stateNotification;
        },
        read(state) {
            state.isOpenNotification = false;
            state.id = '';
            state.content = '';
            state.date = '';
            state.state = EStateNotification.normal;
        }
    }
});