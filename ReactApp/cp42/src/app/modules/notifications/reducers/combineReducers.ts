import { criticalUpdatesSlice } from 'app/modules/notifications/reducers/criticalUpdatesSlice';
import { getNotificationsSlice } from 'app/modules/notifications/reducers/getNotificationsSlice';
import { combineReducers } from 'redux';

export const NotificationsReducer = combineReducers({
    getNotifications: getNotificationsSlice.reducer,
    criticalNotification: criticalUpdatesSlice.reducer
});