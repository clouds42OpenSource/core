import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { GetNotifications, Notification } from 'app/api/endpoints/notification/response';
import { getInitialMetadata } from 'app/common/functions';
import { NOTIFICATIONS } from 'app/modules/notifications/constants';
import { SelectDataMetadataResponseDto } from 'app/web/common';

const initialState: GetNotifications = {
    data: {
        records: [],
        metadata: getInitialMetadata()
    },
    message: null,
    success: false
};

export const getNotificationsSlice = createSlice({
    name: NOTIFICATIONS.gatNotifications,
    initialState,
    reducers: {
        success(state, action: PayloadAction<SelectDataMetadataResponseDto<Notification>>) {
            state.data = action.payload;
            state.message = null;
            state.success = true;
        },
        update(state, action: PayloadAction<Notification>) {
            if (state.data) {
                state.data.records.unshift(action.payload);
            }
        },
        isReadById(state, action: PayloadAction<string>) {
            if (state.data) {
                state.data.records = state.data.records.map(item => {
                    if (item.id === action.payload) {
                        item.isRead = true;
                    }

                    return item;
                });
            }
        },
        readAllNotification(state) {
            if (state.data) {
                state.data.records = state.data.records.map(item => {
                    return {
                        ...item,
                        isRead: true
                    };
                });
            }
        },
        removedNotification(state, action: PayloadAction<string>) {
            if (state.data) {
                state.data.records = state.data.records.filter(item => item.id !== action.payload);
            }
        },
        error(state, action: PayloadAction<string>) {
            state.message = action.payload;
        }
    }
});