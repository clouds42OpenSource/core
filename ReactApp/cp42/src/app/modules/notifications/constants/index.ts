export const NOTIFICATIONS = {
    gatNotifications: 'gatNotifications',
    criticalUpdates: 'criticalUpdates'
} as const;