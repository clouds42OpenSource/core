import { EStateNotification } from 'app/api/endpoints/notification/response';

export type TCriticalUpdate = {
    id: string;
    isOpenNotification: boolean;
    content: string;
    date: string;
    state: EStateNotification;
};