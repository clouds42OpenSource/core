import { combineReducers } from 'redux';

import getExternalUsersListReducer from './getExternalUsersListSlice';

export const AccountDatabasesReducer = combineReducers({
    getExternalUsersListReducer
});