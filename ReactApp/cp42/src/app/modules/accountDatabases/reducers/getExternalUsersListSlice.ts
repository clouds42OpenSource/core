import { TDataLightReturn } from 'app/api/types';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { ACCOUNT_DATABASES_SLICE_NAMES } from 'app/modules/accountDatabases/constants';
import { GetAccessCardDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/AccessInfoDbDataModel';

const initialState: TDataLightReturn<GetAccessCardDataModel> = {
    data: null,
    error: null,
    isLoading: false
};

export const getExternalUsersListSlice = createSlice({
    name: ACCOUNT_DATABASES_SLICE_NAMES.getExternalUsersList,
    initialState,
    reducers: {
        loading(state) {
            state.isLoading = true;
        },
        empty(state) {
            state.isLoading = false;
            state.data = null;
            state.error = null;
        },
        success(state, action: PayloadAction<GetAccessCardDataModel>) {
            state.data = action.payload;
        },
        error(state, action: PayloadAction<string>) {
            state.error = action.payload;
        },
    }
});

export default getExternalUsersListSlice.reducer;