import { DbTemplatesActions } from 'app/modules/DbTemplates/actions';
import { ReceiveDbTemplateDataToEditThunkParams } from 'app/modules/DbTemplates/reducers/receiveDbTemplateDataToEditReducer/params';
import { ReceiveDbTemplateDataToEditActionFailedPayload, ReceiveDbTemplateDataToEditActionStartPayload, ReceiveDbTemplateDataToEditActionSuccessPayload } from 'app/modules/DbTemplates/reducers/receiveDbTemplateDataToEditReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = ReceiveDbTemplateDataToEditActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = ReceiveDbTemplateDataToEditActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = ReceiveDbTemplateDataToEditActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = ReceiveDbTemplateDataToEditThunkParams;

const { GetDbTemplateDataToEdit: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = DbTemplatesActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для запроса списка действий облака для логирования
 */
export class ReceiveDbTemplateDataToEditThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new ReceiveDbTemplateDataToEditThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        const processFlowState = args.getStore().DbTemplatesState;
        return {
            condition: !processFlowState.hasSuccessFor.hasCurrentTemplateDataReceived || args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().DbTemplatesState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        try {
            const api = InterlayerApiProxy.getDbTemplateApiProxy();
            const response = await api.getDbTemplateDataToEdit(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, args.inputParams!);
            args.success({
                currentTemplateDataEdit: response
            });
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}