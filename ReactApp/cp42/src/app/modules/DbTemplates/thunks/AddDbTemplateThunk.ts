import { PlatformTypeDescriptionExtensions } from 'app/common/enums';
import { DbTemplatesActions } from 'app/modules/DbTemplates/actions';
import { AddDbTemplateThunkParams } from 'app/modules/DbTemplates/reducers/addDbTemplateReducer/params';
import { AddDbTemplateActionFailedPayload, AddDbTemplateActionStartPayload, AddDbTemplateActionSuccessPayload } from 'app/modules/DbTemplates/reducers/addDbTemplateReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = AddDbTemplateActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = AddDbTemplateActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = AddDbTemplateActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = AddDbTemplateThunkParams;

const { AddDbTemplate: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = DbTemplatesActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для запроса списка действий облака для логирования
 */
export class AddDbTemplateThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new AddDbTemplateThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        return {
            condition: args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().DbTemplatesState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        try {
            const api = InterlayerApiProxy.getDbTemplateApiProxy();
            const result = await api.addDbtemplate(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, args.inputParams!);

            args.success({
                dbTemplateToAdd: {
                    dbTemplateId: result.dbTemplateId,
                    dbTemplateName: args.inputParams!.templateName,
                    dbTemplateDescription: args.inputParams!.templateDescription,
                    platform: PlatformTypeDescriptionExtensions.getPlatformTypeText(args.inputParams!.platform)
                }
            });
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}