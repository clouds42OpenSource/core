import { DbTemplatesActions } from 'app/modules/DbTemplates/actions';
import { GetDbTemplateConfiguration1CNameThunkParams } from 'app/modules/DbTemplates/reducers/getDbTemplateConfiguration1CNameReducer/params';
import { GetDbTemplateConfiguration1CNameFailedPayload, GetDbTemplateConfiguration1CNameStartPayload, GetDbTemplateConfiguration1CNameSuccessPayload } from 'app/modules/DbTemplates/reducers/getDbTemplateConfiguration1CNameReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсере при вызове действия ACTION_START
 */
type TActionStartPayload = GetDbTemplateConfiguration1CNameStartPayload;

/**
 * Тип данных в редюсере при вызове действия ACTION_FAILED
 */
type TActionFailedPayload = GetDbTemplateConfiguration1CNameFailedPayload;

/**
 * Тип данных в редюсере при вызове действия ACTION_SUCCESS
 */
type TActionSuccessPayload = GetDbTemplateConfiguration1CNameSuccessPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = GetDbTemplateConfiguration1CNameThunkParams;

const { GetDbTemplateConfiguration1CName: { START_ACTION, FAILED_ACTION, SUCCESS_ACTION } } = DbTemplatesActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для запроса списка имен конфигурации
 */
export class GetDbTemplateConfiguration1CNameThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new GetDbTemplateConfiguration1CNameThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        return {
            condition: args.getStore().DbTemplatesState.hasSuccessFor.hasDbTemplateConfiguration1CName || args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().DbTemplatesState
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        try {
            const api = InterlayerApiProxy.getDbTemplateApiProxy();
            const response = await api.getDbTemplateConfiguration1CName(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY);
            args.success({
                dbTemplateConfiguration1CName: response
            });
        } catch (er) {
            console.log(er);
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}