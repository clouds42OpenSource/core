/**
 * Процесы DbTemplates
 */
export enum DbTemplatesProcessId {
    /**
     * Процесс получить шаблоны
     */
    ReceiveDbTemplates = 'RECEIVE_DB_TEMPLATES',

    /**
     * Процесс получить данные для редактирования шаблона
     */
    GetDbTemplateDataToEdit = 'GET_DB_TEMPLATE_DATA_TO_EDIT',

    /**
     * Процесс обновить шаблон
     */
    UpdateDbTemplate = 'UPDATE_DB_TEMPLATE',

    /**
     * Процесс добавления шаблона
     */
    AddDbTemplate = 'ADD_DB_TEMPLATE',

    /**
     * Процесс удаления шаблона
     */
    DeleteDbTemplate = 'DELETE_DB_TEMPLATE',

    /**
     * Процесс получения списка имен конфигурации
     */
    GetDbTemplateConfiguration1CName = 'GET_TEMPLATE_CONFIGURATION_1C_NAME'
}