import { Configurations1CConsts } from 'app/modules/configurations1C/constants';
import { DbTemplatesProcessId } from 'app/modules/DbTemplates/IndustryProcessId';
import { createReducerActions } from 'core/redux/functions';

/**
 * Все доступные actions дле редюсера DbTemplates
 */
export const DbTemplatesActions = {
    /**
     * Набор action на получение шаблонов
     */
    ReceiveDbTemplates: createReducerActions(Configurations1CConsts.reducerName, DbTemplatesProcessId.ReceiveDbTemplates),

    /**
     * Набор action на получение данных для редактирования шаблона
     */
    GetDbTemplateDataToEdit: createReducerActions(Configurations1CConsts.reducerName, DbTemplatesProcessId.GetDbTemplateDataToEdit),

    /**
     * Набор action на обновление шаблона
     */
    UpdateDbTemplate: createReducerActions(Configurations1CConsts.reducerName, DbTemplatesProcessId.UpdateDbTemplate),

    /**
     * Набор action на добавление шаблона
     */
    AddDbTemplate: createReducerActions(Configurations1CConsts.reducerName, DbTemplatesProcessId.AddDbTemplate),

    /**
     * Набор action на удаление шаблона
     */
    DeleteDbTemplate: createReducerActions(Configurations1CConsts.reducerName, DbTemplatesProcessId.DeleteDbTemplate),

    /**
     * Набор action на получение списка имен конфигурации
     */
    GetDbTemplateConfiguration1CName: createReducerActions(Configurations1CConsts.reducerName, DbTemplatesProcessId.GetDbTemplateConfiguration1CName)
};