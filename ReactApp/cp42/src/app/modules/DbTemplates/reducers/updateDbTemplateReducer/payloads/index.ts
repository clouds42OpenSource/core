import { DbTemplateSelectItemDataModel } from 'app/web/InterlayerApiProxy/DbTemplateApiProxy/receiveDbTemplates';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения обновления шаблона
 */
export type UpdateDbTemplateActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении обновления шаблона
 */
export type UpdateDbTemplateActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении обновления шаблона
 */
export type UpdateDbTemplateActionSuccessPayload = ReducerActionSuccessPayload & {

    /**
     * шаблон для обновления
     */
    dbTemplateToUpdateToUpdate: DbTemplateSelectItemDataModel;
};

/**
 * Все возможные Action при обновлении шаблона
 */
export type UpdateDbTemplateActionAnyPayload =
    | UpdateDbTemplateActionStartPayload
    | UpdateDbTemplateActionFailedPayload
    | UpdateDbTemplateActionSuccessPayload;