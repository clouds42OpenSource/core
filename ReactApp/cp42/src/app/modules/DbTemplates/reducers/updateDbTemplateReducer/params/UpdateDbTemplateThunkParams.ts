import { UpdateDbTemplateParams } from 'app/web/InterlayerApiProxy/DbTemplateApiProxy/updateDbTemplate/input-params';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для обновления шаблона
 */
export type UpdateDbTemplateThunkParams = IForceThunkParam & UpdateDbTemplateParams;