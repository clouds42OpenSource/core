import { reducerActionState } from 'app/common/functions/reducerActionState';
import { DbTemplatesActions } from 'app/modules/DbTemplates/actions';
import { DbTemplatesProcessId } from 'app/modules/DbTemplates/IndustryProcessId';
import { DbTemplatesReducerState } from 'app/modules/DbTemplates/reducers';
import { UpdateDbTemplateActionAnyPayload, UpdateDbTemplateActionSuccessPayload } from 'app/modules/DbTemplates/reducers/updateDbTemplateReducer/payloads';
import { reducerStateProcessFail, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для редактирования записи в DbTemplate
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const updateDbTemplateReducer: TPartialReducerObject<DbTemplatesReducerState, UpdateDbTemplateActionAnyPayload> =
    (state, action) => {
        const processId = DbTemplatesProcessId.UpdateDbTemplate;
        const actions = DbTemplatesActions.UpdateDbTemplate;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as UpdateDbTemplateActionSuccessPayload;

                const foundIndex = state.dbTemplates.records.findIndex(item => item.dbTemplateId === payload.dbTemplateToUpdateToUpdate.dbTemplateId);

                if (foundIndex < 0) {
                    return reducerStateProcessFail(state, processId, new Error(`Не получилось найти шаблон с ID ${ payload.dbTemplateToUpdateToUpdate.dbTemplateId }`));
                }

                const newRecords = [
                    ...state.dbTemplates.records
                ];

                newRecords[foundIndex] = {
                    ...payload.dbTemplateToUpdateToUpdate
                };

                return reducerStateProcessSuccess(state, processId, {
                    dbTemplates: {
                        ...state.dbTemplates,
                        records: newRecords
                    }
                });
            }
        });
    };