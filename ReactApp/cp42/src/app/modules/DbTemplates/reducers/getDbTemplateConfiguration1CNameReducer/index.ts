import { TPartialReducerObject } from 'core/redux/types';
import { DbTemplatesReducerState } from 'app/modules/DbTemplates/reducers';
import { GetDbTemplateConfiguration1CNameAnyPayload, GetDbTemplateConfiguration1CNameSuccessPayload } from 'app/modules/DbTemplates/reducers/getDbTemplateConfiguration1CNameReducer/payloads';
import { DbTemplatesProcessId } from 'app/modules/DbTemplates/IndustryProcessId';
import { DbTemplatesActions } from 'app/modules/DbTemplates/actions';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { reducerStateProcessSuccess } from 'core/redux/functions';

/**
 * Редюсер для получения списка имен конфигураций
 */
export const GetDbTemplateConfiguration1CNameReducer: TPartialReducerObject<DbTemplatesReducerState, GetDbTemplateConfiguration1CNameAnyPayload> = (state, action) => {
    const processId = DbTemplatesProcessId.GetDbTemplateConfiguration1CName;
    const actions = DbTemplatesActions.GetDbTemplateConfiguration1CName;

    return reducerActionState({
        action,
        actions,
        processId,
        state,
        onSuccessProcessState: () => {
            const payload = action.payload as GetDbTemplateConfiguration1CNameSuccessPayload;
            return reducerStateProcessSuccess(state, processId, {
                dbTemplateConfiguration1CName: payload.dbTemplateConfiguration1CName,
                hasSuccessFor: {
                    ...state.hasSuccessFor,
                    hasDbTemplateConfiguration1CName: true
                }
            });
        }
    });
};