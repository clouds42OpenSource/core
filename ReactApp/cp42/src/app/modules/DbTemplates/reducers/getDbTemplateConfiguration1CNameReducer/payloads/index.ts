import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсере при старте получения списка имен конфигурации
 */
export type GetDbTemplateConfiguration1CNameStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсере при неуспешном получении списка имен конфигураций
 */
export type GetDbTemplateConfiguration1CNameFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсере при успешном получении списка имен конфигураций
 */
export type GetDbTemplateConfiguration1CNameSuccessPayload = ReducerActionSuccessPayload & {
    dbTemplateConfiguration1CName: string[]
};

/**
 * Все возможные actions при получении списка имен конфигураций
 */
export type GetDbTemplateConfiguration1CNameAnyPayload =
    GetDbTemplateConfiguration1CNameStartPayload |
    GetDbTemplateConfiguration1CNameFailedPayload |
    GetDbTemplateConfiguration1CNameSuccessPayload;