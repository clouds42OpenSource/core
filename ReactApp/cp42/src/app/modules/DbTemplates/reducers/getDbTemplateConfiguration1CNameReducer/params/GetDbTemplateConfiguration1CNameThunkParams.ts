import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров на получение списка имен конфигурации
 */
export type GetDbTemplateConfiguration1CNameThunkParams = IForceThunkParam;