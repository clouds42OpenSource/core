import { reducerActionState } from 'app/common/functions/reducerActionState';
import { DbTemplatesActions } from 'app/modules/DbTemplates/actions';
import { DbTemplatesProcessId } from 'app/modules/DbTemplates/IndustryProcessId';
import { AddDbTemplateActionAnyPayload, AddDbTemplateActionSuccessPayload } from 'app/modules/DbTemplates/reducers/addDbTemplateReducer/payloads';
import { DbTemplatesReducerState } from 'app/modules/DbTemplates/reducers/DbTemplatesReducerState';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для добавления записи в DbTemplate
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const addDbTemplateReducer: TPartialReducerObject<DbTemplatesReducerState, AddDbTemplateActionAnyPayload> =
    (state, action) => {
        const processId = DbTemplatesProcessId.AddDbTemplate;
        const actions = DbTemplatesActions.AddDbTemplate;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as AddDbTemplateActionSuccessPayload;

                const newRecords = [
                    payload.dbTemplateToAdd,
                    ...state.dbTemplates.records
                ];

                return reducerStateProcessSuccess(state, processId, {
                    dbTemplates: {
                        ...state.dbTemplates,
                        records: newRecords,
                        metadata: {
                            ...state.dbTemplates.metadata,
                            pageSize: state.dbTemplates.metadata.pageSize + 1,
                            totalItemCount: state.dbTemplates.metadata.totalItemCount + 1
                        }
                    }
                });
            }
        });
    };