import { DbTemplateSelectItemDataModel } from 'app/web/InterlayerApiProxy/DbTemplateApiProxy/receiveDbTemplates';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения добавления шаблона
 */
export type AddDbTemplateActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении добавления шаблона
 */
export type AddDbTemplateActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении добавления шаблона
 */
export type AddDbTemplateActionSuccessPayload = ReducerActionSuccessPayload & {
    /**
     * Шаблон для добавления
     */
    dbTemplateToAdd: DbTemplateSelectItemDataModel;
};

/**
 * Все возможные Action при добавлении шаблона
 */
export type AddDbTemplateActionAnyPayload =
    | AddDbTemplateActionStartPayload
    | AddDbTemplateActionFailedPayload
    | AddDbTemplateActionSuccessPayload;