import { AddDbTemplateParams } from 'app/web/InterlayerApiProxy/DbTemplateApiProxy/addDbTemplate/input-params';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для добавления шаблона
 */
export type AddDbTemplateThunkParams = IForceThunkParam & AddDbTemplateParams;