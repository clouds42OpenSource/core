import { DbTemplatesProcessId } from 'app/modules/DbTemplates/IndustryProcessId';
import { DbTemplateDataToEditDataModel } from 'app/web/InterlayerApiProxy/DbTemplateApiProxy/getTemplateDataToEdit/data-models/DbTemplateDataToEditDataModel';
import { GetDbTemplatesResultDataModel } from 'app/web/InterlayerApiProxy/DbTemplateApiProxy/receiveDbTemplates';
import { IReducerState } from 'core/redux/interfaces';

/**
 * Состояние редюсера для работы с шаблонами
 */
export type DbTemplatesReducerState = IReducerState<DbTemplatesProcessId> & {

    /**
     * Шаблоны
     */
    dbTemplates: GetDbTemplatesResultDataModel
    /**
     * Текущий шаблон в состоянии редактирования
     */
    currentTemplateDataEdit: DbTemplateDataToEditDataModel;
    /**
     * Список имен конфигурации
     */
    dbTemplateConfiguration1CName: string[];
    hasSuccessFor: {
        /**
         * Если true, то шаблоны получены
         */
        hasDbTemplatesReceived: boolean,
        /**
         * Если true, то шаблон для редактирования получен
         */
        hasCurrentTemplateDataReceived: boolean,
        /**
         * Если true, то список имен конфигурации получен
         */
        hasDbTemplateConfiguration1CName: boolean
    };
};