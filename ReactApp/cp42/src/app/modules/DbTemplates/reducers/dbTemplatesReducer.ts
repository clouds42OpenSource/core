import { getInitialMetadata } from 'app/common/functions';
import { DbTemplatesConsts } from 'app/modules/DbTemplates/constants';
import { addDbTemplateReducer } from 'app/modules/DbTemplates/reducers/addDbTemplateReducer';
import { DbTemplatesReducerState } from 'app/modules/DbTemplates/reducers/DbTemplatesReducerState';
import { deleteDbTemplateReducer } from 'app/modules/DbTemplates/reducers/deleteDbTemplateReducer';
import { receiveDbTemplateDataToEdit } from 'app/modules/DbTemplates/reducers/receiveDbTemplateDataToEditReducer';
import { receiveDbTemplatesReducer } from 'app/modules/DbTemplates/reducers/receiveDbTemplatesReducer';
import { updateDbTemplateReducer } from 'app/modules/DbTemplates/reducers/updateDbTemplateReducer';
import { GetDbTemplateConfiguration1CNameReducer } from 'app/modules/DbTemplates/reducers/getDbTemplateConfiguration1CNameReducer';
import { createReducer, initReducerState } from 'core/redux/functions';

/**
 * Начальное состояние редюсера DbTemplates
 */
const initialState = initReducerState<DbTemplatesReducerState>(
    DbTemplatesConsts.reducerName,
    {
        dbTemplates: {
            records: [],
            metadata: getInitialMetadata()
        },
        currentTemplateDataEdit: {
            dbTemplateToEdit: undefined
        },
        dbTemplateConfiguration1CName: [],
        hasSuccessFor: {
            hasDbTemplatesReceived: false,
            hasCurrentTemplateDataReceived: false,
            hasDbTemplateConfiguration1CName: false
        }
    }
);

/**
 * Объединённые части редюсера для всего состояния DbTemplates
 */
const partialReducers = [
    receiveDbTemplatesReducer,
    receiveDbTemplateDataToEdit,
    updateDbTemplateReducer,
    addDbTemplateReducer,
    deleteDbTemplateReducer,
    GetDbTemplateConfiguration1CNameReducer
];

/**
 * Редюсер DbTemplates
 */
export const dbTemplatesReducer = createReducer(initialState, partialReducers);