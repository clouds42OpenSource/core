import { DbTemplateDataToEditDataModel } from 'app/web/InterlayerApiProxy/DbTemplateApiProxy/getTemplateDataToEdit/data-models/DbTemplateDataToEditDataModel';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения шаблона для редактирования
 */
export type ReceiveDbTemplateDataToEditActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения шаблона для редактирования
 */
export type ReceiveDbTemplateDataToEditActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения шаблона для редактирования
 */
export type ReceiveDbTemplateDataToEditActionSuccessPayload = ReducerActionSuccessPayload & {

    /**
     * Шаблон для редактирования
     */
    currentTemplateDataEdit: DbTemplateDataToEditDataModel;
};

/**
 * Все возможные Action при получении шаблона для редактирования
 */
export type ReceiveDbTemplateDataToEditActionAnyPayload =
    | ReceiveDbTemplateDataToEditActionStartPayload
    | ReceiveDbTemplateDataToEditActionFailedPayload
    | ReceiveDbTemplateDataToEditActionSuccessPayload;