import { reducerActionState } from 'app/common/functions/reducerActionState';
import { DbTemplatesActions } from 'app/modules/DbTemplates/actions';
import { DbTemplatesProcessId } from 'app/modules/DbTemplates/IndustryProcessId';
import { DbTemplatesReducerState } from 'app/modules/DbTemplates/reducers';
import { ReceiveDbTemplateDataToEditActionAnyPayload, ReceiveDbTemplateDataToEditActionSuccessPayload } from 'app/modules/DbTemplates/reducers/receiveDbTemplateDataToEditReducer/payloads';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения записи в DbTemplate
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const receiveDbTemplateDataToEdit: TPartialReducerObject<DbTemplatesReducerState, ReceiveDbTemplateDataToEditActionAnyPayload> =
    (state, action) => {
        const processId = DbTemplatesProcessId.GetDbTemplateDataToEdit;
        const actions = DbTemplatesActions.GetDbTemplateDataToEdit;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onStartProcessState: () => {
                return reducerStateProcessStart(state, processId, {
                    currentTemplateDataEdit: {
                        ...state.currentTemplateDataEdit,
                        dbTemplateToEdit: undefined
                    },
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasCurrentTemplateDataReceived: false
                    }
                });
            },
            onSuccessProcessState: () => {
                const payload = action.payload as ReceiveDbTemplateDataToEditActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    currentTemplateDataEdit: { ...payload.currentTemplateDataEdit },
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasCurrentTemplateDataReceived: true
                    }
                });
            }
        });
    };