import { GetDbTemplateDataToEditParams } from 'app/web/InterlayerApiProxy/DbTemplateApiProxy/getTemplateDataToEdit/input-params';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для получения шаблонов
 */
export type ReceiveDbTemplateDataToEditThunkParams = IForceThunkParam & GetDbTemplateDataToEditParams;