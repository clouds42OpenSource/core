import { reducerActionState } from 'app/common/functions/reducerActionState';
import { DbTemplatesActions } from 'app/modules/DbTemplates/actions';
import { DbTemplatesProcessId } from 'app/modules/DbTemplates/IndustryProcessId';
import { DbTemplatesReducerState } from 'app/modules/DbTemplates/reducers';
import { ReceiveDbTemplatesActionAnyPayload, ReceiveDbTemplatesActionSuccessPayload } from 'app/modules/DbTemplates/reducers/receiveDbTemplatesReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения записи в DbTemplate
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const receiveDbTemplatesReducer: TPartialReducerObject<DbTemplatesReducerState, ReceiveDbTemplatesActionAnyPayload> =
    (state, action) => {
        const processId = DbTemplatesProcessId.ReceiveDbTemplates;
        const actions = DbTemplatesActions.ReceiveDbTemplates;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as ReceiveDbTemplatesActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    dbTemplates: { ...payload.dbTemplates },
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasDbTemplatesReceived: true
                    }
                });
            }
        });
    };