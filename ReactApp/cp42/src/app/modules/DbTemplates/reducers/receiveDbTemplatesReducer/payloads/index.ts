import { GetDbTemplatesResultDataModel } from 'app/web/InterlayerApiProxy/DbTemplateApiProxy/receiveDbTemplates';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения шаблонов
 */
export type ReceiveDbTemplatesActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения шаблонов
 */
export type ReceiveDbTemplatesActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения шаблонов
 */
export type ReceiveDbTemplatesActionSuccessPayload = ReducerActionSuccessPayload & {

    /**
     * шаблоны
     */
    dbTemplates: GetDbTemplatesResultDataModel
};

/**
 * Все возможные Action при получении шаблонов
 */
export type ReceiveDbTemplatesActionAnyPayload =
    | ReceiveDbTemplatesActionStartPayload
    | ReceiveDbTemplatesActionFailedPayload
    | ReceiveDbTemplatesActionSuccessPayload;