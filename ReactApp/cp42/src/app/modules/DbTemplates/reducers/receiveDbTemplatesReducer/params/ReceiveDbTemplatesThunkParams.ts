import { ReceiveDbTemplatesParams } from 'app/web/InterlayerApiProxy/DbTemplateApiProxy/receiveDbTemplates/input-params';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для получения шаблонов
 */
export type ReceiveDbTemplatesThunkParams = IForceThunkParam & ReceiveDbTemplatesParams;