import { DeleteDbTemplateParams } from 'app/web/InterlayerApiProxy/DbTemplateApiProxy/deleteDbTemplate/input-params';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для удаления шаблона
 */
export type DeleteDbTemplateThunkParams = IForceThunkParam & DeleteDbTemplateParams;