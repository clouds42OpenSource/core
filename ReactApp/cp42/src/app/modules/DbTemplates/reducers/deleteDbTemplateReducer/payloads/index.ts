import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения удаления шаблона
 */
export type DeleteDbTemplateActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении удаления шаблона
 */
export type DeleteDbTemplateActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении удаления шаблона
 */
export type DeleteDbTemplateActionSuccessPayload = ReducerActionSuccessPayload & {

    /**
     * ID шаблона для удаления
     */
    dbTemplateId: string;
};

/**
 * Все возможные Action при удалении шаблона
 */
export type DeleteDbTemplateActionAnyPayload =
    | DeleteDbTemplateActionStartPayload
    | DeleteDbTemplateActionFailedPayload
    | DeleteDbTemplateActionSuccessPayload;