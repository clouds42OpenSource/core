import { reducerActionState } from 'app/common/functions/reducerActionState';
import { DbTemplatesActions } from 'app/modules/DbTemplates/actions';
import { DbTemplatesProcessId } from 'app/modules/DbTemplates/IndustryProcessId';
import { DbTemplatesReducerState } from 'app/modules/DbTemplates/reducers';
import { DeleteDbTemplateActionAnyPayload, DeleteDbTemplateActionSuccessPayload } from 'app/modules/DbTemplates/reducers/deleteDbTemplateReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для удаления записи в DbTemplate
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const deleteDbTemplateReducer: TPartialReducerObject<DbTemplatesReducerState, DeleteDbTemplateActionAnyPayload> =
    (state, action) => {
        const processId = DbTemplatesProcessId.DeleteDbTemplate;
        const actions = DbTemplatesActions.DeleteDbTemplate;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as DeleteDbTemplateActionSuccessPayload;

                const newRecords = state.dbTemplates.records.filter(item => item.dbTemplateId !== payload.dbTemplateId);

                return reducerStateProcessSuccess(state, processId, {
                    dbTemplates: {
                        ...state.dbTemplates,
                        records: newRecords,
                        metadata: {
                            ...state.dbTemplates.metadata,
                            pageSize: state.dbTemplates.metadata.pageSize - 1,
                            totalItemCount: state.dbTemplates.metadata.totalItemCount - 1
                        }
                    }
                });
            }
        });
    };