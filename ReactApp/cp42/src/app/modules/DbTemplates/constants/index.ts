/**
 * Константы для модуля DbTemplates
 */
export const DbTemplatesConsts = {
    /**
     * Название редюсера для модуля DbTemplates
     */
    reducerName: 'DBTEMPLATES'
};