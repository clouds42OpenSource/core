export const ARM_MANAGER_SLICE_NAMES = {
    getArmPeriod: 'getArmPeriod',
    getArmDetails: 'getArmDetails',
    getArmConfigurationRelease: 'getArmConfigurationRelease'
} as const;