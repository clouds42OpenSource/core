import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { TDataLightReturn } from 'app/api/types';
import { TGetArmConfigurationReleasesResponse } from 'app/api/endpoints/armManager/response';
import { ARM_MANAGER_SLICE_NAMES } from '../constants';

const initialState: TDataLightReturn<TGetArmConfigurationReleasesResponse> = {
    data: null,
    error: null,
    isLoading: false
};

export const getArmConfigurationReleaseSlice = createSlice({
    name: ARM_MANAGER_SLICE_NAMES.getArmConfigurationRelease,
    initialState,
    reducers: {
        loading(state) {
            state.isLoading = true;
            state.data = null;
            state.error = null;
        },
        success(state, action: PayloadAction<TGetArmConfigurationReleasesResponse>) {
            state.data = action.payload;
            state.error = null;
            state.isLoading = false;
        },
        error(state, action: PayloadAction<string>) {
            state.error = action.payload;
            state.isLoading = false;
            state.data = null;
        }
    }
});

export default getArmConfigurationReleaseSlice.reducer;