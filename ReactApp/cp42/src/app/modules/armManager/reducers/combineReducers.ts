import { combineReducers } from 'redux';

import armConfigurationReleaseReducer from './getArmConfigurationReleaseSlice';
import armDetailsReducer from './getArmDetailsSlice';
import armPeriodReducer from './getArmPeriodSlice';

export const ArmManagerReducer = combineReducers({
    armConfigurationReleaseReducer,
    armDetailsReducer,
    armPeriodReducer
});