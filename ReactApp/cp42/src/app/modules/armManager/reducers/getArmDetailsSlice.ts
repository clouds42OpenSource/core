import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { TDataLightReturn } from 'app/api/types';
import { TGetArmDetailsResponse } from 'app/api/endpoints/armManager/response';
import { ARM_MANAGER_SLICE_NAMES } from '../constants';

const initialState: TDataLightReturn<TGetArmDetailsResponse[]> = {
    data: [],
    error: null,
    isLoading: false
};

export const getArmDetailsSlice = createSlice({
    name: ARM_MANAGER_SLICE_NAMES.getArmDetails,
    initialState,
    reducers: {
        loading(state) {
            state.data = [];
            state.error = null;
            state.isLoading = true;
        },
        success(state, action: PayloadAction<TGetArmDetailsResponse[]>) {
            state.data = action.payload;
            state.error = null;
            state.isLoading = false;
        },
        error(state, action: PayloadAction<string>) {
            state.data = [];
            state.error = action.payload;
            state.isLoading = false;
        }
    }
});

export default getArmDetailsSlice.reducer;