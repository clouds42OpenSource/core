import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { TDataLightReturn } from 'app/api/types';
import { TGetArmPeriodResponse } from 'app/api/endpoints/armManager/response';
import { ARM_MANAGER_SLICE_NAMES } from '../constants';

const initialState: TDataLightReturn<TGetArmPeriodResponse[]> = {
    data: [],
    error: null,
    isLoading: false
};

export const getArmPeriodSlice = createSlice({
    name: ARM_MANAGER_SLICE_NAMES.getArmPeriod,
    initialState,
    reducers: {
        loading(state) {
            state.isLoading = true;
            state.data = [];
            state.error = null;
        },
        success(state, action: PayloadAction<TGetArmPeriodResponse[]>) {
            state.data = action.payload;
            state.error = null;
            state.isLoading = false;
        },
        error(state, action: PayloadAction<string>) {
            state.error = action.payload;
            state.isLoading = false;
            state.data = [];
        }
    }
});

export default getArmPeriodSlice.reducer;