/**
 * Процесы AccountList
 */
export enum AccountListProcessId {
    /**
     * Процесс получить список аккаунтов
     */
    ReceiveAccountList = 'RECEIVE_ACCOUNT_LIST',

    /**
     * Процесс обновления аккаунта в списке
     */
    UpdateAccountInList = 'UPDATE_ACCOUNT_IN_LIST'
}