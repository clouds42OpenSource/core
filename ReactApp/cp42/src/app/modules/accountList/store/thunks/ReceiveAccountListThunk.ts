import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { ReceiveAccountListActionFailedPayload, ReceiveAccountListActionStartPayload, ReceiveAccountListActionSuccessPayload } from 'app/modules/accountList/store/reducers/receiveAccountListReducer/payloads';
import { AccountListActions } from 'app/modules/accountList/store/actions';
import { AppReduxStoreState } from 'app/redux/types';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { ErrorObject } from 'core/redux';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ReceiveAccountListThunkParams } from 'app/modules/accountList/store/reducers/receiveAccountListReducer/params';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = ReceiveAccountListActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = ReceiveAccountListActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = ReceiveAccountListActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = ReceiveAccountListThunkParams;

const { ReceiveAccountList: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = AccountListActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для запроса списка действий облака для логирования
 */
export class ReceiveAccountListThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new ReceiveAccountListThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        const processFlowState = args.getStore().AccountListState;
        return {
            condition: !processFlowState.hasSuccessFor.hasAccountListReceived || args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().AccountListState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        try {
            const api = InterlayerApiProxy.getAccountApi();
            const response = await api.receiveAccountList(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, args.inputParams!);

            args.success({
                accountList: response
            });
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}