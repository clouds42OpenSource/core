import { AccountListActions } from 'app/modules/accountList/store/actions';
import { UpdateAccountInListThunkParams } from 'app/modules/accountList/store/reducers/updateAccountInListReducer/params';
import { UpdateAccountInListActionAnyPayload, UpdateAccountInListActionFailedPayload, UpdateAccountInListActionSuccessPayload } from 'app/modules/accountList/store/reducers/updateAccountInListReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = UpdateAccountInListActionAnyPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = UpdateAccountInListActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = UpdateAccountInListActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = UpdateAccountInListThunkParams;

const { UpdateAccountInList: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = AccountListActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для обновления аккаунта в списке
 */
export class UpdateAccountInListThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new UpdateAccountInListThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        return {
            condition: args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().AccountListState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        try {
            if (!args.inputParams) throw new Error('Не верно переданы параметры аккаунта');
            if (args.inputParams.isVip === null || args.inputParams.isVip === undefined) throw new Error('Не указан признак IsVip для аккаунта');
            if (!args.inputParams.caption) throw new Error('Не указано название аккаунта');
            if (!args.inputParams.accountId) throw new Error('Не указан ID аккаунта');

            args.success({
                accountData: {
                    isVip: args.inputParams.isVip,
                    caption: args.inputParams.caption,
                    accountId: args.inputParams.accountId
                }
            });
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}