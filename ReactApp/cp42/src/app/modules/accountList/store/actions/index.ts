import { AccountListProcessId } from 'app/modules/accountList/AccountListProcessId';
import { AccountListConsts } from 'app/modules/accountList/constants';
import { createReducerActions } from 'core/redux/functions';

/**
 * Все доступные actions дле редюсера AccountList
 */
export const AccountListActions = {
    /**
     * Набор action на получение списка аккаунтов
     */
    ReceiveAccountList: createReducerActions(AccountListConsts.reducerName, AccountListProcessId.ReceiveAccountList),

    /**
     * Набор action для обновления аккаунта в списке
     */
    UpdateAccountInList: createReducerActions(AccountListConsts.reducerName, AccountListProcessId.UpdateAccountInList)
};