import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';
import { UpdateAccountInListParams } from 'app/modules/accountList/store/reducers/updateAccountInListReducer/params';

/**
 * Данные в редюсер при старте выполнения обновления аккаунта в списке
 */
export type UpdateAccountInListActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении обновления аккаунта в списке
 */
export type UpdateAccountInListActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении обновления аккаунта в списке
 */
export type UpdateAccountInListActionSuccessPayload = ReducerActionSuccessPayload & {

    /**
     * Данные аккаунта
     */
    accountData: UpdateAccountInListParams
};

/**
 * Все возможные Action при обновлении аккаунта в списке
 */
export type UpdateAccountInListActionAnyPayload =
    | UpdateAccountInListActionStartPayload
    | UpdateAccountInListActionFailedPayload
    | UpdateAccountInListActionSuccessPayload;