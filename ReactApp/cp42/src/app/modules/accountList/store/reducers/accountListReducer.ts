import { getInitialMetadata } from 'app/common/functions';
import { AccountListReducerState } from 'app/modules/accountList/store/reducers';
import { receiveAccountListReducer } from 'app/modules/accountList/store/reducers/receiveAccountListReducer';
import { updateAccountInListReducer } from 'app/modules/accountList/store/reducers/updateAccountInListReducer';
import { DatabaseListConsts } from 'app/modules/databaseList/constants';
import { createReducer, initReducerState } from 'core/redux/functions';

/**
 * Начальное состояние редюсера AccountList
 */
const initialState = initReducerState<AccountListReducerState>(
    DatabaseListConsts.reducerName,
    {
        accountList: {
            records: [],
            metadata: getInitialMetadata()
        },
        hasSuccessFor: {
            hasAccountListReceived: false
        }
    }
);

/**
 * Объединённые части редюсера для всего состояния AccountList
 */
const partialReducers = [
    receiveAccountListReducer,
    updateAccountInListReducer
];

/**
 * Редюсер AccountList
 */
export const accountListReducer = createReducer(initialState, partialReducers);