import { reducerActionState } from 'app/common/functions/reducerActionState';
import { AccountListProcessId } from 'app/modules/accountList/AccountListProcessId';
import { AccountListActions } from 'app/modules/accountList/store/actions';
import { AccountListReducerState } from 'app/modules/accountList/store/reducers';
import { UpdateAccountInListActionAnyPayload, UpdateAccountInListActionSuccessPayload } from 'app/modules/accountList/store/reducers/updateAccountInListReducer/payloads';
import { reducerStateProcessFail, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для обновления аккаунта в списке
 * @param state состояние списка аккаунтов
 * @param action действие
 */
export const updateAccountInListReducer: TPartialReducerObject<AccountListReducerState, UpdateAccountInListActionAnyPayload> =
    (state, action) => {
        const processId = AccountListProcessId.UpdateAccountInList;
        const actions = AccountListActions.UpdateAccountInList;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as UpdateAccountInListActionSuccessPayload;
                const errorMessage = `Аккаунт по ID ${ payload.accountData.accountId } не найден`;

                const accountItem = state.accountList.records.find(item => item.accountId === payload.accountData.accountId);
                if (!accountItem) return reducerStateProcessFail(state, processId, new Error(errorMessage));

                const accountItemIndex = state.accountList.records.indexOf(accountItem);
                if (accountItemIndex < 0) return reducerStateProcessFail(state, processId, new Error(errorMessage));

                const newAccountItem = {
                    ...state.accountList.records[accountItemIndex],
                    isVip: payload.accountData.isVip,
                    accountCaption: payload.accountData.caption
                };

                const newRecords = [...state.accountList.records];
                newRecords[accountItemIndex] = newAccountItem;

                return reducerStateProcessSuccess(state, processId, {
                    accountList: {
                        ...state.accountList,
                        records: newRecords
                    }
                });
            }
        });
    };