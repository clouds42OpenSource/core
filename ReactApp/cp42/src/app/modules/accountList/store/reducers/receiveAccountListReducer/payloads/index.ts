import { GetAccountListResultDataModel } from 'app/web/InterlayerApiProxy/AccountApiProxy/receiveAccountList/data-models';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения списка аккаунтов
 */
export type ReceiveAccountListActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения списка аккаунтов
 */
export type ReceiveAccountListActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения списка аккаунтов
 */
export type ReceiveAccountListActionSuccessPayload = ReducerActionSuccessPayload & {

    /**
     * Список аккаунтов
     */
    accountList: GetAccountListResultDataModel
};

/**
 * Все возможные Action при получении списка аккаунтов
 */
export type ReceiveAccountListActionAnyPayload =
    | ReceiveAccountListActionStartPayload
    | ReceiveAccountListActionFailedPayload
    | ReceiveAccountListActionSuccessPayload;