import { AccountListProcessId } from 'app/modules/accountList/AccountListProcessId';
import { GetAccountListResultDataModel } from 'app/web/InterlayerApiProxy/AccountApiProxy/receiveAccountList/data-models';
import { IReducerState } from 'core/redux/interfaces';

/**
 * Состояние редюсера для работы с списком аккаунтов
 */
export type AccountListReducerState = IReducerState<AccountListProcessId> & {

    /**
     * список аккаунтов
     */
    accountList: GetAccountListResultDataModel
    hasSuccessFor: {
        /**
         * Если true, то список аккаунтов получен
         */
        hasAccountListReceived: boolean
    };
};