import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров на обновление аккаунта в списке
 */
export type UpdateAccountInListParams = {
    /**
     * Название аккаунта
     */
    caption: string,

    /**
     * признак что аккаунт ВИП
     */
    isVip: boolean,

    /**
     * ID аккаунта
     */
    accountId: string
};

/**
 * Модель параметров санки на обновление аккаунта в списке
 */
export type UpdateAccountInListThunkParams = UpdateAccountInListParams & IForceThunkParam;