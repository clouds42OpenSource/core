import { ReceiveAccountListParams } from 'app/web/InterlayerApiProxy/AccountApiProxy/receiveAccountList/input-params';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для получения списка аккаунтов
 */
export type ReceiveAccountListThunkParams = IForceThunkParam & ReceiveAccountListParams;