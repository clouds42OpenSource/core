import { reducerActionState } from 'app/common/functions/reducerActionState';
import { AccountListProcessId } from 'app/modules/accountList/AccountListProcessId';
import { AccountListActions } from 'app/modules/accountList/store/actions';
import { AccountListReducerState } from 'app/modules/accountList/store/reducers';
import { ReceiveAccountListActionAnyPayload, ReceiveAccountListActionSuccessPayload } from 'app/modules/accountList/store/reducers/receiveAccountListReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

export const receiveAccountListReducer: TPartialReducerObject<AccountListReducerState, ReceiveAccountListActionAnyPayload> =
    (state, action) => {
        const processId = AccountListProcessId.ReceiveAccountList;
        const actions = AccountListActions.ReceiveAccountList;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as ReceiveAccountListActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    accountList: { ...payload.accountList },
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasAccountListReceived: true
                    }
                });
            }
        });
    };