/**
 * Константы для модуля AccountList
 */
export const AccountListConsts = {
    /**
     * Название редюсера для модуля AccountList
     */
    reducerName: 'ACCOUNT_LIST'
};