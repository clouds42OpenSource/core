import { TGetTransactionInfoResponse } from 'app/api/endpoints/articles/response/getTransactionInfoResponse';
import { TData, TDataReturn } from 'app/api/types';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { ARTICLES_SLICE_NAMES } from 'app/modules/articles/constants';
import { SelectDataResultMetadataModel } from 'app/web/common';

const initialState: TDataReturn<SelectDataResultMetadataModel<TGetTransactionInfoResponse>> = {
    data: null,
    error: null,
    isLoading: false
};

export const getArticlesTransactionsSlice = createSlice({
    name: ARTICLES_SLICE_NAMES.getArticlesTransactions,
    initialState,
    reducers: {
        loading(state) {
            state.isLoading = true;
            state.error = null;
        },
        success(state, action: PayloadAction<TData<SelectDataResultMetadataModel<TGetTransactionInfoResponse>>>) {
            state.isLoading = false;
            state.error = null;
            state.data = action.payload;
        },
        error(state, action: PayloadAction<string>) {
            state.isLoading = false;
            state.error = action.payload;
            state.data = null;
        }
    }
});

export default getArticlesTransactionsSlice.reducer;