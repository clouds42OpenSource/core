import { combineReducers } from 'redux';

import getArticlesSummaryInfoReducer from './getArticlesSummaryInfoSlice';
import getArticleCategoriesReducer from './getArticleCategoriesSlice';
import getArticlesTransactionsReducer from './getArticlesTransactionsSlice';

export const ArticlesReducer = combineReducers({
    getArticlesSummaryInfoReducer,
    getArticleCategoriesReducer,
    getArticlesTransactionsReducer
});