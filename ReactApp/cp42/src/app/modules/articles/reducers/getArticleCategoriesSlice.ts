import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { TGetArticleCategories } from 'app/api/endpoints/articles/response/getArticleCategories';
import { TData, TDataReturn } from 'app/api/types';
import { ARTICLES_SLICE_NAMES } from 'app/modules/articles/constants';

const initialState: TDataReturn<TGetArticleCategories[]> = {
    data: null,
    error: null,
    isLoading: false,
};

export const getArticleCategoriesSlice = createSlice({
    name: ARTICLES_SLICE_NAMES.getArticleCategories,
    initialState,
    reducers: {
        loading(state) {
            state.isLoading = true;
            state.error = null;
        },
        success(state, action: PayloadAction<TData<TGetArticleCategories[]>>) {
            state.isLoading = false;
            state.error = null;
            state.data = action.payload;
        },
        error(state, action: PayloadAction<string>) {
            state.isLoading = false;
            state.error = action.payload;
            state.data = null;
        }
    }
});

export default getArticleCategoriesSlice.reducer;