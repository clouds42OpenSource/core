import { TData, TDataReturn } from 'app/api/types';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { ARTICLES_SLICE_NAMES } from 'app/modules/articles/constants';
import { getArticlesSummaryInfoResponseDto } from 'app/api/endpoints/articles/response/getArticlesSummaryInfoResponseDto';

const initialState: TDataReturn<getArticlesSummaryInfoResponseDto> = {
    data: null,
    error: null,
    isLoading: false
};

export const getArticlesSummaryInfoSlice = createSlice({
    name: ARTICLES_SLICE_NAMES.getArticlesSummaryInfo,
    initialState,
    reducers: {
        loading(state) {
            state.isLoading = true;
            state.error = null;
        },
        success(state, action: PayloadAction<TData<getArticlesSummaryInfoResponseDto>>) {
            state.isLoading = false;
            state.error = null;
            state.data = action.payload;
        },
        error(state, action: PayloadAction<string>) {
            state.isLoading = false;
            state.error = action.payload;
            state.data = null;
        }
    }
});

export default getArticlesSummaryInfoSlice.reducer;