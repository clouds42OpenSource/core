export const ARTICLES_SLICE_NAMES = {
    getArticlesSummaryInfo: 'getArticlesSummaryInfo',
    getArticleCategories: 'getArticleCategories',
    getArticlesTransactions: 'getArticlesTransactions',
} as const;