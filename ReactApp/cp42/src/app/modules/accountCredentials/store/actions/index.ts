import { AccCredConsts } from 'app/modules/accountCredentials/constants';
import { AccountCredentialsProcessId } from 'app/modules/accountCredentials';
import { createReducerActions } from 'core/redux/functions';

/**
 * Все доступные actions дле редюсера NavMenu
 */
export const AccountCredentialsActions = {
    /**
     * Набор action для изменения состояния NavMenu элемента
     */
    getUserPermissionsByType: createReducerActions(AccCredConsts.reducerName, AccountCredentialsProcessId.GetUserPermissionByType),
};