import { GetPermissionByTypeActionAnyPayload, GetPermissionByTypeActionSuccessPayload } from 'app/modules/accountCredentials/store/reducers/getPermissionsByTypeReducer/payloads';

import { AccountCredentialsProcessId } from 'app/modules/accountCredentials';
import { AccountCredentialsReducerState } from 'app/modules/accountCredentials/store/reducers/AccountCredentialsReducerState';
import { IReducerAction } from 'core/redux/interfaces';
import { reducerStateProcessSuccess } from 'core/redux/functions';

/**
 * Функция для изменений состояния редюсера
 * @param state — состояние редюсера
 * @param action — действие над редюсером
 * @returns обьект с учетными данными
 */
export function getPermissionsByTypeSuccess(state: AccountCredentialsReducerState, action: IReducerAction<GetPermissionByTypeActionAnyPayload>) {
    const processId = AccountCredentialsProcessId.GetUserPermissionByType;
    const payload = action.payload as GetPermissionByTypeActionSuccessPayload;
    return reducerStateProcessSuccess(state, processId, {
        permissionType: [...payload.permissionByType],
        hasSuccessFor: {
            ...state.hasSuccessFor,
            permissionTypeReceived: true
        }
    });
}