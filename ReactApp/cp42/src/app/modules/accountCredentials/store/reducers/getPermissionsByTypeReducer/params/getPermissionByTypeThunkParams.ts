/**
 * Модель параметров на получение User-credentials
 */
export type getPermissionByTypeThunkParams = {
    Permissions: string[];
}