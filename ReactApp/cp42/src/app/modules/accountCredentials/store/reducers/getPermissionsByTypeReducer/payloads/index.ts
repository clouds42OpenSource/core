import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения user-credentials
 */
export type GetPermissionByTypeActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения user-credentials
 */
export type GetPermissionByTypeActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения user-credentials
 */
export type GetPermissionByTypeActionSuccessPayload = ReducerActionSuccessPayload & {
    permissionByType: string[];
};

/**
 * Все возможные Action при получении user-credentials
 */
export type GetPermissionByTypeActionAnyPayload =
    | GetPermissionByTypeActionStartPayload
    | GetPermissionByTypeActionFailedPayload
    | GetPermissionByTypeActionSuccessPayload;