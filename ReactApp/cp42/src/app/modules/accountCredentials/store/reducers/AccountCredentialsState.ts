import { createReducer, initReducerState } from 'core/redux/functions';
import { AccountCredentialsReducerState } from 'app/modules/accountCredentials/store/reducers/AccountCredentialsReducerState';
import { getPermissionsByTypeReducer } from 'app/modules/accountCredentials/store/reducers/getPermissionsByTypeReducer';

/**
 * Начальное состояние редюсера NavBarMenu
 */
const initialState = initReducerState<AccountCredentialsReducerState>(
    'AccountPermissions',
    {
        permissionType: [],
        hasSuccessFor: {
            permissionTypeReceived: false
        },
    }
);

/**
 * Объединённые части редюсера для всего состояния NavBarMenu
 */
const partialReducers = [
    getPermissionsByTypeReducer,
];

/**
 * Редюсер NavBarMenu
 */
export const AccountCredentialsReducer = createReducer(initialState, partialReducers);