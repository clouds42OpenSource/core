import { AccountCredentialsProcessId } from 'app/modules/accountCredentials';
import { IReducerState } from 'core/redux/interfaces';

/**
 * Состояние редюсера для работы с AccountCredentials
 */
export type AccountCredentialsReducerState = IReducerState<AccountCredentialsProcessId> & {
    /**
     * Всe user-credentials
     */
    permissionType: string[];
    /**
     * содержит ярлыки для фиксации загрузок данных, была ли загрузка или ещё нет
     */
    hasSuccessFor: {
        /**
         * Если true, то user-credentials уже было загружёно
         */
        permissionTypeReceived: boolean;
    };
};