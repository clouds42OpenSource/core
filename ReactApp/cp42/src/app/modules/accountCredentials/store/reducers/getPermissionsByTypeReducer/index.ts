import { AccountCredentialsActions } from 'app/modules/accountCredentials/store/actions';
import { AccountCredentialsProcessId } from 'app/modules/accountCredentials';
import { AccountCredentialsReducerState } from 'app/modules/accountCredentials/store/reducers/AccountCredentialsReducerState';
import { GetPermissionByTypeActionAnyPayload } from 'app/modules/accountCredentials/store/reducers/getPermissionsByTypeReducer/payloads';
import { TPartialReducerObject } from 'core/redux/types';
import { getPermissionsByTypeSuccess } from 'app/modules/accountCredentials/store/reducers/functions/getPermissionsByTypeSuccess';
import { reducerActionState } from 'app/common/functions/reducerActionState';

/**
 * Редюсер для получения AccountCredentials
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const getPermissionsByTypeReducer: TPartialReducerObject<AccountCredentialsReducerState, GetPermissionByTypeActionAnyPayload> =
    (state, action) => {
        const processId = AccountCredentialsProcessId.GetUserPermissionByType;
        const actions = AccountCredentialsActions.getUserPermissionsByType;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => getPermissionsByTypeSuccess(state, action)
        });
    };