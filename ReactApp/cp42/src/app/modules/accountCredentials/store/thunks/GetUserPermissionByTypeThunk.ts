import { GetPermissionByTypeActionFailedPayload, GetPermissionByTypeActionStartPayload, GetPermissionByTypeActionSuccessPayload } from 'app/modules/accountCredentials/store/reducers/getPermissionsByTypeReducer/payloads';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';

import { AccountCredentialsActions } from 'app/modules/accountCredentials/store/actions';
import { AppReduxStoreState } from 'app/redux/types';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { ErrorObject } from 'core/redux';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { RequestKind } from 'core/requestSender/enums';
import { getPermissionByTypeThunkParams } from 'app/modules/accountCredentials/store/reducers/getPermissionsByTypeReducer/params';

type TActionStartPayload = GetPermissionByTypeActionStartPayload;
type TActionSuccessPayload = GetPermissionByTypeActionSuccessPayload;
type TActionFailedPayload = GetPermissionByTypeActionFailedPayload;
type TInputDataModel = getPermissionByTypeThunkParams;

const { getUserPermissionsByType: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = AccountCredentialsActions;

type TThunkStartActionArguments = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputDataModel>;
type TThunkStartActionArgumentsResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;
type TThunkStartExecutionArguments = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputDataModel>;

/**
 * Thunk для загрузки User-Permission-By-Type
 */
export class GetUserPermissionByTypeThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputDataModel> {
    public static invoke(args?: TInputDataModel) {
        return new GetUserPermissionByTypeThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionArguments): TThunkStartActionArgumentsResult {
        const { AccountCredentialsState } = args.getStore();
        return {
            condition: !AccountCredentialsState.hasSuccessFor.permissionTypeReceived,
            getReducerStateFunc: () => args.getStore().AccountCredentialsState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionArguments): Promise<void> {
        const getUserPermissionsApi = InterlayerApiProxy.getUserPermissionsApiProxy();

        try {
            const permissionByType = await getUserPermissionsApi.getUserPermissionByType(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, args.inputParams!);
            args.success({
                permissionByType
            });
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}