/**
 * Процесы NavMenu
 */
export enum AccountCredentialsProcessId {
    /**
     * Получить PERMISSIONS по типу
     */
    GetUserPermissionByType = 'GET_USER_PERMISSION_BY_TYPE',
}