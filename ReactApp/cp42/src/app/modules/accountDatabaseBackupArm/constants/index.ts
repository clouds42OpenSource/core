/**
 * Константы для модуля Account Database Backup Arm
 */
export const AccountDatabaseBackupArmConsts = {
    /**
     * Название редюсера для модуля Account Database Backup Arm
     */
    reducerName: 'ACCOUNT_DATABASE_BACKUP_ARM'
};