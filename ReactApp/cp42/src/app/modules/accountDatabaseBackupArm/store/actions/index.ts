import { AccountDatabaseBackupArmProcessId } from 'app/modules/accountDatabaseBackupArm/AccountDatabaseBackupArmProcessId';
import { AccountDatabaseBackupArmConsts } from 'app/modules/accountDatabaseBackupArm/constants';
import { createReducerActions } from 'core/redux/functions';

/**
 * Все доступные actions дле редюсера AccountDatabaseBackupArm
 */
export const AccountDatabaseBackupArmActions = {
    /**
     * Набор action на получение списка историй бэкапов баз аккаунтов с пагинацией для таблиц Арм бэкапирования
     */
    GetAccountDatabaseBackupHistoriesWithPagination: createReducerActions(AccountDatabaseBackupArmConsts.reducerName, AccountDatabaseBackupArmProcessId.GetAccountDatabaseBackupHistoriesWithPagination)
};