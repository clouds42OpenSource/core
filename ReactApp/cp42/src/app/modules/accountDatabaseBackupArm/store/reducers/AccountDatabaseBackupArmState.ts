import { AccountDatabaseBackupArmProcessId } from 'app/modules/accountDatabaseBackupArm/AccountDatabaseBackupArmProcessId';
import { AccountDatabaseBackupArmPaginationDataModel } from 'app/web/InterlayerApiProxy/AccountDatabaseBackupArmApiProxy/getAccountDatabaseBackupHistoriesPagination';
import { IReducerState } from 'core/redux/interfaces';

/**
 * Состояние редюсера для работы с историей бэкапов баз аккаунта
 */
export type AccountDatabaseBackupArmState = IReducerState<AccountDatabaseBackupArmProcessId> & {
    /**
     * Модель списка историй бэкапов баз аккаунтов с пагинацией для таблиц Арм бэкапирования
     */
    accountDatabaseBackupHistory: AccountDatabaseBackupArmPaginationDataModel,

    /**
     * Если true, то модель списка историй бэкапов баз аккаунтов с пагинацией для таблиц Арм бэкапирования уже получена
     */
    hasAccountDatabaseBackupArmReceived: boolean
};