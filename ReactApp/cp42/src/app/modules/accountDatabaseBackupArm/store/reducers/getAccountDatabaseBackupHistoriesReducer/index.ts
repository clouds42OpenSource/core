import { partialFillOf } from 'app/common/functions';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { AccountDatabaseBackupArmProcessId } from 'app/modules/accountDatabaseBackupArm/AccountDatabaseBackupArmProcessId';
import { AccountDatabaseBackupArmActions } from 'app/modules/accountDatabaseBackupArm/store/actions';
import { AccountDatabaseBackupArmState } from 'app/modules/accountDatabaseBackupArm/store/reducers';
import { GetAccountDatabaseBackupArmActionAnyPayload, GetAccountDatabaseBackupArmActionSuccessPayload } from 'app/modules/accountDatabaseBackupArm/store/reducers/getAccountDatabaseBackupHistoriesReducer/payload';
import { PaginationDataModel } from 'app/web/common/data-models';
import { AccountDatabaseBackupHistoryPeriodDataModel } from 'app/web/InterlayerApiProxy/AccountDatabaseBackupArmApiProxy/getAccountDatabaseBackupHistoriesPagination';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения списка историй бэкапов баз аккаунтов с пагинацией для таблиц Арм бэкапирования
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const getAccountDatabaseBackupHistoriesReducer: TPartialReducerObject<AccountDatabaseBackupArmState, GetAccountDatabaseBackupArmActionAnyPayload> =
    (state, action) => {
        const processId = AccountDatabaseBackupArmProcessId.GetAccountDatabaseBackupHistoriesWithPagination;
        const actions = AccountDatabaseBackupArmActions.GetAccountDatabaseBackupHistoriesWithPagination;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onStartProcessState: () => {
                return reducerStateProcessStart(state, processId, {
                    accountDatabaseBackupHistory: {
                        dangerTable: {
                            records: [],
                            pagination: partialFillOf<PaginationDataModel>()
                        },
                        warningTable: {
                            records: [],
                            pagination: partialFillOf<PaginationDataModel>()
                        },
                        dangerTablePeriod: partialFillOf<AccountDatabaseBackupHistoryPeriodDataModel>(),
                        warningTablePeriod: partialFillOf<AccountDatabaseBackupHistoryPeriodDataModel>()
                    },
                    hasAccountDatabaseBackupArmReceived: false
                });
            },
            onSuccessProcessState: () => {
                const payload = action.payload as GetAccountDatabaseBackupArmActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    accountDatabaseBackupHistory: payload,
                    hasAccountDatabaseBackupArmReceived: true
                });
            }
        });
    };