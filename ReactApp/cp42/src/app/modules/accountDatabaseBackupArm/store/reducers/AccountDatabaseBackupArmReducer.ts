import { partialFillOf } from 'app/common/functions';
import { AccountDatabaseBackupArmConsts } from 'app/modules/accountDatabaseBackupArm/constants';
import { AccountDatabaseBackupArmState } from 'app/modules/accountDatabaseBackupArm/store/reducers/AccountDatabaseBackupArmState';
import { getAccountDatabaseBackupHistoriesReducer } from 'app/modules/accountDatabaseBackupArm/store/reducers/getAccountDatabaseBackupHistoriesReducer';
import { PaginationDataModel } from 'app/web/common/data-models';
import { AccountDatabaseBackupHistoryPeriodDataModel } from 'app/web/InterlayerApiProxy/AccountDatabaseBackupArmApiProxy/getAccountDatabaseBackupHistoriesPagination';
import { createReducer, initReducerState } from 'core/redux/functions';

/**
 * Начальное состояние редюсера AccountDatabaseBackupArm
 */
const initialState = initReducerState<AccountDatabaseBackupArmState>(
    AccountDatabaseBackupArmConsts.reducerName,
    {
        accountDatabaseBackupHistory: {
            dangerTable: {
                records: [],
                pagination: partialFillOf<PaginationDataModel>()
            },
            warningTable: {
                records: [],
                pagination: partialFillOf<PaginationDataModel>()
            },
            dangerTablePeriod: partialFillOf<AccountDatabaseBackupHistoryPeriodDataModel>(),
            warningTablePeriod: partialFillOf<AccountDatabaseBackupHistoryPeriodDataModel>()
        },

        hasAccountDatabaseBackupArmReceived: false,
    }
);

/**
 * Объединённые части редюсера для всего состояния AccountDatabaseBackupArm
 */
const partialReducers = [
    getAccountDatabaseBackupHistoriesReducer
];

/**
 * Редюсер AccountDatabaseBackupArm
 */
export const AccountDatabaseBackupArmReducer = createReducer(initialState, partialReducers);