import { AccountDatabaseBackupArmPaginationDataModel } from 'app/web/InterlayerApiProxy/AccountDatabaseBackupArmApiProxy/getAccountDatabaseBackupHistoriesPagination';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте получения списка историй бэкапов баз аккаунтов с пагинацией для таблиц Арм бэкапирования
 */
export type GetAccountDatabaseBackupArmActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при ошибке получения списка историй бэкапов баз аккаунтов с пагинацией для таблиц Арм бэкапирования
 */
export type GetAccountDatabaseBackupArmActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном получении списка историй бэкапов баз аккаунтов с пагинацией для таблиц Арм бэкапирования
 */
export type GetAccountDatabaseBackupArmActionSuccessPayload = ReducerActionSuccessPayload & AccountDatabaseBackupArmPaginationDataModel;

/**
 * Все возможные Action при получении списка историй бэкапов баз аккаунтов с пагинацией для таблиц Арм бэкапирования
 */
export type GetAccountDatabaseBackupArmActionAnyPayload =
    | GetAccountDatabaseBackupArmActionStartPayload
    | GetAccountDatabaseBackupArmActionSuccessPayload
    | GetAccountDatabaseBackupArmActionFailedPayload;