import { GetAccountDatabaseBackupHistoriesParams } from 'app/web/InterlayerApiProxy/AccountDatabaseBackupArmApiProxy/getAccountDatabaseBackupHistoriesPagination';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель на запрос получения истории бэкапов баз аккаунта
 */
export type GetAccountDatabaseBackupHistoriesThunkParams = IForceThunkParam & GetAccountDatabaseBackupHistoriesParams;