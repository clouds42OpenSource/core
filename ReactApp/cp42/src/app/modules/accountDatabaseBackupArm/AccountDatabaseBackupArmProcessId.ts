/**
 * Процесы AccountDatabaseBackupArm
 */
export enum AccountDatabaseBackupArmProcessId {
    /**
     * Процесс на получение списка историй бэкапов баз аккаунтов с пагинацией для таблиц Арм бэкапирования
     */
    GetAccountDatabaseBackupHistoriesWithPagination = 'GET_ACCOUNT_DATABASE_BACKUP_HISTORIES_WITH_PAGINATION'
}