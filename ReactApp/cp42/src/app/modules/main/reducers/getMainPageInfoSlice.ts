import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { TDataLightReturn } from 'app/api/types';
import { TMainPageResponse } from 'app/api/endpoints/main/response';
import { MAIN_PAGE_SLICE_NAMES } from 'app/modules/main/constants';

const initialState: TDataLightReturn<TMainPageResponse> = {
    data: null,
    error: null,
    isLoading: false
};

export const getMainPageInfoSlice = createSlice({
    name: MAIN_PAGE_SLICE_NAMES.getMainPageInfo,
    initialState,
    reducers: {
        loading(state) {
            state.data = null;
            state.error = null;
            state.isLoading = true;
        },
        success(state, action: PayloadAction<TMainPageResponse>) {
            state.data = action.payload;
            state.error = null;
            state.isLoading = false;
        },
        error(state, action: PayloadAction<string>) {
            state.data = null;
            state.error = action.payload;
            state.isLoading = false;
        }
    }
});

export default getMainPageInfoSlice.reducer;