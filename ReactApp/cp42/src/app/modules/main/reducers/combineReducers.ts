import { combineReducers } from 'redux';

import mainPageReducer from './getMainPageInfoSlice';

export const MainPageReducer = combineReducers({
    mainPageReducer
});