import { getInitialMetadata } from 'app/common/functions';
import { DatabaseListConsts } from 'app/modules/databaseList/constants';
import { DatabaseListReducerState } from 'app/modules/databaseList/store/reducers/DatabaseListReducerState';
import { receiveDatabaseListReducer } from 'app/modules/databaseList/store/reducers/receiveDatabaseListReducer';
import { createReducer, initReducerState } from 'core/redux/functions';

/**
 * Начальное состояние редюсера DatabaseList
 */
const initialState = initReducerState<DatabaseListReducerState>(
    DatabaseListConsts.reducerName,
    {
        databaseList: {
            records: [],
            metadata: getInitialMetadata(),
        },
        hasSuccessFor: {
            hasDatabaseListReceived: false
        }
    }
);

/**
 * Объединённые части редюсера для всего состояния DatabaseList
 */
const partialReducers = [
    receiveDatabaseListReducer
];

/**
 * Редюсер DatabaseList
 */
export const databaseListReducer = createReducer(initialState, partialReducers);