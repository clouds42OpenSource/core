import { ReceiveDatabaseListParams } from 'app/web/InterlayerApiProxy/DatabaseApiProxy/receiveDatabaseList/input-params';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для получения списка баз
 */
export type ReceiveDatabaseListThunkParams = IForceThunkParam & ReceiveDatabaseListParams;