import { GetDatabaseListResultDataModel } from 'app/web/InterlayerApiProxy/DatabaseApiProxy/receiveDatabaseList/data-models';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения списка баз
 */
export type ReceiveDatabaseListActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения списка баз
 */
export type ReceiveDatabaseListActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения списка баз
 */
export type ReceiveDatabaseListActionSuccessPayload = ReducerActionSuccessPayload & {

    /**
     * Список баз
     */
    DatabaseList: GetDatabaseListResultDataModel
    orderBy?: string
    search?: string
};

/**
 * Все возможные Action при получении списка баз
 */
export type ReceiveDatabaseListActionAnyPayload =
    | ReceiveDatabaseListActionStartPayload
    | ReceiveDatabaseListActionFailedPayload
    | ReceiveDatabaseListActionSuccessPayload;