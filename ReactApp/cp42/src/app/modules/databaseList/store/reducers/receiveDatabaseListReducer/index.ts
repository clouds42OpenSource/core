import { reducerActionState } from 'app/common/functions/reducerActionState';
import { DatabaseListProcessId } from 'app/modules/databaseList/DatabaseListProcessId';
import { DatabaseListActions } from 'app/modules/databaseList/store/actions';
import { DatabaseListReducerState } from 'app/modules/databaseList/store/reducers/DatabaseListReducerState';
import { ReceiveDatabaseListActionAnyPayload, ReceiveDatabaseListActionSuccessPayload } from 'app/modules/databaseList/store/reducers/receiveDatabaseListReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

export const receiveDatabaseListReducer: TPartialReducerObject<DatabaseListReducerState, ReceiveDatabaseListActionAnyPayload> =
    (state, action) => {
        const processId = DatabaseListProcessId.ReceiveDatabaseList;
        const actions = DatabaseListActions.ReceiveDatabaseList;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as ReceiveDatabaseListActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    databaseList: {
                        ...payload.DatabaseList,
                        orderBy: payload.orderBy,
                        searchQuery: payload.search,
                    },
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasDatabaseListReceived: true
                    }
                });
            }
        });
    };