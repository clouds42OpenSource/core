import { DatabaseListProcessId } from 'app/modules/databaseList/DatabaseListProcessId';
import { GetDatabaseListResultDataModel } from 'app/web/InterlayerApiProxy/DatabaseApiProxy/receiveDatabaseList/data-models';
import { IReducerState } from 'core/redux/interfaces';

/**
 * Состояние редюсера для работы с списком баз
 */
export type DatabaseListReducerState = IReducerState<DatabaseListProcessId> & {

    /**
     * список баз
     */
    databaseList: GetDatabaseListResultDataModel
    hasSuccessFor: {
        /**
         * Если true, то список баз получен
         */
        hasDatabaseListReceived: boolean,
    };
};