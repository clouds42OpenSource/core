import { DatabaseListConsts } from 'app/modules/databaseList/constants';
import { DatabaseListProcessId } from 'app/modules/databaseList/DatabaseListProcessId';
import { createReducerActions } from 'core/redux/functions';

/**
 * Все доступные actions дле редюсера DatabaseList
 */
export const DatabaseListActions = {
    /**
     * Набор action на получение списка баз
     */
    ReceiveDatabaseList: createReducerActions(DatabaseListConsts.reducerName, DatabaseListProcessId.ReceiveDatabaseList)
};