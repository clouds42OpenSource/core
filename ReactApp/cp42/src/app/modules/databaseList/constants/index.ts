/**
 * Константы для модуля DatabaseList
 */
export const DatabaseListConsts = {
    /**
     * Название редюсера для модуля DatabaseList
     */
    reducerName: 'DATABASE_LIST'
};