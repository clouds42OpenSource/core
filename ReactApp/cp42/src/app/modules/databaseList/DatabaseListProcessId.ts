/**
 * Процесы DatabaseList
 */
export enum DatabaseListProcessId {
    /**
     * Процесс получить список баз
     */
    ReceiveDatabaseList = 'RECEIVE_DATABASE_LIST',
}