/**
 * Процесы для локалей
 */
export enum LocalesProcessId {

    /**
     * Процесс для получения данных по локалям
     */
    ReceiveLocalesData = 'RECEIVE_LOCALES_DATA',

    /**
     * Процесс для получения данных локали для редактирования
     */
    ReceiveLocaleDataForEditing = 'RECEIVE_LOCALE_DATA_FOR_EDITING',

    /**
     * Процесс для редактирования локали
     */
    EditLocaleData = 'EDIT_LOCALE_DATA',

    // Процесс получения локализации

    ReceiveLocalizations = 'RECEIVE_LOCALIZATIONS_DATA'
}