import { LocalesActions } from 'app/modules/locales/store/actions';
import { ReceiveLocalesThunkParams } from 'app/modules/locales/store/reducers/receiveLocalesData/params';
import { ReceiveLocalesActionSuccessPayload } from 'app/modules/locales/store/reducers/receiveLocalesData/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { ReducerActionFailedPayload, ReducerActionStartPayload } from 'core/redux/interfaces';
import { RequestKind } from 'core/requestSender/enums';

const { ReceiveLocalesData: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = LocalesActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, ReducerActionStartPayload, ReceiveLocalesActionSuccessPayload, ReducerActionFailedPayload, ReceiveLocalesThunkParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<ReducerActionStartPayload, ReducerActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, ReducerActionStartPayload, ReceiveLocalesActionSuccessPayload, ReducerActionFailedPayload, ReceiveLocalesThunkParams>;

/**
 * Thunk получения данных по локалям
 */
export class ReceiveLocalesThunk extends BaseReduxThunkObject<AppReduxStoreState, ReducerActionStartPayload, ReceiveLocalesActionSuccessPayload, ReducerActionFailedPayload, ReceiveLocalesThunkParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: ReceiveLocalesThunkParams) {
        return new ReceiveLocalesThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        const reducerState = args.getStore().LocalesState;
        return {
            condition: !reducerState.hasSuccessFor.hasLocalesDataReceived || args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().LocalesState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        try {
            const requestArgs = args.inputParams!;
            const localesApiProxy = InterlayerApiProxy.getLocalesApiProxy();
            const response = await localesApiProxy.receiveLocales(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, {
                pageNumber: requestArgs.pageNumber,
                filter: requestArgs.filter,
                orderBy: requestArgs.orderBy
            });

            args.success({
                locales: response.records,
                metadata: response.metadata,
                filter: requestArgs.filter!,
            });
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}