import { LocalesActions } from 'app/modules/locales/store/actions';
import { EditLocaleThunkParams } from 'app/modules/locales/store/reducers/editLocale/params/EditLocaleThunkParams';
import { EditLocaleDataActionSuccessPayload } from 'app/modules/locales/store/reducers/editLocale/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { ReducerActionFailedPayload, ReducerActionStartPayload } from 'core/redux/interfaces';
import { RequestKind } from 'core/requestSender/enums';

const { EditLocaleData: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = LocalesActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, ReducerActionStartPayload, EditLocaleDataActionSuccessPayload, ReducerActionFailedPayload, EditLocaleThunkParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<ReducerActionStartPayload, ReducerActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, ReducerActionStartPayload, EditLocaleDataActionSuccessPayload, ReducerActionFailedPayload, EditLocaleThunkParams>;

/**
 * Thunk редактирования данных локали
 */
export class EditLocaleThunk extends BaseReduxThunkObject<AppReduxStoreState, ReducerActionStartPayload, EditLocaleDataActionSuccessPayload, ReducerActionFailedPayload, EditLocaleThunkParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: EditLocaleThunkParams) {
        return new EditLocaleThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        return {
            condition: args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().LocalesState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        const requestArgs = args.inputParams!;
        const localesApiProxy = InterlayerApiProxy.getLocalesApiProxy();

        try {
            await localesApiProxy.editLocale(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, {
                localeId: requestArgs.localeId,
                cpSiteUrl: requestArgs.cpSiteUrl,
                defaultSegmentId: requestArgs.defaultSegmentId
            });

            args.success(requestArgs);
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}