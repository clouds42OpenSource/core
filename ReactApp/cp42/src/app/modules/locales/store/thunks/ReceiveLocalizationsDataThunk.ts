import { LocalesActions } from 'app/modules/locales/store/actions';
import { ReceiveLocalizationsThunkParams } from 'app/modules/locales/store/reducers/receiveLocalizationsData/params';
import { ReceiveLocalizationsActionSuccessPayload } from 'app/modules/locales/store/reducers/receiveLocalizationsData/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { ReducerActionFailedPayload, ReducerActionStartPayload } from 'core/redux/interfaces';
import { RequestKind } from 'core/requestSender/enums';

const { ReceiveLocalizationsData: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = LocalesActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, ReducerActionStartPayload, ReceiveLocalizationsActionSuccessPayload, ReducerActionFailedPayload, ReceiveLocalizationsThunkParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<ReducerActionStartPayload, ReducerActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, ReducerActionStartPayload, ReceiveLocalizationsActionSuccessPayload, ReducerActionFailedPayload, ReceiveLocalizationsThunkParams>;

/**
 * Thunk получения данных по локалям
 */
export class ReceiveLocalizationsDataThunk extends BaseReduxThunkObject<AppReduxStoreState, ReducerActionStartPayload, ReceiveLocalizationsActionSuccessPayload, ReducerActionFailedPayload, ReceiveLocalizationsThunkParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: ReceiveLocalizationsThunkParams) {
        return new ReceiveLocalizationsDataThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        return {
            condition: true,
            getReducerStateFunc: () => args.getStore().LocalesState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        try {
            const localesApiProxy = InterlayerApiProxy.getLocalesApiProxy();
            const response = await localesApiProxy.receiveLocalizations(RequestKind.SEND_BY_USER_SYNCHRONOUSLY);

            args.success({
                localizations: response
            });
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}