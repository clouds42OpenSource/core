import { LocalesActions } from 'app/modules/locales/store/actions';
import { ReceiveLocaleDataForEditingThunkParams } from 'app/modules/locales/store/reducers/receiveLocaleDataForEditing/params';
import { ReceiveLocaleDataForEditingActionSuccessPayload } from 'app/modules/locales/store/reducers/receiveLocaleDataForEditing/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { ReducerActionFailedPayload, ReducerActionStartPayload } from 'core/redux/interfaces';
import { RequestKind } from 'core/requestSender/enums';

const { ReceiveLocaleDataForEditing: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = LocalesActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, ReducerActionStartPayload, ReceiveLocaleDataForEditingActionSuccessPayload, ReducerActionFailedPayload, ReceiveLocaleDataForEditingThunkParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<ReducerActionStartPayload, ReducerActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, ReducerActionStartPayload, ReceiveLocaleDataForEditingActionSuccessPayload, ReducerActionFailedPayload, ReceiveLocaleDataForEditingThunkParams>;

/**
 * Thunk получения данных локали для редактирования
 */
export class ReceiveLocaleDataForEditingThunk extends BaseReduxThunkObject<AppReduxStoreState, ReducerActionStartPayload, ReceiveLocaleDataForEditingActionSuccessPayload, ReducerActionFailedPayload, ReceiveLocaleDataForEditingThunkParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: ReceiveLocaleDataForEditingThunkParams) {
        return new ReceiveLocaleDataForEditingThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        const reducerState = args.getStore().LocalesState;
        return {
            condition: !reducerState.hasSuccessFor.hasLocaleDataReceived || args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().LocalesState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        try {
            const requestArgs = args.inputParams!;
            const localesApiProxy = InterlayerApiProxy.getLocalesApiProxy();
            const response = await localesApiProxy.receiveLocaleDataForEditing(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, {
                localeId: requestArgs.localeId
            });

            args.success({
                data: response
            });
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}