import { LocalesProcessId } from 'app/modules/locales/LocalesProcessId';
import { LocalesConstants } from 'app/modules/locales/consts';
import { createReducerActions } from 'core/redux/functions';

/**
 * Все действия (Actions) для редюсера Locales
 */
export const LocalesActions = {

    /**
     * Действие для получения данных по локалям
     */
    ReceiveLocalesData: createReducerActions(LocalesConstants.reducerName, LocalesProcessId.ReceiveLocalesData),

    /**
     * Действие для получения данных локали для редактирования
     */
    ReceiveLocaleDataForEditing: createReducerActions(LocalesConstants.reducerName, LocalesProcessId.ReceiveLocaleDataForEditing),

    /**
     * Действие для редактирования локали
     */
    EditLocaleData: createReducerActions(LocalesConstants.reducerName, LocalesProcessId.EditLocaleData),

    // Действие для получения локализации
    ReceiveLocalizationsData: createReducerActions(LocalesConstants.reducerName, LocalesProcessId.ReceiveLocalizations)

};