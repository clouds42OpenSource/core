import { reducerActionState } from 'app/common/functions/reducerActionState';
import { LocalesProcessId } from 'app/modules/locales/LocalesProcessId';
import { LocalesActions } from 'app/modules/locales/store/actions';
import { EditLocaleDataActionAnyPayload, EditLocaleDataActionSuccessPayload } from 'app/modules/locales/store/reducers/editLocale/payloads';
import { localesState } from 'app/modules/locales/store/reducers/localesState';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для редактирования локали
 * @param state состояние
 * @param action действие
 */
export const editLocaleDataReducer: TPartialReducerObject<localesState, EditLocaleDataActionAnyPayload> =
    (state, action) => {
        const processId = LocalesProcessId.EditLocaleData;
        const actions = LocalesActions.EditLocaleData;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as EditLocaleDataActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    editLocaleData: payload,
                    localeData: { ...state.localeData }
                });
            }
        });
    };