import { reducerActionState } from 'app/common/functions/reducerActionState';
import { LocalesProcessId } from 'app/modules/locales/LocalesProcessId';
import { LocalesActions } from 'app/modules/locales/store/actions';
import { localesState } from 'app/modules/locales/store/reducers/localesState';
import { ReceiveLocalizationsActionSuccessPayload } from 'app/modules/locales/store/reducers/receiveLocalizationsData/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения данных по локалям
 * @param state состояние
 * @param action действие
 */
export const receiveLocalizationsDataReducer: TPartialReducerObject<localesState, ReceiveLocalizationsActionSuccessPayload> =
    (state, action) => {
        const processId = LocalesProcessId.ReceiveLocalizations;
        const actions = LocalesActions.ReceiveLocalizationsData;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as ReceiveLocalizationsActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    localizations: payload.localizations
                });
            }
        });
    };