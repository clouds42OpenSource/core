import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';
import { ReceiveLocaleDataForEditingParams } from 'app/web/InterlayerApiProxy/LocalesApiProxy/receiveLocaleDataForEditing/input-params';

/**
 * Модель параметров для получения данных локали для редактирования
 */
export type ReceiveLocaleDataForEditingThunkParams = IForceThunkParam & ReceiveLocaleDataForEditingParams;