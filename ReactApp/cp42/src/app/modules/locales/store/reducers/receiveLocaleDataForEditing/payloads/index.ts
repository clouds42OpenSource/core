import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';
import { LocaleDataModel } from 'app/web/InterlayerApiProxy/LocalesApiProxy/receiveLocales/data-models';

/**
 * Данные в редюсер при успешном выполнении получения данных локали
 */
export type ReceiveLocaleDataForEditingActionSuccessPayload = ReducerActionSuccessPayload & {

    /**
     * Данные локали
     */
    data: LocaleDataModel;
};

/**
 * Все возможные Action при получении данных локали
 */
export type ReceiveLocaleDataForEditingActionAnyPayload =
    | ReducerActionStartPayload
    | ReducerActionFailedPayload
    | ReceiveLocaleDataForEditingActionSuccessPayload;