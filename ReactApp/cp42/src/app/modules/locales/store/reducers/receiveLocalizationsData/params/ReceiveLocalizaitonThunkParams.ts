import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров на получение данных по локалям
 */
export type ReceiveLocalizationsThunkParams = IForceThunkParam;