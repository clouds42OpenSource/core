import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';
import { ReceiveLocalesParams } from 'app/web/InterlayerApiProxy/LocalesApiProxy/receiveLocales/input-params';

/**
 * Модель параметров на получение данных по локалям
 */
export type ReceiveLocalesThunkParams = IForceThunkParam & ReceiveLocalesParams;