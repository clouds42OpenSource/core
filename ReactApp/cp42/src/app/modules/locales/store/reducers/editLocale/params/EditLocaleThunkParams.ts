import { EditLocaleDataParams } from 'app/web/InterlayerApiProxy/LocalesApiProxy/editLocale/data-models';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров редактирования локали
 */
export type EditLocaleThunkParams = IForceThunkParam & EditLocaleDataParams;