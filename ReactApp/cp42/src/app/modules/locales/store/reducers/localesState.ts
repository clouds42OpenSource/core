import { LocalesProcessId } from 'app/modules/locales/LocalesProcessId';
import { EditLocaleDataParams } from 'app/web/InterlayerApiProxy/LocalesApiProxy/editLocale/data-models';
import { LocaleDataModel, LocaleItemDataModel, LocalesFilterDataModel } from 'app/web/InterlayerApiProxy/LocalesApiProxy/receiveLocales/data-models';
import { LocalizationsDataModel } from 'app/web/InterlayerApiProxy/LocalesApiProxy/receiveLocalizations/data-models';
import { MetadataModel } from 'app/web/common/data-models';
import { IReducerState } from 'core/redux/interfaces';

/**
 * Состояние редюсера для локалей
 */
export type localesState = IReducerState<LocalesProcessId> & {

    /**
     * Локали
     */
    locales: {

        /**
         * Массив записей
         */
        items: LocaleItemDataModel[];

        /**
         * Текущий фильтр для получения записей
         */
        filter: LocalesFilterDataModel;

        /**
         * Данные о странице для текущих записей
         */
        metadata: MetadataModel;
    };

    /**
     * Модель данных локали
     */
    localeData: LocaleDataModel;

    /**
     * Параметры редактирования локали
     */
    editLocaleData: EditLocaleDataParams;

    /**
     * содержит ярлыки для фиксации загрузок данных, была ли загрузка или ещё нет
     */
    hasSuccessFor: {
        /**
         * Если true, то данные по локалям уже получены
         */
        hasLocalesDataReceived: boolean;

        /**
         * Если true, то данные локали уже получены
         */
        hasLocaleDataReceived: boolean;
    };
    localizations: LocalizationsDataModel;
};