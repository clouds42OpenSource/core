import { reducerActionState } from 'app/common/functions/reducerActionState';
import { LocalesProcessId } from 'app/modules/locales/LocalesProcessId';
import { LocalesActions } from 'app/modules/locales/store/actions';
import { localesState } from 'app/modules/locales/store/reducers/localesState';
import { ReceiveLocalesActionAnyPayload, ReceiveLocalesActionSuccessPayload } from 'app/modules/locales/store/reducers/receiveLocalesData/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения данных по локалям
 * @param state состояние
 * @param action действие
 */
export const receiveLocalesDataReducer: TPartialReducerObject<localesState, ReceiveLocalesActionAnyPayload> =
    (state, action) => {
        const processId = LocalesProcessId.ReceiveLocalesData;
        const actions = LocalesActions.ReceiveLocalesData;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as ReceiveLocalesActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    locales: {
                        ...state.locales,
                        items: [...payload.locales],
                        filter: { ...payload.filter },
                        metadata: { ...payload.metadata }
                    },
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasLocalesDataReceived: true
                    }
                });
            }
        });
    };