import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';
import { EditLocaleDataParams } from 'app/web/InterlayerApiProxy/LocalesApiProxy/editLocale/data-models';

/**
 * Данные в редюсер при успешном редактировании локали
 */
export type EditLocaleDataActionSuccessPayload = ReducerActionSuccessPayload & EditLocaleDataParams;

/**
 * Все возможные Action при редактировании локали
 */
export type EditLocaleDataActionAnyPayload =
    | ReducerActionStartPayload
    | ReducerActionFailedPayload
    | EditLocaleDataActionSuccessPayload;