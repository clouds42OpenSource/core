import { reducerActionState } from 'app/common/functions/reducerActionState';
import { LocalesProcessId } from 'app/modules/locales/LocalesProcessId';
import { LocalesActions } from 'app/modules/locales/store/actions';
import { localesState } from 'app/modules/locales/store/reducers/localesState';
import { ReceiveLocaleDataForEditingActionAnyPayload, ReceiveLocaleDataForEditingActionSuccessPayload } from 'app/modules/locales/store/reducers/receiveLocaleDataForEditing/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения данных локали для редактирования
 * @param action действие
 */
export const receiveLocaleDataForEditingReducer: TPartialReducerObject<localesState, ReceiveLocaleDataForEditingActionAnyPayload> =
    (state, action) => {
        const processId = LocalesProcessId.ReceiveLocaleDataForEditing;
        const actions = LocalesActions.ReceiveLocaleDataForEditing;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as ReceiveLocaleDataForEditingActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    localeData: payload.data,
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasLocaleDataReceived: true
                    }
                });
            }
        });
    };