import { MetadataModel } from 'app/web/common/data-models';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';
import { LocaleItemDataModel, LocalesFilterDataModel } from 'app/web/InterlayerApiProxy/LocalesApiProxy/receiveLocales/data-models';

/**
 * Данные в редюсер при успешном выполнении получения записей по локалям
 */
export type ReceiveLocalesActionSuccessPayload = ReducerActionSuccessPayload & {

    /**
     * Массив записей
     */
    locales: LocaleItemDataModel[];

    /**
     * Текущий фильтр для получения записей
     */
    filter: LocalesFilterDataModel;

    /**
     * Данные о странице для текущих записей
     */
    metadata: MetadataModel;
};

/**
 * Все возможные Action при получении списка записей по локалям
 */
export type ReceiveLocalesActionAnyPayload =
    | ReducerActionStartPayload
    | ReducerActionFailedPayload
    | ReceiveLocalesActionSuccessPayload;