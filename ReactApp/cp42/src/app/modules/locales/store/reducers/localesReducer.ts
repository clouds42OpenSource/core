import { getInitialMetadata, partialFillOf } from 'app/common/functions';
import { LocalesConstants } from 'app/modules/locales/consts';
import { editLocaleDataReducer } from 'app/modules/locales/store/reducers/editLocale';
import { localesState } from 'app/modules/locales/store/reducers/localesState';
import { receiveLocaleDataForEditingReducer } from 'app/modules/locales/store/reducers/receiveLocaleDataForEditing';
import { receiveLocalesDataReducer } from 'app/modules/locales/store/reducers/receiveLocalesData';
import { receiveLocalizationsDataReducer } from 'app/modules/locales/store/reducers/receiveLocalizationsData';

import { EditLocaleDataParams } from 'app/web/InterlayerApiProxy/LocalesApiProxy/editLocale/data-models';
import { createReducer, initReducerState } from 'core/redux/functions';

/**
 * Начальное состояние редьюсера
 */
const initialState = initReducerState<localesState>(
    LocalesConstants.reducerName,
    {
        locales: {
            items: [],
            metadata: getInitialMetadata(),
            filter: {
                searchString: null
            }
        },
        localeData: {
            localeId: '',
            name: '',
            country: '',
            currencyCode: null,
            currency: '',
            defaultSegmentId: '',
            cpSiteUrl: ''
        },
        editLocaleData: partialFillOf<EditLocaleDataParams>(),
        hasSuccessFor: {
            hasLocalesDataReceived: false,
            hasLocaleDataReceived: false
        },
        localizations: []
    }
);

const partialReducers = [
    receiveLocalesDataReducer,
    receiveLocaleDataForEditingReducer,
    editLocaleDataReducer,
    receiveLocalizationsDataReducer
];

export const localesReducer = createReducer(initialState, partialReducers);