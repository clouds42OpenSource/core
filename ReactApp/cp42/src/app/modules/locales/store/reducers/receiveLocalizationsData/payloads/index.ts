import { LocalizationsDataModel } from 'app/web/InterlayerApiProxy/LocalesApiProxy/receiveLocalizations/data-models';
import { ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при успешном выполнении получения записей по локалям
 */
export type ReceiveLocalizationsActionSuccessPayload = ReducerActionSuccessPayload & {

    localizations: LocalizationsDataModel;
};