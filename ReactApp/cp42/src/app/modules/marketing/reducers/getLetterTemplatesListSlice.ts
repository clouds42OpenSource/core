import { TDataLightReturn } from 'app/api/types';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { MARKETING_SLICE_NAMES } from 'app/modules/marketing/constants';
import { TGetLetterTemplatesResponse } from 'app/api/endpoints/marketing/response';

const initialState: TDataLightReturn<TGetLetterTemplatesResponse[]> = {
    data: null,
    error: null,
    isLoading: false
};

export const getLetterTemplatesListSlice = createSlice({
    name: MARKETING_SLICE_NAMES.getLetterTemplatesList,
    initialState,
    reducers: {
        loading(state) {
            state.isLoading = true;
        },
        empty(state) {
            state.isLoading = false;
            state.data = null;
            state.error = null;
        },
        success(state, action: PayloadAction<TGetLetterTemplatesResponse[]>) {
            state.data = action.payload;
        },
        error(state, action: PayloadAction<string>) {
            state.error = action.payload;
        }
    }
});

export default getLetterTemplatesListSlice.reducer;