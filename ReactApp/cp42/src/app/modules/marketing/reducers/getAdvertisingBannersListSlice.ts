import { TDataLightReturn } from 'app/api/types';
import { TGetAdvertisingBannerResponse } from 'app/api/endpoints/marketing/response';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { MARKETING_SLICE_NAMES } from 'app/modules/marketing/constants';

const initialState: TDataLightReturn<TGetAdvertisingBannerResponse[]> = {
    data: null,
    error: null,
    isLoading: false
};

export const getAdvertisingBannersListSlice = createSlice({
    name: MARKETING_SLICE_NAMES.getAdvertisingBannersList,
    initialState,
    reducers: {
        loading(state) {
            state.isLoading = true;
        },
        empty(state) {
            state.isLoading = false;
            state.data = null;
            state.error = null;
        },
        success(state, action: PayloadAction<TGetAdvertisingBannerResponse[]>) {
            state.data = action.payload;
        },
        error(state, action: PayloadAction<string>) {
            state.error = action.payload;
        }
    }
});

export default getAdvertisingBannersListSlice.reducer;