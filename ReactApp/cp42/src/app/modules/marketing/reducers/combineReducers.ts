import { combineReducers } from 'redux';

import advertisingBannersListReducer from './getAdvertisingBannersListSlice';
import letterTemplatesListReducer from './getLetterTemplatesListSlice';

export const MarketingReducer = combineReducers({
    advertisingBannersListReducer,
    letterTemplatesListReducer
});