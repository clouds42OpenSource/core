export const MARKETING_SLICE_NAMES = {
    getAdvertisingBannersList: 'getAdvertisingBannersList',
    getLetterTemplatesList: 'getLetterTemplatesListSlice'
} as const;