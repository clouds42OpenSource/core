import { createReducerActions } from 'core/redux/functions';
import { WorkerAvailableTasksBagConstants } from 'app/modules/workerAvailableTasksBags/consts';
import { WorkerAvailableTasksBagsProcessId } from 'app/modules/workerAvailableTasksBags/WorkerAvailableTasksBagsProcessId';

/**
 * Все действия (Actions) для редюсера WorkerAvailableTasksBags
 */
export const WorkerAvailableTasksBagsActions = {

    /**
     * Действие для получения данных по взаимодействию воркера и задач
     */
    ReceiveWorkerAvailableTasksBagsData: createReducerActions(WorkerAvailableTasksBagConstants.reducerName, WorkerAvailableTasksBagsProcessId.ReceiveWorkerAvailableTasksBagsData),

    /**
     * Действие для получения приоритета доступной задачи воркера
     */
    ReceiveWorkerTaskBagPriority: createReducerActions(WorkerAvailableTasksBagConstants.reducerName, WorkerAvailableTasksBagsProcessId.ReceiveWorkerTaskBagPriority),

    /**
     * Действие для смены приоритета доступной задачи воркера
     */
    ChangeWorkerTaskBagPriority: createReducerActions(WorkerAvailableTasksBagConstants.reducerName, WorkerAvailableTasksBagsProcessId.ChangeWorkerTaskBagPriority)
};