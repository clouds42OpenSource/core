import {
    ReceiveWorkerAvailableTasksBagsActionFailedPayload,
    ReceiveWorkerAvailableTasksBagsActionStartPayload,
    ReceiveWorkerAvailableTasksBagsActionSuccessPayload
} from 'app/modules/workerAvailableTasksBags/store/reducers/receiveWorkerAvailableTasksBags/payloads';
import { ReceiveWorkerAvailableTasksBagsThunkParams } from 'app/modules/workerAvailableTasksBags/store/reducers/receiveWorkerAvailableTasksBags/params';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
export type TActionStartPayload = ReceiveWorkerAvailableTasksBagsActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
export type TActionSuccessPayload = ReceiveWorkerAvailableTasksBagsActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
export type TActionFailedPayload = ReceiveWorkerAvailableTasksBagsActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
export type TInputParams = ReceiveWorkerAvailableTasksBagsThunkParams;