import { WorkerAvailableTasksBagsActions } from 'app/modules/workerAvailableTasksBags/store/actions';
import { ReceiveWorkerTaskBagPriorityThunkParams } from 'app/modules/workerAvailableTasksBags/store/reducers/receiveWorkerTaskBagPriority/params';
import {
    ReceiveWorkerTaskBagPriorityActionFailedPayload,
    ReceiveWorkerTaskBagPriorityActionStartPayload,
    ReceiveWorkerTaskBagPriorityActionSuccessPayload
} from 'app/modules/workerAvailableTasksBags/store/reducers/receiveWorkerTaskBagPriority/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

const { ReceiveWorkerTaskBagPriority: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = WorkerAvailableTasksBagsActions;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = ReceiveWorkerTaskBagPriorityThunkParams;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, ReceiveWorkerTaskBagPriorityActionStartPayload, ReceiveWorkerTaskBagPriorityActionSuccessPayload, ReceiveWorkerTaskBagPriorityActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<ReceiveWorkerTaskBagPriorityActionStartPayload, ReceiveWorkerTaskBagPriorityActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<
    AppReduxStoreState, ReceiveWorkerTaskBagPriorityActionStartPayload, ReceiveWorkerTaskBagPriorityActionSuccessPayload, ReceiveWorkerTaskBagPriorityActionFailedPayload, TInputParams
>;

/**
 * Thunk для запроса данных по приоритету доступной задачи воркера
 */
export class ReceiveWorkerTaskBagPriorityThunk extends BaseReduxThunkObject<
    AppReduxStoreState, ReceiveWorkerTaskBagPriorityActionStartPayload, ReceiveWorkerTaskBagPriorityActionSuccessPayload, ReceiveWorkerTaskBagPriorityActionFailedPayload, TInputParams
> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new ReceiveWorkerTaskBagPriorityThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        const reducerState = args.getStore().WorkerAvailableTasksBagsReducerState;
        return {
            condition: !reducerState.hasSuccessFor.hasWorkerAvailableTasksBagsReceived || args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().WorkerAvailableTasksBagsReducerState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        try {
            const requestArgs = args.inputParams!;
            const workerAvailableTasksBagsApiProxy = InterlayerApiProxy.getWorkerAvailableTasksBagsApiProxy();
            const taskBagPriorityDataModel = await workerAvailableTasksBagsApiProxy.receiveWorkerTaskBagPriority(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, {
                taskId: requestArgs.taskId,
                workerId: requestArgs.workerId
            });

            args.success({
                commonInfo: taskBagPriorityDataModel
            });
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}