import { WorkerAvailableTasksBagsActions } from 'app/modules/workerAvailableTasksBags/store/actions';
import { ChangeWorkerTaskBagPriorityThunkParams } from 'app/modules/workerAvailableTasksBags/store/reducers/changeWorkerTaskBagPriority/params';
import {
    ChangeWorkerTaskBagPriorityActionFailedPayload,
    ChangeWorkerTaskBagPriorityActionStartPayload,
    ChangeWorkerTaskBagPriorityActionSuccessPayload
} from 'app/modules/workerAvailableTasksBags/store/reducers/changeWorkerTaskBagPriority/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

const { ChangeWorkerTaskBagPriority: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = WorkerAvailableTasksBagsActions;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = ChangeWorkerTaskBagPriorityThunkParams;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, ChangeWorkerTaskBagPriorityActionStartPayload, ChangeWorkerTaskBagPriorityActionSuccessPayload, ChangeWorkerTaskBagPriorityActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<ChangeWorkerTaskBagPriorityActionStartPayload, ChangeWorkerTaskBagPriorityActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, ChangeWorkerTaskBagPriorityActionStartPayload, ChangeWorkerTaskBagPriorityActionSuccessPayload, ChangeWorkerTaskBagPriorityActionFailedPayload, TInputParams>;

/**
 * Thunk для смены приоритета доступной задачи воркера
 */
export class ChangeWorkerTaskBagPriorityThunk extends BaseReduxThunkObject<
    AppReduxStoreState, ChangeWorkerTaskBagPriorityActionStartPayload, ChangeWorkerTaskBagPriorityActionSuccessPayload, ChangeWorkerTaskBagPriorityActionFailedPayload, TInputParams
> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new ChangeWorkerTaskBagPriorityThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        return {
            condition: args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().WorkerAvailableTasksBagsReducerState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        try {
            const requestArgs = args.inputParams!;
            const workerAvailableTasksBagsApiProxy = InterlayerApiProxy.getWorkerAvailableTasksBagsApiProxy();
            await workerAvailableTasksBagsApiProxy.changeWorkerTaskBagPriority(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, {
                taskId: requestArgs.taskId,
                coreWorkerId: requestArgs.workerId,
                priority: requestArgs.priority
            });

            args.success({
                commonInfo: {
                    priority: requestArgs.priority,
                    coreWorkerId: requestArgs.workerId,
                    taskId: requestArgs.taskId
                }
            });
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}