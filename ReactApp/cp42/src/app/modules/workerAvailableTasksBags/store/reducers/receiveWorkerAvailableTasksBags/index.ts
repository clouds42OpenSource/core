import { reducerActionState } from 'app/common/functions/reducerActionState';
import { WorkerAvailableTasksBagsActions } from 'app/modules/workerAvailableTasksBags/store/actions';
import { ReceiveWorkerAvailableTasksBagsActionAnyPayload, ReceiveWorkerAvailableTasksBagsActionSuccessPayload } from 'app/modules/workerAvailableTasksBags/store/reducers/receiveWorkerAvailableTasksBags/payloads';
import { workerAvailableTasksBagsState } from 'app/modules/workerAvailableTasksBags/store/reducers/workerAvailableTasksBagsState';
import { WorkerAvailableTasksBagsProcessId } from 'app/modules/workerAvailableTasksBags/WorkerAvailableTasksBagsProcessId';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения данных по взаимодействия воркера и задач
 * @param state состояние
 * @param action действие
 */
export const receiveWorkerAvailableTasksBagsDataReducer: TPartialReducerObject<workerAvailableTasksBagsState, ReceiveWorkerAvailableTasksBagsActionAnyPayload> =
    (state, action) => {
        const processId = WorkerAvailableTasksBagsProcessId.ReceiveWorkerAvailableTasksBagsData;
        const actions = WorkerAvailableTasksBagsActions.ReceiveWorkerAvailableTasksBagsData;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as ReceiveWorkerAvailableTasksBagsActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    workerAvailableTasksBags: {
                        ...state.workerAvailableTasksBags,
                        items: [...payload.workerAvailableTasksBags],
                        filter: { ...payload.filter },
                        metadata: { ...payload.metadata }
                    },
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasWorkerAvailableTasksBagsReceived: true
                    }
                });
            }
        });
    };