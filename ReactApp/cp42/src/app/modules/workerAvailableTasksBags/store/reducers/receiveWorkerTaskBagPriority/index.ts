import { reducerActionState } from 'app/common/functions/reducerActionState';
import { WorkerAvailableTasksBagsActions } from 'app/modules/workerAvailableTasksBags/store/actions';
import { ReceiveWorkerTaskBagPriorityActionAnyPayload, ReceiveWorkerTaskBagPriorityActionSuccessPayload } from 'app/modules/workerAvailableTasksBags/store/reducers/receiveWorkerTaskBagPriority/payloads';
import { workerAvailableTasksBagsState } from 'app/modules/workerAvailableTasksBags/store/reducers/workerAvailableTasksBagsState';
import { WorkerAvailableTasksBagsProcessId } from 'app/modules/workerAvailableTasksBags/WorkerAvailableTasksBagsProcessId';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения данных по приоритету доступной задачи воркера
 * @param state состояние
 * @param action действие
 */
export const receiveWorkerTaskBagPriorityDataReducer: TPartialReducerObject<workerAvailableTasksBagsState, ReceiveWorkerTaskBagPriorityActionAnyPayload> =
    (state, action) => {
        const processId = WorkerAvailableTasksBagsProcessId.ReceiveWorkerTaskBagPriority;
        const actions = WorkerAvailableTasksBagsActions.ReceiveWorkerTaskBagPriority;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as ReceiveWorkerTaskBagPriorityActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    workerTaskBagPriority: payload.commonInfo,
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasWorkerTaskBagPriorityReceived: true
                    }
                });
            }
        });
    };