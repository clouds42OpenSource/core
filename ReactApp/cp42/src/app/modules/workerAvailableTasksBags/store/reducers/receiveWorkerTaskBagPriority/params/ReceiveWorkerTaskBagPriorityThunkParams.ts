import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров на получение данных по приоритету доступной задачи воркера
 */
export type ReceiveWorkerTaskBagPriorityThunkParams = IForceThunkParam & {
    /**
     * ID задачи
     */
    taskId:string,

    /**
     * ID воркера
     */
    workerId: number
};