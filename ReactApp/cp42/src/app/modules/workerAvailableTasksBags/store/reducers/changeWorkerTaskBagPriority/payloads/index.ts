import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';
import { ChangeWorkerTaskBagPriorityDataModel } from 'app/web/InterlayerApiProxy/WorkerAvailableTasksBagsApiProxy/changeWorkerTaskBagPriority/data-models';

/**
 * Данные в редюсер при старте смены приоритета доступной задачи воркера
 */
export type ChangeWorkerTaskBagPriorityActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешной смене приоритета доступной задачи воркера
 */
export type ChangeWorkerTaskBagPriorityActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешной смене приоритета доступной задачи воркера
 */
export type ChangeWorkerTaskBagPriorityActionSuccessPayload = ReducerActionSuccessPayload & {

    /**
     * Модель смены приоритета доступной задачи воркера
     */
    commonInfo: ChangeWorkerTaskBagPriorityDataModel;
};

/**
 * Все возможные Action при смене приоритета доступной задачи воркера
 */
export type ChangeWorkerTaskBagPriorityActionAnyPayload =
    | ChangeWorkerTaskBagPriorityActionStartPayload
    | ChangeWorkerTaskBagPriorityActionFailedPayload
    | ChangeWorkerTaskBagPriorityActionSuccessPayload;