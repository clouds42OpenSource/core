import { getInitialMetadata, partialFillOf } from 'app/common/functions';
import { WorkerAvailableTasksBagConstants } from 'app/modules/workerAvailableTasksBags/consts';
import { changeWorkerTaskBagPriorityDataReducer } from 'app/modules/workerAvailableTasksBags/store/reducers/changeWorkerTaskBagPriority';
import { receiveWorkerAvailableTasksBagsDataReducer } from 'app/modules/workerAvailableTasksBags/store/reducers/receiveWorkerAvailableTasksBags';
import { receiveWorkerTaskBagPriorityDataReducer } from 'app/modules/workerAvailableTasksBags/store/reducers/receiveWorkerTaskBagPriority';
import { workerAvailableTasksBagsState } from 'app/modules/workerAvailableTasksBags/store/reducers/workerAvailableTasksBagsState';
import { WorkerTaskBagPriorityDataModel } from 'app/web/InterlayerApiProxy/WorkerAvailableTasksBagsApiProxy/receiveWorkerTaskBagPriority/data-models';
import { createReducer, initReducerState } from 'core/redux/functions';

/**
 * Начальное состояние редьюсера
 */
const initialState = initReducerState<workerAvailableTasksBagsState>(
    WorkerAvailableTasksBagConstants.reducerName,
    {
        workerAvailableTasksBags: {
            items: [],
            metadata: getInitialMetadata(),
            filter: {
                workerId: null,
                taskPriority: null,
                taskId: null
            }
        },
        workerTaskBagPriority: partialFillOf<WorkerTaskBagPriorityDataModel>(),
        hasSuccessFor: {
            hasWorkerAvailableTasksBagsReceived: false,
            hasWorkerTaskBagPriorityReceived: false
        }
    }
);

const partialReducers = [
    receiveWorkerAvailableTasksBagsDataReducer,
    receiveWorkerTaskBagPriorityDataReducer,
    changeWorkerTaskBagPriorityDataReducer
];

export const workerAvailableTasksBagsReducer = createReducer(initialState, partialReducers);