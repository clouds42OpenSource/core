import { ReceiveWorkerAvailableTasksBagsParams } from 'app/web/InterlayerApiProxy/WorkerAvailableTasksBagsApiProxy/receiveWorkerAvailableTasksBagsData/input-params';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров на получение данных по взаимодействию воркера и задач
 */
export type ReceiveWorkerAvailableTasksBagsThunkParams = IForceThunkParam & ReceiveWorkerAvailableTasksBagsParams;