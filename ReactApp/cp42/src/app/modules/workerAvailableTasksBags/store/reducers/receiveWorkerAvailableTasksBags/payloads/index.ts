import { MetadataModel } from 'app/web/common/data-models';
import { WorkerAvailableTasksBagItemDataModel, WorkerAvailableTasksBagsFilterDataModel } from 'app/web/InterlayerApiProxy/WorkerAvailableTasksBagsApiProxy/receiveWorkerAvailableTasksBagsData/data-models';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения записей по взаимодействию воркера и задач
 */
export type ReceiveWorkerAvailableTasksBagsActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения записей по взаимодействию воркера и задач
 */
export type ReceiveWorkerAvailableTasksBagsActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения записей по взаимодействию воркера и задач
 */
export type ReceiveWorkerAvailableTasksBagsActionSuccessPayload = ReducerActionSuccessPayload & {

    /**
     * Массив записей по взаимодействию воркера и задач
     */
    workerAvailableTasksBags: WorkerAvailableTasksBagItemDataModel[];

    /**
     * Текущий фильтр для получения записей
     */
    filter: WorkerAvailableTasksBagsFilterDataModel;

    /**
     * Данные о странице для текущих записей
     */
    metadata: MetadataModel;
};

/**
 * Все возможные Action при получении списка записей по взаимодействию воркера и задач
 */
export type ReceiveWorkerAvailableTasksBagsActionAnyPayload =
    | ReceiveWorkerAvailableTasksBagsActionStartPayload
    | ReceiveWorkerAvailableTasksBagsActionFailedPayload
    | ReceiveWorkerAvailableTasksBagsActionSuccessPayload;