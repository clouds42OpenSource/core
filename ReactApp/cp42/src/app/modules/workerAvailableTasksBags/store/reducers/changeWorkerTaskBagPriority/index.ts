import { reducerActionState } from 'app/common/functions/reducerActionState';
import { WorkerAvailableTasksBagsActions } from 'app/modules/workerAvailableTasksBags/store/actions';
import { ChangeWorkerTaskBagPriorityActionAnyPayload, ChangeWorkerTaskBagPriorityActionSuccessPayload } from 'app/modules/workerAvailableTasksBags/store/reducers/changeWorkerTaskBagPriority/payloads';
import { workerAvailableTasksBagsState } from 'app/modules/workerAvailableTasksBags/store/reducers/workerAvailableTasksBagsState';
import { WorkerAvailableTasksBagsProcessId } from 'app/modules/workerAvailableTasksBags/WorkerAvailableTasksBagsProcessId';
import { reducerStateProcessFail, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для смены приоритета доступной задачи воркера
 * @param state состояние
 * @param action действие
 */
export const changeWorkerTaskBagPriorityDataReducer: TPartialReducerObject<workerAvailableTasksBagsState, ChangeWorkerTaskBagPriorityActionAnyPayload> =
    (state, action) => {
        const processId = WorkerAvailableTasksBagsProcessId.ChangeWorkerTaskBagPriority;
        const actions = WorkerAvailableTasksBagsActions.ChangeWorkerTaskBagPriority;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as ChangeWorkerTaskBagPriorityActionSuccessPayload;

                const foundWorkerTaskBag = state.workerAvailableTasksBags.items.findIndex(item => item.taskId === payload.commonInfo.taskId &&
                    item.workerId === payload.commonInfo.coreWorkerId);

                if (Number.isNaN(foundWorkerTaskBag)) {
                    return reducerStateProcessFail(state, processId, new Error(`Обновляемая запись связи воркера и задачи c воркером ID:(${ payload.commonInfo.coreWorkerId }) и с задачей ID:(${ payload.commonInfo.taskId }) не найдена.`));
                }

                const items = [...state.workerAvailableTasksBags.items];
                items[foundWorkerTaskBag] = {
                    ...items[foundWorkerTaskBag],
                    taskPriority: payload.commonInfo.priority
                };

                return reducerStateProcessSuccess(state, processId, {
                    workerAvailableTasksBags: {
                        ...state.workerAvailableTasksBags,
                        items
                    }
                });
            }
        });
    };