import { WorkerAvailableTasksBagsProcessId } from 'app/modules/workerAvailableTasksBags/WorkerAvailableTasksBagsProcessId';
import { WorkerAvailableTasksBagItemDataModel, WorkerAvailableTasksBagsFilterDataModel } from 'app/web/InterlayerApiProxy/WorkerAvailableTasksBagsApiProxy/receiveWorkerAvailableTasksBagsData/data-models';
import { WorkerTaskBagPriorityDataModel } from 'app/web/InterlayerApiProxy/WorkerAvailableTasksBagsApiProxy/receiveWorkerTaskBagPriority/data-models';
import { MetadataModel } from 'app/web/common/data-models';
import { IReducerState } from 'core/redux/interfaces';

/**
 * Состояние редюсера для взаимодействия воркера и задач
 */
export type workerAvailableTasksBagsState = IReducerState<WorkerAvailableTasksBagsProcessId> & {

    /**
     * Взаимодействия воркера и задач
     */
    workerAvailableTasksBags: {

        /**
         * Массив записей по взаимодействию воркера и задач
         */
        items: WorkerAvailableTasksBagItemDataModel[],

        /**
         * Текущий фильтр для получения записей
         */
        filter: WorkerAvailableTasksBagsFilterDataModel;

        /**
         * Данные о странице для текущих записей
         */
        metadata: MetadataModel;
    },

    /**
     * Модель приоритета доступной задачи воркера
     */
    workerTaskBagPriority: WorkerTaskBagPriorityDataModel,

    /**
     * содержит ярлыки для фиксации загрузок данных, была ли загрузка или ещё нет
     */
    hasSuccessFor: {
        /**
         * Если true, то данные по взаимодействию воркера и задач уже получены
         */
        hasWorkerAvailableTasksBagsReceived: boolean,

        /**
         * Если true, то данные по приоритету доступной задачи воркера уже получены
         */
        hasWorkerTaskBagPriorityReceived: boolean
    };
};