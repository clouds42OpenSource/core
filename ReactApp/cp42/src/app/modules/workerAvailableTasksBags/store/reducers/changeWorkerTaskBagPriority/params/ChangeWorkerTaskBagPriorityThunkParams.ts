import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для смены приоритета доступной задачи воркера
 */
export type ChangeWorkerTaskBagPriorityThunkParams = IForceThunkParam & {
    /**
     * ID задачи
     */
    taskId: string,

    /**
     * ID воркера
     */
    workerId: number

    /**
     * Приоритет
     */
    priority: number
};