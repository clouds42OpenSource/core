import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';
import { WorkerTaskBagPriorityDataModel } from 'app/web/InterlayerApiProxy/WorkerAvailableTasksBagsApiProxy/receiveWorkerTaskBagPriority/data-models';

/**
 * Данные в редюсер при старте выполнения получения данных по приоритету доступной задачи воркера
 */
export type ReceiveWorkerTaskBagPriorityActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения данных по приоритету доступной задачи воркера
 */
export type ReceiveWorkerTaskBagPriorityActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения данных по приоритету доступной задачи воркера
 */
export type ReceiveWorkerTaskBagPriorityActionSuccessPayload = ReducerActionSuccessPayload & {

    /**
     * Модель приоритета доступной задачи воркера
     */
    commonInfo: WorkerTaskBagPriorityDataModel;
};

/**
 * Все возможные Action при получении данных по приоритету доступной задачи воркера
 */
export type ReceiveWorkerTaskBagPriorityActionAnyPayload =
    | ReceiveWorkerTaskBagPriorityActionStartPayload
    | ReceiveWorkerTaskBagPriorityActionFailedPayload
    | ReceiveWorkerTaskBagPriorityActionSuccessPayload;