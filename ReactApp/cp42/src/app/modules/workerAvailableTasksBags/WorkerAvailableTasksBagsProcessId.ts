/**
 * Процесы по взаимодействию воркера и задач
 */
export enum WorkerAvailableTasksBagsProcessId {

    /**
     * Процесс для получения данных по взаимодействию воркера и задач
     */
    ReceiveWorkerAvailableTasksBagsData = 'RECEIVE_WORKER_AVAILABLE_TASKS_BAGS_DATA',

    /**
     * Процесс для получения приоритета доступной задачи воркера
     */
    ReceiveWorkerTaskBagPriority = 'RECEIVE_WORKER_TASK_BAG_PRIORITY',

    /**
     * Процесс для смены приоритета доступной задачи воркера
     */
    ChangeWorkerTaskBagPriority = 'CHANGE_WORKER_TASK_BAG_PRIORITY'
}