/**
 * Константы для модуля TaskInQueueItemCard
 */
export const TaskInQueueItemCardConstants = {
    /**
     * Название редюсера для модуля TaskInQueueItemCard
     */
    reducerName: 'TASK_IN_QUEUE_ITEM_CARD'
};