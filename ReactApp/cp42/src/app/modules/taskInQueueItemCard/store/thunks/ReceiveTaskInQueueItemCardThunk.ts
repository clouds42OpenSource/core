import { TaskInQueueItemCardActions } from 'app/modules/taskInQueueItemCard/store/actions';
import {
    TaskInQueueCardActionFailedPayload,
    TaskInQueueCardActionStartPayload,
    TaskInQueueCardActionSuccessPayload,
    TaskInQueueCardInputParams,
    TaskInQueueCardThunkStartActionParams,
    TaskInQueueCardThunkStartActionResult,
    TaskInQueueCardThunkStartExecutionParams
} from 'app/modules/taskInQueueItemCard/store/thunks/types/ReceiveTaskInQueueItemCardThunkTypes';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { RequestKind } from 'core/requestSender/enums';

const { ReceiveTaskInQueueItemCard: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = TaskInQueueItemCardActions;

/**
 * Thunk для запроса списка записей логирования запуска баз и открытия RDP
 */
export class ReceiveTaskInQueueItemCardThunk extends BaseReduxThunkObject<AppReduxStoreState, TaskInQueueCardActionStartPayload, TaskInQueueCardActionSuccessPayload, TaskInQueueCardActionFailedPayload, TaskInQueueCardInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TaskInQueueCardInputParams) {
        return new ReceiveTaskInQueueItemCardThunk().execute(args);
    }

    protected get startActionValue(): string { return START_ACTION; }

    protected get successActionValue(): string { return SUCCESS_ACTION; }

    protected get failedActionValue(): string { return FAILED_ACTION; }

    protected getStartActionArguments(args: TaskInQueueCardThunkStartActionParams): TaskInQueueCardThunkStartActionResult {
        const taskInQueueItemCardState = args.getStore().TaskInQueueItemCardState;
        return {
            condition: !taskInQueueItemCardState.hasSuccessFor.hasTaskInQueueItemCardReceived || args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().TaskInQueueItemCardState,
        };
    }

    protected async startExecution(args: TaskInQueueCardThunkStartExecutionParams): Promise<void> {
        const requestArgs = args.inputParams!;
        const taskInQueueItemCardApiProxy = InterlayerApiProxy.getTaskInQueueItemCardApiProxy();

        try {
            const accountCardResponse = await taskInQueueItemCardApiProxy.receiveTaskInQueueItemCard(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, {
                taskInQueueItemId: requestArgs.taskInQueueItemId
            });

            args.success({
                taskInQueueItemDetails: accountCardResponse
            });
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}