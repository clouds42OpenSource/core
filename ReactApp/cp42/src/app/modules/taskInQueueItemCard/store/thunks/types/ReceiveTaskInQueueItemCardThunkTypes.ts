import { ReceiveTaskInQueueItemCardThunkParams } from 'app/modules/taskInQueueItemCard/store/reducers/receiveTaskInQueueItemCardReducer/params';
import {
    ReceiveTaskInQueueItemCardActionFailedPayload,
    ReceiveTaskInQueueItemCardActionStartPayload,
    ReceiveTaskInQueueItemCardActionSuccessPayload
} from 'app/modules/taskInQueueItemCard/store/reducers/receiveTaskInQueueItemCardReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
export type TaskInQueueCardActionStartPayload = ReceiveTaskInQueueItemCardActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
export type TaskInQueueCardActionSuccessPayload = ReceiveTaskInQueueItemCardActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
export type TaskInQueueCardActionFailedPayload = ReceiveTaskInQueueItemCardActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
export type TaskInQueueCardInputParams = ReceiveTaskInQueueItemCardThunkParams;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
export type TaskInQueueCardThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TaskInQueueCardActionStartPayload, TaskInQueueCardActionSuccessPayload, TaskInQueueCardActionFailedPayload, TaskInQueueCardInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
export type TaskInQueueCardThunkStartActionResult = IThunkStartActionResult<TaskInQueueCardActionStartPayload, TaskInQueueCardActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
export type TaskInQueueCardThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TaskInQueueCardActionStartPayload, TaskInQueueCardActionSuccessPayload, TaskInQueueCardActionFailedPayload, TaskInQueueCardInputParams>;