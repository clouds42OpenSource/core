import { AddTaskToQueueCardThunkParams } from 'app/modules/taskInQueueItemCard/store/reducers/addTaskToQueueReducer/params';
import { AddTaskToQueueActionFailedPayload, AddTaskToQueueActionStartPayload, AddTaskToQueueActionSuccessPayload } from 'app/modules/taskInQueueItemCard/store/reducers/addTaskToQueueReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
export type AddTaskToQueueThActionStartPayload = AddTaskToQueueActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
export type AddTaskToQueueThActionSuccessPayload = AddTaskToQueueActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
export type AddTaskToQueueThActionFailedPayload = AddTaskToQueueActionFailedPayload;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
export type AddTaskToQueueThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, AddTaskToQueueThActionStartPayload, AddTaskToQueueThActionSuccessPayload, AddTaskToQueueThActionFailedPayload, AddTaskToQueueCardThunkParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
export type AddTaskToQueueThunkStartActionResult = IThunkStartActionResult<AddTaskToQueueThActionStartPayload, AddTaskToQueueThActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
export type AddTaskToQueueThunkStartExecutionParams =
    IThunkStartExecutionParams<AppReduxStoreState, AddTaskToQueueThActionStartPayload, AddTaskToQueueThActionSuccessPayload, AddTaskToQueueThActionFailedPayload, AddTaskToQueueCardThunkParams>;