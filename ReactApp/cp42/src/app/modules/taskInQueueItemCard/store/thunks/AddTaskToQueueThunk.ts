import { TaskInQueueItemCardActions } from 'app/modules/taskInQueueItemCard/store/actions';
import { AddTaskToQueueCardThunkParams } from 'app/modules/taskInQueueItemCard/store/reducers/addTaskToQueueReducer/params';
import {
    AddTaskToQueueThActionFailedPayload,
    AddTaskToQueueThActionStartPayload,
    AddTaskToQueueThActionSuccessPayload,
    AddTaskToQueueThunkStartActionParams,
    AddTaskToQueueThunkStartActionResult,
    AddTaskToQueueThunkStartExecutionParams
} from 'app/modules/taskInQueueItemCard/store/thunks/types/AddTaskToQueueThunkTypes';
import { ReceiveTasksInQueueDataThunk } from 'app/modules/tasksTable/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { DateUtility } from 'app/utils';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { RequestKind } from 'core/requestSender/enums';

const { AddTaskToQueue: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = TaskInQueueItemCardActions;

/**
 * Thunk для добавления новой задачи в очередь
 */
export class AddTaskToQueueThunk extends BaseReduxThunkObject<AppReduxStoreState, AddTaskToQueueThActionStartPayload, AddTaskToQueueThActionSuccessPayload, AddTaskToQueueThActionFailedPayload, AddTaskToQueueCardThunkParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: AddTaskToQueueCardThunkParams) {
        return new AddTaskToQueueThunk().execute(args);
    }

    protected get startActionValue(): string { return START_ACTION; }

    protected get successActionValue(): string { return SUCCESS_ACTION; }

    protected get failedActionValue(): string { return FAILED_ACTION; }

    protected getStartActionArguments(args: AddTaskToQueueThunkStartActionParams): AddTaskToQueueThunkStartActionResult {
        return {
            condition: args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().TaskInQueueItemCardState,
        };
    }

    protected async startExecution(args: AddTaskToQueueThunkStartExecutionParams): Promise<void> {
        const requestArgs = args.inputParams!;
        const taskInQueueItemCardApiProxy = InterlayerApiProxy.getTaskInQueueItemCardApiProxy();

        try {
            await taskInQueueItemCardApiProxy.addTaskToQueue(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, {
                TaskParams: requestArgs.taskParams,
                CloudTaskId: requestArgs.taskId,
                Comment: requestArgs.comment
            });

            args.success(requestArgs);
            args.thunkDispatch(new ReceiveTasksInQueueDataThunk(), {
                filter: {
                    searchString: null,
                    status: null,
                    workerId: null,
                    taskId: null,
                    periodTo: DateUtility.getTomorrow(new Date()),
                    periodFrom: DateUtility.setDayTimeToMidnight(DateUtility.getSomeMonthAgoDate(3))
                },
                force: true,
                pageNumber: 1,
            });
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}