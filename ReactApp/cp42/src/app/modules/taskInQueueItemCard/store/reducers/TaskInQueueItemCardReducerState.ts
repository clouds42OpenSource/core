import { IReducerState } from 'core/redux/interfaces';
import { TaskInQueueItemCardProcessId } from 'app/modules/taskInQueueItemCard/TaskInQueueItemCardProcessId';
import { TaskInQueueItemCardDataModel } from 'app/web/InterlayerApiProxy/TaskInQueueItemCardApiProxy/receiveTaskInQueueItemCard/data-models';
import { AddTaskToQueueCardParams } from 'app/modules/taskInQueueItemCard/store/reducers/addTaskToQueueReducer/params';

export type TaskInQueueItemCardReducerState = IReducerState<TaskInQueueItemCardProcessId> & {

    /**
     * Информация о задаче из очереди задач воркера
     */
    details: TaskInQueueItemCardDataModel;

    /**
     * Параметры добавления новой задачи в очередь
     */
    addTaskToQueueCard: AddTaskToQueueCardParams;

    /**
     * содержит ярлыки для фиксации загрузок данных, была ли загрузка или ещё нет
     */
    hasSuccessFor: {
        /**
         * Если true, то карточка нформации о задаче из очереди задач воркера
         */
        hasTaskInQueueItemCardReceived: boolean
    };
};