import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TaskInQueueItemCardActions } from 'app/modules/taskInQueueItemCard/store/actions';
import { ReceiveTaskInQueueItemCardActionAnyPayload, ReceiveTaskInQueueItemCardActionSuccessPayload } from 'app/modules/taskInQueueItemCard/store/reducers/receiveTaskInQueueItemCardReducer/payloads';
import { TaskInQueueItemCardReducerState } from 'app/modules/taskInQueueItemCard/store/reducers/TaskInQueueItemCardReducerState';
import { TaskInQueueItemCardProcessId } from 'app/modules/taskInQueueItemCard/TaskInQueueItemCardProcessId';
import { TaskInQueueItemCardDataModel } from 'app/web/InterlayerApiProxy/TaskInQueueItemCardApiProxy/receiveTaskInQueueItemCard/data-models';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения карточки информации о задаче из очереди
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const receiveTaskInQueueItemCardReducer: TPartialReducerObject<TaskInQueueItemCardReducerState, ReceiveTaskInQueueItemCardActionAnyPayload> =
    (state, action) => {
        const processId = TaskInQueueItemCardProcessId.ReceiveTaskInQueueItemCard;
        const actions = TaskInQueueItemCardActions.ReceiveTaskInQueueItemCard;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onStartProcessState: () => {
                return reducerStateProcessStart(state, processId, {
                    details: {} as TaskInQueueItemCardDataModel,
                    addTaskToQueueCard: { ...state.addTaskToQueueCard },
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasTaskInQueueItemCardReceived: false
                    }
                });
            },
            onSuccessProcessState: () => {
                const payload = action.payload as ReceiveTaskInQueueItemCardActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    details: payload.taskInQueueItemDetails,
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasTaskInQueueItemCardReceived: true
                    }
                });
            }
        });
    };