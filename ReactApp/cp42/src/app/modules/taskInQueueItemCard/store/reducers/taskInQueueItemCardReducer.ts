import { createReducer, initReducerState } from 'core/redux/functions';
import { TaskInQueueItemCardReducerState } from 'app/modules/taskInQueueItemCard/store/reducers/TaskInQueueItemCardReducerState';
import { TaskInQueueItemCardConstants } from 'app/modules/taskInQueueItemCard/constants';
import { receiveTaskInQueueItemCardReducer } from 'app/modules/taskInQueueItemCard/store/reducers/receiveTaskInQueueItemCardReducer';
import { addTaskToQueueReducer } from 'app/modules/taskInQueueItemCard/store/reducers/addTaskToQueueReducer';
import { TaskInQueueItemCardDataModel } from 'app/web/InterlayerApiProxy/TaskInQueueItemCardApiProxy/receiveTaskInQueueItemCard/data-models';
import { partialFillOf } from 'app/common/functions';

/**
 * Начальное состояние редюсера TaskInQueueItemCardReducer
 */
const initialState = initReducerState<TaskInQueueItemCardReducerState>(
    TaskInQueueItemCardConstants.reducerName,
    {
        details: partialFillOf<TaskInQueueItemCardDataModel>(),
        addTaskToQueueCard: {
            comment: null,
            taskId: '',
            taskParams: null
        },
        hasSuccessFor: {
            hasTaskInQueueItemCardReceived: false
        }
    }
);

/**
 * Объединённые части редюсера для всего состояния TaskInQueueItemCardReducer
 */
const partialReducers = [
    receiveTaskInQueueItemCardReducer,
    addTaskToQueueReducer
];

/**
 * Редюсер TaskInQueueItemCardReducer
 */
export const taskInQueueItemCardReducer = createReducer(initialState, partialReducers);