import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Параметры добавления задачи в очередь
 */
export type AddTaskToQueueCardParams = {
    /**
     * ID задачи
     */
    taskId: string,

    /**
     * Параметры задачи
     */
    taskParams: string|null

    /**
     * Коментарий к задаче
     */
    comment: string|null
};

/**
 * Модель на запрос добавления задачи в очередь
 */
export type AddTaskToQueueCardThunkParams = AddTaskToQueueCardParams & IForceThunkParam;