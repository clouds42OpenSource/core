import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';
import { TaskInQueueItemCardDataModel } from 'app/web/InterlayerApiProxy/TaskInQueueItemCardApiProxy/receiveTaskInQueueItemCard/data-models';

/**
 * Данные в редюсер при старте получения карточки информации о задаче из очереди задач воркера
 */
export type ReceiveTaskInQueueItemCardActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при ошибки получения карточки информации о задаче из очереди задач воркера
 */
export type ReceiveTaskInQueueItemCardActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном получении карточки информации о задаче из очереди задач воркера
 */
export type ReceiveTaskInQueueItemCardActionSuccessPayload = ReducerActionSuccessPayload & {
    taskInQueueItemDetails: TaskInQueueItemCardDataModel;
};

/**
 * Все возможные Action при получении карточки информации о задаче из очереди задач воркера
 */
export type ReceiveTaskInQueueItemCardActionAnyPayload =
    | ReceiveTaskInQueueItemCardActionStartPayload
    | ReceiveTaskInQueueItemCardActionSuccessPayload
    | ReceiveTaskInQueueItemCardActionFailedPayload;