import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель на запрос получения карточки информации о задаче из очереди задач воркера
 */
export type ReceiveTaskInQueueItemCardThunkParams = IForceThunkParam & {
    /**
     * ID задачи из очереди задач воркера
     */
    taskInQueueItemId: string;
};