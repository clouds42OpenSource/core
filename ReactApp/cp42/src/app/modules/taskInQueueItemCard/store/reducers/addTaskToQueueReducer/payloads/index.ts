import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';
import { AddTaskToQueueCardParams } from 'app/modules/taskInQueueItemCard/store/reducers/addTaskToQueueReducer/params';

/**
 * Данные в редюсер при старте добавления новой задачи в очередь
 */
export type AddTaskToQueueActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при ошибке добавления новой задачи в очередь
 */
export type AddTaskToQueueActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном добавлении новой задачи в очередь
 */
export type AddTaskToQueueActionSuccessPayload = ReducerActionSuccessPayload & AddTaskToQueueCardParams;

/**
 * Все возможные Action при добавлении новой задачи в очередь
 */
export type AddTaskToQueueActionAnyPayload =
    | AddTaskToQueueActionStartPayload
    | AddTaskToQueueActionSuccessPayload
    | AddTaskToQueueActionFailedPayload;