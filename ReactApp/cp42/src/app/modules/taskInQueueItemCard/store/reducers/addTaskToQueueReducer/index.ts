import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TaskInQueueItemCardActions } from 'app/modules/taskInQueueItemCard/store/actions';
import { AddTaskToQueueActionAnyPayload, AddTaskToQueueActionSuccessPayload } from 'app/modules/taskInQueueItemCard/store/reducers/addTaskToQueueReducer/payloads';
import { TaskInQueueItemCardReducerState } from 'app/modules/taskInQueueItemCard/store/reducers/TaskInQueueItemCardReducerState';
import { TaskInQueueItemCardProcessId } from 'app/modules/taskInQueueItemCard/TaskInQueueItemCardProcessId';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для добавления задачи в очередь
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const addTaskToQueueReducer: TPartialReducerObject<TaskInQueueItemCardReducerState, AddTaskToQueueActionAnyPayload> =
    (state, action) => {
        const processId = TaskInQueueItemCardProcessId.AddTaskToQueue;
        const actions = TaskInQueueItemCardActions.AddTaskToQueue;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as AddTaskToQueueActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    addTaskToQueueCard: payload,
                    details: { ...state.details }
                });
            }
        });
    };