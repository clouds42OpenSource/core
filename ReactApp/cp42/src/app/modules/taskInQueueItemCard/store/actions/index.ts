import { createReducerActions } from 'core/redux/functions';
import { TaskInQueueItemCardProcessId } from 'app/modules/taskInQueueItemCard/TaskInQueueItemCardProcessId';
import { TaskInQueueItemCardConstants } from 'app/modules/taskInQueueItemCard/constants';

/**
 * Все доступные actions дле редюсера TaskInQueueItemCard
 */
export const TaskInQueueItemCardActions = {

    /**
     * Набор action на получение информации о задаче из очереди задач воркера
     */
    ReceiveTaskInQueueItemCard: createReducerActions(TaskInQueueItemCardConstants.reducerName, TaskInQueueItemCardProcessId.ReceiveTaskInQueueItemCard),

    /**
     * Набор action на добавление новой задачи в очередь
     */
    AddTaskToQueue: createReducerActions(TaskInQueueItemCardConstants.reducerName, TaskInQueueItemCardProcessId.AddTaskToQueue)
};