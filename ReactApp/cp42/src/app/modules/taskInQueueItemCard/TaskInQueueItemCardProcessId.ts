/**
 * Процесы DatabaseCard
 */
export enum TaskInQueueItemCardProcessId {
    /**
     * Процесс на получение информации о задаче из очереди задач воркера
     */
    ReceiveTaskInQueueItemCard = 'RECEIVE_TASK_IN_QUEUE_ITEM_CARD',

    /**
     * Процесс для добавления задачи в очередь
     */
    AddTaskToQueue = 'ADD_TASK_TO_QUEUE'
}