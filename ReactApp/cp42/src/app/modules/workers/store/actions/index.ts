import { createReducerActions } from 'core/redux/functions';
import { TasksConstants } from 'app/modules/tasksTable/consts';
import { WorkersProcessId } from 'app/modules/workers/WorkersProcessId';

/**
 * Все действия (Actions) для редюсера Workers
 */
export const WorkersActions = {
    /**
     * Действие для получения данных по воркерам в системе
     */
    ReceiveWorkersInSystemData: createReducerActions(TasksConstants.reducerName, WorkersProcessId.ReceiveWorkersInSystemData)
};