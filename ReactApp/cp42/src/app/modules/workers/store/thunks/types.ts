import { ReceiveWorkersInSystemDataActionFailedPayload, ReceiveWorkersInSystemDataActionStartPayload, ReceiveWorkersInSystemDataActionSuccessPayload } from 'app/modules/workers/store/reducers/receiveWorkersInSystemData/payloads';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
export type TActionStartPayload = ReceiveWorkersInSystemDataActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
export type TActionSuccessPayload = ReceiveWorkersInSystemDataActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
export type TActionFailedPayload = ReceiveWorkersInSystemDataActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
export type TInputParams = IForceThunkParam;