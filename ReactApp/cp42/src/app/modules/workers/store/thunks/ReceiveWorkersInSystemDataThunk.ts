import { WorkersActions } from 'app/modules/workers/store/actions';
import { TActionFailedPayload, TActionStartPayload, TActionSuccessPayload, TInputParams } from 'app/modules/workers/store/thunks/types';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

const { ReceiveWorkersInSystemData: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = WorkersActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для запроса списка записей по очереди задач воркеров
 */
export class ReceiveWorkersInSystemDataThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new ReceiveWorkersInSystemDataThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        const tasksState = args.getStore().WorkersState;
        return {
            condition: !tasksState.hasSuccessFor.hasWorkersDataReceived || args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().WorkersState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        try {
            const workersApi = InterlayerApiProxy.getWorkersApi();
            const workersResponse = await workersApi.receiveWorkers(RequestKind.SEND_BY_ROBOT);
            args.success({
                workerItemsData: workersResponse
            });
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}