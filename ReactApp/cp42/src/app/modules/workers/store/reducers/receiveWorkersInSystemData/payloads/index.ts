import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';
import { WorkerDescriptionDataModel } from 'app/web/InterlayerApiProxy/WorkersApiProxy/receiveWorkersData/data-models';

/**
 * Данные в редюсер при старте выполнения получения записей по очереди задач воркеров
 */
export type ReceiveWorkersInSystemDataActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения записей по очереди задач воркеров
 */
export type ReceiveWorkersInSystemDataActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения записей по очереди задач воркеров
 */
export type ReceiveWorkersInSystemDataActionSuccessPayload = ReducerActionSuccessPayload & {

    /**
     * Массив записей
     */
    workerItemsData: WorkerDescriptionDataModel[];
};

/**
 * Все возможные Action при получении списка воркеров в системе
 */
export type ReceiveWorkersInSystemDataActionAnyPayload =
    | ReceiveWorkersInSystemDataActionStartPayload
    | ReceiveWorkersInSystemDataActionSuccessPayload
    | ReceiveWorkersInSystemDataActionFailedPayload;