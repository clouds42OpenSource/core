import { reducerActionState } from 'app/common/functions/reducerActionState';
import { WorkersActions } from 'app/modules/workers/store/actions';
import { ReceiveWorkersInSystemDataActionAnyPayload, ReceiveWorkersInSystemDataActionSuccessPayload } from 'app/modules/workers/store/reducers/receiveWorkersInSystemData/payloads';
import { WorkersReducerState } from 'app/modules/workers/store/reducers/WorkersReducerState';
import { WorkersProcessId } from 'app/modules/workers/WorkersProcessId';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

export const receiveWorkersInSystemDataReducer: TPartialReducerObject<WorkersReducerState, ReceiveWorkersInSystemDataActionAnyPayload> =
    (state, action) => {
        const processId = WorkersProcessId.ReceiveWorkersInSystemData;
        const actions = WorkersActions.ReceiveWorkersInSystemData;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as ReceiveWorkersInSystemDataActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    availableWorkers: {
                        items: [...payload.workerItemsData]
                    },
                    hasSuccessFor: {
                        hasWorkersDataReceived: true
                    }
                });
            }
        });
    };