import { IReducerState } from 'core/redux/interfaces';
import { WorkersProcessId } from 'app/modules/workers/WorkersProcessId';
import { WorkerDescriptionDataModel } from 'app/web/InterlayerApiProxy/WorkersApiProxy/receiveWorkersData/data-models';

/**
 * Состояние редюсера для воркеров
 */
export type WorkersReducerState = IReducerState<WorkersProcessId> & {
    /**
     * Доступные воркеры
     */
    availableWorkers: {
        /**
         * Массив записей
         */
        items: WorkerDescriptionDataModel[]
    }

    /**
     * содержит ярлыки для фиксации загрузок данных, была ли загрузка или ещё нет
     */
    hasSuccessFor: {
        /**
         * Если true, то данные по воркерам уже получены
         */
        hasWorkersDataReceived: boolean
    };
};