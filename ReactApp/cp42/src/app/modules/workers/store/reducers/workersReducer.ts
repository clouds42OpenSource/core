import { createReducer, initReducerState } from 'core/redux/functions';
import { receiveWorkersInSystemDataReducer } from 'app/modules/workers/store/reducers/receiveWorkersInSystemData';
import { WorkersReducerState } from 'app/modules/workers/store/reducers/WorkersReducerState';
import { WorkersConstants } from 'app/modules/workers/consts';

/**
 * Начальное состояние редьюсера
 */
const initialState = initReducerState<WorkersReducerState>(
    WorkersConstants.reducerName,
    {
        availableWorkers: {
            items: []
        },
        hasSuccessFor: {
            hasWorkersDataReceived: false
        }
    }
);

const partialReducers = [
    receiveWorkersInSystemDataReducer
];

export const workersReducer = createReducer(initialState, partialReducers);