/**
 * Процесы для воркеров
 */
export enum WorkersProcessId {
    /**
     * Процесс для получения списка воркеров в системе
     */
    ReceiveWorkersInSystemData = 'RECEIVE_WORKER_IN_SYSTEM_DATA'
}