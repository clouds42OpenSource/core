/**
 * Процесы ServiceAccounts
 */
export enum ServiceAccountsProcessId {
    /**
     * Процесс получить служебные аккаунты
     */
    ReceiveServiceAccounts = 'RECEIVE_SERVICE_ACCOUNTS',

    /**
     * Процесс добавить служебный аккаунт
     */
    InsertServiceAccount = 'INSERT_SERVICE_ACCOUNT',

    /**
     * Процесс удалить служебный аккаунт
     */
    DeleteServiceAccount = 'DELETE_SERVICE_ACCOUNT'
}