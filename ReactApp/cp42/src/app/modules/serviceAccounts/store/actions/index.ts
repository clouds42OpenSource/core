import { ServiceAccountsConsts } from 'app/modules/serviceAccounts/constants';
import { ServiceAccountsProcessId } from 'app/modules/serviceAccounts/ServiceAccountsProcessId';
import { createReducerActions } from 'core/redux/functions';

/**
 * Все доступные actions дле редюсера ServiceAccounts
 */
export const ServiceAccountsActions = {
    /**
     * Набор action на получение служебных аккаунтов
     */
    ReceiveServiceAccounts: createReducerActions(ServiceAccountsConsts.reducerName, ServiceAccountsProcessId.ReceiveServiceAccounts),

    /**
     * Набор action на удаление служебного аккаунта
     */
    DeleteServiceAccount: createReducerActions(ServiceAccountsConsts.reducerName, ServiceAccountsProcessId.DeleteServiceAccount),

    /**
     * Набор action на добавление служебного аккаунта
     */
    InsertServiceAccount: createReducerActions(ServiceAccountsConsts.reducerName, ServiceAccountsProcessId.InsertServiceAccount)
};