import { ServiceAccountsActions } from 'app/modules/serviceAccounts/store/actions';
import { ReceiveServiceAccountsThunkParams } from 'app/modules/serviceAccounts/store/reducers/receiveServiceAccountsReducer/params';
import { ReceiveServiceAccountsActionFailedPayload, ReceiveServiceAccountsActionStartPayload, ReceiveServiceAccountsActionSuccessPayload } from 'app/modules/serviceAccounts/store/reducers/receiveServiceAccountsReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = ReceiveServiceAccountsActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = ReceiveServiceAccountsActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = ReceiveServiceAccountsActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = ReceiveServiceAccountsThunkParams;

const { ReceiveServiceAccounts: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = ServiceAccountsActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для запроса списка действий облака для логирования
 */
export class ReceiveServiceAccountsThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new ReceiveServiceAccountsThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        const processFlowState = args.getStore().ServiceAccountsState;
        return {
            condition: !processFlowState.hasSuccessFor.hasServiceAccountsReceived || args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().ServiceAccountsState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        try {
            const serviceAccountsApi = InterlayerApiProxy.getServiceAccountsApi();
            const response = await serviceAccountsApi.receiveServiceAccounts(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, args.inputParams!);

            args.success({
                serviceAccounts: response
            });
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}