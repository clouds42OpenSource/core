import { ServiceAccountsActions } from 'app/modules/serviceAccounts/store/actions';
import { InsertServiceAccountThunkParams } from 'app/modules/serviceAccounts/store/reducers/insertServiceAccountReducer/params';
import { InsertServiceAccountActionFailedPayload, InsertServiceAccountActionStartPayload, InsertServiceAccountActionSuccessPayload } from 'app/modules/serviceAccounts/store/reducers/insertServiceAccountReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = InsertServiceAccountActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = InsertServiceAccountActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = InsertServiceAccountActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = InsertServiceAccountThunkParams;

const { InsertServiceAccount: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = ServiceAccountsActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для запроса списка действий облака для логирования
 */
export class InsertServiceAccountThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new InsertServiceAccountThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        return {
            condition: args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().ServiceAccountsState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        const inputArgs = args.inputParams!;
        try {
            const serviceAccountsApi = InterlayerApiProxy.getServiceAccountsApi();
            const result = await serviceAccountsApi.addServiceAccount(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, {
                accountId: inputArgs.accountId
            });

            args.success({
                newItem: {
                    accountId: inputArgs.accountId,
                    accountCaption: inputArgs.accountCaption,
                    accountIndexNumber: inputArgs.accountIndexNumber,
                    accountUserInitiatorName: result.accountUserInitiatorName,
                    creationDateTime: result.creationDateTime
                }
            });
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}