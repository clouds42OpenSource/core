import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения удаления записи служебного аккаунта
 */
export type DeleteServiceAccountActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении удаления записи служебного аккаунта
 */
export type DeleteServiceAccountActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении удаления записи служебного аккаунта
 */
export type DeleteServiceAccountActionSuccessPayload = ReducerActionSuccessPayload & {

    /**
     * Id аккаунта для удаления
     */
    accountId: string;
};

/**
 * Все возможные Action при удалении записи служебного аккаунта
 */
export type DeleteServiceAccountActionAnyPayload =
    | DeleteServiceAccountActionStartPayload
    | DeleteServiceAccountActionFailedPayload
    | DeleteServiceAccountActionSuccessPayload;