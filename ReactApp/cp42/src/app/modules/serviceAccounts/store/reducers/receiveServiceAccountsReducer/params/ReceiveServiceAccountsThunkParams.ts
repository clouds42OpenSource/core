import { ReceiveServiceAccountsParams } from 'app/web/InterlayerApiProxy/ServiceAccountsApiProxy/receiveServiceAccounts/input-params';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для получения служебных аккаунтов
 */
export type ReceiveServiceAccountsThunkParams = IForceThunkParam & ReceiveServiceAccountsParams;