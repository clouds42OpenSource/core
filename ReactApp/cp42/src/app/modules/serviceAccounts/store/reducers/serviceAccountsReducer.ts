import { getInitialMetadata } from 'app/common/functions';
import { ServiceAccountsConsts } from 'app/modules/serviceAccounts/constants';
import { deleteServiceAccountReducer } from 'app/modules/serviceAccounts/store/reducers/deleteServiceAccountReducer';
import { insertServiceAccountReducer } from 'app/modules/serviceAccounts/store/reducers/insertServiceAccountReducer';
import { receiveServiceAccountsReducer } from 'app/modules/serviceAccounts/store/reducers/receiveServiceAccountsReducer';
import { ServiceAccountsReducerState } from 'app/modules/serviceAccounts/store/reducers/ServiceAccountsReducerState';
import { createReducer, initReducerState } from 'core/redux/functions';

/**
 * Начальное состояние редюсера ServiceAccounts
 */
const initialState = initReducerState<ServiceAccountsReducerState>(
    ServiceAccountsConsts.reducerName,
    {
        serviceAccounts: {
            records: [],
            metadata: getInitialMetadata()
        },
        hasSuccessFor: {
            hasServiceAccountsReceived: false
        }
    }
);

/**
 * Объединённые части редюсера для всего состояния ServiceAccounts
 */
const partialReducers = [
    receiveServiceAccountsReducer,
    deleteServiceAccountReducer,
    insertServiceAccountReducer
];

/**
 * Редюсер ServiceAccounts
 */
export const serviceAccountsReducer = createReducer(initialState, partialReducers);