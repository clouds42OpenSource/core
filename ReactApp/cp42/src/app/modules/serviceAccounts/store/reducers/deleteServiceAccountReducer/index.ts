import { reducerActionState } from 'app/common/functions/reducerActionState';
import { ServiceAccountsProcessId } from 'app/modules/serviceAccounts/ServiceAccountsProcessId';
import { ServiceAccountsActions } from 'app/modules/serviceAccounts/store/actions';
import { DeleteServiceAccountActionAnyPayload, DeleteServiceAccountActionSuccessPayload } from 'app/modules/serviceAccounts/store/reducers/deleteServiceAccountReducer/payloads';
import { ServiceAccountsReducerState } from 'app/modules/serviceAccounts/store/reducers/ServiceAccountsReducerState';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для удаления записи в ServiceAccount
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const deleteServiceAccountReducer: TPartialReducerObject<ServiceAccountsReducerState, DeleteServiceAccountActionAnyPayload> =
    (state, action) => {
        const processId = ServiceAccountsProcessId.DeleteServiceAccount;
        const actions = ServiceAccountsActions.DeleteServiceAccount;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as DeleteServiceAccountActionSuccessPayload;

                const newRecords = state.serviceAccounts.records.filter(item => !(item.accountId === payload.accountId));

                return reducerStateProcessSuccess(state, processId, {
                    serviceAccounts: {
                        ...state.serviceAccounts,
                        records: newRecords,
                        metadata: {
                            ...state.serviceAccounts.metadata,
                            pageSize: state.serviceAccounts.metadata.pageSize - 1,
                            totalItemCount: state.serviceAccounts.metadata.totalItemCount - 1
                        }
                    }
                });
            }
        });
    };