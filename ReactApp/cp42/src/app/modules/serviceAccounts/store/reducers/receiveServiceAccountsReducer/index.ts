import { reducerActionState } from 'app/common/functions/reducerActionState';
import { ServiceAccountsProcessId } from 'app/modules/serviceAccounts/ServiceAccountsProcessId';
import { ServiceAccountsActions } from 'app/modules/serviceAccounts/store/actions';
import { ReceiveServiceAccountsActionAnyPayload, ReceiveServiceAccountsActionSuccessPayload } from 'app/modules/serviceAccounts/store/reducers/receiveServiceAccountsReducer/payloads';
import { ServiceAccountsReducerState } from 'app/modules/serviceAccounts/store/reducers/ServiceAccountsReducerState';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения записи в ServiceAccount
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const receiveServiceAccountsReducer: TPartialReducerObject<ServiceAccountsReducerState, ReceiveServiceAccountsActionAnyPayload> =
    (state, action) => {
        const processId = ServiceAccountsProcessId.ReceiveServiceAccounts;
        const actions = ServiceAccountsActions.ReceiveServiceAccounts;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as ReceiveServiceAccountsActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    serviceAccounts: { ...payload.serviceAccounts },
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasServiceAccountsReceived: true
                    }
                });
            }
        });
    };