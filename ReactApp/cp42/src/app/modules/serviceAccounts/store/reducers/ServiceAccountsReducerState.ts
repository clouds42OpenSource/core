import { ServiceAccountsProcessId } from 'app/modules/serviceAccounts/ServiceAccountsProcessId';
import { ServiceAccountsDataModel } from 'app/web/InterlayerApiProxy/ServiceAccountsApiProxy/receiveServiceAccounts';
import { IReducerState } from 'core/redux/interfaces';

/**
 * Состояние редюсера для работы с служебными аккаунтами
 */
export type ServiceAccountsReducerState = IReducerState<ServiceAccountsProcessId> & {

    serviceAccounts: ServiceAccountsDataModel;
    hasSuccessFor: {
        /**
         * Если true, то служебные аккаунты получены
         */
        hasServiceAccountsReceived: boolean,
    };
};