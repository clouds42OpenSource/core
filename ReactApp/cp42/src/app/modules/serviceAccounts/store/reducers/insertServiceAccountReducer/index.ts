import { reducerActionState } from 'app/common/functions/reducerActionState';
import { ServiceAccountsProcessId } from 'app/modules/serviceAccounts/ServiceAccountsProcessId';
import { ServiceAccountsActions } from 'app/modules/serviceAccounts/store/actions';
import { InsertServiceAccountActionAnyPayload, InsertServiceAccountActionSuccessPayload } from 'app/modules/serviceAccounts/store/reducers/insertServiceAccountReducer/payloads';
import { ServiceAccountsReducerState } from 'app/modules/serviceAccounts/store/reducers/ServiceAccountsReducerState';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для добавления записи в ServiceAccount
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const insertServiceAccountReducer: TPartialReducerObject<ServiceAccountsReducerState, InsertServiceAccountActionAnyPayload> =
    (state, action) => {
        const processId = ServiceAccountsProcessId.InsertServiceAccount;
        const actions = ServiceAccountsActions.InsertServiceAccount;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as InsertServiceAccountActionSuccessPayload;

                const newRecords = [payload.newItem, ...state.serviceAccounts.records];

                return reducerStateProcessSuccess(state, processId, {
                    serviceAccounts: {
                        ...state.serviceAccounts,
                        records: newRecords,
                        metadata: {
                            ...state.serviceAccounts.metadata,
                            pageSize: state.serviceAccounts.metadata.pageSize + 1,
                            totalItemCount: state.serviceAccounts.metadata.totalItemCount + 1
                        }
                    }
                });
            }
        });
    };