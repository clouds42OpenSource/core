import { ServiceAccountsDataModel } from 'app/web/InterlayerApiProxy/ServiceAccountsApiProxy/receiveServiceAccounts';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения служебных аккаунтов
 */
export type ReceiveServiceAccountsActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения служебных аккаунтов
 */
export type ReceiveServiceAccountsActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения служебных аккаунтов
 */
export type ReceiveServiceAccountsActionSuccessPayload = ReducerActionSuccessPayload & {

    /**
     * служебные аккаунты
     */
    serviceAccounts: ServiceAccountsDataModel
};

/**
 * Все возможные Action при получении служебных аккаунтов
 */
export type ReceiveServiceAccountsActionAnyPayload =
    | ReceiveServiceAccountsActionStartPayload
    | ReceiveServiceAccountsActionFailedPayload
    | ReceiveServiceAccountsActionSuccessPayload;