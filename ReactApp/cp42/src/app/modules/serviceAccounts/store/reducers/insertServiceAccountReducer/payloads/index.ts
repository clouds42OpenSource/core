import { ServiceAccountItemDataModel } from 'app/web/InterlayerApiProxy/ServiceAccountsApiProxy/receiveServiceAccounts';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения добавления записи служебного аккаунта
 */
export type InsertServiceAccountActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении добавления записи служебного аккаунта
 */
export type InsertServiceAccountActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении добавления записи служебного аккаунта
 */
export type InsertServiceAccountActionSuccessPayload = ReducerActionSuccessPayload & {

    newItem: ServiceAccountItemDataModel
};

/**
 * Все возможные Action при добавлении записи служебного аккаунта
 */
export type InsertServiceAccountActionAnyPayload =
    | InsertServiceAccountActionStartPayload
    | InsertServiceAccountActionFailedPayload
    | InsertServiceAccountActionSuccessPayload;