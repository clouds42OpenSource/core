import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для добавления записи служебного аккаунта
 */
export type InsertServiceAccountThunkParams = IForceThunkParam & {
    /**
     * ID аккаунта.
     */
    accountId: string;

    /**
     * Номер аккаунта.
     */
    accountIndexNumber: number;

    /**
     * Название аккаунта.
     */
    accountCaption: string;
};