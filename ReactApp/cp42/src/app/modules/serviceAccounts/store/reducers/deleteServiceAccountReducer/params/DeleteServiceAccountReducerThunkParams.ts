import { DeleteServiceAccountParams } from 'app/web/InterlayerApiProxy/ServiceAccountsApiProxy/deleteServiceAccount/input-params';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для удаления записи служебного аккаунта
 */
export type DeleteServiceAccountThunkParams = IForceThunkParam & DeleteServiceAccountParams;