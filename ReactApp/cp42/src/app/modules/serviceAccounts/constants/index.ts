/**
 * Константы для модуля ServiceAccounts
 */
export const ServiceAccountsConsts = {
    /**
     * Название редюсера для модуля ServiceAccounts
     */
    reducerName: 'SERVICE_ACCOUNTS'
};