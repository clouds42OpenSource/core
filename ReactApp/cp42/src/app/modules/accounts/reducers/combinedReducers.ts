import { combineReducers } from 'redux';

import getConfirmationFilesReducer from './getConfirmationFilesSlice';

export const AccountsReducer = combineReducers({
    getConfirmationFilesReducer,
});