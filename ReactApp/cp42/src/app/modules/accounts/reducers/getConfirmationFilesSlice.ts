import { TGetConfirmationFilesResponse } from 'app/api/endpoints/accounts/response';
import { TData, TDataReturn } from 'app/api/types';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { ACCOUNTS_SLICE } from '../constants';

const initialState: TDataReturn<TGetConfirmationFilesResponse> = {
    data: null,
    error: null,
    isLoading: false
};

export const getConfirmationFilesSlice = createSlice({
    name: ACCOUNTS_SLICE.getConfirmationFiles,
    initialState,
    reducers: {
        loading(state) {
            state.isLoading = true;
        },
        empty(state) {
            state.error = null;
            state.data = null;
        },
        success(state, action: PayloadAction<TData<TGetConfirmationFilesResponse>>) {
            state.isLoading = false;
            state.error = null;
            state.data = action.payload;
        },
        error(state, action: PayloadAction<string>) {
            state.isLoading = false;
            state.error = action.payload;
            state.data = null;
        }
    }
});

export default getConfirmationFilesSlice.reducer;