import { DbTemplateDelimetersActions } from 'app/modules/dbTemplateDelimeters/actions';
import { AddDbTemplateDelimeterThunkParams } from 'app/modules/dbTemplateDelimeters/reducers/addDbTemplateDelimeterReducer/params';
import { AddDbTemplateDelimeterActionFailedPayload, AddDbTemplateDelimeterActionStartPayload, AddDbTemplateDelimeterActionSuccessPayload } from 'app/modules/dbTemplateDelimeters/reducers/addDbTemplateDelimeterReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = AddDbTemplateDelimeterActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = AddDbTemplateDelimeterActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = AddDbTemplateDelimeterActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = AddDbTemplateDelimeterThunkParams;

const { AddDbTemplateDelimeter: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = DbTemplateDelimetersActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для запроса списка действий облака для логирования
 */
export class AddDbTemplateDelimeterThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new AddDbTemplateDelimeterThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        return {
            condition: args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().DbTemplateDelimetersState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        try {
            const api = InterlayerApiProxy.getDbTemplateDelimetersApiProxy();
            const res = await api.addDbTemplateDelimeter(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, args.inputParams!);

            // @ts-ignore
            if (res.success === false) {
                throw res;
            }

            args.success({
                newItem: args.inputParams!
            });
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}