import { DbTemplateDelimetersActions } from 'app/modules/dbTemplateDelimeters/actions';
import { ReceiveDbTemplateDelimetersThunkParams } from 'app/modules/dbTemplateDelimeters/reducers/receiveDbTemplateDelimetersReducer/params';
import {
    ReceiveDbTemplateDelimetersActionFailedPayload,
    ReceiveDbTemplateDelimetersActionStartPayload,
    ReceiveDbTemplateDelimetersActionSuccessPayload
} from 'app/modules/dbTemplateDelimeters/reducers/receiveDbTemplateDelimetersReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = ReceiveDbTemplateDelimetersActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = ReceiveDbTemplateDelimetersActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = ReceiveDbTemplateDelimetersActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = ReceiveDbTemplateDelimetersThunkParams;

const { ReceiveDbTemplateDelimeters: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = DbTemplateDelimetersActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для запроса списка действий облака для логирования
 */
export class ReceiveDbTemplateDelimetersThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new ReceiveDbTemplateDelimetersThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        const processFlowState = args.getStore().DbTemplateDelimetersState;
        return {
            condition: !processFlowState.hasSuccessFor.hasDbTemplateDelimetersReceived || args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().DbTemplateDelimetersState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        try {
            const api = InterlayerApiProxy.getDbTemplateDelimetersApiProxy();
            const response = await api.receiveDbTemplateDelimeters(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, args.inputParams!);

            args.success({
                dbTemplateDelimeters: response
            });
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}