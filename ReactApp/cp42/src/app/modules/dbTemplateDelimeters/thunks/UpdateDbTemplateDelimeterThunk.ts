import { DbTemplateDelimetersActions } from 'app/modules/dbTemplateDelimeters/actions';
import { UpdateDbTemplateDelimeterThunkParams } from 'app/modules/dbTemplateDelimeters/reducers/updateDbTemplateDelimeterReducer/params';
import { UpdateDbTemplateDelimeterActionFailedPayload, UpdateDbTemplateDelimeterActionStartPayload, UpdateDbTemplateDelimeterActionSuccessPayload } from 'app/modules/dbTemplateDelimeters/reducers/updateDbTemplateDelimeterReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = UpdateDbTemplateDelimeterActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = UpdateDbTemplateDelimeterActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = UpdateDbTemplateDelimeterActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = UpdateDbTemplateDelimeterThunkParams;

const { UpdateDbTemplateDelimeter: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = DbTemplateDelimetersActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для запроса списка действий облака для логирования
 */
export class UpdateDbTemplateDelimeterThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new UpdateDbTemplateDelimeterThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        return {
            condition: args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().DbTemplateDelimetersState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        try {
            const api = InterlayerApiProxy.getDbTemplateDelimetersApiProxy();
            await api.updateDbTemplateDelimeter(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, args.inputParams!);

            args.success({
                updatedItem: args.inputParams!
            });
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}