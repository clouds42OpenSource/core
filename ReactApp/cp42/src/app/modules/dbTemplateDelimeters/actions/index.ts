import { Configurations1CConsts } from 'app/modules/configurations1C/constants';
import { DbTemplateDelimetersProcessId } from 'app/modules/dbTemplateDelimeters/DbTemplateDelimetersProcessId';
import { createReducerActions } from 'core/redux/functions';

/**
 * Все доступные actions дле редюсера DbTemplateDelimeters
 */
export const DbTemplateDelimetersActions = {
    /**
     * Набор action на получение шаблонов баз на разделителях
     */
    ReceiveDbTemplateDelimeters: createReducerActions(Configurations1CConsts.reducerName, DbTemplateDelimetersProcessId.ReceiveDbTemplateDelimeters),

    /**
     * Набор action на обновление шаблона базы на разделителях
     */
    UpdateDbTemplateDelimeter: createReducerActions(Configurations1CConsts.reducerName, DbTemplateDelimetersProcessId.UpdateDbTemplateDelimeter),

    /**
     * Набор action на добавление шаблона базы на разделителях
     */
    AddDbTemplateDelimeter: createReducerActions(Configurations1CConsts.reducerName, DbTemplateDelimetersProcessId.AddDbTemplateDelimeter),

    /**
     * Набор action на удаление шаблона базы на разделителях
     */
    DeleteDbTemplateDelimeter: createReducerActions(Configurations1CConsts.reducerName, DbTemplateDelimetersProcessId.DeleteDbTemplateDelimeter)
};