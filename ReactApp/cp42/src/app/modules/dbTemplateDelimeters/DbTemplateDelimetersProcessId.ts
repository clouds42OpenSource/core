/**
 * Процесы DbTemplateDelimeters
 */
export enum DbTemplateDelimetersProcessId {
    /**
     * Процесс получить шаблоны
     */
    ReceiveDbTemplateDelimeters = 'RECEIVE_DB_TEMPLATES_DELIMETER',

    /**
     * Процесс обновить шаблон
     */
    UpdateDbTemplateDelimeter = 'UPDATE_DB_TEMPLATE_DELIMETER',

    /**
     * Процесс добавления шаблона
     */
    AddDbTemplateDelimeter = 'ADD_DB_TEMPLATE_DELIMETER',

    /**
     * Процесс удаления шаблона
     */
    DeleteDbTemplateDelimeter = 'DELETE_DB_TEMPLATE_DELIMETER'
}