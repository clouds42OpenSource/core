import { addDbTemplateDelimeterReducer } from 'app/modules/dbTemplateDelimeters/reducers/addDbTemplateDelimeterReducer';
import { DbTemplateDelimetersReducerState } from 'app/modules/dbTemplateDelimeters/reducers/DbTemplateDelimetersReducerState';
import { deleteDbTemplateDelimeterReducer } from 'app/modules/dbTemplateDelimeters/reducers/deleteDbTemplateDelimeterReducer';
import { receiveDbTemplateDelimetersReducer } from 'app/modules/dbTemplateDelimeters/reducers/receiveDbTemplateDelimetersReducer';
import { updateDbTemplateDelimeterReducer } from 'app/modules/dbTemplateDelimeters/reducers/updateDbTemplateDelimeterReducer';
import { DbTemplatesConsts } from 'app/modules/DbTemplates/constants';
import { createReducer, initReducerState } from 'core/redux/functions';

/**
 * Начальное состояние редюсера DbTemplateDelimeters
 */
const initialState = initReducerState<DbTemplateDelimetersReducerState>(
    DbTemplatesConsts.reducerName,
    {
        dbTemplateDelimeters: {
            records: [],
        },
        hasSuccessFor: {
            hasDbTemplateDelimetersReceived: false
        }
    }
);

/**
 * Объединённые части редюсера для всего состояния DbTemplateDelimeters
 */
const partialReducers = [
    receiveDbTemplateDelimetersReducer,
    updateDbTemplateDelimeterReducer,
    addDbTemplateDelimeterReducer,
    deleteDbTemplateDelimeterReducer
];

/**
 * Редюсер DbTemplateDelimeters
 */
export const dbTemplateDelimetersReducer = createReducer(initialState, partialReducers);