import { reducerActionState } from 'app/common/functions/reducerActionState';
import { DbTemplateDelimetersActions } from 'app/modules/dbTemplateDelimeters/actions';
import { DbTemplateDelimetersProcessId } from 'app/modules/dbTemplateDelimeters/DbTemplateDelimetersProcessId';
import { AddDbTemplateDelimeterActionAnyPayload, AddDbTemplateDelimeterActionSuccessPayload } from 'app/modules/dbTemplateDelimeters/reducers/addDbTemplateDelimeterReducer/payloads';
import { DbTemplateDelimetersReducerState } from 'app/modules/dbTemplateDelimeters/reducers/DbTemplateDelimetersReducerState';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для добавления записи в DbTemplateDelimeter
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const addDbTemplateDelimeterReducer: TPartialReducerObject<DbTemplateDelimetersReducerState, AddDbTemplateDelimeterActionAnyPayload> =
    (state, action) => {
        const processId = DbTemplateDelimetersProcessId.AddDbTemplateDelimeter;
        const actions = DbTemplateDelimetersActions.AddDbTemplateDelimeter;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as AddDbTemplateDelimeterActionSuccessPayload;

                const newRecords = [
                    payload.newItem,
                    ...state.dbTemplateDelimeters.records
                ];

                return reducerStateProcessSuccess(state, processId, {
                    dbTemplateDelimeters: {
                        records: newRecords
                    },
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasDbTemplateDelimetersReceived: true
                    }
                });
            }
        });
    };