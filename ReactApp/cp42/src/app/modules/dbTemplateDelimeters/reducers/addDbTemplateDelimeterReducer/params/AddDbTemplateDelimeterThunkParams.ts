import { AddDbTemplateDelimeterParams } from 'app/web/InterlayerApiProxy/DbTemplateDelimetersApiProxy/addDbTemplateDelimeter/input-params';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для обновления шаблонов баз разделителей
 */
export type AddDbTemplateDelimeterThunkParams = IForceThunkParam & AddDbTemplateDelimeterParams;