import { DbTemplateDelimeterItemDataModel } from 'app/web/InterlayerApiProxy/DbTemplateDelimetersApiProxy/receiveDbTemplateDelimeters';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения добавления шаблона базы на разделителях
 */
export type AddDbTemplateDelimeterActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении добавления шаблона баз на разделителях
 */
export type AddDbTemplateDelimeterActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении добавления шаблона баз на разделителях
 */
export type AddDbTemplateDelimeterActionSuccessPayload = ReducerActionSuccessPayload & {

    /**
     * добавленный шаблон базы на разделителях
     */
    newItem: DbTemplateDelimeterItemDataModel
};

/**
 * Все возможные Action при добавлении шаблона базы на разделителях
 */
export type AddDbTemplateDelimeterActionAnyPayload =
    | AddDbTemplateDelimeterActionStartPayload
    | AddDbTemplateDelimeterActionFailedPayload
    | AddDbTemplateDelimeterActionSuccessPayload;