import { DbTemplateDelimeterItemDataModel } from 'app/web/InterlayerApiProxy/DbTemplateDelimetersApiProxy/receiveDbTemplateDelimeters';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения обновления шаблона базы на разделителях
 */
export type UpdateDbTemplateDelimeterActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении обновления шаблона баз на разделителях
 */
export type UpdateDbTemplateDelimeterActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении обновления шаблона баз на разделителях
 */
export type UpdateDbTemplateDelimeterActionSuccessPayload = ReducerActionSuccessPayload & {

    /**
     * обновлённый шаблон базы на разделителях
     */
    updatedItem: DbTemplateDelimeterItemDataModel
};

/**
 * Все возможные Action при обновлении шаблона базы на разделителях
 */
export type UpdateDbTemplateDelimeterActionAnyPayload =
    | UpdateDbTemplateDelimeterActionStartPayload
    | UpdateDbTemplateDelimeterActionFailedPayload
    | UpdateDbTemplateDelimeterActionSuccessPayload;