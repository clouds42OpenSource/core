import { reducerActionState } from 'app/common/functions/reducerActionState';
import { DbTemplateDelimetersActions } from 'app/modules/dbTemplateDelimeters/actions';
import { DbTemplateDelimetersProcessId } from 'app/modules/dbTemplateDelimeters/DbTemplateDelimetersProcessId';
import { DbTemplateDelimetersReducerState } from 'app/modules/dbTemplateDelimeters/reducers/DbTemplateDelimetersReducerState';
import { UpdateDbTemplateDelimeterActionAnyPayload, UpdateDbTemplateDelimeterActionSuccessPayload } from 'app/modules/dbTemplateDelimeters/reducers/updateDbTemplateDelimeterReducer/payloads';
import { reducerStateProcessFail, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для редактирования записи в DbTemplateDelimeter
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const updateDbTemplateDelimeterReducer: TPartialReducerObject<DbTemplateDelimetersReducerState, UpdateDbTemplateDelimeterActionAnyPayload> =
    (state, action) => {
        const processId = DbTemplateDelimetersProcessId.UpdateDbTemplateDelimeter;
        const actions = DbTemplateDelimetersActions.UpdateDbTemplateDelimeter;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as UpdateDbTemplateDelimeterActionSuccessPayload;

                const foundIndex = state.dbTemplateDelimeters.records.findIndex(item => item.configurationId === payload.updatedItem.configurationId);

                if (foundIndex < 0) {
                    return reducerStateProcessFail(state, processId, new Error(`Не получилось найти шаблон базы на разделителях с ID ${ payload.updatedItem.configurationId }`));
                }

                const newRecords = [
                    ...state.dbTemplateDelimeters.records
                ];

                newRecords[foundIndex] = {
                    ...payload.updatedItem
                };

                return reducerStateProcessSuccess(state, processId, {
                    dbTemplateDelimeters: {
                        records: newRecords
                    },
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasDbTemplateDelimetersReceived: true
                    }
                });
            }
        });
    };