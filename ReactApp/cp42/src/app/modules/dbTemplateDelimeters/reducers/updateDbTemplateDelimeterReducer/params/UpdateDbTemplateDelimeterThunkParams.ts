import { UpdateDbTemplateDelimeterParams } from 'app/web/InterlayerApiProxy/DbTemplateDelimetersApiProxy/updateDbTemplateDelimeter/input-params';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для обновления шаблонов баз разделителей
 */
export type UpdateDbTemplateDelimeterThunkParams = IForceThunkParam & UpdateDbTemplateDelimeterParams;