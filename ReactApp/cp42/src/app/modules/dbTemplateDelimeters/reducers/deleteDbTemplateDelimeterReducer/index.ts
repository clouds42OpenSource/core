import { reducerActionState } from 'app/common/functions/reducerActionState';
import { DbTemplateDelimetersActions } from 'app/modules/dbTemplateDelimeters/actions';
import { DbTemplateDelimetersProcessId } from 'app/modules/dbTemplateDelimeters/DbTemplateDelimetersProcessId';
import { DbTemplateDelimetersReducerState } from 'app/modules/dbTemplateDelimeters/reducers/DbTemplateDelimetersReducerState';
import { DeleteDbTemplateDelimeterActionAnyPayload, DeleteDbTemplateDelimeterActionSuccessPayload } from 'app/modules/dbTemplateDelimeters/reducers/deleteDbTemplateDelimeterReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для удаления записи в DbTemplateDelimeter
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const deleteDbTemplateDelimeterReducer: TPartialReducerObject<DbTemplateDelimetersReducerState, DeleteDbTemplateDelimeterActionAnyPayload> =
    (state, action) => {
        const processId = DbTemplateDelimetersProcessId.DeleteDbTemplateDelimeter;
        const actions = DbTemplateDelimetersActions.DeleteDbTemplateDelimeter;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as DeleteDbTemplateDelimeterActionSuccessPayload;

                const newRecords = state.dbTemplateDelimeters.records.filter(item => item.configurationId !== payload.configurationDbtemplateDelimeterId);

                return reducerStateProcessSuccess(state, processId, {
                    dbTemplateDelimeters: {
                        records: newRecords
                    },
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasDbTemplateDelimetersReceived: true
                    }
                });
            }
        });
    };