import { DeleteDbTemplateDelimeterParams } from 'app/web/InterlayerApiProxy/DbTemplateDelimetersApiProxy/deleteDbTemplateDelimeter/input-params';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для удаления шаблона баз разделителей
 */
export type DeleteDbTemplateDelimeterThunkParams = IForceThunkParam & DeleteDbTemplateDelimeterParams;