import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения удаления шаблона базы на разделителях
 */
export type DeleteDbTemplateDelimeterActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении удаления шаблона баз на разделителях
 */
export type DeleteDbTemplateDelimeterActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении удаления шаблона баз на разделителях
 */
export type DeleteDbTemplateDelimeterActionSuccessPayload = ReducerActionSuccessPayload & {

    /**
     * Код конфигурации шаблона базы на разделителях для удаления
     */
    configurationDbtemplateDelimeterId: string;
};

/**
 * Все возможные Action при удалении шаблона базы на разделителях
 */
export type DeleteDbTemplateDelimeterActionAnyPayload =
    | DeleteDbTemplateDelimeterActionStartPayload
    | DeleteDbTemplateDelimeterActionFailedPayload
    | DeleteDbTemplateDelimeterActionSuccessPayload;