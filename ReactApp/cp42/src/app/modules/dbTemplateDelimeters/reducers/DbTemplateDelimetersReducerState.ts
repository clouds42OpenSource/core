import { DbTemplateDelimetersProcessId } from 'app/modules/dbTemplateDelimeters/DbTemplateDelimetersProcessId';
import { DbTemplateDelimeterItemDataModel } from 'app/web/InterlayerApiProxy/DbTemplateDelimetersApiProxy/receiveDbTemplateDelimeters';
import { IReducerState } from 'core/redux/interfaces';

/**
 * Состояние редюсера для работы с шаблонами баз на разделителях
 */
export type DbTemplateDelimetersReducerState = IReducerState<DbTemplateDelimetersProcessId> & {

    /**
     * Шаблоны баз на разделителях
     */
    dbTemplateDelimeters: {
        records: Array<DbTemplateDelimeterItemDataModel>;
    }
    hasSuccessFor: {
        /**
         * Если true, то шаблоны баз на разделителях получены
         */
        hasDbTemplateDelimetersReceived: boolean,
    };
};