import { reducerActionState } from 'app/common/functions/reducerActionState';
import { DbTemplateDelimetersActions } from 'app/modules/dbTemplateDelimeters/actions';
import { DbTemplateDelimetersProcessId } from 'app/modules/dbTemplateDelimeters/DbTemplateDelimetersProcessId';
import { DbTemplateDelimetersReducerState } from 'app/modules/dbTemplateDelimeters/reducers/DbTemplateDelimetersReducerState';
import { ReceiveDbTemplateDelimetersActionAnyPayload, ReceiveDbTemplateDelimetersActionSuccessPayload } from 'app/modules/dbTemplateDelimeters/reducers/receiveDbTemplateDelimetersReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения записи в DbTemplateDelimeter
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const receiveDbTemplateDelimetersReducer: TPartialReducerObject<DbTemplateDelimetersReducerState, ReceiveDbTemplateDelimetersActionAnyPayload> =
    (state, action) => {
        const processId = DbTemplateDelimetersProcessId.ReceiveDbTemplateDelimeters;
        const actions = DbTemplateDelimetersActions.ReceiveDbTemplateDelimeters;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as ReceiveDbTemplateDelimetersActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    dbTemplateDelimeters: { ...payload.dbTemplateDelimeters },
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasDbTemplateDelimetersReceived: true
                    }
                });
            }
        });
    };