import { GetDbTemplateDelimetersParams } from 'app/web/InterlayerApiProxy/DbTemplateDelimetersApiProxy/receiveDbTemplateDelimeters/input-params';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для получения шаблонов баз разделителей
 */
export type ReceiveDbTemplateDelimetersThunkParams = IForceThunkParam & GetDbTemplateDelimetersParams;