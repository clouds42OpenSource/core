import { GetDbTemplateDelimetersResultDataModel } from 'app/web/InterlayerApiProxy/DbTemplateDelimetersApiProxy/receiveDbTemplateDelimeters/data-models/GetDbTemplateDelimetersResultDataModel';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения шаблонов баз на разделителях
 */
export type ReceiveDbTemplateDelimetersActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения шаблонов баз на разделителях
 */
export type ReceiveDbTemplateDelimetersActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения шаблонов баз на разделителях
 */
export type ReceiveDbTemplateDelimetersActionSuccessPayload = ReducerActionSuccessPayload & {

    /**
     * шаблоны баз на разделителях
     */
    dbTemplateDelimeters: GetDbTemplateDelimetersResultDataModel
};

/**
 * Все возможные Action при получении шаблонов баз на разделителях
 */
export type ReceiveDbTemplateDelimetersActionAnyPayload =
    | ReceiveDbTemplateDelimetersActionStartPayload
    | ReceiveDbTemplateDelimetersActionFailedPayload
    | ReceiveDbTemplateDelimetersActionSuccessPayload;