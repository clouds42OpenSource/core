/**
 * Константы для модуля DbTemplateDelimeters
 */
export const DbTemplateDelimetersConsts = {
    /**
     * Название редюсера для модуля DbTemplateDelimeters
     */
    reducerName: 'DBTEMPLATEDELIMETERS'
};