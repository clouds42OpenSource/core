import { createReducerActions } from 'core/redux/functions';
import { WorkerTasksBagsControlCardConstants } from 'app/modules/workerTasksBagsControlCard/consts';
import { WorkerTasksBagsControlCardProcessId } from 'app/modules/workerTasksBagsControlCard/WorkerTasksBagsControlCardProcessId';

/**
 * Все действия (Actions) для редюсера WorkerTasksBagsControlCard
 */
export const WorkerTasksBagsControlCardActions = {

    /**
     * Действие для получения данных по карточке взаимодействия воркера и задач
     */
    ReceiveWorkerTasksBagsControlCardData: createReducerActions(WorkerTasksBagsControlCardConstants.reducerName, WorkerTasksBagsControlCardProcessId.ReceiveWorkerTasksBagsControlCardData),

    /**
     * Действие для изменения данных в карточке взаимодействия воркера и задач
     */
    ChangeWorkerTasksBags: createReducerActions(WorkerTasksBagsControlCardConstants.reducerName, WorkerTasksBagsControlCardProcessId.ChangeWorkerTasksBags)
};