import { AppReduxStoreState } from 'app/redux/types';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { ChangeTasksBagsActionFailedPayload, ChangeTasksBagsActionStartPayload, ChangeTasksBagsActionSuccessPayload } from 'app/modules/workerTasksBagsControlCard/store/reducers/changeTasksBags/payloads';
import { ChangeTasksBagsThunkParams } from 'app/modules/workerTasksBagsControlCard/store/reducers/changeTasksBags/params';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
export type ChangeTasksBagsThunkActionStartPayload = ChangeTasksBagsActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
export type ChangeTasksBagsThunkActionSuccessPayload = ChangeTasksBagsActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
export type ChangeTasksBagsThunkActionFailedPayload = ChangeTasksBagsActionFailedPayload;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
export type ChangeTasksBagsThunkStartActionParams =
    IThunkStartActionParams<AppReduxStoreState, ChangeTasksBagsThunkActionStartPayload, ChangeTasksBagsThunkActionSuccessPayload, ChangeTasksBagsThunkActionFailedPayload, ChangeTasksBagsThunkParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
export type ChangeTasksBagsThunkStartActionResult = IThunkStartActionResult<ChangeTasksBagsThunkActionStartPayload, ChangeTasksBagsThunkActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
export type ChangeTasksBagsThunkStartExecutionParams =
    IThunkStartExecutionParams<AppReduxStoreState, ChangeTasksBagsThunkActionStartPayload, ChangeTasksBagsThunkActionSuccessPayload, ChangeTasksBagsThunkActionFailedPayload, ChangeTasksBagsThunkParams>;