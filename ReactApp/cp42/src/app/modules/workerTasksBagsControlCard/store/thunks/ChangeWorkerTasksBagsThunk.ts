import { WorkerTasksBagsControlCardActions } from 'app/modules/workerTasksBagsControlCard/store/actions';
import { ChangeTasksBagsThunkParams } from 'app/modules/workerTasksBagsControlCard/store/reducers/changeTasksBags/params';
import {
    ChangeTasksBagsThunkActionFailedPayload,
    ChangeTasksBagsThunkActionStartPayload,
    ChangeTasksBagsThunkActionSuccessPayload,
    ChangeTasksBagsThunkStartActionParams,
    ChangeTasksBagsThunkStartActionResult,
    ChangeTasksBagsThunkStartExecutionParams
} from 'app/modules/workerTasksBagsControlCard/store/thunks/types/ChangeWorkerTasksBagsThunkTypes';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { RequestKind } from 'core/requestSender/enums';

const { ChangeWorkerTasksBags: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = WorkerTasksBagsControlCardActions;

/**
 * Thunk для добавления новой задачи в очередь
 */
export class ChangeWorkerTasksBagsThunk extends BaseReduxThunkObject<
    AppReduxStoreState, ChangeTasksBagsThunkActionStartPayload, ChangeTasksBagsThunkActionSuccessPayload, ChangeTasksBagsThunkActionFailedPayload, ChangeTasksBagsThunkParams
> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: ChangeTasksBagsThunkParams) {
        return new ChangeWorkerTasksBagsThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: ChangeTasksBagsThunkStartActionParams): ChangeTasksBagsThunkStartActionResult {
        return {
            condition: args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().TaskInQueueItemCardState,
        };
    }

    protected async startExecution(args: ChangeTasksBagsThunkStartExecutionParams): Promise<void> {
        const requestArgs = args.inputParams!;
        const workerTasksBagsControlCardApiProxy = InterlayerApiProxy.getWorkerTasksBagsControlCardApiProxy();

        try {
            await workerTasksBagsControlCardApiProxy.changeTasksBags(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, {
                coreWorkerId: requestArgs.coreWorkerId,
                workerTasksBagsIds: requestArgs.workerTasksBagsIds
            });

            args.success(requestArgs);
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}