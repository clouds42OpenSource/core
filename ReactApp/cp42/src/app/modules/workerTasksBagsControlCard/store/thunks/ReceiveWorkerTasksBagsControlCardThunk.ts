import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';
import {
    ReceiveWorkerTasksBagsControlCardActionFailedPayload,
    ReceiveWorkerTasksBagsControlCardActionStartPayload,
    ReceiveWorkerTasksBagsControlCardActionSuccessPayload
} from 'app/modules/workerTasksBagsControlCard/store/reducers/receiveWorkerTasksBagsControlCard/payloads';
import { WorkerTasksBagsControlCardActions } from 'app/modules/workerTasksBagsControlCard/store/actions';
import { ReceiveWorkerTaskBagsControlCardThunkParams } from 'app/modules/workerTasksBagsControlCard/store/reducers/receiveWorkerTasksBagsControlCard/params/ReceiveWorkerTaskBagsControlCardThunkParams';
import { ErrorObject } from 'core/redux';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = ReceiveWorkerTasksBagsControlCardActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = ReceiveWorkerTasksBagsControlCardActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = ReceiveWorkerTasksBagsControlCardActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = ReceiveWorkerTaskBagsControlCardThunkParams;

const { ReceiveWorkerTasksBagsControlCardData: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = WorkerTasksBagsControlCardActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для запроса списка действий облака для логирования
 */
export class ReceiveWorkerTasksBagsControlCardThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new ReceiveWorkerTasksBagsControlCardThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        const tasksBagsControlCardState = args.getStore().WorkerTasksBagsControlCardState;
        return {
            condition: !tasksBagsControlCardState.hasSuccessFor.hasTasksBagsControlCardReceived || args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().WorkerTasksBagsControlCardState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        try {
            const api = InterlayerApiProxy.getWorkerTasksBagsControlCardApiProxy();
            const response = await api.receiveCardData(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, args.inputParams!);

            args.success({
                details: response
            });
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}