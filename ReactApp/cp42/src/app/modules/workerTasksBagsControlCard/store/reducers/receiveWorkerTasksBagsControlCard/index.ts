import { reducerActionState } from 'app/common/functions/reducerActionState';
import { WorkerTasksBagsControlCardActions } from 'app/modules/workerTasksBagsControlCard/store/actions';
import { ReceiveWorkerTasksBagsControlCardActionAnyPayload, ReceiveWorkerTasksBagsControlCardActionSuccessPayload } from 'app/modules/workerTasksBagsControlCard/store/reducers/receiveWorkerTasksBagsControlCard/payloads';
import { workerTasksBagsControlCardState } from 'app/modules/workerTasksBagsControlCard/store/reducers/workerTasksBagsControlCardState';
import { WorkerTasksBagsControlCardProcessId } from 'app/modules/workerTasksBagsControlCard/WorkerTasksBagsControlCardProcessId';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения данных по взаимодействия воркера и задач
 * @param state состояние
 * @param action действие
 */
export const receiveWorkerTasksBagsControlCardDataReducer: TPartialReducerObject<workerTasksBagsControlCardState, ReceiveWorkerTasksBagsControlCardActionAnyPayload> =
    (state, action) => {
        const processId = WorkerTasksBagsControlCardProcessId.ReceiveWorkerTasksBagsControlCardData;
        const actions = WorkerTasksBagsControlCardActions.ReceiveWorkerTasksBagsControlCardData;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as ReceiveWorkerTasksBagsControlCardActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    details: payload.details,
                    hasSuccessFor: {
                        hasTasksBagsControlCardReceived: true
                    }
                });
            }
        });
    };