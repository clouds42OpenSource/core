import { IReducerState } from 'core/redux/interfaces';
import { WorkerTasksBagsControlCardProcessId } from 'app/modules/workerTasksBagsControlCard/WorkerTasksBagsControlCardProcessId';
import { WorkerTasksBagsControlCardDataModel } from 'app/web/InterlayerApiProxy/WorkerTasksBagsControlCardApiProxy/receiveWorkerTasksBagsControlCard/data-models';
import { ChangeTasksBagsParams } from 'app/modules/workerTasksBagsControlCard/store/reducers/changeTasksBags/params';

/**
 * Состояние для карточки управления доступными задачами воркера
 */
export type workerTasksBagsControlCardState = IReducerState<WorkerTasksBagsControlCardProcessId> & {

    /**
     * Данные карточки управления доступными задачами воркера
     */
    details: WorkerTasksBagsControlCardDataModel;

    /**
     * Параметры изменения доступных задач воркера
     */
    changeTasksBags: ChangeTasksBagsParams;

    /**
     * содержит ярлыки для фиксации загрузок данных, была ли загрузка или ещё нет
     */
    hasSuccessFor: {
        /**
         * Если true, то карточка взаимодействия воркера и задач уже получена
         */
        hasTasksBagsControlCardReceived: boolean
    };
};