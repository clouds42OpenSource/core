import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель изменения данных в карточке взаимодействия воркера и задач
 */
export type ChangeTasksBagsParams = {
    /**
     * ID воркера
     */
    coreWorkerId: number,

    /**
     * Список ID доступных задач воркера
     */
    workerTasksBagsIds: Array<string>
};

/**
 * Модель на запрос изменения данных в карточке взаимодействия воркера и задач
 */
export type ChangeTasksBagsThunkParams = ChangeTasksBagsParams & IForceThunkParam;