import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';
import { ChangeTasksBagsParams } from 'app/modules/workerTasksBagsControlCard/store/reducers/changeTasksBags/params';

/**
 * Данные в редюсер при старте изменения данных в карточке взаимодействия воркера и задач
 */
export type ChangeTasksBagsActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при ошибке изменения данных в карточке взаимодействия воркера и задач
 */
export type ChangeTasksBagsActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном изменении данных в карточке взаимодействия воркера и задач
 */
export type ChangeTasksBagsActionSuccessPayload = ReducerActionSuccessPayload & ChangeTasksBagsParams;

/**
 * Все возможные Action при изменении данных в карточке взаимодействия воркера и задач
 */
export type ChangeTasksBagsActionAnyPayload =
    | ChangeTasksBagsActionStartPayload
    | ChangeTasksBagsActionSuccessPayload
    | ChangeTasksBagsActionFailedPayload;