import { reducerActionState } from 'app/common/functions/reducerActionState';
import { WorkerTasksBagsControlCardActions } from 'app/modules/workerTasksBagsControlCard/store/actions';
import { ChangeTasksBagsActionAnyPayload, ChangeTasksBagsActionSuccessPayload } from 'app/modules/workerTasksBagsControlCard/store/reducers/changeTasksBags/payloads';
import { workerTasksBagsControlCardState } from 'app/modules/workerTasksBagsControlCard/store/reducers/workerTasksBagsControlCardState';
import { WorkerTasksBagsControlCardProcessId } from 'app/modules/workerTasksBagsControlCard/WorkerTasksBagsControlCardProcessId';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для изменения данных в карточке взаимодействия воркера и задач
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const changeTasksBagsReducer: TPartialReducerObject<workerTasksBagsControlCardState, ChangeTasksBagsActionAnyPayload> =
    (state, action) => {
        const processId = WorkerTasksBagsControlCardProcessId.ChangeWorkerTasksBags;
        const actions = WorkerTasksBagsControlCardActions.ChangeWorkerTasksBags;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as ChangeTasksBagsActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    changeTasksBags: payload,
                    details: { ...state.details }
                });
            }
        });
    };