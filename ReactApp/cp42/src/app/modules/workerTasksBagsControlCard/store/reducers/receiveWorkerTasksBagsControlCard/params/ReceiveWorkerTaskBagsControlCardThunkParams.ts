import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Параметры санки для получения данных карточки взаимодействия воркера и задач
 */
export type ReceiveWorkerTaskBagsControlCardThunkParams = IForceThunkParam & {
    /**
     * ID воркера
     */
    coreWorkerId: number
};