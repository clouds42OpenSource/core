import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';
import { WorkerTasksBagsControlCardDataModel } from 'app/web/InterlayerApiProxy/WorkerTasksBagsControlCardApiProxy/receiveWorkerTasksBagsControlCard/data-models';

/**
 * Данные в редюсер при старте получения данных карточки управления доступными задачами воркера
 */
export type ReceiveWorkerTasksBagsControlCardActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном получении данных карточки управления доступными задачами воркера
 */
export type ReceiveWorkerTasksBagsControlCardActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном получении данных карточки управления доступными задачами воркера
 */
export type ReceiveWorkerTasksBagsControlCardActionSuccessPayload = ReducerActionSuccessPayload & {

    /**
     * Модель карточки управления доступными задачами воркера
     */
    details: WorkerTasksBagsControlCardDataModel;
};

/**
 * Все возможные Action при получении данных карточки управления доступными задачами воркера
 */
export type ReceiveWorkerTasksBagsControlCardActionAnyPayload =
    | ReceiveWorkerTasksBagsControlCardActionStartPayload
    | ReceiveWorkerTasksBagsControlCardActionFailedPayload
    | ReceiveWorkerTasksBagsControlCardActionSuccessPayload;