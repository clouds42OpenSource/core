import { createReducer, initReducerState } from 'core/redux/functions';
import { workerTasksBagsControlCardState } from 'app/modules/workerTasksBagsControlCard/store/reducers/workerTasksBagsControlCardState';
import { WorkerTasksBagsControlCardConstants } from 'app/modules/workerTasksBagsControlCard/consts';
import { partialFillOf } from 'app/common/functions';
import { WorkerTasksBagsControlCardDataModel } from 'app/web/InterlayerApiProxy/WorkerTasksBagsControlCardApiProxy/receiveWorkerTasksBagsControlCard/data-models';
import { receiveWorkerTasksBagsControlCardDataReducer } from 'app/modules/workerTasksBagsControlCard/store/reducers/receiveWorkerTasksBagsControlCard';
import { changeTasksBagsReducer } from 'app/modules/workerTasksBagsControlCard/store/reducers/changeTasksBags';

/**
 * Начальное состояние редьюсера
 */
const initialState = initReducerState<workerTasksBagsControlCardState>(
    WorkerTasksBagsControlCardConstants.reducerName,
    {
        details: partialFillOf<WorkerTasksBagsControlCardDataModel>(),
        changeTasksBags: {
            coreWorkerId: -1,
            workerTasksBagsIds: []
        },
        hasSuccessFor: {
            hasTasksBagsControlCardReceived: false
        }
    }
);

const partialReducers = [
    receiveWorkerTasksBagsControlCardDataReducer,
    changeTasksBagsReducer
];

export const workerTasksBagsControlCardReducer = createReducer(initialState, partialReducers);