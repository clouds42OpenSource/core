/**
 * Процесы для карточки взаимодействия воркера и задач
 */
export enum WorkerTasksBagsControlCardProcessId {

    /**
     * Процесс для получения данных по карточке взаимодействия воркера и задач
     */
    ReceiveWorkerTasksBagsControlCardData = 'RECEIVE_WORKER_TASKS_BAGS_CONTROL_DATA',

    /**
     * Процесс для изменения данных в карточке взаимодействия воркера и задач
     */
    ChangeWorkerTasksBags = 'CHANGE_WORKER_TASKS_BAGS'
}