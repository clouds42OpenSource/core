import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { TStatusItem } from 'app/api/endpoints/createDb/response';
import { EDatabaseState, EInformationBasesType } from 'app/api/endpoints/informationBases/enum';
import { TGetInformationBasesRequest } from 'app/api/endpoints/informationBases/request';
import { TGetExternalUser, TDatabaseAccesses, TGetAccessPrice, TGetAvailability, TGetInformationBaseInfoResponse, TInformationBaseItem, TInformationBasesList, TRateData } from 'app/api/endpoints/informationBases/response';
import { INFORMATION_BASES } from 'app/modules/informationBases/constants';
import { TSetParamsAction } from 'app/modules/informationBases/types';
import { TPaginationResponseDto } from 'app/web/common';

type TInformationBasesListSlice = {
    loading: boolean;
    error: string | null;
    informationBasesList: TInformationBasesList;
    otherInformationBasesList: TInformationBasesList;
    selectedDatabases: TInformationBaseItem[];
    valueCheck: string[];
    users: TGetExternalUser[];
    accessInfoDb: TDatabaseAccesses[];
    accessInfoInit: string[];
    informationBase?: TGetInformationBaseInfoResponse;
    availability?: TGetAvailability;
    params?: TGetInformationBasesRequest;
    otherParams?: TGetInformationBasesRequest;
    accessPrice?: TGetAccessPrice[];
    isOther?: boolean;
    rateData?: TRateData;
    clientServerAccessCost: number
};

const paginationInitialState: TPaginationResponseDto = {
    pageCount: 0,
    pageSize: 0,
    firstNumber: 0,
    currentPage: 0,
    lastNumber: 0,
    totalCount: 0
};

const initialState: TInformationBasesListSlice = {
    loading: false,
    error: null,
    informationBasesList: {
        records: [],
        pagination: paginationInitialState
    },
    otherInformationBasesList: {
        records: [],
        pagination: paginationInitialState
    },
    params: {
        type: 0
    },
    selectedDatabases: [],
    valueCheck: [],
    users: [],
    accessInfoDb: [],
    accessInfoInit: [],
    clientServerAccessCost: 0,
};

export const informationBasesList = createSlice({
    name: INFORMATION_BASES.informationBasesList,
    initialState,
    reducers: {
        setLoading(state, action: PayloadAction<boolean>) {
            state.loading = action.payload;
        },
        setError(state, action: PayloadAction<string | null>) {
            state.error = action.payload;
            state.loading = false;
        },
        getInformationBase(state, action: PayloadAction<TGetInformationBaseInfoResponse>) {
            state.informationBase = action.payload;
            state.error = null;
            state.loading = false;
        },
        setIsOther(state, action: PayloadAction<boolean>) {
            state.isOther = action.payload;
        },
        closeInformationBase(state) {
            state.informationBase = undefined;
            state.error = null;
            state.loading = false;
            state.valueCheck = [];
            state.users = [];
            state.accessInfoDb = [];
            state.accessInfoInit = [];
            state.accessPrice = undefined;
            state.isOther = undefined;
        },
        getInformationBasesList(state, action: PayloadAction<TInformationBasesList>) {
            state.informationBasesList = action.payload;
            state.error = null;
            state.loading = false;
        },
        getOtherInformationBasesList(state, action: PayloadAction<TInformationBasesList>) {
            state.otherInformationBasesList = action.payload;
            state.error = null;
            state.loading = false;
        },
        getAvailability(state, action: PayloadAction<TGetAvailability>) {
            state.availability = action.payload;
            state.error = null;
            state.loading = false;
        },
        setParams(state, { payload: { field, value } }: PayloadAction<TSetParamsAction>) {
            state[field] = {
                ...state[field],
                ...value
            };
        },
        setSearch(state, action: PayloadAction<string>) {
            state.params = { ...state.params, search: action.payload, page: 1 };
            state.otherParams = { ...state.otherParams, search: action.payload, page: 1 };
        },
        setType(state, action: PayloadAction<EInformationBasesType>) {
            state.params = { ...state.params, type: action.payload, page: 1 };
            state.otherParams = { ...state.otherParams, type: action.payload, page: 1 };
        },
        setSelectedDatabases(state, action: PayloadAction<TInformationBaseItem[]>) {
            state.selectedDatabases = action.payload;
        },
        updateDatabasesStates(state, action: PayloadAction<TInformationBaseItem[]>) {
            state.informationBasesList.records = state.informationBasesList.records.map(database => {
                const updateDatabase = action.payload.find(updateDatabaseItem => updateDatabaseItem.id === database.id);

                if (updateDatabase) {
                    database.state = updateDatabase.state;
                    database.publishState = updateDatabase.publishState;
                    database.webPublishPath = updateDatabase.webPublishPath;
                    database.needShowWebLink = updateDatabase.needShowWebLink;
                }

                return database;
            });
        },
        updateDatabaseOnDelimitersStates(state, action: PayloadAction<TStatusItem[]>) {
            state.informationBasesList.records = state.informationBasesList.records.map(database => {
                const updateDatabase = action.payload.find(updateDatabaseItem => updateDatabaseItem.id === database.id);

                if (updateDatabase) {
                    if (updateDatabase.state !== 'Используется') {
                        database.createStatus = updateDatabase;
                    } else {
                        database.createStatus = undefined;
                        database.state = EDatabaseState.Ready;
                    }
                }

                return database;
            });
        },
        changeValueCheck(state, action: PayloadAction<string[]>) {
            state.valueCheck = action.payload;
        },
        changeUsersList(state, action: PayloadAction<TGetExternalUser[]>) {
            state.users = action.payload;
        },
        changeAccessInfoDb(state, action: PayloadAction<TDatabaseAccesses[]>) {
            state.accessInfoDb = action.payload;
        },
        addRateData(state, action: PayloadAction<TRateData & { clientServerAccessCost: number }>) {
            state.rateData = action.payload;
            state.clientServerAccessCost = action.payload.clientServerAccessCost;
        },
        setInitialAccess(state, action: PayloadAction<string[]>) {
            state.accessInfoInit = action.payload;
        },
        onResetAddUser(state, action: PayloadAction<{ isClose?: boolean }>) {
            state.valueCheck = state.accessInfoInit;
            if (action.payload) {
                state.users = [];
            }
            state.accessPrice = undefined;
        },
        updateAccessPrice(state, action: PayloadAction<TGetAccessPrice[]>) {
            state.accessPrice = action.payload;
        }
    }
});

export default informationBasesList.reducer;