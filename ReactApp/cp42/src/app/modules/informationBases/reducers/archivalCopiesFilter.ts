import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { INFORMATION_BASES } from 'app/modules/informationBases/constants';
import { TSetParamsArchivalFilter } from 'app/modules/informationBases/types';

type TArchivalCopiesFilterSlice = {
    from: Date | null;
    to: Date | null;
};

const initialState: TArchivalCopiesFilterSlice = {
    from: null,
    to: null
};

export const archivalCopiesFilter = createSlice({
    name: INFORMATION_BASES.archivalCopiesFilter,
    initialState,
    reducers: {
        changeFilter(state, { payload: { field, value } }: PayloadAction<TSetParamsArchivalFilter>) {
            state[field] = value;
        },
        resetFilter(state) {
            state.from = null;
            state.to = null;
        }
    }
});

export default archivalCopiesFilter.reducer;