import informationBasesList from 'app/modules/informationBases/reducers/informationBasesList';
import archivalCopiesFilter from 'app/modules/informationBases/reducers/archivalCopiesFilter';
import { combineReducers } from 'redux';

export const InformationBasesReducer = combineReducers({
    informationBasesList,
    archivalCopiesFilter
});