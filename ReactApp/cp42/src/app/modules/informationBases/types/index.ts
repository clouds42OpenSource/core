import { TGetInformationBasesRequest } from 'app/api/endpoints/informationBases/request';

export type TSetParamsAction = {
    field: 'params' | 'otherParams';
    value: TGetInformationBasesRequest;
};

export type TSetParamsArchivalFilter = {
    field: 'from' | 'to';
    value: Date;
};