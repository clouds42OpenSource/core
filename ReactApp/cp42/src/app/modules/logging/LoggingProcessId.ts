/**
 * Процесы логирования
 */
export enum LoggingProcessId {
    /**
     * Процесс получить логирование запуска баз и RDP
     */
    ReceiveLaunchDbAndRdpLog = 'RECEIVE_LAUNCH_DB_AND_RDP_LOG',

    /**
     * Процесс получить логирование дейсвий облака
     */
    ReceiveCloudChanges = 'Receive_Cloud_Changes',

    /**
     * Процесс получить действия логирования облака
     */
    ReceiveCloudChangesActionsData = 'Receive_Cloud_Changes_Actions_Data'
}