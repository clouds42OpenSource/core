import { ReceiveLaunchDbAndRdpActionFailedPayload, ReceiveLaunchDbAndRdpActionStartPayload, ReceiveLaunchDbAndRdpActionSuccessPayload } from 'app/modules/logging/store/reducers/receiveLaunchDbAndRdpReducer/payloads';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';

import { LoggingActions } from 'app/modules/logging/store/actions';
import { ReceiveLaunchDbAndRdpLogThunkParams } from 'app/modules/logging/store/reducers/receiveLaunchDbAndRdpReducer/params';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = ReceiveLaunchDbAndRdpActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = ReceiveLaunchDbAndRdpActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = ReceiveLaunchDbAndRdpActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = ReceiveLaunchDbAndRdpLogThunkParams;

const { ReceiveLaunchDbAndRdpLog: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = LoggingActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для запроса списка записей логирования запуска баз и открытия RDP
 */
export class ReceiveLaunchDbAndRdpLogThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new ReceiveLaunchDbAndRdpLogThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        const loggingState = args.getStore().LoggingState;
        return {
            condition: !loggingState.hasSuccessFor.hasDbLaunchAndRdpLogReceived || args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().LoggingState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        try {
            const requestArgs = args.inputParams!;
            const loggingApi = InterlayerApiProxy.getLoggingApi();

            const logResponse = await loggingApi.receiveLaunchDbAndRdpLog(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, {
                pageNumber: requestArgs.pageNumber,
                filter: requestArgs.filter,
                orderBy: requestArgs.orderBy
            });

            args.success({
                log: logResponse.records,
                metadata: logResponse.metadata
            });
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}