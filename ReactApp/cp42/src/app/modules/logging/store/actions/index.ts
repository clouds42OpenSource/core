import { LoggingProcessId } from 'app/modules/logging';
import { LoggingConsts } from 'app/modules/logging/constants';
import { createReducerActions } from 'core/redux/functions';

/**
 * Все доступные actions дле редюсера Logging
 */
export const LoggingActions = {
    /**
     * Набор action на получения записей логирования запуск баз и RDP
     */
    ReceiveLaunchDbAndRdpLog: createReducerActions(LoggingConsts.reducerName, LoggingProcessId.ReceiveLaunchDbAndRdpLog),

    /**
     * Набор action на получение логирования дейсвий облака
     */
    ReceiveCloudChanges: createReducerActions(LoggingConsts.reducerName, LoggingProcessId.ReceiveCloudChanges),

    /**
     * Набор action на получение действий логирования облака
     */
    ReceiveCloudChangesActionsData: createReducerActions(LoggingConsts.reducerName, LoggingProcessId.ReceiveCloudChangesActionsData)
};