import { MetadataModel } from 'app/web/common/data-models';
import { CloudChangesItemDataModel } from 'app/web/InterlayerApiProxy/LoggingApiProxy/receiveCloudChanges/data-models';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения списка записей логирования действий
 */
export type ReceiveCloudChangesActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения списка записей логирования действий
 */
export type ReceiveCloudChangesActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения списка записей логирования действий
 */
export type ReceiveCloudChangesActionSuccessPayload = ReducerActionSuccessPayload & {

    /**
     * Записи логирования дейстаий
     */
    log: CloudChangesItemDataModel[];

    /**
     * Информация о пагинации
     */
    metadata: MetadataModel;
};

/**
 * Все возможные Action при получении списка записей логирования действий
 */
export type ReceiveCloudChangesActionAnyPayload =
    | ReceiveCloudChangesActionStartPayload
    | ReceiveCloudChangesActionFailedPayload
    | ReceiveCloudChangesActionSuccessPayload;