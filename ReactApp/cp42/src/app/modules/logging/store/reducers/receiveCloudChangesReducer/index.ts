import { reducerActionState } from 'app/common/functions/reducerActionState';
import { LoggingProcessId } from 'app/modules/logging/LoggingProcessId';
import { LoggingActions } from 'app/modules/logging/store/actions';
import { LoggingReducerState } from 'app/modules/logging/store/reducers/LoggingReducerState';
import { ReceiveCloudChangesActionAnyPayload, ReceiveCloudChangesActionSuccessPayload } from 'app/modules/logging/store/reducers/receiveCloudChangesReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения записей логирования действий облака
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const receiveCloudChangesReducer: TPartialReducerObject<LoggingReducerState, ReceiveCloudChangesActionAnyPayload> =
    (state, action) => {
        const processId = LoggingProcessId.ReceiveCloudChanges;
        const actions = LoggingActions.ReceiveCloudChanges;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as ReceiveCloudChangesActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    cloudChanges: {
                        ...state.cloudChanges,
                        items: [...payload.log],
                        metadata: { ...payload.metadata }
                    },
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasCloudChangesReceived: true
                    }
                });
            }
        });
    };