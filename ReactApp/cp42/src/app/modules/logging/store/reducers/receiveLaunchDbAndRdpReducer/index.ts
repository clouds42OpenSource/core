import { reducerActionState } from 'app/common/functions/reducerActionState';
import { LoggingProcessId } from 'app/modules/logging';
import { LoggingActions } from 'app/modules/logging/store/actions';
import { LoggingReducerState } from 'app/modules/logging/store/reducers';
import { ReceiveLaunchDbAndRdpActionAnyPayload, ReceiveLaunchDbAndRdpActionSuccessPayload } from 'app/modules/logging/store/reducers/receiveLaunchDbAndRdpReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения записей логирования запуска баз и старта RDP
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const receiveLaunchDbAndRdpLogReducer: TPartialReducerObject<LoggingReducerState, ReceiveLaunchDbAndRdpActionAnyPayload> =
    (state, action) => {
        const processId = LoggingProcessId.ReceiveLaunchDbAndRdpLog;
        const actions = LoggingActions.ReceiveLaunchDbAndRdpLog;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as ReceiveLaunchDbAndRdpActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    launchDbAndRdpLog: {
                        ...state.launchDbAndRdpLog,
                        items: [...payload.log],
                        metadata: { ...payload.metadata }
                    },
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasDbLaunchAndRdpLogReceived: true
                    }
                });
            }
        });
    };