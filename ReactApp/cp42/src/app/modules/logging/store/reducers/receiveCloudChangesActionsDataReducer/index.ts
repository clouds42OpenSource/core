import { reducerActionState } from 'app/common/functions/reducerActionState';
import { LoggingProcessId } from 'app/modules/logging/LoggingProcessId';
import { LoggingActions } from 'app/modules/logging/store/actions';
import { LoggingReducerState } from 'app/modules/logging/store/reducers/LoggingReducerState';
import { ReceiveCloudChangesActionsDataActionAnyPayload, ReceiveCloudChangesActionsDataActionSuccessPayload } from 'app/modules/logging/store/reducers/receiveCloudChangesActionsDataReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения записи в CloudChangesActionsData
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const receiveCloudChangesActionsDataReducer: TPartialReducerObject<LoggingReducerState, ReceiveCloudChangesActionsDataActionAnyPayload> =
    (state, action) => {
        const processId = LoggingProcessId.ReceiveCloudChangesActionsData;
        const actions = LoggingActions.ReceiveCloudChangesActionsData;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as ReceiveCloudChangesActionsDataActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    cloudChangesActions: {
                        ...state.cloudChangesActions,
                        items: [...payload.items]
                    },
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasCloudChangesActionsReceived: true
                    }
                });
            }
        });
    };