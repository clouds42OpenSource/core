import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';
import { CloudChangesActionDataModel } from 'app/web/InterlayerApiProxy/LoggingApiProxy/receiveCloudChangesActionsData/data-models';

/**
 * Данные в редюсер при старте выполнения получения списка действий облака для логирования
 */
export type ReceiveCloudChangesActionsDataActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения списка действий облака для логирования
 */
export type ReceiveCloudChangesActionsDataActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения списка действий облака для логирования
 */
export type ReceiveCloudChangesActionsDataActionSuccessPayload = ReducerActionSuccessPayload & {

    /**
     * Массив данных по действиям облака для логирования
     */
    items: CloudChangesActionDataModel[]
};

/**
 * Все возможные Action при получении списка действий облака для логирования
 */
export type ReceiveCloudChangesActionsDataActionAnyPayload =
    | ReceiveCloudChangesActionsDataActionStartPayload
    | ReceiveCloudChangesActionsDataActionFailedPayload
    | ReceiveCloudChangesActionsDataActionSuccessPayload;