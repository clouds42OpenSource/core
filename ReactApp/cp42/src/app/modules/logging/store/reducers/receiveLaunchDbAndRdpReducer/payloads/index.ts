import { MetadataModel } from 'app/web/common/data-models';
import { LaunchDbAndRdpLogItemDataModel } from 'app/web/InterlayerApiProxy/LoggingApiProxy/receiveLaunchDbAndRdpLog';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения списка записей логирования запуска баз и RDP
 */
export type ReceiveLaunchDbAndRdpActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения списка записей логирования запуска баз и RDP
 */
export type ReceiveLaunchDbAndRdpActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения списка записей логирования запуска баз и RDP
 */
export type ReceiveLaunchDbAndRdpActionSuccessPayload = ReducerActionSuccessPayload & {

    /**
     * Записи логирования запуска баз и открытия RDP
     */
    log: LaunchDbAndRdpLogItemDataModel[];

    /**
     * Информация о страницах логирования запуска баз и старта RDP по выбранному фильтру
     */
    metadata: MetadataModel;
};

/**
 * Все возможные Action при получении списка записей логирования запуска баз и старта RDP
 */
export type ReceiveLaunchDbAndRdpActionAnyPayload =
    | ReceiveLaunchDbAndRdpActionStartPayload
    | ReceiveLaunchDbAndRdpActionSuccessPayload
    | ReceiveLaunchDbAndRdpActionFailedPayload;