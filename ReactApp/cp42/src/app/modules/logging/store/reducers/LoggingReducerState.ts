import { LoggingProcessId } from 'app/modules/logging';
import { MetadataModel } from 'app/web/common/data-models';
import { CloudChangesItemDataModel } from 'app/web/InterlayerApiProxy/LoggingApiProxy/receiveCloudChanges/data-models';
import { CloudChangesActionDataModel } from 'app/web/InterlayerApiProxy/LoggingApiProxy/receiveCloudChangesActionsData/data-models';
import { LaunchDbAndRdpLogItemDataModel } from 'app/web/InterlayerApiProxy/LoggingApiProxy/receiveLaunchDbAndRdpLog';
import { IReducerState } from 'core/redux/interfaces';

/**
 * Состояние редюсера для работы с логированием
 */
export type LoggingReducerState = IReducerState<LoggingProcessId> & {

    /**
     * Логи запуска базы и RDP
     */
    launchDbAndRdpLog: {
        /**
         * Массив записей логирования запуска баз и старта RDP
         */
        items: LaunchDbAndRdpLogItemDataModel[];

        /**
         * Данные о странице для текущих записей логирования запуска баз и старта RDP по фильтру
         */
        metadata: MetadataModel;
    },

    /**
     * Логи действий облака
     */
    cloudChanges: {

        /**
         * Массив данных по логам действий облака
         */
        items: CloudChangesItemDataModel[],

        /**
         * Данные о странице для текущих записей логирования действий
         */
        metadata: MetadataModel;
    },

    /**
     * Действия логирования
     */
    cloudChangesActions: {

        /**
         * Массив данных по действиям облака для логирования
         */
        items: CloudChangesActionDataModel[]
    }

    /**
     * содержит ярлыки для фиксации загрузок данных, была ли загрузка или ещё нет
     */
    hasSuccessFor: {
        /**
         * Если true, то логирование запуска баз и открытия RDP уже был получен
         */
        hasDbLaunchAndRdpLogReceived: boolean,

        /**
         * Если true, то логирование действий уже было получено
         */
        hasCloudChangesReceived: boolean,

        /**
         * Если true, то действия облака уже были получены
         */
        hasCloudChangesActionsReceived: boolean
    };
};