import { ReceiveCloudChangesParams } from 'app/web/InterlayerApiProxy/LoggingApiProxy/receiveCloudChanges';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров на получение логирования действий облака
 */
export type ReceiveCloudChangesThunkParams = IForceThunkParam & ReceiveCloudChangesParams;