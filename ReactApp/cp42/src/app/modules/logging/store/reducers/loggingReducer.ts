import { getInitialMetadata } from 'app/common/functions';
import { LoggingConsts } from 'app/modules/logging/constants';
import { LoggingReducerState } from 'app/modules/logging/store/reducers';
import { receiveCloudChangesActionsDataReducer } from 'app/modules/logging/store/reducers/receiveCloudChangesActionsDataReducer';
import { receiveCloudChangesReducer } from 'app/modules/logging/store/reducers/receiveCloudChangesReducer';
import { receiveLaunchDbAndRdpLogReducer } from 'app/modules/logging/store/reducers/receiveLaunchDbAndRdpReducer';
import { createReducer, initReducerState } from 'core/redux/functions';

/**
 * Начальное состояние редюсера Logging
 */
const initialState = initReducerState<LoggingReducerState>(
    LoggingConsts.reducerName,
    {
        launchDbAndRdpLog: {
            items: [],
            metadata: getInitialMetadata(),
        },
        cloudChanges: {
            items: [],
            metadata: getInitialMetadata()
        },
        cloudChangesActions: {
            items: []
        },
        hasSuccessFor: {
            hasDbLaunchAndRdpLogReceived: false,
            hasCloudChangesActionsReceived: false,
            hasCloudChangesReceived: false
        }
    }
);

/**
 * Объединённые части редюсера для всего состояния Logging
 */
const partialReducers = [
    receiveLaunchDbAndRdpLogReducer,
    receiveCloudChangesReducer,
    receiveCloudChangesActionsDataReducer
];

/**
 * Редюсер Logging
 */
export const loggingReducer = createReducer(initialState, partialReducers);