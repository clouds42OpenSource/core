import { ReceiveLaunchDbAndRdpLogParams } from 'app/web/InterlayerApiProxy/LoggingApiProxy/receiveLaunchDbAndRdpLog';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров на получение логирования о запуске баз и RDP
 */
export type ReceiveLaunchDbAndRdpLogThunkParams = IForceThunkParam & ReceiveLaunchDbAndRdpLogParams;