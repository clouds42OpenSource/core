import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { TGetInstallLimitsList } from 'app/api/endpoints/link42/request';

import { TInstallLimitsList } from 'app/api/endpoints/link42/response';
import { TDataLightReturn } from 'app/api/types';
import { generateUniqueId } from 'app/common/functions';

import { INSTALL_LIMITS_SLICE_NAMES } from '../constants';

const initialState: TDataLightReturn<TInstallLimitsList> & { params: TGetInstallLimitsList } = {
    params: {},
    data: null,
    error: null,
    isLoading: false
};

export const getInstallLimitsListSlice = createSlice({
    name: INSTALL_LIMITS_SLICE_NAMES.getInstallLimitsList,
    initialState,
    reducers: {
        saveParams(state, action: PayloadAction<TGetInstallLimitsList>) {
            state.params = action.payload;
        },
        loading(state) {
            state.isLoading = true;
        },
        empty(state) {
            state.isLoading = false;
            state.data = null;
            state.error = null;
        },
        success(state, action: PayloadAction<TInstallLimitsList>) {
            action.payload.records.forEach(record => {
                record.bitDepths.forEach(bitDepth => {
                    bitDepth.id = generateUniqueId('bit-depth');
                });
            });

            state.data = action.payload;
        },
        error(state, action: PayloadAction<string>) {
            state.error = action.payload;
        }
    },
});

export default getInstallLimitsListSlice.reducer;