import { combineReducers } from 'redux';

import installLimitsListReducer from './getInstallLimitsListSlice';

export const Link42Reducer = combineReducers({
    installLimitsListReducer,
});