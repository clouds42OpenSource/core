import { PrintedHtmlFormsReferenceActions } from 'app/modules/printedHtmlFormsReference/store/actions';
import { GetPrintedHtmlFormByIdThunkParams } from 'app/modules/printedHtmlFormsReference/store/reducers/getPrintedHtmlFormByIdReducer/params';
import { GetPrintedHtmlFormByIdActionFailedPayload, GetPrintedHtmlFormByIdActionStartPayload, GetPrintedHtmlFormByIdActionSuccessPayload } from 'app/modules/printedHtmlFormsReference/store/reducers/getPrintedHtmlFormByIdReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = GetPrintedHtmlFormByIdActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = GetPrintedHtmlFormByIdActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = GetPrintedHtmlFormByIdActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = GetPrintedHtmlFormByIdThunkParams;

const { GetPrintedHtmlFormById: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = PrintedHtmlFormsReferenceActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для получения печатной формы HTML по ID
 */
export class GetPrintedHtmlFormByIdThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new GetPrintedHtmlFormByIdThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        return {
            condition: args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().PrintedHtmlFormsState
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        const api = InterlayerApiProxy.getPrintedHtmlFormApiProxy();

        try {
            const response = await api.getPrintedHtmlFormById(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, args.inputParams!.printedHtmlId);
            args.success(response);
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}