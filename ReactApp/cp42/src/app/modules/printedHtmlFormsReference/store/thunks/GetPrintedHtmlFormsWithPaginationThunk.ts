import { PrintedHtmlFormsReferenceActions } from 'app/modules/printedHtmlFormsReference/store/actions';
import { GetPrintedHtmlFormsWithPaginationThunkParams } from 'app/modules/printedHtmlFormsReference/store/reducers/getPrintedHtmlFormsWithPaginationReducer/params';
import {
    GetPrintedHtmlFormsWithPaginationActionFailedPayload,
    GetPrintedHtmlFormsWithPaginationActionStartPayload,
    GetPrintedHtmlFormsWithPaginationActionSuccessPayload
} from 'app/modules/printedHtmlFormsReference/store/reducers/getPrintedHtmlFormsWithPaginationReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = GetPrintedHtmlFormsWithPaginationActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = GetPrintedHtmlFormsWithPaginationActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = GetPrintedHtmlFormsWithPaginationActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = GetPrintedHtmlFormsWithPaginationThunkParams;

const { GetPrintedHtmlForms: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = PrintedHtmlFormsReferenceActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для получения данныx шаблонов писем
 */
export class GetPrintedHtmlFormsWithPaginationThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new GetPrintedHtmlFormsWithPaginationThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        return {
            condition: args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().PrintedHtmlFormsState
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        const api = InterlayerApiProxy.getPrintedHtmlFormApiProxy();
        try {
            const response = await api.getPrintedHtmlFormsWithPagination(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, args.inputParams!);
            args.success({ ...response, searchQuery: args.inputParams?.filter?.name });
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}