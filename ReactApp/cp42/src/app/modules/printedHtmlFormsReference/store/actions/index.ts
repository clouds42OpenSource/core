import { PrintedHtmlFormsReferenceProccessId } from 'app/modules/printedHtmlFormsReference';
import { PrintedHtmlFormsReferenceConsts } from 'app/modules/printedHtmlFormsReference/constants';
import { createReducerActions } from 'core/redux/functions';

/**
 * Все доступные actions дле редюсера PrintedHtmlForms
 */
export const PrintedHtmlFormsReferenceActions = {
    /**
     * Набор action на получение печатные формы HTML
     */
    GetPrintedHtmlForms: createReducerActions(PrintedHtmlFormsReferenceConsts.reducerName, PrintedHtmlFormsReferenceProccessId.GetPrintedHtmlForms),

    /**
     * Набор action на получение типов моделей
     */
    GetModelTypes: createReducerActions(PrintedHtmlFormsReferenceConsts.reducerName, PrintedHtmlFormsReferenceProccessId.GetModelTypes),

    /**
     * Набор action на создание печатной формы HTML
     */
    CreatePrintedHtmlForm: createReducerActions(PrintedHtmlFormsReferenceConsts.reducerName, PrintedHtmlFormsReferenceProccessId.CreatePrintedHtmlForm),

    /**
     * Набор action на получение печатной формы HTML по ID
     */
    GetPrintedHtmlFormById: createReducerActions(PrintedHtmlFormsReferenceConsts.reducerName, PrintedHtmlFormsReferenceProccessId.GetPrintedHtmlFormById),

    /**
     * Набор action на редактирование печатной формы HTML по ID
     */
    EditPrintedHtmlForm: createReducerActions(PrintedHtmlFormsReferenceConsts.reducerName, PrintedHtmlFormsReferenceProccessId.EditPrintedHtmlForm),

    /**
     * Набор action на скачивание тестового шаблона печатной формы HTML
     */
    DownloadDocumentWithTestData: createReducerActions(PrintedHtmlFormsReferenceConsts.reducerName, PrintedHtmlFormsReferenceProccessId.DownloadDocumentWithTestData)
};