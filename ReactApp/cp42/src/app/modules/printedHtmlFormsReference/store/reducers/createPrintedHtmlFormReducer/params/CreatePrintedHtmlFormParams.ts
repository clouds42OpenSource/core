import { CreatePrintedHtmlFormDataModel } from 'app/web/InterlayerApiProxy/PrintedHtmlFormApiProxy/createPrintedHtmlForm/data-models';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Тип параметра для создания печатной формы HTML
 */
export type CreatePrintedHtmlFormParams = IForceThunkParam & CreatePrintedHtmlFormDataModel;