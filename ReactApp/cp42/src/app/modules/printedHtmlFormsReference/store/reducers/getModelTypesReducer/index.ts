import { reducerActionState } from 'app/common/functions/reducerActionState';
import { PrintedHtmlFormsReferenceProccessId } from 'app/modules/printedHtmlFormsReference';
import { PrintedHtmlFormsReferenceActions } from 'app/modules/printedHtmlFormsReference/store/actions';
import { GetModelTypesActionAnyPayload, GetModelTypesActionSuccessPayload } from 'app/modules/printedHtmlFormsReference/store/reducers/getModelTypesReducer/payloads';
import { PrintedHtmlFormsState } from 'app/modules/printedHtmlFormsReference/store/reducers/PrintedHtmlFormsState';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения типов моделей
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const GetModelTypesReducer: TPartialReducerObject<PrintedHtmlFormsState, GetModelTypesActionAnyPayload> =
    (state, action) => {
        const processId = PrintedHtmlFormsReferenceProccessId.GetModelTypes;
        const actions = PrintedHtmlFormsReferenceActions.GetModelTypes;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onStartProcessState: () => {
                return reducerStateProcessStart(state, processId, {
                    modelTypes: []
                });
            },
            onSuccessProcessState: () => {
                const payload = action.payload as GetModelTypesActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    modelTypes: payload
                });
            }
        });
    };