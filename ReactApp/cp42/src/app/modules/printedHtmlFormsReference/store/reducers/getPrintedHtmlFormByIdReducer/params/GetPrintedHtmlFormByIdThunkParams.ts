import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Параметры thunk для получения печатной формы HTML по ID
 */
export type GetPrintedHtmlFormByIdThunkParams = IForceThunkParam & {
    /**
     * ID печатной формы HTML
     */
    printedHtmlId: string;
};