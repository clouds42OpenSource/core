import { SelectDataResultMetadataModel } from 'app/web/common/data-models';
import { PrintedHtmlFormInfoDataModel } from 'app/web/InterlayerApiProxy/PrintedHtmlFormApiProxy/getPrintedHtmlFormsWithPagination/data-models';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения данных печатных форм HTML
 */
export type GetPrintedHtmlFormsWithPaginationActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения данных печатных форм HTML
 */
export type GetPrintedHtmlFormsWithPaginationActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения данных печатных форм HTML
 */
export type GetPrintedHtmlFormsWithPaginationActionSuccessPayload = ReducerActionSuccessPayload & SelectDataResultMetadataModel<PrintedHtmlFormInfoDataModel>;

/**
 * Все возможные Action при получении данных печатных форм HTML
 */
export type GetPrintedHtmlFormsWithPaginationActionAnyPayload =
    | GetPrintedHtmlFormsWithPaginationActionStartPayload
    | GetPrintedHtmlFormsWithPaginationActionFailedPayload
    | GetPrintedHtmlFormsWithPaginationActionSuccessPayload;