import { reducerActionState } from 'app/common/functions/reducerActionState';
import { PrintedHtmlFormsReferenceProccessId } from 'app/modules/printedHtmlFormsReference';
import { PrintedHtmlFormsReferenceActions } from 'app/modules/printedHtmlFormsReference/store/actions';
import { EditPrintedHtmlFormActionAnyPayload } from 'app/modules/printedHtmlFormsReference/store/reducers/editPrintedHtmlFormReducer/payloads';
import { PrintedHtmlFormsState } from 'app/modules/printedHtmlFormsReference/store/reducers/PrintedHtmlFormsState';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для редактирования печатных форм HTML
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const editPrintedHtmlFormReducer: TPartialReducerObject<PrintedHtmlFormsState, EditPrintedHtmlFormActionAnyPayload> =
    (state, action) => {
        const processId = PrintedHtmlFormsReferenceProccessId.EditPrintedHtmlForm;
        const actions = PrintedHtmlFormsReferenceActions.EditPrintedHtmlForm;

        return reducerActionState({
            action,
            actions,
            processId,
            state
        });
    };