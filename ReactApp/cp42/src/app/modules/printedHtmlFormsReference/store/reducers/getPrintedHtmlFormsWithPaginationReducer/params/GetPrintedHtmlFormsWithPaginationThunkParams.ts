import { ReceivePrintedHtmlFormFilterParams } from 'app/web/InterlayerApiProxy/PrintedHtmlFormApiProxy/getPrintedHtmlFormsWithPagination/input-params';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Параметры thunk для получения печатных форм HTML
 */
export type GetPrintedHtmlFormsWithPaginationThunkParams = IForceThunkParam & ReceivePrintedHtmlFormFilterParams;