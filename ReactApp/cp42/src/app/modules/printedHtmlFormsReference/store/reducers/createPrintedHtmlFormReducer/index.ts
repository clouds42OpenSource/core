import { reducerActionState } from 'app/common/functions/reducerActionState';
import { PrintedHtmlFormsReferenceProccessId } from 'app/modules/printedHtmlFormsReference';
import { PrintedHtmlFormsReferenceActions } from 'app/modules/printedHtmlFormsReference/store/actions';
import { CreatePrintedHtmlFormActionAnyPayload } from 'app/modules/printedHtmlFormsReference/store/reducers/createPrintedHtmlFormReducer/payloads';
import { PrintedHtmlFormsState } from 'app/modules/printedHtmlFormsReference/store/reducers/PrintedHtmlFormsState';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для создания печатных форм HTML
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const createPrintedHtmlFormReducer: TPartialReducerObject<PrintedHtmlFormsState, CreatePrintedHtmlFormActionAnyPayload> =
    (state, action) => {
        const processId = PrintedHtmlFormsReferenceProccessId.CreatePrintedHtmlForm;
        const actions = PrintedHtmlFormsReferenceActions.CreatePrintedHtmlForm;

        return reducerActionState({
            action,
            actions,
            processId,
            state
        });
    };