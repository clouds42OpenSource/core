import { EditPrintedHtmlFormDataModel } from 'app/web/InterlayerApiProxy/PrintedHtmlFormApiProxy/common/data-models';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Тип параметров для скачивания тестового шаблона печатных форм
 */
export type DownloadDocumentWithTestDataThunkParams = IForceThunkParam & EditPrintedHtmlFormDataModel;