import { getInitialMetadata, partialFillOf } from 'app/common/functions';
import { PrintedHtmlFormsReferenceConsts } from 'app/modules/printedHtmlFormsReference/constants';
import { createPrintedHtmlFormReducer } from 'app/modules/printedHtmlFormsReference/store/reducers/createPrintedHtmlFormReducer';
import { downloadDocumentWithTestDataReducer } from 'app/modules/printedHtmlFormsReference/store/reducers/downloadDocumentWithTestDataReducer';
import { editPrintedHtmlFormReducer } from 'app/modules/printedHtmlFormsReference/store/reducers/editPrintedHtmlFormReducer';
import { GetModelTypesReducer } from 'app/modules/printedHtmlFormsReference/store/reducers/getModelTypesReducer';
import { getPrintedHtmlFormByIdReducer } from 'app/modules/printedHtmlFormsReference/store/reducers/getPrintedHtmlFormByIdReducer';
import { getPrintedHtmlFormsWithPaginationReducer } from 'app/modules/printedHtmlFormsReference/store/reducers/getPrintedHtmlFormsWithPaginationReducer';
import { PrintedHtmlFormsState } from 'app/modules/printedHtmlFormsReference/store/reducers/PrintedHtmlFormsState';
import { EditPrintedHtmlFormDataModel } from 'app/web/InterlayerApiProxy/PrintedHtmlFormApiProxy/common/data-models';
import { createReducer, initReducerState } from 'core/redux/functions';

/**
 * Начальное состояние редюсера PrintedHtmlForms
 */
const initialState = initReducerState<PrintedHtmlFormsState>(
    PrintedHtmlFormsReferenceConsts.reducerName,
    {
        printedHtmlForms: {
            metadata: getInitialMetadata(),
            records: [],
        },
        modelTypes: [],
        printedHtmlForm: partialFillOf<EditPrintedHtmlFormDataModel>(),
        hasSuccessFor: {
            hasPrintedHtmlFormsReceived: false
        }
    }
);

/**
 * Объединённые части редюсера для всего состояния PrintedHtmlForms
 */
const partialReducers = [
    getPrintedHtmlFormsWithPaginationReducer,
    GetModelTypesReducer,
    createPrintedHtmlFormReducer,
    getPrintedHtmlFormByIdReducer,
    editPrintedHtmlFormReducer,
    downloadDocumentWithTestDataReducer
];

/**
 * Редюсер PrintedHtmlForms
 */
export const PrintedHtmlFormsReducer = createReducer(initialState, partialReducers);