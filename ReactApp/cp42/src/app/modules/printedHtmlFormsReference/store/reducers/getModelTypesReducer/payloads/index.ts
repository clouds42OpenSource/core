import { KeyValueDataModel } from 'app/web/common/data-models';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения данных типов моделей
 */
export type GetModelTypesActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения данных типов моделей
 */
export type GetModelTypesActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения данных типов моделей
 */
export type GetModelTypesActionSuccessPayload = ReducerActionSuccessPayload & Array<KeyValueDataModel<string, string>>;

/**
 * Все возможные Action при получении данных типов моделей
 */
export type GetModelTypesActionAnyPayload =
    | GetModelTypesActionStartPayload
    | GetModelTypesActionFailedPayload
    | GetModelTypesActionSuccessPayload;