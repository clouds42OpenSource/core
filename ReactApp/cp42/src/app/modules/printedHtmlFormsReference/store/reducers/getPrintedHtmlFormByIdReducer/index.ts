import { partialFillOf } from 'app/common/functions';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { PrintedHtmlFormsReferenceProccessId } from 'app/modules/printedHtmlFormsReference';
import { PrintedHtmlFormsReferenceActions } from 'app/modules/printedHtmlFormsReference/store/actions';
import { GetPrintedHtmlFormByIdActionAnyPayload, GetPrintedHtmlFormByIdActionSuccessPayload } from 'app/modules/printedHtmlFormsReference/store/reducers/getPrintedHtmlFormByIdReducer/payloads';
import { PrintedHtmlFormsState } from 'app/modules/printedHtmlFormsReference/store/reducers/PrintedHtmlFormsState';
import { EditPrintedHtmlFormDataModel } from 'app/web/InterlayerApiProxy/PrintedHtmlFormApiProxy/common/data-models';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для полечения печатной форм HTML
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const getPrintedHtmlFormByIdReducer: TPartialReducerObject<PrintedHtmlFormsState, GetPrintedHtmlFormByIdActionAnyPayload> =
    (state, action) => {
        const processId = PrintedHtmlFormsReferenceProccessId.GetPrintedHtmlFormById;
        const actions = PrintedHtmlFormsReferenceActions.GetPrintedHtmlFormById;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onStartProcessState: () => {
                return reducerStateProcessStart(state, processId, {
                    printedHtmlForm: partialFillOf<EditPrintedHtmlFormDataModel>()
                });
            },
            onSuccessProcessState: () => {
                const payload = action.payload as GetPrintedHtmlFormByIdActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    printedHtmlForm: payload
                });
            }
        });
    };