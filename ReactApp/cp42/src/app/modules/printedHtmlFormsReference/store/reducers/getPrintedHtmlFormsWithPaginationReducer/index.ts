import { getInitialMetadata } from 'app/common/functions';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { PrintedHtmlFormsReferenceProccessId } from 'app/modules/printedHtmlFormsReference';
import { PrintedHtmlFormsReferenceActions } from 'app/modules/printedHtmlFormsReference/store/actions';
import { GetPrintedHtmlFormsWithPaginationActionAnyPayload, GetPrintedHtmlFormsWithPaginationActionSuccessPayload } from 'app/modules/printedHtmlFormsReference/store/reducers/getPrintedHtmlFormsWithPaginationReducer/payloads';
import { PrintedHtmlFormsState } from 'app/modules/printedHtmlFormsReference/store/reducers/PrintedHtmlFormsState';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения информации печатных форм HTML
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const getPrintedHtmlFormsWithPaginationReducer: TPartialReducerObject<PrintedHtmlFormsState, GetPrintedHtmlFormsWithPaginationActionAnyPayload> =
    (state, action) => {
        const processId = PrintedHtmlFormsReferenceProccessId.GetPrintedHtmlForms;
        const actions = PrintedHtmlFormsReferenceActions.GetPrintedHtmlForms;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onStartProcessState: () => {
                return reducerStateProcessStart(state, processId, {
                    printedHtmlForms: {
                        metadata: getInitialMetadata(),
                        records: []
                    },
                    hasSuccessFor: {
                        hasPrintedHtmlFormsReceived: false
                    }
                });
            },
            onSuccessProcessState: () => {
                const payload = action.payload as GetPrintedHtmlFormsWithPaginationActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    printedHtmlForms: payload,
                    hasSuccessFor: {
                        hasPrintedHtmlFormsReceived: true
                    }
                });
            }
        });
    };