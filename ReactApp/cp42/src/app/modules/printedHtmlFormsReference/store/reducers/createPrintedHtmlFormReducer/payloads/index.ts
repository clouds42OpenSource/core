import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения создания печатных форм HTML
 */
export type CreatePrintedHtmlFormActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении создания печатных форм HTML
 */
export type CreatePrintedHtmlFormActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении создания печатных форм HTML
 */
export type CreatePrintedHtmlFormActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при создании печатных форм HTML
 */
export type CreatePrintedHtmlFormActionAnyPayload =
    | CreatePrintedHtmlFormActionStartPayload
    | CreatePrintedHtmlFormActionFailedPayload
    | CreatePrintedHtmlFormActionSuccessPayload;