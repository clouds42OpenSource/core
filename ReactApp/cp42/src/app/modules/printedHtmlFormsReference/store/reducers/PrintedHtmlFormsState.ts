import { PrintedHtmlFormsReferenceProccessId } from 'app/modules/printedHtmlFormsReference';
import { KeyValueDataModel, SelectDataResultMetadataModel } from 'app/web/common/data-models';
import { EditPrintedHtmlFormDataModel } from 'app/web/InterlayerApiProxy/PrintedHtmlFormApiProxy/common/data-models';
import { PrintedHtmlFormInfoDataModel } from 'app/web/InterlayerApiProxy/PrintedHtmlFormApiProxy/getPrintedHtmlFormsWithPagination/data-models';
import { IReducerState } from 'core/redux/interfaces';

/**
 * Состояние редюсера для работы с процесами PrintedHtmlFormsReference
 */
export type PrintedHtmlFormsState = IReducerState<PrintedHtmlFormsReferenceProccessId> & {
    /**
     * Массив информаций печатной формы HTML с пагинацией
     */
    printedHtmlForms: SelectDataResultMetadataModel<PrintedHtmlFormInfoDataModel>,

    /**
     * Массив типов моделей
     */
    modelTypes: Array<KeyValueDataModel<string, string>>,

    /**
     * Печатная форма HTML
     */
    printedHtmlForm: EditPrintedHtmlFormDataModel,

    /**
     * содержит ярлыки для фиксации загрузок данных, была ли загрузка или ещё нет
     */
    hasSuccessFor: {
        /**
         * Если true, то печатные формы получены
         */
        hasPrintedHtmlFormsReceived: boolean,
    };
};