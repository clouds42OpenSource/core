import { EditPrintedHtmlFormDataModel } from 'app/web/InterlayerApiProxy/PrintedHtmlFormApiProxy/common/data-models';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Тип параметров для редактирования печатной формы HTML
 */
export type EditPrintedHtmlFormThunkParams = IForceThunkParam & EditPrintedHtmlFormDataModel;