import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения редактирования печатных форм HTML
 */
export type EditPrintedHtmlFormActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении редактирования печатных форм HTML
 */
export type EditPrintedHtmlFormActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении редактирования печатных форм HTML
 */
export type EditPrintedHtmlFormActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при редактировании печатных форм HTML
 */
export type EditPrintedHtmlFormActionAnyPayload =
    | EditPrintedHtmlFormActionStartPayload
    | EditPrintedHtmlFormActionFailedPayload
    | EditPrintedHtmlFormActionSuccessPayload;