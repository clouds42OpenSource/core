import { EditPrintedHtmlFormDataModel } from 'app/web/InterlayerApiProxy/PrintedHtmlFormApiProxy/common/data-models';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения данных печатных форм HTML по ID
 */
export type GetPrintedHtmlFormByIdActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения данных печатных форм HTML по ID
 */
export type GetPrintedHtmlFormByIdActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения данных печатных форм HTML по ID
 */
export type GetPrintedHtmlFormByIdActionSuccessPayload = ReducerActionSuccessPayload & EditPrintedHtmlFormDataModel;

/**
 * Все возможные Action при получении данных печатных форм HTML по ID
 */
export type GetPrintedHtmlFormByIdActionAnyPayload =
    | GetPrintedHtmlFormByIdActionStartPayload
    | GetPrintedHtmlFormByIdActionFailedPayload
    | GetPrintedHtmlFormByIdActionSuccessPayload;