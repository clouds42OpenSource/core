import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения скачивания тестового шаблона печатных форм HTML
 */
export type DownloadDocumentWithTestDataActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении скачивания тестового шаблона печатных форм HTML
 */
export type DownloadDocumentWithTestDataActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении скачивания тестового шаблона печатных форм HTML
 */
export type DownloadDocumentWithTestDataActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при скачивании тестового шаблона печатных форм HTML
 */
export type DownloadDocumentWithTestDataActionAnyPayload =
    | DownloadDocumentWithTestDataActionStartPayload
    | DownloadDocumentWithTestDataActionFailedPayload
    | DownloadDocumentWithTestDataActionSuccessPayload;