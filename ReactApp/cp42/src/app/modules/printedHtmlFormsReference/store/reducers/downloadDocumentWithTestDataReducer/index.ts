import { reducerActionState } from 'app/common/functions/reducerActionState';
import { PrintedHtmlFormsReferenceProccessId } from 'app/modules/printedHtmlFormsReference';
import { PrintedHtmlFormsReferenceActions } from 'app/modules/printedHtmlFormsReference/store/actions';
import { DownloadDocumentWithTestDataActionAnyPayload } from 'app/modules/printedHtmlFormsReference/store/reducers/downloadDocumentWithTestDataReducer/payloads';
import { PrintedHtmlFormsState } from 'app/modules/printedHtmlFormsReference/store/reducers/PrintedHtmlFormsState';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для скачивания тестового файла печатных форм HTML
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const downloadDocumentWithTestDataReducer: TPartialReducerObject<PrintedHtmlFormsState, DownloadDocumentWithTestDataActionAnyPayload> =
    (state, action) => {
        const processId = PrintedHtmlFormsReferenceProccessId.DownloadDocumentWithTestData;
        const actions = PrintedHtmlFormsReferenceActions.DownloadDocumentWithTestData;

        return reducerActionState({
            action,
            actions,
            processId,
            state
        });
    };