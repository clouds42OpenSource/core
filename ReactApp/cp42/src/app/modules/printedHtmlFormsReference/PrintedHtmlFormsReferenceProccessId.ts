/**
 * Процесы PrintedHtmlFormsReference
 */
export enum PrintedHtmlFormsReferenceProccessId {
    /**
     * Процесс на получение печатные формы HTML
     */
    GetPrintedHtmlForms = 'GET_PRINTED_HTML_FORMS',

    /**
     * Процесс на получение типов моделей
     */
    GetModelTypes = 'GET_MODEL_TYPES',

    /**
     * Процесс на создание печатной формы HTML
     */
    CreatePrintedHtmlForm = 'CREATE_PRINTED_HTML_FORM',

    /**
     * Процесс на получение печатной формы HTML по ID
     */
    GetPrintedHtmlFormById = 'GET_PRINTED_HTML_FORM_ID',

    /**
     * Процесс на редактирование печатной формы HTML
     */
    EditPrintedHtmlForm = 'EDIT_PRINTED_HTML_FORM',

    /**
     * Процесс на скачивание тестового шаблона печатной формы HTML
     */
    DownloadDocumentWithTestData = 'DOWNLOAD_DOCUMENT_WITH_TEST_DATA'
}