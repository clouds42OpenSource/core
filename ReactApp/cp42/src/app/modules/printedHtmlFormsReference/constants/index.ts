/**
 * Константы для модуля PrintedHtmlFormsReference
 */
export const PrintedHtmlFormsReferenceConsts = {
    /**
     * Название редюсера для модуля PrintedHtmlFormsReference
     */
    reducerName: 'PRINTED_HTML_FORMS_REFERENCE'
};