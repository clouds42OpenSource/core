/**
 * Процессы AdditionalSessionsProcessId
 */
export enum AdditionalSessionsProcessId {
    /**
     * Процесс получения списка сеансов
     */
    GetSessionList = 'GET_SESSION_LIST',
    /**
     * Процесс завершения сеанса
     */
    EndingUserSession = 'ENDING_USER_SESSION',
    /**
     * Процесс получения информации о купленных ресурсах
     */
    GetCloudServiceResources = 'GET_CLOUD_SERVICE_RESOURCES',
    /**
     * Процесс изменения или покупки дополнительных сеансов
     */
    ChangeCloudServiceResources = 'CHANGE_CLOUD_SERVICE_RESOURCES',
    /**
     * Процесс на покупку сервиса за счет собственных средств или за счет обещанного платежа
     */
    BuyCloudServiceResources = 'BUY_CLOUD_SERVICE_RESOURCES',
    /**
     * Процесс на обновление состояния при расчете стоимости дополнительных сеансов
     */
    ResetCloudServiceResources = 'RESET_CLOUD_SERVICE_RESOURCES'
}