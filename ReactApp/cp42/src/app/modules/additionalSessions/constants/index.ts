export const AdditionalSessionConsts = {
    /**
     * Название редюсера для дополнительных сеансов
     */
    reducerName: 'ADDITIONAL_SESSION'
};