import { AdditionalSessionsAction } from 'app/modules/additionalSessions/store/actions';
import { ChangeCloudServiceResourcesThunkParams } from 'app/modules/additionalSessions/store/reducers/ChangeCloudServiceResourcesReducer/params';
import { ChangeCloudServiceResourcesFailedPayload, ChangeCloudServiceResourcesStartPayload, ChangeCloudServiceResourcesSuccessPayload } from 'app/modules/additionalSessions/store/reducers/ChangeCloudServiceResourcesReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсере при вызове действия START_ACTION
 */
type TActionStartPayload = ChangeCloudServiceResourcesStartPayload;

/**
 * Тип данных в редюсере при вызове действия FAILED_ACTION
 */
type TActionFailedPayload = ChangeCloudServiceResourcesFailedPayload;

/**
 * Тип данных в редюсере при вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = ChangeCloudServiceResourcesSuccessPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = ChangeCloudServiceResourcesThunkParams;

const { ChangeCloudServiceResources: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = AdditionalSessionsAction;

/**
 * Тип параметров Thunk-и при старте выполнения
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметра выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для запроса получения данных о ресурсах
 */
export class ChangeCloudServiceResourcesThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new ChangeCloudServiceResourcesThunk().execute(args, args?.showLoadingProgress);
    }

    protected get startActionValue(): string { return START_ACTION; }

    protected get successActionValue(): string { return SUCCESS_ACTION; }

    protected get failedActionValue(): string { return FAILED_ACTION; }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        const store = args.getStore().AdditionalSessionState;
        return {
            condition: !store.hasSuccessFor.hasChangeCloudServiceResources || args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().AdditionalSessionState
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        try {
            const api = InterlayerApiProxy.getAdditionalSessionsApiProxy();
            const response = await api.changeCloudServiceResources(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, args.inputParams!);

            args.success({
                changeCloudServiceResources: response
            });
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}