import { AdditionalSessionsAction } from 'app/modules/additionalSessions/store/actions';
import { EndingUserSessionThunkParams } from 'app/modules/additionalSessions/store/reducers/EndingUserSessionReducer/params';
import { EndingUserSessionActionFailedPayload, EndingUserSessionActionStartPayload, EndingUserSessionActionSuccessPayload } from 'app/modules/additionalSessions/store/reducers/EndingUserSessionReducer/payload';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = EndingUserSessionActionStartPayload;

/**
 * Тип данных в редюсер при вызове действия FAILED_ACTION
 */
type TActionFailedPayload = EndingUserSessionActionFailedPayload;

/**
 * Тип данных в редюсере при вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = EndingUserSessionActionSuccessPayload;

/**
 * Тип параметра выполнения Thunk-и
 */
type TInputParams = EndingUserSessionThunkParams;

const { EndingUserSession: { START_ACTION, FAILED_ACTION, SUCCESS_ACTION } } = AdditionalSessionsAction;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для запроса завершения сеанса
 */
export class EndingUserSessionThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new EndingUserSessionThunk().execute(args, args?.showLoadingProgress);
    }

    protected get startActionValue(): string { return START_ACTION; }

    protected get successActionValue(): string { return SUCCESS_ACTION; }

    protected get failedActionValue(): string { return FAILED_ACTION; }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        return {
            condition: true,
            getReducerStateFunc: () => args.getStore().AdditionalSessionState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        const requestArgs = args.inputParams!;

        try {
            const api = InterlayerApiProxy.getAdditionalSessionsApiProxy();
            await api.endingUserSession(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, requestArgs);

            args.success({});
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
            window.location.href = `${ window.location.origin + window.location.pathname }?message=При завершении сеанса произошла ошибка. Попробуйте позже.`;
        }
    }
}