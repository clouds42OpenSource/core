import { createReducerActions } from 'core/redux/functions';
import { AdditionalSessionConsts } from 'app/modules/additionalSessions/constants';
import { AdditionalSessionsProcessId } from 'app/modules/additionalSessions/AdditionalSessionsProcessId';

/**
 * Все доступные actions для редюсера AdditionalSessions
 */
export const AdditionalSessionsAction = {
    /**
     * Набор actions на получение списка сеансов
     */
    GetSessionList: createReducerActions(AdditionalSessionConsts.reducerName, AdditionalSessionsProcessId.GetSessionList),

    /**
     * Набор actions на завершение сеанса пользователя
     */
    EndingUserSession: createReducerActions(AdditionalSessionConsts.reducerName, AdditionalSessionsProcessId.EndingUserSession),

    /**
     * Набор actions на получение информации о купленных ресурсах
     */
    GetCloudServiceResources: createReducerActions(AdditionalSessionConsts.reducerName, AdditionalSessionsProcessId.GetCloudServiceResources),

    /**
     * Набор actions на изменение или покупку дополнительных сеансов
     */
    ChangeCloudServiceResources: createReducerActions(AdditionalSessionConsts.reducerName, AdditionalSessionsProcessId.ChangeCloudServiceResources),

    /**
     * Набор actions на покупку сервиса за счет собственных средств или за счет обещанного платежа
     */
    BuyCloudServiceResources: createReducerActions(AdditionalSessionConsts.reducerName, AdditionalSessionsProcessId.BuyCloudServiceResources),

    /**
     * Набор actions на обновление цена дополнительных сеансов
     */
    ResetCloudServiceResources: createReducerActions(AdditionalSessionConsts.reducerName, AdditionalSessionsProcessId.ResetCloudServiceResources)
};