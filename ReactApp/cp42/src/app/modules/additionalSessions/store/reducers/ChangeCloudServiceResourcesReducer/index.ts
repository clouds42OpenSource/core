import { TPartialReducerObject } from 'core/redux/types';
import { additionalSessionsReducerState } from 'app/modules/additionalSessions/store/reducers';
import { ChangeCloudServiceResourcesAnyPayload, ChangeCloudServiceResourcesSuccessPayload } from 'app/modules/additionalSessions/store/reducers/ChangeCloudServiceResourcesReducer/payloads';
import { AdditionalSessionsProcessId } from 'app/modules/additionalSessions/AdditionalSessionsProcessId';
import { AdditionalSessionsAction } from 'app/modules/additionalSessions/store/actions';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { reducerStateProcessSuccess } from 'core/redux/functions';

export const changeCloudServiceResourcesReducer: TPartialReducerObject<additionalSessionsReducerState, ChangeCloudServiceResourcesAnyPayload> = (state, action) => {
    const processId = AdditionalSessionsProcessId.ChangeCloudServiceResources;
    const actions = AdditionalSessionsAction.ChangeCloudServiceResources;

    return reducerActionState({
        action,
        actions,
        processId,
        state,
        onSuccessProcessState: () => {
            const payload = action.payload as ChangeCloudServiceResourcesSuccessPayload;
            return reducerStateProcessSuccess(state, processId, {
                changeCloudServiceResources: payload.changeCloudServiceResources,
                hasSuccessFor: {
                    ...state.hasSuccessFor,
                    hasChangeCloudServiceResources: true
                }
            });
        }
    });
};