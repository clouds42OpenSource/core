import { TPartialReducerObject } from 'core/redux/types';
import { additionalSessionsReducerState } from 'app/modules/additionalSessions/store/reducers';
import { GetSessionListActionAnyPayload, GetSessionListActionSuccessPayload } from 'app/modules/additionalSessions/store/reducers/GetSessionListReducer/payloads';
import { AdditionalSessionsProcessId } from 'app/modules/additionalSessions/AdditionalSessionsProcessId';
import { AdditionalSessionsAction } from 'app/modules/additionalSessions/store/actions';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { reducerStateProcessSuccess } from 'core/redux/functions';

export const getSessionListReducer: TPartialReducerObject<additionalSessionsReducerState, GetSessionListActionAnyPayload> = (state, action) => {
    const processId = AdditionalSessionsProcessId.GetSessionList;
    const actions = AdditionalSessionsAction.GetSessionList;

    return reducerActionState({
        action,
        actions,
        processId,
        state,
        onSuccessProcessState: () => {
            const payload = action.payload as GetSessionListActionSuccessPayload;
            return reducerStateProcessSuccess(state, processId, {
                sessionsList: payload.sessionList,
                hasSuccessFor: {
                    ...state.hasSuccessFor,
                    hasSessionsListReceived: true
                }
            });
        }
    });
};