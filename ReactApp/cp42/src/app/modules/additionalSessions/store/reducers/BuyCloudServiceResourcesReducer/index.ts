import { TPartialReducerObject } from 'core/redux/types';
import { additionalSessionsReducerState } from 'app/modules/additionalSessions/store/reducers';
import { BuyCloudServiceResourcesAnyAction } from 'app/modules/additionalSessions/store/reducers/BuyCloudServiceResourcesReducer/payload';
import { AdditionalSessionsProcessId } from 'app/modules/additionalSessions/AdditionalSessionsProcessId';
import { AdditionalSessionsAction } from 'app/modules/additionalSessions/store/actions';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';

export const buyCloudServiceResourcesReducer: TPartialReducerObject<additionalSessionsReducerState, BuyCloudServiceResourcesAnyAction> = (state, action) => {
    const processId = AdditionalSessionsProcessId.BuyCloudServiceResources;
    const actions = AdditionalSessionsAction.BuyCloudServiceResources;

    return reducerActionState({
        action,
        actions,
        processId,
        state,
        onStartProcessState: () => {
            return reducerStateProcessStart(state, processId, {
                hasSuccessFor: {
                    ...state.hasSuccessFor,
                    hasBuyResources: false
                }
            });
        },
        onSuccessProcessState: () => {
            return reducerStateProcessSuccess(state, processId, {
                hasSuccessFor: {
                    ...state.hasSuccessFor,
                    hasBuyResources: true
                }
            });
        }
    });
};