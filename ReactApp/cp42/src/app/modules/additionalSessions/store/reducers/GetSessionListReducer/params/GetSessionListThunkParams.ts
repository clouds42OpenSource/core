import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';
import { GetSessionListParams } from 'app/web/InterlayerApiProxy/AdditionalSessionsApiProxy/getSessionList';

/**
 * Модель параметров для получения списка сеансов
 */
export type GetSessionListThunkParams = IForceThunkParam & GetSessionListParams;