import { IReducerState } from 'core/redux/interfaces';
import { AdditionalSessionsProcessId } from 'app/modules/additionalSessions/AdditionalSessionsProcessId';
import { SessionListDataModel } from 'app/web/InterlayerApiProxy/AdditionalSessionsApiProxy/getSessionList';
import { CloudServiceResourcesDataModel } from 'app/web/InterlayerApiProxy/AdditionalSessionsApiProxy/getCloudServiceResources';
import { ChangeCloudServiceResourcesDataModel } from 'app/web/InterlayerApiProxy/AdditionalSessionsApiProxy/changeCloudServiceResources';

/**
 * Состояние редюсера для работы с дополнительными сеансами
 */
export type additionalSessionsReducerState = IReducerState<AdditionalSessionsProcessId> & {
    /**
     * Список сеансов
     */
    sessionsList: SessionListDataModel;
    /**
     * Информация о купленных ресурсах
     */
    cloudServiceResources: CloudServiceResourcesDataModel;
    cloudServiceResourcesError?: string;
    /**
     * Информация об успешности изменения или покупки дополнительных сеансов
     */
    changeCloudServiceResources: ChangeCloudServiceResourcesDataModel;
    hasSuccessFor: {
        /**
         * Если true, то список сеансов получен
         */
        hasSessionsListReceived: boolean;
        /**
         * Если true, то информация о ресурсах получена
         */
        hasCloudServiceResources: boolean;
        /**
         * Если true, то статус изменения или покупки дополнительных сеансов получен
         */
        hasChangeCloudServiceResources: boolean;
        hasBuyResources: boolean;
    }
};