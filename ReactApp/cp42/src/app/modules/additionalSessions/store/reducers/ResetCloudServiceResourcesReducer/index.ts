import { TPartialReducerObject } from 'core/redux/types';
import { additionalSessionsReducerState } from 'app/modules/additionalSessions/store/reducers';
import { ResetCloudServiceResourcesAnyPayload } from 'app/modules/additionalSessions/store/reducers/ResetCloudServiceResourcesReducer/payloads';
import { AdditionalSessionsProcessId } from 'app/modules/additionalSessions/AdditionalSessionsProcessId';
import { AdditionalSessionsAction } from 'app/modules/additionalSessions/store/actions';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { reducerStateProcessSuccess } from 'core/redux/functions';

export const resetCloudServiceResourcesReducer: TPartialReducerObject<additionalSessionsReducerState, ResetCloudServiceResourcesAnyPayload> = (state, action) => {
    const processId = AdditionalSessionsProcessId.ResetCloudServiceResources;
    const actions = AdditionalSessionsAction.ResetCloudServiceResources;

    return reducerActionState({
        action,
        actions,
        processId,
        state,
        onSuccessProcessState: () => {
            return reducerStateProcessSuccess(state, processId, {
                hasSuccessFor: {
                    ...state.hasSuccessFor,
                    hasChangeCloudServiceResources: false
                }
            });
        }
    });
};