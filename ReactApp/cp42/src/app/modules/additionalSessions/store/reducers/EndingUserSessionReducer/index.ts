import { TPartialReducerObject } from 'core/redux/types';
import { additionalSessionsReducerState } from 'app/modules/additionalSessions/store/reducers';
import { EndingUserSessionActionAnyPayload } from 'app/modules/additionalSessions/store/reducers/EndingUserSessionReducer/payload';
import { AdditionalSessionsProcessId } from 'app/modules/additionalSessions/AdditionalSessionsProcessId';
import { AdditionalSessionsAction } from 'app/modules/additionalSessions/store/actions';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { reducerStateProcessSuccess } from 'core/redux/functions';

export const endingUserSessionReducer: TPartialReducerObject<additionalSessionsReducerState, EndingUserSessionActionAnyPayload> = (state, action) => {
    const processId = AdditionalSessionsProcessId.EndingUserSession;
    const actions = AdditionalSessionsAction.EndingUserSession;

    return reducerActionState({
        action,
        actions,
        processId,
        state,
        onSuccessProcessState: () => {
            return reducerStateProcessSuccess(state, processId, {});
        }
    });
};