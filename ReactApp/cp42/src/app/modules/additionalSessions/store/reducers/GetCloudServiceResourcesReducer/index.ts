import { TPartialReducerObject } from 'core/redux/types';
import { additionalSessionsReducerState } from 'app/modules/additionalSessions/store/reducers';
import { GetCloudServiceResourcesAnyPayload, GetCloudServiceResourcesSuccessPayload } from 'app/modules/additionalSessions/store/reducers/GetCloudServiceResourcesReducer/payloads';
import { AdditionalSessionsProcessId } from 'app/modules/additionalSessions/AdditionalSessionsProcessId';
import { AdditionalSessionsAction } from 'app/modules/additionalSessions/store/actions';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { reducerStateProcessSuccess } from 'core/redux/functions';

export const getCloudServiceResourcesReducer: TPartialReducerObject<additionalSessionsReducerState, GetCloudServiceResourcesAnyPayload> = (state, action) => {
    const processId = AdditionalSessionsProcessId.GetCloudServiceResources;
    const actions = AdditionalSessionsAction.GetCloudServiceResources;

    return reducerActionState({
        action,
        actions,
        processId,
        state,
        onSuccessProcessState: () => {
            const payload = action.payload as GetCloudServiceResourcesSuccessPayload;
            return reducerStateProcessSuccess(state, processId, {
                cloudServiceResources: payload.cloudServiceResources,
                cloudServiceResourcesError: payload.cloudServiceResourcesError,
                hasSuccessFor: {
                    ...state.hasSuccessFor,
                    hasCloudServiceResources: true
                }
            });
        }
    });
};