import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель на обновление стоимости дополнительных сеансов
 */
export type ResetCloudServiceResourcesThunkParams = IForceThunkParam;