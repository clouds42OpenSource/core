import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсере при старте завершении сеанса
 */
export type EndingUserSessionActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсере при неуспешном завершении сеанса
 */
export type EndingUserSessionActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсере при успешном завершении сеанса
 */
export type EndingUserSessionActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные actions при завершение сеанса
 */
export type EndingUserSessionActionAnyPayload =
    EndingUserSessionActionStartPayload |
    EndingUserSessionActionFailedPayload |
    EndingUserSessionActionSuccessPayload;