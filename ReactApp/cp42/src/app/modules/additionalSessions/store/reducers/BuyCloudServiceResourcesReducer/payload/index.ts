import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсере при старте покупки сервиса за счет собственных средств или за счет обещанного платежа
 */
export type BuyCloudServiceResourcesStartAction = ReducerActionStartPayload;

/**
 * Данные в редюсере при неуспешной покупки сервиса за счет собственных средств или за счет обещанного платежа
 */
export type BuyCloudServiceResourcesFailedAction = ReducerActionFailedPayload;

/**
 * Данные в редюсере при успешной покупки сервиса за счет собственных средств или за счет обещанного платежа
 */
export type BuyCloudServiceResourcesSuccessAction = ReducerActionSuccessPayload;

/**
 * Все возможные actions при покупке сервиса за счет собственных средств или за счет обещанного платежа
 */
export type BuyCloudServiceResourcesAnyAction =
    BuyCloudServiceResourcesStartAction |
    BuyCloudServiceResourcesFailedAction |
    BuyCloudServiceResourcesSuccessAction;