import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';
import { CloudServiceResourcesDataModel } from 'app/web/InterlayerApiProxy/AdditionalSessionsApiProxy/getCloudServiceResources';

/**
 * Данные в редюсере при старте получении данных о ресурсах
 */
export type GetCloudServiceResourcesStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсере при неуспешном получении данных о ресурсах
 */
export type GetCloudServiceResourcesFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсере при успешном получении данных о ресурсах
 */
export type GetCloudServiceResourcesSuccessPayload = ReducerActionSuccessPayload & {
    /**
     * Информация о ресурсах
     */
    cloudServiceResources?: CloudServiceResourcesDataModel;
    cloudServiceResourcesError?: string;
};

/**
 * Все возможные actions при получении данных о ресурсах
 */
export type GetCloudServiceResourcesAnyPayload =
    GetCloudServiceResourcesStartPayload |
    GetCloudServiceResourcesFailedPayload |
    GetCloudServiceResourcesSuccessPayload;