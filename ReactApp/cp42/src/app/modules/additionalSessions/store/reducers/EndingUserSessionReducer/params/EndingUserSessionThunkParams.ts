import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';
import { EndingUserSessionParams } from 'app/web/InterlayerApiProxy/AdditionalSessionsApiProxy/endingUserSession/input-params';

/**
 * Модель параметров для завершения сеанса
 */
export type EndingUserSessionThunkParams = IForceThunkParam & EndingUserSessionParams;