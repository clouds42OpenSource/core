import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для получения информации о ресурсах
 */
export type GetCloudServiceResourcesThunkParams = IForceThunkParam;