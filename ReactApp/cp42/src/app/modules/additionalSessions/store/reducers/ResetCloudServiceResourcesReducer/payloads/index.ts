import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсере при старте обновления стоимости дополнительных сеансов
 */
export type ResetCloudServiceResourcesStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсере при неуспешном обновлении стоимости дополнительных сеансов
 */
export type ResetCloudServiceResourcesFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсере при успешном обновлении стоимости дополнительных сеансов
 */
export type ResetCloudServiceResourcesSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные действия при обновлении стоимости дополнительных сеансов
 */
export type ResetCloudServiceResourcesAnyPayload =
    ResetCloudServiceResourcesStartPayload |
    ResetCloudServiceResourcesFailedPayload |
    ResetCloudServiceResourcesSuccessPayload;