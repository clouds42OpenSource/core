import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';
import { BuyCloudServiceResourcesParams } from 'app/web/InterlayerApiProxy/AdditionalSessionsApiProxy/buyCloudServiceResources/input-params';

/**
 * Модель параметров для покупки сервиса за счет собственных средств или за счет обещанного платежа
 */
export type BuyCloudServiceResourcesThunkParams = IForceThunkParam & BuyCloudServiceResourcesParams;