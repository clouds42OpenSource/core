import { createReducer, initReducerState } from 'core/redux/functions';
import { additionalSessionsReducerState } from 'app/modules/additionalSessions/store/reducers/AdditionalSessionsReducerState';
import { AdditionalSessionConsts } from 'app/modules/additionalSessions/constants';
import { getSessionListReducer } from 'app/modules/additionalSessions/store/reducers/GetSessionListReducer';
import { getCloudServiceResourcesReducer } from 'app/modules/additionalSessions/store/reducers/GetCloudServiceResourcesReducer';
import { endingUserSessionReducer } from 'app/modules/additionalSessions/store/reducers/EndingUserSessionReducer';
import { changeCloudServiceResourcesReducer } from 'app/modules/additionalSessions/store/reducers/ChangeCloudServiceResourcesReducer';
import { buyCloudServiceResourcesReducer } from 'app/modules/additionalSessions/store/reducers/BuyCloudServiceResourcesReducer';
import { resetCloudServiceResourcesReducer } from 'app/modules/additionalSessions/store/reducers/ResetCloudServiceResourcesReducer';

/**
 * Начальное состояние редюсера AdditionalSession
 */
const initialState = initReducerState<additionalSessionsReducerState>(
    AdditionalSessionConsts.reducerName,
    {
        sessionsList: {
            sessions: [],
            additionalSessionsUsed: 0
        },
        cloudServiceResources: {
            id: '',
            name: '',
            dependServiceId: '',
            serviceExpireDate: '',
            serviceDependsOnRent: false,
            isActivatedPromisPayment: false,
            amount: 0,
            usedLicenses: 0,
            currency: '',
        },
        cloudServiceResourcesError: '',
        changeCloudServiceResources: {
            complete: false,
            error: null,
            enoughMoney: 0,
            canGetPromisePayment: false,
            costOftariff: 0,
            monthlyCost: 0,
            currency: '',
            balance: 0
        },
        hasSuccessFor: {
            hasSessionsListReceived: false,
            hasCloudServiceResources: false,
            hasChangeCloudServiceResources: false,
            hasBuyResources: false
        }
    }
);

/**
 * Объединение частей редюсера для всего состояния additionalSessions
 */
const partialReducers = [
    getSessionListReducer,
    getCloudServiceResourcesReducer,
    changeCloudServiceResourcesReducer,
    endingUserSessionReducer,
    buyCloudServiceResourcesReducer,
    resetCloudServiceResourcesReducer
];

/**
 * Редюсер additionalSessions
 */
export const additionalSessionReducer = createReducer(initialState, partialReducers);