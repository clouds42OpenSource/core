import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';
import { SessionListDataModel } from 'app/web/InterlayerApiProxy/AdditionalSessionsApiProxy/getSessionList';

/**
 * Данные в редюсере при старте получения списка сеансов
 */
export type GetSessionListActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсере при неуспешном получении списка сеансов
 */
export type GetSessionListActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсере при успешном получении списка сеансов
 */
export type GetSessionListActionSuccessPayload = ReducerActionSuccessPayload & {
    /**
     * Список сеансов
     */
    sessionList: SessionListDataModel
};

/**
 * Все возможные actions при получении списка сеансов
 */
export type GetSessionListActionAnyPayload =
    GetSessionListActionStartPayload |
    GetSessionListActionFailedPayload |
    GetSessionListActionSuccessPayload;