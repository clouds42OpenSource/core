import personalAdditionalSessionsReducer from 'app/modules/additionalSessions/store/reducers/PersonalAdditionalSessionsSlice';
import { combineReducers } from 'redux';

export const CombineReducerPersonalSessions = combineReducers({
    personalAdditionalSessionsReducer
});