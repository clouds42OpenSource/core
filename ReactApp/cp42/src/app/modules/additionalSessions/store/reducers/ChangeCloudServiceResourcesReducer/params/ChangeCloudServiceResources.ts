import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';
import { ChangeCloudServiceResourcesParams } from 'app/web/InterlayerApiProxy/AdditionalSessionsApiProxy/changeCloudServiceResources';

/**
 * Модель на изменение или покупку дополнительных сеансов
 */
export type ChangeCloudServiceResourcesThunkParams = IForceThunkParam & ChangeCloudServiceResourcesParams;