import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';
import { ChangeCloudServiceResourcesDataModel } from 'app/web/InterlayerApiProxy/AdditionalSessionsApiProxy/changeCloudServiceResources';

/**
 * Данные в редюсере при старте изменения или покупки дополнительных сеансов
 */
export type ChangeCloudServiceResourcesStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсере при неуспешном изменении или покупки дополнительных сеансов
 */
export type ChangeCloudServiceResourcesFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсере при успешном изменении или покупки дополнительных сеансов
 */
export type ChangeCloudServiceResourcesSuccessPayload = ReducerActionSuccessPayload & {
    /**
     * Информация о изменении или покупке дополнительных сеансов
     */
    changeCloudServiceResources: ChangeCloudServiceResourcesDataModel
};

/**
 * Все возможные actions при покупке или изменении дополнительных сеансов
 */
export type ChangeCloudServiceResourcesAnyPayload =
    ChangeCloudServiceResourcesStartPayload |
    ChangeCloudServiceResourcesFailedPayload |
    ChangeCloudServiceResourcesSuccessPayload;