import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { TGetExtraSessionsResponse } from 'app/api/endpoints/additionalSessions/response';
import { TResponseLowerCase } from 'app/api/types';
import { AdditionalSessionConsts } from 'app/modules/additionalSessions/constants';

type InitialState = {
    personalAdditionalSessions: TResponseLowerCase<TGetExtraSessionsResponse>;
};

const initialState: InitialState = {
    personalAdditionalSessions: {
        data: {
            userSessions: [],
            commonSessions: 0
        },
        message: null,
        success: false
    }
};

export const personalAdditionalSessionsSlice = createSlice({
    name: AdditionalSessionConsts.reducerName,
    initialState,
    reducers: {
        change(state, action: PayloadAction<TResponseLowerCase<TGetExtraSessionsResponse>>) {
            state.personalAdditionalSessions = action.payload;
        },
        plus(state, action: PayloadAction<string>) {
            if (state.personalAdditionalSessions.data) {
                state.personalAdditionalSessions.data.userSessions = state.personalAdditionalSessions.data.userSessions.map(item => {
                    if (item.userID === action.payload) {
                        item.sessions++;
                    }

                    return item;
                });
            }
        },
        minus(state, action: PayloadAction<string>) {
            if (state.personalAdditionalSessions.data) {
                state.personalAdditionalSessions.data.userSessions = state.personalAdditionalSessions.data.userSessions.map(item => {
                    if (item.userID === action.payload && item.sessions > 0) {
                        item.sessions--;
                    }

                    return item;
                });
            }
        },
        onChange(state, { payload: [id, value] }: PayloadAction<[string, number]>) {
            if (state.personalAdditionalSessions.data) {
                state.personalAdditionalSessions.data.userSessions = state.personalAdditionalSessions.data.userSessions.map(item => {
                    if (item.userID === id) {
                        item.sessions = value;
                    }

                    return item;
                });
            }
        }
    }
});

export default personalAdditionalSessionsSlice.reducer;