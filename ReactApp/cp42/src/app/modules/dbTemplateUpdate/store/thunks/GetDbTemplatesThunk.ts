import { GetDbTemplatesActionFailedPayload, GetDbTemplatesActionStartPayload, GetDbTemplatesActionSuccessPayload } from 'app/modules/dbTemplateUpdate/store/reducers/getDbTemplatesReducer/payload';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';

import { DbTemplateUpdateActions } from 'app/modules/dbTemplateUpdate/store/actions';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = GetDbTemplatesActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = GetDbTemplatesActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = GetDbTemplatesActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = IForceThunkParam;

const { GetDbTemplates: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = DbTemplateUpdateActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для запроса данныx всех шаблонов баз
 */
export class GetDbTemplatesThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new GetDbTemplatesThunk().execute(args, args?.showLoadingProgress);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        const state = args.getStore().DbTemplateUpdateState;

        return {
            condition: !state.hasSuccessFor!.hasDbTemplateUpdatesReceived || args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().DbTemplateUpdateState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        const api = InterlayerApiProxy.getDbTemplateUpdateApiProxy();

        try {
            const dbTemplateUpdatesResponse = await api.getDbTemplates(RequestKind.SEND_BY_ROBOT);
            args.success(dbTemplateUpdatesResponse);
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}