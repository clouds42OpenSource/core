import { GetDbTemplateUpdatesActionFailedPayload, GetDbTemplateUpdatesActionStartPayload, GetDbTemplateUpdatesActionSuccessPayload } from 'app/modules/dbTemplateUpdate/store/reducers/getDbTemplateUpdatesReducer/payload';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';

import { DbTemplateUpdateStateEnumType } from 'app/common/enums';
import { DbTemplateUpdateActions } from 'app/modules/dbTemplateUpdate/store/actions';
import { GetDbTemplateUpdatesParams } from 'app/modules/dbTemplateUpdate/store/reducers/getDbTemplateUpdatesReducer/params';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = GetDbTemplateUpdatesActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = GetDbTemplateUpdatesActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = GetDbTemplateUpdatesActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = GetDbTemplateUpdatesParams;

const { GetTemplateUpdates: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = DbTemplateUpdateActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для запроса данныx обновлений шаблона баз
 */
export class GetDbTemplateUpdatesThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new GetDbTemplateUpdatesThunk().execute(args, args?.showLoadingProgress);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        const state = args.getStore().DbTemplateUpdateState;

        return {
            condition: !state.hasSuccessFor!.hasDbTemplateUpdatesReceived || args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().DbTemplateUpdateState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        const api = InterlayerApiProxy.getDbTemplateUpdateApiProxy();
        const inputArgs = args.inputParams!;

        try {
            const dbTemplateUpdatesResponse = await api.getTemplateUpdates(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, {
                validateState: inputArgs.validateState ?? DbTemplateUpdateStateEnumType.New,
                templateId: inputArgs.templateId
            });
            args.success(dbTemplateUpdatesResponse);
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}