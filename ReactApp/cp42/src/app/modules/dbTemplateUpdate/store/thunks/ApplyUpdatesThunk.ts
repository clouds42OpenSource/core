import { DbTemplateUpdateActions } from 'app/modules/dbTemplateUpdate/store/actions';
import { ApplyUpdatesParams } from 'app/modules/dbTemplateUpdate/store/reducers/applyUpdatesReducer/params';
import { ApplyUpdatesActionFailedPayload, ApplyUpdatesActionStartPayload, ApplyUpdatesActionSuccessPayload } from 'app/modules/dbTemplateUpdate/store/reducers/applyUpdatesReducer/payload';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = ApplyUpdatesActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = ApplyUpdatesActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = ApplyUpdatesActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = ApplyUpdatesParams;

const { ApplyUpdates: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = DbTemplateUpdateActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для запроса принятия обновлений шаблона баз
 */
export class ApplyUpdatesThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new ApplyUpdatesThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        return {
            condition: args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().DbTemplateUpdateState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        const api = InterlayerApiProxy.getDbTemplateUpdateApiProxy();

        try {
            const response = await api.applyUpdates(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, args.inputParams!.dbTemplateUpdateId);

            if (response !== null) {
                args.failed({ error: new Error(response.message ?? 'Не удалось обновить') });
            } else {
                args.success();
            }
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}