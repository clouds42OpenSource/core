import { DbTemplateUpdateActions } from 'app/modules/dbTemplateUpdate/store/actions';
import { RejectUpdatesParams } from 'app/modules/dbTemplateUpdate/store/reducers/rejectUpdatesReducer/params';
import { RejectUpdatesActionFailedPayload, RejectUpdatesActionStartPayload, RejectUpdatesActionSuccessPayload } from 'app/modules/dbTemplateUpdate/store/reducers/rejectUpdatesReducer/payload';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = RejectUpdatesActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = RejectUpdatesActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = RejectUpdatesActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = RejectUpdatesParams;

const { RejectUpdates: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = DbTemplateUpdateActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для запроса отклонения обновлений шаблона баз
 */
export class RejectUpdatesThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new RejectUpdatesThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        return {
            condition: args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().DbTemplateUpdateState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        const api = InterlayerApiProxy.getDbTemplateUpdateApiProxy();

        try {
            const response = await api.rejectUpdates(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, args.inputParams!.dbTemplateUpdateId);

            if (response !== null) {
                args.failed({ error: new Error(response.message ?? 'Не удалось отменить') });
            } else {
                args.success();
            }
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}