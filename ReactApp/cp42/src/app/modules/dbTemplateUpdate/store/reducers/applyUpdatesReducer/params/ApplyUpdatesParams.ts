import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для принятия обновлений шаблона баз
 */
export type ApplyUpdatesParams = IForceThunkParam & {
    /**
     * ID обновлений шаблона
     */
    dbTemplateUpdateId: string
}