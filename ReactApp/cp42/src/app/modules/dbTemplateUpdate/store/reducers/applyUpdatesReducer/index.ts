import { reducerActionState } from 'app/common/functions/reducerActionState';
import { DbTemplateUpdateProcessId } from 'app/modules/dbTemplateUpdate';
import { DbTemplateUpdateActions } from 'app/modules/dbTemplateUpdate/store/actions';
import { DbTemplateUpdateState } from 'app/modules/dbTemplateUpdate/store/reducers';
import { ApplyUpdatesActionAnyPayload } from 'app/modules/dbTemplateUpdate/store/reducers/applyUpdatesReducer/payload';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для принятия обновлений шаблона баз
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const applyUpdatesReducer: TPartialReducerObject<DbTemplateUpdateState, ApplyUpdatesActionAnyPayload> =
    (state, action) => {
        const processId = DbTemplateUpdateProcessId.ApplyUpdates;
        const actions = DbTemplateUpdateActions.ApplyUpdates;

        return reducerActionState({
            action,
            actions,
            processId,
            state
        });
    };