import { reducerActionState } from 'app/common/functions/reducerActionState';
import { DbTemplateUpdateProcessId } from 'app/modules/dbTemplateUpdate';
import { DbTemplateUpdateActions } from 'app/modules/dbTemplateUpdate/store/actions';
import { DbTemplateUpdateState } from 'app/modules/dbTemplateUpdate/store/reducers';
import { DeleteUpdatesActionAnyPayload } from 'app/modules/dbTemplateUpdate/store/reducers/deleteUpdatesReducer/payload';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для удаления обновлений шаблона баз
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const deleteUpdatesReducer: TPartialReducerObject<DbTemplateUpdateState, DeleteUpdatesActionAnyPayload> =
    (state, action) => {
        const processId = DbTemplateUpdateProcessId.DeleteUpdates;
        const actions = DbTemplateUpdateActions.DeleteUpdates;

        return reducerActionState({
            action,
            actions,
            processId,
            state
        });
    };