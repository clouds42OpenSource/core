import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте отклонения обновлений шаблона баз
 */
export type RejectUpdatesActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном отклонении обновлений шаблона баз
 */
export type RejectUpdatesActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном отклонении обновлений шаблона баз
 */
export type RejectUpdatesActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при отклонении обновлений шаблона баз
 */
export type RejectUpdatesActionAnyPayload =
    | RejectUpdatesActionStartPayload
    | RejectUpdatesActionSuccessPayload
    | RejectUpdatesActionFailedPayload;