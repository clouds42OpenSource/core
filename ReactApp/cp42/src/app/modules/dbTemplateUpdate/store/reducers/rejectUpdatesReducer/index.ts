import { reducerActionState } from 'app/common/functions/reducerActionState';
import { DbTemplateUpdateProcessId } from 'app/modules/dbTemplateUpdate';
import { DbTemplateUpdateActions } from 'app/modules/dbTemplateUpdate/store/actions';
import { DbTemplateUpdateState } from 'app/modules/dbTemplateUpdate/store/reducers';
import { RejectUpdatesActionAnyPayload } from 'app/modules/dbTemplateUpdate/store/reducers/rejectUpdatesReducer/payload';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для отклонения обновлений шаблона баз
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const rejectUpdatesReducer: TPartialReducerObject<DbTemplateUpdateState, RejectUpdatesActionAnyPayload> =
    (state, action) => {
        const processId = DbTemplateUpdateProcessId.RejectUpdates;
        const actions = DbTemplateUpdateActions.RejectUpdates;

        return reducerActionState({
            action,
            actions,
            processId,
            state
        });
    };