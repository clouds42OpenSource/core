import { DbTemplateUpdateProcessId } from 'app/modules/dbTemplateUpdate';
import { DbTemplatesDataModel } from 'app/web/InterlayerApiProxy/DbTemplateUpdateApiProxy/getDbTemplates/data-models';
import { DbTemplateUpdatesDataModel } from 'app/web/InterlayerApiProxy/DbTemplateUpdateApiProxy/getTemplateUpdates/data-models';
import { IReducerState } from 'core/redux/interfaces';

/**
 * Состояние редюсера для работы с обновлениями шаблона баз
 */
export type DbTemplateUpdateState = IReducerState<DbTemplateUpdateProcessId> & {
    /**
     * Массив обновления шаблона баз
     */
    dbTemplateUpdates: Array<DbTemplateUpdatesDataModel>,

    /**
     * Массив всех шаблонов ввиде словаря
     */
    dbTemplates: Array<DbTemplatesDataModel>,

    /**
     * содержит ярлыки для фиксации загрузок данных, была ли загрузка или ещё нет
     */
    hasSuccessFor: {
        /**
         * Если true, то все шаблоны получены
         */
        hasDbTemplatesReceived: boolean,

        /**
         * Если true, то обновлений шаблона баз получен
         */
        hasDbTemplateUpdatesReceived: boolean
    };
};