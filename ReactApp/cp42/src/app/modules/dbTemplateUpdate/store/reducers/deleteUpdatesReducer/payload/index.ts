import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте удаления обновлений шаблона баз
 */
export type DeleteUpdatesActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном удалении обновлений шаблона баз
 */
export type DeleteUpdatesActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном удалении обновлений шаблона баз
 */
export type DeleteUpdatesActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при удалении обновлений шаблона баз
 */
export type DeleteUpdatesActionAnyPayload =
    | DeleteUpdatesActionStartPayload
    | DeleteUpdatesActionSuccessPayload
    | DeleteUpdatesActionFailedPayload;