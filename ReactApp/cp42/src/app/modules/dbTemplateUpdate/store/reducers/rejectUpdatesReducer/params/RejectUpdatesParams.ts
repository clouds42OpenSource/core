import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для отклонения обновлений шаблона баз
 */
export type RejectUpdatesParams = IForceThunkParam & {
    /**
     * ID обновлений шаблона
     */
    dbTemplateUpdateId: string
}