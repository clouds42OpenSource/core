import { DbTemplatesDataModel } from 'app/web/InterlayerApiProxy/DbTemplateUpdateApiProxy/getDbTemplates/data-models';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте получения данныx всех шаблонов баз
 */
export type GetDbTemplatesActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном получении данныx всех шаблонов баз
 */
export type GetDbTemplatesActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном получении данныx всех шаблонов баз
 */
export type GetDbTemplatesActionSuccessPayload = ReducerActionSuccessPayload & Array<DbTemplatesDataModel>;

/**
 * Все возможные Action при получении данныx всех шаблонов баз
 */
export type GetDbTemplatesActionAnyPayload =
    | GetDbTemplatesActionStartPayload
    | GetDbTemplatesActionSuccessPayload
    | GetDbTemplatesActionFailedPayload;