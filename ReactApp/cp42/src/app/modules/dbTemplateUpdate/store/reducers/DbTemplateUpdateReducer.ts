import { DbTemplateUpdateConsts } from 'app/modules/dbTemplateUpdate/constants';
import { applyUpdatesReducer } from 'app/modules/dbTemplateUpdate/store/reducers/applyUpdatesReducer';
import { DbTemplateUpdateState } from 'app/modules/dbTemplateUpdate/store/reducers/DbTemplateUpdateState';
import { deleteUpdatesReducer } from 'app/modules/dbTemplateUpdate/store/reducers/deleteUpdatesReducer';
import { getDbTemplatesReducer } from 'app/modules/dbTemplateUpdate/store/reducers/getDbTemplatesReducer';
import { getDbTemplateUpdatesReducer } from 'app/modules/dbTemplateUpdate/store/reducers/getDbTemplateUpdatesReducer';
import { rejectUpdatesReducer } from 'app/modules/dbTemplateUpdate/store/reducers/rejectUpdatesReducer';
import { createReducer, initReducerState } from 'core/redux/functions';

/**
 * Начальное состояние редюсера DbTemplateUpdateReducer
 */
const initialState = initReducerState<DbTemplateUpdateState>(
    DbTemplateUpdateConsts.reducerName,
    {
        /**
         * Массив обновления шаблона баз
         */
        dbTemplateUpdates: [],

        /**
         * Массив всех шаблонов ввиде словаря
         */
        dbTemplates: [],

        /**
         * содержит ярлыки для фиксации загрузок данных, была ли загрузка или ещё нет
         */
        hasSuccessFor: {
            /**
             * Если true, то все шаблоны получены
             */
            hasDbTemplateUpdatesReceived: false,

            /**
             * Если true, то обновления шаблона баз получен
             */
            hasDbTemplatesReceived: false
        }
    }
);

/**
 * Объединённые части редюсера для всего состояния DbTemplateUpdateReducer
 */
const partialReducers = [
    getDbTemplateUpdatesReducer,
    getDbTemplatesReducer,
    applyUpdatesReducer,
    rejectUpdatesReducer,
    deleteUpdatesReducer
];

/**
 * Редюсер DbTemplateUpdateReducer
 */
export const DbTemplateUpdateReducer = createReducer(initialState, partialReducers);