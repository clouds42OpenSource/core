import { DbTemplateUpdatesParams } from 'app/web/InterlayerApiProxy/DbTemplateUpdateApiProxy/getTemplateUpdates/input-params';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Параметры для получения обновления шаблона баз
 */
export type GetDbTemplateUpdatesParams = IForceThunkParam & DbTemplateUpdatesParams;