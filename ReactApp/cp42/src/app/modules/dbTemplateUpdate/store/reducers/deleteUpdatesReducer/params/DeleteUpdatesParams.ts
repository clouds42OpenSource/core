import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для удаления обновлений шаблона баз
 */
export type DeleteUpdatesParams = IForceThunkParam & {
    /**
     * ID обвнолений шаблона
     */
    dbTemplateUpdateId: string
}