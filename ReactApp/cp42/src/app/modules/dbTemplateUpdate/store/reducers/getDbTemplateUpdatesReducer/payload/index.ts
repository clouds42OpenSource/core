import { DbTemplateUpdatesDataModel } from 'app/web/InterlayerApiProxy/DbTemplateUpdateApiProxy/getTemplateUpdates/data-models';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте получения данныx обвновления шаблона баз
 */
export type GetDbTemplateUpdatesActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном получении данныx обвновления шаблона баз
 */
export type GetDbTemplateUpdatesActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном получении данныx обвновления шаблона баз
 */
export type GetDbTemplateUpdatesActionSuccessPayload = ReducerActionSuccessPayload & Array<DbTemplateUpdatesDataModel>;

/**
 * Все возможные Action при получении данныx обвновления шаблона баз
 */
export type GetDbTemplateUpdatesActionAnyPayload =
    | GetDbTemplateUpdatesActionStartPayload
    | GetDbTemplateUpdatesActionSuccessPayload
    | GetDbTemplateUpdatesActionFailedPayload;