import { reducerActionState } from 'app/common/functions/reducerActionState';
import { DbTemplateUpdateProcessId } from 'app/modules/dbTemplateUpdate';
import { DbTemplateUpdateActions } from 'app/modules/dbTemplateUpdate/store/actions';
import { DbTemplateUpdateState } from 'app/modules/dbTemplateUpdate/store/reducers';
import { GetDbTemplatesActionAnyPayload, GetDbTemplatesActionSuccessPayload } from 'app/modules/dbTemplateUpdate/store/reducers/getDbTemplatesReducer/payload';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения всех шаблонов баз
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const getDbTemplatesReducer: TPartialReducerObject<DbTemplateUpdateState, GetDbTemplatesActionAnyPayload> =
    (state, action) => {
        const processId = DbTemplateUpdateProcessId.GetDbTemplates;
        const actions = DbTemplateUpdateActions.GetDbTemplates;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onStartProcessState: () => {
                return reducerStateProcessStart(state, processId, {
                    dbTemplates: []
                });
            },
            onSuccessProcessState: () => {
                const payload = action.payload as GetDbTemplatesActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    dbTemplates: payload
                });
            }
        });
    };