import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте принятия обновлений шаблона баз
 */
export type ApplyUpdatesActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном принятии обновлений шаблона баз
 */
export type ApplyUpdatesActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном принятии обновлений шаблона баз
 */
export type ApplyUpdatesActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при принятии обновлений шаблона баз
 */
export type ApplyUpdatesActionAnyPayload =
    | ApplyUpdatesActionStartPayload
    | ApplyUpdatesActionSuccessPayload
    | ApplyUpdatesActionFailedPayload;