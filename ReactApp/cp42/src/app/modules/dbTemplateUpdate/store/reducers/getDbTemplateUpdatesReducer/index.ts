import { reducerActionState } from 'app/common/functions/reducerActionState';
import { DbTemplateUpdateProcessId } from 'app/modules/dbTemplateUpdate';
import { DbTemplateUpdateActions } from 'app/modules/dbTemplateUpdate/store/actions';
import { DbTemplateUpdateState } from 'app/modules/dbTemplateUpdate/store/reducers';
import { GetDbTemplateUpdatesActionAnyPayload, GetDbTemplateUpdatesActionSuccessPayload } from 'app/modules/dbTemplateUpdate/store/reducers/getDbTemplateUpdatesReducer/payload';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения обвновления шаблона баз
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const getDbTemplateUpdatesReducer: TPartialReducerObject<DbTemplateUpdateState, GetDbTemplateUpdatesActionAnyPayload> =
    (state, action) => {
        const processId = DbTemplateUpdateProcessId.GetTemplateUpdates;
        const actions = DbTemplateUpdateActions.GetTemplateUpdates;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onStartProcessState: () => {
                return reducerStateProcessStart(state, processId, {
                    dbTemplateUpdates: []
                });
            },
            onSuccessProcessState: () => {
                const payload = action.payload as GetDbTemplateUpdatesActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    dbTemplateUpdates: payload
                });
            }
        });
    };