import { DbTemplateUpdateProcessId } from 'app/modules/dbTemplateUpdate';
import { DbTemplateUpdateConsts } from 'app/modules/dbTemplateUpdate/constants';
import { createReducerActions } from 'core/redux/functions';

/**
 * Все доступные actions дле редюсера DbTemplateUpdate
 */
export const DbTemplateUpdateActions = {
    /**
     * Набор action на получение обновленные шаблоны баз
     */
    GetTemplateUpdates: createReducerActions(DbTemplateUpdateConsts.reducerName, DbTemplateUpdateProcessId.GetTemplateUpdates),

    /**
     * Набор action на получение всех шаблонов баз
     */
    GetDbTemplates: createReducerActions(DbTemplateUpdateConsts.reducerName, DbTemplateUpdateProcessId.GetDbTemplates),

    /**
     * Набор action на принятия обновление шаблона
     */
    ApplyUpdates: createReducerActions(DbTemplateUpdateConsts.reducerName, DbTemplateUpdateProcessId.ApplyUpdates),

    /**
     * Набор action на отклоненние обновление шаблона
     */
    RejectUpdates: createReducerActions(DbTemplateUpdateConsts.reducerName, DbTemplateUpdateProcessId.RejectUpdates),

    /**
     * Набор action на удаление обновление шаблона
     */
    DeleteUpdates: createReducerActions(DbTemplateUpdateConsts.reducerName, DbTemplateUpdateProcessId.DeleteUpdates),
};