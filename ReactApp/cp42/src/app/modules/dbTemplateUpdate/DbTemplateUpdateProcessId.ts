/**
 * Процесы DbTemplateUpdate
 */
export enum DbTemplateUpdateProcessId {
    /**
     * Процесс получить обновленные шаблоны
     */
    GetTemplateUpdates = 'GET_TEMPLATE_UPDATES',

    /**
     * Процесс получить все шаблоны
     */
    GetDbTemplates = 'GET_DB_TEMPLATES',

    /**
     * Процесс принятия обновления шаблона
     */
    ApplyUpdates = 'UPPLY_UPDATES',

    /**
     * Процесс отклонения обновления шаблона
     */
    RejectUpdates = 'REJECT_UPDATES',

    /**
     * Процесс удаления обновления шаблона
     */
    DeleteUpdates = 'DELETE_UPDATES'
}