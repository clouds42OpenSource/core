/**
 * Константы для модуля DbTemplateUpdate
 */
export const DbTemplateUpdateConsts = {
    /**
     * Название редюсера для модуля DbTemplateUpdate
     */
    reducerName: 'DB_TEMPLATE_UPDATE'
};