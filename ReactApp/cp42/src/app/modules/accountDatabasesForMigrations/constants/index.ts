/**
 * Константы для модуля AccountDatabasesForMigration
 */
export const AccountDatabasesForMigrationConsts = {
    /**
     * Название редюсера для модуля AccountDatabasesForMigration
     */
    reducerName: 'ACCOUNT_DATABASES_FOR_MIGRATION'
};