import { AccountDatabasesForMigrationProcessId } from 'app/modules/accountDatabasesForMigrations/AccountDatabasesForMigrationProcessId';
import { AccountDatabasesForMigrationConsts } from 'app/modules/accountDatabasesForMigrations/constants';
import { createReducerActions } from 'core/redux/functions';

/**
 * Все доступные actions дле редюсера AccountDatabasesForMigration
 */
export const AccountDatabasesForMigrationActions = {
    /**
     * Набор action на получение списка информационных баз аккаунта для миграция
     */
    ReceiveAccountDatabasesForMigration: createReducerActions(AccountDatabasesForMigrationConsts.reducerName, AccountDatabasesForMigrationProcessId.ReceiveAccountDatabasesForMigration),

    /**
     * Процесс миграции выбранных баз в выбранное хранилище
     */
    MigrateSelectedDatabases: createReducerActions(AccountDatabasesForMigrationConsts.reducerName, AccountDatabasesForMigrationProcessId.MigrateSelectedDatabases)
};