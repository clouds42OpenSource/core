import { AccountDatabasesForMigrationActions } from 'app/modules/accountDatabasesForMigrations/store/actions';
import { ReceiveAccountDatabasesForMigrationThunkParams } from 'app/modules/accountDatabasesForMigrations/store/reducers/receiveAccountDatabasesForMigrationReducer/params';
import {
    ReceiveAccountDatabasesForMigrationActionFailedPayload,
    ReceiveAccountDatabasesForMigrationActionStartPayload,
    ReceiveAccountDatabasesForMigrationActionSuccessPayload
} from 'app/modules/accountDatabasesForMigrations/store/reducers/receiveAccountDatabasesForMigrationReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

type TActionStartPayload = ReceiveAccountDatabasesForMigrationActionStartPayload;
type TActionSuccessPayload = ReceiveAccountDatabasesForMigrationActionSuccessPayload;
type TActionFailedPayload = ReceiveAccountDatabasesForMigrationActionFailedPayload;

type TInputParams = ReceiveAccountDatabasesForMigrationThunkParams;

const { ReceiveAccountDatabasesForMigration: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = AccountDatabasesForMigrationActions;

type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

export class AccountDatabasesForMigrationThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    public static invoke(args?: TInputParams) {
        return new AccountDatabasesForMigrationThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        const state = args.getStore().AccountDatabasesForMigrationState;
        return {
            condition: !!args.inputParams?.filter?.accountIndexNumber &&
                (
                    args.inputParams.filter.accountIndexNumber !== state.activeAccountIndexNumber ||
                    !state.hasSuccessFor.hasDatabasesForMigrationReceived ||
                    args.inputParams?.force === true
                ),
            startActionPayload: {
                accountId: args.inputParams!.filter!.accountId,
                accountIndexNumber: args.inputParams!.filter!.accountIndexNumber
            },
            getReducerStateFunc: () => args.getStore().AccountDatabasesForMigrationState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        try {
            const api = InterlayerApiProxy.getAccountApi();
            const response = await api.receiveAccountDatabasesForMigration(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, args.inputParams!);
            args.success({
                accountDatabasesMigrationInfo: response,
                activeAccountIndexNumber: args.inputParams!.filter!.accountIndexNumber
            });
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}