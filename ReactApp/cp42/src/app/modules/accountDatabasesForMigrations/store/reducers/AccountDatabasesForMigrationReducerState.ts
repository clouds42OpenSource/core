import { AccountDatabasesForMigrationProcessId } from 'app/modules/accountDatabasesForMigrations/AccountDatabasesForMigrationProcessId';
import { AccountDatabasesMigrationResultDataModel } from 'app/web/InterlayerApiProxy/AccountApiProxy/receiveAccountDatabasesForMigration/data-models';
import { IReducerState } from 'core/redux/interfaces';

/**
 * Состояние редюсера для работы с списком информационных баз для миграций
 */
export type AccountDatabasesForMigrationReducerState = IReducerState<AccountDatabasesForMigrationProcessId> & {

    /**
     * Информацияо информационных базах аккаунта для миграций
     */
    accountDatabasesMigrationInfo: AccountDatabasesMigrationResultDataModel;

    /**
     * Для какого аккаунта были загружены информационные базы
     */
    activeAccountIndexNumber: number;

    hasSuccessFor: {
        /**
         * Если true, то список информационных баз для миграций получен
         */
        hasDatabasesForMigrationReceived: boolean
    };
};