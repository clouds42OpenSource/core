import { reducerActionState } from 'app/common/functions/reducerActionState';
import { AccountDatabasesForMigrationProcessId } from 'app/modules/accountDatabasesForMigrations/AccountDatabasesForMigrationProcessId';
import { AccountDatabasesForMigrationActions } from 'app/modules/accountDatabasesForMigrations/store/actions';
import { AccountDatabasesForMigrationReducerState } from 'app/modules/accountDatabasesForMigrations/store/reducers';
import { MigrateSelectedDatabasesActionAnyPayload, MigrateSelectedDatabasesActionSuccessPayload } from 'app/modules/accountDatabasesForMigrations/store/reducers/migrateSelectedDatabasesReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

export const migrateSelectedDatabasesReducer: TPartialReducerObject<AccountDatabasesForMigrationReducerState, MigrateSelectedDatabasesActionAnyPayload> =
    (state, action) => {
        const processId = AccountDatabasesForMigrationProcessId.MigrateSelectedDatabases;
        const actions = AccountDatabasesForMigrationActions.MigrateSelectedDatabases;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as MigrateSelectedDatabasesActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, payload);
            }
        });
    };