import { MigrateSelectedDatabasesParams } from 'app/web/InterlayerApiProxy/AccountApiProxy/migrateSelectedDatabases/input-params';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для переноса выбранных баз в новое хранилище
 */
export type MigrateSelectedDatabasesThunkParams = IForceThunkParam & MigrateSelectedDatabasesParams;