import { getInitialMetadata } from 'app/common/functions';
import { AccountDatabasesForMigrationConsts } from 'app/modules/accountDatabasesForMigrations/constants';
import { AccountDatabasesForMigrationReducerState } from 'app/modules/accountDatabasesForMigrations/store/reducers/AccountDatabasesForMigrationReducerState';
import { migrateSelectedDatabasesReducer } from 'app/modules/accountDatabasesForMigrations/store/reducers/migrateSelectedDatabasesReducer';
import { receiveAccountDatabasesForMigrationReducer } from 'app/modules/accountDatabasesForMigrations/store/reducers/receiveAccountDatabasesForMigrationReducer';
import { createReducer, initReducerState } from 'core/redux/functions';

/**
 * Начальное состояние редюсера AccountDatabasesForMigration
 */
const initialState = initReducerState<AccountDatabasesForMigrationReducerState>(
    AccountDatabasesForMigrationConsts.reducerName,
    {
        accountDatabasesMigrationInfo: {
            availableStoragePath: [],
            avalableFileStorages: [],
            fileStorageAccountDatabasesSummary: [],
            numberOfDatabasesToMigrate: 0,
            databases: {
                records: [],
                metadata: getInitialMetadata()
            }
        },
        activeAccountIndexNumber: 0,
        hasSuccessFor: {
            hasDatabasesForMigrationReceived: false
        }
    }
);

/**
 * Объединённые части редюсера для всего состояния AccountDatabasesForMigration
 */
const partialReducers = [
    receiveAccountDatabasesForMigrationReducer,
    migrateSelectedDatabasesReducer
];

/**
 * Редюсер AccountDatabasesForMigration
 */
export const accountDatabasesForMigrationReducer = createReducer(initialState, partialReducers);