import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения переноса выбранных баз в новое хранилище
 */
export type MigrateSelectedDatabasesActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении переноса выбранных баз в новое хранилище
 */
export type MigrateSelectedDatabasesActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении переноса выбранных баз в новое хранилище
 */
export type MigrateSelectedDatabasesActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при переносе выбранных баз в новое хранилище
 */
export type MigrateSelectedDatabasesActionAnyPayload =
    | MigrateSelectedDatabasesActionStartPayload
    | MigrateSelectedDatabasesActionFailedPayload
    | MigrateSelectedDatabasesActionSuccessPayload;