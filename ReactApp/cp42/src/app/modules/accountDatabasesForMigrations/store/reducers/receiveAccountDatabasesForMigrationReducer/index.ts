import { getInitialMetadata } from 'app/common/functions';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { AccountDatabasesForMigrationProcessId } from 'app/modules/accountDatabasesForMigrations/AccountDatabasesForMigrationProcessId';
import { AccountDatabasesForMigrationActions } from 'app/modules/accountDatabasesForMigrations/store/actions';
import { AccountDatabasesForMigrationReducerState } from 'app/modules/accountDatabasesForMigrations/store/reducers/AccountDatabasesForMigrationReducerState';
import {
    ReceiveAccountDatabasesForMigrationActionAnyPayload,
    ReceiveAccountDatabasesForMigrationActionStartPayload,
    ReceiveAccountDatabasesForMigrationActionSuccessPayload
} from 'app/modules/accountDatabasesForMigrations/store/reducers/receiveAccountDatabasesForMigrationReducer/payloads';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TManualReducerState, TPartialReducerObject } from 'core/redux/types';

export const receiveAccountDatabasesForMigrationReducer: TPartialReducerObject<AccountDatabasesForMigrationReducerState, ReceiveAccountDatabasesForMigrationActionAnyPayload> =
    (state, action) => {
        const processId = AccountDatabasesForMigrationProcessId.ReceiveAccountDatabasesForMigration;
        const actions = AccountDatabasesForMigrationActions.ReceiveAccountDatabasesForMigration;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onStartProcessState: () => {
                const payload = action.payload as ReceiveAccountDatabasesForMigrationActionStartPayload;
                const userState: Partial<TManualReducerState<AccountDatabasesForMigrationReducerState>> | undefined =
                    state.activeAccountIndexNumber !== payload.accountIndexNumber
                        ? {
                            accountDatabasesMigrationInfo: {
                                ...state.accountDatabasesMigrationInfo,
                                databases: {
                                    records: [],
                                    metadata: getInitialMetadata()
                                },
                            },
                            hasSuccessFor: {
                                ...state.hasSuccessFor,
                                hasDatabasesForMigrationReceived: false
                            }
                        } : undefined;

                return reducerStateProcessStart(state, processId, userState);
            },
            onSuccessProcessState: () => {
                const payload = action.payload as ReceiveAccountDatabasesForMigrationActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    accountDatabasesMigrationInfo: { ...payload.accountDatabasesMigrationInfo },
                    activeAccountIndexNumber: payload.activeAccountIndexNumber,
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasDatabasesForMigrationReceived: true
                    }
                });
            }
        });
    };