import { AccountDatabasesMigrationResultDataModel } from 'app/web/InterlayerApiProxy/AccountApiProxy/receiveAccountDatabasesForMigration/data-models';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения списка информационных баз аккауета для миграций
 */
export type ReceiveAccountDatabasesForMigrationActionStartPayload = ReducerActionStartPayload & {
    /**
     * Для какого аккаунта были загружены информационные базы
     */
    accountIndexNumber: number;
    accountId: string;
};

/**
 * Данные в редюсер при неуспешном выполнении получения списка информационных баз аккауета для миграций
 */
export type ReceiveAccountDatabasesForMigrationActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения списка информационных баз аккауета для миграций
 */
export type ReceiveAccountDatabasesForMigrationActionSuccessPayload = ReducerActionSuccessPayload & {
    /**
     * Информацияо информационных базах аккаунта для миграций
     */
    accountDatabasesMigrationInfo: AccountDatabasesMigrationResultDataModel;
    /**
     * Для какого аккаунта были загружены информационные базы
     */
    activeAccountIndexNumber: number;
};

/**
 * Все возможные Action при получении списка информационных баз аккауета для миграций
 */
export type ReceiveAccountDatabasesForMigrationActionAnyPayload =
    | ReceiveAccountDatabasesForMigrationActionStartPayload
    | ReceiveAccountDatabasesForMigrationActionFailedPayload
    | ReceiveAccountDatabasesForMigrationActionSuccessPayload;