import { ReceiveAccountDatabasesForMigrationParams } from 'app/web/InterlayerApiProxy/AccountApiProxy/receiveAccountDatabasesForMigration/input-params';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для получения списка информационныз баз аккаунта для миграций
 */
export type ReceiveAccountDatabasesForMigrationThunkParams = IForceThunkParam & ReceiveAccountDatabasesForMigrationParams;