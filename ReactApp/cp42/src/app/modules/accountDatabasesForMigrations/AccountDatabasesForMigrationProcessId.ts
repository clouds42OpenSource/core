/**
 * Процесы AccountDatabasesForMigration
 */
export enum AccountDatabasesForMigrationProcessId {
    /**
     * Процесс получить список информационных баз аккаунта для миграций
     */
    ReceiveAccountDatabasesForMigration = 'RECEIVE_ACCOUNT_DATABASES_FOR_MIGRATION',

    /**
     * Процесс миграции выбранных баз в выбранное хранилище
     */
    MigrateSelectedDatabases = 'MIGRATE_SELECTED_DATABASES'
}