import { TGetAccountProfileResponse } from 'app/api/endpoints/accountUsers/response';
import { TDataLightReturn } from 'app/api/types';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { ACCOUNT_USERS_SLICE_NAMES } from 'app/modules/accountUsers/constants';

const initialState: TDataLightReturn<TGetAccountProfileResponse[]> = {
    data: [],
    error: null,
    isLoading: false
};

export const getAccountProfilesListSlice = createSlice({
    name: ACCOUNT_USERS_SLICE_NAMES.getAccountProfilesList,
    initialState,
    reducers: {
        loading(state) {
            state.isLoading = true;
        },
        empty(state) {
            state.isLoading = false;
            state.data = [];
            state.error = null;
        },
        success(state, action: PayloadAction<TGetAccountProfileResponse[]>) {
            state.data = action.payload;
        },
        error(state, action: PayloadAction<string>) {
            state.error = action.payload;
        },
        refresh(state) {
            if (state.data) {
                state.data = [...state.data];
            }
        }
    }
});

export default getAccountProfilesListSlice.reducer;