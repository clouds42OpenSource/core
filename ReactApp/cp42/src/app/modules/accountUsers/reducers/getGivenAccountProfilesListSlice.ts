import { TPermissions } from 'app/api/endpoints/accountUsers/request';
import { TAccountProfile, TGetGivenAccountProfilesResponse } from 'app/api/endpoints/accountUsers/response';
import { TDataLightReturn } from 'app/api/types';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { ACCOUNT_USERS_SLICE_NAMES } from 'app/modules/accountUsers/constants';

type TExtendedGetGivenAccountProfiles = {
    requestData: TPermissions[];
};

export type TChangeRequestDataPayload = {
    profiles?: Omit<TAccountProfile, 'databases'>[];
    userId: string;
    databaseId: string;
    allowBackup?: boolean;
}

const initialState: TDataLightReturn<TGetGivenAccountProfilesResponse> & TExtendedGetGivenAccountProfiles = {
    data: null,
    error: null,
    isLoading: false,
    requestData: []
};

export const getGivenAccountProfilesListSlice = createSlice({
    name: ACCOUNT_USERS_SLICE_NAMES.getGivenAccountProfilesList,
    initialState,
    reducers: {
        loading(state) {
            state.isLoading = true;
            state.data = null;
            state.error = null;
            state.requestData = [];
        },
        success(state, action: PayloadAction<TGetGivenAccountProfilesResponse>) {
            state.data = action.payload;
            state.error = null;
            state.isLoading = false;
            state.requestData = [];
        },
        error(state, action: PayloadAction<string>) {
            state.data = null;
            state.error = action.payload;
            state.isLoading = false;
            state.requestData = [];
        },
        changeRequestData(state, action: PayloadAction<TChangeRequestDataPayload>) {
            const { userId, allowBackup, databaseId, profiles } = action.payload;

            if (state.requestData.find(data => data['user-id'] === userId)) {
                state.requestData = state.requestData.map(data => {
                    if (data['user-id'] === userId) {
                        if (!allowBackup && profiles) {
                            data.profiles = profiles;
                        }

                        if (!data.profiles.find(profile => profile.admin)) {
                            data['allow-backup'] = false;
                        } else {
                            data['allow-backup'] = allowBackup;
                        }
                    }

                    return data;
                });
            } else {
                state.requestData = [...state.requestData, {
                    'application-id': databaseId,
                    'user-id': userId,
                    profiles: profiles ?? [],
                    'allow-backup': allowBackup
                }];
            }
        },
        clear(state) {
            state.requestData = [];
        }
    }
});

export default getGivenAccountProfilesListSlice.reducer;