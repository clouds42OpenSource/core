import { combineReducers } from 'redux';

import databaseAccessUsersListReducer from './getDatabaseAccessUsersListSlice';
import accountProfilesListReducer from './getAccountProfilesListSlice';
import givenAccountProfilesListReducer from './getGivenAccountProfilesListSlice';

export const AccountUsersReducer = combineReducers({
    databaseAccessUsersListReducer,
    accountProfilesListReducer,
    givenAccountProfilesListReducer
});