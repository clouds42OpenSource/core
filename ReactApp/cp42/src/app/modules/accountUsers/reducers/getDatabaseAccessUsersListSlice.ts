import { TAccountUsersAccessResponse } from 'app/api/endpoints/accountUsers/response';
import { TDataLightReturn } from 'app/api/types';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { ACCOUNT_USERS_SLICE_NAMES } from 'app/modules/accountUsers/constants';

type TAccountUsersAccess = TAccountUsersAccessResponse & {
    hasAccessArray: string[];
}

const initialState: TDataLightReturn<TAccountUsersAccess> = {
    data: null,
    error: null,
    isLoading: false
};

export const getDatabaseAccessUsersListSlice = createSlice({
    name: ACCOUNT_USERS_SLICE_NAMES.getDatabaseAccessUsersList,
    initialState,
    reducers: {
        loading(state) {
            state.isLoading = true;
        },
        empty(state) {
            state.isLoading = false;
            state.data = null;
            state.error = null;
        },
        success(state, action: PayloadAction<TAccountUsersAccessResponse>) {
            state.data = {
                ...action.payload,
                hasAccessArray: action.payload.databaseAccesses.flatMap(databaseInfo => {
                    if (databaseInfo.hasAccess) {
                        return databaseInfo.databaseId;
                    }

                    return [];
                })
            };
        },
        error(state, action: PayloadAction<string>) {
            state.error = action.payload;
        },
        changeStatus(state, action: PayloadAction<string>) {
            if (state.data) {
                state.data.databaseAccesses = state.data.databaseAccesses.map(databaseInfo => {
                    if (databaseInfo.databaseId === action.payload) {
                        databaseInfo.hasAccess = !databaseInfo.hasAccess;
                    }

                    return databaseInfo;
                });
            }
        }
    }
});

export default getDatabaseAccessUsersListSlice.reducer;