export const ACCOUNT_USERS_SLICE_NAMES = {
    getDatabaseAccessUsersList: 'getDatabaseAccessUsersList',
    getAccountProfilesList: 'getAccountProfilesList',
    getGivenAccountProfilesList: 'getGivenAccountProfilesList'
} as const;