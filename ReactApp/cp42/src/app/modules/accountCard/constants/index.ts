/**
 * Константы для модуля AccountCard
 */
export const AccountCardConsts = {
    /**
     * Название редюсера для модуля AccountCard
     */
    reducerName: 'ACCOUNT_CARD'
};