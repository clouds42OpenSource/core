import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель на запрос обновления карточки аккаунта
 */
export type UpdateAccountCardParams = {
    /**
     *  ID аккаунта
     */
    accountId: string;

    /**
     * Название аккаунта
     */
    accountCaption: string;

    /**
     * ИНН
     */
    inn: string;

    /**
     * ID локали
     */
    localeId: string;

    /**
     * Описание
     */
    description: string;

    /**
     * Признак что аккаунт ВИП
     */
    isVip: boolean;

    /**
     * Письма для рассылок
     */
    emails: string[];
    deployment: number | null;
};

/**
 * Модель на запрос обновления карточки аккаунта
 */
export type UpdateAccountCardThunkParams = UpdateAccountCardParams & IForceThunkParam;