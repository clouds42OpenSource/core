import { AccountCardButtonsVisibilityDataModel, AccountCardCommonDataDataModel } from 'app/web/InterlayerApiProxy/AccountCardApiProxy/receiveAccountCard';
import { ReceiveAccountCardActionAnyPayload, ReceiveAccountCardActionSuccessPayload } from 'app/modules/accountCard/store/reducers/receiveAccountCardReducer/payloads';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';

import { AccountCardActions } from 'app/modules/accountCard/store/actions';
import { AccountCardProcessId } from 'app/modules/accountCard/AccountCardProcessId';
import { AccountCardReducerState } from 'app/modules/accountCard/store/reducers/AccountCardReducerState';
import { TPartialReducerObject } from 'core/redux/types';
import { reducerActionState } from 'app/common/functions/reducerActionState';

/**
 * Редюсер для получения карточки аккаунта
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const receiveAccountCardReducer: TPartialReducerObject<AccountCardReducerState, ReceiveAccountCardActionAnyPayload> =
    (state, action) => {
        const processId = AccountCardProcessId.ReceiveAccountCard;
        const actions = AccountCardActions.ReceiveAccountCard;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onStartProcessState: () => {
                return reducerStateProcessStart(state, processId, {
                    commonData: {} as AccountCardCommonDataDataModel,
                    buttonsVisibility: {} as AccountCardButtonsVisibilityDataModel,
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasAccountCardReceived: false
                    }
                });
            },
            onSuccessProcessState: () => {
                const payload = action.payload as ReceiveAccountCardActionSuccessPayload;

                return reducerStateProcessSuccess(state, processId, {
                    buttonsVisibility: { ...payload.accountCard.buttonsVisibility },
                    accountInfo: { ...payload.accountCard.accountInfo },
                    commonData: { ...payload.accountCard.commonData },
                    editableFields: { ...payload.accountCard.editableFields },
                    locales: [...payload.accountCard.locales],
                    segments: [...payload.accountCard.segments],
                    tabVisibility: { ...payload.accountCard.tabVisibility },
                    visibleFields: { ...payload.accountCard.visibleFields },
                    hasSuccessFor: {
                        hasAccountCardReceived: true
                    }
                });
            }
        });
    };