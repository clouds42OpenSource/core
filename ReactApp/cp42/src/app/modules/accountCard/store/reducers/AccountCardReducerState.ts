import {
    AccountCardAccountInfoDataModel,
    AccountCardButtonsVisibilityDataModel,
    AccountCardCommonDataDataModel,
    AccountCardEditableFieldsDataModel,
    AccountCardLocaleInfoItemDataModel,
    AccountCardSegmentInfoItemDataModel,
    AccountCardTabVisibilityDataModel,
    AccountCardVisibleFieldsDataModel
} from 'app/web/InterlayerApiProxy/AccountCardApiProxy/receiveAccountCard';

import { AccountCardProcessId } from 'app/modules/accountCard/AccountCardProcessId';
import { IReducerState } from 'core/redux/interfaces';

/**
 * Состояние редюсера для работы с карточкой аккаунта
 */
export type AccountCardReducerState = IReducerState<AccountCardProcessId> & {
    /**
     * Видимость кнопок для карточки аккаунта
     */
    buttonsVisibility: AccountCardButtonsVisibilityDataModel;
    /**
     * Общая информация о аккаунте
     */
    commonData: AccountCardCommonDataDataModel;
    /**
     * Информация о аккаунте
     */
    accountInfo: AccountCardAccountInfoDataModel;
    /**
     * Массив доступных локалей
     */
    locales: AccountCardLocaleInfoItemDataModel[];
    /**
     * Массив доступных сегментов
     */
    segments: AccountCardSegmentInfoItemDataModel[];
    /**
     * Список полей которые настраеваемы как видимы или нет
     */
    visibleFields: AccountCardVisibleFieldsDataModel;
    /**
     * Список полей которые настраеваемы как редактируемые или нет
     */
    editableFields: AccountCardEditableFieldsDataModel;
    /**
     * Содержит информацию о видимости табов карточки аккаунта
     */
    tabVisibility: AccountCardTabVisibilityDataModel;
    /**
     * содержит ярлыки для фиксации загрузок данных, была ли загрузка или ещё нет
     */
    hasSuccessFor: {
        /**
         * Если true, то карточка аккаунта уже получена
         */
        hasAccountCardReceived: boolean,
    };
};