import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель на запрос обновления сегмента аккаунта
 */
export type ChangeAccountSegmentThunkParams = IForceThunkParam & {
    /**
     * Id аккаунта
     */
    accountId: string;

    /**
     * Id сегмента на который нужно сменить
     */
    segmentId: string;
};