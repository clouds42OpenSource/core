import {
    AccountCardAccountInfoDataModel,
    AccountCardButtonsVisibilityDataModel,
    AccountCardCommonDataDataModel,
    AccountCardEditableFieldsDataModel,
    AccountCardTabVisibilityDataModel,
    AccountCardVisibleFieldsDataModel
} from 'app/web/InterlayerApiProxy/AccountCardApiProxy/receiveAccountCard';
import { createReducer, initReducerState } from 'core/redux/functions';

import { AccountCardConsts } from 'app/modules/accountCard/constants';
import { AccountCardReducerState } from 'app/modules/accountCard/store/reducers/AccountCardReducerState';
import { changeAccountSegmentReducer } from 'app/modules/accountCard/store/reducers/changeAccountSegmentReducer';
import { partialFillOf } from 'app/common/functions/fillObjectWithUndefined';
import { receiveAccountCardReducer } from 'app/modules/accountCard/store/reducers/receiveAccountCardReducer';
import { updateAccountCardReducer } from 'app/modules/accountCard/store/reducers/updateAccountCardReducer';

/**
 * Начальное состояние редюсера AccountCard
 */

const initialState = initReducerState<AccountCardReducerState>(
    AccountCardConsts.reducerName,
    {
        /**
         * Видимость кнопок для карточки аккаунта
         */
        buttonsVisibility: partialFillOf<AccountCardButtonsVisibilityDataModel>(),
        /**
         * Общая информация о аккаунте
         */
        commonData: partialFillOf<AccountCardCommonDataDataModel>(),
        /**
         * Информация о аккаунте
         */
        accountInfo: partialFillOf<AccountCardAccountInfoDataModel>(),
        /**
         * Массив доступных локалей
         */
        locales: [],
        /**
         * Массив доступных сегментов
         */
        segments: [],
        /**
         * Список полей которые настраеваемы как видимы или нет
         */
        visibleFields: partialFillOf<AccountCardVisibleFieldsDataModel>(),
        /**
         * Список полей которые настраеваемы как редактируемые или нет
         */
        editableFields: partialFillOf<AccountCardEditableFieldsDataModel>(),
        /**
         * Содержит информацию о видимости табов карточки аккаунта
         */
        tabVisibility: partialFillOf<AccountCardTabVisibilityDataModel>(),
        hasSuccessFor: {
            /**
             * Если true, то карточка аккаунта уже получена
             */
            hasAccountCardReceived: false,
        }
    }
);

/**
 * Объединённые части редюсера для всего состояния AccountCard
 */
const partialReducers = [
    receiveAccountCardReducer,
    updateAccountCardReducer,
    changeAccountSegmentReducer
];

/**
 * Редюсер AccountCard
 */
export const accountCardReducer = createReducer(initialState, partialReducers);