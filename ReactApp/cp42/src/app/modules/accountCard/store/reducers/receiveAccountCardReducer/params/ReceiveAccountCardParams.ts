import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель на запрос получения карточки аккаунта
 */
export type ReceiveAccountCardThunkParams = IForceThunkParam & {
    /**
     * Номер аккаунта
     */
    accountNumber: number;
};