import { AccountCardDataDataModel } from 'app/web/InterlayerApiProxy/AccountCardApiProxy/receiveAccountCard';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте получения карточки информациооной базы
 */
export type ReceiveAccountCardActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при ошибки получения карточки информациооной базы
 */
export type ReceiveAccountCardActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном получении карточки информациооной базы
 */
export type ReceiveAccountCardActionSuccessPayload = ReducerActionSuccessPayload & {
    accountCard: AccountCardDataDataModel;
};

/**
 * Все возможные Action при получении списка записей логирования запуска баз и старта RDP
 */
export type ReceiveAccountCardActionAnyPayload =
    | ReceiveAccountCardActionStartPayload
    | ReceiveAccountCardActionSuccessPayload
    | ReceiveAccountCardActionFailedPayload;