import { reducerActionState } from 'app/common/functions/reducerActionState';
import { AccountCardProcessId } from 'app/modules/accountCard/AccountCardProcessId';
import { AccountCardActions } from 'app/modules/accountCard/store/actions';
import { AccountCardReducerState } from 'app/modules/accountCard/store/reducers/AccountCardReducerState';
import { UpdateAccountCardActionAnyPayload, UpdateAccountCardActionSuccessPayload } from 'app/modules/accountCard/store/reducers/updateAccountCardReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения карточки аккаунта
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const updateAccountCardReducer: TPartialReducerObject<AccountCardReducerState, UpdateAccountCardActionAnyPayload> =
    (state, action) => {
        const processId = AccountCardProcessId.UpdateAccountCard;
        const actions = AccountCardActions.UpdateAccountCard;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as UpdateAccountCardActionSuccessPayload;

                return reducerStateProcessSuccess(state, processId, {
                    accountInfo: {
                        ...state.accountInfo,
                        accountCaption: payload.UpdatedFields.accountCaption,
                        description: payload.UpdatedFields.description,
                        localeId: payload.UpdatedFields.localeId,
                        inn: payload.UpdatedFields.inn,
                        isVip: payload.UpdatedFields.isVip
                    },
                    commonData: {
                        ...state.commonData,
                        emails: payload.UpdatedFields.emails
                    }
                });
            }
        });
    };