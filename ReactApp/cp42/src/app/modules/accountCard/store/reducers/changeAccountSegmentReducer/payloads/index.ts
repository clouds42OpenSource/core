import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте смены сегмента аккаунта
 */
export type ChangeAccountSegmentActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при ошибки смены сегмента аккаунта
 */
export type ChangeAccountSegmentActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешной смене сегмента аккаунта
 */
export type ChangeAccountSegmentActionSuccessPayload = ReducerActionSuccessPayload & {
    /**
     * Новый id сегмента
     */
    segmentId: string;
};

/**
 * Все возможные Action при смене сегмента аккаунта
 */
export type ChangeAccountSegmentActionAnyPayload =
    | ChangeAccountSegmentActionStartPayload
    | ChangeAccountSegmentActionSuccessPayload
    | ChangeAccountSegmentActionFailedPayload;