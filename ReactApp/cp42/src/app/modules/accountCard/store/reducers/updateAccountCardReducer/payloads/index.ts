import { UpdateAccountCardParams } from 'app/modules/accountCard/store/reducers/updateAccountCardReducer/params';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте обновления карточки информациооной базы
 */
export type UpdateAccountCardActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при ошибки обновления карточки информациооной базы
 */
export type UpdateAccountCardActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном обновления карточки информациооной базы
 */
export type UpdateAccountCardActionSuccessPayload = ReducerActionSuccessPayload & {
    UpdatedFields: UpdateAccountCardParams;
};

/**
 * Все возможные Action при обновления карточки информационной базы
 */
export type UpdateAccountCardActionAnyPayload =
    | UpdateAccountCardActionStartPayload
    | UpdateAccountCardActionSuccessPayload
    | UpdateAccountCardActionFailedPayload;