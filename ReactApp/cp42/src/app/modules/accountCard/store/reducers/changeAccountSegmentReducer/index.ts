import { reducerActionState } from 'app/common/functions/reducerActionState';
import { AccountCardProcessId } from 'app/modules/accountCard/AccountCardProcessId';
import { AccountCardActions } from 'app/modules/accountCard/store/actions';
import { AccountCardReducerState } from 'app/modules/accountCard/store/reducers/AccountCardReducerState';
import { ChangeAccountSegmentActionAnyPayload, ChangeAccountSegmentActionSuccessPayload } from 'app/modules/accountCard/store/reducers/changeAccountSegmentReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для смены сегмента аккаунта
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const changeAccountSegmentReducer: TPartialReducerObject<AccountCardReducerState, ChangeAccountSegmentActionAnyPayload> =
    (state, action) => {
        const processId = AccountCardProcessId.ChangeAccountSegment;
        const actions = AccountCardActions.ChangeAccountSegment;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as ChangeAccountSegmentActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    accountInfo: {
                        ...state.accountInfo,
                        segmentID: payload.segmentId
                    }
                });
            }
        });
    };