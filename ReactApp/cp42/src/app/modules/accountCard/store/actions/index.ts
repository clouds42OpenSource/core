import { AccountCardProcessId } from 'app/modules/accountCard/AccountCardProcessId';
import { AccountCardConsts } from 'app/modules/accountCard/constants';
import { createReducerActions } from 'core/redux/functions';

/**
 * Все доступные actions дле редюсера AccountCard
 */
export const AccountCardActions = {
    /**
     * Набор action на получения карточки аккаунта
     */
    ReceiveAccountCard: createReducerActions(AccountCardConsts.reducerName, AccountCardProcessId.ReceiveAccountCard),

    /**
     * Набор action для обновления карточки аккаунта
     */
    UpdateAccountCard: createReducerActions(AccountCardConsts.reducerName, AccountCardProcessId.UpdateAccountCard),

    /**
     * Набор action для смены сегмента аккаунта
     */
    ChangeAccountSegment: createReducerActions(AccountCardConsts.reducerName, AccountCardProcessId.ChangeAccountSegment),

};