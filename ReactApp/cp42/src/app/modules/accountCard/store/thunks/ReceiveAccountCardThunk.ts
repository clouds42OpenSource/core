import { AccountCardActions } from 'app/modules/accountCard/store/actions';
import { ReceiveAccountCardThunkParams } from 'app/modules/accountCard/store/reducers/receiveAccountCardReducer/params';
import { ReceiveAccountCardActionFailedPayload, ReceiveAccountCardActionStartPayload, ReceiveAccountCardActionSuccessPayload } from 'app/modules/accountCard/store/reducers/receiveAccountCardReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = ReceiveAccountCardActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = ReceiveAccountCardActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = ReceiveAccountCardActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = ReceiveAccountCardThunkParams;

const { ReceiveAccountCard: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = AccountCardActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для запроса списка записей логирования запуска баз и открытия RDP
 */
export class ReceiveAccountCardThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new ReceiveAccountCardThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        const accountCardState = args.getStore().AccountCardState;
        return {
            condition: !accountCardState.hasSuccessFor.hasAccountCardReceived || args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().AccountCardState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        const requestArgs = args.inputParams!;
        const accountCardApi = InterlayerApiProxy.getAccountCardApi();

        try {
            const accountCardResponse = await accountCardApi.receiveAccountCard(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, {
                accountNumber: requestArgs.accountNumber
            });

            args.success({
                accountCard: accountCardResponse
            });
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}