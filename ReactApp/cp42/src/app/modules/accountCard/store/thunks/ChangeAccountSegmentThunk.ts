import { AccountCardActions } from 'app/modules/accountCard/store/actions';
import { ChangeAccountSegmentThunkParams } from 'app/modules/accountCard/store/reducers/changeAccountSegmentReducer/params';
import { ChangeAccountSegmentActionFailedPayload, ChangeAccountSegmentActionStartPayload, ChangeAccountSegmentActionSuccessPayload } from 'app/modules/accountCard/store/reducers/changeAccountSegmentReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = ChangeAccountSegmentActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = ChangeAccountSegmentActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = ChangeAccountSegmentActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = ChangeAccountSegmentThunkParams;

const { ChangeAccountSegment: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = AccountCardActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для запроса списка записей логирования запуска баз и открытия RDP
 */
export class ChangeAccountSegmentThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new ChangeAccountSegmentThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        return {
            condition: args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().AccountCardState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        const requestArgs = args.inputParams!;
        const accountCardApi = InterlayerApiProxy.getAccountCardApi();

        try {
            await accountCardApi.changeAccountSegment(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, {
                accountId: requestArgs.accountId,
                segmentId: requestArgs.segmentId
            });

            args.success({
                segmentId: requestArgs.segmentId
            });
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}