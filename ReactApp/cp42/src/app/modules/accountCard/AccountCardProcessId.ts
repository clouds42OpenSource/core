/**
 * Процесы DatabaseCard
 */
export enum AccountCardProcessId {
    /**
     * Процесс получить карточку аккаунта
     */
    ReceiveAccountCard = 'RECEIVE_ACCOUNT_CARD',

    /**
     * Процесс обновить карточку аккаунта
     */
    UpdateAccountCard = 'UPDATE_ACCOUNT_CARD',

    /**
     * Процесс обновить карточку аккаунта
     */
    ChangeAccountSegment = 'CHANGE_ACCOUNT_SEGMENT',

}