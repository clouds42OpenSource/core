/**
 * Процесы для задач
 */
export enum TasksProcessId {
    /**
     * Процесс для получения данных очереди задач
     */
    ReceiveTasksInQueueData = 'RECEIVE_TASKS_IN_QUEUE_DATA',

    /**
     * Процесс отмены запущенной задачи
     */
    CancelCoreWorkerTasksQueue = 'CANCEL_WORKER_TASKS_QUEUE'
}