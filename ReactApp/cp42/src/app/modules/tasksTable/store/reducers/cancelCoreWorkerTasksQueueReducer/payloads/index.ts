import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';
import { CancelCoreWorkerTasksQueueParams } from 'app/web/InterlayerApiProxy/TasksApiProxy/cancelCoreWorkerTasksQueue/input-params';

/**
 * Данные в редюсер при старте выполнения отмены запущенной задачи
 */
export type CancelCoreWorkerTasksQueueActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении отмены запущенной задачи
 */
export type CancelCoreWorkerTasksQueueActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении отмены запущенной задачи
 */
export type CancelCoreWorkerTasksQueueActionSuccessPayload = ReducerActionSuccessPayload & CancelCoreWorkerTasksQueueParams;

/**
 * Все возможные Action при отмене запущенной задачи
 */
export type CancelCoreWorkerTasksQueueActionAnyPayload =
    | CancelCoreWorkerTasksQueueActionStartPayload
    | CancelCoreWorkerTasksQueueActionFailedPayload
    | CancelCoreWorkerTasksQueueActionSuccessPayload;