import { TasksProcessId } from 'app/modules/tasksTable/TasksProcessId';
import { MetadataModel } from 'app/web/common/data-models';
import { TaskInQueueItemDataModel, TasksInQueueFilterDataModel } from 'app/web/InterlayerApiProxy/TasksApiProxy/receiveTasksData/data-models';
import { IReducerState } from 'core/redux/interfaces';

/**
 * Состояние редюсера для задач
 */
export type TasksReducerState = IReducerState<TasksProcessId> & {

    /**
     * Задачи в очереди
     */
    tasksInQueue: {
        /**
         * Массив записей по очереди задач воркеров
         */
        items: TaskInQueueItemDataModel[],

        /**
         * Текущий фильтр для получения записей по очереди задач воркеров
         */
        filter: TasksInQueueFilterDataModel;

        /**
         * Данные о странице для текущих записей по очереди задач воркеров
         */
        metadata: MetadataModel;
    }

    /**
     * содержит ярлыки для фиксации загрузок данных, была ли загрузка или ещё нет
     */
    hasSuccessFor: {
        /**
         * Если true, то данные по очереди задач воркеров уже получены
         */
        hasTasksInQueueDataReceived: boolean
    };
};