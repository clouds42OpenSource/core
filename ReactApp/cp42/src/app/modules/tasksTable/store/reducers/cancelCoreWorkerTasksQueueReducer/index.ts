import { CoreWorkerTaskInQueueStatusType } from 'app/common/enums';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TasksActions } from 'app/modules/tasksTable/store/actions';
import { CancelCoreWorkerTasksQueueActionAnyPayload, CancelCoreWorkerTasksQueueActionSuccessPayload } from 'app/modules/tasksTable/store/reducers/cancelCoreWorkerTasksQueueReducer/payloads';
import { TasksReducerState } from 'app/modules/tasksTable/store/reducers/TasksReducerState';
import { TasksProcessId } from 'app/modules/tasksTable/TasksProcessId';
import { reducerStateProcessFail, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для отмены запущенной задачи в CoreWorkerTasksQueue
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const cancelCoreWorkerTasksQueueReducer: TPartialReducerObject<TasksReducerState, CancelCoreWorkerTasksQueueActionAnyPayload> =
    (state, action) => {
        const processId = TasksProcessId.CancelCoreWorkerTasksQueue;
        const actions = TasksActions.CancelCoreWorkerTasksQueue;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as CancelCoreWorkerTasksQueueActionSuccessPayload;

                const foundItem = state.tasksInQueue.items.find(item => item.id === payload.coreWorkerTasksQueueId);
                const foundIndex = state.tasksInQueue.items.findIndex(item => item.id === payload.coreWorkerTasksQueueId);

                if (foundItem == null) {
                    return reducerStateProcessFail(state, processId, new Error(`Запись с ID: ${ payload.coreWorkerTasksQueueId } не найдена.`));
                }

                const newRecords = [...state.tasksInQueue.items];

                newRecords[foundIndex] = {
                    ...foundItem,
                    status: CoreWorkerTaskInQueueStatusType[CoreWorkerTaskInQueueStatusType.Error]
                };

                return reducerStateProcessSuccess(state, processId, {
                    tasksInQueue: {
                        ...state.tasksInQueue,
                        items: newRecords
                    }
                });
            }
        });
    };