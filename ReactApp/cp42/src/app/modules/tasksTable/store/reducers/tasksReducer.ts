import { getInitialMetadata } from 'app/common/functions';
import { TasksConstants } from 'app/modules/tasksTable/consts';
import { cancelCoreWorkerTasksQueueReducer } from 'app/modules/tasksTable/store/reducers/cancelCoreWorkerTasksQueueReducer';
import { receiveTasksInQueueDataReducer } from 'app/modules/tasksTable/store/reducers/receiveTasksInQueueData';
import { TasksReducerState } from 'app/modules/tasksTable/store/reducers/TasksReducerState';
import { DateUtility } from 'app/utils';
import { createReducer, initReducerState } from 'core/redux/functions';

const periodToDate = DateUtility.getTomorrow(new Date());
const periodFromDate = DateUtility.setDayTimeToMidnight(DateUtility.getSomeDaysAgoDate(7));

/**
 * Начальное состояние редьюсера
 */
const initialState = initReducerState<TasksReducerState>(
    TasksConstants.reducerName,
    {
        tasksInQueue: {
            items: [],
            metadata: getInitialMetadata(),
            filter: {
                workerId: null,
                periodFrom: periodFromDate,
                periodTo: periodToDate,
                status: '',
                taskId: '',
                searchString: null
            }
        },
        hasSuccessFor: {
            hasTasksInQueueDataReceived: false
        }
    }
);

const partialReducers = [
    receiveTasksInQueueDataReducer,
    cancelCoreWorkerTasksQueueReducer
];

export const tasksReducer = createReducer(initialState, partialReducers);