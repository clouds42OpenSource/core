import { ReceiveTasksInQueueParams } from 'app/web/InterlayerApiProxy/TasksApiProxy/receiveTasksData';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров на получение записей по очереди задач воркеров
 */
export type ReceiveTasksInQueueDataThunkParams = IForceThunkParam & ReceiveTasksInQueueParams;