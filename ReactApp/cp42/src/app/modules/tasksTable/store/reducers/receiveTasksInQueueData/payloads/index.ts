import { MetadataModel } from 'app/web/common/data-models';
import { TaskInQueueItemDataModel, TasksInQueueFilterDataModel } from 'app/web/InterlayerApiProxy/TasksApiProxy/receiveTasksData/data-models';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения записей по очереди задач воркеров
 */
export type ReceiveTasksInQueueActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения записей по очереди задач воркеров
 */
export type ReceiveTasksInQueueActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения записей по очереди задач воркеров
 */
export type ReceiveTasksInQueueActionSuccessPayload = ReducerActionSuccessPayload & {

    /**
     * Массив записей по очереди задач воркеров
     */
    queueData: TaskInQueueItemDataModel[];

    /**
     * Текущий фильтр для получения записей по очереди задач воркеров
     */
    filter: TasksInQueueFilterDataModel;

    /**
     * Данные о странице для текущих записей по очереди задач воркеров
     */
    metadata: MetadataModel;
};

/**
 * Все возможные Action при получении списка записей логирования запуска баз и старта RDP
 */
export type ReceiveTasksInQueueActionAnyPayload =
    | ReceiveTasksInQueueActionStartPayload
    | ReceiveTasksInQueueActionSuccessPayload
    | ReceiveTasksInQueueActionFailedPayload;