import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TasksActions } from 'app/modules/tasksTable/store/actions';
import { ReceiveTasksInQueueActionAnyPayload, ReceiveTasksInQueueActionSuccessPayload } from 'app/modules/tasksTable/store/reducers/receiveTasksInQueueData/payloads';
import { TasksReducerState } from 'app/modules/tasksTable/store/reducers/TasksReducerState';
import { TasksProcessId } from 'app/modules/tasksTable/TasksProcessId';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения данных по очереди задач воркеров
 * @param state состояние
 * @param action действие
 */
export const receiveTasksInQueueDataReducer: TPartialReducerObject<TasksReducerState, ReceiveTasksInQueueActionAnyPayload> =
    (state, action) => {
        const processId = TasksProcessId.ReceiveTasksInQueueData;
        const actions = TasksActions.ReceiveTasksInQueueData;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as ReceiveTasksInQueueActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    tasksInQueue: {
                        ...state.tasksInQueue,
                        items: [...payload.queueData],
                        filter: { ...payload.filter },
                        metadata: { ...payload.metadata }
                    },
                    hasSuccessFor: {
                        hasTasksInQueueDataReceived: true
                    }
                });
            }
        });
    };