import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';
import { CancelCoreWorkerTasksQueueParams } from 'app/web/InterlayerApiProxy/TasksApiProxy/cancelCoreWorkerTasksQueue/input-params';

/**
 * Модель параметров для отмены запущенной задачи
 */
export type CancelCoreWorkerTasksQueueThunkParams = IForceThunkParam & CancelCoreWorkerTasksQueueParams;