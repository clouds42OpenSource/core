import { TasksActions } from 'app/modules/tasksTable/store/actions';
import { CancelCoreWorkerTasksQueueThunkParams } from 'app/modules/tasksTable/store/reducers/cancelCoreWorkerTasksQueueReducer/params';
import { CancelCoreWorkerTasksQueueActionFailedPayload, CancelCoreWorkerTasksQueueActionStartPayload, CancelCoreWorkerTasksQueueActionSuccessPayload } from 'app/modules/tasksTable/store/reducers/cancelCoreWorkerTasksQueueReducer/payloads';
import { ReceiveTasksInQueueDataThunk } from 'app/modules/tasksTable/store/thunks/ReceiveTasksInQueueDataThunk';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = CancelCoreWorkerTasksQueueActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = CancelCoreWorkerTasksQueueActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = CancelCoreWorkerTasksQueueActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = CancelCoreWorkerTasksQueueThunkParams;

const { CancelCoreWorkerTasksQueue: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = TasksActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для запроса на отмену запущенной задачи
 */
export class CancelCoreWorkerTasksQueueThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new CancelCoreWorkerTasksQueueThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        return {
            condition: args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().TasksInQueueState
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        try {
            const requestArgs = args.inputParams!;
            const tasksApi = InterlayerApiProxy.getTasksApi();
            await tasksApi.cancelCoreWorkerTasksQueue(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, requestArgs);
            const tasksState = args.getStore().TasksInQueueState;
            args.thunkDispatch(new ReceiveTasksInQueueDataThunk(), {
                force: true,
                pageNumber: tasksState.tasksInQueue.metadata.pageNumber,
                filter: { ...tasksState.tasksInQueue.filter }
            });
            args.success(requestArgs);
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}