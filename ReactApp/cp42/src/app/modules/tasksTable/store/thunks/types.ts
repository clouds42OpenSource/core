import { ReceiveTasksInQueueDataThunkParams } from 'app/modules/tasksTable/store/reducers/receiveTasksInQueueData/params';
import { ReceiveTasksInQueueActionFailedPayload, ReceiveTasksInQueueActionStartPayload, ReceiveTasksInQueueActionSuccessPayload } from 'app/modules/tasksTable/store/reducers/receiveTasksInQueueData/payloads';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
export type TActionStartPayload = ReceiveTasksInQueueActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
export type TActionSuccessPayload = ReceiveTasksInQueueActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
export type TActionFailedPayload = ReceiveTasksInQueueActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
export type TInputParams = ReceiveTasksInQueueDataThunkParams;