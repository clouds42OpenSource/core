import { TasksProcessId } from 'app/modules/tasksTable/TasksProcessId';
import { TasksConstants } from 'app/modules/tasksTable/consts';
import { createReducerActions } from 'core/redux/functions';

/**
 * Все действия (Actions) для редюсера Tasks
 */
export const TasksActions = {
    /**
     * Действие для получения данных по очереди задач воркеров
     */
    ReceiveTasksInQueueData: createReducerActions(TasksConstants.reducerName, TasksProcessId.ReceiveTasksInQueueData),

    /**
     * Набор action на отмену запущенной задачи
     */
    CancelCoreWorkerTasksQueue: createReducerActions(TasksConstants.reducerName, TasksProcessId.CancelCoreWorkerTasksQueue)
};