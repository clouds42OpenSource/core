import { createReducerActions } from 'core/redux/functions';
import { TasksPageConstants } from 'app/modules/tasksPage/constants';
import { TasksPageProcessId } from 'app/modules/tasksPage/TasksPageProcessId';

/**
 * Все доступные actions дле редюсера TasksPage
 */
export const TasksPageActions = {
    /**
     * Набор action для получения страницы задач
     */
    ReceiveTasksPage: createReducerActions(TasksPageConstants.reducerName, TasksPageProcessId.ReceiveTasksPage)
};