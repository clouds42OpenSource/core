import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TasksPageActions } from 'app/modules/tasksPage/store/actions';
import { ReceiveTasksPageActionAnyPayload, ReceiveTasksPageActionSuccessPayload } from 'app/modules/tasksPage/store/reducers/receiveTasksPageReducer/payloads';
import { TasksPageReducerState } from 'app/modules/tasksPage/store/reducers/TasksPageReducerState';
import { TasksPageProcessId } from 'app/modules/tasksPage/TasksPageProcessId';
import { TasksPageTabVisibilityDataModel } from 'app/web/InterlayerApiProxy/TasksPageApiProxy/receiveTasksPage/data-models';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения старницы задач
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const receiveTasksPageReducer: TPartialReducerObject<TasksPageReducerState, ReceiveTasksPageActionAnyPayload> =
    (state, action) => {
        const processId = TasksPageProcessId.ReceiveTasksPage;
        const actions = TasksPageActions.ReceiveTasksPage;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onStartProcessState: () => {
                return reducerStateProcessStart(state, processId, {
                    tabVisibility: {} as TasksPageTabVisibilityDataModel,
                    hasSuccessFor: {
                        hasTasksPageReceived: false
                    }
                });
            },
            onSuccessProcessState: () => {
                const payload = action.payload as ReceiveTasksPageActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    tabVisibility: { ...payload.tabVisibility },
                    hasSuccessFor: {
                        hasTasksPageReceived: true
                    }
                });
            }
        });
    };