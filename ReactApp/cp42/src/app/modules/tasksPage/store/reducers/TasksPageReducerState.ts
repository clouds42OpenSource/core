import { IReducerState } from 'core/redux/interfaces';
import { TasksPageProcessId } from 'app/modules/tasksPage/TasksPageProcessId';
import { TasksPageTabVisibilityDataModel } from 'app/web/InterlayerApiProxy/TasksPageApiProxy/receiveTasksPage/data-models';

/**
 * Состояние редюсера для работы со страницей задач
 */
export type TasksPageReducerState = IReducerState<TasksPageProcessId> & {
    /**
     * Видимость вкладок страницы задач
     */
    tabVisibility: TasksPageTabVisibilityDataModel;
    /**
     * содержит ярлыки для фиксации загрузок данных, была ли загрузка или ещё нет
     */
    hasSuccessFor: {
        /**
         * Если true, то страница задач уже получена
         */
        hasTasksPageReceived: boolean,
    };
};