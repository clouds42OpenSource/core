import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров на получение страницы задач
 */
export type ReceiveTasksPageThunkParams = IForceThunkParam;