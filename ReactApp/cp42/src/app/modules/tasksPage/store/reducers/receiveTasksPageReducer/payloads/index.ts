import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';
import { TasksPageTabVisibilityDataModel } from 'app/web/InterlayerApiProxy/TasksPageApiProxy/receiveTasksPage/data-models';

/**
 * Данные в редюсер при старте получения страницы задач
 */
export type ReceiveTasksPageActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при ошибки получения страницы задач
 */
export type ReceiveTasksPageActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном получении страницы задач
 */
export type ReceiveTasksPageActionSuccessPayload = ReducerActionSuccessPayload & {
    tabVisibility: TasksPageTabVisibilityDataModel;
};

/**
 * Все возможные Action при получении страницы задач
 */
export type ReceiveTasksPageActionAnyPayload =
    | ReceiveTasksPageActionStartPayload
    | ReceiveTasksPageActionSuccessPayload
    | ReceiveTasksPageActionFailedPayload;