import { TasksPageActions } from 'app/modules/tasksPage/store/actions';
import { ReceiveTasksPageThunkParams } from 'app/modules/tasksPage/store/reducers/receiveTasksPageReducer/params';
import { ReceiveTasksPageActionFailedPayload, ReceiveTasksPageActionStartPayload, ReceiveTasksPageActionSuccessPayload } from 'app/modules/tasksPage/store/reducers/receiveTasksPageReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

const { ReceiveTasksPage: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = TasksPageActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, ReceiveTasksPageActionStartPayload, ReceiveTasksPageActionSuccessPayload, ReceiveTasksPageActionFailedPayload, ReceiveTasksPageThunkParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<ReceiveTasksPageActionStartPayload, ReceiveTasksPageActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, ReceiveTasksPageActionStartPayload, ReceiveTasksPageActionSuccessPayload, ReceiveTasksPageActionFailedPayload, ReceiveTasksPageThunkParams>;

/**
 * Thunk для запроса списка записей логирования запуска баз и открытия RDP
 */
export class ReceiveTasksPageThunk extends BaseReduxThunkObject<AppReduxStoreState, ReceiveTasksPageActionStartPayload, ReceiveTasksPageActionSuccessPayload, ReceiveTasksPageActionFailedPayload, ReceiveTasksPageThunkParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: ReceiveTasksPageThunkParams) {
        return new ReceiveTasksPageThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        const accountCardState = args.getStore().TasksPageState;
        return {
            condition: !accountCardState.hasSuccessFor.hasTasksPageReceived || args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().TasksPageState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        const tasksPageApiProxy = InterlayerApiProxy.getTasksPageApiProxy();
        try {
            const tasksPageResponse = await tasksPageApiProxy.receiveTasksPage(RequestKind.SEND_BY_ROBOT);
            args.success({
                tabVisibility: tasksPageResponse
            });
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}