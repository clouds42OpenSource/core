import { partialFillOf } from 'app/common/functions/fillObjectWithUndefined';
import { createReducer, initReducerState } from 'core/redux/functions';
import { TasksPageReducerState } from 'app/modules/tasksPage/store/reducers/TasksPageReducerState';
import { TasksPageConstants } from 'app/modules/tasksPage/constants';
import { receiveTasksPageReducer } from 'app/modules/tasksPage/store/reducers/receiveTasksPageReducer';
import { TasksPageTabVisibilityDataModel } from 'app/web/InterlayerApiProxy/TasksPageApiProxy/receiveTasksPage/data-models';

/**
 * Начальное состояние редюсера TasksPage
 */
const initialState = initReducerState<TasksPageReducerState>(
    TasksPageConstants.reducerName,
    {
        /**
         * Видимость вкладок страницы задач
         */
        tabVisibility: partialFillOf<TasksPageTabVisibilityDataModel>(),
        hasSuccessFor: {
            /**
             * Если true, то страница задач уже получена
             */
            hasTasksPageReceived: false,
        }
    }
);

/**
 * Объединённые части редюсера для всего состояния TasksPage
 */
const partialReducers = [
    receiveTasksPageReducer
];

/**
 * Редюсер AccountCard
 */
export const tasksPageReducer = createReducer(initialState, partialReducers);