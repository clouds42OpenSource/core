/**
 * Константы для модуля TasksPage
 */
export const TasksPageConstants = {
    /**
     * Название редюсера для модуля TasksPage
     */
    reducerName: 'TASKS_PAGE'
};