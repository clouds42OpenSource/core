/**
 * Процесы TasksPage
 */
export enum TasksPageProcessId {
    /**
     * Процесс для получения страницы задач
     */
    ReceiveTasksPage = 'RECEIVE_TASKS_PAGE'
}