/**
 * Константы для модуля ItsAuthorizationData
 */
export const ItsAuthorizationDataConstants = {
    /**
     * Название редюсера для модуля ItsAuthorizationData
     */
    reducerName: 'ITS_AUTHORIZATION_DATA'
};