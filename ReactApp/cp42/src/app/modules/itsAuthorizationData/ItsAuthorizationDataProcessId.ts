/**
 * Процессы справочника "Доступ к ИТС"
 */
export enum ItsAuthorizationDataProcessId {
    /**
     * Процесс на получение списка данных авторизации в ИТС
     */
    GetItsDataAuthorizations = 'GET_ITS_DATA_AUTHORIZATIONS',

    /**
     * Процесс на создание данных авторизации в ИТС
     */
    CreateItsDataAuthorization = 'CREATE_ITS_DATA_AUTHORIZATION',

    /**
     * Процесс на редактирование данных авторизации в ИТС
     */
    EditItsDataAuthorization = 'EDIT_ITS_DATA_AUTHORIZATION',

    /**
     * Процесс на получение данных авторизации в ИТС по ID
     */
    GetItsDataAuthorization = 'GET_ITS_DATA_AUTHORIZATION',

    /**
     * Процесс на удаление данных авторизации в ИТС по ID
     */
    DeleteItsDataAuthorization = 'DELETE_ITS_DATA_AUTHORIZATION',

    /**
     * Процесс на получения коллекции данных авторизации в ИТС по значению поиска
     */
    GetItsAuthorizationDataBySearchValue = 'GET_ITS_AUTHORIZATION_DATA_BY_SEARCH_VALUE'
}