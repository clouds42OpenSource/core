import { ItsAuthorizationDataProcessId } from 'app/modules/itsAuthorizationData';
import { ItsAuthorizationDataConstants } from 'app/modules/itsAuthorizationData/constants';
import { createReducerActions } from 'core/redux/functions';

/**
 * Все доступные actions для редюсера Справочник -> "Доступ к ИТС"
 */
export const ItsAuthorizationDataActions = {
    /**
     * Набор action на получение списка данных авторизации в ИТС
     */
    GetItsDataAuthorizations: createReducerActions(ItsAuthorizationDataConstants.reducerName, ItsAuthorizationDataProcessId.GetItsDataAuthorizations),

    /**
     * Набор action на создание данных авторизации в ИТС
     */
    CreateItsDataAuthorization: createReducerActions(ItsAuthorizationDataConstants.reducerName, ItsAuthorizationDataProcessId.CreateItsDataAuthorization),

    /**
     * Набор action на редактирование данных авторизации в ИТС
     */
    EditItsDataAuthorization: createReducerActions(ItsAuthorizationDataConstants.reducerName, ItsAuthorizationDataProcessId.EditItsDataAuthorization),

    /**
     * Набор action на получение данных авторизации в ИТС по ID
     */
    GetItsDataAuthorization: createReducerActions(ItsAuthorizationDataConstants.reducerName, ItsAuthorizationDataProcessId.GetItsDataAuthorization),

    /**
     * Набор action на удаление данных авторизации в ИТС по ID
     */
    DeleteItsDataAuthorization: createReducerActions(ItsAuthorizationDataConstants.reducerName, ItsAuthorizationDataProcessId.DeleteItsDataAuthorization),

    /**
     * Набор action на получения коллекции данных авторизации в ИТС по значению поиска
     */
    GetItsAuthorizationDataBySearchValue: createReducerActions(ItsAuthorizationDataConstants.reducerName, ItsAuthorizationDataProcessId.GetItsAuthorizationDataBySearchValue),
};