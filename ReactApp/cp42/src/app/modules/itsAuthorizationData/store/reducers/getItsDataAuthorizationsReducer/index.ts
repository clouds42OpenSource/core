import { getInitialMetadata } from 'app/common/functions';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { ItsAuthorizationDataProcessId } from 'app/modules/itsAuthorizationData';
import { ItsAuthorizationDataActions } from 'app/modules/itsAuthorizationData/store/actions';
import { ItsAuthorizationDataState } from 'app/modules/itsAuthorizationData/store/reducers';
import { GetItsDataAuthorizationsActionAnyPayload, GetItsDataAuthorizationsActionSuccessPayload } from 'app/modules/itsAuthorizationData/store/reducers/getItsDataAuthorizationsReducer/payloads';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения списка данных авторизации в ИТС
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const getItsDataAuthorizationsReducer: TPartialReducerObject<ItsAuthorizationDataState, GetItsDataAuthorizationsActionAnyPayload> =
    (state, action) => {
        const processId = ItsAuthorizationDataProcessId.GetItsDataAuthorizations;
        const actions = ItsAuthorizationDataActions.GetItsDataAuthorizations;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onStartProcessState: () => {
                return reducerStateProcessStart(state, processId, {
                    itsDataAuthorizations: {
                        metadata: getInitialMetadata(),
                        records: []
                    },
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasItsDataAuthorizationsReceived: false
                    }
                });
            },
            onSuccessProcessState: () => {
                const payload = action.payload as GetItsDataAuthorizationsActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    itsDataAuthorizations: payload,
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasItsDataAuthorizationsReceived: true
                    }
                });
            }
        });
    };