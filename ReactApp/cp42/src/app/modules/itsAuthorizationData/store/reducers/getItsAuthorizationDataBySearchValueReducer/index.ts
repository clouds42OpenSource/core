import { reducerActionState } from 'app/common/functions/reducerActionState';
import { ItsAuthorizationDataProcessId } from 'app/modules/itsAuthorizationData';
import { ItsAuthorizationDataActions } from 'app/modules/itsAuthorizationData/store/actions';
import { ItsAuthorizationDataState } from 'app/modules/itsAuthorizationData/store/reducers';
import { GetItsAuthorizationDataBySearchValueActionAnyPayload, GetItsAuthorizationDataBySearchValueActionSuccessPayload } from 'app/modules/itsAuthorizationData/store/reducers/getItsAuthorizationDataBySearchValueReducer/payloads';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения списка данных авторизации в ИТС по значению поиска
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const getItsAuthorizationDataBySearchValueReducer: TPartialReducerObject<ItsAuthorizationDataState, GetItsAuthorizationDataBySearchValueActionAnyPayload> =
    (state, action) => {
        const processId = ItsAuthorizationDataProcessId.GetItsAuthorizationDataBySearchValue;
        const actions = ItsAuthorizationDataActions.GetItsAuthorizationDataBySearchValue;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onStartProcessState: () => {
                return reducerStateProcessStart(state, processId, {
                    itsDataAutorizationsBySearchValue: [],
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasItsDataAutorizationsBySearchValueReceived: false
                    }
                });
            },
            onSuccessProcessState: () => {
                const payload = action.payload as GetItsAuthorizationDataBySearchValueActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    itsDataAutorizationsBySearchValue: payload,
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasItsDataAutorizationsBySearchValueReceived: true
                    }
                });
            }
        });
    };