import { ItsDataAuthorizationDataModel } from 'app/web/InterlayerApiProxy/ItsAuthorizationDataApiProxy/data-models';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при успешном выполнении получения списка данных авторизации в ИТС по значению поиска
 */
export type GetItsAuthorizationDataBySearchValueActionSuccessPayload = ReducerActionSuccessPayload & Array<ItsDataAuthorizationDataModel>;

/**
 * Все возможные Action при получении списка данных авторизации в ИТС по значению поиска
 */
export type GetItsAuthorizationDataBySearchValueActionAnyPayload =
    | ReducerActionStartPayload
    | ReducerActionFailedPayload
    | GetItsAuthorizationDataBySearchValueActionSuccessPayload;