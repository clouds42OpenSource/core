import { ItsDataAuthorizationDataModel } from 'app/web/InterlayerApiProxy/ItsAuthorizationDataApiProxy/data-models';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при успешном выполнении получения данных авторизации в ИТС
 */
export type GetItsDataAuthorizationActionSuccessPayload = ReducerActionSuccessPayload & ItsDataAuthorizationDataModel;

/**
 * Все возможные Action при получении списка данных авторизации в ИТС
 */
export type GetItsDataAuthorizationActionAnyPayload =
    | ReducerActionStartPayload
    | ReducerActionFailedPayload
    | GetItsDataAuthorizationActionSuccessPayload;