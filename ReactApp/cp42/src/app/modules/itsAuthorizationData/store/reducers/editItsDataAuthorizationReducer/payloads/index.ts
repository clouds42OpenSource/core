import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Все возможные Action при редактировании данных авторизации в ИТС
 */
export type EditItsDataAuthorizationActionAnyPayload =
    | ReducerActionStartPayload
    | ReducerActionFailedPayload
    | ReducerActionSuccessPayload;