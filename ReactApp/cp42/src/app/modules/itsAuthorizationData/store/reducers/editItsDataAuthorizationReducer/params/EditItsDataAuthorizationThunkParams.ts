import { ItsDataAuthorizationDataModel } from 'app/web/InterlayerApiProxy/ItsAuthorizationDataApiProxy/data-models';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для редактирования данных авторизации в ИТС
 */
export type EditItsDataAuthorizationThunkParams = IForceThunkParam & ItsDataAuthorizationDataModel;