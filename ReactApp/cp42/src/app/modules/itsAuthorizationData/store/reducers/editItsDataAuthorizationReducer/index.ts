import { reducerActionState } from 'app/common/functions/reducerActionState';
import { ItsAuthorizationDataProcessId } from 'app/modules/itsAuthorizationData';
import { ItsAuthorizationDataActions } from 'app/modules/itsAuthorizationData/store/actions';
import { ItsAuthorizationDataState } from 'app/modules/itsAuthorizationData/store/reducers';
import { EditItsDataAuthorizationActionAnyPayload } from 'app/modules/itsAuthorizationData/store/reducers/editItsDataAuthorizationReducer/payloads';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для редактирования данных авторизации в ИТС
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const editItsDataAuthorizationReducer: TPartialReducerObject<ItsAuthorizationDataState, EditItsDataAuthorizationActionAnyPayload> =
    (state, action) => {
        const processId = ItsAuthorizationDataProcessId.EditItsDataAuthorization;
        const actions = ItsAuthorizationDataActions.EditItsDataAuthorization;

        return reducerActionState({
            action,
            actions,
            processId,
            state
        });
    };