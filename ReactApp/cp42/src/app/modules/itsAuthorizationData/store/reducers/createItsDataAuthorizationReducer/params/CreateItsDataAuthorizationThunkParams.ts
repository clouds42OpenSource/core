import { ItsDataAuthorizationDataModel } from 'app/web/InterlayerApiProxy/ItsAuthorizationDataApiProxy/data-models';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для создания данных авторизации в ИТС
 */
export type CreateItsDataAuthorizationThunkParams = IForceThunkParam & ItsDataAuthorizationDataModel;