import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для получения модели данных авторизации в ИТС
 */
export type GetItsDataAuthorizationThunkParams = IForceThunkParam & {
    /**
     * ID модель данных авторизации в ИТС
     */
    itsAuthorizationId: string
}