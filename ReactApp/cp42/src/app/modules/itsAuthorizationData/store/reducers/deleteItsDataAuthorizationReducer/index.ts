import { reducerActionState } from 'app/common/functions/reducerActionState';
import { ItsAuthorizationDataProcessId } from 'app/modules/itsAuthorizationData';
import { ItsAuthorizationDataActions } from 'app/modules/itsAuthorizationData/store/actions';
import { ItsAuthorizationDataState } from 'app/modules/itsAuthorizationData/store/reducers';
import { DeleteItsDataAuthorizationActionAnyPayload } from 'app/modules/itsAuthorizationData/store/reducers/deleteItsDataAuthorizationReducer/payloads';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для удаления данных авторизации в ИТС
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const deleteItsDataAuthorizationReducer: TPartialReducerObject<ItsAuthorizationDataState, DeleteItsDataAuthorizationActionAnyPayload> =
    (state, action) => {
        const processId = ItsAuthorizationDataProcessId.DeleteItsDataAuthorization;
        const actions = ItsAuthorizationDataActions.DeleteItsDataAuthorization;

        return reducerActionState({
            action,
            actions,
            processId,
            state
        });
    };