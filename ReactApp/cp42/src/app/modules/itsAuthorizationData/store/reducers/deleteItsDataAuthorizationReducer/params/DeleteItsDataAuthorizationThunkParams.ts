import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для удаления данных авторизации в ИТС
 */
export type DeleteItsDataAuthorizationThunkParams = IForceThunkParam & {
    /**
     * ID модель данных авторизации в ИТС
     */
    itsAuthorizationId: string
}