import { partialFillOf } from 'app/common/functions';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { ItsAuthorizationDataProcessId } from 'app/modules/itsAuthorizationData';
import { ItsAuthorizationDataActions } from 'app/modules/itsAuthorizationData/store/actions';
import { ItsAuthorizationDataState } from 'app/modules/itsAuthorizationData/store/reducers';
import { GetItsDataAuthorizationActionAnyPayload, GetItsDataAuthorizationActionSuccessPayload } from 'app/modules/itsAuthorizationData/store/reducers/getItsDataAuthorizationReducer/payloads';
import { ItsDataAuthorizationDataModel } from 'app/web/InterlayerApiProxy/ItsAuthorizationDataApiProxy/data-models';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения данных авторизации в ИТС
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const getItsDataAuthorizationReducer: TPartialReducerObject<ItsAuthorizationDataState, GetItsDataAuthorizationActionAnyPayload> =
    (state, action) => {
        const processId = ItsAuthorizationDataProcessId.GetItsDataAuthorization;
        const actions = ItsAuthorizationDataActions.GetItsDataAuthorization;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onStartProcessState: () => {
                return reducerStateProcessStart(state, processId, {
                    itsDataAuthorization: partialFillOf<ItsDataAuthorizationDataModel>()
                });
            },
            onSuccessProcessState: () => {
                const payload = action.payload as GetItsDataAuthorizationActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    itsDataAuthorization: payload
                });
            }
        });
    };