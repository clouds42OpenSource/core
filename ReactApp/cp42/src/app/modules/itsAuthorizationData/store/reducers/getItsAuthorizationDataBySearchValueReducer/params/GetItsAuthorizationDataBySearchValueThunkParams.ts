import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для получения коллекции данных авторизации в ИТС по значению поиска
 */
export type GetItsAuthorizationDataBySearchValueThunkParams = IForceThunkParam & {
    /**
     * Значения поиска
     */
    searchValue?: string;
}