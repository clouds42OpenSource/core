import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Все возможные Action при удалении данных авторизации в ИТС
 */
export type DeleteItsDataAuthorizationActionAnyPayload =
    | ReducerActionStartPayload
    | ReducerActionFailedPayload
    | ReducerActionSuccessPayload;