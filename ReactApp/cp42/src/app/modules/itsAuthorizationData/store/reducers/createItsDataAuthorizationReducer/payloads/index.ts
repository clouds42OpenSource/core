import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Все возможные Action при создании данных авторизации в ИТС
 */
export type CreateItsDataAuthorizationActionAnyPayload =
    | ReducerActionStartPayload
    | ReducerActionFailedPayload
    | ReducerActionSuccessPayload;