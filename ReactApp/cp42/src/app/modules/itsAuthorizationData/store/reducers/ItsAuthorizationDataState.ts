import { ItsAuthorizationDataProcessId } from 'app/modules/itsAuthorizationData';
import { ItsAuthorizationDataPaginationDataModel, ItsDataAuthorizationDataModel } from 'app/web/InterlayerApiProxy/ItsAuthorizationDataApiProxy/data-models';
import { IReducerState } from 'core/redux/interfaces';

/**
 * Состояние редюсера для работы с процесами справочника -> "Доступ к ИТС"
 */
export type ItsAuthorizationDataState = IReducerState<ItsAuthorizationDataProcessId> & {
    /**
     * Массив данных авторизации в ИТС
     */
    itsDataAuthorizations: ItsAuthorizationDataPaginationDataModel,

    /**
     * Модель данных авторизации в ИТС
     */
    itsDataAuthorization: ItsDataAuthorizationDataModel,

    /**
     * Массив данных авторизации в ИТС полученный по значению поиска
     */
    itsDataAutorizationsBySearchValue: Array<ItsDataAuthorizationDataModel>,

    /**
     * содержит ярлыки для фиксации загрузок данных, была ли загрузка или ещё нет
     */
    hasSuccessFor: {
        /**
         * Если true, то массив данных авторизации в ИТС получены
         */
        hasItsDataAuthorizationsReceived: boolean;

        /**
         * Если true, то массив данных авторизации в ИТС по значению поиска получены
         */
        hasItsDataAutorizationsBySearchValueReceived: boolean;
    };
};