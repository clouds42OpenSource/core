import { getInitialMetadata, partialFillOf } from 'app/common/functions';
import { ItsAuthorizationDataConstants } from 'app/modules/itsAuthorizationData/constants';
import { ItsAuthorizationDataState } from 'app/modules/itsAuthorizationData/store/reducers';
import { createItsDataAuthorizationReducer } from 'app/modules/itsAuthorizationData/store/reducers/createItsDataAuthorizationReducer';
import { deleteItsDataAuthorizationReducer } from 'app/modules/itsAuthorizationData/store/reducers/deleteItsDataAuthorizationReducer';
import { editItsDataAuthorizationReducer } from 'app/modules/itsAuthorizationData/store/reducers/editItsDataAuthorizationReducer';
import { getItsAuthorizationDataBySearchValueReducer } from 'app/modules/itsAuthorizationData/store/reducers/getItsAuthorizationDataBySearchValueReducer';
import { getItsDataAuthorizationReducer } from 'app/modules/itsAuthorizationData/store/reducers/getItsDataAuthorizationReducer';
import { getItsDataAuthorizationsReducer } from 'app/modules/itsAuthorizationData/store/reducers/getItsDataAuthorizationsReducer';
import { ItsDataAuthorizationDataModel } from 'app/web/InterlayerApiProxy/ItsAuthorizationDataApiProxy/data-models';
import { createReducer, initReducerState } from 'core/redux/functions';

/**
 * Начальное состояние редюсера справочника -> "Доступ к ИТС"
 */
const initialState = initReducerState<ItsAuthorizationDataState>(
    ItsAuthorizationDataConstants.reducerName,
    {
        itsDataAuthorizations: {
            metadata: getInitialMetadata(),
            records: []
        },
        itsDataAuthorization: partialFillOf<ItsDataAuthorizationDataModel>(),
        itsDataAutorizationsBySearchValue: [],
        hasSuccessFor: {
            hasItsDataAuthorizationsReceived: false,
            hasItsDataAutorizationsBySearchValueReceived: false
        }
    }
);

/**
 * Объединённые части редюсера для всего состояния справочника -> "Доступ к ИТС"
 */
const partialReducers = [
    getItsDataAuthorizationsReducer,
    deleteItsDataAuthorizationReducer,
    getItsDataAuthorizationReducer,
    createItsDataAuthorizationReducer,
    editItsDataAuthorizationReducer,
    getItsAuthorizationDataBySearchValueReducer
];

/**
 * Редюсер справочника -> "Доступ к ИТС"
 */
export const ItsAuthorizationDataReducer = createReducer(initialState, partialReducers);