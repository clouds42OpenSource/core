import { ItsAuthorizationFilterParams } from 'app/web/InterlayerApiProxy/ItsAuthorizationDataApiProxy/params/ItsAuthorizationFilterParams';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для получения списка данных авторизации в ИТС
 */
export type GetItsDataAuthorizationsThunkParams = IForceThunkParam & ItsAuthorizationFilterParams;