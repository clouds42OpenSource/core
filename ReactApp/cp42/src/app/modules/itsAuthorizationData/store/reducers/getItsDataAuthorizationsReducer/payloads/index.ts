import { ItsAuthorizationDataPaginationDataModel } from 'app/web/InterlayerApiProxy/ItsAuthorizationDataApiProxy/data-models';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при успешном выполнении получения списка данных авторизации в ИТС
 */
export type GetItsDataAuthorizationsActionSuccessPayload = ReducerActionSuccessPayload & ItsAuthorizationDataPaginationDataModel;

/**
 * Все возможные Action при получении списка данных авторизации в ИТС
 */
export type GetItsDataAuthorizationsActionAnyPayload =
    | ReducerActionStartPayload
    | ReducerActionFailedPayload
    | GetItsDataAuthorizationsActionSuccessPayload;