import { reducerActionState } from 'app/common/functions/reducerActionState';
import { ItsAuthorizationDataProcessId } from 'app/modules/itsAuthorizationData';
import { ItsAuthorizationDataActions } from 'app/modules/itsAuthorizationData/store/actions';
import { ItsAuthorizationDataState } from 'app/modules/itsAuthorizationData/store/reducers';
import { CreateItsDataAuthorizationActionAnyPayload } from 'app/modules/itsAuthorizationData/store/reducers/createItsDataAuthorizationReducer/payloads';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для создания данных авторизации в ИТС
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const createItsDataAuthorizationReducer: TPartialReducerObject<ItsAuthorizationDataState, CreateItsDataAuthorizationActionAnyPayload> =
    (state, action) => {
        const processId = ItsAuthorizationDataProcessId.CreateItsDataAuthorization;
        const actions = ItsAuthorizationDataActions.CreateItsDataAuthorization;

        return reducerActionState({
            action,
            actions,
            processId,
            state
        });
    };