/**
 * Процессы ТиИ/АО "АРМ Автообновления"
 */
export enum ArmAutoUpdateAccountDatabaseProcessId {
    /**
     * Процесс на получение списка инф. баз, подключенных к АО
     */
    GetConnectedDatabasesOnAutoUpdates = 'GET_CONNECTED_DATABASE_ON_AUTO_UPDATES',

    /**
     * Процесс на получение списка инф. баз, в очереди на АО
     */
    GetDatabaseInAutoUpdateQueues = 'GET_DATABASE_IN_AUTO_UPDATE_QUEUES',

    /**
     * Процесс на получение детали авто обновления конфигурации 1С
     */
    GetAutoUpdateConfiguration1CDetails = 'GET_AUTO_UPDATE_CONFIGURATION_1C_DETAILS',

    /**
     * Процесс на получение списка названия конфигураций 1С
     */
    GetConfigurationNames = 'GET_CONFIGURATION_NAMES',

    /**
     * Процесс на получение списка данных по выполненным АО инф. баз
     */
    GetPerformedAutoUpdateForDatabases = 'GET_PERFORMED_AUTO_UPDATE_FOR_DATABASE_DATA',

    /**
     * Процесс на получение списка воркеров которые выполняют авто обновление
     */
    GetWorkers = 'GET_WORKERS',

    /**
     * Процесс на получение списка данных технических результатов АО инф. базы
     */
    GetDatabaseAutoUpdateTechnicalResults = 'GET_DATABASE_AUTO_UPDATE_TECHNICAL_RESULTS'
}