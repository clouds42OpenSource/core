/**
 * Константы для модуля ArmAutoUpdateAccountDatabase
 */
export const ArmAutoUpdateAccountDatabaseConstants = {
    /**
     * Название редюсера для модуля ArmAutoUpdateAccountDatabase
     */
    reducerName: 'ARM_AUTO_UPDATE_ACCOUNT_DATABASE'
};