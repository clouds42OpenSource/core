import { SelectDataResultMetadataModel } from 'app/web/common';
import { PerformedAutoUpdateForDatabasesDataModel } from 'app/web/InterlayerApiProxy/ArmAutoUpdateAccountDatabaseApiProxy/data-models';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при успешном выполнении получения списка данных по выполненным АО инф. баз
 */
export type GetPerformedAutoUpdateForDatabasesActionSuccessPayload = ReducerActionSuccessPayload & SelectDataResultMetadataModel<PerformedAutoUpdateForDatabasesDataModel>;

/**
 * Все возможные Action при получении списка данных по выполненным АО инф. баз
 */
export type GetPerformedAutoUpdateForDatabasesActionAnyPayload =
    | ReducerActionStartPayload
    | ReducerActionFailedPayload
    | ReducerActionSuccessPayload;