import { SelectDataResultCommonDataModel } from 'app/web/common';
import { DatabaseAutoUpdateTechnicalResultDataModel } from 'app/web/InterlayerApiProxy/ArmAutoUpdateAccountDatabaseApiProxy/data-models';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при успешном выполнении получения данных технических результатов АО инф. базы
 */
export type GetDatabaseAutoUpdateTechnicalResultsActionSuccessPayload = ReducerActionSuccessPayload & SelectDataResultCommonDataModel<DatabaseAutoUpdateTechnicalResultDataModel>;

/**
 * Все возможные Action при получении списка данных технических результатов АО инф. базы
 */
export type GetDatabaseAutoUpdateTechnicalResultsActionAnyPayload =
    | ReducerActionStartPayload
    | ReducerActionFailedPayload
    | ReducerActionSuccessPayload;