import { SelectDataResultMetadataModel } from 'app/web/common';
import { ConnectedDatabaseOnAutoUpdateDataModel } from 'app/web/InterlayerApiProxy/ArmAutoUpdateAccountDatabaseApiProxy/data-models';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при успешном выполнении получения списка данных базы подключенной на АО
 */
export type GetConnectedDatabasesOnAutoUpdatesActionSuccessPayload = ReducerActionSuccessPayload & SelectDataResultMetadataModel<ConnectedDatabaseOnAutoUpdateDataModel>;

/**
 * Все возможные Action при получении списка данных базы подключенной на АО
 */
export type GetConnectedDatabasesOnAutoUpdatesActionAnyPayload =
    | ReducerActionStartPayload
    | ReducerActionFailedPayload
    | GetConnectedDatabasesOnAutoUpdatesActionSuccessPayload;