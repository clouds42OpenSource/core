import { getInitialMetadata } from 'app/common/functions';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { ArmAutoUpdateAccountDatabaseProcessId } from 'app/modules/armAutoUpdateAccountDatabase';
import { ArmAutoUpdateAccountDatabaseActions } from 'app/modules/armAutoUpdateAccountDatabase/store/actions';
import { ArmAutoUpdateAccountDatabaseState } from 'app/modules/armAutoUpdateAccountDatabase/store/reducers';
import { GetConnectedDatabasesOnAutoUpdatesActionAnyPayload, GetConnectedDatabasesOnAutoUpdatesActionSuccessPayload } from 'app/modules/armAutoUpdateAccountDatabase/store/reducers/getConnectedDatabasesOnAutoUpdatesReducer/payloads';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения списка данных инф. баз, подключенных к АО
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const getConnectedDatabasesOnAutoUpdatesReducer: TPartialReducerObject<ArmAutoUpdateAccountDatabaseState, GetConnectedDatabasesOnAutoUpdatesActionAnyPayload> =
    (state, action) => {
        const processId = ArmAutoUpdateAccountDatabaseProcessId.GetConnectedDatabasesOnAutoUpdates;
        const actions = ArmAutoUpdateAccountDatabaseActions.GetConnectedDatabasesOnAutoUpdates;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onStartProcessState: () => {
                return reducerStateProcessStart(state, processId, {
                    connectedDatabasesOnAutoUpdates: {
                        metadata: getInitialMetadata(),
                        records: []
                    },
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasConnectedDatabasesOnAutoUpdateReceived: false
                    }
                });
            },
            onSuccessProcessState: () => {
                const payload = action.payload as GetConnectedDatabasesOnAutoUpdatesActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    connectedDatabasesOnAutoUpdates: payload,
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasConnectedDatabasesOnAutoUpdateReceived: true
                    }
                });
            }
        });
    };