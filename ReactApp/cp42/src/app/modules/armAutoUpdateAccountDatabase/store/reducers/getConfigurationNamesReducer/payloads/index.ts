import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при успешном выполнении получения списка названия конфигураций 1С
 */
export type GetConfigurationNamesActionSuccessPayload = ReducerActionSuccessPayload & Array<string>;

/**
 * Все возможные Action при получении списка названия конфигураций 1С
 */
export type GetConfigurationNamesActionAnyPayload =
    | ReducerActionStartPayload
    | ReducerActionFailedPayload
    | ReducerActionSuccessPayload;