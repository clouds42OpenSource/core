import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при успешном выполнении получения списка воркеров которые выполняют авто обновление
 */
export type GetWorkersActionSuccessPayload = ReducerActionSuccessPayload & Array<number>;

/**
 * Все возможные Action при получении списка воркеров которые выполняют авто обновление
 */
export type GetWorkersActionAnyPayload =
    | ReducerActionStartPayload
    | ReducerActionFailedPayload
    | ReducerActionSuccessPayload;