import { reducerActionState } from 'app/common/functions/reducerActionState';
import { ArmAutoUpdateAccountDatabaseProcessId } from 'app/modules/armAutoUpdateAccountDatabase';
import { ArmAutoUpdateAccountDatabaseActions } from 'app/modules/armAutoUpdateAccountDatabase/store/actions';
import { ArmAutoUpdateAccountDatabaseState } from 'app/modules/armAutoUpdateAccountDatabase/store/reducers';
import { GetConfigurationNamesActionAnyPayload, GetConfigurationNamesActionSuccessPayload } from 'app/modules/armAutoUpdateAccountDatabase/store/reducers/getConfigurationNamesReducer/payloads';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения списка названия конфигураций 1С
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const getConfigurationNamesReducer: TPartialReducerObject<ArmAutoUpdateAccountDatabaseState, GetConfigurationNamesActionAnyPayload> =
    (state, action) => {
        const processId = ArmAutoUpdateAccountDatabaseProcessId.GetConfigurationNames;
        const actions = ArmAutoUpdateAccountDatabaseActions.GetConfigurationNames;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onStartProcessState: () => {
                return reducerStateProcessStart(state, processId, {
                    configurationNames: [],
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasConfigurationNamesReceived: false
                    }
                });
            },
            onSuccessProcessState: () => {
                const payload = action.payload as GetConfigurationNamesActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    configurationNames: payload,
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasConfigurationNamesReceived: true
                    }
                });
            }
        });
    };