import { SelectDataResultMetadataModel } from 'app/web/common';
import { DatabaseInAutoUpdateQueueDataModel } from 'app/web/InterlayerApiProxy/ArmAutoUpdateAccountDatabaseApiProxy/data-models';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при успешном выполнении получения списка инф. баз, в очереди на АО
 */
export type GetDatabaseInAutoUpdateQueuesActionSuccessPayload = ReducerActionSuccessPayload & SelectDataResultMetadataModel<DatabaseInAutoUpdateQueueDataModel>;

/**
 * Все возможные Action при получении списка инф. баз, в очереди на АО
 */
export type GetDatabaseInAutoUpdateQueuesActionAnyPayload =
    | ReducerActionStartPayload
    | ReducerActionFailedPayload
    | ReducerActionSuccessPayload;