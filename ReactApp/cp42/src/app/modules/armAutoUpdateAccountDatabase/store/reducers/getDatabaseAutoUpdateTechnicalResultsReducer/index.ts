import { partialFillOf } from 'app/common/functions';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { ArmAutoUpdateAccountDatabaseProcessId } from 'app/modules/armAutoUpdateAccountDatabase';
import { ArmAutoUpdateAccountDatabaseActions } from 'app/modules/armAutoUpdateAccountDatabase/store/actions';
import { ArmAutoUpdateAccountDatabaseState } from 'app/modules/armAutoUpdateAccountDatabase/store/reducers';
import {
    GetDatabaseAutoUpdateTechnicalResultsActionAnyPayload,
    GetDatabaseAutoUpdateTechnicalResultsActionSuccessPayload
} from 'app/modules/armAutoUpdateAccountDatabase/store/reducers/getDatabaseAutoUpdateTechnicalResultsReducer/payloads';
import { PaginationDataModel } from 'app/web/common';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения списка данных технических результатов АО инф. базы
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const getDatabaseAutoUpdateTechnicalResultsReducer: TPartialReducerObject<ArmAutoUpdateAccountDatabaseState, GetDatabaseAutoUpdateTechnicalResultsActionAnyPayload> =
    (state, action) => {
        const processId = ArmAutoUpdateAccountDatabaseProcessId.GetDatabaseAutoUpdateTechnicalResults;
        const actions = ArmAutoUpdateAccountDatabaseActions.GetDatabaseAutoUpdateTechnicalResults;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onStartProcessState: () => {
                return reducerStateProcessStart(state, processId, {
                    databaseAutoUpdateTechnicalResults: {
                        pagination: partialFillOf<PaginationDataModel>(),
                        records: []
                    },
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasDatabaseAutoUpdateTechnicalResultsReceived: false
                    }
                });
            },
            onSuccessProcessState: () => {
                const payload = action.payload as GetDatabaseAutoUpdateTechnicalResultsActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    databaseAutoUpdateTechnicalResults: payload,
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasDatabaseAutoUpdateTechnicalResultsReceived: true
                    }
                });
            }
        });
    };