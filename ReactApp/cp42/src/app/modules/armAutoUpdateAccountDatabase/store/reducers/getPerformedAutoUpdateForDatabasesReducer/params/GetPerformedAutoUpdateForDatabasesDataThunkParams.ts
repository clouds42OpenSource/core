import { PerformedAutoUpdateForDatabasesFilterParams } from 'app/web/InterlayerApiProxy/ArmAutoUpdateAccountDatabaseApiProxy/params';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для получения данных по выполненным АО инф. баз
 */
export type GetPerformedAutoUpdateForDatabasesDataThunkParams = IForceThunkParam & PerformedAutoUpdateForDatabasesFilterParams;