import { DatabaseAutoUpdateTechnicalResultFilterParams } from 'app/web/InterlayerApiProxy/ArmAutoUpdateAccountDatabaseApiProxy/params';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для получения данных технического результата АО инф. базы
 */
export type GetDatabaseAutoUpdateTechnicalResultsThunkParams = IForceThunkParam & DatabaseAutoUpdateTechnicalResultFilterParams;