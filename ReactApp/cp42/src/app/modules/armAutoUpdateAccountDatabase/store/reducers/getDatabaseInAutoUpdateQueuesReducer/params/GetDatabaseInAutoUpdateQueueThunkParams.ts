import { DatabaseInAutoUpdateQueueFilterParams } from 'app/web/InterlayerApiProxy/ArmAutoUpdateAccountDatabaseApiProxy/params';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для получения списка инф. баз, в очереди на АО
 */
export type GetDatabaseInAutoUpdateQueueThunkParams = IForceThunkParam & DatabaseInAutoUpdateQueueFilterParams;