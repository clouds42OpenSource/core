import { partialFillOf } from 'app/common/functions';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { ArmAutoUpdateAccountDatabaseProcessId } from 'app/modules/armAutoUpdateAccountDatabase';
import { ArmAutoUpdateAccountDatabaseActions } from 'app/modules/armAutoUpdateAccountDatabase/store/actions';
import { ArmAutoUpdateAccountDatabaseState } from 'app/modules/armAutoUpdateAccountDatabase/store/reducers';
import { GetAutoUpdateConfiguration1CDetailsActionAnyPayload, GetAutoUpdateConfiguration1CDetailsActionSuccessPayload } from 'app/modules/armAutoUpdateAccountDatabase/store/reducers/getAutoUpdateConfiguration1CDetailsReducer/payloads';
import { AutoUpdateConfiguration1CDetailsDataModel } from 'app/web/InterlayerApiProxy/ArmAutoUpdateAccountDatabaseApiProxy/data-models';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения детали авто обновления конфигурации 1С
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const getAutoUpdateConfiguration1CDetailsReducer: TPartialReducerObject<ArmAutoUpdateAccountDatabaseState, GetAutoUpdateConfiguration1CDetailsActionAnyPayload> =
    (state, action) => {
        const processId = ArmAutoUpdateAccountDatabaseProcessId.GetAutoUpdateConfiguration1CDetails;
        const actions = ArmAutoUpdateAccountDatabaseActions.GetAutoUpdateConfiguration1CDetails;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onStartProcessState: () => {
                return reducerStateProcessStart(state, processId, {
                    autoUpdateConfiguration1CDetailsDataModel: partialFillOf<AutoUpdateConfiguration1CDetailsDataModel>()
                });
            },
            onSuccessProcessState: () => {
                const payload = action.payload as GetAutoUpdateConfiguration1CDetailsActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    autoUpdateConfiguration1CDetailsDataModel: payload
                });
            }
        });
    };