import { ArmAutoUpdateAccountDatabaseProcessId } from 'app/modules/armAutoUpdateAccountDatabase';
import { SelectDataResultCommonDataModel, SelectDataResultMetadataModel } from 'app/web/common';
import {
    AutoUpdateConfiguration1CDetailsDataModel,
    ConnectedDatabaseOnAutoUpdateDataModel,
    DatabaseAutoUpdateTechnicalResultDataModel,
    DatabaseInAutoUpdateQueueDataModel,
    PerformedAutoUpdateForDatabasesDataModel
} from 'app/web/InterlayerApiProxy/ArmAutoUpdateAccountDatabaseApiProxy/data-models';
import { IReducerState } from 'core/redux/interfaces';

/**
 * Состояние редюсера для работы с процесами ТиИ/АО "АРМ Автообновления"
 */
export type ArmAutoUpdateAccountDatabaseState = IReducerState<ArmAutoUpdateAccountDatabaseProcessId> & {
    /**
     * Массив данных базы подключенной на АО
     */
    connectedDatabasesOnAutoUpdates: SelectDataResultMetadataModel<ConnectedDatabaseOnAutoUpdateDataModel>,

    /**
     * Массив данных инф. баз, в очереди на АО
     */
    databaseInAutoUpdateQueues: SelectDataResultMetadataModel<DatabaseInAutoUpdateQueueDataModel>,

    /**
     * Модель детали авто обновления конфигурации 1С
     */
    autoUpdateConfiguration1CDetailsDataModel: AutoUpdateConfiguration1CDetailsDataModel,

    /**
     * Массив названия конфигураций 1С
     */
    configurationNames: Array<string>,

    /**
     * Массив данных выполненного АО базы
     */
    performedAutoUpdateForDatabases: SelectDataResultMetadataModel<PerformedAutoUpdateForDatabasesDataModel>,

    /**
     * Список воркеров
     */
    workers: Array<number>,

    /**
     * Массив данных технического результата АО инф. базы
     */
    databaseAutoUpdateTechnicalResults: SelectDataResultCommonDataModel<DatabaseAutoUpdateTechnicalResultDataModel>,

    /**
     * Содержит ярлыки для фиксации загрузок данных, была ли загрузка или ещё нет
     */
    hasSuccessFor: {
        /**
         * Если true, то массив данных базы подключенной на АО получены
         */
        hasConnectedDatabasesOnAutoUpdateReceived: boolean;

        /**
         * Если true, то массив данных инф. баз, в очереди на АО получены
         */
        hasDatabaseInAutoUpdateQueuesReceived: boolean;

        /**
         * Если true, то массив названия конфигураций 1С получены
         */
        hasConfigurationNamesReceived: boolean;

        /**
         * Если true, то массив данных выполненного АО базы получены
         */
        hasPerformedAutoUpdateForDatabasesReceived: boolean;

        /**
         * Если true, то массив данных выполненного АО базы получены
         */
        hasDatabaseAutoUpdateTechnicalResultsReceived: boolean;
    };
};