import { AutoUpdateConfiguration1CDetailsDataModel } from 'app/web/InterlayerApiProxy/ArmAutoUpdateAccountDatabaseApiProxy/data-models';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при успешном выполнении получения детали авто обновления конфигурации 1С
 */
export type GetAutoUpdateConfiguration1CDetailsActionSuccessPayload = ReducerActionSuccessPayload & AutoUpdateConfiguration1CDetailsDataModel;

/**
 * Все возможные Action при получении детали авто обновления конфигурации 1С
 */
export type GetAutoUpdateConfiguration1CDetailsActionAnyPayload =
    | ReducerActionStartPayload
    | ReducerActionFailedPayload
    | ReducerActionSuccessPayload;