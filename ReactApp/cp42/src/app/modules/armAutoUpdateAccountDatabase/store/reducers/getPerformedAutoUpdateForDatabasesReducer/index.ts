import { getInitialMetadata } from 'app/common/functions';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { ArmAutoUpdateAccountDatabaseProcessId } from 'app/modules/armAutoUpdateAccountDatabase';
import { ArmAutoUpdateAccountDatabaseActions } from 'app/modules/armAutoUpdateAccountDatabase/store/actions';
import { ArmAutoUpdateAccountDatabaseState } from 'app/modules/armAutoUpdateAccountDatabase/store/reducers';
import { GetPerformedAutoUpdateForDatabasesActionAnyPayload, GetPerformedAutoUpdateForDatabasesActionSuccessPayload } from 'app/modules/armAutoUpdateAccountDatabase/store/reducers/getPerformedAutoUpdateForDatabasesReducer/payloads';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения списка данных по выполненным АО инф. баз
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const getPerformedAutoUpdateForDatabasesReducer: TPartialReducerObject<ArmAutoUpdateAccountDatabaseState, GetPerformedAutoUpdateForDatabasesActionAnyPayload> =
    (state, action) => {
        const processId = ArmAutoUpdateAccountDatabaseProcessId.GetPerformedAutoUpdateForDatabases;
        const actions = ArmAutoUpdateAccountDatabaseActions.GetPerformedAutoUpdateForDatabases;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onStartProcessState: () => {
                return reducerStateProcessStart(state, processId, {
                    performedAutoUpdateForDatabases: {
                        metadata: getInitialMetadata(),
                        records: []
                    },
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasPerformedAutoUpdateForDatabasesReceived: false
                    }
                });
            },
            onSuccessProcessState: () => {
                const payload = action.payload as GetPerformedAutoUpdateForDatabasesActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    performedAutoUpdateForDatabases: payload,
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasPerformedAutoUpdateForDatabasesReceived: true
                    }
                });
            }
        });
    };