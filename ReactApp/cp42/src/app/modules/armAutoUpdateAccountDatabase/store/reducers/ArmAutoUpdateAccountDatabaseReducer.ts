import { getInitialMetadata, partialFillOf } from 'app/common/functions';
import { ArmAutoUpdateAccountDatabaseConstants } from 'app/modules/armAutoUpdateAccountDatabase/constants';
import { ArmAutoUpdateAccountDatabaseState } from 'app/modules/armAutoUpdateAccountDatabase/store/reducers';
import { getAutoUpdateConfiguration1CDetailsReducer } from 'app/modules/armAutoUpdateAccountDatabase/store/reducers/getAutoUpdateConfiguration1CDetailsReducer';
import { getConfigurationNamesReducer } from 'app/modules/armAutoUpdateAccountDatabase/store/reducers/getConfigurationNamesReducer';
import { getConnectedDatabasesOnAutoUpdatesReducer } from 'app/modules/armAutoUpdateAccountDatabase/store/reducers/getConnectedDatabasesOnAutoUpdatesReducer';
import { getDatabaseAutoUpdateTechnicalResultsReducer } from 'app/modules/armAutoUpdateAccountDatabase/store/reducers/getDatabaseAutoUpdateTechnicalResultsReducer';
import { getDatabaseInAutoUpdateQueuesReducer } from 'app/modules/armAutoUpdateAccountDatabase/store/reducers/getDatabaseInAutoUpdateQueuesReducer';
import { getPerformedAutoUpdateForDatabasesReducer } from 'app/modules/armAutoUpdateAccountDatabase/store/reducers/getPerformedAutoUpdateForDatabasesReducer';
import { getWorkersReducer } from 'app/modules/armAutoUpdateAccountDatabase/store/reducers/getWorkersReducer';
import { PaginationDataModel } from 'app/web/common/data-models';
import { AutoUpdateConfiguration1CDetailsDataModel } from 'app/web/InterlayerApiProxy/ArmAutoUpdateAccountDatabaseApiProxy/data-models';
import { createReducer, initReducerState } from 'core/redux/functions';

/**
 * Начальное состояние редюсера ТиИ/АО -> "АРМ Автообновления"
 */
const initialState = initReducerState<ArmAutoUpdateAccountDatabaseState>(
    ArmAutoUpdateAccountDatabaseConstants.reducerName,
    {
        connectedDatabasesOnAutoUpdates: {
            metadata: getInitialMetadata(),
            records: []
        },
        databaseInAutoUpdateQueues: {
            metadata: getInitialMetadata(),
            records: []
        },
        autoUpdateConfiguration1CDetailsDataModel: partialFillOf<AutoUpdateConfiguration1CDetailsDataModel>(),
        configurationNames: [],
        performedAutoUpdateForDatabases: {
            metadata: getInitialMetadata(),
            records: []
        },
        workers: [],
        databaseAutoUpdateTechnicalResults: {
            pagination: partialFillOf<PaginationDataModel>(),
            records: []
        },
        hasSuccessFor: {
            hasConnectedDatabasesOnAutoUpdateReceived: false,
            hasDatabaseInAutoUpdateQueuesReceived: false,
            hasConfigurationNamesReceived: false,
            hasPerformedAutoUpdateForDatabasesReceived: false,
            hasDatabaseAutoUpdateTechnicalResultsReceived: false
        }
    }
);

/**
 * Объединённые части редюсера для всего состояния ТиИ/АО -> "АРМ Автообновления"
 */
const partialReducers = [
    getConnectedDatabasesOnAutoUpdatesReducer,
    getDatabaseInAutoUpdateQueuesReducer,
    getAutoUpdateConfiguration1CDetailsReducer,
    getConfigurationNamesReducer,
    getPerformedAutoUpdateForDatabasesReducer,
    getWorkersReducer,
    getDatabaseAutoUpdateTechnicalResultsReducer
];

/**
 * Редюсер ТиИ/АО -> "АРМ Автообновления"
 */
export const ArmAutoUpdateAccountDatabaseReducer = createReducer(initialState, partialReducers);