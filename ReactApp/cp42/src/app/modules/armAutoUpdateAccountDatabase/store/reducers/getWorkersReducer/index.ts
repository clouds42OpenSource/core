import { reducerActionState } from 'app/common/functions/reducerActionState';
import { ArmAutoUpdateAccountDatabaseProcessId } from 'app/modules/armAutoUpdateAccountDatabase';
import { ArmAutoUpdateAccountDatabaseActions } from 'app/modules/armAutoUpdateAccountDatabase/store/actions';
import { ArmAutoUpdateAccountDatabaseState } from 'app/modules/armAutoUpdateAccountDatabase/store/reducers';
import { GetWorkersActionAnyPayload, GetWorkersActionSuccessPayload } from 'app/modules/armAutoUpdateAccountDatabase/store/reducers/getWorkersReducer/payloads';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения списка воркеров которые выполняют авто обновление
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const getWorkersReducer: TPartialReducerObject<ArmAutoUpdateAccountDatabaseState, GetWorkersActionAnyPayload> =
    (state, action) => {
        const processId = ArmAutoUpdateAccountDatabaseProcessId.GetWorkers;
        const actions = ArmAutoUpdateAccountDatabaseActions.GetWorkers;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onStartProcessState: () => {
                return reducerStateProcessStart(state, processId, {
                    workers: []
                });
            },
            onSuccessProcessState: () => {
                const payload = action.payload as GetWorkersActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    workers: payload
                });
            }
        });
    };