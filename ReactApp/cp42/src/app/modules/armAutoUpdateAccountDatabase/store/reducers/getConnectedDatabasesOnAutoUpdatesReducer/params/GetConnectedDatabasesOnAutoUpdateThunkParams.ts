import { ConnectedDatabaseOnAutoUpdateFilterParams } from 'app/web/InterlayerApiProxy/ArmAutoUpdateAccountDatabaseApiProxy/params';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для получения списка инф. баз, подключенных к АО
 */
export type GetConnectedDatabasesOnAutoUpdateThunkParams = IForceThunkParam & ConnectedDatabaseOnAutoUpdateFilterParams;