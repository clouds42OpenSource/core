import { getInitialMetadata } from 'app/common/functions';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { ArmAutoUpdateAccountDatabaseProcessId } from 'app/modules/armAutoUpdateAccountDatabase';
import { ArmAutoUpdateAccountDatabaseActions } from 'app/modules/armAutoUpdateAccountDatabase/store/actions';
import { ArmAutoUpdateAccountDatabaseState } from 'app/modules/armAutoUpdateAccountDatabase/store/reducers';
import { GetDatabaseInAutoUpdateQueuesActionAnyPayload, GetDatabaseInAutoUpdateQueuesActionSuccessPayload } from 'app/modules/armAutoUpdateAccountDatabase/store/reducers/getDatabaseInAutoUpdateQueuesReducer/payloads';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения списка инф. баз, в очереди на АО
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const getDatabaseInAutoUpdateQueuesReducer: TPartialReducerObject<ArmAutoUpdateAccountDatabaseState, GetDatabaseInAutoUpdateQueuesActionAnyPayload> =
    (state, action) => {
        const processId = ArmAutoUpdateAccountDatabaseProcessId.GetDatabaseInAutoUpdateQueues;
        const actions = ArmAutoUpdateAccountDatabaseActions.GetDatabaseInAutoUpdateQueues;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onStartProcessState: () => {
                return reducerStateProcessStart(state, processId, {
                    databaseInAutoUpdateQueues: {
                        metadata: getInitialMetadata(),
                        records: []
                    },
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasDatabaseInAutoUpdateQueuesReceived: false
                    }
                });
            },
            onSuccessProcessState: () => {
                const payload = action.payload as GetDatabaseInAutoUpdateQueuesActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    databaseInAutoUpdateQueues: payload,
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasDatabaseInAutoUpdateQueuesReceived: true
                    }
                });
            }
        });
    };