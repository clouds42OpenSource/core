import { AutoUpdateConfiguration1CDetailsFilterParams } from 'app/web/InterlayerApiProxy/ArmAutoUpdateAccountDatabaseApiProxy/params';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для получения детали авто обновления конфигурации 1С
 */
export type GetAutoUpdateConfiguration1CDetailsThunkParams = IForceThunkParam & AutoUpdateConfiguration1CDetailsFilterParams;