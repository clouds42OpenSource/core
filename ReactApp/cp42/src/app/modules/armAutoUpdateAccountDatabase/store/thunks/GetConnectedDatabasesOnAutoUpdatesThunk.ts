//#region custom user types
import { ArmAutoUpdateAccountDatabaseActions } from 'app/modules/armAutoUpdateAccountDatabase/store/actions';
import { GetConnectedDatabasesOnAutoUpdateThunkParams } from 'app/modules/armAutoUpdateAccountDatabase/store/reducers/getConnectedDatabasesOnAutoUpdatesReducer/params';
import { GetConnectedDatabasesOnAutoUpdatesActionSuccessPayload } from 'app/modules/armAutoUpdateAccountDatabase/store/reducers/getConnectedDatabasesOnAutoUpdatesReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { ReducerActionFailedPayload, ReducerActionStartPayload } from 'core/redux/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = ReducerActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = GetConnectedDatabasesOnAutoUpdatesActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = ReducerActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = GetConnectedDatabasesOnAutoUpdateThunkParams;

const { GetConnectedDatabasesOnAutoUpdates: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = ArmAutoUpdateAccountDatabaseActions;

//#endregion

//#region Fixed types

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

//#endregion

/**
 * Thunk для получения списка данных инф. баз, подключенных к АО
 */
export class GetConnectedDatabasesOnAutoUpdatesThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new GetConnectedDatabasesOnAutoUpdatesThunk().execute(args, args?.showLoadingProgress);
    }

    //#region значения START/SUCCESS/FAILED дейсвий

    protected get startActionValue(): string { return START_ACTION; }

    protected get successActionValue(): string { return SUCCESS_ACTION; }

    protected get failedActionValue(): string { return FAILED_ACTION; }

    //#endregion
    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        const armAutoUpdateAccountDatabaseState = args.getStore().ArmAutoUpdateAccountDatabaseState;

        return {
            condition: !armAutoUpdateAccountDatabaseState.hasSuccessFor.hasConnectedDatabasesOnAutoUpdateReceived || args.inputParams?.force === true,
            getReducerStateFunc: () => armAutoUpdateAccountDatabaseState
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        const api = InterlayerApiProxy.getArmAutoUpdateAccountDatabaseApiProxy();

        try {
            const response = await api.getConnectedDatabasesOnAutoUpdate(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, args.inputParams!);
            args.success({
                records: response.records,
                metadata: response.metadata
            });
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}