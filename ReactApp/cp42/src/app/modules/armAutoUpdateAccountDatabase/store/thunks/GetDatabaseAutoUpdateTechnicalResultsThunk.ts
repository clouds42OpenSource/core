//#region custom user types
import { ArmAutoUpdateAccountDatabaseActions } from 'app/modules/armAutoUpdateAccountDatabase/store/actions';
import { GetDatabaseAutoUpdateTechnicalResultsThunkParams } from 'app/modules/armAutoUpdateAccountDatabase/store/reducers/getDatabaseAutoUpdateTechnicalResultsReducer/params';
import { GetDatabaseAutoUpdateTechnicalResultsActionSuccessPayload } from 'app/modules/armAutoUpdateAccountDatabase/store/reducers/getDatabaseAutoUpdateTechnicalResultsReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { ReducerActionFailedPayload, ReducerActionStartPayload } from 'core/redux/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = ReducerActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = GetDatabaseAutoUpdateTechnicalResultsActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = ReducerActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = GetDatabaseAutoUpdateTechnicalResultsThunkParams;

const { GetDatabaseAutoUpdateTechnicalResults: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = ArmAutoUpdateAccountDatabaseActions;

//#endregion

//#region Fixed types

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

//#endregion

/**
 * Thunk для получения списка данных технических результатов АО инф. базы
 */
export class GetDatabaseAutoUpdateTechnicalResultsThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new GetDatabaseAutoUpdateTechnicalResultsThunk().execute(args, args?.showLoadingProgress);
    }

    //#region значения START/SUCCESS/FAILED дейсвий

    protected get startActionValue(): string { return START_ACTION; }

    protected get successActionValue(): string { return SUCCESS_ACTION; }

    protected get failedActionValue(): string { return FAILED_ACTION; }

    //#endregion
    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        const armAutoUpdateAccountDatabaseState = args.getStore().ArmAutoUpdateAccountDatabaseState;

        return {
            condition: !armAutoUpdateAccountDatabaseState.hasSuccessFor.hasDatabaseAutoUpdateTechnicalResultsReceived || args.inputParams?.force === true,
            getReducerStateFunc: () => armAutoUpdateAccountDatabaseState
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        const api = InterlayerApiProxy.getArmAutoUpdateAccountDatabaseApiProxy();

        try {
            const response = await api.getDatabaseAutoUpdateTechnicalResults(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, args.inputParams!);
            args.success(response);
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}