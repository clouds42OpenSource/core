import { ArmAutoUpdateAccountDatabaseActions } from 'app/modules/armAutoUpdateAccountDatabase/store/actions';
import { GetConfigurationNamesActionSuccessPayload } from 'app/modules/armAutoUpdateAccountDatabase/store/reducers/getConfigurationNamesReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { ReducerActionFailedPayload, ReducerActionStartPayload } from 'core/redux/interfaces';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';
import { RequestKind } from 'core/requestSender/enums';

type TActionStartPayload = ReducerActionStartPayload;
type TActionSuccessPayload = GetConfigurationNamesActionSuccessPayload;
type TActionFailedPayload = ReducerActionFailedPayload;
type TInputParams = IForceThunkParam;

const { GetConfigurationNames: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = ArmAutoUpdateAccountDatabaseActions;

type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

export class GetConfigurationNamesThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new GetConfigurationNamesThunk().execute(args, args?.showLoadingProgress);
    }

    protected get startActionValue(): string { return START_ACTION; }

    protected get successActionValue(): string { return SUCCESS_ACTION; }

    protected get failedActionValue(): string { return FAILED_ACTION; }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        const armAutoUpdateAccountDatabaseState = args.getStore().ArmAutoUpdateAccountDatabaseState;

        return {
            condition: !armAutoUpdateAccountDatabaseState.hasSuccessFor.hasConfigurationNamesReceived || args.inputParams?.force === true,
            getReducerStateFunc: () => armAutoUpdateAccountDatabaseState
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        const api = InterlayerApiProxy.getArmAutoUpdateAccountDatabaseApiProxy();

        try {
            const response = await api.getConfigurationNames(RequestKind.SEND_BY_USER_SYNCHRONOUSLY);
            args.success(response);
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}