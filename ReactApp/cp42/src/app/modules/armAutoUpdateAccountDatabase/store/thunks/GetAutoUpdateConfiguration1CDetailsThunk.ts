import { ArmAutoUpdateAccountDatabaseActions } from 'app/modules/armAutoUpdateAccountDatabase/store/actions';
import { GetAutoUpdateConfiguration1CDetailsThunkParams } from 'app/modules/armAutoUpdateAccountDatabase/store/reducers/getAutoUpdateConfiguration1CDetailsReducer/params';
import { GetAutoUpdateConfiguration1CDetailsActionSuccessPayload } from 'app/modules/armAutoUpdateAccountDatabase/store/reducers/getAutoUpdateConfiguration1CDetailsReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { ReducerActionFailedPayload, ReducerActionStartPayload } from 'core/redux/interfaces';
import { RequestKind } from 'core/requestSender/enums';

type TActionStartPayload = ReducerActionStartPayload;
type TActionSuccessPayload = GetAutoUpdateConfiguration1CDetailsActionSuccessPayload;
type TActionFailedPayload = ReducerActionFailedPayload;
type TInputParams = GetAutoUpdateConfiguration1CDetailsThunkParams;

const { GetAutoUpdateConfiguration1CDetails: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = ArmAutoUpdateAccountDatabaseActions;

type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

export class GetAutoUpdateConfiguration1CDetailsThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    public static invoke(args?: TInputParams) {
        return new GetAutoUpdateConfiguration1CDetailsThunk().execute(args, args?.showLoadingProgress);
    }

    protected get startActionValue(): string { return START_ACTION; }

    protected get successActionValue(): string { return SUCCESS_ACTION; }

    protected get failedActionValue(): string { return FAILED_ACTION; }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        return {
            condition: args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().ArmAutoUpdateAccountDatabaseState
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        const api = InterlayerApiProxy.getArmAutoUpdateAccountDatabaseApiProxy();

        try {
            const response = await api.getAutoUpdateConfiguration1CDetails(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, args.inputParams!);
            args.success(response);
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}