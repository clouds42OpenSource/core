import { ArmAutoUpdateAccountDatabaseProcessId } from 'app/modules/armAutoUpdateAccountDatabase';
import { ArmAutoUpdateAccountDatabaseConstants } from 'app/modules/armAutoUpdateAccountDatabase/constants';
import { createReducerActions } from 'core/redux/functions';

/**
 * Все доступные actions для редюсера ТиИ/АО -> "АРМ Автообновления"
 */
export const ArmAutoUpdateAccountDatabaseActions = {
    /**
     * Набор action на получение списка инф. баз, подключенных к АО
     */
    GetConnectedDatabasesOnAutoUpdates: createReducerActions(ArmAutoUpdateAccountDatabaseConstants.reducerName, ArmAutoUpdateAccountDatabaseProcessId.GetConnectedDatabasesOnAutoUpdates),

    /**
     * Набор action на получение списка названия конфигураций 1С
     */
    GetConfigurationNames: createReducerActions(ArmAutoUpdateAccountDatabaseConstants.reducerName, ArmAutoUpdateAccountDatabaseProcessId.GetConfigurationNames),

    /**
     * Набор action на получение детали авто обновления конфигурации 1С
     */
    GetAutoUpdateConfiguration1CDetails: createReducerActions(ArmAutoUpdateAccountDatabaseConstants.reducerName, ArmAutoUpdateAccountDatabaseProcessId.GetAutoUpdateConfiguration1CDetails),

    /**
     * Набор action на получение списка инф. баз, в очереди на АО
     */
    GetDatabaseInAutoUpdateQueues: createReducerActions(ArmAutoUpdateAccountDatabaseConstants.reducerName, ArmAutoUpdateAccountDatabaseProcessId.GetDatabaseInAutoUpdateQueues),

    /**
     * Набор action на получение списка данных по выполненным АО инф. баз
     */
    GetPerformedAutoUpdateForDatabases: createReducerActions(ArmAutoUpdateAccountDatabaseConstants.reducerName, ArmAutoUpdateAccountDatabaseProcessId.GetPerformedAutoUpdateForDatabases),

    /**
     * Набор action на получение списка воркеров которые выполняют авто обновление
     */
    GetWorkers: createReducerActions(ArmAutoUpdateAccountDatabaseConstants.reducerName, ArmAutoUpdateAccountDatabaseProcessId.GetWorkers),

    /**
     * Набор action на получение списка данных технических результатов АО инф. базы
     */
    GetDatabaseAutoUpdateTechnicalResults: createReducerActions(ArmAutoUpdateAccountDatabaseConstants.reducerName, ArmAutoUpdateAccountDatabaseProcessId.GetDatabaseAutoUpdateTechnicalResults)
};