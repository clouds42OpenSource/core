import { InfoDbListConsts } from 'app/modules/infoDbList/constants';
import { InfoDbListProcessId } from 'app/modules/infoDbList/InfoDbListProcessId';
import { createReducerActions } from 'core/redux/functions';

/**
 * Все доступные actions дле редюсера InfoDbList
 */
export const InfoDbListActions = {
    /**
     * Набор action на получение списка баз
     */
    ReceiveInfoDbList: createReducerActions(InfoDbListConsts.reducerName, InfoDbListProcessId.ReceiveInfoDbList),
    /**
     * Набор action на получение списка баз из других аккаунтов
     */
    ReceiveInfoDbListOther: createReducerActions(InfoDbListConsts.reducerNameOther, InfoDbListProcessId.ReceiveInfoDbListOther),
    /**
     * Набор action на удаление баз
     */
    DeleteInfoDb: createReducerActions(InfoDbListConsts.reducerName, InfoDbListProcessId.DeleteInfoDb),
    /**
     * Набор action на редактирование баз
     */
    EditInfoDb: createReducerActions(InfoDbListConsts.reducerName, InfoDbListProcessId.EditInfoDb),
    /**
     * Набор action на смены статуса баз
     */
    StatusInfoDb: createReducerActions(InfoDbListConsts.reducerName, InfoDbListProcessId.StatusInfoDb),
    StatusInfoDbOnDelimiters: createReducerActions(InfoDbListConsts.reducerName, InfoDbListProcessId.StatusInfoDbOnDelimiters),
    /**
     * Набор action на получения данных для работы с базами
     */
    AvailabilityInfoDb: createReducerActions(InfoDbListConsts.reducerName, InfoDbListProcessId.AvailabilityInfoDb)
};