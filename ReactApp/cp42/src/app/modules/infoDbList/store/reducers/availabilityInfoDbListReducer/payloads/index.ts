import { GetAvailabilityInfoDb } from 'app/web/api/InfoDbProxy/responce-dto/GetAvailabilityInfoDb';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения данных для работы с базами
 */
export type AvailabilityInfoDbListActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения данных для работы с базами
 */
export type AvailabilityInfoDbListActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения данных для работы с базами
 */

export type AvailabilityInfoDbListActionSuccessPayload = ReducerActionSuccessPayload & {
    /**
     * Общая информация о базах
     */
    AvailabilityInfoDb: GetAvailabilityInfoDb;
};

/**
 * Все возможные Action при получении данных для работы с базами
 */
export type AvailabilityInfoDbListActionAnyPayload =
    | AvailabilityInfoDbListActionStartPayload
    | AvailabilityInfoDbListActionFailedPayload
    | AvailabilityInfoDbListActionSuccessPayload;