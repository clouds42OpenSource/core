import { getInitialPagination } from 'app/common/functions';
import { InfoDbListConsts } from 'app/modules/infoDbList/constants';
import { InfoDbListOtherReducerState } from 'app/modules/infoDbList/store/reducers';
import { receiveInfoDbListOtherReducer } from 'app/modules/infoDbList/store/reducers/receiveInfoDbListOtherReducer';
import { createReducer, initReducerState } from 'core/redux/functions';

/**
 * Начальное состояние редюсера InfoDbListOther
 */
const initialState = initReducerState<InfoDbListOtherReducerState>(
    InfoDbListConsts.reducerNameOther,
    {
        infoDbListOther: {
            records: [],
            pagination: getInitialPagination(),
            countBase: {
                AllDatabasesCount: 0,
                ServerDatabasesCount: 0,
                ArchievedDatabasesCount: 0,
                DelimeterDatabasesCount: 0,
                FileDatabasesCount: 0,
                DeletedDatabases: 0
            },
            AllDatabasesCount: 0,
            ServerDatabasesCount: 0,
            ArchievedDatabasesCount: 0,
            DelimeterDatabasesCount: 0,
            FileDatabasesCount: 0,
            DeletedDatabases: 0
        },
        hasSuccessFor: {
            hasInfoDbListOtherReceived: false
        }
    }
);

/**
 * Объединённые части редюсера для всего состояния InfoDbListOther
 */
const partialReducers = [
    receiveInfoDbListOtherReducer
];

/**
 * Редюсер InfoDbList
 */
export const infoDbListOtherReducer = createReducer(initialState, partialReducers);