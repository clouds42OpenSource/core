import { getInitialPagination } from 'app/common/functions';
import { InfoDbListConsts } from 'app/modules/infoDbList/constants';
import { InfoDbListReducerState } from 'app/modules/infoDbList/store/reducers';
import { availabilityInfoDbListReducer } from 'app/modules/infoDbList/store/reducers/availabilityInfoDbListReducer';
import { deleteInfoDbReducer } from 'app/modules/infoDbList/store/reducers/deleteDbTemplateReducer';
import { editInfoDbReducer } from 'app/modules/infoDbList/store/reducers/editInfoDbReducer';
import { receiveInfoDbListReducer } from 'app/modules/infoDbList/store/reducers/receiveInfoDbListReducer';
import { statusInfoDbListReducer } from 'app/modules/infoDbList/store/reducers/receiveStatusesInfoDbListReducer';
import { statusInfoDbOnDelimitersListReducer } from 'app/modules/infoDbList/store/reducers/receiveStatusesInfoDbOnDelimitersListReducer';
import { createReducer, initReducerState } from 'core/redux/functions';

/**
 * Начальное состояние редюсера InfoDbList
 */
export const initialStateInfoDbList = initReducerState<InfoDbListReducerState>(
    InfoDbListConsts.reducerName,
    {
        statusInfoDbList: [],
        AvailabilityInfoDb: {
            IsVipAccount: false,
            IsMainServiceAllowed: false,
            AccessToServerDatabaseServiceTypeId: '',
            AccountAdminInfo: '',
            AccountId: '',
            AccountLocaleName: '',
            AccountUserId: '',
            MyDatabaseServiceId: '',
            AccountAdminPhoneNumber: '',
            MainServiceStatusInfo: {
                PromisedPaymentExpireDate: null,
                PromisedPaymentIsActive: false,
                PromisedPaymentSum: null,
                ServiceIsLocked: false,
                ServiceLockReason: null,
                ServiceLockReasonType: null
            },
            AllowAddingDatabaseInfo: {
                ForbiddeningReasonMessage: '',
                IsAllowedCreateDb: false,
                SurchargeForCreation: 0
            },
            PermissionsForWorkingWithDatabase: {
                HasPermissionForCreateAccountDatabase: false,
                HasPermissionForDisplayAutoUpdateButton: false,
                HasPermissionForDisplayPayments: false,
                HasPermissionForDisplaySupportData: false,
                HasPermissionForMultipleActionsWithDb: false,
                HasPermissionForRdp: false,
                HasPermissionForWeb: false,
            }
        },
        infoDbList: {
            records: [],
            pagination: getInitialPagination(),
            AllDatabasesCount: 0,
            ServerDatabasesCount: 0,
            ArchievedDatabasesCount: 0,
            DelimeterDatabasesCount: 0,
            FileDatabasesCount: 0,
            DeletedDatabases: 0

        },
        databasesId: [],
        databaseId: '',
        HasSupport: false,
        HasAutoUpdate: false,
        caption: '',
        errorMessage: null,
        hasSuccessFor: {
            hasInfoDbListReceived: false,
            hasAvailabilityDbListReceived: false,
            hasInfoDbListStatusReceived: false
        }
    }
);

/**
 * Объединённые части редюсера для всего состояния InfoDbList
 */
const partialReducers = [
    receiveInfoDbListReducer,
    deleteInfoDbReducer,
    editInfoDbReducer,
    statusInfoDbListReducer,
    statusInfoDbOnDelimitersListReducer,
    availabilityInfoDbListReducer
];

/**
 * Редюсер InfoDbList
 */
export const infoDbListReducer = createReducer(initialStateInfoDbList, partialReducers);