import { reducerActionState } from 'app/common/functions/reducerActionState';
import { InfoDbListProcessId } from 'app/modules/infoDbList/InfoDbListProcessId';
import { InfoDbListActions } from 'app/modules/infoDbList/store/actions';
import { initialStateInfoDbList } from 'app/modules/infoDbList/store/reducers/infoDbListReducer';
import { InfoDbListReducerState } from 'app/modules/infoDbList/store/reducers/InfoDbListReducerState';
import { ReceiveInfoDbListActionAnyPayload, ReceiveInfoDbListActionSuccessPayload } from 'app/modules/infoDbList/store/reducers/receiveInfoDbListReducer/payloads';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

export const receiveInfoDbListReducer: TPartialReducerObject<InfoDbListReducerState, ReceiveInfoDbListActionAnyPayload> =
    (state, action) => {
        const processId = InfoDbListProcessId.ReceiveInfoDbList;
        const actions = InfoDbListActions.ReceiveInfoDbList;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onStartProcessState: () => {
                return reducerStateProcessStart(state, processId, {
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasInfoDbListReceived: false
                    },
                    infoDbList: { ...initialStateInfoDbList.infoDbList }
                });
            },
            onSuccessProcessState: () => {
                const payload = action.payload as ReceiveInfoDbListActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    infoDbList: { ...payload.InfoDbList },
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasInfoDbListReceived: true
                    }
                });
            }
        });
    };