import { reducerActionState } from 'app/common/functions/reducerActionState';
import { InfoDbListProcessId } from 'app/modules/infoDbList/InfoDbListProcessId';
import { InfoDbListActions } from 'app/modules/infoDbList/store/actions';
import { InfoDbListReducerState } from 'app/modules/infoDbList/store/reducers/InfoDbListReducerState';
import { StatusInfoDbListOnDelimitersActionAnyPayload, StatusInfoDbListOnDelimitersActionSuccessPayload } from 'app/modules/infoDbList/store/reducers/receiveStatusesInfoDbOnDelimitersListReducer/payloads';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

export const statusInfoDbOnDelimitersListReducer: TPartialReducerObject<InfoDbListReducerState, StatusInfoDbListOnDelimitersActionAnyPayload> =
    (state, action) => {
        const processId = InfoDbListProcessId.StatusInfoDbOnDelimiters;
        const actions = InfoDbListActions.StatusInfoDbOnDelimiters;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onStartProcessState: () => {
                return reducerStateProcessStart(state, processId, {
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasInfoDbListStatusReceived: false
                    }
                });
            },
            onSuccessProcessState: () => {
                const payload = action.payload as StatusInfoDbListOnDelimitersActionSuccessPayload;

                const newRecords = state.infoDbList.records.map(item => {
                    for (const i of payload.statusInfoDbList) {
                        if (item.databaseId === i.id) {
                            return {
                                ...item,
                                createStatus: i
                            };
                        }
                    }

                    return {
                        ...item,
                        createStatus: undefined
                    };
                });

                return reducerStateProcessSuccess(state, processId, {
                    infoDbList: {
                        ...state.infoDbList,
                        records: newRecords,
                    },
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasInfoDbListStatusReceived: true
                    }
                });
            }
        });
    };