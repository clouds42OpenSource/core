import { deleteDbType } from 'app/web/InterlayerApiProxy/InfoDbApiProxy/deleteInfoDbList/input-params';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для удаления шаблона
 */
export type DeleteInfoDbThunkParams = IForceThunkParam & deleteDbType;