import { editDbListType } from 'app/web/InterlayerApiProxy/InfoDbApiProxy/editInfoDbList/input-params';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров редактирования ИБ
 */
export type EditInfoDbThunkParams = IForceThunkParam & editDbListType;