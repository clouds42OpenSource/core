import { InputAccountId } from 'app/web/api/InfoDbProxy/request-dto/InputAccountId';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для получения данных для работы с базами
 */
export type AvailabilityInfoDbListThunkParams = IForceThunkParam & InputAccountId;