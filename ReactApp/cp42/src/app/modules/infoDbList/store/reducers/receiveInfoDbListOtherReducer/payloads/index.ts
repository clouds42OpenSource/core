import { GetInfoDbListResultDataModel } from 'app/web/InterlayerApiProxy/InfoDbApiProxy/receiveDatabaseList/data-models/GetInfoDbListResultDataModel';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения списка баз из других аакаунтов
 */
export type ReceiveInfoDbListOtherActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения списка баз из других аакаунтов
 */
export type ReceiveInfoDbListOtherActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения списка баз из других аакаунтов
 */
export type ReceiveInfoDbListOtherActionSuccessPayload = ReducerActionSuccessPayload & {

    /**
     * Список баз
     */
    InfoDbListOther: GetInfoDbListResultDataModel
};

/**
 * Все возможные Action при получении списка баз из других аакаунтов
 */
export type ReceiveInfoDbListOtherActionAnyPayload =
    | ReceiveInfoDbListOtherActionStartPayload
    | ReceiveInfoDbListOtherActionFailedPayload
    | ReceiveInfoDbListOtherActionSuccessPayload;