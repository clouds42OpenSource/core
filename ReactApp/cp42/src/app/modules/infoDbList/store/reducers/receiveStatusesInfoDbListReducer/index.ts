import { reducerActionState } from 'app/common/functions/reducerActionState';
import { InfoDbListProcessId } from 'app/modules/infoDbList/InfoDbListProcessId';
import { InfoDbListActions } from 'app/modules/infoDbList/store/actions';
import { InfoDbListReducerState } from 'app/modules/infoDbList/store/reducers/InfoDbListReducerState';
import { StatusInfoDbListActionAnyPayload, StatusInfoDbListActionSuccessPayload } from 'app/modules/infoDbList/store/reducers/receiveStatusesInfoDbListReducer/payloads';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

export const statusInfoDbListReducer: TPartialReducerObject<InfoDbListReducerState, StatusInfoDbListActionAnyPayload> =
    (state, action) => {
        const processId = InfoDbListProcessId.StatusInfoDb;
        const actions = InfoDbListActions.StatusInfoDb;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onStartProcessState: () => {
                return reducerStateProcessStart(state, processId, {
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasInfoDbListStatusReceived: false
                    }
                });
            },
            onSuccessProcessState: () => {
                const payload = action.payload as StatusInfoDbListActionSuccessPayload;

                const newRecords = state.infoDbList.records.map(item => {
                    for (const i of payload.statusInfoDbList) {
                        if (item.databaseId === i.databaseId) {
                            return {
                                ...item,
                                state: i.state,
                                publishState: i.publishState,
                                webPublishPath: i.webPublishPath,
                                needShowWebLink: i.needShowWebLink
                            };
                        }
                    }

                    return item;
                });

                return reducerStateProcessSuccess(state, processId, {
                    infoDbList: {
                        ...state.infoDbList,
                        records: newRecords,
                    },
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasInfoDbListStatusReceived: true
                    }
                });
            }
        });
    };