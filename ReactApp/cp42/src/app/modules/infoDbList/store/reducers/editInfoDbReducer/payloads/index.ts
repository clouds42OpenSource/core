import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте редактирования ИБ
 */
export type EditInfoDbActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном редактировании ИБ
 */
export type EditInfoDbActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном редактировании ИБ
 */
export type EditInfoDbActionSuccessPayload = ReducerActionSuccessPayload & {

    databaseId: string;

    hasSupport?: boolean;

    HasAutoUpdate?: boolean;

    caption?: string;

    errorMessage?: string | null
};

/**
 * Все возможные Action при редактировании ИБ
 */
export type EditInfoDbActionAnyPayload =
    | EditInfoDbActionStartPayload
    | EditInfoDbActionFailedPayload
    | EditInfoDbActionSuccessPayload;