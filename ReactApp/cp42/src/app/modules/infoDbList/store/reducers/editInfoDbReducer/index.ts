import { reducerActionState } from 'app/common/functions/reducerActionState';
import { InfoDbListProcessId } from 'app/modules/infoDbList/InfoDbListProcessId';
import { InfoDbListActions } from 'app/modules/infoDbList/store/actions';
import { InfoDbListReducerState } from 'app/modules/infoDbList/store/reducers';
import { EditInfoDbActionAnyPayload, EditInfoDbActionSuccessPayload } from 'app/modules/infoDbList/store/reducers/editInfoDbReducer/payloads';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для редактирования ИБ
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const editInfoDbReducer: TPartialReducerObject<InfoDbListReducerState, EditInfoDbActionAnyPayload> =
    (state, action) => {
        const processId = InfoDbListProcessId.EditInfoDb;
        const actions = InfoDbListActions.EditInfoDb;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onStartProcessState: () => {
                return reducerStateProcessStart(state, processId, {
                    errorMessage: null

                });
            },
            onSuccessProcessState: () => {
                const payload = action.payload as EditInfoDbActionSuccessPayload;
                const newRecords = state.infoDbList.records.map(item => {
                    if (payload.hasSupport !== undefined && payload.databaseId === item.databaseId && !payload.errorMessage) {
                        return { ...item, hasSupport: payload.hasSupport! };
                    }
                    if (payload.HasAutoUpdate !== undefined && payload.databaseId === item.databaseId && !payload.errorMessage) {
                        return { ...item, hasAutoUpdate: payload.HasAutoUpdate! };
                    }
                    return item;
                });
                return reducerStateProcessSuccess(state, processId, {

                    AvailabilityInfoDb: {
                        ...state.AvailabilityInfoDb
                    },
                    databaseId: payload.databaseId,
                    HasSupport: payload.hasSupport,
                    HasAutoUpdate: payload.HasAutoUpdate,
                    caption: payload.caption,
                    errorMessage: payload.errorMessage,

                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasInfoDbListReceived: true
                    },
                    infoDbList: {

                        ...state.infoDbList,
                        records: newRecords,

                        pagination: {
                            ...state.infoDbList.pagination,
                            pageSize: state.infoDbList.pagination.pageSize,
                            totalCount: state.infoDbList.pagination.totalCount,

                        },
                    }

                });
            }
        });
    };