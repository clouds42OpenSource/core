import { InfoDbListItemDataModel } from 'app/web/InterlayerApiProxy/InfoDbApiProxy/receiveDatabaseList/data-models';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения статусов списка баз
 */
export type StatusInfoDbListActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения статусов списка баз
 */
export type StatusInfoDbListActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения статусов списка баз
 */

export type StatusInfoDbListActionSuccessPayload = ReducerActionSuccessPayload & {
    /**
     * Список статусов баз
     */
    statusInfoDbList: InfoDbListItemDataModel[];
};

/**
 * Все возможные Action при получении статусов баз
 */
export type StatusInfoDbListActionAnyPayload =
    | StatusInfoDbListActionStartPayload
    | StatusInfoDbListActionFailedPayload
    | StatusInfoDbListActionSuccessPayload;