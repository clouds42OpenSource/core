import { InfoDbListProcessId } from 'app/modules/infoDbList/InfoDbListProcessId';
import { GetAvailabilityInfoDb } from 'app/web/api/InfoDbProxy/responce-dto/GetAvailabilityInfoDb';
import { InfoDbListItemDataModel } from 'app/web/InterlayerApiProxy/InfoDbApiProxy/receiveDatabaseList/data-models';
import { GetInfoDbListResultDataModel } from 'app/web/InterlayerApiProxy/InfoDbApiProxy/receiveDatabaseList/data-models/GetInfoDbListResultDataModel';
import { IReducerState } from 'core/redux/interfaces';

/**
 * Состояние редюсера для работы с списком баз
 */
export type InfoDbListReducerState = IReducerState<InfoDbListProcessId> & {

    AvailabilityInfoDb: GetAvailabilityInfoDb;

    databaseId?: string;

    HasSupport?: boolean;

    caption?: string;

    databasesId?: string[];

    HasAutoUpdate?: boolean;

    errorMessage?: string | null;

    infoDbList: GetInfoDbListResultDataModel;

    statusInfoDbList: InfoDbListItemDataModel[];

    hasSuccessFor: {
        /**
         * Если true, то список баз получен
         */
        hasInfoDbListReceived: boolean,
        hasAvailabilityDbListReceived: boolean,
        hasInfoDbListStatusReceived: boolean
    };
};

export type InfoDbListOtherReducerState = IReducerState<InfoDbListProcessId> & {
    /**
     * список строннних баз
     */
    infoDbListOther: GetInfoDbListResultDataModel;
    hasSuccessFor: {
        /**
         * Если true, то список баз получен
         */
        hasInfoDbListOtherReceived: boolean
    };
};