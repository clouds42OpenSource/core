import { reducerActionState } from 'app/common/functions/reducerActionState';
import { InfoDbListProcessId } from 'app/modules/infoDbList/InfoDbListProcessId';
import { InfoDbListActions } from 'app/modules/infoDbList/store/actions';
import { AvailabilityInfoDbListActionAnyPayload, AvailabilityInfoDbListActionSuccessPayload } from 'app/modules/infoDbList/store/reducers/availabilityInfoDbListReducer/payloads';
import { InfoDbListReducerState } from 'app/modules/infoDbList/store/reducers/InfoDbListReducerState';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

export const availabilityInfoDbListReducer: TPartialReducerObject<InfoDbListReducerState, AvailabilityInfoDbListActionAnyPayload> =
    (state, action) => {
        const processId = InfoDbListProcessId.AvailabilityInfoDb;
        const actions = InfoDbListActions.AvailabilityInfoDb;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as AvailabilityInfoDbListActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    AvailabilityInfoDb: { ...payload.AvailabilityInfoDb },
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasAvailabilityDbListReceived: true
                    }
                });
            }
        });
    };