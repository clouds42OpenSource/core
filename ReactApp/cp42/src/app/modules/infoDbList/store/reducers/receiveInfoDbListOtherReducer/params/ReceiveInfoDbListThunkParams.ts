import { ReceiveInfoDbListParams } from 'app/web/InterlayerApiProxy/InfoDbApiProxy/receiveDatabaseList/input-params';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для получения списка баз из других аакаунтов
 */
export type ReceiveInfoDbListThunkParams = IForceThunkParam & ReceiveInfoDbListParams;