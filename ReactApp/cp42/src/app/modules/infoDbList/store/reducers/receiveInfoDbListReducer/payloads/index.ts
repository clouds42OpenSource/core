import { GetInfoDbListResultDataModel } from 'app/web/InterlayerApiProxy/InfoDbApiProxy/receiveDatabaseList/data-models/GetInfoDbListResultDataModel';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения списка баз
 */
export type ReceiveInfoDbListActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения списка баз
 */
export type ReceiveInfoDbListActionFailedPayload = ReducerActionFailedPayload;

export type ReceiveInfoDbListActionSuccessPayload = ReducerActionSuccessPayload & {
    /**
     * Список баз других аккаунтов
     */
    InfoDbList: GetInfoDbListResultDataModel;

};

/**
 * Все возможные Action при получении списка баз
 */
export type ReceiveInfoDbListActionAnyPayload =
    | ReceiveInfoDbListActionStartPayload
    | ReceiveInfoDbListActionFailedPayload
    | ReceiveInfoDbListActionSuccessPayload;