import { TStatusItem } from 'app/api/endpoints/createDb/response';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения статусов списка баз
 */
export type StatusInfoDbListOnDelimitersActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения статусов списка баз
 */
export type StatusInfoDbListOnDelimitersActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения статусов списка баз
 */

export type StatusInfoDbListOnDelimitersActionSuccessPayload = ReducerActionSuccessPayload & {
    /**
     * Список статусов баз
     */
    statusInfoDbList: TStatusItem[];

};

/**
 * Все возможные Action при получении статусов баз
 */
export type StatusInfoDbListOnDelimitersActionAnyPayload =
    | StatusInfoDbListOnDelimitersActionStartPayload
    | StatusInfoDbListOnDelimitersActionFailedPayload
    | StatusInfoDbListOnDelimitersActionSuccessPayload;