import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения удаления баз
 */
export type DeleteInfoDbActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении удаления баз
 */
export type DeleteInfoDbActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении удаления баз
 */
export type DeleteInfoDbActionSuccessPayload = ReducerActionSuccessPayload & {

    /**
     * ID базы для удаления
     */
    databasesId: string[];
};

/**
 * Все возможные Action при удалении шаблона
 */
export type DeleteInfoDbActionAnyPayload =
    | DeleteInfoDbActionStartPayload
    | DeleteInfoDbActionFailedPayload
    | DeleteInfoDbActionSuccessPayload;