import { reducerActionState } from 'app/common/functions/reducerActionState';
import { InfoDbListProcessId } from 'app/modules/infoDbList/InfoDbListProcessId';
import { InfoDbListActions } from 'app/modules/infoDbList/store/actions';
import { InfoDbListOtherReducerState } from 'app/modules/infoDbList/store/reducers/InfoDbListReducerState';
import { ReceiveInfoDbListOtherActionAnyPayload, ReceiveInfoDbListOtherActionSuccessPayload } from 'app/modules/infoDbList/store/reducers/receiveInfoDbListOtherReducer/payloads';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';
/**
 * Редюсер для получения баз из других аакаунтов
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const receiveInfoDbListOtherReducer: TPartialReducerObject<InfoDbListOtherReducerState, ReceiveInfoDbListOtherActionAnyPayload> =
    (state, action) => {
        const processId = InfoDbListProcessId.ReceiveInfoDbListOther;
        const actions = InfoDbListActions.ReceiveInfoDbListOther;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onStartProcessState: () => {
                return reducerStateProcessStart(state, processId, {
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasInfoDbListOtherReceived: false
                    }
                });
            },
            onSuccessProcessState: () => {
                const payload = action.payload as ReceiveInfoDbListOtherActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    infoDbListOther: { ...payload.InfoDbListOther },
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasInfoDbListOtherReceived: true
                    }
                });
            }
        });
    };