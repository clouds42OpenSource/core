import { reducerActionState } from 'app/common/functions/reducerActionState';
import { InfoDbListProcessId } from 'app/modules/infoDbList/InfoDbListProcessId';
import { InfoDbListActions } from 'app/modules/infoDbList/store/actions';
import { InfoDbListReducerState } from 'app/modules/infoDbList/store/reducers';
import { DeleteInfoDbActionAnyPayload, DeleteInfoDbActionSuccessPayload } from 'app/modules/infoDbList/store/reducers/deleteDbTemplateReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для удаления базы
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const deleteInfoDbReducer: TPartialReducerObject<InfoDbListReducerState, DeleteInfoDbActionAnyPayload> =
    (state, action) => {
        const processId = InfoDbListProcessId.DeleteInfoDb;
        const actions = InfoDbListActions.DeleteInfoDb;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as DeleteInfoDbActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, payload);
            }
        });
    };