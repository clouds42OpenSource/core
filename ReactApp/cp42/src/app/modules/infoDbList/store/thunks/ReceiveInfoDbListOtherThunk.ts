import { InfoDbListActions } from 'app/modules/infoDbList/store/actions';
import { ReceiveInfoDbListOtherActionFailedPayload, ReceiveInfoDbListOtherActionStartPayload, ReceiveInfoDbListOtherActionSuccessPayload } from 'app/modules/infoDbList/store/reducers/receiveInfoDbListOtherReducer/payloads';
import { ReceiveInfoDbListThunkParams } from 'app/modules/infoDbList/store/reducers/receiveInfoDbListReducer/params';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = ReceiveInfoDbListOtherActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = ReceiveInfoDbListOtherActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = ReceiveInfoDbListOtherActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = ReceiveInfoDbListThunkParams;

const { ReceiveInfoDbListOther: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = InfoDbListActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для запроса списка баз из других аккаунтов
 */
export class ReceiveInfoDbListOtherThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new ReceiveInfoDbListOtherThunk().execute(args, args?.showLoadingProgress);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        const processFlowState = args.getStore().InfoDbListOtherState;
        return {
            condition: !processFlowState.hasSuccessFor.hasInfoDbListOtherReceived || args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().InfoDbListOtherState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        try {
            const api = InterlayerApiProxy.getInfoDbApi();
            const response = await api.receiveInfoDbListOther(RequestKind.SEND_BY_ROBOT, args.inputParams!);
            args.success({
                InfoDbListOther: response
            });
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}