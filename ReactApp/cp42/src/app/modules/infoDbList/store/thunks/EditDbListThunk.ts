import { InfoDbListActions } from 'app/modules/infoDbList/store/actions';
import { EditInfoDbThunkParams } from 'app/modules/infoDbList/store/reducers/editInfoDbReducer/params';
import { EditInfoDbActionFailedPayload, EditInfoDbActionStartPayload, EditInfoDbActionSuccessPayload } from 'app/modules/infoDbList/store/reducers/editInfoDbReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = EditInfoDbActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = EditInfoDbActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = EditInfoDbActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = EditInfoDbThunkParams;

const { EditInfoDb: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = InfoDbListActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для редактирования ИБ
 */
export class EditDbListThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new EditDbListThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        return {
            condition: args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().InfoDbListState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        try {
            let errorMessage: string | null = null;
            const api = InterlayerApiProxy.getInfoDbApi();
            const result = await api.editInfoDbList(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, args.inputParams!);
            if (!result.Success) {
                errorMessage = result.Message;
            }
            args.success({
                databaseId: args.inputParams!.databaseId,
                caption: args.inputParams!.caption,
                errorMessage
            });
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}