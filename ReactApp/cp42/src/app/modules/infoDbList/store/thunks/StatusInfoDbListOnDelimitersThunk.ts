import { FETCH_API } from 'app/api/useFetchApi';
import { InfoDbListActions } from 'app/modules/infoDbList/store/actions';
import {
    StatusInfoDbListOnDelimitersActionFailedPayload,
    StatusInfoDbListOnDelimitersActionStartPayload,
    StatusInfoDbListOnDelimitersActionSuccessPayload
} from 'app/modules/infoDbList/store/reducers/receiveStatusesInfoDbOnDelimitersListReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InputIdsArr } from 'app/web/api/InfoDbProxy/request-dto/InputIdsArr';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

const { getStatusCreateDb } = FETCH_API.CRETE_DB;

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = StatusInfoDbListOnDelimitersActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = StatusInfoDbListOnDelimitersActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = StatusInfoDbListOnDelimitersActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = IForceThunkParam & InputIdsArr;

const { StatusInfoDbOnDelimiters: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = InfoDbListActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для запроса списка статусов ИБ
 */
export class StatusInfoDbOnDelimitersListThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new StatusInfoDbOnDelimitersListThunk().execute(args, args?.showLoadingProgress);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        const processFlowState = args.getStore().InfoDbListState;
        return {
            condition: !processFlowState.hasSuccessFor.hasInfoDbListReceived || args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().InfoDbListState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        try {
            const response = await getStatusCreateDb(args.inputParams?.ids ?? []);

            args.success({
                statusInfoDbList: response.data?.Results ?? [],
            });
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}