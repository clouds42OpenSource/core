import { InfoDbListActions } from 'app/modules/infoDbList/store/actions';
import { ReceiveInfoDbListThunkParams } from 'app/modules/infoDbList/store/reducers/receiveInfoDbListReducer/params';
import { ReceiveInfoDbListActionFailedPayload, ReceiveInfoDbListActionStartPayload, ReceiveInfoDbListActionSuccessPayload } from 'app/modules/infoDbList/store/reducers/receiveInfoDbListReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = ReceiveInfoDbListActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = ReceiveInfoDbListActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = ReceiveInfoDbListActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = ReceiveInfoDbListThunkParams;

const { ReceiveInfoDbList: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = InfoDbListActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для запроса списка ИБ
 */
export class ReceiveInfoDbListThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new ReceiveInfoDbListThunk().execute(args, args?.showLoadingProgress);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        const processFlowState = args.getStore().InfoDbListState;
        return {
            condition: !processFlowState.hasSuccessFor.hasInfoDbListReceived || args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().InfoDbListState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        try {
            args.start();

            const api = InterlayerApiProxy.getInfoDbApi();
            const response = await api.receiveInfoDbList(RequestKind.SEND_BY_ROBOT, args.inputParams!);

            args.success({
                InfoDbList: response
            });
        } catch (err: unknown) {
            args.failed({
                error: err as ErrorObject
            });
        }
    }
}