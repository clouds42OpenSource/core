/**
 * Константы для модуля DatabaseList
 */
export const InfoDbListConsts = {
    /**
     * Название редюсера для модуля InfoDbList
     */
    reducerName: 'INFODB_LIST',
    /**
     * Название редюсера для модуля InfoDbListOther
     */
    reducerNameOther: 'INFODB_LIST_OTHER'
};