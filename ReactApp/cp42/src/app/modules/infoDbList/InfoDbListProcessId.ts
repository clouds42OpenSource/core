/**
 * Процессы InfoDbList
 */
export enum InfoDbListProcessId {

    /**
     * Процесс получить список баз
     */
    ReceiveInfoDbList = 'RECEIVE_INFODB_LIST',

    /**
    * Процесс получить список баз другого аккаунта
    */
    ReceiveInfoDbListOther = 'RECEIVE_INFODB_LIST_OTHER',

    /**
    * Процесс удаления базы
    */
    DeleteInfoDb = 'DELETE_INFO_DB',

    /**
    * Процесс редактирования базы
    */
    EditInfoDb = 'Edit_INFO_DB',

    /**
    * Процесс получения статуса баз
    */
    StatusInfoDb = 'STATUS_INFO_DB',
    StatusInfoDbOnDelimiters = 'STATUS_INFO_DB_ON_DELIMITERS',

    /**
     * Процесс получения данных для баз
     */
    AvailabilityInfoDb = 'Availability_INFO_DB'
}