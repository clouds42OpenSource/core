import { CloudCoreProcessId } from 'app/modules/cloudCore';
import { CloudCoreConstants } from 'app/modules/cloudCore/constants';
import { createReducerActions } from 'core/redux/functions';

/**
 * Все доступные actions для редюсера CloudCore
 */
export const CloudCoreActions = {
    /**
     * Набор action для получения дефолтных конфигураций
     */
    receiveDefaultConfiguration: createReducerActions(CloudCoreConstants.reducerName, CloudCoreProcessId.ReceiveDefaultConfiguration),

    /**
     * Набор action для редактирования разницы между Украиной и Москвой
     */
    editHoursDifferenceUkraineAndMoscow: createReducerActions(CloudCoreConstants.reducerName, CloudCoreProcessId.EditHoursDifferenceUkraineAndMoscow),

    /**
     * Набор action для редактирования дефолтного сегмента
     */
    editDefaultSegment: createReducerActions(CloudCoreConstants.reducerName, CloudCoreProcessId.EditDefaultSegment),

    /**
     * Набор action для редактирования главной страницы уведомления
     */
    editMainPageNotification: createReducerActions(CloudCoreConstants.reducerName, CloudCoreProcessId.EditMainPageNotification),

    /**
     * Набор action для редактирования переключателя между Юкассой и Робокассой
     */
    editRussianLocalePaymentAggregator: createReducerActions(CloudCoreConstants.reducerName, CloudCoreProcessId.EditRussianLocalePaymentAggregator)
};