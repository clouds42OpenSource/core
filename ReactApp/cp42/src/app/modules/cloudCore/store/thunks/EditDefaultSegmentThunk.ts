import { CloudCoreActions } from 'app/modules/cloudCore/store/actions';
import { EditDefaultSegmentParams } from 'app/modules/cloudCore/store/reducers/editDefaultSegmentReducer/params';
import { EditDefaultSegmentActionFailedPayload, EditDefaultSegmentActionStartPayload, EditDefaultSegmentActionSuccessPayload } from 'app/modules/cloudCore/store/reducers/editDefaultSegmentReducer/payload';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = EditDefaultSegmentActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = EditDefaultSegmentActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = EditDefaultSegmentActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = EditDefaultSegmentParams;

const { editHoursDifferenceUkraineAndMoscow: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = CloudCoreActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для редактирования дефолтного сегмента
 */
export class EditDefaultSegmentThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new EditDefaultSegmentThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        return {
            condition: args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().CloudCoreState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        const cloudCoreApi = InterlayerApiProxy.getCloudCoreApi();
        const requestArgs = args.inputParams!;
        try {
            const result = await cloudCoreApi.editDefaultSegment(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, {
                defSegmentId: requestArgs.defSegmentId
            });
            args.success({
                success: result
            });
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}