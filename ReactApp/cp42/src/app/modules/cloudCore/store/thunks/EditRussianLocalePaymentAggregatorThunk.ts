import { CloudCoreActions } from 'app/modules/cloudCore/store/actions';
import { EditRussianLocalePaymentParams } from 'app/modules/cloudCore/store/reducers/editRussianLocalePaymentAggregatorReducer/params';
import { EditRussianLocalePaymentActionFailedPayload, EditRussianLocalePaymentActionStartPayload, EditRussianLocalePaymentActionSuccessPayload } from 'app/modules/cloudCore/store/reducers/editRussianLocalePaymentAggregatorReducer/payload';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = EditRussianLocalePaymentActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = EditRussianLocalePaymentActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = EditRussianLocalePaymentActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = EditRussianLocalePaymentParams;

const { editRussianLocalePaymentAggregator: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = CloudCoreActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для редактирования переключателя между Юкассой и Робокассой
 */
export class EditRussianLocalePaymentAggregatorThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new EditRussianLocalePaymentAggregatorThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        return {
            condition: args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().CloudCoreState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        const cloudCoreApi = InterlayerApiProxy.getCloudCoreApi();
        const requestArgs = args.inputParams!;

        try {
            const result = await cloudCoreApi.editRussianLocaleAggregator(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, {
                aggregator: requestArgs.aggregator
            });
            args.success({
                success: result
            });
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}