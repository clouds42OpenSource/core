import { CloudCoreActions } from 'app/modules/cloudCore/store/actions';
import { ReceiveDefaultConfigurationThunkParams } from 'app/modules/cloudCore/store/reducers/receiveDefaultConfigurationReducer/params';
import {
    ReceiveDefaultConfigurationActionFailedPayload,
    ReceiveDefaultConfigurationActionStartPayload,
    ReceiveDefaultConfigurationActionSuccessPayload
} from 'app/modules/cloudCore/store/reducers/receiveDefaultConfigurationReducer/payload';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = ReceiveDefaultConfigurationActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = ReceiveDefaultConfigurationActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = ReceiveDefaultConfigurationActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = ReceiveDefaultConfigurationThunkParams;

const { receiveDefaultConfiguration: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = CloudCoreActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для запроса списка дефолтных данных для Настройки облака
 */
export class ReceiveDefaultConfigurationThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new ReceiveDefaultConfigurationThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        const cloudCoreState = args.getStore().CloudCoreState;
        return {
            condition: !cloudCoreState.hasSuccessFor.hasCloudCoreConfigurationReceived || args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().CloudCoreState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        const cloudCoreApi = InterlayerApiProxy.getCloudCoreApi();

        try {
            const cloudCoreResponse = await cloudCoreApi.receiveCloudCoreConfigurationData(RequestKind.SEND_BY_USER_SYNCHRONOUSLY);

            args.success({
                hoursDifferenceOfUkraineAndMoscow: cloudCoreResponse.hourseDifferenceOfUkraineAndMoscow,
                defaultSegment: cloudCoreResponse.defaultSegment,
                segments: cloudCoreResponse.segments,
                mainPageNotification: cloudCoreResponse.mainPageNotification,
                russianLocalePaymentAggregator: cloudCoreResponse.russianLocalePaymentAggregator,
                bannerUrl: cloudCoreResponse.bannerUrl
            });
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}