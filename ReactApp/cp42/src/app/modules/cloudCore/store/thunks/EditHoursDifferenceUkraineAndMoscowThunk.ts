import { CloudCoreActions } from 'app/modules/cloudCore/store/actions';
import { EditHoursDifferenceUkraineAndMoscowParams } from 'app/modules/cloudCore/store/reducers/editHoursDiferenceUkraineAndMoscowReducer/params';
import {
    EditHoursDeferenceUkraineAndMoscowActionFailedPayload,
    EditHoursDeferenceUkraineAndMoscowActionStartPayload,
    EditHoursDeferenceUkraineAndMoscowActionSuccessPayload
} from 'app/modules/cloudCore/store/reducers/editHoursDiferenceUkraineAndMoscowReducer/payload';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = EditHoursDeferenceUkraineAndMoscowActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = EditHoursDeferenceUkraineAndMoscowActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = EditHoursDeferenceUkraineAndMoscowActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = EditHoursDifferenceUkraineAndMoscowParams;

const { editHoursDifferenceUkraineAndMoscow: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = CloudCoreActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для редактирования разницы между Украиной и Москвой
 */
export class EditHoursDifferenceUkraineAndMoscowThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new EditHoursDifferenceUkraineAndMoscowThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        return {
            condition: args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().CloudCoreState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        const cloudCoreApi = InterlayerApiProxy.getCloudCoreApi();
        const requestArgs = args.inputParams!;
        try {
            const result = await cloudCoreApi.editHoursDifUkAndMsc(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, {
                hours: requestArgs.hours
            });
            args.success({
                success: result
            });
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}