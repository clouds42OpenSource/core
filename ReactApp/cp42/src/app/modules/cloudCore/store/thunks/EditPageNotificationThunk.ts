import { CloudCoreActions } from 'app/modules/cloudCore/store/actions';
import { EditPageNotificationParams } from 'app/modules/cloudCore/store/reducers/editPageNotificationReducer/params';
import { EditPageNotificationActionFailedPayload, EditPageNotificationActionStartPayload, EditPageNotificationActionSuccessPayload } from 'app/modules/cloudCore/store/reducers/editPageNotificationReducer/payload';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = EditPageNotificationActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = EditPageNotificationActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = EditPageNotificationActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = EditPageNotificationParams;

const { editHoursDifferenceUkraineAndMoscow: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = CloudCoreActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для редактирования страницы уведомления
 */
export class EditPageNotificationThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new EditPageNotificationThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        return {
            condition: args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().CloudCoreState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        const cloudCoreApi = InterlayerApiProxy.getCloudCoreApi();
        const requestArgs = args.inputParams!;

        try {
            const result = await cloudCoreApi.editMainPageNotification(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, {
                pageNotification: requestArgs.pageNotification
            });
            args.success({
                success: result
            });
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}