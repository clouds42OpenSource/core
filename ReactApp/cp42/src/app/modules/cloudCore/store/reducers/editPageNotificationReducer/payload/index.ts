import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте редактирования страницы уведомления
 */
export type EditPageNotificationActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при ошибки редактирования страницы уведомления
 */
export type EditPageNotificationActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном редактирования страницы уведомления
 */
export type EditPageNotificationActionSuccessPayload = ReducerActionSuccessPayload & {
    /**
     * Результат редактирования
     */
    success: boolean
};

/**
 * Все возможные Action при редактировании страницы уведомления
 */
export type EditPageNotificationAnyPayload =
    | EditPageNotificationActionStartPayload
    | EditPageNotificationActionFailedPayload
    | EditPageNotificationActionSuccessPayload;