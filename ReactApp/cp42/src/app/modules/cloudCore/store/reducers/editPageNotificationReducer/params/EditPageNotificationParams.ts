import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для редактирования контента главной страницы уведомления
 */
export type EditPageNotificationParams = IForceThunkParam & {
    /**
     * Контент страницы уведомления
     */
    pageNotification: string
};