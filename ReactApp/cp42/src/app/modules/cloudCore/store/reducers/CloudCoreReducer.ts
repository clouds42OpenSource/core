import { partialFillOf } from 'app/common/functions/fillObjectWithUndefined';
import { CloudCoreConstants } from 'app/modules/cloudCore/constants';
import { CloudCoreReducerState } from 'app/modules/cloudCore/store/reducers';
import { CloudCoreSegmentDataModel } from 'app/web/InterlayerApiProxy/CloudCoreApiProxy/receiveCloudCoreConfiguration/data-models';
import { createReducer, initReducerState } from 'core/redux/functions';
import { receiveDefaultConfigurationReducer } from '../reducers/receiveDefaultConfigurationReducer';
import { editDefaultSegmentReducer } from './editDefaultSegmentReducer';
import { editHoursDifferenceUkraineAndMoscowReducer } from './editHoursDiferenceUkraineAndMoscowReducer';
import { editPageNotificationReducer } from './editPageNotificationReducer';
import { editRussianLocalePaymentAggregatorReducer } from './editRussianLocalePaymentAggregatorReducer';

/**
 * Начальное состояние редюсера CloudCore
 */
const initialState = initReducerState<CloudCoreReducerState>(
    CloudCoreConstants.reducerName,
    {
        hoursDifferenceOfUkraineAndMoscow: 0,
        defaultSegment: partialFillOf<CloudCoreSegmentDataModel>(),
        segments: [],
        bannerUrl: null,
        currentSegmentId: '',
        editedPageNotification: '',
        mainPageNotification: '',
        russianLocalePaymentAggregator: 0,
        hasSuccessFor: {
            hasCloudCoreConfigurationReceived: false
        }
    }
);

/**
 * Объединённые части редюсера для всего состояния CloudCore
 */
const partialReducers = [
    receiveDefaultConfigurationReducer,
    editHoursDifferenceUkraineAndMoscowReducer,
    editDefaultSegmentReducer,
    editPageNotificationReducer,
    editRussianLocalePaymentAggregatorReducer
];

/**
 * Редюсер CloudCore
 */
export const CloudCoreReducer = createReducer(initialState, partialReducers);