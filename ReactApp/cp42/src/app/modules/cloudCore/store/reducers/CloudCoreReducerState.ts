import { DatabaseCardProcessId } from 'app/modules/databaseCard';
import { CloudCoreSegmentDataModel } from 'app/web/InterlayerApiProxy/CloudCoreApiProxy/receiveCloudCoreConfiguration/data-models';
import { IReducerState } from 'core/redux/interfaces';

/**
 * Состояние редюсера для работы с настройкой облака
 */
export type CloudCoreReducerState = IReducerState<DatabaseCardProcessId> & {
    /**
     * Разница между Украиной и Москвой
     */
    hoursDifferenceOfUkraineAndMoscow: number,

    /**
     * Дефолтный сегмент
     */
    defaultSegment: CloudCoreSegmentDataModel,

    /**
     * Коллекция всех сегментов
     */
    segments: Array<CloudCoreSegmentDataModel>,

    /**
     * ID сегмента по умолчания
     */
    currentSegmentId: string,

    /**
     * Поле для отредактированной страницы для уведомления
     */
    editedPageNotification: string,

    /**
     * Поле полученный от дефолтных конфигураций главная страница уведомления
     */
    mainPageNotification: string,

    bannerUrl: string | null,

    /**
     * Тип платежей Российкого аггрегатора
     */
    russianLocalePaymentAggregator: number,

    /**
     * Поле для пометки что дефолтные конфиги получены
     */
    hasSuccessFor: {
        hasCloudCoreConfigurationReceived: boolean
    }
};