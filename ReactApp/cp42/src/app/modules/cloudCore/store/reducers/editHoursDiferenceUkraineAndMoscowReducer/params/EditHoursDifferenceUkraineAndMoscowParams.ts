import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для редактирования разницы между Украиной и Москвой
 */
export type EditHoursDifferenceUkraineAndMoscowParams = IForceThunkParam & {
    /**
     * Разница часов между Украиной и Москвой
     */
    hours: number
};