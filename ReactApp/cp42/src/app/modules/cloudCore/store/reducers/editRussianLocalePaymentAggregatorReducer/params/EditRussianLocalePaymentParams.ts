import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для редактирования переключателя между Юкассой и Робокассой
 */
export type EditRussianLocalePaymentParams = IForceThunkParam & {
    /**
     * Тип аггрегатора
     */
    aggregator: number
};