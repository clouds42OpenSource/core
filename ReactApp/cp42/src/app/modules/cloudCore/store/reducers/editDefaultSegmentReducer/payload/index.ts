import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте редактирования дефолтного сегмента
 */
export type EditDefaultSegmentActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при ошибки редактирования дефолтного сегмента
 */
export type EditDefaultSegmentActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном редактирования дефолтного сегмента
 */
export type EditDefaultSegmentActionSuccessPayload = ReducerActionSuccessPayload & {
    /**
     * Результат редактирования
     */
    success: boolean
};

/**
 * Все возможные Action при редактировании дефолтного сегмента
 */
export type EditDefaultSegmentAnyPayload =
    | EditDefaultSegmentActionStartPayload
    | EditDefaultSegmentActionFailedPayload
    | EditDefaultSegmentActionSuccessPayload;