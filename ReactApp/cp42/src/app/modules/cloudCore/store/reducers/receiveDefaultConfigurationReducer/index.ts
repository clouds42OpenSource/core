import { reducerActionState } from 'app/common/functions/reducerActionState';
import { CloudCoreProcessId } from 'app/modules/cloudCore';
import { CloudCoreActions } from 'app/modules/cloudCore/store/actions';
import { CloudCoreReducerState } from 'app/modules/cloudCore/store/reducers';
import { ReceiveDefaultConfigurationActionAnyPayload, ReceiveDefaultConfigurationActionSuccessPayload } from 'app/modules/cloudCore/store/reducers/receiveDefaultConfigurationReducer/payload';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения дефолтных конфигураций
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const receiveDefaultConfigurationReducer: TPartialReducerObject<CloudCoreReducerState, ReceiveDefaultConfigurationActionAnyPayload> =
    (state, action) => {
        const processId = CloudCoreProcessId.ReceiveDefaultConfiguration;
        const actions = CloudCoreActions.receiveDefaultConfiguration;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as ReceiveDefaultConfigurationActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    hoursDifferenceOfUkraineAndMoscow: payload.hoursDifferenceOfUkraineAndMoscow,
                    defaultSegment: payload.defaultSegment,
                    mainPageNotification: payload.mainPageNotification,
                    segments: payload.segments,
                    hasSuccessFor: {
                        hasCloudCoreConfigurationReceived: true
                    },
                    russianLocalePaymentAggregator: payload.russianLocalePaymentAggregator,
                    bannerUrl: payload.bannerUrl
                });
            }
        });
    };