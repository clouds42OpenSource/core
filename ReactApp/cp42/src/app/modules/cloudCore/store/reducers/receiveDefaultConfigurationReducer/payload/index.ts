import { CloudCoreSegmentDataModel } from 'app/web/InterlayerApiProxy/CloudCoreApiProxy/receiveCloudCoreConfiguration/data-models';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте получения дефолтных конфигураций
 */
export type ReceiveDefaultConfigurationActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при ошибки получения дефолтных конфигураций
 */
export type ReceiveDefaultConfigurationActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном получении дефолтных конфигураций
 */
export type ReceiveDefaultConfigurationActionSuccessPayload = ReducerActionSuccessPayload & {
    /**
     * Разница между Украиной и Москвой
     */
    hoursDifferenceOfUkraineAndMoscow: number,

    /**
     * Дефолтный сегмент
     */
    defaultSegment: CloudCoreSegmentDataModel,

    /**
     * Коллекция всех сегментов
     */
    segments: Array<CloudCoreSegmentDataModel>,

    /**
     * Поле полученный от дефолтных конфигураций главная страница уведомления
     */
    mainPageNotification: string,

    /**
     * Поле полученный от дефолтных конфигураций типа платежей Российкого аггрегатора
     */
    russianLocalePaymentAggregator: number,

    bannerUrl: string | null;
};

/**
 * Все возможные Action при получении дефолтных конфигураций
 */
export type ReceiveDefaultConfigurationActionAnyPayload =
    | ReceiveDefaultConfigurationActionStartPayload
    | ReceiveDefaultConfigurationActionFailedPayload
    | ReceiveDefaultConfigurationActionSuccessPayload;