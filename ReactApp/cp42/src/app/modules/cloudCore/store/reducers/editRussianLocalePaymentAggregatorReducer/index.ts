import { reducerActionState } from 'app/common/functions/reducerActionState';
import { CloudCoreProcessId } from 'app/modules/cloudCore';
import { CloudCoreActions } from 'app/modules/cloudCore/store/actions';
import { CloudCoreReducerState } from 'app/modules/cloudCore/store/reducers';
import { EditRussianLocalePaymentAnyPayload } from 'app/modules/cloudCore/store/reducers/editRussianLocalePaymentAggregatorReducer/payload';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для редактирования переключателя между Юкассой и Робокассой
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const editRussianLocalePaymentAggregatorReducer: TPartialReducerObject<CloudCoreReducerState, EditRussianLocalePaymentAnyPayload> =
    (state, action) => {
        const processId = CloudCoreProcessId.EditRussianLocalePaymentAggregator;
        const actions = CloudCoreActions.editRussianLocalePaymentAggregator;

        return reducerActionState({
            action,
            actions,
            processId,
            state
        });
    };