import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте редактирования переключателя между Юкассой и Робокассой
 */
export type EditRussianLocalePaymentActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при ошибки редактирования переключателя между Юкассой и Робокассой
 */
export type EditRussianLocalePaymentActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном редактирования переключателя между Юкассой и Робокассой
 */
export type EditRussianLocalePaymentActionSuccessPayload = ReducerActionSuccessPayload & {
    /**
     * Результат редактирования
     */
    success: boolean
};

/**
 * Все возможные Action при редактировании переключателя между Юкассой и Робокассой
 */
export type EditRussianLocalePaymentAnyPayload =
    | EditRussianLocalePaymentActionStartPayload
    | EditRussianLocalePaymentActionFailedPayload
    | EditRussianLocalePaymentActionSuccessPayload;