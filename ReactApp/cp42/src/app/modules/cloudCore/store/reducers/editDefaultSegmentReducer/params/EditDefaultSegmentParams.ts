import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для редактирования дефолтного сегмента
 */
export type EditDefaultSegmentParams = IForceThunkParam & {
    /**
     * ID дефолтного сегмента
     */
    defSegmentId: string
};