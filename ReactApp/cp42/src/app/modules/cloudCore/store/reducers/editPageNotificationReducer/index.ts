import { reducerActionState } from 'app/common/functions/reducerActionState';
import { CloudCoreProcessId } from 'app/modules/cloudCore';
import { CloudCoreActions } from 'app/modules/cloudCore/store/actions';
import { CloudCoreReducerState } from 'app/modules/cloudCore/store/reducers';
import { EditPageNotificationAnyPayload } from 'app/modules/cloudCore/store/reducers/editPageNotificationReducer/payload';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для редактирования страницы уведомления
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const editPageNotificationReducer: TPartialReducerObject<CloudCoreReducerState, EditPageNotificationAnyPayload> =
    (state, action) => {
        const processId = CloudCoreProcessId.EditMainPageNotification;
        const actions = CloudCoreActions.editMainPageNotification;

        return reducerActionState({
            action,
            actions,
            processId,
            state
        });
    };