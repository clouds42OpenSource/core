import { reducerActionState } from 'app/common/functions/reducerActionState';
import { CloudCoreProcessId } from 'app/modules/cloudCore';
import { CloudCoreActions } from 'app/modules/cloudCore/store/actions';
import { CloudCoreReducerState } from 'app/modules/cloudCore/store/reducers';
import { EditDefaultSegmentAnyPayload } from 'app/modules/cloudCore/store/reducers/editDefaultSegmentReducer/payload';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для редактирования дефолтного сегмента
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const editDefaultSegmentReducer: TPartialReducerObject<CloudCoreReducerState, EditDefaultSegmentAnyPayload> =
    (state, action) => {
        const processId = CloudCoreProcessId.EditDefaultSegment;
        const actions = CloudCoreActions.editDefaultSegment;

        return reducerActionState({
            action,
            actions,
            processId,
            state
        });
    };