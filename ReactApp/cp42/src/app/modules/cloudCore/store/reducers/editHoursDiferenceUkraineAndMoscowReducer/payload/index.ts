import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте редактирования разницы между Украиной и Москвой
 */
export type EditHoursDeferenceUkraineAndMoscowActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при ошибки редактирования разницы между Украиной и Москвой
 */
export type EditHoursDeferenceUkraineAndMoscowActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном редактирования разницы между Украиной и Москвой
 */
export type EditHoursDeferenceUkraineAndMoscowActionSuccessPayload = ReducerActionSuccessPayload & {
    /**
     * Результат редактирования
     */
    success: boolean
};

/**
 * Все возможные Action при редактировании разницы между Украиной и Москвой
 */
export type EditHoursDifferenceUkraineAndMoscowAnyPayload =
    | EditHoursDeferenceUkraineAndMoscowActionStartPayload
    | EditHoursDeferenceUkraineAndMoscowActionFailedPayload
    | EditHoursDeferenceUkraineAndMoscowActionSuccessPayload;