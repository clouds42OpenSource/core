import { reducerActionState } from 'app/common/functions/reducerActionState';
import { CloudCoreProcessId } from 'app/modules/cloudCore';
import { CloudCoreActions } from 'app/modules/cloudCore/store/actions';
import { CloudCoreReducerState } from 'app/modules/cloudCore/store/reducers';
import { EditHoursDifferenceUkraineAndMoscowAnyPayload } from 'app/modules/cloudCore/store/reducers/editHoursDiferenceUkraineAndMoscowReducer/payload';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для редактирования разницы между Украиной и Москвой
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const editHoursDifferenceUkraineAndMoscowReducer: TPartialReducerObject<CloudCoreReducerState, EditHoursDifferenceUkraineAndMoscowAnyPayload> =
    (state, action) => {
        const processId = CloudCoreProcessId.EditHoursDifferenceUkraineAndMoscow;
        const actions = CloudCoreActions.editHoursDifferenceUkraineAndMoscow;

        return reducerActionState({
            action,
            actions,
            processId,
            state
        });
    };