import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для получения дефолтных конфигураций для Настройки облака
 */
export type ReceiveDefaultConfigurationThunkParams = IForceThunkParam;