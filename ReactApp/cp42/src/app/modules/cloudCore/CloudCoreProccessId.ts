/**
 * Процесы Настройки облака
 */
export enum CloudCoreProcessId {
    /**
     * ID Процесс для получения дефолтных конфигураций
     */
    ReceiveDefaultConfiguration = 'RECEIVE_DEFAULT_CONFIGURATION',

    /**
     * ID процеса для применения редактирования разницы между украиной и москвой
     */
    EditHoursDifferenceUkraineAndMoscow = 'EDIT_DIFERENCE_UKRAINE_AND_MOSCOW',

    /**
     * ID процеса для применения редактирования дефолтного сегмента
     */
    EditDefaultSegment = 'EDIT_DEFAULT_SEGMENT',

    /**
     * ID процеса для применения редактирования уведомления главной страницы
     */
    EditMainPageNotification = 'EDIT_MAIN_PAGE_NOTIFICATION',

    /**
     * ID процеса для применения редактирования переключения между Юкассой и Робокассой
     */
    EditRussianLocalePaymentAggregator = 'EDIT_RUSSIAN_LOCALE_PAYMENT_AGGREGATOR',

    /**
     * ID процеса для применения редактирования файла баннера для главной страницы
     */
    EditMarketBannerFile = 'EDIT_MARKET_BANNER_FILE'
}