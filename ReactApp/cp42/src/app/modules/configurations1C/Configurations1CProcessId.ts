/**
 * Процесы Configurations1C
 */
export enum Configurations1CProcessId {
    /**
     * Процесс получить конфигурации 1С
     */
    ReceiveConfigurations1C = 'RECEIVE_Configurations_1C',

    /**
     * Процесс обновить конфигурацию 1С
     */
    UpdateConfiguration1C = 'UPDATE_Configuration_1C',

    /**
     * Процесс добавления конфигурацию 1С
     */
    AddConfiguration1C = 'ADD_Configuration_1C',

    /**
     * Процесс удаления конфигурацию 1С
     */
    DeleteConfiguration1C = 'DELETE_Configuration_1C'
}