/**
 * Константы для модуля Configurations1C
 */
export const Configurations1CConsts = {
    /**
     * Название редюсера для модуля Configurations1C
     */
    reducerName: 'CONFIGURATIONS_1C'
};