import { Configurations1CItemDataModel } from 'app/web/InterlayerApiProxy/Configurations1CApiProxy/receiveConfigurations1C';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения обновления конфигурации 1С
 */
export type UpdateConfiguration1CActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении обновления конфигурации 1С
 */
export type UpdateConfiguration1CActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении обновления конфигурации 1С
 */
export type UpdateConfiguration1CActionSuccessPayload = ReducerActionSuccessPayload & {

    /**
     * конфигурация для обновления
     */
    configuration1CToAdd: Configurations1CItemDataModel;
};

/**
 * Все возможные Action при обновлении конфигурации 1С
 */
export type UpdateConfiguration1CActionAnyPayload =
    | UpdateConfiguration1CActionStartPayload
    | UpdateConfiguration1CActionFailedPayload
    | UpdateConfiguration1CActionSuccessPayload;