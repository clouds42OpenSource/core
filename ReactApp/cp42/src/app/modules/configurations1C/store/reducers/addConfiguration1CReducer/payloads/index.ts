import { Configurations1CItemDataModel } from 'app/web/InterlayerApiProxy/Configurations1CApiProxy/receiveConfigurations1C';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения добавления конфигурации 1С
 */
export type AddConfiguration1CActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении добавления конфигурации 1С
 */
export type AddConfiguration1CActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении добавления конфигурации 1С
 */
export type AddConfiguration1CActionSuccessPayload = ReducerActionSuccessPayload & {
    /**
     * конфигурация для добавления
     */
    configuration1CToAdd: Configurations1CItemDataModel;
};

/**
 * Все возможные Action при обновлении конфигурации 1С
 */
export type AddConfiguration1CActionAnyPayload =
    | AddConfiguration1CActionStartPayload
    | AddConfiguration1CActionFailedPayload
    | AddConfiguration1CActionSuccessPayload;