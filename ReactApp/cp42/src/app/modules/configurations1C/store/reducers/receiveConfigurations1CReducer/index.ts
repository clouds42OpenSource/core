import { reducerActionState } from 'app/common/functions/reducerActionState';
import { Configurations1CProcessId } from 'app/modules/configurations1C/Configurations1CProcessId';
import { Configurations1CActions } from 'app/modules/configurations1C/store/actions';
import { Configurations1CReducerState } from 'app/modules/configurations1C/store/reducers';
import { ReceiveConfigurations1CActionAnyPayload, ReceiveConfigurations1CActionSuccessPayload } from 'app/modules/configurations1C/store/reducers/receiveConfigurations1CReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения записи в Configuration1C
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const receiveConfigurations1CReducer: TPartialReducerObject<Configurations1CReducerState, ReceiveConfigurations1CActionAnyPayload> =
    (state, action) => {
        const processId = Configurations1CProcessId.ReceiveConfigurations1C;
        const actions = Configurations1CActions.ReceiveConfigurations1C;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as ReceiveConfigurations1CActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    configurations1C: { ...payload.configurations1C },
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasConfigurations1CReceived: true
                    }
                });
            }
        });
    };