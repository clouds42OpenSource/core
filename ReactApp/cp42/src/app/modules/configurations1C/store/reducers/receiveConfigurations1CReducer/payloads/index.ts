import { GetConfigurations1CResultDataModel } from 'app/web/InterlayerApiProxy/Configurations1CApiProxy/receiveConfigurations1C';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения конфигураций 1С
 */
export type ReceiveConfigurations1CActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения конфигураций 1С
 */
export type ReceiveConfigurations1CActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения конфигураций 1С
 */
export type ReceiveConfigurations1CActionSuccessPayload = ReducerActionSuccessPayload & {

    /**
     * Конфигурации 1С
     */
    configurations1C: GetConfigurations1CResultDataModel
};

/**
 * Все возможные Action при получении конфигураций 1С
 */
export type ReceiveConfigurations1CActionAnyPayload =
    | ReceiveConfigurations1CActionStartPayload
    | ReceiveConfigurations1CActionFailedPayload
    | ReceiveConfigurations1CActionSuccessPayload;