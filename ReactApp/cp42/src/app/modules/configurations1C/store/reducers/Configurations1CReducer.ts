import { getInitialMetadata } from 'app/common/functions';
import { Configurations1CConsts } from 'app/modules/configurations1C/constants';
import { Configurations1CReducerState } from 'app/modules/configurations1C/store/reducers';
import { addConfiguration1CReducer } from 'app/modules/configurations1C/store/reducers/addConfiguration1CReducer';
import { deleteConfiguration1CReducer } from 'app/modules/configurations1C/store/reducers/deleteConfiguration1CReducer';
import { receiveConfigurations1CReducer } from 'app/modules/configurations1C/store/reducers/receiveConfigurations1CReducer';
import { updateConfiguration1CReducer } from 'app/modules/configurations1C/store/reducers/updateConfiguration1CReducer';
import { createReducer, initReducerState } from 'core/redux/functions';

/**
 * Начальное состояние редюсера Configurations1C
 */
const initialState = initReducerState<Configurations1CReducerState>(
    Configurations1CConsts.reducerName,
    {
        configurations1C: {
            records: [],
            metadata: getInitialMetadata()
        },
        hasSuccessFor: {
            hasConfigurations1CReceived: false
        }
    }
);

/**
 * Объединённые части редюсера для всего состояния Configurations1C
 */
const partialReducers = [
    receiveConfigurations1CReducer,
    updateConfiguration1CReducer,
    addConfiguration1CReducer,
    deleteConfiguration1CReducer
];

/**
 * Редюсер Configurations1C
 */
export const configurations1CReducer = createReducer(initialState, partialReducers);