import { Configurations1CProcessId } from 'app/modules/configurations1C/Configurations1CProcessId';
import { GetConfigurations1CResultDataModel } from 'app/web/InterlayerApiProxy/Configurations1CApiProxy/receiveConfigurations1C';
import { IReducerState } from 'core/redux/interfaces';

/**
 * Состояние редюсера для работы с конфигурациями 1С
 */
export type Configurations1CReducerState = IReducerState<Configurations1CProcessId> & {

    /**
     * Конфигурации 1С
     */
    configurations1C: GetConfigurations1CResultDataModel
    hasSuccessFor: {
        /**
         * Если true, то конфигурации 1С получены
         */
        hasConfigurations1CReceived: boolean,
    };
};