import { reducerActionState } from 'app/common/functions/reducerActionState';
import { Configurations1CProcessId } from 'app/modules/configurations1C/Configurations1CProcessId';
import { Configurations1CActions } from 'app/modules/configurations1C/store/actions';
import { Configurations1CReducerState } from 'app/modules/configurations1C/store/reducers';
import { UpdateConfiguration1CActionAnyPayload, UpdateConfiguration1CActionSuccessPayload } from 'app/modules/configurations1C/store/reducers/updateConfiguration1CReducer/payloads';
import { reducerStateProcessFail, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для редактирования записи в Configuration1C
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const updateConfiguration1CReducer: TPartialReducerObject<Configurations1CReducerState, UpdateConfiguration1CActionAnyPayload> =
    (state, action) => {
        const processId = Configurations1CProcessId.UpdateConfiguration1C;
        const actions = Configurations1CActions.UpdateConfiguration1C;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as UpdateConfiguration1CActionSuccessPayload;

                const foundConfigurationIndex = state.configurations1C.records.findIndex(item => item.configurationName === payload.configuration1CToAdd.configurationName);

                if (foundConfigurationIndex < 0) {
                    return reducerStateProcessFail(state, processId, new Error(`Не получилось найти конфигурацию с названием ${ payload.configuration1CToAdd.configurationName }`));
                }

                const newRecords = [
                    ...state.configurations1C.records
                ];

                newRecords[foundConfigurationIndex] = {
                    ...payload.configuration1CToAdd
                };

                return reducerStateProcessSuccess(state, processId, {
                    configurations1C: {
                        ...state.configurations1C,
                        records: newRecords
                    }
                });
            }
        });
    };