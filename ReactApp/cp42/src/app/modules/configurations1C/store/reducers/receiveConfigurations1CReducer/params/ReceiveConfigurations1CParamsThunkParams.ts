import { ReceiveConfigurations1CParams } from 'app/web/InterlayerApiProxy/Configurations1CApiProxy/receiveConfigurations1C/input-params';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для получения конфигураций 1С
 */
export type ReceiveConfigurations1CThunkParams = IForceThunkParam & ReceiveConfigurations1CParams;