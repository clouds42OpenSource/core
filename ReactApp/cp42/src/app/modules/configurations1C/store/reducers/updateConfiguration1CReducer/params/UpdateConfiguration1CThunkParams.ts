import { UpdateConfiguration1CParams } from 'app/web/InterlayerApiProxy/Configurations1CApiProxy/updateConfiguration1C/input-params';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для обновления конфигурации 1С
 */
export type UpdateConfiguration1CThunkParams = IForceThunkParam & UpdateConfiguration1CParams;