import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения удаления конфигурации 1С
 */
export type DeleteConfiguration1CActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении удаления конфигурации 1С
 */
export type DeleteConfiguration1CActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении удаления конфигурации 1С
 */
export type DeleteConfiguration1CActionSuccessPayload = ReducerActionSuccessPayload & {

    /**
     * Название конфигурации для удаления
     */
    configurationName: string;
};

/**
 * Все возможные Action при удалении конфигурации 1С
 */
export type DeleteConfiguration1CActionAnyPayload =
    | DeleteConfiguration1CActionStartPayload
    | DeleteConfiguration1CActionFailedPayload
    | DeleteConfiguration1CActionSuccessPayload;