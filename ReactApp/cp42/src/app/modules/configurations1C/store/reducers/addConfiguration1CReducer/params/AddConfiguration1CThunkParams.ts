import { AddConfiguration1CParams } from 'app/web/InterlayerApiProxy/Configurations1CApiProxy/addConfiguration1C/input-params';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для добавления конфигурации 1С
 */
export type AddConfiguration1CThunkParams = IForceThunkParam & AddConfiguration1CParams;