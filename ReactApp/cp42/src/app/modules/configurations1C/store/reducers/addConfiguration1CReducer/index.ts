import { reducerActionState } from 'app/common/functions/reducerActionState';
import { Configurations1CProcessId } from 'app/modules/configurations1C/Configurations1CProcessId';
import { Configurations1CActions } from 'app/modules/configurations1C/store/actions';
import { Configurations1CReducerState } from 'app/modules/configurations1C/store/reducers';
import { AddConfiguration1CActionAnyPayload, AddConfiguration1CActionSuccessPayload } from 'app/modules/configurations1C/store/reducers/addConfiguration1CReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для добавления записи в Configuration1C
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const addConfiguration1CReducer: TPartialReducerObject<Configurations1CReducerState, AddConfiguration1CActionAnyPayload> =
    (state, action) => {
        const processId = Configurations1CProcessId.AddConfiguration1C;
        const actions = Configurations1CActions.AddConfiguration1C;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as AddConfiguration1CActionSuccessPayload;

                const newRecords = [
                    payload.configuration1CToAdd,
                    ...state.configurations1C.records
                ];

                return reducerStateProcessSuccess(state, processId, {
                    configurations1C: {
                        ...state.configurations1C,
                        records: newRecords,
                        metadata: {
                            ...state.configurations1C.metadata,
                            pageSize: state.configurations1C.metadata.pageSize + 1,
                            totalItemCount: state.configurations1C.metadata.totalItemCount + 1
                        }
                    }
                });
            }
        });
    };