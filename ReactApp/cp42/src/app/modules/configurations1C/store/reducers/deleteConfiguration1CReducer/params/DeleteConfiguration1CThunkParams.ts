import { DeleteConfiguration1CParams } from 'app/web/InterlayerApiProxy/Configurations1CApiProxy/deleteConfiguration1C/input-params';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для удаления конфигурации 1С
 */
export type DeleteConfiguration1CThunkParams = IForceThunkParam & DeleteConfiguration1CParams;