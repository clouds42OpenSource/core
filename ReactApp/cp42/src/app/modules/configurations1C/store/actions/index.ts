import { Configurations1CProcessId } from 'app/modules/configurations1C/Configurations1CProcessId';
import { Configurations1CConsts } from 'app/modules/configurations1C/constants';
import { createReducerActions } from 'core/redux/functions';

/**
 * Все доступные actions дле редюсера Configurations1C
 */
export const Configurations1CActions = {
    /**
     * Набор action на получение конфигураций 1С
     */
    ReceiveConfigurations1C: createReducerActions(Configurations1CConsts.reducerName, Configurations1CProcessId.ReceiveConfigurations1C),

    /**
     * Набор action на обновление конфигурации 1С
     */
    UpdateConfiguration1C: createReducerActions(Configurations1CConsts.reducerName, Configurations1CProcessId.UpdateConfiguration1C),

    /**
     * Набор action на добавление конфигурации 1С
     */
    AddConfiguration1C: createReducerActions(Configurations1CConsts.reducerName, Configurations1CProcessId.AddConfiguration1C),

    /**
     * Набор action на удаление конфигурации 1С
     */
    DeleteConfiguration1C: createReducerActions(Configurations1CConsts.reducerName, Configurations1CProcessId.DeleteConfiguration1C)
};