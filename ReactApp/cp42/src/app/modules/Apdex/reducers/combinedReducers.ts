import { combineReducers } from 'redux';

import getApdexProfilesReducer from './getApdexProfilesReducer';

export const ApdexReducer = combineReducers({
    getApdexProfilesReducer,
});