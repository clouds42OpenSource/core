import { TData, TDataReturn } from 'app/api/types';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { ARTICLES_SLICE_NAMES } from 'app/modules/articles/constants';
import { ApdexProfileResponseDto } from 'app/api/endpoints/apdex/response';

const initialState: TDataReturn<ApdexProfileResponseDto> = {
    data: null,
    error: null,
    isLoading: false
};

export const getApdexProfilesReducer = createSlice({
    name: ARTICLES_SLICE_NAMES.getArticlesSummaryInfo,
    initialState,
    reducers: {
        loading(state) {
            state.isLoading = true;
            state.error = null;
        },
        success(state, action: PayloadAction<TData<ApdexProfileResponseDto>>) {
            state.isLoading = false;
            state.error = null;
            state.data = action.payload;
        },
        error(state, action: PayloadAction<string>) {
            state.isLoading = false;
            state.error = action.payload;
            state.data = null;
        }
    }
});

export default getApdexProfilesReducer.reducer;