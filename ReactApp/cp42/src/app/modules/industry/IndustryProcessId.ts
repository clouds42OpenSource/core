/**
 * Процесы Industry
 */
export enum IndustryProcessId {
    /**
     * Процесс получить отрасли
     */
    ReceiveIndustry = 'RECEIVE_INDUSTRY',

    /**
     * Процесс обновить отрасль
     */
    UpdateIndustry = 'UPDATE_INDUSTRY',

    /**
     * Процесс добавления отрасли
     */
    AddIndustry = 'ADD_INDUSTRY',

    /**
     * Процесс удаления отрасли
     */
    DeleteIndustry = 'DELETE_INDUSTRY'
}