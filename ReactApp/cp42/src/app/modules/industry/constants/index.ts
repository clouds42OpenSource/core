/**
 * Константы для модуля Industry
 */
export const IndustryConsts = {
    /**
     * Название редюсера для модуля Industry
     */
    reducerName: 'INDUSTRY'
};