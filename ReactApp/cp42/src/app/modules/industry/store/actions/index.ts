import { Configurations1CConsts } from 'app/modules/configurations1C/constants';
import { IndustryProcessId } from 'app/modules/industry/IndustryProcessId';
import { createReducerActions } from 'core/redux/functions';

/**
 * Все доступные actions дле редюсера Industry
 */
export const IndustryActions = {
    /**
     * Набор action на получение отраслей
     */
    ReceiveIndustry: createReducerActions(Configurations1CConsts.reducerName, IndustryProcessId.ReceiveIndustry),

    /**
     * Набор action на обновление отрасли
     */
    UpdateIndustry: createReducerActions(Configurations1CConsts.reducerName, IndustryProcessId.UpdateIndustry),

    /**
     * Набор action на добавление отрасли
     */
    AddIndustry: createReducerActions(Configurations1CConsts.reducerName, IndustryProcessId.AddIndustry),

    /**
     * Набор action на удаление отрасли
     */
    DeleteIndustry: createReducerActions(Configurations1CConsts.reducerName, IndustryProcessId.DeleteIndustry)
};