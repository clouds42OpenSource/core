import { reducerActionState } from 'app/common/functions/reducerActionState';
import { IndustryProcessId } from 'app/modules/industry/IndustryProcessId';
import { IndustryActions } from 'app/modules/industry/store/actions';
import { IndustryReducerState } from 'app/modules/industry/store/reducers/IndustryReducerState';
import { ReceiveIndustryActionAnyPayload, ReceiveIndustryActionSuccessPayload } from 'app/modules/industry/store/reducers/receiveIndustryReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения записи в Industry
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const receiveIndustryReducer: TPartialReducerObject<IndustryReducerState, ReceiveIndustryActionAnyPayload> =
    (state, action) => {
        const processId = IndustryProcessId.ReceiveIndustry;
        const actions = IndustryActions.ReceiveIndustry;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as ReceiveIndustryActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    Industry: { ...payload.industry },
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasIndustryReceived: true
                    }
                });
            }
        });
    };