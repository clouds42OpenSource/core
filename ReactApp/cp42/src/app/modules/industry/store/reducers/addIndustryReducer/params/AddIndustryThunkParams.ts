import { AddIndustryParams } from 'app/web/InterlayerApiProxy/IndustryApiProxy/addIndustry/input-params';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для добавления отрасли
 */
export type AddIndustryThunkParams = IForceThunkParam & AddIndustryParams;