import { ReceiveIndustryParams } from 'app/web/InterlayerApiProxy/IndustryApiProxy/receiveIndustry/input-params';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для получения отраслей
 */
export type ReceiveIndustryThunkParams = IForceThunkParam & ReceiveIndustryParams;