import { DeleteIndustryParams } from 'app/web/InterlayerApiProxy/IndustryApiProxy/deleteIndustry/input-params';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для удаления отрасли
 */
export type DeleteIndustryThunkParams = IForceThunkParam & DeleteIndustryParams;