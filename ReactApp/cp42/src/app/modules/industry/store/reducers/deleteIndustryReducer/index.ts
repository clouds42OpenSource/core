import { reducerActionState } from 'app/common/functions/reducerActionState';
import { IndustryProcessId } from 'app/modules/industry/IndustryProcessId';
import { IndustryActions } from 'app/modules/industry/store/actions';
import { DeleteIndustryActionAnyPayload, DeleteIndustryActionSuccessPayload } from 'app/modules/industry/store/reducers/deleteIndustryReducer/payloads';
import { IndustryReducerState } from 'app/modules/industry/store/reducers/IndustryReducerState';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для удаления записи в Industry
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const deleteIndustryReducer: TPartialReducerObject<IndustryReducerState, DeleteIndustryActionAnyPayload> =
    (state, action) => {
        const processId = IndustryProcessId.DeleteIndustry;
        const actions = IndustryActions.DeleteIndustry;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as DeleteIndustryActionSuccessPayload;

                const newRecords = state.Industry.records.filter(item => item.industryId !== payload.industryId);

                return reducerStateProcessSuccess(state, processId, {
                    Industry: {
                        ...state.Industry,
                        records: newRecords,
                        metadata: {
                            ...state.Industry.metadata,
                            pageSize: state.Industry.metadata.pageSize - 1,
                            totalItemCount: state.Industry.metadata.totalItemCount - 1
                        }
                    }
                });
            }
        });
    };