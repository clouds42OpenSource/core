import { reducerActionState } from 'app/common/functions/reducerActionState';
import { IndustryProcessId } from 'app/modules/industry/IndustryProcessId';
import { IndustryActions } from 'app/modules/industry/store/actions';
import { IndustryReducerState } from 'app/modules/industry/store/reducers';
import { UpdateIndustryActionAnyPayload, UpdateIndustryActionSuccessPayload } from 'app/modules/industry/store/reducers/updateIndustryReducer/payloads';
import { reducerStateProcessFail, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для редактировании записи в Industry
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const updateIndustryReducer: TPartialReducerObject<IndustryReducerState, UpdateIndustryActionAnyPayload> =
    (state, action) => {
        const processId = IndustryProcessId.UpdateIndustry;
        const actions = IndustryActions.UpdateIndustry;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as UpdateIndustryActionSuccessPayload;

                const foundConfigurationIndex = state.Industry.records.findIndex(item => item.industryId === payload.industryToUpdate.industryId);

                if (foundConfigurationIndex < 0) {
                    return reducerStateProcessFail(state, processId, new Error(`Не получилось найти отрасль с ID ${ payload.industryToUpdate.industryId }`));
                }

                const newRecords = [
                    ...state.Industry.records
                ];

                newRecords[foundConfigurationIndex] = {
                    ...payload.industryToUpdate
                };

                return reducerStateProcessSuccess(state, processId, {
                    Industry: {
                        ...state.Industry,
                        records: newRecords
                    }
                });
            }
        });
    };