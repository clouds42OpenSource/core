import { UpdateIndustryParams } from 'app/web/InterlayerApiProxy/IndustryApiProxy/updateIndustry/input-params';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для обновления отрасли
 */
export type UpdateIndustryThunkParams = IForceThunkParam & UpdateIndustryParams;