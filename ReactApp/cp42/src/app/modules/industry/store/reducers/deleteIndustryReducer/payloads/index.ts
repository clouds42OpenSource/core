import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения удаления отрасли
 */
export type DeleteIndustryActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении удаления отрасли
 */
export type DeleteIndustryActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении удаления отрасли
 */
export type DeleteIndustryActionSuccessPayload = ReducerActionSuccessPayload & {

    /**
     * Название отрасли для удаления
     */
    industryId: string;
};

/**
 * Все возможные Action при удалении отрасли
 */
export type DeleteIndustryActionAnyPayload =
    | DeleteIndustryActionStartPayload
    | DeleteIndustryActionFailedPayload
    | DeleteIndustryActionSuccessPayload;