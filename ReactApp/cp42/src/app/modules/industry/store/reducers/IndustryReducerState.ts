import { IndustryProcessId } from 'app/modules/industry/IndustryProcessId';
import { GetIndustryResultDataModel } from 'app/web/InterlayerApiProxy/IndustryApiProxy/receiveIndustry';
import { IReducerState } from 'core/redux/interfaces';

/**
 * Состояние редюсера для работы с отраслями
 */
export type IndustryReducerState = IReducerState<IndustryProcessId> & {

    /**
     * Отрасли
     */
    Industry: GetIndustryResultDataModel
    hasSuccessFor: {
        /**
         * Если true, то отрасли получены
         */
        hasIndustryReceived: boolean,
    };
};