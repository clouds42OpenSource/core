import { IndustryItemDataModel } from 'app/web/InterlayerApiProxy/IndustryApiProxy/receiveIndustry';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения обновления отрасли
 */
export type UpdateIndustryActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении обновления отрасли
 */
export type UpdateIndustryActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении обновления отрасли
 */
export type UpdateIndustryActionSuccessPayload = ReducerActionSuccessPayload & {

    /**
     * отрасль для обновления
     */
    industryToUpdate: IndustryItemDataModel;
};

/**
 * Все возможные Action при обновлении отрасли
 */
export type UpdateIndustryActionAnyPayload =
    | UpdateIndustryActionStartPayload
    | UpdateIndustryActionFailedPayload
    | UpdateIndustryActionSuccessPayload;