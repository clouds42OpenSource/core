import { reducerActionState } from 'app/common/functions/reducerActionState';
import { IndustryProcessId } from 'app/modules/industry/IndustryProcessId';
import { IndustryActions } from 'app/modules/industry/store/actions';
import { IndustryReducerState } from 'app/modules/industry/store/reducers';
import { AddIndustryActionAnyPayload, AddIndustryActionSuccessPayload } from 'app/modules/industry/store/reducers/addIndustryReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для добавления записи в Industry
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const addIndustryReducer: TPartialReducerObject<IndustryReducerState, AddIndustryActionAnyPayload> =
    (state, action) => {
        const processId = IndustryProcessId.AddIndustry;
        const actions = IndustryActions.AddIndustry;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as AddIndustryActionSuccessPayload;

                const newRecords = [
                    payload.industryToAdd,
                    ...state.Industry.records
                ];

                return reducerStateProcessSuccess(state, processId, {
                    Industry: {
                        ...state.Industry,
                        records: newRecords,
                        metadata: {
                            ...state.Industry.metadata,
                            pageSize: state.Industry.metadata.pageSize + 1,
                            totalItemCount: state.Industry.metadata.totalItemCount + 1
                        }
                    }
                });
            }
        });
    };