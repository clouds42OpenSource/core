import { getInitialMetadata } from 'app/common/functions';
import { IndustryConsts } from 'app/modules/industry/constants';
import { addIndustryReducer } from 'app/modules/industry/store/reducers/addIndustryReducer';
import { deleteIndustryReducer } from 'app/modules/industry/store/reducers/deleteIndustryReducer';
import { IndustryReducerState } from 'app/modules/industry/store/reducers/IndustryReducerState';
import { receiveIndustryReducer } from 'app/modules/industry/store/reducers/receiveIndustryReducer';
import { updateIndustryReducer } from 'app/modules/industry/store/reducers/updateIndustryReducer';
import { createReducer, initReducerState } from 'core/redux/functions';

/**
 * Начальное состояние редюсера Industry
 */
const initialState = initReducerState<IndustryReducerState>(
    IndustryConsts.reducerName,
    {
        Industry: {
            records: [],
            metadata: getInitialMetadata()
        },
        hasSuccessFor: {
            hasIndustryReceived: false
        }
    }
);

/**
 * Объединённые части редюсера для всего состояния Industry
 */
const partialReducers = [
    receiveIndustryReducer,
    updateIndustryReducer,
    addIndustryReducer,
    deleteIndustryReducer
];

/**
 * Редюсер Industry
 */
export const industryReducer = createReducer(initialState, partialReducers);