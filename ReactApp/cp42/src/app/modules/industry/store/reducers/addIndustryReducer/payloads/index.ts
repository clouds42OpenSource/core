import { IndustryItemDataModel } from 'app/web/InterlayerApiProxy/IndustryApiProxy/receiveIndustry';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения добавления отрасли
 */
export type AddIndustryActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении добавления отрасли
 */
export type AddIndustryActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении добавления отрасли
 */
export type AddIndustryActionSuccessPayload = ReducerActionSuccessPayload & {
    /**
     * Отрасль для добавления
     */
    industryToAdd: IndustryItemDataModel;
};

/**
 * Все возможные Action при обновлении отрасли
 */
export type AddIndustryActionAnyPayload =
    | AddIndustryActionStartPayload
    | AddIndustryActionFailedPayload
    | AddIndustryActionSuccessPayload;