import { GetIndustryResultDataModel } from 'app/web/InterlayerApiProxy/IndustryApiProxy/receiveIndustry';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения отраслей
 */
export type ReceiveIndustryActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения отраслей
 */
export type ReceiveIndustryActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения отраслей
 */
export type ReceiveIndustryActionSuccessPayload = ReducerActionSuccessPayload & {

    /**
     * Отрасли
     */
    industry: GetIndustryResultDataModel
};

/**
 * Все возможные Action при получении отраслей
 */
export type ReceiveIndustryActionAnyPayload =
    | ReceiveIndustryActionStartPayload
    | ReceiveIndustryActionFailedPayload
    | ReceiveIndustryActionSuccessPayload;