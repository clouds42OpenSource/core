/**
 * Константы для модуля AutoPaymentConsts
 */
export const AutoPaymentConsts = {
    /**
     * Название редюсера для модуля AutoPayment
     */
    reducerName: 'AUTO_PAYMENT'
};