import { createReducer, initReducerState } from 'core/redux/functions';
import { AutoPaymentConsts } from 'app/modules/AccountManagement/AutoPayment/constants';
import { autoPaymentReducerState } from 'app/modules/AccountManagement/AutoPayment/store/reducers';
import { deletePaymentMethodReducer } from 'app/modules/AccountManagement/AutoPayment/store/reducers/deletePaymentMethod';
import { getSavedPaymentMethodsReducer } from 'app/modules/AccountManagement/AutoPayment/store/reducers/getSavedPaymentMethods';

/**
 * Начальное состояние редюсера balanceManagement
 */
const initialState = initReducerState<autoPaymentReducerState>(
    AutoPaymentConsts.reducerName,
    {
        savedPaymentMethods: [],
        hasSuccessFor: {
            hasSavedPaymentMethods: false
        }
    }
);

/**
 * Объединённые части редюсера для всего состояния balanceManagement
 */
const partialReducers = [
    getSavedPaymentMethodsReducer,
    deletePaymentMethodReducer
];

/**
 * Редюсер BalanceManagement
 */
export const autoPaymentReducer = createReducer(initialState, partialReducers);