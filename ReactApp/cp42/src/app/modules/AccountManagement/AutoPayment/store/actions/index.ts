import { AutoPaymentConsts } from 'app/modules/AccountManagement/AutoPayment/constants';
import { AutoPaymentProcessId } from 'app/modules/AccountManagement/AutoPayment/AutoPaymentProcessId';
import { createReducerActions } from 'core/redux/functions';

/**
 * Все доступные actions дле редюсера AutoPaymentsActions
 */
export const AutoPaymentsActions = {
    /**
     * Список сохраненных способов оплат
     */
    GetSavedPaymentMethods: createReducerActions(AutoPaymentConsts.reducerName, AutoPaymentProcessId.GetSavedPaymentMethods),
    /**
     * Список сохраненных способов оплат
     */
    DeletePaymentMethod: createReducerActions(AutoPaymentConsts.reducerName, AutoPaymentProcessId.DeletePaymentMethod),
};