import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

import { GetSavedPaymentMethods } from 'app/web/InterlayerApiProxy/AutoPaymentApiProxy/getSavedPaymentMethods';

/**
 * Данные в редюсер при старте выполнения получения списка счетов
 */
export type GetSavedPaymentMethodsActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения списка счетов
 */
export type GetSavedPaymentMethodsActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения списка счетов
 */
export type GetSavedPaymentMethodsActionSuccessPayload = ReducerActionSuccessPayload & {

    /**
     * Список счетов
     */
    data: GetSavedPaymentMethods[]
};

/**
 * Все возможные Action при получении списка счетов
 */
export type GetSavedPaymentMethodsActionAnyPayload =
    | GetSavedPaymentMethodsActionStartPayload
    | GetSavedPaymentMethodsActionFailedPayload
    | GetSavedPaymentMethodsActionSuccessPayload;