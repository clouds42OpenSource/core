import { DeletePaymentMethodActionFailedPayload, DeletePaymentMethodActionStartPayload, DeletePaymentMethodActionSuccessPayload } from 'app/modules/AccountManagement/AutoPayment/store/reducers/deletePaymentMethod/payloads';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { AppReduxStoreState } from 'app/redux/types';
import { AutoPaymentsActions } from 'app/modules/AccountManagement/AutoPayment/store/actions';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { DeletePaymentMethodInputParams } from 'app/modules/AccountManagement/AutoPayment/store/reducers/deletePaymentMethod/params';
import { ErrorObject } from 'core/redux';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { RequestKind } from 'core/requestSender/enums';

type TActionStartPayload = DeletePaymentMethodActionStartPayload;
type TActionSuccessPayload = DeletePaymentMethodActionSuccessPayload;
type TActionFailedPayload = DeletePaymentMethodActionFailedPayload;
type TInputParams = DeletePaymentMethodInputParams;

const { DeletePaymentMethod: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = AutoPaymentsActions;

type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

export class DeletePaymentMethodThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    public static invoke(args?: TInputParams) {
        return new DeletePaymentMethodThunk().execute(args);
    }

    protected get startActionValue(): string { return START_ACTION; }

    protected get successActionValue(): string { return SUCCESS_ACTION; }

    protected get failedActionValue(): string { return FAILED_ACTION; }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        return {
            condition: true,
            getReducerStateFunc: () => args.getStore().AutoPaymentState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        try {
            const api = InterlayerApiProxy.getAutoPaymentApiProxy();
            await api.deletePaymentMethods(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, args.inputParams!);
            args.success({
                id: args.inputParams!.id
            });
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}