import { DeletePaymentMethodParams } from 'app/web/InterlayerApiProxy/AutoPaymentApiProxy/deletePaymentMethod';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для получения списка счетов
 */
export type DeletePaymentMethodInputParams = IForceThunkParam & DeletePaymentMethodParams;