import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения списка счетов
 */
export type DeletePaymentMethodActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения списка счетов
 */
export type DeletePaymentMethodActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения списка счетов
 */
export type DeletePaymentMethodActionSuccessPayload = ReducerActionSuccessPayload & {
    id: string
};

/**
 * Все возможные Action при получении списка счетов
 */
export type DeletePaymentMethodActionAnyPayload =
    | DeletePaymentMethodActionStartPayload
    | DeletePaymentMethodActionFailedPayload
    | DeletePaymentMethodActionSuccessPayload;