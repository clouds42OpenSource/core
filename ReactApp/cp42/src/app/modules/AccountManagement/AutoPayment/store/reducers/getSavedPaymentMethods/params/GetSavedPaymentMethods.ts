import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для получения списка счетов
 */
export type GetSavedPaymentMethodsParams = IForceThunkParam;