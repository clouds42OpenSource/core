import { AutoPaymentProcessId } from 'app/modules/AccountManagement/AutoPayment/AutoPaymentProcessId';
import { GetSavedPaymentMethods } from 'app/web/InterlayerApiProxy/AutoPaymentApiProxy/getSavedPaymentMethods';
import { IReducerState } from 'core/redux/interfaces';

/**
 * Состояние редюсера для работы с списком счетов
 */
export type autoPaymentReducerState = IReducerState<AutoPaymentProcessId> & {
    /**
     * данные калькулятора Фаста
     */
    savedPaymentMethods: GetSavedPaymentMethods[]

    hasSuccessFor: {
        /**
         * Если true, то ID счета получен
         */
        hasSavedPaymentMethods: boolean;
    };
};