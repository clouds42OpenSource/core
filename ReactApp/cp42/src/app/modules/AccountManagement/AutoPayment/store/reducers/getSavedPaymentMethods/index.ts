import { GetSavedPaymentMethodsActionAnyPayload, GetSavedPaymentMethodsActionSuccessPayload } from 'app/modules/AccountManagement/AutoPayment/store/reducers/getSavedPaymentMethods/payloads';
import { AutoPaymentProcessId } from 'app/modules/AccountManagement/AutoPayment/AutoPaymentProcessId';
import { AutoPaymentsActions } from 'app/modules/AccountManagement/AutoPayment/store/actions';
import { TPartialReducerObject } from 'core/redux/types';
import { autoPaymentReducerState } from 'app/modules/AccountManagement/AutoPayment/store/reducers/AutoPaymentReducerState';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { reducerStateProcessSuccess } from 'core/redux/functions';

export const getSavedPaymentMethodsReducer: TPartialReducerObject<autoPaymentReducerState, GetSavedPaymentMethodsActionAnyPayload> =
    (state, action) => {
        const processId = AutoPaymentProcessId.GetSavedPaymentMethods;
        const actions = AutoPaymentsActions.GetSavedPaymentMethods;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as GetSavedPaymentMethodsActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    savedPaymentMethods: payload.data,
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasSavedPaymentMethods: true
                    }
                });
            }
        });
    };