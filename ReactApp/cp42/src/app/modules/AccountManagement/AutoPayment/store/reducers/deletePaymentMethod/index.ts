import { DeletePaymentMethodActionAnyPayload, DeletePaymentMethodActionSuccessPayload } from 'app/modules/AccountManagement/AutoPayment/store/reducers/deletePaymentMethod/payloads';
import { AutoPaymentProcessId } from 'app/modules/AccountManagement/AutoPayment/AutoPaymentProcessId';
import { AutoPaymentsActions } from 'app/modules/AccountManagement/AutoPayment/store/actions';
import { TPartialReducerObject } from 'core/redux/types';
import { autoPaymentReducerState } from 'app/modules/AccountManagement/AutoPayment/store/reducers/AutoPaymentReducerState';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { reducerStateProcessSuccess } from 'core/redux/functions';

export const deletePaymentMethodReducer: TPartialReducerObject<autoPaymentReducerState, DeletePaymentMethodActionAnyPayload> =
    (state, action) => {
        const processId = AutoPaymentProcessId.DeletePaymentMethod;
        const actions = AutoPaymentsActions.DeletePaymentMethod;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as DeletePaymentMethodActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    savedPaymentMethods: state.savedPaymentMethods.filter(item => item.Id !== payload.id),
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                    }
                });
            }
        });
    };