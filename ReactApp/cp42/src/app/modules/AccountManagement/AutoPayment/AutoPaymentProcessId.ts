/**
 * Процесы BalanceManagementProcessId
 */
export enum AutoPaymentProcessId {
    /**
     *
     */
    GetSavedPaymentMethods = 'GET_SAVED_PAYMENT_METHODS',
    /**
     *
     */
    DeletePaymentMethod = 'DELETE_PAYMENT_METHOD'
}