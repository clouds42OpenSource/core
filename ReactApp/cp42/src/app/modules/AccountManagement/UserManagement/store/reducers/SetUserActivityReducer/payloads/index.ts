import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте изменения статуса активности
 */
export type SetUserActivityActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном изменении статуса активности
 */
export type SetUserActivityActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном изменении статуса активности
 */
export type SetUserActivityActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при изменении статуса активности
 */
export type SetUserActivityActionAnyPayload =
    | SetUserActivityActionStartPayload
    | SetUserActivityActionFailedPayload
    | SetUserActivityActionSuccessPayload;