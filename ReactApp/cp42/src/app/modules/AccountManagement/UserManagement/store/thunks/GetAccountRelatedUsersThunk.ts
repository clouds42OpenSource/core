import {
    GetAccountRelatedUsersActionFailedPayload,
    GetAccountRelatedUsersActionStartPayload,
    GetAccountRelatedUsersActionSuccessPayload
} from 'app/modules/AccountManagement/UserManagement/store/reducers/GetAccountRelatedUsersReducer/payloads';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';

import { AppReduxStoreState } from 'app/redux/types';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { ErrorObject } from 'core/redux';
import { GetAccountRelatedUsersThunkParams } from 'app/modules/AccountManagement/UserManagement/store/reducers/GetAccountRelatedUsersReducer/params';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { RequestKind } from 'core/requestSender/enums';
import { UserManagementActions } from 'app/modules/AccountManagement/UserManagement/store/actions';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = GetAccountRelatedUsersActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = GetAccountRelatedUsersActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = GetAccountRelatedUsersActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = GetAccountRelatedUsersThunkParams;

const { GetAccountRelatedUsers: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = UserManagementActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для запроса получения списка пользователей с доступом к базам текущего аккаунта
 */
export class GetAccountRelatedUsersThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new GetAccountRelatedUsersThunk().execute(args, args?.showLoadingProgress);
    }

    protected get startActionValue(): string { return START_ACTION; }

    protected get successActionValue(): string { return SUCCESS_ACTION; }

    protected get failedActionValue(): string { return FAILED_ACTION; }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        const store = args.getStore().UserManagementState;
        return {
            condition: !store.hasSuccessFor.hasAccountRelatedUsersReceived || args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().UserManagementState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        const requestArgs = args.inputParams!;

        try {
            const api = InterlayerApiProxy.getUserManagementApiProxy();
            const users = await api.getAccountRelatedUsers(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, {
                'page-size': requestArgs['page-size'],
                page: requestArgs.page,
                'account-related': requestArgs['account-related']
            });
            args.success({
                accountRelatedUsers: users
            });
        } catch (er) {
            console.log(er, 'error');
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}