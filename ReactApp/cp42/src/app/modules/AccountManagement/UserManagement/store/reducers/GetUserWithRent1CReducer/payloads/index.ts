import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';
import { GetUserWithRent1CDataModel } from 'app/web/InterlayerApiProxy/UserManagementApiProxy/getUserWithRent1C/data-models';

/**
 * Данные в редюсере при старте получения статуса аренды 1С
 */
export type GetUserWithRent1CActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсере при неуспешном получения статуса аренды 1С
 */
export type GetUserWithRent1CActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсере при успешном получения статуса аренды 1С
 */
export type GetUserWithRent1CActionSuccessPayload = ReducerActionSuccessPayload & {
    rent1C: GetUserWithRent1CDataModel
};

/**
 * Все возможные action при получении статуса аренды 1С
 */
export type GetUserWithRent1CActionAnyPayload =
    GetUserWithRent1CActionStartPayload |
    GetUserWithRent1CActionFailedPayload |
    GetUserWithRent1CActionSuccessPayload;