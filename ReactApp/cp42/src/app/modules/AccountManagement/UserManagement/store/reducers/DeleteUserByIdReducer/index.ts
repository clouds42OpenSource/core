import { DeleteUserByIdActionAnyPayload } from 'app/modules/AccountManagement/UserManagement/store/reducers/DeleteUserByIdReducer/payloads';
import { TPartialReducerObject } from 'core/redux/types';
import { UserManagementActions } from 'app/modules/AccountManagement/UserManagement/store/actions';
import { UserManagementProcessId } from 'app/modules/AccountManagement/UserManagement/UserManagementProcessId';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { userManagemenReducerState } from 'app/modules/AccountManagement/UserManagement/store/reducers';

export const deleteUserByIdReducer: TPartialReducerObject<userManagemenReducerState, DeleteUserByIdActionAnyPayload> =
    (state, action) => {
        const processId = UserManagementProcessId.DeleteUserById;
        const actions = UserManagementActions.DeleteUserById;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                return reducerStateProcessSuccess(state, processId, {});
            }
        });
    };