import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';
import { GetUserWithRent1CParams } from 'app/web/InterlayerApiProxy/UserManagementApiProxy/getUserWithRent1C/input-params';

/**
 * Модель параметров на получение статуса аренды 1С
 */
export type Get1CStatusForUserThunkParams = IForceThunkParam & GetUserWithRent1CParams;