import { GetUserWithRent1CActionAnyPayload, GetUserWithRent1CActionSuccessPayload } from 'app/modules/AccountManagement/UserManagement/store/reducers/GetUserWithRent1CReducer/payloads';
import { TPartialReducerObject } from 'core/redux/types';
import { UserManagementActions } from 'app/modules/AccountManagement/UserManagement/store/actions';
import { UserManagementProcessId } from 'app/modules/AccountManagement/UserManagement/UserManagementProcessId';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { userManagemenReducerState } from 'app/modules/AccountManagement/UserManagement/store/reducers';

export const getUserWithRent1CReducer: TPartialReducerObject<userManagemenReducerState, GetUserWithRent1CActionAnyPayload> = (state, action) => {
    const processId = UserManagementProcessId.Get1CStatusForUser;
    const actions = UserManagementActions.Get1CStatusForUser;

    return reducerActionState({
        action,
        actions,
        processId,
        state,
        onSuccessProcessState: () => {
            const payload = action.payload as GetUserWithRent1CActionSuccessPayload;
            return reducerStateProcessSuccess(state, processId, {
                rent1C: payload.rent1C,
                hasSuccessFor: {
                    ...state.hasSuccessFor,
                    hasRent1CReceived: true
                }
            });
        }
    });
};