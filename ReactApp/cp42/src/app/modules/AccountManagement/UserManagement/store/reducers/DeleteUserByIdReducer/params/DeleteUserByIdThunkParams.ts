import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для активации обещанного платежа
 */
export type DeleteUserByIdThunkParams = IForceThunkParam & {
    AccountUserID: string;
    refreshUserData?: () => void;
};