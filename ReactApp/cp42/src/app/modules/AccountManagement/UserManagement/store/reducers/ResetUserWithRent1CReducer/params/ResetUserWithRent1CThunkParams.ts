import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров на сброс статуса аренды 1С
 */
export type Reset1CStatusForUserThunkParams = IForceThunkParam;