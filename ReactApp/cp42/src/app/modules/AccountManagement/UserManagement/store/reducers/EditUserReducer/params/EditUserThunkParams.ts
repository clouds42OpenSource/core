import { TAccountDepartment } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/receiveMyCompany';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для активации обещанного платежа
 */
export type EditUserThunkParams = IForceThunkParam & {
    /** ID пользователя */
    id: string;
    /** Логин */
    login: string;
    /** Телефон */
    phoneNumber: string;
    /** Почта */
    email: string;
    /** Фамилия */
    lastName: string;
    /** Имя */
    firstName: string;
    /** Отчество */
    midName: string;
    /** Пароль */
    password: string;
    /** Подтверждение пароля */
    confirmPassword: string;
    /** Роли */
    roles: Array<string>;
    /** Текущий пароль */
    currentPassword?: string;
    refreshUserData?: () => void;
    departmentRequest: TAccountDepartment | null;
};