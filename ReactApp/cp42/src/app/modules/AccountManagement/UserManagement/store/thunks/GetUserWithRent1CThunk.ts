import { UserManagementActions } from 'app/modules/AccountManagement/UserManagement/store/actions';
import { Get1CStatusForUserThunkParams } from 'app/modules/AccountManagement/UserManagement/store/reducers/GetUserWithRent1CReducer/params';
import { GetUserWithRent1CActionFailedPayload, GetUserWithRent1CActionStartPayload, GetUserWithRent1CActionSuccessPayload } from 'app/modules/AccountManagement/UserManagement/store/reducers/GetUserWithRent1CReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсере при вызове действия START_ACTION
 */
type TActionStartPayload = GetUserWithRent1CActionStartPayload;

/**
 * Тип данных в редюсере при вызове действия FAILED_ACTION
 */
type TActionFailedPayload = GetUserWithRent1CActionFailedPayload;

/**
 * Тип данных в редюсере при вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = GetUserWithRent1CActionSuccessPayload;

/**
 * Параметр выполнения Thunk-и
 */
type TInputParams = Get1CStatusForUserThunkParams;

const { Get1CStatusForUser: { START_ACTION, FAILED_ACTION, SUCCESS_ACTION } } = UserManagementActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для запроса получения состояния аренды 1С
 */
export class GetUserWithRent1CThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new GetUserWithRent1CThunk().execute(args);
    }

    protected get startActionValue(): string { return START_ACTION; }

    protected get successActionValue(): string { return SUCCESS_ACTION; }

    protected get failedActionValue(): string { return FAILED_ACTION; }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        const store = args.getStore().UserManagementState;

        return {
            condition: !store.hasSuccessFor.hasRent1CReceived || args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().UserManagementState
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        try {
            const api = InterlayerApiProxy.getUserManagementApiProxy();
            const response = await api.getUserWithRent1C(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, args!.inputParams);

            args.success({
                rent1C: response
            });
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}