import { TResponseLowerCase } from 'app/api/types';
import { CreateUserResponseDto } from 'app/web/api/UserManagementProxy/response-dto';
import { AccountRelatedUsersDataModel } from 'app/web/InterlayerApiProxy/UserManagementApiProxy/getUsersWithAccess/data-models';
import { GetDatabaseListDataModel } from 'app/web/InterlayerApiProxy/UserManagementApiProxy/getDatabaseList/data-models';
import { GetUserWithRent1CDataModel } from 'app/web/InterlayerApiProxy/UserManagementApiProxy/getUserWithRent1C/data-models';
import { IReducerState } from 'core/redux/interfaces';
import { UserManagementProcessId } from 'app/modules/AccountManagement/UserManagement/UserManagementProcessId';

/**
 * Состояние редюсера для работы с списком счетов
 */
export type userManagemenReducerState = IReducerState<UserManagementProcessId> & {
    databaseList: GetDatabaseListDataModel;
    accountRelatedUsers: AccountRelatedUsersDataModel;
    rent1C: GetUserWithRent1CDataModel;
    lastCreatedUser: TResponseLowerCase<CreateUserResponseDto>;
    hasSuccessFor: {
        hasAccountRelatedUsersReceived: boolean;
        hasDatabaseListReceived: boolean;
        hasRent1CReceived: boolean;
    };
};