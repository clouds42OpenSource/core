import { ResetUserWithRent1CActionAnyPayload } from 'app/modules/AccountManagement/UserManagement/store/reducers/ResetUserWithRent1CReducer/payloads';
import { TPartialReducerObject } from 'core/redux/types';
import { UserManagementActions } from 'app/modules/AccountManagement/UserManagement/store/actions';
import { UserManagementProcessId } from 'app/modules/AccountManagement/UserManagement/UserManagementProcessId';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { userManagemenReducerState } from 'app/modules/AccountManagement/UserManagement/store/reducers';

export const resetUserWithRent1CReducer: TPartialReducerObject<userManagemenReducerState, ResetUserWithRent1CActionAnyPayload> = (state, action) => {
    const processId = UserManagementProcessId.Reset1CStatusForUser;
    const actions = UserManagementActions.Reset1CStatusForUser;

    return reducerActionState({
        action,
        actions,
        processId,
        state,
        onSuccessProcessState: () => {
            return reducerStateProcessSuccess(state, processId, {
                rent1C: {
                    success: false,
                    message: ''
                },
                hasSuccessFor: {
                    ...state.hasSuccessFor,
                    hasRent1CReceived: false
                }
            });
        }
    });
};