import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте создания нового пользователя
 */
export type AddNewUserActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном создании нового пользователя
 */
export type AddNewUserActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном создании нового пользователя
 */
export type AddNewUserActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при создании нового пользователя
 */
export type AddNewUserActionAnyPayload =
    | AddNewUserActionStartPayload
    | AddNewUserActionFailedPayload
    | AddNewUserActionSuccessPayload;