import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для активации обещанного платежа
 */
export type SetUserActivityThunkParams = IForceThunkParam & {
    /** Статус активности */
    isActivate: boolean;
    /** ID пользователя */
    accountUserID: string;
};