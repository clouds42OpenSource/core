import { TResponseLowerCase } from 'app/api/types';
import { AddNewUserActionAnyPayload } from 'app/modules/AccountManagement/UserManagement/store/reducers/AddNewUserReducer/payloads';
import { CreateUserResponseDto } from 'app/web/api/UserManagementProxy/response-dto';
import { TPartialReducerObject } from 'core/redux/types';
import { UserManagementActions } from 'app/modules/AccountManagement/UserManagement/store/actions';
import { UserManagementProcessId } from 'app/modules/AccountManagement/UserManagement/UserManagementProcessId';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { userManagemenReducerState } from 'app/modules/AccountManagement/UserManagement/store/reducers';

export const AddNewUserReducer: TPartialReducerObject<userManagemenReducerState, AddNewUserActionAnyPayload> =
    (state, action) => {
        const processId = UserManagementProcessId.AddNewUser;
        const actions = UserManagementActions.AddNewUser;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                return reducerStateProcessSuccess(state, processId, {
                    lastCreatedUser: action.payload as TResponseLowerCase<CreateUserResponseDto>
                });
            }
        });
    };