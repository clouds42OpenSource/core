import { AccountRelatedUsersParams } from 'app/web/InterlayerApiProxy/UserManagementApiProxy/getUsersWithAccess/input-params';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для активации обещанного платежа
 */
export type GetAccountRelatedUsersThunkParams = IForceThunkParam & AccountRelatedUsersParams;