import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

import { AccountRelatedUsersDataModel } from 'app/web/InterlayerApiProxy/UserManagementApiProxy/getUsersWithAccess/data-models';

/**
 * Данные в редюсер при старте получения пользователей
 */
export type GetAccountRelatedUsersActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном получении пользователей
 */
export type GetAccountRelatedUsersActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном получении пользователей
 */
export type GetAccountRelatedUsersActionSuccessPayload = ReducerActionSuccessPayload & {
    accountRelatedUsers: AccountRelatedUsersDataModel
};

/**
 * Все возможные Action при получении пользователей
 */
export type GetAccountRelatedUsersActionAnyPayload =
    | GetAccountRelatedUsersActionStartPayload
    | GetAccountRelatedUsersActionFailedPayload
    | GetAccountRelatedUsersActionSuccessPayload;