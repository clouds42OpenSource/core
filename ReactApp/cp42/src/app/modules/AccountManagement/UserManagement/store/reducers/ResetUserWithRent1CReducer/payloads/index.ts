import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';
import { GetUserWithRent1CDataModel } from 'app/web/InterlayerApiProxy/UserManagementApiProxy/getUserWithRent1C/data-models';

/**
 * Данные в редюсере при старте сброса статуса аренды 1С
 */
export type ResetUserWithRent1CActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсере при неуспешном сбросе статуса аренды 1С
 */
export type ResetUserWithRent1CActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсере при успешном сбросе статуса аренды 1С
 */
export type ResetUserWithRent1CActionSuccessPayload = ReducerActionSuccessPayload & {
    rent1C: GetUserWithRent1CDataModel
};

/**
 * Все возможные action при сбросе статуса аренды 1С
 */
export type ResetUserWithRent1CActionAnyPayload =
    ResetUserWithRent1CActionStartPayload |
    ResetUserWithRent1CActionFailedPayload |
    ResetUserWithRent1CActionSuccessPayload;