import { createReducer, initReducerState } from 'core/redux/functions';

import { AddNewUserReducer } from 'app/modules/AccountManagement/UserManagement/store/reducers/AddNewUserReducer';
import { BalanceManagementConsts } from 'app/modules/AccountManagement/BalanceManagement/constants';
import { deleteUserByIdReducer } from 'app/modules/AccountManagement/UserManagement/store/reducers/DeleteUserByIdReducer';
import { editUserReducer } from 'app/modules/AccountManagement/UserManagement/store/reducers/EditUserReducer';
import { getAccountRelatedUsersReducer } from 'app/modules/AccountManagement/UserManagement/store/reducers/GetAccountRelatedUsersReducer';
import { getDatabaseListReducer } from 'app/modules/AccountManagement/UserManagement/store/reducers/GetDatabaseListReducer';
import { setUserActivityReducer } from 'app/modules/AccountManagement/UserManagement/store/reducers/SetUserActivityReducer';
import { userManagemenReducerState } from 'app/modules/AccountManagement/UserManagement/store/reducers';
import { getUserWithRent1CReducer } from 'app/modules/AccountManagement/UserManagement/store/reducers/GetUserWithRent1CReducer';
import { resetUserWithRent1CReducer } from 'app/modules/AccountManagement/UserManagement/store/reducers/ResetUserWithRent1CReducer';

/**
 * Начальное состояние редюсера userManagementReducer
 */
const initialState = initReducerState<userManagemenReducerState>(
    BalanceManagementConsts.reducerName,
    {
        databaseList: {
            'records-count': 0,
            applications: [],
            pages: 0
        },
        accountRelatedUsers: {
            pages: 0,
            recordsCount: 0,
            users: []
        },
        rent1C: {
            success: false,
            message: ''
        },
        lastCreatedUser: {
            success: false,
            message: null,
            data: null
        },
        hasSuccessFor: {
            hasAccountRelatedUsersReceived: false,
            hasDatabaseListReceived: false,
            hasRent1CReceived: false
        }
    }
);

/**
 * Объединённые части редюсера для всего состояния userManagementReducer
 */
const partialReducers = [
    getAccountRelatedUsersReducer,
    getDatabaseListReducer,
    deleteUserByIdReducer,
    setUserActivityReducer,
    AddNewUserReducer,
    editUserReducer,
    getUserWithRent1CReducer,
    resetUserWithRent1CReducer
];

/**
 * Редюсер BalanceManagement
 */
export const userManagementReducer = createReducer(initialState, partialReducers);