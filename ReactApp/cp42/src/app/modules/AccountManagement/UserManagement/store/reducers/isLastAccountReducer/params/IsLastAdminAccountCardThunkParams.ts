import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель на запрос получения карточки аккаунта
 */
export type IsLastAdminAccountCardThunkParams = IForceThunkParam & {
    /**
     * Номер аккаунта
     */
    accountId: string | null;
};