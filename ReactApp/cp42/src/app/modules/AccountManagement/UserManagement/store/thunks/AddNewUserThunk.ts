import { TResponseLowerCase } from 'app/api/types';
import { AddNewUserActionFailedPayload, AddNewUserActionStartPayload, AddNewUserActionSuccessPayload } from 'app/modules/AccountManagement/UserManagement/store/reducers/AddNewUserReducer/payloads';
import { CreateUserResponseDto } from 'app/web/api/UserManagementProxy/response-dto';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';

import { UserManagementActions } from 'app/modules/AccountManagement/UserManagement/store/actions';
import { AddNewUserThunkParams } from 'app/modules/AccountManagement/UserManagement/store/reducers/AddNewUserReducer/params';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = AddNewUserActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = AddNewUserActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = AddNewUserActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = AddNewUserThunkParams;

const { AddNewUser: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = UserManagementActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для запроса создания нового пользователя
 */
export class AddNewUserThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new AddNewUserThunk().execute(args, args?.showLoadingProgress);
    }

    protected get startActionValue(): string { return START_ACTION; }

    protected get successActionValue(): string { return SUCCESS_ACTION; }

    protected get failedActionValue(): string { return FAILED_ACTION; }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        return {
            condition: true,
            getReducerStateFunc: () => args.getStore().UserManagementState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        const requestArgs = args.inputParams;

        if (requestArgs) {
            try {
                const data = await InterlayerApiProxy.getUserManagementApiProxy().addNewUser(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, requestArgs);

                if ('message' in data) {
                    args.success(data);
                } else {
                    args.success({
                        success: true,
                        message: null,
                        data,
                    } as TResponseLowerCase<CreateUserResponseDto>);

                    if (requestArgs.refreshUserData) {
                        requestArgs.refreshUserData();
                    }
                }
            } catch (er: unknown) {
                args.failed({
                    error: er as ErrorObject
                });
            }
        }
    }
}