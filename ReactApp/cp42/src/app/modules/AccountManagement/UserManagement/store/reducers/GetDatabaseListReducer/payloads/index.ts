import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

import { GetDatabaseListDataModel } from 'app/web/InterlayerApiProxy/UserManagementApiProxy/getDatabaseList/data-models';

/**
 * Данные в редюсер при старте получения списка баз
 */
export type GetDatabaseListActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном получении списка баз
 */
export type GetDatabaseListActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном получении списка баз
 */
export type GetDatabaseListActionSuccessPayload = ReducerActionSuccessPayload & {
    databaseList: GetDatabaseListDataModel
};

/**
 * Все возможные Action при получении списка баз
 */
export type GetDatabaseListActionAnyPayload =
    | GetDatabaseListActionStartPayload
    | GetDatabaseListActionFailedPayload
    | GetDatabaseListActionSuccessPayload;