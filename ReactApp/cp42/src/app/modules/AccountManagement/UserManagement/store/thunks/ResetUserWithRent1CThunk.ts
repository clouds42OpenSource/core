import { UserManagementActions } from 'app/modules/AccountManagement/UserManagement/store/actions';
import { Reset1CStatusForUserThunkParams } from 'app/modules/AccountManagement/UserManagement/store/reducers/ResetUserWithRent1CReducer/params';
import { ResetUserWithRent1CActionFailedPayload, ResetUserWithRent1CActionStartPayload, ResetUserWithRent1CActionSuccessPayload } from 'app/modules/AccountManagement/UserManagement/store/reducers/ResetUserWithRent1CReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';

/**
 * Тип данных в редюсере при вызове действия START_ACTION
 */
type TActionStartPayload = ResetUserWithRent1CActionStartPayload;

/**
 * Тип данных в редюсере при вызове действия FAILED_ACTION
 */
type TActionFailedPayload = ResetUserWithRent1CActionFailedPayload;

/**
 * Тип данных в редюсере при вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = ResetUserWithRent1CActionSuccessPayload;

/**
 * Параметр выполнения Thunk-и
 */
type TInputParams = Reset1CStatusForUserThunkParams;

const { Reset1CStatusForUser: { START_ACTION, FAILED_ACTION, SUCCESS_ACTION } } = UserManagementActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для запроса получения состояния аренды 1С
 */
export class ResetUserWithRent1CThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new ResetUserWithRent1CThunk().execute(args);
    }

    protected get startActionValue(): string { return START_ACTION; }

    protected get successActionValue(): string { return SUCCESS_ACTION; }

    protected get failedActionValue(): string { return FAILED_ACTION; }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        const store = args.getStore().UserManagementState;

        return {
            condition: !store.hasSuccessFor.hasRent1CReceived || args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().UserManagementState
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        try {
            args.success({
                rent1C: {
                    success: false,
                    message: ''
                }
            });
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}