import { UserManagementConsts } from 'app/modules/AccountManagement/UserManagement/constants';
import { UserManagementProcessId } from 'app/modules/AccountManagement/UserManagement/UserManagementProcessId';
import { createReducerActions } from 'core/redux/functions';

/**
 * Все доступные actions дле редюсера BalanceManagement
 */
export const UserManagementActions = {
    /**
     * Набор action для получения списка баз
     */
    GetDatabaseList: createReducerActions(UserManagementConsts.reducerName, UserManagementProcessId.GetDatabaseList),
    /**
     * Набор action удаления пользователя по ID
     */
    DeleteUserById: createReducerActions(UserManagementConsts.reducerName, UserManagementProcessId.DeleteUserById),
    /**
     * Набор action для изменения статуса активности
     */
    SetUserActivity: createReducerActions(UserManagementConsts.reducerName, UserManagementProcessId.SetUserActivity),
    /**
     * Набор action на создание нового пользователя
     */
    AddNewUser: createReducerActions(UserManagementConsts.reducerName, UserManagementProcessId.AddNewUser),
    /**
     * Набор action на редактирование полей пользвателя
     */
    EditUser: createReducerActions(UserManagementConsts.reducerName, UserManagementProcessId.EditUser),
    /**
     * Набор action на получениe пользователей связанных с аккаунтом
     */
    GetAccountRelatedUsers: createReducerActions(UserManagementConsts.reducerName, UserManagementProcessId.GetAccountRelatedUsers),
    /**
     * Набор action на получения статуса аренды 1С
     */
    Get1CStatusForUser: createReducerActions(UserManagementConsts.reducerName, UserManagementProcessId.Get1CStatusForUser),
    /**
     * Набор action для сброса состояния до начального
     */
    Reset1CStatusForUser: createReducerActions(UserManagementConsts.reducerName, UserManagementProcessId.Reset1CStatusForUser)
};