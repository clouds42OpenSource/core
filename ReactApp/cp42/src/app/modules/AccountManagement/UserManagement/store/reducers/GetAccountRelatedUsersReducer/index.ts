import { GetAccountRelatedUsersActionAnyPayload, GetAccountRelatedUsersActionSuccessPayload } from 'app/modules/AccountManagement/UserManagement/store/reducers/GetAccountRelatedUsersReducer/payloads';

import { TPartialReducerObject } from 'core/redux/types';
import { UserManagementActions } from 'app/modules/AccountManagement/UserManagement/store/actions';
import { UserManagementProcessId } from 'app/modules/AccountManagement/UserManagement/UserManagementProcessId';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { userManagemenReducerState } from 'app/modules/AccountManagement/UserManagement/store/reducers';

export const getAccountRelatedUsersReducer: TPartialReducerObject<userManagemenReducerState, GetAccountRelatedUsersActionAnyPayload> =
    (state, action) => {
        const processId = UserManagementProcessId.GetAccountRelatedUsers;
        const actions = UserManagementActions.GetAccountRelatedUsers;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as GetAccountRelatedUsersActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    accountRelatedUsers: payload.accountRelatedUsers,
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasAccountRelatedUsersReceived: true
                    }
                });
            }
        });
    };