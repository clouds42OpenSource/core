import { EditUserActionAnyPayload } from 'app/modules/AccountManagement/UserManagement/store/reducers/EditUserReducer/payloads';
import { TPartialReducerObject } from 'core/redux/types';
import { UserManagementActions } from 'app/modules/AccountManagement/UserManagement/store/actions';
import { UserManagementProcessId } from 'app/modules/AccountManagement/UserManagement/UserManagementProcessId';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { userManagemenReducerState } from 'app/modules/AccountManagement/UserManagement/store/reducers';

export const editUserReducer: TPartialReducerObject<userManagemenReducerState, EditUserActionAnyPayload> =
    (state, action) => {
        const processId = UserManagementProcessId.EditUser;
        const actions = UserManagementActions.EditUser;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                return reducerStateProcessSuccess(state, processId, {});
            }
        });
    };