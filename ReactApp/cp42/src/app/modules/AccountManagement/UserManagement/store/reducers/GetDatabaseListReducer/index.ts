import { GetDatabaseListActionAnyPayload, GetDatabaseListActionSuccessPayload } from 'app/modules/AccountManagement/UserManagement/store/reducers/GetDatabaseListReducer/payloads';

import { TPartialReducerObject } from 'core/redux/types';
import { UserManagementActions } from 'app/modules/AccountManagement/UserManagement/store/actions';
import { UserManagementProcessId } from 'app/modules/AccountManagement/UserManagement/UserManagementProcessId';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { userManagemenReducerState } from 'app/modules/AccountManagement/UserManagement/store/reducers';

export const getDatabaseListReducer: TPartialReducerObject<userManagemenReducerState, GetDatabaseListActionAnyPayload> =
    (state, action) => {
        const processId = UserManagementProcessId.GetDatabaseList;
        const actions = UserManagementActions.GetDatabaseList;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as GetDatabaseListActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    databaseList: payload.databaseList,
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasDatabaseListReceived: true
                    }
                });
            }
        });
    };