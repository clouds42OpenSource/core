import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте редактировании полей пользователя
 */
export type EditUserActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном редактировании полей пользователя
 */
export type EditUserActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном редактировании полей пользователя
 */
export type EditUserActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при редактировании полей пользователя
 */
export type EditUserActionAnyPayload =
    | EditUserActionStartPayload
    | EditUserActionFailedPayload
    | EditUserActionSuccessPayload;