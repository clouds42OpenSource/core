import { SetUserActivityActionAnyPayload } from 'app/modules/AccountManagement/UserManagement/store/reducers/SetUserActivityReducer/payloads';
import { TPartialReducerObject } from 'core/redux/types';
import { UserManagementActions } from 'app/modules/AccountManagement/UserManagement/store/actions';
import { UserManagementProcessId } from 'app/modules/AccountManagement/UserManagement/UserManagementProcessId';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { userManagemenReducerState } from 'app/modules/AccountManagement/UserManagement/store/reducers';

export const setUserActivityReducer: TPartialReducerObject<userManagemenReducerState, SetUserActivityActionAnyPayload> =
    (state, action) => {
        const processId = UserManagementProcessId.SetUserActivity;
        const actions = UserManagementActions.SetUserActivity;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                return reducerStateProcessSuccess(state, processId, {});
            }
        });
    };