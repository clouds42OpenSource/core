import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для активации обещанного платежа
 */
export type GetDatabaseListThunkParams = IForceThunkParam & {
    /** количество записей на странице */
    size: number;
    /** Страница */
    page: number;
    /** ID пользователя */
    userid: string;
};