import { TAccountDepartment } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/receiveMyCompany';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для активации обещанного платежа
 */
export type AddNewUserThunkParams = IForceThunkParam & {
    /** Логин */
    login: string;
    /** Телефон */
    phoneNumber: string;
    /** Почта */
    email: string;
    /** Фамилия */
    lastName: string;
    /** Имя */
    firstName: string;
    /** Отчество */
    midName: string;
    /** Пароль */
    password: string;
    /** Подтверждение пароля */
    confirmPassword: string;
    /** Роли */
    roles: Array<string>;
    refreshUserData?: (noNeedToggle?: boolean) => void;
    departmentRequest: TAccountDepartment | null;
};