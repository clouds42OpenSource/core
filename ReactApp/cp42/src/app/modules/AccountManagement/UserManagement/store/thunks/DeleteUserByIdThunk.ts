import { DeleteUserByIdActionFailedPayload, DeleteUserByIdActionStartPayload, DeleteUserByIdActionSuccessPayload } from 'app/modules/AccountManagement/UserManagement/store/reducers/DeleteUserByIdReducer/payloads';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';

import { AppReduxStoreState } from 'app/redux/types';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { DeleteUserByIdThunkParams } from 'app/modules/AccountManagement/UserManagement/store/reducers/DeleteUserByIdReducer/params';
import { ErrorObject } from 'core/redux';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { RequestKind } from 'core/requestSender/enums';
import { UserManagementActions } from 'app/modules/AccountManagement/UserManagement/store/actions';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = DeleteUserByIdActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = DeleteUserByIdActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = DeleteUserByIdActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = DeleteUserByIdThunkParams;

const { DeleteUserById: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = UserManagementActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для запроса удаления пользователя
 */
export class DeleteUserByIdThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new DeleteUserByIdThunk().execute(args, args?.showLoadingProgress);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        return {
            condition: true,
            getReducerStateFunc: () => args.getStore().UserManagementState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        const requestArgs = args.inputParams!;

        try {
            await InterlayerApiProxy.getUserManagementApiProxy().deleteUserById(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, requestArgs);

            if (requestArgs.refreshUserData) {
                requestArgs.refreshUserData();
            }

            args.success({});
        } catch (err: unknown) {
            args.failed({
                error: err as ErrorObject
            });
        }
    }
}