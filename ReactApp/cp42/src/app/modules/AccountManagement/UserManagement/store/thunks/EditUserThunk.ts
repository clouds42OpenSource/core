import { EditUserActionFailedPayload, EditUserActionStartPayload, EditUserActionSuccessPayload } from 'app/modules/AccountManagement/UserManagement/store/reducers/EditUserReducer/payloads';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';

import { UserManagementActions } from 'app/modules/AccountManagement/UserManagement/store/actions';
import { EditUserThunkParams } from 'app/modules/AccountManagement/UserManagement/store/reducers/EditUserReducer/params';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { RequestKind } from 'core/requestSender/enums';
import { getLogout, userId } from 'app/api';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = EditUserActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = EditUserActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = EditUserActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = EditUserThunkParams;

const { EditUser: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = UserManagementActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для запроса редактировании полей пользователя
 */
export class EditUserThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new EditUserThunk().execute(args, args?.showLoadingProgress);
    }

    protected get startActionValue(): string { return START_ACTION; }

    protected get successActionValue(): string { return SUCCESS_ACTION; }

    protected get failedActionValue(): string { return FAILED_ACTION; }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        return {
            condition: true,
            getReducerStateFunc: () => args.getStore().UserManagementState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        const requestArgs = args.inputParams!;

        try {
            const api = InterlayerApiProxy.getUserManagementApiProxy();
            const response = await api.editUser(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, requestArgs!);

            if (response.success) {
                if (requestArgs.id === userId() && requestArgs.password) {
                    getLogout();
                }

                if (requestArgs.refreshUserData) {
                    requestArgs.refreshUserData();
                }

                args.success({});
            } else {
                args.failed({
                    error: response.message as unknown as ErrorObject
                });
            }
        } catch (er: unknown) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}