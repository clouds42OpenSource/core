import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте удалении пользователя
 */
export type DeleteUserByIdActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном создании удалении пользователя
 */
export type DeleteUserByIdActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном создании удалении пользователя
 */
export type DeleteUserByIdActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при создании удалении пользователя
 */
export type DeleteUserByIdActionAnyPayload =
    | DeleteUserByIdActionStartPayload
    | DeleteUserByIdActionFailedPayload
    | DeleteUserByIdActionSuccessPayload;