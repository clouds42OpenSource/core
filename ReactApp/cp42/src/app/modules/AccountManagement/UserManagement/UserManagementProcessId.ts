/**
 * Процесы BalanceManagementProcessId
 */
export enum UserManagementProcessId {
    /**
     * Проццесс получения списка баз
     */
    GetDatabaseList = 'GET_DATABASES_LIST',
     /**
     * Проццесс удаление пользователя
     */
    DeleteUserById = 'DELETE_USER_BY_ID',
     /**
     * Проццесс для смены активности пользователя
     */
    SetUserActivity = 'SET_USER_ACTIVITY',
     /**
     * Проццесс добавления нового пользователя
     */
    AddNewUser = 'ADD_NEW_USER',
     /**
     * Проццесс редактирования полей пользователя
     */
    EditUser = 'EDIT_USER',
     /**
     * Проццесс получения пользователей связанных с аккаунтом
     */
    GetAccountRelatedUsers = 'GET_ACCOUNT_RELATED_USERS',
    /**
     * Процесс получения состояния аренды 1С
     */
    Get1CStatusForUser = 'GET_1C_STATUS_FOR_USER',
    /**
     * Процесс сброса состояния аренды 1с
     */
    Reset1CStatusForUser = 'RESET_1C_STATUS_FOR_USER',

}