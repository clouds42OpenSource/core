/**
 * Константы для модуля BalanceManagement
 */
export const UserManagementConsts = {
    /**
     * Название редюсера для модуля BalanceManagement
     */
    reducerName: 'USER_MANAGEMENT'
};