import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';
import { ReceiveTransactionsListParams } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/recieveTransactions';

/**
 * Модель параметров для получения списка транзакций
 */
export type ReceiveTransactionsThunkParams = IForceThunkParam & ReceiveTransactionsListParams;