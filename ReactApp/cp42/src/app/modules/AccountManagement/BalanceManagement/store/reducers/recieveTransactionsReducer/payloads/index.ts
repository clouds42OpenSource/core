import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

import { TransactionsDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/recieveTransactions/data-models/TransactionsDataModel';

/**
 * Данные в редюсер при старте выполнения получения списка транзакций
 */
export type ReceiveTransactionsActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения списка транзакций
 */
export type ReceiveTransactionsActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения списка транзакций
 */
export type ReceiveTransactionsActionSuccessPayload = ReducerActionSuccessPayload & {

    /**
     * Список транзакций
     */
    transactions: TransactionsDataModel
};

/**
 * Все возможные Action при получении списка транзакций
 */
export type ReceiveTransactionsActionAnyPayload =
    | ReceiveTransactionsActionStartPayload
    | ReceiveTransactionsActionFailedPayload
    | ReceiveTransactionsActionSuccessPayload;