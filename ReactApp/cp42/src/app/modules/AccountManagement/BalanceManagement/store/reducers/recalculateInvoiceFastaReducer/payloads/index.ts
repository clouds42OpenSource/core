import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';
import { InovoiceCalculatorDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/getInvoiceCalculator';

/**
 * Данные в редюсер при старте для перерасчета сервисов фаста
 */
export type RecalculateInvoiceFastaActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном перерасчете сервисов фаста
 */
export type RecalculateInvoiceFastaActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном перерасчете сервисов фаста
 */
export type RecalculateInvoiceFastaActionSuccessPayload = ReducerActionSuccessPayload & {
    invoiceCalculatorFasta: InovoiceCalculatorDataModel,
};

/**
 * Все возможные Action при перерасчете сервисов фаста
 */
export type RecalculateInvoiceFastaActionAnyPayload =
    | RecalculateInvoiceFastaActionStartPayload
    | RecalculateInvoiceFastaActionFailedPayload
    | RecalculateInvoiceFastaActionSuccessPayload;