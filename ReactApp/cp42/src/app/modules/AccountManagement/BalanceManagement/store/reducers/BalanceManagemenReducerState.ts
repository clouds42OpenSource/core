import { AccountUsersResponseDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/getAccountUsers';
import { BalanceManagementProcessId } from 'app/modules/AccountManagement/BalanceManagement/BalanceManagementProcessId';
import { BillingAccountDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/getBilingAccountData';
import { IReducerState } from 'core/redux/interfaces';
import { InovoiceCalculatorDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/getInvoiceCalculator';
import { InvoiceByIdItemDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/getInvoiceById';
import { InvoicesDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/receiveInvoices/data-models/InvoicesDataModel';
import { ProvidedLongServicesDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/recieveProvidedLongServices';
import { ProvidedServicesDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/recieveProvidedServices';
import { TransactionsDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/recieveTransactions/data-models/TransactionsDataModel';
import { QuickPayDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/quickPay';

/**
 * Состояние редюсера для работы с списком счетов
 */
export type balanceManagemenReducerState = IReducerState<BalanceManagementProcessId> & {
    /**
     * данные калькулятора Фаста
     */
    invoiceCalculatorFasta: InovoiceCalculatorDataModel
    /**
     * данные калькулятора основных сервисов
     */
    invoiceCalculatorStandard: InovoiceCalculatorDataModel
    /**
     * список счетов
     */
    invoiceList: InvoicesDataModel
    /**
     * список транзакций
     */
    transactionsList: TransactionsDataModel
    /**
     * список оказанных услуг
     */
    providedServicesList: ProvidedServicesDataModel
    /**
     * список длительных услуг
     */
    providedLongServicesList: ProvidedLongServicesDataModel
    /**
     * данные о аккаунте
     */
    billingAccount: BillingAccountDataModel
    /**
     * ID созданного счета на оплату
     */
    invoiceId: string
    /**
     * данные созданного счета на оплату
     */
    invoice: InvoiceByIdItemDataModel
    /**
     * данные созданного счета на оплату
     */
    PaymentSystem: 'Yookassa' | 'UkrPay' | 'Paybox' | 'Robokassa' | ''
    /**
     * данные созданного счета на оплату
     */
    users: AccountUsersResponseDataModel
    /**
     * данные о платеже с использованием сохраненных
     */
    quickPay: QuickPayDataModel
    hasSuccessFor: {
        /**
         * Если true, то ID счета получен
         */
        hasAccountUsersReceived: boolean
        /**
         * Если true, то ID счета получен
         */
        hasInvoiceByIdReceived: boolean
        /**
         * Если true, то создан счет на оплату
         */
        hasInvoiceCreated: boolean
        /**
         * Если true, то параметры калькулятора получены
         */
        hasInovoiceCalculatorDataModelReceived: boolean
        /**
         * Если true, то список счетов получен
         */
        hasInvoiceListReceived: boolean
        /**
         * Если true, то список транзакций получен
         */
        hasTransactionsListRecieved: boolean
        /**
         * Если true, то список оказанных услуг получен
         */
        hasProvidedServicesListRecieved: boolean
        /**
         * Если true, то список получен
         */
        hasProvidedLongServicesListRecieved: boolean
        /**
         * Если true, то список длительных услуг получен
         */
        hasBillingAccountRecieved: boolean
        /**
         * Если true, данные об аккаунте получен
         */
        hasProlongPromisePayment: boolean
        /**
         * Если true, то информация обещанного платежа прошел успешно
         */
        hasRePayPromisePayment: boolean
        /**
         * Если true, добавление платежа прошла успешно успешно
         */
        hasAddTransaction: boolean
        /**
         * Если true, то информация о платеже с запомненным методом получена
         */
        hasQuickPay: boolean
    };
};