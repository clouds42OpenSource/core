import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения продления обещанного платежа
 */
export type ProlongPromisePaymentActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении продления обещанного платежа
 */
export type ProlongPromisePaymentActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении продления обещанного платежа
 */
export type ProlongPromisePaymentActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при продления обещанного платежа
 */
export type ProlongPromisePaymentActionAnyPayload =
    | ProlongPromisePaymentActionStartPayload
    | ProlongPromisePaymentActionFailedPayload
    | ProlongPromisePaymentActionSuccessPayload;