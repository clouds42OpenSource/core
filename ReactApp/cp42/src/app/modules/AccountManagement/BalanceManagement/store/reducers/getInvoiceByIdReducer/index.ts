import { GetInvoiceByIdActionAnyPayload, GetInvoiceByIdActionSuccessPayload } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/getInvoiceByIdReducer/payloads';
import { BalanceManagementActions } from 'app/modules/AccountManagement/BalanceManagement/store/actions';
import { BalanceManagementProcessId } from 'app/modules/AccountManagement/BalanceManagement/BalanceManagementProcessId';
import { TPartialReducerObject } from 'core/redux/types';
import { balanceManagemenReducerState } from 'app/modules/AccountManagement/BalanceManagement/store/reducers';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { reducerStateProcessSuccess } from 'core/redux/functions';

export const getInvoiceByIdReducer: TPartialReducerObject<balanceManagemenReducerState, GetInvoiceByIdActionAnyPayload> =
    (state, action) => {
        const processId = BalanceManagementProcessId.GetInvoiceById;
        const actions = BalanceManagementActions.GetInvoiceById;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as GetInvoiceByIdActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    invoice: payload.invoice,
                    PaymentSystem: payload.PaymentSystem,
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasInvoiceByIdReceived: true
                    }
                });
            }
        });
    };