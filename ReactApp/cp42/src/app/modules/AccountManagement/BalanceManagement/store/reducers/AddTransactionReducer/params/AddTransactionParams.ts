import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

type AddTransaction = {
    /**
     * Сумма
     */
    Total: string
    /**
     * Тип транзакции
     */
    Type: number
    /**
     * Тип транзакции
     */
    TransactionType: number
    /**
     * Дата
     */
    Date: Date
    /**
     * Описание
     */
    Description: string
}
/**
 * Модель параметров для добавления транзакции
 */
export type AddTransactionParams = IForceThunkParam & AddTransaction;