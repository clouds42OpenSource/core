import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

import { InvoicesDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/receiveInvoices/data-models/InvoicesDataModel';

/**
 * Данные в редюсер при старте выполнения получения списка счетов
 */
export type ReceiveInvoiceListActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения списка счетов
 */
export type ReceiveInvoiceListActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения списка счетов
 */
export type ReceiveInvoiceListActionSuccessPayload = ReducerActionSuccessPayload & {

    /**
     * Список счетов
     */
    invoiceList: InvoicesDataModel
};

/**
 * Все возможные Action при получении списка счетов
 */
export type ReceiveInvoiceListActionAnyPayload =
    | ReceiveInvoiceListActionStartPayload
    | ReceiveInvoiceListActionFailedPayload
    | ReceiveInvoiceListActionSuccessPayload;