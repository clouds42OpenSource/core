import { GetAccountUsersActionAnyPayload, GetAccountUsersActionSuccessPayload } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/GetAccountUsersReducer/payloads';
import { BalanceManagementActions } from 'app/modules/AccountManagement/BalanceManagement/store/actions';
import { BalanceManagementProcessId } from 'app/modules/AccountManagement/BalanceManagement/BalanceManagementProcessId';
import { TPartialReducerObject } from 'core/redux/types';
import { balanceManagemenReducerState } from 'app/modules/AccountManagement/BalanceManagement/store/reducers';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { reducerStateProcessSuccess } from 'core/redux/functions';

export const GetAccountUsersReducer: TPartialReducerObject<balanceManagemenReducerState, GetAccountUsersActionAnyPayload> =
    (state, action) => {
        const processId = BalanceManagementProcessId.GetAccountUsers;
        const actions = BalanceManagementActions.GetAccountUsers;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as GetAccountUsersActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    users: payload.users,
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasAccountUsersReceived: true
                    }
                });
            }
        });
    };