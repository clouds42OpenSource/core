import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для перерасчета основных сервисов
 */
type recalculateStandard = {
    /** ID аккаунта */
    AccountId: string;
    /** Дата */
    PayPeriod: number,
    /** Список сервисов */
    Products: {
        /** ID сервиса */
        Identifier: string;
        /** Количество */
        Quantity: number;
        /** Активность */
        IsActive: boolean;
    }[]
};

export type RecalculateInvoiceStandardParams = IForceThunkParam & recalculateStandard;