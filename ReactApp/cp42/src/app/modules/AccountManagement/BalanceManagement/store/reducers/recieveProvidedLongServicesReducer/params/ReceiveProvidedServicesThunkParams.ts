import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';
import { ReceiveProvidedLongServicesParams } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/recieveProvidedLongServices';

/**
 * Модель параметров для получения списка длительных услуг
 */
export type ReceiveProvidedLongServicesThunkParams = IForceThunkParam & ReceiveProvidedLongServicesParams;