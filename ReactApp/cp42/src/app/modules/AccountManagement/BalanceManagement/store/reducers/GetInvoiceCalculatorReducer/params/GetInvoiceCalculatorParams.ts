import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для получения параметров калькулятора и списка сервисов
 */
export type GetInvoiceCalculatorParams = IForceThunkParam;