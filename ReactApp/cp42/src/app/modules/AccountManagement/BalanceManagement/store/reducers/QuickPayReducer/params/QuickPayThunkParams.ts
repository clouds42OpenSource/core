import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';
import { QuickPayDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/quickPay';

/**
 * Модель параметров для оплаты с сохраненным способом оплаты
 */
export type QuickPayThunkParams = IForceThunkParam & QuickPayDataModel;