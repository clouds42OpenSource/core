import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте создания обещанного платежа
 */
export type ActivatePromisePaymentActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном создании обещанного платежа
 */
export type ActivatePromisePaymentActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном создании обещанного платежа
 */
export type ActivatePromisePaymentActionSuccessPayload = ReducerActionSuccessPayload;