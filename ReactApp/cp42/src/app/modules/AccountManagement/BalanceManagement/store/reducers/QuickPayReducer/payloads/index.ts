import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';
import { QuickPayResponseDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/quickPay';

/**
 * Данные в редюсере при старте оплаты с сохраненным способом оплаты
 */
export type QuickPayActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсере при неуспешной оплате с сохраненным способом оплаты
 */
export type QuickPayFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсере при успешной оплате с сохраненными данными
 */
export type QuickPaySuccessPayload = ReducerActionSuccessPayload & {
    /**
     * Информация об оплате
     */
    quickPayStatus: QuickPayResponseDataModel
};

/**
 * Все возможные action при оплате с сохраненными данными
 */
export type QuickPayAnyPayload =
    QuickPayActionStartPayload |
    QuickPayFailedPayload |
    QuickPaySuccessPayload;