import { ReceiveTransactionsActionAnyPayload, ReceiveTransactionsActionSuccessPayload } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/recieveTransactionsReducer/payloads';

import { BalanceManagementActions } from 'app/modules/AccountManagement/BalanceManagement/store/actions';
import { BalanceManagementProcessId } from 'app/modules/AccountManagement/BalanceManagement/BalanceManagementProcessId';
import { TPartialReducerObject } from 'core/redux/types';
import { balanceManagemenReducerState } from 'app/modules/AccountManagement/BalanceManagement/store/reducers';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { reducerStateProcessSuccess } from 'core/redux/functions';

export const receiveTransactionsReducer: TPartialReducerObject<balanceManagemenReducerState, ReceiveTransactionsActionAnyPayload> =
    (state, action) => {
        const processId = BalanceManagementProcessId.ReceiveTransactions;
        const actions = BalanceManagementActions.ReceiveTransactions;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as ReceiveTransactionsActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    transactionsList: { ...payload.transactions },
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasTransactionsListRecieved: true
                    }
                });
            }
        });
    };