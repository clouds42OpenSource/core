import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';
import { CreateInvoiceDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/createInvoice';

/**
 * Данные в редюсер при старте создания счета на оплату
 */
export type CreateInvoiceActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном создании счета на оплату
 */
export type CreateInvoiceActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном создании счета на оплату
 */
export type CreateInvoiceActionSuccessPayload = ReducerActionSuccessPayload & CreateInvoiceDataModel;

/**
 * Все возможные Action при создании счета на оплату
 */
export type CreateInvoiceActionAnyPayload =
    | CreateInvoiceActionStartPayload
    | CreateInvoiceActionFailedPayload
    | CreateInvoiceActionSuccessPayload;