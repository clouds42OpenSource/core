import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { ReceiveInvoiceListActionFailedPayload, ReceiveInvoiceListActionStartPayload, ReceiveInvoiceListActionSuccessPayload } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/receiveInvoiceListReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { BalanceManagementActions } from 'app/modules/AccountManagement/BalanceManagement/store/actions';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { ErrorObject } from 'core/redux';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ReceiveInvoiceListParams } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/receiveInvoices';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = ReceiveInvoiceListActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = ReceiveInvoiceListActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = ReceiveInvoiceListActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = ReceiveInvoiceListParams;

const { ReceiveInvoiceList: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = BalanceManagementActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для запроса получения списка счетов на оплату
 */
export class ReceiveInvoiceListThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new ReceiveInvoiceListThunk().execute(args, args?.showLoadingProgress);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        const processFlowState = args.getStore().BalanceManagementState;
        return {
            condition: !processFlowState.hasSuccessFor.hasInvoiceListReceived || args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().BalanceManagementState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        try {
            const api = InterlayerApiProxy.getBalanceManagementApiProxy();
            const response = await api.receiveInvoices(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, args.inputParams!);
            args.success({
                invoiceList: response
            });
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}