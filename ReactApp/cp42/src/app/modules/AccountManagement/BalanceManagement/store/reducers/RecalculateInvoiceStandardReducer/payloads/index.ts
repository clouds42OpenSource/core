import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

import { InovoiceCalculatorDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/getInvoiceCalculator';

/**
 * Данные в редюсер при старте выполнения перерасчета основных сервисов
 */
export type RecalculateInvoiceStandardActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении перерасчета основных сервисов
 */
export type RecalculateInvoiceStandardActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении перерасчета основных сервисов
 */
export type RecalculateInvoiceStandardActionSuccessPayload = ReducerActionSuccessPayload & {
    invoiceCalculatorStandard: InovoiceCalculatorDataModel,
};

/**
 * Все возможные Action при перерасчете основных сервисов
 */
export type RecalculateInvoiceStandardActionAnyPayload =
    | RecalculateInvoiceStandardActionStartPayload
    | RecalculateInvoiceStandardActionFailedPayload
    | RecalculateInvoiceStandardActionSuccessPayload;