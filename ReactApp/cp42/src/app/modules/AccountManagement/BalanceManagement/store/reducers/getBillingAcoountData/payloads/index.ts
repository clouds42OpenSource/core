import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';
import { BillingAccountDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/getBilingAccountData';

/**
 * Данные в редюсер при старте выполнения получения данных о счете аккаунта
 */
export type GetBillingAccountDataActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения данных о счете аккаунта
 */
export type GetBillingAccountDataActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения данных о счете аккаунта
 */
export type GetBillingAccountDataActionSuccessPayload = ReducerActionSuccessPayload & {

    /**
     * Информация о аккаунте
     */
    billingAccount: BillingAccountDataModel
};

/**
 * Все возможные Action при получении данных о счете аккаунта
 */
export type GetBillingAccountDataActionAnyPayload =
    | GetBillingAccountDataActionStartPayload
    | GetBillingAccountDataActionFailedPayload
    | GetBillingAccountDataActionSuccessPayload;