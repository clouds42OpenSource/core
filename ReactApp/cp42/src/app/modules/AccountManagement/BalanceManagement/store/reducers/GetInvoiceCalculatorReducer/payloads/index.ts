import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';
import { InovoiceCalculatorDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/getInvoiceCalculator';

/**
 * Данные в редюсер при старте получения параметров калькулятора и списка сервисов
 */
export type GetInvoiceCalculatorActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном получении параметров калькулятора и списка сервисов
 */
export type GetInvoiceCalculatorActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном получении параметров калькулятора и списка сервисов
 */
export type GetInvoiceCalculatorActionSuccessPayload = ReducerActionSuccessPayload & {
    invoiceCalculatorFasta: InovoiceCalculatorDataModel,
    invoiceCalculatorStandard: InovoiceCalculatorDataModel,
};

/**
 * Все возможные Action при получении параметров калькулятора и списка сервисов
 */
export type GetInvoiceCalculatorActionAnyPayload =
    | GetInvoiceCalculatorActionStartPayload
    | GetInvoiceCalculatorActionFailedPayload
    | GetInvoiceCalculatorActionSuccessPayload;