import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

import { AccountUsersResponseDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/getAccountUsers';

/**
 * Данные в редюсер при старте выполнения получения списка транзакций
 */
export type GetAccountUsersActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения списка транзакций
 */
export type GetAccountUsersActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения списка транзакций
 */
export type GetAccountUsersActionSuccessPayload = ReducerActionSuccessPayload & {
    users: AccountUsersResponseDataModel
};

/**
 * Все возможные Action при получении списка транзакций
 */
export type GetAccountUsersActionAnyPayload =
    | GetAccountUsersActionStartPayload
    | GetAccountUsersActionFailedPayload
    | GetAccountUsersActionSuccessPayload;