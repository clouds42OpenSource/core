import { ReceiveProvidedServicesActionAnyPayload, ReceiveProvidedServicesActionSuccessPayload } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/recieveProvidedServicesReducer/payloads';

import { BalanceManagementActions } from 'app/modules/AccountManagement/BalanceManagement/store/actions';
import { BalanceManagementProcessId } from 'app/modules/AccountManagement/BalanceManagement/BalanceManagementProcessId';
import { TPartialReducerObject } from 'core/redux/types';
import { balanceManagemenReducerState } from 'app/modules/AccountManagement/BalanceManagement/store/reducers';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { reducerStateProcessSuccess } from 'core/redux/functions';

export const receiveProvidedServicesReducer: TPartialReducerObject<balanceManagemenReducerState, ReceiveProvidedServicesActionAnyPayload> =
    (state, action) => {
        const processId = BalanceManagementProcessId.ReceiveProvidedServices;
        const actions = BalanceManagementActions.ReceiveProvidedServices;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as ReceiveProvidedServicesActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    providedServicesList: { ...payload.providedServices },
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasProvidedServicesListRecieved: true
                    }
                });
            }
        });
    };