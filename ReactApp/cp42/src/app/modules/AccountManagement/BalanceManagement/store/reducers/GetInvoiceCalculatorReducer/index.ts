import { GetInvoiceCalculatorActionAnyPayload, GetInvoiceCalculatorActionSuccessPayload } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/GetInvoiceCalculatorReducer/payloads';
import { BalanceManagementActions } from 'app/modules/AccountManagement/BalanceManagement/store/actions';
import { BalanceManagementProcessId } from 'app/modules/AccountManagement/BalanceManagement/BalanceManagementProcessId';
import { TPartialReducerObject } from 'core/redux/types';
import { balanceManagemenReducerState } from 'app/modules/AccountManagement/BalanceManagement/store/reducers';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { reducerStateProcessSuccess } from 'core/redux/functions';

export const GetInvoiceCalculatorReducer: TPartialReducerObject<balanceManagemenReducerState, GetInvoiceCalculatorActionAnyPayload> =
    (state, action) => {
        const processId = BalanceManagementProcessId.GetInvoiceCalculator;
        const actions = BalanceManagementActions.GetInvoiceCalulator;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as GetInvoiceCalculatorActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    invoiceCalculatorFasta: payload.invoiceCalculatorFasta,
                    invoiceCalculatorStandard: payload.invoiceCalculatorStandard,
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasInovoiceCalculatorDataModelReceived: true
                    }
                });
            }
        });
    };