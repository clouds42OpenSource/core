import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import {
    ReceiveProvidedServicesActionFailedPayload,
    ReceiveProvidedServicesActionStartPayload,
    ReceiveProvidedServicesActionSuccessPayload
} from 'app/modules/AccountManagement/BalanceManagement/store/reducers/recieveProvidedServicesReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { BalanceManagementActions } from 'app/modules/AccountManagement/BalanceManagement/store/actions';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { ErrorObject } from 'core/redux';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ReceiveProvidedServicesThunkParams } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/recieveProvidedServicesReducer/params';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = ReceiveProvidedServicesActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = ReceiveProvidedServicesActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = ReceiveProvidedServicesActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = ReceiveProvidedServicesThunkParams;

const { ReceiveProvidedServices: { FAILED_ACTION, START_ACTION, SUCCESS_ACTION } } = BalanceManagementActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для запроса получения оказанных услуг
 */
export class ProvidedServicesThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new ProvidedServicesThunk().execute(args, args?.showLoadingProgress);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        const processFlowState = args.getStore().BalanceManagementState;
        return {
            condition: !processFlowState.hasSuccessFor.hasProvidedServicesListRecieved || args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().BalanceManagementState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        try {
            const api = InterlayerApiProxy.getBalanceManagementApiProxy();
            const requestArgs = args.inputParams!;
            const response = await api.recieveProvidedServices(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, {
                pageNumber: requestArgs.pageNumber ?? 1,
                filter: requestArgs.filter,
                showLoadingProgress: false
            });
            args.success({
                providedServices: response
            });
        } catch (er) {
            console.log(er, 'error');
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}