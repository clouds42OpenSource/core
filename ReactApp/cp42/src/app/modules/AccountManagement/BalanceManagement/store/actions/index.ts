import { BalanceManagementConsts } from 'app/modules/AccountManagement/BalanceManagement/constants';
import { BalanceManagementProcessId } from 'app/modules/AccountManagement/BalanceManagement/BalanceManagementProcessId';
import { createReducerActions } from 'core/redux/functions';

/**
 * Все доступные actions дле редюсера BalanceManagement
 */
export const BalanceManagementActions = {
    /**
     * Набор action на получение счета на оплату по ID
     */
    GetAccountUsers: createReducerActions(BalanceManagementConsts.reducerName, BalanceManagementProcessId.GetAccountUsers),
    /**
     * Набор action на получение счета на оплату по ID
     */
    GetInvoiceById: createReducerActions(BalanceManagementConsts.reducerName, BalanceManagementProcessId.GetInvoiceById),
    /**
     * Набор action на создание счета на оплату
     */
    CreateInvoice: createReducerActions(BalanceManagementConsts.reducerName, BalanceManagementProcessId.CreateInvoice),
    /**
     * Набор action на перерасчет сервисов Фаста
     */
    RecalculateInvoiceStandard: createReducerActions(BalanceManagementConsts.reducerName, BalanceManagementProcessId.RecalculateInvoiceStandard),
    /**
     * Набор action на перерасчет основных сервисов
     */
    RecalculateInvoiceFasta: createReducerActions(BalanceManagementConsts.reducerName, BalanceManagementProcessId.RecalculateInvoiceFasta),
    /**
     * Набор action на получение списка сервисов и параметров калькулятора
     */
    GetInvoiceCalulator: createReducerActions(BalanceManagementConsts.reducerName, BalanceManagementProcessId.GetInvoiceCalculator),
    /**
     * Набор action на получение списка счетов
     */
    ReceiveInvoiceList: createReducerActions(BalanceManagementConsts.reducerName, BalanceManagementProcessId.ReceiveInvoiceList),
    /**
     * Набор action на получение списка транзакций
     */
    ReceiveTransactions: createReducerActions(BalanceManagementConsts.reducerName, BalanceManagementProcessId.ReceiveTransactions),
    /**
     * Набор action на получение списка оказанных услуг
     */
    ReceiveProvidedServices: createReducerActions(BalanceManagementConsts.reducerName, BalanceManagementProcessId.ReceiveProvidedServices),
    /**
     * Набор action на получение списка длительных услуг
     */
    ReceiveProvidedLongServices: createReducerActions(BalanceManagementConsts.reducerName, BalanceManagementProcessId.ReceiveProvidedLongServices),
    /**
     * Набор action на получение данных об аккаунте
     */
    GetBillingAccountData: createReducerActions(BalanceManagementConsts.reducerName, BalanceManagementProcessId.getBillingAccountData),
    /**
     * Набор action на продления обещанного платежа
     */
    ProlongPromisePayment: createReducerActions(BalanceManagementConsts.reducerName, BalanceManagementProcessId.prolongPromisePayment),
    /**
     * Набор action на оплату обещанного платежа
     */
    RePayPromisePayment: createReducerActions(BalanceManagementConsts.reducerName, BalanceManagementProcessId.rePayPromisePayment),
    /**
     * Набор action на добавление платежа
     */
    AddTransaction: createReducerActions(BalanceManagementConsts.reducerName, BalanceManagementProcessId.AddTransaction),
    /**
     * Набор action на оплату с сохраненным способом платежа
     */
    QuickPay: createReducerActions(BalanceManagementConsts.reducerName, BalanceManagementProcessId.QuickPay)
};