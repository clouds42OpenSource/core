import { RecalculateInvoiceStandardActionAnyPayload, RecalculateInvoiceStandardActionSuccessPayload } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/RecalculateInvoiceStandardReducer/payloads';
import { BalanceManagementActions } from 'app/modules/AccountManagement/BalanceManagement/store/actions';
import { BalanceManagementProcessId } from 'app/modules/AccountManagement/BalanceManagement/BalanceManagementProcessId';
import { TPartialReducerObject } from 'core/redux/types';
import { balanceManagemenReducerState } from 'app/modules/AccountManagement/BalanceManagement/store/reducers';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { reducerStateProcessSuccess } from 'core/redux/functions';

export const RecalculateInvoiceStandardReducer: TPartialReducerObject<balanceManagemenReducerState, RecalculateInvoiceStandardActionAnyPayload> =
    (state, action) => {
        const processId = BalanceManagementProcessId.RecalculateInvoiceFasta;
        const actions = BalanceManagementActions.RecalculateInvoiceStandard;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as RecalculateInvoiceStandardActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    invoiceCalculatorStandard: payload.invoiceCalculatorStandard
                });
            }
        });
    };