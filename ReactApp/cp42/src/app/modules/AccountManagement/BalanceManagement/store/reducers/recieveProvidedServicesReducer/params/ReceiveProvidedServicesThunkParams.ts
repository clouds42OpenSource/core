import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';
import { ReceiveProvidedServicesParams } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/recieveProvidedServices';

/**
 * Модель параметров для получения оказанных услуг
 */
export type ReceiveProvidedServicesThunkParams = IForceThunkParam & ReceiveProvidedServicesParams;