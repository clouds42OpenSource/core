import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';
import { getBillingAccountParams } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/getBilingAccountData';

/**
 *  Параметры для загрузки данных о аккаунте
 */
export type GetBillingAccountDataThunkParams = IForceThunkParam & getBillingAccountParams;