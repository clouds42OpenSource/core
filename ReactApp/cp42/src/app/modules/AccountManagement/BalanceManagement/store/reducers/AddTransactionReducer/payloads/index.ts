import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения списка транзакций
 */
export type AddTransactionActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения списка транзакций
 */
export type AddTransactionActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения списка транзакций
 */
export type AddTransactionActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при получении списка транзакций
 */
export type AddTransactionActionAnyPayload =
    | AddTransactionActionStartPayload
    | AddTransactionActionFailedPayload
    | AddTransactionActionSuccessPayload;