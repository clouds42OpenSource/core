import { CreateInvoiceActionAnyPayload, CreateInvoiceActionSuccessPayload } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/createInvoiceReducer/payloads';
import { BalanceManagementActions } from 'app/modules/AccountManagement/BalanceManagement/store/actions';
import { BalanceManagementProcessId } from 'app/modules/AccountManagement/BalanceManagement/BalanceManagementProcessId';
import { TPartialReducerObject } from 'core/redux/types';
import { balanceManagemenReducerState } from 'app/modules/AccountManagement/BalanceManagement/store/reducers';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { reducerStateProcessSuccess } from 'core/redux/functions';

export const createInvoiceReducer: TPartialReducerObject<balanceManagemenReducerState, CreateInvoiceActionAnyPayload> =
    (state, action) => {
        const processId = BalanceManagementProcessId.CreateInvoice;
        const actions = BalanceManagementActions.CreateInvoice;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as CreateInvoiceActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    invoiceId: payload.Id
                });
            }
        });
    };