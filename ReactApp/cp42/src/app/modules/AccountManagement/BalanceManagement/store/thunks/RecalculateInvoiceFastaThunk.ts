import { BalanceManagementActions } from 'app/modules/AccountManagement/BalanceManagement/store/actions';
import { RecalculateInvoiceFastaParams } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/recalculateInvoiceFastaReducer/params';
import {
    RecalculateInvoiceFastaActionFailedPayload,
    RecalculateInvoiceFastaActionStartPayload,
    RecalculateInvoiceFastaActionSuccessPayload
} from 'app/modules/AccountManagement/BalanceManagement/store/reducers/recalculateInvoiceFastaReducer/payloads';
import { updateCalculatorFields } from 'app/modules/AccountManagement/BalanceManagement/store/thunks/updateCalculatorFields';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = RecalculateInvoiceFastaActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = RecalculateInvoiceFastaActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = RecalculateInvoiceFastaActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = RecalculateInvoiceFastaParams;

const { RecalculateInvoiceFasta: { FAILED_ACTION, START_ACTION, SUCCESS_ACTION } } = BalanceManagementActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для запроса перерасчета сервисов Фаста
 */
export class RecalculateInvoiceFastaThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new RecalculateInvoiceFastaThunk().execute(args, args?.showLoadingProgress);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        return {
            condition: true,
            getReducerStateFunc: () => args.getStore().BalanceManagementState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        try {
            const api = InterlayerApiProxy.getBalanceManagementApiProxy();
            const fasta = await api.recalculateFasta(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, args.inputParams!);
            const initial = args.getStore().BalanceManagementState.invoiceCalculatorFasta;

            args.success({
                invoiceCalculatorFasta: updateCalculatorFields(initial, fasta)
            });
        } catch (er) {
            console.log(er, 'error');
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}