import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения оплаты обещанного платежа
 */
export type RePayPromisePaymentActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении оплаты обещанного платежа
 */
export type RePayPromisePaymentActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении оплаты обещанного платежа
 */
export type RePayPromisePaymentActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при оплаты обещанного платежа
 */
export type RePayPromisePaymentActionAnyPayload =
    | RePayPromisePaymentActionStartPayload
    | RePayPromisePaymentActionFailedPayload
    | RePayPromisePaymentActionSuccessPayload;