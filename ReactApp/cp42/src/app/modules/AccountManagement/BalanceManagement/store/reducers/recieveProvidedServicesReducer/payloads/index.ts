import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

import { ProvidedServicesDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/recieveProvidedServices';

/**
 * Данные в редюсер при старте выполнения получения списка  оказанных услуг
 */
export type ReceiveProvidedServicesActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения списка  оказанных услуг
 */
export type ReceiveProvidedServicesActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения списка  оказанных услуг
 */
export type ReceiveProvidedServicesActionSuccessPayload = ReducerActionSuccessPayload & {

    /**
     * Список счетов
     */
    providedServices: ProvidedServicesDataModel
};

/**
 * Все возможные Action при получении списка  оказанных услуг
 */
export type ReceiveProvidedServicesActionAnyPayload =
    | ReceiveProvidedServicesActionStartPayload
    | ReceiveProvidedServicesActionFailedPayload
    | ReceiveProvidedServicesActionSuccessPayload;