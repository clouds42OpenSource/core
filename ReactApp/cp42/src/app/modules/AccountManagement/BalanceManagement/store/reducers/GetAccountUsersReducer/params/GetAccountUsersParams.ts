import { SelectDataCommonDataModel } from 'app/web/common/data-models';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для добавления транзакции
 */
export type GetAccountUsersParams = IForceThunkParam & SelectDataCommonDataModel<string>;