import { AddTransactionActionAnyPayload } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/AddTransactionReducer/payloads';
import { BalanceManagementActions } from 'app/modules/AccountManagement/BalanceManagement/store/actions';
import { BalanceManagementProcessId } from 'app/modules/AccountManagement/BalanceManagement/BalanceManagementProcessId';
import { TPartialReducerObject } from 'core/redux/types';
import { balanceManagemenReducerState } from 'app/modules/AccountManagement/BalanceManagement/store/reducers';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { reducerStateProcessSuccess } from 'core/redux/functions';

export const AddTransactionReducer: TPartialReducerObject<balanceManagemenReducerState, AddTransactionActionAnyPayload> =
    (state, action) => {
        const processId = BalanceManagementProcessId.AddTransaction;
        const actions = BalanceManagementActions.AddTransaction;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                return reducerStateProcessSuccess(state, processId, {
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasAddTransaction: true
                    }
                });
            }
        });
    };