import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';
import { Nullable } from 'app/common/types';

export type CreateCustomInvoice = {
    Description: string;
    Amount: number;
    NotifyAccountUserId: Nullable<string>;
};

export enum invoiceTypeEnum {
    fasta = 'fasta',
    subscriptionServices = 'subscription-services'
}

/**
 * Модель параметров для создания счета на произвольную сумму
 */
export type CreateCustomInvoiceDataModelInputParams = IForceThunkParam & CreateCustomInvoice;