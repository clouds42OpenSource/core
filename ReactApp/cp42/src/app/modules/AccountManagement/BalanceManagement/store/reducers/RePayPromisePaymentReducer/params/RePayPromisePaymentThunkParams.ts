import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для оплаты обещанного платежа
 */
export type RePayPromisePaymentThunkParams = IForceThunkParam;