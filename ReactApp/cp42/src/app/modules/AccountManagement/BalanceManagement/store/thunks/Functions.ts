import { AppRoutes } from 'app/AppRoutes';
import { InovoiceCalculatorDataModel, Services } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/getInvoiceCalculator';
import { RecalculateItemDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/recalculateInvoice';
import { FETCH_API } from 'app/api/useFetchApi';

/**
 *
 * @param prevState начальное состояние после получение калькулятора с параметрами
 * @param response состояние после recalculate
 * @returns возвращает модель InovoiceCalculatorDataModel обновляя только измененные поля
 */
export function updateCalculatorFields(prevState: InovoiceCalculatorDataModel, response: RecalculateItemDataModel): InovoiceCalculatorDataModel {
    const newProducts = response.Products;
    const prevProducts = prevState.products;

    const mapProducts = (item: Services): Services => {
        const newProd = newProducts.find(prod => prod.Identifier === item.identifier);
        return {
            name: item.name,
            description: item.description,
            QuantityControl: item.QuantityControl,
            isInitialActive: item.isInitialActive,
            identifier: newProd?.Identifier ?? item.identifier,
            isActive: newProd?.IsActive ?? item.isActive,
            payPeriod: newProd?.PayPeriod ?? item.payPeriod,
            quantity: newProd?.Quantity ?? item.quantity,
            rate: newProd?.Rate ?? item.rate,
            totalBonus: newProd?.TotalBonus ?? item.totalBonus,
            totalPrice: newProd?.TotalPrice ?? item.totalPrice,
        };
    };

    return ({
        /** Поля в которых не ожидается изменения */
        PayPeriodControl: prevState.PayPeriodControl,
        supplierName: prevState.supplierName,
        buyerName: prevState.buyerName,
        currency: prevState.currency,
        accountId: prevState.accountId,
        /** Поля в которых ожидается изменения */
        payPeriod: response.PayPeriod!,
        totalAfterVAT: response.TotalAfterVAT,
        totalBeforeVAT: response.TotalBeforeVAT,
        totalBonus: response.TotalBonus,
        VAT: response.VAT,
        /** маппинг массива сервисов */
        products: prevProducts.map(item => mapProducts(item))
    });
}

type PaymentSystem = 'Yookassa' | 'UkrPay' | 'Paybox' | 'Robokassa';

export function redirectTo(paymentSystem: PaymentSystem, tokenOrUrl: string, sum: number, isAuto: boolean, isSbp: boolean) {
    if (paymentSystem === 'Paybox') {
        return window.location.href = `${ tokenOrUrl }`;
    }

    if (paymentSystem === 'Yookassa') {
        return window.location.href = `${ window.location.origin }${ AppRoutes.accountManagement.balanceManagement }/confirm-payment?confirmation-token=${ tokenOrUrl }&total-sum=${ sum }&is-auto=${ isAuto }&is-sbp=${ isSbp }`;
    }

    void FETCH_API.PAYMENTS.getPaymentForm(sum);

    return '';
}

export function quickPayEvents(type: string, tokenOrUrl: string | null, totalSum: number) {
    if (type === 'embedded') {
        return window.location.href = `${ window.location.origin }${ AppRoutes.accountManagement.balanceManagement }/confirm-payment?confirmation-token=${ tokenOrUrl }&total-sum=${ totalSum }`;
    }

    if (type === 'redirect') {
        return window.location.href = `${ tokenOrUrl }`;
    }

    return window.location.href = `${ window.location.origin }${ AppRoutes.accountManagement.balanceManagement }`;
}