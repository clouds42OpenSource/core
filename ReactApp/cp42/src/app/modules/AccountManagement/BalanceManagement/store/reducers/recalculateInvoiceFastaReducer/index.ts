import { RecalculateInvoiceFastaActionAnyPayload, RecalculateInvoiceFastaActionSuccessPayload } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/recalculateInvoiceFastaReducer/payloads';
import { BalanceManagementActions } from 'app/modules/AccountManagement/BalanceManagement/store/actions';
import { BalanceManagementProcessId } from 'app/modules/AccountManagement/BalanceManagement/BalanceManagementProcessId';
import { TPartialReducerObject } from 'core/redux/types';
import { balanceManagemenReducerState } from 'app/modules/AccountManagement/BalanceManagement/store/reducers';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { reducerStateProcessSuccess } from 'core/redux/functions';

export const recalculateInvoiceFastaReducer: TPartialReducerObject<balanceManagemenReducerState, RecalculateInvoiceFastaActionAnyPayload> =
    (state, action) => {
        const processId = BalanceManagementProcessId.RecalculateInvoiceFasta;
        const actions = BalanceManagementActions.RecalculateInvoiceFasta;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as RecalculateInvoiceFastaActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    invoiceCalculatorFasta: payload.invoiceCalculatorFasta
                });
            }
        });
    };