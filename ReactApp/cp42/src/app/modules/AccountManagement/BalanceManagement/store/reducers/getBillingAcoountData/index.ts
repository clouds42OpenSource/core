import { GetBillingAccountDataActionAnyPayload, GetBillingAccountDataActionSuccessPayload } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/getBillingAcoountData/payloads';
import { BalanceManagementActions } from 'app/modules/AccountManagement/BalanceManagement/store/actions';
import { BalanceManagementProcessId } from 'app/modules/AccountManagement/BalanceManagement/BalanceManagementProcessId';
import { TPartialReducerObject } from 'core/redux/types';
import { balanceManagemenReducerState } from 'app/modules/AccountManagement/BalanceManagement/store/reducers';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { reducerStateProcessSuccess } from 'core/redux/functions';

export const getBillingAccountData: TPartialReducerObject<balanceManagemenReducerState, GetBillingAccountDataActionAnyPayload> =
    (state, action) => {
        const processId = BalanceManagementProcessId.getBillingAccountData;
        const actions = BalanceManagementActions.GetBillingAccountData;
        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as GetBillingAccountDataActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    billingAccount: { ...payload.billingAccount },
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasBillingAccountRecieved: true
                    }
                });
            }
        });
    };