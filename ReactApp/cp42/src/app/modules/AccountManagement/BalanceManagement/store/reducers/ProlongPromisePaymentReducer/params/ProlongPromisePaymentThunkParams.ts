import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для продления обещанного платежа
 */
export type ProlongPromisePaymentThunkParams = IForceThunkParam;