import { ReceiveInvoiceListActionAnyPayload, ReceiveInvoiceListActionSuccessPayload } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/receiveInvoiceListReducer/payloads';

import { BalanceManagementActions } from 'app/modules/AccountManagement/BalanceManagement/store/actions';
import { BalanceManagementProcessId } from 'app/modules/AccountManagement/BalanceManagement/BalanceManagementProcessId';
import { TPartialReducerObject } from 'core/redux/types';
import { balanceManagemenReducerState } from 'app/modules/AccountManagement/BalanceManagement/store/reducers';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { reducerStateProcessSuccess } from 'core/redux/functions';

export const receiveInvoiceListReducer: TPartialReducerObject<balanceManagemenReducerState, ReceiveInvoiceListActionAnyPayload> =
    (state, action) => {
        const processId = BalanceManagementProcessId.ReceiveInvoiceList;
        const actions = BalanceManagementActions.ReceiveInvoiceList;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as ReceiveInvoiceListActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    invoiceList: { ...payload.invoiceList },
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasInvoiceListReceived: true
                    }
                });
            }
        });
    };