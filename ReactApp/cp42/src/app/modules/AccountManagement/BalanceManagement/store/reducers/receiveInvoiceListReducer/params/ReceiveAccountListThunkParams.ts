import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';
import { ReceiveInvoiceListParams } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/receiveInvoices';

/**
 * Модель параметров для получения списка счетов
 */
export type ReceiveInvoiceListThunkParams = IForceThunkParam & ReceiveInvoiceListParams;