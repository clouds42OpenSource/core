import { GetInvoiceCalculatorActionFailedPayload, GetInvoiceCalculatorActionStartPayload, GetInvoiceCalculatorActionSuccessPayload } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/GetInvoiceCalculatorReducer/payloads';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { AppReduxStoreState } from 'app/redux/types';
import { BalanceManagementActions } from 'app/modules/AccountManagement/BalanceManagement/store/actions';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { ErrorObject } from 'core/redux';
import { GetInvoiceCalculatorParams } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/GetInvoiceCalculatorReducer/params';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = GetInvoiceCalculatorActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = GetInvoiceCalculatorActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = GetInvoiceCalculatorActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = GetInvoiceCalculatorParams;

const { GetInvoiceCalulator: { FAILED_ACTION, START_ACTION, SUCCESS_ACTION } } = BalanceManagementActions;

/**
 * Тип параметров Thunk-и при старте выполнения
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для получения параметров калькулятора
 */
export class GetInvoiceCalculatorThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new GetInvoiceCalculatorThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        const processFlowState = args.getStore().BalanceManagementState;
        return {
            condition: !processFlowState.hasSuccessFor.hasInovoiceCalculatorDataModelReceived || args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().BalanceManagementState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        try {
            const api = InterlayerApiProxy.getBalanceManagementApiProxy();
            const standard = await api.getInvoiceCalculator(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, { type: 'subscription-services' });
            const fasta = await api.getInvoiceCalculator(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, { type: 'fasta' });

            args.success({
                invoiceCalculatorFasta: fasta,
                invoiceCalculatorStandard: standard
            });
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}