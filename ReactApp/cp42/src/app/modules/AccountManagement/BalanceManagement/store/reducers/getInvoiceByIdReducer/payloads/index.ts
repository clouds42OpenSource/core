import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';
import { InvoiceByIdItemDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/getInvoiceById';

/**
 * Данные в редюсер при старте выполнения получения счета на оплату по ID
 */
export type GetInvoiceByIdActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получении счета на оплату по ID
 */
export type GetInvoiceByIdActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получении счета на оплату по ID
 */
export type GetInvoiceByIdActionSuccessPayload = ReducerActionSuccessPayload & {
    invoice: InvoiceByIdItemDataModel,
    PaymentSystem: 'Yookassa' | 'UkrPay' | 'Paybox' | 'Robokassa'
};

/**
 * Все возможные Action при получении счета на оплату по ID
 */
export type GetInvoiceByIdActionAnyPayload =
    | GetInvoiceByIdActionStartPayload
    | GetInvoiceByIdActionFailedPayload
    | GetInvoiceByIdActionSuccessPayload;