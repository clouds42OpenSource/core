import { BalanceManagementActions } from 'app/modules/AccountManagement/BalanceManagement/store/actions';
import { QuickPayActionStartPayload, QuickPayFailedPayload, QuickPaySuccessPayload } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/QuickPayReducer/payloads';
import { quickPayEvents } from 'app/modules/AccountManagement/BalanceManagement/store/thunks/Functions';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { QuickPayDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/quickPay/input-params';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсере при вызове действия START_ACTION
 */
type TActionStartPayload = QuickPayActionStartPayload;

/**
 * Тип данный в редюсере при вызове действия FAILED_ACTION
 */
type TActionFailedPayload = QuickPayFailedPayload;

/**
 * Тип данных в редюсере при вызове действия SUCCESS_ACTIONS
 */
type TActionSuccessPayload = QuickPaySuccessPayload;

/**
 * Тип параметра выполнения Thunk-и
 */
type TInputParams = IForceThunkParam & QuickPayDataModel;

const { QuickPay: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = BalanceManagementActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

export class QuickPayThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    public static invoke(args?: TInputParams) {
        return new QuickPayThunk().execute(args, args?.showLoadingProgress);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        const quickPay = args.getStore().BalanceManagementState;
        return {
            condition: !quickPay.hasSuccessFor.hasQuickPay || args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().BalanceManagementState
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        try {
            const api = InterlayerApiProxy.getBalanceManagementApiProxy();
            const response = await api.quickPay(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, args.inputParams!);
            args.success({
                quickPayStatus: response
            });
            quickPayEvents(response.type, response.token || response.redirectUrl, response.totalSum);
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}