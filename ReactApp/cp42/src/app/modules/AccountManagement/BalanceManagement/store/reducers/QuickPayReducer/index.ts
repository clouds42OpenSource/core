import { TPartialReducerObject } from 'core/redux/types';
import { balanceManagemenReducerState } from 'app/modules/AccountManagement/BalanceManagement/store/reducers';
import { QuickPayAnyPayload } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/QuickPayReducer/payloads';
import { BalanceManagementActions } from 'app/modules/AccountManagement/BalanceManagement/store/actions';
import { BalanceManagementProcessId } from 'app/modules/AccountManagement/BalanceManagement/BalanceManagementProcessId';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { reducerStateProcessSuccess } from 'core/redux/functions';

export const QuickPayReducer: TPartialReducerObject<balanceManagemenReducerState, QuickPayAnyPayload> = (state, action) => {
    const processId = BalanceManagementProcessId.QuickPay;
    const actions = BalanceManagementActions.QuickPay;

    return reducerActionState({
        action,
        actions,
        processId,
        state,
        onSuccessProcessState: () => {
            return reducerStateProcessSuccess(state, processId, {
                quickPay: { ...state.quickPay },
                hasSuccessFor: {
                    ...state.hasSuccessFor,
                    hasQuickPay: true
                }
            });
        }
    });
};