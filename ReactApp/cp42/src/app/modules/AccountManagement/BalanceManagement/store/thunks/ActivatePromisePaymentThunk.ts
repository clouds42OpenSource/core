import { FETCH_API } from 'app/api/useFetchApi';
import {
    ActivatePromisePaymentActionFailedPayload,
    ActivatePromisePaymentActionStartPayload,
    ActivatePromisePaymentActionSuccessPayload
} from 'app/modules/AccountManagement/BalanceManagement/store/reducers/ActivatePromisePaymentReducer/payloads';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { ActivatePromisePaymentParams } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/ActivatePromisePaymentReducer/params';
import { AppReduxStoreState } from 'app/redux/types';
import { BalanceManagementActions } from 'app/modules/AccountManagement/BalanceManagement/store/actions';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { ErrorObject } from 'core/redux';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = ActivatePromisePaymentActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = ActivatePromisePaymentActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = ActivatePromisePaymentActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = ActivatePromisePaymentParams;

const { CreateInvoice: { FAILED_ACTION, START_ACTION, SUCCESS_ACTION } } = BalanceManagementActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для создания счета и оплаты онлайн
 */
export class ActivatePromisePaymentThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new ActivatePromisePaymentThunk().execute(args, true);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        const processFlowState = args.getStore().BalanceManagementState;
        return {
            condition: !processFlowState.hasSuccessFor.hasInvoiceCreated || args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().BalanceManagementState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        try {
            const api = InterlayerApiProxy.getBalanceManagementApiProxy();

            if (args.inputParams?.common) {
                const invoiceId = await api.createInvoice(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, args.inputParams.common);
                await api.activatePromisePayment(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, { InvoiceId: invoiceId.Id });
                args.success({
                    Id: invoiceId.Id
                });
            }

            if (args.inputParams?.custom) {
                const invoiceId = await api.createCustomInvoice(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, args.inputParams.custom);
                await api.activatePromisePayment(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, { InvoiceId: invoiceId.Id });
                args.success({
                    Id: invoiceId.Id
                });
            }

            if (args.inputParams?.increase) {
                const invoiceId = await api.createCustomInvoice(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, args.inputParams.increase);
                const { error } = await FETCH_API.PROMISE_PAYMENT.increasePromisePayment({ invoiceId: invoiceId.Id });

                if (error) {
                    args.failed({ error: new Error(error) });
                } else {
                    args.success({
                        Id: invoiceId.Id
                    });
                }
            }
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}