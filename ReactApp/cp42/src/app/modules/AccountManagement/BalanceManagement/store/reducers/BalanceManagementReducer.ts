import { getInitialMetadata, getInitialPagination } from 'app/common/functions';
import { BalanceManagementConsts } from 'app/modules/AccountManagement/BalanceManagement/constants';
import { AddTransactionReducer } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/AddTransactionReducer';
import { balanceManagemenReducerState } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/BalanceManagemenReducerState';
import { GetAccountUsersReducer } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/GetAccountUsersReducer';
import { GetInvoiceCalculatorReducer } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/GetInvoiceCalculatorReducer';
import { ProlongPromisePaymentReducer } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/ProlongPromisePaymentReducer';
import { QuickPayReducer } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/QuickPayReducer';
import { RePayPromisePaymentReducer } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/RePayPromisePaymentReducer';
import { RecalculateInvoiceStandardReducer } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/RecalculateInvoiceStandardReducer';
import { createInvoiceReducer } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/createInvoiceReducer';
import { getBillingAccountData } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/getBillingAcoountData';
import { getInvoiceByIdReducer } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/getInvoiceByIdReducer';
import { recalculateInvoiceFastaReducer } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/recalculateInvoiceFastaReducer';
import { receiveInvoiceListReducer } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/receiveInvoiceListReducer';
import { receiveProvidedLongServicesReducer } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/recieveProvidedLongServicesReducer';
import { receiveProvidedServicesReducer } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/recieveProvidedServicesReducer';
import { receiveTransactionsReducer } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/recieveTransactionsReducer';
import { createReducer, initReducerState } from 'core/redux/functions';

/**
 * Начальное состояние редюсера balanceManagement
 */
const initialState = initReducerState<balanceManagemenReducerState>(
    BalanceManagementConsts.reducerName,
    {
        invoiceList: {
            records: [],
            pagination: getInitialPagination()
        },
        transactionsList: {
            records: [],
            pagination: getInitialPagination()
        },
        providedServicesList: {
            records: [],
            pagination: getInitialPagination()
        },
        providedLongServicesList: {
            records: [],
            pagination: getInitialPagination()
        },
        users: {
            records: [],
            metadata: getInitialMetadata()
        },
        invoiceCalculatorStandard: {
            accountId: '',
            buyerName: '',
            payPeriod: 1,
            PayPeriodControl: {
                Settings: [],
                Type: ''
            },
            currency: {
                Currency: '',
                CurrencyCode: 0
            },
            products: [],
            supplierName: '',
            totalAfterVAT: 0,
            totalBeforeVAT: 0,
            totalBonus: 0,
            VAT: 0
        },
        invoiceCalculatorFasta: {
            accountId: '',
            buyerName: '',
            payPeriod: 0,
            PayPeriodControl: {
                Settings: [],
                Type: ''
            },
            currency: {
                Currency: '',
                CurrencyCode: 0
            },
            products: [],
            supplierName: '',
            totalAfterVAT: 0,
            totalBeforeVAT: 0,
            totalBonus: 0,
            VAT: 0
        },
        billingAccount: {
            accountIsDemo: false,
            accountIsVip: false,
            currentBalance: 0,
            currentBonusBalance: 0,
            hasErrorInPayment: false,
            locale: { currency: '', name: '' },
            promisePayment: { blockReason: '', canTakePromisePayment: false, canIncreasePromisePayment: false },
            canEarlyPromisePaymentRepay: false,
            suggestedPayment: '',
            canProlongPromisePayment: false,
            regularPayment: 0
        },
        invoiceId: '',
        invoice: {
            ActDescription: '',
            ActId: '',
            Description: '',
            Id: '',
            InvoiceDate: '',
            InvoiceSum: 0,
            IsNewInvoice: false,
            ReceiptFiscalNumber: '',
            State: ''
        },
        PaymentSystem: '',
        quickPay: {
            paymentMethodId: '',
            totalSum: 0,
            isAutoPay: false
        },
        hasSuccessFor: {
            hasAccountUsersReceived: false,
            hasInvoiceByIdReceived: false,
            hasInvoiceCreated: false,
            hasInovoiceCalculatorDataModelReceived: false,
            hasInvoiceListReceived: false,
            hasBillingAccountRecieved: false,
            hasProvidedLongServicesListRecieved: false,
            hasProvidedServicesListRecieved: false,
            hasTransactionsListRecieved: false,
            hasProlongPromisePayment: false,
            hasRePayPromisePayment: false,
            hasAddTransaction: false,
            hasQuickPay: false
        }
    }
);

/**
 * Объединённые части редюсера для всего состояния balanceManagement
 */
const partialReducers = [
    getInvoiceByIdReducer,
    GetAccountUsersReducer,
    createInvoiceReducer,
    RecalculateInvoiceStandardReducer,
    recalculateInvoiceFastaReducer,
    GetInvoiceCalculatorReducer,
    receiveInvoiceListReducer,
    receiveTransactionsReducer,
    receiveProvidedServicesReducer,
    receiveProvidedLongServicesReducer,
    getBillingAccountData,
    ProlongPromisePaymentReducer,
    RePayPromisePaymentReducer,
    AddTransactionReducer,
    QuickPayReducer
];

/**
 * Редюсер BalanceManagement
 */
export const balanceManagementReducer = createReducer(initialState, partialReducers);