import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

import { ProvidedLongServicesDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/recieveProvidedLongServices';

/**
 * Данные в редюсер при старте выполнения получения длительных услуг
 */
export type ReceiveProvidedLongServicesActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения длительных услуг
 */
export type ReceiveProvidedLongServicesActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения длительных услуг
 */
export type ReceiveProvidedLongServicesActionSuccessPayload = ReducerActionSuccessPayload & {

    /**
     * Список счетов
     */
    providedLongServices: ProvidedLongServicesDataModel
};

/**
 * Все возможные Action при получении длительных услуг
 */
export type ReceiveProvidedLongServicesActionAnyPayload =
    | ReceiveProvidedLongServicesActionStartPayload
    | ReceiveProvidedLongServicesActionFailedPayload
    | ReceiveProvidedLongServicesActionSuccessPayload;