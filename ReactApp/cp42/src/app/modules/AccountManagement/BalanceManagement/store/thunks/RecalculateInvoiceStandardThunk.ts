import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import {
    RecalculateInvoiceStandardActionFailedPayload,
    RecalculateInvoiceStandardActionStartPayload,
    RecalculateInvoiceStandardActionSuccessPayload
} from 'app/modules/AccountManagement/BalanceManagement/store/reducers/RecalculateInvoiceStandardReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { BalanceManagementActions } from 'app/modules/AccountManagement/BalanceManagement/store/actions';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { ErrorObject } from 'core/redux';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { RecalculateInvoiceStandardParams } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/RecalculateInvoiceStandardReducer/params';
import { RequestKind } from 'core/requestSender/enums';
import { updateCalculatorFields } from 'app/modules/AccountManagement/BalanceManagement/store/thunks/updateCalculatorFields';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = RecalculateInvoiceStandardActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = RecalculateInvoiceStandardActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = RecalculateInvoiceStandardActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = RecalculateInvoiceStandardParams;

const { RecalculateInvoiceStandard: { FAILED_ACTION, START_ACTION, SUCCESS_ACTION } } = BalanceManagementActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для запроса перерасчета основных сервисов
 */
export class RecalculateInvoiceStandardThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new RecalculateInvoiceStandardThunk().execute(args, args?.showLoadingProgress);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        return {
            condition: true,
            getReducerStateFunc: () => args.getStore().BalanceManagementState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        try {
            const api = InterlayerApiProxy.getBalanceManagementApiProxy();
            const response = await api.recalculateStandard(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, args.inputParams!);
            const prevState = args.getStore().BalanceManagementState.invoiceCalculatorStandard;
            args.success({
                invoiceCalculatorStandard: updateCalculatorFields(prevState, response)
            });
        } catch (er) {
            console.log(er, 'error');
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}