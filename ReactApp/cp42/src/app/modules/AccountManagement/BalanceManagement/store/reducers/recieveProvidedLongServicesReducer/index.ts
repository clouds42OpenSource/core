import { ReceiveProvidedLongServicesActionAnyPayload, ReceiveProvidedLongServicesActionSuccessPayload } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/recieveProvidedLongServicesReducer/payloads';

import { BalanceManagementActions } from 'app/modules/AccountManagement/BalanceManagement/store/actions';
import { BalanceManagementProcessId } from 'app/modules/AccountManagement/BalanceManagement/BalanceManagementProcessId';
import { TPartialReducerObject } from 'core/redux/types';
import { balanceManagemenReducerState } from 'app/modules/AccountManagement/BalanceManagement/store/reducers';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { reducerStateProcessSuccess } from 'core/redux/functions';

export const receiveProvidedLongServicesReducer: TPartialReducerObject<balanceManagemenReducerState, ReceiveProvidedLongServicesActionAnyPayload> =
    (state, action) => {
        const processId = BalanceManagementProcessId.ReceiveProvidedLongServices;
        const actions = BalanceManagementActions.ReceiveProvidedLongServices;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as ReceiveProvidedLongServicesActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    providedLongServicesList: { ...payload.providedLongServices },
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasProvidedLongServicesListRecieved: true
                    }
                });
            }
        });
    };