import { CreateCustomInvoice } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/createInvoiceReducer/params/CreateCustomInvoiceDataModelInputParams';
import { CreateInvoice } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/createInvoiceReducer/params';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для активации обещанного платежа
 */
export type ActivatePromisePaymentParams = IForceThunkParam & {
    custom?: CreateCustomInvoice;
    common?: CreateInvoice & { type: string };
    increase?: CreateCustomInvoice;
};