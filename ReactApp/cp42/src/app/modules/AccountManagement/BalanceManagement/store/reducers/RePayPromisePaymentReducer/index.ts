import { BalanceManagementActions } from 'app/modules/AccountManagement/BalanceManagement/store/actions';
import { BalanceManagementProcessId } from 'app/modules/AccountManagement/BalanceManagement/BalanceManagementProcessId';
import { RePayPromisePaymentActionAnyPayload } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/RePayPromisePaymentReducer/payloads';
import { TPartialReducerObject } from 'core/redux/types';
import { balanceManagemenReducerState } from 'app/modules/AccountManagement/BalanceManagement/store/reducers';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { reducerStateProcessSuccess } from 'core/redux/functions';

export const RePayPromisePaymentReducer: TPartialReducerObject<balanceManagemenReducerState, RePayPromisePaymentActionAnyPayload> =
    (state, action) => {
        const processId = BalanceManagementProcessId.rePayPromisePayment;
        const actions = BalanceManagementActions.RePayPromisePayment;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                return reducerStateProcessSuccess(state, processId, {
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasRePayPromisePayment: true
                    }
                });
            }
        });
    };