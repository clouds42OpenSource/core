import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

export type RecalculateProduct = {
    /** ID сервиса */
    Identifier: string;
    /** Количество */
    Quantity: number;
    /** Активность */
    IsActive: boolean;
};

/**
 * Модель параметров для перерасчета сервисов фаста
 */
type RecalculateFasta = {
    /** ID аккаунта */
    AccountId: string;
    /** Дата */
    PayPeriod: number,
    /** Список сервисов */
    Products: RecalculateProduct[]
};

export type RecalculateInvoiceFastaParams = IForceThunkParam & RecalculateFasta;