import { BalanceManagementActions } from 'app/modules/AccountManagement/BalanceManagement/store/actions';
import { BalanceManagementProcessId } from 'app/modules/AccountManagement/BalanceManagement/BalanceManagementProcessId';
import { ProlongPromisePaymentActionAnyPayload } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/ProlongPromisePaymentReducer/payloads';
import { TPartialReducerObject } from 'core/redux/types';
import { balanceManagemenReducerState } from 'app/modules/AccountManagement/BalanceManagement/store/reducers';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { reducerStateProcessSuccess } from 'core/redux/functions';

export const ProlongPromisePaymentReducer: TPartialReducerObject<balanceManagemenReducerState, ProlongPromisePaymentActionAnyPayload> =
    (state, action) => {
        const processId = BalanceManagementProcessId.prolongPromisePayment;
        const actions = BalanceManagementActions.ProlongPromisePayment;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                return reducerStateProcessSuccess(state, processId, {
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasProlongPromisePayment: true
                    }
                });
            }
        });
    };