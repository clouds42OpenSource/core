import { CreateInvoiceProduct } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/createInvoice/input-params/CreateInvoiceProduct';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';
import { Nullable } from 'app/common/types';

export type CreateInvoice = {
    NotifyAccountUserId: Nullable<string>;
    AccountId: string;
    PayPeriod: number;
    Products: CreateInvoiceProduct[];
    TotalBeforeVAT: number;
    VAT: number;
    TotalAfterVAT: number;
    TotalBonus: number;
    AccountAdditionalCompanyId?: string;
};

/**
 * Модель параметров для создания счета на оплату
 */
export type CreateInvoiceInputParams = IForceThunkParam & CreateInvoice;