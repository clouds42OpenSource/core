import { CreateInvoiceActionFailedPayload, CreateInvoiceActionStartPayload, CreateInvoiceActionSuccessPayload } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/createInvoiceReducer/payloads';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { AppReduxStoreState } from 'app/redux/types';
import { BalanceManagementActions } from 'app/modules/AccountManagement/BalanceManagement/store/actions';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { CreateCustomInvoiceDataModelInputParams } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/createInvoiceReducer/params/CreateCustomInvoiceDataModelInputParams';
import { ErrorObject } from 'core/redux';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = CreateInvoiceActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = CreateInvoiceActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = CreateInvoiceActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = CreateCustomInvoiceDataModelInputParams;

const { CreateInvoice: { FAILED_ACTION, START_ACTION, SUCCESS_ACTION } } = BalanceManagementActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для запроса создания счета на произвольную сумму
 */
export class CreateCustomInvoiceThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new CreateCustomInvoiceThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        const processFlowState = args.getStore().BalanceManagementState;
        return {
            condition: !processFlowState.hasSuccessFor.hasInvoiceCreated || args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().BalanceManagementState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        try {
            const api = InterlayerApiProxy.getBalanceManagementApiProxy();
            const createInvoice = await api.createCustomInvoice(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, args.inputParams!);

            args.success({
                Id: createInvoice.Id
            });
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}