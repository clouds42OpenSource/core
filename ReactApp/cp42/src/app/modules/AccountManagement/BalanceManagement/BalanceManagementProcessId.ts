/**
 * Процесы BalanceManagementProcessId
 */
export enum BalanceManagementProcessId {
    /**
     *
     */
    GetAccountUsers = 'GET_ACCOUNT_USERS',
    /**
     * Процесс получения счета на оплату
     */
    GetInvoiceById = 'GET_INVOICE_BY_ID',
    /**
     * Процесс получения создания счета на оплату
     */
    CreateInvoice = 'CREATE_INVOICE',
    /**
     * Процесс получения перерасчета основных сервисов
     */
    RecalculateInvoiceStandard = 'RE_CALCULATE_INVOICE_STANDARD',
    /**
     * Процесс получения перерасчета сервисов Фаста
     */
    RecalculateInvoiceFasta = 'RE_CALCULATE_INVOICE_FASTA',
    /**
     * Процесс получения калькулятора
     */
    GetInvoiceCalculator = 'GET_INVOICE_CALCULATOR',
    /**
     * Процесс получения список счетов
     */
    ReceiveInvoiceList = 'RECEIVE_INVOICE_LIST',
    /**
     * Процесс получения список транзакций
     */
    ReceiveTransactions = 'RECEIVE_TRANSACTIONS_LIST',
    /**
     * Процесс получения список оказанных услуг
     */
    ReceiveProvidedServices = 'RECEIVE_PROVIDED_SERVICES_LIST',
    /**
     * Процесс получения список длительных услуг
     */
    ReceiveProvidedLongServices = 'RECEIVE_PROVIDED_LONG_SERVICES_LIST',
    /**
     * Процесс получения данных об аккаунте
     */
    getBillingAccountData = 'GET_BILLING_ACCOUNT_DATA',
    /**
     * Процесс продления обещанного платежа
     */
    prolongPromisePayment = 'PROLONG_PROMISE_PAYMENT',
    /**
     * Процесс оплаты обещанного платежа
     */
    rePayPromisePayment = 'REPAY_PROMISE_PAYMENT',
    /**
     * Процесс добавления платежа
     */
    AddTransaction = 'ADD_TRANSACTION',
    /**
     * Процесс оплаты с сохраненным способом оплаты
     */
    QuickPay = 'QUICK_PAY'
}