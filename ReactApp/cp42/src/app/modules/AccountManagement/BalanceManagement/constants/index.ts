/**
 * Константы для модуля BalanceManagement
 */
export const BalanceManagementConsts = {
    /**
     * Название редюсера для модуля BalanceManagement
     */
    reducerName: 'BALANCE_MANAGEMENT'
};