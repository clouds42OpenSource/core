export type TData<R> = {
    rawData: R;
    status?: number;
    headers?: Headers;
};