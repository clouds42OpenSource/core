import { WebVerb } from 'core/requestSender/enums';

export type TFetchParams<T> = {
    url: string;
    method: WebVerb;
    headers?: HeadersInit;
    body?: T;
    hasStandardResponseType?: boolean;
    isFormData?: boolean;
    noAuthorizationToken?: boolean;
    noCredentials?: boolean;
};