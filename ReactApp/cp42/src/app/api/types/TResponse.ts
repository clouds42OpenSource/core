export type TResponseLowerCase<R> = {
    success: boolean;
    data: R | null;
    message: string | null;
};

export type TResponseUpperCase<R> = {
    Success: boolean;
    Data: R | null;
    Message: string | null;
};

export type TZabbixResponse<R> = {
    id: number;
    jsonrpc: string;
    result: R | null;
};