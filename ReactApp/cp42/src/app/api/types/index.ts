export * from './TResponse';
export * from './TData';
export * from './TFetchParams';
export * from './TDataReturn';
export * from './TFetchedData';