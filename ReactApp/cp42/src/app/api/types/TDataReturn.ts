import { TData } from 'app/api/types/TData';

export type TDataReturn<R> = {
    data: TData<R> | null;
    error: string | null;
    isLoading: boolean;
    refreshData?: () => void;
};

export type TDataLightReturn<R> = {
    data: R | null;
    error: string | null;
    isLoading: boolean;
};