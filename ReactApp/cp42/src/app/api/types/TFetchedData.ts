import { TResponseLowerCase } from 'app/api/types/TResponse';

export type TFetchedData<R> = {
    responseBody: TResponseLowerCase<R>;
    status: number;
    headers: Headers;
};

export type TFetchedBlobData = {
    responseBody: Blob;
    status: number;
    headers: Headers;
};