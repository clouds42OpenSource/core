import { zabbixApiUrlEnum } from 'app/api/endpoints/zabbix/enum';
import { TFetchedBlobData, TFetchedData, TFetchParams, TResponseLowerCase } from 'app/api/types';
import { AppRoutes } from 'app/AppRoutes';
import { localStorageHelper } from 'app/common/helpers/LocalStorageHelper';
import { encodeToBase64 } from 'app/utils';
import { changeToCamelCase } from 'app/web/common';
import { AjaxSenderBase } from 'core/requestSender/AjaxSenderBase';
import { paramsObject } from 'core/requestSender/interfaces';

export const templateErrorText = 'Произошла ошибка при получении данных от сервера';

export const accountId = () => localStorageHelper.getAccountId() ?? '';

export const contextAccountId = () => localStorageHelper.getContextAccountId() ?? '';

export const userId = () => localStorageHelper.getUserId() ?? '';

export const token = () => localStorageHelper.getJWTToken() ?? '';

export const toQueryParams = <T extends paramsObject>(obj: T) => `?${ AjaxSenderBase.objectToQueryString(obj).slice(0, -1) }`;

export const msHost = (endpoint: string) => `${ import.meta.env.REACT_APP_MAIN_MS_HOST }/${ endpoint }`;

export const msProvider = (endpoint: string) => `${ import.meta.env.REACT_APP_MS_PROVIDER }/${ endpoint }`;

export const wpMarketHost = (endpoint: string) => `${ import.meta.env.REACT_WP_MARKET_API_URL }/${ endpoint }`;

export const wpHost = (endpoint: string) => `${ import.meta.env.REACT_WP_URL }/${ endpoint }`;

export const apiHost = (endpoint: string) => {
    if (import.meta.env.REACT_APP_API_HOST) {
        return `${ import.meta.env.REACT_APP_API_HOST }/${ endpoint }`;
    }

    const hostParts = window.location.hostname.split('.');
    const cpPrefix = hostParts[0].toLowerCase().replace('cp', '');

    return `https://${ cpPrefix }core.42clouds.com/${ endpoint }`;
};

export const cpHost = (endpoint: string) => {
    if (import.meta.env.REACT_APP_API_HOST) {
        return `${ import.meta.env.REACT_APP_CP_HOST }/${ endpoint }`;
    }

    const hostParts = window.location.hostname.split('.');
    const cpPrefix = hostParts[0].toLowerCase().replace('cp', '');

    return `https://${ cpPrefix }cp.42clouds.com/${ endpoint }`;
};

export const getRedirected = () => {
    window.location.href = AppRoutes.homeRoute;
};

export const getLogout = () => {
    window.location.href = AppRoutes.signInRoute;
};

const basicToken = `Basic ${ encodeToBase64(`${ import.meta.env.REACT_WP_API_LOGIN }:${ import.meta.env.REACT_WP_API_PASSWORD }`) }`;

export const WP_FETCH_PARAMS = {
    noAuthorizationToken: true,
    hasStandardResponseType: false,
    headers: {
        Authorization: basicToken,
    },
} as const;

export const getFetchedData = async <R extends object | string | boolean | number | null, T extends object | string | boolean | number | void>(
    {
        url,
        body,
        headers,
        method,
        isFormData = false,
        hasStandardResponseType = true,
        noAuthorizationToken = false,
        noCredentials = false,
    }: TFetchParams<T>) => {
    let response = new Response();
    let responseBody = {} as TResponseLowerCase<R>;

    const headersRequest: HeadersInit = {
        accept: 'application/json'
    };

    if (!noAuthorizationToken) {
        headersRequest.authorization = `Bearer ${ token() }`;
    }

    if (!isFormData) {
        headersRequest['content-type'] = 'application/json';
    }

    try {
        response = await fetch(url, {
            method,
            headers: {
                ...headersRequest,
                ...headers
            },
            body: isFormData ? body as FormData : JSON.stringify(body),
            credentials: !url.includes(import.meta.env.REACT_APP_MAIN_MS_HOST) && !noCredentials ? 'include' : undefined
        });
        responseBody = await response.json();

        if (hasStandardResponseType) {
            if (Object.hasOwn(responseBody, 'Success')) {
                responseBody = changeToCamelCase(responseBody);
            }

            if (!response.ok && responseBody.message === undefined) {
                responseBody.message = templateErrorText;
            }
        } else {
            const hasError = Object.hasOwn(responseBody, 'message');
            responseBody = {
                success: !hasError && response.ok,
                data: responseBody as R,
                message: hasError ? responseBody.message : null,
            };
        }

        if (response.status === 401) {
            getLogout();
        }
    } catch (err: unknown) { /* empty */ }

    return { responseBody, status: response.status, headers: response.headers } as TFetchedData<R>;
};

export const getFetchedBlobData = async <T extends object | string | boolean | number | void>({ url, body, headers, method }: TFetchParams<T>) => {
    let response = new Response();

    try {
        response = await fetch(url, {
            method,
            headers: {
                Authorization: `Bearer ${ token() }`,
                ...headers,
            },
            body: JSON.stringify(body),
            credentials: !url.includes(import.meta.env.REACT_APP_MAIN_MS_HOST) ? 'include' : undefined,
        });

        if (response.status === 401) {
            getLogout();
        }
    } catch (err: unknown) { /* empty */ }

    const blobbedData = await response.blob();

    return { responseBody: blobbedData, status: response.status, headers: response.headers } as TFetchedBlobData;
};

export const getZabbixFetchedData = async <R extends object | string | boolean | number | null, T extends object>(
    {
        method,
        params,
        id = 1,
        noAuthToken = false
    }: {
        method: zabbixApiUrlEnum;
        params: T;
        id?: number;
        hasStandardResponseType?: boolean;
        noAuthToken?: boolean
    }) => {
    let response = new Response();
    let responseBody = {} as R;

    const headersRequest: HeadersInit = {
        accept: 'application/json',
        'content-type': 'application/json',
    };

    const zabbixUrl = import.meta.env.REACT_ZABBIX_API_URL ?? '';
    try {
        response = await fetch(zabbixUrl, {
            method: 'POST',
            headers: headersRequest,
            body: JSON.stringify({
                jsonrpc: '2.0',
                method,
                params,
                id,
                auth: !noAuthToken ? import.meta.env.REACT_ZABBIX_API_TOKEN : null,
            }),
        });

        responseBody = await response.json();

        if (response.status === 401) {
            // Handle unauthorized response, e.g., logout
        }
    } catch (err: unknown) { /* empty */ }

    return { responseBody, status: response.status, headers: response.headers };
};