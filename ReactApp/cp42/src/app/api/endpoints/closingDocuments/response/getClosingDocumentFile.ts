export type TGetClosingDocumentFile = {
    fileName: string;
    contentType: string;
    content: string;
}