export enum EInvoiceStatus {
    New,
    CorpError,
    Rejected,
    Accepted,
    Processing,
    Processed,
}

export type TPutClosingDocumentRequest = {
    invoiceStatus: {
        invoiceId: string;
        status: EInvoiceStatus;
    }
};