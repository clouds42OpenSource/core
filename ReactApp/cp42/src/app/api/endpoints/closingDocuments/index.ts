import { EInvoiceStatus, TPutClosingDocumentRequest } from 'app/api/endpoints/closingDocuments/request';
import { TGetClosingDocumentFile } from 'app/api/endpoints/closingDocuments/response';
import { apiHost, getFetchedData } from 'app/api/fetch';
import { WebVerb } from 'core/requestSender/enums';

const putClosingDocument = async (invoiceId: string, status: EInvoiceStatus) => {
    const { responseBody } = await getFetchedData<object, TPutClosingDocumentRequest>({
        url: apiHost('closing-documents/change-status'),
        method: WebVerb.PUT,
        body: {
            invoiceStatus: {
                invoiceId,
                status
            }
        }
    });

    return responseBody;
};

const getClosingDocumentFile = async (actId: string) => {
    const { responseBody } = await getFetchedData<TGetClosingDocumentFile, void>({
        url: apiHost(`closing-documents/file?actId=${ actId }`),
        method: WebVerb.GET,
    });

    if (responseBody && responseBody.success && responseBody.data) {
        const { content, contentType, fileName } = responseBody.data;

        const byteCharacters = atob(content);
        const byteArrays = [];

        for (let offset = 0; offset < byteCharacters.length; offset += 1024) {
            const slice = byteCharacters.slice(offset, offset + 1024);
            const byteNumbers = new Array(slice.length);

            for (let i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }

            byteArrays.push(new Uint8Array(byteNumbers));
        }

        const fileBlob = new Blob(byteArrays, { type: contentType });

        const link = document.createElement('a');
        link.href = URL.createObjectURL(fileBlob);
        link.download = fileName;
        link.click();
    }
};

export const CLOSING_DOCUMENTS = {
    putClosingDocument,
    getClosingDocumentFile
} as const;