import { TGetExtraSessionsResponse } from 'app/api/endpoints/additionalSessions/response';
import { contextAccountId, getFetchedData, msHost } from 'app/api/fetch';
import { personalAdditionalSessionsSlice } from 'app/modules/additionalSessions/store/reducers/PersonalAdditionalSessionsSlice';
import { TAppDispatch } from 'app/redux';
import { changeToCamelCase } from 'app/web/common';
import { WebVerb } from 'core/requestSender/enums';

const { change } = personalAdditionalSessionsSlice.actions;

export const getExtraSession = async (dispatch: TAppDispatch) => {
    const { responseBody: { data, message, success } } = await getFetchedData<TGetExtraSessionsResponse, void>({
        url: msHost(`pl42/hs/Platform/accounts/${ contextAccountId() }/extra-sessions`),
        method: WebVerb.GET,
        hasStandardResponseType: false
    });

    dispatch(change({
        data: changeToCamelCase(data),
        message,
        success
    }));
};

export const ADDITIONAL_SESSIONS_REDUX = {
    getExtraSession
} as const;