export type TUserSessionsItem = {
    login: string;
    userID : string;
    sessions: number;
};

export type TGetExtraSessionsResponse = {
    commonSessions: number;
    userSessions: TUserSessionsItem[];
};