import { apiHost, getFetchedData, toQueryParams } from 'app/api/fetch';
import { WebVerb } from 'core/requestSender/enums';
import { GetChangePasswordSessionRequest, PostNewPasswordRequest, PostResetPasswordLetterRequest } from 'app/api/endpoints/resetPassword/request';
import { getReturnObject } from 'app/api/utility';
import { GetChangePasswordSessionResponse } from 'app/api/endpoints/resetPassword/response';

const resetPasswordUrl = apiHost('');

const postResetPasswordLetter = async (email: PostResetPasswordLetterRequest) => {
    const response = await getFetchedData<boolean, PostResetPasswordLetterRequest>({
        method: WebVerb.POST,
        url: `${ resetPasswordUrl }reset-password-letter`,
        headers: {
            authorization: ''
        },
        body: email
    });

    return getReturnObject(response);
};

const getChangePasswordSession = async (resetCode: GetChangePasswordSessionRequest) => {
    const response = await getFetchedData<GetChangePasswordSessionResponse, GetChangePasswordSessionRequest>({
        method: WebVerb.GET,
        url: `${ resetPasswordUrl }change-password-session${ toQueryParams(resetCode) }`,
        headers: {
            authorization: ''
        },
    });

    return getReturnObject(response);
};

const postNewPassword = async (params: PostNewPasswordRequest) => {
    const response = await getFetchedData({
        method: WebVerb.POST,
        url: `${ resetPasswordUrl }new-password`,
        body: params,
        headers: {
            authorization: ''
        },
    });

    return getReturnObject(response);
};

export const RESET_PASSWORD = {
    postResetPasswordLetter,
    getChangePasswordSession,
    postNewPassword
} as const;