export type GetChangePasswordSessionRequest = {
    resetCode: string;
};