export type PostNewPasswordRequest = {
    password: string;
    controlPassword: string;
    loginToken: string;
    login: string;
};