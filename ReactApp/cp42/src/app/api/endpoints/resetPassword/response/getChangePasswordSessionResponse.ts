export type GetChangePasswordSessionResponse = {
    password: string;
    controlPassword: string;
    loginToken: string;
    login: string;
};