import { IdNameRecord } from '../../apdex/common';

export type getBlockAnalysisDatabasesResponseDto = IdNameRecord[];