type Field = {
    name: string;
    name1C: string;
    value: string;
    valueDecrypted: string;
};

export type LockedObject = {
    tableName: string;
    tableName1C: string;
    fields: Field[];
};

export type Victims = {
    id: string;
    context: string;
    title: string;
    waitTime: number;
    user: string;
    lockedObjects: LockedObject[];
    metadata: string;
    datetime: string;
};

export type LockRecord = {
    title: string;
    context: string;
    datetime: string;
    id: string;
    user: string;
    waitTime: number;
    victimsCount: number;
    victims: Victims[];
};

export type Lock = {
    title: string;
    waitTime: number;
    victimsCount: number;
    lastDate: string;
    records: LockRecord[];
};

export type getBlockAnalysisLockResponseDto = {
    Locks: Lock[];
};