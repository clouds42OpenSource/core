export type getBlockAnalysisLockRequestDto = {
    clientId: string;
    query: {
        fromDate?: string;
        databaseId: string;
        includeVictims?: boolean;
        lockId?: string;
        toDate?: string;
        minLockDuration?: number;
    };
};