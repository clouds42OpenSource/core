import { getFetchedData, toQueryParams } from 'app/api/fetch';
import { encodeToBase64 } from 'app/utils';
import { WebVerb } from 'core/requestSender/enums';
import { getBlockAnalysisDatabasesResponseDto } from './response/getBlockAnalysisDatabasesResponseDto';
import { getBlockAnalysisLockResponseDto } from './response/getBlockAnalysisLock';
import { getBlockAnalysisLockRequestDto } from './request';

const blockAnalysisUrl = import.meta.env.REACT_BLOCK_ANALYSIS_URL;
const basicToken = `Basic ${ encodeToBase64(`${ import.meta.env.REACT_BLOCK_ANALYSIS_LOGIN }:${ import.meta.env.REACT_BLOCK_ANALYSIS_PASSWORD }`) }`;

const params = {
    noAuthorizationToken: true,
    noCredentials: true,
    hasStandardResponseType: false,
    headers: {
        Authorization: basicToken
    }
};

const getBlockAnalysisDatabasesList = async (clientId: string) => {
    const { responseBody } = await getFetchedData<getBlockAnalysisDatabasesResponseDto, void>({
        url: `${ blockAnalysisUrl }/${ clientId }/databases`,
        method: WebVerb.GET,
        ...params
    });

    return responseBody;
};

const getBlockAnalysisLocks = async (requestParam: getBlockAnalysisLockRequestDto) => {
    const { responseBody } = await getFetchedData<getBlockAnalysisLockResponseDto, void>({
        url: `${ blockAnalysisUrl }/${ requestParam.clientId }/locks${ toQueryParams(requestParam.query) }`,
        method: WebVerb.GET,
        ...params
    });

    return responseBody;
};

export const BLOCKANALYSIS = {
    getBlockAnalysisDatabasesList,
    getBlockAnalysisLocks
} as const;