import { navMenuItemInfoMapFrom } from 'app/api/endpoints/global/functions';
import { GetAlertMessages, GetCurrentSessionsSettingsResponse } from 'app/api/endpoints/global/response';
import { GetNavMenu } from 'app/api/endpoints/global/response/getNavMenu';
import { apiHost, contextAccountId, getFetchedData } from 'app/api/fetch';
import { getReturnLightObject } from 'app/api/utility';
import { localStorageHelper } from 'app/common/helpers/LocalStorageHelper';
import { getCurrentSessionSettingsSlice } from 'app/modules/global/reducers/getCurrentSessionSettingsSlice';
import { getAlertMessagesSlice } from 'app/modules/global/reducers/getAlertMessagesSlice';
import { getNavMenuSlice } from 'app/modules/global/reducers/getNavMenuSlice';
import { getUserPermissionsSlice } from 'app/modules/global/reducers/getUserPermissionsSlice';
import { Credentials } from 'app/api/endpoints/global/enum';
import { TAppDispatch } from 'app/redux';
import { WebVerb } from 'core/requestSender/enums';
import { ELogEvent } from './enum';
import { GetUserPermissionsRequest, TLogEventRequest } from './request';

export const sendLogEvent = async (log: ELogEvent, description: string) => {
    const { responseBody } = await getFetchedData<null, TLogEventRequest>({
        url: apiHost('logs/log-event'),
        method: WebVerb.POST,
        body: {
            accountId: contextAccountId(),
            log,
            description
        }
    });

    return getReturnLightObject<null>(responseBody);
};

export const getCurrentSessionSettings = async (dispatch: TAppDispatch) => {
    const { responseBody } = await getFetchedData<GetCurrentSessionsSettingsResponse, void>({
        url: apiHost('current-session-settings'),
        method: WebVerb.GET
    });

    if (responseBody.success && responseBody.data) {
        localStorageHelper.setItem(Credentials.ContextAccountId, responseBody.data.currentContextInfo.contextAccountId);
        localStorageHelper.setItem(Credentials.AccountID, responseBody.data.currentContextInfo.accountId);
        localStorageHelper.setItem(Credentials.UserID, responseBody.data.currentContextInfo.userId);

        dispatch(getCurrentSessionSettingsSlice.actions.success({
            settings: responseBody.data,
            hasSuccessFor: {
                hasSettingsReceived: true
            }
        }));
    }
};

export const getAlertMessages = async (dispatch: TAppDispatch) => {
    const { responseBody } = await getFetchedData<GetAlertMessages[], void>({
        url: apiHost('current-session-settings/temp-data-messages'),
        method: WebVerb.GET
    });

    if (responseBody.success && responseBody.data) {
        dispatch(getAlertMessagesSlice.actions.success(responseBody.data));
    }
};

export const getUserPermissions = async (dispatch: TAppDispatch) => {
    const { responseBody } = await getFetchedData<string[], GetUserPermissionsRequest>({
        url: apiHost(`api_v2/AccountUserPermissions/${ localStorageHelper.getUserId() }/GetPermissionsForContext`),
        method: WebVerb.POST,
        body: {
            Context: {
                AccountId: localStorageHelper.getContextAccountId() ?? '',
                UserAccountId: localStorageHelper.getUserId() ?? ''
            }
        },
        hasStandardResponseType: false
    });

    if (responseBody.success && responseBody.data) {
        dispatch(getUserPermissionsSlice.actions.success(responseBody.data));
    }
};

export const getNavMenu = async (dispatch: TAppDispatch) => {
    dispatch(getNavMenuSlice.actions.loading());

    const { responseBody } = await getFetchedData<GetNavMenu, void>({
        url: apiHost('navigation'),
        method: WebVerb.GET
    });

    if (responseBody.success && responseBody.data && responseBody.data.topSideMenu) {
        dispatch(getNavMenuSlice.actions.success({
            leftSideMenu: responseBody.data.leftSideMenu.map(item => navMenuItemInfoMapFrom(item)),
            topSideMenu: {
                balanceMenu: responseBody.data.topSideMenu.balanceMenu ? navMenuItemInfoMapFrom(responseBody.data.topSideMenu.balanceMenu) : null,
                greetingsMenu: navMenuItemInfoMapFrom(responseBody.data.topSideMenu.greetingsMenu),
                exitMenu: navMenuItemInfoMapFrom(responseBody.data.topSideMenu.exitMenu)
            },
            isOpenNavMobile: false
        }));
    } else {
        dispatch(getNavMenuSlice.actions.error(responseBody.message ?? ''));
    }
};

export const GLOBAL = {
    sendLogEvent
} as const;

export const GLOBAL_REDUX = {
    getCurrentSessionSettings,
    getAlertMessages,
    getUserPermissions,
    getNavMenu
} as const;