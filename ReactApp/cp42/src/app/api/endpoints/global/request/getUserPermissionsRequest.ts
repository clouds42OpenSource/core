export type GetUserPermissionsRequest = {
    Context: {
        AccountId: string;
        UserAccountId: string;
    }
}