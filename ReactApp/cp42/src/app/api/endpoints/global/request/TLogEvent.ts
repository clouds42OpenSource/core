import { ELogEvent } from '../enum';

export type TLogEventRequest = {
    accountId: string;
    log: ELogEvent;
    description: string;
};