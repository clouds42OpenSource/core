import { NavMenuItem, NavMenuItemInfo } from 'app/api/endpoints/global/response/getNavMenu';

/**
 * Mapping модели элемента NavMenu
 * @param value Модель элемента NavMenu
 */
export function navMenuItemMapFrom(value: NavMenuItem): NavMenuItem {
    return {
        ...value,
        isActive: false,
        isExpanded: false,
        key: value.caption
    };
}

/**
 * Mapping модели элемента NavMenu
 * @param value Модель элемента NavMenu
 */
export function navMenuItemInfoMapFrom(value: NavMenuItemInfo): NavMenuItemInfo {
    return {
        ...value,
        startMenu: navMenuItemMapFrom(value.startMenu),
        children: value.children.map(item => navMenuItemMapFrom(item))
    };
}