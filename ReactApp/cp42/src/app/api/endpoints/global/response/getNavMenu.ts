export type NavMenuItem = {
    key: string;
    faIcon: string;
    caption: string;
    href: string;
    isExpanded: boolean;
    isActive: boolean;
};

export type NavMenuItemInfo = {
    categoryKey: string;
    startMenu: NavMenuItem;
    children: NavMenuItem[];
};

export type GetNavMenu = {
    leftSideMenu: NavMenuItemInfo[];
    topSideMenu: {
        balanceMenu: NavMenuItemInfo | null;
        greetingsMenu: NavMenuItemInfo;
        exitMenu: NavMenuItemInfo;
    } | null;
    isOpenNavMobile: boolean;
};