export type GetAlertMessages = {
    Text: string;
    Type: string;
}

export type GetAlertMessagesData = {
    text: string;
    type: string;
}

export type AlertMessage = {
    alert: GetAlertMessagesData[];
    hasSuccessFor: {
        hasAlertsReceived: boolean;
    };
}

export enum AlertType {
    error = 'error',
    success = 'success',
    info = 'info',
    warning = 'warning'
}