import { AccountUserGroup } from 'app/common/enums';

export type TLocale = 'ru-ru' | 'ru-kz' | 'ru-ua'

export type CurrentContextInfo = {
    isDifferentContextAccount: boolean;
    contextAccountName: string;
    contextAccountIndex: number;
    currentUserGroups: AccountUserGroup[];
    isEmailVerified: boolean | null;
    isEmailExists: boolean;
    isPhoneVerified: boolean | null;
    isPhoneExists: boolean;
    userId: string;
    accountId: string;
    contextAccountId: string;
    isNew: boolean;
    locale: TLocale;
    userSource: string;
    showSupportMessage: boolean;
};

export type GetCurrentSessionsSettingsResponse = {
    currentContextInfo: CurrentContextInfo
}

export type GetCurrentSessionSettings = {
    settings: {
        currentContextInfo: CurrentContextInfo;
    };
    hasSuccessFor: {
        hasSettingsReceived: boolean;
    }
};