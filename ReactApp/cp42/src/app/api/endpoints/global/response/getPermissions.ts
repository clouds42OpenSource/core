export type GetPermissions = {
    permissions: string[];
    hasSuccessFor: {
        permissionsReceived: boolean;
    };
}