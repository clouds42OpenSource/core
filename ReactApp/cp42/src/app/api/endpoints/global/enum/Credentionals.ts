export enum Credentials {
    ContextAccountId = 'CtxtGUID',
    UserID = 'UsrID',
    AccountID = 'AccGUID',
    JsonWebToken = 'AccessToken',
    RefreshToken= 'RefreshToken',
    ExpiresIn = 'ExpiresIn'
}