export enum ELogEvent {
    addAccessToInfoBase = 18,
    removeAccessToInfoBase = 19,
    uploadDt = 43,
    InstallServiceInDatabase = 90,
    CreatePrivateService = 111,
    DeletePrivateService = 112,
    requestSent = 150,
}