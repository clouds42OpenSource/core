import { AccountUserGroup } from 'app/common/enums';

export type putProfileRequestDto = {
    id: string;
    accountId: string;
    isActivated: boolean;
    login: string;
    roles: AccountUserGroup[];
    lastName: string;
    firstName: string;
    midName: string;
    email: string;
    password: string;
    confirmPassword: string;
    phoneNumber: string;
    middleName?: string;
};