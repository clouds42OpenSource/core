export type getProfileRequestDto = {
    'filter.accountId': string;
    'filter.id': string;
}