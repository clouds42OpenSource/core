import { getMyDiskResponseDto } from 'app/api/endpoints/myDisk/response';
import { TNotificationSettingsItem } from 'app/api/endpoints/notification/request/postNotificationSettings';
import { AccountUserGroup } from 'app/common/enums';

export enum RentType {
    None,
    Standart,
    Web
}

export type ProfileItemResponseDto = {
    id: string;
    login: string;
    firstName: string;
    lastName: string;
    fullName: string;
    email: string;
    phoneNumber: string;
    activated: boolean;
    createdInAd: boolean;
    unsubscibed: boolean;
    creationDate: string;
    accountRoles: AccountUserGroup[];
    middleName: string | null;
    notificationSettings: TNotificationSettingsItem[];
    rentType: RentType;
    myDiskInfo: getMyDiskResponseDto;
    accountIndexNumber: number;
};