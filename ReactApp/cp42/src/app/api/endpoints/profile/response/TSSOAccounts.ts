type TAccounts = {
    id: string;
    description: string;
    provider: 'tg' | 'google';
};

export type TSSOAccountsResponse = {
    accounts: TAccounts[];
};