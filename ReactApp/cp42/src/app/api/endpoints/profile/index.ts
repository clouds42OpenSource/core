import { apiHost, getFetchedData, msHost, userId } from 'app/api/fetch';
import { TDataReturn, TResponseLowerCase } from 'app/api/types';
import { useFetch } from 'app/hooks';
import { WebVerb } from 'core/requestSender/enums';
import { ProfileItemResponseDto, TSSOAccountsResponse } from './response';
import { putProfileRequestDto } from './request';

const profileUlr = apiHost('account-users');

const useGetProfile = (): TDataReturn<ProfileItemResponseDto> => {
    const { data, error, isLoading, refreshData } = useFetch<ProfileItemResponseDto, void>({
        url: `${ profileUlr }/${ userId() }`,
        method: WebVerb.GET
    });

    return { data, error, isLoading, refreshData };
};

const putProfile = async (requestParam: putProfileRequestDto): Promise<TResponseLowerCase<object>> => {
    requestParam.middleName = requestParam.midName;

    const { responseBody } = await getFetchedData<TResponseLowerCase<object>, putProfileRequestDto>({
        url: profileUlr,
        method: WebVerb.PUT,
        body: requestParam
    });

    return {
        success: responseBody.success,
        data: responseBody.data?.data ?? {},
        message: responseBody.message
    };
};

const useGetSsoAccounts = (id?: string) => {
    return useFetch<TSSOAccountsResponse, void>({
        url: msHost(`pl42/hs/Platform/users/${ id }/sso-accounts`),
        method: WebVerb.GET,
        isRequestBlocked: !id,
        hasStandardResponseType: false
    });
};

const deleteSsoAccount = async (id: string, ssoId: string) => {
    const { responseBody } = await getFetchedData<null, void>({
        url: msHost(`pl42/hs/Platform/users/${ id }/sso-accounts/${ ssoId }`),
        method: WebVerb.DELETE,
        hasStandardResponseType: false
    });

    return responseBody;
};

export const PROFILE = {
    useGetProfile,
    putProfile,
    useGetSsoAccounts,
    deleteSsoAccount
} as const;