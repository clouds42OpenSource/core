import { apiHost, getFetchedData } from 'app/api/fetch';
import { ArrServerDataModel } from 'app/web/api/SegmentsProxy/TabArrServers/response-dto';
import { WebVerb } from 'core/requestSender/enums';

const autoUpdateNodesUrl = apiHost('auto-update-nodes');

const putAutoUpdateNodes = async (node: ArrServerDataModel) => {
    const { responseBody } = await getFetchedData({
        url: autoUpdateNodesUrl,
        method: WebVerb.PUT,
        body: {
            ...node,
            isBusy: false
        }
    });

    return responseBody;
};

export const AUTO_UPDATE_NODES = {
    putAutoUpdateNodes
} as const;