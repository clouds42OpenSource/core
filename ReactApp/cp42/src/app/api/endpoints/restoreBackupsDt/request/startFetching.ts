export type StartFetching = {
    backupId: string;
    uploadedFileId: string;
};