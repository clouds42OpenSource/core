export enum RestoreType {
    ToCurrent = 0,
    ToNow = 1
}

export type RestoreRequest = {
    accountDatabaseBackupId: string;
    uploadedFileId: string;
    accountDatabaseName: string;
    needUsePromisePayment: boolean;
    restoreType: RestoreType;
    usersIdForAddAccess: string[];
    myDatabasesServiceTypeId: string;
};