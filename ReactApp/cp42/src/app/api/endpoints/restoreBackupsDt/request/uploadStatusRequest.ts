export type UploadStatusRequest = {
    uploadedFileId: string;
}