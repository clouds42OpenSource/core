export type GetDatabaseBackupsResponse = {
    accountDatabaseBackupId: string;
    uploadedFileId: string;
    accountDatabaseName: string;
    uploadFilesUri: string;
    currencyName: string;
    isDbOnDelimiters: string;
    emailAddresses: string[];
    myDatabasesServiceTypeId: string;
};