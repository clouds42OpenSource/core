export enum ResourceForRedirect {
    DiskSpace = 4,
    CountOfDatabasesOverLimit = 9,
    ServerDatabasePlacement = 10,
    AccessToServerDatabase = 11,
    AccessToConfiguration1C = 12
}

export type RestoreResponse = {
    isComplete: boolean;
    notEnoughMoney: number;
    amount: number;
    currency: string;
    canUsePromisePayment: boolean;
    errorMessage: string;
    resourceForRedirect: ResourceForRedirect;
};