export type UploadStatusResponse = {
    uploadedFileStatus: number,
    statusComment: string
};