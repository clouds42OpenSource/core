import { GetDatabaseBackupsRequset, RestoreRequest, StartFetching, UploadStatusRequest } from 'app/api/endpoints/restoreBackupsDt/request';
import { GetDatabaseBackupsResponse, RestoreResponse, UploadStatusResponse } from 'app/api/endpoints/restoreBackupsDt/response';
import { apiHost, getFetchedData } from 'app/api/fetch';
import { TResponseLowerCase } from 'app/api/types';
import { WebVerb } from 'core/requestSender/enums';

const databaseBackupUrl = apiHost('database-backup');

const getDatabaseBackup = async (requestParam: GetDatabaseBackupsRequset): Promise<TResponseLowerCase<GetDatabaseBackupsResponse>> => {
    const { responseBody } = await getFetchedData<GetDatabaseBackupsResponse, GetDatabaseBackupsRequset>({
        url: `${ databaseBackupUrl }/${ requestParam.id }`,
        method: WebVerb.GET
    });

    return {
        success: responseBody.success,
        data: responseBody.data ?? null,
        message: responseBody.message
    };
};

const putStartFetching = async (requestParam: StartFetching) => {
    const { responseBody } = await getFetchedData<TResponseLowerCase<null>, StartFetching>({
        url: `${ databaseBackupUrl }/${ requestParam.backupId }/start-fetching?uploadedFileId=${ requestParam.uploadedFileId }`,
        method: WebVerb.PUT
    });

    return responseBody;
};

const postRestore = async (requestParam: RestoreRequest): Promise<TResponseLowerCase<RestoreResponse>> => {
    const { responseBody } = await getFetchedData<RestoreResponse, RestoreRequest>({
        url: `${ databaseBackupUrl }/restore`,
        method: WebVerb.POST,
        body: requestParam
    });

    return responseBody;
};

const getUploadStatus = async (requestParam: UploadStatusRequest) => {
    const { responseBody } = await getFetchedData<UploadStatusResponse, UploadStatusResponse>({
        url: `${ databaseBackupUrl }/upload-status?uploadedFileId=${ requestParam.uploadedFileId }`,
        method: WebVerb.GET
    });

    return responseBody;
};

export const RESTORE_DATABASE_BACKUP = {
    getDatabaseBackup,
    putStartFetching,
    postRestore,
    getUploadStatus
} as const;