import { EAgentPaymentSourceType, EAgentRequisitesStatus, EPaymentType } from 'app/api/endpoints/partners/enum';

import {
    TAgencyAgreement,
    TAgentCashOutChangeStatusRequest,
    TAgentCashOutDeleteRequest,
    TAgentCashOutEditRequest,
    TAgentCashOutRequest,
    TAgentPaymentListRequest,
    TAgentPaymentRequest,
    TAgentReportFileCreateRequest,
    TAgentReportFileRequest,
    TAgentRequisiteDeleteRequest,
    TAgentsRequisitesListRequest,
    TBankDetailsRequest,
    TBindAccountsRequest,
    TCashOutToBalanceRequest,
    TClientRequest,
    TClientsListRequest,
    TDownloadAgentReportFileRequest,
    TGetAgentRequisiteRequest, TGetSheetIdRequest,
    TInnDetailsRequest,
    TRecalculationServiceCostRequest,
    TServicesListRequest,
    TSummaryDataRequest
} from 'app/api/endpoints/partners/request';
import {
    TAgentCashOutEditResponse,
    TAgentCashOutResponse,
    TAgentPaymentListResponse,
    TAgentRequisite,
    TAgentRequisiteResponse,
    TAgentRequisitesListResponse,
    TBankDetailsResponse,
    TClientListResponse,
    TLegalEntityDetailsResponse,
    TRecalculationServiceCostResponse,
    TSearchAccountsResponse,
    TSearchServicesResponse,
    TServicesListResponse,
    TSoleProprietorDetailsResponse,
    TSummaryDataResponse
} from 'app/api/endpoints/partners/response';
import { apiHost, contextAccountId, getFetchedBlobData, getFetchedData, templateErrorText, toQueryParams, userId } from 'app/api/fetch';
import { TDataReturn } from 'app/api/types';
import { getReturnObject } from 'app/api/utility';
import {
    getAgencyAgreementStatusSlice,
    getAgentCashOutRequestListSlice,
    getAgentPaymentListSlice,
    getAgentRequisitesListSlice,
    getClientListSlice,
    getRecalculationServiceCostListSlice,
    getServicesListSlice,
    getSummaryDataSlice
} from 'app/modules/partners/reducers';
import { TAppDispatch } from 'app/redux';
import { changeToCamelCase, objectToFormData } from 'app/web/common';
import { WebVerb } from 'core/requestSender/enums';

const getLegalEntityDetails = async (query: TInnDetailsRequest) => {
    const params = await getFetchedData<TLegalEntityDetailsResponse, void>({
        url: apiHost(`partners/legal-entity-details${ toQueryParams(query) }`),
        method: WebVerb.GET
    });

    return getReturnObject(params);
};

const acceptAgencyAgreement = async (): Promise<TDataReturn<null>> => {
    const params = await getFetchedData<null, string>({
        url: apiHost('partners/agency-agreement/accept'),
        method: WebVerb.POST,
        body: contextAccountId()
    });

    return getReturnObject(params);
};

const searchAccounts = async (searchLine: string) => {
    const params = await getFetchedData<TSearchAccountsResponse[], void>({
        url: apiHost(`partners/account${ toQueryParams({ searchLine }) }`),
        method: WebVerb.GET
    });

    return getReturnObject(params);
};

const searchServices = async (searchLine: string) => {
    const params = await getFetchedData<TSearchServicesResponse[], void>({
        url: apiHost(`partners/services-by-name${ toQueryParams({ searchLine }) }`),
        method: WebVerb.GET
    });

    return getReturnObject(params);
};

const bindAccounts = async (data: TBindAccountsRequest) => {
    const params = await getFetchedData<boolean, TBindAccountsRequest>({
        url: apiHost('partners/bind-account'),
        method: WebVerb.POST,
        body: changeToCamelCase(data, true)
    });

    return getReturnObject(params);
};

const unbindAccounts = async (id: string) => {
    const params = await getFetchedData<boolean, string>({
        url: apiHost('partners/unbind-account'),
        method: WebVerb.POST,
        body: id
    });

    return getReturnObject(params);
};

const deleteAgentRequisite = async (query: TAgentRequisiteDeleteRequest) => {
    const params = await getFetchedData<null, void>({
        url: apiHost(`partners/agent-requisite${ toQueryParams(query) }`),
        method: WebVerb.DELETE
    });

    return getReturnObject(params);
};

const createAgentRequisite = async (body: FormData, method: WebVerb.PUT | WebVerb.POST) => {
    const params = await getFetchedData<null, typeof body>({
        url: apiHost('partners/agent-requisite'),
        method,
        body,
        isFormData: true
    });

    return getReturnObject(params);
};

const createAgentCashOut = async (body: FormData, method: WebVerb.PUT | WebVerb.POST) => {
    const params = await getFetchedData<null, typeof body>({
        url: apiHost('partners/agent-cash-out-request'),
        method,
        body,
        isFormData: true
    });

    return getReturnObject(params);
};

const deleteAgentCashOut = async (query: TAgentCashOutDeleteRequest) => {
    const params = await getFetchedData<null, void>({
        url: apiHost(`partners/agent-cash-out-request${ toQueryParams(query) }`),
        method: WebVerb.DELETE,
    });

    return getReturnObject(params);
};

const getAgentCashOutRequest = async (query: TAgentCashOutEditRequest) => {
    const params = await getFetchedData<TAgentCashOutEditResponse, void>({
        url: apiHost(`partners/agent-cash-out-request${ toQueryParams(query) }`),
        method: WebVerb.GET,
    });

    return getReturnObject(params);
};

const getAgentReportFile = (query: TAgentReportFileRequest) => {
    return getFetchedBlobData<void>({
        url: apiHost(`partners/agent-report${ toQueryParams(query) }`),
        method: WebVerb.GET,
    });
};

const createAgentReportFile = (query: TAgentReportFileCreateRequest) => {
    return getFetchedBlobData<void>({
        url: apiHost(`partners/agent-report-print${ toQueryParams(query) }`),
        method: WebVerb.GET,
    });
};

const downloadAgentReportFile = (query: TDownloadAgentReportFileRequest) => {
    return getFetchedBlobData<void>({
        url: apiHost(`partners/agent-document-file${ toQueryParams(query) }`),
        method: WebVerb.GET,
    });
};

const changeStatusAgentCashOut = async (body: TAgentCashOutChangeStatusRequest) => {
    const params = await getFetchedData<null, TAgentCashOutChangeStatusRequest>({
        url: apiHost('partners/agent-cash-out-request/status'),
        method: WebVerb.PUT,
        body
    });

    return getReturnObject(params);
};

const changeAgentRequisiteStatus = async (status: EAgentRequisitesStatus, id: string) => {
    const params = await getFetchedData<null, FormData>({
        url: apiHost('partners/agent-requisite-status'),
        method: WebVerb.PUT,
        body: objectToFormData(changeToCamelCase({
            agentRequisitesStatus: status,
            id,
            accountId: contextAccountId()
        }, true)),
        isFormData: true
    });

    return getReturnObject(params);
};

const getAgentRequisite = async (query: TGetAgentRequisiteRequest) => {
    const params = await getFetchedData<TAgentRequisiteResponse, void>({
        url: apiHost(`partners/agent-requisite${ toQueryParams(query) }`),
        method: WebVerb.GET,
    });

    return getReturnObject(params);
};

const getSoleProprietorDetails = async (query: TInnDetailsRequest) => {
    const params = await getFetchedData<TSoleProprietorDetailsResponse, void>({
        url: apiHost(`partners/sole-proprietor-details${ toQueryParams(query) }`),
        method: WebVerb.GET
    });

    return getReturnObject(params);
};

const getAgentContracts = async () => {
    const query = {
        accountId: contextAccountId()
    };
    const params = await getFetchedData<TAgentRequisite[], void>({
        url: apiHost(`partners/agent-requisite/active-contracts${ toQueryParams(query) }`),
        method: WebVerb.GET
    });

    return getReturnObject(params);
};

const checkCashOutToBalance = async () => {
    const query = {
        accountId: contextAccountId()
    };
    const params = await getFetchedData<boolean, void>({
        url: apiHost(`partners/cash-out-to-balance/available${ toQueryParams(query) }`),
        method: WebVerb.GET
    });

    return getReturnObject(params);
};

const checkAgentCashOut = async () => {
    const query = {
        accountId: contextAccountId()
    };
    const params = await getFetchedData<boolean, void>({
        url: apiHost(`partners/agent-cash-out-request/available${ toQueryParams(query) }`),
        method: WebVerb.GET
    });

    return getReturnObject(params);
};

const getCashOutBalance = async () => {
    const query = {
        accountId: contextAccountId()
    };

    const params = await getFetchedData<number, void>({
        url: apiHost(`partners/cash-out-to-balance/available-sum${ toQueryParams(query) }`),
        method: WebVerb.GET
    });

    return getReturnObject(params);
};

const createCashOutToBalance = async (sum: number) => {
    const body: TCashOutToBalanceRequest = {
        sum,
        fromAccountId: contextAccountId(),
        toAccountId: contextAccountId(),
        initiatorId: userId(),
        comment: ''
    };
    const params = await getFetchedData<null, TCashOutToBalanceRequest>({
        url: apiHost('partners/cash-out-to-balance'),
        method: WebVerb.POST,
        body: changeToCamelCase(body, true)
    });

    return getReturnObject(params);
};

const getBankDetails = async (query: TBankDetailsRequest) => {
    const params = await getFetchedData<TBankDetailsResponse, void>({
        url: apiHost(`partners/bank-details${ toQueryParams(query) }`),
        method: WebVerb.GET
    });

    return getReturnObject(params);
};

const createAgentPayment = async (query: TAgentPaymentRequest) => {
    if (!query.agentAccountId) {
        query.agentAccountId = contextAccountId();
    }

    query.agentPaymentSourceType = EAgentPaymentSourceType.manualInput;
    query.paymentType = EPaymentType.inflow;

    const params = await getFetchedData<null, void>({
        url: apiHost(`partners/agent-payment${ toQueryParams(query) }`),
        method: WebVerb.GET,
    });

    return getReturnObject(params);
};

const getAgentCashOut = (filterParams: TAgentCashOutRequest, dispatch: TAppDispatch) => {
    dispatch(getAgentCashOutRequestListSlice.actions.loading());

    if (!filterParams.pageSize) {
        filterParams.pageSize = 50;
    }

    getFetchedData<TAgentCashOutResponse, void>({
        url: apiHost(`partners/agent-cash-out-requests${ toQueryParams(filterParams) }`),
        method: WebVerb.GET,
    }).then(({ responseBody, status, headers }) => {
        dispatch(getAgentCashOutRequestListSlice.actions.empty());

        if (responseBody.data && responseBody.success && !responseBody.message) {
            dispatch(getAgentCashOutRequestListSlice.actions.success({
                rawData: responseBody.data,
                status,
                headers
            }));
        } else {
            dispatch(getAgentCashOutRequestListSlice.actions.error(responseBody.message ?? templateErrorText));
        }
    });
};

const getAgentPayments = (filterParams: TAgentPaymentListRequest, dispatch: TAppDispatch) => {
    dispatch(getAgentPaymentListSlice.actions.loading());

    if (!filterParams.pageSize) {
        filterParams.pageSize = 50;
    }

    getFetchedData<TAgentPaymentListResponse, void>({
        url: apiHost(`partners/agent-payments${ toQueryParams(filterParams) }`),
        method: WebVerb.GET,
    }).then(({ responseBody, status, headers }) => {
        dispatch(getAgentPaymentListSlice.actions.empty());

        if (responseBody.data && responseBody.success && !responseBody.message) {
            dispatch(getAgentPaymentListSlice.actions.success({
                rawData: responseBody.data,
                status,
                headers
            }));
        } else {
            dispatch(getAgentPaymentListSlice.actions.error(responseBody.message ?? templateErrorText));
        }
    });
};

const getAgentRequisites = (filterParams: TAgentsRequisitesListRequest, dispatch: TAppDispatch) => {
    dispatch(getAgentRequisitesListSlice.actions.loading());

    if (!filterParams.pageSize) {
        filterParams.pageSize = 50;
    }

    getFetchedData<TAgentRequisitesListResponse, void>({
        url: apiHost(`partners/agent-requisites${ toQueryParams(filterParams) }`),
        method: WebVerb.GET,
    }).then(({ responseBody, status, headers }) => {
        dispatch(getAgentRequisitesListSlice.actions.empty());

        if (responseBody.data && responseBody.success && !responseBody.message) {
            dispatch(getAgentRequisitesListSlice.actions.success({
                rawData: responseBody.data,
                status,
                headers
            }));
        } else {
            dispatch(getAgentRequisitesListSlice.actions.error(responseBody.message ?? templateErrorText));
        }
    });
};

const getServices = (filterParams: TServicesListRequest, dispatch: TAppDispatch) => {
    dispatch(getServicesListSlice.actions.loading());

    if (!filterParams.pageSize) {
        filterParams.pageSize = 50;
    }

    getFetchedData<TServicesListResponse, void>({
        url: apiHost(`partners/services${ toQueryParams(filterParams) }`),
        method: WebVerb.GET,
    }).then(({ responseBody, status, headers }) => {
        dispatch(getServicesListSlice.actions.empty());

        if (responseBody.data && responseBody.success && !responseBody.message) {
            dispatch(getServicesListSlice.actions.success({
                rawData: responseBody.data,
                status,
                headers
            }));
        } else {
            dispatch(getServicesListSlice.actions.error(responseBody.message ?? templateErrorText));
        }
    });
};

const getRecalculationServiceCosts = (filterParams: TRecalculationServiceCostRequest, dispatch: TAppDispatch) => {
    dispatch(getRecalculationServiceCostListSlice.actions.loading());

    if (!filterParams.pageSize) {
        filterParams.pageSize = 50;
    }

    getFetchedData<TRecalculationServiceCostResponse, void>({
        url: apiHost(`partners/recalculation-service-cost${ toQueryParams(filterParams) }`),
        method: WebVerb.GET
    }).then(({ responseBody, status, headers }) => {
        dispatch(getRecalculationServiceCostListSlice.actions.empty());

        if (responseBody.data && responseBody.success && !responseBody.message) {
            dispatch(getRecalculationServiceCostListSlice.actions.success({
                rawData: responseBody.data,
                status,
                headers
            }));
        } else {
            dispatch(getRecalculationServiceCostListSlice.actions.error(responseBody.message ?? templateErrorText));
        }
    });
};

const getClients = (filterParams: TClientsListRequest, dispatch: TAppDispatch) => {
    dispatch(getClientListSlice.actions.loading());

    if (!filterParams.pageSize) {
        filterParams.pageSize = 50;
    }

    getFetchedData<TClientListResponse, void>({
        url: apiHost(`partners/clients${ toQueryParams(filterParams) }${ filterParams.serviceIsActiveForClient === false ? '&serviceIsActiveForClient=false' : '' }`),
        method: WebVerb.GET,
    }).then(({ responseBody, status, headers }) => {
        dispatch(getClientListSlice.actions.empty());

        if (responseBody.data && responseBody.success && !responseBody.message) {
            dispatch(getClientListSlice.actions.success({
                rawData: responseBody.data,
                status,
                headers
            }));
        } else {
            dispatch(getClientListSlice.actions.error(responseBody.message ?? templateErrorText));
        }
    });
};

const createClient = async (phoneNumber: string, email: string) => {
    const data = changeToCamelCase({ phoneNumber, email, referralAccountId: contextAccountId() }, true);
    const params = await getFetchedData<null, TClientRequest>({
        url: apiHost('partners/client'),
        method: WebVerb.POST,
        body: data
    });

    return getReturnObject(params);
};

const getMonthlyCharge = async () => {
    const { responseBody } = await getFetchedData<number, void>({
        url: apiHost(`partners/monthly-charge?accountId=${ contextAccountId() }`),
        method: WebVerb.GET
    });

    return responseBody;
};

const getSummaryData = async (dispatch: TAppDispatch) => {
    dispatch(getSummaryDataSlice.actions.loading());

    const { responseBody, status, headers } = await getFetchedData<TSummaryDataResponse, void>({
        url: apiHost(`partners/summary-data${ toQueryParams<TSummaryDataRequest>({ accountId: contextAccountId() }) }`),
        method: WebVerb.GET
    });
    const monthlyCharge = await getMonthlyCharge();

    if (responseBody.data && responseBody.success && !responseBody.message) {
        dispatch(getSummaryDataSlice.actions.success({
            rawData: responseBody.data,
            status,
            headers
        }));
    } else {
        dispatch(getSummaryDataSlice.actions.error(responseBody.message ?? templateErrorText));
    }

    if (monthlyCharge.success && monthlyCharge.data && monthlyCharge.data >= 0) {
        dispatch(getSummaryDataSlice.actions.setMonthlyCharge(monthlyCharge.data));
    }
};

const getAgencyAgreementStatus = (dispatch: TAppDispatch) => {
    dispatch(getAgencyAgreementStatusSlice.actions.loading());

    const query: TAgencyAgreement = {
        accountId: contextAccountId()
    };

    getFetchedData<boolean, void>({
        url: apiHost(`partners/agency-agreement/accepted${ toQueryParams(query) }`),
        method: WebVerb.GET
    }).then(({ responseBody }) => {
        if (responseBody.success && !responseBody.message) {
            dispatch(getAgencyAgreementStatusSlice.actions.success({
                rawData: !!responseBody.data
            }));
        } else {
            dispatch(getAgencyAgreementStatusSlice.actions.error(responseBody.message ?? templateErrorText));
        }
    });
};

const getAgentRequisitesFile = async (id: string) => {
    const { responseBody } = await getFetchedBlobData({
        url: `${ apiHost('partners/agent-document-file?agentDocumentFileId=') }${ id }`,
        method: WebVerb.GET
    });

    return responseBody;
};

const getSheetId = async (params: TGetSheetIdRequest) => {
    const { responseBody } = await getFetchedData<string, void>({
        url: apiHost(`sheets/get-or-create-partner-clients-sheet${ toQueryParams(params) }`),
        method: WebVerb.POST,
    });

    return responseBody;
};

export const PARTNERS = {
    createClient,
    createAgentPayment,
    getLegalEntityDetails,
    getSoleProprietorDetails,
    getBankDetails,
    createAgentRequisite,
    deleteAgentRequisite,
    acceptAgencyAgreement,
    getAgentRequisite,
    checkCashOutToBalance,
    getCashOutBalance,
    createCashOutToBalance,
    changeAgentRequisiteStatus,
    checkAgentCashOut,
    createAgentCashOut,
    getAgentContracts,
    deleteAgentCashOut,
    changeStatusAgentCashOut,
    getAgentReportFile,
    createAgentReportFile,
    getAgentCashOutRequest,
    searchAccounts,
    bindAccounts,
    searchServices,
    downloadAgentReportFile,
    unbindAccounts,
    getAgentRequisitesFile,
    getSheetId
};

export const PARTNERS_REDUX = {
    getSummaryData,
    getClients,
    getServices,
    getAgentRequisites,
    getAgentPayments,
    getAgentCashOut,
    getAgencyAgreementStatus,
    getRecalculationServiceCosts
};