import { EAgentRequisiteFileType } from 'app/api/endpoints/partners/enum';

export type TAgentRequisiteFile = {
    cloudFileId?: string;
    fileName: string;
    content?: File | string;
    contentType: string;
    base64?: string;
    agentRequisitesFileType: EAgentRequisiteFileType;
};

export type TPartialAgentRequisiteFile = Partial<TAgentRequisiteFile>;

export type TLegalPersonRequisites = {
    organizationName: string;
    headFullName: string;
    headPosition: string;
    legalAddress: string;
    phoneNumber: string;
    ogrn: string;
    kpp: string;
    inn: string;
    settlementAccount: string;
    bik: string;
    bankName: string;
    correspondentAccount: string;
    addressForSendingDocuments: string;
    files: TAgentRequisiteFile[];
};

export type TPartialLegalPersonRequisites = Omit<Partial<TLegalPersonRequisites>, 'files'> & {
    files: TPartialAgentRequisiteFile[];
};

export type TPhysicalPersonRequisites = {
    fullName: string;
    dateOfBirth: string;
    passportSeries: string;
    passportNumber: string;
    whomIssuedPassport: string;
    passportDateOfIssue: string;
    registrationAddress: string;
    inn: string;
    snils: string;
    settlementAccount: string;
    bik: string;
    bankName: string;
    correspondentAccount: string;
    addressForSendingDocuments: string;
    files: TAgentRequisiteFile[];
};

export type TPartialPhysicalPersonRequisites = Omit<Partial<TPhysicalPersonRequisites>, 'files'> & {
    files: TPartialAgentRequisiteFile[];
};

export type TSoleProprietorRequisites = {
    fullName: string;
    legalAddress: string;
    phoneNumber: string;
    ogrn: string;
    inn: string;
    settlementAccount: string;
    bik: string;
    bankName: string;
    correspondentAccount: string;
    addressForSendingDocuments: string;
    files: TAgentRequisiteFile[];
};

export type TPartialSoleProprietorRequisites = Omit<Partial<TSoleProprietorRequisites>, 'files'> & {
    files: TPartialAgentRequisiteFile[];
};