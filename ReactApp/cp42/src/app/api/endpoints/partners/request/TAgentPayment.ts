import { EAgentPaymentSourceType, EPaymentType } from 'app/api/endpoints/partners/enum';

export type TAgentPaymentRequest = {
    agentAccountId?: string;
    sum: number;
    date: string;
    comment?: string;
    accountNumber: number;
    clientPaymentSum: number;
    agentPaymentSourceType?: EAgentPaymentSourceType;
    paymentType?: EPaymentType;
};