import { EAgentRequisitesStatus, EAgentRequisitesType } from 'app/api/endpoints/partners/enum';
import { SortingKind } from 'app/common/enums';

export type TAgentsRequisitesListRequest = {
    partnerAccountData?: string;
    agentRequisitesStatus?: EAgentRequisitesStatus;
    agentRequisitesType?: EAgentRequisitesType;
    number?: string;
    sortFieldName?: string;
    sortType?: SortingKind;
    pageNumber?: number;
    pageSize?: number;
    periodFrom?: string;
    periodTo?: string;
    accountId?: string;
    accountData?: string;
};