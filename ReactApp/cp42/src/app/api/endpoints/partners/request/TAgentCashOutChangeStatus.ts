import { ERequestStatus } from 'app/api/endpoints/partners/enum';

export type TAgentCashOutChangeStatusRequest = {
    requestNumber: string;
    sum: number;
    status: ERequestStatus;
};