import { ERecalculationTaskStatus } from 'app/api/endpoints/partners/enum';
import { SortingKind } from 'app/common/enums';

export type TRecalculationServiceCostRequest = {
    periodCreationDateFrom?: string;
    periodCreationDateTo?: string;
    periodRecalculationDateFrom?: string;
    periodRecalculationDateTo?: string;
    serviceId?: string;
    queueStatus?: ERecalculationTaskStatus;
    pageNumber?: number;
    pageSize?: number;
    sortFieldName?: string;
    sortType?: SortingKind;
};