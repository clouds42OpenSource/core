export type TAgentReportFileCreateRequest = {
    accountId: string;
    agentRequisitesId: string;
};