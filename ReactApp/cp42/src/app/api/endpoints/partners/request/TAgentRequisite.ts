import { EAgentRequisitesStatus, EAgentRequisitesType } from '../enum';
import {
    TPartialLegalPersonRequisites,
    TPartialPhysicalPersonRequisites,
    TPartialSoleProprietorRequisites
} from '../type';

export type TPartialAgentRequisiteRequest = {
    accountId: string;
    id?: string;
    isEditMode: boolean;
    agentRequisitesType: EAgentRequisitesType;
    agentRequisitesStatus: EAgentRequisitesStatus;
    legalPersonRequisites: TPartialLegalPersonRequisites | null;
    physicalPersonRequisites: TPartialPhysicalPersonRequisites | null;
    soleProprietorRequisites: TPartialSoleProprietorRequisites | null;
    creationDate: string;
};