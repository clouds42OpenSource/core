export type TDownloadAgentReportFileRequest = {
    agentDocumentFileId: string;
};