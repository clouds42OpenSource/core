export type TBindAccountsRequest = {
    partnerAccountId: string;
    clientAccountId: string;
};