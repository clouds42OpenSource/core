import { SortingKind } from 'app/common/enums';

export type TAgentCashOutRequest = {
    partnerAccountData?: string;
    requestNumber?: string;
    agentRequisitesType?: number;
    accountUserId?: string;
    requestStatus?: number;
    periodFrom?: string;
    periodTo?: string;
    accountId?: string;
    pageNumber: number;
    pageSize?: number;
    sortFieldName?: string;
    sortType?: SortingKind;
};