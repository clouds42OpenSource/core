import { TClientsListRequest } from './TClientsList';

export type TGetSheetIdRequest = TClientsListRequest & {
    userEmail: string;
};