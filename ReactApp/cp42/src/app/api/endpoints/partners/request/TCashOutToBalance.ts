export type TCashOutToBalanceRequest = {
    sum: number;
    fromAccountId: string;
    toAccountId: string;
    initiatorId: string;
    comment: string;
};