import { SortingKind } from 'app/common/enums';
import { ETypeOfPartnership } from 'app/api/endpoints/partners/enum';

export type TClientsListRequest = {
    accountData?: string;
    partnerAccountData?: string;
    typeOfPartnership?: ETypeOfPartnership;
    periodFrom?: string;
    periodTo?: string;
    periodServiceExpireDateFrom?: string;
    periodServiceExpireDateTo?: string;
    serviceId?: string;
    serviceIsActiveForClient?: boolean;
    accountId?: string;
    pageNumber?: number;
    pageSize?: number;
    sortFieldName?: string;
    sortType?: SortingKind;
};