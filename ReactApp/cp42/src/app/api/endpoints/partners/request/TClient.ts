export type TClientRequest = {
    phoneNumber: string;
    email: string;
    referralAccountId: string;
};