import { SortingKind } from 'app/common/enums';

export type TAgentPaymentListRequest = {
    accountData?: string;
    partnerAccountData?: string;
    operationData?: string;
    accountId?: string;
    periodFrom?: string;
    periodTo?: string;
    pageNumber?: number;
    pageSize?: number;
    sortFieldName?: string;
    sortType?: SortingKind;
};