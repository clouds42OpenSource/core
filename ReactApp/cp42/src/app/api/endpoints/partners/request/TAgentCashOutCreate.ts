import { ERequestStatus } from '../enum';
import { TAgentRequisiteFile } from '../type';

export type TAgentCashOutCreateRequest = {
    accountId?: string;
    id?: string;
    agentRequisitesId: string;
    requestStatus: ERequestStatus;
    totalSum: number;
    paySum: number;
    isEditMode: boolean;
    files: TAgentRequisiteFile[];
};