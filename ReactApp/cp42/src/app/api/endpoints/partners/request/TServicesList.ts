import { EBillingServiceStatus } from 'app/api/endpoints/partners/enum';
import { SortingKind } from 'app/common/enums';

export type TServicesListRequest = {
    pageNumber?: number;
    pageSize?: number;
    accountId?: string;
    billingServiceStatus?: EBillingServiceStatus;
    sortFieldName?: string;
    sortType?: SortingKind;
    periodTo?: string;
    periodFrom?: string;
    name?: string;
    partnerAccountData?: string;
};