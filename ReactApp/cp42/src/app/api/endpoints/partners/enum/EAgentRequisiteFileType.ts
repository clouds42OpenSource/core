/**
 * Тип сканов документов
 */
export const enum EAgentRequisiteFileType {
    /**
     * Скан копии договора
     */
    scanCopyContract = 1,
    /**
     * Скан копии ИНН
     */
    scanCopyInn,
    /**
     * Скан копии СНИЛС
     */
    scanCopySnils,
    /**
     * Скан копии паспорта и прописки
     */
    scanCopyPassportAndRegistration,
    /**
     * Скан копии свидетельства о регистрации юр. лица
     */
    scanCopyCertificateRegistrationLegalEntity,
    /**
     * Скан копии свидетельства о регистрации ИП
     */
    scanCopyCertificateRegistrationSoleProprietor
}