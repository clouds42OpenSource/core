export const enum ERecalculationTaskStatus {
    undefined = -1,
    new,
    captured,
    processing,
    ready,
    error,
    needRetry
}