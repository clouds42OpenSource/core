export const enum EAgentRequisitesStatus {
    undefined,
    draft,
    onCheck,
    verified
}