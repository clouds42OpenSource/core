/**
 * Тип партнерства
 */
export const enum ETypeOfPartnership {
    /**
     * Агент
     */
    agent = 1,
    /**
     * Владелец программного продукта
     */
    softOwner
}