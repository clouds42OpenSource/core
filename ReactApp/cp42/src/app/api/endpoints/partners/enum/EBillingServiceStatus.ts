export const enum EBillingServiceStatus {
    draft = 1,
    moderate,
    active
}