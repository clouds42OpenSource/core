/**
 * Тип источника агентского платежа
 */
export const enum EAgentPaymentSourceType {
    /**
     * Заявка на вывод средств агента
     */
    agentCashOutRequest,
    /**
     * Агентское вознаграждение
     */
    agentReward,
    /**
     * Ручной ввод
     */
    manualInput,
    /**
     * Заявка на перевод баланса агента
     */
    agentTransferBalanceRequest
}