export const enum ERequestStatus {
    undefined,
    new,
    operation,
    paid
}

export enum ERequisiteType {
    PhysicalPersonRequisites,
    LegalPersonRequisites,
    SoleProprietorRequisites
}