/**
 * Тип реквизитов
 */
export const enum EAgentRequisitesType {
    undefined,
    /**
     * Реквизиты физ.лица
     */
    physicalPersonRequisites = 1,
    /**
     * Реквизиты юр. лица
     */
    legalPersonRequisites,
    /**
     * Реквизиты ИП
     */
    soleProprietorRequisites
}