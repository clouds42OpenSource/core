export type TSoleProprietorDetailsResponse = {
    companyName: string | null;
    inn: string | null;
    ogrn: string | null;
    fullName: string | null;
    address: string | null;
    success: boolean;
    errorMessage: string | null;
};