export type TSearchServicesResponse = {
    text: string;
    value: string;
};