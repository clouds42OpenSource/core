import { ERecalculationTaskStatus } from 'app/api/endpoints/partners/enum';

type TRecalculationServiceCost = {
    serviceId: string;
    serviceName: string;
    creationDate: string;
    recalculationDate: string;
    oldServiceCost: number;
    newServiceCost: number;
    coreWorkerTasksQueueId: string;
    comment: string;
    status: string;
    queueStatus: ERecalculationTaskStatus;
};

export type TRecalculationServiceCostResponse = {
    chunkDataOfPagination: TRecalculationServiceCost[];
    totalCount: number;
    pageSize: number;
    pageNumber: number;
};