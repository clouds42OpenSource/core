import { ERequestStatus, ERequisiteType } from 'app/api/endpoints/partners/enum';

type TAgentCashOut = {
    id: string;
    partnerAccountIndexNumber: number;
    partnerAccountCaption: string;
    partnerAccountId: string;
    requestNumber: string;
    creationDateTime: string;
    agentRequisites: string;
    requestStatus: ERequestStatus;
    statusDateTime: string | null;
    requestedSum: number;
    requisiteType: ERequisiteType;
};

export type TAgentCashOutResponse = {
    chunkDataOfPagination: TAgentCashOut[];
    totalCount: number;
    pageSize: number;
    pageNumber: number;
};