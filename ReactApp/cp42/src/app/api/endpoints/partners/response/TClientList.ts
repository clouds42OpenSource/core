import { ETypeOfPartnership } from '../enum';

export type TClient = {
    partnerAccountId: string;
    partnerAccountIndexNumber: number;
    partnerAccountCaption: string;
    serviceExpireDate: string;
    accountId: string;
    accountIndexNumber: number;
    accountCaption: string;
    isVipAccount: boolean;
    typeOfPartnership: ETypeOfPartnership;
    serviceName: string;
    clientActivationDate: string;
    serviceIsActiveForClient: boolean;
    monthlyBonus: number;
    isDemoPeriod: boolean;
};

export type TClientListResponse = {
    chunkDataOfPagination: TClient[];
    pageNumber: number;
    pageSize: number;
    totalCount: number;
};