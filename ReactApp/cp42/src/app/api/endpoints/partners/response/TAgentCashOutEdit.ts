import { TAgentRequisiteFile } from 'app/api/endpoints/partners/type';
import { ERequestStatus } from 'app/api/endpoints/partners/enum';

export type TAgentCashOutEditResponse = {
    accountId: string;
    agentRequisitesId: string;
    files: TAgentRequisiteFile[];
    id: string;
    isEditMode: boolean;
    paySum: number;
    requestNumber: string;
    requestStatus: ERequestStatus;
    totalSum: number;
};