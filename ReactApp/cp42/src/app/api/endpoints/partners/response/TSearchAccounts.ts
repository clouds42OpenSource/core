export type TSearchAccountsResponse = {
    accountCaption: string;
    accountId: string
    accountIndexNumber: number;
};