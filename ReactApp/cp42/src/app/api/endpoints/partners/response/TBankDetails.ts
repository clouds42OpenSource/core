export type TBankDetailsResponse = {
    bankName: string | null;
    bik: string | null;
    swift: string | null;
    correspondentAccount: string | null;
    registrationNumber: string | null;
    bankAdress: string | null;
    success: boolean;
    errorMessage: string | null;
};