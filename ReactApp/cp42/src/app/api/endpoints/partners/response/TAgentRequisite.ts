import { EAgentRequisitesStatus, EAgentRequisitesType } from 'app/api/endpoints/partners/enum';
import { TLegalPersonRequisites, TPhysicalPersonRequisites, TSoleProprietorRequisites } from 'app/api/endpoints/partners/type';

export type TAgentRequisiteResponse = {
    accountId: string;
    id?: string;
    isEditMode: boolean;
    agentRequisitesType: EAgentRequisitesType;
    agentRequisitesStatus: EAgentRequisitesStatus;
    legalPersonRequisites: TLegalPersonRequisites | null;
    physicalPersonRequisites: TPhysicalPersonRequisites | null;
    soleProprietorRequisites: TSoleProprietorRequisites | null;
    creationDate: string;
};