import { EAgentPaymentSourceType, EPaymentType, ETransactionType } from '../enum';

export type TAgentPayment = {
    transactionDate: string;
    accountCaption: string;
    accountIndexNumber: number;
    isVipAccount: boolean;
    accountId: string;
    partnerAccountIndexNumber: number;
    partnerAccountCaption: string;
    partnerAccountId: string;
    operation: string | null;
    clientPaymentSum: number;
    transactionSum: number;
    agentPaymentSourceType: EAgentPaymentSourceType;
    paymentType: EPaymentType;
    transactionType: ETransactionType;
};

export type TAgentPaymentListResponse = {
    chunkDataOfPagination: TAgentPayment[];
    totalCount: number;
    pageSize: number;
    pageNumber: number;
};