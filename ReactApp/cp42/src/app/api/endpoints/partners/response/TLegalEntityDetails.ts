export type TLegalEntityDetailsResponse = {
    companyName: string | null;
    inn: string | null;
    ogrn: string | null;
    kpp: string | null;
    address: string | null;
    headFullName: string | null;
    headPosition: string | null;
    success: boolean;
    errorMessage: string | null;
};