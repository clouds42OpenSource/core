type TAgencyAgreement = {
    id: string;
    name: string;
    printedHtmlFormName: string;
    printedHtmlForms: {};
    printedHtmlFormId: string;
    effectiveDate: string;
    rent1CRewardPercent: number;
    myDiskRewardPercent: number;
    serviceOwnerRewardPercent: number;
    rent1CRewardPercentForVipAccounts: number;
    myDiskRewardPercentForVipAccounts: number;
    serviceOwnerRewardPercentForVipAccounts: number;
};

export type TSummaryDataResponse = {
    companyName: string;
    referrerLink: string;
    forum: null;
    totalEarned: number;
    monthlyCharge: number;
    availableToPay: number;
    currencySymbol: string;
    needApplyAgencyAgreement: boolean;
    actualAgencyAgreement: TAgencyAgreement;
    agencyAgreementEndDate: string;
    nextEffectiveAgencyAgreement: TAgencyAgreement | null;
};