import { EBillingServiceStatus } from '../enum';

export type TService = {
    billingServiceStatus: EBillingServiceStatus;
    countUsers: number;
    id: string;
    isActive: boolean;
    name: string;
    partnerAccountCaption: string;
    partnerAccountId: string;
    partnerAccountIndexNumber: number;
    serviceActivationDate: string | null;
    serviceCost: number;
    statusDateTime: string;
};

export type TServicesListResponse = {
    chunkDataOfPagination: TService[];
    pageNumber: number;
    pageSize: number;
    totalCount: number;
};