import { EAgentRequisitesStatus } from '../enum';

export type TAgentRequisite = {
    id: string;
    number: string;
    agentRequisitesStatus: EAgentRequisitesStatus;
    agentRequisitesType: number;
    partnerAccountIndexNumber: number;
    partnerAccountCaption: string;
    partnerAccountId: string;
    agentRequisitesTypeString: string;
    creationDate: string;
    statusDateTime: string;
};

export type TAgentRequisitesListResponse = {
    chunkDataOfPagination: TAgentRequisite[];
    totalCount: number;
    pageSize: number;
    pageNumber: number;
};