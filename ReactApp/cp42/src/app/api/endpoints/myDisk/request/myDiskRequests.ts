export type getMyDiskRequestDto = {
    accountIsVip: boolean;
    tariff: number;
    expireDate: string;
    discountGroup: number;
    cost: number;
    locale: {
        name: string;
        currency: string;
    }
}

export type getPayTariffRequestDto = {
    sizeGb: number;
    byPromisedPayment: boolean;
}