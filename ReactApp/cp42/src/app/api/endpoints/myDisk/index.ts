import { getMyDiskRequestDto, getPayTariffRequestDto } from 'app/api/endpoints/myDisk/request';
import { changeTariffResponseDto, getMyDiskManagementResponseDto, getMyDiskResponseDto, getSizeChangeCost } from 'app/api/endpoints/myDisk/response';
import { apiHost, getFetchedData, toQueryParams } from 'app/api/fetch';
import { TDataReturn, TResponseLowerCase } from 'app/api/types';
import { localStorageHelper } from 'app/common/helpers/LocalStorageHelper';
import { useFetch } from 'app/hooks';
import { WebVerb } from 'core/requestSender/enums';

const myDiskUrl = apiHost('my-disk');

const useGetMyDisk = (): TDataReturn<getMyDiskResponseDto> => {
    const { data, error, isLoading, refreshData } = useFetch<getMyDiskResponseDto, void>({
        url: `${ myDiskUrl }/${ localStorageHelper.getContextAccountId() }`,
        method: WebVerb.GET
    });

    return { data, error, isLoading, refreshData };
};

const useChangeSize = async (requestParams: number): Promise<TResponseLowerCase<getSizeChangeCost>> => {
    const { responseBody } = await getFetchedData<getSizeChangeCost, void>({
        url: `${ myDiskUrl }/size-change-cost${ toQueryParams({ accountId: localStorageHelper.getContextAccountId() as string, sizeGb: requestParams }) }`,
        method: WebVerb.GET,
    });

    return {
        success: responseBody.success,
        data: responseBody.data,
        message: responseBody.message
    };
};

const useChangeTariff = async (requestParams: number): Promise<TResponseLowerCase<changeTariffResponseDto>> => {
    const { responseBody } = await getFetchedData<changeTariffResponseDto, void>({
        url: `${ myDiskUrl }/tariff${ toQueryParams({ accountId: localStorageHelper.getContextAccountId() as string, sizeGb: requestParams }) }`,
        method: WebVerb.PUT,
    });

    return {
        success: responseBody.success,
        data: responseBody.data,
        message: responseBody.message
    };
};

const getMyDiskManagement = async (): Promise<TResponseLowerCase<getMyDiskManagementResponseDto>> => {
    const { responseBody } = await getFetchedData<getMyDiskManagementResponseDto, void>({
        url: `${ myDiskUrl }/managment`,
        method: WebVerb.GET,
    });

    return {
        success: responseBody.success,
        data: responseBody.data,
        message: responseBody.message
    };
};

const useEditMyDiskManagement = async (requestParams: getMyDiskRequestDto): Promise<TResponseLowerCase<boolean>> => {
    const { responseBody } = await getFetchedData<boolean, getMyDiskRequestDto>({
        url: `${ myDiskUrl }/managment`,
        method: WebVerb.PUT,
        body: requestParams
    });

    return {
        success: responseBody.success,
        data: responseBody.data,
        message: responseBody.message
    };
};

const useStartRecalculation = async (): Promise<TResponseLowerCase<string | null>> => {
    const { responseBody } = await getFetchedData<string, void>({
        url: `${ myDiskUrl }/start-recalculation${ toQueryParams({ accountId: localStorageHelper.getContextAccountId() as string }) }`,
        method: WebVerb.PUT,
    });

    return {
        success: responseBody.success,
        data: responseBody.data,
        message: responseBody.message
    };
};

const useCheckRecalculation = async (requestParams: string): Promise<TResponseLowerCase<boolean | null>> => {
    const { responseBody } = await getFetchedData<boolean, void>({
        url: `${ myDiskUrl }/recalculation-finished${ toQueryParams({ taskId: requestParams }) }`,
        method: WebVerb.GET,
    });

    return {
        success: responseBody.success,
        data: responseBody.data,
        message: responseBody.message
    };
};

const usePayTariff = async (requestParams: getPayTariffRequestDto): Promise<TResponseLowerCase<boolean>> => {
    const { responseBody } = await getFetchedData<boolean, getPayTariffRequestDto & { accountId: string }>({
        url: `${ myDiskUrl }/tariff/payment`,
        method: WebVerb.POST,
        body: { ...requestParams, accountId: localStorageHelper.getContextAccountId() as string }
    });

    return {
        success: responseBody.success,
        data: responseBody.data ?? null,
        message: responseBody.message
    };
};

export const MYDISK = {
    useGetMyDisk,
    useChangeSize,
    useChangeTariff,
    useEditMyDiskManagement,
    useStartRecalculation,
    useCheckRecalculation,
    usePayTariff,
    getMyDiskManagement
} as const;