export type getCorpCloudResponse = {
    accountId: string;
    countOfClicks: number;
    lastUseDate: string;
};