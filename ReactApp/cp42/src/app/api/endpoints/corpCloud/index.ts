import { getCorpCloudResponse } from 'app/api/endpoints/corpCloud/response';
import { getSizeChangeCost } from 'app/api/endpoints/myDisk/response';
import { apiHost, getFetchedData } from 'app/api/fetch';
import { TResponseLowerCase } from 'app/api/types';
import { localStorageHelper } from 'app/common/helpers/LocalStorageHelper';
import { WebVerb } from 'core/requestSender/enums';

const emailsUrl = apiHost('api/AccountCorpCloud/CorpCloudInfo');

const getCorpCloudInfo = async (): Promise<TResponseLowerCase<getCorpCloudResponse>> => {
    const { responseBody } = await getFetchedData<getCorpCloudResponse, void>({
        url: `${ emailsUrl }?accountId=${ localStorageHelper.getContextAccountId() } `,
        method: WebVerb.GET,
    });

    return responseBody;
};

const putCorpCloudInfo = async (): Promise<TResponseLowerCase<getSizeChangeCost>> => {
    const { responseBody } = await getFetchedData<getSizeChangeCost, void>({
        url: `${ emailsUrl }?accountId=${ localStorageHelper.getContextAccountId() } `,
        method: WebVerb.PUT,
    });

    return responseBody;
};

export const CORPCLOUD = {
    getCorpCloudInfo,
    putCorpCloudInfo
} as const;