import { createArticleRequestDto } from 'app/api/endpoints/articles/requests/createArticleAuthorRequestDto';
import { TCreateChatRequest } from 'app/api/endpoints/articles/requests/createChat';
import { createCoreArticleRequestDto } from 'app/api/endpoints/articles/requests/createCoreArticleRequestDto';
import { editArticleRequest, editArticleRequestDto } from 'app/api/endpoints/articles/requests/editArticleRequestDto';
import { editCoreArticleRequestDto } from 'app/api/endpoints/articles/requests/editCoreArticleRequestDto';
import { getArticleByIdRequestDto } from 'app/api/endpoints/articles/requests/getArticleByIdRequestDto';
import { getCoreArticleRequestDto } from 'app/api/endpoints/articles/requests/getCoreArticleRequestDto';
import { TGetTransactionsInfoRequest } from 'app/api/endpoints/articles/requests/getTransactionsInfo';
import { TPostCashOutToBalanceRequest } from 'app/api/endpoints/articles/requests/postCashOutToBalanceRequest';
import { selectArticleThemeRequestDto } from 'app/api/endpoints/articles/requests/selectArticleThemeRequestDto';
import { TEditThemeStatus } from 'app/api/endpoints/articles/response/editThemeStatus';
import { TGetArticleCategories } from 'app/api/endpoints/articles/response/getArticleCategories';
import { getArticleResponseDto, getArticleThemeResponseDto, getMediaResponseDto } from 'app/api/endpoints/articles/response/getArticleResponseDto';
import { getArticlesSummaryInfoResponseDto } from 'app/api/endpoints/articles/response/getArticlesSummaryInfoResponseDto';
import { getAuthorResponseDto } from 'app/api/endpoints/articles/response/getAuthorResponseDto';
import { TGetChatResponse } from 'app/api/endpoints/articles/response/getChat';
import { ArticleCoreStatusEnum, getCoreArticleResponseDto } from 'app/api/endpoints/articles/response/getCoreArticleResponseDto';
import { TGetTransactionInfoResponse } from 'app/api/endpoints/articles/response/getTransactionInfoResponse';
import { apiHost, getFetchedData, templateErrorText, toQueryParams, userId, WP_FETCH_PARAMS } from 'app/api/fetch';
import { TDataReturn } from 'app/api/types';
import { useFetch } from 'app/hooks';
import { getArticleCategoriesSlice, getArticlesSummaryInfoSlice, getArticlesTransactionsSlice } from 'app/modules/articles/reducers';
import { TAppDispatch } from 'app/redux';
import { ArticleStatusEnum } from 'app/views/modules/Articles/types/ArticleStatusEnum';
import { SelectDataMetadataResponseDto, SelectDataResultMetadataModel } from 'app/web/common';
import { WebVerb } from 'core/requestSender/enums';

const articlesCoreUrl = apiHost('articles');

const getArticleById = async (requestParams: getArticleByIdRequestDto) => {
    const { responseBody } = await getFetchedData<getArticleResponseDto, void>({
        url: `${ import.meta.env.REACT_WP_API_URL }/articles/${ requestParams.id }`,
        method: WebVerb.GET,
        ...WP_FETCH_PARAMS
    });

    return responseBody;
};

const getArticles = async () => {
    const { responseBody } = await getFetchedData<getArticleResponseDto[], void>({
        url: `${ import.meta.env.REACT_WP_API_URL }/articles?status=publish,draft,pending`,
        method: WebVerb.GET,
        ...WP_FETCH_PARAMS
    });

    return responseBody;
};

const uploadMedia = async (requestParams: File[]) => {
    const formData = new FormData();
    formData.append('file', requestParams[0]);
    formData.append('title', requestParams[0].name);
    formData.append('alt_text', requestParams[0].name);

    const { responseBody } = await getFetchedData<getMediaResponseDto, FormData>({
        url: `${ import.meta.env.REACT_WP_API_URL }/media`,
        method: WebVerb.POST,
        isFormData: true,
        body: formData,
        ...WP_FETCH_PARAMS
    });

    return responseBody;
};

const getMediaById = async (requestParams: string) => {
    const { responseBody } = await getFetchedData<getMediaResponseDto, FormData>({
        url: `${ import.meta.env.REACT_WP_API_URL }/media/${ requestParams }`,
        method: WebVerb.GET,
        ...WP_FETCH_PARAMS,
    });

    return responseBody;
};

const editArticle = async (requestParams: editArticleRequest & { id: string, status: ArticleStatusEnum }) => {
    const { responseBody } = await getFetchedData<getArticleResponseDto, editArticleRequestDto>({
        url: `${ import.meta.env.REACT_WP_API_URL }/articles/${ requestParams.id }`,
        method: WebVerb.POST,
        ...WP_FETCH_PARAMS,
        body: {
            ...requestParams,
            type: 'articles',
        },
    });

    return responseBody;
};

const editArticleStatus = async (requestParams: { id: string, status: string }) => {
    const { responseBody } = await getFetchedData<getArticleResponseDto, { status: string }>({
        url: `${ import.meta.env.REACT_WP_API_URL }/articles/${ requestParams.id }`,
        method: WebVerb.POST,
        ...WP_FETCH_PARAMS,
        body: {
            status: requestParams.status
        },
    });

    return responseBody;
};

const editBlogArticleStatus = async (requestParams: { id: string, status: string }) => {
    const { responseBody } = await getFetchedData<getArticleResponseDto, { status: string }>({
        url: `${ import.meta.env.REACT_WP_API_URL }/posts/${ requestParams.id }`,
        method: WebVerb.POST,
        ...WP_FETCH_PARAMS,
        body: {
            status: requestParams.status
        },
    });

    return responseBody;
};

const createArticle = async (requestParams: editArticleRequest) => {
    const { responseBody } = await getFetchedData<getArticleResponseDto, editArticleRequestDto>({
        url: `${ import.meta.env.REACT_WP_API_URL }/articles`,
        method: WebVerb.POST,
        ...WP_FETCH_PARAMS,
        body: {
            ...requestParams,
            status: 'draft',
            type: 'articles',
        },
    });

    return responseBody;
};

const getBlogArticleById = async (requestParams: getArticleByIdRequestDto) => {
    const { responseBody } = await getFetchedData<getArticleResponseDto, void>({
        url: `${ import.meta.env.REACT_WP_API_URL }/posts/${ requestParams.id }`,
        method: WebVerb.GET,
        ...WP_FETCH_PARAMS
    });

    return responseBody;
};

const createBlogArticle = async (requestParams: editArticleRequest & { status: ArticleStatusEnum }) => {
    const { responseBody } = await getFetchedData<getArticleResponseDto, editArticleRequestDto>({
        url: `${ import.meta.env.REACT_WP_API_URL }/posts`,
        method: WebVerb.POST,
        ...WP_FETCH_PARAMS,
        body: {
            ...requestParams,
            type: 'articles',
        },
    });

    return responseBody;
};

const editBlogArticle = async (requestParams: editArticleRequest & { id: string, status: ArticleStatusEnum }) => {
    const { responseBody } = await getFetchedData<getArticleResponseDto, editArticleRequestDto>({
        url: `${ import.meta.env.REACT_WP_API_URL }/posts/${ requestParams.id }`,
        method: WebVerb.POST,
        ...WP_FETCH_PARAMS,
        body: {
            ...requestParams,
            type: 'articles',
        },
    });

    return responseBody;
};

const deleteArticle = async (requestParams: { id: string }) => {
    const { responseBody } = await getFetchedData<null, void>({
        url: `${ import.meta.env.REACT_WP_API_URL }/articles/${ requestParams.id }`,
        method: WebVerb.POST,
        ...WP_FETCH_PARAMS
    });

    return responseBody;
};

const deletePost = async (id: string) => {
    const { responseBody } = await getFetchedData({
        url: `${ import.meta.env.REACT_WP_API_URL }/posts/${ id }`,
        method: WebVerb.DELETE,
        ...WP_FETCH_PARAMS
    });

    return responseBody;
};

const getArticleThemes = async () => {
    const { responseBody } = await getFetchedData<getArticleThemeResponseDto[], void>({
        url: `${ import.meta.env.REACT_WP_API_URL }/themes?per_page=100&order=asc&status=publish`,
        method: WebVerb.GET,
        noAuthorizationToken: true,
        hasStandardResponseType: false
    });

    return responseBody;
};

const checkArticleAuthor = async (requestParam: string) => {
    const { responseBody } = await getFetchedData<getAuthorResponseDto[], void>({
        url: `${ import.meta.env.REACT_WP_API_URL }/users?search=${ requestParam }`,
        method: WebVerb.GET,
        ...WP_FETCH_PARAMS
    });

    return responseBody;
};

const createArticleAuthor = async (requestParam: createArticleRequestDto) => {
    const { responseBody } = await getFetchedData<getAuthorResponseDto, createArticleRequestDto>({
        url: `${ import.meta.env.REACT_WP_API_URL }/users`,
        method: WebVerb.POST,
        ...WP_FETCH_PARAMS,
        body: requestParam
    });

    return responseBody;
};

const editThemeStatus = async (requestParams: selectArticleThemeRequestDto & TEditThemeStatus) => {
    const { responseBody } = await getFetchedData<getArticleThemeResponseDto[], selectArticleThemeRequestDto>({
        url: `${ import.meta.env.REACT_WP_API_URL }/themes/${ requestParams.id }`,
        method: requestParams.method ?? WebVerb.POST,
        ...WP_FETCH_PARAMS,
        body: {
            status: requestParams.status
        }
    });

    return responseBody;
};

const useGetCoreArticles = (filterParams: getCoreArticleRequestDto): TDataReturn<SelectDataMetadataResponseDto<getCoreArticleResponseDto>> => {
    const { data, error, isLoading, refreshData } = useFetch<SelectDataMetadataResponseDto<getCoreArticleResponseDto>, void>({
        url: `${ articlesCoreUrl }${ toQueryParams(filterParams) }`,
        method: WebVerb.GET
    });

    return { data, error, isLoading, refreshData };
};

const getCoreArticles = async (filterParams: getCoreArticleRequestDto) => {
    const { responseBody } = await getFetchedData<SelectDataMetadataResponseDto<getCoreArticleResponseDto>, void>({
        url: `${ articlesCoreUrl }${ toQueryParams(filterParams) }`,
        method: WebVerb.GET,
    });

    return responseBody;
};

const getCoreArticle = async (requestParams: { id: string }) => {
    const { responseBody } = await getFetchedData<getCoreArticleResponseDto, void>({
        url: `${ articlesCoreUrl }/${ requestParams.id }`,
        method: WebVerb.GET,
    });

    return responseBody;
};

const createCoreArticle = async (requestParams: createCoreArticleRequestDto) => {
    const { responseBody } = await getFetchedData<getCoreArticleResponseDto, createCoreArticleRequestDto>({
        url: `${ articlesCoreUrl }`,
        method: WebVerb.POST,
        body: requestParams
    });

    return responseBody;
};

const getSummaryInfo = async (dispatch: TAppDispatch, contextUserId?: string) => {
    dispatch(getArticlesSummaryInfoSlice.actions.loading());

    const { responseBody, status, headers } = await getFetchedData<getArticlesSummaryInfoResponseDto, void>({
        url: `${ articlesCoreUrl }/summary-info/${ contextUserId ?? userId() }`,
        method: WebVerb.GET
    });

    if (responseBody.data && responseBody.success) {
        dispatch(getArticlesSummaryInfoSlice.actions.success({
            rawData: responseBody.data,
            status,
            headers
        }));
    } else {
        dispatch(getArticlesSummaryInfoSlice.actions.error(responseBody.message ?? templateErrorText));
    }

    return responseBody;
};

const getArticleCategories = async (dispatch: TAppDispatch) => {
    const { loading, success, error } = getArticleCategoriesSlice.actions;

    dispatch(loading());

    const { responseBody, status, headers } = await getFetchedData<TGetArticleCategories[], void>({
        url: `${ import.meta.env.REACT_WP_API_URL }/categories?per_page=100`,
        method: WebVerb.GET,
        ...WP_FETCH_PARAMS
    });

    if (responseBody.data && responseBody.success) {
        dispatch(success({
            rawData: responseBody.data,
            status,
            headers
        }));
    } else {
        dispatch(error(responseBody.message ?? templateErrorText));
    }
};

const getTransactionInfo = async (dispatch: TAppDispatch, params?: TGetTransactionsInfoRequest) => {
    const { loading, success, error } = getArticlesTransactionsSlice.actions;

    if (!params || !params.filter) {
        params = {
            ...params,
            filter: {
                accountUserId: params?.filter?.accountUserId ?? userId()
            }
        };
    }

    dispatch(loading());

    const { responseBody, status, headers } = await getFetchedData<SelectDataResultMetadataModel<TGetTransactionInfoResponse>, void>({
        url: `${ articlesCoreUrl }/transaction-info${ toQueryParams(params) }`,
        method: WebVerb.GET,
    });

    if (responseBody.data && responseBody.success) {
        dispatch(success({
            rawData: responseBody.data,
            status,
            headers
        }));
    } else {
        dispatch(error(responseBody.message ?? templateErrorText));
    }
};

const deleteCoreArticle = async (requestParams: { id: string }) => {
    const { responseBody } = await getFetchedData<null, void>({
        url: `${ articlesCoreUrl }/${ requestParams.id }`,
        method: WebVerb.DELETE,
    });

    return responseBody;
};

const editCoreArticleStatus = async (requestParams: { id: string, status: ArticleCoreStatusEnum }) => {
    const { responseBody } = await getFetchedData<getCoreArticleResponseDto, {status: ArticleCoreStatusEnum }>({
        url: `${ articlesCoreUrl }/${ requestParams.id }/change-status`,
        method: WebVerb.PUT,
        body: {
            status: requestParams.status
        }
    });

    return responseBody;
};

const editCoreArticle = async (requestParams: editCoreArticleRequestDto) => {
    const { responseBody } = await getFetchedData<getCoreArticleResponseDto, editCoreArticleRequestDto>({
        url: `${ articlesCoreUrl }/${ requestParams.id }`,
        method: WebVerb.PUT,
        body: requestParams
    });

    return responseBody;
};

const useGetChat = (postId: string) => {
    return useFetch<TGetChatResponse[], void>({
        method: WebVerb.GET,
        url: `${ import.meta.env.REACT_WP_API_URL }/chats?search=${ postId }`,
        isRequestBlocked: !postId,
        ...WP_FETCH_PARAMS
    });
};

const createChat = async (body: TCreateChatRequest) => {
    const { responseBody } = await getFetchedData({
        method: WebVerb.POST,
        url: `${ import.meta.env.REACT_WP_API_URL }/chats/`,
        body: {
            ...body,
            status: 'publish'
        },
        ...WP_FETCH_PARAMS
    });

    return responseBody;
};

const editChat = async (chatId: number, content: string) => {
    const { responseBody } = await getFetchedData({
        method: WebVerb.POST,
        url: `${ import.meta.env.REACT_WP_API_URL }/chats/${ chatId }`,
        body: {
            content,
        },
        ...WP_FETCH_PARAMS
    });

    return responseBody;
};

const getThemeById = async (id: string) => {
    const { responseBody } = await getFetchedData<getArticleResponseDto, void>({
        url: `${ import.meta.env.REACT_WP_API_URL }/themes/${ id }`,
        method: WebVerb.GET,
        ...WP_FETCH_PARAMS
    });

    return responseBody;
};

const getCashOutToBalance = async (contextUserId?: string) => {
    const { responseBody } = await getFetchedData<number, void>({
        url: `${ articlesCoreUrl }/cash-out-to-balance/available-sum${ toQueryParams({ accountUserId: contextUserId ?? userId() }) }`,
        method: WebVerb.GET
    });

    return responseBody;
};

const postCashOutToBalance = async (body: TPostCashOutToBalanceRequest) => {
    const { responseBody } = await getFetchedData<string, TPostCashOutToBalanceRequest>({
        url: `${ articlesCoreUrl }/cash-out-to-balance/available-sum`,
        method: WebVerb.POST,
        body
    });

    return responseBody;
};

export const ARTICLES = {
    getArticles,
    getArticleById,
    uploadMedia,
    getMediaById,
    editArticle,
    createArticle,
    deleteArticle,
    getArticleThemes,
    createCoreArticle,
    editThemeStatus,
    useGetCoreArticles,
    getSummaryInfo,
    deleteCoreArticle,
    deletePost,
    editCoreArticleStatus,
    checkArticleAuthor,
    createArticleAuthor,
    getBlogArticleById,
    createBlogArticle,
    editBlogArticle,
    editCoreArticle,
    getCoreArticle,
    editArticleStatus,
    getCoreArticles,
    editBlogArticleStatus,
    useGetChat,
    createChat,
    editChat,
    getThemeById,
    getCashOutToBalance,
    postCashOutToBalance
} as const;

export const ARTICLES_REDUX = {
    getArticleCategories,
    getTransactionInfo
};