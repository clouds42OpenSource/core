type TContent = {
    protected: boolean;
    rendered: string;
};

type TRendered = {
    rendered: string;
}

export type TGetChatResponse = {
    acf: string[];
    content: TContent;
    date: string;
    date_gmt: string;
    guid: TRendered;
    id: number;
    link: string;
    modified: string;
    modified_gmt: string;
    slug: string;
    status: string;
    template: string;
    title: TRendered;
    type: string;
};