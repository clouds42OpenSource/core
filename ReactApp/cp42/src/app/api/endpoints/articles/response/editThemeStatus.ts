import { WebVerb } from 'core/requestSender/enums';

export type TEditThemeStatus = {
    id: string;
    method?: WebVerb;
};