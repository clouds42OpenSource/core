export enum ETransactionCause {
    undefined,
    articlePublication,
    userRegistration
}

export enum ETransactionType {
    undefined,
    inflow,
    outflow
}

export type TGetTransactionInfoResponse = {
    id: number;
    topic: string;
    createdOn: string;
    transactionCause: ETransactionCause;
    transactionType: ETransactionType;
    amount: number;
}