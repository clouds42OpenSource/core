export enum ArticleCoreStatusEnum {
    Draft = 1,
    UnderInspection = 2,
    Accepted = 3,
    ForRevision = 4,
    Published = 5
}

export type getCoreArticleResponseDto = {
    id: string;
    topic: string;
    topicId: string;
    createdOn: string;
    status: ArticleCoreStatusEnum;
    type: ArticleCoreStatusEnum;
    googleCloudLink: string;
    wpLink: string;
    wpId: string | null;
    accountUserId: string;
    registrationCount: number;
    accountUserLogin: string;
    publicationDate: null | string;
};