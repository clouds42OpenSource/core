export type getArticlesSummaryInfoResponseDto = {
    count: string;
    usersCount: string;
    totalEarned: number;
    availableSum: number;
};