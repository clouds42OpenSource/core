import { ArticleStatusEnum } from 'app/views/modules/Articles/types/ArticleStatusEnum';

type LinkHref = {
    href: string;
};

type RawRendered = {
    raw: string;
    rendered: string;
}

type MediaDetails = {
    filesize: number;
    sizes: Record<string, unknown>;
};

type EmbeddableLinkHref = {
    embeddable: boolean;
} & LinkHref;

type VersionHistory = {
    count: number;
    href: string;
};

type WpTerm = {
    taxonomy: string;
    embeddable: boolean;
} & LinkHref;

type Cury = {
    name: string;
    href: string;
    templated: boolean;
};

type Rendered = {
    rendered: string;
};

type RenderedContent = {
    rendered: string;
    protected: boolean;
};

type Acf = {
    author: string;
    theme_description: string;
    theme_keywords: string;
    theme_resources: string;
    theme_unique: number;
};

type Links = {
    self: LinkHref[];
    collection: LinkHref[];
    about: LinkHref[];
    replies: EmbeddableLinkHref[];
    'version-history': VersionHistory[];
    'wp:featuredmedia': EmbeddableLinkHref[];
    'wp:attachment': LinkHref[];
    'wp:term': WpTerm[];
    curies: Cury[];
};

export type getArticleResponseDto = {
    id: string;
    date: string;
    date_gmt: string;
    guid: Rendered;
    modified: string;
    modified_gmt: string;
    slug: string;
    status: ArticleStatusEnum;
    type: string;
    link: string;
    title: RenderedContent;
    content: RenderedContent;
    excerpt: RenderedContent;
    featured_media: string;
    parent: number;
    comment_status: string;
    ping_status: string;
    template: string;
    author: string;
    acf: Acf;
    _links: Links;
    categories: string[];
};

export type getMediaResponseDto = {
    id: string;
    date: string;
    date_gmt: string;
    guid: Rendered;
    modified: string;
    modified_gmt: string;
    slug: string;
    status: ArticleStatusEnum;
    type: string;
    link: string;
    title: RawRendered;
    author: number;
    comment_status: string;
    ping_status: string;
    template: string;
    permalink_template: string;
    generated_slug: string;
    description: RawRendered;
    caption: RawRendered;
    alt_text: string;
    media_type: string;
    mime_type: string;
    media_details: MediaDetails;
    source_url: string;
    _links: Links;
};

export type getArticleThemeResponseDto = {
    acf: Acf;
    id: string;
    date: string;
    date_gmt: string;
    guid: Rendered;
    modified: string;
    modified_gmt: string;
    slug: string;
    status: ArticleStatusEnum;
    type: string;
    link: string;
    title: Rendered;
    content: RenderedContent;
    template: string;
    _links: Links;
};