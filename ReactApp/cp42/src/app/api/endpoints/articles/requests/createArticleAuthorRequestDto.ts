export type createArticleRequestDto = {
    username: string;
    email: string;
    password: string;
    roles: 'author_blog_ext' | 'author_articles_ext';
    name: string;
    first_name?: string;
    last_name?: string;
};