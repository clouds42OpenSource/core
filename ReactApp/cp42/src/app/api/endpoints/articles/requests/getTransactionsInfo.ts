export type TGetTransactionsInfoRequest = {
    pageSize?: number;
    pageNumber?: number;
    filter?: {
        accountUserId?: string;
    }
};