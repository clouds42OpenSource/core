import { ERequestStatus } from 'app/api/endpoints/partners/enum';

export type TPostCashOutToBalanceRequest = {
    accountUserId: string;
    agentRequisitesId: string;
    requestStatus: ERequestStatus;
    totalSum: number;
    paySum: number;
    isEditMode: boolean;
}