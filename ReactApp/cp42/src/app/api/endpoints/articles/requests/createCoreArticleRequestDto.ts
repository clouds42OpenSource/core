export type createCoreArticleRequestDto = {
    topic: string;
    topicId?: string;
    type: number;
    wpId?: string;
};