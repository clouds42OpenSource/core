export type getArticleByIdRequestDto = {
    id: string;
};