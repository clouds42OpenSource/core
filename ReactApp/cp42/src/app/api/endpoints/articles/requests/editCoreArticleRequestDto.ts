export type editCoreArticleRequestDto = {
    topic: string;
    title: string;
    text: string;
    wpLink: string;
    wpId: string;
    id: number;
    registrationCount?: number;
}