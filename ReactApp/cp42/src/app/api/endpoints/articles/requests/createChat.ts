export type TCreateChatRequest = {
    title: string;
    content: string;
};