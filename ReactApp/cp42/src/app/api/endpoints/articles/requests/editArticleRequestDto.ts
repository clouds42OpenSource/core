export type editArticleRequestDto = {
    status: string;
    type: string;
    title: string;
    content: string;
    excerpt: string;
    featured_media: string;
    author: string;
    id?: string;
}

export type editArticleRequest = {
    title: string;
    content: string;
    excerpt: string;
    featured_media: string;
    author: string;
    rank_math_description: string;
}