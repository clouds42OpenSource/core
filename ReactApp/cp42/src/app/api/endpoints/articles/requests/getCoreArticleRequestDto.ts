import { ArticleCoreStatusEnum } from 'app/api/endpoints/articles/response/getCoreArticleResponseDto';
import { SelectDataCommonDataModel } from 'app/web/common';

export type getCoreArticlesFilterParams = {
    status?: ArticleCoreStatusEnum;
    type?: number;
    accountUserLogin?: string;
    accountUserId?: string;
    topic?: string;
    createdFrom?: string;
    createdTo?: string;
};

export type getCoreArticleRequestDto = SelectDataCommonDataModel<getCoreArticlesFilterParams> & {
    orderBy?: string;
};