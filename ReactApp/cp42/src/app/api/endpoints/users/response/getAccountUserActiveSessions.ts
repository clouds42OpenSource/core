type TUserInformation = {
    id: string;
    accountId: string;
    login: string;
    firstName: string;
    lastName: string;
    middleName: string;
    fullName: string;
    email: string;
    phoneNumber: string;
    activated: boolean;
    createdInAd: boolean;
    unsubscribed: boolean;
    creationDate: string;
    accountRoles: string[];
    password: string;
    authToken: string;
    roles: number[];
    corpUserSyncStatus: string;
    accountCaption: string;
    resetCode: string;
};

export type TFetAccountUserActiveSessions = {
    userTerminateList: TUserInformation[]
};