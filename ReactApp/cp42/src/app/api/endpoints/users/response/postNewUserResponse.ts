export type TPostNewUserResponse = {
    accountCaption: string;
    accountId: string;
    accountRoles: number[];
    activated: boolean;
    authToken: string | null;
    corpUserSyncStatus: string;
    createdInAd: boolean;
    creationDate: string | null;
    email: string;
    firstName: string | null;
    fullName: string | null;
    id: string;
    lastName: string | null;
    login: string;
    middleName: string | null;
    password: string;
    phoneNumber: string | null;
    resetCode: string | null;
    roles: Record<number, number>;
    unsubscribed: string | null;
};