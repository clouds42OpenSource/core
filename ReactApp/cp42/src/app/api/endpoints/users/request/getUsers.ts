export type TFilterParams = {
    accountId?: string | null;
    searchLine?: string;
};

export type TGetUsersParams = {
    pageNumber?: number;
    orderBy?: string;
    group?: string;
    filter?: TFilterParams;
};

export type TGetUsersParamsState = {
    pageNumber?: number;
    orderBy?: string;
    group?: string;
    accountId: string;
    searchLine?: string;
};

export type TGetUserParamsRequest = {
    pageNumber?: number;
    orderBy?: string;
    'filter.groups'?: string;
    'filter.accountId': string;
    'filter.searchLine'?: string;
};