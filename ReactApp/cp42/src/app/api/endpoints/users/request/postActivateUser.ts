export type TPostActivateUser = {
    isActivate: boolean,
    accountUserID?: string;
}