import { EDepartmentAction, TAccountDepartment } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/receiveMyCompany';

type TDepartmentRequest = {
    departmentId: string;
    name: string;
    actionType: EDepartmentAction;
};

export type TPostNewUserRequestDto = {
    Login: string;
    PhoneNumber: string;
    Email: string;
    LastName: string;
    FirstName: string;
    MiddleName: string;
    Password: string;
    ConfirmPassword: string;
    Roles: string[];
    DepartmentRequest: TDepartmentRequest | null;
};

export type TPostNewUser = {
    login: string;
    phoneNumber: string;
    email: string;
    lastName: string;
    firstName: string;
    middleName: string;
    password: string;
    confirmPassword: string;
    roles: string[];
    departmentRequest: TAccountDepartment | null;
};

export type TPutEditUserRequestDto = {
    Id: string;
    Login: string;
    PhoneNumber: string;
    Email: string;
    LastName: string;
    FirstName: string;
    MiddleName: string;
    Password: string;
    ConfirmPassword: string;
    Roles: string[];
    CurrentPassword?: string;
    DepartmentRequest: TDepartmentRequest | null;
};

export type TPutEditUser = {
    id: string;
    login: string;
    phoneNumber: string;
    email: string;
    lastName: string;
    firstName: string;
    middleName: string;
    password: string;
    confirmPassword: string;
    roles: string[];
    currentPassword?: string;
    departmentRequest: TAccountDepartment | null;
};