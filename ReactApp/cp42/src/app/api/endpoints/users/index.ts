import { TGetUserParamsRequest, TGetUsersParams, TPostActivateUser, TPostDeleteUser, TPostNewUser, TPostNewUserRequestDto, TPutEditUser, TPutEditUserRequestDto } from 'app/api/endpoints/users/request';
import { TFetAccountUserActiveSessions, TGetUsers, TPostNewUserResponse, TUserItem } from 'app/api/endpoints/users/response';
import { apiHost, contextAccountId, getFetchedData, toQueryParams } from 'app/api/fetch';
import { TResponseLowerCase } from 'app/api/types';
import { usersSlice } from 'app/modules/users';
import { userCard } from 'app/modules/users/reducers/userCard';
import { TAppDispatch } from 'app/redux';
import { changeToCamelCase } from 'app/web/common';
import { WebVerb } from 'core/requestSender/enums';
import React from 'react';

const { loading, get, error } = usersSlice.actions;
const { setLastCreateUser } = userCard.actions;

const accountUsersHost = apiHost('account-users');
const accountUsersV2Host = apiHost('api_v2/AccountUsers');

const getUsers = async (dispatch: TAppDispatch, params?: TGetUsersParams) => {
    dispatch(loading());

    const { responseBody } = await getFetchedData<TGetUsers, TGetUsersParams>({
        url: `${ accountUsersHost }${ toQueryParams<TGetUserParamsRequest>({
            'filter.accountId': contextAccountId(),
            'filter.groups': params?.group,
            'filter.searchLine': params?.filter?.searchLine,
            pageNumber: params?.pageNumber ?? 1,
            orderBy: params?.orderBy ?? 'login.asc'
        }) }`,
        method: WebVerb.GET
    });

    if (responseBody.success && responseBody.data) {
        dispatch(get(responseBody.data));
    } else {
        dispatch(error(responseBody.message ?? ''));
    }
};

const getUser = async (id: string) => {
    const { responseBody } = await getFetchedData<TUserItem, void>({
        url: `${ accountUsersHost }/${ id }`,
        method: WebVerb.GET
    });

    return responseBody;
};

const getAccountUserActiveSessions = async (): Promise<TResponseLowerCase<TFetAccountUserActiveSessions>> => {
    const { responseBody } = await getFetchedData<TFetAccountUserActiveSessions, void>({
        url: `${ accountUsersV2Host }/${ contextAccountId() }/AccountUserActiveSessions`,
        method: WebVerb.GET,
        hasStandardResponseType: false
    });

    return changeToCamelCase(responseBody);
};

const postAccountUserActiveSessions = async (userIds: React.ReactText[]) => {
    const { responseBody } = await getFetchedData({
        url: `${ apiHost('api_v2/AccountUsers') }/AccountUserActiveSessions`,
        method: WebVerb.POST,
        hasStandardResponseType: false,
        body: {
            userIds,
            accountId: contextAccountId()
        }
    });

    return responseBody;
};

const postCreateNewUser = async (dispatch: TAppDispatch, args: TPostNewUser) => {
    const { responseBody } = await getFetchedData<TPostNewUserResponse, TPostNewUserRequestDto>({
        url: accountUsersHost,
        method: WebVerb.POST,
        body: {
            ConfirmPassword: args.confirmPassword,
            Email: args.email,
            FirstName: args.firstName,
            LastName: args.lastName,
            Login: args.login,
            MiddleName: args.middleName,
            Password: args.password,
            PhoneNumber: args.phoneNumber,
            Roles: args.roles.length ? args.roles : ['AccountUser'],
            DepartmentRequest: changeToCamelCase(args.departmentRequest, true),
        }
    });

    if (responseBody.success) {
        dispatch(setLastCreateUser(responseBody));
    }

    return responseBody;
};

const putEditUser = async (args: TPutEditUser) => {
    const { responseBody } = await getFetchedData<boolean, TPutEditUserRequestDto>({
        url: accountUsersHost,
        method: WebVerb.PUT,
        body: changeToCamelCase(args, true)
    });

    return responseBody;
};

const postDeleteUser = async (accountId?: string) => {
    const { responseBody } = await getFetchedData<object, TPostDeleteUser>({
        url: `${ accountUsersV2Host }/Delete`,
        method: WebVerb.POST,
        body: {
            AccountUserID: accountId ?? ''
        },
        hasStandardResponseType: false
    });

    return responseBody;
};

const postActivateUser = async ({ accountUserID, isActivate }: TPostActivateUser) => {
    const { responseBody } = await getFetchedData<object, TPostActivateUser>({
        url: `${ accountUsersV2Host }/Activate`,
        method: WebVerb.POST,
        body: {
            accountUserID: accountUserID ?? '',
            isActivate
        },
        hasStandardResponseType: false
    });

    return responseBody;
};

export const USERS = {
    getAccountUserActiveSessions,
    postAccountUserActiveSessions,
    putEditUser,
    postDeleteUser,
    postActivateUser,
    getUser
} as const;

export const USERS_REDUX = {
    getUsers,
    postCreateNewUser
} as const;