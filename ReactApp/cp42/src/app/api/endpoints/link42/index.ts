import { apiHost, getFetchedData, templateErrorText, toQueryParams } from 'app/api/fetch';
import { useFetch } from 'app/hooks';
import { getInstallLimitsListSlice } from 'app/modules/link42';
import { TAppDispatch } from 'app/redux';
import { WebVerb } from 'core/requestSender/enums';

import { TInstallLimits, TInstallLimitsList } from './response';
import { TCreateLinkConfiguration, TEditLinkConfiguration, TGetInstallLimitsList } from './request';

const useGetInstallLimits = () => {
    return useFetch<TInstallLimits, void>({
        method: WebVerb.GET,
        url: apiHost('link42-configuration/limits'),
        noAuthorizationToken: true
    });
};

const getInstallLimitsList = (dispatch: TAppDispatch, params?: TGetInstallLimitsList) => {
    const { actions: { empty, loading, success, error, saveParams } } = getInstallLimitsListSlice;

    dispatch(saveParams(params ?? {}));
    dispatch(loading());

    (async () => {
        const { responseBody: { data, success: successFetch, message } } = await getFetchedData<TInstallLimitsList, void>({
            method: WebVerb.GET,
            url: apiHost(`link42-configuration${ params ? toQueryParams(params) : '' }`)
        });

        dispatch(empty());

        if (successFetch && data && !message) {
            dispatch(success(data));
        } else {
            dispatch(error(message ?? templateErrorText));
        }
    })();
};

const deleteLinkConfiguration = async (id: string) => {
    const { responseBody } = await getFetchedData<string, void>({
        method: WebVerb.DELETE,
        url: apiHost(`link42-configuration/${ id }`)
    });

    return responseBody;
};

const createLinkConfiguration = async (body: TCreateLinkConfiguration) => {
    const { responseBody } = await getFetchedData<string, TCreateLinkConfiguration>({
        method: WebVerb.POST,
        url: apiHost('link42-configuration'),
        body,
    });

    return responseBody;
};

const editLinkConfiguration = async (body: TEditLinkConfiguration) => {
    const { responseBody } = await getFetchedData<string, TEditLinkConfiguration>({
        method: WebVerb.PUT,
        url: apiHost('link42-configuration'),
        body,
    });

    return responseBody;
};

export const LINK42 = {
    useGetInstallLimits,
    deleteLinkConfiguration,
    createLinkConfiguration,
    editLinkConfiguration
} as const;

export const LINK42_REDUX = {
    getInstallLimitsList
} as const;