import { TCreateLinkConfiguration } from './TCreateLinkConfiguration';

export type TEditLinkConfiguration = TCreateLinkConfiguration & {
    id: string;
};