import { TInstallLimits } from '../response';

export type TInstallLimitsFilters = Partial<Omit<TInstallLimits, 'bitDepths'>>;

export type TGetInstallLimitsList = {
    pageNumber?: number;
    pageSize?: number;
    orderBy?: string;
    filter?: TInstallLimitsFilters;
};