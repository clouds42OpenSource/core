import { TBitDepths } from '../response';

export type TCreateLinkConfiguration = {
    isCurrentVersion: boolean;
    version: string;
    downloadsLimit: number;
    bitDepths: TBitDepths[];
};