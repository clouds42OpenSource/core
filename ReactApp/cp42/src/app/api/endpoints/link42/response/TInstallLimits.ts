import { EBitDepth, ELinkAppType, ESystemType } from '../enum';

export type TBitDepths = {
    linkAppType: ELinkAppType;
    bitDepth: EBitDepth;
    systemType: ESystemType;
    downloadLink: string;
    id?: string;
};

export type TInstallLimits = {
    id: string;
    downloadsCount: number;
    downloadsLimit: number;
    version: string;
    isCurrentVersion: boolean;
    createdOn: string;
    updatedOn?: string;
    updatedBy?: string;
    createdBy: string;
    bitDepths: TBitDepths[];
    allowDownload: boolean;
};