import { MetadataModel } from 'app/web/common';
import { TInstallLimits } from './TInstallLimits';

export type TInstallLimitsList = {
    metadata: MetadataModel;
    records: TInstallLimits[];
};