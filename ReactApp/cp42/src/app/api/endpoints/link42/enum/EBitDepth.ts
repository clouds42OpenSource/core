export enum EBitDepth {
    x86 = 1,
    x64,
    arm,
}