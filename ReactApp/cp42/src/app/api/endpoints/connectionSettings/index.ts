import { apiHost, getFetchedData } from 'app/api/fetch';
import { TResponseLowerCase } from 'app/api/types';
import { WebVerb } from 'core/requestSender/enums';
import { GetConnectionSettings } from './response';

const connectionSettings = apiHost('accounts/connection-settings');

const getConnectionSettings = async (): Promise<TResponseLowerCase<GetConnectionSettings>> => {
    const { responseBody } = await getFetchedData<GetConnectionSettings, void>({
        url: connectionSettings,
        method: WebVerb.GET
    });

    return {
        success: responseBody.success,
        data: responseBody.data ?? null,
        message: responseBody.message
    };
};

export const CONNECTION_SETTINGS = {
    getConnectionSettings
} as const;