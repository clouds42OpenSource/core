export type GetConnectionSettings = {
    gatewayTerminalsName: string;
    servicesTerminalFarmName: string;
    login: string;
    localesName: string;
    platformThinClientVersions: PlatformThinClientItem[];
};

export type PlatformThinClientItem = {
    version: string;
    macOsDownloadLink: string;
    windowsDownloadLink: string;
};