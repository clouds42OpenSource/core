export type getLocalizationsItem = {
    key: number;
    localeId: string;
    value: string;
}

export type getLocalizationsResponseDto = getLocalizationsItem[]