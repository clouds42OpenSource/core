import { getLocalizationsResponseDto } from 'app/api/endpoints/locale/response/getLocaleResponseDto';
import { TDataReturn } from 'app/api/types';
import { useFetch } from 'app/hooks';
import { WebVerb } from 'core/requestSender/enums';
import { apiHost } from 'app/api/fetch';

const localeUrl = apiHost('locales');

const useGetLocaleLocalizations = (): TDataReturn<getLocalizationsResponseDto> => {
    const { data, error, isLoading, refreshData } = useFetch<getLocalizationsResponseDto, void>({
        url: `${ localeUrl }/localizations`,
        method: WebVerb.GET
    });

    return { data, error, isLoading, refreshData };
};

export const LOCALE = {
    useGetLocaleLocalizations,
} as const;