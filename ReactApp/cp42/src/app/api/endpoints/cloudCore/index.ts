import { apiHost, getFetchedData } from 'app/api/fetch';
import { WebVerb } from 'core/requestSender/enums';
import { getReturnLightObject } from 'app/api/utility';

export const uploadMainPageBannerFile = async (formData: FormData) => {
    const { responseBody } = await getFetchedData<null, FormData>({
        url: apiHost('cloud-core/banner'),
        method: WebVerb.POST,
        isFormData: true,
        body: formData
    });

    return getReturnLightObject(responseBody);
};

export const CLOUD_CORE = {
    uploadMainPageBannerFile
} as const;