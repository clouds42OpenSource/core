export type getZabbixCommonRequestDto<T = object> = {
    output: string[];
} & Partial<T>;