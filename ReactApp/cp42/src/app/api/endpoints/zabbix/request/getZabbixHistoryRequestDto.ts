import { getZabbixCommonRequestDto } from 'app/api/endpoints/zabbix/request/common';

export type getZabbixHistoryRequestDto = getZabbixCommonRequestDto<{
    itemids: string;
    sortfield: string;
    sortorder: string;
    limit: number;
    history: number;
    time_from?: number;
    time_till?: number;
}>;