import { getZabbixCommonRequestDto } from 'app/api/endpoints/zabbix/request/common';

export type getZabbixTrendsRequestDto = getZabbixCommonRequestDto<{
  itemids: string;
  sortfield: string;
  sortorder: string;
  limit: number;
  time_from?: number;
  time_till?: number;
}>;