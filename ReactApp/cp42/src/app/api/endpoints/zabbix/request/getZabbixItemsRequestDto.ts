import { getZabbixCommonRequestDto } from 'app/api/endpoints/zabbix/request/common';

export type getZabbixItemsRequestDto = getZabbixCommonRequestDto<{
    hostids?: string;
}>;