import { getZabbixCommonRequestDto } from 'app/api/endpoints/zabbix/request/common';

export type getZabbixHostsRequestDto = getZabbixCommonRequestDto<{
    groupids: string;
}>;