import { zabbixApiUrlEnum } from 'app/api/endpoints/zabbix/enum';
import { getZabbixHistoryRequestDto, getZabbixHostsRequestDto, getZabbixItemsRequestDto, getZabbixTrendsRequestDto } from 'app/api/endpoints/zabbix/request';
import { getZabbixCommonRequestDto } from 'app/api/endpoints/zabbix/request/common';
import { getZabbixHistoryResponseDto, getZabbixHostGroupsResponseDto, getZabbixHostsResponseDto, getZabbixItemsResponseDto, getZabbixTrendsResponseDto } from 'app/api/endpoints/zabbix/response';
import { getZabbixFetchedData } from 'app/api/fetch';
import { TZabbixResponse } from 'app/api/types';

const getHostGroups = async (): Promise<TZabbixResponse<getZabbixHostGroupsResponseDto[]>> => {
    const { responseBody } = await getZabbixFetchedData<TZabbixResponse<getZabbixHostGroupsResponseDto[]>, getZabbixCommonRequestDto>({
        method: zabbixApiUrlEnum.hostgroupGet,
        params: {
            output: ['groupdid', 'name'],
        }
    });

    return responseBody;
};

const getHosts = async (groupids: string): Promise<TZabbixResponse<getZabbixHostsResponseDto[]>> => {
    const { responseBody } = await getZabbixFetchedData<TZabbixResponse<getZabbixHostsResponseDto[]>, getZabbixHostsRequestDto>({
        method: zabbixApiUrlEnum.hostGet,
        params: {
            output: ['groupdid', 'name'],
            groupids
        }
    });

    return responseBody;
};

const getItems = async (hostids?: string): Promise<TZabbixResponse<getZabbixItemsResponseDto[]>> => {
    const { responseBody } = await getZabbixFetchedData<TZabbixResponse<getZabbixItemsResponseDto[]>, getZabbixItemsRequestDto>({
        method: zabbixApiUrlEnum.itemGet,
        params: {
            output: ['itemid', 'name', 'key_', 'lastvalue', 'value_type'],
            hostids,
        }
    });

    return responseBody;
};

const getHistory = async (itemId: string, type: number, from?: number, till?: number): Promise<TZabbixResponse<getZabbixHistoryResponseDto[]>> => {
    const { responseBody } = await getZabbixFetchedData<TZabbixResponse<getZabbixHistoryResponseDto[]>, getZabbixHistoryRequestDto>({
        method: zabbixApiUrlEnum.historyGet,
        params: {
            output: ['itemid', 'clock', 'value'],
            itemids: itemId,
            history: type,
            sortfield: 'clock',
            sortorder: 'DESC',
            limit: 100,
            time_from: from,
            time_till: till
        }
    });

    return responseBody;
};

const getTrends = async (itemId: string, from?: number, till?: number): Promise<TZabbixResponse<getZabbixTrendsResponseDto[]>> => {
    const { responseBody } = await getZabbixFetchedData<TZabbixResponse<getZabbixTrendsResponseDto[]>, getZabbixTrendsRequestDto>({
        method: zabbixApiUrlEnum.trendGet,
        params: {
            output: ['itemid', 'clock', 'value_min', 'value_avg', 'value_max', 'num'],
            itemids: itemId,
            sortfield: 'clock',
            sortorder: 'DESC',
            time_from: from,
            time_till: till
        }
    });

    return responseBody;
};

export const ZABBIX = {
    getHostGroups,
    getHosts,
    getItems,
    getHistory,
    getTrends
} as const;