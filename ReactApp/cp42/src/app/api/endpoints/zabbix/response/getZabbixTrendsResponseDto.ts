export type getZabbixTrendsResponseDto = {
    itemid: string;
    clock: string;
    value_min: string;
    value_avg: string;
    value_max: string;
    num: string;
};