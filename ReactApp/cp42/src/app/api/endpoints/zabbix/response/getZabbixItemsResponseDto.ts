export type getZabbixItemsResponseDto = {
    itemid: string;
    name: string;
    key_: string;
    lastvalue: string;
    value_type: number;
};