export type getZabbixHistoryResponseDto = {
    itemid: string;
    clock: string;
    value: string;
};