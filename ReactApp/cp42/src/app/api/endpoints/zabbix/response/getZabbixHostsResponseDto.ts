export type getZabbixHostsResponseDto = {
    hostid: string;
    name: string;
};