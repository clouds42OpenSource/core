export type getZabbixHostGroupsResponseDto = {
    groupid: string;
    name: string;
};