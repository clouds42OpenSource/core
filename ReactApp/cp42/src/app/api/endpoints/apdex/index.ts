import { getFetchedData, templateErrorText, toQueryParams } from 'app/api/fetch';
import { encodeToBase64 } from 'app/utils';
import { WebVerb } from 'core/requestSender/enums';
import { getApdexProfilesReducer } from 'app/modules/Apdex';
import { TAppDispatch } from 'app/redux';
import { ApdexScoresListResponseDto, ApdexProfileResponseDto, DatabaseKeyOperationsResponseDto, getApdexProfileDataResponseDto, updateTargetTimeResponseDto } from './response';
import { ApdexProfileRequestDto } from './request/createApdexProfile';
import { getApdexScoresList, getApdexProfileDataRequestDto, getDatabaseKeyOperationsRequestDto } from './request';
import { IdNameRecord } from './common';

const ckkUrl = `${ import.meta.env.REACT_CKK_URL }/performance/hs/performance`;
const params = {
    noAuthorizationToken: true,
    noCredentials: true,
    hasStandardResponseType: false,
    headers: {
        Authorization: `Basic ${ encodeToBase64(`${ import.meta.env.REACT_CKK_LOGIN }:${ import.meta.env.REACT_CKK_PASSWORD }`) }`
    }
};

const getApdexDatabaseList = async (requestParam: { clientName: string }) => {
    const { responseBody } = await getFetchedData<IdNameRecord[], void>({
        url: `${ ckkUrl }/${ requestParam.clientName }/databases`,
        method: WebVerb.GET,
        ...params
    });

    return responseBody;
};

const getApdexScores = async (requestParam: getApdexScoresList) => {
    const { responseBody } = await getFetchedData<ApdexScoresListResponseDto, void>({
        url: `${ ckkUrl }/${ requestParam.clientName }/score${ toQueryParams(requestParam.query ?? {}) }`,
        method: WebVerb.GET,
        ...params
    });

    return responseBody;
};

const getDatabaseKeyOperations = async (requestParam: getDatabaseKeyOperationsRequestDto) => {
    const { clientName, clientId, pageNumber, pageSize = 10, query } = requestParam;
    const { responseBody } = await getFetchedData<DatabaseKeyOperationsResponseDto, void>({
        url: `${ ckkUrl }/${ clientName }/DatabaseKeyOperations?databaseId=${ clientId }&page=${ pageNumber }&pageSize=${ pageSize }${ query.keyOperationName ? `&keyOperationName=${ query.keyOperationName }` : '' }`,
        method: WebVerb.GET,
        ...params
    });

    return responseBody;
};

const createApdexProfile = async (requestParam: { data: ApdexProfileRequestDto; clientName: string }) => {
    const { responseBody } = await getFetchedData<DatabaseKeyOperationsResponseDto, ApdexProfileRequestDto>({
        url: `${ ckkUrl }/${ requestParam.clientName }/PerformanceProfile`,
        method: WebVerb.PUT,
        ...params,
        body: requestParam.data
    });

    return responseBody;
};

const updateTargetTime = async (requestParam: {
    data: { keyOperations: { id: string; targetTime: number }[] };
    clientName: string;
    databaseId: string;
}) => {
    const { responseBody } = await getFetchedData<updateTargetTimeResponseDto, { keyOperations: { id: string; targetTime: number }[] }>({
        url: `${ ckkUrl }/${ requestParam.clientName }/DatabaseKeyOperations?databaseId=${ requestParam.databaseId }`,
        method: WebVerb.PATCH,
        ...params,
        body: requestParam.data,
    });

    return responseBody;
};

const getApdexProfileData = async (requestParam: getApdexProfileDataRequestDto) => {
    const { clientName } = requestParam;
    const { responseBody } = await getFetchedData<getApdexProfileDataResponseDto, void>({
        url: `${ ckkUrl }/${ clientName }/score/details${ toQueryParams(requestParam.query) }`,
        method: WebVerb.GET,
        ...params
    });

    return responseBody;
};

const getApdexProfiles = async (dispatch: TAppDispatch, clientName: string, clientId: string) => {
    dispatch(getApdexProfilesReducer.actions.loading());

    const { responseBody, status, headers } = await getFetchedData<ApdexProfileResponseDto, void>({
        url: `${ ckkUrl }/${ clientName }/PerformanceProfile?databaseId=${ clientId }`,
        method: WebVerb.GET,
        ...params
    });

    if (responseBody.data && responseBody.success) {
        dispatch(getApdexProfilesReducer.actions.success({
            rawData: responseBody.data,
            status,
            headers
        }));
    } else {
        dispatch(getApdexProfilesReducer.actions.error(responseBody.message ?? templateErrorText));
    }

    return responseBody;
};

export const APDEX_REDUX = {
    getApdexDatabaseList,
    getApdexScores,
    getDatabaseKeyOperations,
    createApdexProfile,
    getApdexProfiles,
    getApdexProfileData,
    updateTargetTime
} as const;