export type updateTargetTimeResponseDto = {
    keyOperations: {
        id: string;
        name: string;
        targetTime: number;
        targetTimeBasic: number;
        APDEX_RateRelevanceDate: string;
    }[];
};