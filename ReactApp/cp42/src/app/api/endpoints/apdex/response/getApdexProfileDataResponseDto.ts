import { IdNameRecord } from '../common';
import { DatabaseKeyOperationsPagination } from './getApdexProfileResponseDto';

export type Score = {
    keyOperation: IdNameRecord & {
        targetTime: number;
        targetTimeBasic: number;
        APDEX_RateRelevanceDate: string;
    };
    score: number;
    measuringCount: number;
    averageExecutionTime: number;
    apdex_rate?: string;
    temp?: string;
};

export type getApdexProfileDataResponseDto = {
    database: IdNameRecord;
    profile: IdNameRecord;
    fromDate: string;
    toDate: string;
    scores: {
        records: {
            date: string;
            score: number;
            details: Score[];
        }[],
        pagination: DatabaseKeyOperationsPagination;
    };
};