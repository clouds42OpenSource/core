import { KeyOperation } from '../common';

export type DatabaseKeyOperationsPagination = {
    'total-pages': number;
    'page-size': number;
    'total-items': number;
    'page-number': number;
};

export type DatabaseKeyOperation = {
    name: string;
    id: string;
    measuringCount: number;
};

export type DatabaseKeyOperationsResponseDto = {
    databaseID: string;
    fromDate: string;
    toDate?: string;
    keyOperations: DatabaseKeyOperation[];
    pagination: DatabaseKeyOperationsPagination;
};

export type PerformanceProfileData = {
    database: {
        id: string;
    };
    id: string;
    name: string;
    keyOperations: KeyOperation[];
};

export type ApdexProfileResponseDto = {
    performanceProfiles: PerformanceProfileData[];
    pagination: DatabaseKeyOperationsPagination;
};