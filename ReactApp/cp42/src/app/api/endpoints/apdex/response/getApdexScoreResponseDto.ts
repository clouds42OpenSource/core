import { IdNameRecord } from '../common';

export type PerformanceScore = {
    profile: IdNameRecord;
    score: number;
    scoreDate: string;
};

export type DatabasePerformance = {
    database: IdNameRecord;
    scores: PerformanceScore[];
};

export type ApdexScoresListResponseDto = {
    clientId: string;
    performanceScores: DatabasePerformance[];
};