export type KeyOperation = {
    id: string
};

export type IdNameRecord = {
    id: string;
    name: string;
};