export type TQuery = {
    databaseId: string;
    profileId: string;
    fromDate?: string;
    toDate?: string;
};