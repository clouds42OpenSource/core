import { KeyOperation } from '../common';

export type ApdexProfileRequestDto = {
    database: {
        id: string;
    };
    name: string;
    keyOperations: KeyOperation[];
};