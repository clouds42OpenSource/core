export type getDatabaseKeyOperationsRequestDto = {
    clientName: string;
    clientId: string;
    pageNumber: number;
    pageSize?: number;
    query: {
        keyOperationName: string | null;
    };
};