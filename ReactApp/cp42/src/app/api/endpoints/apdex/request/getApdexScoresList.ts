import { TQuery } from './common';

export type getApdexScoresList = {
    clientName: string;
    query?: TQuery;
};