import { TQuery } from './common';

export type getApdexProfileDataRequestDto = {
    clientName: string;
    query: TQuery & {
        keyOperationId?: string;
        page?: number;
        pageSize?: number;
        orderBy?: string;
    };
};