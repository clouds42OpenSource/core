export type TGetBackupsRequestDto = {
    DatabaseId: string;
    CreationDateFrom: string;
    CreationDateTo: string;
}

export type TGetBackupsRequest = {
    databaseId: string;
    from: string;
    to: string;
}