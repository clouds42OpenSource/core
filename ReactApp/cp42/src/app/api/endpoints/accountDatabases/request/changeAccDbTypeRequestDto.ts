export type ChangeAccDbTypeRequestDto = {
    accountDatabseId: string;
    adminPhoneNumber: string;
    databaseAdminLogin: string;
    databaseAdminPassword: string;
    changeTypeDate: string;
}