export type TFromFileToClusterRequest = {
    accountDatabaseId: string;
    accountUserId: string;
    accountId: string;
    login: string;
    password: string;
    isCopy: boolean;
    phoneNumber: string;
};