export type TGetBackupsItem = {
    backupPath: string;
    creationDate: string;
    eventTrigger: number;
    eventTriggerDescription: string;
    id: string;
    initiator: string;
    isDbOnDelimiters: boolean;
    sourceType: number;
}

export type TGetBackups = {
    databaseBackups: TGetBackupsItem[];
    dbBackupsCount: number;
}