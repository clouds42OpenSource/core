import { ChangeAccDbTypeRequestDto, checkDatabaseNameRequestDto, TFromFileToClusterRequest, TGetBackupsRequest, TGetBackupsRequestDto } from 'app/api/endpoints/accountDatabases/request';
import { TGetBackups } from 'app/api/endpoints/accountDatabases/response';
import { accountId, apiHost, getFetchedData, templateErrorText, toQueryParams } from 'app/api/fetch';
import { TResponseLowerCase } from 'app/api/types';
import { getExternalUsersListSlice } from 'app/modules/accountDatabases';
import { TAppDispatch } from 'app/redux';
import { GetAccessCardDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/AccessInfoDbDataModel';
import { WebVerb } from 'core/requestSender/enums';

const accountDatabasesUrl = apiHost('account-databases');
const changeAccDb = apiHost('api_v2/AccountDatabases/ChangeAccDbTypeRequest');

const checkDatabaseName = async (requestParam: checkDatabaseNameRequestDto): Promise<TResponseLowerCase<boolean>> => {
    const { responseBody } = await getFetchedData<boolean, checkDatabaseNameRequestDto>({
        url: `${ accountDatabasesUrl }/check-name${ toQueryParams(requestParam) }`,
        method: WebVerb.GET,
    });

    return responseBody;
};

const getBackupUrl = async (backupId: string) => {
    const { responseBody } = await getFetchedData<string, void>({
        url: apiHost(`signed-urls?backupId=${ backupId }`),
        method: WebVerb.GET,
    });

    return responseBody;
};

const changeAccDbTypeRequest = async (params: ChangeAccDbTypeRequestDto): Promise<TResponseLowerCase<boolean>> => {
    const { responseBody } = await getFetchedData<boolean, ChangeAccDbTypeRequestDto>({
        url: changeAccDb,
        method: WebVerb.POST,
        body: params
    });

    return responseBody;
};

const getExternalUsersList = (dispatch: TAppDispatch, databaseId: string, needShowLoading = true) => {
    const { loading, success: successAction, error, empty } = getExternalUsersListSlice.actions;

    if (needShowLoading) {
        dispatch(loading());
    }

    getFetchedData<GetAccessCardDataModel, void>({
        url: apiHost(`api_v2/AccountDatabases/external-access?databaseId=${ databaseId }`),
        method: WebVerb.GET
    }).then(({ responseBody: { success, message, data } }) => {
        dispatch(empty());

        if (success && data && !message) {
            dispatch(successAction(data));
        } else {
            dispatch(error(message ?? templateErrorText));
        }
    });
};

const postUpdateDatabase = async (accountDatabaseId: string) => {
    const { responseBody } = await getFetchedData({
        url: apiHost('api_v2/AccountDatabases/update-database'),
        method: WebVerb.POST,
        body: accountDatabaseId
    });

    return responseBody;
};

const fromFileToCluster = async (body: TFromFileToClusterRequest) => {
    const { responseBody } = await getFetchedData({
        url: `${ accountDatabasesUrl }/from-file-to-cluster`,
        method: WebVerb.POST,
        body
    });

    return responseBody;
};

const postNotificationState = async () => {
    const { responseBody } = await getFetchedData({
        url: `${ accountDatabasesUrl }/${ accountId() }/notification-state`,
        method: WebVerb.POST
    });

    return responseBody;
};

const getBackups = async ({ databaseId, from, to }: TGetBackupsRequest) => {
    const params: TGetBackupsRequestDto = {
        DatabaseId: databaseId,
        CreationDateFrom: from,
        CreationDateTo: to
    };

    const { responseBody } = await getFetchedData<TGetBackups, void>({
        url: apiHost(`api_v2/AccountDatabases/Backups${ toQueryParams(params) }`),
        method: WebVerb.GET
    });

    return responseBody;
};

export const ACCOUNT_DATABASES = {
    checkDatabaseName,
    changeAccDbTypeRequest,
    postUpdateDatabase,
    postNotificationState,
    getBackups,
    getBackupUrl,
    fromFileToCluster
} as const;

export const ACCOUNT_DATABASES_REDUX = {
    getExternalUsersList
} as const;