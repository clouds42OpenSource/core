export type TGetAdditionalCompaniesResponse = {
    key: string;
    value: string;
};