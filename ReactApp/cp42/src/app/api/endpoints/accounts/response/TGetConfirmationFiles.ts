import { EVerificationStatus } from 'app/api/endpoints/accounts/enum';
import { MetadataModel } from 'app/web/common';

type TRecord = {
    id: string;
    status: EVerificationStatus;
    type: number;
    accountIndexNumber: number;
    accountCaption: string;
    cloudFileId: string;
    accountId: string;
};

export type TGetConfirmationFilesResponse = {
    records: TRecord[];
    metadata: MetadataModel;
};