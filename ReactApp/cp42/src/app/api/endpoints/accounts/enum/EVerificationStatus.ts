export enum EVerificationStatus {
    undefined = -1,
    underReview,
    verified,
    canceled,
}