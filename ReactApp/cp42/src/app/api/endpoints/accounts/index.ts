import { apiHost, contextAccountId, getFetchedBlobData, getFetchedData, templateErrorText, toQueryParams } from 'app/api/fetch';
import { useFetch } from 'app/hooks';
import { getConfirmationFilesSlice } from 'app/modules/accounts';
import { TAppDispatch } from 'app/redux';
import { WebVerb } from 'core/requestSender/enums';
import { TGetConfirmationFilesResponse } from './response';
import { TCreateAdditionalCompanyRequest, TEditAdditionalCompanyRequest, TEditCertificateStatusRequest, TGetConfirmationFilesRequest } from './request';

const accountUrl = apiHost('accounts');
const additionalCompanies = apiHost('additional-companies');
const accountFiles = apiHost('account-files');

const postDeleteConfirmation = async () => {
    const { responseBody } = await getFetchedData({
        url: `${ accountUrl }/${ contextAccountId() }/delete-confirmation`,
        method: WebVerb.POST
    });

    return responseBody;
};

const deleteConfirmDeletion = async (code: string) => {
    const { responseBody } = await getFetchedData({
        url: `${ accountUrl }/confirm-deletion`,
        method: WebVerb.DELETE,
        body: code
    });

    return responseBody;
};

const sendCertificate = async (formData: FormData) => {
    const { responseBody } = await getFetchedData<null, FormData>({
        url: accountFiles,
        method: WebVerb.POST,
        body: formData,
        isFormData: true,
    });

    return responseBody;
};

const editCertificateStatus = async (body: TEditCertificateStatusRequest) => {
    const { responseBody } = await getFetchedData({
        url: `${ accountFiles }/confirmations/status-change`,
        method: WebVerb.POST,
        body
    });

    return responseBody;
};

const getConfirmationFiles = async (dispatch: TAppDispatch, query: TGetConfirmationFilesRequest) => {
    const { actions: { loading, success, error, empty } } = getConfirmationFilesSlice;

    dispatch(loading());

    const { responseBody: { data, success: ok, message }, status, headers } = await getFetchedData<TGetConfirmationFilesResponse, void>({
        url: `${ accountFiles }/confirmations${ toQueryParams(query) }`,
        method: WebVerb.GET,
    });

    dispatch(empty());

    if (ok && data) {
        dispatch(success({ rawData: data, status, headers }));
    } else {
        dispatch(error(message ?? templateErrorText));
    }
};

const getConfirmationFile = async (accountId: string, id: string) => {
    const { responseBody } = await getFetchedBlobData({
        url: `${ accountUrl }/${ accountId }/account-files/${ id }`,
        method: WebVerb.GET,
    });

    return responseBody;
};

const deleteConfirmationFile = async (accountId: string, id: string) => {
    const { responseBody } = await getFetchedData({
        url: `${ accountUrl }/${ accountId }/account-files/${ id }`,
        method: WebVerb.DELETE,
    });

    return responseBody;
};

const createAdditionalCompany = async (body: TCreateAdditionalCompanyRequest) => {
    const { responseBody } = await getFetchedData({
        url: additionalCompanies,
        method: WebVerb.POST,
        body
    });

    return responseBody;
};

const editAdditionalCompany = async (body: TEditAdditionalCompanyRequest) => {
    const { responseBody } = await getFetchedData({
        url: additionalCompanies,
        method: WebVerb.PUT,
        body
    });

    return responseBody;
};

const deleteAdditionalCompany = async (id: string) => {
    const { responseBody } = await getFetchedData({
        url: `${ accountUrl }/${ contextAccountId() }/additional-companies/${ id }`,
        method: WebVerb.DELETE,
    });

    return responseBody;
};

const useGetAdditionalCompanies = () => {
    return useFetch<[{ key: string; value: string; }], void>({
        url: `${ additionalCompanies }/as-list?accountId=${ contextAccountId() }`,
        method: WebVerb.GET,
    });
};

export const ACCOUNTS = {
    postDeleteConfirmation,
    deleteConfirmDeletion,
    sendCertificate,
    editCertificateStatus,
    createAdditionalCompany,
    editAdditionalCompany,
    deleteAdditionalCompany,
    useGetAdditionalCompanies,
    getConfirmationFile,
    deleteConfirmationFile
} as const;

export const ACCOUNTS_REDUX = {
    getConfirmationFiles,
} as const;