import { EVerificationStatus } from 'app/api/endpoints/accounts/enum';

type TFilter = {
    accountIndexNumber?: number;
    status?: EVerificationStatus;
};

export type TGetConfirmationFilesRequest = {
    pageNumber?: number;
    pageSize?: number;
    orderBy?: string;
    searchLine?: string;
    filter?: TFilter;
};