import { EVerificationStatus } from '../enum';

export type TEditCertificateStatusRequest = {
    id: string;
    status: EVerificationStatus;
};