import { TCreateAdditionalCompanyRequest } from './TCreateAdditionalCompany';

export type TEditAdditionalCompanyRequest = Partial<TCreateAdditionalCompanyRequest> & {
    id: string;
};