export type TCreateAdditionalCompanyRequest = {
    name: string;
    inn: string;
    description?: string;
    email?: string;
    accountId: string;
};