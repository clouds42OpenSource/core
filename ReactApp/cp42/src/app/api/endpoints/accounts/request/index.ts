export type { TCreateAdditionalCompanyRequest } from './TCreateAdditionalCompany';
export type { TEditAdditionalCompanyRequest } from './TEditAdditionalCompany';
export type { TEditCertificateStatusRequest } from './TEditCertificateStatus';
export type { TGetConfirmationFilesRequest } from './TGetConfirmationFiles';