export type PaymentRequest = {
    serviseId: string;
    sum: number;
    usePromise: boolean;
};

export type PaymentRequestDto = {
    'service-id': string,
    sum: number
    'use-promise': boolean,
};

export enum RequestType {
    regular = 'regular',
    eternal = 'eternal',
    dt = 'dt'
}

export type PostBackupsRequest = {
    databaseId: string;
    /**
     * Forced backup (kick users out)
     */
    force: boolean;
    type: RequestType;
    payment?: PaymentRequest;
};

export type PostBackupsRequestDto = {
    /**
     * Forced backup (kick users out)
     */
    force: boolean;
    type: RequestType;
    payment?: PaymentRequestDto;
};