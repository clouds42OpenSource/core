export type PutPayRequest = {
    ServiceId: string;
    AccountId: string;
    PaymentSum: number;
    UsePromisePayment: boolean;
};

export type PutPayParams = {
    serviceId: string;
    paymentSum: number;
    usePromisePayment: boolean;
};