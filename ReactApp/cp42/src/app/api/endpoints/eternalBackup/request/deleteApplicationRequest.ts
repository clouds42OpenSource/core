export enum DeleteType {
    manual = 'manual',
    auto = 'auto',
    tomb = 'tomb'
}

export type DeleteApplicationRequest = {
    databaseId: string;
    paymentServiceId: string;
    paymentSum: number;
    paymentUsePromise: boolean;
    type: DeleteType;
}