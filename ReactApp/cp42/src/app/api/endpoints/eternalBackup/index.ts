import { PutPayParams, PutPayRequest } from 'app/api/endpoints/eternalBackup/request/putPayRequest';
import { PutPayModel } from 'app/api/endpoints/eternalBackup/response/putPayResponse';
import { apiHost, getFetchedData, msHost, toQueryParams } from 'app/api/fetch';
import { TResponseLowerCase } from 'app/api/types';
import { localStorageHelper } from 'app/common/helpers/LocalStorageHelper';
import { WebVerb } from 'core/requestSender/enums';
import { DeleteApplicationRequest, PostBackupsRequest, PostBackupsRequestDto } from './request';

const eternalBackupUrl = msHost('pl42/hs/Platform/applications');
const databaseBackupUrl = apiHost('database-backup/pay');

const postBackups = async (requestParam: PostBackupsRequest) => {
    const requestBody: PostBackupsRequestDto = {
        force: requestParam.force,
        type: requestParam.type,
    };

    if (requestParam.payment) {
        requestBody.payment = {
            'service-id': requestParam.payment.serviseId,
            sum: requestParam.payment.sum,
            'use-promise': requestParam.payment.usePromise,
        };
    }

    const { responseBody } = await getFetchedData<null, PostBackupsRequestDto>({
        url: `${ eternalBackupUrl }/${ requestParam.databaseId }/backups`,
        method: WebVerb.POST,
        body: requestBody,
        hasStandardResponseType: false
    });

    return responseBody;
};

const deleteApplication = async (requestParam: DeleteApplicationRequest) => {
    const { responseBody } = await getFetchedData({
        url: `${ eternalBackupUrl }/${ requestParam.databaseId }${ toQueryParams({
            'payment-service-id': requestParam.paymentServiceId,
            'payment-sum': requestParam.paymentSum,
            'payment-use-promise': requestParam.paymentUsePromise,
            type: requestParam.type
        }) }`,
        method: WebVerb.DELETE,
        hasStandardResponseType: false
    });

    return responseBody;
};

const putPay = async (params: PutPayParams): Promise<TResponseLowerCase<PutPayModel>> => {
    const { responseBody } = await getFetchedData<PutPayModel, PutPayRequest>({
        url: databaseBackupUrl,
        method: WebVerb.PUT,
        body: {
            ServiceId: params.serviceId,
            PaymentSum: params.paymentSum,
            UsePromisePayment: params.usePromisePayment,
            AccountId: localStorageHelper.getContextAccountId() ?? ''
        }
    });

    return responseBody;
};

export const ETERNAL_BACKUPS = {
    postBackups,
    deleteApplication,
    putPay
} as const;