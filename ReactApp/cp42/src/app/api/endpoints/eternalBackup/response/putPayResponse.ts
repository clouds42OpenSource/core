export type PutPayResponse = {
    Complete: boolean;
    ErrorMessage: string;
    NotEnoughMoney: number;
    Amount: number;
    Currency: string;
    CanUsePromisePayment: boolean;
    CanIncreasePromisePayment: boolean;
};

export type PutPayModel = {
    complete: boolean;
    errorMessage: string;
    notEnoughMoney: number;
    amount: number;
    currency: string;
    canUsePromisePayment: boolean;
    canIncreasePromisePayment: boolean;
};