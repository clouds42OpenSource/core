export type TGetSoloUpdateQueueResponse = {
    name: string;
    addDate: string;
};

export type TGetSoloUpdateQueueView = {
    number: number;
    name: string;
    addDate: string;
};