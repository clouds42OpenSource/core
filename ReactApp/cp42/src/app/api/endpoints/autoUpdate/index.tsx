import { TGetSoloUpdateQueueResponse, TGetSoloUpdateQueueView } from 'app/api/endpoints/autoUpdate/response';
import { apiHost, getFetchedData } from 'app/api/fetch';
import { TResponseLowerCase } from 'app/api/types';
import { SelectDataResultMetadataModel } from 'app/web/common';
import { WebVerb } from 'core/requestSender/enums';

const getSoloUpdateQueue = async (onlyVip: boolean): Promise<TResponseLowerCase<TGetSoloUpdateQueueView[]>> => {
    const { responseBody } = await getFetchedData<SelectDataResultMetadataModel<TGetSoloUpdateQueueResponse>, void>({
        url: apiHost(`auto-update-account-database-data/solo-update-queue?onlyVip=${ onlyVip }`),
        method: WebVerb.GET
    });

    return {
        ...responseBody,
        data: responseBody.data?.records?.map((item, index) => ({
            ...item,
            number: index
        })) ?? []
    };
};

export const AUTO_UPDATE = {
    getSoloUpdateQueue
} as const;