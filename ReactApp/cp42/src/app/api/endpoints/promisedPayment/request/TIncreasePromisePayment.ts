export type TIncreasePromisePaymentRequest = {
    invoiceId: string;
};