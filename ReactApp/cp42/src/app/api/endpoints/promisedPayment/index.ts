import { apiHost, contextAccountId, getFetchedData } from 'app/api/fetch';
import { getReturnLightObject } from 'app/api/utility';
import { changeToCamelCase } from 'app/web/common';
import { WebVerb } from 'core/requestSender/enums';
import { TIncreasePromisePaymentRequest } from './request';

const increasePromisePayment = async (params: TIncreasePromisePaymentRequest) => {
    const { responseBody } = await getFetchedData<object, TIncreasePromisePaymentRequest>({
        url: apiHost(`api_v2/accounts/${ contextAccountId() }/promised-payment/increase`),
        method: WebVerb.POST,
        body: changeToCamelCase(params, true),
        hasStandardResponseType: false
    });

    if (responseBody.data && 'Description' in responseBody.data) {
        responseBody.message = responseBody.data.Description as string;
    }

    return getReturnLightObject(responseBody);
};

export const PROMISE_PAYMENT = {
    increasePromisePayment
} as const;