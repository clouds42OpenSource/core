export type GetSponsorshipRequest = {
    email: string;
    serviceId?: string;
};