import { Sponsorship } from 'app/api/endpoints/managingServiceSubscriptions/response';

export type Services = {
    /**
     * Идентификатор субъекта
     */
    subject: string;
    /**
     * Идентификатор услуги
     */
    billingServiceTypeId: string;
    /**
     * Выбранный статус
     */
    status: boolean;
    /**
     * Данные о спонсировании
     */
    sponsorship?: Sponsorship;
};

export type PutServicesPrecalculatedCostOrApplyRequest = {
    /**
     * Идентификатор сервиса
     */
    billingServiceId: string;
    /**
     * Идентификатор аккаунта
     */
    accountId: string;
    /**
     * Услуга для аккаунта
     */
    changedLicense?: Services;
    /**
     * Услуги для аккаунта
     */
    changedLicenses?: Services[];
    /**
     * Услуги для пользователей
     */
    changedUsers: Services[];
    /**
     * Использовать ОП
     */
    usePromisePayment?: boolean;
};