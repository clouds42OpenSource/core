export type PutServicesManagementRequest = {
    accountId: string;
    /**
     * Идентификатор сервиса
     */
    billingServiceId: string;
    /**
     * Список новых цен на услуги
     */
    newCosts: { [key: string]: number };
    /**
     * Новая дата окончания
     */
    newExpireDate: string;
};

export type NewCostsRequest = {
    key: string,
    value: number
};