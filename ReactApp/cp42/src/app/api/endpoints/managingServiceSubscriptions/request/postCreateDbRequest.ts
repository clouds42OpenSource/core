export type PostCreateDbRequest = {
    id: string;
    templateId: string;
};