export type GetServicesUsersRequest = {
    accountId: string;
    orderBy: string;
    'filter.accountUserName'?: string;
    pageNumber?: number;
    pageSize?: number;
};