export type PutServicesActivationDemoRequest = {
    serviceId: string;
    accountId: string;
    usePromisePayment: boolean;
};