export type PostProlongDemo = {
    serviceId: string;
    accountId: string;
    prolongDayCount: number;
};