export type GetExtensionsRequest = {
    id: string;
    accountId: string;
    pageSize?: number;
    pageNumber?: number;
};