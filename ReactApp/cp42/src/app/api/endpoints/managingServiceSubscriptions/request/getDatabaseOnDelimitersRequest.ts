export type GetDatabaseOnDelimitersRequest = {
    databaseId: string;
};