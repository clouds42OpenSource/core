export type GetServicesUserRequest = {
    id: string;
    userLogin: string;
};