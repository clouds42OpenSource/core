export type PostExtensionsRequest = {
    /**
     * Идентификатор сервиса
     */
    serviceId: string;
    /**
     * Идентификаторы баз для установки расширения
     */
    databaseIds: string[];
};