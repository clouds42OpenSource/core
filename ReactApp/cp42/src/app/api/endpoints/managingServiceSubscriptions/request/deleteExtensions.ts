export type DeleteExtensions = {
    id: string;
    accountDatabaseId: string;
};