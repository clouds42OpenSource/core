export type GetExtensionsStateRequest = {
    /**
     * идентификатор сервиса
     */
    id: string;
    /**
     * идентификатор инф. базы
     */
    accountDatabaseId: string;
};