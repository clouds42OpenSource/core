export enum DatabaseStatus {
    Undefined = 0,
    NewItem = 1,
    Ready = 2,
    Unknown = 3,
    Detached = 4,
    Attaching = 5,
    ErrorNotEnoughSpace = 6,
    ErrorCreate = 7,
    ErrorDtFormat = 8,
    ProcessingSupport = 9,
    TransferDb = 10,
    TransferArchive = 11,
    RestoringFromTomb = 12,
    DelitingToTomb = 13,
    DetachingToTomb = 14,
    DeletedToTomb = 15,
    DetachedToTomb = 16,
    DeletedFromCloud = 17
}

export enum TimerType {
    'Green' = 0,
    'Orange' = 1,
    'Red' = 2
}

export enum BillingType {
    ForAccountUser = 0,
    ForAccount = 1
}

export enum Clouds42Service {
    Unknown = 0,
    MyDisk = 1,
    MyEnterprise = 2,
    Recognition = 3,
    Esdl = 4,
    MyDatabases = 5
}

export enum ExtensionState {
    DoneInstall,
    DoneDelete,
    ProcessingInstall,
    ProcessingDelete,
    ErrorProcessingInstall,
    ErrorProcessingDelete,
    NotInstalled
}

export enum Status {
    ServiceNotPaid,
    OverduePromisedPayment,
    NoDiskSpace
}

export enum ServiceLockReasonType {
    ServiceNotPaid = 0,
    OverduePromisedPayment = 1,
    NoDiskSpace = 2
}

export enum SystemService {
    Unknown = 0,
    MyDisk = 1,
    MyEnterprise = 2,
    Recognition = 3,
    Esdl = 4,
    MyDatabases = 5
}

export enum BillingServiceStatus {
    Draft = 1,
    OnModeration = 2,
    IsActive = 3
}

export enum Code {
    Rent1CIsNotActive = 0,
    AccountUserIsNotActivated = 1
}