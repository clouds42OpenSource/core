import { TResponseLowerCase } from 'app/api/types';

export type PutServicesActivationDemo = {
    /**
     * Успешность операции
     */
    complete: boolean;
    /**
     * Недостающее количество денег для принятия подписки
     */
    needMoney?: number;
    /**
     * Возможность использовать ОП
     */
    canUsePromisePayment?: boolean;
    /**
     * Сообщение об ошибки
     */
    errorMessage: string;
};

export type PutServicesActivationDemoResponse = TResponseLowerCase<PutServicesActivationDemo>;