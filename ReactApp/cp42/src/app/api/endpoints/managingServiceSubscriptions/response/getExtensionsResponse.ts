import { TResponseLowerCase } from 'app/api/types';
import { SelectDataMetadataResponseDto } from 'app/web/common';
import { DatabaseState } from 'app/common/enums';
import { ExtensionState } from 'app/api/endpoints/managingServiceSubscriptions/enum';

export type GetExtensions = {
    id: string;
    caption: string;
    v82Name: string;
    templateName: string;
    extensionLastActivityDate?: Date;
    extensionState?: ExtensionState,
    databaseStateString: string;
    databaseState: DatabaseState;
    isInstalled: boolean;
    databaseImageCssClass: string;
};

export type GetExtensionsResponse = TResponseLowerCase<SelectDataMetadataResponseDto<GetExtensions>>;