import { TResponseLowerCase } from 'app/api/types';

export type PutServicesApply = {
    /**
     * Успешность операции
     */
    isComplete: boolean;
    /**
     * Недостающее количество денег для принятия подписки
     */
    notEnoughMoney?: number,
    /**
     * Сумма
     */
    amount: number;
    /**
     * Валюта
     */
    currency: string;
    /**
     * Возможность использовать ОП
     */
    canUsePromisePayment: boolean;
    /**
     * Платежи
     */
    paymentIds: string[];
};

export type PutServicesApplyResponse = TResponseLowerCase<PutServicesApply>;