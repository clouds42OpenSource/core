import { TResponseLowerCase } from 'app/api/types';

export type GetServicesCompatibleTemplates = {
    /**
     * Идентификатор шаблона
     */
    templateId: string;
    /**
     * Наименование шаблона
     */
    templateName: string;
};

export type GetServicesCompatibleTemplatesResponse = TResponseLowerCase<GetServicesCompatibleTemplates[]>;