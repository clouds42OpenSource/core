type TTargetAudience = {
    name: string;
    id: string;
};

type TServiceInformationDraftFilesCommands = {
    active: boolean;
    name: string;
    presentation: string;
    beginTime?: string;
    daysRepeatPeriod?: number;
    repeatPeriodInDay?: number;
    endTime?: string;
    weekDays?: number[] | Set<number>;
};

type TServiceInformationDraftFiles = {
    comment: string;
    configurationsMinVersions?: Record<string, string>;
    fileId: string;
    name: string;
    synonym: string;
    type: string;
    version: string;
    unsafeMode?: boolean;
    commands?: TServiceInformationDraftFilesCommands[];
};

type TServiceInformation = {
    serviceID: string;
    draft: boolean;
    stringID?: string;
    serviceName: string;
    serviceCost?: string;
    serviceDescription: string;
    serviceShortDescription: string;
    serviceIcon: string;
    serviceIconID: string;
    instructionURL: string;
    instruction: string;
    hybrid: boolean;
    instructionID: string;
    industries: string[];
    tags: string[];
    video?: string;
    targetAudience: TTargetAudience[] | string[];
    userBilling?: boolean;
    private: boolean;
    serviceScreenshots: string[];
    draftFiles: TServiceInformationDraftFiles[];
};

export type TServiceInformationResponse = {
    result: TServiceInformation;
};