import { Sponsorship } from 'app/api/endpoints/managingServiceSubscriptions/response/sponsorship';
import { TResponseLowerCase } from 'app/api/types';

export type GetSponsorship = {
    /**
     * Идентификатор пользователя аккаунта
     */
    accountUserId: string;
    /**
     * Полное имя пользователя
     */
    accountUserName: string;
    /**
     * Информация о спонсировании
     */
    sponsorship: Sponsorship;
    /**
     * Активные услуги пользователя
     */
    services: string[];
    /**
     * Услуги, спонсируемые аккаунтом пользователя
     */
    sponsoredServiceTypesByMyAccount: string[];
};

export type GetSponsorshipResponse = TResponseLowerCase<GetSponsorship>;