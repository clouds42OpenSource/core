export enum SystemServiceType {
    Esdl = 1,
    MyEntUser = 2,
    MyEntUserWeb = 3,
    DiskSpace = 4,
    Rec10 = 5,
    Rec1000 = 6,
    Rec3000 = 7,
    Rec5000 = 8,
    CountOfDatabasesOverLimit = 9,
    ServerDatabasePlacement = 10,
    AccessToServerDatabase = 11,
    AccessToConfiguration1C = 12
}