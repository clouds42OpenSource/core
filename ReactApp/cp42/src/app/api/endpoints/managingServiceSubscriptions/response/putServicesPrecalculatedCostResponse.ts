import { Sponsorship } from 'app/api/endpoints/managingServiceSubscriptions/response/sponsorship';
import { TResponseLowerCase } from 'app/api/types';
import { Code } from 'app/api/endpoints/managingServiceSubscriptions/enum';

export type Error = {
    /**
     * Код ошибки
     */
    code: Code;
    /**
     * описание
     */
    description: string;
};

export type PutServicesPrecalculatedCost = {
    /**
     * Идентификатор субъекта (аккаунт/пользователь)
     */
    subject: string;
    /**
     * Идентификатор услуги
     */
    billingServiceTypeId: string;
    /**
     * Выбранный статус
     */
    status: boolean;
    /**
     * Данные о спонсировании
     */
    sponsorship: Sponsorship;
    /**
     * Стоимость
     */
    cost: number;
    /**
     * Название услуги
     */
    serviceTypeName: string;
    /**
     * Ошибки
     */
    errors: Error[];
};

export type PutServicesPrecalculatedCostResponse = TResponseLowerCase<PutServicesPrecalculatedCost[]>;