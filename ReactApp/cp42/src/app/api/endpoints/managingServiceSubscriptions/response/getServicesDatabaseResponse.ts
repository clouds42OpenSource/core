import { TResponseLowerCase } from 'app/api/types';
import { AccountDatabaseToRun } from 'app/api/endpoints/managingServiceSubscriptions/response/accountDatabaseToRun';

export type GetServicesDatabaseResponse = TResponseLowerCase<AccountDatabaseToRun>;