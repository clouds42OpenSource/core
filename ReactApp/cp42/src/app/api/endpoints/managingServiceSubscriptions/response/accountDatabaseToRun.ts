import { DatabaseStatus, TimerType } from 'app/api/endpoints/managingServiceSubscriptions/enum';

export type TimerLeft = {
    hours: number;
    minutes: number;
    seconds: number;
    totalSeconds: number;
};

export type CreationTimer = {
    /**
     * Отображение таймера
     */
    visible: boolean;
    /**
     * Детали времени
     */
    timerLeft: TimerLeft;
    /**
     * Состояние таймера
     */
    timerType: TimerType
};

export type AccountDatabasesOnDelimitersToRun = {
    /**
     * Идентификатор базы
     */
    id: string;
    /**
     * Название базы
     */
    caption: string;
    /**
     * Ссылка веб-публикации
     */
    webPublishPath: string;
    /**
     * Статус базы
     */
    databaseStatus: DatabaseStatus;
    /**
     * Таймер создания
     */
    creationTimer: CreationTimer
};

export type AccountDatabaseToRun = {
    /**
     * Наличие соответствующих конфигурация 1С
     */
    availabilityOfSuitableConfigurations1C: boolean;
    /**
     * Идентификатор шаблона конфигурации по умолчанию
     */
    templateId: string;
    /**
     * Название шаблонов, совместимых с конфигурацией
     */
    templateName: string;
    /**
     * Базы на разделителях, доступные для запуска сервиса
     */
    accountDatabasesOnDelimitersToRun: AccountDatabasesOnDelimitersToRun[];
    /**
     * Существуют ли совместимые базы со статусом "Готова"
     */
    isCompatibleDatabasesWithStatusReadyExist: boolean;
    /**
     * Существуют ли совместимые базы со статусом "Новая"
     */
    isCompatibleDatabasesWithStatusNewExist: boolean
};