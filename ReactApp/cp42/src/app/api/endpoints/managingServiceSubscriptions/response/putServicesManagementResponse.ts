import { TResponseLowerCase } from 'app/api/types';

export type PutServicesManagementResponse = TResponseLowerCase<boolean>;