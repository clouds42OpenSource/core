import { TResponseLowerCase } from 'app/api/types';
import { ExtensionState } from 'app/api/endpoints/managingServiceSubscriptions/enum';

export type GetExtensionsState = {
    /**
     * статус расширения сервиса
     */
    extensionDatabaseStatus: ExtensionState;
    /**
     * дата установки статус
     */
    setStatusDateTime?: Date;
    /**
     * наличие даты установки статуса
     */
    serviceStatus: boolean;
};

export type GetExtensionsStateResponse = TResponseLowerCase<GetExtensionsState>;