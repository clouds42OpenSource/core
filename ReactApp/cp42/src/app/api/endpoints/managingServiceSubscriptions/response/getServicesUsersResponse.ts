import { TResponseLowerCase } from 'app/api/types';
import { SelectDataMetadataResponseDto } from 'app/web/common';
import { Sponsorship } from 'app/api/endpoints/managingServiceSubscriptions/response/sponsorship';

export type GetServicesUsers = {
    /**
     * Идентификатор пользователя
     */
    accountUserId: string;
    /**
     * ФИО пользователя
     */
    accountUserName: string;
    /**
     * Спонсирование
     */
    sponsorship: Sponsorship;
    /**
     * Активированные услуги пользователя
     */
    services: string[];
    /**
     * Спонсируемые услуги моим аккаунтом
     */
    sponsoredServiceTypesByMyAccount: string[];
};

export type GetServicesUsersResponse = TResponseLowerCase<SelectDataMetadataResponseDto<GetServicesUsers>>;