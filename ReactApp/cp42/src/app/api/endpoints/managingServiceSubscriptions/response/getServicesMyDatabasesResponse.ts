import { TResponseLowerCase } from 'app/api/types';
import { SystemServiceType } from 'app/api/endpoints/managingServiceSubscriptions/response/systemServiceType';
import { Status } from 'app/api/endpoints/managingServiceSubscriptions/enum';

export type ServiceTypeStatus = {
    /**
     * Статус блокировки услуги сервиса
     */
    serviceIsLocked: boolean;
    /**
     * Причина блокировки услуги сервиса
     */
    serviceLockReason: string;
    /**
     * Тип причины блокировки услуги сервиса
     */
    serviceLockReasonType: Status,
    /**
     * Наличие непогашенного ОП
     */
    promisedPaymentIsActive: boolean;
    /**
     * Сумма ОП
     */
    promisedPaymentSum: string;
    /**
     * Дата, до которой необходимо погасить ОП
     */
    promisedPaymentExpireDate: Date;
};

export type GetServicesMyDatabases = {
    /**
     * Идентификатор услуги сервиса
     */
    id: string;
    /**
     * Идентификатор сервиса
     */
    billingServiceId: string;
    /**
     * Название услуги сервиса
     */
    name: string;
    /**
     * Отображаемое название услуги сервиса
     */
    displayedName: string;
    /**
     * Дата окончания действия сервиса
     */
    expireDate: Date;
    /**
     * Отображаемая дата окончания
     */
    displayedExpireDate: Date;
    /**
     * Статус услуги сервиса
     */
    serviceTypeStatus: ServiceTypeStatus;
    /**
     * Код валюты
     */
    currencyCode: string;
    /**
     * Количество информационных баз
     */
    databasesCount: number;
    /**
     * Стоимость за одну инф. базу
     */
    costPerOneDatabase: number;
    /**
     * Общая стоимость инф. баз
     */
    totalDatabasesCost: number;
    /**
     * Тип системной услуги
     */
    systemServiceType?: SystemServiceType;
    /**
     * Я спонсирую
     */
    iamSponsorLicenses: number;
    /**
     * Меня спонсируют
     */
    meSponsorLicenses: number;
    /**
     * Количество использованных лицензий
     */
    usedLicenses: number;
    /**
     * Стоимость лицензии сервиса
     */
    cost: number;
    /**
     * Общая стоимость лицензии сервиса
     */
    amount: number;
};

export type GetServicesMyDatabasesResponse = TResponseLowerCase<GetServicesMyDatabases>;