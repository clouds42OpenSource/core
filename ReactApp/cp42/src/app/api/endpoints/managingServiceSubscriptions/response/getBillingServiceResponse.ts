import { BillingType, Clouds42Service } from 'app/api/endpoints/managingServiceSubscriptions/enum';
import { SystemServiceType } from 'app/api/endpoints/managingServiceSubscriptions/response/systemServiceType';
import { TResponseLowerCase } from 'app/api/types';

export type GetBillingServiceResponse = TResponseLowerCase<GetBillingService[]>;

export type GetBillingService = {
    /**
     * Идентификатор услуги
     */
    id: string;
    /**
     * Услуги, от которых наследуется услуга сервиса
     */
    parentServiceTypes: string[];
    /**
     * Услуги, которые наследуются от услуги сервиса
     */
    childServiceTypes: string[];
    /**
     * Название услуги
     */
    name: string;
    /**
     * Общая стоимость услуги сервиса
     */
    amount: number;
    /**
     * Валюта сервиса
     */
    currency: string;
    /**
     * Стоимость услуги сервиса
     */
    cost: number;
    /**
     * Стоимость одной лицензии
     */
    costPerLicense: number;
    /**
     * Количество используемых лицензий
     */
    usedLicenses: number;
    /**
     * Количество спонсируемых лицензий (аккаунт спонсирует пользователей другого аккаунта)
     */
    sponsoringLicenses: number;
    /**
     * Количество спонсирующих лицензий (другой аккаунт спонсирует пользователей аккаунта)
     */
    sponsoredLicenses: number;
    /**
     * Тип работы сервиса
     */
    billingType: BillingType;
    /**
     * Тип системной услуги
     */
    systemServiceType?: SystemServiceType;
    /**
     * Идентификатор сервиса биллинга
     */
    serviceId: string;
    /**
     * Имя сервиса биллинга
     */
    serviceName: string;
    /**
     * Тип системного сервиса облака
     */
    clouds42Service?: Clouds42Service;
    /**
     * Состояние сервиса
     */
    isActive: boolean;
};