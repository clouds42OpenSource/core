import { TResponseLowerCase } from 'app/api/types';

export type GetServicesActivation = {
    /**
     * Индикатор сервиса
     */
    id: string;
    /**
     * Название сервиса
     */
    name: string;
    /**
     * Краткое описание сервиса
     */
    shortDescription: string;
    /**
     * Ссылка для активации сервиса
     */
    serviceActivationLink: string
};

export type GetServicesActivationResponse = TResponseLowerCase<GetServicesActivation>;