import { TResponseLowerCase } from 'app/api/types';

/**
 * Список идентификаторов активных услуг сервиса
 */
export type GetActiveServicesResponse = TResponseLowerCase<string[]>;