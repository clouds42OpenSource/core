import { TResponseLowerCase } from 'app/api/types';

export type GetServicesUser = {
    /**
     * Активность сервиса
     */
    isActive: boolean;
    /**
     * Демо-период
     */
    isDemoPeriod: boolean;
    /**
     * Дата истечения демо-периода
     */
    serviceDemoPeriodExpiredDate: Date;
    /**
     * Дата окончания текущей подписки для пользователя
     */
    serviceExpireDate: Date;
    /**
     * Список подключенных услуг
     */
    enabledServiceTypesList: string[];
    /**
     * Отключен/включен сервис
     */
    isServiceDisabled: boolean;
};

export type GetServicesUserResponse = TResponseLowerCase<GetServicesUser>;