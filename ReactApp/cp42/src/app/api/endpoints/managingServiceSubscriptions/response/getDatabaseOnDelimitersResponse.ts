import { TResponseLowerCase } from 'app/api/types';
import { CreationTimer } from 'app/api/endpoints/managingServiceSubscriptions/response/accountDatabaseToRun';
import { DatabaseStatus } from 'app/api/endpoints/managingServiceSubscriptions/enum';

export type GetDatabaseOnDelimiters = {
    /**
     * Идентификатор базы
     */
    id: string;
    /**
     * Название базы
     */
    caption: string;
    /**
     * Ссылка веб-публикации
     */
    webPublishPath: string;
    /**
     * Статус базы
     */
    databaseState: DatabaseStatus;
    /**
     * Таймер создания
     */
    creationTimer: CreationTimer;
};

export type GetDatabaseOnDelimitersResponse = TResponseLowerCase<GetDatabaseOnDelimiters>;
