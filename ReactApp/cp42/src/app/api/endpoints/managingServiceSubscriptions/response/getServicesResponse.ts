import { TResponseLowerCase } from 'app/api/types';
import { AccountDatabaseToRun } from 'app/api/endpoints/managingServiceSubscriptions/response/accountDatabaseToRun';
import { BillingServiceStatus, ServiceLockReasonType, SystemService } from 'app/api/endpoints/managingServiceSubscriptions/enum';

export type MainServiceData = {
    /**
     * Стоимость
     */
    cost: number;
    /**
     * Фиксирована ли стоимость
     */
    costIsFixed?: boolean;
    /**
     * Дата окончания действия сервиса
     */
    expireDate?: Date;
    /**
     * Заморожен ли сервис
     */
    frozen?: boolean;
    /**
     * Группа дисконтов
     */
    discountGroup?: number;
    /**
     * Дата создания сервиса
     */
    createDate?: Date;
    /**
     * Демо-период
     */
    isDemoPeriod: boolean;
    /**
     * Идентификатор аккаунта биллинга
     */
    accountId: string;
    /**
     * Идентификатор сервиса биллинга
     */
    billingServiceId: string;
    /**
     * Включена ли авто-подписка
     */
    isAutoSubscriptionEnable?: boolean;
};

export type Currency = {
    /**
     * Название валюты
     */
    currency: string;
    /**
     * Международный код валюты
     */
    currencyCode?: number;
};

export type AmountData = {
    /**
     * Сумма ежемесячного платежа
     */
    serviceAmount: number;
    /**
     * Необходимая сумма для работы сервиса до конца расчетного периода
     */
    servicePartialAmount?: number;
    /**
     * Требуемая сумма за один день
     */
    amountForOneDay: number;
    /**
     * Информация о валюте
     */
    currency: Currency;
};

export type ServiceStatus = {
    /**
     * Блокировка сервиса
     */
    serviceIsLocked: boolean;
    /**
     * Причина блокировки
     */
    serviceLockReason: string;
    /**
     * Тип причины блокировки
     */
    serviceLockReasonType: ServiceLockReasonType;
    /**
     * Наличие непогашенного ОП
     */
    promisedPaymentIsActive: boolean;
    /**
     * Сумма ОП
     */
    promisedPaymentSum: number;
    /**
     * Максимальная дата оплаты ОП
     */
    promisedPaymentExpireDate: Date;
};

export type GetServices = {
    /**
     * Идентификатор сервиса
     */
    id: string;
    /**
     * Название сервиса
     */
    name: string;
    /**
     * Краткое описание сервиса
     */
    shortDescription: string;
    /**
     * Возможности сервиса
     */
    opportunities: string;
    /**
     * Идентификатор иконки сервиса
     */
    iconCloudFileId?: string;
    /**
     * Идентификатор основного сервиса
     */
    mainServiceId?: string;
    /**
     * Идентификатор услуги, от которой зависит сервис
     */
    dependServiceTypeId?: string;
    /**
     * Аккаунт, к которому принадлежит сервис
     */
    accountOwnerId?: string;
    /**
     * Тип системного сервиса
     */
    systemService?: SystemService;
    /**
     * Статус сервиса
     */
    billingServiceStatus: BillingServiceStatus;
    /**
     * Дата активации сервиса
     */
    serviceActivationDate?: Date;
    /**
     * Активность сервиса
     */
    isActive: boolean;
    /**
     * Дата окончания использования
     */
    serviceExpireDate: Date;
    /**
     * Данные по основному сервису
     */
    mainServiceData: MainServiceData;
    /**
     * Сервис находится в демо-периоде
     */
    isDemoPeriod: boolean;
    /**
     * Сервис зависит от аренды 1С
     */
    serviceDependsOnRent: boolean;
    /**
     * Информация о стоимости сервиса
     */
    amountData: AmountData;
    /**
     * Статус сервиса
     */
    serviceStatus: ServiceStatus;
    /**
     * Является ли сервис системным
     */
    isSystemService: boolean;
    /**
     * Модель запуска базы
     */
    accountDatabaseToRun: AccountDatabaseToRun;
    /**
     * Возможность установки расширения сервиса
     */
    canInstallExtension: boolean;
    /**
     * Необходимость создания базы для активации
     */
    needCreateDatabaseToActivateService: boolean;
    /**
     * Дата окончания аренды
     */
    rent1CExpireDate: Date;
};

export type GetServicesResponse = TResponseLowerCase<GetServices>;