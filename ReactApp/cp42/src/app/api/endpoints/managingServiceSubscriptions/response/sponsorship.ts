export type Sponsorship = {
    /**
     * Я спонсирую
     */
    i: boolean;
    /**
     * Меня спонсируют
     */
    me: boolean;
    /**
     * Название аккаунта
     */
    label: string;
};