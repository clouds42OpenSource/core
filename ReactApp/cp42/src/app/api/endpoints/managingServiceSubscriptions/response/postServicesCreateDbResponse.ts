import { TResponseLowerCase } from 'app/api/types';

export type PostServicesCreateDbResponse = TResponseLowerCase<boolean>;