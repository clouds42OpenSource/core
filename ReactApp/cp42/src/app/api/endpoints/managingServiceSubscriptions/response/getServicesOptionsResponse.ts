import { TResponseLowerCase } from 'app/api/types';
import { BillingType } from 'app/api/endpoints/managingServiceSubscriptions/enum';
import { SystemServiceType } from 'app/api/endpoints/managingServiceSubscriptions/response/systemServiceType';

export type GetServicesOptions = {
    /**
     * Идентификатор услуги
     */
    id: string;
    /**
     * Наименование услуги
     */
    name: string;
    /**
     * Описание услуги
     */
    description: string;
    /**
     * Тип работы биллинга
     */
    billingType: BillingType;
    /**
     * Системный тип услуги
     */
    systemServiceType?: SystemServiceType;
    /**
     * Услуга, от которой зависит текущая услуга
     */
    dependServiceTypeId?: string;
};

export type GetServicesOptionsResponse = TResponseLowerCase<GetServicesOptions>;