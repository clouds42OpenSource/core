export type GetAutoInstallResponse = {
    autoinstall: boolean;
};