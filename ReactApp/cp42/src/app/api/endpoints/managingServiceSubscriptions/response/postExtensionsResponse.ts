import { TResponseLowerCase } from 'app/api/types';

export type PostExtensionsResponse = TResponseLowerCase<boolean>;