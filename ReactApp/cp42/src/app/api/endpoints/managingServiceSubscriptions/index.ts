import {
    GetExtensionsRequest,
    GetExtensionsStateRequest,
    GetServicesRequest,
    GetServicesUserRequest,
    GetServicesUsersRequest,
    GetSponsorshipRequest,
    PostCreateDbRequest,
    PostExtensionsRequest,
    PostProlongDemo,
    PutServicesActivationDemoRequest,
    PutServicesManagementRequest,
    PutServicesPrecalculatedCostOrApplyRequest
} from 'app/api/endpoints/managingServiceSubscriptions/request';
import { DeleteExtensions } from 'app/api/endpoints/managingServiceSubscriptions/request/deleteExtensions';
import {
    AccountDatabaseToRun,
    GetActiveServicesResponse,
    GetAutoInstallResponse,
    GetBillingService,
    GetBillingServiceResponse,
    GetDatabaseOnDelimiters,
    GetDatabaseOnDelimitersResponse,
    GetExtensions,
    GetExtensionsResponse,
    GetExtensionsState,
    GetExtensionsStateResponse,
    GetServices,
    GetServicesActivation,
    GetServicesActivationResponse,
    GetServicesCompatibleTemplates,
    GetServicesCompatibleTemplatesResponse,
    GetServicesDatabaseResponse,
    GetServicesMyDatabases,
    GetServicesMyDatabasesResponse,
    GetServicesOptions,
    GetServicesOptionsResponse,
    GetServicesResponse,
    GetServicesUser,
    GetServicesUserResponse,
    GetServicesUsers,
    GetServicesUsersResponse,
    PostExtensionsResponse,
    PostServicesCreateDbResponse,
    PutServicesActivationDemo,
    PutServicesActivationDemoResponse,
    PutServicesApply,
    PutServicesApplyResponse,
    PutServicesManagementResponse,
    PutServicesPrecalculatedCost,
    PutServicesPrecalculatedCostResponse,
    TServiceInformationResponse
} from 'app/api/endpoints/managingServiceSubscriptions/response';
import { GetSponsorship, GetSponsorshipResponse } from 'app/api/endpoints/managingServiceSubscriptions/response/getSponsorshipResponse';
import { accountId, apiHost, contextAccountId, getFetchedData, msHost, toQueryParams } from 'app/api/fetch';
import { TDataReturn, TResponseLowerCase } from 'app/api/types';
import { localStorageHelper } from 'app/common/helpers/LocalStorageHelper';
import { useFetch } from 'app/hooks';
import { changeToCamelCase, SelectDataMetadataResponseDto } from 'app/web/common';
import { WebVerb } from 'core/requestSender/enums';

const managingServiceSubscriptionsUrl = apiHost('billing-services');
const autoInstallServiceUrl = msHost('pl42/hs/Platform/accounts');

const getServices = async ({ id }: GetServicesRequest): Promise<GetServicesResponse | null> => {
    const { responseBody } = await getFetchedData<GetServices, GetServicesRequest>({
        url: `${ managingServiceSubscriptionsUrl }/${ id }`,
        method: WebVerb.GET
    });

    return responseBody;
};

const useGetService = (id: string) => {
    return useFetch<GetServices, void>({
        url: `${ managingServiceSubscriptionsUrl }/${ id }`,
        method: WebVerb.GET
    });
};

const getServicesActivation = async ({ id }: GetServicesRequest) => {
    const { responseBody } = await getFetchedData<GetServicesActivation, GetServicesRequest>({
        url: `${ managingServiceSubscriptionsUrl }/${ id }/activation`,
        method: WebVerb.GET
    });

    return responseBody;
};

const getBillingService = async ({ id }: GetServicesRequest): Promise<GetBillingServiceResponse> => {
    const { responseBody } = await getFetchedData<GetBillingService[], GetServicesRequest>({
        url: `${ managingServiceSubscriptionsUrl }/${ id }/services?accountId=${ contextAccountId() }`,
        method: WebVerb.GET
    });

    return responseBody;
};

const useGetBillingService = (id: string) => {
    return useFetch<GetBillingService[], void>({
        url: `${ managingServiceSubscriptionsUrl }/${ id }/services?accountId=${ contextAccountId() }`,
        method: WebVerb.GET
    });
};

const useGetServiceMs = (id: string): TDataReturn<TServiceInformationResponse> => {
    const result = useFetch<TServiceInformationResponse, void>({
        url: msHost(`pl42/hs/Platform/services/${ id }/`),
        method: WebVerb.GET,
        hasStandardResponseType: false,
    });

    return changeToCamelCase(result);
};

const getActiveService = async ({ id }: GetServicesRequest): Promise<GetActiveServicesResponse | null> => {
    const { responseBody: { data, success, message } } = await getFetchedData<string[], GetServicesRequest>({
        url: `${ managingServiceSubscriptionsUrl }/${ id }/active-services?accountId=${ contextAccountId() }`,
        method: WebVerb.GET
    });

    return {
        data,
        success,
        message
    };
};

const getServicesUsers = async ({ id }: GetServicesRequest, sortParams: GetServicesUsersRequest): Promise<GetServicesUsersResponse> => {
    const { responseBody: { data, success, message } } = await getFetchedData<SelectDataMetadataResponseDto<GetServicesUsers>, GetServicesRequest & GetServicesUsersRequest>({
        url: `${ managingServiceSubscriptionsUrl }/${ id }/users${ toQueryParams<GetServicesUsersRequest>(sortParams) }`,
        method: WebVerb.GET
    });

    return {
        data,
        success,
        message
    };
};

const getServicesDatabase = async ({ id }: GetServicesRequest): Promise<GetServicesDatabaseResponse | null> => {
    const { responseBody: { data, success, message } } = await getFetchedData<AccountDatabaseToRun, GetServicesRequest>({
        url: `${ managingServiceSubscriptionsUrl }/${ id }/database?accountId=${ contextAccountId() }`,
        method: WebVerb.GET
    });

    return {
        data,
        success,
        message
    };
};

const getServicesDatabaseOnDelimiters = async ({ id }: GetServicesRequest): Promise<GetDatabaseOnDelimitersResponse | null> => {
    const { responseBody: { data, success, message } } = await getFetchedData<GetDatabaseOnDelimiters, GetServicesRequest>({
        url: `${ managingServiceSubscriptionsUrl }/database-on-delimiters?databaseId=${ id }`,
        method: WebVerb.GET
    });

    return {
        data,
        success,
        message
    };
};

const putServicesManagement = async (requestParams: PutServicesManagementRequest): Promise<PutServicesManagementResponse | null> => {
    const { responseBody: { data, success, message } } = await getFetchedData<boolean, PutServicesManagementRequest>({
        url: `${ managingServiceSubscriptionsUrl }/management`,
        method: WebVerb.PUT,
        body: requestParams
    });

    return {
        data,
        success,
        message
    };
};

const getServicesOptions = async ({ id }: GetServicesRequest): Promise<GetServicesOptionsResponse | null> => {
    const { responseBody: { data, success, message } } = await getFetchedData<GetServicesOptions, GetServicesRequest>({
        url: `${ managingServiceSubscriptionsUrl }/${ id }/options`,
        method: WebVerb.GET
    });

    return {
        data,
        success,
        message
    };
};

const getServicesCompatibleTemplates = async ({ id }: GetServicesRequest): Promise<GetServicesCompatibleTemplatesResponse | null> => {
    const { responseBody: { data, success, message } } = await getFetchedData<GetServicesCompatibleTemplates[], GetServicesRequest>({
        url: `${ managingServiceSubscriptionsUrl }/${ id }/compatible-templates`,
        method: WebVerb.GET
    });

    return {
        data,
        success,
        message
    };
};

const postCreateDb = async ({ id, templateId }: PostCreateDbRequest): Promise<PostServicesCreateDbResponse | null> => {
    const { responseBody: { data, success, message } } = await getFetchedData<boolean, PostCreateDbRequest>({
        url: `${ managingServiceSubscriptionsUrl }/${ id }${ toQueryParams({ accountId: accountId(), templateId }) }`,
        method: WebVerb.POST
    });

    return {
        data,
        success,
        message
    };
};

const getServicesUser = async ({ id, userLogin }: GetServicesUserRequest): Promise<GetServicesUserResponse | null> => {
    const { responseBody: { data, success, message } } = await getFetchedData<GetServicesUser, GetServicesUserRequest>({
        url: `${ managingServiceSubscriptionsUrl }/${ id }/user?userLogin=${ userLogin }`,
        method: WebVerb.GET
    });

    return {
        data,
        success,
        message
    };
};

const putServicesActivationDemo = async (requestParams: PutServicesActivationDemoRequest): Promise<PutServicesActivationDemoResponse | null> => {
    const { responseBody: { data, success, message } } = await getFetchedData<PutServicesActivationDemo, PutServicesActivationDemoRequest>({
        url: `${ managingServiceSubscriptionsUrl }/${ requestParams.serviceId }/activation/demo?accountId=${ requestParams.accountId }&usePromisePayment=${ requestParams.usePromisePayment }`,
        method: WebVerb.PUT,
    });

    return {
        data,
        success,
        message
    };
};

const putServicesPrecalculatedCost = async (requestParams: PutServicesPrecalculatedCostOrApplyRequest): Promise<PutServicesPrecalculatedCostResponse> => {
    const { responseBody: { data, success, message } } = await getFetchedData<PutServicesPrecalculatedCost[], PutServicesPrecalculatedCostOrApplyRequest>({
        url: `${ managingServiceSubscriptionsUrl }/precalculated-cost`,
        method: WebVerb.PUT,
        body: requestParams
    });

    return {
        data,
        success,
        message
    };
};

const putServicesApply = async (requestParams: PutServicesPrecalculatedCostOrApplyRequest): Promise<PutServicesApplyResponse> => {
    const { responseBody: { data, success, message } } = await getFetchedData<PutServicesApply, PutServicesPrecalculatedCostOrApplyRequest>({
        url: `${ managingServiceSubscriptionsUrl }/apply`,
        method: WebVerb.PUT,
        body: requestParams
    });

    return {
        data,
        success,
        message
    };
};

const getActivationStatus = async ({ id }: GetServicesRequest): Promise<TResponseLowerCase<boolean | null>> => {
    const { responseBody } = await getFetchedData<boolean, GetServicesRequest>({
        url: `${ managingServiceSubscriptionsUrl }/${ id }/activation-status`,
        method: WebVerb.GET
    });

    return responseBody;
};

const useGetActivationStatus = (id: string) => {
    return useFetch<boolean, void>({
        url: `${ managingServiceSubscriptionsUrl }/${ id }/activation-status`,
        method: WebVerb.GET
    });
};

const getExtensions = async (requestArgs: GetExtensionsRequest): Promise<GetExtensionsResponse> => {
    const { responseBody: { data, success, message } } = await getFetchedData<SelectDataMetadataResponseDto<GetExtensions>, GetServicesRequest & GetExtensionsRequest>({
        url: `${ managingServiceSubscriptionsUrl }/${ requestArgs.id }/extensions/compatible-databases${ toQueryParams({
            accountId: requestArgs.accountId,
            pageNumber: requestArgs.pageNumber ?? 1,
            pageSize: requestArgs.pageSize ?? 50
        }) }`,
        method: WebVerb.GET
    });

    return {
        data,
        success,
        message
    };
};

const postExtensions = async (requestArgs: PostExtensionsRequest): Promise<PostExtensionsResponse> => {
    const { responseBody: { data, success, message } } = await getFetchedData<boolean, PostExtensionsRequest>({
        url: `${ managingServiceSubscriptionsUrl }/extensions`,
        method: WebVerb.POST,
        body: requestArgs
    });

    return {
        data,
        success,
        message
    };
};

const getExtensionsState = async (requestArgs: GetExtensionsStateRequest): Promise<GetExtensionsStateResponse> => {
    const { responseBody: { data, success, message } } = await getFetchedData<GetExtensionsState, GetExtensionsStateRequest>({
        url: `${ managingServiceSubscriptionsUrl }/${ requestArgs.id }/extensions/state?accountDatabaseId=${ requestArgs.accountDatabaseId }`,
        method: WebVerb.GET
    });

    return {
        data,
        success,
        message
    };
};

const deleteExtensions = async (requestArgs: DeleteExtensions) => {
    await getFetchedData({
        url: `${ managingServiceSubscriptionsUrl }/${ requestArgs.id }/extensions?accountDatabaseId=${ requestArgs.accountDatabaseId }`,
        method: WebVerb.DELETE
    });
};

const getSponsorship = async (id: string, requestArgs: GetSponsorshipRequest): Promise<GetSponsorshipResponse> => {
    const { responseBody: { data, success, message } } = await getFetchedData<GetSponsorship, GetSponsorshipRequest>({
        url: `${ managingServiceSubscriptionsUrl }/${ id }/sponsorship/user${ toQueryParams(requestArgs) }`,
        method: WebVerb.GET
    });

    return {
        data,
        success,
        message
    };
};

const getMyDatabasesServices = async (serviceId: string): Promise<GetServicesMyDatabasesResponse> => {
    const { responseBody: { data, success, message } } = await getFetchedData<GetServicesMyDatabases, void>({
        url: `${ managingServiceSubscriptionsUrl }/my-databases?serviceId=${ serviceId }&accountId=${ localStorageHelper.getContextAccountId() }`,
        method: WebVerb.GET
    });

    return {
        data,
        success,
        message
    };
};

const postProlongDemo = async (body: PostProlongDemo): Promise<TResponseLowerCase<null>> => {
    const { responseBody: { data, success, message } } = await getFetchedData<null, PostProlongDemo>({
        url: `${ managingServiceSubscriptionsUrl }/prolong-demo`,
        method: WebVerb.POST,
        body
    });

    return {
        data,
        success,
        message
    };
};

const getAutoInstall = async (serviceId: string): Promise<TResponseLowerCase<GetAutoInstallResponse>> => {
    const { responseBody } = await getFetchedData<GetAutoInstallResponse, object>({
        url: `${ autoInstallServiceUrl }/${ contextAccountId() }/services/${ serviceId }`,
        method: WebVerb.GET,
        hasStandardResponseType: false
    });

    return responseBody;
};

const postAutoInstall = async (serviceId: string, autoinstall: boolean): Promise<TResponseLowerCase<object>> => {
    const { responseBody } = await getFetchedData<object, object>({
        url: `${ autoInstallServiceUrl }/${ contextAccountId() }/services/${ serviceId }`,
        method: WebVerb.POST,
        body: {
            autoinstall
        },
        hasStandardResponseType: false
    });

    return responseBody;
};

export const MANAGING_SERVICE_SUBSCRIPTIONS = {
    getServices,
    getServicesActivation,
    getBillingService,
    getActiveService,
    getServicesUsers,
    getServicesDatabase,
    getServicesDatabaseOnDelimiters,
    putServicesManagement,
    getServicesOptions,
    getServicesCompatibleTemplates,
    postCreateDb,
    getServicesUser,
    putServicesActivationDemo,
    putServicesPrecalculatedCost,
    putServicesApply,
    getActivationStatus,
    getExtensions,
    postExtensions,
    getExtensionsState,
    deleteExtensions,
    getSponsorship,
    getMyDatabasesServices,
    postProlongDemo,
    getAutoInstall,
    postAutoInstall,
    useGetActivationStatus,
    useGetBillingService,
    useGetService,
    useGetServiceMs
} as const;