export type editFastaResourcesRequestDto = {
    accountId: string;
    pagesResourceId: string;
    expireDate: string;
    pagesAmount: number;
    comment: string;
}