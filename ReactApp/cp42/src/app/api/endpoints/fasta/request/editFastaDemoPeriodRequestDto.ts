export type editFastaDemoPeriodRequestDto = {
    accountId: string;
    demoExpiredDate: string;
}