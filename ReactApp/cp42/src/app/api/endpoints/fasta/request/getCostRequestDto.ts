export type getCostRequestDto = {
    pagesAmount: number;
    yearsAmount: number;
}