export type getFastaDemoResponseDto = {
    accountId: string;
    demoExpiredDate: string;
    maxDemoExpiredDate: string;
}