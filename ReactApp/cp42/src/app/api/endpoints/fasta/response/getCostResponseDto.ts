export type getCostResponseDto = {
    fastaCost: number;
    recognitionCost: number;
    totalCost: number;
}