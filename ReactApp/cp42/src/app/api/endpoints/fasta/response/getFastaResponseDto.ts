export type getFastaResponseDto = {
    serviceNameRepresentation: {
        id: string;
        name: string;
        summary: string;
        imageUrl: string
    };
    pagesRemain: number;
    licensePeriod: number;
    defaultPagesTariff: number;
    expireDate: string;
    isFrozen: boolean;
    pagesTariffList: number[];
    currentPagesTariff: number;
    allowIncreaseDocLoaderByStandardRent1C: boolean;
    rent1CExpiredDate: string;
    accountLevel: number;
    locale: {
        name: string;
        currency: string
    };
    serviceStatus: {
        serviceIsLocked: boolean;
        serviceLockReason: string;
        serviceLockReasonType: number;
        promisedPaymentIsActive: boolean;
        promisedPaymentSum: string;
        promisedPaymentExpireDate: string;
    };
    isEsdlLocked: boolean;
}

export type getFastaResourcesResponseDto = {
    accountId: string;
    pagesLeft: number;
    currentExpireDate: string;
    expiredate: string;
    daysLeft: number;
    pagesResourceId: string;
    daysResourceId: string;
    pagesAmount: number;
    isFrozenEsdl: boolean;
    isFrozenRec42: boolean;
    isDemoPeriod: boolean;
}