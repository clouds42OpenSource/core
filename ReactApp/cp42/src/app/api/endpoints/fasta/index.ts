import { editFastaDemoPeriodRequestDto, editFastaResourcesRequestDto, getCostRequestDto } from 'app/api/endpoints/fasta/request';
import { getCostResponseDto, getFastaDemoResponseDto, getFastaResourcesResponseDto, getFastaResponseDto } from 'app/api/endpoints/fasta/response';
import { apiHost, getFetchedBlobData, getFetchedData, toQueryParams } from 'app/api/fetch';
import { TDataReturn, TResponseLowerCase } from 'app/api/types';
import { localStorageHelper } from 'app/common/helpers/LocalStorageHelper';

import { useFetch } from 'app/hooks';
import { WebVerb } from 'core/requestSender/enums';

const fastaUrl = apiHost('fasta');

const useGetFasta = (): TDataReturn<getFastaResponseDto> => {
    const { data, error, isLoading, refreshData } = useFetch<getFastaResponseDto, void>({
        url: fastaUrl,
        method: WebVerb.GET
    });

    return { data, error, isLoading, refreshData };
};

const getCalculatedCost = async (requestParam: getCostRequestDto): Promise<TResponseLowerCase<getCostResponseDto>> => {
    const { responseBody } = await getFetchedData<getCostResponseDto, getCostRequestDto>({
        url: `${ fastaUrl }/cost${ toQueryParams(requestParam) }`,
        method: WebVerb.GET,
    });

    return {
        success: responseBody.success,
        data: responseBody.data,
        message: responseBody.message
    };
};

const getFastaResources = async (): Promise<TResponseLowerCase<getFastaResourcesResponseDto>> => {
    const { responseBody } = await getFetchedData<getFastaResourcesResponseDto, void>({
        url: `${ fastaUrl }/resources`,
        method: WebVerb.GET
    });

    return responseBody;
};

const tryBuyFasta = async (requestParam: getCostRequestDto): Promise<TResponseLowerCase<getCostResponseDto>> => {
    const { responseBody } = await getFetchedData<getCostResponseDto, getCostRequestDto>({
        url: `${ fastaUrl }/buy`,
        method: WebVerb.PUT,
        body: requestParam
    });

    return {
        success: responseBody.success,
        data: responseBody.data,
        message: responseBody.message
    };
};

const editFastaResources = async (requestParam: editFastaResourcesRequestDto): Promise<TResponseLowerCase<boolean>> => {
    const { responseBody } = await getFetchedData<boolean, editFastaResourcesRequestDto>({
        url: `${ fastaUrl }/resources`,
        method: WebVerb.PUT,
        body: requestParam
    });

    return {
        success: responseBody.success,
        data: responseBody.data,
        message: responseBody.message
    };
};

const getFastaDemoPeriod = async (): Promise<TResponseLowerCase<getFastaDemoResponseDto>> => {
    const { responseBody } = await getFetchedData<getFastaDemoResponseDto, void>({
        url: `${ fastaUrl }/demo-period`,
        method: WebVerb.GET,
    });

    return {
        success: responseBody.success,
        data: responseBody.data,
        message: responseBody.message
    };
};

const editFastaDemoPeriod = async (requestParam: string): Promise<TResponseLowerCase<getCostResponseDto>> => {
    const { responseBody } = await getFetchedData<getCostResponseDto, editFastaDemoPeriodRequestDto>({
        url: `${ fastaUrl }/demo-period`,
        method: WebVerb.PUT,
        body: {
            accountId: localStorageHelper.getContextAccountId()!,
            demoExpiredDate: requestParam,
        },
    });

    return {
        success: responseBody.success,
        data: responseBody.data,
        message: responseBody.message
    };
};

const getFastaProgram = async (): Promise<TResponseLowerCase<Blob>> => {
    const { responseBody } = await getFetchedBlobData<void>({
        url: `${ fastaUrl }/program`,
        method: WebVerb.GET,
    });

    return {
        success: true,
        data: responseBody,
        message: null
    };
};

export const FASTA = {
    useGetFasta,
    getCalculatedCost,
    getFastaResources,
    tryBuyFasta,
    editFastaResources,
    getFastaDemoPeriod,
    editFastaDemoPeriod,
    getFastaProgram
} as const;