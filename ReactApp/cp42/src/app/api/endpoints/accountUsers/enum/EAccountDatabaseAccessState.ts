export const enum EAccountDatabaseAccessState {
    done,
    processingGrant,
    processingDelete,
    error,
    undefined
}