export type TChangeDelimitersDatabaseStatusLoggerParams = {
    databaseId: string;
    userId: string;
    databaseName: string;
    userName: string;
    v82Name: string;
    isExternal?: boolean;
};