export type TProfiles = {
    admin: boolean;
    default: boolean;
    description: string;
    error: boolean;
    id: string;
    name: string;
    predefined: boolean;
};

type TApplications = {
    id: string;
    state: string;
    profiles: TProfiles[];
    allowBackup: boolean;
};

export type TGetGivenAccountProfilesResponse = {
    permissions: TApplications[];
};