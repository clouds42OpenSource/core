import { SelectDataResultMetadataModel } from 'app/web/common';

export type TUserItem = {
    id: string;
    login: string;
    firstName: string;
    lastName: string;
    middleName: string;
    fullName: string;
    email: string;
    phoneNumber: string;
    activated: boolean;
    createdInAd: boolean;
    unsubscribed: boolean;
    creationDate: string;
    isPhoneVerified: boolean;
    isEmailVerified: boolean;
    accountRoles: string[];
};

export type TGetAllUserList = SelectDataResultMetadataModel<TUserItem>;