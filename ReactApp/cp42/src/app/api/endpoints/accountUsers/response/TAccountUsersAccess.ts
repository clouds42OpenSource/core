import { EAccountDatabaseAccessState } from '../enum';

export type TDatabaseAccesses = {
    databaseId: string;
    v82Name: string;
    hasAccess: boolean;
    caption: string;
    isFile: boolean;
    isDbOnDelimiters: boolean;
    myDatabasesServiceTypeId: string;
    state: EAccountDatabaseAccessState;
    configurationName: string | null;
    templateImageName: string;
    configurationCode: string | null;
};

export type TAccountUsersAccessResponse = {
    accessToServerDatabaseServiceTypeId: string;
    databaseAccesses: TDatabaseAccesses[];
    currency: string;
    myDatabaseBillingServiceId: string;
};