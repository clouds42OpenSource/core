type TResults = {
    'application-id': string;
    status: 'success' | 'failure';
    error?: string;
    'user-id': string;
};

export type TChangeDatabaseAccessResponse = {
    message?: string;
    results?: TResults[];
};