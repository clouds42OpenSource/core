export type TPutAccountUserAccessResponse = {
    message: string;
    success: boolean;
    userId: string | null;
    userName: string | null;
    databasesName: string | null;
    userEmail: string | null;
};