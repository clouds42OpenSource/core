export type TAccountProfile = {
    id: string;
    name: string;
    admin: boolean;
    predefined: boolean;
    databases: string[];
};

export type TGetAccountProfileResponse = {
    code: string;
    name: string;
    profiles: TAccountProfile[];
};