export type TPutAccountUserAccessRequest = {
    usersId: string;
    databaseIds: string [];
    giveAccess: boolean;
};