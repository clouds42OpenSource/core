type TProfiles = {
    id: string;
    name: string;
    admin: boolean;
    predefined: boolean;
};

export type TPermissions = {
    'application-id': string;
    'user-id': string;
    profiles: TProfiles[];
    'allow-backup'?: boolean;
}

export type TChangeDatabaseAccessRequest = {
    permissions: TPermissions[];
};