import { TGetAllUserList } from 'app/api/endpoints/accountUsers/response/TGetAllUserList';
import { TChangeDelimitersDatabaseStatusLoggerParams } from 'app/api/endpoints/accountUsers/type';
import { ELogEvent } from 'app/api/endpoints/global/enum';
import { TLocale } from 'app/api/endpoints/global/response';
import { apiHost, contextAccountId, getFetchedData, msHost, templateErrorText } from 'app/api/fetch';
import { getReturnLightObject } from 'app/api/utility';
import { getAccountProfilesListSlice, getDatabaseAccessUsersListSlice, getGivenAccountProfilesListSlice } from 'app/modules/accountUsers';
import { TAppDispatch } from 'app/redux';
import { changeToCamelCase, removeDashesFromKeys } from 'app/web/common';
import { WebVerb } from 'core/requestSender/enums';
import { sendLogEvent } from '../global';

import { TChangeDatabaseAccessRequest, TPutAccountUserAccessRequest } from './request';
import { TAccountUsersAccessResponse, TChangeDatabaseAccessResponse, TGetAccountProfileResponse, TGetGivenAccountProfilesResponse, TPutAccountUserAccessResponse } from './response';

const getDatabaseAccessUsersList = (dispatch: TAppDispatch, userId: string, needShowLoading = true, isExternalUser = false) => {
    const { loading, success: successAction, error, empty } = getDatabaseAccessUsersListSlice.actions;

    if (needShowLoading) {
        dispatch(loading());
    }

    getFetchedData<TAccountUsersAccessResponse, void>({
        url: apiHost(`account-users/${ userId }/${ isExternalUser ? `external-access?accountId=${ contextAccountId() }` : 'access' }`),
        method: WebVerb.GET
    }).then(({ responseBody: { data, success, message } }) => {
        dispatch(empty());

        if (success && data && !message) {
            dispatch(successAction(data));
        } else {
            dispatch(error(message ?? templateErrorText));
        }
    });
};

const getAllUserList = async (searchLine: string) => {
    const { responseBody } = await getFetchedData<TGetAllUserList, void>({
        url: apiHost(`account-users?filter.searchLine=${ searchLine }`),
        method: WebVerb.GET
    });

    return responseBody;
};

const getAccountProfilesList = (dispatch: TAppDispatch, locale?: TLocale, needShowLoading = false) => {
    const { loading, success: successAction, error, empty } = getAccountProfilesListSlice.actions;

    if (needShowLoading) {
        dispatch(loading());
    }

    getFetchedData<TGetAccountProfileResponse[], void>({
        url: msHost(`pl42/hs/Platform/accounts/${ contextAccountId() }/profiles${ locale ? `?locale=${ locale }` : '' }`),
        method: WebVerb.GET,
        hasStandardResponseType: false
    }).then(({ responseBody: { message, data } }) => {
        dispatch(empty());

        if (data && !message) {
            dispatch(successAction(data));
        } else {
            dispatch(error(message ?? templateErrorText));
        }
    });

    return { type: getAccountProfilesListSlice.name };
};

const getGivenAccountProfilesList = (dispatch: TAppDispatch, userId?: string, databaseId?: string, needShowLoading = true) => {
    const { loading, success: successAction, error } = getGivenAccountProfilesListSlice.actions;

    if (needShowLoading) {
        dispatch(loading());
    }

    getFetchedData<TGetGivenAccountProfilesResponse, void>({
        url: msHost(`pl42/hs/Platform/permissions?${ userId ? `user-id=${ userId }` : '' }${ databaseId ? `application-id=${ databaseId }` : '' }&account-id=${ contextAccountId() }`),
        method: WebVerb.GET,
        hasStandardResponseType: false
    }).then(({ responseBody: { message, data } }) => {
        if (data && !message) {
            dispatch(successAction(removeDashesFromKeys(data)));
        } else {
            dispatch(error(message ?? templateErrorText));
        }
    });
};

const putDatabaseAccess = async (body: TPutAccountUserAccessRequest) => {
    const { responseBody } = await getFetchedData<TPutAccountUserAccessResponse[], TPutAccountUserAccessRequest>({
        url: apiHost('account-users/access'),
        method: WebVerb.POST,
        body: changeToCamelCase(body, true)
    });

    return getReturnLightObject(responseBody);
};

const changeDatabaseAccessOnDelimiters = async (body: TChangeDatabaseAccessRequest, loggerParams: TChangeDelimitersDatabaseStatusLoggerParams[]) => {
    const { responseBody } = await getFetchedData<TChangeDatabaseAccessResponse, TChangeDatabaseAccessRequest>({
        url: msHost('pl42/hs/Platform/permissions'),
        method: WebVerb.POST,
        body,
        hasStandardResponseType: false
    });

    body.permissions.forEach(permission => {
        const hasAccessError = !!responseBody.data?.results?.find(result => result['user-id'] === permission['user-id'] && permission['application-id'] === result['application-id'] && result.status === 'failure');
        const currentLoggerParams = loggerParams.find(param => param.userId === permission['user-id'] && param.databaseId === permission['application-id']);
        const templateString = `доступ ${ currentLoggerParams?.isExternal ? 'внешнему' : 'внутреннему' } пользователю "${ currentLoggerParams?.userName }" к базе на разделителях "${ currentLoggerParams?.databaseName }" (${ currentLoggerParams?.v82Name })`;

        if (!hasAccessError) {
            if (permission.profiles.length === 0) {
                void sendLogEvent(ELogEvent.removeAccessToInfoBase, `Удален ${ templateString }.`);
            } else {
                let profilesList: string;
                const adminProfile = permission.profiles.find(profile => profile.admin);

                if (adminProfile) {
                    profilesList = adminProfile.name;
                } else {
                    profilesList = permission.profiles.map(profile => profile.name).join(', ');
                }

                void sendLogEvent(ELogEvent.addAccessToInfoBase, `Предоставлен ${ templateString } с следующими правами "${ profilesList }"`);
            }
        }
    });

    return getReturnLightObject(responseBody);
};

export const ACCOUNT_USERS_REDUX = {
    getDatabaseAccessUsersList,
    getAccountProfilesList,
    getGivenAccountProfilesList
} as const;

export const ACCOUNT_USERS = {
    putDatabaseAccess,
    changeDatabaseAccessOnDelimiters,
    getAllUserList
} as const;