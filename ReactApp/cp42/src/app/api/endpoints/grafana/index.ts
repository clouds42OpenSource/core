import { WebVerb } from 'core/requestSender/enums';
import { TDataReturn } from 'app/api/types';
import { useFetch } from 'app/hooks';
import {
    getGrafanaDashboardResponseDto,
    getGrafanaTokenResponseDto
} from './response';

const params = {
    noAuthorizationToken: true,
    noCredentials: true,
    hasStandardResponseType: false,
};

export const useGetGrafanaDashboards = (id?: string): TDataReturn<getGrafanaDashboardResponseDto[]> => {
    return useFetch<getGrafanaDashboardResponseDto[], void>({
        url: `${ import.meta.env.REACT_GRAFANA_URL }/search?type=dash-db&tag=${ id }&tag=clients`,
        method: WebVerb.GET,
        isRequestBlocked: !id,
        ...params
    });
};

export const useGetGrafanaToken = (uid?: string): TDataReturn<getGrafanaTokenResponseDto> => {
    return useFetch<getGrafanaTokenResponseDto, void>({
        url: `${ import.meta.env.REACT_GRAFANA_URL }/dashboards/uid/${ uid }/public-dashboards`,
        method: WebVerb.GET,
        isRequestBlocked: !uid,
        ...params
    });
};

export const useGetApdexDashboards = (clientId?: string): TDataReturn<getGrafanaDashboardResponseDto[]> => {
    return useFetch<getGrafanaDashboardResponseDto[], void>({
        url: `${ import.meta.env.REACT_GRAFANA_URL }/search?type=dash-db&tag=APDEX&tag=${ clientId }`,
        method: WebVerb.GET,
        isRequestBlocked: !clientId,
        ...params
    });
};

export const GRAFANA = {
    useGetGrafanaDashboards,
    useGetGrafanaToken
} as const;