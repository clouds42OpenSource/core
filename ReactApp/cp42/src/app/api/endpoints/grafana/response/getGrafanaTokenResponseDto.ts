export type getGrafanaTokenResponseDto = {
    uid: string;
    dashboardUid: string;
    accessToken: string;
    createdBy: number;
    updatedBy: number;
    createdAt: string;
    updatedAt: string;
    timeSelectionEnabled: boolean;
    isEnabled: boolean;
    annotationsEnabled: boolean;
    share: string
};