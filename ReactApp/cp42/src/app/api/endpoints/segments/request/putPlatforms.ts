export type PutPlatforms = {
    segmentIds: string[];
    stable83VersionId: string;
};