export type GetAllSegmentsResponse = {
    id: string;
    name: string;
};