import { PutPlatforms } from 'app/api/endpoints/segments/request';
import { GetAllSegmentsResponse } from 'app/api/endpoints/segments/response';
import { apiHost, getFetchedData } from 'app/api/fetch';
import { WebVerb } from 'core/requestSender/enums';

const segmentsUrl = apiHost('segments');

const getAllSegments = async () => {
    const { responseBody } = await getFetchedData<GetAllSegmentsResponse[], void>({
        url: `${ segmentsUrl }/list`,
        method: WebVerb.GET
    });

    return responseBody;
};

const putPlatforms = async (params: PutPlatforms) => {
    const { responseBody } = await getFetchedData<string, PutPlatforms>({
        url: `${ segmentsUrl }/edit-platform`,
        method: WebVerb.PUT,
        body: params
    });

    return responseBody;
};

export const SEGMENTS = {
    getAllSegments,
    putPlatforms
} as const;