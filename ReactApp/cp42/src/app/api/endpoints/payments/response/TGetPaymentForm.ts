export type TGetPaymentFormResponse = {
    amount: number;
    encoding: string;
    order: string;
    payboxUrl: string | null;
    paymentSystem: number;
    requestUrl: string | null;
    robokassaUrl: string | null;
    serviceId: string;
    successurl: string;
};