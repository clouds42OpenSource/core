import { TGetPaymentFormResponse } from 'app/api/endpoints/payments/response';
import { apiHost, contextAccountId, getFetchedData } from 'app/api/fetch';
import { WebVerb } from 'core/requestSender/enums';

const addHidenInput = (formName: string, value: string) => {
    const input = document.createElement('input');
    input.setAttribute('type', 'hidden');
    input.setAttribute('name', formName);
    input.setAttribute('value', value);

    return input;
};

export const getPaymentForm = async (totalSum: number) => {
    const { responseBody: { data } } = await getFetchedData<TGetPaymentFormResponse, void>({
        url: apiHost(`api_v2/Payments/${ contextAccountId() }/pay?totalSum=${ totalSum }`),
        method: WebVerb.GET
    });

    if (data) {
        const form = document.createElement('form');
        form.setAttribute('method', 'POST');
        form.setAttribute('action', data.requestUrl ?? '');

        form.appendChild(addHidenInput('service_id', data.serviceId));
        form.appendChild(addHidenInput('order', data.order));
        form.appendChild(addHidenInput('sus_url', data.successurl));
        form.appendChild(addHidenInput('amount', data.amount.toString()));
        form.appendChild(addHidenInput('charset', data.encoding));

        document.body.appendChild(form);

        form.submit();
    }
};

export const PAYMENTS = {
    getPaymentForm
} as const;