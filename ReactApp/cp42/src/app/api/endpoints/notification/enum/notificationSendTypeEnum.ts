export enum NotificationSendTypeEnum {
    OnlyActiveUsers = 1,
    UsersWithConnectedService,
    OnlyAccountAdminsOrManagers,
    ByUsersIdsList,
}