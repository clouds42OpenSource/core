export enum ETypeNotification {
    Email = 1,
    Sms,
    Telegram,
    WhatsApp
}

export type TNotificationSettingsItem = {
    code: null | string;
    id?: string;
    notification: {
        type: ETypeNotification;
        typeText?: string;
    };
    isActive: boolean;
    hiddenElement?: boolean;
};

export type TPostNotificationSettings = {
    accountUserId: string;
    notificationSettings: TNotificationSettingsItem[];
};