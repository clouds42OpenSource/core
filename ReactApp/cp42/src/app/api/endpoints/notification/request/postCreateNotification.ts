import { NotificationSendTypeEnum } from 'app/api/endpoints/notification/enum';
import { EStateNotification } from 'app/api/endpoints/notification/response';

export type TPostCreateNotification = {
    ignoreMailSending: boolean;
    userIds?: string[];
    message: string;
    header: string;
    subject: string;
    state: EStateNotification;
    notificationSendType: NotificationSendTypeEnum;
};