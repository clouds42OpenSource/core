export type TUpdateNotifications = {
    idList?: string[];
    accountUserId?: string;
};