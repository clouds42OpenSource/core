import { TResponseLowerCase } from 'app/api/types';
import { SelectDataMetadataResponseDto } from 'app/web/common';

export enum EStateNotification {
    normal,
    critical,
    information,
    review
}

export type Notification = {
    id: string;
    message: string;
    createdOn: string;
    accountUserId: string;
    isRead: boolean;
    state: EStateNotification;
};

export type GetNotifications = TResponseLowerCase<SelectDataMetadataResponseDto<Notification>>;