import { TUpdateNotifications } from 'app/api/endpoints/notification/request';
import { TPostCreateNotification } from 'app/api/endpoints/notification/request/postCreateNotification';
import { TNotificationSettingsItem, TPostNotificationSettings } from 'app/api/endpoints/notification/request/postNotificationSettings';
import { Notification } from 'app/api/endpoints/notification/response';
import { apiHost, getFetchedData, userId } from 'app/api/fetch';
import { getNotificationsSlice } from 'app/modules/notifications/reducers/getNotificationsSlice';
import { TAppDispatch } from 'app/redux';
import { SelectDataMetadataResponseDto } from 'app/web/common';
import { WebVerb } from 'core/requestSender/enums';

const notificationUrl = apiHost('notifications');
const notificationSettingsUrl = apiHost('notification-settings');

const { success, error } = getNotificationsSlice.actions;

const getNotifications = async (dispatch: TAppDispatch, userId: string) => {
    const { responseBody } = await getFetchedData<SelectDataMetadataResponseDto<Notification>, void>({
        url: `${ notificationUrl }?filter.accountUserId=${ userId }&pageSize=1000`,
        method: WebVerb.GET
    });

    if (responseBody.success && responseBody.data) {
        dispatch(success(responseBody.data));
    } else {
        dispatch(error(responseBody.message ?? ''));
    }
};

const deleteNotification = async ({ idList, accountUserId }: TUpdateNotifications) => {
    const { responseBody } = await getFetchedData({
        url: `${ notificationUrl }?${
            idList
                ? idList.map(item => `messageIds=${ item }`).join('&')
                : `accountUserId=${ accountUserId }`
        }`,
        method: WebVerb.DELETE
    });

    return responseBody;
};

const readNotification = async ({ idList, accountUserId }: TUpdateNotifications) => {
    const { responseBody } = await getFetchedData({
        url: `${ notificationUrl }/mark-is-read`,
        method: WebVerb.POST,
        body: {
            messageIds: idList,
            accountUserId
        }
    });

    return responseBody;
};

const changeNotificationSettings = async (params: TNotificationSettingsItem[]) => {
    const { responseBody } = await getFetchedData<object, TPostNotificationSettings>({
        url: `${ notificationSettingsUrl }`,
        method: WebVerb.POST,
        body: {
            accountUserId: userId(),
            notificationSettings: params
        }
    });

    return responseBody;
};

export const createNotification = async (body: TPostCreateNotification) => {
    const { responseBody } = await getFetchedData<object, TPostCreateNotification>({
        url: notificationUrl,
        method: WebVerb.POST,
        body
    });

    return responseBody;
};

export const NOTIFICATIONS = {
    deleteNotification,
    readNotification,
    changeNotificationSettings,
    createNotification
} as const;

export const NOTIFICATIONS_REDUX = {
    getNotifications
} as const;