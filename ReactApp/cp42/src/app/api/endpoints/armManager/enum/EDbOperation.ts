export const enum EDbOperation {
    undefined = -1,
    autoUpdate,
    techSupport,
    planAutoUpdate,
    planTehSupport,
    authToDb,
    autoUpdateRequest,
}