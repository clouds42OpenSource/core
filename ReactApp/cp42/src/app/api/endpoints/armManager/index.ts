import { TAppDispatch } from 'app/redux';
import { apiHost, getFetchedData, templateErrorText, toQueryParams } from 'app/api/fetch';
import { getArmConfigurationReleaseSlice, getArmDetailsSlice, getArmPeriodSlice } from 'app/modules/armManager';
import { TChangeArmConfigurationReleaseRequest, TGetArmConfigurationReleasesRequest, TGetArmDetailsRequest, TGetArmPeriodRequest } from 'app/api/endpoints/armManager/request';
import { TGetArmConfigurationReleasesResponse, TGetArmDetailsResponse, TGetArmPeriodResponse } from 'app/api/endpoints/armManager/response';
import { WebVerb } from 'core/requestSender/enums';
import { getReturnLightObject } from 'app/api/utility';

const armManagerHost = (endpoint: string) => `${ apiHost('arm') }/${ endpoint }`;

const getArmPeriod = (dispatch: TAppDispatch, params: TGetArmPeriodRequest) => {
    dispatch(getArmPeriodSlice.actions.loading());

    getFetchedData<TGetArmPeriodResponse[], TGetArmPeriodRequest>({
        url: `${ armManagerHost('period') }`,
        method: WebVerb.POST,
        body: params
    }).then(({ responseBody: { data, success, message } }) => {
        if (success && data && !message) {
            dispatch(getArmPeriodSlice.actions.success(data));
        } else {
            dispatch(getArmPeriodSlice.actions.error(message ?? templateErrorText));
        }
    });
};

const getArmDetails = (dispatch: TAppDispatch, params: TGetArmDetailsRequest) => {
    dispatch(getArmDetailsSlice.actions.loading());

    getFetchedData<TGetArmDetailsResponse[], TGetArmDetailsRequest>({
        url: `${ armManagerHost('details') }`,
        method: WebVerb.POST,
        body: params
    }).then(({ responseBody: { data, success, message } }) => {
        if (success && data && !message) {
            dispatch(getArmDetailsSlice.actions.success(data));
        } else {
            dispatch(getArmDetailsSlice.actions.error(message ?? templateErrorText));
        }
    });
};

const getArmConfigurationReleases = (dispatch: TAppDispatch, query: TGetArmConfigurationReleasesRequest) => {
    dispatch(getArmConfigurationReleaseSlice.actions.loading());

    getFetchedData<TGetArmConfigurationReleasesResponse, void>({
        url: `${ armManagerHost('configuration-releases') }${ toQueryParams(query) }`,
        method: WebVerb.GET
    }).then(({ responseBody: { data, success, message } }) => {
        if (success && data && !message) {
            dispatch(getArmConfigurationReleaseSlice.actions.success(data));
        } else {
            dispatch(getArmConfigurationReleaseSlice.actions.error(message ?? templateErrorText));
        }
    });
};

const changeArmConfigurationRelease = async (query: TChangeArmConfigurationReleaseRequest) => {
    const params = await getFetchedData<boolean, void>({
        url: `${ armManagerHost('configuration-release') }${ toQueryParams(query) }`,
        method: WebVerb.PUT
    });

    return getReturnLightObject(params.responseBody);
};

export const ARM_MANAGER = {
    changeArmConfigurationRelease,
} as const;

export const ARM_MANAGER_REDUX = {
    getArmPeriod,
    getArmDetails,
    getArmConfigurationReleases
} as const;