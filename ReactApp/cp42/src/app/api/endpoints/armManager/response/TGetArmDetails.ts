export type TGetArmDetailsResponse = {
    accountId: string;
    accountDatabaseId: string;
    accountCaption: string;
    accountDatabaseName: string;
    accountDatabaseNumber: string;
    accountNumber: string;
    date: string;
    currentRelease: string;
    previousRelease: string;
    description: string;
};