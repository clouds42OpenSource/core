export type TGetArmPeriodResponse = {
    dateString: string;
    date: string;
    tehSupportPlanCount: number;
    tehSupportFactCount: number;
    autoUpdatePlanCount: number;
    autoUpdateFactCount: number;
    errorsCount: number;
    errorsConnectCount: number;
};