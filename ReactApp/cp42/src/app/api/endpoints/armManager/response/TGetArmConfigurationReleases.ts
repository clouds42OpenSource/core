export type TGetArmConfigurationReleasesResponse = {
    id: string;
    name: string;
    version: string;
    initiator: string;
    validateState: boolean;
};