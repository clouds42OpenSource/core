import { EDbOperation } from '../enum';

export type TGetArmPeriodFilterRequest = {
    startRange?: string;
    endRange?: string;
    dbOperation?: EDbOperation;
    configurations?: string[];
};

export type TGetArmPeriodRequest = {
    filter?: TGetArmPeriodFilterRequest;
};