export type TChangeArmConfigurationReleaseRequest = {
    configurationId: string;
    approvalStatus: boolean;
};