import { EDbOperation } from 'app/api/endpoints/armManager/enum';

export type TGetArmConfigurationReleasesRequest = {
    'filter.startRange'?: string;
    'filter.endRange'?: string;
    'filter.dbOperation'?: EDbOperation;
};