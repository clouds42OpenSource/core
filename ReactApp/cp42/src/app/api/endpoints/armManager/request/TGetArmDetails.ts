import { TGetArmPeriodRequest } from './TGetArmPeriod';

export type TGetArmDetailsRequest = TGetArmPeriodRequest;