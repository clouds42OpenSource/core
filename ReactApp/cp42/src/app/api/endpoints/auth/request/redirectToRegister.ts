export type RedirectToRegister = {
    userSource?: string;
    cloudService?: string,
    cloudServiceId?: string,
    configuration1C?: string,
    referralAccountId?: string,
    registrationSource?: number
}