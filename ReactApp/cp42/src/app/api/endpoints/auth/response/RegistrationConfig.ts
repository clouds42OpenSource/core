export type RegistrationConfig = {
    cloudService: string;
    cloudServiceId: string;
    configuration1C: string;
    referralAccountId: string;
    registrationSource: number;
};