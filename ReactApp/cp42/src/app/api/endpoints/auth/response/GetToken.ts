export type GetToken = {
    accessToken: string;
    expiresIn: number;
    refreshToken: string;
    tokenType: string;
};