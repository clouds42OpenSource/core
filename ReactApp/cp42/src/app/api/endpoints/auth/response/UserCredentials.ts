export type AuthToken = {
    access_token: string;
    expires_in: number;
    refresh_token: string;
    token_type: string;
    error_code?: number;
    error_message?: string;
};