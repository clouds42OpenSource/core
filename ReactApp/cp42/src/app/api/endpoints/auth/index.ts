import { getTokenMapFrom } from 'app/api/endpoints/auth/functions/getTokenMapFrom';
import { AuthToken } from 'app/api/endpoints/auth/response/UserCredentials';
import { getFetchedData, getLogout, msProvider } from 'app/api/fetch';
import { AppRoutes } from 'app/AppRoutes';
import { localStorageHelper } from 'app/common/helpers/LocalStorageHelper';
import { GetToken } from 'app/api/endpoints/auth/response/GetToken';
import { WebVerb } from 'core/requestSender/enums';

const authUrl = (cmd: string) => msProvider(`oid/hs/oid2op?cmd=${ cmd }`);
export const registerUrlFrame = msProvider('oid/hs/ExternalRegistration/iframe');

const postAuth = async (formBody: string) => {
    const { responseBody } = await getFetchedData<GetToken, string>({
        url: authUrl('auth'),
        method: WebVerb.POST,
        body: formBody,
        isFormData: true,
        noAuthorizationToken: true,
        hasStandardResponseType: false
    });

    return responseBody;
};

const postCheckToken = async (checkData?: string[]) => {
    let formBody = [
        'openid.42clouds.jwt=true',
        'openid.42clouds.jwt_scope=core',
        'openid.42clouds.jwt_audience=CP',
        `openid.42clouds.refresh_token=${ localStorageHelper.getRefreshToken() }`
    ];

    if (checkData) {
        const [uid, user] = checkData;
        formBody = [
            `openid.auth.uid=${ encodeURIComponent(uid) }`,
            `openid.auth.user=${ encodeURIComponent(user) }`,
            'openid.42clouds.jwt=true',
            'openid.42clouds.jwt_scope=core',
            'openid.42clouds.jwt_audience=CP',
        ];
    }

    const { responseBody, status } = await getFetchedData<AuthToken, string>({
        url: authUrl('check'),
        method: WebVerb.POST,
        body: formBody.join('&'),
        isFormData: true,
        noAuthorizationToken: true,
        hasStandardResponseType: false
    });

    if (status === 400) {
        getLogout();
    }

    return getTokenMapFrom(responseBody);
};

const postLogout = async () => {
    await getFetchedData<null, string>({
        url: authUrl('logout'),
        method: WebVerb.POST,
        body: `openid.42clouds.refresh_token=${ localStorageHelper.getRefreshToken() }`,
        noAuthorizationToken: true,
        isFormData: true
    });
    localStorageHelper.removeAllUserItems();
    window.location.href = AppRoutes.signInRoute;
};

export const AUTH = {
    postAuth,
    postCheckToken,
    postLogout,
} as const;