/**
 * Функция для получения срока истeчения Токена
 * @param expiry срок хранения в секундах
 * @returns expiry + currentDate в секундах
 */

export const getTokenExpiry = (expiry: number) => new Date().getTime() / 1000 + expiry;