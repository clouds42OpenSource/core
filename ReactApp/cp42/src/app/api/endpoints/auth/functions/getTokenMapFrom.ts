import { TResponseLowerCase } from 'app/api/types';
import { Credentials } from 'app/api/endpoints/global/enum';
import { AuthToken } from 'app/api/endpoints/auth/response/UserCredentials';
import { getTokenExpiry } from 'app/api/endpoints/auth/functions/getTokenExpiry';
import { GetToken } from 'app/api/endpoints/auth/response/GetToken';
import Cookies from 'js-cookie';

export function getTokenMapFrom(value: TResponseLowerCase<AuthToken>): TResponseLowerCase<GetToken | null> {
    if (value.data?.error_message) {
        return {
            success: false,
            data: null,
            message: value.data?.error_message
        };
    }

    const accessToken = value.data?.access_token ?? '';
    const refreshToken = value.data?.refresh_token ?? '';
    const expiresIn = getTokenExpiry(value.data?.expires_in ?? 0);
    const tokenType = value.data?.token_type ?? '';
    const domain = window.location.host.match(/\.42clouds\.(.+)/)?.[0] ?? '';

    Cookies.set(Credentials.RefreshToken, refreshToken, { domain, expires: 1, secure: true });
    Cookies.set(Credentials.JsonWebToken, accessToken, { domain, expires: expiresIn, secure: true });
    Cookies.set(Credentials.ExpiresIn, expiresIn.toString(), { domain, expires: 1, secure: true });

    if (domain.includes('pro')) {
        Cookies.set(Credentials.JsonWebToken, accessToken, { domain: '.42clouds.com', expires: expiresIn, secure: true });
    }

    return {
        success: true,
        data: {
            accessToken,
            tokenType,
            expiresIn,
            refreshToken
        },
        message: null
    };
}