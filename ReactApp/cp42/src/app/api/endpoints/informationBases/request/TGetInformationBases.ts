import { EInformationBasesType } from '../enum';

export type TGetInformationBasesRequest = {
    search?: string;
    page?: number;
    size?: number;
    affiliation?: number;
    field?: string;
    sort?: number;
    type?: EInformationBasesType;
    accountID?: string;
    accountUserId?: string;
}