export type TClearCacheRequest = {
    accountDatabaseId: string;
    clearDatabaseLogs?: boolean;
}

export type TClearCacheRequestDto = {
    AccountDatabaseId: string;
    ClearDatabaseLogs?: boolean;
}