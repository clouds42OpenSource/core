export type TGetAccessRequest = {
    databaseId: string;
    searchString?: string;
    usersByAccessFilterType?: number;
}

export type TGetAccessRequestDto = {
    DatabaseId: string;
    SearchString?: string;
    UsersByAccessFilterType?: number;
}