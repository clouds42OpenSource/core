export type TSignInSupportData = {
    databaseId: string;
    login: string;
    password: string;
};