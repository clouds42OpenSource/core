export type TInputDatabaseId = {
    databaseId: string;
    adminPhoneNumber?: string;
    databaseAdminLogin?: string;
    databaseAdminPassword?: string;
}