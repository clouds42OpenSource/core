export type TChangedUserItem = {
    billingServiceTypeId: string;
    sponsorship: {
        i: boolean;
        label: string;
        me: boolean;
    }
    status: boolean;
    subject: string;
}

export type TPutTryPayAccessRequest = {
    accountId: string;
    billingServiceId: string;
    changedUsers: TChangedUserItem[];
    usePromisePayment: boolean;
}