export type TDeleteDatabasesRequest = {
    databasesId: string[];
}