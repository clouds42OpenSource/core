export type TPutAccessUsersRequest = {
    databaseId: string;
    usersId: string[];
    giveAccess: boolean;
}