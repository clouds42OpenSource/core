import { DatabaseState, DistributionType, PlatformType, AccountDatabaseRestoreModelType } from 'app/common/enums';

export type TEditInformationBaseRequest = {
    databaseId: string;
    v82Name: string;
    databaseCaption: string;
    databaseTemplateId: string;
    platformType: PlatformType;
    distributionType: DistributionType;
    usedWebServices: boolean;
    databaseState: DatabaseState;
    fileStorageId: string;
    restoreModel: AccountDatabaseRestoreModelType
};