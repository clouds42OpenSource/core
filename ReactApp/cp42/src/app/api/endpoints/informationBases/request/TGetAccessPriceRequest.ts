export type TAccountUserDataModelsListItem = {
    userId: string;
    hasAccess: boolean;
    state?: number;
    hasLicense?: boolean;
}

export type TGetAccessPriceRequest = {
    serviceTypesIdsList: (string | null)[];
    accountUserDataModelsList: TAccountUserDataModelsListItem[]
};