export type TPostPublishDatabaseRequest = {
    databaseId: string;
    publish: boolean;
};