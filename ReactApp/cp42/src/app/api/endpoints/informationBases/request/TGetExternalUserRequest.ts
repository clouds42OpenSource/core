export type TGetExternalUserRequest = {
    databaseId: string;
    accountUserEmail: string;
};