import { TCreateStatusResponse } from 'app/api/endpoints/createDb/response';
import {
    TGetAccess,
    TGetAccessPrice,
    TGetAvailability,
    TGetExternalUser,
    TGetInformationBaseInfoResponse,
    TGetSupportData,
    TInformationBaseItem,
    TInformationBasesList,
    TPutAccessUsers,
    TPutTryPayAccess
} from 'app/api/endpoints/informationBases/response';
import { apiHost, contextAccountId, getFetchedData, msHost, toQueryParams, userId } from 'app/api/fetch';
import { useFetch } from 'app/hooks';
import { informationBasesList } from 'app/modules/informationBases';
import { TAppDispatch } from 'app/redux';
import { WebVerb } from 'core/requestSender/enums';
import { EInformationBasesType } from './enum';
import {
    TClearCacheRequest,
    TClearCacheRequestDto,
    TDeleteDatabasesRequest,
    TEditInformationBaseRequest,
    TEditSupportData,
    TGetAccessPriceRequest,
    TGetAccessRequest,
    TGetAccessRequestDto, TGetExternalUserRequest,
    TGetInformationBasesRequest,
    TInputDatabaseId,
    TPostPublishDatabaseRequest,
    TPutAccessUsersRequest,
    TPutTryPayAccessRequest,
    TSignInSupportData
} from './request';

const {
    setLoading,
    getInformationBasesList,
    getOtherInformationBasesList,
    getAvailability,
    updateDatabasesStates,
    updateDatabaseOnDelimitersStates,
    setError,
    getInformationBase,
    updateAccessPrice,
    changeAccessInfoDb,
    setInitialAccess,
    changeValueCheck,
    addRateData
} = informationBasesList.actions;

const informationBasesHost = apiHost('api_v2/AccountDatabases');
const informationBasesMsHost = msHost('pl42/hs/Platform/applications');

const getInformationBases = async (dispatch: TAppDispatch, args?: TGetInformationBasesRequest, myDatabase = true) => {
    dispatch(setLoading(true));

    const { responseBody } = await getFetchedData<TInformationBasesList, TGetInformationBasesRequest>({
        url: `${ informationBasesHost }/${ contextAccountId() }${ toQueryParams({
            isNewPage: myDatabase,
            accountID: contextAccountId(),
            accountUserId: userId(),
            type: args?.type ?? EInformationBasesType.all,
            page: args?.page ?? 1,
            size: args?.size ?? 10,
            affiliation: myDatabase ? 0 : 1,
            sort: args?.sort ?? 0,
            field: args?.field ? `${ args?.field?.charAt(0).toUpperCase() }${ args?.field?.slice(1) }` : '',
            search: args?.search ?? ''
        }) }`,
        method: WebVerb.GET,
    });

    if (responseBody.success && responseBody.data) {
        dispatch(myDatabase ? getInformationBasesList(responseBody.data) : getOtherInformationBasesList(responseBody.data));
    } else {
        dispatch(setError(responseBody.message));
    }
};

const getInformationBaseInfo = async (dispatch: TAppDispatch, databaseNumber: string, showLoading = true) => {
    if (showLoading) {
        dispatch(setLoading(true));
    }

    const { responseBody: { success, data, message } } = await getFetchedData<TGetInformationBaseInfoResponse, void>({
        url: apiHost(`account-databases/${ databaseNumber }`),
        method: WebVerb.GET
    });

    if (success && data) {
        dispatch(getInformationBase(data));
    } else {
        dispatch(setError(message));
    }
};

const editInformationBase = async (body: TEditInformationBaseRequest) => {
    const { responseBody } = await getFetchedData<boolean, TEditInformationBaseRequest>({
        url: apiHost('account-databases'),
        method: WebVerb.PUT,
        body
    });

    return responseBody;
};

const editInformationBaseItems = async (databaseId: string, hasModifications: boolean) => {
    const { responseBody } = await getFetchedData({
        url: `${ informationBasesHost }/Items`,
        method: WebVerb.PUT,
        body: { databaseId, hasModifications }
    });

    return responseBody;
};

const getAvailabilityInformationBases = async (dispatch: TAppDispatch) => {
    dispatch(setLoading(true));

    const { responseBody } = await getFetchedData<TGetAvailability, void>({
        url: `${ informationBasesHost }/Availability${ toQueryParams({ accountId: contextAccountId(), accountUserId: userId() }) }`,
        method: WebVerb.GET
    });

    if (responseBody.success && responseBody.data) {
        dispatch(getAvailability(responseBody.data));
    } else {
        dispatch(setError(responseBody.message));
    }
};

const deleteDatabases = async (databasesId: string[]) => {
    const { responseBody } = await getFetchedData<null, TDeleteDatabasesRequest>({
        url: informationBasesHost,
        method: WebVerb.DELETE,
        body: { databasesId }
    });

    return responseBody;
};

const getStates = async (dispatch: TAppDispatch, databasesId: string[]) => {
    const { responseBody } = await getFetchedData<TInformationBaseItem[], void>({
        url: `${ informationBasesHost }/States?AccountDatabasesIDs=${ databasesId.join('&') }`,
        method: WebVerb.GET,
    });

    if (responseBody.success) {
        dispatch(updateDatabasesStates(responseBody.data ?? []));
    } else {
        dispatch(setError(responseBody.message));
    }
};

const getStatesOnDelimiters = async (dispatch: TAppDispatch, databasesId: string[]) => {
    const { responseBody } = await getFetchedData<TCreateStatusResponse, object>({
        url: `${ informationBasesMsHost }?id=${ databasesId.join(',') }`,
        method: WebVerb.GET,
        hasStandardResponseType: false
    });

    if (responseBody.success) {
        dispatch(updateDatabaseOnDelimitersStates(responseBody.data?.Results ?? []));
    } else {
        dispatch(setError(responseBody.message));
    }
};

const useGetSupportData = (databaseId: string) => {
    return useFetch<TGetSupportData, void>({
        method: WebVerb.GET,
        url: `${ informationBasesHost }/SupportData?databaseId=${ databaseId }`
    });
};

const editSupportData = async (body: TEditSupportData) => {
    const { responseBody } = await getFetchedData<null, TEditSupportData>({
        method: WebVerb.POST,
        url: `${ informationBasesHost }/SupportData`,
        body
    });

    return responseBody;
};

const signOutSupport = async (databaseId: string) => {
    const { responseBody } = await getFetchedData<null, { databaseId: string }>({
        method: WebVerb.POST,
        url: apiHost('account-databases/deauthorization'),
        body: { databaseId }
    });

    return responseBody;
};

const signInSupport = async (body: TSignInSupportData) => {
    const { responseBody } = await getFetchedData<null, TSignInSupportData>({
        method: WebVerb.POST,
        url: apiHost('account-databases/authorization'),
        body
    });

    return responseBody;
};

const getAccess = async (dispatch: TAppDispatch, { databaseId, usersByAccessFilterType, searchString }: TGetAccessRequest) => {
    const { responseBody: { success, data, message } } = await getFetchedData<TGetAccess, void>({
        method: WebVerb.GET,
        url: `${ informationBasesHost }/Access${ toQueryParams<TGetAccessRequestDto>({ DatabaseId: databaseId, UsersByAccessFilterType: usersByAccessFilterType, SearchString: searchString }) }`
    });

    if (success && data) {
        const accessInfoDb = data.databaseAccesses;
        const hasAccessUsers = accessInfoDb.flatMap(item => {
            if (item.hasAccess) {
                return item.userId;
            }

            return [];
        });

        dispatch(addRateData({ ...data.rateData, clientServerAccessCost: data.clientServerAccessCost }));
        dispatch(changeAccessInfoDb(accessInfoDb));
        dispatch(setInitialAccess(hasAccessUsers));
        dispatch(changeValueCheck(hasAccessUsers));
    } else {
        dispatch(setError(message));
    }
};

const getAccessPriceResponse = async (body: TGetAccessPriceRequest) => {
    const { responseBody } = await getFetchedData<TGetAccessPrice[], TGetAccessPriceRequest>({
        method: WebVerb.POST,
        url: apiHost('api_v2/AccountDatabases/Access/Access-price'),
        body
    });

    return responseBody;
};

const getAccessPrice = async (dispatch: TAppDispatch, body: TGetAccessPriceRequest) => {
    const { responseBody } = await getFetchedData<TGetAccessPrice[], TGetAccessPriceRequest>({
        method: WebVerb.POST,
        url: apiHost('api_v2/AccountDatabases/Access/Access-price'),
        body
    });

    if (responseBody.success && responseBody.data) {
        dispatch(updateAccessPrice(responseBody.data));
    } else {
        dispatch(setError(responseBody.message));
    }
};

const tryPayAccesses = async (body: TPutTryPayAccessRequest) => {
    const { responseBody } = await getFetchedData<TPutTryPayAccess, TPutTryPayAccessRequest>({
        method: WebVerb.PUT,
        url: apiHost('billing-services/apply'),
        body
    });

    return responseBody;
};

const putAccessUsers = async (body: TPutAccessUsersRequest) => {
    const { responseBody } = await getFetchedData<TPutAccessUsers[], TPutAccessUsersRequest>({
        method: WebVerb.PUT,
        url: apiHost('api_v2/AccountDatabases/Access'),
        body
    });

    return responseBody;
};

const postPublishDatabase = async (body: TPostPublishDatabaseRequest) => {
    const { responseBody } = await getFetchedData<boolean, TPostPublishDatabaseRequest>({
        method: WebVerb.POST,
        url: apiHost('api_v2/AccountDatabases/Publish'),
        body
    });

    return responseBody;
};

const postRestartIISPollInfoDbCard = async (body: TInputDatabaseId) => {
    const { responseBody } = await getFetchedData<boolean, TInputDatabaseId>({
        method: WebVerb.POST,
        url: apiHost('api_v2/AccountDatabases/IIS-pool'),
        body
    });

    return responseBody;
};

const postChangeTypeDatabase = async (body: TInputDatabaseId) => {
    const { responseBody } = await getFetchedData<boolean, TInputDatabaseId>({
        method: WebVerb.POST,
        url: apiHost('api_v2/AccountDatabases/Type'),
        body
    });

    return responseBody;
};

const postSessionInfoDbCard = async (body: TInputDatabaseId) => {
    const { responseBody } = await getFetchedData<boolean, TInputDatabaseId>({
        method: WebVerb.POST,
        url: apiHost('api_v2/AccountDatabases/Session'),
        body
    });

    return responseBody;
};

const postClearCash = async ({ accountDatabaseId, clearDatabaseLogs }: TClearCacheRequest) => {
    const { responseBody } = await getFetchedData<boolean, TClearCacheRequestDto>({
        method: WebVerb.POST,
        url: apiHost('api_v2/AccountDatabases/ClearCache'),
        body: {
            AccountDatabaseId: accountDatabaseId,
            ClearDatabaseLogs: clearDatabaseLogs
        }
    });

    return responseBody;
};

const getExternalUser = async (params: TGetExternalUserRequest) => {
    const { responseBody } = await getFetchedData<TGetExternalUser, TGetExternalUserRequest>({
        method: WebVerb.GET,
        url: apiHost(`api_v2/AccountDatabases/Access/External-user${ toQueryParams(params) }`)
    });

    return responseBody;
};

export const INFORMATION_BASES = {
    deleteDatabases,
    editInformationBase,
    useGetSupportData,
    editSupportData,
    signOutSupport,
    signInSupport,
    editInformationBaseItems,
    tryPayAccesses,
    putAccessUsers,
    postPublishDatabase,
    postRestartIISPollInfoDbCard,
    postChangeTypeDatabase,
    postSessionInfoDbCard,
    postClearCash,
    getExternalUser,
    getAccessPriceResponse
};

export const INFORMATION_BASES_REDUX = {
    getInformationBases,
    getAvailabilityInformationBases,
    getStates,
    getStatesOnDelimiters,
    getInformationBaseInfo,
    getAccess,
    getAccessPrice
} as const;