export type TDatabaseAccesses = {
    accountCaption: string;
    accountIndexNumber: number;
    accountInfo: string;
    delayReason: string | null;
    hasAccess: boolean;
    isExternalAccess: boolean;
    state: number;
    userEmail: string;
    userFirstName: string | null;
    userFullName: string;
    userId: string;
    userLastName: string | null;
    userLogin: string;
    userMiddleName: string | null;
};

export type TRateData = {
    serviceId: string;
    serviceTypeId: string;
    serviceTypeName: string;
    serviceTypeDescription: string;
    serviceName: string;
    billingType: number;
    systemServiceType: number;
    clouds42ServiceType: number;
    isActiveService: boolean;
    costPerOneLicense: number;
    limitOnFreeLicenses: number;
    volumeInQuantity: number;
    totalAmount: number;
};

export type TGetAccess = {
    currency: string;
    clientServerAccessCost: number;
    databaseAccesses: TDatabaseAccesses[];
    rateData: TRateData;
};