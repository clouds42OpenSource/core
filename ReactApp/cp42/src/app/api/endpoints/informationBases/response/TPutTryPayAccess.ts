export type TPutTryPayAccess = {
    amount: number;
    canUsePromisePayment: boolean;
    currency: string;
    isComplete: boolean;
    notEnoughMoney: number;
    paymentIds: string[];
};