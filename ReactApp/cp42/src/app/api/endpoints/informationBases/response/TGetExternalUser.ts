export type TGetExternalUser = {
    accessCost?: number;
    accountCaption?: string;
    accountIndexNumber?: number;
    accountInfo?: string;
    hasAccess?: boolean;
    hasLicense?: boolean;
    userEmail?: string;
    userFirstName?: string | null;
    userFullName?: string;
    userId?: string;
    userLastName?: string | null;
    userLogin?: string;
    userMiddleName?: string | null;
};