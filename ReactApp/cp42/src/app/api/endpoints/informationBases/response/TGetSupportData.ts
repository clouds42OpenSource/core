type TAcDbSupportHistories = {
    date: string;
    description: string;
    operation: string;
};

export type TGetSupportData = {
    acDbSupportHistories: TAcDbSupportHistories[];
    completeSessioin: boolean;
    configurationName: string;
    databaseId: string;
    hasAcDbSupport: boolean;
    hasAutoUpdate: boolean;
    hasModifications: boolean;
    hasSupport: boolean;
    isAuthorized: boolean;
    isConnects: boolean;
    lastTehSupportDate: string | null;
    login: string;
    password: string;
    supportState: number;
    supportStateDescription: string;
    timeOfUpdate: number;
    version: string;
};