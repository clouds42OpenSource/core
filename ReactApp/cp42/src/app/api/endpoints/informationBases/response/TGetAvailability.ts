import { EServiceLockReasonType } from '../enum';

export type TGetAvailability = {
    accessToServerDatabaseServiceTypeId: string;
    accountAdminInfo: string;
    accountId: string;
    accountLocaleName: string;
    accountUserId: string;
    isMainServiceAllowed: boolean;
    myDatabaseServiceId: string;
    isVipAccount: boolean;
    accountAdminPhoneNumber: string;
    mainServiceStatusInfo: {
        promisedPaymentExpireDate: string | null;
        promisedPaymentIsActive: boolean;
        promisedPaymentSum: number | null;
        serviceIsLocked: boolean;
        serviceLockReason: string | null;
        serviceLockReasonType: EServiceLockReasonType | null;
    },
    allowAddingDatabaseInfo: {
        forbiddeningReasonMessage: string;
        isAllowedCreateDb: boolean;
        surchargeForCreation: number;
    },
    permissionsForWorkingWithDatabase: {
        hasPermissionForCreateAccountDatabase: boolean;
        hasPermissionForDisplayAutoUpdateButton: boolean;
        hasPermissionForDisplayPayments: boolean;
        hasPermissionForDisplaySupportData: boolean;
        hasPermissionForMultipleActionsWithDb: boolean;
        hasPermissionForRdp: boolean;
        hasPermissionForWeb: boolean;
    }
};