export type TGetAccessPrice = {
    accessCost: number;
    accountUserId: string;
    hasLicense: boolean;
};