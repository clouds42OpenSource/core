import { AccountDatabaseRestoreModelType, DatabaseState, DbPublishState, DistributionType, PlatformType, SupportState } from 'app/common/enums';
import { KeyValueDataModel } from 'app/web/common';

type TDatabaseCardEditModeDictionaries = {
    availableDatabaseStatuses: KeyValueDataModel<DatabaseState, string>[];
    availablePlatformTypes: KeyValueDataModel<number, string>[];
    availableAccountDatabaseRestoreModelTypes: KeyValueDataModel<number, string>[];
};

type TDatabaseOperationItem = {
    cost: number;
    currency: string;
    id: string;
    name: string;
};

type TDatabase = {
    id: string;
    accountId: string;
    v82Name: string;
    caption: string;
    sizeInMB: number;
    state: DatabaseState;
    createAccountDatabaseComment: string;
    templateName: string;
    templateImgUrl: string;
    publishState: DbPublishState;
    webPublishPath: string;
    needShowWebLink: boolean;
    lastActivityDate: string;
    timezoneName: string;
    isDeleted: boolean;
    version: string;
    lastEditedDateTime: string;
    isDbOnDelimiters: boolean;
    isDemoDelimiters: boolean;
    zoneNumber?: number;
    configurationName: string;
    configurationCode: string;
    isExternalDb: boolean;
    creationDate: string;
    backupDate?: string;
    backupDateString: string;
    existBackUpPath: boolean;
    calculateSizeDateTime?: Date;
    templatePlatform: PlatformType;
    distributionType: DistributionType;
    stable82Version: string;
    alpha83Version: string;
    stable83Version: string;
    dbNumber: number;
    isFile?: boolean;
    filePath: string;
    isReady: boolean;
    dbTemplate: string;
    accountCaption: string;
    v82Server: string;
    sqlServer: string;
    archivePath: string;
    serviceName: string;
    lockedState: string;
    platformType: PlatformType;
    canWebPublish: boolean;
    databaseState: DatabaseState;
    usedWebServices: boolean;
    cloudStorageWebLink: string;
    fileStorageId: string;
    restoreModelType: AccountDatabaseRestoreModelType;
    databaseConnectionPath: string;
    canChangeRestoreModel: boolean;
    fileStorageName: string;
    myDatabasesServiceTypeId: string;
    commonDataForWorkingWithDB: TDatabaseCardEditModeDictionaries;
    isExistTerminatingSessionsProcessInDatabase: boolean;
    databaseOperations: TDatabaseOperationItem[];
    actualDatabaseBackupId: null | string;
};

type TFieldsAccessReadModeInfo = {
    canShowWebPublishPath: boolean;
    canShowUsedWebServices: boolean;
    canShowDatabaseDetails: boolean;
    canShowSqlServer: boolean;
    canShowDatabaseState: boolean;
    canShowDatabaseFileStorage: boolean;
};

type TBackups = {
    id: string;
    initiator: string;
    creationDate: Date;
    backupPath: string;
    eventTrigger: number;
    eventTriggerDescription: string;
    isDbOnDelimiters: boolean;
};

type TTehSupportInfo = {
    isInConnectingState: boolean;
    lastHistoryDate?: Date;
    supportState: SupportState;
    supportStateDescription: string;
};

export type TTabVisibility = {
    isBackupTabVisible: boolean;
    isTehSupportTabVisible: boolean;
};

type TCommandVisibility = {
    isPublishDatabaseCommandVisible: boolean;
    isCancelPublishDatabaseCommandVisible: boolean;
    isDeleteDatabaseCommandVisible: boolean;
    isDatabasePublishingInfoVisible: boolean;
    isCanacelingDatabasePublishingInfoVisible: boolean;
};

type TAccountCardEditModeDictionaries = {
    availablePlatformTypes: PlatformType[];
    availableDestributionTypes: KeyValueDataModel<DistributionType, string>[];
    availableDatabaseStates: DatabaseState[];
    availableFileStorages: KeyValueDataModel<string, string>[];
    availableDatabaseTemplates: KeyValueDataModel<string, string>[];
};

export type TGetInformationBaseInfoResponse = {
    database: TDatabase;
    launchPublishedDbUrl: string;
    fieldsAccessReadModeInfo: TFieldsAccessReadModeInfo;
    backups: TBackups[];
    tehSupportInfo: TTehSupportInfo | null;
    tabVisibility: TTabVisibility;
    commandVisibility: TCommandVisibility;
    accountCardEditModeDictionaries: TAccountCardEditModeDictionaries;
    canEditDatabase: boolean;
};