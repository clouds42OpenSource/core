import { TSelectDataResultCommonResponseDto } from 'app/web/common';
import { TStatusItem } from '../../createDb/response';
import { EDatabaseState, EPublishState } from '../enum';

export type TInformationBaseItem = {
    id: string;
    name: string;
    templateCaption: string;
    templateImageName: string;
    sizeInMb: number;
    lastActivityDate: string;
    databaseLaunchLink: string;
    isFile: boolean;
    isDbOnDelimiters: boolean;
    isDemo: boolean;
    hasSupport: boolean;
    hasAutoUpdate: boolean;
    hasAcDbSupport: boolean;
    state: EDatabaseState;
    publishState: EPublishState;
    needShowWebLink: boolean;
    createAccountDatabaseComment: string | null;
    isExistSessionTerminationProcess: boolean;
    webPublishPath?: string;
    configurationName: string;
    createStatus?: TStatusItem;
    v82Name: string;
};

export type TInformationBasesList = TSelectDataResultCommonResponseDto<TInformationBaseItem>;