export enum EServiceLockReasonType {
    ServiceNotPaid,
    OverduePromisedPayment,
    NoDiskSpace
}