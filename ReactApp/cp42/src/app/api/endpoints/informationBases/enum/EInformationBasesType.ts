export enum EInformationBasesType {
    all,
    server,
    archive,
    file,
    delimeter,
    trash
}