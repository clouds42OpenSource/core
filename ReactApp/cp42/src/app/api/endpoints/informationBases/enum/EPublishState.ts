export enum EPublishState {
    Published,
    PendingPublication,
    PendingUnpublication,
    Unpublished,
    RestartingPool
}