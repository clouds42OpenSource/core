import { TGetUserRate, TMainPageResponse } from 'app/api/endpoints/main/response';
import { apiHost, getFetchedData, templateErrorText, WP_FETCH_PARAMS, wpHost } from 'app/api/fetch';
import { getMainPageInfoSlice } from 'app/modules/main';
import { TAppDispatch } from 'app/redux';
import { WebVerb } from 'core/requestSender/enums';

const getMainPageInfo = (dispatch: TAppDispatch) => {
    dispatch(getMainPageInfoSlice.actions.loading());

    getFetchedData<TMainPageResponse, void>({
        url: apiHost('navigation/main-page'),
        method: WebVerb.GET
    }).then(({ responseBody: { data, success, message } }) => {
        if (success && data && !message) {
            dispatch(getMainPageInfoSlice.actions.success(data));
        } else {
            dispatch(getMainPageInfoSlice.actions.error(message ?? templateErrorText));
        }
    });
};

const getUserAchievement = async (idForReview: string) => {
    const { responseBody } = await getFetchedData<TGetUserRate[], void>({
        method: WebVerb.GET,
        url: wpHost(`wp-json/wp/v2/userrate?search=${ idForReview }`),
        ...WP_FETCH_PARAMS
    });

    return responseBody;
};

export const MAIN_PAGE = {
    getUserAchievement
} as const;

export const MAIN_PAGE_REDUX = {
    getMainPageInfo
} as const;