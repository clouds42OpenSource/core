export type TAcf = {
    'user_score': number;
    'user_status': string;
    'user_achievement': string;
    'next_rate_score': number;
    'next_status_score': number;
};

export type TGetUserRate = {
    acf?: TAcf;
};