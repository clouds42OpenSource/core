type TMarket = {
    marketUrlWithSource: string;
    mainPageNotification: string;
    bannerUrl: string | null;
};

type TAccountAdministrator = {
    firstName: string | null;
    lastName: string | null;
    middleName: string | null;
    email: string | null;
    phoneNumber: string | null;
};

type TAccountUser = {
    accountCaption: string;
    accountInn: string;
    accountLocaleName: string;
    accountAdministrators: TAccountAdministrator[];
};

export type TMainPageResponse = {
    accountId: string;
    accountUserId: string;
    firstName: string;
    login: string;
    email: string;
    market: TMarket;
    accountUser: TAccountUser;
};