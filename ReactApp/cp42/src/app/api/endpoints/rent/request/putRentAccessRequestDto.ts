export type accessListItem = {
    accountUserId: string;
    webResourceId: string | null;
    rdpResourceId: string | null;
    webResource: boolean;
    standartResource: boolean;
}

export type tryPayAccessRequestDto = {
    accesses: accessListItem[];
}

export type serviceRequestDto = {
    expireDate: string;
    accountIsVip: boolean;
    additionalResourceName: string;
    additionalResourceCost: number;
    costOfWebLicense: number;
    costOfRdpLicense: number;
    isDemoPeriod: boolean;
    maxDemoExpiredDate: string;
    limitOnFreeCreationDb: number;
    costOfCreatingDbOverFreeLimit: number;
    serverDatabasePlacementCost: number;
}