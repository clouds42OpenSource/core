import { accessListItem, serviceRequestDto, tryPayAccessRequestDto } from 'app/api/endpoints/rent/request';
import { getRentManagementResponseDto, getRentResponseDto, serviceResponseDto, tryPayAccessResponeDto, userItemModel, userResource } from 'app/api/endpoints/rent/response';

import { apiHost, getFetchedData } from 'app/api/fetch';
import { TDataReturn, TResponseLowerCase } from 'app/api/types';
import { useFetch, } from 'app/hooks';
import { WebVerb } from 'core/requestSender/enums';

const rentUrl = apiHost('billing-services/rent-1c');
const billingUrl = apiHost('billing-services');

const useGetRent = (isRequestBlocked?: boolean) => {
    return useFetch<getRentResponseDto, void>({
        url: `${ rentUrl }`,
        method: WebVerb.GET,
        isRequestBlocked
    });
};

const getRentManagement = async (): Promise<TResponseLowerCase<getRentManagementResponseDto>> => {
    const { responseBody } = await getFetchedData<getRentManagementResponseDto, void>({
        url: `${ rentUrl }/management`,
        method: WebVerb.GET
    });

    return responseBody;
};

const editRentManagement = async (requestParam: serviceRequestDto): Promise<TResponseLowerCase<null>> => {
    const { responseBody } = await getFetchedData<TResponseLowerCase<null>, serviceRequestDto>({
        url: `${ rentUrl }/management`,
        method: WebVerb.POST,
        body: requestParam
    });

    return {
        success: responseBody.success,
        data: null,
        message: responseBody.message
    };
};

const tryPayAccess = async (requestParam: accessListItem[]): Promise<TResponseLowerCase<tryPayAccessResponeDto>> => {
    const { responseBody } = await getFetchedData<tryPayAccessResponeDto, tryPayAccessRequestDto>({
        url: `${ rentUrl }/try-pay-access`,
        method: WebVerb.POST,
        body: { accesses: requestParam }
    });

    return {
        success: responseBody.success,
        data: responseBody.data,
        message: responseBody.message
    };
};

const tryPromisePayment = async (requestParam: accessListItem[]): Promise<TResponseLowerCase<boolean>> => {
    const { responseBody } = await getFetchedData<TResponseLowerCase<boolean>, tryPayAccessRequestDto>({
        url: `${ rentUrl }/try-pay-access-by-promise-payment`,
        method: WebVerb.POST,
        body: { accesses: requestParam }
    });

    return {
        success: responseBody.success,
        data: !!responseBody.data?.data,
        message: responseBody.message
    };
};

const tryExternalUser = async (requestParam: string): Promise<TResponseLowerCase<userItemModel>> => {
    const { responseBody } = await getFetchedData<userItemModel, void>({
        url: `${ rentUrl }/external-user?email=${ requestParam }`,
        method: WebVerb.GET,
    });

    return {
        success: responseBody.success,
        data: responseBody.data,
        message: responseBody.message
    };
};

const getUserResource = async (requestParam: string): Promise<TResponseLowerCase<userResource>> => {
    const { responseBody } = await getFetchedData<userResource, void>({
        url: `${ billingUrl }/resources/${ requestParam }`,
        method: WebVerb.GET,
    });

    return {
        success: responseBody.success,
        data: responseBody.data,
        message: responseBody.message
    };
};

const useGetUserResource = (requestParam: string): TDataReturn<userResource> => {
    const { data, error, isLoading, refreshData } = useFetch<userResource, void>({
        url: `${ billingUrl }/resources/${ requestParam }`,
        method: WebVerb.GET
    });

    return { data, error, isLoading, refreshData };
};

const editUserResource = async (requestParam: userResource): Promise<TResponseLowerCase<boolean>> => {
    const { responseBody } = await getFetchedData<TResponseLowerCase<boolean>, userResource>({
        url: `${ billingUrl }/resources`,
        method: WebVerb.POST,
        body: requestParam
    });

    return {
        success: responseBody.success,
        data: !!responseBody.data?.data,
        message: responseBody.message
    };
};

const getDemoPeriod = async (): Promise<TResponseLowerCase<serviceResponseDto>> => {
    const { responseBody } = await getFetchedData<serviceResponseDto, void>({
        url: `${ rentUrl }/demo-period`,
        method: WebVerb.GET,
    });

    return {
        success: responseBody.success,
        data: responseBody.data,
        message: responseBody.message
    };
};

const editDemoPeriod = async (requestParam: serviceRequestDto): Promise<TResponseLowerCase<serviceResponseDto>> => {
    const { responseBody } = await getFetchedData<TResponseLowerCase<null>, serviceRequestDto>({
        url: `${ rentUrl }/demo-period`,
        method: WebVerb.POST,
        body: requestParam
    });

    return {
        success: responseBody.success,
        data: null,
        message: responseBody.message
    };
};

const turnOnRent = async (): Promise<TResponseLowerCase<boolean>> => {
    const { responseBody } = await getFetchedData<TResponseLowerCase<boolean>, void>({
        url: `${ rentUrl }/turn-on`,
        method: WebVerb.GET,
    });

    return {
        success: responseBody.success,
        data: !!responseBody.data,
        message: responseBody.message
    };
};

export const RENT = {
    useGetRent,
    getRentManagement,
    tryPayAccess,
    tryPromisePayment,
    tryExternalUser,
    getUserResource,
    useGetUserResource,
    editUserResource,
    editRentManagement,
    getDemoPeriod,
    editDemoPeriod,
    turnOnRent
} as const;