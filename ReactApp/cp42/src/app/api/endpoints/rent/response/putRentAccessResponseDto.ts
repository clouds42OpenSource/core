export type tryPayAccessResponeDto = {
    complete: boolean,
    enoughMoney: number,
    errorMessage: string,
    canGetPromisePayment: boolean,
    costForPay: number
}