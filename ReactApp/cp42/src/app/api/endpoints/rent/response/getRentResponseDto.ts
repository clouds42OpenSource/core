export type link42Info = {
    isCurrentVersion: boolean;
    version: string;
};

export type userItemModel = {
    id: string;
    accountId: string;
    login: string;
    fullUserName: string;
    isActive: boolean;
    email: string;
    phone: string;
    webResourceId: string;
    rdpResourceId: string;
    sponsoredLicenseAccountName: string;
    sponsorshipLicenseAccountName: string;
    partialUserCostWeb: number;
    partialUserCostRdp: number;
    partialUserCostStandart: number;
    namesOfDependentActiveServices: {
        key: number;
        value: string[];
        dependServiceIds: string[];
    }[];
    link42Info: null | link42Info;
};

export type rentLocale = {
    id: string;
    name: string;
    country: string;
    currencyCode: number;
    currency: string;
    cpSiteUrl: string;
    defaultSegmentId: string;
};

export type getRentResponseDto = {
    expireDate: string;
    accountIsVip: boolean;
    locale: rentLocale;
    monthlyCostTotal: number;
    monthlyCostWeb: number;
    monthlyCostStandart: number;
    rateWeb: number;
    rateRdp: number;
    webResourcesCount: number;
    standartResourcesCount: number;
    sponsoredWebCount: number;
    sponsoredStandartCount: number;
    sponsorshipWebCount: number;
    sponsorshipStandartCount: number;
    freeWebResources: string[];
    freeRdpResources: string[];
    userList: userItemModel[];
    serviceStatus: {
        serviceIsLocked: boolean;
        serviceLockReason: string;
        serviceLockReasonType: number;
        promisedPaymentIsActive: boolean;
        promisedPaymentSum: string;
        promisedPaymentExpireDate: string;
    };
    myDatabasesServiceData: {
        limitOnFreeCreationDb: number;
        countDatabasesOverLimit: number;
        serverDatabasesCount: number;
        databasePlacementCost: number;
        serverDatabasePlacementCost: number;
        totalAmountForServerDatabases: number;
        totalAmountForDatabases: number;
    };
    additionalAccountResources: {
        additionalResourcesName: string;
        additionalResourcesCost: number;
        needShowAdditionalResources: boolean;
    };
};

export type serviceResponseDto = {
    expireDate: string;
    accountIsVip: boolean;
    additionalResourceName: string;
    additionalResourceCost: number;
    costOfWebLicense: number;
    costOfRdpLicense: number;
    isDemoPeriod: boolean;
    maxDemoExpiredDate: string;
    limitOnFreeCreationDb: number;
    costOfCreatingDbOverFreeLimit: number;
    serverDatabasePlacementCost: number;
};

export type userResource = {
    login: string;
    name: string;
    cost: number;
    resourceId: string;
    serviceTypeName: string;
};

export type getRentManagementResponseDto = {
    users: userResource[];
    service: serviceResponseDto | null;
};