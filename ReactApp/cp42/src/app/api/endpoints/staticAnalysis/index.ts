import { getFetchedData } from 'app/api/fetch';
import { encodeToBase64 } from 'app/utils';
import { WebVerb } from 'core/requestSender/enums';
import { createProjectResponseDto, getIsStaticAnalysisConnectedResponseDto, getProjectResultResponseDto, getProjectsResponseDto, getUserCredsResponseDto } from './response';
import { NewUserNewProjectRequest, OldUserProjectRequestDto } from './request';

const workerUrl = import.meta.env.REACT_STATIC_ANALYSIS_URL;

const basicToken = `Basic ${ encodeToBase64(`${ import.meta.env.REACT_LKSQL_LOGIN }:${ import.meta.env.REACT_LKSQL_PASSWORD }`) }`;

const baseParams = {
    noAuthorizationToken: true,
    noCredentials: true,
    hasStandardResponseType: false,
    headers: {
        Prefer: 'return=representation',
        Authorization: basicToken
    }
};

const createNewUserNewProject = async (requestParam: NewUserNewProjectRequest) => {
    const body = { ...requestParam, status: 'NUNP', ischanged: '1' };
    const { responseBody } = await getFetchedData<createProjectResponseDto[], NewUserNewProjectRequest>({
        url: workerUrl,
        method: WebVerb.POST,
        ...baseParams,
        body
    });
    return responseBody;
};

const createOldUserProject = async (requestParam: OldUserProjectRequestDto) => {
    const body = { ...requestParam, ischanged: '1' };
    const { responseBody } = await getFetchedData<createProjectResponseDto[], OldUserProjectRequestDto>({
        url: workerUrl,
        method: WebVerb.POST,
        ...baseParams,
        body
    });
    return responseBody;
};

const getUserCredsByCompanyId = async (companyid: string | number) => {
    const { responseBody } = await getFetchedData<getUserCredsResponseDto[], void>({
        url: `${ workerUrl }?select=usernameportal,pass,login&companyid=eq.${ companyid }&pass=not.is.null&usernameportal=not.is.null`,
        method: WebVerb.GET,
        ...baseParams
    });
    return responseBody;
};

const getProjectsByCompanyId = async (companyid: string) => {
    const { responseBody } = await getFetchedData<getProjectsResponseDto[], void>({
        url: `${ workerUrl }?select=addinfo,id,project&companyid=eq.${ companyid }`,
        method: WebVerb.GET,
        ...baseParams
    });
    return responseBody;
};

const getIsStaticAnalysisConnected = async (companyid: string | number) => {
    const { responseBody } = await getFetchedData<getIsStaticAnalysisConnectedResponseDto[], void>({
        url: `${ workerUrl }?select=isconnected&companyid=eq.${ companyid }&isconnected=eq.1&limit=1`,
        method: WebVerb.GET,
        ...baseParams
    });
    return responseBody;
};

const getResultByProjectName = async (projectId: string) => {
    const { responseBody } = await getFetchedData<getProjectResultResponseDto[], void>({
        url: `${ workerUrl }?select=scanurl,login,pass,usernameportal,status,project,url&id=eq.${ projectId }`,
        method: WebVerb.GET,
        ...baseParams
    });
    return responseBody;
};

export const STATICANALYSIS = {
    createNewUserNewProject,
    createOldUserProject,
    getUserCredsByCompanyId,
    getProjectsByCompanyId,
    getResultByProjectName,
    getIsStaticAnalysisConnected
} as const;