type createProject = {
    companyid: string;
    url: string;
    description: string;
    usernameportal: string;
    ischanged: string;
    status: string;
};

export type NewUserNewProjectRequest = createProject;

export type OldUserProjectRequestDto = createProject & {
    login: string;
    pass: string;
    project?: string;
    addinfo?: string;
};