export type getIsStaticAnalysisConnectedResponseDto = {
    isconnected: string | null;
};