export type createProjectResponseDto = {
    id: string;
    companyid: string;
    url: string;
    login: string | null;
    pass: string | null;
    project: string | null;
    status: string;
    description: string;
    timestampcreated: string;
    timestampchanged: string;
    usernameportal: string;
    ischanged: string;
};