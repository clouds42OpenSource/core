export type getProjectsResponseDto = {
    addinfo: string;
    id: string;
    project: string;
};