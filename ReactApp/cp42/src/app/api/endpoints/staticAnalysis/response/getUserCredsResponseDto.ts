export type getUserCredsResponseDto = {
    usernameportal: string;
    pass: string;
    login: string;
};

export type getProjectResultResponseDto = getUserCredsResponseDto & {
    scanurl: string;
    project: string;
    status: string;
    url: string;
};