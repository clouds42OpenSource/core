import { apiHost, getFetchedData } from 'app/api/fetch';
import { TResponseLowerCase } from 'app/api/types';
import { WebVerb } from 'core/requestSender/enums';

const verificationHost = apiHost('account-users');

const getVerifyEmail = async (): Promise<TResponseLowerCase<string>> => {
    const response = await getFetchedData<string, void>({
        method: WebVerb.GET,
        url: `${ verificationHost }/verify-email`
    });

    return response.responseBody;
};

const getConfirmEmail = async (code: string, isLinkClicked?: boolean): Promise<TResponseLowerCase<string>> => {
    const response = await getFetchedData<string, string>({
        method: WebVerb.GET,
        url: `${ verificationHost }/confirm-email?code=${ code }${ isLinkClicked ? '&isLinkClicked=true' : '' }`,
        noAuthorizationToken: isLinkClicked
    });

    return response.responseBody;
};

const getVerifyPhone = async (): Promise<TResponseLowerCase<string | null>> => {
    const response = await getFetchedData<string, void>({
        method: WebVerb.GET,
        url: `${ verificationHost }/verify-phone`
    });

    return response.responseBody;
};

const getConfirmPhone = async (code: string) => {
    const response = await getFetchedData<string, string>({
        method: WebVerb.GET,
        url: `${ verificationHost }/confirm-phone?code=${ code }`
    });

    return response.responseBody;
};

const forcePhoneVerification = async (accountUserId: string) => {
    const { responseBody } = await getFetchedData({
        method: WebVerb.GET,
        url: `${ verificationHost }/confirm-phone?accountUserId=${ accountUserId }`
    });

    return responseBody;
};

const forceMailVerification = async (accountUserId: string) => {
    const { responseBody } = await getFetchedData({
        method: WebVerb.GET,
        url: `${ verificationHost }/confirm-email?accountUserId=${ accountUserId }`
    });

    return responseBody;
};

export const VERIFICATION_CODE = {
    getVerifyEmail,
    getConfirmEmail,
    getVerifyPhone,
    getConfirmPhone,
    forcePhoneVerification,
    forceMailVerification
} as const;