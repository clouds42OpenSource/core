import { apiHost, getFetchedData } from 'app/api/fetch';
import { WebVerb } from 'core/requestSender/enums';

const getPromoPage = async () => {
    const { responseBody } = await getFetchedData<string, void>({
        url: apiHost('navigation/promo-page'),
        method: WebVerb.GET
    });

    return responseBody.data ?? '';
};

export const PROMO_PAGE = {
    getPromoPage
} as const;