export type TGetBackupsRequest = {
    from_date?: string;
    to_date?: string;
}