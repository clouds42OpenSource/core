export type setBackupsScheduleRequestDto = {
    StartingTime: string;
    EndingTime: string;
};