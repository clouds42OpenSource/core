import { getBackupsScheduleRequestDto, setBackupsScheduleRequestDto, TGetBackupsRequest } from 'app/api/endpoints/applications/request';
import { TGetBackups, TGetBackupsSchedule, TGetDownloadFile } from 'app/api/endpoints/applications/response';
import { getFetchedData, msHost, toQueryParams } from 'app/api/fetch';
import { TResponseLowerCase } from 'app/api/types';
import { changeToCamelCase, removeDashesFromKeys } from 'app/web/common';

import { WebVerb } from 'core/requestSender/enums';

const applicationsUrl = msHost('pl42/hs/Platform/applications');

const getBackups = async (applicationId: string, filters: TGetBackupsRequest) => {
    const { responseBody } = await getFetchedData<TGetBackups, void>({
        url: `${ applicationsUrl }/${ applicationId }/backups${ toQueryParams(filters) }`,
        method: WebVerb.GET,
        hasStandardResponseType: false
    });

    return removeDashesFromKeys(responseBody);
};

const getBackupsSchedule = async (requestParam: getBackupsScheduleRequestDto) => {
    const { responseBody } = await getFetchedData<TGetBackupsSchedule, void>({
        url: `${ applicationsUrl }/${ requestParam.applicationId }/backups-schedule`,
        method: WebVerb.GET,
        hasStandardResponseType: false,
    });

    return changeToCamelCase(responseBody);
};

const setBackupsSchedule = async (requestParam: { applicationId: string } & setBackupsScheduleRequestDto): Promise<TResponseLowerCase<null>> => {
    const { responseBody } = await getFetchedData<null, setBackupsScheduleRequestDto>({
        url: `${ applicationsUrl }/${ requestParam.applicationId }/backups-schedule`,
        method: WebVerb.POST,
        hasStandardResponseType: false,
        body: {
            StartingTime: requestParam.StartingTime,
            EndingTime: requestParam.EndingTime
        }
    });

    return responseBody;
};

const getDownloadFile = (fieldId: string) => {
    return getFetchedData<TGetDownloadFile, void>({
        url: msHost(`pl42/hs/Platform/files/${ fieldId }/download`),
        method: WebVerb.GET,
        hasStandardResponseType: false
    });
};

export const APPLICATIONS = {
    getBackups,
    getBackupsSchedule,
    setBackupsSchedule,
    getDownloadFile
} as const;