export type TBackupsItem = {
    id: string;
    fileId: string;
    fileSize: string;
    date: string;
    appVersion: string;
    isOriginal: boolean;
    isOndemand: boolean;
    isAnnual: boolean;
    isMounthly: boolean;
    isDaily: boolean;
    isEternal: boolean;
    type: string;
    eternalBackupUrl: string;
    comment: string;
};

export type TGetBackups = {
    backups: TBackupsItem[];
}