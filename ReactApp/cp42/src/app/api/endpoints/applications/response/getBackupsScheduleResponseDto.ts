export type getBackupsScheduleResponseDto = {
    EndingTime: string;
    MinimumTime: string
    StartingTime: string;
};

export type TGetBackupsSchedule = {
    endingTime: string;
    minimumTime: string
    startingTime: string;
}