import { TStatus } from 'app/api/endpoints/services/response';

export type TPromoProposalListQueryRequest = {
    page: number;
    pageSize?: number;
    status: TStatus;
    active?: boolean;
    'service-id': string;
};