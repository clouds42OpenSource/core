export * from './TPromoProposalList';
export * from './TPromoProposalCreate';
export * from './TPromoProposalDelete';
export type { TGetServicesListRequest } from './TGetServicesList';
export type { TConnectToDatabase } from './TConnectToDatabase';