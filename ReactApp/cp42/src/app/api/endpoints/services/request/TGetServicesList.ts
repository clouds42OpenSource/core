export type TGetServicesListRequest = {
    accountId: string;
    serviceName: string;
    serviceExtensionsForDatabaseType: number;
    accountDatabaseId: string;
};