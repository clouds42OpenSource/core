export type TPromoProposalCreateRequest = {
    'service-id': string;
    query: string;
    title?: string;
    description?: string;
};