export type TConnectToDatabase = {
    serviceId: string;
    databaseId: string;
    install: boolean;
};