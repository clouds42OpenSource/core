export type TGetServicesList = {
    extensionLastActivityDate: string | null;
    extensionState: number | null;
    id: string;
    isInstalled: boolean;
    isActiveService: boolean;
    isFrozenService: boolean;
    name: string;
    shortDescription: string;
};