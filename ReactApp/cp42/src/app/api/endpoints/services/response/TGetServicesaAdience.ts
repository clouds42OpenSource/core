export type TGetServicesAudience = {
    name: string;
    id: string;
};