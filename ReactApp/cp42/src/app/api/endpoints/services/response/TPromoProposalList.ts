type TPaginationResponse = {
    'page-number': number;
    'page-size': number;
    'total-items': number;
    'total-pages': number;
};

type TPagination = {
    pageNumber: number;
    pageSize: number;
    totalItems: number;
    totalPages: number;
};

export type TStatus = 'accepted' | 'declined' | 'pending' | 'deleted' | '';

type TRecordResponse = {
    id: string;
    'service-id': string;
    status: TStatus;
    result: string;
    active: boolean;
    query: string;
    title: string;
    description: string;
    created: string;
    modified: string;
};

export type TRecord = Omit<TRecordResponse, 'service-id'> & {
    serviceId: string;
};

export type TPromoProposalListResponse = {
    pagination: TPaginationResponse;
    records: TRecordResponse[];
};

export type TPromoProposalList = {
    pagination: TPagination;
    records: TRecord[];
};