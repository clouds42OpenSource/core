import { TConnectToDatabase, TGetServicesListRequest, TPromoProposalCreateRequest, TPromoProposalDeleteRequest, TPromoProposalListQueryRequest } from 'app/api/endpoints/services/request';
import { TGetServicesAudience, TGetServicesList, TPromoProposalCreateResponse, TPromoProposalList, TPromoProposalListResponse } from 'app/api/endpoints/services/response';
import { TGetVersionUrl } from 'app/api/endpoints/services/response/TGetVersionUrl';
import { apiHost, contextAccountId, getFetchedData, msHost, toQueryParams } from 'app/api/fetch';
import { TData, TDataReturn } from 'app/api/types';
import { useFetch } from 'app/hooks';
import { removeDashesFromKeys } from 'app/web/common';
import { WebVerb } from 'core/requestSender/enums';

const promoProposalsUrl = 'pl42/hs/Platform/promo-proposals';

const useGetMarketPromo = (filterParams: TPromoProposalListQueryRequest): TDataReturn<TPromoProposalList> => {
    const { error, data, isLoading, refreshData } = useFetch<TPromoProposalListResponse, void>({
        url: msHost(`${ promoProposalsUrl }${ toQueryParams(filterParams) }`),
        method: WebVerb.GET,
        hasStandardResponseType: false
    });

    if (data) {
        const mappedData: TData<TPromoProposalList> = {
            headers: data.headers,
            status: data.status,
            rawData: removeDashesFromKeys(data.rawData) as TPromoProposalList
        };

        return { error, data: mappedData, isLoading, refreshData };
    }

    return { error, data, isLoading };
};

const createMarketPromo = async (requestParams: TPromoProposalCreateRequest): Promise<TDataReturn<TPromoProposalCreateResponse | null>> => {
    const { status, headers, responseBody } = await getFetchedData<TPromoProposalCreateResponse, TPromoProposalCreateRequest>({
        url: msHost(promoProposalsUrl),
        method: WebVerb.POST,
        body: requestParams,
        hasStandardResponseType: false
    });

    return {
        data: {
            rawData: responseBody.data,
            status,
            headers
        },
        error: responseBody.message,
        isLoading: false
    };
};

const deleteMarketPromo = async (promoProposalId: string, requestParams: TPromoProposalDeleteRequest): Promise<TDataReturn<{} | null>> => {
    const { status, headers, responseBody } = await getFetchedData<{}, TPromoProposalDeleteRequest>({
        url: `${ msHost(promoProposalsUrl) }/${ promoProposalId }`,
        method: WebVerb.DELETE,
        body: requestParams,
        hasStandardResponseType: false
    });

    return {
        data: {
            rawData: responseBody.data,
            status,
            headers
        },
        error: responseBody.message,
        isLoading: false
    };
};

const getVersionUrl = async (serviceId: string, versionId: string) => {
    const { responseBody } = await getFetchedData<TGetVersionUrl, void>({
        url: msHost(`pl42/hs/Platform/services/${ serviceId }/versions/${ versionId }`),
        method: WebVerb.GET,
        hasStandardResponseType: false
    });

    return responseBody;
};

const getServicesTags = async () => {
    const { responseBody } = await getFetchedData<{ Tags: string[] }, void>({
        url: msHost('pl42/hs/Platform/services/tags'),
        method: WebVerb.GET,
        hasStandardResponseType: false
    });

    return responseBody;
};
const getServicesAudience = async () => {
    const { responseBody } = await getFetchedData<TGetServicesAudience[], void>({
        url: msHost('pl42/hs/Platform/services/audience'),
        method: WebVerb.GET,
        hasStandardResponseType: false
    });

    return responseBody;
};

const getImageUrl = async (fileID: string) => {
    const { responseBody } = await getFetchedData<{ url: string }, void>({
        url: msHost(`pl42/hs/Platform/files/${ fileID }/download?type=service_attachment`),
        method: WebVerb.GET,
        hasStandardResponseType: false
    });

    return responseBody;
};

const useGetServicesList = (query: TGetServicesListRequest) => {
    return useFetch<TGetServicesList[], TGetServicesListRequest>({
        url: apiHost(`api_v2/AccountDatabases/Services${ toQueryParams(query) }`),
        method: WebVerb.GET
    });
};

const connectToDatabase = async (body: TConnectToDatabase) => {
    const { responseBody } = await getFetchedData<null, TConnectToDatabase>({
        url: apiHost('api_v2/AccountDatabases/Services'),
        method: WebVerb.POST,
        body
    });

    return responseBody;
};

const demoConnectToDatabase = async (body: Omit<TConnectToDatabase, 'install'>) => {
    const { responseBody } = await getFetchedData<null, Omit<TConnectToDatabase, 'install'> & { accountId: string }>({
        url: apiHost('api_v2/AccountDatabases/Services/Demo'),
        method: WebVerb.POST,
        body: { ...body, accountId: contextAccountId() }
    });

    return responseBody;
};

export const SERVICES = {
    useGetMarketPromo,
    createMarketPromo,
    deleteMarketPromo,
    getVersionUrl,
    getServicesTags,
    getServicesAudience,
    getImageUrl,
    useGetServicesList,
    connectToDatabase,
    demoConnectToDatabase
} as const;