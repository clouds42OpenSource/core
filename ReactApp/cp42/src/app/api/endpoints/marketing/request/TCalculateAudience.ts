import { EAccountType } from '../enum';

export type TCalculateAudienceRequest = {
    accountType: EAccountType;
    availabilityPayment: number;
    rentalServiceState: number;
    locales: string[];
};