export type TEditLetterTemplateRequest = {
    body: string;
    id: number;
    subject: string;
    header: string;
    sendGridTemplateId: string;
    notificationText: string;
    advertisingBannerTemplateId?: number | null;
};