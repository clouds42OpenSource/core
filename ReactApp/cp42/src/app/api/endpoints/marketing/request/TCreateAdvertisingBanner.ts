import { TCalculateAudienceRequest } from './TCalculateAudience';

type TAdvertisingImage = {
    fileName: string;
    content?: string;
    contentType: string;
    base64?: string;
};

export type TCreateAdvertisingBannerRequest = {
    image: TAdvertisingImage;
    header: string;
    body: string;
    captionLink: string;
    link: string;
    advertisingBannerAudience: TCalculateAudienceRequest;
    showFrom: string;
    showTo: string;
};