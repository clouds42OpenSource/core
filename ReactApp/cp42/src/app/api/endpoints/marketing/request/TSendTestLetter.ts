import { TEditLetterTemplateRequest } from './TEditLetterTemplate';

export type TSendTestLetterRequest = {
    letterTemplateDataForNotification: TEditLetterTemplateRequest;
    email: string;
};