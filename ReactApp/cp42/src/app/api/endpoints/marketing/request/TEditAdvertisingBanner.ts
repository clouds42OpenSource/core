import { TCreateAdvertisingBannerRequest } from './TCreateAdvertisingBanner';

export type TEditAdvertisingBannerRequest = TCreateAdvertisingBannerRequest & {
    templateId: number;
};