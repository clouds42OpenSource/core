export type TGetLetterTemplateDetailsRequest = {
    templateId: number;
};