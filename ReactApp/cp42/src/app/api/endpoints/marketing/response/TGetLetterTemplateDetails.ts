import { TEditLetterTemplateRequest } from '../request';

type TBannerTemplateShortInfo = {
    id: number;
    header: string;
};

export type TGetLetterTemplateDetailsResponse = TEditLetterTemplateRequest & {
    advertisingBannerTemplatesShortInfo: TBannerTemplateShortInfo[];
    category: string;
};