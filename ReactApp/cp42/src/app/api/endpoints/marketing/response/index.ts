export * from './TGetAdvertisingBanner';
export * from './TGetAdvertisingBannerDetails';
export * from './TGetLetterTemplateDetails';
export * from './TCalculateAudience';
export * from './TGetLetterTemplates';