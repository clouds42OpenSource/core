export type TCalculateAudienceResponse = {
    accountsCount: number;
    accountUsersCount: number;
};