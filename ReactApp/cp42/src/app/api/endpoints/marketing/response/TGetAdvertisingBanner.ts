export type TGetAdvertisingBannerResponse = {
    id: number;
    header: string;
    body: string;
    captionLink: string;
    showFrom: string;
    showTo: string;
};