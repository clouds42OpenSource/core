export type TGetLetterTemplatesResponse = {
    id: number;
    subject: string;
    letterCategory: string;
    isSystem: boolean;
    advertisingBannerTemplateName: string;
};