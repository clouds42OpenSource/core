import { TEditAdvertisingBannerRequest } from '../request';
import { TCalculateAudienceResponse } from './TCalculateAudience';

export type TGetAdvertisingBannerDetailsResponse = TEditAdvertisingBannerRequest & {
    calculationAdvertisingBannerAudience: TCalculateAudienceResponse;
};