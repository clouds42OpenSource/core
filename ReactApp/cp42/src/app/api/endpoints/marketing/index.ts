import { apiHost, getFetchedData, templateErrorText, toQueryParams } from 'app/api/fetch';
import { WebVerb } from 'core/requestSender/enums';
import { TAppDispatch } from 'app/redux';
import { getReturnLightObject } from 'app/api/utility';
import {
    TCalculateAudienceRequest,
    TDeleteAdvertisingBannerRequest,
    TEditLetterTemplateRequest,
    TGetAdvertisingBannerDetailsRequest,
    TGetAdvertisingBannerImageRequest, TGetLetterTemplateDetailsRequest, TSendTestLetterRequest
} from 'app/api/endpoints/marketing/request';
import {
    TCalculateAudienceResponse, TGetAdvertisingBannerDetailsResponse,
    TGetAdvertisingBannerResponse, TGetLetterTemplateDetailsResponse,
    TGetLetterTemplatesResponse
} from 'app/api/endpoints/marketing/response';
import {
    getAdvertisingBannersListSlice,
    getLetterTemplatesListSlice
} from 'app/modules/marketing';

const getAdvertisingBannersList = (dispatch: TAppDispatch) => {
    dispatch(getAdvertisingBannersListSlice.actions.loading());

    getFetchedData<TGetAdvertisingBannerResponse[], void>({
        url: apiHost('marketing/advertising-banners/templates'),
        method: WebVerb.GET
    }).then(({ responseBody: { data, success, message } }) => {
        dispatch(getAdvertisingBannersListSlice.actions.empty());

        if (success && data && !message) {
            dispatch(getAdvertisingBannersListSlice.actions.success(data));
        } else {
            dispatch(getAdvertisingBannersListSlice.actions.error(message ?? templateErrorText));
        }
    });
};

const createAdvertisingBanner = async (body: FormData) => {
    const params = await getFetchedData<null, FormData>({
        url: apiHost('marketing/advertising-banners/templates'),
        method: WebVerb.POST,
        body,
        isFormData: true
    });

    return getReturnLightObject<null>(params.responseBody);
};

const editAdvertisingBanner = async (body: FormData) => {
    const params = await getFetchedData<null, FormData>({
        url: apiHost('marketing/advertising-banners/templates'),
        method: WebVerb.PUT,
        body,
        isFormData: true
    });

    return getReturnLightObject<null>(params.responseBody);
};

const deleteAdvertisingBanner = async (query: TDeleteAdvertisingBannerRequest) => {
    const params = await getFetchedData<null, void>({
        url: apiHost(`marketing/advertising-banners/templates${ toQueryParams(query) }`),
        method: WebVerb.DELETE,
    });

    return getReturnLightObject<null>(params.responseBody);
};

const getAdvertisingBannerDetails = async (query: TGetAdvertisingBannerDetailsRequest) => {
    const params = await getFetchedData<TGetAdvertisingBannerDetailsResponse, void>({
        url: apiHost(`marketing/advertising-banners/templates/details${ toQueryParams(query) }`),
        method: WebVerb.GET,
    });

    return getReturnLightObject<TGetAdvertisingBannerDetailsResponse>(params.responseBody);
};

const getAdvertisingBannerImage = async (query: TGetAdvertisingBannerImageRequest) => {
    const params = await getFetchedData<null, void>({
        url: apiHost(`marketing/advertising-banners/image${ toQueryParams(query) }`),
        method: WebVerb.GET,
    });

    return getReturnLightObject<null>(params.responseBody);
};

const getLetterTemplatesList = (dispatch: TAppDispatch) => {
    dispatch(getLetterTemplatesListSlice.actions.loading());

    getFetchedData<TGetLetterTemplatesResponse[], void>({
        url: apiHost('marketing/letters/templates'),
        method: WebVerb.GET
    }).then(({ responseBody: { data, success, message } }) => {
        dispatch(getLetterTemplatesListSlice.actions.empty());

        if (success && data && !message) {
            dispatch(getLetterTemplatesListSlice.actions.success(data));
        } else {
            dispatch(getLetterTemplatesListSlice.actions.error(message ?? templateErrorText));
        }
    });
};

const editLetterTemplate = async (body: TEditLetterTemplateRequest) => {
    const params = await getFetchedData<boolean, TEditLetterTemplateRequest>({
        url: apiHost('marketing/letters/templates'),
        method: WebVerb.PUT,
        body,
    });

    return getReturnLightObject(params.responseBody);
};

const getLetterTemplateDetails = async (query: TGetLetterTemplateDetailsRequest) => {
    const params = await getFetchedData<TGetLetterTemplateDetailsResponse, void>({
        url: apiHost(`marketing/letters/templates/details${ toQueryParams(query) }`),
        method: WebVerb.GET,
    });

    return getReturnLightObject(params.responseBody);
};

const sendTestLetter = async (body: TSendTestLetterRequest) => {
    const params = await getFetchedData<boolean, TSendTestLetterRequest>({
        url: apiHost('marketing/letters/send-test'),
        method: WebVerb.PUT,
        body,
    });

    return getReturnLightObject(params.responseBody);
};

const calculateAudience = async (body: TCalculateAudienceRequest) => {
    const params = await getFetchedData<TCalculateAudienceResponse, TCalculateAudienceRequest>({
        url: apiHost('marketing/audience/calculation'),
        method: WebVerb.PUT,
        body,
    });

    return getReturnLightObject(params.responseBody);
};

export const MARKETING = {
    createAdvertisingBanner,
    editAdvertisingBanner,
    deleteAdvertisingBanner,
    getAdvertisingBannerDetails,
    getAdvertisingBannerImage,
    editLetterTemplate,
    getLetterTemplateDetails,
    sendTestLetter,
    calculateAudience
};

export const MARKETING_REDUX = {
    getAdvertisingBannersList,
    getLetterTemplatesList
};