import { TFeedbackItem, TPutFeedback } from 'app/api/endpoints/feedback/request';
import { apiHost, getFetchedData, userId } from 'app/api/fetch';
import { WebVerb } from 'core/requestSender/enums';

const feedbackUrl = apiHost('feedback');

const putFeedback = async (body: Omit<TFeedbackItem, 'accountUserId'>) => {
    const { responseBody } = await getFetchedData<object, TPutFeedback>({
        url: feedbackUrl,
        method: WebVerb.PUT,
        body: {
            feedback: {
                ...body,
                accountUserId: userId()
            }
        }
    });

    return responseBody;
};

export const FEEDBACK = {
    putFeedback
} as const;