export type TFeedbackItem = {
    id: string;
    accountUserId: string;
    delay: string;
};

export type TPutFeedback = {
    feedback: TFeedbackItem;
};