export type TEditAccountSupplierRequest = {
    supplierId: string;
    accountId: string;
};