import { apiHost, getFetchedData } from 'app/api/fetch';
import { getReturnLightObject } from 'app/api/utility';
import { WebVerb } from 'core/requestSender/enums';
import { TEditAccountSupplierRequest } from './request';

const editAccountSupplier = async (supplierId: string, accountId: string) => {
    const { responseBody } = await getFetchedData<boolean, TEditAccountSupplierRequest>({
        url: apiHost('suppliers-reference/account'),
        method: WebVerb.POST,
        body: {
            accountId,
            supplierId
        }
    });

    return getReturnLightObject(responseBody);
};

export const SUPPLIER_REFERENCE = {
    editAccountSupplier
} as const;