type Treferences = {
  id: string;
  name: string;
  type: string;
};
export type SavedObject<T> = {
  id: string;
  type: string;
  attributes: T;
  references?: Treferences[];
  migrationVersion?: Record<string, string>;
  updated_at?: string;
  version?: string;
};

export type DashboardAttributes = {
  title: string;
  description?: string;
  hits?: number;
  kibanaSavedObjectMeta?: {
    searchSourceJSON: string;
  };
};

type SavedObjectsFindResponse<T> = {
  page: number;
  per_page: number;
  total: number;
  saved_objects: SavedObject<T>[];
};

export type getKibanaDashboardsResponseDto = SavedObjectsFindResponse<DashboardAttributes>;