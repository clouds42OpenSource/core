import { getKibanaDashboardsResponseDto } from 'app/api/endpoints/kibana/response';
import { getFetchedData } from 'app/api/fetch';
import { encodeToBase64 } from 'app/utils';
import { WebVerb } from 'core/requestSender/enums';

const getKibanaDashboards = async () => {
    const { responseBody } = await getFetchedData<getKibanaDashboardsResponseDto, void>({
        url: `${ import.meta.env.REACT_KIBANA_URL }/saved_objects/_find?type=dashboard&per_page=100`,
        method: WebVerb.GET,
        noAuthorizationToken: true,
        noCredentials: true,
        hasStandardResponseType: false,
        headers: {
            Authorization: `Basic ${ encodeToBase64(`${ import.meta.env.REACT_KIBANA_LOGIN }:${ import.meta.env.REACT_KIBANA_PASSWORD }`) }`
        }
    });

    return responseBody;
};

export const KIBANA = {
    getKibanaDashboards
} as const;