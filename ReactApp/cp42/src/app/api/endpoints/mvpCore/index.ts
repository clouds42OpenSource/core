import { apiHost, getFetchedData } from 'app/api/fetch';
import { WebVerb } from 'core/requestSender/enums';
import { getExpectationDBMSDescriptionResponseDto } from './response';

const expectationUrl = apiHost('waits');

const getExpectationDBMSDescription = async (params: string[]) => {
    const query = `?${ params.map(value => `filter.waitTypes=${ value }`).join('&') }`;
    const { responseBody } = await getFetchedData<getExpectationDBMSDescriptionResponseDto[], void>({
        url: `${ expectationUrl }${ query }`,
        method: WebVerb.GET,
    });

    return responseBody;
};

export const MVPCORE = {
    getExpectationDBMSDescription
} as const;