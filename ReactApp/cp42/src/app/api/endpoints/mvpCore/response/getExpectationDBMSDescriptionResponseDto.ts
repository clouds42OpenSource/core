export type getExpectationDBMSDescriptionResponseDto = {
  waitType: string;
  description: string;
  recommendations: string[];
  causes: string[];
};