export type postLKSQLResponseDto = {
    lkid: string;
    lks3status: string;
    lks3url: string;
    lkcfstatus: string;
    lkgitstatus: string;
    lkrepostatus: string;
    lksquserstatus: string;
    lksqprojectstatus: string;
    lksqscanstatus: string;
    lksqurl: string;
    lksquser: string;
    lksqpassword: string;
    lkdate: string;
}