export enum zabbixApiUrlEnum {
    userLogin = 'user.login',
    historyGet = 'history.get',
    hostGet = 'host.get',
    hostgroupGet = 'hostgroup.get',
    itemGet = 'item.get',
    trendGet = 'trend.get'
}