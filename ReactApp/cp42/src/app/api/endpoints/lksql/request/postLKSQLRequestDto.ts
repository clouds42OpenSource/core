export type postLKSQLRequestDto = {
    lkuserlogin: string;
    lkuseremail: string;
    lks3status: string;
    lkdate: string;
    lks3url: string;
};