import { postLKSQLRequestDto } from 'app/api/endpoints/lksql/request/postLKSQLRequestDto';
import { postLKSQLResponseDto } from 'app/api/endpoints/lksql/response/postLKSQLResponseDto';
import { getFetchedData } from 'app/api/fetch';
import { encodeToBase64 } from 'app/utils';
import { WebVerb } from 'core/requestSender/enums';

const lksqlURL = import.meta.env.REACT_LKSQL_URL;
const basicToken = `Basic ${ encodeToBase64(`${ import.meta.env.REACT_LKSQL_LOGIN }:${ import.meta.env.REACT_LKSQL_PASSWORD }`) }`;

const params = {
    noAuthorizationToken: true,
    noCredentials: true,
    hasStandardResponseType: false,
    headers: {
        Prefer: 'return=representation',
        Authorization: basicToken
    }
};

const getLKSQLDataList = async (login: string) => {
    const { responseBody } = await getFetchedData<postLKSQLResponseDto[], void>({
        url: `${ lksqlURL }/lksq?select=lkcfstatus,lkdate,lkgitstatus,lkid,lkrepostatus,lks3status,lks3url,lksqpassword,lksqprojectstatus,lksqscanstatus,lksqurl,lksquser,lksquserstatus&lkuserlogin=eq.${ login }`,
        method: WebVerb.GET,
        ...params
    });

    return responseBody;
};

const getLKSQLData = async (requestParam: string) => {
    const { responseBody } = await getFetchedData<postLKSQLResponseDto[], void>({
        url: `${ lksqlURL }/lksq?select=lkcfstatus,lkdate,lkgitstatus,lkid,lkrepostatus,lks3status,lks3url,lksqpassword,lksqprojectstatus,lksqscanstatus,lksqurl,lksquser,lksquserstatus,lkuseremail&lkid=eq.${ requestParam }`,
        method: WebVerb.GET,
        ...params
    });

    return responseBody;
};

const postLKSQLData = async (requestParam: postLKSQLRequestDto) => {
    const { responseBody } = await getFetchedData<postLKSQLResponseDto[], postLKSQLRequestDto>({
        url: `${ lksqlURL }/lksq`,
        method: WebVerb.POST,
        ...params,
        body: requestParam
    });

    return responseBody;
};

export const LKSQL = {
    getLKSQLDataList,
    getLKSQLData,
    postLKSQLData
} as const;