import { TInvoiceDocument } from 'app/api/endpoints/invoices/request/postClosingDocuments';
import { WebVerb } from 'core/requestSender/enums';
import { apiHost, contextAccountId, getFetchedData, toQueryParams } from 'app/api/fetch';
import { getReturnLightObject } from 'app/api/utility';
import { TGetDocumentResponse } from './response';

const deleteInvoice = async (invoiceId: string) => {
    const { responseBody } = await getFetchedData<boolean, void>({
        url: apiHost(`billing-accounts/cancel-invoice/${ invoiceId }`),
        method: WebVerb.POST
    });

    return getReturnLightObject(responseBody);
};

const getDocuments = async (invoiceIds: string[]) => {
    const { responseBody } = await getFetchedData<null, void>({
        url: apiHost(`closing-documents/required${ toQueryParams({ filter: { invoiceIds } }) }`),
        method: WebVerb.GET
    });

    return responseBody;
};

const uploadSignedDocument = async (body: FormData) => {
    const { responseBody } = await getFetchedData<TGetDocumentResponse, FormData>({
        url: apiHost('closing-documents/upload-signed'),
        method: WebVerb.POST,
        body,
        isFormData: true,
    });

    return responseBody;
};

const postClosingDocuments = async (invoice: TInvoiceDocument) => {
    const formData = new FormData();

    formData.append('accountId', contextAccountId());
    formData.append('invoice.invoiceId', invoice.invoiceId);
    formData.append('invoice.actId', invoice.actId);
    formData.append('invoice.file', invoice.file);

    const { responseBody } = await getFetchedData({
        url: apiHost('closing-documents'),
        method: WebVerb.POST,
        body: formData,
        isFormData: true,
    });

    return responseBody;
};

export const INVOICE = {
    deleteInvoice,
    getDocuments,
    uploadSignedDocument,
    postClosingDocuments
} as const;