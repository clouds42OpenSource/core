export type TGetDocumentResponse = {
    invoiceId: string;
    fileId: string;
    status: number;
    contentType: string;
    fileName: string;
    content: string;
};