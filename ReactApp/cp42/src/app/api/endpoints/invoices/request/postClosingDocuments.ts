export type TInvoiceDocument = {
    invoiceId: string;
    actId: string;
    file: File;
}

export type TPostClosingDocuments = {
    accountId: string;
    invoice: TInvoiceDocument;
}