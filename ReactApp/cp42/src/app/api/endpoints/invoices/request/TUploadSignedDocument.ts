export type TUploadSignedDocumentRequest = {
    fileId: string;
    content: string;
    accountId: string;
};