export type CreateDatabasesModelDcItem = {
    /**
     * Идентификатор аккаунта
     */
    accountId: string;
    /**
     * Идентификатор шаблона
     */
    templateId: string;
    /**
     * Имя создаваемой базы
     */
    dataBaseName: string;
    /**
     * Выбрана или нет
     */
    isChecked: boolean;
    /**
     * Необходимость публикации
     */
    publish: boolean;
    /**
     * Необходимость добавления демо-данных
     */
    demoData: boolean;
    /**
     * Файловая или серверная, по умолчанию true (для разделителей - false)
     */
    isFile: boolean;
    /**
     * На разделителях
     */
    dbTemplateDelimiters: boolean;
    /**
     * Список пользователей для предоставления доступов
     */
    usersToGrantAccess: string[];
    hasAutoupdate: boolean;
    hasSupport: boolean;
};

export type PostFromTemplateRequest = {
    /**
     * Необходимость использования ОП
     */
    needUsePromisePayment: boolean;
    /**
     * Список моделей инф. баз
     */
    createDatabasesModelDc: CreateDatabasesModelDcItem[];
};