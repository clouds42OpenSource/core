export type GetTemplatesRequest = {
    onlyTemplatesOnDelimiters?: boolean;
};