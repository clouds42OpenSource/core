import { GetTemplatesRequest, PostActivateRentIfNeedRequest } from 'app/api/endpoints/createDb/request';
import { PostFromTemplateRequest } from 'app/api/endpoints/createDb/request/postFromTemplateRequest';
import { GetTemplatesResponse, PostFromTemplateResponse, TCreateStatusResponse } from 'app/api/endpoints/createDb/response';
import { apiHost, getFetchedData, msHost } from 'app/api/fetch';
import { TResponseLowerCase } from 'app/api/types';
import { localStorageHelper } from 'app/common/helpers/LocalStorageHelper';
import { WebVerb } from 'core/requestSender/enums';

const createDbUrl = apiHost('account-databases');
const statusCreateDbUrl = msHost('pl42/hs/Platform/applications');

const getTemplates = async (requestParams: GetTemplatesRequest): Promise<TResponseLowerCase<GetTemplatesResponse>> => {
    const { responseBody } = await getFetchedData<GetTemplatesResponse, void>({
        url: `${ createDbUrl }/templates?accountId=${ localStorageHelper.getContextAccountId() }&onlyTemplatesOnDelimiters=${ requestParams.onlyTemplatesOnDelimiters ?? false }`,
        method: WebVerb.GET
    });

    return {
        success: responseBody.success,
        data: responseBody.data ?? null,
        message: responseBody.message
    };
};

const postFromTemplate = async (requestParam: PostFromTemplateRequest): Promise<TResponseLowerCase<PostFromTemplateResponse>> => {
    const { responseBody } = await getFetchedData<PostFromTemplateResponse, PostFromTemplateRequest>({
        url: `${ createDbUrl }/from-template`,
        method: WebVerb.POST,
        body: requestParam
    });

    return {
        success: responseBody.success,
        data: responseBody.data ?? null,
        message: responseBody.message
    };
};

const postActivateRentIfNeed = async () => {
    return getFetchedData<null, PostActivateRentIfNeedRequest>({
        url: `${ createDbUrl }/${ localStorageHelper.getContextAccountId() ?? '' }/activate-rent-if-need`,
        method: WebVerb.POST
    });
};

const getStatusCreateDb = async (idDatabase: string[]) => {
    const { responseBody } = await getFetchedData<TCreateStatusResponse, object>({
        url: `${ statusCreateDbUrl }?id=${ idDatabase.join(',') }`,
        method: WebVerb.GET,
        hasStandardResponseType: false
    });

    return responseBody;
};

export const CRETE_DB = {
    getTemplates,
    postFromTemplate,
    postActivateRentIfNeed,
    getStatusCreateDb
} as const;