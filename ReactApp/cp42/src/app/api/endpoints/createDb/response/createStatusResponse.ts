export type TStatusItem = {
    id: string;
    errorCode: string;
    timeout: number;
    state: string;
    error: string;
};

export type TCreateStatusResponse = {
    Results: TStatusItem[];
};