export type AvailableTemplatesItem = {
    /**
     * Идентификатор шаблона
     */
    id: string;
    /**
     * Идентификатор услуги сервиса “Мои информационные базы”
     */
    myDatabasesServiceTypeId: string | null;
    /**
     * Название
     */
    caption: string;
    /**
     * Название для разделителей
     */
    captionOnDelimiters: string;
    /**
     * Доступность демо-данных
     */
    isDemoDataAvailable: boolean;
    /**
     * Возможность опубликовать шаблон
     */
    canWebPublish: boolean;
    /**
     * Шаблон базы на разделителях
     */
    isDbTemplateDelimiters: boolean;
    /**
     * Данные демо-шаблона базы на разделителях
     */
    dbTemplateOnDelimitersDemoData: {
        /**
         * Наличие базы на разделителях с демо-данными
         */
        hasDbOnDelimitersWithDemoData: boolean;
        /**
         * Путь к опубликованной базе
         */
        webPublishPath: string;
    },
    /**
     * Ссылка на изображение шаблона
     */
    templateImgUrl: string;
    /**
     * Версия шаблона
     */
    versionTemplate: string;
};

export type AvailableUsersForGrantAccessItem = {
    /**
     * Идентификатор пользователя
     */
    userId: string;
    /**
     * Логин пользователя
     */
    userLogin: string;
    /**
     * Фамилия пользователя
     */
    userLastName: string;
    /**
     * Имя пользователя
     */
    userFirstName: string;
    /**
     * Отчество пользователя
     */
    userMiddleName: string;
    /**
     * E-mail пользователя
     */
    userEmail: string;
    /**
     * Номер аккаунта пользователя
     */
    accountIndexNumber: number;
    /**
     * Название аккаунта пользователя
     */
    accountCaption: string;
    /**
     * ФИО пользователя
     */
    userFullName: string;
    /**
     * Информация об аккаунте пользователя
     */
    accountInfo: string;
    /**
     * Наличие доступа
     */
    hasAccess: boolean;
};

export type GetTemplatesResponse = {
    /**
     * Признак доступности сервиса
     */
    isMainServiceAllow: boolean;
    /**
     * Идентификатор сервиса “Мои информационные базы”
     */
    myDatatbasesServiceId: string;
    /**
     * Доступные для создания шаблоны
     */
    availableTemplates: AvailableTemplatesItem[];
    /**
     * Доступные пользователи для выдачи доступа к информационной базе
     */
    availableUsersForGrantAccess: AvailableUsersForGrantAccessItem[];

};