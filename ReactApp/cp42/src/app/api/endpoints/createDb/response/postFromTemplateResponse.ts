export type PostFromTemplateResponse = {
    isComplete: boolean;
    errorMessage: string;
    notEnoughMoney: number;
}