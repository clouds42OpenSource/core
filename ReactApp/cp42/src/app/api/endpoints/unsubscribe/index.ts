import { apiHost, getFetchedData } from 'app/api/fetch';
import { getReturnObject } from 'app/api/utility';
import { WebVerb } from 'core/requestSender/enums';

const unsubscribeUrl = apiHost('emails');

const tryUnsubscribe = async (code: string) => {
    const response = await getFetchedData<boolean, void>({
        method: WebVerb.GET,
        url: `${ unsubscribeUrl }/unsubscribe?code=${ code }`,
        noAuthorizationToken: true,
    });

    return getReturnObject(response);
};

export const UNSUBSCRIBE = {
    tryUnsubscribe,
} as const;