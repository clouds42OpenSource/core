export type myDiskVolumeInfo = {
    availableSize: number;
    freeSizeOnDisk: number;
    serverDataBaseSize: number;
    fileDataBaseSize: number;
    clientFilesSize: number;
    usedSizeOnDisk: number;
};

export type getSizeChangeCost = {
    cost: number;
    myDiskVolumeInfo: myDiskVolumeInfo;
};

export type getMyDiskManagementResponseDto = {
    accountIsVip: boolean;
    tariff: number;
    expireDate: string;
    discountGroup: number;
    cost: number;
    locale: {
        name: string;
        currency: string;
    };
};

export type changeTariffResponseDto = {
    balance: number;
    canGetPromisePayment: boolean;
    complete: boolean;
    costOftariff: number;
    currency: string | null;
    enoughMoney: number;
    error: string | null;
    monthlyCost: number;
};

export type getMyDiskResponseDto = {
    expireDate: string;
    serviceStatus: {
        serviceIsLocked: boolean;
        serviceLockReason: string;
        serviceLockReasonType: 0 | 1 | 2;
        promisedPaymentIsActive: boolean;
        promisedPaymentSum: string;
        promisedPaymentExpireDate: string;
    };
    monthlyCost: number;
    locale: {
        name: string;
        currency: string;
    };
    currentTariff: number;
    myDiskVolumeInfo: myDiskVolumeInfo;
    clientFilesCalculateDate: string;
    accountIsVip: boolean;
    recalculationTaskId: string;
};