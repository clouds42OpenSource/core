import { TSendEmailRequest, sendEmailRequestDto, TAttachments } from 'app/api/endpoints/emails/request';
import { apiHost, getFetchedData } from 'app/api/fetch';
import { TResponseLowerCase } from 'app/api/types';
import { WebVerb } from 'core/requestSender/enums';

const emailsUrl = apiHost('emails');

const sendEmail = async (requestParams: TSendEmailRequest): Promise<TResponseLowerCase<string>> => {
    const { responseBody } = await getFetchedData<string, sendEmailRequestDto>({
        url: `${ emailsUrl }/send`,
        method: WebVerb.POST,
        body: {
            Body: `<div style="padding: 8px; max-width: 400px; word-wrap: break-word;">${ requestParams.body }</div>`,
            Subject: requestParams.subject,
            Categories: [requestParams.categories],
            Locale: 'ru-ru',
            Header: requestParams.header,
            To: requestParams.reference || '',
            CopyTo: [],
            Attachments: requestParams.attachments?.map(attachments => ({
                Bytes: attachments.bytes,
                FileName: attachments.fileName
            })) ?? [],
            SendGridTemplateId: import.meta.env.REACT_SEND_GRID_TEMPLATE_ID
        }
    });

    return responseBody;
};

const sendEmailBySupportPage = async (body: string, headerInfo: string, responseEmail?: string, attachments?: TAttachments[]) => {
    const emailResponse = await sendEmail({
        reference: import.meta.env.REACT_MANAGER_EMAIL ?? '',
        body,
        categories: 'support',
        header: headerInfo,
        subject: headerInfo,
        attachments
    });

    if (responseEmail) {
        await sendEmail({
            reference: responseEmail,
            body: `
                <p>Благодарим Вас за обращение!</p>
                <p>Ваша заявка успешно отправлена и в ближайшее время будет взята в работу нашей командой. Мы постараемся обработать ваш запрос как можно скорее, чтобы предоставить вам качественное решение.</p>
                <br/>
                <p>Спасибо, что выбрали нас! Мы ценим ваше доверие и с радостью поможем вам.</p>
                <br/>
                <p>С уважением, команда 42Clouds.</p>
            `,
            categories: 'support',
            header: 'Заявка успешно отправлена',
            subject: 'Ваша заявка успешно отправлена',
        });
    }

    return emailResponse;
};

export const EMAILS = {
    sendEmail,
    sendEmailBySupportPage
} as const;