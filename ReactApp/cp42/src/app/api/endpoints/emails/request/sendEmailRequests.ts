export type TAttachmentsDto = {
    Bytes: string;
    FileName: string;
}

export type TAttachments = {
    bytes: string;
    fileName: string;
}

export type sendEmailRequestDto = {
    Subject: string;
    Header: string;
    Categories: string[];
    Body: string;
    SendGridTemplateId: string;
    Locale: string;
    To: string;
    CopyTo: string[];
    Attachments: TAttachmentsDto[];
};

export type TSendEmailRequest = {
    body: string;
    subject: string;
    header: string;
    reference: string;
    categories: string;
    attachments?: TAttachments[]
};