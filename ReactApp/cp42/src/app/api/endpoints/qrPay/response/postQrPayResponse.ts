export type PostQrPayResponse = {
    paymentId: string;
    paymentSystem: string;
    totalSum: number;
    token: string;
    type: string;
    redirectUrl: string;
};