export type PostQrPayRequest = {
    accountId: string;
    totalSum: number;
};