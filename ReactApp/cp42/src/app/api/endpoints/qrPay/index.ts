import { PostQrPayRequest } from 'app/api/endpoints/qrPay/request';
import { PostQrPayResponse } from 'app/api/endpoints/qrPay/response';
import { apiHost, contextAccountId, getFetchedData } from 'app/api/fetch';
import { TResponseLowerCase } from 'app/api/types';
import { WebVerb } from 'core/requestSender/enums';

const qrPay = apiHost('payments/qr-pay');

const postQrPay = async (params: Omit<PostQrPayRequest, 'accountId'>): Promise<TResponseLowerCase<PostQrPayResponse>> => {
    const { responseBody } = await getFetchedData<PostQrPayResponse, PostQrPayRequest>({
        url: qrPay,
        method: WebVerb.POST,
        body: {
            accountId: contextAccountId(),
            totalSum: params.totalSum
        }
    });

    return responseBody;
};

export const QR_PAY = {
    postQrPay
} as const;