import { TDataLightReturn, TDataReturn, TFetchedData, TResponseLowerCase } from 'app/api/types';

export const getReturnObject = <R>({ responseBody, status, headers }: TFetchedData<R>): TDataReturn<R | null> => ({
    data: {
        rawData: responseBody.data,
        status,
        headers
    },
    error: responseBody.message,
    isLoading: false
});

export const getReturnLightObject = <R>(responseBody: TResponseLowerCase<R>): TDataLightReturn<R | null> => ({
    data: responseBody.data,
    error: responseBody.message,
    isLoading: false
});