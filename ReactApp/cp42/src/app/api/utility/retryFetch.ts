export const retryFetch = <T>(callback: () => Promise<T>, retries = 5, interval = 5000, isAborted = false): Promise<T> => {
    return new Promise((resolve, reject) => {
        callback()
            .then(resolve)
            .catch(error => {
                if (retries === 1) {
                    reject(error);
                    return;
                }

                if (!isAborted) {
                    setTimeout(() => {
                        retryFetch(callback, retries - 1, interval).then(resolve, reject);
                    }, interval);
                }
            });
    });
};