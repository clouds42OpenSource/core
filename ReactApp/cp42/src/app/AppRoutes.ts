const basePath = '';

/**
 * Содержит Routes приложения
 */
export const AppRoutes = {
    /**
     * Route стартовой страницы
     *     {basePath}
     */
    homeRoute: `${ basePath }/`,

    /**
     * Путь до прокси страницы
     */
    proxyPage: `${ basePath }/_`,

    /**
     * Путь до страницы с ошибкой 404
     */
    notFound: `${ basePath }/not-found`,

    /**
     * Путь до страницы с ошибкой 500
     */
    serverError: `${ basePath }/server-error`,

    /**
     * Путь до страницы с ошибкой отсутствия средств
     */
    noMoneyNoHoney: `${ basePath }/no-money-no-honey`,

    /**
     * Путь до страницы c ошибкой прав доступа
     */
    accessError: `${ basePath }/access-error`,

    /**
     * Route для аутентификации
     *     {basePath}/authentication
     */
    authenticationRoute: `${ basePath }/authentication`,

    /**
     * Route для авторизации
     *     {basePath}/signin
     */
    signInRoute: `${ basePath }/signin`,

    /**
     * Route для авторизации
     *     {basePath}/signup
     */
    signUpRoute: `${ basePath }/signup`,

    /**
     * Rout для восстановления пароля
     * {basePath}/resetpassword
     */
    resetPassword: `${ basePath }/resetpassword`,

    /**
     * Rout для отписки от рассылок
     * {basePath}/unsubscription
     */
    unsubscribe: `${ basePath }/unsubscription`,

    /**
     * Route для скачивания актуальной версии Линк42
     */
    link42Downloader: `${ basePath }/link42-downloader`,

    /**
     * Route для промо страницы сервиса Нейро42
     */
    neuro: `${ basePath }/neuro42`,

    /**
     * Содержит Routes приложения для администрирования
     */
    administration: {
        /**
         * Route для работы с логированием
         */
        loggingRoute: `${ basePath }/administration/logging`,

        /**
         * Путь на страницу Маркетинг
         */
        marketing: `${ basePath }/administration/marketing`,

        /**
         * Путь на страницу Админ панель -> Партнеры
         */
        partners: `${ basePath }/administration/partners`,

        /**
         * Путь для работы с задачами воркера
         */
        tasksRoute: `${ basePath }/administration/tasks`,

        /**
         * Путь для работы с рабочими процесами
         */
        processFlowRoute: `${ basePath }/administration/process-flow`,

        /**
         * Путь на страницу деталей рабочего процесса
         */
        getProcessFlowDetailsRoute: `${ basePath }/administration/process-flow/details/:id`,

        /**
         * Путь для работы с служебными аккаунтами
         */
        serviceAccountsRoute: `${ basePath }/administration/service-accounts`,

        /**
         * Путь для работы с конфигурациями 1С
         */
        configurations1CRoute: `${ basePath }/administration/configurations-1c`,

        /**
         * Путь для работы с отраслями
         */
        industryRoute: `${ basePath }/administration/industry`,

        /**
         * Путь для работы с шаблонами
         */
        dbTemplatesRoute: `${ basePath }/administration/db-templates`,

        /**
         * Путь для работы с базами на разделителях
         */
        dbTemplateDelimitersRoute: `${ basePath }/administration/db-template-delimiters`,

        /**
         * Путь для работы с агентскими соглашениями
         */
        agencyAgreementRoute: `${ basePath }/administration/agency-agreement`,

        /**
         * Путь для работы с настройками облака
         */
        settingCloudRoute: `${ basePath }/administration/cloud-core`,

        /**
         * Путь для работы с списком баз
         */
        accountDatabasesListRoute: `${ basePath }/administration/account-databases`,

        /**
         * Путь для работы с списком аккаунтов
         */
        accountsListRoute: `${ basePath }/administration/accounts`,

        /**
         * Путь для работы с обновленными шаблонами базы
         */
        dbTemplateUpdatesRoute: `${ basePath }/administration/db-template-updates`,

        /**
         * Путь для работы с поставщиками
         */
        suppliersRoute: `${ basePath }/administration/supplier-reference`,

        /**
         * Путь для работы с локалями
         */
        localesRoute: `${ basePath }/administration/locales`,

        /**
         * Путь для работы с печатными формами HTML
         */
        printedHtmlFormsReferenceRoute: `${ basePath }/administration/printed-html-form-reference`,

        /**
         * Путь для работы с сегментами
         */
        segmentsRoute: `${ basePath }/administration/cloud-segments-reference`,

        /**
         * Путь для работы с доступом к ИТС
         */
        itsAuthorizationDataRoute: `${ basePath }/administration/its-authorization-data`,

        /**
         * Путь для работы с ARM Автообновлениями
         */
        armAutoUpdateAccountDatabase: `${ basePath }/administration/auto-update-account-db`,

        /**
         * Содержит Routes приложения для справочников
         */
        dictionary: {
            /**
             * Route для редактирования справочника CloudServices
             *     {basePath}/administration/dictionary/cloud-services
             */
            editCloudServicesRoute: `${ basePath }/administration/dictionary/cloud-services`,

            /**
             * Путь для работы с материнскими базами разделителей
             */
            delimiterSourceAccountDatabasesRoute: `${ basePath }/administration/dictionary/delimiter-source-databases`
        },
        /**
         * Содержит Routes для поставщиков
         */
        suppliers: {
            /**
             * Путь на страницу редактирования/создания поставщика
             */
            editCreateSupplier: `${ basePath }/administration/supplier-reference/edit-create-supplier/:id?`,

            editCreateSupplierPath: `${ basePath }/administration/supplier-reference/edit-create-supplier`,

            /**
             * Путь на страницу реферальным аккаунтам
             */
            supplierReferalAccounts: `${ basePath }/administration/supplier-reference/referal-accounts/:id`,

            supplierReferalAccountsPath: `${ basePath }/administration/supplier-reference/referal-accounts`
        },

        /**
         * Содержит Routes для печатных форм HTML
         */
        printedHtmlForms: {
            /**
             * Путь на страницу создания печатных форм HTML
             */
            createPrintedHtmlForm: `${ basePath }/administration/create-printed-html-form`,

            /**
             * Путь на страницу редактирования печатных форм HTML
             */
            editPrintedHtmlForm: `${ basePath }/administration/edit-printed-html-form/:id`,

            editPrintedHtmlFormPath: `${ basePath }/administration/edit-printed-html-form`
        },
        /**
         * Путь для работы с настройкой рассылок уведомлений
         */
        sendingNotifications: `${ basePath }/administration/sending-notifications`
    },
    /**
     * Содержит Routes для
     */
    accountManagement: {
        /**
         * Путь на страницу Управления Аккаунтом => Баланс
         */
        balanceManagement: `${ basePath }/billing`,
        /**
         * Путь на страницу выставления счета Управления Аккаунтом => Баланс => Пополнить баланс
         */
        replenishBalance: `${ basePath }/billing/replenish-balance`,
        /**
         * Путь для подтверждения оплаты
         */
        confirmPayment: `${ basePath }/billing/confirm-payment`,
        /**
         * Путь на страницу управления пользователями аккаунта Управления Аккаунтом => Пользователи
         */
        userManagement: `${ basePath }/users`,
        /**
         * Путь на страницу АРМ
         */
        armManager: `${ basePath }/arm-manager`,
        /**
         * Путь на страницу управления пользователями аккаунта Управления Аккаунтом => Информационные базы
         */
        informationBases: `${ basePath }/my-databases`,
        legacyInformationBases: `${ basePath }/my-databases-legacy`,

        /**
         * Путь на страницу данных о компании Управление Аккаунтом => Данные О Компании
         */
        myCompany: `${ basePath }/my-company`,
        createAccountDatabase: {
            /**
             * Путь на страницу Управление аккаунтом -> Информационные базы -> Создать новую
             */
            createDb: `${ basePath }/create-db`,
            /**
             * Путь на страницу Управление аккаунтом -> Информационные базы -> Загрузить свою базу 1С
             * Для восстановления баз принимает locationProps
             *     isZip?: boolean;
             *     fileId?: string;
             *     caption?: string;
             *     date?: string;
             *     restoreType?: string;
             */
            loadingDatabase: `${ basePath }/loading-database`,
        }
    },
    services: {
        /**
         * Путь на страницу Сервисы -> Дополнительные сеансы
         */
        additionalSessions: `${ basePath }/additional-sessions`,
        /**
         * Путь на страницу Используемые сервисы -> Маркет42
         */
        m42: `${ basePath }/m42`,
        /**
         * Путь на страницу Используемые сервисы -> Расширения и обработки
         */
        myServices: `${ basePath }/my-services`,
        /**
         * Путь на страницу Сервисы -> Создание сервиса
         */
        createService: `${ basePath }/create-service`,
        /**
         * Путь на страницу Сервисы -> Сервис
         */
        serviceCard: `${ basePath }/my-services/:id`,
        /**
         * Путь на страницу Сервисы -> Карточка сервиса
         */
        editService: `${ basePath }/edit-service/:id`,
        /**
         * Путь на страницу Сервисы -> Карточка сервиса для редиректов
         */
        editServicePath: `${ basePath }/edit-service`,
        /**
         * Путь на страницу модерирования всех сервисов
         */
        moderateAllServices: `${ basePath }/moderate-services`,
        /**
         * Путь на страницу Сервисы -> Аренда 1С
         */
        rentReference: `${ basePath }/rent`,
        /**
         * Путь на страницу Сервисы -> Fasta
         */
        fasta: `${ basePath }/esdl`,
        rent: {
            /**
             * Путь на страницу Сервисы -> Аренда 1С -> Редактирование
             */
            editRentResource: `${ basePath }/rent/edit-resource/:id`,

            editRentResourcePath: `${ basePath }/rent/edit-resource`
        },
        /**
         * Путь на страницу Сервисы -> Мой диск
         */
        myDisk: `${ basePath }/my-disk`,
        /**
         * Путь на страницу Сервисы -> (саюри, дардис, ...)
         */
        managingServiceDescriptions: `${ basePath }/billing-service`,
        managingServiceSubscriptions: `${ basePath }/billing-service/:id`,
        /**
         * Путь на страницу Сервисы -> (Клиент серверный режим, Комплексная автоматизация, ...)
         */
        managingServicesMyDatabaseDescriptions: `${ basePath }/my-database-services`,
        managingServicesMyDatabaseSubscription: `${ basePath }/my-database-services/:id`,
        alpacaMeet: `${ basePath }/alpaca-meet`,
    },
    settings: {
        /**
         * Путь на страницу Настройки -> Профиль
         */
        profile: `${ basePath }/profile`,
        /**
         * Путь на страницу Настройки -> Настройки подключения
         */
        connectionSettings: `${ basePath }/connection-settings`,
        /**
         * Путь на страницу Настройки -> Консультант по 1С
         */
        consultations1C: `${ basePath }/consultations-1C`,
        /**
         * Путь на страницу Настройки -> Регистрационный номер
         */
        registrationNumber: `${ basePath }/registration-number`,
        /**
         * Путь на страницу Настройки -> 1С Отчетность
         */
        reporting: `${ basePath }/reporting`,
        /**
         * Пусть на страницу Настройки -> 1С ЭДО
         */
        edo: `${ basePath }/edo`,
        /**
         * Путь на страницу Настройки -> Акт сверки
         */
        reconciliationStatement: `${ basePath }/reconciliation-statement`,
    },
    partners: {
        /**
         * Путь на страницу Партнерам
         */
        toPartners: `${ basePath }/partners`,
        /**
         * Путь на страницу создания реквизитов
         */
        createAgentRequisites: `${ basePath }/create-agent-requisites`,
        /**
         * Путь на страницу редактирования реквизитов
         */
        editAgentRequisites: `${ basePath }/edit-agent-requisites`,
        /**
         * Путь на страницу создания заявки на выплату
         */
        createAgentCashOut: `${ basePath }/create-agent-cash-out`,
        /**
         * Путь на страницу редактирования заявки на выплату
         */
        editAgentCashOut: `${ basePath }/edit-agent-cash-out`,
    },
    marketing: {
        /**
         * Путь на страницу редактирования шаблона письма
         */
        editLetterTemplate: `${ basePath }/edit-letter-template`,
        /**
         * Путь на страницу создания рекламного баннера
         */
        createAdvertisingBanner: `${ basePath }/create-advertising-banner`,
        /**
         * Путь на страницу редактирования рекламного баннера
         */
        editAdvertisingBanner: `${ basePath }/edit-advertising-banner`
    },
    ultra: {
        servers: `${ basePath }/ultra/servers`,
        performance: `${ basePath }/ultra/performance`,
        devops: `${ basePath }/ultra/devops`,
    },
    /**
     * Страница подтверждения почты после ее смены **Переход только по ссылке**
     */
    emailVerify: `${ basePath }/email-verify`,

    articlesReference: `${ basePath }/articles`,
    articles: {
        /**
         * Путь на страницу Сервисы -> Аренда 1С -> Редактирование
         */
        editArticleForBlog: `${ basePath }/articles/for-blog/:id?`,
        editArticleForBlogReference: `${ basePath }/articles/for-blog`,
        editArticleForSite: `${ basePath }/articles/for-site/:id`,
        editArticleForSiteReference: `${ basePath }/articles/for-site`,
    },
    confirmationOfAccountDeletion: `${ basePath }/confirm-deletion`,
    confirmationAct: `${ basePath }/closing-documents/change-status`
};