import { IReducerState } from 'core/redux/interfaces';
import { ProcessIdType } from 'core/redux/types';

export const ReduxProcessActionStateUtility = {
    hasProcessInProgressState: (reducerState: IReducerState<ProcessIdType>, processId: ProcessIdType) => {
        return reducerState.reducerActions[processId]?.hasProcessActionStateChanged &&
            reducerState.reducerActions[processId].processActionState.isInProgressState;
    },
    hasProcessInSuccessState: (reducerState: IReducerState<ProcessIdType>, processId: ProcessIdType) => {
        return reducerState.reducerActions[processId]?.hasProcessActionStateChanged &&
            reducerState.reducerActions[processId].processActionState.isInSuccessState;
    },
    hasProcessInErrorState: (reducerState: IReducerState<ProcessIdType>, processId: ProcessIdType) => {
        return reducerState.reducerActions[processId]?.hasProcessActionStateChanged &&
            reducerState.reducerActions[processId].processActionState.isInErrorState;
    }
};