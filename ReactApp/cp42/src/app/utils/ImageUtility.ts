import { GetSavedPaymentMethods } from 'app/web/InterlayerApiProxy/AutoPaymentApiProxy/getSavedPaymentMethods';

/**
 * Утилита для работы с путями к картинкам
 */
export const ImageUtility = (() => {
    return {
        /**
         * Функция для путей иконок платежных систем
         * @param props данные метода оплаты
         * @returns путь в строке
         */
        getImageSrc(props: GetSavedPaymentMethods) {
            if (props.Type === 'bank_card') {
                return `${ import.meta.env.REACT_APP_CP_HOST }/img/paymentIcons/${ props.BankCardDetails?.CardType }.svg`;
            }

            return `${ import.meta.env.REACT_APP_CP_HOST }/img/paymentIcons/${ props.Type }.svg`;
        }
    };
})();