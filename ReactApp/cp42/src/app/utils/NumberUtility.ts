/**
 * Утилита для работы с числами
 */
export const NumberUtility = (() => {
    return {
        /**
         * Сконвертировать число в денежный формат
         * @param value число
         */
        numberToMoneyFormat: (value: number): string => {
            return value.toFixed(2);
        },

        /**
         * Сконвертировать размер в Мб в Гб
         * @param sizeInMb размер в Мб
         * @param defaultValue дефолтное значение если не указан размер
         */
        megabytesToClassicFormat: (defaultValue: string, sizeInMb?: number): string => {
            if (!sizeInMb) return defaultValue;

            const sizeInGb = (sizeInMb / 1024).toFixed(1);
            return `${ sizeInGb } Гб`;
        },

        /**
         * Сконвертировать размер из байта в Кб/Мб/Гб
         * @param sizeInByte размер в байтах
         */
        bytesToMegabytes: (sizeInByte: number): string => {
            if (!sizeInByte) return '0';

            if (sizeInByte >= 1000000000) {
                return `${ (sizeInByte / 1000000000).toFixed(2) } GB`;
            }

            if (sizeInByte >= 1000000) {
                return `${ (sizeInByte / 1000000).toFixed(2) } MB`;
            }
            return `${ (sizeInByte / 1000).toFixed(2) } KB`;
        }
    };
})();