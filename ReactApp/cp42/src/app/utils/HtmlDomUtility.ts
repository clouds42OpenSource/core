export const HtmlDomUtility = {
    copyTextFrom: (inputCopyFrom: HTMLInputElement) => {
        if (!inputCopyFrom) {
            return false;
        }

        let isSuccess = false;
        try {
            const selection = document.getSelection();
            const hasSelectedText = !!selection && (selection.rangeCount ?? 0) > 0
                ? selection.getRangeAt(0)
                : false;

            inputCopyFrom.select();

            isSuccess = document.execCommand('copy');

            if (hasSelectedText) {
                const currentSelection = document.getSelection();
                if (currentSelection) {
                    currentSelection.removeAllRanges();
                    currentSelection.addRange(hasSelectedText);
                }
            }
        } catch {
        }

        return isSuccess;
    }
};