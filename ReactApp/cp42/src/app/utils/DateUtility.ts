import { DateUtilityFormatType } from 'app/common/types';
import dayjs from 'dayjs';

/**
 * Утилита для работы с датой и временем
 */
export const DateUtility = (() => {
    /**
     * Возвращает строку об ошибке, то что дата в виде строки имеет неверный формат
     * @param dateValue дата в виде строки
     * @returns строку ошибки о неверном формате даты
     */
    function getInvalidDateFormatErrorMessage(dateValue: string): string {
        return `Invalid date format of the date:${ dateValue }`;
    }

    return {
        /**
         * Конвертирует дату в виде строки в Date объект
         * @param dateValue дата в виде строки, которую нужно преобразовать в объект Date
         * - ex: /Date(1238626800000)/
         * -
         * - ex: 02.02.2020
         * - ex: 02.02.2020 10:11
         * - ex: 02.02.2020, 10:11
         * - ex: 02.02.2020 10:11:45
        * - ex: 02.02.2020, 10: 11: 45
         * -
         * - 2012-02-02T10:11:45.9999-06:00
         * - 2012-02-02T10:11:45.9999+06:00
         * - 2012-02-02T10:11:45.9999Z
         * - 2012-02-02T10:11:45Z
         * - 2012-02-02T10:11:45
         *
         * @returns объект даты Date
         */
        convertToDate: (dateValue: string): Date => {
            if (!dateValue) {
                throw new Error(getInvalidDateFormatErrorMessage(dateValue));
            }
            const isoDate = dateValue.match(/(\d{4})-(\d{2})-(\d{2})[T|t](\d{2}):(\d{2}):(\d{2}(?:\.\d*)?)(([-|+](\d{2}):(\d{2})|[Z|z])?)/);
            if (isoDate && isoDate.length >= 1) {
                return new Date(isoDate[0]);
            }

            const dmyhms = dateValue.match(/(\d{1,2})\.(\d{1,2})\.(\d{4}),?\s*(\d{1,2}):(\d{1,2}):(\d{1,2})/);
            if (dmyhms && dmyhms.length >= 7) {
                const day = parseInt(dmyhms[1], 10);
                const month = parseInt(dmyhms[2], 10) - 1;
                const year = parseInt(dmyhms[3], 10);
                const hour = parseInt(dmyhms[4], 10);
                const minute = parseInt(dmyhms[5], 10);
                const second = parseInt(dmyhms[6], 10);

                return new Date(year, month, day, hour, minute, second);
            }

            const dmyhm = dateValue.match(/(\d{1,2})\.(\d{1,2})\.(\d{4}),?\s*(\d{1,2}):(\d{1,2})/);
            if (dmyhm && dmyhm.length >= 6) {
                const day = parseInt(dmyhm[1], 10);
                const month = parseInt(dmyhm[2], 10) - 1;
                const year = parseInt(dmyhm[3], 10);
                const hour = parseInt(dmyhm[4], 10);
                const minute = parseInt(dmyhm[5], 10);

                return new Date(year, month, day, hour, minute);
            }

            const dmy = dateValue.match(/(\d{1,2})\.(\d{1,2})\.(\d{4})/);
            if (dmy && dmy.length >= 4) {
                const day = parseInt(dmy[1], 10);
                const month = parseInt(dmy[2], 10) - 1;
                const year = parseInt(dmy[3], 10);

                return new Date(year, month, day);
            }

            const ymd = dateValue.match(/(\d{4})-(\d{1,2})-(\d{1,2})/);
            if (ymd && ymd.length >= 4) {
                const year = parseInt(ymd[1], 10);
                const month = parseInt(ymd[2], 10) - 1;
                const day = parseInt(ymd[3], 10);

                return new Date(year, month, day);
            }

            const dateTicks = dateValue.match(/\d+/);

            if (!dateTicks || !dateTicks.length) {
                throw new Error(getInvalidDateFormatErrorMessage(dateValue));
            }

            try {
                return new Date(parseInt(dateTicks[0], 10));
            } catch {
                throw new Error(getInvalidDateFormatErrorMessage(dateValue));
            }
        },

        /**
         * Возвращает дату в виде строки в формате dd.MM.yyyy
         * @param value дата которую нужно представить в виде строки формата: dd.MM.yyyy
         * @returns Дату в виде строки в формате dd.MM.yyyy
         */
        dateToDayMonthYear: (value: Date): string => {
            return `${ (`0${ value.getDate() }`).slice(-2) }.${ (`0${ value.getMonth() + 1 }`).slice(-2) }.${ value.getFullYear() }`;
        },

        /**
         * Возвращает дату в виде строки в формате yyyy-MM-dd
         * @param value дата которую нужно представить в виде строки формата: yyyy-MM-dd
         * @returns Дату в виде строки в формате yyyy-MM-dd
         */
        dateToYearMonthDay: (value: Date): string => {
            return `${ value.getFullYear() }-${ (`0${ value.getMonth() + 1 }`).slice(-2) }-${ (`0${ value.getDate() }`).slice(-2) }`;
        },

        /**
         * Возвращает дату в виде строки в формате dd.MM.yyyy HH:mm
         * @param value дата которую нужно представить в виде строки формата: dd.MM.yyyy HH:mm
         * @returns Дату в виде строки в формате dd.MM.yyyy HH:mm
         */
        dateToDayMonthYearHourMinute: (value: Date): string => {
            return `${ (`0${ value.getDate() }`).slice(-2) }.${ (`0${ value.getMonth() + 1 }`).slice(-2) }.${ value.getFullYear() } ${ (`0${ value.getHours() }`).slice(-2) }:${ (`0${ value.getMinutes() }`).slice(-2) }`;
        },

        /**
         * Возвращает дату в виде строки в формате dd.MM.yyyy HH:mm:ss
         * @param value дата которую нужно представить в виде строки формата: dd.MM.yyyy HH:mm:ss
         * @returns Дату в виде строки в формате dd.MM.yyyy HH:mm:ss
         */
        dateToDayMonthYearHourMinuteSeconds: (value: Date): string => {
            return `${ (`0${ value.getDate() }`).slice(-2) }.${ (`0${ value.getMonth() + 1 }`).slice(-2) }.${ value.getFullYear() } ${ (`0${ value.getHours() }`).slice(-2) }:${ (`0${ value.getMinutes() }`).slice(-2) }:${ (`0${ value.getSeconds() }`).slice(-2) }`;
        },

        /**
         * Возвращает дату в виде строки в формате yyyy-mm-dd HH:mm:ss
         * @param value дата которую нужно представить в виде строки формата: dd.MM.yyyy HH:mm:ss
         * @returns Дату в виде строки в формате dd-MM-yyyy HH:mm:ss
         */
        dateToYarMonthDayHourMinuteSecondsIso: (value: Date): string => {
            return `${ value.getFullYear() }-${ (`0${ value.getMonth() + 1 }`).slice(-2) }-${ (`0${ value.getDate() }`).slice(-2) } ${ (`0${ value.getHours() }`).slice(-2) }:${ (`0${ value.getMinutes() }`).slice(-2) }:${ (`0${ value.getSeconds() }`).slice(-2) }`;
        },

        /**
         * Возвращает дату в виде строки в формате yyyy-mm-ddTHH:mm:SS
         * @param value дата которую нужно представить в виде строки формата: dd.MM.yyyy HH:mm:ss
         * @returns Дату в виде строки в формате dd.MM.yyyy HH:mm:ss
         */
        dateToDayMonthYearHourMinuteISO: (value: Date): string => {
            return `${ value.getFullYear() }-${ (`0${ value.getMonth() + 1 }`).slice(-2) }-${ (`0${ value.getDate() }`).slice(-2) }T${ (`0${ value.getHours() }`).slice(-2) }:${ (`0${ value.getMinutes() }`).slice(-2) }:${ (`0${ value.getSeconds() }`).slice(-2) }`;
        },

        /**
         * Возвращает дату в виде строки в формате HH:mm
         * @param value дата которую нужно представить в виде строки формата: HH:mm
         * @returns Дату в виде строки в формате HH:mm
         */
        dateToHourMinute: (value: Date) => {
            return `${ (`0${ value.getHours() }`).slice(-2) }:${ (`0${ value.getMinutes() }`).slice(-2) }`;
        },

        /**
         * Получить дату в виде строки HH:mm:ss
         * @param value дата
         */
        dateToHourMinutesSeconds: (value: Date): string => {
            return `${ (`0${ value.getHours() }`).slice(-2) }:${ (`0${ value.getMinutes() }`).slice(-2) }:${ (`0${ value.getSeconds() }`).slice(-2) }`;
        },

        /**
         * Получить дату за несколько дней назад
         * @param daysAgoCount кол-во дней назад
         * @returns Дата за несколько дней назад
         */
        getSomeDaysAgoDate: (daysAgoCount: number): Date => {
            const date = new Date();
            date.setDate(date.getDate() - daysAgoCount);
            return date;
        },

        /**
         * Получить дату за несколько месяцев назад
         * @param monthAgoCount кол-во месяцев назад
         * @returns Дата за несколько месяцев назад
         */
        getSomeMonthAgoDate: (monthAgoCount: number): Date => {
            const date = new Date();
            date.setMonth(date.getMonth() - monthAgoCount);
            return date;
        },

        /**
         * Получить дату за несколько лет назад
         * @param yearAgoCount кол-во год назад
         * @returns Дата за несколько лет назад
         */
        getSomeYearAgoDate: (yearAgoCount: number): Date => {
            const date = new Date();
            date.setFullYear(date.getFullYear() - yearAgoCount);
            return date;
        },

        /**
         * Установить время начала дня (00:00:00)
         * @param date дата
         * @returns Дата со временем начала дня
         */
        setDayTimeToMidnight: (date: Date): Date => {
            date.setHours(0, 0, 0);
            return date;
        },

        /**
         * Установить время конца дня (23:59:59)
         * @param date дата
         * @returns Дата со временем конца дня
         */
        setDayEndTime: (date: Date): Date => {
            date.setHours(23, 59, 59);
            return date;
        },

        /**
         * Сконвертировать дату в строку в формате ISO
         * @param date дата
         * @param options параметры
         * @returns Строка, в формате ISO
         */
        dateToIsoDateString: (date: Date, options?: { includeTime?: boolean, includeMilliseconds?: boolean }): string => {
            const isoDateString = `${ (`${ date.getFullYear() }`) }-${ (`0${ date.getMonth() + 1 }`).slice(-2) }-${ (`0${ date.getDate() }`).slice(-2) }`;
            if (!options) return isoDateString;

            let isoDateTimeString = `${ isoDateString }T${ DateUtility.dateToHourMinutesSeconds(date) }`;
            if (options.includeMilliseconds) isoDateTimeString = `${ isoDateTimeString }.${ date.getMilliseconds() }`;
            if (options.includeTime) return isoDateTimeString;
            return isoDateString;
        },

        /**
         * конвертировать дату в строку в формате dd [название месяца] yyyy года
         * @param date дата
         * @returns Строка, dd [название месяца] yyyy года
         */
        getDateWithMonthName: (date: Date): string => {
            return date.toLocaleString('ru', { year: 'numeric', month: 'long', day: 'numeric', timeZone: 'UTC' });
        },

        /**
         * конвертировать дату в строку в формате dd [название месяца] yyyy года
         * @param date дата
         * @returns Строка, dd [название месяца] yyyy года
         */
        getDateWithDayMonthNameYear: (date: Date): string => {
            const months = [
                'января', 'февраля', 'марта', 'апреля', 'мая', 'июня',
                'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря',
            ];

            const formatData = dayjs(date);

            return `${ formatData.date() } ${ months[formatData.month()] } ${ formatData.year() } г.`;
        },

        getTomorrow: (date: Date = new Date()): Date => {
            return new Date(date.getTime() + (24 * 60 * 60 * 1000));
        },

        /**
         * Получить дату по формату
         * @param date Дата
         * @param format Тип для форматирования даты
         * @returns Отформатированную дату
         */
        getDateByFormat(date: Date | null, format: DateUtilityFormatType) {
            if (date) {
                switch (format) {
                    case 'dd.MM.yyyy':
                        return this.dateToDayMonthYear(date);
                    case 'dd.MM.yyyy HH:mm':
                        return this.dateToDayMonthYearHourMinute(date);
                    case 'dd.MM.yyyy HH:mm:ss':
                        return this.dateToDayMonthYearHourMinuteSeconds(date);
                    case 'HH:mm':
                        return this.dateToHourMinute(date);
                    case 'HH:mm:ss':
                        return this.dateToHourMinutesSeconds(date);
                    default:
                        return date;
                }
            }

            return '---';
        },
        getDayWord(count: number) {
            const lastDigit = count % 10;
            const lastTwoDigits = count % 100;

            if (lastTwoDigits > 10 && lastTwoDigits < 20) {
                return 'дней';
            }

            switch (lastDigit) {
                case 1:
                    return 'день';
                case 2:
                case 3:
                case 4:
                    return 'дня';
                default:
                    return 'дней';
            }
        }
    };
})();