import { createAppReduxStore } from 'app/redux/functions';
import { composeWithDevTools } from 'redux-devtools-extension';
import { applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

export const getNewStore = () => createAppReduxStore(composeWithDevTools(applyMiddleware(thunk)));