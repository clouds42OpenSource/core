export const ReplaceNullOrUndefinedUtility = (arr: Record<string, string>[]) => {
    arr.forEach((obj: { [x: string]: string; }) => {
        Object.keys(obj).forEach(key => {
            if (obj[key] === null || obj[key] === undefined) {
                obj[key] = ' ';
            }
        });
    });
    return arr;
};