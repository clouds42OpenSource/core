import { createTheme } from '@mui/material';

export const COLORS = {
    main: '#1ab394',
    mainDark: '#149a82',
    mainTranslucent: '#1ab3943d',
    font: '#676a6c',
    fontLight: '#c7c7c7',
    link: '#3379b7',
    linkDark: '#23527c',
    disabled: '#999999',
    error: '#ed5565',
    errorLight: '#e18891',
    errorDark: '#ec4757',
    alertError: '#fdeded',
    alertTextError: '#5f2120',
    alertBorderError: 'rgba(211, 47, 47, 0.9)',
    border: '#c9c9c9',
    borderLight: '#e5e6e7',
    warning: '#f8ac59',
    info: '#1c84c6',
    white: '#fff',
    backgroundElement: '#f8f8f8',
    background: '#f3f3f4',
    backgroundDialog: '#f8f8fa',
    linkHover: '#e7eaec',
    leftSideBar: '#3a4459',
    black: '#000000'
} as const;

export const THEME = createTheme({
    palette: {
        primary: {
            main: COLORS.main,
            contrastText: COLORS.white,
        },
        text: {
            primary: COLORS.font,
            disabled: COLORS.disabled,
        },
        error: {
            main: COLORS.error
        },
        warning: {
            main: COLORS.warning,
            contrastText: COLORS.white
        },
        info: {
            main: COLORS.info
        },
    },
    components: {
        MuiInputLabel: {
            styleOverrides: {
                root: {
                    transitionDuration: '0.2s',
                    '&.Mui-focused:not(.Mui-error)': {
                        color: COLORS.mainDark
                    },
                    '&.hover:not(.Mui-error)': {
                        color: COLORS.mainDark
                    },
                    '&.Mui-focused': {
                        color: COLORS.errorDark
                    }
                }
            }
        },
        MuiSwitch: {
            styleOverrides: {
                root: {
                    width: 50,
                    height: 22,
                    padding: 0,
                    display: 'flex',
                    '&.active': {
                        '& .MuiSwitch-thumb': {
                            width: 20,
                        },
                        '& .MuiSwitch-switchBase.Mui-checked': {
                        },
                    },
                    '& .MuiSwitch-switchBase': {
                        padding: 2,
                        '&.Mui-checked': {
                            transform: 'translateX(28px)',
                            color: '#fff !important',
                            '& + .MuiSwitch-track': {
                                opacity: 1,
                                backgroundColor: COLORS.main,
                            },
                        },
                    },
                    '& .MuiSwitch-thumb': {
                        boxShadow: '0 2px 4px 0 rgb(0 35 11 / 20%)',
                        width: 18,
                        height: 18,
                    },
                    '& .MuiSwitch-track': {
                        borderRadius: 22 / 2,
                        opacity: 1,
                        backgroundColor: 'rgba(0,0,0,.25)',
                        boxSizing: 'border-box',
                    },
                }
            },
        },
        MuiOutlinedInput: {
            styleOverrides: {
                notchedOutline: {
                    transitionDuration: '0.2s',
                },
                input: {
                    padding: '1px 8px'
                },
                root: {
                    '& legend': {
                        fontSize: '0.91em'
                    },
                    '&.Mui-focused': {
                        '& .MuiOutlinedInput-notchedOutline': {
                            borderWidth: '1px',
                            borderColor: COLORS.errorDark,
                        }
                    },
                    '&.Mui-focused:not(.Mui-error)': {
                        '& .MuiOutlinedInput-notchedOutline': {
                            borderWidth: '1px',
                            borderColor: COLORS.main
                        }
                    },
                    '&:not(.Mui-error):hover': {
                        '& .MuiOutlinedInput-notchedOutline': {
                            borderColor: COLORS.main,
                        }
                    },
                    '&:hover': {
                        '& .MuiOutlinedInput-notchedOutline': {
                            borderColor: COLORS.errorDark,
                        }
                    },
                    fontSize: '13px',
                    borderRadius: '3px',
                    minHeight: '35px',
                    border: 0
                }
            }
        },
        MuiLink: {
            styleOverrides: {
                root: {
                    color: COLORS.link,
                    fontSize: '13px',
                    textDecoration: 'none',
                    '&:hover': {
                        color: COLORS.linkDark,
                        cursor: 'pointer'
                    }
                }
            }
        },
        MuiChip: {
            styleOverrides: {
                root: {
                    height: '20px',
                    fontSize: '10px',
                    borderRadius: '3px',
                    paddingTop: '1px'
                }
            }
        },
        MuiStepLabel: {
            styleOverrides: {
                root: {
                    '& .Mui-active .MuiStepIcon-text': {
                        fill: COLORS.white,
                    },
                }
            }
        },
        MuiMenuItem: {
            styleOverrides: {
                root: {
                    fontSize: '13px'
                }
            }
        },
        MuiPagination: {
            defaultProps: {
                shape: 'rounded',
                variant: 'outlined',
                siblingCount: 1,
            },
        },
        MuiPaginationItem: {
            styleOverrides: {
                root: {
                    fontSize: '13px',
                    height: '25px',
                    transitionDuration: '0.2s',
                    background: COLORS.white,
                    '&:hover:not(.MuiPaginationItem-ellipsis)': {
                        border: `1px solid ${ COLORS.main }93`,
                        backgroundColor: `${ COLORS.mainTranslucent }`
                    },
                    '&.Mui-selected': {
                        backgroundColor: `${ COLORS.mainTranslucent }`,
                        border: `1px solid ${ COLORS.main }93`,
                    }
                }
            }
        },
        MuiAlert: {
            styleOverrides: {
                root: ({ ownerState }) => ({
                    ...(ownerState.severity === 'info' && {
                        color: COLORS.info
                    }),
                    ...(ownerState.severity === 'warning' && {
                        color: COLORS.warning
                    }),
                    ...(ownerState.severity === 'error' && {
                        color: COLORS.error
                    }),
                    ...(ownerState.severity === 'success' && {
                        color: COLORS.main
                    }),
                    fontSize: '13px',
                }),
            }
        },
        MuiAutocomplete: {
            styleOverrides: {
                root: {
                    '.MuiOutlinedInput-root': {
                        padding: '4px 4px 4px 8px'
                    }
                }
            }
        },
        MuiAccordion: {
            styleOverrides: {
                root: {
                    backgroundColor: COLORS.backgroundDialog,
                    boxShadow: '0 1px 0 -1px rgba(0,0,0,0.2), 0 1px 1px 0 rgba(0,0,0,0.14), 0 1px 3px 0 rgba(0,0,0,0.12)'
                }
            },
        }
    }
});