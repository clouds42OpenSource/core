export const getFileFromBase64 = (string64: string, fileName: string, type: string) => {
    const imageContent = atob(string64);
    const buffer = new ArrayBuffer(imageContent.length);
    const view = new Uint8Array(buffer);

    for (let n = 0; n < imageContent.length; n++) {
        view[n] = imageContent.charCodeAt(n);
    }

    const blob = new Blob([buffer], { type });

    return new File([blob], fileName, { lastModified: new Date().getTime(), type });
};

export const encodeToBase64 = (input: string) => {
    const utf8Encoder = new TextEncoder();
    const data = utf8Encoder.encode(input);
    return btoa(String.fromCharCode(...data));
};

export const fileToBase64 = (file: File) => new Promise<string | ArrayBuffer | null>((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = reject;
});