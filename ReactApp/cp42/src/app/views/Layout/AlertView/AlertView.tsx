import Stack from '@mui/material/Stack';
import { VERIFICATION_CODE } from 'app/api/endpoints';
import { AlertType, GetAlertMessagesData, GetCurrentSessionsSettingsResponse } from 'app/api/endpoints/global/response';
import { FETCH_API } from 'app/api/useFetchApi';
import { REDUX_API } from 'app/api/useReduxApi';
import { AppRoutes } from 'app/AppRoutes';
import { AccountUserGroup } from 'app/common/enums';
import { EMessageType, useAppSelector, useFloatMessages } from 'app/hooks';
import { useQuery } from 'app/hooks/useQuery';
import { AppReduxStoreState } from 'app/redux/types';
import { Dialog } from 'app/views/components/controls/Dialog';
import { TextOut } from 'app/views/components/TextOut';
import { AlertItemView } from 'app/views/Layout/AlertView/AlertItemView';
import { AlertViewCard } from 'app/views/Layout/AlertView/AlertViewCard';
import { VerificationForm } from 'app/views/modules/_common/components/VerificationForm';
import { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { useLocation } from 'react-router';
import style from './style.module.css';

type ConfirmationDialog = {
    isOpen: boolean;
    type: 'phone' | 'mail' | string;
    onClick: () => void;
}

type DispatchProps = {
    getAlerts: () => void;
};

type StateProps = {
    alerts: GetAlertMessagesData[];
    currentSettings: GetCurrentSessionsSettingsResponse;
    hasSettingsReceived: boolean;
};

type AllProps = StateProps & DispatchProps;

const { getVerifyEmail, getVerifyPhone } = VERIFICATION_CODE;
const { getAlertMessages } = REDUX_API.GLOBAL_REDUX;
const { postNotificationState } = FETCH_API.ACCOUNT_DATABASES;

const isRussianPhoneNumber = (phone: string) => /^(\+7|7|8)([234589])\d{9}$/.test(phone);

const AlertViewComponent = ({ alerts, hasSettingsReceived, currentSettings, getAlerts }: AllProps) => {
    const params = useQuery();
    const { pathname } = useLocation();
    const { show } = useFloatMessages();

    const message = params.get('message');
    const url = window.location.origin + window.location.pathname;

    const { locale } = useAppSelector(state => state.Global.getCurrentSessionSettingsReducer.settings.currentContextInfo);
    const { data: profileData } = FETCH_API.PROFILE.useGetProfile();

    const [confirmationDialog, setConfirmationDialog] = useState<ConfirmationDialog>({
        type: '',
        isOpen: false,
        onClick: () => { /* empty */ }
    });
    const [disableResendNewCode, setDisableResendNewCode] = useState(false);
    const [countSecond, setCountSecond] = useState(0);

    useEffect(() => {
        if (!hasSettingsReceived) {
            getAlerts();
        }
    }, [getAlerts, hasSettingsReceived]);

    useEffect(() => {
        const timer = setTimeout(() => {
            if (countSecond !== 0) {
                setCountSecond(prevState => prevState - 1);
            }
        }, 1000);

        return () => clearInterval(timer);
    }, [countSecond]);

    const verifyPhone = () => {
        getVerifyPhone().then(response => {
            if (!response.success) {
                const countSecondRes = parseInt(response.message ?? '', 10);

                if (countSecondRes) {
                    setCountSecond(countSecondRes);
                    setDisableResendNewCode(true);
                    setConfirmationDialog(prev => ({
                        ...prev,
                        isOpen: true
                    }));
                } else {
                    setDisableResendNewCode(true);
                    show(EMessageType.error, response.message);
                }
            } else {
                setConfirmationDialog(prev => ({
                    ...prev,
                    isOpen: true
                }));
            }
        });
    };

    const verifyEmail = () => {
        getVerifyEmail().then(response => {
            if (!response.success) {
                const countSecondRes = parseInt(response.message ?? '', 10);

                if (countSecondRes) {
                    setCountSecond(countSecondRes);
                    setDisableResendNewCode(true);
                    setConfirmationDialog(prev => ({
                        ...prev,
                        isOpen: true
                    }));
                } else {
                    setDisableResendNewCode(true);
                    show(EMessageType.error, response.message);
                }
            } else {
                setConfirmationDialog(prev => ({
                    ...prev,
                    isOpen: true
                }));
            }
        });
    };

    const openConfirmationDialog = (type: 'phone' | 'mail', onClick: () => void) => {
        setConfirmationDialog(prev => ({
            ...prev,
            type,
            onClick
        }));

        if (type === 'phone') {
            verifyPhone();
        } else {
            verifyEmail();
        }
    };

    const closeConfirmationDialog = () => {
        setConfirmationDialog({
            type: '',
            isOpen: false,
            onClick: () => { /* empty */ }
        });
        setCountSecond(0);
    };

    const addUserNotifiedState = async () => {
        const { success, message: responseMessage } = await postNotificationState();

        if (!success) {
            show(EMessageType.error, responseMessage);
        }
    };

    return (
        <>
            <Stack
                className={ style.stack }
                spacing={ 1 }
            >
                {
                    Array.isArray(alerts) && alerts.map(item => {
                        return item.type === 'Success'
                            ? <AlertItemView key={ item.text } message={ item.text } type={ AlertType.success } />
                            : <AlertItemView key={ item.text } message={ item.text } type={ AlertType.warning } />;
                    })
                }
                {
                    pathname === AppRoutes.accountManagement.informationBases
                        ? (
                            <AlertViewCard
                                description="Это новая страница работы с информационными базами. Работает в тестовом режиме. Если у вас возникли проблемы с просмотром, изменением или удалением баз, перейдите на старую страницy по ссылке&nbsp;"
                                legacyUrl={ AppRoutes.accountManagement.legacyInformationBases }
                            />
                        )
                        : null
                }
                {
                    !currentSettings.currentContextInfo.isEmailVerified &&
                    currentSettings.currentContextInfo.isEmailExists && (
                        <AlertItemView
                            title="Внимание! Ваша электронная почта еще не подтверждена!"
                            message={
                                <TextOut>
                                    Пожалуйста, подтвердите свой адрес электронной почты сейчас, чтобы улучшить безопасность вашей учетной записи. Это не только поможет вам <strong>восстановить доступ в случае потери пароля</strong>, но и обеспечит доступ <strong>к важным уведомлениям и полезным функциям</strong>. Нажмите <strong style={ { cursor: 'pointer' } } onClick={ () => openConfirmationDialog('mail', () => { /* empty */ }) }>здесь</strong>, чтобы подтвердить вашу почту и легко восстановить доступ, если вы забудете пароль.
                                </TextOut>
                            }
                            type={ AlertType.info }
                            disableCloseButton={ true }
                        />
                    )
                }
                {
                    !currentSettings.currentContextInfo.isPhoneVerified &&
                    currentSettings.currentContextInfo.isPhoneExists &&
                    profileData?.rawData.phoneNumber &&
                    isRussianPhoneNumber(profileData?.rawData.phoneNumber) &&
                    locale === 'ru-ru' && (
                        <AlertItemView
                            title="Внимание! Ваш номер телефона еще не подтвержден!"
                            message={
                                <TextOut>
                                    Подтвердите его прямо сейчас, чтобы обеспечить максимальную безопасность вашей учетной записи и получить доступ к неотложным уведомлениям и полезным функциям.&nbsp;
                                    Для подтверждения телефона нажмите <strong style={ { cursor: 'pointer' } } onClick={ () => openConfirmationDialog('phone', () => { /* empty */ }) }>сюда</strong>.
                                </TextOut>
                            }
                            type={ AlertType.info }
                            disableCloseButton={ true }
                        />
                    )
                }
                {
                    currentSettings.currentContextInfo.showSupportMessage && (
                        <AlertItemView
                            title="Вы можете поставить базу на АО и ТиИ"
                            message={
                                <TextOut>
                                    В вашем аккаунте есть базы, для которых можно подключить функцию &quot;Автоматического обновления (АО)&quot; и &quot;Тестирования и исправления ошибок (ТиИ)&quot;.&nbsp;
                                    Рекомендуем подключить данный функционал в ближайшее время.
                                </TextOut>
                            }
                            type={ AlertType.info }
                            closeButtonHandler={ addUserNotifiedState }
                            disableCloseButton={ !currentSettings.currentContextInfo.currentUserGroups.includes(AccountUserGroup.AccountAdmin) }
                        />
                    )
                }
                {
                    message && <AlertItemView message={ message } type={ AlertType.info } url={ url } />
                }
            </Stack>
            <Dialog
                isOpen={ confirmationDialog.isOpen }
                isTitleSmall={ true }
                title={ `Подтверждение ${ confirmationDialog.type === 'phone' ? 'телефона' : 'почты' }` }
                dialogWidth="xs"
                dialogVerticalAlign="center"
                onCancelClick={ closeConfirmationDialog }
            >
                <VerificationForm
                    type={ confirmationDialog.type }
                    description={
                        <TextOut>Код активации отправлен Вам на { confirmationDialog.type === 'phone' ? 'телефон' : 'почту' }</TextOut>
                    }
                    getNewCode={ confirmationDialog.type === 'phone' ? verifyPhone : verifyEmail }
                    disableResendCode={ disableResendNewCode }
                    countSecond={ countSecond }
                    closeDialog={ closeConfirmationDialog }
                />
            </Dialog>
        </>
    );
};

export const AlertView = connect<StateProps, DispatchProps, NonNullable<unknown>, AppReduxStoreState>(
    state => {
        return {
            currentSettings: state.Global.getCurrentSessionSettingsReducer.settings,
            alerts: state.Global.getAlertMessagesReducer.alert,
            hasSettingsReceived: state.Global.getCurrentSessionSettingsReducer.hasSuccessFor.hasSettingsReceived
        };
    },
    dispatch => ({
        getAlerts: () => getAlertMessages(dispatch)
    })
)(AlertViewComponent);