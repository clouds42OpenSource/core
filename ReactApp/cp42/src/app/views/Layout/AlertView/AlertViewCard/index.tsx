import { Paper } from '@mui/material';
import React from 'react';
import { Link } from 'react-router-dom';

type AlertViewCardProps = {
    description: string;
    legacyUrl: string;
};

export const AlertViewCard = ({ legacyUrl, description }: AlertViewCardProps) => {
    return (
        <Paper style={ {
            padding: '8px',
            backgroundColor: '#f5edd5',
            fontSize: '14px',
            lineHeight: '1.2'
        } }
        >
            { description }
            <Link
                to={ legacyUrl }
                style={ { textDecoration: 'underline', color: '-webkit-link' } }
            >
                Перейти на старую страницу
            </Link>
        </Paper>
    );
};