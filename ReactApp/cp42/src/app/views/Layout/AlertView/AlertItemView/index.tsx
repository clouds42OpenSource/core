import { AlertTitle } from '@mui/material';
import Alert from '@mui/material/Alert';
import Button from '@mui/material/Button';
import CloseOutlined from '@mui/icons-material/CloseOutlined';
import Collapse from '@mui/material/Collapse';
import { AlertType } from 'app/api/endpoints/global/response';
import React from 'react';

type OwnProps = {
    message: string | React.ReactNode;
    type: AlertType;
    url?: string;
    disableCloseButton?: boolean;
    closeButtonHandler?: () => void;
    title?: string;
};

type OwnState = {
    isVisible: boolean
};

export class AlertItemView extends React.Component<OwnProps, OwnState> {
    constructor(props: OwnProps) {
        super(props);

        this.onClose = this.onClose.bind(this);

        this.state = {
            isVisible: true
        };
    }

    public componentDidMount() {
        if (this.props.url) {
            window.history.pushState(null, '', this.props.url);
        }
    }

    private onClose() {
        if (this.props.closeButtonHandler) {
            this.props.closeButtonHandler();
        }

        this.setState({ isVisible: false });
    }

    public render() {
        return (
            <Collapse in={ this.state.isVisible }>
                <Alert
                    severity={ this.props.type }
                    action={
                        !this.props.disableCloseButton && (
                            <Button onClick={ this.onClose } style={ { outline: 'none' } }>
                                <CloseOutlined fontSize="inherit" color="action" />
                            </Button>
                        )
                    }
                    sx={ { border: 1 } }
                >
                    {
                        this.props.title && (
                            <AlertTitle>{ this.props.title }</AlertTitle>
                        )
                    }
                    { this.props.message }
                </Alert>
            </Collapse>
        );
    }
}