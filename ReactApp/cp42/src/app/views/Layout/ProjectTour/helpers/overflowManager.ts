export const overflowEnable = () => {
    document.body.style.overflow = 'visible';
};

export const overflowDisable = () => {
    document.body.style.overflow = 'hidden';
};

export const getCurrentOverflowY = () => document.body.style.overflow;