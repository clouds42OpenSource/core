import { EPartnersTourId, ERefreshId, ETourId } from '../enums';

export const getSelector = (tourId?: ETourId | EPartnersTourId) => `[data-tourid="${ tourId ?? 'undefined' }"]`;

export const getMutation = (refreshId: ERefreshId) => `[data-refreshid="${ refreshId }"]`;