import { EPartnersSteps, ESteps, EUsersSteps } from 'app/views/Layout/ProjectTour/enums';

export const actionDisabler = (step: number) => !(
    step === ESteps.createDatabasePage ||
    step === ESteps.createDatabasePageDemoCheckbox ||
    step === ESteps.createDatabasePageButton ||
    step === ESteps.rentPageTariff
);

export const partnersActionDisabled = (step: number) => !(
    step === EPartnersSteps.withdrawalTab ||
    step === EPartnersSteps.servicesTab ||
    step === EPartnersSteps.servicesTabCreate ||
    step === EPartnersSteps.createServiceCheckbox
);

export const usersActionDisabled = (step: number) => !(
    step === EUsersSteps.usersPageTable
);