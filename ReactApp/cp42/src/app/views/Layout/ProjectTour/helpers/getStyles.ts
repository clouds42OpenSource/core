import { StylesObj } from '@reactour/tour';
import { COLORS } from 'app/utils';

export const getStyles = (): StylesObj => ({
    badge: base => ({ ...base, background: COLORS.main }),
    close: base => ({ ...base, top: '10px', right: '10px' }),
    controls: base => ({ ...base, marginTop: '10px' }),
});