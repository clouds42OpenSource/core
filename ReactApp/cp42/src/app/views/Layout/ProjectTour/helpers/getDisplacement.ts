import { EConnectionSteps, EPartnersSteps, ESteps, ETourStages, EUsersSteps } from '../enums';

export const getDisplacement = (stage: ETourStages | string): number => {
    switch (stage) {
        case ETourStages.databaseCard:
            return 0;
        case ETourStages.informationBases:
            return ESteps.mainPageMyDatabases;
        case ETourStages.connectionSettings:
            return EConnectionSteps.mainPageConnectionSettings;
        case ETourStages.partners:
            return EPartnersSteps.referral;
        case ETourStages.balance:
            return ESteps.mainPageBalance;
        case ETourStages.rent:
            return ESteps.mainPageRent;
        case ETourStages.users:
            return EUsersSteps.mainPageUsers;
        default:
            return ESteps.mainPage;
    }
};