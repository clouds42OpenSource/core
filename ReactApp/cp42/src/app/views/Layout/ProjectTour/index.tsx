import { TourProvider } from '@reactour/tour';

import { AppRoutes } from 'app/AppRoutes';
import { ContainedButton, SuccessButton } from 'app/views/components/controls/Button';
import cn from 'classnames';
import React, { Dispatch, memo, useMemo, useState } from 'react';
import { useHistory } from 'react-router';

import { CONNECTION_STEPS, DATABASE_CARD_STEPS, PARTNERS_STEPS, STEPS, USER_STEPS } from './constants';
import { EConnectionSteps, EPartnersSteps, ESteps, ETourStages, EUsersSteps } from './enums';
import { actionDisabler, getDisplacement, getStyles, overflowDisable, overflowEnable, partnersActionDisabled, usersActionDisabled } from './helpers';

import styles from './style.module.css';

type TProjectTour = {
    children: React.ReactNode;
};

type TClickClose = {
    setIsOpen: Dispatch<React.SetStateAction<boolean>>;
    setCurrentStep: Dispatch<React.SetStateAction<number>>;
    currentStep: number;
};

type TSteps = EPartnersSteps | ESteps | EUsersSteps | EConnectionSteps;

export const ProjectTour = memo(({ children }: TProjectTour) => {
    const history = useHistory();

    const [currentStep, setCurrentStep] = useState<TSteps>(ESteps.mainPage);
    const [currentTour, setCurrentTour] = useState<string | ETourStages>(ETourStages.full);

    const displacement = useMemo(() => {
        const currentDisplacement = getDisplacement(currentTour);

        return currentTour === ETourStages.full ? currentDisplacement : currentDisplacement - 1;
    }, [currentTour]);

    const getSteps = () => {
        switch (currentTour) {
            case ETourStages.databaseCard:
                return DATABASE_CARD_STEPS;
            case ETourStages.partners:
                return PARTNERS_STEPS;
            case ETourStages.users:
                return USER_STEPS;
            case ETourStages.connectionSettings:
                return CONNECTION_STEPS;
            default:
                return STEPS;
        }
    };

    const nextButtonEnabler = () => {
        switch (currentTour) {
            case ETourStages.partners:
                return partnersActionDisabled(currentStep);
            case ETourStages.users:
                return usersActionDisabled(currentStep + displacement);
            case ETourStages.databaseCard:
            case ETourStages.connectionSettings:
                return true;
            default:
                return actionDisabler(currentStep + displacement);
        }
    };

    const nextStepHandler = () => {
        switch (currentTour) {
            case ETourStages.databaseCard:
            case ETourStages.partners:
                setCurrentStep(prev => prev + 1);
                break;
            case ETourStages.users:
                setCurrentStep(prev => {
                    switch (prev) {
                        case (EUsersSteps.mainPageUsers - displacement):
                            history.push(AppRoutes.accountManagement.userManagement);
                            break;
                        default:
                    }

                    return prev + 1;
                });
                break;
            case ETourStages.connectionSettings:
                setCurrentStep(prev => {
                    switch (prev) {
                        case EConnectionSteps.mainPageConnectionSettings:
                            history.push(AppRoutes.settings.connectionSettings);
                            break;
                        default:
                    }

                    return prev + 1;
                });
                break;
            default:
                setCurrentStep(prev => {
                    switch (prev) {
                        case (ESteps.mainPageMyDatabases - displacement):
                            history.push(AppRoutes.accountManagement.informationBases);
                            break;
                        case (ESteps.myDatabasesPageCreateButton - displacement):
                            history.push(AppRoutes.accountManagement.createAccountDatabase.createDb);
                            break;
                        case (ESteps.mainPageBalance - displacement):
                            history.push(AppRoutes.accountManagement.balanceManagement);
                            break;
                        case (ESteps.billingPageReplenishBalanceButton - displacement):
                            history.push(AppRoutes.accountManagement.replenishBalance);
                            break;
                        case (ESteps.replenishBalancePageGenerateInvoice - displacement):
                            history.push(AppRoutes.accountManagement.balanceManagement);
                            break;
                        case (ESteps.mainPageRent - displacement):
                            history.push(AppRoutes.services.rentReference);
                            break;
                        default:
                    }

                    return prev + 1;
                });
        }
    };

    const onClickMaskHandler = () => {
        return void 0;
    };

    const onCloseHandler = ({ setIsOpen }: TClickClose) => {
        setIsOpen(false);
    };

    const getNextButton = (stepsLength: number, closeProps: TClickClose) => {
        const isLastStep = stepsLength - 1 === currentStep;

        return (
            <SuccessButton isEnabled={ nextButtonEnabler() } onClick={ isLastStep ? () => onCloseHandler(closeProps) : nextStepHandler }>
                { isLastStep ? 'Завершить' : 'Далее' }
            </SuccessButton>
        );
    };

    const getPrevButton = (closeProps: TClickClose) => {
        if (currentTour === ETourStages.full && currentStep === ESteps.mainPage) {
            return (
                <ContainedButton onClick={ () => onCloseHandler(closeProps) }>Пропустить</ContainedButton>
            );
        }

        return (
            <ContainedButton onClick={ () => onCloseHandler(closeProps) }>Закрыть</ContainedButton>
        );
    };

    return (
        <TourProvider
            onClickClose={ onCloseHandler }
            padding={ { mask: 0 } }
            disableKeyboardNavigation={ ['left', 'right', 'esc'] }
            showCloseButton={ false }
            meta={ currentTour }
            setMeta={ setCurrentTour }
            scrollSmooth={ true }
            styles={ getStyles() }
            onClickMask={ onClickMaskHandler }
            currentStep={ currentStep }
            setCurrentStep={ setCurrentStep }
            showPrevNextButtons={ true }
            beforeClose={ currentTour === ETourStages.databaseCard || currentTour === ETourStages.users ? undefined : overflowEnable }
            afterOpen={ currentTour === ETourStages.databaseCard || currentTour === ETourStages.users ? undefined : overflowDisable }
            nextButton={ ({ stepsLength, ...other }) => getNextButton(stepsLength, { ...other }) }
            prevButton={ getPrevButton }
            showDots={ false }
            disableInteraction={ true }
            className={ cn({
                [styles.modal]: true,
                [styles.bigModal]: currentTour === ETourStages.databaseCard
            }) }
            steps={ getSteps() }
            showNavigation={ true }
        >
            <>
                <div data-tourid="undefined" className={ styles.center } />
                { children }
            </>
        </TourProvider>
    );
});