import { COLORS } from 'app/utils';
import { TextOut } from 'app/views/components/TextOut';
import { EUsersSteps } from '../enums';

type TUserSteps = {
    step: EUsersSteps;
};

export const UsersSteps = ({ step }: TUserSteps) => {
    switch (step) {
        case EUsersSteps.mainPageUsers:
            return (
                <>
                    <TextOut>
                        Если у вас есть еще сотрудники, которым Вы хотите предоставить доступ к вашим базам, то Вы всегда можете их добавить в меню
                        <TextOut fontWeight={ 700 }> &quot;Пользователи&quot;</TextOut>.
                    </TextOut>
                    <TextOut style={ { color: COLORS.warning } } fontWeight={ 700 }>Переход на страницу &quot;Пользователи&quot;.</TextOut>
                </>
            );
        case EUsersSteps.usersPageAddUser:
            return (
                <TextOut>Чтобы добавить пользователя достаточно нажать кнопку <TextOut fontWeight={ 700 }>&quot;Добавить пользователя&quot;</TextOut>.</TextOut>
            );
        case EUsersSteps.usersPageTable:
            return (
                <>
                    <TextOut>Для редактирования своих данных или других пользователей необходимо открыть карточку.</TextOut>
                    <TextOut style={ { color: COLORS.info } } fontWeight={ 700 }>Нажмите на логин пользователя.</TextOut>
                </>
            );
        case EUsersSteps.usersPageDisableButton:
            return (
                <TextOut>Вы можете удалить или отключить пользователя. Оплата и доступ в базы будут для него недоступны.</TextOut>
            );
        case EUsersSteps.usersPageAccessTab:
            return (
                <TextOut>Выдать доступ в базы можно на вкладке <TextOut fontWeight={ 700 }>настройка доступов</TextOut>.</TextOut>
            );
        default:
            return (
                <TextOut textAlign="center">Добро пожаловать в инструкцию использования Личного Кабинета!</TextOut>
            );
    }
};