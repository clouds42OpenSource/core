import { MenuItem, Select, SelectChangeEvent } from '@mui/material';
import { useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import { StepType, useTour } from '@reactour/tour';

import { AppRoutes } from 'app/AppRoutes';
import { Dialog } from 'app/views/components/controls/Dialog';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { TextOut } from 'app/views/components/TextOut';

import { Steps } from '../components';
import { CONNECTION_STEPS, PARTNERS_STEPS, STEPS, USER_STEPS } from '../constants';
import { ESteps, ETourStages } from '../enums';
import { getCurrentOverflowY, getSelector, overflowDisable } from '../helpers';

import styles from './style.module.css';

type TStartTourDialog = {
    isOpen: boolean;
    onCancelClickHandler: () => void;
};

export const StartTourDialog = ({ onCancelClickHandler, isOpen }: TStartTourDialog) => {
    const { isOpen: isOpenTour, setCurrentStep, setIsOpen, setMeta, setSteps, meta } = useTour();
    const history = useHistory();

    const [tourStage, setTourStage] = useState<ETourStages>(ETourStages.full);

    const overflow = getCurrentOverflowY();

    const tourStageHandler = ({ target: { value } }: SelectChangeEvent<ETourStages>) => {
        setTourStage(value as ETourStages);
    };

    useEffect(() => {
        if (isOpenTour && overflow !== 'hidden' && meta !== ETourStages.users) {
            overflowDisable();
        }
    }, [isOpenTour, overflow]);

    const startTourHandler = () => {
        if (setMeta && setSteps) {
            const startStep: StepType = { selector: getSelector(), content: <Steps />, position: 'center' };

            switch (tourStage) {
                case ETourStages.informationBases:
                    setSteps([startStep, ...STEPS.slice(ESteps.mainPageMyDatabases, ESteps.myDatabasesPageTable + 1)]);
                    break;
                case ETourStages.rent:
                    setSteps([startStep, ...STEPS.slice(ESteps.mainPageRent, ESteps.rentPageAcceptButton + 1)]);
                    break;
                case ETourStages.balance:
                    setSteps([startStep, ...STEPS.slice(ESteps.mainPageBalance, ESteps.billingPagePromisePaymentTab + 1)]);
                    break;
                case ETourStages.users:
                    setSteps([startStep, ...USER_STEPS]);
                    break;
                case ETourStages.partners:
                    setSteps(PARTNERS_STEPS);
                    history.push(AppRoutes.partners.toPartners);
                    break;
                case ETourStages.connectionSettings:
                    setSteps([startStep, ...CONNECTION_STEPS]);
                    break;
                default:
                    setSteps(STEPS);
                    break;
            }

            onCancelClickHandler();
            setMeta(tourStage);
            setCurrentStep(0);
            setIsOpen(true);
        }
    };

    return (
        <Dialog
            maxHeight="330px"
            isOpen={ isOpen }
            title="Инструкция по использованию ЛК"
            dialogWidth="xs"
            isTitleSmall={ true }
            onCancelClick={ onCancelClickHandler }
            contentClassName={ styles.dialogContent }
            buttons={ [
                {
                    content: 'Пропустить',
                    kind: 'default',
                    onClick: onCancelClickHandler
                },
                {
                    content: 'Начнем',
                    kind: 'success',
                    onClick: () => startTourHandler()
                }
            ] }
        >
            <FormAndLabel label="Выберите необходимую инструкцию">
                <Select fullWidth={ true } onChange={ tourStageHandler } value={ tourStage }>
                    <MenuItem value={ ETourStages.full }>Полная инструкция</MenuItem>
                    <MenuItem value={ ETourStages.informationBases }>Информационные базы</MenuItem>
                    <MenuItem value={ ETourStages.connectionSettings }>Настройки подключения</MenuItem>
                    <MenuItem value={ ETourStages.rent }>Аренда</MenuItem>
                    <MenuItem value={ ETourStages.balance }>Баланс</MenuItem>
                    <MenuItem value={ ETourStages.users }>Пользователи</MenuItem>
                    <MenuItem value={ ETourStages.partners }>Партнерам</MenuItem>
                </Select>
            </FormAndLabel>
            <TextOut fontSize={ 11 }>
                После нажатия кнопки &quot;Начнем&quot; будет запущена инструкция по использованию выбранного функционала. Пропустить данное действие нельзя!
            </TextOut>
        </Dialog>
    );
};