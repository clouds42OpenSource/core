import { COLORS } from 'app/utils';
import { TextOut } from 'app/views/components/TextOut';

import { EPartnersSteps } from '../enums';

type TPartnersSteps = {
    step: EPartnersSteps;
};

export const PartnersSteps = ({ step }: TPartnersSteps) => {
    switch (step) {
        case EPartnersSteps.referral:
            return (
                <TextOut>Ссылка на подключение партнеров или клиентов, которые хотят работать с Вами.</TextOut>
            );
        case EPartnersSteps.currentSum:
            return (
                <TextOut>Сумма Вашего заработка.</TextOut>
            );
        case EPartnersSteps.cashOut:
            return (
                <TextOut>Сумма, которую Вы можете вывести на свой счет или использовать для оплаты услуг в Личном Кабинете.</TextOut>
            );
        case EPartnersSteps.clientsTabTable:
            return (
                <TextOut>Всех клиентов, которые подключились по Вашей ссылке, Вы можете увидеть здесь.</TextOut>
            );
        case EPartnersSteps.clientsTabRegisterClient:
            return (
                <TextOut>Или Вы можете зарегистрировать клиента лично.</TextOut>
            );
        case EPartnersSteps.transactionsTab:
            return (
                <TextOut>Здесь Вы найдете все начисления от Ваших клиентов и Ваши вознаграждения.</TextOut>
            );
        case EPartnersSteps.requisitesTab:
            return (
                <TextOut>Для вывода средств на счет, достаточно заполнить свои реквизиты. После проверки Вы сможете смело их выводить.</TextOut>
            );
        case EPartnersSteps.withdrawalTab:
            return (
                <>
                    <TextOut>Здесь происходит управление Вашими полученными денежными средствами.</TextOut>
                    <TextOut style={ { color: COLORS.info } } fontWeight={ 700 }>Нажмите на вкладку &quot;Вывод&quot;.</TextOut>
                </>
            );
        case EPartnersSteps.withdrawalTabCreate:
            return (
                <TextOut>Создайте заявку на вывод средств, подпишите ее и в течение 5 дней средства будут на Вашем счете.</TextOut>
            );
        case EPartnersSteps.withdrawalTabCashOut:
            return (
                <TextOut>Вы также можете перевести средства сразу в Личный Кабинет для оплаты своих услуг.</TextOut>
            );
        case EPartnersSteps.servicesTab:
            return (
                <>
                    <TextOut>Здесь отображаются созданные Вами сервисы.</TextOut>
                    <TextOut style={ { color: COLORS.info } } fontWeight={ 700 }>Нажмите на вкладку &quot;Сервисы&quot;.</TextOut>
                </>

            );
        case EPartnersSteps.servicesTabCreate:
            return (
                <>
                    <TextOut>Давайте посмотрим, как создается сервис.</TextOut>
                    <TextOut style={ { color: COLORS.info } } fontWeight={ 700 }>Нажмите на кнопку &quot;Создать сервис&quot;.</TextOut>
                </>
            );
        case EPartnersSteps.createServiceCheckbox:
            return (
                <>
                    <TextOut>Чтобы Вашим сервисом могли пользоваться другие клиенты, разместите его в нашем Маркете.</TextOut>
                    <TextOut style={ { color: COLORS.info } } fontWeight={ 700 }>Нажмите на чекбокс &quot;Сервис доступен в Маркет42&quot;.</TextOut>
                </>
            );
        case EPartnersSteps.createServiceTitle:
            return (
                <TextOut>Придумайте уникальное название и понятное описание.</TextOut>
            );
        case EPartnersSteps.createServiceLogo:
            return (
                <TextOut>Загрузите логотип сервиса.</TextOut>
            );
        case EPartnersSteps.createServiceFile:
            return (
                <TextOut>Загрузите свое уникальное расширение.</TextOut>
            );
        case EPartnersSteps.createServiceCost:
            return (
                <TextOut>Назначьте стоимость Вашего сервиса.</TextOut>
            );
        case EPartnersSteps.createServiceDraft:
            return (
                <TextOut>В любой момент Вы можете сохранить сервис и продолжить редактировать его, когда будут готовы недостающие файлы.</TextOut>
            );
        case EPartnersSteps.createServiceConclusion:
            return (
                <TextOut>После создания сервиса в течение 3-х дней будет проведена проверка, после чего другие клиенты смогут покупать Ваш сервис и Вы получите свое вознаграждение. Удачи вам!</TextOut>
            );
        default:
            return null;
    }
};