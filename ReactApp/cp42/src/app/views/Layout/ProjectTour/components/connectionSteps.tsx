import { COLORS } from 'app/utils';
import { TextOut } from 'app/views/components/TextOut';

import { EConnectionSteps } from '../enums';

type TConnectionSteps = {
    step: EConnectionSteps;
};

export const ConnectionSteps = ({ step }: TConnectionSteps) => {
    switch (step) {
        case EConnectionSteps.mainPageConnectionSettings:
            return (
                <>
                    <TextOut>Для работы в тонком клиенте Вы можете скачать последние актуальные версии у нас.</TextOut>
                    <TextOut style={ { color: COLORS.warning } } fontWeight={ 700 }>Переход на страницу &quot;Настройки подключения&quot;.</TextOut>
                </>
            );
        case EConnectionSteps.connectionSettingsPageInstructions:
            return (
                <TextOut>Здесь расположены основные инструкции подключения для различных систем.</TextOut>
            );
        case EConnectionSteps.connectionSettingsPageVersions:
            return (
                <TextOut>Версии тонкого клиента можно скачивать здесь.</TextOut>
            );
        case EConnectionSteps.connectionSettingsPageRdp:
            return (
                <TextOut>А здесь находятся данные для настройки РДП подключения.</TextOut>
            );
        default:
            return (
                <TextOut textAlign="center">Добро пожаловать в инструкцию использования Личного Кабинета!</TextOut>
            );
    }
};