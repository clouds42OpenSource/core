import { COLORS } from 'app/utils';
import { TextOut } from 'app/views/components/TextOut';

import { ESteps } from '../enums';

import styles from './style.module.css';

type TSteps = {
    step?: ESteps;
};

export const Steps = ({ step }: TSteps) => {
    switch (step) {
        case ESteps.mainPage:
            return (
                <>
                    <TextOut fontSize={ 16 } fontWeight={ 700 }>Спасибо за регистрацию!</TextOut>
                    <TextOut>Мы дарим Вам <TextOut fontWeight={ 700 }>7 дней</TextOut> бесплатного тестирования нашего сервиса без ограничения по функционалу.</TextOut>
                    <TextOut>А сейчас за <TextOut fontWeight={ 700 }>1 минуту</TextOut> мы расскажем, как получить максимальную отдачу от нашего продукта.</TextOut>
                </>
            );
        case ESteps.mainPageServices:
            return (
                <>
                    <TextOut>Рекомендуем ознакомиться с нашей подборкой сервисов и активировать необходимые.</TextOut>
                    <TextOut>Они помогут Вам <TextOut fontWeight={ 700 }>сэкономить</TextOut> время при работе с документами.</TextOut>
                </>
            );
        case ESteps.mainPageProfile:
            return (
                <>
                    <TextOut fontSize={ 14 } fontWeight={ 700 }>Настройка профиля.</TextOut>
                    <TextOut>Рекомендуем заполнить его максимально подробно, чтобы нужная информация подтягивалась во все документы <TextOut fontWeight={ 700 }>автоматически</TextOut>.</TextOut>
                </>
            );
        case ESteps.mainPageMyDatabases:
            return (
                <>
                    <TextOut>Ускорьте работу с базой, переведя ее в клиент-серверный режим. Для этого необходимо:</TextOut>
                    <ul className={ styles.list }>
                        <li>загрузить базу;</li>
                        <li>обновить ее до последнего релиза.</li>
                    </ul>
                    <TextOut>Вы можете сделать это вместе с подсказками или заказать услугу у нас (она будет бесплатной, только при условии покупки тарифа).</TextOut>
                    <TextOut style={ { color: COLORS.warning } } fontWeight={ 700 }>Переход на страницу &quot;Информационные базы&quot;.</TextOut>
                </>
            );
        case ESteps.myDatabasesPageCreateButton:
            return (
                <>
                    <TextOut>Только после добавления первой базы активируется тестовый тариф.</TextOut>
                    <TextOut>
                        Если Вы ведете учет в 1С и хотите тестировать сервис на примере своей базы, то можете нажать
                        <TextOut style={ { color: COLORS.info } } fontWeight={ 700 }> &quot;Загрузить свою базу 1С&quot; </TextOut>
                        и следовать дальнейшей инструкции.
                    </TextOut>
                    <TextOut>Если Вы ранее не вели учет в 1С и подбираете конфигурацию для своих целей, то предлагаем посмотреть демонстрационные данные.</TextOut>
                    <TextOut style={ { color: COLORS.warning } } fontWeight={ 700 }>Переход на страницу &quot;Создание информационной базы&quot;.</TextOut>
                </>
            );
        case ESteps.createDatabasePage:
            return (
                <TextOut style={ { color: COLORS.info } } fontWeight={ 700 }>Выберите нужную конфигурацию из шаблона.</TextOut>
            );
        case ESteps.createDatabasePageDemoCheckbox:
            return (
                <TextOut style={ { color: COLORS.info } } fontWeight={ 700 }>Поставьте галочку в столбике &quot;Демо данные&quot;.</TextOut>
            );
        case ESteps.createDatabasePageButton:
            return (
                <>
                    <TextOut style={ { color: COLORS.info } } fontWeight={ 700 }>Нажмите на кнопку&quot;Создать&quot;.</TextOut>
                    <TextOut style={ { color: COLORS.warning } } fontWeight={ 700 }>Переход на страницу &quot;Мои информационные базы&quot;.</TextOut>
                </>
            );
        case ESteps.myDatabasesPageTable:
            return (
                <TextOut>Здесь Вы можете публиковать базы, переименовывать и удалять их, чистить кэш, выдавать доступ пользователям.</TextOut>
            );
        case ESteps.mainPageRent:
            return (
                <>
                    <TextOut>Выбрать подходящий тариф своим пользователям, узнать срок окончания тарифа и цены можно в меню &quot;Аренда 1С&quot;.</TextOut>
                    <TextOut style={ { color: COLORS.warning } } fontWeight={ 700 }>Переход на страницу &quot;Аренда 1С&quot;.</TextOut>
                </>
            );
        case ESteps.rentPageTariff:
            return (
                <>
                    <TextOut>Для смены тарифа достаточно переключить бегунок на нужный тариф.</TextOut>
                    <ul className={ styles.list }>
                        <li><TextOut fontWeight={ 700 }>WEB</TextOut> позволяет работать только через браузер или &quot;тонкий&quot; клиент;</li>
                        <li>
                            <TextOut fontWeight={ 700 }>Стандарт </TextOut>
                            дает возможность работы также через RDP, RemoteApp, получить <TextOut fontWeight={ 700 }>разовую бесплатную</TextOut> консультацию 1С и дает доступ к сервису &quot;Fasta&quot;.
                        </li>
                    </ul>
                </>
            );
        case ESteps.rentPageAcceptButton:
            return (
                <TextOut>После этого необходимо применить изменения.</TextOut>
            );
        case ESteps.rentPageInfo:
            return (
                <TextOut>В этом меню Вы видите количество подключенных пользователей, стоимость услуг и дату до которой возможно использование сервиса.</TextOut>
            );
        case ESteps.mainPageBalance:
            return (
                <>
                    <TextOut>Сформировать счёт и оплатить сервис можно на странице <TextOut fontWeight={ 700 }>&quot;Баланс&quot;</TextOut>.</TextOut>
                    <TextOut style={ { color: COLORS.warning } } fontWeight={ 700 }>Переход на страницу &quot;Баланс&quot;.</TextOut>
                </>
            );
        case ESteps.billingPageReplenishBalanceButton:
            return (
                <>
                    <TextOut>Для формирования счета и его оплаты необходимо нажать на кнопку <TextOut fontWeight={ 700 }>&quot;Пополнить баланс&quot;</TextOut>.</TextOut>
                    <TextOut style={ { color: COLORS.warning } } fontWeight={ 700 }>Переход на страницу &quot;Создание счета&quot;</TextOut>
                </>

            );
        case ESteps.replenishBalancePagePaymentPeriod:
            return (
                <TextOut>
                    Выберите период оплаты - чем больше период, тем больше бонусов Вы получите на счет.
                </TextOut>
            );
        case ESteps.replenishBalancePageGenerateInvoice:
            return (
                <TextOut>
                    Оплатить счет Вы можете картой или оплатить в банке, сформировав счет.
                </TextOut>
            );
        case ESteps.billingPageTransactionTab:
            return (
                <TextOut>
                    Все списания и зачисления Вы можете видеть в меню <TextOut fontWeight={ 700 }>&quot;Транзакции&quot;</TextOut>.
                </TextOut>
            );
        case ESteps.billingPagePromisePaymentTab:
            return (
                <TextOut>
                    Если Вы не успели оплатить счет вовремя, а работать нужно, Вы можете воспользоваться услугой
                    <TextOut fontWeight={ 700 }> &quot;Обещанный платеж&quot; </TextOut>
                    (доступно после первой оплаты).
                </TextOut>
            );
        case ESteps.mainPageConclusion:
            return (
                <TextOut><TextOut fontWeight={ 700 }>Благодарим за уделенное время!</TextOut> Если Вы что-то забудете, то всегда можете вызвать инструкцию в верхнем меню.</TextOut>
            );
        default:
            return (
                <TextOut textAlign="center">Добро пожаловать в инструкцию использования Личного Кабинета!</TextOut>
            );
    }
};