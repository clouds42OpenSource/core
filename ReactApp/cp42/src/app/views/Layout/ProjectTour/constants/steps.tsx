import { StepType } from '@reactour/tour';

import { Steps } from '../components';
import { getSelector, getMutation } from '../helpers';
import { ERefreshId, ESteps, ETourId } from '../enums';

export const STEPS: StepType[] = [
    {
        selector: getSelector(),
        content: <Steps step={ ESteps.mainPage } />,
        position: 'center'
    },
    {
        selector: getSelector(ETourId.myDatabases),
        content: <Steps step={ ESteps.mainPageMyDatabases } />
    },
    {
        highlightedSelectors: [getSelector(ETourId.createDatabase)],
        mutationObservables: [getMutation(ERefreshId.controlPanelInfo)],
        selector: getSelector(ETourId.createDatabase),
        content: <Steps step={ ESteps.myDatabasesPageCreateButton } />
    },
    {
        selector: getSelector(),
        content: <Steps step={ ESteps.createDatabasePage } />,
        highlightedSelectors: [getSelector(ETourId.createDatabasePageTemplate)],
        mutationObservables: [getMutation(ERefreshId.createDatabaseTable)],
        stepInteraction: true
    },
    {
        selector: getSelector(ETourId.createDatabasePageDemoCheckbox),
        content: <Steps step={ ESteps.createDatabasePageDemoCheckbox } />,
        stepInteraction: true,
        padding: { popover: 50 }
    },
    {
        selector: getSelector(ETourId.createDatabasePageButton),
        content: <Steps step={ ESteps.createDatabasePageButton } />,
        stepInteraction: true,
        padding: { popover: 50 }
    },
    {
        selector: getSelector(),
        content: <Steps step={ ESteps.myDatabasesPageTable } />,
        highlightedSelectors: [getSelector(ETourId.myDatabasesPageTable)],
        mutationObservables: [getMutation(ERefreshId.myDatabasesTable)]
    },
    {
        selector: getSelector(ETourId.rent),
        content: <Steps step={ ESteps.mainPageRent } />
    },
    {
        selector: getSelector(),
        highlightedSelectors: [getSelector(ETourId.rentPageInfo)],
        mutationObservables: [getMutation(ERefreshId.rentInfo)],
        content: <Steps step={ ESteps.rentPageInfo } />
    },
    {
        selector: getSelector(ETourId.rentPageTariff),
        content: <Steps step={ ESteps.rentPageTariff } />,
        stepInteraction: true
    },
    {
        selector: getSelector(ETourId.rentPageAcceptButton),
        content: <Steps step={ ESteps.rentPageAcceptButton } />,
        padding: { popover: 50 }
    },
    {
        selector: getSelector(ETourId.balance),
        content: <Steps step={ ESteps.mainPageBalance } />
    },
    {
        selector: getSelector(),
        content: <Steps step={ ESteps.billingPageReplenishBalanceButton } />,
        highlightedSelectors: [getSelector(ETourId.billingPageReplenishBalanceButton)],
        mutationObservables: [getMutation(ERefreshId.balanceButton)]
    },
    {
        selector: getSelector(),
        content: <Steps step={ ESteps.replenishBalancePagePaymentPeriod } />,
        highlightedSelectors: [getSelector(ETourId.replenishBalancePagePaymentPeriod)],
        mutationObservables: [getMutation(ERefreshId.userPaymentViewTable)]
    },
    {
        selector: getSelector(ETourId.replenishBalancePageGenerateInvoiceButton),
        content: <Steps step={ ESteps.replenishBalancePageGenerateInvoice } />,
        padding: { popover: 50 }
    },
    {
        selector: getSelector(),
        content: <Steps step={ ESteps.billingPageTransactionTab } />,
        highlightedSelectors: [getSelector(ETourId.billingPageTransactionTab)],
        mutationObservables: [getMutation(ERefreshId.replenishBalanceTable)]
    },
    {
        selector: getSelector(ETourId.billingPagePromisePaymentTab),
        content: <Steps step={ ESteps.billingPagePromisePaymentTab } />
    },
    {
        selector: getSelector(ETourId.services),
        content: <Steps step={ ESteps.mainPageServices } />
    },
    {
        selector: getSelector(ETourId.profile),
        content: <Steps step={ ESteps.mainPageProfile } />
    },
    {
        selector: getSelector(ETourId.onboarding),
        content: <Steps step={ ESteps.mainPageConclusion } />,
    }
];