import { StepType } from '@reactour/tour';

import { ConnectionSteps } from '../components';
import { getSelector } from '../helpers';
import { EConnectionSteps, ERefreshId, ETourId } from '../enums';

export const CONNECTION_STEPS: StepType[] = [
    {
        selector: getSelector(ETourId.connectionSettings),
        content: <ConnectionSteps step={ EConnectionSteps.mainPageConnectionSettings } />,
        padding: { popover: 50 }
    },
    {
        selector: getSelector(),
        content: <ConnectionSteps step={ EConnectionSteps.connectionSettingsPageInstructions } />,
        highlightedSelectors: [getSelector(ETourId.connectionSettingsPageInstructions)],
        mutationObservables: [ERefreshId.connectionSettingsInfo]
    },
    {
        selector: getSelector(ETourId.connectionSettingsPageVersions),
        content: <ConnectionSteps step={ EConnectionSteps.connectionSettingsPageVersions } />
    },
    {
        selector: getSelector(ETourId.connectionSettingsPageRdp),
        content: <ConnectionSteps step={ EConnectionSteps.connectionSettingsPageRdp } />
    },
];