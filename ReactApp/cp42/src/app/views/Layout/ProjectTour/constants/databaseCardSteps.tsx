import { StepType } from '@reactour/tour';
import { getSelector } from 'app/views/Layout/ProjectTour/helpers';

const getImage = (name: string) => `${ window.location.origin }/img/project-tour/database-card/${ name }.png`;

const getImageComponent = (name: string) =>
    <img style={ { borderRadius: '3px' } } loading="lazy" src={ getImage(name) } alt={ name } width={ 960 } height={ 540 } />;

export const DATABASE_CARD_STEPS: StepType[] = [
    {
        selector: getSelector(),
        position: 'right',
        content: getImageComponent('pic1')
    },
    {
        selector: getSelector(),
        position: 'right',
        content: getImageComponent('pic2')
    },
    {
        selector: getSelector(),
        position: 'right',
        content: getImageComponent('pic3')
    },
    {
        selector: getSelector(),
        position: 'right',
        content: getImageComponent('pic4')
    },
    {
        selector: getSelector(),
        position: 'right',
        content: getImageComponent('pic5')
    },
    {
        selector: getSelector(),
        position: 'right',
        content: getImageComponent('pic6')
    },
    {
        selector: getSelector(),
        position: 'right',
        content: getImageComponent('pic7')
    }
];