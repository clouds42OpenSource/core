import { StepType } from '@reactour/tour';

import { UsersSteps } from '../components';
import { getSelector, getMutation } from '../helpers';
import { ERefreshId, ETourId, EUsersSteps } from '../enums';

export const USER_STEPS: StepType[] = [
    {
        selector: getSelector(ETourId.users),
        content: <UsersSteps step={ EUsersSteps.mainPageUsers } />
    },
    {
        selector: getSelector(),
        content: <UsersSteps step={ EUsersSteps.usersPageAddUser } />,
        mutationObservables: [getMutation(ERefreshId.usersPage)],
        highlightedSelectors: [getSelector(ETourId.usersPageAddUser)]
    },
    {
        selector: getSelector(ETourId.usersPageTable),
        content: <UsersSteps step={ EUsersSteps.usersPageTable } />,
        stepInteraction: true
    },
    {
        selector: getSelector(ETourId.usersPageDisableButton),
        content: <UsersSteps step={ EUsersSteps.usersPageDisableButton } />,
    },
    {
        selector: getSelector(ETourId.usersPageAccessTab),
        content: <UsersSteps step={ EUsersSteps.usersPageAccessTab } />
    },
];