import { StepType } from '@reactour/tour';

import { PartnersSteps } from '../components';
import { getSelector, getMutation } from '../helpers';
import { EPartnersSteps, EPartnersTourId, ERefreshId } from '../enums';

export const PARTNERS_STEPS: StepType[] = [
    {
        selector: getSelector(),
        content: <PartnersSteps step={ EPartnersSteps.referral } />,
        highlightedSelectors: [getSelector(EPartnersTourId.referral)],
        mutationObservables: [getMutation(ERefreshId.partnersCardInfo), getMutation(ERefreshId.partnersCard)]
    },
    {
        selector: getSelector(EPartnersTourId.currentSum),
        content: <PartnersSteps step={ EPartnersSteps.currentSum } />
    },
    {
        selector: getSelector(EPartnersTourId.cashOut),
        content: <PartnersSteps step={ EPartnersSteps.cashOut } />
    },
    {
        selector: getSelector(EPartnersTourId.clientsTabTable),
        content: <PartnersSteps step={ EPartnersSteps.clientsTabTable } />
    },
    {
        selector: getSelector(EPartnersTourId.clientsTabRegisterClient),
        content: <PartnersSteps step={ EPartnersSteps.clientsTabRegisterClient } />,
        padding: { popover: 50 }
    },
    {
        selector: getSelector(EPartnersTourId.transactionsTab),
        content: <PartnersSteps step={ EPartnersSteps.transactionsTab } />
    },
    {
        selector: getSelector(EPartnersTourId.requisitesTab),
        content: <PartnersSteps step={ EPartnersSteps.requisitesTab } />
    },
    {
        selector: getSelector(EPartnersTourId.withdrawalTab),
        content: <PartnersSteps step={ EPartnersSteps.withdrawalTab } />,
        stepInteraction: true
    },
    {
        selector: getSelector(EPartnersTourId.withdrawalTabCreate),
        content: <PartnersSteps step={ EPartnersSteps.withdrawalTabCreate } />,
        padding: { popover: 50 }
    },
    {
        selector: getSelector(EPartnersTourId.withdrawalTabCashOut),
        content: <PartnersSteps step={ EPartnersSteps.withdrawalTabCashOut } />,
        padding: { popover: 50 }
    },
    {
        selector: getSelector(EPartnersTourId.servicesTab),
        content: <PartnersSteps step={ EPartnersSteps.servicesTab } />,
        stepInteraction: true
    },
    {
        selector: getSelector(EPartnersTourId.servicesTabCreate),
        content: <PartnersSteps step={ EPartnersSteps.servicesTabCreate } />,
        stepInteraction: true
    },
    {
        selector: getSelector(),
        content: <PartnersSteps step={ EPartnersSteps.createServiceCheckbox } />,
        highlightedSelectors: [getSelector(EPartnersTourId.createServiceCheckbox)],
        mutationObservables: [getMutation(ERefreshId.addServiceCard)],
        stepInteraction: true
    },
    {
        selector: getSelector(EPartnersTourId.createServiceTitle),
        content: <PartnersSteps step={ EPartnersSteps.createServiceTitle } />
    },
    {
        selector: getSelector(EPartnersTourId.createServiceLogo),
        content: <PartnersSteps step={ EPartnersSteps.createServiceLogo } />
    },
    {
        selector: getSelector(EPartnersTourId.createServiceFile),
        content: <PartnersSteps step={ EPartnersSteps.createServiceFile } />
    },
    {
        selector: getSelector(EPartnersTourId.createServiceCost),
        content: <PartnersSteps step={ EPartnersSteps.createServiceCost } />
    },
    {
        selector: getSelector(EPartnersTourId.createServiceDraft),
        content: <PartnersSteps step={ EPartnersSteps.createServiceDraft } />,
        padding: { popover: 50 }
    },
    {
        selector: getSelector(EPartnersTourId.createServiceConclusion),
        content: <PartnersSteps step={ EPartnersSteps.createServiceConclusion } />,
        padding: { popover: 50 }
    }
];