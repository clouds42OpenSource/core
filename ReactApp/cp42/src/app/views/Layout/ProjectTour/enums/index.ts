export enum ESteps {
    mainPage,
    mainPageMyDatabases,
    myDatabasesPageCreateButton,
    createDatabasePage,
    createDatabasePageDemoCheckbox,
    createDatabasePageButton,
    myDatabasesPageTable,
    mainPageRent,
    rentPageInfo,
    rentPageTariff,
    rentPageAcceptButton,
    mainPageBalance,
    billingPageReplenishBalanceButton,
    replenishBalancePagePaymentPeriod,
    replenishBalancePageGenerateInvoice,
    billingPageTransactionTab,
    billingPagePromisePaymentTab,
    mainPageServices,
    mainPageProfile,
    mainPageConclusion
}

export enum EUsersSteps {
    mainPageUsers,
    usersPageAddUser,
    usersPageTable,
    usersPageDisableButton,
    usersPageAccessTab
}

export enum EConnectionSteps {
    mainPageConnectionSettings,
    connectionSettingsPageInstructions,
    connectionSettingsPageVersions,
    connectionSettingsPageRdp,
}

export enum ETourId {
    root = 'root',
    services = 'services',
    profile = 'profile',
    myDatabases = 'my-databases',
    createDatabase = 'create-database',
    createDatabasePageTemplate = 'create-database-page-template',
    createDatabasePageDemoCheckbox = 'create-database-page-demo-checkbox',
    createDatabasePageButton = 'create-database-page-create-button',
    myDatabasesPageTable = 'my-databases-page-table',
    rent = 'rent',
    rentPageInfo = 'rent-page-info',
    rentPageTariff = 'rent-page-tariff',
    rentPageAcceptButton = 'rent-page-accept-button',
    connectionSettings = 'connection-settings',
    connectionSettingsPageInstructions = 'connection-settings-page-instructions',
    connectionSettingsPageVersions = 'connection-settings-page-versions',
    connectionSettingsPageRdp = 'connection-settings-page-rdp',
    balance = 'balance',
    billingPageReplenishBalanceButton = 'billing-page-replenish-balance-button',
    replenishBalancePagePaymentPeriod = 'replenish-balance-page-payment-period',
    replenishBalancePageGenerateInvoiceButton = 'replenish-balance-page-generate-invoice-button',
    billingPageTransactionTab = 'billing-page-transaction-tab',
    billingPagePromisePaymentTab = 'billing-page-promise-payment-tab',
    users = 'users',
    usersPageAddUser = 'users-page-add-user',
    usersPageTable = 'users-page-table',
    usersPageDisableButton = 'users-page-disable-button',
    usersPageAccessTab = 'users-page-access-tab',
    onboarding = 'onboarding'
}

export enum EPartnersTourId {
    referral = 'referral',
    currentSum = 'current-sum',
    cashOut = 'cash-out',
    clientsTabTable = 'clients-tab-table',
    clientsTabRegisterClient = 'clients-tab-register-client',
    transactionsTab = 'transactions-tab',
    requisitesTab = 'requisites-tab',
    withdrawalTab = 'withdrawal-tab',
    withdrawalTabCreate = 'withdrawal-tab-create',
    withdrawalTabCashOut = 'withdrawal-tab-cash-out',
    servicesTab = 'services-tab',
    servicesTabCreate = 'services-tab-create',
    createServiceCheckbox = 'create-service-checkbox',
    createServiceTitle = 'create-service-title',
    createServiceLogo = 'create-service-logo',
    createServiceFile = 'create-service-file',
    createServiceCost = 'create-service-cost',
    createServiceDraft = 'create-service-draft',
    createServiceConclusion = 'create-service-conclusion'
}

export enum EPartnersSteps {
    referral,
    currentSum,
    cashOut,
    clientsTabTable,
    clientsTabRegisterClient,
    transactionsTab,
    requisitesTab,
    withdrawalTab,
    withdrawalTabCreate,
    withdrawalTabCashOut,
    servicesTab,
    servicesTabCreate,
    createServiceCheckbox,
    createServiceTitle,
    createServiceLogo,
    createServiceFile,
    createServiceCost,
    createServiceDraft,
    createServiceConclusion
}

export enum ERefreshId {
    leftSideMenu = 'left-side-menu',
    controlPanelInfo = 'control-panel-info',
    createDatabaseTable = 'create-database-table',
    myDatabasesTable = 'my-databases-table',
    balanceButton = 'balance-button',
    replenishBalanceTable = 'replenish-balance-table',
    userPaymentViewTable = 'user-payment-view-table',
    partnersCard = 'partners-card',
    partnersCardInfo = 'partners-card-info',
    addServiceCard = 'add-service-card',
    connectionSettingsInfo = 'connection-settings-info',
    rentInfo = 'rent-info',
    usersPage = 'users-page'
}

export enum ETourStages {
    full = 'full',
    informationBases = 'informationBases',
    connectionSettings = 'connectionSettings',
    rent = 'rent',
    balance = 'balance',
    users = 'users',
    partners = 'partners',
    databaseCard = 'databaseCard'
}