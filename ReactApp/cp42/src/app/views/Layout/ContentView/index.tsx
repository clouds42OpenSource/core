import { Box } from '@mui/material';
import { COLORS } from 'app/utils';
import { TextOut } from 'app/views/components/TextOut';
import { ReactNode, Component } from 'react';

type TProps = {
    children?: ReactNode;
};

type TState = {
    title: string;
};

export class ContentView extends Component<TProps, TState> {
    public constructor(props: TProps) {
        super(props);
        this.state = {
            title: ''
        };
    }

    public componentDidMount() {
        const target = document.querySelector('title') as Node;
        const observer = new MutationObserver(mutations => {
            mutations.forEach(_mutation => {
                this.setState({ title: document.title });
            });
        });
        observer.observe(target, { childList: true });
    }

    public render() {
        return (
            <Box
                id="ContentContainer"
                sx={ {
                    backgroundColor: COLORS.white,
                    margin: '0 16px',
                    flex: 1,
                    marginBottom: 0,
                    display: 'flex',
                    flexDirection: 'column',
                    borderRadius: '5px',
                    border: '1px #e7eaec solid',
                    overflowY: 'hidden',
                } }
            >
                <Box sx={ { borderBottom: '1px #e7eaec solid', paddingLeft: '20px', paddingBlock: '10px' } }>
                    { this.state.title && (
                        <TextOut style={ { paddingBottom: '8px' } } fontSize={ 14 } fontWeight={ 600 }>{ this.state.title }</TextOut>
                    ) }
                </Box>
                <Box sx={ { padding: '15px', overflowY: 'auto', flex: 1 } }>
                    { this.props.children }
                </Box>
            </Box>
        );
    }
}