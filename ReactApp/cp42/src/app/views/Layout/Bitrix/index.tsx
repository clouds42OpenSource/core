import { useEffect } from 'react';

export const Bitrix = () => {
    useEffect(() => {
        const script = document.createElement('script');

        script.src = `${ import.meta.env.PUBLIC_URL }/js/externalServices/Bitrix24Widget.js`;

        document.body.appendChild(script);
    }, []);

    return null;
};