import { GetCurrentSessionsSettingsResponse } from 'app/api/endpoints/global/response';
import { REDUX_API } from 'app/api/useReduxApi';
import { AppRoutes } from 'app/AppRoutes';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { RouteComponentProps } from 'react-router-dom';

import styles from './style.module.css';

type StateProps = {
    settings: GetCurrentSessionsSettingsResponse,
    hasSettingsReceived: boolean
};

type DispatchProps = {
    dispatchReceiveCurrentSessionSettingsThunk: () => void;
};

type TProps = StateProps & DispatchProps & RouteComponentProps;

const { getCurrentSessionSettings } = REDUX_API.GLOBAL_REDUX;

class ExitFromDifferentContextButtonClass extends React.Component<TProps> {
    public constructor(props: TProps) {
        super(props);
        this.exitFromDifferentAccount = this.exitFromDifferentAccount.bind(this);
        this.getExitButtonText = this.getExitButtonText.bind(this);
    }

    private getExitButtonText(accountName: string, accountNumber: number) {
        const maxAccountNameLength = 40;

        if (accountName.length > maxAccountNameLength) {
            accountName = `${ accountName.slice(0, maxAccountNameLength) }...`;
        }

        return `Чужой аккаунт [${ accountName } - ${ accountNumber }] (Вернуться)`;
    }

    private async exitFromDifferentAccount() {
        InterlayerApiProxy.getAccountApi().exitFromDifferentAccount();
        this.props.dispatchReceiveCurrentSessionSettingsThunk();
        window.location.href = AppRoutes.administration.accountsListRoute;
    }

    public render() {
        const isDifferentContext = this.props.hasSettingsReceived && this.props.settings.currentContextInfo.isDifferentContextAccount;
        const accountName = this.props.settings.currentContextInfo.contextAccountName;
        const accountNumber = this.props.settings.currentContextInfo.contextAccountIndex;

        if (isDifferentContext) {
            return (
                <button type="button" onClick={ this.exitFromDifferentAccount } className={ styles.button }>
                    <span>{ this.getExitButtonText(accountName, accountNumber) }</span>
                </button>
            );
        }

        return <div />;
    }
}

export const ExitFromDifferentContextButton = connect<StateProps, DispatchProps, object, AppReduxStoreState>(
    state => {
        const currentSessionSettingsState = state.Global.getCurrentSessionSettingsReducer;
        const { settings } = currentSessionSettingsState;
        const { hasSettingsReceived } = currentSessionSettingsState.hasSuccessFor;

        return {
            settings,
            hasSettingsReceived
        };
    },
    dispatch => ({
        dispatchReceiveCurrentSessionSettingsThunk: () => getCurrentSessionSettings(dispatch)
    })
)(withRouter(ExitFromDifferentContextButtonClass));