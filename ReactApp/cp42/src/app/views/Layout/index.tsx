import { Box } from '@mui/material';
import { FETCH_API } from 'app/api/useFetchApi';
import { REDUX_API } from 'app/api/useReduxApi';
import { AppConsts } from 'app/common/constants';
import { useAppDispatch, useAppSelector, useIsMobile } from 'app/hooks';
import { TalkMe } from 'app/views/Layout/TalkMe';
import { ReactNode, useEffect, useRef, useState } from 'react';
import { AppRoutes } from 'app/AppRoutes';
import { localStorageHelper } from 'app/common/helpers/LocalStorageHelper';
import { isTimeToRefreshToken } from 'app/common/helpers/ExpiredTimeHelper';
import { TimerHelper } from 'app/common/helpers/TimerHelper';
import { AlertView } from 'app/views/Layout/AlertView/AlertView';
import { NoMoneyNoHoney, ServerError } from 'app/views/modules/Errors';
import { ContentView } from './ContentView';
import { LeftSideBar } from './LeftSideBar';
import { TopSideBar } from './TopSideBar';

type TProps = {
    children?: ReactNode;
}

const { getNavMenu, getCurrentSessionSettings, getUserPermissions } = REDUX_API.GLOBAL_REDUX;
const { postCheckToken } = FETCH_API.AUTH;

export const Layout = ({ children }: TProps) => {
    const intervalRef = useRef<NodeJS.Timeout | number>(-1);
    const isMobile = useIsMobile();

    const [isOpen, setIsOpen] = useState(!isMobile);

    const dispatch = useAppDispatch();
    const hasMenuReceived = useAppSelector(state => !!state.Global.getNavMenuReducer.data?.rawData.topSideMenu);
    const hasTokenReceived = !!localStorageHelper.getJWTToken();

    useEffect(() => {
        void getNavMenu(dispatch);
        void getCurrentSessionSettings(dispatch);
        void getUserPermissions(dispatch);
    }, [dispatch]);

    useEffect(() => {
        intervalRef.current = setInterval(() => {
            if (isTimeToRefreshToken()) {
                void postCheckToken();
            }
        }, TimerHelper.waitTimeForCheckTokenRefresh);

        return () => {
            clearInterval(intervalRef.current);
        };
    }, []);

    if (window.location.href.includes(AppRoutes.noMoneyNoHoney)) {
        return (
            <NoMoneyNoHoney />
        );
    }

    if (window.location.href.includes(AppRoutes.serverError)) {
        return (
            <ServerError />
        );
    }

    return (
        <Box
            sx={ {
                overflowX: 'hidden',
                height: '100dvh',
                ml: isOpen && !isMobile ? `${ AppConsts.desktopLeftMenuWidth }px` : 0,
                width: isOpen && !isMobile ? `calc(100% - ${ AppConsts.desktopLeftMenuWidth })px` : '100%',
            } }
        >
            <LeftSideBar isOpen={ isOpen } close={ setIsOpen } />
            <Box sx={ { display: 'flex', flexDirection: 'column', marginBottom: '16px', gap: '16px' } }>
                { hasTokenReceived &&
                    <>
                        { /*<Bitrix />*/ }
                        <TalkMe />
                        <TopSideBar />
                    </>
                }
                <AlertView />
                <ContentView>
                    { hasTokenReceived && hasMenuReceived && children }
                </ContentView>
            </Box>
        </Box>
    );
};