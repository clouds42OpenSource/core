import { HubConnection, HubConnectionBuilder } from '@microsoft/signalr';
import DeleteIcon from '@mui/icons-material/Delete';
import MarkEmailReadIcon from '@mui/icons-material/MarkEmailRead';
import NotificationsNoneOutlinedIcon from '@mui/icons-material/NotificationsNoneOutlined';
import ReportOutlinedIcon from '@mui/icons-material/ReportOutlined';
import { Badge, Box, ClickAwayListener, Fade, Popper } from '@mui/material';
import Stack from '@mui/material/Stack';
import { apiHost, cpHost, wpHost } from 'app/api';
import { EStateNotification, Notification } from 'app/api/endpoints/notification/response';
import { FETCH_API } from 'app/api/useFetchApi';
import { REDUX_API } from 'app/api/useReduxApi';
import { localStorageHelper } from 'app/common/helpers/LocalStorageHelper';
import { EMessageType, useAppDispatch, useAppSelector, useFloatMessages } from 'app/hooks';
import { criticalUpdatesSlice, getNotificationsSlice } from 'app/modules/notifications';
import { COLORS } from 'app/utils';
import { Dialog } from 'app/views/components/controls/Dialog';
import { TextWithTooltip } from 'app/views/components/controls/forms/Tooltips';
import { TooltipPlacementTypes } from 'app/views/components/controls/forms/Tooltips/types';
import { TextOut } from 'app/views/components/TextOut';
import dayjs from 'dayjs';
import React, { memo, useCallback, useEffect, useMemo, useState } from 'react';
import { Virtuoso } from 'react-virtuoso';
import css from './styles.module.css';
import { NotificationItem } from './NotificationItem';

type OwnProps = {
    styles?: React.CSSProperties;
};

const { getNotifications } = REDUX_API.NOTIFICATIONS_REDUX;
const { readNotification, deleteNotification } = FETCH_API.NOTIFICATIONS;
const { putFeedback } = FETCH_API.FEEDBACK;

const { success, update, readAllNotification, isReadById } = getNotificationsSlice.actions;
const { open, read } = criticalUpdatesSlice.actions;

export const NotificationCenter = memo((props: OwnProps) => {
    const { data, message } = useAppSelector(state => state.Notifications.getNotifications);
    const { isOpenNotification, id, content, state: notificationState } = useAppSelector(state => state.Notifications.criticalNotification);
    const dispatch = useAppDispatch();

    const userId = useMemo(() => localStorageHelper.getUserId() ?? '', []);

    const { show } = useFloatMessages();

    const [isOpen, setIsOpen] = useState(false);
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
    const [connection, setConnection] = useState<null | HubConnection>(null);

    useEffect(() => {
        const connect = new HubConnectionBuilder()
            .withUrl(apiHost('hubs/notification'))
            .withAutomaticReconnect()
            .build();

        setConnection(connect);
    }, []);

    useEffect(() => {
        if (connection) {
            connection
                .start()
                .then(() => {
                    connection.on('Broadcast', notification => dispatch(update(notification as Notification)));
                })
                .catch(error => console.log(error));
        }
    }, [connection, dispatch]);

    useEffect(() => {
        void getNotifications(dispatch, userId ?? '');
    }, [dispatch, userId]);

    useEffect(() => {
        if (message) {
            show(EMessageType.error, message);
        }
    }, [message, show]);

    const readNotificationById = async (idNotification: string) => {
        try {
            const response = await readNotification({ idList: [idNotification] });

            if (response.success) {
                dispatch(isReadById(idNotification));
            } else {
                show(EMessageType.error, response.message);
            }
        } catch (err) {
            show(EMessageType.error, 'При прочтение уведомления произошла ошибка. Пожалуйста, повторите позже или обратитесь в поддержку.');
        }
    };

    const readAll = async () => {
        const response = await readNotification({ accountUserId: userId });

        if (response.success) {
            dispatch(readAllNotification());
        } else {
            show(EMessageType.error, response.message);
        }
    };

    const removedAll = async () => {
        if (data) {
            const response = await deleteNotification({ accountUserId: userId });

            if (response.success) {
                dispatch(success(
                    {
                        ...data,
                        records: []
                    }
                ));
            } else {
                show(EMessageType.error, response.message);
            }
        }
    };

    const findCriticalOrReviewNotification = useCallback((stateNotification: EStateNotification) => {
        if (data) {
            const currentDate = new Date();
            const yesterdayDate = new Date(currentDate);
            yesterdayDate.setDate(yesterdayDate.getDate() - 1);

            const firstNotification = data.records
                .filter(item => {
                    const dateCreateOn = new Date(item.createdOn);

                    return (
                        (dateCreateOn.toDateString() === currentDate.toDateString() ||
                        dateCreateOn.toDateString() === yesterdayDate.toDateString()) ||
                        item.state === EStateNotification.review
                    ) && item.state === stateNotification && !item.isRead;
                });

            if (firstNotification.length) {
                const [notification] = firstNotification;
                dispatch(open({ id: notification.id, content: notification.message, date: notification.createdOn, state: notification.state }));

                return notification;
            }
        }
    }, [data, dispatch]);

    const closeCriticalNotification = async () => {
        if (notificationState !== EStateNotification.critical) {
            window.open(wpHost('reviews-promo'), '_blank');
        }

        await readNotificationById(id);

        dispatch(read());

        if (notificationState === EStateNotification.critical) {
            findCriticalOrReviewNotification(EStateNotification.review);
        }
    };

    const forceCloseNotification = async () => {
        await readNotificationById(id);

        dispatch(read());
    };

    useEffect(() => {
        const criticalNotification = findCriticalOrReviewNotification(EStateNotification.critical);

        if (!criticalNotification) {
            findCriticalOrReviewNotification(EStateNotification.review);
        }
    }, [findCriticalOrReviewNotification]);

    const badgeOnClick = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorEl(event.currentTarget);
        setIsOpen(prev => !prev);
    };

    const delayFeedback = async (month: number) => {
        const { success: feedbackSuccess, message: feedbackMessage } = await putFeedback({
            id,
            delay: dayjs().add(month, 'month').toISOString()
        });

        if (!feedbackSuccess) {
            show(EMessageType.error, feedbackMessage);
        } else {
            await readNotificationById(id);
        }

        dispatch(read());
    };

    return (
        <>
            <ClickAwayListener onClickAway={ () => setIsOpen(false) }>
                <Box
                    sx={ { display: 'inline-block', verticalAlign: 'bottom', ...props.styles } }
                >
                    {
                        userId && (
                            <Badge
                                badgeContent={ data?.records.filter(item => !item.isRead).length }
                                color="error"
                                invisible={ data?.records.every(item => item.isRead) }
                                onClick={ badgeOnClick }
                                sx={ {
                                    cursor: 'pointer',
                                    '& .MuiBadge-badge': {
                                        top: '5px',
                                        right: '5px',
                                        lineHeight: 'unset',
                                        padding: '0 3px',
                                        height: 15,
                                        minWidth: 15
                                    }
                                } }
                            >
                                <NotificationsNoneOutlinedIcon />
                            </Badge>
                        )
                    }
                    <Popper
                        open={ isOpen }
                        anchorEl={ anchorEl }
                        placement="bottom-end"
                        transition={ true }
                        sx={ {
                            zIndex: 1000
                        } }
                    >
                        {
                            ({ TransitionProps }) => (
                                <Fade { ...TransitionProps } timeout={ 350 }>
                                    <Box className={ css.notificationWrapper }>
                                        <Box display="flex" flexDirection="row" justifyContent="space-between" alignItems="center" p="0 16px">
                                            <TextOut
                                                fontSize={ 18 }
                                                fontWeight={ 700 }
                                                style={ { margin: '8px 0' } }
                                                inDiv={ true }
                                            >
                                                Уведомления
                                            </TextOut>
                                            <div>
                                                {
                                                    data?.records.every(item => item.isRead)
                                                        ? (
                                                            <MarkEmailReadIcon
                                                                sx={ { cursor: 'default', color: COLORS.disabled } }
                                                            />
                                                        )
                                                        : (
                                                            <TextWithTooltip
                                                                text={
                                                                    <MarkEmailReadIcon
                                                                        sx={ { cursor: 'pointer' } }
                                                                        onClick={ readAll }
                                                                    />
                                                                }
                                                                tooltipPlacement={ TooltipPlacementTypes.top }
                                                                tooltipText="Прочитать все"
                                                            />
                                                        )
                                                }
                                                {
                                                    data?.records.length
                                                        ? (
                                                            <TextWithTooltip
                                                                text={
                                                                    <DeleteIcon
                                                                        sx={ { cursor: 'pointer' } }
                                                                        onClick={ removedAll }
                                                                    />
                                                                }
                                                                tooltipPlacement={ TooltipPlacementTypes.top }
                                                                tooltipText="Удалить все"
                                                            />
                                                        )
                                                        : (
                                                            <DeleteIcon
                                                                sx={ { cursor: 'default', color: COLORS.disabled } }
                                                            />
                                                        )
                                                }
                                            </div>
                                        </Box>
                                        <Stack
                                            sx={ {
                                                height: '400px',
                                                width: 'min(360px, 420px)',
                                                alignItems: 'center',
                                                overflowY: 'auto',
                                                '& > div': {
                                                    width: '100%'
                                                }
                                            } }
                                            spacing={ 2 }
                                        >
                                            {
                                                data && data.records && data.records.length
                                                    ? (
                                                        <Virtuoso
                                                            data={ data.records }
                                                            key={ data.records.length }
                                                            itemContent={ (_, notification) => (
                                                                <div style={ { marginBottom: '8px', padding: '0 8px' } }>
                                                                    <NotificationItem notification={ notification } />
                                                                </div>
                                                            ) }
                                                        />
                                                    )
                                                    : (
                                                        <Box className={ css.empty }>
                                                            <img
                                                                src="/img/emptyNotifications.svg"
                                                                alt="Icon empty notification center"
                                                                loading="lazy"
                                                            />
                                                            <TextOut>Здесь пока ничего нет</TextOut>
                                                        </Box>
                                                    )
                                            }
                                        </Stack>
                                        <Box textAlign="center">
                                            <img
                                                className={ css.logo }
                                                src={ cpHost('img/42cloud_icon.png') }
                                                alt="42 clouds logo"
                                                loading="lazy"
                                            />
                                        </Box>
                                    </Box>
                                </Fade>
                            )
                        }
                    </Popper>
                </Box>
            </ClickAwayListener>
            <Dialog
                title={ notificationState === EStateNotification.critical ? 'Важное уведомление!' : 'Оцените нашу работу' }
                isOpen={ isOpenNotification }
                dialogWidth="sm"
                dialogVerticalAlign={ notificationState === EStateNotification.critical ? 'center' : 'top' }
                hiddenX={ notificationState === EStateNotification.critical }
                onCancelClick={ forceCloseNotification }
                buttons={
                    notificationState === EStateNotification.critical
                        ? [
                            {
                                content: 'Я прочитал',
                                kind: 'success',
                                onClick: () => closeCriticalNotification()
                            }
                        ]
                        : [
                            {
                                content: 'ОЦЕНИТЬ',
                                kind: 'warning',
                                onClick: () => closeCriticalNotification()
                            },
                            {
                                content: 'Отложить на 3 мес.',
                                kind: 'default',
                                onClick: () => delayFeedback(3)
                            },
                            {
                                content: 'Отложить на 6 мес.',
                                kind: 'default',
                                onClick: () => delayFeedback(6)
                            }
                        ]
                }
            >
                <Box display="flex" flexDirection="column" gap="16px" alignItems={ notificationState === EStateNotification.critical ? 'center' : 'flex-start' }>
                    {
                        notificationState === EStateNotification.critical
                            ? (
                                <>
                                    <ReportOutlinedIcon style={ { color: COLORS.warning, fontSize: '100px' } } />
                                    <TextOut fontSize={ 18 }>{ content }</TextOut>
                                </>
                            )
                            : (
                                <>
                                    <TextOut>
                                        Уважаемый клиент, спасибо, что пользуетесь продуктами 42Clouds. Мы действительно рады, что вы остаётесь с нами.&nbsp;
                                        Наша команда стремится развивать продукты, поэтому нам важно получить обратную связь от наших клиентов.&nbsp;
                                        Поделитесь своим мнением, это займёт всего несколько минут.
                                    </TextOut>
                                    <TextOut>
                                        Оцените, пожалуйста, продукты 42Clouds.
                                    </TextOut>
                                </>
                            )
                    }
                </Box>
            </Dialog>
        </>

    );
});