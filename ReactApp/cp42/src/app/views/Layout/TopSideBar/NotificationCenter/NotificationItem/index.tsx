import DeleteIcon from '@mui/icons-material/Delete';
import DraftsIcon from '@mui/icons-material/Drafts';
import MoreHorizIcon from '@mui/icons-material/MoreHoriz';
import { Menu, MenuItem } from '@mui/material';
import Button from '@mui/material/Button';
import { EStateNotification, Notification } from 'app/api/endpoints/notification/response';
import { FETCH_API } from 'app/api/useFetchApi';
import { EMessageType, useAppDispatch, useFloatMessages } from 'app/hooks';
import { getNotificationsSlice } from 'app/modules/notifications';
import { COLORS, DateUtility } from 'app/utils';
import { TextOut } from 'app/views/components/TextOut';
import React, { memo } from 'react';
import styles from './style.module.css';

type TProps = {
    notification: Notification;
};

const { readNotification, deleteNotification } = FETCH_API.NOTIFICATIONS;
const { isReadById, removedNotification } = getNotificationsSlice.actions;

export const NotificationItem = memo(({ notification }: TProps) => {
    const dispatch = useAppDispatch();

    const { show } = useFloatMessages();

    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
    const open = Boolean(anchorEl);

    const { id, message: text, createdOn: date, isRead, state } = notification;

    const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const readNotifications = () => {
        readNotification({ idList: [id] })
            .then(response => {
                if (response.success) {
                    dispatch(isReadById(id));
                } else {
                    show(EMessageType.error, response.message);
                }
            });
    };

    const deleteNotifications = () => {
        deleteNotification({ idList: [id] })
            .then(response => {
                if (response.success) {
                    dispatch(removedNotification(id));
                } else {
                    show(EMessageType.error, response.message);
                }
            });
    };

    if (state === EStateNotification.review) {
        return null;
    }

    return (
        <div className={ `${ styles.item } ${ isRead ? '' : `${ styles.notRead }` }` }>
            <div className={ styles.content }>
                { state === EStateNotification.critical && <TextOut fontSize={ 14 } fontWeight={ 500 }>Важное</TextOut> }
                <TextOut fontSize={ isRead ? 12 : 16 } fontWeight={ isRead ? 400 : 700 } dangerouslySetInnerHTML={ { __html: text.replace(/\n/g, '<br />') } } />
                <TextOut fontSize={ 12 } fontWeight={ 400 }>{ DateUtility.getDateByFormat(new Date(date), 'dd.MM.yyyy HH:mm') }</TextOut>
            </div>
            <div>
                <Button
                    className={ styles.actionsButton }
                    aria-controls={ open ? 'basic-menu' : undefined }
                    aria-haspopup="true"
                    aria-expanded={ open ? 'true' : undefined }
                    onClick={ handleClick }
                    size="small"
                >
                    <MoreHorizIcon sx={ { color: COLORS.disabled } } />
                </Button>
                <Menu
                    anchorEl={ anchorEl }
                    open={ open }
                    onClose={ handleClose }
                    disableScrollLock={ true }
                >
                    {
                        !isRead && (
                            <MenuItem onClick={ readNotifications }><DraftsIcon /> Прочитать</MenuItem>
                        )
                    }
                    <MenuItem onClick={ deleteNotifications }><DeleteIcon /> Удалить</MenuItem>
                </Menu>
            </div>
        </div>
    );
});