import { REDUX_API } from 'app/api/useReduxApi';
import { AppRoutes } from 'app/AppRoutes';
import { AccountUserGroup } from 'app/common/enums';
import { useAppDispatch, useAppSelector } from 'app/hooks';
import { ExitFromDifferentContextButton } from 'app/views/Layout/_buttons/ExitFromDifferentContextButton';
import { StartTourDialog } from 'app/views/Layout/ProjectTour/components';
import { ETourId } from 'app/views/Layout/ProjectTour/enums';
import { useEffect, useState } from 'react';
import { MenuItemView } from './MenuItemView';
import { NotificationCenter } from './NotificationCenter';
import css from './styles.module.css';

const { getNavMenu } = REDUX_API.GLOBAL_REDUX;

export const TopSideBar = () => {
    const dispatch = useAppDispatch();
    const { currentUserGroups } = useAppSelector(state => state.Global.getCurrentSessionSettingsReducer.settings.currentContextInfo);
    const { data } = useAppSelector(state => state.Global.getNavMenuReducer);
    const { topSideMenu } = data?.rawData || { topSideMenu: null, leftSideMenu: [], isOpenNavMobile: false };

    const [isOpen, setIsOpen] = useState(false);

    useEffect(() => {
        void getNavMenu(dispatch);
    }, [dispatch]);

    const onCloseDialogHandler = () => {
        setIsOpen(false);
    };

    const onOpenDialogHandler = () => {
        setIsOpen(true);
    };

    return (
        <>
            <StartTourDialog isOpen={ isOpen } onCancelClickHandler={ onCloseDialogHandler } />
            <div className={ css['top-side-bar'] } id="top-side-bar">
                <ExitFromDifferentContextButton />
                <ul className={ css['top-side-bar__menu'] }>
                    <NotificationCenter />
                    { !currentUserGroups.includes(AccountUserGroup.AccountUser) &&
                        <i className={ `fa fa-question-circle-o ${ css.tour }` } data-tourid={ ETourId.onboarding } aria-hidden="true" onClick={ onOpenDialogHandler } />
                    }
                    { topSideMenu?.balanceMenu &&
                        <MenuItemView
                            key={ `top_menu_${ topSideMenu.balanceMenu.startMenu.caption }` }
                            href={ topSideMenu.balanceMenu.startMenu.href }
                            faIcon="fa-money"
                            className={ css.icon }
                        >
                            { topSideMenu.balanceMenu.startMenu.caption }
                        </MenuItemView>
                    }
                    { topSideMenu?.greetingsMenu &&
                        <MenuItemView
                            key={ `top_menu_${ topSideMenu.greetingsMenu.startMenu.caption }` }
                            href={ topSideMenu.greetingsMenu.startMenu.href }
                            faIcon="fa-user"
                            className={ css.icon }
                        >
                            { topSideMenu.greetingsMenu.startMenu.caption }
                        </MenuItemView>
                    }
                    { topSideMenu?.exitMenu &&
                        <MenuItemView
                            isDefaultHref={ true }
                            key={ `top_menu_${ topSideMenu.exitMenu.startMenu.caption }` }
                            href={ AppRoutes.signInRoute }
                            onClick={ true }
                            faIcon={ topSideMenu.exitMenu.startMenu.faIcon }
                            className={ css.icon }
                        >
                            &nbsp;{ topSideMenu.exitMenu.startMenu.caption }
                        </MenuItemView>
                    }
                </ul>
            </div>
        </>
    );
};