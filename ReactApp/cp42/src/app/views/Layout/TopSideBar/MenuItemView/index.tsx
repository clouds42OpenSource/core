import { useIsMobile } from 'app/hooks';
import cn from 'classnames';
import { ReactNode } from 'react';
import { Link } from 'react-router-dom';
import { FETCH_API } from 'app/api/useFetchApi';
import { ETourId } from 'app/views/Layout/ProjectTour/enums';

import css from './styles.module.css';

type TProps = {
    href: string;
    faIcon?: string;
    className?: string;
    children: ReactNode;
    isDefaultHref?: boolean;
    onClick?: boolean;
};

const { postLogout } = FETCH_API.AUTH;

export const MenuItemView = ({ href, faIcon, className, children, isDefaultHref, onClick }: TProps) => {
    const isMobile = useIsMobile();

    return (
        <li className={ cn({ [css['top-side-bar__menu_item']]: !isMobile }) } data-tourid={ href.includes(ETourId.profile) ? ETourId.profile : undefined }>
            { isDefaultHref
                ? (
                    onClick
                        ? (
                            <div
                                onClick={ () => postLogout() }
                                role="presentation"
                                className={ cn(css['top-side-bar__menu_item--link'], className) }
                            >
                                <a className={ cn(css['top-side-bar__menu_item--link'], className) }>
                                    { faIcon && isMobile && <i className={ `fa ${ faIcon }` } /> }
                                    { (!faIcon || !isMobile) && children }
                                </a>
                            </div>
                        )
                        : (
                            <a href={ href } className={ cn(css['top-side-bar__menu_item--link'], className) }>
                                { faIcon && isMobile && <i className={ `fa ${ faIcon }` } /> }
                                { (!faIcon || !isMobile) && children }
                            </a>
                        )
                )
                : (
                    <Link to={ href } className={ cn(css['top-side-bar__menu_item--link'], className) }>
                        { faIcon && isMobile && <i className={ `fa ${ faIcon }` } /> }
                        { (!faIcon || !isMobile) && children }
                    </Link>
                )
            }
        </li>
    );
};