import { RentType } from 'app/api/endpoints/profile/response';
import { FETCH_API } from 'app/api/useFetchApi';
import { useEffect } from 'react';
import { useLocation } from 'react-router';

declare global {
    interface Window {
        TalkMe: (action: string, params: object) => void;
        supportAPIMethod: string;
    }
}

const retnTypeToText = (type: RentType) => {
    switch (type) {
        case RentType.None:
            return 'Без аренды';
        case RentType.Standart:
            return 'Стандарт';
        case RentType.Web:
            return 'Web';
        default:
            return 'Без аренды';
    }
};

export const TalkMe = () => {
    const location = useLocation();

    const userInfo = FETCH_API.PROFILE.useGetProfile();

    useEffect(() => {
        (() => {
            const c = async (d: Document, w: Window, m: string, i?: boolean) => {
                window.supportAPIMethod = m;
                const s = d.createElement('script');
                s.id = 'supportScript';
                const id = '79545b749a2d2d92cdc0864ab239e67c';
                s.src = `${ !i ? 'https://lcab.talk-me.ru/support/support.js' : 'https://static.site-chat.me/support/support.int.js' }?h=${ id }`;
                s.onerror = i ? () => { /* empty */ } : () => {
                    c(d, w, m, true);
                };

                // @ts-ignore
                w[m] ??= (...args: any[]) => {
                    // @ts-ignore
                    (w[m].q ??= []).push(args);
                };

                // @ts-ignore
                const TalkMeWidget = w[m];

                if (userInfo.data?.rawData) {
                    TalkMeWidget('setClientInfo', {
                        name: `${ userInfo.data.rawData.lastName } ${ userInfo.data.rawData.firstName } ${ userInfo.data.rawData.middleName }`,
                        custom: {
                            'Логин': userInfo.data.rawData.login,
                            'Номер телефона': userInfo.data.rawData.phoneNumber,
                            'Тариф': retnTypeToText(userInfo.data.rawData.rentType),
                            'Статус оплаты': '',
                            'Данные о "Мой диск"': `Занято ${ userInfo.data.rawData.myDiskInfo.myDiskVolumeInfo.usedSizeOnDisk } из ${ userInfo.data.rawData.myDiskInfo.myDiskVolumeInfo.availableSize }`,
                            'Номер аккаунта': userInfo.data.rawData.accountIndexNumber,
                            'Статус ВИП': userInfo.data.rawData.myDiskInfo.accountIsVip ? 'ВИП пользователь' : 'Пользователь'
                        }
                    });
                }

                (d.head ? d.head : d.body).appendChild(s);
            };

            void c(document, window, 'TalkMe');
        })();
    }, [userInfo.data?.rawData]);

    useEffect(() => {
        const TalkMeWidget = window.TalkMe;

        TalkMeWidget('setClientInfo', {
            custom: {
                'Страница сайта': `${ window.location.origin }${ location.pathname }`
            }
        });
    }, [location]);

    return null;
};