import { NavMenuItemInfo } from 'app/api/endpoints/global/response/getNavMenu';
import { localStorageHelper } from 'app/common/helpers/LocalStorageHelper';
import { getNavMenuSlice } from 'app/modules/global/reducers/getNavMenuSlice';
import { AppReduxStoreState } from 'app/redux/types';
import { LeftSideBarView } from 'app/views/Layout/LeftSideBar/views/LeftSideBarView';
import Cookies from 'js-cookie';
import React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps, withRouter } from 'react-router-dom';

type StateProps = {
    hasMenuReceived: boolean;
    hasCredentialsReceived: boolean;
    leftSideMenu?: NavMenuItemInfo[];
};

type OwnState = {
    activeMenuItem?: string
};

type DispatchProps = {
    changeNavMenu: (navMenu: NavMenuItemInfo[]) => void;
};

type TProps = {
    isOpen: boolean;
    close: (status: boolean) => void;
};

type AllProps = StateProps & DispatchProps & RouteComponentProps & TProps;

class LeftSideBarClass extends React.Component<AllProps, OwnState> {
    constructor(props: AllProps) {
        super(props);
        this.state = {
            activeMenuItem: ''
        };
        this.onExpandCollapseMenu = this.onExpandCollapseMenu.bind(this);
        this.handleChangeRoute = this.handleChangeRoute.bind(this);
    }

    private handleChangeRoute(url?: string) {
        this.setState({ activeMenuItem: url });
    }

    private onExpandCollapseMenu(menuKey: string, isCurrentlyExpanded: boolean) {
        if (this.props.leftSideMenu) {
            const currentMenu = this.props.leftSideMenu.map(item => {
                if (item.startMenu.key === menuKey) {
                    this.setCookieIsExpanded(item.categoryKey, !isCurrentlyExpanded);
                    return { ...item, startMenu: { ...item.startMenu, isExpanded: !isCurrentlyExpanded } };
                }

                return item;
            });

            this.props.changeNavMenu(currentMenu);
        }
    }

    private setCookieIsExpanded(menuCategory: string, isExpanded: boolean) {
        Cookies.set(menuCategory, isExpanded ? 'true' : 'false', { expires: 365 });
    }

    public render() {
        if (this.props.hasMenuReceived && this.props.leftSideMenu) {
            return (
                <LeftSideBarView
                    isOpen={ this.props.isOpen }
                    close={ this.props.close }
                    hasTokenReceived={ this.props.hasCredentialsReceived }
                    MenuArray={ this.props.leftSideMenu }
                    activeMenuItem={ this.state.activeMenuItem }
                    handleChangeRoute={ this.handleChangeRoute }
                    onExpandCollapseMenu={ this.onExpandCollapseMenu }
                />
            );
        }

        return null;
    }
}

export const LeftSideBar =
    connect<StateProps, DispatchProps, TProps, AppReduxStoreState>(
        state => {
            const hasMenuReceived = state.Global.getNavMenuReducer.data !== null;
            const hasCredentialsReceived = !!localStorageHelper.getJWTToken();
            const leftSideMenu = state.Global.getNavMenuReducer.data?.rawData.leftSideMenu;

            return {
                hasMenuReceived,
                hasCredentialsReceived,
                leftSideMenu
            };
        },
        dispatch => ({
            changeNavMenu: (navMenu: NavMenuItemInfo[]) => dispatch(getNavMenuSlice.actions.changeLeftMenu(navMenu))
        })
    )(withRouter(LeftSideBarClass));