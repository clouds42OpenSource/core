import { CollapsibleMenuItemView } from 'app/views/Layout/LeftSideBar/views/CollapsibleMenuItemView';
import { SimpleMenuItemView } from 'app/views/Layout/LeftSideBar/views/SimpleMenuItemView';
import { ReactNode } from 'react';

type TProps = {
    children?: ReactNode;
    menuKey: string;
    href?: string;
    caption: string;
    isExpanded?: boolean;
    faIcon?: string;
    isSubMenuItem?: boolean;
    onExpandCollapseMenu?: (menuKey: string, isCurrentlyExpanded: boolean) => void;
    activeMenuItem?: string;
    handleChangeRoute?: (url?: string) => void;
}

export const MenuItemView = ({ caption, faIcon, isExpanded, menuKey, onExpandCollapseMenu, children, activeMenuItem, isSubMenuItem, href, handleChangeRoute }: TProps) => {
    if (children) {
        return (
            <CollapsibleMenuItemView
                caption={ caption }
                faIcon={ faIcon }
                isExpanded={ isExpanded }
                menuKey={ menuKey }
                onExpandCollapseMenu={ onExpandCollapseMenu }
            >
                { children }
            </CollapsibleMenuItemView>
        );
    }

    return (
        <SimpleMenuItemView
            caption={ caption }
            faIcon={ faIcon }
            activeMenuItem={ activeMenuItem }
            isSubMenuItem={ isSubMenuItem }
            href={ href }
            handleChangeRoute={ handleChangeRoute }
        />
    );
};