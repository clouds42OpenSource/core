import { memo, useEffect, useState } from 'react';
import cn from 'classnames';

import { FETCH_API } from 'app/api/useFetchApi';
import css from './styles.module.css';

const EFSOL = `${ import.meta.env.REACT_APP_CP_HOST ?? window.location.origin }/Content/img/efsol.svg`;
const CLOUDS = `${ import.meta.env.REACT_APP_CP_HOST ?? window.location.origin }/Content/img/42clouds.svg`;

const { getPromoPage } = FETCH_API.PROMO_PAGE;

type TProps = {
    noBlockPadding?: boolean;
};

export const LogoView = memo(({ noBlockPadding }: TProps) => {
    const [promoSrc, setPromoSrc] = useState('');

    useEffect(() => {
        let isMounted = true;

        (async () => {
            const promoPageUrl = await getPromoPage();

            if (isMounted) {
                setPromoSrc(promoPageUrl);
            }
        })();

        return () => {
            isMounted = false;
        };
    }, []);

    return (
        <div className={ cn({ [css['side-bar__header']]: !noBlockPadding, [css['side-bar__header_inline']]: noBlockPadding }) }>
            <a href="https://efsol.ru">
                <img alt="Логотип" className={ cn(css['side-bar__header-img']) } src={ EFSOL } />
            </a>
            <a href={ promoSrc }>
                <img alt="Логотип" className={ cn(css['side-bar__header-img--second']) } src={ CLOUDS } />
            </a>
        </div>
    );
});