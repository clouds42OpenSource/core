import { ETourId } from 'app/views/Layout/ProjectTour/enums';
import cn from 'classnames';
import React from 'react';
import { Link } from 'react-router-dom';
import css from 'app/views/Layout/LeftSideBar/views/SimpleMenuItemView/styles.module.css';

/**
 * Свойства меню
 */
export interface IOwnProps {
    /**
     * Текст меню
     */
    caption: string;

    /**
     * Иконка меню
     */
    faIcon?: string;

    /**
     * Если true, то данное меню является активным
     */

    /**
     * Ссылка меню
     */
    href?: string;

    /**
     * Если true, то данное меню пренадлежит подменю
     */
    isSubMenuItem?: boolean;
    handleChangeRoute?: (url?: string) => void;
    activeMenuItem?: string;
}

export class SimpleMenuItemView extends React.Component<IOwnProps> {
    constructor(props: IOwnProps) {
        super(props);
        this.changeRoute = this.changeRoute.bind(this);
    }

    public shouldComponentUpdate(nextProps: IOwnProps) {
        return nextProps.caption !== this.props.caption ||
            nextProps.faIcon !== this.props.faIcon ||
            nextProps.href !== this.props.href ||
            nextProps.isSubMenuItem !== this.props.isSubMenuItem ||
            nextProps.activeMenuItem !== this.props.activeMenuItem;
    }

    private changeRoute(url?: string) {
        if (this.props.handleChangeRoute) {
            this.props.handleChangeRoute(url);
        }
    }

    public render() {
        const { href, faIcon: icon, isSubMenuItem, caption } = this.props;
        const isLink = !/https?:\/\//.test(href || '');
        const faIcon = icon ? cn('fa', icon) : '';
        let tourId = '';

        switch ((href ?? '').slice(1)) {
            case ETourId.myDatabases:
                tourId = ETourId.myDatabases;
                break;
            case ETourId.users:
                tourId = ETourId.users;
                break;
            case ETourId.rent:
                tourId = ETourId.rent;
                break;
            case ETourId.connectionSettings:
                tourId = ETourId.connectionSettings;
                break;
            case 'billing':
                tourId = ETourId.balance;
                break;
            default:
        }

        return (
            <li className={ cn(css['side-bar__simple-menu_container']) } data-tourid={ tourId }>
                { isLink
                    ? (
                        <Link
                            onClick={ () => this.changeRoute(href) }
                            to={ href || '' }
                            className={ cn({
                                [css['side-bar__simple-menu-link']]: !isSubMenuItem,
                                [css['side-bar__simple-sub-menu-link']]: isSubMenuItem,
                                [css['side-bar__simple-menu--active']]: href === window.location.pathname,
                            }) }
                        >
                            <i className={ cn(css['side-bar__simple-menu-icon'], faIcon) } />
                            { this.props.caption }
                        </Link>
                    ) : (
                        <a
                            href={ href || '' }
                            className={ cn({
                                [css['side-bar__simple-menu-link']]: !isSubMenuItem,
                                [css['side-bar__simple-sub-menu-link']]: isSubMenuItem,
                                [css['side-bar__simple-menu--active']]: href === window.location.pathname,
                            }) }
                        >
                            <i className={ cn(css['side-bar__simple-menu-icon'], faIcon) } />
                            { caption }
                        </a>
                    ) }
            </li>
        );
    }
}