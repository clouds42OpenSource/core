import { Collapse } from '@mui/material';
import { TourProps, withTour } from '@reactour/tour';
import { ETourId } from 'app/views/Layout/ProjectTour/enums';
import cn from 'classnames';
import React from 'react';
import css from 'app/views/Layout/LeftSideBar/views/CollapsibleMenuItemView/styles.module.css';

/**
 * Свойства меню
 */
type TProps = Partial<TourProps> & {
    menuKey: string;
    caption: string;
    isExpanded?: boolean;
    faIcon?: string;
    onExpandCollapseMenu?: (menuKey: string, isCurrentlyExpanded: boolean) => void;
    children: React.ReactNode;
};

class CollapsibleMenuItemViewClass extends React.Component<TProps> {
    constructor(props: TProps) {
        super(props);
        this.showHideMenu = this.showHideMenu.bind(this);
    }

    private showHideMenu() {
        if (this.props.onExpandCollapseMenu) {
            this.props.onExpandCollapseMenu(this.props.menuKey, this.props.isExpanded ?? false);
        }
    }

    public render() {
        const expanderIconClass = cn(`fa fa-angle-${ (this.props.isExpanded ? 'down' : 'left') }`);
        const menuIcon = this.props.faIcon ? cn('fa', this.props.faIcon) : '';
        let tourId = '';
        const isTourOpen = !!this.props.isOpen;

        if (menuIcon.includes('fa-tasks')) {
            tourId = ETourId.services;
        }

        return (
            <li data-tourid={ tourId }>
                <div
                    className={ cn({
                        [css['side-bar__collapsible-menu_container']]: true,
                        [css['side-bar__collapsible-menu-link']]: true,
                        [css['side-bar__collapsible-menu--expanded']]: this.props.isExpanded || isTourOpen
                    }) }
                    onClick={ this.showHideMenu }
                >
                    <i className={ cn(css['side-bar__collapsible-menu-icon'], menuIcon) } />
                    { this.props.caption }
                    <i className={ cn(css['side-bar__collapsible-menu-expander'], expanderIconClass) } />
                </div>
                <Collapse
                    className={ cn({
                        [css['side-bar__collapsible-menu-item--open']]: this.props.isExpanded || isTourOpen
                    }) }
                    in={ this.props.isExpanded || isTourOpen }
                >
                    <ul className={ cn(css['side-bar__collapsible-menu']) }>
                        { this.props.children }
                    </ul>
                </Collapse>
            </li>
        );
    }
}

export const CollapsibleMenuItemView = withTour(CollapsibleMenuItemViewClass);