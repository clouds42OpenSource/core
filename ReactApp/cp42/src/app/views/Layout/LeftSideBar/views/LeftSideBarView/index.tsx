import { ClickAwayListener, Drawer } from '@mui/material';
import { AppConsts } from 'app/common/constants';
import { useIsMobile } from 'app/hooks';
import { COLORS } from 'app/utils';
import { LogoView } from 'app/views/Layout/LeftSideBar/views/LogoView';
import { MenuItemView } from 'app/views/Layout/LeftSideBar/views/MenuItemView';
import MenuIcon from '@mui/icons-material/Menu';
import { NavMenuItem, NavMenuItemInfo } from 'app/api/endpoints/global/response/getNavMenu';
import React from 'react';
import css from './styles.module.css';

type TProps = {
    MenuArray: NavMenuItemInfo[];
    onExpandCollapseMenu: (menuKey: string, isCurrentlyExpanded: boolean) => void;
    activeMenuItem?: string;
    handleChangeRoute: () => void;
    hasTokenReceived?: boolean;
    isOpen: boolean;
    close: (status: boolean) => void;
};

type TSigneMenuProps = {
    menu: NavMenuItem;
    isSubMenuItem?: boolean;
    activeMenuItem?: string;
    handleChangeRoute: () => void;
};

type TGroupMenuProps = {
    menu: NavMenuItemInfo,
    activeMenuItem?: string,
    onExpandCollapseMenu: (menuKey: string, isCurrentlyExpanded: boolean) => void,
    handleChangeRoute: () => void,
    key?: string
};

const SingleMenu = ({ menu, activeMenuItem, handleChangeRoute, isSubMenuItem = false }: TSigneMenuProps) => {
    return (
        <MenuItemView
            menuKey={ menu.key }
            href={ menu.href }
            key={ menu.caption }
            caption={ menu.caption }
            faIcon={ menu.faIcon }
            handleChangeRoute={ handleChangeRoute }
            activeMenuItem={ activeMenuItem }
            isSubMenuItem={ isSubMenuItem }
        />
    );
};

const GroupMenu = ({ menu, activeMenuItem, onExpandCollapseMenu, handleChangeRoute, key }: TGroupMenuProps) => {
    return (
        <MenuItemView
            menuKey={ menu.startMenu.key }
            key={ menu.startMenu.caption }
            isExpanded={ menu.startMenu.isExpanded }
            caption={ menu.startMenu.caption }
            faIcon={ menu.startMenu.faIcon }
            activeMenuItem={ activeMenuItem }
            onExpandCollapseMenu={ onExpandCollapseMenu }
        >
            { menu.children.map(subMenu => (
                <SingleMenu key={ subMenu.key } handleChangeRoute={ handleChangeRoute } activeMenuItem={ activeMenuItem } isSubMenuItem={ true } menu={ subMenu } />
            )) }
        </MenuItemView>
    );
};

export const LeftSideBarView = ({ MenuArray, onExpandCollapseMenu, activeMenuItem, hasTokenReceived, handleChangeRoute, isOpen, close }: TProps) => {
    const isMobile = useIsMobile();

    return (
        <>
            <MenuIcon onClick={ () => close(!isOpen) } sx={ { cursor: 'pointer', position: 'absolute', zIndex: '100', left: isOpen ? AppConsts.desktopLeftMenuWidth + 10 : '10px', top: '10px' } } />
            <Drawer
                variant={ isMobile ? 'temporary' : 'persistent' }
                open={ isOpen }
                onClose={ isMobile ? () => close(false) : undefined }
                ModalProps={ {
                    keepMounted: true,
                } }
                sx={ {
                    '& .MuiDrawer-paper': {
                        width: AppConsts.desktopLeftMenuWidth,
                        height: '100vh',
                        backgroundColor: COLORS.leftSideBar,
                        color: COLORS.fontLight,
                    }
                } }
            >
                { hasTokenReceived && <LogoView /> }
                <ul className={ css['side-bar__menu'] }>
                    { MenuArray.map((menu, i) => menu.children?.length
                        ? (
                            <GroupMenu
                                key={ `${ menu.categoryKey }-${ i }` }
                                menu={ menu }
                                onExpandCollapseMenu={ onExpandCollapseMenu }
                                handleChangeRoute={ handleChangeRoute }
                                activeMenuItem={ activeMenuItem }
                            />
                        )
                        : (
                            <SingleMenu
                                key={ `${ menu.categoryKey }-${ i }` }
                                menu={ menu.startMenu }
                                handleChangeRoute={ handleChangeRoute }
                                activeMenuItem={ activeMenuItem }
                            />
                        )
                    ) }
                </ul>
            </Drawer>
        </>
    );
};