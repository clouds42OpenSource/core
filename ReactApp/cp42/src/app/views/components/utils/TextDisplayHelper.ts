import { Nullable } from 'app/common/types';

/**
 * Отобразить значение или
 * значение по умолчанию если значения нет
 * @param value значение
 * @param defaultValue значение по умолчанию
 */
export function displayValueOrDefault<T>(value: Nullable<T>, defaultValue: T): T {
    return value || defaultValue;
}