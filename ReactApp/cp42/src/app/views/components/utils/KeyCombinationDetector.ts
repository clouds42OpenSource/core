import React from 'react';

/**
 * Перечисление кодов клавиш
 */
export enum KeyCodeTypeEnum {
    /**
     * Клавиша V
     */
    VKey = 86,

    /**
     * Клавиша C
     */
    CKey = 67,

    /**
     * Клавиша X
     */
    XKey = 88,

    /**
     * Клавиша A
     */
    AKey = 65,

    /**
     * Клавиша Z
     */
    ZKey = 90,

    /**
     * Клавиша Ctrl
     */
    CtrlKey = 17
}

/**
 * Утилита для определения нажатия комбинации клавиш
 */
export const KeyCombinationDetector = (() => {
    return {
        /**
         * Признак что нажата любая комбинация быстрых действий Windows
         * @param event событие
         */
        isAnyWindowsShortcutsPressed: (event: React.KeyboardEvent<HTMLInputElement>):boolean => {
            return KeyCombinationDetector.ctrlCPressed(event) ||
                KeyCombinationDetector.ctrlVPressed(event) ||
                KeyCombinationDetector.ctrlXPressed(event) ||
                KeyCombinationDetector.ctrlAPressed(event) ||
                KeyCombinationDetector.ctrlZPressed(event);
        },

        /**
         * Признак что нажата комбинация ctrl+c
         * @param event событие
         */
        ctrlCPressed: (event: React.KeyboardEvent<HTMLInputElement>):boolean => {
            return KeyCombinationDetector.isPressedCtrlPlus(event, KeyCodeTypeEnum.CKey);
        },

        /**
         * Признак что нажата комбинация ctrl+v
         * @param event событие
         */
        ctrlVPressed: (event: React.KeyboardEvent<HTMLInputElement>):boolean => {
            return KeyCombinationDetector.isPressedCtrlPlus(event, KeyCodeTypeEnum.VKey);
        },

        /**
         * Признак что нажата комбинация ctrl+x
         * @param event событие
         */
        ctrlXPressed: (event: React.KeyboardEvent<HTMLInputElement>):boolean => {
            return KeyCombinationDetector.isPressedCtrlPlus(event, KeyCodeTypeEnum.XKey);
        },

        /**
         * Признак что нажата комбинация ctrl+a
         * @param event событие
         */
        ctrlAPressed: (event: React.KeyboardEvent<HTMLInputElement>):boolean => {
            return KeyCombinationDetector.isPressedCtrlPlus(event, KeyCodeTypeEnum.AKey);
        },

        /**
         * Признак что нажата комбинация ctrl+z
         * @param event событие
         */
        ctrlZPressed: (event: React.KeyboardEvent<HTMLInputElement>):boolean => {
            return KeyCombinationDetector.isPressedCtrlPlus(event, KeyCodeTypeEnum.ZKey);
        },

        /**
         * Признак что нажата комбинация ctrl + KeyCodeTypeEnum
         * @param event событие
         * @param keyCodeType тип клавиши
         */
        isPressedCtrlPlus: (event: React.KeyboardEvent<HTMLInputElement>, keyCodeType: KeyCodeTypeEnum):boolean => {
            const keyCode = KeyCombinationDetector.getPressedKeyCode(event);
            return keyCode === keyCodeType && KeyCombinationDetector.isCtrlKey(event);
        },

        /**
         * Получить код нажатой клавиши
         * @param event событие
         */
        getPressedKeyCode: (event: React.KeyboardEvent<HTMLInputElement>):number => {
            return event.which || event.keyCode;
        },

        /**
         * Признак что нажата клавиша Ctrl
         * @param event событие
         */
        isCtrlKey: (event:React.KeyboardEvent<HTMLInputElement>):boolean => {
            const keyCode = KeyCombinationDetector.getPressedKeyCode(event);
            return event.ctrlKey ? event.ctrlKey : ((keyCode === KeyCodeTypeEnum.CtrlKey));
        }
    };
})();