export function swapVariables(a: number, b: number) {
    b = a + (a = b) - b;
    return [a, b];
}

export function generateTextFromTextBoxFor(
    textboxValue: string,
    selectionStart: number,
    selectionEnd: number,
    commandOnSelection: 'backspace_8' | 'delete_46' | undefined,
    textToInsert: string): string {
    if (selectionStart > selectionEnd) {
        [selectionStart, selectionEnd] = swapVariables(selectionStart, selectionEnd);
    }

    const leftActualPart = textboxValue
        ? textboxValue.substring(0, selectionStart)
        : '';

    const rightActualPart = textboxValue
        ? textboxValue.substring(selectionEnd)
        : '';

    if (commandOnSelection) {
        if (!textboxValue) {
            return textboxValue;
        }

        if (selectionStart === selectionEnd) {
            if (selectionStart === 0 && commandOnSelection === 'delete_46') {
                return textboxValue.substring(1);
            }

            if (selectionEnd === textboxValue.length && commandOnSelection === 'backspace_8') {
                return textboxValue.slice(0, -1);
            }

            const isBackspace = commandOnSelection === 'backspace_8';

            if (isBackspace) {
                return leftActualPart
                    ? `${ leftActualPart.slice(0, -1) }${ rightActualPart }`
                    : textboxValue;
            }

            return rightActualPart
                ? `${ leftActualPart }${ rightActualPart.substring(1) }`
                : textboxValue;
        }
        return `${ textboxValue.slice(0, selectionStart) }${ textboxValue.slice(selectionEnd) }`;
    }
    return textboxValue
        ? `${ leftActualPart }${ textToInsert }${ rightActualPart }`
        : textToInsert;
}