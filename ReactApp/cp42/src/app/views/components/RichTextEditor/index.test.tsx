import { render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { RichTextEditor } from 'app/views/components/RichTextEditor';
import userEvent from '@testing-library/user-event';

describe('<RichTextEditor />', () => {
    let container: HTMLElement;
    const onValueChangeMock: jest.Mock = jest.fn();

    beforeEach(() => {
        container = render(<RichTextEditor
            value="test_value"
            formName="test_form_name"
            heightPercent="30%"
            toolBarHeight={ 100 }
            autoFocus={ false }
            language="ru"
            toolbarButtonList={ [] }
            onValueChange={ onValueChangeMock }
        />).container;
    });

    it('renders', () => {
        const divElement: HTMLDivElement | null = container.querySelector('.sun-editor');
        expect(divElement!).toBeInTheDocument();
    });

    it('has onValueChange method', async () => {
        const textAreaElement: HTMLTextAreaElement | null = container.querySelector('.se-wrapper-code');
        await userEvent.click(textAreaElement!);
        await userEvent.type(textAreaElement!, 'new_text');
        expect(onValueChangeMock).toBeCalled();
    });
});