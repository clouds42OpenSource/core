import { ValueChangeEvent } from 'app/views/components/controls/forms/_types';
import katex from 'katex';
import 'katex/dist/katex.min.css';
import { Component } from 'react';
import SunEditor, { SunEditorReactProps } from 'suneditor-react';
import 'suneditor/src/assets/css/suneditor.css';
import { ButtonsToolbarListType, LanguageType } from './types';
import styles from './style.module.css';
/**
 * Свойства текст редактора
 */

type RichTextEditorProps = {
    /**
     * Дефолтный контент
     */
    value: string;

    /**
     * Имя формы
     */
    formName: string;

    /**
     * Высота редактора полностью в процентах
     */
    heightPercent: string;

    /**
     * Высота Tool Bar менюшки с кнопками
     */
    toolBarHeight: number;

    /**
     * Авто-фокус после загрузки редактора
     */
    autoFocus: boolean;

    /**
     * Язык интерфеса
     */
    language: LanguageType;

    /**
     * Коллекция кнопок для ToolBar
     */
    toolbarButtonList: ButtonsToolbarListType;

    /**
     * Отключение возможности редактирования
     */
    disable?: boolean;

    /**
     * Отображение коллекции кнопок для Toolbar
     */
    showToolbar?: boolean;

    /**
     * Отображение тегов
     */
    showPathLabel?: boolean;

    /**
     * Стиль по умолчанию для текста редактора
     */
    setDefaultStyle?: string;
    onPaste?: (e: ClipboardEvent, html: string) => string;

    /**
     * Возникает при выборе нового элемента из списка combobox
     * @param formName название формы, которое изменило значение
     * @param newValue новое значение
     * @param prevValue предыдущее значение
     */
    onValueChange?: ValueChangeEvent<string>;
    onImageUploadBefore?: (files: File[], info: object, uploadHandler: Function) => void;
    setOptions?: SunEditorReactProps['setOptions'];
};

export class RichTextEditor extends Component<RichTextEditorProps> {
    public constructor(props: RichTextEditorProps) {
        super(props);

        this.onChange = this.onChange.bind(this);
    }

    private onChange(content: string): boolean | void {
        const newValue = content;
        const oldValue = this.props.value;
        const { formName } = this.props;

        if (this.props.onValueChange) {
            return this.props.onValueChange(formName, newValue, oldValue);
        }

        return true;
    }

    public render() {
        return (
            <div className={ styles.wrapper }>
                <SunEditor
                    onImageUploadBefore={ this.props.onImageUploadBefore }
                    setDefaultStyle={ this.props.setDefaultStyle }
                    showToolbar={ this.props.showToolbar }
                    data-testid="rich-text-editor"
                    disable={ this.props.disable }
                    height={ this.props.heightPercent }
                    setContents={ this.props.value }
                    lang={ this.props.language }
                    onChange={ this.onChange }
                    onPaste={ this.props.onPaste }
                    autoFocus={ this.props.autoFocus }
                    setOptions={ {
                        ...this.props.setOptions,
                        height: this.props.toolBarHeight,
                        buttonList: this.props.toolbarButtonList,
                        showPathLabel: this.props.showPathLabel,
                        formats: ['h2', 'h3', 'h4', 'p'],
                        katex: {
                            src: katex,
                            output: 'html',
                        },
                    } }
                />
            </div>
        );
    }
}