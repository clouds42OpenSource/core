import { ButtonsToolbarType } from './ButtonsToolbarType';

/**
 * Коллекция кнопок для ToolBar
 */
export type ButtonsToolbarListType = Array<Array<ButtonsToolbarType> | ButtonsToolbarType>;