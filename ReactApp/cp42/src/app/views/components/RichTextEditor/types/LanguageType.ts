/**
 * Тип языка
 */
export type LanguageType = 'ru' | 'ua' | 'en';