import { ERefreshId } from 'app/views/Layout/ProjectTour/enums';
import React from 'react';
import cn from 'classnames';

import { hasComponentChangesFor } from 'app/common/functions';

import css from '../styles.module.css';

export type TextFontWeight = 400 | 500 | 600 | 700;
export type TextFontSize = 10 | 11 | 12 | 13 | 14 | 16 | 18 | 20 | 26 | 32;

export type TextHorizontalAlign = 'left' | 'center' | 'right';

export type TextProps = {
    inheritColor?: boolean;
    fontSize?: TextFontSize;
    fontWeight?: TextFontWeight;
    inDiv?: boolean;
    textAlign?: TextHorizontalAlign;
    className?: string;
    children?: React.ReactNode;
    style?: React.CSSProperties;
    onClick?: () => void;
    refreshId?: ERefreshId;
    dangerouslySetInnerHTML?: { __html: string | TrustedHTML } | undefined
};

export class TextView extends React.Component<TextProps> {
    public shouldComponentUpdate(nextProps: TextProps) {
        return hasComponentChangesFor(this.props, nextProps, {
            compareAsRef: ['children']
        });
    }

    public render() {
        const style: React.CSSProperties = {
            fontSize: `${ this.props.fontSize ?? 13 }px`,
            fontWeight: this.props.fontWeight ?? 400,
            textAlign: this.props.textAlign ?? 'left'
        };

        if (this.props.inheritColor === true) {
            style.color = 'inherit';
        }

        if (this.props.inDiv) {
            return (
                <div
                    data-refreshid={ this.props.refreshId }
                    onClick={ this.props.onClick }
                    data-testid="text-view__div"
                    className={ cn(css.text, this.props.className) }
                    style={ { ...style, ...this.props.style } }
                    dangerouslySetInnerHTML={ this.props.dangerouslySetInnerHTML }
                >
                    { this.props.children }
                </div>
            );
        }

        return (
            <span
                data-refreshid={ this.props.refreshId }
                onClick={ this.props.onClick }
                data-testid="text-view__span"
                className={ cn(css.text, this.props.className) }
                style={ { ...style, ...this.props.style } }
                dangerouslySetInnerHTML={ this.props.dangerouslySetInnerHTML }
            >
                { this.props.children }
            </span>
        );
    }
}