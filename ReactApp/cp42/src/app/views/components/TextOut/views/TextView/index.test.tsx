import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { TextView } from 'app/views/components/TextOut/views/TextView';

describe('<TextView />', () => {
    it('renders as div', () => {
        render(<TextView inDiv={ true }>test_text</TextView>);
        expect(screen.getByTestId(/text-view__div/i)).toBeInTheDocument();
        expect(screen.getByText(/test_text/i)).toBeInTheDocument();
    });

    it('renders as span', () => {
        render(<TextView inDiv={ false }>test_text</TextView>);
        expect(screen.getByTestId(/text-view__span/i)).toBeInTheDocument();
        expect(screen.getByText(/test_text/i)).toBeInTheDocument();
    });
});