import React from 'react';

import { TextProps, TextView } from './views/TextView';

export const TextOut = (props: TextProps & { children?: React.ReactNode }) => {
    return <TextView { ...props } />;
};