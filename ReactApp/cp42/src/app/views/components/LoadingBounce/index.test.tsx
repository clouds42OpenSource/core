import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { LoadingBounce } from 'app/views/components/LoadingBounce';

describe('<LoadingBounce />', () => {
    it('renders', () => {
        render(<LoadingBounce />);
        expect(screen.getByTestId(/loading-bounce/i)).toBeInTheDocument();
    });
});