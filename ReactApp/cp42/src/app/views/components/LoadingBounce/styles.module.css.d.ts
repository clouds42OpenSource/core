declare const styles: {
  readonly 'shadow': string;
  readonly 'backdrop__shadow': string;
  readonly 'sk-spinner-double-bounce': string;
  readonly 'sk-spinner': string;
  readonly 'sk-double-bounce1': string;
  readonly 'sk-double-bounce2': string;
  readonly 'sk-doubleBounce': string;
};
export = styles;