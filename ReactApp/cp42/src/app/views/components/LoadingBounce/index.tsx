import { Backdrop } from '@mui/material';
import { AppConsts } from 'app/common/constants';
import withWindowDimensions, { IWithWindowDimensionsProps } from 'app/views/components/_hoc/withWindowDimensions';
import cn from 'classnames';
import React from 'react';
import css from './styles.module.css';

interface IOwnProps extends IWithWindowDimensionsProps {
    noMargin?: boolean;
}

type OwnState = {
    leftPosition: number;
};

export class LoadingBounceClass extends React.Component<IOwnProps, OwnState> {
    public constructor(props: IOwnProps) {
        super(props);
        this.getLeftPosition = this.getLeftPosition.bind(this);
        this.state = {
            leftPosition: this.getLeftPosition()
        };
    }

    public shouldComponentUpdate(nextProps: IOwnProps) {
        return nextProps.windowDimensions.isMobileSized !== this.props.windowDimensions.isMobileSized ||
            this.state.leftPosition !== this.getLeftPosition();
    }

    public componentDidUpdate(_prevProps: IOwnProps, _prevState: OwnState) {
        const currentLeftPosition = this.getLeftPosition();
        if (this.state.leftPosition !== currentLeftPosition) {
            this.setState({
                leftPosition: currentLeftPosition
            });
        }
    }

    private getLeftPosition() {
        const hasModals = document.querySelector('.MuiDialog-container');
        return hasModals
            ? 0
            : this.props.windowDimensions.isMobileSized
                ? AppConsts.mobileLeftMenuWidth
                : AppConsts.desktopLeftMenuWidth;
    }

    public render() {
        const marginLeftContentContainer = this.props.noMargin ? 0 : `${ this.state.leftPosition }px`;

        return (
            <>
                <div
                    data-testid="loading-bounce"
                    className={ cn(css.shadow) }
                    style={ {
                        marginLeft: marginLeftContentContainer,
                    } }
                />
                <Backdrop
                    open={ true }
                    className={ cn(css.backdrop__shadow) }
                >
                    <div className={ cn(css['sk-spinner'], css['sk-spinner-double-bounce']) }>
                        <div className={ cn(css['sk-double-bounce1']) } />
                        <div className={ cn(css['sk-double-bounce2']) } />
                    </div>
                </Backdrop>
            </>
        );
    }
}

export const LoadingBounce = withWindowDimensions(LoadingBounceClass);