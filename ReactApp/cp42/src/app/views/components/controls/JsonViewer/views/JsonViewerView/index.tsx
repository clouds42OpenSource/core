import { JsonViewerProps } from 'app/views/components/controls/JsonViewer/views/JsonViewerView/types';
import { TextOut } from 'app/views/components/TextOut';
import React from 'react';
import ReactJson from 'react-json-view';

/**
 * Представление, описывающее утилиту отображения JSON
 */
export class JsonViewerView extends React.Component<JsonViewerProps> {
    constructor(props: JsonViewerProps) {
        super(props);
        this.getJsonObjectFromProps = this.getJsonObjectFromProps.bind(this);
    }

    /**
     * Получить JSON объект из свойств
     */
    private getJsonObjectFromProps(): object | null {
        if (typeof this.props.jsonData === 'object') return this.props.jsonData;
        try {
            return JSON.parse(this.props.jsonData);
        } catch (er) {
            return null;
        }
    }

    public render() {
        const jsonObj = this.getJsonObjectFromProps();

        return (
            <div>
                {
                    jsonObj !== null
                        ? (
                            <ReactJson
                                data-testid="json-viewer-view"
                                src={ jsonObj }
                                displayDataTypes={ this.props.displayDataTypes }
                                displayObjectSize={ this.props.displayObjectSize }
                                enableClipboard={ this.props.enableClipboard ?? false }
                            />
                        )
                        : (
                            <TextOut inDiv={ true } fontSize={ 13 }>
                                { this.props.jsonData }
                            </TextOut>
                        )
                }
            </div>
        );
    }
}