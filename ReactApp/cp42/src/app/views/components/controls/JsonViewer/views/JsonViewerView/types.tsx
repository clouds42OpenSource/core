/**
 * Свойства компонента JsonViewer
 */
export type JsonViewerProps = {
    /**
     * Данные (сторка json либо объект)
     */
    jsonData: string | object,

    /**
     * Признак необходимости отображать типы данных
     */
    displayDataTypes?: boolean,

    /**
     * Признак необходимости отображать размер объектов
     */
    displayObjectSize?: boolean,

    /**
     * Признак необходимости включать возможность скопировать часть элементов
     */
    enableClipboard?: boolean
};