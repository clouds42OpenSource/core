import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { JsonViewerView } from 'app/views/components/controls/JsonViewer/views/JsonViewerView';

describe('<JsonViewerView />', () => {
    const templateJsonObject = {
        test_key: 'test_value',
        another_test_key: 'another_test_value',
    };

    it('renders as json viewer with string param', () => {
        const { container } = render(<JsonViewerView jsonData={ JSON.stringify(templateJsonObject) } />);
        expect(container.getElementsByClassName('react-json-view').length).toBe(1);
    });

    it('renders as json viewer with object param', () => {
        const { container } = render(<JsonViewerView jsonData={ templateJsonObject } />);
        expect(container.getElementsByClassName('react-json-view').length).toBe(1);
    });

    it('renders as simple text with string param', () => {
        render(<JsonViewerView jsonData="test_string" />);
        expect(screen.getByTestId(/text-view__div/i)).toBeInTheDocument();
    });
});