import React from 'react';
import { JsonViewerView } from 'app/views/components/controls/JsonViewer/views/JsonViewerView';
import { JsonViewerProps } from 'app/views/components/controls/JsonViewer/views/JsonViewerView/types';

/**
 * Вьювер для данных в JSON
 * @param props свойства
 */
export const JsonViewer = (props: JsonViewerProps & { children?: React.ReactNode }) => {
    return <JsonViewerView { ...props } />;
};