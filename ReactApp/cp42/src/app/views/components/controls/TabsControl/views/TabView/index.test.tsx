import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { TabView } from 'app/views/components/controls/TabsControl/views/TabView';

describe('<TabView />', () => {
    it('renders with title as default ', () => {
        render(<TabView headers={ [{ title: 'test_title', isVisible: true }] } />);
        expect(screen.getByText(/test_title/i)).toBeInTheDocument();
        expect(screen.getByTestId(/tab-view__default-tabs__content/i)).toBeInTheDocument();
    });

    it('renders without title as route', () => {
        render(<TabView headers={ [{ title: 'test_title', isVisible: true, path: 'test_path' }] } />);
        expect(screen.getByTestId(/tab-view__router-tabs__content/i)).toBeInTheDocument();
    });

    it('renders without title as default', () => {
        render(<TabView headers={ [{ title: 'test_title', isVisible: false }] } />);
        expect(screen.queryByText(/test_title/i)).not.toBeInTheDocument();
        expect(screen.getByTestId(/tab-view__default-tabs__content/i)).toBeInTheDocument();
    });
});