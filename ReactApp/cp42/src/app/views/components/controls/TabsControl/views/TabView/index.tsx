import { Tab, Tabs } from '@mui/material';
import { SxProps } from '@mui/system';
import { hasComponentChangesFor } from 'app/common/functions';
import { COLORS } from 'app/utils';
import { EPartnersTourId, ERefreshId, ETourId } from 'app/views/Layout/ProjectTour/enums';
import cn from 'classnames';
import { Children, ReactNode, Component } from 'react';
import { Redirect, Route, Switch } from 'react-router';
import { BrowserRouter, Link } from 'react-router-dom';
import css from '../styles.module.css';

export type THeaders = {
    path?: string;
    id?: string;
    title: string;
    isVisible: boolean;
    isRefreshButtonVisible?: boolean;
    onRefreshButtonClick?: (tabIndex: number) => void;
    tourId?: ETourId | EPartnersTourId;
    contentTourId?: ETourId | EPartnersTourId;
    contentRefreshId?: ERefreshId;
}[];

export type TabProps = {
    noUnderline?: boolean;
    sxProps?: SxProps;
    headers: THeaders;
    activeTabIndex?: number;
    onActiveTabIndexChanged?: (tabIndex: number) => void;
    children?: ReactNode;
    headerContent?: ReactNode;
    headerRefreshId?: ERefreshId;
    variant?: 'scrollable' | 'standard' | 'fullWidth';
};

type OwnState = {
    activeTabIndex: number;
};

export class TabView extends Component<TabProps, OwnState> {
    public constructor(props: TabProps) {
        super(props);
        this.state = {
            activeTabIndex: this.props.activeTabIndex ? this.props.activeTabIndex : 0
        };

        this.onTabHeaderClick = this.onTabHeaderClick.bind(this);
        this.RenderTabsWithRouter = this.RenderTabsWithRouter.bind(this);
        this.RenderDefaultTabs = this.RenderDefaultTabs.bind(this);
    }

    public shouldComponentUpdate(nextProps: TabProps, nextState: OwnState) {
        return hasComponentChangesFor(this.props, nextProps) ||
            hasComponentChangesFor(this.state, nextState) ||
            this.props.children !== nextProps.children;
    }

    private onTabHeaderClick(tabIndex: number) {
        if (this.props.onActiveTabIndexChanged) {
            this.props.onActiveTabIndexChanged(tabIndex);
        }

        this.setState({
            activeTabIndex: tabIndex
        });
    }

    private RenderTabsWithRouter() {
        const children = Children.toArray(this.props.children);
        const { headerContent, headers, headerRefreshId, variant } = this.props;

        return (
            <>
                <div data-testid="tab-view__router-tabs__content" data-refreshid={ headerRefreshId }>{ headerContent }</div>
                <BrowserRouter data-testid="tab-view__router-tabs__router">
                    <div className={ css['tab-wrapper'] }>
                        <Tabs
                            value={ this.state.activeTabIndex }
                            variant={ variant ?? 'scrollable' }
                            allowScrollButtonsMobile={ true }
                            TabIndicatorProps={ { style: { background: '#337ab7' } } }
                        >
                            { headers?.map((item, headerIndex) => (
                                <Tab
                                    classes={ { selected: cn(css['tabItem-active']) } }
                                    replace={ true }
                                    key={ item.title }
                                    onClick={ () => this.onTabHeaderClick(headerIndex) }
                                    label={ item.title }
                                    component={ Link }
                                    sx={ { display: item.isVisible ? '' : 'none' } }
                                    className={ css.tabItem }
                                    to={ `${ window.location.pathname }${ headers[headerIndex].path }` }
                                />
                            )) }
                        </Tabs>
                    </div>
                    <Switch>
                        { children.map((_, index) => (
                            <Route
                                exact={ true }
                                key={ headers[index]?.id ?? `tab_${ index }` }
                                path={ `${ window.location.pathname }` }
                                render={ () =>
                                    <div
                                        className={ css['tab-content'] }
                                        data-tourid={ headers[index]?.contentTourId ?? '' }
                                        data-refreshid={ headers[index]?.contentRefreshId ?? '' }
                                    >
                                        { children[this.state.activeTabIndex] }
                                    </div>
                                }
                            />
                        )) }
                        <Redirect to={ `${ window.location.pathname }${ headers[this.state.activeTabIndex].path }` } />
                    </Switch>
                </BrowserRouter>
            </>
        );
    }

    private RenderDefaultTabs() {
        const children = Children.toArray(this.props.children);
        const { headerContent, headers, headerRefreshId, variant, sxProps, noUnderline } = this.props;

        const visibleHeaders = headers.filter(item => item.isVisible);

        return (
            <>
                <div data-testid="tab-view__default-tabs__content" data-refreshid={ headerRefreshId }>{ headerContent }</div>
                <div className={ css['tab-wrapper'] } data-testid="tab-view__default-tabs__tab-wrapper">
                    <Tabs
                        sx={ sxProps }
                        variant={ variant ?? 'scrollable' }
                        allowScrollButtonsMobile={ true }
                        value={ this.state.activeTabIndex }
                        TabIndicatorProps={ { style: { background: COLORS.main, display: noUnderline ? 'none' : 'initial' } } }
                    >
                        { visibleHeaders.map((item, headerIndex) => (
                            <Tab
                                data-tourid={ item.tourId }
                                classes={ { selected: css['tabItem-active'] } }
                                key={ item.title }
                                onClick={ () => this.onTabHeaderClick(headerIndex) }
                                label={ item.title }
                                className={ css.tabItem }
                            />
                        )) }
                    </Tabs>
                </div>
                { children.map((tab, index) => (
                    <div
                        data-tourid={ headers[index]?.contentTourId ?? '' }
                        data-refreshid={ headers[index]?.contentRefreshId ?? '' }
                        key={ headers[index]?.id ?? `tab_${ index }` }
                        style={ { display: index === this.state.activeTabIndex ? '' : 'none' } }
                        className={ cn(css['tab-content']) }
                    >
                        { tab }
                    </div>
                )) }
            </>
        );
    }

    public render() {
        return (this.props.headers[(this.props.headers?.length ?? 0) - 1]?.path ? this.RenderTabsWithRouter() : this.RenderDefaultTabs());
    }
}