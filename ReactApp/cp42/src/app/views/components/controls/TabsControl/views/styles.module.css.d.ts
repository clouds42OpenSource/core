declare const styles: {
  readonly 'tab-wrapper': string;
  readonly 'tabItem': string;
  readonly 'tabItem-active': string;
  readonly 'tabItem-active-dark': string;
  readonly 'tab-item-refresh-button': string;
  readonly 'tab-content': string;
};
export = styles;