import { TabProps, TabView } from 'app/views/components/controls/TabsControl/views/TabView';
import { memo } from 'react';

export const TabsControl = memo((props: TabProps) => <TabView { ...props } />);