declare const styles: {
  readonly 'btn-group': string;
  readonly 'btn': string;
};
export = styles;