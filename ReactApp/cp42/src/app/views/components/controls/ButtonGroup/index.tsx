import { hasComponentChangesFor } from 'app/common/functions';
import { ButtonGroupItem } from 'app/views/components/controls/ButtonGroup/types';
import { ButtonGroupView } from 'app/views/components/controls/ButtonGroup/views/ButtonGroupView';
import React from 'react';

type OwnState = {
    selectedItem: number
};

type OwnProps = {
    items: ButtonGroupItem[];
    selectedButtonIndex: number;
    onClick?: (buttonIndex: number) => void;
    label: string;
    fullWidth?: boolean;
};

export class ButtonGroup extends React.Component<OwnProps, OwnState> {
    constructor(props: OwnProps) {
        super(props);
        this.onClickButton = this.onClickButton.bind(this);

        this.state = {
            selectedItem: this.props.selectedButtonIndex
        };
    }

    public shouldComponentUpdate(nextProps: OwnProps, nextState: OwnState) {
        return hasComponentChangesFor(this.props, nextProps) ||
            hasComponentChangesFor(this.state, nextState);
    }

    public componentDidUpdate(nextProps: OwnProps) {
        const { selectedButtonIndex } = this.props;

        if (nextProps.selectedButtonIndex !== undefined && selectedButtonIndex !== undefined) {
            if (nextProps.selectedButtonIndex !== selectedButtonIndex) {
                this.setState({ selectedItem: selectedButtonIndex });
            }
        }
    }

    /**
     * Событие при клике по кнопке
     * @param buttonIndex Номер кнопки
     */
    private onClickButton(buttonIndex: number): void {
        if (buttonIndex === this.state.selectedItem) return;

        this.setState({ selectedItem: buttonIndex });

        if (this.props.onClick) this.props.onClick(buttonIndex);
    }

    public render() {
        const { items, label, fullWidth } = this.props;

        return (
            <ButtonGroupView
                items={ items }
                selectedButtonIndex={ this.state.selectedItem }
                onClick={ this.onClickButton }
                label={ label }
                fullWidth={ fullWidth }
            />
        );
    }
}