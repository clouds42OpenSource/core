import { ButtonGroupItem } from 'app/views/components/controls/ButtonGroup/types';
import { generateUniqueId, hasComponentChangesFor } from 'app/common/functions';
import cn from 'classnames';
import React from 'react';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { ContainedButton, OutlinedButton } from 'app/views/components/controls/Button';
import css from '../styles.module.css';

type OwnProps = {
    items: ButtonGroupItem[];
    selectedButtonIndex: number;
    onClick: (buttonIndex: number) => void;
    label: string;
    fullWidth?: boolean;
};

export class ButtonGroupView extends React.Component<OwnProps> {
    public shouldComponentUpdate(nextProps: OwnProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    /**
     * Отрендерить кнопки
     */
    private renderButtons() {
        return this.props.items.map(item => {
            const kind = item.index === this.props.selectedButtonIndex ? 'success' : 'default';
            const ButtonView = item.index === this.props.selectedButtonIndex ? ContainedButton : OutlinedButton;
            return (
                <ButtonView
                    title={ item.title }
                    key={ item.index }
                    onClick={ () => { this.props.onClick(item.index); } }
                    kind={ kind }
                    style={ item.style }
                    showProgress={ item.showProgress }
                    isEnabled={ item.isEnabled }
                    className={ cn(css.btn, item.className) }
                >
                    { item.text }
                </ButtonView>
            );
        });
    }

    public render() {
        const formId = generateUniqueId('ButtonGroup_');

        return (
            <FormAndLabel label={ this.props.label } forId={ formId } fullWidth={ this.props.fullWidth }>
                <div className={ cn(css['btn-group']) }>
                    { this.renderButtons() }
                </div>
            </FormAndLabel>
        );
    }
}