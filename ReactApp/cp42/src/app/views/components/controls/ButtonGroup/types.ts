import React from 'react';

/**
 * Тип кнопки для группы кнопок
 */
export type ButtonGroupItem = {
    index: number;

    text: string;

    title?: string;

    isEnabled?: boolean;

    showProgress?: boolean;

    style?: React.CSSProperties;

    className?: string;
};