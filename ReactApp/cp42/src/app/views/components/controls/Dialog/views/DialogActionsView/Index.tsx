import { DialogActions } from '@mui/material';
import { hasComponentChangesFor } from 'app/common/functions';
import { ContainedButton, OutlinedButton, TextButton } from 'app/views/components/controls/Button';
import { ButtonKind } from 'app/views/components/controls/Button/types';
import { TextFontSize } from 'app/views/components/TextOut/views/TextView';
import cn from 'classnames';
import React from 'react';
import css from '../styles.module.css';

export type DialogButtonType = {
    fontSize?: TextFontSize;
    content: React.ReactNode;
    kind: ButtonKind;
    variant?: 'contained' | 'outlined' | 'text';
    isEnabled?: boolean;
    hiddenButton?: boolean;
    onClick: () => void;
}[] | (() => React.ReactNode);

type OwnProps = {
    buttons?: DialogButtonType;
};

export class DialogActionsView extends React.Component<OwnProps> {
    public constructor(props: OwnProps) {
        super(props);
        this.getButtons = this.getButtons.bind(this);
    }

    public shouldComponentUpdate(nextProps: OwnProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private getButtons() {
        switch (typeof this.props.buttons) {
            case 'undefined':
                return null;

            case 'function':
                return this.props.buttons();

            default:
                return this.props.buttons?.map((button, index) => {
                    if (button.hiddenButton) {
                        return null;
                    }

                    const variant = button.variant ?? 'outlined';
                    const ButtonView = variant === 'contained'
                        ? ContainedButton
                        : variant === 'text'
                            ? TextButton
                            : OutlinedButton;
                    return (
                        <ButtonView key={ `cmd_btn_${ index }` } kind={ button.kind } onClick={ button.onClick } isEnabled={ button.isEnabled }>
                            { button.content }
                        </ButtonView>
                    );
                });
        }
    }

    public render() {
        if (this.props.buttons) {
            return (
                <DialogActions className={ cn(css['dialog-actions']) } data-testid="dialog-actions-view">
                    <div className={ css['dialog-container'] }>
                        { this.getButtons() }
                    </div>
                </DialogActions>
            );
        }

        return null;
    }
}