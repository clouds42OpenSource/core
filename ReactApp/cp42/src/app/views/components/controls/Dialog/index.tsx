import { DialogProps, DialogView } from 'app/views/components/controls/Dialog/views/DialogView/Index';

export const Dialog = (props: DialogProps) => {
    const { children, ...other } = props;

    return (
        <DialogView { ...other }>
            { children }
        </DialogView>
    );
};