import './fixed.css';

import { Dialog, Slide } from '@mui/material';
import { DialogActionsView, DialogButtonType } from 'app/views/components/controls/Dialog/views/DialogActionsView/Index';
import { DialogVerticalAlign, DialogWidth } from 'app/views/components/controls/Dialog/types';
import { ERefreshId } from 'app/views/Layout/ProjectTour/enums';
import React, { ReactNode } from 'react';
import { TextFontSize, TextFontWeight, TextHorizontalAlign } from 'app/views/components/TextOut/views/TextView';

import { AppConsts } from 'app/common/constants';
import { DialogContentView } from 'app/views/components/controls/Dialog/views/DialogContentView/Index';
import { DialogTitleView } from 'app/views/components/controls/Dialog/views/DialogTitleView';
import { TransitionProps } from '@mui/material/transitions';
import cn from 'classnames';
import { hasComponentChangesFor } from 'app/common/functions';

export type DialogProps = {
    /**
     * Флаг на открытие диалогового окошка
     */
    isOpen: boolean,
    /**
     * Заголовок
     */
    title?: string,
    /**
     * Размер шрифта заголовка
     */
    titleFontSize?: TextFontSize,
    /**
     * Толщина шрифта заголовка
     */
    titleFontWeight?: TextFontWeight,
    /**
     * Выравнивание текста заголовка
     */
    titleTextAlign?: TextHorizontalAlign,
    /**
     * Флаг для уменьшения заголовка
     */
    isTitleSmall?: boolean,
    /**
     * Вертикальное выравнивание диалогового окошка
     */
    dialogVerticalAlign?: DialogVerticalAlign,
    /**
     * Ширина диалогового окошка
     */
    dialogWidth: DialogWidth,
    /**
     * Функция обработки при нажатии на кнопку отмены
     */
    onCancelClick?: () => void,
    /**
     * Кнопки
     */
    buttons?: DialogButtonType,
    /**
     * Тело диалогового окошка
     */
    children: ReactNode,
    /**
     * Название класса
     */
    className?: string,
    /**
     * Название класса контента
     */
    contentClassName?: string,
    /**
     * Компонент перехода
     */
    TransitionComponent?: React.JSXElementConstructor<TransitionProps & {
        children: React.ReactElement<any, any>
    }>,
    /**
     * Свойства Paper
     */
    PaperProps?: {
        className: string;
    },
    /**
     * Флаг для включения/выключения возможности закрыть кликнув за пределы диалогового окошка
     */
    disableBackdropClick?: boolean;

    maxHeight?: string;

    refreshId?: ERefreshId;
    contentRefreshId?: ERefreshId;
    /**
     * Скрыть иконку крестика в тайтле
     */
    hiddenX?: boolean;
};

export class DialogView extends React.Component<DialogProps> {
    public shouldComponentUpdate(nextProps: DialogProps) {
        return hasComponentChangesFor(this.props, nextProps) ||
            this.props.children !== nextProps.children;
    }

    Transition = React.forwardRef((
        props: TransitionProps & {
            children: React.ReactElement;
        },
        ref: React.Ref<unknown>,
    ) => {
        return <Slide direction="down" ref={ ref } { ...props } />;
    });

    public render() {
        const dialogVerticalAlign = this.props.dialogVerticalAlign ?? 'stretch';
        const isMobileSized = window.innerWidth < AppConsts.mobileScreenWidth;

        return (
            <Dialog
                data-refreshid={ this.props.refreshId }
                data-testid="dialog-view"
                hideBackdrop={ this.props.disableBackdropClick }
                className={
                    cn({
                        'dialog-window': true,
                        'fixed-stretch': dialogVerticalAlign === 'stretch',
                        'fixed-center': dialogVerticalAlign === 'center',
                        'fixed-start': dialogVerticalAlign === 'top',
                        'fixed-end': dialogVerticalAlign === 'bottom'
                    }, this.props.className)
                }
                disableEscapeKeyDown={ false }
                PaperProps={ { ...this.props.PaperProps, style: { overflow: 'visible !important' } } }
                TransitionComponent={ this.Transition || this.props.TransitionComponent }
                fullWidth={ true }
                maxWidth={ this.props.dialogWidth }
                open={ this.props.isOpen }
                scroll="paper"
                fullScreen={ isMobileSized }
                onClose={ this.props.onCancelClick }
                style={ { maxHeight: (this.props?.maxHeight ? this.props?.maxHeight : '') } }
                disableEnforceFocus={ true }
            >
                <DialogTitleView
                    isSmall={ this.props.isTitleSmall }
                    fontSize={ this.props.titleFontSize }
                    fontWeight={ this.props.titleFontWeight }
                    textAlign={ this.props.titleTextAlign }
                    title={ this.props.title }
                    hiddenX={ this.props.hiddenX }
                    onCancelClick={ this.props.onCancelClick }
                />
                <DialogContentView className={ this.props.contentClassName } contentRefreshId={ this.props.contentRefreshId }>
                    { this.props.children }
                </DialogContentView>
                <DialogActionsView buttons={ this.props.buttons } />
            </Dialog>
        );
    }
}