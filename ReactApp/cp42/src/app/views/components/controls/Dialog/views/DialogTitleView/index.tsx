import { DialogTitle } from '@mui/material';
import { hasComponentChangesFor } from 'app/common/functions';
import { TextButton } from 'app/views/components/controls/Button';
import { TextOut } from 'app/views/components/TextOut';
import { TextFontSize, TextFontWeight } from 'app/views/components/TextOut/views/TextView';
import cn from 'classnames';
import React from 'react';
import css from '../styles.module.css';

type OwnProps = {
    title?: string;
    fontSize?: TextFontSize;
    fontWeight?: TextFontWeight;
    textAlign?: 'left' | 'center' | 'right';
    isSmall?: boolean;
    hiddenX?: boolean;
    onCancelClick?: () => void;
};
export class DialogTitleView extends React.Component<OwnProps> {
    public shouldComponentUpdate(nextProps: OwnProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    public render() {
        const titleFontSize = this.props.fontSize ?? 20;
        const titleFontWeight = this.props.fontWeight ?? 600;
        const titleTextAlign = this.props.textAlign ?? 'center';

        return this.props.title
            ? (
                <DialogTitle
                    data-testid="dialog-title-view"
                    className={ cn({
                        [css['dialog-title']]: true,
                        [css['dialog-title-small']]: this.props.isSmall !== false
                    }) }
                >
                    <div>
                        {
                            !this.props.hiddenX && (
                                <TextButton
                                    className={ cn({
                                        [css['dialog-title-close-btn']]: true,
                                        [css['dialog-title-close-btn-small']]: this.props.isSmall === true
                                    }) }
                                    onClick={ this.props.onCancelClick }
                                    title="Закрыть"
                                >
                                    <TextOut inDiv={ false } fontSize={ 20 }>×</TextOut>
                                </TextButton>
                            )
                        }
                        <TextOut fontSize={ titleFontSize } fontWeight={ titleFontWeight } textAlign={ titleTextAlign } inDiv={ true }>
                            { this.props.title }
                        </TextOut>
                    </div>
                </DialogTitle>
            )
            : null;
    }
}