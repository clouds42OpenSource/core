export type DialogWidth = 'xs' | 'sm' | 'md' | 'lg' | 'xl' | false;
export type DialogVerticalAlign = 'stretch' | 'top' | 'bottom' | 'center';