import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { DialogActionsView } from 'app/views/components/controls/Dialog/views/DialogActionsView/Index';
import userEvent from '@testing-library/user-event';

describe('<DialogActionsView', () => {
    const onClickMock: jest.Mock = jest.fn();

    it('renders', () => {
        render(<DialogActionsView buttons={ [
            {
                content: 'test_text',
                kind: 'default',
                onClick: onClickMock,
                variant: 'text'
            }
        ] }
        />);
        expect(screen.queryByTestId(/dialog-actions-view/i)).toBeInTheDocument();
    });

    it('does not renders', () => {
        render(<DialogActionsView />);
        expect(screen.queryByTestId(/dialog-actions-view/i)).not.toBeInTheDocument();
    });

    it('has onClick method', async () => {
        render(<DialogActionsView buttons={ [
            {
                content: 'test_text',
                kind: 'default',
                onClick: onClickMock,
                variant: 'text'
            }
        ] }
        />);
        await userEvent.click(screen.getByTestId(/button-view/i));
        expect(onClickMock).toBeCalled();
    });
});