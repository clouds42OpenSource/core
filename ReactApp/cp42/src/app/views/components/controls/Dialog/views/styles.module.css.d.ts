declare const styles: {
    readonly 'dialog-title': string;
    readonly 'dialog-title-small': string;
    readonly 'dialog-title-content': string;
    readonly 'dialog-title-close-btn': string;
    readonly 'dialog-title-close-btn-small': string;
    readonly 'dialog-actions': string;
    readonly 'dialog-content': string;
    readonly 'dialog-container': string;
};

export = styles;