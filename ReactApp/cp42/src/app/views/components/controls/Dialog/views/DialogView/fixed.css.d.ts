declare const styles: {
  readonly 'dialog-window': string;
  readonly 'fixed-stretch': string;
  readonly 'MuiDialog-scrollPaper': string;
  readonly 'fixed-start': string;
  readonly 'fixed-center': string;
  readonly 'fixed-end': string;
};

export = styles;