import { cleanup, render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import userEvent from '@testing-library/user-event';
import { DialogTitleView } from 'app/views/components/controls/Dialog/views/DialogTitleView';

describe('<DialogTitleView />', () => {
    const onCancelClickMock: jest.Mock = jest.fn();
    let dialogTitleViewComponent: HTMLElement | null;

    beforeEach(() => {
        render(<DialogTitleView onCancelClick={ onCancelClickMock } title="test_text" />);
        dialogTitleViewComponent = screen.queryByTestId(/dialog-title-view/i);
    });

    it('renders', () => {
        expect(dialogTitleViewComponent).toBeInTheDocument();
    });

    it('has child components', () => {
        expect(dialogTitleViewComponent).toHaveTextContent(/test_text/i);
    });

    it('has onCancelClick method', async () => {
        await userEvent.click(screen.getByTestId(/button-view/i));
        expect(onCancelClickMock).toBeCalled();
    });

    it('does not renders', () => {
        cleanup();
        render(<DialogTitleView onCancelClick={ onCancelClickMock } />);
        expect(screen.queryByTestId(/dialog-title-view/i)).not.toBeInTheDocument();
    });
});