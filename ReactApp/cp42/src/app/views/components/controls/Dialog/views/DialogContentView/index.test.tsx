import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { DialogContentView } from 'app/views/components/controls/Dialog/views/DialogContentView/Index';

describe('<DialogContentView />', () => {
    let dialogContentViewComponent: HTMLElement;

    beforeAll(() => {
        render(<DialogContentView>test_text</DialogContentView>);
        dialogContentViewComponent = screen.getByTestId(/dialog-content-view/i);
    });

    it('renders', () => {
        expect(dialogContentViewComponent).toBeInTheDocument();
    });

    it('has child components', () => {
        expect(dialogContentViewComponent).toHaveTextContent(/test_text/i);
    });
});