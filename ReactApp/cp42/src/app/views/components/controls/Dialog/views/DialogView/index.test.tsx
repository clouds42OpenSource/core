import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { DialogView } from 'app/views/components/controls/Dialog/views/DialogView/Index';

describe('<DialogView />', () => {
    const onCancelClickMock: jest.Mock = jest.fn();

    it('renders', () => {
        render(<DialogView isOpen={ true } dialogWidth={ false } onCancelClick={ onCancelClickMock }>test_text</DialogView>);
        expect(screen.getByTestId(/dialog-view/i)).toBeInTheDocument();
        expect(screen.getByText(/test_text/i)).toBeInTheDocument();
    });

    it('does not renders', () => {
        render(<DialogView isOpen={ false } dialogWidth={ false } onCancelClick={ onCancelClickMock }>test_text</DialogView>);
        expect(screen.queryByTestId(/dialog-view/i)).not.toBeInTheDocument();
    });
});