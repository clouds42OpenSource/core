import { DialogContent } from '@mui/material';
import { hasComponentChangesFor } from 'app/common/functions';
import { ERefreshId } from 'app/views/Layout/ProjectTour/enums';
import cn from 'classnames';
import React, { ReactNode } from 'react';
import css from '../styles.module.css';

type OwnProps = {
    className?: string;
    children: ReactNode;
    contentRefreshId?: ERefreshId;
};
export class DialogContentView extends React.Component<OwnProps> {
    public shouldComponentUpdate(nextProps: OwnProps) {
        return hasComponentChangesFor(this.props, nextProps) || this.props.children !== nextProps.children;
    }

    public render() {
        return (
            <DialogContent data-refreshid={ this.props.contentRefreshId } className={ cn(css['dialog-content'], this.props.className) } data-testid="dialog-content-view">
                { this.props.children }
            </DialogContent>
        );
    }
}