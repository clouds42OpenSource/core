import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { DialogMessageView } from 'app/views/components/controls/DialogMessage/views/DialogView/Index';

describe('<DialogMessageView />', () => {
    const onCancelClickMock: jest.Mock = jest.fn();

    it('renders', () => {
        render(<DialogMessageView isOpen={ true } dialogWidth="xs" onCancelClick={ onCancelClickMock } />);
        expect(screen.getByRole(/dialog/i)).toBeInTheDocument();
        expect(screen.getByTestId(/dialog-content-view/i)).toBeInTheDocument();
        expect(screen.getByTestId(/dialog-view/i)).toBeInTheDocument();
    });

    it('renders with children', () => {
        render(<DialogMessageView isOpen={ true } dialogWidth="xs" onCancelClick={ onCancelClickMock }>test_text</DialogMessageView>);
        expect(screen.getByRole(/dialog/i)).toBeInTheDocument();
        expect(screen.getByText(/test_text/i)).toBeInTheDocument();
    });

    it('does not renders', () => {
        render(<DialogMessageView isOpen={ false } dialogWidth="xs" onCancelClick={ onCancelClickMock }>test_text</DialogMessageView>);
        expect(screen.queryByRole(/dialog/i)).not.toBeInTheDocument();
    });
});