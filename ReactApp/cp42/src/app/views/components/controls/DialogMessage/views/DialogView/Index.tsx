import './fixed.css';
import { Zoom } from '@mui/material';
import { hasComponentChangesFor } from 'app/common/functions';

import { Dialog } from 'app/views/components/controls/Dialog';
import { DialogWidth } from 'app/views/components/controls/Dialog/types';
import { DialogButtonType } from 'app/views/components/controls/Dialog/views/DialogActionsView/Index';

import { TextFontSize, TextFontWeight } from 'app/views/components/TextOut/views/TextView';
import cn from 'classnames';
import React from 'react';
import css from '../styles.module.css';

export type DialogMessageProps = {
    isOpen: boolean;
    title?: string;
    titleFontSize?: TextFontSize;
    titleFontWeight?: TextFontWeight;
    titleTextAlign?: 'left' | 'center' | 'right';
    isTitleSmall?: boolean;
    buttons?: DialogButtonType;
    dialogWidth: DialogWidth;
    onCancelClick: () => void;
};

export class DialogMessageView extends React.Component<DialogMessageProps> {
    public shouldComponentUpdate(nextProps: DialogMessageProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    public render() {
        return (
            <Dialog
                data-testid="dialog-message-view"
                title={ this.props.title }
                titleFontSize={ this.props.titleFontSize }
                titleFontWeight={ this.props.titleFontWeight }
                titleTextAlign={ this.props.titleTextAlign }
                isTitleSmall={ this.props.isTitleSmall }
                className="dialog-message-window"
                contentClassName={ cn(css['dialog-message-content']) }
                TransitionComponent={ Zoom }
                dialogVerticalAlign="center"
                PaperProps={ {
                    className: 'showSweetAlert',
                } }
                dialogWidth={ this.props.dialogWidth }
                isOpen={ this.props.isOpen }
                onCancelClick={ this.props.onCancelClick }
                buttons={ this.props.buttons }
            >
                { this.props.children }
            </Dialog>
        );
    }
}