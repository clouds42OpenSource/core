import { DialogMessageProps, DialogMessageView } from 'app/views/components/controls/DialogMessage/views/DialogView/Index';
import React from 'react';

export const DialogMessage = (props: DialogMessageProps & { children?: React.ReactNode }) => {
    return <DialogMessageView { ...props } />;
};