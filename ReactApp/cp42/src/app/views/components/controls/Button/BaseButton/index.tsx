import { hasComponentChangesFor } from 'app/common/functions';
import { ButtonCircularProgressView } from 'app/views/components/controls/Button/BaseButton/views/ButtonCircularProgressView';
import { ButtonView } from 'app/views/components/controls/Button/BaseButton/views/ButtonView';
import { ButtonKind, ButtonSize, ButtonVariant } from 'app/views/components/controls/Button/types';
import { EPartnersTourId, ETourId } from 'app/views/Layout/ProjectTour/enums';
import React, { MouseEventHandler } from 'react';

export type BaseButtonProps = {
    /**
     * Вид кнопки
     */
    kind?: ButtonKind;

    /**
     * Размер кнопки
     */
    size?: ButtonSize;

    /**
     * Вариант кнопки
     */
    variant?: ButtonVariant;

    /**
     * Всплывающая подсказка
     */
    title?: string;

    /**
     * Устанавливает кнопку на всю ширину
     */
    fullWidth?: boolean;

    /**
     * Событие на нажатие кнопки
     */
    onClick?: MouseEventHandler;
     /**
      * Событие наведения на кнопку
      */
   onMouseEnter?: MouseEventHandler;
     /**
      * Событие покидания кнопки курсором
      */
   onMouseLeave?: MouseEventHandler;

    /**
     * Устанавливает стостояние кнопки активное или нет
     */
    isEnabled?: boolean;

    /**
     * Показывать или не показывать спинер
     */
    showProgress?: boolean;

    /**
     * Кастомный стиль для кнопки
     */
    style?: React.CSSProperties;

    /**
     * Кастомный класс для кнопки
     */
    className?: string;

    /**
     * Ссылка на кнопку
     */
    buttonRef?: React.RefObject<HTMLButtonElement>;

    /**
     * Кастомный стиль для спинера
     */
    spinnerStyle?: React.CSSProperties;

    /**
     * Кастомный класс для спинера
     */
    spinnerClassName?: string;

    /**
     * Контент кнопки
     */
    children?: React.ReactNode;

    /**
     * Устанавить фокус на кнопку
     */
    autoFocus?: boolean;

    /**
     * Дать возможность выделять контент кнопки
     */
    canSelectButtonContent?: boolean;

    /**
     * Возможность переносить длинный текст на новую строку
     */
    wrapLongText?: boolean;

    type?: 'submit' | 'reset' | 'button';

    /**
     * Скрыть кнопку
     */
    isHidden?: boolean;

    tourId?: ETourId | EPartnersTourId;
};

export class BaseButton extends React.Component<BaseButtonProps> {
    public shouldComponentUpdate(nextProps: BaseButtonProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    public render() {
        const size = this.props.size ?? 'small';
        const kind = this.props.kind ?? 'default';
        const variant = this.props.variant ?? 'contained';
        const isEnabled = this.props.isEnabled ?? true;
        const fullWidth = this.props.fullWidth ?? false;
        const showProgress = this.props.showProgress ?? false;
        const autoFocus = this.props.autoFocus ?? false;
        const canSelectButtonContent = this.props.canSelectButtonContent ?? false;
        const wrapLongText = this.props.wrapLongText ?? false;

        return (
            <ButtonView
                tourId={ this.props.tourId }
                isHidden={ this.props.isHidden }
                buttonRef={ this.props.buttonRef }
                size={ size }
                autoFocus={ autoFocus }
                fullWidth={ fullWidth }
                isEnabled={ isEnabled }
                kind={ kind }
                variant={ variant }
                title={ this.props.title }
                className={ this.props.className }
                style={ this.props.style }
                canSelectButtonContent={ canSelectButtonContent }
                wrapLongText={ wrapLongText }
                onClick={ this.props.onClick }
                onMouseLeave={ this.props.onMouseLeave }
                onMouseEnter={ this.props.onMouseEnter }
                type={ this.props.type }
            >
                { showProgress
                    ? <ButtonCircularProgressView isEnabled={ isEnabled } kind={ kind } variant={ variant } className={ this.props.spinnerClassName } style={ this.props.spinnerStyle } />
                    : this.props.children
                }
            </ButtonView>
        );
    }
}