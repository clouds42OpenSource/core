import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import '@testing-library/jest-dom/extend-expect';
import { ButtonView } from 'app/views/components/controls/Button/BaseButton/views/ButtonView';

describe('<ButtonView />', () => {
    let baseButton: HTMLElement;
    const onClickMock: jest.Mock = jest.fn();

    beforeEach(() => {
        render(
            <ButtonView
                onClick={ onClickMock }
                kind="default"
                size="medium"
                variant="text"
                fullWidth={ false }
                isEnabled={ true }
                canSelectButtonContent={ false }
                wrapLongText={ false }
            >
                test_text
            </ButtonView>
        );
        baseButton = screen.getByTestId(/button-view/i);
    });

    it('renders', () => {
        expect(baseButton).toBeInTheDocument();
    });

    it('has children props', () => {
        expect(baseButton).toHaveTextContent(/test_text/i);
    });

    it('has onClick method', async () => {
        await userEvent.click(baseButton);
        expect(onClickMock).toHaveBeenCalled();
    });
});