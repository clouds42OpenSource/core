import { CircularProgress } from '@mui/material';
import { hasComponentChangesFor } from 'app/common/functions';
import { ButtonKind, ButtonVariant } from 'app/views/components/controls/Button/types';
import cn from 'classnames';
import React from 'react';
import css from '../styles.module.css';

type OwnProps = {
    isEnabled: boolean;
    kind: ButtonKind;
    variant: ButtonVariant;

    style?: React.CSSProperties;
    className?: string;
};

export class ButtonCircularProgressView extends React.Component<OwnProps> {
    public shouldComponentUpdate(nextProps: OwnProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    public render() {
        const kind = this.props.kind ?? 'default';
        const variant = this.props.variant ?? 'contained';
        const isEnabled = this.props.isEnabled ?? true;

        return (
            <CircularProgress
                data-testid="button-circular-progress-view"
                size="1rem"
                className={ cn(css.spinner,
                    isEnabled
                        ? cn({
                            [css['spinner__contained--default']]: kind === 'default' && variant === 'contained',
                            [css['spinner__outlined--default']]: kind === 'default' && variant === 'outlined',
                            [css['spinner__text--default']]: kind === 'default' && variant === 'text',

                            [css['spinner__contained--success']]: kind === 'success' && variant === 'contained',
                            [css['spinner__outlined--success']]: kind === 'success' && variant === 'outlined',
                            [css['spinner__text--success']]: kind === 'success' && variant === 'text',

                            [css['spinner__contained--error']]: kind === 'error' && variant === 'contained',
                            [css['spinner__outlined--error']]: kind === 'error' && variant === 'outlined',
                            [css['spinner__text--error']]: kind === 'error' && variant === 'text',

                            [css['spinner__contained--turquoise']]: kind === 'turquoise' && variant === 'contained',
                            [css['spinner__outlined--turquoise']]: kind === 'turquoise' && variant === 'outlined',
                            [css['spinner__text--turquoise']]: kind === 'turquoise' && variant === 'text',

                            [css['spinner__contained--primary']]: kind === 'primary' && variant === 'contained',
                            [css['spinner__outlined--primary']]: kind === 'primary' && variant === 'outlined',
                            [css['spinner__text--primary']]: kind === 'primary' && variant === 'text'
                        })
                        : cn(css['spinner-disabled']),
                    this.props.className)
                }
                style={ this.props.style }
            />
        );
    }
}