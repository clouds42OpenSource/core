import cn from 'classnames';
import React from 'react';

import { BaseButton, BaseButtonProps } from 'app/views/components/controls/Button/BaseButton';

import css from './BaseButton/views/styles.module.css';

export const ContainedButton = (props: Omit<BaseButtonProps, 'variant'>) => {
    return (
        <BaseButton { ...props } variant="contained" />
    );
};

export const OutlinedButton = (props: Omit<BaseButtonProps, 'variant'>) => {
    return (
        <BaseButton { ...props } variant="outlined" />
    );
};

export const TextButton = (props: Omit<BaseButtonProps, 'variant'>) => {
    return (
        <BaseButton { ...props } variant="text" />
    );
};

export const SearchByFilterButton = (props: Omit<BaseButtonProps, 'variant'>) => {
    return (
        <BaseButton { ...props } variant="outlined" kind="success">
            <i className="fa fa-search" />&nbsp;Поиск
        </BaseButton>
    );
};

export const SuccessButton = (props: Omit<BaseButtonProps, 'variant'>) => {
    return (
        <BaseButton { ...props } variant="contained" kind="success" className={ cn(css['common-success-button'], props.className) } />
    );
};

export const DefaultButton = (props: Omit<BaseButtonProps, 'variant'>) => {
    return (
        <BaseButton { ...props } variant="contained" kind="default" className={ cn(css['common-default-button']) } />
    );
};

export const TextLinkButton = (props: Omit<BaseButtonProps, 'variant'>) => {
    return (
        <BaseButton { ...props } variant="text" className={ cn(css['common-text-button'], props.className) } />
    );
};