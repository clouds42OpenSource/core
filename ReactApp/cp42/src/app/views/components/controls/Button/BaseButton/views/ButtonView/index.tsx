import { ButtonKind, ButtonSize, ButtonVariant } from 'app/views/components/controls/Button/types';
import { EPartnersTourId, ETourId } from 'app/views/Layout/ProjectTour/enums';
import React, { MouseEventHandler, PropsWithRef } from 'react';
import { Button } from '@mui/material';
import { hasComponentChangesFor } from 'app/common/functions';
import cn from 'classnames';
import css from '../styles.module.css';

type OwnProps = {
    /**
     * Вид кнопки
     */
    kind: ButtonKind;

    /**
     * Размер кнопки
     */
    size: ButtonSize;

    /**
     * Вариант кнопки
     */
    variant: ButtonVariant;

    /**
     * Всплывающая подсказка
     */
    title?: string;

    /**
     * Устанавливает кнопку на всю ширину
     */
    fullWidth: boolean;

    /**
     * Устанавливает стостояние кнопки активное или нет
     */
    isEnabled: boolean;

    /**
     * Событие на нажатие кнопки
     */
    onClick?: MouseEventHandler;
    /**
     * Событие наведения на кнопку
     */
    onMouseEnter?: MouseEventHandler;
    /**
     * Событие покидания кнопки курсором
     */
    onMouseLeave?: MouseEventHandler;

    /**
     * Кастомный стиль для кнопки
     */
    style?: React.CSSProperties;

    /**
     * Кастомный класс для кнопки
     */
    className?: string;

    /**
     * Ссылка на кнопку
     */
    buttonRef?: React.RefObject<HTMLButtonElement>;

    /**
     * Устанавить фокус на кнопку
     */
    autoFocus?: boolean;

    /**
     * Дать возможность выделять контент кнопки
     */
    canSelectButtonContent: boolean;

    /**
     * Возможность переносить длинный текст на новую строку
     */
    wrapLongText: boolean;

    type?: 'submit' | 'reset' | 'button';

    /**
     * Скрыть кнопку
     */
    isHidden?: boolean;

    tourId?: ETourId | EPartnersTourId;
};

export type BaseButtonProps = OwnProps & PropsWithRef<OwnProps>;

export class ButtonView extends React.Component<BaseButtonProps> {
    public shouldComponentUpdate(nextProps: BaseButtonProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    public render() {
        const { isHidden, title, fullWidth, variant, size, kind, isEnabled, wrapLongText, tourId } = this.props;

        return (
            <Button
                data-tourid={ tourId }
                hidden={ isHidden }
                data-testid="button-view"
                onMouseLeave={ isEnabled ? this.props.onMouseLeave : undefined }
                onMouseEnter={ isEnabled ? this.props.onMouseEnter : undefined }
                ref={ this.props.buttonRef }
                autoFocus={ this.props.autoFocus }
                title={ title }
                type={ this.props.type }
                fullWidth={ fullWidth }
                variant={ variant }
                size={ size }
                disabled={ !isEnabled }
                color="inherit"
                disableElevation={ true }
                className={
                    cn(css.button,
                        { [css['can-button-content-select']]: this.props.canSelectButtonContent },
                        { [css.wrapLongText]: wrapLongText },
                        isEnabled
                            ? cn({
                                [css['button__outlined--default']]: kind === 'default' && variant === 'outlined',
                                [css['button__text--default']]: kind === 'default' && variant === 'text',
                                [css['button__contained--default']]: kind === 'default' && variant === 'contained',

                                [css['button__contained--success']]: kind === 'success' && variant === 'contained',
                                [css['button__outlined--success']]: kind === 'success' && variant === 'outlined',
                                [css['button__text--success']]: kind === 'success' && variant === 'text',

                                [css['button__contained--error']]: kind === 'error' && variant === 'contained',
                                [css['button__outlined--error']]: kind === 'error' && variant === 'outlined',
                                [css['button__text--error']]: kind === 'error' && variant === 'text',

                                [css['button__contained--turquoise']]: kind === 'turquoise' && variant === 'contained',
                                [css['button__outlined--turquoise']]: kind === 'turquoise' && variant === 'outlined',
                                [css['button__text--turquoise']]: kind === 'turquoise' && variant === 'text',

                                [css['button__contained--primary']]: kind === 'primary' && variant === 'contained',
                                [css['button__outlined--primary']]: kind === 'primary' && variant === 'outlined',
                                [css['button__text--primary']]: kind === 'primary' && variant === 'text',

                                [css['button__contained--warning']]: kind === 'warning' && variant === 'contained',
                                [css['button__outlined--warning']]: kind === 'warning' && variant === 'outlined',
                                [css['button__text--warning']]: kind === 'warning' && variant === 'text'
                            })
                            : cn(css.disabled_button),
                        this.props.className)
                }
                style={ this.props.style }
                onClick={ isEnabled ? this.props.onClick : undefined }
            >
                { this.props.children }
            </Button>
        );
    }
}