export type ButtonKind = 'default' | 'primary' | 'success' | 'error' | 'turquoise' | 'warning';
export type ButtonVariant = 'contained' | 'outlined' | 'text';
export type ButtonSize = 'small' | 'medium' | 'large';