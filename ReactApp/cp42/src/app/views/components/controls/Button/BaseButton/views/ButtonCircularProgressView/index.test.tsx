import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { ButtonCircularProgressView } from 'app/views/components/controls/Button/BaseButton/views/ButtonCircularProgressView';

describe('<ButtonCircularProgressView />', () => {
    let circularButtonProgress: HTMLElement;

    beforeAll(() => {
        render(
            <ButtonCircularProgressView
                isEnabled={ true }
                kind="default"
                variant="text"
            />
        );
        circularButtonProgress = screen.getByTestId(/button-circular-progress-view/i);
    });

    it('renders', () => {
        expect(circularButtonProgress).toBeInTheDocument();
    });
});