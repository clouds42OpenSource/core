import { Box, Chip, MenuItem, Select, SelectChangeEvent } from '@mui/material';

import styles from './style.module.css';

export type TMenuItem = {
    value: string;
    text: string;
};

export type TMultipleChipSelect = {
    items: TMenuItem[];
    selectedItems: string[];
    handleChange: (ev: SelectChangeEvent<string[]>) => void;
    chipColor?: 'error' | 'default' | 'primary' | 'secondary' | 'info' | 'success' | 'warning';
    chipVariant?: 'outlined' | 'filled';
    name?: string;
    error?: boolean;
};

export const MultipleChipSelect = ({ items, selectedItems, handleChange, chipColor, chipVariant, name, error }: TMultipleChipSelect) => {
    return (
        <Select
            error={ error }
            name={ name }
            fullWidth={ true }
            className={ styles.select }
            value={ selectedItems }
            multiple={ true }
            onChange={ handleChange }
            renderValue={ selected => (
                <Box sx={ { display: 'flex', flexWrap: 'wrap', gap: 1 } }>
                    { selected.map(value => (
                        <Chip
                            color={ chipColor }
                            variant={ chipVariant }
                            key={ value }
                            label={ items.find(item => item.value === value)?.text }
                        />
                    )) }
                </Box>
            ) }
        >
            { items.map(({ value, text }) => (
                <MenuItem
                    key={ value }
                    value={ value }
                >
                    { text }
                </MenuItem>
            )) }
        </Select>
    );
};