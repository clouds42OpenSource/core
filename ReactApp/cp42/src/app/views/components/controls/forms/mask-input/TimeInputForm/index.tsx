import { ValueAppliedEvent, ValueChangeEvent } from 'app/views/components/controls/forms/_types';
import { generateUniqueId, hasComponentChangesFor } from 'app/common/functions';
import React from 'react';
import { TextField } from '@mui/material';
import cn from 'classnames';
import css from './style.module.css';

export type TimeInputProps = {
    formName: string;
    label: string;
    value?: string;
    defaultValue?: string;
    isReadOnly?: boolean;
    onChange?: (e: string) => void;
    onValueChange?: ValueChangeEvent<string>;
    onValueApplied?: ValueAppliedEvent<string>;
};

type TimeInputState = {
    timeValue: string | undefined
};

class TimeInputFormClass extends React.Component<TimeInputProps, TimeInputState> {
    private _id = generateUniqueId('mask_text_box_form_');

    private _timeFormat = '00:00';

    public constructor(props: TimeInputProps) {
        super(props);
        this.onChange = this.onChange.bind(this);
        this.applyTimeChanges = this.applyTimeChanges.bind(this);
        this.onBlur = this.onBlur.bind(this);
        this.onKeyPress = this.onKeyPress.bind(this);
        this.state = {
            timeValue: this.props.value
        };
    }

    public shouldComponentUpdate(nextProps: TimeInputProps, nextState: TimeInputState) {
        return hasComponentChangesFor(this.props, nextProps) || hasComponentChangesFor(this.state, nextState);
    }

    public componentDidUpdate(prevProps: Readonly<TimeInputProps>) {
        if (!hasComponentChangesFor(prevProps, this.props)) return;
        this.setState({
            timeValue: this.props.value
        });
    }

    private onChange(e: React.SyntheticEvent) {
        const eventObj = e as unknown as React.SyntheticEvent<HTMLInputElement>;
        this.setState({
            timeValue: eventObj.currentTarget.value
        });
    }

    private onBlur() {
        this.applyTimeChanges();
    }

    private onKeyPress(e: React.KeyboardEvent) {
        if (e.key === 'Enter') this.applyTimeChanges();
    }

    private applyTimeChanges() {
        const { timeValue } = this.state;
        if (!timeValue) return;

        if (timeValue.length < this._timeFormat.length) return;

        if (this.props.onValueChange) {
            const isSuccess = this.props.onValueChange(this.props.formName, timeValue, timeValue) ?? true;
            if (isSuccess && this.props.onValueApplied) {
                this.props.onValueApplied(this.props.formName, timeValue);
            }
        }
        if (this.props.onChange) {
            this.props.onChange(timeValue);
        }
    }

    public render() {
        return (
            <TextField
                id={ this._id }
                type="time"
                defaultValue={ this.props.defaultValue }
                value={ this.state.timeValue }
                className={ cn(css['time-input']) }
                InputLabelProps={ {
                    shrink: true,
                } }
                onChange={ this.onChange }
                onBlur={ this.onBlur }
                onKeyPress={ this.onKeyPress }
            />
        );
    }
}

export class TimeInputForm extends React.Component<TimeInputProps> {
    public render() {
        return (
            <TimeInputFormClass
                { ...this.props }
            />
        );
    }
}