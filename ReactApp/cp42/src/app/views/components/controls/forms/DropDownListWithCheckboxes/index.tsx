import { Checkbox, Chip, Divider, FormControl, InputLabel, MenuItem, MenuList, OutlinedInput, Select, TextField } from '@mui/material';
import { COLORS } from 'app/utils';
import { TextOut } from 'app/views/components/TextOut';
import { Component, ReactNode } from 'react';
import { generateUniqueId } from 'app/common/functions';
import { OutlinedButton } from 'app/views/components/controls/Button';
import { DropDownListWithCheckboxItem } from 'app/views/components/controls/forms/DropDownListWithCheckboxes/types';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { Box } from '@mui/system';
import css from './styles.module.css';

type DropDownListWithCheckboxesState = {
    isOpen: boolean;
    items: DropDownListWithCheckboxItem[];
    searchValue: string;
};

type DropDownListWithCheckboxesProps = {
    items: DropDownListWithCheckboxItem[];
    label?: string;
    formName: string;
    onValueChange?: (formName: string, items: DropDownListWithCheckboxItem[]) => void;
    disabled?: boolean;
    selectDisabled?: boolean;
    zeroCheckedWarning?: boolean;
    noUpdateValueChangeHandler?: boolean;
    deepCompare?: boolean;
    additionalElement?: ReactNode;
    hiddenAdditionalElement?: boolean;
    selectAllElement?: boolean;
    renderValueIsChip?: boolean;
    hiddenSearchLine?: boolean;
    selectAllText?: string;
    hiddenSelectAll?: boolean;
    handleOpenSelect?: () => void;
    handleCloseSelect?: () => void;
    renderValue?: (selected: DropDownListWithCheckboxItem[]) => ReactNode;
};

export class DropDownListWithCheckboxes extends Component<DropDownListWithCheckboxesProps, DropDownListWithCheckboxesState> {
    public constructor(props: DropDownListWithCheckboxesProps) {
        super(props);
        this.state = {
            isOpen: false,
            items: this.props.items,
            searchValue: ''
        };
        this.renderSelect = this.renderSelect.bind(this);
        this.onChangeSearchItems = this.onChangeSearchItems.bind(this);
        this.onClickToChangeCheckedField = this.onClickToChangeCheckedField.bind(this);
        this.onChangeCheckbox = this.onChangeCheckbox.bind(this);
        this.handleOnChange = this.handleOnChange.bind(this);
        this.setIsOpenHandler = this.setIsOpenHandler.bind(this);
        this.setIsCloseHandler = this.setIsCloseHandler.bind(this);
    }

    public componentDidUpdate(prevProps: Readonly<DropDownListWithCheckboxesProps>) {
        const { items, noUpdateValueChangeHandler, deepCompare } = this.props;

        if (
            (prevProps.items && items && prevProps.items.length !== items.length) ||
            (deepCompare && (JSON.stringify(prevProps.items.sort()) !== JSON.stringify(items.sort())))
        ) {
            if (items) {
                this.setState({ items });

                if (!noUpdateValueChangeHandler) {
                    this.handleOnChange();
                }
            }
        }
    }

    private handleOnChange() {
        if (this.props.onValueChange) {
            this.props.onValueChange(this.props.formName, this.getCheckedItems);
        }
    }

    private onClickToChangeCheckedField(checked: boolean) {
        this.setState(prevState => ({ items: prevState.items.map(item => ({ ...item, checked })) }), this.handleOnChange);
    }

    private onChangeCheckbox(index: number) {
        if (!this.props.disabled) {
            this.setState(prevState => {
                prevState.items[index].checked = !prevState.items[index].checked;
                return {
                    items: prevState.items
                };
            }, this.handleOnChange);
        }
    }

    private onChangeSearchItems(searchValue: string) {
        const { items } = this.state;

        this.setState({ searchValue });

        items.forEach(item => {
            items[items.indexOf(item)].visible = item.text.toLowerCase().indexOf(searchValue.toLowerCase()) !== -1;
        });
        this.setState({ items });
    }

    private get getCheckedItems() {
        return this.state.items.filter(item => item.checked);
    }

    private setIsOpenHandler() {
        if (this.props.handleOpenSelect) {
            this.props.handleOpenSelect();
        }

        this.setState({ isOpen: true });
    }

    private setIsCloseHandler() {
        if (this.props.handleCloseSelect) {
            this.props.handleCloseSelect();
        }

        this.setState({ isOpen: false });
    }

    private renderSelect() {
        const { items, searchValue, isOpen } = this.state;
        const { label, disabled, selectDisabled, renderValueIsChip, renderValue } = this.props;
        const needZeroCheck = !items.find(item => item.checked) && this.props.zeroCheckedWarning;

        return (
            <Box display="flex" flexDirection="column" gap="5px">
                <FormControl
                    variant="outlined"
                    size="small"
                    data-testid="dropdown-list-with-checkboxes"
                    fullWidth={ true }
                >
                    <InputLabel id="select-checkbox-label" shrink={ true }>{ label }</InputLabel>
                    <Select
                        disabled={ selectDisabled }
                        open={ isOpen }
                        onOpen={ this.setIsOpenHandler }
                        onClose={ this.setIsCloseHandler }
                        labelId="select-checkbox-label"
                        displayEmpty={ true }
                        multiple={ true }
                        label={ label }
                        input={ <OutlinedInput notched={ true } label={ label } /> }
                        renderValue={
                            renderValue
                                ? select => renderValue(select)
                                : renderValueIsChip
                                    ? selected => (
                                        <Box sx={ { display: 'flex', flexWrap: 'wrap', gap: 0.5 } }>
                                            {
                                                selected.map(value => (
                                                    <Chip key={ value.value } label={ value.text } />
                                                ))
                                            }
                                        </Box>
                                    )
                                    : () => <TextOut>{ `Выбрано ${ this.getCheckedItems.length } из ${ items.length }` }</TextOut>
                        }
                        value={ this.getCheckedItems }
                        MenuProps={ { PaperProps: { style: { width: '320px' } } } }
                    >
                        {
                            !this.props.hiddenSearchLine && (
                                <TextField
                                    fullWidth={ true }
                                    size="small"
                                    inputProps={ { style: { fontSize: '13px' } } }
                                    InputLabelProps={ { shrink: false } }
                                    value={ searchValue }
                                    onChange={ e => this.onChangeSearchItems(e.target.value) }
                                />
                            )
                        }
                        <MenuList className={ css['control-group'] } style={ { padding: '0' } }>
                            {
                                this.props.selectAllElement && !this.props.hiddenSelectAll && (
                                    <div>
                                        <MenuItem
                                            value=""
                                            style={ { padding: '0' } }
                                            onClick={ () => this.onClickToChangeCheckedField(items.filter(item => item.checked).length !== items.length) }
                                        >
                                            <Checkbox disabled={ disabled } size="small" checked={ items.filter(item => item.checked).length === items.length } />
                                            <TextOut fontSize={ 12 }>{ this.props.selectAllText ?? 'Выбрать все' }</TextOut>
                                        </MenuItem>
                                        <Divider />
                                    </div>
                                )
                            }
                            { items.map((item, index) => (item.visible || item.visible === undefined) &&
                                <MenuItem
                                    key={ item.text }
                                    value={ item.value }
                                    style={ { padding: '0' } }
                                    onClick={ () => this.onChangeCheckbox(index) }
                                    disabled={ item.disabled }
                                >
                                    <Checkbox disabled={ disabled } size="small" checked={ item.checked } />
                                    <TextOut fontSize={ 12 }>{ item.text }</TextOut>
                                </MenuItem>
                            ) }
                        </MenuList>
                        {
                            !this.props.hiddenAdditionalElement && (
                                <div className={ css['configurations-control-buttons'] }>
                                    <Box display="flex" gap="8px">
                                        {
                                            !this.props.selectAllElement && !this.props.hiddenSelectAll && (
                                                <>
                                                    <OutlinedButton
                                                        isEnabled={ !disabled }
                                                        onClick={ () => this.onClickToChangeCheckedField(false) }
                                                        className={ css['button-custom-outline'] }
                                                        kind="success"
                                                    >
                                                        Очистить
                                                    </OutlinedButton>
                                                    <OutlinedButton
                                                        isEnabled={ !disabled }
                                                        onClick={ () => this.onClickToChangeCheckedField(true) }
                                                        className={ css['button-custom-outline'] }
                                                        kind="success"
                                                    >
                                                        Выбрать все
                                                    </OutlinedButton>
                                                </>
                                            )
                                        }
                                        { this.props.additionalElement }
                                    </Box>
                                </div>
                            )
                        }
                    </Select>
                </FormControl>
                { needZeroCheck &&
                    <TextOut fontSize={ 10 } style={ { color: COLORS.warning } }>Отметьте хотя бы один из вариантов</TextOut>
                }
            </Box>
        );
    }

    public render() {
        const dropdownListWithCheckboxesId = generateUniqueId('dropdown_list_with_checkboxes_');

        return (
            <FormAndLabel label="" forId={ dropdownListWithCheckboxesId }>
                { this.renderSelect() }
            </FormAndLabel>
        );
    }
}