import { hasComponentChangesFor } from 'app/common/functions';
import React from 'react';
import { DualListBoxFormView } from 'app/views/components/controls/forms/DualListBoxForm/views/DualListBoxFormView';
import { DualListBoxFormProps } from 'app/views/components/controls/forms/DualListBoxForm/views/types';

/**
 * Компонент для двойного списка с перемещением элементов
 */
export class DualListBoxForm<TKey> extends React.Component<DualListBoxFormProps<TKey>> {
    public shouldComponentUpdate(nextProps: DualListBoxFormProps<TKey>) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    public render() {
        return (
            <DualListBoxFormView
                { ...this.props }
            />
        );
    }
}