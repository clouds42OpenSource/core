/**
 * Тип объекта списка элементов для ComboBox компонента
 */
export type ComboboxItemModel<TValue> = {
    /**
     * Значение элемента
     */
    value: TValue;

    /**
     * Текст элемента
     */
    text: string;

    /**
     * Группировка элемента
     */
    group?: string;
};