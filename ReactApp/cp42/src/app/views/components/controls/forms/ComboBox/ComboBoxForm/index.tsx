import { hasComponentChangesFor } from 'app/common/functions';
import { AutocompleteComboBoxForm } from 'app/views/components/controls/forms/ComboBox/AutocompleteComboBoxForm';
import { SystemComboBoxForm } from 'app/views/components/controls/forms/ComboBox/SystemComboBoxForm';
import { ComboBoxFormProps } from 'app/views/components/controls/forms/ComboBox/models';
import React from 'react';

type OwnProps<TValue> = ComboBoxFormProps<TValue, false> & { allowAutoComplete?: boolean };

export class ComboBoxForm<TValue> extends React.Component<OwnProps<TValue>> {
    public shouldComponentUpdate(nextProps: OwnProps<TValue>) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    public render() {
        const allowAutoComplete = this.props.allowAutoComplete ?? false;
        const { formName } = this.props;
        const isReadOnly = this.props.isReadOnly ?? false;
        const autoFocus = this.props.autoFocus ?? false;
        const label = this.props.label ?? '';
        const items = this.props.items ?? [];
        const value = this.props.value ?? undefined;
        const ComboBoxFormView = allowAutoComplete ? AutocompleteComboBoxForm : SystemComboBoxForm;

        return (
            <ComboBoxFormView
                items={ items }
                value={ value }
                autoFocus={ autoFocus }
                formName={ formName }
                label={ label }
                isReadOnly={ isReadOnly }
                onValueChange={ this.props.onValueChange }
                onValueApplied={ this.props.onValueApplied }
            />
        );
    }
}