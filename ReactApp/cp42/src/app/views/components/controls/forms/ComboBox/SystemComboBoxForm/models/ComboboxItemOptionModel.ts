export type ComboboxItemOptionModel = {
    value: string;
    text: string;
};

export type GroupedComboboxOptionsModel = {
    [group: string]: Array<ComboboxItemOptionModel>;
};