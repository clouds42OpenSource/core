import '../fixed.css';

import { TextField, TextFieldProps } from '@mui/material';
import { DesktopDatePicker, DesktopDateTimePicker, LocalizationProvider, MobileDatePicker, MobileDateTimePicker } from '@mui/x-date-pickers';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { AppConsts } from 'app/common/constants';
import { hasComponentChangesFor } from 'app/common/functions';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { TimeInputForm } from 'app/views/components/controls/forms/mask-input/TimeInputForm';
import ruLocale from 'date-fns/locale/ru';
import React from 'react';
import { COLORS } from 'app/utils';

type BaseDateInputProps = {
    formName: string;
    label?: string;
    className?: string;
    value?: Date | null;
    isReadOnly?: boolean;
    autoFocus?: boolean;
    placeholder?: string;
    startDate?: Date | null;
    endDate?: Date | null;
    minDate?: Date | null;
    maxDate?: Date | null;
    selectsStart?: boolean;
    selectsEnd?: boolean;
    onDateChange: (date: Date | [Date, Date] | null) => void;
    isFullWidth?: boolean;
    includeTime: boolean;
};

export class MuiDateInputView extends React.Component<BaseDateInputProps> {
    private _dateFormat = '__.__.____';

    private _datePickerRef = React.createRef<HTMLDivElement>();

    public constructor(props: BaseDateInputProps) {
        super(props);
        this.renderDesktopDateInput = this.renderDesktopDateInput.bind(this);
        this.renderMobileDateInput = this.renderMobileDateInput.bind(this);
        this.renderDatePicker = this.renderDatePicker.bind(this);
        this.onDateChange = this.onDateChange.bind(this);
        this.getTimeInputElement = this.getTimeInputElement.bind(this);
        this.renderMobilePicker = this.renderMobilePicker.bind(this);
        this.renderMobilePicker = this.renderMobilePicker.bind(this);
        this._dateFormat = this.props?.includeTime
            ? '__.__.____ __:__'
            : '__.__.____';
    }

    public shouldComponentUpdate(nextProps: BaseDateInputProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private onDateChange(date: Date | null, ev: { validationError: string | null }) {
        if (ev.validationError) {
            return;
        }
        if (this.props.onDateChange) {
            this.props.onDateChange(date);
        }
    }

    private getTimeInputElement() {
        return (
            <TimeInputForm
                isReadOnly={ this.props.isReadOnly }
                formName={ this.props.formName }
                label={ this.props.label ?? '' }
            />
        );
    }

    private renderMobileDateInput(params: TextFieldProps, placeholder?: string) {
        return (
            <TextField
                { ...params }
                inputProps={ {
                    ...params.inputProps,
                    placeholder,
                } }
                InputProps={ { ...params.InputProps, readOnly: this.props.isReadOnly } }
                InputLabelProps={ { shrink: true } }
                sx={ { input: { fontSize: '13px', padding: '10.5px' } } }
                fullWidth={ true }
            />
        );
    }

    private renderDatePicker() {
        const mobileSized = window.innerWidth <= AppConsts.mobileScreenWidth;
        return (
            <LocalizationProvider dateAdapter={ AdapterDateFns } adapterLocale={ ruLocale }>
                { !mobileSized
                    ? this.renderDesktopPicker()
                    : this.renderMobilePicker()
                }
            </LocalizationProvider>
        );
    }

    private renderMobilePicker() {
        const dateTimeFormat = this.props.includeTime
            ? 'dd.MM.yyyy HH:mm'
            : 'dd.MM.yyyy';
        return (
            !this.props.includeTime
                ? (
                    <MobileDatePicker
                        disabled={ this.props.isReadOnly }
                        className={ this.props.className }
                        format={ dateTimeFormat }
                        label={ this.props.label }
                        onChange={ this.onDateChange }
                        ref={ this._datePickerRef }
                        slots={ { textField: (params: any) => this.renderMobileDateInput({ name: this.props.formName, fullWidth: true, ...params }, this.props.placeholder) } }
                        value={ this.props.value }
                    />
                )
                : (
                    <MobileDateTimePicker
                        disabled={ this.props.isReadOnly }
                        className={ this.props.className }
                        format={ dateTimeFormat }
                        label={ this.props.label }
                        onChange={ this.onDateChange }
                        ref={ this._datePickerRef }
                        slots={ { textField: (params: any) => this.renderMobileDateInput({ name: this.props.formName, fullWidth: true, ...params }, this.props.placeholder) } }
                        value={ this.props.value }
                    />
                )
        );
    }

    private renderDesktopPicker() {
        const dateTimeFormat = this.props.includeTime
            ? 'dd.MM.yyyy HH:mm'
            : 'dd.MM.yyyy';
        return (
            !this.props.includeTime
                ? (
                    <DesktopDatePicker
                        className={ this.props.className }
                        format={ dateTimeFormat }
                        readOnly={ this.props.isReadOnly }
                        label={ this.props.label }
                        onChange={ this.onDateChange }
                        ref={ this._datePickerRef }
                        slots={ { textField: (params: any) => this.renderDesktopDateInput({ name: this.props.formName, fullWidth: true, ...params }) } }
                        value={ this.props.value }
                    />
                )
                : (
                    <DesktopDateTimePicker
                        className={ this.props.className }
                        format={ dateTimeFormat }
                        readOnly={ this.props.isReadOnly }
                        label={ this.props.label }
                        onChange={ this.onDateChange }
                        ref={ this._datePickerRef }
                        slotProps={ {
                            textField: {
                                name: this.props.formName,
                                fullWidth: true,
                                inputProps: {
                                    placeholder: this.props.placeholder,
                                },
                                InputProps: {
                                    readOnly: this.props.isReadOnly
                                },
                                InputLabelProps: {
                                    shrink: true
                                }
                            }
                        } }
                        sx={ { svg: { color: COLORS.main }, input: { fontSize: '13px', padding: '10.5px' } } }
                        value={ this.props.value }
                    />
                )
        );
    }

    private renderDesktopDateInput(params: TextFieldProps) {
        return (
            <TextField
                { ...params }
                InputLabelProps={ { shrink: true } }
                inputProps={ {
                    ...params.inputProps,
                    placeholder: this.props.placeholder
                } }
                sx={ { svg: { color: COLORS.main }, input: { fontSize: '13px', padding: '12px' } } }
                fullWidth={ true }
            />
        );
    }

    public render() {
        return (
            <FormAndLabel label="">
                { this.renderDatePicker() }
            </FormAndLabel>
        );
    }
}