import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { DoubleDateInputView } from 'app/views/components/controls/forms/DoubleDateInputView/views';
import userEvent from '@testing-library/user-event';

describe('<DoubleDateInputView />', () => {
    let container: HTMLElement;
    const onValueFromChangeMock: jest.Mock = jest.fn();
    const onValueToChangeMock: jest.Mock = jest.fn();

    beforeEach(() => {
        container = render(<DoubleDateInputView
            periodFromValue={ new Date('01.01.2000') }
            periodToValue={ new Date('01.03.2000') }
            isReadOnly={ false }
            label="test_label"
            includeTime={ false }
            onValueFromChange={ onValueFromChangeMock }
            onValueToChange={ onValueToChangeMock }
        />).container;
    });

    it('renders', () => {
        expect(screen.getByText(/по/i)).toBeInTheDocument();
    });

    it('has onChange method', async () => {
        const inputElement = container.querySelector('[name=test_period_from_input]');

        if (inputElement) {
            await userEvent.type(inputElement, '01.02.2000');
            await userEvent.click(container.querySelectorAll('[type=button]')[0]);
        }
    });
});