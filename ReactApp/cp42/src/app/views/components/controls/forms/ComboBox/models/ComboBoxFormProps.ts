import { ComboboxItemModel } from 'app/views/components/controls/forms/ComboBox/models';
import { ValueAppliedEvent, ValueChangeEvent } from 'app/views/components/controls/forms/_types';
import React from 'react';

export type ComboBoxFormProps<TValue, Multiple extends boolean | undefined, TComboItem = ComboboxItemModel<TValue>> = {
    /**
     * Ссылка на компонент auto complete
     */
    autoCompleteKey?: string | number;

    /**
     * Ключ для компонента auto complete
     */
    autoCompleteRef?: React.Ref<any>;

    /**
     * Список элементов для выбора из combobox
     */
    items: Array<TComboItem>;

    /**
     * Если true, то combobox показан только для чтения
     */
    isReadOnly?: boolean;

    /**
     * Если true, то при загрузке (onMounted) combobox, будет установлен фокус
     */
    autoFocus?: boolean;

    /**
     * Название формы
     */
    formName: string;

    /**
     * Текст над combobox
     */
    label?: string;

    /**
     * Выбраное значение из списка
     */
    value: Multiple extends true ? Array<TValue> : TValue;

    /**
     * Возникает при выборе нового элемента из списка combobox
     * @param formName название формы, которое изменило значение
     * @param newValue новое значение
     * @param prevValue предыдущее значение
     */
    onValueChange?: ValueChangeEvent<Multiple extends true ? Array<TValue> : TValue>;

    onValueApplied?: ValueAppliedEvent<Multiple extends true ? Array<TValue> : TValue>;
};