import { hasComponentChangesFor } from 'app/common/functions';
import { ValueAppliedEvent, ValueChangeEvent } from 'app/views/components/controls/forms/_types';

import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { PercentTextFieldView } from 'app/views/components/controls/forms/mask-input/PercentInputForm/PercentTextFieldView/PercentTextFieldView';
import React from 'react';

export type PercentInputProps = {
    /**
     * Если `true`, то устанавливается фокус
     */
    autoFocus?: boolean;
    /**
     * Заголовок
     */
    label: string;

    /**
     * Название формы
     */
    formName: string;
    /**
     * Значение
     */
    value: number;

    /**
     * Начальное значение
     */
    defaultValue?: number;
    /**
     * Если `true`, то показывает маску ввода при патери фокуса
     */
    alwaysShowMask?: boolean;

    /**
     * Если `true`, то показывает маску ввода при вводе
     */
    showMask?: boolean;

    isReadOnly?: boolean;

    onValueChange?: ValueChangeEvent<number>;
    onValueApplied?: ValueAppliedEvent<number>;
};

type OwnState = {
    value: string;
    hasChanges: boolean;
};

export class PercentInputForm extends React.Component<PercentInputProps, OwnState> {
    public constructor(props: PercentInputProps) {
        super(props);

        this.onValueChange = this.onValueChange.bind(this);

        this.state = {
            value: `${ props.value ?? props.defaultValue ?? 0 }`,
            hasChanges: false
        };
    }

    public shouldComponentUpdate(nextProps: PercentInputProps, nextState: OwnState) {
        return hasComponentChangesFor(this.props, nextProps) ||
            hasComponentChangesFor(this.state, nextState);
    }

    private onValueChange(formName: string, newValue: string) {
        let resultValue = 0;

        if (newValue !== '') {
            let intValue = Number(newValue);
            if (intValue > 100) {
                intValue = 100;
            }

            resultValue = intValue;
        }

        this.setState(prevState => {
            if (this.props.onValueChange) {
                const prevValue = prevState.value ? parseInt(prevState.value.replace('%', ''), 10) : 0;
                const isSuccess = this.props.onValueChange(formName, resultValue, prevValue) ?? true;
                if (isSuccess && this.props.onValueApplied) {
                    this.props.onValueApplied(formName, resultValue);
                }
            }
            return {
                value: resultValue === 100 && prevState.value === '100'
                    ? '100%'
                    : resultValue === 100 && prevState.value === '100%'
                        ? '100'
                        : resultValue === 0 && prevState.value === '0'
                            ? '0%'
                            : resultValue === 0 && prevState.value === '0%'
                                ? '0'
                                : `${ resultValue }`
            };
        });
    }

    public render() {
        return (
            <FormAndLabel label="">
                <PercentTextFieldView { ...this.props } onChange={ this.onValueChange } />
            </FormAndLabel>
        );
    }
}