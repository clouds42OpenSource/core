declare const styles: {
  readonly 'root': string;
  readonly 'paper': string;
  readonly 'list': string;
  readonly 'listItem': string;
  readonly 'listItemSelected': string;
  readonly 'listItem-text': string;
  readonly 'label': string;
  readonly 'list-item-similar': string;
  readonly 'list-item-similar-selected': string;
};
export = styles;