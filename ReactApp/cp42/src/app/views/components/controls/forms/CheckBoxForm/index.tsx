import { hasComponentChangesFor } from 'app/common/functions';
import { CheckBoxFormView } from 'app/views/components/controls/forms/CheckBoxForm/views/CheckBoxFormView';
import { ValueAppliedEvent, ValueChangeEvent } from 'app/views/components/controls/forms/_types';
import { TextFontSize, TextFontWeight } from 'app/views/components/TextOut/views/TextView';
import React from 'react';

type OwnProps = {
    label: string;
    captionFontSize?: TextFontSize;
    captionFontWeight?: TextFontWeight;
    captionAsLabel?: boolean;
    trueText?: React.ReactNode;
    falseText?: React.ReactNode;
    isChecked?: boolean;
    formName: string;
    onValueChange?: ValueChangeEvent<boolean>;
    onValueApplied?: ValueAppliedEvent<boolean>;
    isReadOnly?: boolean;
    autoFocus?: boolean;
    className?: string;
    isSmallFormat?: boolean;
};

export class CheckBoxForm extends React.Component<OwnProps> {
    public shouldComponentUpdate(nextProps: OwnProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    public render() {
        const { formName } = this.props;
        const isChecked = this.props.isChecked ?? false;
        const isReadOnly = this.props.isReadOnly ?? false;
        const autoFocus = this.props.autoFocus ?? false;
        const label = this.props.label ?? '';
        const trueText = this.props.trueText ?? '';
        const falseText = this.props.falseText ?? '';
        const captionFontSize = this.props.captionFontSize ?? 13;
        const captionFontWeight = this.props.captionFontWeight ?? 400;
        const captionAsLabel = this.props.captionAsLabel ?? false;
        const { className } = this.props;
        const { isSmallFormat, onValueChange, onValueApplied } = this.props;

        return (
            <CheckBoxFormView
                isSmallFormat={ isSmallFormat }
                trueText={ trueText }
                falseText={ falseText }
                captionFontSize={ captionFontSize }
                captionFontWeight={ captionFontWeight }
                captionAsLabel={ captionAsLabel }
                isChecked={ isChecked }
                autoFocus={ autoFocus }
                formName={ formName }
                label={ label }
                className={ className }
                isReadOnly={ isReadOnly }
                onValueChange={ onValueChange }
                onValueApplied={ onValueApplied }
            />
        );
    }
}