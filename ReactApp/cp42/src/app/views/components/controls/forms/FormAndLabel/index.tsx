import { EPartnersTourId, ETourId } from 'app/views/Layout/ProjectTour/enums';
import React from 'react';
import { TextOut } from 'app/views/components/TextOut';
import cn from 'classnames';
import { hasComponentChangesFor } from 'app/common/functions';
import css from './styles.module.css';

type OwnProps = {
    label: React.ReactNode;
    forId?: string;
    className?: string;
    hiddenLabel?: boolean;
    isSmallFormat?: boolean;
    tourId?: EPartnersTourId | ETourId;
    children?: React.ReactNode;
    fullWidth?: boolean;
};

export class FormAndLabel extends React.Component<OwnProps> {
    public shouldComponentUpdate(nextProps: OwnProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    public render() {
        const { fullWidth, tourId, label, forId, hiddenLabel, isSmallFormat, className, children } = this.props;

        return (
            <div className={ cn({ [className ?? '']: true, [css.container]: !fullWidth, [css.wideContainer]: fullWidth }) } data-testid="form-and-label" data-tourid={ tourId }>
                { !hiddenLabel &&
                    <label className={ cn(css.label) } htmlFor={ forId }>
                        <TextOut fontSize={ isSmallFormat ? 12 : 13 } fontWeight={ isSmallFormat ? 400 : 600 }>
                            { label }
                        </TextOut>
                    </label>
                }
                <div>
                    { children }
                </div>
            </div>
        );
    }
}