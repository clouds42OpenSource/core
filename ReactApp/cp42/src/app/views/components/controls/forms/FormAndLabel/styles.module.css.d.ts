declare const styles: {
    readonly 'container': string;
    readonly 'label': string;
    readonly 'wideContainer': string;
};

export = styles;