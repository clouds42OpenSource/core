import './views/fixed.css';
import { hasComponentChangesFor } from 'app/common/functions';

import { ValueAppliedEvent, ValueChangeEvent } from 'app/views/components/controls/forms/_types';

import { MuiDateInputView } from 'app/views/components/controls/forms/MuiDateInputForm/views/MuiDateInputView';
import React from 'react';

type OwnProps = {
    formName: string;
    label?: string;
    className?: string;
    value?: Date | null;
    isReadOnly?: boolean;
    autoFocus?: boolean;
    placeholder?: string;
    onValueChange?: ValueChangeEvent<Date | null>;
    onValueApplied?: ValueAppliedEvent<Date | null>;
    includeTime?: boolean;
};

type OwnState = {
    selectedDay: Date | null;
};

export class MuiDateInputForm extends React.Component<OwnProps, OwnState> {
    public constructor(props: OwnProps) {
        super(props);

        this.onDateChange = this.onDateChange.bind(this);

        this.state = {
            selectedDay: !props.value ? null : props.value,
        };
    }

    public shouldComponentUpdate(nextProps: OwnProps, nextState: OwnState) {
        return hasComponentChangesFor(this.props, nextProps) ||
            hasComponentChangesFor(this.state, nextState);
    }

    private onDateChange(date: Date | [Date, Date] | null) {
        if (date instanceof Array) {
            throw new Error('Не разрешено использовать начальную и конечную дату, только выбранную.');
        }

        if (!this.props.onValueChange) {
            this.setState({
                selectedDay: date
            });
            return;
        }

        if (this.props.onValueChange(this.props.formName, date, this.state.selectedDay) ?? true) {
            this.setState({
                selectedDay: date
            }, () => {
                if (this.props.onValueApplied) {
                    this.props.onValueApplied(this.props.formName, date);
                }
            });
        }
    }

    public render() {
        return (
            <MuiDateInputView
                autoFocus={ this.props.autoFocus }
                className={ this.props.className }
                placeholder={ this.props.placeholder }
                formName={ this.props.formName }
                label={ this.props.label }
                value={ this.props.value }
                onDateChange={ this.onDateChange }
                isReadOnly={ this.props.isReadOnly }
                includeTime={ this.props.includeTime ?? false }
            />
        );
    }
}