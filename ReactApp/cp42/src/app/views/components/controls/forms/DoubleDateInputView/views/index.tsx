import EventIcon from '@mui/icons-material/Event';
import { Box } from '@mui/material';
import { DesktopDatePicker, DesktopDateTimePicker, LocalizationProvider, MobileDatePicker, MobileDateTimePicker, PickersInputComponentLocaleText } from '@mui/x-date-pickers';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { AppConsts } from 'app/common/constants';
import { TextOut } from 'app/views/components/TextOut';
import ruLocale from 'date-fns/locale/ru';
import React from 'react';

type OwnProps = {
    periodFromValue: Date | null;
    periodToValue: Date | null;
    isReadOnly: boolean;
    label: string;
    includeTime: boolean;
    isNeedToStretch?: boolean;
    onValueFromChange: (date: Date | [Date, Date] | null) => void;
    onValueToChange: (date: Date | [Date, Date] | null) => void;
    maxDateFrom?: Date;
};

/**
 * Представление, описывающее двойной блок для ввода даты
 */
export class DoubleDateInputView extends React.Component<OwnProps> {
    private readonly dateTimeFormat: string = '';

    private localeText: PickersInputComponentLocaleText<Date> = {
        nextMonth: 'Следующий месяц',
        previousMonth: 'Предыдущий месяц',
        cancelButtonLabel: 'Закрыть',
        clearButtonLabel: 'Очистить'
    };

    constructor(props: OwnProps) {
        super(props);
        this.renderToMobile = this.renderToMobile.bind(this);
        this.renderToDesktop = this.renderToDesktop.bind(this);
        this.renderFromDesktop = this.renderFromDesktop.bind(this);
        this.renderFromMobile = this.renderFromMobile.bind(this);
        this.onDateToChange = this.onDateToChange.bind(this);
        this.onDateFromChange = this.onDateFromChange.bind(this);
        this.renderFromInput = this.renderFromInput.bind(this);
        this.renderToInput = this.renderToInput.bind(this);
        this.dateTimeFormat = this.props.includeTime ? 'dd.MM.yyyy HH:mm' : 'dd.MM.yyyy';
    }

    private onDateFromChange(date: Date | null, ev: { validationError: string | null }) {
        if (ev.validationError) {
            return;
        }

        if (this.props.onValueFromChange) {
            this.props.onValueFromChange(date);
        }
    }

    private onDateToChange(date: Date | null, ev: { validationError: string | null }) {
        if (ev.validationError) {
            return;
        }

        if (this.props.onValueToChange) {
            this.props.onValueToChange(date);
        }
    }

    private get openPickerIcon() {
        // eslint-disable-next-line react/no-unstable-nested-components
        return () => <EventIcon color="primary" />;
    }

    private renderFromInput() {
        const isMobileSized = window.innerWidth <= AppConsts.mobileScreenWidth;

        return !isMobileSized ? this.renderFromDesktop() : this.renderFromMobile();
    }

    private renderToInput() {
        const isMobileSized = window.innerWidth <= AppConsts.mobileScreenWidth;

        return !isMobileSized ? this.renderToDesktop() : this.renderToMobile();
    }

    private renderFromMobile() {
        const { isReadOnly, label, periodFromValue, includeTime, maxDateFrom } = this.props;

        if (includeTime) {
            return (
                <MobileDateTimePicker
                    localeText={ this.localeText }
                    format={ this.dateTimeFormat }
                    disabled={ isReadOnly }
                    label={ label }
                    onChange={ this.onDateFromChange }
                    value={ periodFromValue }
                    maxDate={ maxDateFrom }
                    slotProps={ {
                        actionBar: {
                            actions: ['clear', 'cancel', 'accept']
                        }
                    } }
                />
            );
        }

        return (
            <MobileDatePicker
                localeText={ this.localeText }
                format={ this.dateTimeFormat }
                disabled={ isReadOnly }
                label={ label }
                onChange={ this.onDateFromChange }
                value={ periodFromValue }
                maxDate={ maxDateFrom }
                slotProps={ {
                    actionBar: {
                        actions: ['clear', 'cancel', 'accept']
                    }
                } }
            />
        );
    }

    private renderFromDesktop() {
        const { isReadOnly, label, periodFromValue, includeTime, maxDateFrom } = this.props;

        if (includeTime) {
            return (
                <DesktopDateTimePicker
                    localeText={ this.localeText }
                    format={ this.dateTimeFormat }
                    readOnly={ isReadOnly }
                    label={ label }
                    onChange={ this.onDateFromChange }
                    slots={ { openPickerIcon: this.openPickerIcon } }
                    value={ periodFromValue }
                    maxDate={ maxDateFrom }
                    slotProps={ {
                        actionBar: {
                            actions: ['clear', 'accept']
                        }
                    } }
                />
            );
        }

        return (
            <DesktopDatePicker
                sx={ { transform: '-moz-initial' } }
                localeText={ this.localeText }
                format={ this.dateTimeFormat }
                readOnly={ isReadOnly }
                label={ label }
                onChange={ this.onDateFromChange }
                value={ periodFromValue }
                slots={ { openPickerIcon: this.openPickerIcon } }
                maxDate={ maxDateFrom }
                slotProps={ {
                    actionBar: {
                        actions: ['clear', 'accept']
                    }
                } }
            />
        );
    }

    private renderToMobile() {
        const { isReadOnly, periodToValue, includeTime } = this.props;

        if (includeTime) {
            return (
                <MobileDateTimePicker
                    localeText={ this.localeText }
                    format={ this.dateTimeFormat }
                    label={ null }
                    disabled={ isReadOnly }
                    onChange={ this.onDateToChange }
                    value={ periodToValue }
                    slotProps={ {
                        actionBar: {
                            actions: ['clear', 'cancel', 'accept']
                        }
                    } }
                />
            );
        }

        return (
            <MobileDatePicker
                localeText={ this.localeText }
                format={ this.dateTimeFormat }
                label={ null }
                disabled={ isReadOnly }
                onChange={ this.onDateToChange }
                value={ periodToValue }
                slotProps={ {
                    actionBar: {
                        actions: ['clear', 'cancel', 'accept']
                    }
                } }
            />
        );
    }

    private renderToDesktop() {
        const { isReadOnly, periodToValue, includeTime } = this.props;

        if (includeTime) {
            return (
                <DesktopDateTimePicker
                    localeText={ this.localeText }
                    slots={ { openPickerIcon: this.openPickerIcon } }
                    format={ this.dateTimeFormat }
                    readOnly={ isReadOnly }
                    onChange={ this.onDateToChange }
                    label={ null }
                    value={ periodToValue }
                    slotProps={ {
                        actionBar: {
                            actions: ['clear', 'accept']
                        }
                    } }
                />
            );
        }

        return (
            <DesktopDatePicker
                localeText={ this.localeText }
                slots={ { openPickerIcon: this.openPickerIcon } }
                format={ this.dateTimeFormat }
                readOnly={ isReadOnly }
                onChange={ this.onDateToChange }
                label={ null }
                value={ periodToValue }
                slotProps={ {
                    actionBar: {
                        actions: ['clear', 'accept']
                    }
                } }
            />
        );
    }

    public render() {
        return (
            <LocalizationProvider dateAdapter={ AdapterDateFns } adapterLocale={ ruLocale }>
                <Box
                    style={ {
                        display: 'grid',
                        gridTemplateColumns: '7fr 1fr 7fr',
                        alignItems: 'center',
                        gap: '5px',
                        justifyItems: this.props.isNeedToStretch ? 'stretch' : 'center'
                    } }
                >
                    { this.renderFromInput() }
                    <TextOut style={ this.props.isNeedToStretch ? { textAlign: 'center' } : undefined }>по</TextOut>
                    { this.renderToInput() }
                </Box>
            </LocalizationProvider>
        );
    }
}