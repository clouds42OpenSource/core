declare const styles: {
    readonly 'file-uploader': string;
    readonly 'file-uploader-button': string;
    readonly 'input-text': string;
    readonly inputUpload: string;
    readonly textField: string;
    readonly container: string;
};
export = styles;