import '@pathofdev/react-tag-input/build/index.css';
import { Chip, TextField } from '@mui/material';
import { Box } from '@mui/system';
import { ValidationEvent } from 'app/common/types';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';

import { ValidationProps, withValidationContainer } from 'app/views/components/controls/forms/withValidationContainer';
import { generateTextFromTextBoxFor } from 'app/views/components/utils/generateTextFromTextBoxFor';

import React from 'react';

type OwnProps = ValidationProps & {
    tags: string[];
    label: string;
    formName: string;
    onTagsChanged?: (formName: string, newTags: string[], prevTags: string[]) => void;
    isReadOnly?: boolean;
    placeholder?: string;
};

type TState = {
    input: string;
};

class TagInputFormClass extends React.Component<OwnProps, TState> {
    public constructor(props: OwnProps) {
        super(props);

        this.onKeyDown = this.onKeyDown.bind(this);
        this.onValidation = this.onValidation.bind(this);
        this.onChange = this.onChange.bind(this);
        this.deleteChips = this.deleteChips.bind(this);

        this.state = { input: '' };
    }

    private onValidation(value: string, prevValue: string, validationEvent: ValidationEvent = 'onBlur') {
        const { isValid, tags, formName } = this.props;

        return isValid({ value, prevValue, formName, validationEvent, state: tags });
    }

    private onKeyDown({ preventDefault, key, target, defaultPrevented }: React.KeyboardEvent<HTMLDivElement>) {
        const { input } = this.state;
        const isValid = this.onValidation(input, '');

        if (isValid) {
            const buttonKey = key.toLowerCase();
            const { tags } = this.props;

            if (['enter', 'tab'].includes(buttonKey) && input) {
                this.onChange([...this.props.tags, input]);
                this.setState({ input: '' });
            } else if (['backspace', 'delete'].includes(buttonKey) || buttonKey.length === 1) {
                const { selectionStart, selectionEnd, value: prevValue } = target as HTMLInputElement;
                const deleteCharKey = buttonKey === 'delete' ? 'delete_46' : buttonKey === 'backspace' ? 'backspace_8' : undefined;
                const value = generateTextFromTextBoxFor(prevValue, selectionStart ?? 0, selectionEnd ?? 0, deleteCharKey, buttonKey);

                preventDefault();

                console.log(defaultPrevented);

                if (!defaultPrevented && !this.onValidation(value, prevValue, 'onChange')) {
                    preventDefault();
                }

                this.onChange([...tags, input]);
                this.setState({ input: '' });
            }
        }
    }

    private onChange(newTags: string[]) {
        const { formName, tags: prevTags, onTagsChanged } = this.props;

        if (onTagsChanged) {
            onTagsChanged(formName, newTags, prevTags);
        }
    }

    private deleteChips(tag: string) {
        this.onChange(this.props.tags.filter(item => item !== tag));
    }

    public render() {
        const { isReadOnly, placeholder, label, tags } = this.props;

        return (
            <FormAndLabel label={ label }>
                <TextField
                    data-testid="tag-input-form-view"
                    onKeyDown={ e => this.onKeyDown(e) }
                    fullWidth={ true }
                    disabled={ isReadOnly }
                    value={ this.state.input }
                    onChange={ e => this.setState({ input: e.target.value }) }
                    placeholder={ placeholder }
                    InputProps={ {
                        style: { display: 'flex' },
                        startAdornment: !!tags.length && (
                            <Box display="flex" gap="5px">
                                { tags.map(item => (
                                    <Chip
                                        color="primary"
                                        key={ item }
                                        label={ item }
                                        onClick={ () => this.deleteChips(item) }
                                    />
                                )) }
                            </Box>
                        )
                    } }
                />
            </FormAndLabel>
        );
    }
}

export const TagInputForm = withValidationContainer(TagInputFormClass);