import { generateUniqueId } from 'app/common/functions';
import { TextWithTooltipProps } from 'app/views/components/controls/forms/Tooltips/types';
import { TextOut } from 'app/views/components/TextOut';
import React from 'react';
import Tooltip from '@mui/material/Tooltip';
import cn from 'classnames';
import css from './style.module.css';

type TextWithTooltipViewState = {
    tooltipOpen: boolean
};

export class TextWithTooltipView extends React.Component<TextWithTooltipProps, TextWithTooltipViewState> {
    private readonly tooltipId: string;

    constructor(props: TextWithTooltipProps) {
        super(props);
        this.toggleTooltip = this.toggleTooltip.bind(this);
        this.getTextOrComponent = this.getTextOrComponent.bind(this);
        this.tooltipId = generateUniqueId('tooltip-for');
        this.state = {
            tooltipOpen: false
        };
    }

    private getTextOrComponent() {
        const { text } = this.props;

        if (typeof text === 'string') {
            return (
                <TextOut inheritColor={ true }>
                    { this.props.text }
                </TextOut>
            );
        }

        return text;
    }

    private toggleTooltip() {
        this.setState(prevState => ({
            tooltipOpen: !prevState.tooltipOpen
        }));
    }

    public render() {
        return (
            <Tooltip
                data-testid="text-with-tooltip-view"
                placement={ this.props.tooltipPlacement }
                id={ this.tooltipId }
                className={ cn(css['tooltipe-code'], { [css.small]: this.props.isSmall }) }
                title={ this.props.tooltipText }
            >
                <span id={ this.tooltipId }>
                    { this.getTextOrComponent() }
                </span>
            </Tooltip>
        );
    }
}