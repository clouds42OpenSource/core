import { TextField } from '@mui/material';
import { hasComponentChangesFor } from 'app/common/functions';
import { PercentInputProps } from 'app/views/components/controls/forms/mask-input/PercentInputForm';
import React from 'react';

type OwnProps = PercentInputProps & {
    onChange: (formName: string, newValue: string) => void
}
export class PercentTextFieldView extends React.Component<OwnProps> {
    constructor(props: OwnProps) {
        super(props);
        this.onChange = this.onChange.bind(this);
    }

    shouldComponentUpdate(nextProps: OwnProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    onChange(ev: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) {
        const newValue = ev.target.value;
        const { formName } = this.props;
        if (Number(newValue) < 0) { return; }
        this.props.onChange(formName, newValue);
    }

    render() {
        const { label, isReadOnly, value, formName, autoFocus } = this.props;
        return (
            <TextField
                fullWidth={ true }
                label={ label }
                type="number"
                name={ formName }
                autoFocus={ autoFocus }
                value={ value }
                onChange={ this.onChange }
                inputProps={ { sx: { fontSize: '13px', padding: '10px' }, } }
                InputProps={ { startAdornment: '%', readOnly: isReadOnly } }
            />
        );
    }
}