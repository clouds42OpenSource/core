import { FormControl, TextField } from '@mui/material';
import { hasComponentChangesFor, mergeRefs } from 'app/common/functions';
import { ValueAppliedEvent, ValueChangeEvent } from 'app/views/components/controls/forms/_types';

import { generateUniqueId } from 'app/common/functions/generateUniqueId';
import { COLORS } from 'app/utils';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { TextBoxFormType } from 'app/views/components/controls/forms/TextBoxForm/types';
import cn from 'classnames';
import React from 'react';
import css from '../styles.module.css';

type OwnProps = {
    showErrorText?: boolean
    className?: string;
    formAndLabelClassName?: string;
    type: TextBoxFormType;
    value: string;
    isReadOnly: boolean;
    autoFocus: boolean;
    maxLength?: number;
    formName: string;
    label?: React.ReactNode;
    hiddenLabel?: boolean;
    placeholder?: string;
    hasError: boolean;
    medium?: boolean;
    inputRef?: React.Ref<HTMLInputElement>;
    onValueChange?: ValueChangeEvent<string>;
    onValueApplied?: ValueAppliedEvent<string>;
    disabled?: boolean;
    helperText?: string;
    autoComplete?: string;
    onBlur: React.FocusEventHandler<HTMLInputElement>;
    onKeyDown: React.KeyboardEventHandler<HTMLInputElement>;
    onPaste: React.ClipboardEventHandler<HTMLInputElement>;
    fullWidth?: boolean;
    error?: boolean;
};

type OwnState = {
    selectionStart: number;
};

export class TextBoxFormView extends React.Component<OwnProps, OwnState> {
    private inputElRef = React.createRef<HTMLInputElement>();

    private waitTimerId = 0;

    private id = generateUniqueId('textbox_form_');

    private readonly beforeValueAppliedTimeoutInMilliseconds = 700;

    constructor(props: OwnProps) {
        super(props);
        this.onChange = this.onChange.bind(this);
        this.waitForTimeBeforeValueAppliedEvent = this.waitForTimeBeforeValueAppliedEvent.bind(this);
        this.preserveCursor = this.preserveCursor.bind(this);
        this.setCursor = this.setCursor.bind(this);

        this.state = {
            selectionStart: 1
        };
    }

    public componentDidMount() {
        this.preserveCursor();
    }

    public shouldComponentUpdate(nextProps: OwnProps, nextState: OwnState) {
        return hasComponentChangesFor(this.props, nextProps) || hasComponentChangesFor(this.state, nextState);
    }

    public componentDidUpdate() {
        this.setCursor();
    }

    private onChange({ target: { value } }: React.ChangeEvent<HTMLInputElement>): boolean | void {
        const { formName, value: oldValue, onValueChange } = this.props;

        if (onValueChange) {
            return onValueChange(formName, value, oldValue);
        }

        return true;
    }

    private setLabel(element: React.JSX.Element) {
        const hiddenLabel = this.props.hiddenLabel ?? false;

        return (
            <FormAndLabel label="" forId={ this.id } hiddenLabel={ hiddenLabel } className={ this.props.formAndLabelClassName }>
                { element }
                { this.props.showErrorText && <p style={ { color: COLORS.error, fontSize: '13px' } }>Недопустимое значение</p> }
            </FormAndLabel>
        );
    }

    private setCursor() {
        this.inputElRef.current!.setSelectionRange(
            this.state.selectionStart,
            this.state.selectionStart,
        );
    }

    private preserveCursor() {
        this.setState({ selectionStart: this.inputElRef.current!.selectionStart! });
    }

    private waitForTimeBeforeValueAppliedEvent(event: React.ChangeEvent<HTMLInputElement>) {
        this.preserveCursor();

        const isSuccess = this.onChange(event) ?? true;

        if (!isSuccess || !this.props.onValueApplied) {
            return;
        }

        if (this.waitTimerId) {
            clearTimeout(this.waitTimerId);
            this.waitTimerId = 0;
        }

        this.waitTimerId = setTimeout(this.props.onValueApplied, this.beforeValueAppliedTimeoutInMilliseconds, this.props.formName, event.target.value);
    }

    public render() {
        const { maxLength, autoFocus, isReadOnly, formName, label, onBlur, onKeyDown, onPaste, value, placeholder, disabled, helperText, error } = this.props;

        const inputElement = (
            <FormControl fullWidth={ true }>
                <TextField
                    fullWidth={ this.props.fullWidth }
                    multiline={ this.props.type === 'textarea' }
                    InputLabelProps={ { shrink: true } }
                    variant="outlined"
                    color="primary"
                    rows={ 4 }
                    label={ label }
                    id={ this.id }
                    name={ formName }
                    inputProps={ {
                        maxLength,
                        autoFocus,
                        readOnly: isReadOnly,
                        className: this.props.className,
                        autoComplete: this.props.autoComplete,
                    } }
                    sx={ this.props.type === 'password'
                        ? {
                            '& input': {
                                textSecurity: 'disc',
                                MozTextSecurity: 'disc',
                                WebkitTextSecurity: 'disc',
                            },
                        } : this.props.type === 'textarea' ? {
                            '& .MuiInputBase-multiline': {
                                padding: '8px',
                            }
                        }
                            : undefined
                    }
                    error={ error }
                    helperText={ helperText }
                    inputRef={ mergeRefs(this.inputElRef, this.props.inputRef) }
                    size={ this.props.medium ? 'medium' : 'small' }
                    hiddenLabel={ this.props.hiddenLabel }
                    value={ value }
                    placeholder={ placeholder }
                    onChange={ this.waitForTimeBeforeValueAppliedEvent }
                    onBlur={ onBlur }
                    onKeyDown={ onKeyDown }
                    onPaste={ onPaste }
                    disabled={ disabled }
                    className={ cn({
                        [css['form-control']]: true,
                        [css['form-control-error']]: this.props.hasError
                    }, this.props.className) }
                />
            </FormControl>
        );

        return this.setLabel(inputElement);
    }
}