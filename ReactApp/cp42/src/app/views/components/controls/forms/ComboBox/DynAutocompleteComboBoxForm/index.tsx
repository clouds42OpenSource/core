import { hasComponentChangesFor } from 'app/common/functions';
import { AutocompleteComboBoxFormProps } from 'app/views/components/controls/forms/ComboBox/autocomplete/models';
import { AutocompleteComboBoxForm } from 'app/views/components/controls/forms/ComboBox/AutocompleteComboBoxForm';
import { ComboboxItemModel } from 'app/views/components/controls/forms/ComboBox/models';
import React from 'react';

type OwnProps<TOriginalItem, TValue> = Omit<AutocompleteComboBoxFormProps<TValue, false>, 'raiseInputValueChangeEvents' | 'isLoading' | 'noItemsText' | 'items' | 'onValueChange'> & {
    onGetItems: (searchLine: string, setItems: (items: Array<TOriginalItem>) => void) => void;
    onValueChange: (formName: string, originalItems: Array<TOriginalItem>, newValue: TValue, prevValue: TValue) => boolean | void;
    originalItemToComboboxItemModel: (item: TOriginalItem) => ComboboxItemModel<TValue>;
};

type OwnState<TOriginalItem> = {
    items: Array<TOriginalItem>;
    isLoading: boolean;
    noItemsText: string;
};

export class DynAutocompleteComboBoxForm<TOriginalItem, TValue> extends React.Component<OwnProps<TOriginalItem, TValue>, OwnState<TOriginalItem>> {
    public constructor(props: OwnProps<TOriginalItem, TValue>) {
        super(props);
        this.onInputValueChange = this.onInputValueChange.bind(this);
        this.getNoItemsText = this.getNoItemsText.bind(this);
        this.setDynItems = this.setDynItems.bind(this);
        this.onValueChange = this.onValueChange.bind(this);

        this.state = {
            items: [],
            isLoading: false,
            noItemsText: this.getNoItemsText('')
        };
    }

    public shouldComponentUpdate(nextProps: OwnProps<TOriginalItem, TValue>, nextState: OwnState<TOriginalItem>) {
        return hasComponentChangesFor(this.props, nextProps) ||
            hasComponentChangesFor(this.state, nextState);
    }

    private onValueChange(formName: string, newValue: TValue, prevValue: TValue) {
        if (this.props.onValueChange) {
            this.props.onValueChange(formName, this.state.items, newValue, prevValue);
        }
    }

    private onInputValueChange(_formName: string, newValue: string, prevValue: string) {
        if (this.state.isLoading) {
            return;
        }

        const noItemsText = this.getNoItemsText(newValue);

        if (newValue.length < 3) {
            this.setState({
                items: [],
                noItemsText
            });
            return;
        }

        if (this.props.onInputValueChange) {
            this.props.onInputValueChange(_formName, newValue, prevValue);
        }

        this.setState({
            isLoading: true,
            noItemsText
        });
        this.props.onGetItems(newValue, this.setDynItems);
    }

    private setDynItems(items: Array<TOriginalItem>) {
        this.setState({
            items,
            isLoading: false
        });
    }

    private getNoItemsText(inputValue: string) {
        let noItemsText = 'Совпадений не найдено';

        if (inputValue.length < 3) {
            const leftSymbols = 3 - inputValue.length;

            if (leftSymbols >= 2) {
                noItemsText = `Пожалуйста, введите еще хотя бы ${ leftSymbols } символa`;
            } else if (leftSymbols === 1) {
                noItemsText = 'Пожалуйста, введите еще хотя бы 1 символ';
            }
        }
        return noItemsText;
    }

    public render() {
        const {
            children, isReadOnly, label, value, formName,
            onValueApplied, onInputValueApplied,
            autoFocus, placeholder, loadingText, autoCompleteKey, autoCompleteRef
        } = this.props;

        const sortedItems = this.state.items
            .map(item => this.props.originalItemToComboboxItemModel(item))
            .sort((firstItem, secondItem) => (firstItem.text.localeCompare(secondItem.text)));

        return (
            <AutocompleteComboBoxForm
                key={ autoCompleteKey }
                formName={ formName }
                isReadOnly={ isReadOnly }
                items={ sortedItems }
                onValueChange={ this.onValueChange }
                onValueApplied={ onValueApplied }
                onInputValueChange={ this.onInputValueChange }
                onInputValueApplied={ onInputValueApplied }
                autoFocus={ autoFocus }
                raiseInputValueChangeEvents={ true }
                ref={ autoCompleteRef }
                label={ label }
                value={ value }
                placeholder={ placeholder }
                isLoading={ this.state.isLoading }
                loadingText={ loadingText ?? 'Идёт поиск...' }
                noItemsText={ this.state.noItemsText }
                className={ this.props.className }
                popperClassName={ this.props.popperClassName }
                isClearable={ this.props.isClearable }
                inputValue={ this.props.inputValue }
                isWithCustomSearch={ this.props.isWithCustomSearch }
            >
                { children }
            </AutocompleteComboBoxForm>
        );
    }
}