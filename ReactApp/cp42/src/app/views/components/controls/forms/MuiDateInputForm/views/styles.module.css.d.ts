declare const styles: {
  readonly 'min-date-control': string;
  readonly 'calendar-icon-container': string;
};
export = styles;