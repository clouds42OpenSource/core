export enum DualListStateListTypes {
    leftList = 'leftList',
    rightList = 'rightList'
}
export type DualListStateListTypesNames = keyof typeof DualListStateListTypes;

export enum DualListSearchInputFormType {
    leftListSearchString = 'leftListSearchString',
    rightListSearchString = 'rightListSearchString'
}
export type DualListSearchInputFormTypeNames = keyof typeof DualListSearchInputFormType;

export type DualListItem<TKey> = {
    key: TKey
    label: string,
    isSimilar?: boolean
};

export type DualListBoxFormProps<TKey> = {
    allowSelectAll?: boolean;
    isFormDisabled: boolean;
    leftListItems: Array<DualListItem<TKey>>;
    rightListItems: Array<DualListItem<TKey>>;
    mainList: DualListStateListTypes;
    leftListLabel: string;
    rightListLabel: string;
    formName: string;
    onValueChange?: (fieldName: string, newLeftValue: Array<DualListItem<TKey>>, newRightValue: Array<DualListItem<TKey>>, oldLeftValue: Array<DualListItem<TKey>>, oldRightValue: Array<DualListItem<TKey>>, isAllSelect?: boolean) => boolean;
};