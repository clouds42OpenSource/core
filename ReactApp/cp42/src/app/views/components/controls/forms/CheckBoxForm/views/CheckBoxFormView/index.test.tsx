import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { CheckBoxFormView } from 'app/views/components/controls/forms/CheckBoxForm/views/CheckBoxFormView';

describe('<CheckBoxFormViewer />', () => {
    it('renders with caption as label', () => {
        render(<CheckBoxFormView
            trueText="test_true_text"
            falseText="test_false_text"
            captionFontSize={ 12 }
            captionFontWeight={ 400 }
            captionAsLabel={ true }
            isChecked={ false }
            isReadOnly={ false }
            autoFocus={ false }
            formName="test_form_name"
            label="test_label"
        />);
        expect(screen.getByTestId(/checkbox-form-view/i)).toBeInTheDocument();
        expect(screen.getByText(/test_false_text/i).parentElement!.hasAttribute('for'));
    });

    it('renders with caption as text', () => {
        render(<CheckBoxFormView
            trueText="test_true_text"
            falseText="test_false_text"
            captionFontSize={ 12 }
            captionFontWeight={ 400 }
            captionAsLabel={ false }
            isChecked={ true }
            isReadOnly={ false }
            autoFocus={ false }
            formName="test_form_name"
            label="test_label"
        />);
        expect(screen.getByTestId(/checkbox-form-view/i)).toBeInTheDocument();
        expect(!screen.getByText(/test_true_text/i).parentElement!.hasAttribute('for'));
    });
});