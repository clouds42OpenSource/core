import React, { Component, createRef } from 'react';
import { InputFileUploaderProps } from 'app/views/components/controls/forms/InputFileUploaderForm/types';
import { TextField } from '@mui/material';
import cn from 'classnames';
import css from './styles.module.css';

type OwnState = {
    /**
     * Название файла
     */
    fileName: string
};

export class InputFileUploaderForm extends Component<InputFileUploaderProps, OwnState> {
    private _inputFileRef = createRef<HTMLInputElement>();

    public constructor(props: InputFileUploaderProps) {
        super(props);

        this.state = {
            fileName: 'Выберите файл'
        };

        this.handleFileInput = this.handleFileInput.bind(this);
        this.validateUploadedFileOnAcceptedType = this.validateUploadedFileOnAcceptedType.bind(this);
    }

    public componentDidUpdate(nextProps: InputFileUploaderProps) {
        const { defaultContentName } = this.props;

        if (defaultContentName && nextProps.defaultContentName !== defaultContentName) {
            if (defaultContentName) {
                this.setState({ fileName: defaultContentName });
            }
        }
    }

    /**
     * Функция для получения файла и для валидации
     * @param event На получение файла
     */
    private handleFileInput(event: React.ChangeEvent<HTMLInputElement>) {
        let file = {} as File;

        if (event.currentTarget !== null && event.currentTarget.files) {
            file = event.currentTarget.files[0];
            const fileType = file?.name.split('.').pop();

            if (this.validateUploadedFileOnAcceptedType(fileType)) {
                this.setState({
                    fileName: file.name
                });
                this.props.onFileSelectSuccess(file);
            }
        }
    }

    /**
     * Проверка по типу файла
     * @param fileType Тип загруженного файла
     */
    private validateUploadedFileOnAcceptedType(fileType?: string) {
        let isValidType = true;

        if (this.props.accept) {
            const acceptedTypes = this.props.accept.split(',').filter(item => item !== '');
            isValidType = false;

            for (let index = 0; index < acceptedTypes.length; index++) {
                if (fileType && acceptedTypes[index].replace('.', '') === fileType) {
                    isValidType = true;
                }
            }

            if (!isValidType) {
                this.setState({
                    fileName: 'Выберите файл'
                });
                this.props.onFileSelectError(`Выберите другой файл. Допустимые форматы для файла: ${ this.props.accept }`);
            }
        }

        return isValidType;
    }

    public render() {
        return (
            <div className={ cn(css.container) }>
                <TextField
                    type="file"
                    style={ { display: 'none' } }
                    onChange={ this.handleFileInput }
                    inputProps={ { accept: this.props.accept, hidden: true } }
                    inputRef={ this._inputFileRef }
                />
                <TextField
                    fullWidth={ true }
                    InputLabelProps={ { shrink: true } }
                    label={ this.props.label }
                    disabled={ true }
                    value={ this.state.fileName }
                    InputProps={ { className: cn(css['file-uploader']) } }
                />
                <button
                    onClick={ () => this._inputFileRef.current && this._inputFileRef.current.click() }
                    type="button"
                    className={ cn(css['file-uploader-button']) }
                >
                    <i className="fa fa-folder-open" aria-hidden="true" />
                </button>
            </div>
        );
    }
}