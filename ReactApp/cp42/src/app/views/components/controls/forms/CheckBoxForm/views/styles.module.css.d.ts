declare const styles: {
    readonly 'form-check-input-small': string;
    readonly 'form-check-input': string;
    readonly 'caption': string;
};
export = styles;