declare const styles: {
    readonly 'form-control-error': string;
    readonly 'form-control': string;
    readonly 'input-group-addon-sm': string;
};

export = styles;