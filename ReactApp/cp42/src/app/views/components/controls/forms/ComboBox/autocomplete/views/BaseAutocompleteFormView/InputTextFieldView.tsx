import '../fixed.css';
import { AutocompleteRenderInputParams, CircularProgress, TextField } from '@mui/material';
import React from 'react';
import cn from 'classnames';
import { hasComponentChangesFor } from 'app/common/functions';
import css from '../styles.module.css';

type OwnProps = {
    formName: string;
    placeholder?: string;
    params: AutocompleteRenderInputParams;
    autoFocus?: boolean;
    isLoading?: boolean;
    label?: string;
    value?: string;
};

export class InputTextFieldView extends React.Component<OwnProps> {
    public shouldComponentUpdate(nextProps: OwnProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    public render() {
        const { params } = this.props;
        return (
            <TextField
                { ...params }
                value={ this.props.value }
                name={ this.props.formName }
                className={ cn(css['input-style']) }
                placeholder={ this.props.placeholder }
                autoFocus={ this.props.autoFocus }
                variant="outlined"
                label={ this.props.label }
                InputLabelProps={ { shrink: true } }
                InputProps={ {
                    ...params.InputProps,
                    endAdornment: (
                        <>
                            { this.props.isLoading ? <CircularProgress style={ { marginRight: '40px' } } color="inherit" size={ 20 } /> : null }
                            { params.InputProps.endAdornment }
                        </>
                    ),
                } }
            />
        );
    }
}