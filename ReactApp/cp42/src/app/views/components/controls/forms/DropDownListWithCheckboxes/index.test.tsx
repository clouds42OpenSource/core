import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import userEvent from '@testing-library/user-event';
import { DropDownListWithCheckboxes } from 'app/views/components/controls/forms/DropDownListWithCheckboxes';
import { DropDownListWithCheckboxItem } from 'app/views/components/controls/forms/DropDownListWithCheckboxes/types';

describe('<DropDownListWithCheckboxes />', () => {
    const onValueChangeMock: jest.Mock = jest.fn();

    beforeEach(() => {
        render(<DropDownListWithCheckboxes
            items={ Array<DropDownListWithCheckboxItem>({ value: '1', checked: false, text: 'test_text_1' }) }
            label="test_label"
            formName="test_form_name"
            onValueChange={ onValueChangeMock }
        />);
    });

    it('renders', () => {
        expect(screen.getByTestId(/dropdown-list-with-checkboxes/i)).toBeInTheDocument();
    });

    it('has onChange method', async () => {
        const openList: HTMLDivElement = screen.getByRole(/combobox/i);
        await userEvent.click(openList);
        await userEvent.click(screen.getByRole(/menuitem/i));
        expect(onValueChangeMock).toBeCalled();
    });
});