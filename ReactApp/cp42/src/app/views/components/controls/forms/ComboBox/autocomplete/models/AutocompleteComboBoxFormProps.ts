import { ComboBoxFormProps } from 'app/views/components/controls/forms/ComboBox/models';
import { ValueAppliedEvent, ValueChangeEvent } from 'app/views/components/controls/forms/_types';
import React from 'react';

export type AutocompleteComboBoxFormProps<TValue, Multiple extends boolean | undefined> = ComboBoxFormProps<TValue, Multiple> & {
    /**
     * Возникает при наборе текста списка combobox
     * @param formName название формы, которое изменило значение
     * @param newValue новое значение введённого текста
     * @param prevValue предыдущее значение введённого текста
     */
    onInputValueChange?: ValueChangeEvent<string>;

    /**
     * Возникает при принятии введённого текста
     * @param formName название формы, которое изменило значение
     * @param value Введённое значение текста
     */
    onInputValueApplied?: ValueAppliedEvent<string>;

    /**
     * Если true, то будут вызываться события на ввод текста: onInputValueChange и onInputValueApplied
     */
    raiseInputValueChangeEvents?: boolean;

    /**
     * Placeholder
     */
    placeholder?: string;

    /**
     * Индикатор того, что идёт загрузка
     */
    isLoading?: boolean;

    /**
     * Текст загрузки
     */
    loadingText?: React.ReactNode;

    /**
     * Текст в списке, когда нет элементов
     */
    noItemsText?: React.ReactNode;

    /**
     * Название класса
     */
    className?: string;

    /**
     * Название класса всплывающей менюшки
     */
    popperClassName?: string;

    /**
     * Поле для вывода кнопки очистки
     */
    isClearable?: boolean;

    /**
     * Поле поиска
     */
    inputValue?: string;

    /**
     * Флаг для поиска с кастомным значением и получения массива элементов
     */
    isWithCustomSearch?: boolean;
};