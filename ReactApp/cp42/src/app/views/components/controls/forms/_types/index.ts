import { ComboboxItemModel } from 'app/views/components/controls/forms/ComboBox/models';

export type ValueChangeEvent<TValue> = (formName: string, newValue: TValue, prevValue: TValue) => boolean | void;
export type ValueAppliedEvent<TValue> = (formName: string, value: TValue, items?: ComboboxItemModel<string>[]) => void;