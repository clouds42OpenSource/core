import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { TextWithTooltipView } from 'app/views/components/controls/forms/Tooltips/views/TextWithTooltipView';
import { TooltipPlacementTypes } from 'app/views/components/controls/forms/Tooltips/types';

describe('<TextWithTooltipView />', () => {
    it('renders text', () => {
        render(<TextWithTooltipView
            text="test_text"
            tooltipText="test_tooltip_text"
            tooltipPlacement={ TooltipPlacementTypes.rightStart }
        />);
        expect(screen.getByText(/test_text/i)).toBeInTheDocument();
    });

    it('renders JSX Element', () => {
        render(<TextWithTooltipView
            text={ <div data-testid="test_div" /> }
            tooltipText="test_tooltip_text"
            tooltipPlacement={ TooltipPlacementTypes.rightStart }
        />);
        expect(screen.getByTestId(/test_div/i)).toBeInTheDocument();
    });
});