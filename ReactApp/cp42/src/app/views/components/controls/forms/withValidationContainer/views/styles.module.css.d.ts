declare const styles: {
  readonly 'error-container-1': string;
  readonly 'error-container-2': string;
  readonly 'error-container-3': string;
  readonly 'error-content': string;
};
export = styles;