import { Autocomplete, AutocompleteChangeDetails, AutocompleteChangeReason, AutocompleteInputChangeReason, AutocompleteRenderInputParams, MenuItem, Popper, PopperProps } from '@mui/material';
import React from 'react';

import { TextOut } from 'app/views/components/TextOut';
import { AutocompleteComboBoxFormProps } from 'app/views/components/controls/forms/ComboBox/autocomplete/models';
import { InputTextFieldView } from 'app/views/components/controls/forms/ComboBox/autocomplete/views/BaseAutocompleteFormView/InputTextFieldView';
import { ComboboxItemModel } from 'app/views/components/controls/forms/ComboBox/models';

import '../fixed.css';

type OwnProps<TItem, TMultiple extends boolean | undefined> = AutocompleteComboBoxFormProps<TItem, TMultiple> & {
    /**
     * Id Autocomplete
     */
    id: string;

    /**
     * Флаг на множественную выборку
     */
    isMultiple: TMultiple;
};

type AutocompleteState = {
    /**
     * Значения поля поиска
     */
    inputValue: string;
};

export class BaseAutocompleteFormView<TItem, Multiple extends boolean | undefined> extends React.Component<OwnProps<TItem, Multiple>, AutocompleteState> {
    private _waitTimerId = 0;

    /**
     * Время ожидания в миллисекундах перед наступлением события onInputValueApplied
     */
    private readonly beforeInputValueAppliedTimeoutInMilliseconds = 700;

    constructor(props: OwnProps<TItem, Multiple>) {
        super(props);
        this.onChange = this.onChange.bind(this);
        this.onSingleValueChange = this.onSingleValueChange.bind(this);
        this.onMultipleValueChange = this.onMultipleValueChange.bind(this);
        this.findMultipleSelectedOptions = this.findMultipleSelectedOptions.bind(this);
        this.findSingleSelectedOption = this.findSingleSelectedOption.bind(this);
        this.findSelectedOption = this.findSelectedOption.bind(this);
        this.renderInput = this.renderInput.bind(this);
        this.getItemProperty = this.getItemProperty.bind(this);
        this.waitForTimeBeforeInputValueAppliedEvent = this.waitForTimeBeforeInputValueAppliedEvent.bind(this);
        this.onInputValueChange = this.onInputValueChange.bind(this);
        this.customPopperComponent = this.customPopperComponent.bind(this);
        this.setInputValueByEventType = this.setInputValueByEventType.bind(this);
        this.getOptionSelected = this.getOptionSelected.bind(this);

        this.state = {
            inputValue: this.props.inputValue ?? ''
        };
    }

    private onSingleValueChange(_event: React.ChangeEvent<{}>, option: ComboboxItemModel<any> | null) {
        const newValue = option?.value;
        const oldValue = this.props.value;
        const { formName } = this.props;

        if (this.props.onValueChange) {
            const isSuccess = this.props.onValueChange(formName, newValue, oldValue) ?? true;
            if (isSuccess && this.props.onValueApplied) {
                this.props.onValueApplied(formName, newValue);
            }
        }
    }

    private onMultipleValueChange(_event: React.ChangeEvent<{}>, option: ComboboxItemModel<TItem> | ComboboxItemModel<TItem>[]) {
        const newValues: TItem[] = [];

        (option as ComboboxItemModel<TItem>[]).forEach(item => {
            newValues.push(item.value);
        });

        const oldValue = this.props.value;
        const { formName } = this.props;

        if (this.props.onValueChange) {
            const isSuccess = this.props.onValueChange(formName, newValues as any, oldValue) ?? true;
            if (isSuccess && this.props.onValueApplied) {
                this.props.onValueApplied(formName, newValues as any);
            }
        }
    }

    private onChange(event: React.ChangeEvent<{}>, value: ComboboxItemModel<any> | ComboboxItemModel<any>[] | null, _reason: AutocompleteChangeReason, _details?: AutocompleteChangeDetails<any>) {
        if (this.props.isMultiple) {
            this.onMultipleValueChange(event, value as ComboboxItemModel<any>[]);
        } else {
            this.onSingleValueChange(event, value as ComboboxItemModel<any>);
        }
    }

    private onInputValueChange(newValue: string): boolean | void {
        const oldValue = '';
        const { formName } = this.props;

        if (this.props.onInputValueChange) {
            return this.props.onInputValueChange(formName, newValue, oldValue);
        }

        return true;
    }

    private getOptionSelected(option: ComboboxItemModel<any>, value: ComboboxItemModel<any>) {
        return option.value === value.value;
    }

    private getItemProperty(property: keyof ComboboxItemModel<any>) {
        return (item: ComboboxItemModel<any>): string => {
            return item[property];
        };
    }

    private setInputValueByEventType(event: React.ChangeEvent<{}>, newValue: string) {
        if (this.props.isWithCustomSearch && event && event.type && event.type !== 'blur') {
            this.setState({
                inputValue: newValue
            });
        }
    }

    private waitForTimeBeforeInputValueAppliedEvent(event: React.ChangeEvent<{}>, newValue: string, reason: AutocompleteInputChangeReason) {
        this.setInputValueByEventType(event, newValue);

        if (reason === 'reset') {
            return;
        }

        const isSuccess = this.onInputValueChange(newValue) ?? true;

        if (!isSuccess || !this.props.onInputValueApplied) {
            return;
        }

        if (this._waitTimerId) {
            clearTimeout(this._waitTimerId);
            this._waitTimerId = 0;
        }

        this._waitTimerId = setTimeout(this.props.onInputValueApplied, this.beforeInputValueAppliedTimeoutInMilliseconds, this.props.formName, newValue);
    }

    private findSelectedOption(): ComboboxItemModel<TItem> | ComboboxItemModel<TItem>[] | undefined | null {
        return this.props.isMultiple
            ? this.findMultipleSelectedOptions()
            : this.findSingleSelectedOption();
    }

    private findSingleSelectedOption() {
        return this.props.items.find(item => item.value === this.props.value) ?? null;
    }

    private findMultipleSelectedOptions() {
        const foundItems: ComboboxItemModel<TItem>[] = [];

        (this.props.value as TItem[]).forEach(value => {
            const foundOption = this.props.items.find(item => item.value === value);
            if (foundOption) {
                foundItems.push(foundOption);
            }
        });

        return foundItems;
    }

    private customPopperComponent(props: PopperProps) {
        return (<Popper sx={ { zIndex: 3 } } { ...props } className={ this.props.popperClassName } />);
    }

    private renderInput(params: AutocompleteRenderInputParams) {
        return (
            <InputTextFieldView
                params={ params }
                formName={ this.props.formName }
                label={ this.props.label }
                placeholder={ this.props.placeholder }
                autoFocus={ this.props.autoFocus }
                isLoading={ this.props.isLoading }
                value={ this.props.inputValue }
            />
        );
    }

    public render() {
        const inputValue = this.props.isWithCustomSearch ? this.state.inputValue : undefined;
        const { isReadOnly, items, raiseInputValueChangeEvents,
            loadingText, isLoading, noItemsText,
            isMultiple, isClearable } = this.props;

        return (
            <Autocomplete
                data-testid="base-autocomplete-form-view"
                inputValue={ inputValue }
                id={ this.props.id }
                noOptionsText={ <TextOut fontSize={ 13 }>{ noItemsText ?? 'Ничего не найдено' }</TextOut> }
                disabled={ isReadOnly }
                size="small"
                clearText="Очистить"
                disableClearable={ !isClearable }
                options={ items as ComboboxItemModel<any>[] }
                loading={ isLoading }
                loadingText={ <TextOut fontSize={ 13 }>{ loadingText ?? 'Загрузка...' }</TextOut> }
                getOptionLabel={ this.getItemProperty('text') }
                renderOption={ (props, option) => (<MenuItem { ...props } key={ option.value } style={ { fontSize: '13px' } }>{ option.text }</MenuItem>) }
                groupBy={ this.getItemProperty('group') }
                onChange={ this.onChange }
                value={ this.findSelectedOption() }
                isOptionEqualToValue={ this.getOptionSelected }
                blurOnSelect="mouse"
                renderInput={ this.renderInput }
                onInputChange={ raiseInputValueChangeEvents === true ? this.waitForTimeBeforeInputValueAppliedEvent : undefined }
                autoHighlight={ true }
                multiple={ !!isMultiple }
                PopperComponent={ this.props.popperClassName ? this.customPopperComponent : undefined }
            />
        );
    }
}