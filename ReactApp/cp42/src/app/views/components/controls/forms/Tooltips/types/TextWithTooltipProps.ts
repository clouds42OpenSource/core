import React from 'react';

export enum TooltipPlacementTypes {
    topStart = 'top-start',
    top = 'top',
    topEnd = 'top-end',
    leftStart = 'left-start',
    left = 'left',
    leftEnd = 'left-end',
    rightStart = 'right-start',
    right = 'right',
    rightEnd = 'right-end',
    bottomStart = 'bottom-start',
    bottom = 'bottom',
    bottomEnd = 'bottom-end'
}

export type TextWithTooltipProps = {
    text: string | React.JSX.Element,
    tooltipText: string,
    tooltipPlacement: TooltipPlacementTypes;
    isSmall?: boolean;
};