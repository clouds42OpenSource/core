import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';

describe('<FormAndLabel />', () => {
    let formAndLabel: HTMLElement;

    beforeAll(() => {
        render(
            <FormAndLabel label="label">
                test_text
            </FormAndLabel>
        );
        formAndLabel = screen.getByTestId(/form-and-label/i);
    });

    it('renders', () => {
        expect(formAndLabel).toBeInTheDocument();
    });

    it('renders children', () => {
        expect(formAndLabel).toHaveTextContent(/test_text/i);
    });
});