import { FormControl, InputLabel, MenuItem, OutlinedInput, Select, SelectChangeEvent } from '@mui/material';
import { ComboboxItemOptionModel, GroupedComboboxOptionsModel } from 'app/views/components/controls/forms/ComboBox/SystemComboBoxForm/models/ComboboxItemOptionModel';
import React from 'react';

import { hasComponentChangesFor } from 'app/common/functions';
import { generateUniqueId } from 'app/common/functions/generateUniqueId';
import { ComboBoxFormProps } from 'app/views/components/controls/forms/ComboBox/models';
import { ComboboxItemModel } from 'app/views/components/controls/forms/ComboBox/models/ComboboxModels';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';

import styles from './style.module.css';

export class SystemComboBoxForm extends React.Component<ComboBoxFormProps<any, false>> {
    private id = generateUniqueId('combobox_form_');

    constructor(props: ComboBoxFormProps<any, false>) {
        super(props);
        this.onChange = this.onChange.bind(this);
        this.renderOptions = this.renderOptions.bind(this);
        this.generateItemsWithGroup = this.generateItemsWithGroup.bind(this);
        this.generateItemsWithoutGroup = this.generateItemsWithoutGroup.bind(this);
        this.generateItemsWithGroupObjects = this.generateItemsWithGroupObjects.bind(this);
    }

    public shouldComponentUpdate(nextProps: ComboBoxFormProps<any, false>) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private onChange(event: SelectChangeEvent, _: React.ReactNode) {
        const newValue = event.target.value;
        const oldValue = this.props.value;
        const { formName } = this.props;

        if (this.props.onValueChange) {
            const isSuccess = this.props.onValueChange(formName, newValue, oldValue) ?? true;
            if (isSuccess && this.props.onValueApplied) {
                this.props.onValueApplied(formName, newValue);
            }
        }
    }

    private generateItemsWithGroupObjects(): GroupedComboboxOptionsModel {
        const groupedOptions: GroupedComboboxOptionsModel = {};

        if (this.props.items.length <= 0) {
            return { '': [] };
        }

        this.props.items.forEach(({ group, value, text }) => {
            const groupValue = group ?? '';

            if (!groupedOptions[groupValue]) {
                groupedOptions[groupValue] = [];
            }

            groupedOptions[groupValue].push({
                text,
                value
            });
        });

        return groupedOptions;
    }

    private generateItemsWithGroup(items: GroupedComboboxOptionsModel, groups: string[]) {
        return groups.map(group => {
            return (
                <optgroup key={ group } label={ group }>
                    { this.renderOptions(items[group]) }
                </optgroup>
            );
        });
    }

    private generateItemsWithoutGroup(items: Array<ComboboxItemOptionModel>) {
        return this.renderOptions(items);
    }

    private renderOptions(options?: ComboboxItemModel<any>[]) {
        if (options) {
            return options.map(({ value, text }) => {
                return (
                    <MenuItem key={ `${ value }${ text }` } value={ value } style={ { display: 'block', fontSize: '13px' } }>
                        { text }
                    </MenuItem>
                );
            });
        }

        return null;
    }

    public render() {
        const { autoFocus, isReadOnly, formName, label, value } = this.props;
        const itemsWithGroups = this.generateItemsWithGroupObjects();
        const groups = Object.keys(itemsWithGroups);

        return (
            <FormAndLabel label="" forId={ this.id }>
                <FormControl
                    variant="outlined"
                    size="small"
                    fullWidth={ true }
                >
                    <InputLabel id="select-label" shrink={ true } className={ styles.formLabel }>{ label }</InputLabel>
                    <Select
                        className={ styles.select }
                        data-testid="system-combo-box-form__select"
                        labelId="select-label"
                        displayEmpty={ true }
                        renderValue={ (val: unknown) => itemsWithGroups[groups[0]].map((x, i) => x.value === val ? itemsWithGroups[groups[0]][i].text : '') }
                        id={ this.id }
                        name={ formName }
                        autoFocus={ autoFocus }
                        readOnly={ isReadOnly }
                        disabled={ isReadOnly }
                        label={ label }
                        onChange={ this.onChange }
                        value={ this.props.items.length <= 0 ? '' : value }
                        defaultValue={ value }
                        input={ <OutlinedInput notched={ true } label={ label } /> }
                    >
                        { groups.length > 0 && groups[0]
                            ? this.generateItemsWithGroup(itemsWithGroups, groups)
                            : this.generateItemsWithoutGroup(itemsWithGroups[groups[0]])
                        }
                    </Select>
                </FormControl>
            </FormAndLabel>
        );
    }
}