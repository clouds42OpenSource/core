import { Button, Grid, Icon, List, ListItem, ListItemText, Paper } from '@mui/material';
import { DualListBoxFormProps, DualListItem, DualListSearchInputFormType, DualListSearchInputFormTypeNames, DualListStateListTypes, DualListStateListTypesNames } from 'app/views/components/controls/forms/DualListBoxForm/views/types';

import { hasComponentChangesFor } from 'app/common/functions';
import { not } from 'app/common/functions/not';
import { TextOut } from 'app/views/components/TextOut';
import css from 'app/views/components/controls/forms/DualListBoxForm/views/styles.module.css';
import { TextBoxForm } from 'app/views/components/controls/forms/TextBoxForm';
import cn from 'classnames';
import React from 'react';

/**
 * Внутренний тип элемента списка
 */
type DualListItemInternal<TKey> = DualListItem<TKey> & {
    /**
     * Флаг на скрытие строки
     */
    hidden: boolean;

    /**
     * Флаг что строка выбрана
     */
    selected: boolean;
};

/**
 * Собственное состояние компонента
 */
type OwnState<TKey> = {
    leftList: DualListItemInternal<TKey>[],
    rightList: DualListItemInternal<TKey>[],
    oldRightList: DualListItemInternal<TKey>[],
    oldLeftList: DualListItemInternal<TKey>[],
    rightListSearchString: string,
    leftListSearchString: string,
    isAllSelect: boolean
};

/**
 * Представление, описывающие двойной список с перемещением элементов
 */
export class DualListBoxFormView<TKey> extends React.Component<DualListBoxFormProps<TKey>, OwnState<TKey>> {
    constructor(props: DualListBoxFormProps<TKey>) {
        super(props);
        this.handleAllSelectedLeft = this.handleAllSelectedLeft.bind(this);
        this.handleAllSelectedRight = this.handleAllSelectedRight.bind(this);
        this.generateItemsList = this.generateItemsList.bind(this);
        this.handleListItemClick = this.handleListItemClick.bind(this);
        this.onSearchValueChange = this.onSearchValueChange.bind(this);
        this.changeLeftListItems = this.changeLeftListItems.bind(this);
        this.changeRightListItems = this.changeRightListItems.bind(this);
        this.getSortedListByFilter = this.getSortedListByFilter.bind(this);
        this.onChangeStateCallback = this.onChangeStateCallback.bind(this);
        this.handleCheckAll = this.handleCheckAll.bind(this);

        this.state = {
            leftList: this.props.leftListItems.map(item => ({ ...item, hidden: false, selected: false })),
            rightList: this.props.rightListItems.map(item => ({ ...item, hidden: false, selected: false })),
            oldLeftList: this.props.leftListItems.map(item => ({ ...item, hidden: false, selected: false })),
            oldRightList: this.props.rightListItems.map(item => ({ ...item, hidden: false, selected: false })),
            rightListSearchString: '',
            leftListSearchString: '',
            isAllSelect: false,
        };
    }

    public componentDidUpdate(prevProps: Readonly<DualListBoxFormProps<TKey>>) {
        if (!hasComponentChangesFor(prevProps, this.props)) return;
        this.changeLeftListItems(this.state.leftListSearchString, this.props.leftListItems.map(item => ({ ...item, hidden: false, selected: false })));
        this.changeRightListItems(this.state.rightListSearchString, this.props.rightListItems.map(item => ({ ...item, hidden: false, selected: false })));
    }

    private handleCheckAll(type: string) {
        const { leftList, rightList } = this.state;

        if (type === DualListStateListTypes.leftList) {
            this.setState(prevState => ({
                leftList: leftList.map(item =>
                    item.label.toLowerCase().includes(prevState.leftListSearchString.toLowerCase())
                        ? { ...item, selected: true }
                        : item
                )
            }));
        } else {
            this.setState(prevState => ({
                rightList: rightList.map(item =>
                    item.label.toLowerCase().includes(prevState.rightListSearchString.toLowerCase())
                        ? { ...item, selected: true }
                        : item
                )
            }));
        }
    }

    private handleListItemClick(index: number, type: DualListStateListTypesNames) {
        return () => {
            const { leftList, rightList } = this.state;

            if (type === DualListStateListTypes.leftList) {
                leftList[index].selected = !leftList[index].selected;
                this.setState({
                    leftList
                });
            } else {
                rightList[index].selected = !rightList[index].selected;
                this.setState({
                    rightList
                });
            }
        };
    }

    private handleAllSelectedRight() {
        const selectedLeftItems = this.state.leftList.filter(item => item.selected);
        this.setState(prevState => ({
            rightList: prevState.rightList.concat(selectedLeftItems),
            leftList: not(prevState.leftList, selectedLeftItems),
            oldLeftList: prevState.leftList,
            oldRightList: prevState.rightList,
        }), this.onChangeStateCallback);
    }

    private handleAllSelectedLeft() {
        const selectedRightItems = this.state.rightList.filter(item => item.selected);
        this.setState(prevState => ({
            leftList: prevState.leftList.concat(selectedRightItems),
            rightList: not(prevState.rightList, selectedRightItems),
            oldLeftList: prevState.leftList,
            oldRightList: prevState.rightList,
        }), this.onChangeStateCallback);
    }

    private onSearchValueChange(inputTypeName: string, newValue: string) {
        const inputType = inputTypeName as DualListSearchInputFormTypeNames;
        if (inputType === DualListSearchInputFormType.leftListSearchString) {
            this.changeLeftListItems(newValue, this.state.leftList);
        } else {
            this.changeRightListItems(newValue, this.state.rightList);
        }
    }

    private onChangeStateCallback() {
        if (this.props.onValueChange) {
            const isCanChange = this.props.onValueChange(this.props.formName, this.state.leftList, this.state.rightList, this.state.oldLeftList, this.state.oldRightList, this.state.isAllSelect);
            this.setState({
                isAllSelect: false
            });
            if (!isCanChange) {
                this.setState(prevState => ({
                    leftList: prevState.oldLeftList,
                    rightList: prevState.oldRightList
                }));
            }
        }
    }

    private getSortedListByFilter(inputValue: string, items: Array<DualListItemInternal<TKey>>): Array<DualListItemInternal<TKey>> {
        const sortedItems: DualListItemInternal<TKey>[] = [];
        items.forEach(item => {
            item.hidden = !item.label.toLowerCase().includes(inputValue.toLowerCase());
            sortedItems.push(item);
        });
        return sortedItems;
    }

    private changeRightListItems(inputValue: string, list: DualListItemInternal<TKey>[]) {
        this.setState({
            rightListSearchString: inputValue
        });
        const rightListItems = this.getSortedListByFilter(inputValue, list);
        this.setState({
            rightList: rightListItems
        });
    }

    private changeLeftListItems(inputValue: string, list: DualListItemInternal<TKey>[]) {
        this.setState({
            leftListSearchString: inputValue
        });
        const leftListItems = this.getSortedListByFilter(inputValue, list);
        this.setState({
            leftList: leftListItems
        });
    }

    private generateItemsList(items: DualListItemInternal<TKey>[], type: DualListStateListTypesNames) {
        items = items.sort((firstItem, secondItem) => (firstItem.label.localeCompare(secondItem.label)));
        return (
            <Paper className={ cn(css.paper) }>
                <List dense={ true } disablePadding={ true } className={ cn(css.list) } component="div" role="list">
                    { items.map((item, index) => {
                        const labelId = `transfer-list-item-${ item.key }-label`;
                        return (
                            <ListItem
                                key={ `${ item.key }` }
                                role="listitem"
                                button={ true }
                                onClick={ this.handleListItemClick(index, type) }
                                className={ cn(item.isSimilar
                                    ? `${ css['list-item-similar'] } ${ item.selected ? css['list-item-similar-selected'] : '' }`
                                    : `${ css.listItem } ${ item.selected ? css.listItemSelected : '' }`) }
                                sx={ item.hidden
                                    ? {
                                        display: 'none'
                                    }
                                    : {}
                                }
                                disabled={ this.props.isFormDisabled }
                            >
                                <ListItemText
                                    primaryTypographyProps={ {
                                        className: cn(css['listItem-text'])
                                    } }
                                    id={ labelId }
                                    primary={
                                        <>
                                            { item.label }
                                            { item.isSimilar ? <span>(<i className="fa fa-exchange ml-1 mr-1" />совместим)</span> : null }
                                        </>
                                    }
                                />
                            </ListItem>
                        );
                    }) }
                </List>
            </Paper>
        );
    }

    public render() {
        const { rightList, leftList } = this.state;
        const selectedRightItems = rightList.filter(item => item.selected);
        const selectedLeftItems = leftList.filter(item => item.selected);

        return (
            <Grid
                data-testid="dual-list-box-form-view"
                container={ true }
                spacing={ 2 }
                alignItems="center"
                justifyContent="center"
                className={ cn(css.root) }
            >
                <Grid item={ true } sx={ { paddingLeft: '0px !important' } } xs={ 6 }>
                    <Grid container={ true } direction="column" className="m-t-md">
                        <label className={ cn(css.label) }>
                            <TextOut fontSize={ 13 } fontWeight={ 600 }>
                                { this.props.leftListLabel }
                            </TextOut>
                        </label>
                    </Grid>
                    <Grid container={ true } direction="column" className="m-t-md">
                        <TextBoxForm
                            label="Поиск по списку"
                            formName={ DualListSearchInputFormType.leftListSearchString }
                            onValueChange={ this.onSearchValueChange }
                            value={ this.state.leftListSearchString }
                            isReadOnly={ this.props.isFormDisabled }
                        />
                    </Grid>
                    <Grid container={ true } direction="column" alignItems="center" className="m-t-md">
                        <Button
                            variant="outlined"
                            size="small"
                            onClick={ () => this.handleAllSelectedRight() }
                            disabled={ (selectedLeftItems.length === 0 && !this.props.allowSelectAll) || this.props.isFormDisabled }
                            fullWidth={ true }
                        >
                            <Icon className="fa fa-long-arrow-right" fontSize="small" />
                        </Button>
                    </Grid>
                    {
                        this.props.allowSelectAll && (
                            <Grid container={ true } direction="column" alignItems="center" className="m-t-md">
                                <Button
                                    variant="outlined"
                                    size="small"
                                    onClick={ () => this.handleCheckAll(DualListStateListTypes.leftList) }
                                    disabled={ this.props.isFormDisabled }
                                    fullWidth={ true }
                                >
                                    Выбрать все
                                </Button>
                            </Grid>
                        )
                    }
                    { this.generateItemsList(leftList, DualListStateListTypes.leftList) }
                </Grid>
                <Grid item={ true } xs={ 6 }>
                    <Grid container={ true } direction="column" className="m-t-md">
                        <label className={ cn(css.label) }>
                            <TextOut fontSize={ 13 } fontWeight={ 600 }>
                                { this.props.rightListLabel }
                            </TextOut>
                        </label>
                    </Grid>
                    <Grid container={ true } direction="column" className="m-t-md">
                        <TextBoxForm
                            autoFocus={ true }
                            label="Поиск по списку"
                            formName={ DualListSearchInputFormType.rightListSearchString }
                            onValueChange={ this.onSearchValueChange }
                            value={ this.state.rightListSearchString }
                            isReadOnly={ this.props.isFormDisabled }
                        />
                    </Grid>
                    <Grid container={ true } direction="column" alignItems="center" className="m-t-md">
                        <Button
                            variant="outlined"
                            size="small"
                            onClick={ () => this.handleAllSelectedLeft() }
                            disabled={ (selectedRightItems.length === 0 && !this.props.allowSelectAll) || this.props.isFormDisabled }
                            fullWidth={ true }
                        >
                            <Icon className="fa fa-long-arrow-left" fontSize="small" />
                        </Button>
                    </Grid>
                    {
                        this.props.allowSelectAll && (
                            <Grid container={ true } direction="column" alignItems="center" className="m-t-md">
                                <Button
                                    variant="outlined"
                                    size="small"
                                    onClick={ () => this.handleCheckAll(DualListStateListTypes.rightList) }
                                    disabled={ this.props.isFormDisabled }
                                    fullWidth={ true }
                                >
                                    Выбрать все
                                </Button>
                            </Grid>
                        )
                    }
                    { this.generateItemsList(rightList, DualListStateListTypes.rightList) }
                </Grid>
            </Grid>
        );
    }
}