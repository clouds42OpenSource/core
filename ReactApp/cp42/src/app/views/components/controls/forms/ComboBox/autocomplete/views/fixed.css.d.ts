declare const styles: {
  readonly 'auto_complete_combo': string;
  readonly 'MuiFormLabel-root': string;
  readonly 'Mui-focused': string;
  readonly 'MuiOutlinedInput-root': string;
  readonly 'MuiOutlinedInput-notchedOutline': string;
  readonly 'MuiAutocomplete-input': string;
  readonly 'MuiChip-deleteIcon': string;
  readonly 'MuiChip-sizeSmall': string;
  readonly 'MuiAutocomplete-inputRoot': string;
  readonly 'MuiAutocomplete-option': string;
};
export = styles;