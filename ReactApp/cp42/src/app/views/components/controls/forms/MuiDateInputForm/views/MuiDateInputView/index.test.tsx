import { render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import userEvent from '@testing-library/user-event';
import { MuiDateInputView } from 'app/views/components/controls/forms/MuiDateInputForm/views/MuiDateInputView/index';

describe('<MuiDateInputForm />', () => {
    let container: HTMLElement;
    const onDateChangeMock: jest.Mock = jest.fn();

    beforeEach(() => {
        container = render(<MuiDateInputView
            formName="test_form_name"
            label="test_label"
            onDateChange={ onDateChangeMock }
            includeTime={ false }
        />).container;
    });

    it('can open modal', async () => {
        await userEvent.click(container.querySelector('.MuiButtonBase-root')!);
        expect(document.querySelectorAll('.MuiPickersDay-dayWithMargin')[0]).toBeInTheDocument();
    });
});