declare const styles: {
  readonly 'configurations-1c-filter': string;
  readonly 'button-custom-outline-active': string;
  readonly 'configuration-control': string;
  readonly 'configuration-control-text': string;
  readonly 'configuration-control-dropdowm-icon': string;
  readonly 'dropdown-menu-wrapper': string;
  readonly 'configurations-control-buttons': string;
  readonly 'control-group': string;
  readonly 'custom-checkbox': string;
  readonly 'configurations-search': string;
  readonly 'control-label': string;
  readonly 'button-custom-outline': string;
};
export = styles;