declare const styles: {
  readonly 'tooltip-container': string;
  readonly 'tooltipe-code': string;
  readonly small: string;
};
export = styles;