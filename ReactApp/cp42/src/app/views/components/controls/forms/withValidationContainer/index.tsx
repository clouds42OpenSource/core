import { ValidationEvent } from 'app/common/types';
import { ValidationContainerProps, ValidationContainerView } from 'app/views/components/controls/forms/withValidationContainer/views/ValidationContainerView';
import React, { ComponentType } from 'react';

type OwnProps = Omit<ValidationContainerProps, 'Component'>;

export type ValidationProps = {
    isValid: (args: {
        value: string;
        prevValue: string;
        formName: string;
        validationEvent: ValidationEvent;
        justValidation?: boolean;
        state?: any;
    }) => boolean;
    hasError: boolean;
};

export const withValidationContainer =
    <TProps extends ValidationProps>(Component: React.ComponentType<TProps>): ComponentType<OwnProps & Omit<TProps, keyof ValidationProps>> =>
        (props: OwnProps & Omit<TProps, keyof ValidationProps>) =>
            <ValidationContainerView Component={ Component } { ...props } />;