import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { DualListBoxFormView } from 'app/views/components/controls/forms/DualListBoxForm/views/DualListBoxFormView';
import { DualListStateListTypes } from 'app/views/components/controls/forms/DualListBoxForm/views/types';

describe('<DualListBoxFormView />', () => {
    const onValueChangeMock: jest.Mock = jest.fn();

    beforeAll(() => {
        render(<DualListBoxFormView
            isFormDisabled={ false }
            leftListItems={ [{ key: 1, label: 'test_label_1' }, { key: 2, label: 'test_label_2' }] }
            rightListItems={ [{ key: 3, label: 'test_label_3' }, { key: 4, label: 'test_label_4' }, { key: 5, label: 'test_label_5' }, { key: 6, label: 'test_label_6' }, { key: 7, label: 'test_label_7' }] }
            mainList={ DualListStateListTypes.leftList }
            leftListLabel="test_left_list_label"
            rightListLabel="test_right_list_label"
            formName="test_form_name"
            onValueChange={ onValueChangeMock }
        />);
    });

    it('renders', () => {
        expect(screen.getByTestId(/dual-list-box-form-view/i)).toBeInTheDocument();
    });
});