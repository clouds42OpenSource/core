import { DoubleDateInputView } from 'app/views/components/controls/forms/DoubleDateInputView/views';
import { ValueAppliedEvent, ValueChangeEvent } from 'app/views/components/controls/forms/_types';
import React from 'react';

type OwnProps = {
    periodFromValue: Date | null;
    periodToValue: Date | null;
    isReadOnly?: boolean;
    label?: string;
    isNeedToStretch?: boolean;
    periodFromInputName: string;
    periodToInputName: string;
    includeTime?: boolean;
    onValueChange?: ValueChangeEvent<Date>;
    onValueApplied?: ValueAppliedEvent<Date>;
    maxDateFrom?: Date;
};

type OwnState = {
    selectedFromDay: Date | null;
};

/**
 * Компонент для двойного блока ввода даты
 */
export class DoubleDateInputForm extends React.Component<OwnProps, OwnState> {
    private viewRef = React.createRef<DoubleDateInputView>();

    public constructor(props: OwnProps) {
        super(props);

        this.onDateFromChange = this.onDateFromChange.bind(this);
        this.onDateToChange = this.onDateToChange.bind(this);

        this.state = {
            selectedFromDay: !props.periodFromValue ? null : props.periodFromValue,
        };
    }

    private onDateFromChange(date: Date | [Date, Date] | null) {
        if (date instanceof Array) {
            throw new Error('Не разрешено использовать начальную и конечную дату, только выбранную.');
        }

        if (!this.props.onValueChange) {
            return;
        }

        this.props.onValueChange(this.props.periodFromInputName, date!, this.state.selectedFromDay!);

        if (this.props.onValueApplied) {
            this.props.onValueApplied(this.props.periodFromInputName, date!);
        }
    }

    private onDateToChange(date: Date | [Date, Date] | null) {
        if (date instanceof Array) {
            throw new Error('Не разрешено использовать начальную и конечную дату, только выбранную.');
        }

        if (!this.props.onValueChange) {
            return;
        }

        this.props.onValueChange(this.props.periodToInputName, date!, this.state.selectedFromDay!);

        if (this.props.onValueApplied) {
            this.props.onValueApplied(this.props.periodToInputName, date!);
        }
    }

    public render() {
        const {
            isReadOnly,
            label,
            periodToValue,
            periodFromValue,
            isNeedToStretch,
            includeTime,
            maxDateFrom
        } = this.props;

        return (
            <DoubleDateInputView
                includeTime={ includeTime ?? false }
                ref={ this.viewRef }
                isNeedToStretch={ isNeedToStretch }
                label={ label ?? '' }
                isReadOnly={ isReadOnly ?? false }
                periodFromValue={ periodFromValue }
                periodToValue={ periodToValue }
                onValueFromChange={ this.onDateFromChange }
                onValueToChange={ this.onDateToChange }
                maxDateFrom={ maxDateFrom }
            />
        );
    }
}