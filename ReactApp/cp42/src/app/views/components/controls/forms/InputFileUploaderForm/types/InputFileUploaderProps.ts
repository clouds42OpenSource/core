/**
 * Свойства File Uploader'а
 */
export type InputFileUploaderProps = {
    /**
     * Название заголовка
     */
    label: string,

    /**
     * Принимаемый тип
     */
    accept?: string,

    /**
     * Название файла для инициализации
     */
    defaultContentName?: string,

    /**
     * Call back функция на успешное получение файла
     */
    onFileSelectSuccess: (file: File) => void;

    /**
     * Call back функция на отклоненный файл для получения сообщения об ошибке
     */
    onFileSelectError: (errorMessage: string) => void;
}