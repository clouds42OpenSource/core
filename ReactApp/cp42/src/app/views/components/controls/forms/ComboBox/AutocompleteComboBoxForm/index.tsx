import { hasComponentChangesFor } from 'app/common/functions';
import { generateUniqueId } from 'app/common/functions/generateUniqueId';
import { AutocompleteComboBoxFormProps } from 'app/views/components/controls/forms/ComboBox/autocomplete/models';
import { BaseAutocompleteFormView } from 'app/views/components/controls/forms/ComboBox/autocomplete/views/BaseAutocompleteFormView';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import React from 'react';
import '../autocomplete/views/fixed.css';

export class AutocompleteComboBoxForm<TValue> extends React.Component<AutocompleteComboBoxFormProps<TValue, false>> {
    private id = generateUniqueId('autocomplete_combobox_form_');

    public shouldComponentUpdate(nextProps: AutocompleteComboBoxFormProps<any, false>) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    /**
     * Получить элемент комбобокса с заголовком или без заголовка
     * @returns Элемент комбобокса
     */
    private getAutoCompleteFormView() {
        const {
            children, isReadOnly, label, value, items, formName,
            onValueChange, onValueApplied, onInputValueChange, onInputValueApplied,
            autoFocus, raiseInputValueChangeEvents, placeholder, isLoading, loadingText,
            noItemsText, autoCompleteKey, autoCompleteRef
        } = this.props;

        let autoCompleteFormView = (
            <BaseAutocompleteFormView
                key={ autoCompleteKey }
                id={ this.id }
                formName={ formName }
                isReadOnly={ isReadOnly }
                items={ items }
                onValueChange={ onValueChange }
                onValueApplied={ onValueApplied }
                onInputValueChange={ onInputValueChange }
                onInputValueApplied={ onInputValueApplied }
                autoFocus={ autoFocus }
                raiseInputValueChangeEvents={ raiseInputValueChangeEvents }
                ref={ autoCompleteRef }
                label={ label }
                value={ value }
                placeholder={ placeholder }
                isLoading={ isLoading }
                loadingText={ loadingText }
                noItemsText={ noItemsText }
                children={ children }
                isMultiple={ false }
                className={ this.props.className }
                popperClassName={ this.props.popperClassName }
                isClearable={ this.props.isClearable }
                inputValue={ this.props.inputValue }
                isWithCustomSearch={ this.props.isWithCustomSearch }
            />
        );

        if (label) {
            autoCompleteFormView = (
                <FormAndLabel hiddenLabel={ true } label="" forId={ this.id }>
                    { autoCompleteFormView }
                </FormAndLabel>
            );
        }

        return autoCompleteFormView;
    }

    public render() {
        return this.getAutoCompleteFormView();
    }
}