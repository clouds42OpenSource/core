import { hasComponentChangesFor } from 'app/common/functions';
import { ValidationEvent, Validator } from 'app/common/types';
import { ShowErrorMessage } from 'app/views/modules/_common/components/ShowErrorMessage';
import React from 'react';

export type ValidationContainerProps = {
    validator?: Validator<any>;
    initialErrors?: string[];
    onValidation?: (isValid: boolean, formName: string, validationEvent: ValidationEvent) => void;
    Component: React.ComponentType<any>;
};

type OwnState = {
    validateErrors?: string[];
    validationEvent?: ValidationEvent;
    initialErrors?: string[];
};

export class ValidationContainerView extends React.Component<ValidationContainerProps, OwnState> {
    public constructor(props: ValidationContainerProps) {
        super(props);

        this.state = {
            validateErrors: undefined,
            validationEvent: undefined,
            initialErrors: props.initialErrors
        };
        this.validate = this.validate.bind(this);
    }

    public static getDerivedStateFromProps(nextProps: ValidationContainerProps, prevState: OwnState): Partial<OwnState> | null {
        if (prevState.initialErrors !== nextProps.initialErrors) {
            return {
                initialErrors: nextProps.initialErrors,
                validateErrors: undefined,
                validationEvent: undefined
            };
        }

        return null;
    }

    public shouldComponentUpdate(nextProps: ValidationContainerProps, nextState: OwnState) {
        return hasComponentChangesFor(this.props, nextProps) ||
            hasComponentChangesFor(this.state, nextState);
    }

    public render() {
        const { Component, ...rest } = this.props;
        const errors = this.state.initialErrors ?? this.state.validateErrors;
        const hasError = (errors?.length ?? 0) > 0;

        return (
            <>
                <Component isValid={ this.validate } hasError={ hasError } { ...rest } />
                <ShowErrorMessage
                    errors={ { name: errors?.reduce((prev, curr) => {
                        prev += curr;

                        return prev;
                    }) ?? '' } }
                    formName="name"
                />
            </>
        );
    }

    public componentDidUpdate(_prevProps: ValidationContainerProps, prevState: OwnState) {
        if (!prevState.validateErrors && this.state.validateErrors && this.state.validationEvent === 'onChange') {
            setTimeout(() => {
                this.setState({
                    validateErrors: undefined,
                    validationEvent: undefined
                });
            }, 2000);
        }
    }

    private validate(args: {
        value: string;
        prevValue: string;
        formName: string;
        validationEvent: ValidationEvent;
        state?: any;
        justValidation?: boolean;
    }): boolean {
        const { value, formName, validationEvent, prevValue, state } = args;

        let isValid = true;

        if (this.props.validator && this.props.validator[validationEvent] && typeof (this.props.validator[validationEvent]) === 'function') {
            const validationErrors = this.props.validator[validationEvent](value, prevValue, state);

            isValid = (
                (validationErrors instanceof Array) &&
                (validationErrors.length > 0)
            ) || !validationErrors;

            const errors = isValid
                ? undefined
                : validationErrors instanceof Array
                    ? validationErrors
                    : validationErrors
                        ? [validationErrors]
                        : undefined;

            if (!args.justValidation) {
                this.setState({
                    validateErrors: errors,
                    validationEvent,
                    initialErrors: undefined
                });
            }
        }

        if (!args.justValidation && this.props.onValidation) this.props.onValidation(isValid, formName, validationEvent);

        return isValid;
    }
}