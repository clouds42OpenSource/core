import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import userEvent from '@testing-library/user-event';
import { SystemComboBoxForm } from 'app/views/components/controls/forms/ComboBox/SystemComboBoxForm';

describe('<SystemComboBoxForm />', () => {
    const onValueChangeMock: jest.Mock = jest.fn();
    const onValueAppliedMock: jest.Mock = jest.fn();

    beforeEach(() => {
        render(<SystemComboBoxForm
            items={ [{ value: 'test_value_item_1', text: 'test_text_item_1' }, { value: 'test_value_item_2', text: 'test_text_item_2' }] }
            formName="test_form_name"
            value="test_value_item_2"
            onValueChange={ onValueChangeMock }
            onValueApplied={ onValueAppliedMock }
        />);
    });

    it('renders', () => {
        expect(screen.getByTestId(/system-combo-box-form__select/i)).toBeInTheDocument();
    });

    it('has onValueChange and onValueApplied methods', async () => {
        await userEvent.click(screen.getByRole(/combobox/i));
        await userEvent.click(document.querySelector('[data-value=test_value_item_1]')!);
        expect(onValueAppliedMock).toBeCalled();
        expect(onValueChangeMock).toBeCalled();
    });
});