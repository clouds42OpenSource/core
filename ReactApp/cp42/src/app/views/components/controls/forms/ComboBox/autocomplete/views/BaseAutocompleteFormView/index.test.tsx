import { fireEvent, render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import userEvent from '@testing-library/user-event';
import { BaseAutocompleteFormView } from 'app/views/components/controls/forms/ComboBox/autocomplete/views/BaseAutocompleteFormView';
import { ComboboxItemModel } from 'app/views/components/controls/forms/ComboBox/models';

describe('<BaseAutoCompleteFormView /> without isMultiple parameter', () => {
    const itemsArray: Array<ComboboxItemModel<number>> = [
        { value: 0, text: 'test_text_0' },
        { value: 1, text: 'test_text_1' },
    ];

    it('renders with items', () => {
        render(<BaseAutocompleteFormView
            items={ itemsArray }
            formName="test_form_name"
            value={ 1 }
            id="test_id"
            isMultiple={ false }
        />);
        expect(screen.getByTestId(/base-autocomplete-form-view/i)).toBeInTheDocument();
    });

    it('has onChange method', async () => {
        render(<BaseAutocompleteFormView
            items={ itemsArray }
            formName="test_form_name"
            value={ 1 }
            id="test_id"
            isMultiple={ false }
        />);
        const rootElement: HTMLElement = screen.getByTestId(/base-autocomplete-form-view/i);
        const inputElement: Element | null = rootElement.querySelector('#test_id');
        const newInputElementValue = 'new_test_text';
        rootElement.focus();
        await userEvent.type(inputElement!, newInputElementValue);
        fireEvent.keyDown(rootElement, { key: 'ArrowDown' });
        fireEvent.keyDown(rootElement, { key: 'Enter' });
        expect(inputElement!.getAttribute('value')).toEqual(newInputElementValue);
    });

    it('renders selected option', () => {
        render(<BaseAutocompleteFormView
            items={ itemsArray }
            formName="test_form_name"
            value={ 1 }
            id="test_id"
            isMultiple={ false }
        />);
        const rootElement: HTMLElement = screen.getByTestId(/base-autocomplete-form-view/i);
        const inputElement: Element | null = rootElement.querySelector('#test_id');
        expect(inputElement!.getAttribute('value')).toEqual(itemsArray[1].text);
    });

    it('renders selected option with multiple', () => {
        render(<BaseAutocompleteFormView
            items={ itemsArray }
            formName="test_form_name"
            value={ [1, 2] }
            id="test_id"
            isMultiple={ true }
        />);
        expect(screen.getByText(/test_text_1/)).toBeInTheDocument();
    });

    it('has onChange method with multiple', async () => {
        render(<BaseAutocompleteFormView
            items={ itemsArray }
            formName="test_form_name"
            value={ [1, 2] }
            id="test_id"
            isMultiple={ true }
        />);
        const rootElement: HTMLElement = screen.getByTestId(/base-autocomplete-form-view/i);
        const inputElements: Array<Element> = screen.getAllByText(/test_text/i);
        const newInputElementValue = 'new_test_text';
        rootElement.focus();
        for (const index of Object.keys(inputElements)) {
            userEvent.type(inputElements[Number.parseInt(index, 10)], `${ newInputElementValue }_${ index }`).then(() => {
                fireEvent.keyDown(rootElement, { key: 'ArrowDown' });
                fireEvent.keyDown(rootElement, { key: 'Enter' });
                expect(inputElements[Number.parseInt(index, 10)].getAttribute('value')).toEqual(newInputElementValue);
            });
        }
    });
});