import { ComboboxItemModel } from 'app/views/components/controls/forms/ComboBox/models';

/**
 * Тип для выпадающего списка чекбоксов
 */
export type DropDownListWithCheckboxItem = ComboboxItemModel<string> & {
    /**
     * Флаг выбраного чекбокса
     */
    checked: boolean;

    /**
     * Флаг для вскрытия чекбокса
     */
    visible?: boolean;
    disabled?: boolean;
};