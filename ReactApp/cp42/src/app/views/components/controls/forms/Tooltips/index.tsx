import React from 'react';
import { TextWithTooltipView } from 'app/views/components/controls/forms/Tooltips/views/TextWithTooltipView';
import { TextWithTooltipProps } from 'app/views/components/controls/forms/Tooltips/types';

/**
 * Текст с подсказкой
 * @param props свойства
 */
export const TextWithTooltip = (props: TextWithTooltipProps) => {
    return <TextWithTooltipView { ...props } />;
};