declare const styles: {
  readonly 'input-style': string;
  readonly 'autocomplete-combo-style': string;
};
export = styles;