import { ValidationProps, withValidationContainer } from 'app/views/components/controls/forms/withValidationContainer';
import { ValueAppliedEvent, ValueChangeEvent } from 'app/views/components/controls/forms/_types';

import { hasComponentChangesFor } from 'app/common/functions';
import { TextBoxFormType } from 'app/views/components/controls/forms/TextBoxForm/types';
import { TextBoxFormView } from 'app/views/components/controls/forms/TextBoxForm/views/TextBoxFormView';
import { generateTextFromTextBoxFor } from 'app/views/components/utils/generateTextFromTextBoxFor';
import { KeyCombinationDetector } from 'app/views/components/utils/KeyCombinationDetector';
import React from 'react';

type OwnProps = ValidationProps & {
    showErrorText?: boolean;
    className?: string;
    formAndLabelClassName?: string;
    formName: string;
    value?: string;
    isReadOnly?: boolean;
    autoFocus?: boolean;
    maxLength?: number;
    label?: React.ReactNode;
    hiddenLabel?: boolean;
    type?: TextBoxFormType;
    placeholder?: string;
    medium?: boolean
    inputRef?: React.Ref<HTMLInputElement>;
    helperText?: string;
    disabled?: boolean;
    autoComplete?: string;
    onValueChange?: ValueChangeEvent<string>;
    onValueApplied?: ValueAppliedEvent<string>;
    onKeyDown?: React.KeyboardEventHandler<HTMLInputElement>;
    fullWidth?: boolean;
    error?: boolean;
    onBlur?: (ev: React.FocusEvent<HTMLInputElement>) => void;
};

const backspaceKeyCode = 8;
const deleteKeyCode = 46;

class TextBoxClass extends React.Component<OwnProps> {
    private viewRef = React.createRef<TextBoxFormView>();

    constructor(props: OwnProps) {
        super(props);
        this.onKeyDown = this.onKeyDown.bind(this);
        this.onPaste = this.onPaste.bind(this);
        this.onBlur = this.onBlur.bind(this);
        this.callOnKeyDownEvent = this.callOnKeyDownEvent.bind(this);
    }

    public shouldComponentUpdate(nextProps: OwnProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private onPaste(ev: React.ClipboardEvent<HTMLInputElement>) {
        if (ev.defaultPrevented) {
            return;
        }

        const textFromClipboard = ev.clipboardData.getData('text');

        const textboxValue = ev.currentTarget.value;
        const selectionStart = ev.currentTarget.selectionStart!;
        const selectionEnd = ev.currentTarget.selectionEnd!;

        const previewValue = generateTextFromTextBoxFor(textboxValue, selectionStart, selectionEnd, undefined, textFromClipboard)!;

        if (!this.props.isValid({
            value: previewValue,
            prevValue: textboxValue,
            formName: this.props.formName,
            validationEvent: 'onChange',
            state: undefined
        })) {
            ev.preventDefault();
        }
    }

    private onKeyDown(ev: React.KeyboardEvent<HTMLInputElement>) {
        if (ev.defaultPrevented) {
            this.callOnKeyDownEvent(ev);
            return;
        }

        if (ev.keyCode !== backspaceKeyCode && ev.keyCode !== deleteKeyCode && ev.key.length > 1) {
            this.callOnKeyDownEvent(ev);
            return;
        }
        if (KeyCombinationDetector.isAnyWindowsShortcutsPressed(ev)) {
            this.callOnKeyDownEvent(ev);
            return;
        }

        const textboxValue = ev.currentTarget.value;
        const selectionStart = ev.currentTarget.selectionStart!;
        const selectionEnd = ev.currentTarget.selectionEnd!;

        const deleteCharKey = ev.keyCode === 46
            ? 'delete_46'
            : ev.keyCode === 8
                ? 'backspace_8'
                : undefined;

        const previewValue = generateTextFromTextBoxFor(textboxValue, selectionStart, selectionEnd, deleteCharKey, ev.key)!;

        if (!this.props.isValid({
            value: previewValue,
            prevValue: textboxValue,
            formName: this.props.formName,
            validationEvent: 'onChange',
            state: undefined
        })) {
            ev.preventDefault();
        }

        this.callOnKeyDownEvent(ev);
    }

    private onBlur(ev: React.FocusEvent<HTMLInputElement>) {
        if (this.props.onBlur) {
            this.props.onBlur(ev);
        } else {
            if (ev.defaultPrevented) {
                return;
            }

            if (!this.props.isValid({
                value: ev.target.value,
                prevValue: '',
                formName: this.props.formName,
                validationEvent: 'onBlur',
                state: undefined
            })) {
                ev.preventDefault();
                ev.target.focus();
            }
        }
    }

    private callOnKeyDownEvent(ev: React.KeyboardEvent<HTMLInputElement>) {
        if (this.props.onKeyDown) this.props.onKeyDown(ev);
    }

    public isValid(justValidation = true) {
        if (!this.viewRef?.current) {
            return false;
        }

        const { value } = this.viewRef.current.props;

        return this.props.isValid({
            value,
            prevValue: '',
            formName: this.props.formName,
            validationEvent: 'onBlur',
            state: undefined,
            justValidation
        });
    }

    public render() {
        const { formName } = this.props;
        const value = this.props.value ?? '';
        const isReadOnly = this.props.isReadOnly ?? false;
        const autoFocus = this.props.autoFocus ?? false;
        const { maxLength } = this.props;
        const label = this.props.label ?? '';
        const hiddenLabel = this.props.hiddenLabel ?? false;
        const { hasError } = this.props;
        const type = this.props.type ?? 'text';
        const { className } = this.props;
        const { placeholder } = this.props;
        const { inputRef } = this.props;

        return (
            <TextBoxFormView
                fullWidth={ this.props.fullWidth }
                autoComplete={ this.props.autoComplete ?? 'off' }
                showErrorText={ this.props.showErrorText }
                ref={ this.viewRef }
                inputRef={ inputRef }
                className={ className }
                medium={ this.props.medium }
                type={ type }
                value={ value }
                formName={ formName }
                autoFocus={ autoFocus }
                label={ label }
                hiddenLabel={ hiddenLabel }
                maxLength={ maxLength }
                isReadOnly={ isReadOnly }
                placeholder={ placeholder }
                hasError={ hasError }
                onValueChange={ this.props.onValueChange }
                onValueApplied={ this.props.onValueApplied }
                onKeyDown={ this.onKeyDown }
                onPaste={ this.onPaste }
                onBlur={ this.onBlur }
                helperText={ this.props.helperText }
                disabled={ this.props.disabled }
                formAndLabelClassName={ this.props.formAndLabelClassName }
                error={ this.props.error }
            />
        );
    }
}

export const TextBoxForm = withValidationContainer(TextBoxClass);