import { TextFontSize, TextFontWeight } from 'app/views/components/TextOut/views/TextView';
import { ValueAppliedEvent, ValueChangeEvent } from 'app/views/components/controls/forms/_types';

import { Checkbox } from '@mui/material';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import React from 'react';
import { TextOut } from 'app/views/components/TextOut';
import cn from 'classnames';
import { generateUniqueId } from 'app/common/functions/generateUniqueId';
import { hasComponentChangesFor } from 'app/common/functions';
import css from '../styles.module.css';

type OwnProps = {
    trueText: React.ReactNode;
    falseText: React.ReactNode;
    captionFontSize: TextFontSize;
    captionFontWeight: TextFontWeight;
    captionAsLabel: boolean;
    isChecked: boolean;
    isReadOnly: boolean;
    autoFocus: boolean;
    formName: string;
    label: string;
    className?: string;
    onValueChange?: ValueChangeEvent<boolean>;
    onValueApplied?: ValueAppliedEvent<boolean>;
    isSmallFormat?: boolean;
};

export class CheckBoxFormView extends React.Component<OwnProps> {
    private id = generateUniqueId('checkbox_form_');

    constructor(props: OwnProps) {
        super(props);
        this.onChange = this.onChange.bind(this);
        this.renderCaptionAsLabel = this.renderCaptionAsLabel.bind(this);
        this.renderCaptionAsText = this.renderCaptionAsText.bind(this);
        this.state = {
            isChecked: props.isChecked
        };
    }

    public shouldComponentUpdate(nextProps: OwnProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    public render() {
        const { autoFocus, isReadOnly, formName, label, isSmallFormat } = this.props;
        const { isChecked } = this.props;

        return (
            <FormAndLabel className={ this.props.className } label={ label } forId={ this.id } isSmallFormat={ isSmallFormat }>
                <Checkbox
                    data-testid="checkbox-form-view"
                    disabled={ isReadOnly }
                    id={ this.id }
                    name={ formName }
                    size="small"
                    checked={ isChecked }
                    value="true"
                    onChange={ this.onChange }
                    autoFocus={ autoFocus }
                    readOnly={ isReadOnly }
                    className={ cn({
                        [css['form-check-input']]: true,
                        [css['form-check-input-small']]: isSmallFormat
                    }) }
                />
                { this.props.captionAsLabel ? this.renderCaptionAsLabel() : this.renderCaptionAsText() }
            </FormAndLabel>
        );
    }

    private renderCaptionAsLabel() {
        return (
            <label htmlFor={ this.id }>
                { this.renderCaptionAsText() }
            </label>
        );
    }

    private renderCaptionAsText() {
        const { trueText, falseText } = this.props;
        const { isChecked } = this.props;
        return (
            <TextOut
                fontSize={ this.props.captionFontSize }
                fontWeight={ this.props.captionFontWeight }
                className={ cn(css.caption) }
            >
                { isChecked ? trueText : falseText ?? trueText }
            </TextOut>
        );
    }

    private onChange(event: React.ChangeEvent<HTMLInputElement>) {
        const newValue = event.target.checked;
        const oldValue = this.props.isChecked;
        const { formName } = this.props;

        if (this.props.onValueChange) {
            const isSuccess = this.props.onValueChange(formName, newValue, oldValue) ?? true;
            if (isSuccess && this.props.onValueApplied) {
                this.props.onValueApplied(formName, newValue);
            }
        }

        this.setState({
            isChecked: newValue
        });
    }
}