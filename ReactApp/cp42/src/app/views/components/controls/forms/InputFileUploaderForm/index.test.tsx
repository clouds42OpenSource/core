import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import '@testing-library/jest-dom/extend-expect';
import { InputFileUploaderForm } from 'app/views/components/controls/forms/InputFileUploaderForm';

describe('<InputFileUploaderForm />', () => {
    const onFileSelectSuccessMock = jest.fn();
    const onFileSelectErrorMock = jest.fn();
    let container: HTMLElement;

    beforeEach(() => {
        container = render(
            <InputFileUploaderForm
                label="test_label"
                onFileSelectSuccess={ onFileSelectSuccessMock }
                onFileSelectError={ onFileSelectErrorMock }
                accept=".png"
            />
        ).container;
    });

    it('renders', () => {
        expect(screen.getAllByText(/test_label/i)[0]).toBeInTheDocument();
    });

    it('should accept file', async () => {
        const inputElement = container.querySelector('input.MuiOutlinedInput-input');
        const file = new File(['file_data'], 'test_image.png');
        await userEvent.upload(inputElement! as HTMLElement, file);
        expect(onFileSelectSuccessMock).toBeCalled();
    });

    it('should not accept file', async () => {
        const inputElement = container.querySelector('input.MuiOutlinedInput-input');
        const file = new File(['another_file_data'], 'test_image.jpg');
        await userEvent.upload(inputElement! as HTMLElement, file);
        expect(onFileSelectSuccessMock).not.toBeCalled();
    });
});