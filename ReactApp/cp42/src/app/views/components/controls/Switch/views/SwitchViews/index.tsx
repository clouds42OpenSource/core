import FormGroup from '@mui/material/FormGroup';
import Stack from '@mui/material/Stack';
import { styled } from '@mui/material/styles';
import Switch from '@mui/material/Switch';
import { COLORS } from 'app/utils';

const AntSwitch = styled(Switch)(({ theme }) => ({
    width: 50,
    height: 22,
    padding: 0,
    display: 'flex',
    '&:active': {
        '& .MuiSwitch-thumb': {
            width: 20,
        },
        '& .MuiSwitch-switchBase.Mui-checked': {
        },
    },
    '& .MuiSwitch-switchBase': {

        padding: 2,
        '&.Mui-checked': {
            transform: 'translateX(28px)',
            color: '#fff !important',
            '& + .MuiSwitch-track': {
                opacity: 1,
                backgroundColor: theme.palette.mode === 'dark' ? COLORS.info : COLORS.main,
            },
        },
    },
    '& .MuiSwitch-thumb': {

        boxShadow: '0 2px 4px 0 rgb(0 35 11 / 20%)',
        width: 18,
        height: 18,
    },
    '& .MuiSwitch-track': {

        borderRadius: 22 / 2,
        opacity: 1,
        backgroundColor:
      theme.palette.mode === 'dark' ? COLORS.main : 'rgba(0,0,0,.25)',
        boxSizing: 'border-box',
    },
}));

export type ISwitch = {
  onChange: any;
  checked: boolean
  disabled?: boolean;
};

const CustomizedSwitches = (props: ISwitch) => {
    return (
        <FormGroup>
            <Stack direction="row" spacing={ 1 } justifyContent="center" alignItems="center">
                <AntSwitch
                    checked={ props.checked }
                    onChange={ props.onChange }
                    name="checkedA"
                    data-testid="CustomizedSwitches"
                    disabled={ props.disabled }
                />
            </Stack>
        </FormGroup>
    );
};

export default CustomizedSwitches;