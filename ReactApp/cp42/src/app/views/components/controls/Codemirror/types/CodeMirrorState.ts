/**
 * Состояние CodeMirror
 */
export type CodeMirrorState = {
    /**
     * Значение
     */
    value: string;
}