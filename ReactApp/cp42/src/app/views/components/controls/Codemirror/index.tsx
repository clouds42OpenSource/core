import { generateUniqueId } from 'app/common/functions';
import { CodeMirrorProps, CodeMirrorState } from 'app/views/components/controls/Codemirror/types';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import cn from 'classnames';
import React, { Component } from 'react';
import { Controlled as CodeMirror } from 'react-codemirror2';
import css from './styles.module.css';

import 'codemirror/lib/codemirror.css';
import 'codemirror/theme/neat.css';
import 'codemirror/mode/xml/xml.js';
import 'codemirror/mode/javascript/javascript.js';

/**
 * Компонент для редактирования кода разметки
 */
export class CodeMirrorView extends Component<CodeMirrorProps, CodeMirrorState> {
    private _id = generateUniqueId('codemirror_');

    public constructor(props: CodeMirrorProps) {
        super(props);

        this.state = {
            value: ''
        };

        this.setLabel = this.setLabel.bind(this);
        this.onChange = this.onChange.bind(this);
    }

    public componentDidUpdate(nextProps: CodeMirrorProps) {
        const { value } = this.props;

        if (nextProps.value !== value) {
            this.setState({ value: value ?? '' });
        }
    }

    private onChange(_editor: object, _data: object, value: string) {
        this.props.onChange(this.props.formName ?? '', value);
    }

    private setLabel(element: React.JSX.Element) {
        const { label } = this.props;

        return (
            <FormAndLabel label={ label } forId={ this._id }>
                { element }
            </FormAndLabel>
        );
    }

    public render() {
        let codeMirrorElement = (
            <CodeMirror
                data-testid="code-mirror-view"
                onBeforeChange={ this.onChange }
                options={ {
                    lineNumbers: true,
                    mode: 'xml'
                } }
                className={ cn(css['codemirror-custom'], this.props.className) }
                value={ this.state.value }
            />
        );

        if (this.props.label) codeMirrorElement = this.setLabel(codeMirrorElement);

        return codeMirrorElement;
    }
}