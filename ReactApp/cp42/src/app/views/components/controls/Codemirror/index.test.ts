import { CodeMirrorView } from 'app/views/components/controls/Codemirror';

describe('<CodeMirrorView />', () => {
    it('render method works', () => {
        const testCodeMirrorComponent: CodeMirrorView = new CodeMirrorView({ onChange: () => null });
        const renderedTestCodeMirrorComponent = testCodeMirrorComponent.render();
        expect(typeof renderedTestCodeMirrorComponent === 'object').toBeTruthy();
        expect(renderedTestCodeMirrorComponent.props['data-testid'] === 'code-mirror-view').toBeTruthy();
    });

    it('setLabel method works', () => {
        const testLabel = 'test_label';
        const testCodeMirrorComponent: CodeMirrorView = new CodeMirrorView({ onChange: () => null, label: testLabel });
        const renderedTestCodeMirrorComponent = testCodeMirrorComponent.render();
        expect(renderedTestCodeMirrorComponent.props.label === testLabel).toBeTruthy();
    });
});