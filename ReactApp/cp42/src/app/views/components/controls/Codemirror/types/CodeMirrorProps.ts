/**
 * Свойства CodeMirror
 */
export type CodeMirrorProps = {
    /**
     * Значение
     */
    value?: string;

    /**
     * Название класса
     */
    className?: string;

    /**
     * Название заголовка
     */
    label?: string;

    /**
     * Название формы
     */
    formName?: string;

    /**
     * Call back функция на изменение значения редактора разметки
     */
    onChange: (formName: string, newValue: string) => void;
}