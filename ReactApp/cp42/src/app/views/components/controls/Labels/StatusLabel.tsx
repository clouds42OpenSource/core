import React from 'react';
import { StatusLabelProps, StatusLabelView } from 'app/views/components/controls/Labels/views/StatusLabelView';

/**
 * Лейбл для статуса
 * @param props свойства
 */
export const StatusLabel = (props: StatusLabelProps & { children?: React.ReactNode }) => {
    return <StatusLabelView { ...props } />;
};