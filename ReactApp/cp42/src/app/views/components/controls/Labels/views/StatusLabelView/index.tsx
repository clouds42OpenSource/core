import { StatusLabelTypes } from 'app/views/components/controls/Labels/views/StatusLabelView/types';
import React, { CSSProperties } from 'react';
import { Chip } from '@mui/material';

/**
 * Собственные свойства компонента
 */
export type StatusLabelProps = {
    text: string,
    type: StatusLabelTypes,
    style?: CSSProperties
};

/**
 * Представление, описывающее лейбл для статуса
 */
export class StatusLabelView extends React.PureComponent<StatusLabelProps> {
    public render() {
        return (
            <div data-testid="status-label-view">
                <Chip color={ this.props.type ?? 'success' } label={ this.props.text } style={ this.props.style } />
            </div>
        );
    }
}