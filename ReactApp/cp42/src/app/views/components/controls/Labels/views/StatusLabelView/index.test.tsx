import '@testing-library/jest-dom/extend-expect';
import { render, screen } from '@testing-library/react';
import { StatusLabelView } from 'app/views/components/controls/Labels/views/StatusLabelView';

describe('<StatusLabelView />', () => {
    it('renders', () => {
        render(<StatusLabelView text="test_text" type="primary" />);
        expect(screen.getByTestId(/status-label-view/i)).toBeInTheDocument();
    });

    it('renders with children', () => {
        render(<StatusLabelView text="test_text" type="primary" />);
        expect(screen.getByTestId(/status-label-view/i)).toBeInTheDocument();
    });

    it('renders with text', () => {
        render(<StatusLabelView text="test_text" type="primary" />);
        expect(screen.getByTestId(/status-label-view/i)).toBeInTheDocument();
        expect(screen.getByText(/test_text/i)).toBeInTheDocument();
    });
});