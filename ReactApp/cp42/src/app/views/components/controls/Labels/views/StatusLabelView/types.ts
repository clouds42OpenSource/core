/**
 * Перечисление возможных типов лейбла статуса
 */
export type StatusLabelTypes = 'primary' | 'secondary' | 'success' | 'error' | 'warning' | 'info';