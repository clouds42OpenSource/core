import React from 'react';

type TClockLoading = {
    width?: string;
    height?: string;
}

export const ClockLoading = ({ width, height }: TClockLoading) =>
    <img src="img/database-icons/animated-clock.gif" alt="timer" style={ { width: width ?? '12px', height: height ?? '12px' } } />;