import { TextOut } from 'app/views/components/TextOut';
import { SelectProps } from 'app/views/components/controls/Select2List/types';
import { Component } from 'react';

import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import OutlinedInput from '@mui/material/OutlinedInput';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import css from './styles.module.css';
/**
 * Состояния Select2
 */
type OwnState = {
    /**
     * Выбранные опшины
     */
    selectedOptions: string[]
}

/**
 * Компонент для выпадающего списка Select2
 */
export class Select2List extends Component<SelectProps, OwnState> {
    public constructor(props: SelectProps) {
        super(props);

        this.state = {
            selectedOptions: []
        };

        this.getLabel = this.getLabel.bind(this);
        this.onChange = this.onChange.bind(this);
    }

    public componentDidUpdate(nextProps: SelectProps) {
        const { defaultValue } = this.props;

        if (nextProps.defaultValue && defaultValue && nextProps.defaultValue.length !== defaultValue.length) {
            if (defaultValue) {
                this.setState({ selectedOptions: defaultValue.map(({ value }) => value) });
            }
        }
    }

    /**
     * Ивент функция на изменения при выборе опшина
     * @param event Опшины для Select2
     */
    private onChange(event: SelectChangeEvent<string[]>) {
        const {
            target: { value }
        } = event;
        if (this.props.onChange && value !== null) {
            this.setState({
                selectedOptions: value as string[]
            });
            this.props.onChange(value as string[]);
        }
    }

    /**
     * Получить заголовок Select2
     * @returns JSX элемент для заголовка
     */
    private getLabel() {
        return this.props.label !== undefined
            ? (
                <label className={ css.label } htmlFor={ this.props.name }>
                    <TextOut fontSize={ 13 } fontWeight={ 600 }>
                        { this.props.label }
                    </TextOut>
                </label>
            )
            : null;
    }

    public render() {
        const { selectedOptions } = this.state;

        return (
            <>
                { this.getLabel() }
                <FormControl sx={ { marginLeft: '10px' } } data-testid="select2">
                    <InputLabel id="demo-multiple-name-label">Локаль аккаунта</InputLabel>
                    {
                        this.props.options
                            ? (
                                <Select
                                    multiple={ true }
                                    MenuProps={ { variant: 'selectedMenu' } }
                                    value={ selectedOptions }
                                    className={ css.select }
                                    onChange={ this.onChange }
                                    input={ <OutlinedInput label="Выберите локаль" /> }
                                >
                                    { this.props.options.map((item, i) => (
                                        <MenuItem
                                            key={ i }
                                            value={ item.value }
                                        >
                                            { item.label }
                                        </MenuItem>
                                    )) }
                                </Select>
                            )
                            : null
                    }
                </FormControl>
            </>
        );
    }
}