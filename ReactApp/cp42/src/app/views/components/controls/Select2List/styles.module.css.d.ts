declare const styles: {
    readonly 'label': string;
    readonly 'select': string;
};
export = styles;