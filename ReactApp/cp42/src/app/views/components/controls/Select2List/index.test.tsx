import '@testing-library/jest-dom/extend-expect';
import { render, screen } from '@testing-library/react';
import { Select2List } from 'app/views/components/controls/Select2List';

describe('<Select2List />', () => {
    it('renders', () => {
        render(<Select2List />);
        expect(screen.getByTestId(/select2/i)).toBeInTheDocument();
    });
});