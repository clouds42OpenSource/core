import { SelectOption } from 'app/views/components/controls/Select2List/types';

export type SelectProps = {
    options?: Array<SelectOption>;
    isMulti?: boolean;
    defaultValue?: SelectOption[];
    name?: string;
    placeholder?: string;
    className?: string;
    label?: string;
    isSearchable?: boolean;
    isClearable?: boolean;
    isMenuOpen?: boolean;
    isLoading?: boolean;
    onChange?: (value: Array<string>) => void;
}