/**
 * Опшины для Select2
 */
export type SelectOption = {
    /**
     * Значение опшина
     */
    value: string,

    /**
     * Название опшина
     */
    label: string
};