import { Pagination } from 'app/views/components/controls/Pagination';
import { TableProps } from 'app/views/components/controls/Table/types';
import { TableResponsiveView } from 'app/views/components/controls/Table/views/TableResponsiveView';
import { TableView } from 'app/views/components/controls/Table/views/TableView';
import css from './views/styles.module.css';

export const Table = <TDataRow, >(props: TableProps<TDataRow>) => {
    const { maxTableHeight, pagination, isResponsiveTable } = props;

    return (
        <>
            <div className={ css['table-wrapper'] } style={ { height: maxTableHeight } }>
                { isResponsiveTable ? <TableResponsiveView { ...props } /> : <TableView { ...props } /> }
            </div>
            { pagination && <Pagination { ...pagination } /> }
        </>
    );
};