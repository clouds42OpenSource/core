/**
 * Функция для обрезания строки
 * @param text текст который нужно обрезать
 * @param n количество символов которые должны остаться
 * @returns строку: n символов от text
 */
export function truncate(text: string, n?: number) {
    if (!n) return text;
    return (text.length > n) ? text.slice(0, n - 4) : text;
}