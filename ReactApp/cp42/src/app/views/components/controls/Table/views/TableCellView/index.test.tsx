import '@testing-library/jest-dom/extend-expect';
import { render, screen } from '@testing-library/react';
import { TableCellView } from 'app/views/components/controls/Table/views/TableCellView';

describe('<TableCellView />', () => {
    it('renders', () => {
        render(<TableCellView />);
        expect(screen.getByTestId(/table-cell-view__table-cell/i)).toBeInTheDocument();
    });

    it('renders with child', () => {
        render(<TableCellView>test_text</TableCellView>);
        expect(screen.getByText(/test_text/i)).toBeInTheDocument();
    });
});