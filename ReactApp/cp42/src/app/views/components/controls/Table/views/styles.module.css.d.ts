declare const styles: {
  readonly 'table-cell-sorting-margin': string;
  readonly 'table-wrapper': string;
  readonly 'pagination-bottom': string;
};
export = styles;