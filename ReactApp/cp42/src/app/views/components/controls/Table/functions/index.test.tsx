import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { renderSortingDesc } from 'app/views/components/controls/Table/functions/renderSortingDesc';
import { renderSortingAsc } from 'app/views/components/controls/Table/functions/renderSortingAsc';
import { renderSorting } from 'app/views/components/controls/Table/functions/renderSorting';
import { getSortaLabelElement } from 'app/views/components/controls/Table/functions/getSortaLabelElement';
import { truncate } from 'app/views/components/controls/Table/functions/getStringTruncate';
import React from 'react';

describe('Table helping functions', () => {
    it('has renderSortingDesc function', () => {
        render(renderSortingDesc());
        expect(screen.getByTestId(/render-sorting-desc/i)).toBeInTheDocument();
    });

    it('has renderSortingAsc function', () => {
        render(renderSortingAsc());
        expect(screen.getByTestId(/render-sorting-asc/i)).toBeInTheDocument();
    });

    it('has renderSorting function', () => {
        render(renderSorting());
        expect(screen.getByTestId(/render-sorting/i)).toBeInTheDocument();
    });

    it('has getSortaLabelElement function with isSortable param', () => {
        render(getSortaLabelElement({ isSortable: true }) as React.ReactElement<any, string | React.JSXElementConstructor<any>>);
        expect(screen.getByRole(/button/i)).toBeInTheDocument();
    });

    it('has getSortaLabelElement function without isSortable param', () => {
        render(getSortaLabelElement({ isSortable: false }) as React.ReactElement<any, string | React.JSXElementConstructor<any>>);
        expect(screen.queryByRole(/button/i)).not.toBeInTheDocument();
    });

    it('has right working getStringTruncate function', () => {
        expect(truncate('test_text')).toBe('test_text');
        expect(truncate('test_text', 4)).toBe('');
        expect(truncate('test_text', 5)).toBe('t');
        expect(truncate('test_text', -100)).toBe('');
    });
});