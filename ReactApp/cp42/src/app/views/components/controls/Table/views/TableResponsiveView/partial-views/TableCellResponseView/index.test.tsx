import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { TableCellResponseView } from 'app/views/components/controls/Table/views/TableResponsiveView/partial-views/TableCellResponseView';

describe('<TableCellResponseView />', () => {
    it('renders and that`s not header', () => {
        render(<TableCellResponseView />);
        expect(screen.getByTestId(/table-cell-response-view__td/i)).toBeInTheDocument();
    });

    it('renders and it`s header', () => {
        render(<TableCellResponseView isHeader={ true } />);
        expect(screen.getByTestId(/table-cell-response-view__th/i)).toBeInTheDocument();
    });
});