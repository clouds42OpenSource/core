import cn from 'classnames';
import React from 'react';
import css from '../views/styles.module.css';

/**
 * Функция для возвращения иконки по убыванию для таблицы
 * @returns Иконки по убыванию для таблицы
 */
export function renderSortingDesc() {
    return <i data-testid="render-sorting-desc" className={ cn('fa fa-sort-desc', css['table-cell-sorting-margin']) } />;
}