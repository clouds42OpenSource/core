import { ERefreshId } from 'app/views/Layout/ProjectTour/enums';
import { HeaderTitleTable } from 'app/views/modules/_common/components/CommonTableWithFilter/TableView';
import { PaginationProps } from 'app/views/components/controls/Pagination';
import { SortingKind } from 'app/common/enums/SortingKind';
import React from 'react';

/**
 * Свойства ячейки
 */
export type TableCellProps<TDataRow> = {
    /**
     * Если true, то строка будет серого цвета
     */
    IsHistoricalRow?: boolean;
    /**
     * Ширина ячейки
     */
    fieldWidth?: string;

    /**
     * Если true, то контент ячейки не будет съезжать на новую строчку
     */
    fieldContentNoWrap?: boolean;

    /**
     * Если true, то заголовок ячейки будет съезжать на новую строку
     */
    wrapCellHeader?: boolean;

    /**
     * Если `true` и `fieldContentNoWrap = false`, то контент ячейки будет съезжать на новую строчку на каждом символе если строчка не помещается в ширину ячейки
     */
    fieldContentWrapEverySymbol?: boolean;

    /**
     * Заголовок
     */
    caption?: React.ReactNode;

    /**
     * Заголовок для мобильной версии
     */
    mobileCaption?: string;

    /**
     * Скрыть отображение поля для мобильной версии
     */
    noNeedForMobile?: boolean;

    /**
     * Формат отображения значения в ячейки
     */
    format?: (value: any, data: TDataRow) => React.ReactNode

    /**
     * Если true, то колонка может сортироваться
     */
    isSortable?: boolean;

    /**
     * Клик по заголовку
     */
    onHeaderSortClick?: (sortKind: SortingKind, fieldName: keyof TDataRow, isInitial: boolean) => void;

    style?: React.CSSProperties;

    className?: string;
};

export type TableCellInternalProps<TDataRow> = TableCellProps<TDataRow> & {
    /**
     * Наименование поля с которым связана ячейка
     */
    fieldName?: keyof TDataRow;

    /**
     * Вид сортировки
     */
    sortKind?: SortingKind;

    /**
     * Если true сгенерит th элементы, если false td
     */
    isHeader?: boolean;
};

/**
 * Свойства таблицы
 */
export type TableProps<TDataRow, TRowFields = keyof TDataRow> = {
    /**
     * Выделяет строку серым цветом (Необходимо для страницы "Баланс")
     */
    isHistoricalRow?: TRowFields;
    /**
     * Содержит название поля с уникальным значением
     */
    keyFieldName: TRowFields;
    /**
     * Содержит текст если таблица пуста
     */
    emptyText?: string;

    /**
     * Генерирует уникальное значение для строки
     */
    generateKeyFieldValue?: (row: TDataRow) => string;

    /**
     * Содержит название поля по которому сортировать
     */
    sortFieldName?: TRowFields;

    /**
     * Вид сортировки
     */
    sortKind?: SortingKind;

    /**
     * Mapping между полем элемента данных и названием колонки
     */
    fieldsView: { [fieldName in keyof TDataRow]?: TableCellProps<TDataRow> };

    /**
     * Набор данных
     */
    dataset: TDataRow[];

    /**
     * Действие для создания комманд для строчки данных
     */
    createRowActions?: (row: TDataRow) => React.ReactNode;

    /**
     * Клик по заголовку
     */
    onHeaderSortClick?: (sortKind: SortingKind, fieldName: keyof TDataRow, isInitial: boolean) => void;

    /**
     * Информация о страницах данных в таблице
     */
    pagination?: PaginationProps;

    /**
     * Обьект для заголовка таблицы
     */
    headerTitleTable?: HeaderTitleTable;

    /**
     * Название класса
     */
    className?: string;

    /**
     * Флаг для переключения между адаптивной таблицей и стандартной
     */
    isResponsiveTable?: boolean;

    /**
     * Флаг для хедера таблицы для переноса текста
     * True будет перенос, False не будет переноса
     */
    isHeaderWrap?: boolean;

    /**
     * Добавляет к ключу строки ее ID
     */
    isIndexToKeyFieldName?: boolean;

    /**
     * Добавляет к responsive view рамки
     */
    isBorderStyled?: boolean;

    maxTableHeight?: string;

    /**
     * ID for triggering rerender onboarding component
     */
    refreshId?: ERefreshId;
    filterData?: any;
};

/**
 * Модель параметра для получения элемента сортировки
 */
export type GetSortableLabelElementParams = {
    /**
     * Если true, то колонка может сортироваться
     */
    isSortable?: boolean;

    /**
     * Вид сортировки
     */
    sortKind?: SortingKind;
};