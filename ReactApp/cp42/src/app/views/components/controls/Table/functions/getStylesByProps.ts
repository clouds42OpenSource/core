import { TableCellInternalProps } from 'app/views/components/controls/Table/types';
import React from 'react';

/**
 * Функция для получения объекта стилей по значениям свойства таблицы
 * @param props Свойства таблицы
 * @returns Объект стилей
 */
export function getStylesByProps<TDataRow>(props: TableCellInternalProps<TDataRow>) {
    const style: React.CSSProperties = {};

    if (props.fieldContentNoWrap === true) {
        style.width = '1px';
    } else if (props.fieldWidth) {
        style.width = props.fieldWidth;
    }

    return style;
}