import cn from 'classnames';
import React from 'react';
import css from '../views/styles.module.css';

/**
 * Функция для возвращения иконки фильтра для таблицы
 * @returns Иконки фильтра для таблицы
 */
export function renderSorting() {
    return <i data-testid="render-sorting" className={ cn('fa fa-sort', css['table-cell-sorting-margin']) } />;
}