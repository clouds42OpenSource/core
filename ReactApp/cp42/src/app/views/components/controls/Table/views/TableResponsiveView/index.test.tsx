import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { TableResponsiveView } from 'app/views/components/controls/Table/views/TableResponsiveView';

describe('<TableResponsiveView />', () => {
    it('renders', () => {
        render(<TableResponsiveView keyFieldName="indexOf" fieldsView="test_field_view" dataset={ ['test_text_1', 'test_text_2', 'test_text_3'] } />);
        expect(screen.getByTestId(/table-responsive-view/i)).toBeInTheDocument();
    });

    it('renders with title', () => {
        const testTitle = 'test_text';

        render(<TableResponsiveView
            keyFieldName="indexOf"
            fieldsView="test_field_view"
            dataset={ ['test_text_1', 'test_text_2', 'test_text_3'] }
            headerTitleTable={ { text: testTitle } }
        />);
        expect(screen.getByText(testTitle)).toBeInTheDocument();
    });

    it('renders with custom createRowActions', () => {
        const customRowActions = () => <div />;
        render(<TableResponsiveView
            keyFieldName="toFixed"
            fieldsView={ 1 }
            dataset={ [1, 2, 3] }
            createRowActions={ customRowActions }
        />);
        expect(screen.getByTestId(/table-responsive-view/i)).toBeInTheDocument();
    });
});