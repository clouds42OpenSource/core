import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { TableView } from 'app/views/components/controls/Table/views/TableView';

describe('<TableView />', () => {
    it('renders', () => {
        render(<TableView keyFieldName="indexOf" fieldsView="test_field_view" key={ Math.random() } dataset={ ['test_text_1', 'test_text_2', 'test_text_3'] } />);
        expect(screen.getByTestId(/table-view/i)).toBeInTheDocument();
    });

    it('renders with title', () => {
        const testTitle = 'test_text';
        render(<TableView
            key={ Math.random() }
            keyFieldName="indexOf"
            fieldsView="test_field_view"
            dataset={ ['test_text_1', 'test_text_2', 'test_text_3'] }
            headerTitleTable={ { text: testTitle } }
        />);
        expect(screen.getByText(testTitle)).toBeInTheDocument();
    });

    it('renders with custom createRowActions', () => {
        const customRowActions = () => <div />;
        render(<TableView
            key={ Math.random() }
            keyFieldName="toFixed"
            fieldsView={ 1 }
            dataset={ [1, 2, 3] }
            createRowActions={ customRowActions }
        />);
        expect(screen.getByTestId(/table-view/i)).toBeInTheDocument();
    });
});