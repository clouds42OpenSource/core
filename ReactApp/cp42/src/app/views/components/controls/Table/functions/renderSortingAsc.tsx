import cn from 'classnames';
import React from 'react';
import css from '../views/styles.module.css';

/**
 * Функция для возвращения иконки по возрастанию для таблицы
 * @returns Иконки по возрастанию для таблицы
 */
export function renderSortingAsc() {
    return <i data-testid="render-sorting-asc" className={ cn('fa fa-sort-asc', css['table-cell-sorting-margin']) } />;
}