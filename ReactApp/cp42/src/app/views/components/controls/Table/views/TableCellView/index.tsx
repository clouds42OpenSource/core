import { TableCell } from '@mui/material';

import { hasComponentChangesFor } from 'app/common/functions';
import { getSortaLabelElement, getStylesByProps, onHeaderClick } from 'app/views/components/controls/Table/functions';
import { TableCellInternalProps } from 'app/views/components/controls/Table/types';
import cn from 'classnames';
import React from 'react';

export class TableCellView<TDataRow> extends React.Component<TableCellInternalProps<TDataRow>> {
    public constructor(props: TableCellInternalProps<TDataRow>) {
        super(props);
    }

    public shouldComponentUpdate(nextProps: TableCellInternalProps<TDataRow>) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    public render() {
        return (

            <TableCell
                data-testid="table-cell-view__table-cell"
                onClick={ () => { onHeaderClick(this.props); } }
                className={ cn(this.props.className) }
                style={ {
                    ...getStylesByProps(this.props),
                    ...(this.props.style ?? {})
                } }
            >
                { this.props.children }
                { getSortaLabelElement({ isSortable: this.props.isSortable, sortKind: this.props.sortKind }) }
            </TableCell>
        );
    }
}