declare const styles: {
  readonly 'table': string;
  readonly 'table-cell': string;
  readonly 'table-header-cell': string;
  readonly 'wrap-table-header-cell': string;
  readonly 'pointer': string;
  readonly 'cell-no-wrap': string;
  readonly 'cell-wrap-all': string;
  readonly 'table-body-cell': string;
  readonly 'table-body-row': string;
};
export = styles;