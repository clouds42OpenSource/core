import { renderSorting, renderSortingAsc, renderSortingDesc } from 'app/views/components/controls/Table/functions';

import { GetSortableLabelElementParams } from 'app/views/components/controls/Table/types';
import { SortingKind } from 'app/common/enums';
import { TableSortLabel } from '@mui/material';

/**
 * Функция для получения элемента сортировки
 * @param params Модель параметра для получения элемента сортировки
 * @returns Элемента сортировки
 */
export function getSortaLabelElement(params: GetSortableLabelElementParams) {
    const direction = params.sortKind === SortingKind.Asc ? 'asc' : 'desc';

    return params.isSortable &&
        <TableSortLabel
            IconComponent={ changeSortIcon(params.isSortable, params.sortKind) }
            active={ true }
            direction={ direction }
        />;
}

/**
 * Функция для смены иконок по значению вида сортировки
 * @param isSortable Eсли true, то колонка может сортироваться
 * @param sortKind Вид сортировки
 * @returns Фукнцию с элементом иконки
 */
function changeSortIcon(isSortable?: boolean, sortKind?: SortingKind) {
    if (!isSortable) return void 0;

    switch (sortKind) {
        case SortingKind.Asc:
            return renderSortingAsc;
        case SortingKind.Desc:
            return renderSortingDesc;
        default:
            return renderSorting;
    }
}