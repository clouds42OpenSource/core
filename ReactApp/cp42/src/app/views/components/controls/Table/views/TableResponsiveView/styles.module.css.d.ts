declare const styles: {
    readonly 'super-responsive-table': string;
    readonly 'super-responsive-table-bordered': string;
    readonly 'table-cell': string;
    readonly 'table-header-cell': string;
    readonly 'table-header-notwrap': string;
    readonly 'historical-row': string;
    readonly 'table-body-cell': string;
    readonly 'pointer': string;
    readonly 'table-header': string
    readonly 'table-body-cell-bordered': string;
    readonly 'hidden': string;
};
export = styles;