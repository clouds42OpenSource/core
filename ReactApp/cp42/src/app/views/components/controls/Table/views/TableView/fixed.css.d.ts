declare const styles: {
  readonly 'MuiTableBody-root': string;
  readonly 'MuiTableRow-root': string;
  readonly 'MuiTableCell-sizeSmall': string;
  readonly 'table-row-highlight-on-hover': string;
};
export = styles;