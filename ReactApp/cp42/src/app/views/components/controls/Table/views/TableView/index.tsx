import './fixed.css';

import { Box, Table, TableBody, TableHead, TableRow } from '@mui/material';
import { TableCellProps, TableProps } from 'app/views/components/controls/Table/types';

import { hasComponentChangesFor } from 'app/common/functions';
import { TextOut } from 'app/views/components/TextOut';
import { TableCellView } from 'app/views/components/controls/Table/views/TableCellView';
import cn from 'classnames';
import React from 'react';
import css from './styles.module.css';

export class TableView<TDataRow> extends React.Component<TableProps<TDataRow>> {
    constructor(props: TableProps<TDataRow>) {
        super(props);
        this.renderTable = this.renderTable.bind(this);
        this.renderEmptyTable = this.renderEmptyTable.bind(this);
    }

    public componentDidMount() {
        if (this.props.sortFieldName && this.props.sortKind !== void 0 && this.props.onHeaderSortClick) {
            this.props.onHeaderSortClick(this.props.sortKind, this.props.sortFieldName, true);
        }
    }

    public shouldComponentUpdate(nextProps: TableProps<TDataRow>) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private renderEmptyTable() {
        return (
            <TextOut
                refreshId={ this.props.refreshId }
                inDiv={ true }
                style={ { marginTop: '15px' } }
                textAlign="center"
                fontSize={ 13 }
            >
                {
                    this.props.emptyText
                        ? this.props.emptyText
                        : this.props.filterData?.searchLine
                            ? `По запросу ${ this.props.filterData?.searchLine ? `"${ this.props.filterData?.searchLine }"` : '' }   не удалось найти совпадений. Попробуйте другой вариант поисковой фразы.`
                            : 'нет записей'
                }
            </TextOut>
        );
    }

    private renderTable() {
        const fieldNames = Object.keys(this.props.fieldsView) as Array<keyof TDataRow>;
        const fieldValues = Object.values(this.props.fieldsView) as Array<TableCellProps<TDataRow>>;
        const dataRows = this.props.dataset ?? [];
        const { keyFieldName } = this.props;
        const { generateKeyFieldValue } = this.props;

        return (
            <div data-refreshid={ this.props.refreshId }>
                { this.props.headerTitleTable?.text &&
                    <Box marginY="10px">
                        <TextOut fontWeight={ 700 } fontSize={ 14 }>{ this.props.headerTitleTable?.text }</TextOut>
                    </Box>
                }
                <Table size="small" className={ `${ cn(css.table) } ${ this.props.className }` } data-testid="table-view">
                    <TableHead>
                        <TableRow>
                            { fieldValues.map((field, index) => (
                                <TableCellView
                                    { ...field }
                                    onHeaderSortClick={ this.props.onHeaderSortClick }
                                    fieldName={ fieldNames[index] }
                                    sortKind={ this.props.sortFieldName === fieldNames[index] ? this.props.sortKind : void 0 }
                                    key={ (fieldNames[index]).toString() }
                                    className={ cn({
                                        [css['table-cell']]: true,
                                        [css['table-header-cell']]: true,
                                        [css.pointer]: field.isSortable === true,
                                        [css['wrap-table-header-cell']]: fieldValues[index].wrapCellHeader
                                    }) }
                                >
                                    { field.caption ?? fieldNames[index] }
                                </TableCellView>
                            )) }
                            <TableCellView className={ cn(css['table-header-cell']) } fieldWidth="1px" />
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        { dataRows.map((row, rowIndex) => (
                            <TableRow
                                key={
                                    generateKeyFieldValue
                                        ? `${ generateKeyFieldValue(row) }${ (row[keyFieldName] as unknown as object).toString() }`
                                        : `${ (row[keyFieldName] as unknown as object)?.toString() ?? '' } ${ this.props.isIndexToKeyFieldName ? rowIndex : '' }`
                                }
                                className={ cn(css['table-body-row']) }
                            >
                                { fieldNames.map((dataField, cellIndex) => (
                                    <TableCellView
                                        key={
                                            generateKeyFieldValue
                                                ? `${ generateKeyFieldValue(row) }${ row[dataField] ?? '' }${ cellIndex }`
                                                : `${ row[keyFieldName] }${ row[dataField] ?? '' }${ cellIndex }`
                                        }
                                        { ...fieldValues[cellIndex] }
                                        isSortable={ false }
                                        className={ cn({
                                            [css['table-cell']]: true,
                                            [css['table-body-cell']]: true,
                                            [css['cell-no-wrap']]: fieldValues[cellIndex].fieldContentNoWrap,
                                            [css['cell-wrap-all']]: !fieldValues[cellIndex].fieldContentNoWrap && fieldValues[cellIndex].fieldContentWrapEverySymbol
                                        }) }
                                    >
                                        { (fieldValues && fieldValues[cellIndex] && fieldValues[cellIndex].format)
                                            ? fieldValues[cellIndex].format!(row[dataField], row)
                                            : row[dataField]
                                        }
                                    </TableCellView>
                                )) }
                                { this.props.createRowActions
                                    ? (
                                        <TableCellView fieldWidth="1px" style={ { padding: 0, textAlign: 'right', verticalAlign: 'middle' } }>
                                            <div style={ { whiteSpace: 'nowrap' } }>
                                                { this.props.createRowActions(row) }
                                            </div>
                                        </TableCellView>
                                    )
                                    : <TableCellView fieldWidth="1px" />
                                }
                            </TableRow>
                        )) }
                    </TableBody>
                </Table>
            </div>
        );
    }

    public render() {
        return this.props.dataset?.length > 0 ? this.renderTable() : this.renderEmptyTable();
    }
}