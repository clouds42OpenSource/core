import { Box } from '@mui/material';
import { TableCellProps, TableProps } from 'app/views/components/controls/Table/types';
import { TableCellResponseView } from 'app/views/components/controls/Table/views/TableResponsiveView/partial-views/TableCellResponseView';
import { TextOut } from 'app/views/components/TextOut';
import cn from 'classnames';
import React from 'react';
import { Table, Tbody, Thead, Tr } from 'react-super-responsive-table';
import 'react-super-responsive-table/dist/SuperResponsiveTableStyle.css';
import css from './styles.module.css';

export class TableResponsiveView<TDataRow> extends React.Component<TableProps<TDataRow>> {
    constructor(props: TableProps<TDataRow>) {
        super(props);
        this.renderTable = this.renderTable.bind(this);
        this.renderEmptyText = this.renderEmptyText.bind(this);
    }

    public componentDidMount() {
        const { sortFieldName, sortKind, onHeaderSortClick } = this.props;

        if (sortFieldName && sortKind !== void 0 && onHeaderSortClick) {
            onHeaderSortClick(sortKind, sortFieldName, true);
        }
    }

    private renderEmptyText() {
        return (
            <TextOut
                refreshId={ this.props.refreshId }
                inDiv={ true }
                style={ { marginTop: '15px' } }
                textAlign="center"
                fontSize={ 13 }
            >
                {
                    this.props.emptyText
                        ? this.props.emptyText
                        : this.props.filterData?.searchLine
                            ? `По запросу ${ this.props.filterData?.searchLine ? `"${ this.props.filterData?.searchLine }"` : '' } не удалось найти совпадений. Попробуйте другой вариант поисковой фразы.`
                            : 'нет записей'
                }
            </TextOut>
        );
    }

    private renderTable() {
        const fieldNames = Object.keys(this.props.fieldsView) as Array<keyof TDataRow>;
        const fieldValues = Object.values(this.props.fieldsView) as Array<TableCellProps<TDataRow>>;
        const dataRows = this.props.dataset ?? [];
        const { keyFieldName } = this.props;
        const { generateKeyFieldValue } = this.props;

        return (
            <div data-refreshid={ this.props.refreshId }>
                { this.props.headerTitleTable?.text &&
                <Box marginY="10px">
                    <TextOut fontWeight={ 700 } fontSize={ 14 }>{ this.props.headerTitleTable?.text }</TextOut>
                </Box>
                }
                <Table
                    size="small"
                    className={ `${ cn({
                        [css['super-responsive-table']]: true,
                        [css['super-responsive-table-bordered']]: this.props.isBorderStyled
                    }) } ${ this.props.className }` }
                    data-testid="table-responsive-view"
                >
                    <Thead>
                        <Tr>
                            {
                                fieldValues.map((field, index) => (
                                    <TableCellResponseView
                                        isHeader={ true }
                                        { ...field }
                                        onHeaderSortClick={ this.props.onHeaderSortClick }
                                        fieldName={ fieldNames[index] }
                                        sortKind={ this.props.sortFieldName === fieldNames[index] ? this.props.sortKind : void 0 }
                                        key={ (fieldNames[index]).toString() }
                                        className={ cn({
                                            [css['table-header-notwrap']]: !this.props.isHeaderWrap,
                                            [css['table-cell']]: true,
                                            [css['table-header-cell']]: true,
                                            [css.pointer]: field.isSortable === true
                                        }) }
                                    >

                                        { field.caption ?? fieldNames[index] }

                                    </TableCellResponseView>
                                ))
                            }
                            <TableCellResponseView
                                isHeader={ true }
                                className={ cn(css['table-header-cell']) }
                                fieldWidth="1px"
                            />
                        </Tr>
                    </Thead>
                    <Tbody>
                        { dataRows.map(row => (
                            <Tr
                                key={
                                    generateKeyFieldValue
                                        ? `${ generateKeyFieldValue(row) }${ JSON.stringify(row[keyFieldName]) }${ Math.random() }`
                                        : JSON.stringify(row[keyFieldName])
                                }
                                className={ (row as any).IsHistoricalRow ? css['historical-row'] : undefined }
                            >
                                { fieldNames.map((dataField, cellIndex) => (
                                    <TableCellResponseView
                                        key={
                                            generateKeyFieldValue
                                                ? `${ generateKeyFieldValue(row) }${ row[dataField] ?? '' }${ cellIndex }`
                                                : `${ row[keyFieldName] }${ row[dataField] ?? '' }${ cellIndex }`
                                        }
                                        { ...fieldValues[cellIndex] }
                                        className={ cn({
                                            [css['table-cell']]: true,
                                            [css['table-body-cell']]: true,
                                            [css['table-body-cell-bordered']]: this.props.isBorderStyled,
                                            [css.hidden]: fieldValues[cellIndex].noNeedForMobile
                                        }) }
                                    >
                                        {
                                            (fieldValues && fieldValues[cellIndex] && fieldValues[cellIndex].format)
                                                ? fieldValues[cellIndex].format!(row[dataField], row)
                                                : row[dataField]

                                        }
                                    </TableCellResponseView>
                                )) }
                                { this.props.createRowActions &&
                                <TableCellResponseView
                                    fieldWidth="1px"
                                    style={ { padding: 0, textAlign: 'right', verticalAlign: 'middle' } }
                                >
                                    <div style={ {
                                        whiteSpace: 'nowrap'
                                    } }
                                    >
                                        { this.props.createRowActions(row) }
                                    </div>
                                </TableCellResponseView>
                                }
                            </Tr>
                        )) }
                    </Tbody>
                </Table>
            </div>
        );
    }

    public render() {
        return this.props.dataset.length > 0 ? this.renderTable() : this.renderEmptyText();
    }
}