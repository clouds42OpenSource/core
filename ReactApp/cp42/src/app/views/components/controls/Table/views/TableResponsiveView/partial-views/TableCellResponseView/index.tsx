import { AppConsts } from 'app/common/constants';
import { getSortaLabelElement, getStylesByProps, onHeaderClick } from 'app/views/components/controls/Table/functions';
import { TableCellInternalProps } from 'app/views/components/controls/Table/types';
import cn from 'classnames';
import React from 'react';

export class TableCellResponseView<TDataRow> extends React.Component<TableCellInternalProps<TDataRow>> {
    public constructor(props: TableCellInternalProps<TDataRow>) {
        super(props);

        this.getTableRows = this.getTableRows.bind(this);
    }

    /**
     * Получить Element ячейки td/th в зависимости от значения флага isHeader
     * @returns Element ячейки td/th в зависимости от значения флага isHeader
     */
    private getTableRows() {
        const isMobileSize = window.innerWidth <= AppConsts.mobileScreenWidth;
        const style = getStylesByProps(this.props);
        const sortableHeader = getSortaLabelElement({ isSortable: this.props.isSortable, sortKind: this.props.sortKind });
        const childValue = !this.props.children ? <br /> : this.props.children;
        let label: string;

        if (isMobileSize && this.props.mobileCaption) {
            label = `${ this.props.mobileCaption }:`;
        } else {
            label = !this.props.caption ? '' : `${ this.props.caption }:`;

            if (label === '[object Object]:') {
                label = '';
            }
        }

        if (!this.props.isHeader) {
            return (
                <td
                    data-testid="table-cell-response-view__td"
                    data-label={ label }
                    onClick={ () => { onHeaderClick(this.props); } }
                    className={ cn(this.props.className) }
                    style={ { ...(this.props.style ?? {}) } }
                >
                    { childValue }
                </td>
            );
        }

        return (
            <th
                data-testid="table-cell-response-view__th"
                onClick={ () => { onHeaderClick(this.props); } }
                className={ cn(this.props.className) }
                style={ {
                    ...style,
                    ...(this.props.style ?? {})
                } }
            >
                { this.props.children }
                { sortableHeader }
            </th>
        );
    }

    public render() {
        return this.getTableRows();
    }
}