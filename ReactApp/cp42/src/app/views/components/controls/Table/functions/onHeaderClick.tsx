import { SortingKind } from 'app/common/enums';
import { TableCellInternalProps } from 'app/views/components/controls/Table/types';

/**
 * Функция для обработки клика по заголовку при сортировке
 * @param props Свойства таблицы
 */
export function onHeaderClick<TDataRow>(props: TableCellInternalProps<TDataRow>) {
    if (props.isSortable && props.onHeaderSortClick) {
        props.onHeaderSortClick(
            props.sortKind === SortingKind.Asc
                ? SortingKind.Desc
                : SortingKind.Asc,
            props.fieldName!,
            false);
    }
}