import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import userEvent from '@testing-library/user-event';

import { Pagination } from './index';

describe('<PaginationView />', () => {
    const onPageChangedMock: jest.Mock = jest.fn();

    it('renders', () => {
        const { container } = render(<Pagination selectCount={ [] } totalPages={ 2 } currentPage={ 10 } pageSize={ 0 } recordsCount={ 2 } onPageChanged={ onPageChangedMock } />);
        expect(screen.getByTestId(/text-view__span/i)).toBeInTheDocument();
        expect(container.getElementsByTagName('nav').length).toBe(1);
    });

    it('does not renders pagination view', () => {
        const { container } = render(<Pagination selectCount={ [] } totalPages={ 1 } currentPage={ 10 } pageSize={ 0 } recordsCount={ 2 } onPageChanged={ onPageChangedMock } />);
        expect(screen.getByTestId(/text-view__span/i)).toBeInTheDocument();
        expect(container.getElementsByTagName('nav').length).toBe(0);
    });

    it('does not renders pagination text view', () => {
        const { container } = render(<Pagination selectCount={ [] } totalPages={ 2 } currentPage={ 10 } pageSize={ 0 } recordsCount={ 0 } onPageChanged={ onPageChangedMock } />);
        expect(screen.queryByTestId(/text-view__span/i)).not.toBeInTheDocument();
        expect(container.getElementsByTagName('nav').length).toBe(1);
    });

    it('does not renders', () => {
        const { container } = render(<Pagination selectCount={ [] } totalPages={ 0 } currentPage={ 0 } pageSize={ 0 } recordsCount={ 0 } onPageChanged={ onPageChangedMock } />);
        expect(screen.getByTestId(/pagination-view/i)).toBeInTheDocument();
        expect(screen.queryByTestId(/text-view__span/i)).not.toBeInTheDocument();
        expect(container.getElementsByTagName('nav').length).toBe(0);
    });

    it('has onChange method', async () => {
        render(<Pagination selectCount={ [] } totalPages={ 2 } currentPage={ 10 } pageSize={ 0 } recordsCount={ 2 } onPageChanged={ onPageChangedMock } />);
        const objectWithOnChange = screen.queryAllByRole('button')[0];
        await userEvent.click(objectWithOnChange);
        expect(onPageChangedMock).toHaveBeenCalled();
    });
});