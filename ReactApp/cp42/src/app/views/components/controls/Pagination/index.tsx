import { memo, ReactNode } from 'react';
import { Box, MenuItem, Pagination as MuiPagination, Select } from '@mui/material';

import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { TextOut } from 'app/views/components/TextOut';

export type PaginationProps = {
    selectIsVisible?: boolean;
    hideRecordsCount?: boolean;
    totalPages: number;
    currentPage: number;
    pageSize: number;
    recordsCount: number;
    onPageChanged: (selectedPage: number) => void;
    onSelectCount?: (pageSize: number) => void;
    selectCount: string[];
    leftAlignContent?: ReactNode;
};

export const Pagination = memo(({ selectIsVisible, hideRecordsCount, totalPages, currentPage, pageSize, recordsCount, onPageChanged, onSelectCount, selectCount, leftAlignContent }: PaginationProps) => {
    return (
        <Box data-testid="pagination-view" display="flex" flexWrap="wrap" alignItems="center" justifyContent="space-between" marginTop="15px">
            { recordsCount >= 1 && !hideRecordsCount && !leftAlignContent && (
                <div data-testid="pagination-text-view">
                    <TextOut fontSize={ 13 } fontWeight={ 400 }>
                        Записи: { (currentPage - 1) * pageSize + 1 }-{ Math.min((currentPage) * pageSize, recordsCount) } из { recordsCount }
                    </TextOut>
                </div>
            ) }
            { recordsCount >= 9 && !hideRecordsCount && selectIsVisible && !leftAlignContent && (
                <FormAndLabel label="Записей на странице">
                    <Select
                        sx={ { width: '130px' } }
                        variant="outlined"
                        value={ pageSize }
                        onChange={ ({ target: { value } }) => onSelectCount ? onSelectCount(typeof value === 'string' ? parseInt(value, 10) : value) : undefined }
                    >
                        { selectCount.map(item => (<MenuItem key={ item } value={ item }>{ item }</MenuItem>)) }
                    </Select>
                </FormAndLabel>
            ) }
            { leftAlignContent }
            { totalPages > 1 && (
                <MuiPagination
                    count={ totalPages }
                    onChange={ (_, p) => onPageChanged(p) }
                    page={ currentPage }
                />
            ) }
        </Box>
    );
});