import { InitializeReduxFormParams } from 'app/redux/redux-forms/store/reducers/initializeReduxFormReducer/params';
import { ResetReduxFormParams } from 'app/redux/redux-forms/store/reducers/resetReduxFormReducer/params';
import { UpdateReduxFormFieldsParams } from 'app/redux/redux-forms/store/reducers/updateReduxFormFieldsReducer/params';
import { InitializeReduxFormThunk, ResetReduxFormThunk, UpdateReduxFormFieldsThunk } from 'app/redux/redux-forms/store/thunks';
import { ReduxFormData } from 'app/redux/redux-forms/types/ReduxFormData';
import { AppReduxStoreState } from 'app/redux/types';
import { DateUtility } from 'app/utils';
import React, { forwardRef } from 'react';
import { connect } from 'react-redux';

export type UrlReduxFormData<TFormDataType> = Partial<{ [key in keyof TFormDataType]: string }>;

export type ReduxFormPropsType<TFormDataType> = {
    reduxFormName: string;
    isInitialized: boolean;
    getReduxFormFields: (onlyChanged: boolean) => TFormDataType;
    updateReduxFormFields: (payload: Partial<TFormDataType>) => void;
    resetReduxForm: () => void;
    setInitializeFormDataAction: (action: () => TFormDataType | undefined) => void;
    setUrlDataToReduxFormDataConverter: (converter: (urlData: UrlReduxFormData<TFormDataType>) => Partial<TFormDataType>) => void;
};
export type ReduxFormProps<TFormDataType> = {
    reduxForm: ReduxFormPropsType<TFormDataType>
};

type WithReduxFormClassOwnProps<TFormDataType> = {
    Component: React.ComponentType<ReduxFormProps<TFormDataType>>;
    reduxFormName: string;
};
type WithReduxFormClassDispatchProps = {
    dispatchUpdateReduxFormFieldsThunk: <TFormDataType>(args: UpdateReduxFormFieldsParams<TFormDataType>) => void;
    dispatchResetReduxFormThunk: (args: ResetReduxFormParams) => void;
    dispatchInitializeReduxFormThunk: <TFormDataType>(args: InitializeReduxFormParams<TFormDataType>) => void;
};

type WithReduxFormClassStateProps<TFormDataType> = {
    formData: ReduxFormData<TFormDataType> | undefined;
};

export interface IReduxForm<TFormDataType> {
    updateReduxFormFields(payload: Partial<TFormDataType>): void;
    resetReduxForm(): void;
    getReduxForm(): ReduxFormPropsType<TFormDataType>;
}

function getChangedProps<TProps extends { [key: string]: any }>(newProps: TProps, initialProps: TProps): TProps {
    const changedProps = Object.keys(newProps).reduce((diff, key) => {
        if (initialProps[key] === newProps[key]) return diff;
        return {
            ...diff,
            [key]: newProps[key]
        };
    }, {});

    return changedProps as TProps;
}

function getWithReduxFormConnectedComponent<TFormDataType extends { [key: string]: any; }>(reduxFormName: string, resetOnUnmount?: boolean) {
    type WithReduxFormClassAllProps = WithReduxFormClassOwnProps<TFormDataType> & WithReduxFormClassDispatchProps & WithReduxFormClassStateProps<TFormDataType>;

    class WithReduxFormClass extends React.Component<WithReduxFormClassAllProps> implements IReduxForm<TFormDataType> {
        private _actions: {
            getInitializeFormData?: () => (TFormDataType | undefined);
            urlDataToReduxFormDataConverter?: (urlData: UrlReduxFormData<TFormDataType>) => Partial<TFormDataType>;
        } = {};

        public constructor(props: WithReduxFormClassAllProps) {
            super(props);
            this.updateReduxFormFields = this.updateReduxFormFields.bind(this);
            this.resetReduxForm = this.resetReduxForm.bind(this);
            this.initReduxFormData = this.initReduxFormData.bind(this);
            this.getReduxForm = this.getReduxForm.bind(this);
            this.getIsReduxFormInitialized = this.getIsReduxFormInitialized.bind(this);
            this.setInitializeFormDataAction = this.setInitializeFormDataAction.bind(this);
            this.setUrlDataToReduxFormDataConverter = this.setUrlDataToReduxFormDataConverter.bind(this);
            this.getUrlFormDataFields = this.getUrlFormDataFields.bind(this);
            this.getReduxFormFields = this.getReduxFormFields.bind(this);
        }

        public componentDidUpdate() {
            if (this.getIsReduxFormInitialized()) {
                return;
            }

            const changedFields = this.getReduxFormFields(true);

            if (Object.keys(changedFields).length === 0) {
                return;
            }

            this.initReduxFormData(changedFields);
        }

        public componentWillUnmount() {
            if (resetOnUnmount ?? true) {
                this.resetReduxForm();
            }
            this._actions.getInitializeFormData = void 0;
            this._actions.urlDataToReduxFormDataConverter = void 0;
        }

        public getReduxForm(): ReduxFormPropsType<TFormDataType> & { _fields?: Partial<TFormDataType> } {
            return {
                /**
                 * Нужно для отрисовки, так как проверяется изменчивасть
                 */
                _fields: this.props.formData?.fields,
                reduxFormName,
                updateReduxFormFields: this.updateReduxFormFields,
                resetReduxForm: this.resetReduxForm,
                setInitializeFormDataAction: this.setInitializeFormDataAction,
                isInitialized: this.getIsReduxFormInitialized(),
                getReduxFormFields: this.getReduxFormFields,
                setUrlDataToReduxFormDataConverter: this.setUrlDataToReduxFormDataConverter
            };
        }

        private getIsReduxFormInitialized() {
            return this.props.formData?.isInitialized ?? false;
        }

        private getReduxFormFields(onlyChanged: boolean): TFormDataType {
            const reduxFormFields = this.props.formData?.fields ?? {} as TFormDataType;

            const initFields = this._actions.getInitializeFormData
                ? {
                    ...(this._actions.getInitializeFormData() ?? {}),
                    ...this.getUrlFormDataFields()
                } as TFormDataType
                : {} as TFormDataType;

            const changedFields = getChangedProps<TFormDataType>(reduxFormFields, initFields);

            if (onlyChanged) {
                return changedFields;
            }

            return {
                ...initFields,
                ...changedFields
            };
        }

        private getUrlFormDataFields() {
            if (this._actions.urlDataToReduxFormDataConverter) {
                const url = new URL(window.location.href);
                const urlParams: Partial<{ [key: string]: string }> = {};

                url.searchParams.forEach((value, key) => {
                    urlParams[key] = value;
                });

                const converted = this._actions.urlDataToReduxFormDataConverter(urlParams as any);

                Object.keys(converted).forEach(key => converted[key as keyof TFormDataType] === void 0 && delete converted[key as keyof TFormDataType]);

                return converted;
            }
            return {};
        }

        private setInitializeFormDataAction(action: () => TFormDataType | undefined) {
            this._actions.getInitializeFormData = action;
        }

        private setUrlDataToReduxFormDataConverter(action: (urlData: UrlReduxFormData<TFormDataType>) => Partial<TFormDataType>) {
            this._actions.urlDataToReduxFormDataConverter = action;
        }

        private initReduxFormData(data: TFormDataType) {
            const isInitialized = this.props.formData?.isInitialized ?? false;
            if (isInitialized) { return; }

            this.props.dispatchInitializeReduxFormThunk({
                form: this.props.reduxFormName,
                data
            });
        }

        public updateReduxFormFields(payload: Partial<TFormDataType>) {
            this.props.dispatchUpdateReduxFormFieldsThunk({
                form: this.props.reduxFormName,
                data: payload
            });

            if (this._actions.urlDataToReduxFormDataConverter) {
                const url = new URL(window.location.href);

                const initFields: Partial<TFormDataType> = this._actions.getInitializeFormData
                    ? this._actions.getInitializeFormData() ?? {}
                    : {};

                Object.keys(payload).forEach(fieldKey => {
                    const field = fieldKey as keyof TFormDataType;
                    const value: any = payload[field];

                    const fieldValue = value instanceof Date
                        ? DateUtility.dateToIsoDateString(value, {
                            includeTime: true,
                            includeMilliseconds: true
                        })
                        : payload[field] as unknown as string;

                    if (fieldValue && initFields[field] !== void 0 && initFields[field] as any !== fieldValue) {
                        url.searchParams.set(fieldKey, fieldValue);
                    } else {
                        url.searchParams.delete(fieldKey);
                    }

                    if (window.history && window.history.pushState) {
                        window.history.pushState(void 0, '', url.toString());
                    }
                });
            }
        }

        public resetReduxForm() {
            this.props.dispatchResetReduxFormThunk({
                form: this.props.reduxFormName
            });
        }

        public render() {
            const {
                Component,
                ...rest
            } = this.props;
            return <Component reduxForm={ this.getReduxForm() } { ...rest } />;
        }
    }

    return connect<WithReduxFormClassStateProps<TFormDataType>, WithReduxFormClassDispatchProps, WithReduxFormClassOwnProps<TFormDataType>, AppReduxStoreState>(
        state => {
            const formData = state.ReduxForms[reduxFormName];
            return {
                formData
            };
        },
        {
            dispatchUpdateReduxFormFieldsThunk: UpdateReduxFormFieldsThunk.invoke,
            dispatchResetReduxFormThunk: ResetReduxFormThunk.invoke,
            dispatchInitializeReduxFormThunk: InitializeReduxFormThunk.invoke
        },
        null,
        { forwardRef: true }
    )(WithReduxFormClass);
}

export function withReduxForm<TComponentProps extends ReduxFormProps<any>, OwnProps = Omit<TComponentProps, keyof ReduxFormProps<any>>>(
    Component: React.ComponentType<TComponentProps>,
    options: {
        reduxFormName: string;
        resetOnUnmount?: boolean;
    }
) {
    const WithReduxFormConnected = getWithReduxFormConnectedComponent(options.reduxFormName, options.resetOnUnmount);

    return forwardRef<IReduxForm<any>, OwnProps>((props, ref) => {
        return <WithReduxFormConnected Component={ Component as any } { ...props } ref={ ref as any } resetOnUnmount={ options.resetOnUnmount } reduxFormName={ options.reduxFormName } />;
    });
}