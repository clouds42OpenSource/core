import { memo } from 'react';
import { Helmet } from 'react-helmet';

type TProps = {
    title: string;
};

const defaultTitle = '...';

export const PageHeaderView = memo(({ title }: TProps) => {
    return (
        <Helmet>
            <title>{ title || defaultTitle }</title>
        </Helmet>
    );
});