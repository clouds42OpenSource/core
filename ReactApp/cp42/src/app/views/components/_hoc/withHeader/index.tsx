import { ComponentType } from 'react';
import { PageHeaderView } from './PageHeaderView';

type TProps = {
/* eslint-disable @typescript-eslint/no-explicit-any */
    page: ComponentType<any>;
    title: string;
};

export const withHeader = ({ page: Component, title }: TProps) => () => (
    <>
        <PageHeaderView title={ title } />
        <Component />
    </>
);