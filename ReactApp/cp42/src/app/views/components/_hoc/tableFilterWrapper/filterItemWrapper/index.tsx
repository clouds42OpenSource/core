import { AppConsts } from 'app/common/constants';
import React from 'react';
import { hasComponentChangesFor } from 'app/common/functions';

type OwnProps = {
    /** ширина контейнера можно указывать в px | % */
    width: string;
    /** стили для контейнера */
    style?: React.CSSProperties;
};

export class FilterItemWrapper extends React.Component<OwnProps> {
    public shouldComponentUpdate(props: OwnProps) {
        return hasComponentChangesFor(this.props, props);
    }

    public render() {
        const isMobileSized = window.innerWidth <= AppConsts.mobileScreenWidth;
        const width = !isMobileSized ? this.props.width : '100%';
        return <div style={ { width, ...this.props.style } }>{ this.props.children }</div>;
    }
}