import React from 'react';
import cn from 'classnames';

import { EPartnersTourId, ERefreshId, ETourId } from 'app/views/Layout/ProjectTour/enums';
import { hasComponentChangesFor } from 'app/common/functions';

import css from './styles.module.css';

type TTableFilterWrapper = {
    children?: React.ReactNode;
    tourId?: ETourId | EPartnersTourId;
    refreshId?: ERefreshId;
};

export class TableFilterWrapper extends React.Component<TTableFilterWrapper> {
    public shouldComponentUpdate(nextProps: TTableFilterWrapper) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    public render() {
        return (
            <div className={ cn(css['filter-wrapper']) } data-tourid={ this.props.tourId } data-refreshid={ this.props.refreshId }>
                { this.props.children }
            </div>
        );
    }
}