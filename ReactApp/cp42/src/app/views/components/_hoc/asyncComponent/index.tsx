import { SkeletonLoading } from 'app/views/components/Loading';
import { ComponentType, Suspense, lazy } from 'react';

export const asyncComponent = (factory: () => Promise<ComponentType>) => () => {
    const Component = lazy(() => factory().then(ComponentClass => ({ default: ComponentClass })));

    return (
        <Suspense fallback={ <SkeletonLoading /> }>
            <Component />
        </Suspense>
    );
};