import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps, IShowMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { showDefaultMessage, showErrorMessage, showInfoMessage, showSuccessMessage, showWarningMessage } from 'app/views/components/_hoc/withFloatMessages/showMessage/Index';
import { withSnackbar, WithSnackbarProps } from 'notistack';
import React, { ReactNode } from 'react';
import './fixed.css';

export function withFloatMessages<P extends FloatMessageProps, OwnProps extends Omit<P, keyof FloatMessageProps> & WithSnackbarProps>(Component: React.ComponentType<P>) {
    const showMessageOfType = (props: WithSnackbarProps, type: MessageType, content: ReactNode, autoHideDuration?: number) => {
        const msgProps: IShowMessageProps = {
            content: typeof (content) === 'string'
                ? content.split('\n').map(value => <div key={ value }>{ value }</div>)
                : content,
            autoHideDuration,
            ...props
        };

        switch (type) {
            case MessageType.Success: {
                showSuccessMessage(msgProps);
                break;
            }
            case MessageType.Error: {
                showErrorMessage(msgProps);
                break;
            }
            case MessageType.Warning: {
                showWarningMessage(msgProps);
                break;
            }
            case MessageType.Info: {
                showInfoMessage(msgProps);
                break;
            }
            default: {
                showDefaultMessage(msgProps);
                break;
            }
        }
    };

    const FloatMessages = (props: OwnProps) => {
        const showMessage = (type: MessageType, content: ReactNode, autoHideDuration?: number) => {
            showMessageOfType(props, type, content, autoHideDuration);
        };

        const { ...rest } = props;
        return (<Component { ...rest as any } floatMessage={ { show: showMessage } } />);
    };

    return withSnackbar(FloatMessages);
}