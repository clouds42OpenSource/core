import cn from 'classnames';
import React, { ReactNode } from 'react';
import css from './styles.module.css';

interface IOwnProps {
    children?: ReactNode;
}

export const MessageContentView = ({ children }: IOwnProps) => {
    return (
        <div className={ cn(css['msg-content']) }>
            { children }
        </div>
    );
};