import { IShowMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { MessageContentView } from 'app/views/components/_hoc/withFloatMessages/MessageContent/MessageContent';
import React from 'react';

export function showSuccessMessage(props: IShowMessageProps) {
    const key = props.enqueueSnackbar((<MessageContentView>{ props.content } </MessageContentView>),
        {
            variant: 'success',
            anchorOrigin: {
                vertical: 'top',
                horizontal: 'right',
            },
            onClick: () => {
                props.closeSnackbar(key);
            },
            className: 'override-success',
            autoHideDuration: props.autoHideDuration ?? 3000
        });
}