import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { ReactNode } from 'react';

export type FloatMessageProps = {
    floatMessage: {
        show: (type: MessageType, content: ReactNode, autoHideDuration?: number) => void;
    }
};