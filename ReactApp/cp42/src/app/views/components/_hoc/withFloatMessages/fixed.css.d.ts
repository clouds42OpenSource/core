declare const styles: {
  readonly 'MuiCollapse-wrapperInner': string;
  readonly 'override-success': string;
  readonly 'override-warning': string;
};
export = styles;