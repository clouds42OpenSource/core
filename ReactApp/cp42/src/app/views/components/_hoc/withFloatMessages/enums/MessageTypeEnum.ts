export enum MessageType {
    Default,
    Success,
    Error,
    Info,
    Warning
}