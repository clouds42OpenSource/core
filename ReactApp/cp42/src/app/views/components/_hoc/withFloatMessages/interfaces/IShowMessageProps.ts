import { WithSnackbarProps } from 'notistack';
import React from 'react';

export interface IShowMessageProps extends WithSnackbarProps {
    content: React.ReactNode;
    autoHideDuration?: number;
}