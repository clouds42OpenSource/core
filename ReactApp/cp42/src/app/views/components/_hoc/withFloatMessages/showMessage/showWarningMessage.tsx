import { IShowMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { MessageContentView } from 'app/views/components/_hoc/withFloatMessages/MessageContent/MessageContent';
import React from 'react';

export function showWarningMessage(props: IShowMessageProps) {
    const key = props.enqueueSnackbar((<MessageContentView>{ props.content }</MessageContentView>),
        {
            variant: 'warning',
            anchorOrigin: {
                vertical: 'top',
                horizontal: 'right'
            },
            className: 'override-warning',
            onClick: () => {
                props.closeSnackbar(key);
            },
            autoHideDuration: props.autoHideDuration ?? 3000
        });
}