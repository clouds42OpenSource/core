import React from 'react';
import { Skeleton } from '@mui/material';

export const SkeletonLoading = () => {
    return (
        <>
            <Skeleton animation="pulse" />
            <Skeleton animation="pulse" />
            <Skeleton animation="pulse" />
        </>
    );
};