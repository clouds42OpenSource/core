export function recagency(width: number, height: number) {
  return `<picture uniq="kadrovoev3-svg">
  <svg id="el_WwxlgbKKd" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 270 270" height="${height}" width="${width}">
    <style>
      @-webkit-keyframes el_zDP2BWOgyX_XrXrmBjC6_Animation {
      16.67% {
      -webkit-transform: translate(144.4600067138672px, 88.4000015258789px) scale(1, 1) translate(-144.4600067138672px, -88.4000015258789px);
      transform: translate(144.4600067138672px, 88.4000015258789px) scale(1, 1) translate(-144.4600067138672px, -88.4000015258789px);
      }

      18.33% {
      -webkit-transform: translate(144.4600067138672px, 88.4000015258789px) scale(0, 1) translate(-144.4600067138672px, -88.4000015258789px);
      transform: translate(144.4600067138672px, 88.4000015258789px) scale(0, 1) translate(-144.4600067138672px, -88.4000015258789px);
      }

      33.33% {
      -webkit-transform: translate(144.4600067138672px, 88.4000015258789px) scale(0, 1) translate(-144.4600067138672px, -88.4000015258789px);
      transform: translate(144.4600067138672px, 88.4000015258789px) scale(0, 1) translate(-144.4600067138672px, -88.4000015258789px);
      }

      38.89% {
      -webkit-transform: translate(144.4600067138672px, 88.4000015258789px) scale(1, 1) translate(-144.4600067138672px, -88.4000015258789px);
      transform: translate(144.4600067138672px, 88.4000015258789px) scale(1, 1) translate(-144.4600067138672px, -88.4000015258789px);
      }

      66.67% {
      -webkit-transform: translate(144.4600067138672px, 88.4000015258789px) scale(1, 1) translate(-144.4600067138672px, -88.4000015258789px);
      transform: translate(144.4600067138672px, 88.4000015258789px) scale(1, 1) translate(-144.4600067138672px, -88.4000015258789px);
      }

      68.33% {
      -webkit-transform: translate(144.4600067138672px, 88.4000015258789px) scale(0, 1) translate(-144.4600067138672px, -88.4000015258789px);
      transform: translate(144.4600067138672px, 88.4000015258789px) scale(0, 1) translate(-144.4600067138672px, -88.4000015258789px);
      }

      83.33% {
      -webkit-transform: translate(144.4600067138672px, 88.4000015258789px) scale(0, 1) translate(-144.4600067138672px, -88.4000015258789px);
      transform: translate(144.4600067138672px, 88.4000015258789px) scale(0, 1) translate(-144.4600067138672px, -88.4000015258789px);
      }

      90.56% {
      -webkit-transform: translate(144.4600067138672px, 88.4000015258789px) scale(1, 1) translate(-144.4600067138672px, -88.4000015258789px);
      transform: translate(144.4600067138672px, 88.4000015258789px) scale(1, 1) translate(-144.4600067138672px, -88.4000015258789px);
      }

      0% {
      -webkit-transform: translate(144.4600067138672px, 88.4000015258789px) scale(1, 1) translate(-144.4600067138672px, -88.4000015258789px);
      transform: translate(144.4600067138672px, 88.4000015258789px) scale(1, 1) translate(-144.4600067138672px, -88.4000015258789px);
      }

      100% {
      -webkit-transform: translate(144.4600067138672px, 88.4000015258789px) scale(1, 1) translate(-144.4600067138672px, -88.4000015258789px);
      transform: translate(144.4600067138672px, 88.4000015258789px) scale(1, 1) translate(-144.4600067138672px, -88.4000015258789px);
      }
      }

      @keyframes el_zDP2BWOgyX_XrXrmBjC6_Animation {
      16.67% {
      -webkit-transform: translate(144.4600067138672px, 88.4000015258789px) scale(1, 1) translate(-144.4600067138672px, -88.4000015258789px);
      transform: translate(144.4600067138672px, 88.4000015258789px) scale(1, 1) translate(-144.4600067138672px, -88.4000015258789px);
      }

      18.33% {
      -webkit-transform: translate(144.4600067138672px, 88.4000015258789px) scale(0, 1) translate(-144.4600067138672px, -88.4000015258789px);
      transform: translate(144.4600067138672px, 88.4000015258789px) scale(0, 1) translate(-144.4600067138672px, -88.4000015258789px);
      }

      33.33% {
      -webkit-transform: translate(144.4600067138672px, 88.4000015258789px) scale(0, 1) translate(-144.4600067138672px, -88.4000015258789px);
      transform: translate(144.4600067138672px, 88.4000015258789px) scale(0, 1) translate(-144.4600067138672px, -88.4000015258789px);
      }

      38.89% {
      -webkit-transform: translate(144.4600067138672px, 88.4000015258789px) scale(1, 1) translate(-144.4600067138672px, -88.4000015258789px);
      transform: translate(144.4600067138672px, 88.4000015258789px) scale(1, 1) translate(-144.4600067138672px, -88.4000015258789px);
      }

      66.67% {
      -webkit-transform: translate(144.4600067138672px, 88.4000015258789px) scale(1, 1) translate(-144.4600067138672px, -88.4000015258789px);
      transform: translate(144.4600067138672px, 88.4000015258789px) scale(1, 1) translate(-144.4600067138672px, -88.4000015258789px);
      }

      68.33% {
      -webkit-transform: translate(144.4600067138672px, 88.4000015258789px) scale(0, 1) translate(-144.4600067138672px, -88.4000015258789px);
      transform: translate(144.4600067138672px, 88.4000015258789px) scale(0, 1) translate(-144.4600067138672px, -88.4000015258789px);
      }

      83.33% {
      -webkit-transform: translate(144.4600067138672px, 88.4000015258789px) scale(0, 1) translate(-144.4600067138672px, -88.4000015258789px);
      transform: translate(144.4600067138672px, 88.4000015258789px) scale(0, 1) translate(-144.4600067138672px, -88.4000015258789px);
      }

      90.56% {
      -webkit-transform: translate(144.4600067138672px, 88.4000015258789px) scale(1, 1) translate(-144.4600067138672px, -88.4000015258789px);
      transform: translate(144.4600067138672px, 88.4000015258789px) scale(1, 1) translate(-144.4600067138672px, -88.4000015258789px);
      }

      0% {
      -webkit-transform: translate(144.4600067138672px, 88.4000015258789px) scale(1, 1) translate(-144.4600067138672px, -88.4000015258789px);
      transform: translate(144.4600067138672px, 88.4000015258789px) scale(1, 1) translate(-144.4600067138672px, -88.4000015258789px);
      }

      100% {
      -webkit-transform: translate(144.4600067138672px, 88.4000015258789px) scale(1, 1) translate(-144.4600067138672px, -88.4000015258789px);
      transform: translate(144.4600067138672px, 88.4000015258789px) scale(1, 1) translate(-144.4600067138672px, -88.4000015258789px);
      }
      }

      @-webkit-keyframes el_DI7HfEkfEG_hxjvQ4sC4_Animation {
      16.67% {
      -webkit-transform: translate(144.4600067138672px, 116.80000305175781px) scale(1, 1) translate(-144.4600067138672px, -116.80000305175781px);
      transform: translate(144.4600067138672px, 116.80000305175781px) scale(1, 1) translate(-144.4600067138672px, -116.80000305175781px);
      }

      18.33% {
      -webkit-transform: translate(144.4600067138672px, 116.80000305175781px) scale(0, 1) translate(-144.4600067138672px, -116.80000305175781px);
      transform: translate(144.4600067138672px, 116.80000305175781px) scale(0, 1) translate(-144.4600067138672px, -116.80000305175781px);
      }

      33.33% {
      -webkit-transform: translate(144.4600067138672px, 116.80000305175781px) scale(0, 1) translate(-144.4600067138672px, -116.80000305175781px);
      transform: translate(144.4600067138672px, 116.80000305175781px) scale(0, 1) translate(-144.4600067138672px, -116.80000305175781px);
      }

      43.89% {
      -webkit-transform: translate(144.4600067138672px, 116.80000305175781px) scale(1, 1) translate(-144.4600067138672px, -116.80000305175781px);
      transform: translate(144.4600067138672px, 116.80000305175781px) scale(1, 1) translate(-144.4600067138672px, -116.80000305175781px);
      }

      66.67% {
      -webkit-transform: translate(144.4600067138672px, 116.80000305175781px) scale(1, 1) translate(-144.4600067138672px, -116.80000305175781px);
      transform: translate(144.4600067138672px, 116.80000305175781px) scale(1, 1) translate(-144.4600067138672px, -116.80000305175781px);
      }

      68.33% {
      -webkit-transform: translate(144.4600067138672px, 116.80000305175781px) scale(0, 1) translate(-144.4600067138672px, -116.80000305175781px);
      transform: translate(144.4600067138672px, 116.80000305175781px) scale(0, 1) translate(-144.4600067138672px, -116.80000305175781px);
      }

      83.33% {
      -webkit-transform: translate(144.4600067138672px, 116.80000305175781px) scale(0, 1) translate(-144.4600067138672px, -116.80000305175781px);
      transform: translate(144.4600067138672px, 116.80000305175781px) scale(0, 1) translate(-144.4600067138672px, -116.80000305175781px);
      }

      95% {
      -webkit-transform: translate(144.4600067138672px, 116.80000305175781px) scale(1, 1) translate(-144.4600067138672px, -116.80000305175781px);
      transform: translate(144.4600067138672px, 116.80000305175781px) scale(1, 1) translate(-144.4600067138672px, -116.80000305175781px);
      }

      0% {
      -webkit-transform: translate(144.4600067138672px, 116.80000305175781px) scale(1, 1) translate(-144.4600067138672px, -116.80000305175781px);
      transform: translate(144.4600067138672px, 116.80000305175781px) scale(1, 1) translate(-144.4600067138672px, -116.80000305175781px);
      }

      100% {
      -webkit-transform: translate(144.4600067138672px, 116.80000305175781px) scale(1, 1) translate(-144.4600067138672px, -116.80000305175781px);
      transform: translate(144.4600067138672px, 116.80000305175781px) scale(1, 1) translate(-144.4600067138672px, -116.80000305175781px);
      }
      }

      @keyframes el_DI7HfEkfEG_hxjvQ4sC4_Animation {
      16.67% {
      -webkit-transform: translate(144.4600067138672px, 116.80000305175781px) scale(1, 1) translate(-144.4600067138672px, -116.80000305175781px);
      transform: translate(144.4600067138672px, 116.80000305175781px) scale(1, 1) translate(-144.4600067138672px, -116.80000305175781px);
      }

      18.33% {
      -webkit-transform: translate(144.4600067138672px, 116.80000305175781px) scale(0, 1) translate(-144.4600067138672px, -116.80000305175781px);
      transform: translate(144.4600067138672px, 116.80000305175781px) scale(0, 1) translate(-144.4600067138672px, -116.80000305175781px);
      }

      33.33% {
      -webkit-transform: translate(144.4600067138672px, 116.80000305175781px) scale(0, 1) translate(-144.4600067138672px, -116.80000305175781px);
      transform: translate(144.4600067138672px, 116.80000305175781px) scale(0, 1) translate(-144.4600067138672px, -116.80000305175781px);
      }

      43.89% {
      -webkit-transform: translate(144.4600067138672px, 116.80000305175781px) scale(1, 1) translate(-144.4600067138672px, -116.80000305175781px);
      transform: translate(144.4600067138672px, 116.80000305175781px) scale(1, 1) translate(-144.4600067138672px, -116.80000305175781px);
      }

      66.67% {
      -webkit-transform: translate(144.4600067138672px, 116.80000305175781px) scale(1, 1) translate(-144.4600067138672px, -116.80000305175781px);
      transform: translate(144.4600067138672px, 116.80000305175781px) scale(1, 1) translate(-144.4600067138672px, -116.80000305175781px);
      }

      68.33% {
      -webkit-transform: translate(144.4600067138672px, 116.80000305175781px) scale(0, 1) translate(-144.4600067138672px, -116.80000305175781px);
      transform: translate(144.4600067138672px, 116.80000305175781px) scale(0, 1) translate(-144.4600067138672px, -116.80000305175781px);
      }

      83.33% {
      -webkit-transform: translate(144.4600067138672px, 116.80000305175781px) scale(0, 1) translate(-144.4600067138672px, -116.80000305175781px);
      transform: translate(144.4600067138672px, 116.80000305175781px) scale(0, 1) translate(-144.4600067138672px, -116.80000305175781px);
      }

      95% {
      -webkit-transform: translate(144.4600067138672px, 116.80000305175781px) scale(1, 1) translate(-144.4600067138672px, -116.80000305175781px);
      transform: translate(144.4600067138672px, 116.80000305175781px) scale(1, 1) translate(-144.4600067138672px, -116.80000305175781px);
      }

      0% {
      -webkit-transform: translate(144.4600067138672px, 116.80000305175781px) scale(1, 1) translate(-144.4600067138672px, -116.80000305175781px);
      transform: translate(144.4600067138672px, 116.80000305175781px) scale(1, 1) translate(-144.4600067138672px, -116.80000305175781px);
      }

      100% {
      -webkit-transform: translate(144.4600067138672px, 116.80000305175781px) scale(1, 1) translate(-144.4600067138672px, -116.80000305175781px);
      transform: translate(144.4600067138672px, 116.80000305175781px) scale(1, 1) translate(-144.4600067138672px, -116.80000305175781px);
      }
      }

      @-webkit-keyframes el_tqT-ZInBTV_0TwQ18YaH_Animation {
      16.67% {
      -webkit-transform: translate(144.4600067138672px, 142.7899932861328px) scale(1, 1) translate(-144.4600067138672px, -142.7899932861328px);
      transform: translate(144.4600067138672px, 142.7899932861328px) scale(1, 1) translate(-144.4600067138672px, -142.7899932861328px);
      }

      18.33% {
      -webkit-transform: translate(144.4600067138672px, 142.7899932861328px) scale(0, 1) translate(-144.4600067138672px, -142.7899932861328px);
      transform: translate(144.4600067138672px, 142.7899932861328px) scale(0, 1) translate(-144.4600067138672px, -142.7899932861328px);
      }

      33.33% {
      -webkit-transform: translate(144.4600067138672px, 142.7899932861328px) scale(0, 1) translate(-144.4600067138672px, -142.7899932861328px);
      transform: translate(144.4600067138672px, 142.7899932861328px) scale(0, 1) translate(-144.4600067138672px, -142.7899932861328px);
      }

      50% {
      -webkit-transform: translate(144.4600067138672px, 142.7899932861328px) scale(1, 1) translate(-144.4600067138672px, -142.7899932861328px);
      transform: translate(144.4600067138672px, 142.7899932861328px) scale(1, 1) translate(-144.4600067138672px, -142.7899932861328px);
      }

      66.67% {
      -webkit-transform: translate(144.4600067138672px, 142.7899932861328px) scale(1, 1) translate(-144.4600067138672px, -142.7899932861328px);
      transform: translate(144.4600067138672px, 142.7899932861328px) scale(1, 1) translate(-144.4600067138672px, -142.7899932861328px);
      }

      68.33% {
      -webkit-transform: translate(144.4600067138672px, 142.7899932861328px) scale(0, 1) translate(-144.4600067138672px, -142.7899932861328px);
      transform: translate(144.4600067138672px, 142.7899932861328px) scale(0, 1) translate(-144.4600067138672px, -142.7899932861328px);
      }

      83.33% {
      -webkit-transform: translate(144.4600067138672px, 142.7899932861328px) scale(0, 1) translate(-144.4600067138672px, -142.7899932861328px);
      transform: translate(144.4600067138672px, 142.7899932861328px) scale(0, 1) translate(-144.4600067138672px, -142.7899932861328px);
      }

      100% {
      -webkit-transform: translate(144.4600067138672px, 142.7899932861328px) scale(1, 1) translate(-144.4600067138672px, -142.7899932861328px);
      transform: translate(144.4600067138672px, 142.7899932861328px) scale(1, 1) translate(-144.4600067138672px, -142.7899932861328px);
      }

      0% {
      -webkit-transform: translate(144.4600067138672px, 142.7899932861328px) scale(1, 1) translate(-144.4600067138672px, -142.7899932861328px);
      transform: translate(144.4600067138672px, 142.7899932861328px) scale(1, 1) translate(-144.4600067138672px, -142.7899932861328px);
      }
      }

      @keyframes el_tqT-ZInBTV_0TwQ18YaH_Animation {
      16.67% {
      -webkit-transform: translate(144.4600067138672px, 142.7899932861328px) scale(1, 1) translate(-144.4600067138672px, -142.7899932861328px);
      transform: translate(144.4600067138672px, 142.7899932861328px) scale(1, 1) translate(-144.4600067138672px, -142.7899932861328px);
      }

      18.33% {
      -webkit-transform: translate(144.4600067138672px, 142.7899932861328px) scale(0, 1) translate(-144.4600067138672px, -142.7899932861328px);
      transform: translate(144.4600067138672px, 142.7899932861328px) scale(0, 1) translate(-144.4600067138672px, -142.7899932861328px);
      }

      33.33% {
      -webkit-transform: translate(144.4600067138672px, 142.7899932861328px) scale(0, 1) translate(-144.4600067138672px, -142.7899932861328px);
      transform: translate(144.4600067138672px, 142.7899932861328px) scale(0, 1) translate(-144.4600067138672px, -142.7899932861328px);
      }

      50% {
      -webkit-transform: translate(144.4600067138672px, 142.7899932861328px) scale(1, 1) translate(-144.4600067138672px, -142.7899932861328px);
      transform: translate(144.4600067138672px, 142.7899932861328px) scale(1, 1) translate(-144.4600067138672px, -142.7899932861328px);
      }

      66.67% {
      -webkit-transform: translate(144.4600067138672px, 142.7899932861328px) scale(1, 1) translate(-144.4600067138672px, -142.7899932861328px);
      transform: translate(144.4600067138672px, 142.7899932861328px) scale(1, 1) translate(-144.4600067138672px, -142.7899932861328px);
      }

      68.33% {
      -webkit-transform: translate(144.4600067138672px, 142.7899932861328px) scale(0, 1) translate(-144.4600067138672px, -142.7899932861328px);
      transform: translate(144.4600067138672px, 142.7899932861328px) scale(0, 1) translate(-144.4600067138672px, -142.7899932861328px);
      }

      83.33% {
      -webkit-transform: translate(144.4600067138672px, 142.7899932861328px) scale(0, 1) translate(-144.4600067138672px, -142.7899932861328px);
      transform: translate(144.4600067138672px, 142.7899932861328px) scale(0, 1) translate(-144.4600067138672px, -142.7899932861328px);
      }

      100% {
      -webkit-transform: translate(144.4600067138672px, 142.7899932861328px) scale(1, 1) translate(-144.4600067138672px, -142.7899932861328px);
      transform: translate(144.4600067138672px, 142.7899932861328px) scale(1, 1) translate(-144.4600067138672px, -142.7899932861328px);
      }

      0% {
      -webkit-transform: translate(144.4600067138672px, 142.7899932861328px) scale(1, 1) translate(-144.4600067138672px, -142.7899932861328px);
      transform: translate(144.4600067138672px, 142.7899932861328px) scale(1, 1) translate(-144.4600067138672px, -142.7899932861328px);
      }
      }

      @-webkit-keyframes el_o82oP_Zzxu_Animation {
      15% {
      opacity: 1;
      }

      16.67% {
      opacity: 0;
      }

      18.33% {
      opacity: 0;
      }

      20% {
      opacity: 1;
      }

      65% {
      opacity: 1;
      }

      66.67% {
      opacity: 0;
      }

      68.33% {
      opacity: 0;
      }

      70% {
      opacity: 1;
      }

      0% {
      opacity: 1;
      }

      100% {
      opacity: 1;
      }
      }

      @keyframes el_o82oP_Zzxu_Animation {
      15% {
      opacity: 1;
      }

      16.67% {
      opacity: 0;
      }

      18.33% {
      opacity: 0;
      }

      20% {
      opacity: 1;
      }

      65% {
      opacity: 1;
      }

      66.67% {
      opacity: 0;
      }

      68.33% {
      opacity: 0;
      }

      70% {
      opacity: 1;
      }

      0% {
      opacity: 1;
      }

      100% {
      opacity: 1;
      }
      }

      @-webkit-keyframes el_SEAWr5aA7w_PkOl_1E1X_Animation {
      0% {
      -webkit-transform: translate(95.42000198364258px, 114.3141860961914px) scale(1, 1) translate(-95.42000198364258px, -114.3141860961914px);
      transform: translate(95.42000198364258px, 114.3141860961914px) scale(1, 1) translate(-95.42000198364258px, -114.3141860961914px);
      }

      16.67% {
      -webkit-transform: translate(95.42000198364258px, 114.3141860961914px) scale(1, 1) translate(-95.42000198364258px, -114.3141860961914px);
      transform: translate(95.42000198364258px, 114.3141860961914px) scale(1, 1) translate(-95.42000198364258px, -114.3141860961914px);
      }

      18.33% {
      -webkit-transform: translate(95.42000198364258px, 114.3141860961914px) scale(0, 0) translate(-95.42000198364258px, -114.3141860961914px);
      transform: translate(95.42000198364258px, 114.3141860961914px) scale(0, 0) translate(-95.42000198364258px, -114.3141860961914px);
      }

      66.67% {
      -webkit-transform: translate(95.42000198364258px, 114.3141860961914px) scale(1, 1) translate(-95.42000198364258px, -114.3141860961914px);
      transform: translate(95.42000198364258px, 114.3141860961914px) scale(1, 1) translate(-95.42000198364258px, -114.3141860961914px);
      }

      68.33% {
      -webkit-transform: translate(95.42000198364258px, 114.3141860961914px) scale(0, 0) translate(-95.42000198364258px, -114.3141860961914px);
      transform: translate(95.42000198364258px, 114.3141860961914px) scale(0, 0) translate(-95.42000198364258px, -114.3141860961914px);
      }

      83.33% {
      -webkit-transform: translate(95.42000198364258px, 114.3141860961914px) scale(1, 1) translate(-95.42000198364258px, -114.3141860961914px);
      transform: translate(95.42000198364258px, 114.3141860961914px) scale(1, 1) translate(-95.42000198364258px, -114.3141860961914px);
      }

      100% {
      -webkit-transform: translate(95.42000198364258px, 114.3141860961914px) scale(1, 1) translate(-95.42000198364258px, -114.3141860961914px);
      transform: translate(95.42000198364258px, 114.3141860961914px) scale(1, 1) translate(-95.42000198364258px, -114.3141860961914px);
      }
      }

      @keyframes el_SEAWr5aA7w_PkOl_1E1X_Animation {
      0% {
      -webkit-transform: translate(95.42000198364258px, 114.3141860961914px) scale(1, 1) translate(-95.42000198364258px, -114.3141860961914px);
      transform: translate(95.42000198364258px, 114.3141860961914px) scale(1, 1) translate(-95.42000198364258px, -114.3141860961914px);
      }

      16.67% {
      -webkit-transform: translate(95.42000198364258px, 114.3141860961914px) scale(1, 1) translate(-95.42000198364258px, -114.3141860961914px);
      transform: translate(95.42000198364258px, 114.3141860961914px) scale(1, 1) translate(-95.42000198364258px, -114.3141860961914px);
      }

      18.33% {
      -webkit-transform: translate(95.42000198364258px, 114.3141860961914px) scale(0, 0) translate(-95.42000198364258px, -114.3141860961914px);
      transform: translate(95.42000198364258px, 114.3141860961914px) scale(0, 0) translate(-95.42000198364258px, -114.3141860961914px);
      }

      66.67% {
      -webkit-transform: translate(95.42000198364258px, 114.3141860961914px) scale(1, 1) translate(-95.42000198364258px, -114.3141860961914px);
      transform: translate(95.42000198364258px, 114.3141860961914px) scale(1, 1) translate(-95.42000198364258px, -114.3141860961914px);
      }

      68.33% {
      -webkit-transform: translate(95.42000198364258px, 114.3141860961914px) scale(0, 0) translate(-95.42000198364258px, -114.3141860961914px);
      transform: translate(95.42000198364258px, 114.3141860961914px) scale(0, 0) translate(-95.42000198364258px, -114.3141860961914px);
      }

      83.33% {
      -webkit-transform: translate(95.42000198364258px, 114.3141860961914px) scale(1, 1) translate(-95.42000198364258px, -114.3141860961914px);
      transform: translate(95.42000198364258px, 114.3141860961914px) scale(1, 1) translate(-95.42000198364258px, -114.3141860961914px);
      }

      100% {
      -webkit-transform: translate(95.42000198364258px, 114.3141860961914px) scale(1, 1) translate(-95.42000198364258px, -114.3141860961914px);
      transform: translate(95.42000198364258px, 114.3141860961914px) scale(1, 1) translate(-95.42000198364258px, -114.3141860961914px);
      }
      }

      @-webkit-keyframes el_9EImEnyK-u_f2FVQyhEB_Animation {
      16.67% {
      -webkit-transform: translate(95.42000198364258px, 114.06267547607422px) scale(0, 0) translate(-95.42000198364258px, -114.06267547607422px);
      transform: translate(95.42000198364258px, 114.06267547607422px) scale(0, 0) translate(-95.42000198364258px, -114.06267547607422px);
      }

      33.33% {
      -webkit-transform: translate(95.42000198364258px, 114.06267547607422px) scale(1, 1) translate(-95.42000198364258px, -114.06267547607422px);
      transform: translate(95.42000198364258px, 114.06267547607422px) scale(1, 1) translate(-95.42000198364258px, -114.06267547607422px);
      }

      0% {
      -webkit-transform: translate(95.42000198364258px, 114.06267547607422px) scale(0, 0) translate(-95.42000198364258px, -114.06267547607422px);
      transform: translate(95.42000198364258px, 114.06267547607422px) scale(0, 0) translate(-95.42000198364258px, -114.06267547607422px);
      }

      100% {
      -webkit-transform: translate(95.42000198364258px, 114.06267547607422px) scale(1, 1) translate(-95.42000198364258px, -114.06267547607422px);
      transform: translate(95.42000198364258px, 114.06267547607422px) scale(1, 1) translate(-95.42000198364258px, -114.06267547607422px);
      }
      }

      @keyframes el_9EImEnyK-u_f2FVQyhEB_Animation {
      16.67% {
      -webkit-transform: translate(95.42000198364258px, 114.06267547607422px) scale(0, 0) translate(-95.42000198364258px, -114.06267547607422px);
      transform: translate(95.42000198364258px, 114.06267547607422px) scale(0, 0) translate(-95.42000198364258px, -114.06267547607422px);
      }

      33.33% {
      -webkit-transform: translate(95.42000198364258px, 114.06267547607422px) scale(1, 1) translate(-95.42000198364258px, -114.06267547607422px);
      transform: translate(95.42000198364258px, 114.06267547607422px) scale(1, 1) translate(-95.42000198364258px, -114.06267547607422px);
      }

      0% {
      -webkit-transform: translate(95.42000198364258px, 114.06267547607422px) scale(0, 0) translate(-95.42000198364258px, -114.06267547607422px);
      transform: translate(95.42000198364258px, 114.06267547607422px) scale(0, 0) translate(-95.42000198364258px, -114.06267547607422px);
      }

      100% {
      -webkit-transform: translate(95.42000198364258px, 114.06267547607422px) scale(1, 1) translate(-95.42000198364258px, -114.06267547607422px);
      transform: translate(95.42000198364258px, 114.06267547607422px) scale(1, 1) translate(-95.42000198364258px, -114.06267547607422px);
      }
      }

      @-webkit-keyframes el_SEAWr5aA7w_Animation {
      0% {
      opacity: 1;
      }

      16.67% {
      opacity: 1;
      }

      18.33% {
      opacity: 0;
      }

      66.67% {
      opacity: 0;
      }

      68.33% {
      opacity: 1;
      }

      100% {
      opacity: 1;
      }
      }

      @keyframes el_SEAWr5aA7w_Animation {
      0% {
      opacity: 1;
      }

      16.67% {
      opacity: 1;
      }

      18.33% {
      opacity: 0;
      }

      66.67% {
      opacity: 0;
      }

      68.33% {
      opacity: 1;
      }

      100% {
      opacity: 1;
      }
      }

      @-webkit-keyframes el_9EImEnyK-u_Animation {
      0% {
      opacity: 0;
      }

      16.67% {
      opacity: 0;
      }

      18.33% {
      opacity: 1;
      }

      66.67% {
      opacity: 1;
      }

      68.33% {
      opacity: 0;
      }

      100% {
      opacity: 0;
      }
      }

      @keyframes el_9EImEnyK-u_Animation {
      0% {
      opacity: 0;
      }

      16.67% {
      opacity: 0;
      }

      18.33% {
      opacity: 1;
      }

      66.67% {
      opacity: 1;
      }

      68.33% {
      opacity: 0;
      }

      100% {
      opacity: 0;
      }
      }

      @-webkit-keyframes el_o82oP_Zzxu_2g7q-rXK6_Animation {
      0% {
      -webkit-transform: translate(136.68000411987305px, 114.06747436523438px) scale(1, 1) translate(-136.68000411987305px, -114.06747436523438px);
      transform: translate(136.68000411987305px, 114.06747436523438px) scale(1, 1) translate(-136.68000411987305px, -114.06747436523438px);
      }

      16.67% {
      -webkit-transform: translate(136.68000411987305px, 114.06747436523438px) scale(0, 0) translate(-136.68000411987305px, -114.06747436523438px);
      transform: translate(136.68000411987305px, 114.06747436523438px) scale(0, 0) translate(-136.68000411987305px, -114.06747436523438px);
      }

      18.33% {
      -webkit-transform: translate(136.68000411987305px, 114.06747436523438px) scale(1, 1) translate(-136.68000411987305px, -114.06747436523438px);
      transform: translate(136.68000411987305px, 114.06747436523438px) scale(1, 1) translate(-136.68000411987305px, -114.06747436523438px);
      }

      50% {
      -webkit-transform: translate(136.68000411987305px, 114.06747436523438px) scale(1, 1) translate(-136.68000411987305px, -114.06747436523438px);
      transform: translate(136.68000411987305px, 114.06747436523438px) scale(1, 1) translate(-136.68000411987305px, -114.06747436523438px);
      }

      66.67% {
      -webkit-transform: translate(136.68000411987305px, 114.06747436523438px) scale(0, 0) translate(-136.68000411987305px, -114.06747436523438px);
      transform: translate(136.68000411987305px, 114.06747436523438px) scale(0, 0) translate(-136.68000411987305px, -114.06747436523438px);
      }

      68.33% {
      -webkit-transform: translate(136.68000411987305px, 114.06747436523438px) scale(1, 1) translate(-136.68000411987305px, -114.06747436523438px);
      transform: translate(136.68000411987305px, 114.06747436523438px) scale(1, 1) translate(-136.68000411987305px, -114.06747436523438px);
      }

      100% {
      -webkit-transform: translate(136.68000411987305px, 114.06747436523438px) scale(1, 1) translate(-136.68000411987305px, -114.06747436523438px);
      transform: translate(136.68000411987305px, 114.06747436523438px) scale(1, 1) translate(-136.68000411987305px, -114.06747436523438px);
      }
      }

      @keyframes el_o82oP_Zzxu_2g7q-rXK6_Animation {
      0% {
      -webkit-transform: translate(136.68000411987305px, 114.06747436523438px) scale(1, 1) translate(-136.68000411987305px, -114.06747436523438px);
      transform: translate(136.68000411987305px, 114.06747436523438px) scale(1, 1) translate(-136.68000411987305px, -114.06747436523438px);
      }

      16.67% {
      -webkit-transform: translate(136.68000411987305px, 114.06747436523438px) scale(0, 0) translate(-136.68000411987305px, -114.06747436523438px);
      transform: translate(136.68000411987305px, 114.06747436523438px) scale(0, 0) translate(-136.68000411987305px, -114.06747436523438px);
      }

      18.33% {
      -webkit-transform: translate(136.68000411987305px, 114.06747436523438px) scale(1, 1) translate(-136.68000411987305px, -114.06747436523438px);
      transform: translate(136.68000411987305px, 114.06747436523438px) scale(1, 1) translate(-136.68000411987305px, -114.06747436523438px);
      }

      50% {
      -webkit-transform: translate(136.68000411987305px, 114.06747436523438px) scale(1, 1) translate(-136.68000411987305px, -114.06747436523438px);
      transform: translate(136.68000411987305px, 114.06747436523438px) scale(1, 1) translate(-136.68000411987305px, -114.06747436523438px);
      }

      66.67% {
      -webkit-transform: translate(136.68000411987305px, 114.06747436523438px) scale(0, 0) translate(-136.68000411987305px, -114.06747436523438px);
      transform: translate(136.68000411987305px, 114.06747436523438px) scale(0, 0) translate(-136.68000411987305px, -114.06747436523438px);
      }

      68.33% {
      -webkit-transform: translate(136.68000411987305px, 114.06747436523438px) scale(1, 1) translate(-136.68000411987305px, -114.06747436523438px);
      transform: translate(136.68000411987305px, 114.06747436523438px) scale(1, 1) translate(-136.68000411987305px, -114.06747436523438px);
      }

      100% {
      -webkit-transform: translate(136.68000411987305px, 114.06747436523438px) scale(1, 1) translate(-136.68000411987305px, -114.06747436523438px);
      transform: translate(136.68000411987305px, 114.06747436523438px) scale(1, 1) translate(-136.68000411987305px, -114.06747436523438px);
      }
      }

      #el_WwxlgbKKd * {
      -webkit-animation-duration: 6s;
      animation-duration: 6s;
      -webkit-animation-iteration-count: infinite;
      animation-iteration-count: infinite;
      -webkit-animation-play-state: running;
      animation-play-state: running;
      -webkit-animation-timing-function: cubic-bezier(0, 0, 1, 1);
      animation-timing-function: cubic-bezier(0, 0, 1, 1);
      }

      #el_jSrQNRIvQk {
      fill: #2f4050;
      }

      #el_B8Z9H4Nve0 {
      fill: #ffb81a;
      }

      #el_TlibNm5Rt4 {
      fill: #2f4050;
      }

      #el_-e_eWMz7vf {
      fill: #2f4050;
      }

      #el_LhPdIVDp7c {
      fill: #2f4050;
      }

      #el_LT8YLok75R {
      fill: #2f4050;
      }

      #el_zDP2BWOgyX {
      fill: #ffb81a;
      stroke: #2f4050;
      stroke-miterlimit: 10;
      stroke-width: 13px;
      }

      #el_DI7HfEkfEG {
      fill: #ffb81a;
      stroke: #2f4050;
      stroke-miterlimit: 10;
      stroke-width: 13px;
      }

      #el_tqT-ZInBTV {
      fill: #ffb81a;
      stroke: #2f4050;
      stroke-miterlimit: 10;
      stroke-width: 13px;
      }

      #el_JGjfQ8RoK9N {
      stroke: #2f4050;
      fill: none;
      stroke-width: 16px;
      }

      #el_o82oP_Zzxu_2g7q-rXK6 {
      -webkit-transform: translate(59.36000442504883px, 71px) scale(1, 1) translate(-59.36000442504883px, -71px);
      transform: translate(59.36000442504883px, 71px) scale(1, 1) translate(-59.36000442504883px, -71px);
      }

      #el_WwxlgbKKd:hover #el_o82oP_Zzxu_2g7q-rXK6 {
      -webkit-animation-name: el_o82oP_Zzxu_2g7q-rXK6_Animation;
      animation-name: el_o82oP_Zzxu_2g7q-rXK6_Animation;
      }

      #el_9EImEnyK-u {
      fill: rgb(0, 0, 0);
      opacity: 0;
      }

      #el_WwxlgbKKd:hover #el_9EImEnyK-u {
      -webkit-animation-name: el_9EImEnyK-u_Animation;
      animation-name: el_9EImEnyK-u_Animation;
      }

      #el_WwxlgbKKd:hover #el_SEAWr5aA7w {
      -webkit-animation-name: el_SEAWr5aA7w_Animation;
      animation-name: el_SEAWr5aA7w_Animation;
      }

      #el_WwxlgbKKd:hover #el_9EImEnyK-u_f2FVQyhEB {
      -webkit-animation-name: el_9EImEnyK-u_f2FVQyhEB_Animation;
      animation-name: el_9EImEnyK-u_f2FVQyhEB_Animation;
      }

      #el_WwxlgbKKd:hover #el_SEAWr5aA7w_PkOl_1E1X {
      -webkit-animation-name: el_SEAWr5aA7w_PkOl_1E1X_Animation;
      animation-name: el_SEAWr5aA7w_PkOl_1E1X_Animation;
      }

      #el_WwxlgbKKd:hover #el_o82oP_Zzxu {
      -webkit-animation-name: el_o82oP_Zzxu_Animation;
      animation-name: el_o82oP_Zzxu_Animation;
      }

      #el_WwxlgbKKd:hover #el_tqT-ZInBTV_0TwQ18YaH {
      -webkit-animation-name: el_tqT-ZInBTV_0TwQ18YaH_Animation;
      animation-name: el_tqT-ZInBTV_0TwQ18YaH_Animation;
      }

      #el_WwxlgbKKd:hover #el_DI7HfEkfEG_hxjvQ4sC4 {
      -webkit-animation-name: el_DI7HfEkfEG_hxjvQ4sC4_Animation;
      animation-name: el_DI7HfEkfEG_hxjvQ4sC4_Animation;
      }

      #el_WwxlgbKKd:hover #el_zDP2BWOgyX_XrXrmBjC6 {
      -webkit-animation-name: el_zDP2BWOgyX_XrXrmBjC6_Animation;
      animation-name: el_zDP2BWOgyX_XrXrmBjC6_Animation;
      }

    </style>
    <defs></defs>
    <g id="el_HfwRgDRZ1H">
      <path id="el_jSrQNRIvQk" data-name="roznica-optica-svg Shape-3" d="M184.39,228.15c-7,0-12.61-6-12.61-13.18v-6.65H98.61V215c0,7.3-5.73,13.18-12.61,13.18H53.17v13.18H217V228.15Z"></path>
      <path d="M226.18,33.76H44.62A16.17,16.17,0,0,0,28.47,50.22V175.63c0,10.79,8,19.83,18.45,19.83H223.36c10.32,0,19-8.83,19-19.83V50.22A16.44,16.44,0,0,0,226.18,33.76Zm-.1,137a6.15,6.15,0,0,1-5.94,6.21H49.83a6.15,6.15,0,0,1-5.94-6.21V57.08a6.15,6.15,0,0,1,5.94-6.21h170.3a6.15,6.15,0,0,1,5.94,6.21Z" id="el_B8Z9H4Nve0"></path>
    </g>
    <g id="el_o82oP_Zzxu_2g7q-rXK6" data-animator-group="true" data-animator-type="2">
      <g id="el_o82oP_Zzxu">
        <g id="el_En9irQ6nu6">
          <g id="el_SEAWr5aA7w_PkOl_1E1X" data-animator-group="true" data-animator-type="2">
            <g id="el_SEAWr5aA7w">
              <path d="M79.7,108.81c.81,5.21,4.84,11.86,11.46,14.21a12.49,12.49,0,0,0,8.39,0c6.5-2.34,10.65-9,11.46-14.17.89-.08,2-1.29,3.27-5.69,1.69-6-.12-6.9-1.62-6.74a14.59,14.59,0,0,0,.65-2.46c2.58-15.54-5.08-16.1-5.08-16.1a11.73,11.73,0,0,0-4.6-4.28,15.74,15.74,0,0,0-9.48-2,14.06,14.06,0,0,0-3.75.72,16.86,16.86,0,0,0-4.16,2.14A23.6,23.6,0,0,0,82,77.93,17.12,17.12,0,0,0,77.36,86a17.47,17.47,0,0,0,0,7.91A18.24,18.24,0,0,0,78,96.42c-1.54-.16-3.35.73-1.62,6.74C77.68,107.51,78.85,108.76,79.7,108.81Z" id="el_TlibNm5Rt4"></path>
              <path d="M123.16,130a81.65,81.65,0,0,1-14.57-7.06l-7,22.2-1,3-3.15-8.92c7.22-10.09-.56-10.57-1.9-10.57H95.4c-1.33,0-9.12.48-1.9,10.57l-3.15,8.92-1-3-7-22.2A85.24,85.24,0,0,1,67.8,130c-4.92,1.78-7.26,4.68-8.44,9a39.6,39.6,0,0,0,4.36,5,44.82,44.82,0,0,0,63.4,0,45.91,45.91,0,0,0,4.36-5C130.43,134.63,128,131.73,123.16,130Z" id="el_-e_eWMz7vf"></path>
            </g>
          </g>
          <g id="el_9EImEnyK-u_f2FVQyhEB" data-animator-group="true" data-animator-type="2">
            <g id="el_9EImEnyK-u">
              <path d="M77.94,120a8.62,8.62,0,0,0,2.38.68c3.36.5,7.47-.27,12-.54a10.47,10.47,0,0,0,5.16,0c11.67.57,20.77,3.87,20.77-19.88C118.2,84.13,107.74,71,94.82,71S71.43,84.13,71.43,100.25C71.43,113.22,73.91,118.26,77.94,120ZM75.71,92a2.13,2.13,0,0,1,.88-.87c2.45,3.39,8.79,2.67,14.27-1.63a20.89,20.89,0,0,0,3.58-3.6,3.24,3.24,0,0,1,.38.06,12.64,12.64,0,0,1,1.58.46,14.55,14.55,0,0,1,1.5.73,13.87,13.87,0,0,1,1.37.9,12.59,12.59,0,0,1,1.17,1.06,8.24,8.24,0,0,1,.93,1.09,10.71,10.71,0,0,1,.7,1,4.79,4.79,0,0,1,.47.85l.38.77s0-.3-.14-.85a7.78,7.78,0,0,0-.21-1,8.13,8.13,0,0,0-.39-1.26,12.19,12.19,0,0,0-.63-1.44,16.16,16.16,0,0,0-.92-1.53,17.65,17.65,0,0,0-1.23-1.5,11.77,11.77,0,0,0-3.06-2.42,12.26,12.26,0,0,0,.58-1.47,18.55,18.55,0,0,1,5.56,3.6c5.86,5.86,3.55,12.66,8.07,8.16a12.14,12.14,0,0,0,1.75-2.2,2,2,0,0,1,.44,0A2.07,2.07,0,0,1,114,92a5.36,5.36,0,0,1,.38,4c-.57,2.21-2,3.52-3.19,3.66l-.95.13-.25.93c-2.24,8.13-8.29,16.79-15.13,16.79S82,108.86,79.72,100.73l-.25-.93-.95-.13c-1.15-.16-2.62-1.47-3.19-3.66A5.37,5.37,0,0,1,75.71,92Z" id="el_LhPdIVDp7c"></path>
              <path d="M123.16,130l-1.41-.55a91.31,91.31,0,0,1-10.42-4.9,17,17,0,0,1-4.84,5c-.29.18-.58.36-.88.51-1.91,1-3.66,1.89-5.15,2.69-3,1.62-5,2.71-5,2.71l0,0v0l0,0,0,0v0l0,0s-1.89-1.05-4.86-2.63C89,132,87.19,131.05,85.22,130c-.25-.13-.49-.28-.73-.43a16.8,16.8,0,0,1-5-5.07A96,96,0,0,1,69,129.47L67.8,130c-4.92,1.78-7.26,4.68-8.44,9a39.6,39.6,0,0,0,4.36,5l.12.11a44.82,44.82,0,0,0,63.16,0l.12-.11a45.91,45.91,0,0,0,4.36-5C130.43,134.63,128,131.73,123.16,130Z" id="el_LT8YLok75R"></path>
            </g>
          </g>
        </g>
        <g id="el_5rya-rUpE4">
          <g id="el_zDP2BWOgyX_XrXrmBjC6" data-animator-group="true" data-animator-type="2">
            <line x1="144.46" y1="88.4" x2="214" y2="88.4" id="el_zDP2BWOgyX"></line>
          </g>
          <g id="el_DI7HfEkfEG_hxjvQ4sC4" data-animator-group="true" data-animator-type="2">
            <line x1="144.46" y1="116.8" x2="214" y2="116.8" id="el_DI7HfEkfEG"></line>
          </g>
          <g id="el_tqT-ZInBTV_0TwQ18YaH" data-animator-group="true" data-animator-type="2">
            <line x1="144.46" y1="142.79" x2="214" y2="142.79" id="el_tqT-ZInBTV"></line>
          </g>
        </g>
      </g>
    </g>
    <path id="el_JGjfQ8RoK9N" d="M250,262H20A12,12,0,0,1,8,250V20A12,12,0,0,1,20,8H250a12,12,0,0,1,12,12V250A12,12,0,0,1,250,262Z"></path>
  </svg>
</picture>`}