export function bp20(width: number, height: number) {
	return `<picture uniq="bookkeeping_bp20">
  <svg version="1.1" id="el_SzZTLGoAa" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 268 270" style="enable-background:new 0 0 268 270;" xml:space="preserve" height="${height}" width="${width}"><style>@-webkit-keyframes el_yzSk2PceCC_h7cjtlTtZ_Animation{0%{-webkit-transform: translate(135.8000030517578px, 141.1999969482422px) translate(-135.8000030517578px, -141.1999969482422px) translate(0px, 0px);transform: translate(135.8000030517578px, 141.1999969482422px) translate(-135.8000030517578px, -141.1999969482422px) translate(0px, 0px);}50%{-webkit-transform: translate(135.8000030517578px, 141.1999969482422px) translate(-135.8000030517578px, -141.1999969482422px) translate(0px, 0px);transform: translate(135.8000030517578px, 141.1999969482422px) translate(-135.8000030517578px, -141.1999969482422px) translate(0px, 0px);}100%{-webkit-transform: translate(135.8000030517578px, 141.1999969482422px) translate(-135.8000030517578px, -141.1999969482422px) translate(-95px, 0px);transform: translate(135.8000030517578px, 141.1999969482422px) translate(-135.8000030517578px, -141.1999969482422px) translate(-95px, 0px);}}@keyframes el_yzSk2PceCC_h7cjtlTtZ_Animation{0%{-webkit-transform: translate(135.8000030517578px, 141.1999969482422px) translate(-135.8000030517578px, -141.1999969482422px) translate(0px, 0px);transform: translate(135.8000030517578px, 141.1999969482422px) translate(-135.8000030517578px, -141.1999969482422px) translate(0px, 0px);}50%{-webkit-transform: translate(135.8000030517578px, 141.1999969482422px) translate(-135.8000030517578px, -141.1999969482422px) translate(0px, 0px);transform: translate(135.8000030517578px, 141.1999969482422px) translate(-135.8000030517578px, -141.1999969482422px) translate(0px, 0px);}100%{-webkit-transform: translate(135.8000030517578px, 141.1999969482422px) translate(-135.8000030517578px, -141.1999969482422px) translate(-95px, 0px);transform: translate(135.8000030517578px, 141.1999969482422px) translate(-135.8000030517578px, -141.1999969482422px) translate(-95px, 0px);}}@-webkit-keyframes el_yzSk2PceCC_ik112pR-R_Animation{0%{-webkit-transform: translate(135.8000030517578px, 141.1999969482422px) scale(1, 1) translate(-135.8000030517578px, -141.1999969482422px);transform: translate(135.8000030517578px, 141.1999969482422px) scale(1, 1) translate(-135.8000030517578px, -141.1999969482422px);}50%{-webkit-transform: translate(135.8000030517578px, 141.1999969482422px) scale(15, 1) translate(-135.8000030517578px, -141.1999969482422px);transform: translate(135.8000030517578px, 141.1999969482422px) scale(15, 1) translate(-135.8000030517578px, -141.1999969482422px);}100%{-webkit-transform: translate(135.8000030517578px, 141.1999969482422px) scale(0.7, 1) translate(-135.8000030517578px, -141.1999969482422px);transform: translate(135.8000030517578px, 141.1999969482422px) scale(0.7, 1) translate(-135.8000030517578px, -141.1999969482422px);}}@keyframes el_yzSk2PceCC_ik112pR-R_Animation{0%{-webkit-transform: translate(135.8000030517578px, 141.1999969482422px) scale(1, 1) translate(-135.8000030517578px, -141.1999969482422px);transform: translate(135.8000030517578px, 141.1999969482422px) scale(1, 1) translate(-135.8000030517578px, -141.1999969482422px);}50%{-webkit-transform: translate(135.8000030517578px, 141.1999969482422px) scale(15, 1) translate(-135.8000030517578px, -141.1999969482422px);transform: translate(135.8000030517578px, 141.1999969482422px) scale(15, 1) translate(-135.8000030517578px, -141.1999969482422px);}100%{-webkit-transform: translate(135.8000030517578px, 141.1999969482422px) scale(0.7, 1) translate(-135.8000030517578px, -141.1999969482422px);transform: translate(135.8000030517578px, 141.1999969482422px) scale(0.7, 1) translate(-135.8000030517578px, -141.1999969482422px);}}#el_SzZTLGoAa *{-webkit-animation-duration: 3s;animation-duration: 3s;-webkit-animation-iteration-count: infinite;animation-iteration-count: infinite;-webkit-animation-play-state: running;animation-play-state: running;-webkit-animation-timing-function: cubic-bezier(0, 0, 1, 1);animation-timing-function: cubic-bezier(0, 0, 1, 1);}#el_2crAUimmhu{fill: none;stroke: #2F4050;stroke-width: 15;}#el_yzSk2PceCC{fill: #FFB81A;}#el_kjRA8MVADg{fill: #2F4050;}#el_IL4daE4L5u{fill: #2F4050;}#el_gcxt9ybwpA{fill: #FFFFFF;}#el_2uFrqH6BNK{fill: #2F4050;}#el_Dl1_yN_O_A{fill: #2F4050;}#el_RK6VcbF9Ak{fill: #2F4050;}#el_GkZB78ND8v{fill: #2F4050;}#el_M68xNwWkdk{fill: #2F4050;}#el_t6P250TK2b{fill: #2F4050;}#el_IILpIL4yKl{fill: #2F4050;}#el_MLfhQ-SkSn{fill: #2F4050;}#el_gccEr9tuenQ{fill: #2F4050;}#el_0-0AolZpmsj{fill: none;stroke: #2F4050;}#el_qSFptQkJ1WR{fill: #2F4050;stroke: #2F4050;}#el_omiwDAVI5bt{fill: #2F4050;stroke: #2F4050;}#el_SU21KP7TWP4{fill: #2F4050;}#el_z9xLrXlH9F0{fill: #2F4050;stroke: #2F4050;}#el_DVacSQ1hI3T{fill: #2F4050;stroke: #2F4050;}#el_V1w77osMJEr{fill: #2F4050;stroke: #2F4050;}#el___VmsSkceZC{fill: #2F4050;stroke: #2F4050;}#el_-ELpP9lOiiA{fill: #2F4050;stroke: #2F4050;}#el_8GX9DpJSRnN{fill: #2F4050;stroke: #2F4050;}#el_YjURJLpFbrO{fill: #2F4050;}#el_9fedw1-uGM2{fill: none;stroke: #FFFFFF;}#el_YXEMbeRnLIH{fill: #2F4050;}#el_XYcztSnJlvL{fill: none;stroke: #FFFFFF;}#el_usAGVUnuFXt{fill: #2F4050;}#el_8aZysptywQd{fill: none;stroke: #FFFFFF;}#el_nRjrBykZbwg{fill: #2F4050;stroke: #2F4050;}#el_1qsM4hYvbJf{fill: #2F4050;}#el_DXb4_RMjJuj{fill: none;stroke: #FFFFFF;}#el_KDHTPN1RZTu{fill: #2F4050;}#el_Uj-K1nVvsi4{fill: none;stroke: #FFFFFF;}#el_AWKiuIEirUc{fill: #2F4050;}#el_1qp9vCD2efx{fill: none;stroke: #FFFFFF;}#el_b4M7GYc6e9p{fill: #2F4050;}#el_kWCeuIAQnA4{fill: #2F4050;}#el_3G6XCuPrv2Q{fill: none;}#el_zIQCnhZdELa{fill: none;}#el_QrHvgBbABE8{fill: none;}#el_jGzOv9cjZhF{fill: none;}#el_yzSk2PceCC_ik112pR-R{-webkit-transform: translate(135.8000030517578px, 141.1999969482422px) scale(1, 1) translate(-135.8000030517578px, -141.1999969482422px);transform: translate(135.8000030517578px, 141.1999969482422px) scale(1, 1) translate(-135.8000030517578px, -141.1999969482422px);}#el_SzZTLGoAa:hover #el_yzSk2PceCC_ik112pR-R{-webkit-animation-name: el_yzSk2PceCC_ik112pR-R_Animation;animation-name: el_yzSk2PceCC_ik112pR-R_Animation;}#el_SzZTLGoAa:hover #el_yzSk2PceCC_h7cjtlTtZ{-webkit-animation-name: el_yzSk2PceCC_h7cjtlTtZ_Animation;animation-name: el_yzSk2PceCC_h7cjtlTtZ_Animation;}</style>

<path id="el_2crAUimmhu" d="M248.2,262H19.8c-6.7,0-12.2-5.5-12.2-12.3V20.3C7.5,13.5,13,8,19.8,8h228.5
	c6.7,0,12.2,5.5,12.2,12.3v229.4C260.5,256.5,255,262,248.2,262z"></path>
<g id="el_m4oDAJTntA">
	<g id="el_yzSk2PceCC_h7cjtlTtZ" data-animator-group="true" data-animator-type="0"><g id="el_yzSk2PceCC_ik112pR-R" data-animator-group="true" data-animator-type="2"><rect id="el_yzSk2PceCC" x="129.3" y="47.1" width="6.5" height="188.2"></rect></g></g>
	<path d="M36.5,38.9v206h45.9v-206H36.5z M59.4,231.6c-7.7,0-13.9-6.2-13.9-13.9s6.2-13.9,13.9-13.9
		c7.7,0,13.9,6.2,13.9,13.9S67.1,231.6,59.4,231.6z M74.4,176.9H44.5V62.1h29.9V176.9z" id="el_kjRA8MVADg"></path>
	<path d="M89.9,38.9v206h45.9v-206H89.9z M112.9,231.6c-7.7,0-13.9-6.2-13.9-13.9s6.2-13.9,13.9-13.9
		c7.7,0,13.9,6.2,13.9,13.9S120.5,231.6,112.9,231.6z M127.8,176.9H97.9V62.1h29.9V176.9z" id="el_IL4daE4L5u"></path>
	<rect x="82.4" y="38.9" width="7.5" height="206" id="el_gcxt9ybwpA"></rect>
</g>
<g id="el_v2xYNX-lRs">
	<path id="el_2uFrqH6BNK" d="M182.3,109.8c-0.3,0-0.5,0-0.5,0h-0.1h-0.1c0,0-0.2,0-0.5,0c-2.8,0-11.9-0.9-15.2-12.5
		c-3.9-13.8-10.5-21.2-10.6-21.3l-0.1-0.1H155c-8.7-4.5-8.6-5.9-8.6-6.1c0-0.2,0.6-0.5,1.9-0.5c3.6,0,8.6,1.8,8.9,3.2
		c0.7,4.1,8.4,10.3,8.8,10.5l0.1,0.1l0.3,0.1c6.3,2.4,15.6,14.1,15.7,14.2c3.7,6.9,3.5,9.8,2.7,11.2
		C184.1,109.7,182.9,109.8,182.3,109.8z"></path>
	<path id="el_Dl1_yN_O_A" d="M148.5,69.9c3.6,0,8.2,1.8,8.4,2.8c0.7,4.2,8.1,10.2,8.9,10.9l0.1,0.1l0.2,0.1l0.2,0.1
		c5.9,2.2,14.6,13,15.4,14c3.7,7,3.3,9.6,2.8,10.7c-0.5,0.8-1.4,0.9-2,0.9c-0.2,0-0.4,0-0.4,0h-0.1h-0.1c0,0-0.2,0-0.5,0
		c-2.7,0-11.5-0.9-14.7-12.1c-4-13.9-10.4-21.2-10.7-21.5l-0.1-0.1l-0.2-0.1c-6.5-3.4-8-5-8.3-5.5C147.4,70,147.8,69.9,148.5,69.9z
		 M148.5,68.8c-3.6,0-4.6,1.9,6.4,7.6c0,0,6.5,7.2,10.5,21.1c3.4,12.1,13.3,12.8,15.7,12.8c0.4,0,0.6,0,0.6,0s0.2,0,0.6,0
		c1.7,0,6.7-1,0.2-13.2c0,0-9.4-12-15.9-14.4c-0.1,0-0.1,0-0.2-0.1c0,0-8-6.3-8.6-10.2C157.5,70.6,151.9,68.8,148.5,68.8z"></path>
	<path id="el_RK6VcbF9Ak" d="M197,114.1c-6,0-9.1-2.8-9.9-3.7c4.6-8.2-6.2-17.9-8.1-19.5c0.3-1.1,2.8-1.2,4.2-1.2
		c4.4,0,10.5,1.4,10.6,1.4h0.2l0.2-0.1c3.8-1.6,7.6-2.5,11.4-2.5c10.6,0,17.9,6.5,18.8,7.4c1,4.1,4.5,7.7,6.4,9.5
		c-2.2-0.3-4.4-0.4-6.2-0.4c-5.4,0-7,1.2-7.2,1.4C209.8,111.5,202.9,114.1,197,114.1z"></path>
	<path id="el_GkZB78ND8v" d="M205.5,89.2c10,0,17,5.9,18.3,7.1c0.9,3.5,3.5,6.6,5.5,8.5c-1.7-0.2-3.3-0.3-4.7-0.3
		c-5.2,0-7,1.1-7.4,1.4c-7.7,5-14.4,7.7-20.1,7.7c-5.3,0-8.3-2.3-9.3-3.3c4.2-8.1-5.4-17.2-8-19.5c0.4-0.3,1.5-0.6,3.6-0.6
		c4.4,0,10.4,1.4,10.4,1.4l0.3,0.1l0.3-0.1C198.1,90,201.8,89.2,205.5,89.2z M205.5,88.2c-3.6,0-7.4,0.7-11.6,2.6
		c0,0-6.1-1.4-10.7-1.4c-2.7,0-4.7,0.5-4.8,1.9c0,0,13.3,10.8,8,19.3c0,0,3.2,4.1,10.5,4.1c4.9,0,11.7-1.8,20.7-7.8
		c0,0,1.5-1.2,6.8-1.2c2,0,4.6,0.2,7.9,0.7c0,0-6.3-4.8-7.8-10.5C224.8,95.8,217.1,88.2,205.5,88.2z"></path>
	<path id="el_M68xNwWkdk" d="M233.7,98.5c-6.2,0-14.7-11-14.8-11.1l-0.1-0.1l-0.1-0.1c-4.4-2.4-9.3-3.6-14.7-3.6
		c-2.8,0-5.2,0.3-7,0.7c3.7-1.8,9.2-4.2,13.1-4.2c1.3,0,2.3,0.3,3,0.8c4.8,3.9,15.7,5,16.1,5.1c0.8,0,7.8-0.3,9.1-2.5
		c0.9-1.4,4.5-1.9,5.7-1.9c0.9,0.1,1.8,0.1,2.8,0.2c-3.8,1.1-6.1,4.7-6.2,4.8l-0.1,0.1V87c0,0.1-0.2,11-6.3,11.5
		C233.9,98.5,233.8,98.5,233.7,98.5z"></path>
	<path id="el_t6P250TK2b" d="M210,80.7c1.2,0,2,0.2,2.7,0.7c4.9,4,15.9,5.2,16.4,5.2h0.1h0.1c1.8-0.1,8.1-0.5,9.5-2.7
		c0.6-1,3.5-1.6,5.3-1.7c0.2,0,0.4,0,0.7,0.1c-2.8,1.5-4.5,4.1-4.6,4.2l-0.2,0.2v0.3c0,0.1-0.2,10.4-5.8,11c-0.1,0-0.2,0-0.4,0
		c-5,0-12-7.8-14.4-10.9l-0.1-0.2l-0.2-0.1c-4.4-2.5-9.5-3.7-15-3.7c-1.3,0-2.6,0.1-3.8,0.2C203.5,82,207.1,80.7,210,80.7z
		 M210,79.7c-6.4,0-17.1,6.4-17.1,6.4s4.7-1.7,11.1-1.7c4.4,0,9.5,0.8,14.5,3.5c0,0,8.6,11.3,15.2,11.3c0.1,0,0.3,0,0.4,0
		c6.6-0.5,6.8-11.9,6.8-11.9s3.2-4.8,7.9-4.9c4.7-0.2-4.8-0.9-4.8-0.9s-4.9,0.3-6.1,2.3s-8.7,2.3-8.7,2.3s-11.1-1.1-15.9-5
		C212.4,80,211.3,79.7,210,79.7z"></path>
	<path id="el_IILpIL4yKl" d="M226.9,84.1c-6.2,0-13.5-4.6-15.7-6.1c1.9-1.1,7.5-4.6,14.1-12.8c2.2-2.8,4.3-4.3,5.8-4.3
		c2.6,0,3.4,3.9,3.4,3.9l0.1,0.4h0.4c0.3,0,8.1,0.4,11.4,12.9c-0.4-0.1-1-0.1-1.8-0.1c-2.9,0-7.3,0.8-12.2,4.5
		C230.9,83.5,229.1,84.1,226.9,84.1z"></path>
	<path id="el_MLfhQ-SkSn" d="M231.1,61.4c2,0,2.9,3.5,2.9,3.5l0.2,0.8h0.8c0.1,0,7.3,0.3,10.7,11.8c-0.3,0-0.7,0-1.1,0
		c-3,0-7.5,0.8-12.5,4.6c-1.3,1-3.1,1.5-5.1,1.5c-5.5,0-12-3.8-14.8-5.5c2.4-1.4,7.5-5.1,13.5-12.5
		C228.4,62.1,230.1,61.4,231.1,61.4z M231.1,60.3c-1.5,0-3.6,1.1-6.2,4.5c-8.1,10-14.6,13-14.6,13s9.1,6.7,16.7,6.7
		c2,0,4.1-0.5,5.7-1.7c4.9-3.7,9.3-4.4,11.9-4.4c1.5,0,2.4,0.2,2.4,0.2c-3.5-13.7-12.1-14-12.1-14S234.1,60.3,231.1,60.3z"></path>
	<path id="el_gccEr9tuenQ" d="M223.7,60.6c0,0-4.6,2.9-10.4,1.6c-5.8-1.2-15.8,0.9-15.8,0.9s-4.8,1.3-6.4,6.9
		c-1.6,5.4-7.7,13.1-13.3,14.6c0,0,7.8,0.3,8.7,1.1C187.4,86.8,199,80,199,80s20.8-4.6,25.2-19.5"></path>
	<path id="el_0-0AolZpmsj" d="M223.7,60.6c0,0-4.6,2.9-10.4,1.6c-5.8-1.2-15.8,0.9-15.8,0.9s-4.8,1.3-6.4,6.9
		c-1.6,5.4-7.7,13.1-13.3,14.6c0,0,7.8,0.3,8.7,1.1C187.4,86.8,199,80,199,80s20.8-4.6,25.2-19.5"></path>
	<path id="el_qSFptQkJ1WR" d="M174.6,86.2c0,0,0.1-4.6,5.4-4.9c5.3-0.3,5.6-7.5,5.6-7.5s-2.2-4.6-15.8-8
		c-13.6-3.4-15.7-12.5-15.7-12.5S153.7,76.1,174.6,86.2z"></path>
	<path id="el_omiwDAVI5bt" d="M180.7,68.8c0,0-24.9-13.5-13.1-23c0,0-13-6.6-10.1,9.2C157.7,55,158.4,62.3,180.7,68.8z"></path>
	<path id="el_SU21KP7TWP4" d="M186,73c0,0-8.8-9.5-11.4-10.9c-2.7-1.3-16.2-10.1-2.3-17s11.8,25.9,13.9,28.3"></path>
	<path id="el_z9xLrXlH9F0" d="M186,73c0,0-8.8-9.5-11.4-10.9c-2.7-1.3-16.2-10.1-2.3-17s11.8,25.9,13.9,28.3"></path>
	<path id="el_DVacSQ1hI3T" d="M188,68.6c0,0,4-8.4,16.3-8.7c12.3-0.3,15.3-7.4,15.3-7.4s-11.1,11.8-36.4-8.2
		C183.1,44.4,185.4,65.5,188,68.6z"></path>
	<path id="el_V1w77osMJEr" d="M203.2,50.6c0,0,0.5-2.2-6.6-3.4c-7.1-1.1-3.6,4.9,1.2,3.5C202.7,49.4,203.2,50.6,203.2,50.6z"></path>
	<path id="el___VmsSkceZC" d="M205.4,51.3c0,0,9.3,0.7-1.1-7.2c-10.4-7.9-18.7-0.6-18.7-0.6s6.2,9.1,6.6,1.7
		C192.7,37.9,205.4,51.3,205.4,51.3z"></path>
	<path id="el_-ELpP9lOiiA" d="M180.9,114.2c0,0-16.3,18.9-22.2,53.4c-5.9,34.4,4-0.2,4-0.2s4.2-30.7,20.2-51.8
		C182.9,115.4,184,113.1,180.9,114.2z"></path>
	<path id="el_8GX9DpJSRnN" d="M163,149.9c0,0-12.5,1.9-12.5-13.5l1.4-11.8c0,0,4.4,0.7,8.8,5.8
		C165.1,135.6,169.7,141.4,163,149.9z"></path>
	<path id="el_YjURJLpFbrO" d="M162.9,148.8c0,0-8.8-13.9-8.8-19.9L162.9,148.8z"></path>
	<path id="el_9fedw1-uGM2" d="M162.9,148.8c0,0-8.8-13.9-8.8-19.9"></path>
	<path id="el_YXEMbeRnLIH" d="M160.7,143.7c0,0,2.2-5.8,0.7-7.4L160.7,143.7z"></path>
	<path id="el_XYcztSnJlvL" d="M160.7,143.7c0,0,2.2-5.8,0.7-7.4"></path>
	<path id="el_usAGVUnuFXt" d="M158,140c0,0-5.3-3-5.3-4.4L158,140z"></path>
	<path id="el_8aZysptywQd" d="M158,140c0,0-5.3-3-5.3-4.4"></path>
	<path id="el_nRjrBykZbwg" d="M163.4,159.4c0,0-2.6-16.7,17.8-16.7l15.5,1.9c0,0-0.9,5.8-7.8,11.7
		C182.1,162.1,174.6,168.3,163.4,159.4z"></path>
	<path id="el_1qsM4hYvbJf" d="M164.7,159.3c0,0,18.4-11.7,26.1-11.7L164.7,159.3z"></path>
	<path id="el_DXb4_RMjJuj" d="M164.7,159.3c0,0,18.4-11.7,26.1-11.7"></path>
	<path id="el_KDHTPN1RZTu" d="M171.5,156.3c0,0,7.8,2.9,9.7,0.9L171.5,156.3z"></path>
	<path id="el_Uj-K1nVvsi4" d="M171.5,156.3c0,0,7.8,2.9,9.7,0.9"></path>
	<path id="el_AWKiuIEirUc" d="M176.3,152.7c0,0,3.9-7.1,5.8-7.1L176.3,152.7z"></path>
	<path id="el_1qp9vCD2efx" d="M176.3,152.7c0,0,3.9-7.1,5.8-7.1"></path>
	<path id="el_b4M7GYc6e9p" d="M168.6,176.8h-23.8v68.1h56.5C177.4,226.9,164.3,204.9,168.6,176.8z"></path>
	<path id="el_kWCeuIAQnA4" d="M167.2,176.8h17.6c-17.1,12-4,38.4,16.5,68.1C201.3,245,160.1,220.5,167.2,176.8z"></path>
</g>
<g id="el_ajW5rPNVuCj">
	<rect x="44.5" y="62.1" width="29.9" height="114.8" id="el_3G6XCuPrv2Q"></rect>
	<ellipse cx="59.4" cy="217.7" rx="13.9" ry="13.9" id="el_zIQCnhZdELa"></ellipse>
	<ellipse cx="112.9" cy="217.7" rx="13.9" ry="13.9" id="el_QrHvgBbABE8"></ellipse>
	<rect x="97.9" y="62.1" width="29.9" height="114.8" id="el_jGzOv9cjZhF"></rect>
</g>
</svg>
</picture>`}