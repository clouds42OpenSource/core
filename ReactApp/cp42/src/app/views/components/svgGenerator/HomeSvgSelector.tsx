import { bp20 } from 'app/views/components/svgGenerator/svg/bp20';
import { bp20_corp } from 'app/views/components/svgGenerator/svg/bp20_corp';
import { bp30 } from 'app/views/components/svgGenerator/svg/bp30';
import { bp30_corp } from 'app/views/components/svgGenerator/svg/bp30_corp';
import { ca } from 'app/views/components/svgGenerator/svg/ca';
import { crm30 } from 'app/views/components/svgGenerator/svg/crm30';
import { custom } from 'app/views/components/svgGenerator/svg/custom';
import { recagency } from 'app/views/components/svgGenerator/svg/recagency';
import { roznica } from 'app/views/components/svgGenerator/svg/roznica';
import { roznicaOptika } from 'app/views/components/svgGenerator/svg/roznica-optika';
import { unf14 } from 'app/views/components/svgGenerator/svg/unf14';
import { unf15 } from 'app/views/components/svgGenerator/svg/unf15';
import { ut10 } from 'app/views/components/svgGenerator/svg/ut10';
import { ut11 } from 'app/views/components/svgGenerator/svg/ut11';
import { zup } from 'app/views/components/svgGenerator/svg/zup';
import { zup30 } from 'app/views/components/svgGenerator/svg/zup30';
import cn from 'classnames';
import css from './styles.module.css';

interface Props {
    id: string;
    height: number;
    width: number;
}

export const HomeSvgSelector = ({ id, width, height }: Props) => {
    switch (id) {
        case 'bp30':
            return (
                <div className={ cn(css.svg) }>
                    <div dangerouslySetInnerHTML={ { __html: bp30(width, height) } } />
                </div>
            );
        case 'bp20_corp':
            return (
                <div className={ cn(css.svg) }>
                    <div dangerouslySetInnerHTML={ { __html: bp20_corp(width, height) } } />
                </div>
            );
        case 'ca':
            return (
                <div className={ cn(css.svg) }>
                    <div dangerouslySetInnerHTML={ { __html: ca(width, height) } } />
                </div>
            );
        case 'bp20':
            return (
                <div className={ cn(css.svg) }>
                    <div dangerouslySetInnerHTML={ { __html: bp20(width, height) } } />
                </div>
            );
        case 'bp30_corp':
            return (
                <div className={ cn(css.svg) }>
                    <div dangerouslySetInnerHTML={ { __html: bp30_corp(width, height) } } />
                </div>
            );
        case 'crm30':
            return (
                <div className={ cn(css.svg) }>
                    <div dangerouslySetInnerHTML={ { __html: crm30(width, height) } } />
                </div>
            );
        case 'custom':
            return (
                <div className={ cn(css.svg) }>
                    <div dangerouslySetInnerHTML={ { __html: custom(width, height) } } />
                </div>
            );
        case 'recagency':
            return (
                <div className={ cn(css.svg) }>
                    <div dangerouslySetInnerHTML={ { __html: recagency(width, height) } } />
                </div>
            );
        case 'roznica-optika':
            return (
                <div className={ cn(css.svg) }>
                    <div dangerouslySetInnerHTML={ { __html: roznicaOptika(width, height) } } />
                </div>
            );
        case 'roznica':
            return (
                <div className={ cn(css.svg) }>
                    <div dangerouslySetInnerHTML={ { __html: roznica(width, height) } } />
                </div>
            );
        case 'unf14':
            return (
                <div className={ cn(css.svg) }>
                    <div dangerouslySetInnerHTML={ { __html: unf14(width, height) } } />
                </div>
            );
        case 'unf15':
            return (
                <div className={ cn(css.svg) }>
                    <div dangerouslySetInnerHTML={ { __html: unf15(width, height) } } />
                </div>
            );
        case 'ut10':
            return (
                <div className={ cn(css.svg) }>
                    <div dangerouslySetInnerHTML={ { __html: ut10(width, height) } } />
                </div>
            );
        case 'ut11':
            return (
                <div className={ cn(css.svg) }>
                    <div dangerouslySetInnerHTML={ { __html: ut11(width, height) } } />
                </div>
            );
        case 'zup':
            return (
                <div className={ cn(css.svg) }>
                    <div dangerouslySetInnerHTML={ { __html: zup(width, height) } } />
                </div>
            );
        case 'zup30':
            return (
                <div className={ cn(css.svg) }>
                    <div dangerouslySetInnerHTML={ { __html: zup30(width, height) } } />
                </div>
            );

        default:
            return (
                <div className={ cn(css.svg) }>
                    <div dangerouslySetInnerHTML={ { __html: custom(width, height) } } />
                </div>
            );
    }
};