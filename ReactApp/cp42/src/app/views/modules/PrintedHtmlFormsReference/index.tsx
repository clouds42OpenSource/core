import { AppRoutes } from 'app/AppRoutes';
import { GetPrintedHtmlFormsWithPaginationThunkParams } from 'app/modules/printedHtmlFormsReference/store/reducers/getPrintedHtmlFormsWithPaginationReducer/params';
import { GetPrintedHtmlFormsWithPaginationThunk } from 'app/modules/printedHtmlFormsReference/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { withHeader } from 'app/views/components/_hoc/withHeader';
import { OutlinedButton } from 'app/views/components/controls/Button';
import { SearchPrintedHtmlFormsView } from 'app/views/modules/PrintedHtmlFormsReference/views/SearchPrintedHtmlFormsView';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { TablePagination } from 'app/views/modules/_common/components/CommonTableWithFilter/TableView';
import { PrintedHtmlFormInfoDataModel } from 'app/web/InterlayerApiProxy/PrintedHtmlFormApiProxy/getPrintedHtmlFormsWithPagination/data-models';
import { SelectDataCommonDataModel, SelectDataResultMetadataModel } from 'app/web/common/data-models';
import { Component, createRef } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

type PrintedHtmlFormsProps = {
    /**
     * Массив информаций печатной формы HTML с пагинацией
     */
    printedHtmlForms: SelectDataResultMetadataModel<PrintedHtmlFormInfoDataModel>;
    metadata: TablePagination;
    searchQuery: string;
};

type DispatchProps = {
    dispatchGetPrintedHtmlFormsWithPaginationThunk: (args: GetPrintedHtmlFormsWithPaginationThunkParams) => void;
};

type AllProps = PrintedHtmlFormsProps & DispatchProps;

class PrintedHtmlFormsReferenceClass extends Component<AllProps> {
    private _searchViewRef = createRef<SearchPrintedHtmlFormsView>();

    public constructor(props: AllProps) {
        super(props);

        this.onDataSelect = this.onDataSelect.bind(this);
        this.getOperationsButton = this.getOperationsButton.bind(this);
    }

    public componentDidMount() {
        this.props.dispatchGetPrintedHtmlFormsWithPaginationThunk({
            force: true,
            pageNumber: 1,
            filter: {
                name: this.props.searchQuery
            }
        });
    }

    private onDataSelect(args: SelectDataCommonDataModel<undefined>) {
        this.props.dispatchGetPrintedHtmlFormsWithPaginationThunk({
            pageNumber: args.pageNumber,
            filter: {
                name: this._searchViewRef.current?.state.searchValue!
            },
            force: true
        });
    }

    private getOperationsButton(printedFormId: string) {
        return (
            <Link to={ `${ AppRoutes.administration.printedHtmlForms.editPrintedHtmlFormPath }/${ printedFormId }` }>
                <OutlinedButton kind="turquoise">
                    Редактировать
                </OutlinedButton>
            </Link>
        );
    }

    public render() {
        const { printedHtmlForms: { records } } = this.props;

        return (
            <>
                <SearchPrintedHtmlFormsView
                    ref={ this._searchViewRef }
                    defaultValue={ this.props.searchQuery }
                    dispatchOnSearchClick={ this.props.dispatchGetPrintedHtmlFormsWithPaginationThunk }
                />
                <CommonTableWithFilter
                    uniqueContextProviderStateId="PrintedHtmlFormsContextProviderStateId"
                    tableProps={ {
                        dataset: records,
                        keyFieldName: 'id',
                        fieldsView: {
                            name: {
                                caption: 'Название печатной формы',
                                fieldWidth: 'auto',
                                fieldContentNoWrap: false
                            },
                            id: {
                                caption: 'Операция',
                                fieldWidth: '30px',
                                format: this.getOperationsButton
                            }
                        },
                        pagination: this.props.metadata,
                    } }
                    onDataSelect={ this.onDataSelect }
                />
            </>
        );
    }
}
const PrintedHtmlFormsReferenceConnected = connect<PrintedHtmlFormsProps, DispatchProps, {}, AppReduxStoreState>(
    state => {
        const printedFormsState = state.PrintedHtmlFormsState;
        const { printedHtmlForms } = printedFormsState;
        const currentPage = printedHtmlForms.metadata.pageNumber;
        const totalPages = printedHtmlForms.metadata.pageCount;
        const { pageSize } = printedHtmlForms.metadata;
        const recordsCount = printedHtmlForms.metadata.totalItemCount;
        const { searchQuery } = printedHtmlForms;

        return {
            printedHtmlForms,
            searchQuery: searchQuery as string,
            metadata: {
                currentPage,
                totalPages,
                pageSize,
                recordsCount
            }
        };
    },
    {
        dispatchGetPrintedHtmlFormsWithPaginationThunk: GetPrintedHtmlFormsWithPaginationThunk.invoke
    }
)(PrintedHtmlFormsReferenceClass);

export const PrintedHtmlFormsReferenceView = withHeader({
    page: PrintedHtmlFormsReferenceConnected,
    title: 'Печатные формы HTML'
});