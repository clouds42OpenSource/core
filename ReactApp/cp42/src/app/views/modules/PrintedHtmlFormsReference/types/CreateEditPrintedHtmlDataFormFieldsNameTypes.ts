import { CreateEditPrintedHtmlDataForm } from 'app/views/modules/PrintedHtmlFormsReference/types';

export type CreateEditPrintedHtmlDataFormFieldsNameTypes = keyof CreateEditPrintedHtmlDataForm;