import { CloudFileDataModel } from 'app/web/InterlayerApiProxy/PrintedHtmlFormApiProxy/common/data-models';

/**
 * Модель Redux формы для создания, редактирования печатных форм HTML
 */
export type CreateEditPrintedHtmlDataForm = {
    /**
     * Название печатной формы Html
     */
    name: string;

    /**
     * Html разметка
     */
    htmlData: string;

    /**
     * Тип модели
     */
    modelType: string;

    /**
     * Прикрепленные файлы
     */
    files?: Array<CloudFileDataModel>;

    /**
     * Сообщение об ошибке скачивания тестового шаблона печатной формы
     */
    downloadErrorMessage?: string;
}