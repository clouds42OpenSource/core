/**
 * Тип параметра для печатных форм HTML
 */
export type EditRouteParams = {
    /**
     * ID печатной формы HTML
     */
    id?: string
}