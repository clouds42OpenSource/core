import { Box } from '@mui/material';
import { AppRoutes } from 'app/AppRoutes';
import { PageEnumType } from 'app/common/enums';
import { checkValidOfStringField } from 'app/common/functions';
import { IErrorsType } from 'app/common/interfaces';
import { PrintedHtmlFormsReferenceProccessId } from 'app/modules/printedHtmlFormsReference';
import { CreatePrintedHtmlFormParams } from 'app/modules/printedHtmlFormsReference/store/reducers/createPrintedHtmlFormReducer/params';
import { DownloadDocumentWithTestDataThunkParams } from 'app/modules/printedHtmlFormsReference/store/reducers/downloadDocumentWithTestDataReducer/params';
import { EditPrintedHtmlFormThunkParams } from 'app/modules/printedHtmlFormsReference/store/reducers/editPrintedHtmlFormReducer/params';
import { GetPrintedHtmlFormByIdThunkParams } from 'app/modules/printedHtmlFormsReference/store/reducers/getPrintedHtmlFormByIdReducer/params';
import { CreatePrintedHtmlFormThunk, DownloadDocumentWithTestDataThunk } from 'app/modules/printedHtmlFormsReference/store/thunks';
import { EditPrintedHtmlFormThunk } from 'app/modules/printedHtmlFormsReference/store/thunks/EditPrintedHtmlFormThunk';
import { GetModelTypesThunk } from 'app/modules/printedHtmlFormsReference/store/thunks/GetModelTypesThunk';
import { GetPrintedHtmlFormByIdThunk } from 'app/modules/printedHtmlFormsReference/store/thunks/GetPrintedHtmlFormByIdThunk';
import { AppReduxStoreState } from 'app/redux/types';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { PageHeaderView } from 'app/views/components/_hoc/withHeader/PageHeaderView';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { DefaultButton, OutlinedButton, SuccessButton } from 'app/views/components/controls/Button';
import { CodeMirrorView } from 'app/views/components/controls/Codemirror';
import { ComboBoxForm, TextBoxForm } from 'app/views/components/controls/forms';
import { CreateEditPrintedHtmlDataForm, CreateEditPrintedHtmlDataFormFieldsNameTypes, EditRouteParams } from 'app/views/modules/PrintedHtmlFormsReference/types';
import { AttachedFilesView } from 'app/views/modules/PrintedHtmlFormsReference/views/AttachedFilesView';
import { ShowErrorMessage } from 'app/views/modules/_common/components/ShowErrorMessage';
import { WatchReducerProcessActionState } from 'app/views/modules/_common/components/WatchReducerProcessActionState';
import { EditPrintedHtmlFormDataModel } from 'app/web/InterlayerApiProxy/PrintedHtmlFormApiProxy/common/data-models';
import { KeyValueDataModel } from 'app/web/common/data-models';
import { IReducerProcessActionInfo } from 'core/redux/interfaces';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';
import { Component, createRef } from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { Link } from 'react-router-dom';
import css from '../../styles.module.css';

type CreateEditPrintedHtmlFormProps = ReduxFormProps<CreateEditPrintedHtmlDataForm>;

type StateProps = {
    /**
     * Список типов моделей
     */
    modelTypes: KeyValueDataModel<string, string>[];

    /**
     * Печатная форма HTML
     */
    printedHtmlForm: EditPrintedHtmlFormDataModel;
};

type CreateEditPrintedHtmlFormState = {
    /**
     * Обьект Ошибок для валидации
     */
    errors: IErrorsType;

    /**
     * Тип страницы для динамического компонента редактирования/создания
     */
    pageType: PageEnumType;

    /**
     * ID печатной формы HTML
     */
    currentPrintedHtmlFormId?: string;
};

type DispatchProps = {
    dispatchGetModelTypesThunk: (args: IForceThunkParam) => void;
    dispatchCreatePrintedHtmlFormThunk: (args: CreatePrintedHtmlFormParams) => void;
    dispatchGetPrintedHtmlFormByIdThunk: (args: GetPrintedHtmlFormByIdThunkParams) => void;
    dispatchEditPrintedHtmlFormThunk: (args: EditPrintedHtmlFormThunkParams) => void;
    dispatchDownloadDocumentWithTestDataThunk: (args: DownloadDocumentWithTestDataThunkParams) => void;
};

type AllProps = CreateEditPrintedHtmlFormProps & StateProps & DispatchProps & FloatMessageProps & RouteComponentProps<EditRouteParams>;

class CreateEditPrintedHtmlFormClass extends Component<AllProps, CreateEditPrintedHtmlFormState> {
    private _attachedFileRef = createRef<AttachedFilesView>();

    public constructor(props: AllProps) {
        super(props);

        this.state = {
            errors: {},
            pageType: this.props.match.params.id ? PageEnumType.Edit : PageEnumType.Create,
            currentPrintedHtmlFormId: this.props.match.params.id
        };

        this.onProcessActionStateChanged = this.onProcessActionStateChanged.bind(this);
        this.onProcessGetModelTypes = this.onProcessGetModelTypes.bind(this);
        this.onProcessGetPrintedHtmlFormById = this.onProcessGetPrintedHtmlFormById.bind(this);
        this.onProcessCreatePrintedHtmlForm = this.onProcessCreatePrintedHtmlForm.bind(this);
        this.onProcessEditPrintedHtmlForm = this.onProcessEditPrintedHtmlForm.bind(this);
        this.onProcessDownloadDocumentWithTestData = this.onProcessDownloadDocumentWithTestData.bind(this);

        this.onValueChange = this.onValueChange.bind(this);
        this.submitButton = this.submitButton.bind(this);
        this.isValidPrintedHtmlForm = this.isValidPrintedHtmlForm.bind(this);
        this.onClickToDownloadTestData = this.onClickToDownloadTestData.bind(this);
        this.props.reduxForm.setInitializeFormDataAction(this.defaultInitDateReduxForm);
    }

    public componentDidMount() {
        this.props.dispatchGetModelTypesThunk({
            force: true
        });

        if (this.state.currentPrintedHtmlFormId) {
            this.props.dispatchGetPrintedHtmlFormByIdThunk({
                printedHtmlId: this.state.currentPrintedHtmlFormId,
                force: true
            });
        }
    }

    private onClickToDownloadTestData() {
        const formFields = this.props.reduxForm.getReduxFormFields(false);
        const { uploadedFiles, files } = this._attachedFileRef.current!.state;

        if (!this.isValidPrintedHtmlForm(formFields)) return;

        this.props.dispatchDownloadDocumentWithTestDataThunk({
            id: this.state.currentPrintedHtmlFormId,
            htmlData: formFields.htmlData,
            modelType: formFields.modelType,
            name: formFields.name,
            files,
            uploadedFiles,
            force: true
        });
    }

    private onProcessGetPrintedHtmlFormById(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId === PrintedHtmlFormsReferenceProccessId.GetPrintedHtmlFormById) {
            if (actionState.isInSuccessState) {
                const { printedHtmlForm } = this.props;
                this.props.reduxForm.updateReduxFormFields({
                    htmlData: printedHtmlForm.htmlData,
                    modelType: printedHtmlForm.modelType,
                    name: printedHtmlForm.name,
                    files: printedHtmlForm.files
                });
            }
            if (actionState.isInErrorState) {
                this.props.floatMessage.show(MessageType.Error, `Ошибка при получении печатной формы: ${ error?.message }`);
            }
        }
    }

    private onProcessEditPrintedHtmlForm(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId === PrintedHtmlFormsReferenceProccessId.EditPrintedHtmlForm) {
            if (actionState.isInSuccessState) {
                this.props.floatMessage.show(MessageType.Success, 'Изменения печатной формы сохранена');
                this.props.history.push(AppRoutes.administration.printedHtmlFormsReferenceRoute);
            }
            if (actionState.isInErrorState) {
                this.props.floatMessage.show(MessageType.Error, `Ошибка при изменении печатной формы: ${ error?.message }`);
            }
        }
    }

    private onProcessDownloadDocumentWithTestData(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId === PrintedHtmlFormsReferenceProccessId.DownloadDocumentWithTestData) {
            if (actionState.isInErrorState) {
                this.props.reduxForm.updateReduxFormFields({
                    downloadErrorMessage: error?.message
                });
            }
        }
    }

    private onProcessCreatePrintedHtmlForm(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId === PrintedHtmlFormsReferenceProccessId.CreatePrintedHtmlForm) {
            if (actionState.isInSuccessState) {
                this.props.floatMessage.show(MessageType.Success, 'Печатная форма создана');
                this.props.history.push(AppRoutes.administration.printedHtmlFormsReferenceRoute);
            }
            if (actionState.isInErrorState) {
                this.props.floatMessage.show(MessageType.Error, `Ошибка при создании: ${ error?.message }`);
            }
        }
    }

    private onProcessActionStateChanged(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        this.onProcessCreatePrintedHtmlForm(processId, actionState, error);
        void this.onProcessGetModelTypes(processId, actionState, error);
        this.onProcessGetPrintedHtmlFormById(processId, actionState, error);
        this.onProcessEditPrintedHtmlForm(processId, actionState, error);
        this.onProcessDownloadDocumentWithTestData(processId, actionState, error);
    }

    private async onProcessGetModelTypes(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId === PrintedHtmlFormsReferenceProccessId.GetModelTypes) {
            if (actionState.isInSuccessState) {
                this.props.reduxForm.updateReduxFormFields({
                    modelType: this.props.modelTypes[0].key
                });
            }
            if (actionState.isInErrorState) {
                this.props.floatMessage.show(MessageType.Error, `Ошибка при получении типов моделей: ${ error?.message }`);
            }
        }
    }

    private onValueChange<TValue, TForm extends string = CreateEditPrintedHtmlDataFormFieldsNameTypes>(formName: TForm, newValue: TValue) {
        this.props.reduxForm.updateReduxFormFields({
            [formName]: newValue
        });

        const { errors } = this.state;

        if (typeof newValue === 'string') {
            switch (formName) {
                case 'name':
                    errors[formName] = checkValidOfStringField(newValue, 'Заполните название печатной формы');
                    break;
                case 'htmlData':
                    errors[formName] = checkValidOfStringField(newValue, 'Заполните разметку HTML');
                    break;
            }

            this.setState({ errors });
        }
    }

    private submitButton() {
        const { pageType, currentPrintedHtmlFormId } = this.state;
        const formFields = this.props.reduxForm.getReduxFormFields(false);
        const { uploadedFiles, files } = this._attachedFileRef.current!.state;

        if (!this.isValidPrintedHtmlForm(formFields)) return;

        if (pageType === PageEnumType.Create) {
            this.props.dispatchCreatePrintedHtmlFormThunk({
                htmlData: formFields.htmlData,
                modelType: formFields.modelType,
                name: formFields.name,
                uploadedFiles,
                force: true
            });
        }

        if (pageType === PageEnumType.Edit) {
            this.props.dispatchEditPrintedHtmlFormThunk({
                id: currentPrintedHtmlFormId!,
                htmlData: formFields.htmlData,
                modelType: formFields.modelType,
                name: formFields.name,
                files,
                uploadedFiles,
                force: true
            });
        }
    }

    private defaultInitDateReduxForm(): CreateEditPrintedHtmlDataForm | undefined {
        return {
            htmlData: '',
            modelType: '',
            name: ''
        };
    }

    private isValidPrintedHtmlForm(formFields: CreateEditPrintedHtmlDataForm) {
        const errors: IErrorsType = {};
        let result = true;

        if (!formFields.name || formFields.name === '') {
            errors.name = 'Заполните название печатной формы';
            result = false;
        }
        if (!formFields.htmlData || formFields.htmlData === '') {
            errors.htmlData = 'Заполните разметку HTML';
            result = false;
        }

        if (!result) {
            this.props.floatMessage.show(MessageType.Warning, 'Форма к отправке не готова. Пожалуйста, заполните все поля.');
            this.setState({ errors });
        }

        return result;
    }

    public render() {
        const formFields = this.props.reduxForm.getReduxFormFields(false);
        const { errors, pageType } = this.state;
        const textTitle = pageType === PageEnumType.Create ? 'Создание' : 'Редактирование';

        return (
            <>
                <PageHeaderView title={ `${ textTitle } печатной формы` } />
                {
                    !formFields.downloadErrorMessage
                        ? <Box display="flex" flexDirection="column" gap="16px">
                            <div>
                                <TextBoxForm
                                    label="Название печатной формы"
                                    formName="name"
                                    onValueChange={ this.onValueChange }
                                    value={ formFields.name }
                                    placeholder="Введите название печатной формы"
                                />
                                <ShowErrorMessage errors={ errors } formName="name" />
                            </div>
                            <div>
                                <ComboBoxForm
                                    formName="modelType"
                                    label="Модель"
                                    value={ formFields.modelType }
                                    items={ this.props.modelTypes.map(item => ({
                                        text: item.value,
                                        value: item.key
                                    })) }
                                    onValueChange={ this.onValueChange }
                                />
                            </div>
                            <div>
                                <CodeMirrorView
                                    onChange={ this.onValueChange }
                                    value={ formFields.htmlData }
                                    formName="htmlData"
                                    label="Разметка Html"
                                />
                                <ShowErrorMessage errors={ errors } formName="htmlData" />
                            </div>
                            <AttachedFilesView { ...this.props } ref={ this._attachedFileRef } defaultFiles={ formFields.files } />
                            <hr />
                            <Box display="flex" justifyContent="space-between" sx={ { flexDirection: { xs: 'column', md: 'row' } } } gap="16px">
                                <div>
                                    <OutlinedButton
                                        kind="primary"
                                        onClick={ this.onClickToDownloadTestData }
                                    >
                                        <i className="fa fa-download mr-1" />
                                        Скачать тестовый шаблон
                                    </OutlinedButton>
                                </div>
                                <Box display="flex" gap="16px">
                                    <Link to={ AppRoutes.administration.printedHtmlFormsReferenceRoute }>
                                        <DefaultButton>
                                            Назад
                                        </DefaultButton>
                                    </Link>
                                    <SuccessButton
                                        onClick={ this.submitButton }
                                    >
                                        {
                                            pageType === PageEnumType.Create
                                                ? 'Создать'
                                                : 'Сохранить'
                                        }
                                    </SuccessButton>
                                </Box>
                            </Box>
                          </Box>
                        : <div className={ css['print-error-message'] } dangerouslySetInnerHTML={ { __html: formFields.downloadErrorMessage } } />
                }
                <WatchReducerProcessActionState
                    watch={ [{
                        reducerStateName: 'PrintedHtmlFormsState',
                        processIds: [
                            PrintedHtmlFormsReferenceProccessId.GetModelTypes,
                            PrintedHtmlFormsReferenceProccessId.CreatePrintedHtmlForm,
                            PrintedHtmlFormsReferenceProccessId.GetPrintedHtmlFormById,
                            PrintedHtmlFormsReferenceProccessId.EditPrintedHtmlForm,
                            PrintedHtmlFormsReferenceProccessId.DownloadDocumentWithTestData
                        ]
                    }] }
                    onProcessActionStateChanged={ this.onProcessActionStateChanged }
                />
            </>
        );
    }
}

export const CreateEditPrintedHtmlFormConnected = connect<StateProps, DispatchProps, CreateEditPrintedHtmlFormProps, AppReduxStoreState>(
    state => {
        const printedHtmlFormsState = state.PrintedHtmlFormsState;
        const { modelTypes } = printedHtmlFormsState;
        const { printedHtmlForm } = printedHtmlFormsState;

        return {
            modelTypes,
            printedHtmlForm
        };
    },
    {
        dispatchGetModelTypesThunk: GetModelTypesThunk.invoke,
        dispatchCreatePrintedHtmlFormThunk: CreatePrintedHtmlFormThunk.invoke,
        dispatchGetPrintedHtmlFormByIdThunk: GetPrintedHtmlFormByIdThunk.invoke,
        dispatchEditPrintedHtmlFormThunk: EditPrintedHtmlFormThunk.invoke,
        dispatchDownloadDocumentWithTestDataThunk: DownloadDocumentWithTestDataThunk.invoke

    }
)(CreateEditPrintedHtmlFormClass);

export const CreateEditPrintedHtmlFormView = withReduxForm(withFloatMessages(CreateEditPrintedHtmlFormConnected),
    {
        reduxFormName: 'Create_Edit_Printed_Html_Form',
        resetOnUnmount: true
    });