declare const styles: {
  readonly 'attached-file-wrapper': string;
  readonly 'span-group-addon': string;
  readonly 'input-group-addon': string;
  readonly 'label-custom': string;
  readonly 'attached-button': string;
  readonly 'remove-button': string;
  readonly 'input-custom': string;
};
export = styles;