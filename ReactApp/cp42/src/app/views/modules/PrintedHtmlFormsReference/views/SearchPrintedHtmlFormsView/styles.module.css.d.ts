declare const styles: {
  readonly 'search-custom': string;
  readonly 'search-button': string;
};
export = styles;