import { ContainedButton } from 'app/views/components/controls/Button';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { CloudFileDataModel } from 'app/web/InterlayerApiProxy/PrintedHtmlFormApiProxy/common/data-models';
import cn from 'classnames';
import React, { Component, createRef } from 'react';
import { Box } from '@mui/material';
import css from './styles.module.css';

type AttachedFilesProps = {
    /**
     * Массив дефолтных облачных файлов
     */
    defaultFiles?: CloudFileDataModel[];
};

type AttachedFilesState = {
    /**
     * Массив облачных файлов
     */
    files: CloudFileDataModel[];

    /**
     * Массив подгруженных файлов
     */
    uploadedFiles: File[];
};

type AllProps = AttachedFilesProps & FloatMessageProps;

export class AttachedFilesView extends Component<AllProps, AttachedFilesState> {
    private _inputFileRef = createRef<HTMLInputElement>();

    private _acceptedTypes = '.png,.gif,.jpg';

    public constructor(props: AllProps) {
        super(props);

        this.state = {
            files: this.props.defaultFiles ?? [],
            uploadedFiles: []
        };

        this.showCloudFiles = this.showCloudFiles.bind(this);
        this.showUploadedFiles = this.showUploadedFiles.bind(this);

        this.onChangeInput = this.onChangeInput.bind(this);
        this.onClickToRemoveFile = this.onClickToRemoveFile.bind(this);
        this.getAttachedFileElement = this.getAttachedFileElement.bind(this);
    }

    public componentDidUpdate(nextProps: AttachedFilesProps) {
        const { defaultFiles } = this.props;

        if (nextProps.defaultFiles !== defaultFiles && nextProps.defaultFiles?.length !== defaultFiles?.length) {
            if (defaultFiles) {
                this.setState({ files: defaultFiles });
            }
        }
    }

    private onFocusInput(event: React.FocusEvent<HTMLInputElement>) {
        const { target } = event;
        setTimeout(() => target.select(), 0);
    }

    private onClickToRemoveFile(index: number, isCloudFile = true) {
        if (isCloudFile) {
            const { files } = this.state;

            if (files[index].id) files[index].isDeleted = true;
            else files.splice(index, 1);

            this.setState({
                files
            });
        } else {
            const { uploadedFiles } = this.state;
            uploadedFiles.splice(index, 1);
            this.setState({
                uploadedFiles
            });
        }
    }

    private onChangeInput(event: React.ChangeEvent<HTMLInputElement>) {
        let file = {} as File;

        if (event.currentTarget !== null && event.currentTarget.files) {
            file = event.currentTarget.files[0];
            const fileType = file?.name.split('.').pop();

            if (this.validateUploadedFile(fileType, file.name)) {
                this.setState(prevState => ({
                    uploadedFiles: [...prevState.uploadedFiles, file]
                }));
            }

            event.currentTarget.value = '';
        }
    }

    private getAttachedFileElement(fileName: string, index: number, isCloudFile = true) {
        return (
            <Box key={ index } className={ cn(css['attached-file-wrapper'], 'mt-2') } display="flex" flexDirection="column">
                <span className={ css['label-custom'] }>Название:</span>
                <span className="ml-1">{ fileName }</span>
                <Box display="flex" flexDirection="row">
                    <div className="input-group">
                        <span className={ css['span-group-addon'] }>Ключ:</span>
                        <input
                            type="text"
                            className={ cn(css['input-group-addon'], 'form-control') }
                            value={ `@@${ fileName }@@` }
                            onFocus={ this.onFocusInput }
                            readOnly={ true }
                        />
                    </div>
                    <ContainedButton
                        kind="error"
                        onClick={ () => { this.onClickToRemoveFile(index, isCloudFile); } }
                        className={ css['remove-button'] }
                    >
                        <i className="fa fa-times mr-1" /> Удалить
                    </ContainedButton>
                </Box>
            </Box>
        );
    }

    private showUploadedFiles() {
        return this.state.uploadedFiles.map((element, index) => {
            return this.getAttachedFileElement(element.name, index, false);
        });
    }

    private showCloudFiles() {
        return this.state.files.map((element, index) => {
            return !element.isDeleted ? this.getAttachedFileElement(element.fileName, index) : null;
        });
    }

    private validateUploadedFile(fileType?: string, fileName?: string) {
        if (!this.isValidNameFile(fileName)) return false;

        return this.isAcceptedType(fileType);
    }

    private isValidNameFile(fileName?: string) {
        const { files, uploadedFiles } = this.state;
        let isValid = true;

        if (files && files.length > 0) {
            isValid = !files.some(item => {
                return item.fileName === fileName;
            });
        }

        if (isValid && uploadedFiles && uploadedFiles.length > 0) {
            isValid = !uploadedFiles.some(file => {
                return file.name === fileName;
            });
        }

        if (!isValid) this.props.floatMessage.show(MessageType.Error, 'Файл с таким названием уже был закреплен ранее');

        return isValid;
    }

    private isAcceptedType(fileType?: string) {
        let isValid = true;

        if (this._acceptedTypes) {
            const acceptedTypes = this._acceptedTypes.split(',').filter(item => { return item !== ''; });
            isValid = false;

            for (let index = 0; index < acceptedTypes.length; index++) {
                if (fileType && acceptedTypes[index].replace('.', '') === fileType) {
                    isValid = true;
                }
            }

            if (!isValid) this.props.floatMessage.show(MessageType.Error, `Не допустимый формат файла, допустимый формат: ${ this._acceptedTypes }`);
        }

        return isValid;
    }

    public render() {
        return (
            <>
                { this.showCloudFiles() }
                { this.showUploadedFiles() }
                <input
                    ref={ this._inputFileRef }
                    type="file"
                    onChange={ this.onChangeInput }
                    accept={ this._acceptedTypes }
                    className={ css['input-custom'] }
                />
                <ContainedButton
                    kind="default"
                    className={ css['attached-button'] }
                    onClick={ () => {
                        this._inputFileRef.current?.click();
                    } }
                >
                    + Прикрепить файл
                </ContainedButton>
            </>
        );
    }
}