import { AppRoutes } from 'app/AppRoutes';
import { GetPrintedHtmlFormsWithPaginationThunkParams } from 'app/modules/printedHtmlFormsReference/store/reducers/getPrintedHtmlFormsWithPaginationReducer/params';
import { TableFilterWrapper } from 'app/views/components/_hoc/tableFilterWrapper';
import { FilterItemWrapper } from 'app/views/components/_hoc/tableFilterWrapper/filterItemWrapper';
import { SearchByFilterButton, SuccessButton } from 'app/views/components/controls/Button';
import { TextBoxForm } from 'app/views/components/controls/forms';
import { Component } from 'react';
import { Link } from 'react-router-dom';
import css from './styles.module.css';

type SearchPrintedHtmlFormsProps = {
    /**
     * Thunk на поиск и получение печатных форм
     */
    dispatchOnSearchClick: (args: GetPrintedHtmlFormsWithPaginationThunkParams) => void;
    defaultValue: string;
};

type SearchPrintedHtmlFormsState = {
    /**
     * Значение поисковика
     */
    searchValue: string;
};

export class SearchPrintedHtmlFormsView extends Component<SearchPrintedHtmlFormsProps, SearchPrintedHtmlFormsState> {
    public constructor(props: SearchPrintedHtmlFormsProps) {
        super(props);

        this.state = {
            searchValue: this.props.defaultValue
        };

        this.onChangeSearchTextBox = this.onChangeSearchTextBox.bind(this);
        this.onClickSearchButton = this.onClickSearchButton.bind(this);
    }

    private onChangeSearchTextBox(_formName: string, newValue: string) {
        this.setState({
            searchValue: newValue
        });
    }

    private onClickSearchButton() {
        this.props.dispatchOnSearchClick({
            filter: {
                name: this.state.searchValue
            },
            pageNumber: 1,
            force: true
        });
    }

    public render() {
        return (
            <div style={ { paddingBottom: '16px' } }>
                <TableFilterWrapper>
                    <FilterItemWrapper width="350px">
                        <TextBoxForm
                            formName="PrintedHtmlFormsSearch"
                            label="Печатная форма"
                            placeholder="Название печатной формы"
                            value={ this.state.searchValue }
                            onValueChange={ this.onChangeSearchTextBox }
                        />
                    </FilterItemWrapper>
                    <SearchByFilterButton
                        className={ css['search-button'] }
                        kind="success"
                        onClick={ this.onClickSearchButton }
                    />
                    <FilterItemWrapper width="350px">
                        <Link to={ AppRoutes.administration.printedHtmlForms.createPrintedHtmlForm }>
                            <SuccessButton
                                kind="success"
                            >
                                <i className="fa fa-plus-circle mr-1" />
                                Добавить печатную форму HTML
                            </SuccessButton>
                        </Link>
                    </FilterItemWrapper>
                </TableFilterWrapper>
            </div>
        );
    }
}