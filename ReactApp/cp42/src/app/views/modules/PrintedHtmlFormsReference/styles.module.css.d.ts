declare const styles: {
  readonly 'custom-table': string;
  readonly 'print-error-message': string;
};
export = styles;