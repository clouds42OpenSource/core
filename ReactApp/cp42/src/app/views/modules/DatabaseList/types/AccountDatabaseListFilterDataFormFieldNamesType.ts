import { AccountDatabaseListFilterDataForm } from 'app/views/modules/DatabaseList/types';

export type AccountDatabaseListFilterDataFormFieldNamesType = keyof AccountDatabaseListFilterDataForm;