/**
 * Модель Redux формы для редактирования фильтра для таблицы списка информационных баз
 */
export type AccountDatabaseListFilterDataForm = {
    /**
     * Строка фильтра
     */
    accountDatabaseSearchLine: string;
};