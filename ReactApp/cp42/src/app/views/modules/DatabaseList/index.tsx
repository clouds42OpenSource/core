/* eslint-disable react/no-unstable-nested-components */
import { contextAccountId, userId } from 'app/api';
import { yandexCounter, YandexMetrics } from 'app/common/_external-services/yandexMetrics';
import { SortingKind } from 'app/common/enums';
import { hasComponentChangesFor } from 'app/common/functions';
import { ProcessIdStateHelper } from 'app/common/helpers/ProcessIdStateHelper';
import { useDatabaseCard } from 'app/hooks';
import { DatabaseListProcessId } from 'app/modules/databaseList/DatabaseListProcessId';
import {
    ReceiveDatabaseListThunkParams
} from 'app/modules/databaseList/store/reducers/receiveDatabaseListReducer/params';
import { ReceiveDatabaseListThunk } from 'app/modules/databaseList/store/thunks';
import { AvailabilityInfoDbListThunk } from 'app/modules/infoDbList/store/thunks/AvailabilityInfoDbListThunk';
import { AppReduxStoreState } from 'app/redux/types';
import { DateUtility } from 'app/utils';
import { withHeader } from 'app/views/components/_hoc/withHeader';
import { TextButton } from 'app/views/components/controls/Button';
import { TextOut } from 'app/views/components/TextOut';
import { CommonTableWithFilter, SelectDataInitiator } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { TablePagination } from 'app/views/modules/_common/components/CommonTableWithFilter/TableView';
import { AccountCard } from 'app/views/modules/_common/reusable-modules/AccountCard';
import { InformationDatabaseCard } from 'app/views/modules/_common/reusable-modules/InformationDatabaseCard';
import { AccountDatabaseListFilterDataForm } from 'app/views/modules/DatabaseList/types';
import { DatabaseListFilterFormView } from 'app/views/modules/DatabaseList/views/DatabaseListFilterFormView';
import { InputAccountId } from 'app/web/api/InfoDbProxy/request-dto/InputAccountId';
import { SelectDataCommonDataModel, SortingDataModel } from 'app/web/common/data-models';
import { DatabaseListItemDataModel } from 'app/web/InterlayerApiProxy/DatabaseApiProxy/receiveDatabaseList/data-models';
import { Check as CheckIcon } from '@mui/icons-material';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';
import React, { ReactNode } from 'react';
import { connect } from 'react-redux';

type TProps = {
    openDatabase: (databaseNumber: string) => void;
    databaseCard: ReactNode;
};

type StateProps<TDataRow = DatabaseListItemDataModel> = {
    dataset: TDataRow[];
    isInLoadingDataProcess: boolean;
    pagination: TablePagination;
};

type DispatchProps = {
    dispatchReceiveDatabaseListThunk: (args: ReceiveDatabaseListThunkParams) => void;
    dispatchAvailabilityInfoDbThunk: (args: IForceThunkParam & InputAccountId) => void;
};

type AllProps = TProps & StateProps & DispatchProps;

type OwnState = {
    isDatabaseCardVisible: boolean;
    isAccountCardVisible: boolean;
    accountNumber: number;
    changeField: boolean;
    activeTab: number;
    databaseId: string;
};

class DatabaseListViewClass extends React.Component<AllProps, OwnState> {
    public constructor(props: AllProps) {
        super(props);
        this.onDataSelect = this.onDataSelect.bind(this);

        this.showAccountCard = this.showAccountCard.bind(this);
        this.hideAccountCard = this.hideAccountCard.bind(this);
        this.trimLongAccountName = this.trimLongAccountName.bind(this);
        this.getSortQuery = this.getSortQuery.bind(this);
        this.onChangeField = this.onChangeField.bind(this);
        this.onChangeTab = this.onChangeTab.bind(this);

        this.state = {
            isDatabaseCardVisible: false,
            isAccountCardVisible: false,
            accountNumber: 0,
            changeField: false,
            activeTab: 0,
            databaseId: ''
        };
    }

    public componentDidMount() {
        this.props.dispatchAvailabilityInfoDbThunk({ accountId: contextAccountId(), accountUserId: userId(), force: true });
    }

    public shouldComponentUpdate(nextProps: AllProps, nextState: OwnState) {
        return hasComponentChangesFor(this.props, nextProps) ||
            hasComponentChangesFor(this.state, nextState);
    }

    private onDataSelect(args: SelectDataCommonDataModel<AccountDatabaseListFilterDataForm>, _filterFormName: string | null, initiator: SelectDataInitiator) {
        this.props.dispatchReceiveDatabaseListThunk({
            pageNumber: args.pageNumber,
            filter: {
                searchLine: args.filter?.accountDatabaseSearchLine ?? ''
            },
            orderBy: args.sortingData ? this.getSortQuery(args.sortingData) : 'creationDate.desc',
            force: true
        });
        if (initiator === 'filter-changed') {
            yandexCounter.getInstance().reachGoal(YandexMetrics.SearchUserAdminDb);
        }
    }

    private onChangeField() {
        this.setState(({ changeField }) => ({
            changeField: !changeField
        }));
    }

    private onChangeTab(value: number) {
        this.setState({
            activeTab: value
        });
    }

    private getSortQuery(args: SortingDataModel) {
        switch (args.fieldName) {
            case 'creationDate':
                return `creationDate.${ args.sortKind ? 'desc' : 'asc' }`;
            case 'lastActivityDate':
                return `lastActivityDate.${ args.sortKind ? 'desc' : 'asc' }`;
            default:
                return 'creationDate.desc';
        }
    }

    private hideAccountCard() {
        this.setState({
            isAccountCardVisible: false,
            accountNumber: 0,
        });
    }

    private showAccountCard(accountNumber: number) {
        return () => {
            this.setState({
                isAccountCardVisible: true,
                accountNumber
            });
        };
    }

    /**
     * Обрезать длинное название аккаунта
     * @param accountName название аккаунта
     */
    private trimLongAccountName(accountName: string): string {
        const maxAvailableSymbols = 16;
        if (accountName.length <= maxAvailableSymbols) { return accountName; }
        const shortAccountName = accountName.slice(0, maxAvailableSymbols - 1);
        return `${ shortAccountName }...`;
    }

    public render() {
        return (
            <>
                <CommonTableWithFilter
                    isResponsiveTable={ true }
                    uniqueContextProviderStateId="AccountDatabaseListContextProviderStateId"
                    filterProps={ {
                        getFilterContentView: (ref, onFilterChanged) => <DatabaseListFilterFormView ref={ ref } isFilterFormDisabled={ this.props.isInLoadingDataProcess } onFilterChanged={ onFilterChanged } />
                    } }
                    isBorderStyled={ true }
                    tableProps={ {
                        dataset: this.props.dataset,
                        keyFieldName: 'accountDatabaseId',
                        sorting: {
                            sortFieldName: 'creationDate',
                            sortKind: SortingKind.Desc
                        },
                        fieldsView: {
                            v82Name: {
                                caption: 'Номер',
                                fieldContentNoWrap: false,
                                fieldWidth: '10%',
                                format: (value: string) => {
                                    return (
                                        <TextButton
                                            onClick={ () => this.props.openDatabase(value) }
                                            canSelectButtonContent={ true }
                                        >
                                            <TextOut fontSize={ 13 } className="text-link">
                                                { value }
                                            </TextOut>
                                        </TextButton>
                                    );
                                }
                            },
                            caption: {
                                caption: 'Название',
                                fieldContentWrapEverySymbol: true,
                                fieldWidth: '33%'
                            },
                            accountCaption: {
                                caption: 'Аккаунт',
                                fieldContentNoWrap: false,
                                fieldWidth: '17%',
                                format: (value: string, row) => {
                                    return (
                                        <TextButton
                                            onClick={ this.showAccountCard(row.accountNumber) }
                                            canSelectButtonContent={ true }
                                            wrapLongText={ true }
                                        >
                                            <TextOut fontSize={ 13 } className="text-link">
                                                { this.trimLongAccountName(value ?? '') }
                                            </TextOut>
                                        </TextButton>
                                    );
                                }
                            },
                            creationDate: {
                                caption: 'Дата создания',
                                fieldContentNoWrap: false,
                                wrapCellHeader: true,
                                isSortable: true,
                                fieldWidth: '10%',
                                format: (value: Date) => {
                                    return DateUtility.dateToDayMonthYearHourMinute(value);
                                }
                            },
                            lastActivityDate: {
                                caption: 'Дата посл. запуска',
                                isSortable: true,
                                fieldContentNoWrap: false,
                                wrapCellHeader: true,
                                fieldWidth: '15%',
                                format: (value: Date) => {
                                    return DateUtility.dateToDayMonthYearHourMinute(value);
                                }
                            },
                            state: {
                                caption: 'Состояние',
                                fieldContentNoWrap: false,
                                fieldWidth: '5%'
                            },
                            isFile: {
                                caption: 'Тип',
                                fieldContentNoWrap: false,
                                fieldWidth: '4%',
                                format: (value: boolean) => {
                                    return value ? 'Файловая' : 'Серверная';
                                }
                            },
                            platform: {
                                caption: 'Платформа',
                                fieldContentNoWrap: false,
                                fieldWidth: '2%'
                            },
                            sizeInMB: {
                                caption: 'Размер',
                                fieldContentNoWrap: false,
                                fieldWidth: '2%'
                            },
                            lockedState: {
                                caption: 'Блокировка',
                                fieldContentNoWrap: false,
                                fieldWidth: '2%'
                            },
                            isPublish: {
                                caption: 'Опубликована',
                                fieldContentNoWrap: false,
                                fieldWidth: '2%',
                                format: value => value ? <CheckIcon /> : ''
                            }
                        },
                        pagination: this.props.pagination
                    } }
                    onDataSelect={ this.onDataSelect }
                />
                { this.props.databaseCard }
                <AccountCard
                    accountNumber={ this.state.accountNumber }
                    isOpen={ this.state.isAccountCardVisible }
                    onCancelDialogButtonClick={ this.hideAccountCard }
                />
            </>
        );
    }
}

const DatabaseListViewConnected = connect<StateProps, DispatchProps, NonNullable<unknown>, AppReduxStoreState>(
    state => {
        const databaseListState = state.DatabaseListState;
        const dataset = databaseListState.databaseList.records;

        const currentPage = databaseListState.databaseList.metadata.pageNumber;
        const totalPages = databaseListState.databaseList.metadata.pageCount;
        const { pageSize } = databaseListState.databaseList.metadata;
        const recordsCount = databaseListState.databaseList.metadata.totalItemCount;

        const isInLoadingDataProcess = ProcessIdStateHelper.isInProgress(databaseListState.reducerActions, DatabaseListProcessId.ReceiveDatabaseList);

        return {
            dataset,
            isInLoadingDataProcess,
            pagination: {
                currentPage,
                totalPages,
                pageSize,
                recordsCount
            }
        };
    },
    {
        dispatchReceiveDatabaseListThunk: ReceiveDatabaseListThunk.invoke,
        dispatchAvailabilityInfoDbThunk: AvailabilityInfoDbListThunk.invoke
    }
)(DatabaseListViewClass);

const DatabaseListViewFunction = () => {
    const { openDatabaseDialogHandler, closeDatabaseDialogHandler, dialogDatabaseNumber, isOpen } = useDatabaseCard();

    return (
        <DatabaseListViewConnected
            openDatabase={ openDatabaseDialogHandler }
            databaseCard={
                <InformationDatabaseCard
                    isOpen={ isOpen }
                    databaseNumber={ dialogDatabaseNumber }
                    onClose={ closeDatabaseDialogHandler }
                />
            }
        />
    );
};

export const DatabaseListView = withHeader({
    title: 'Информационные базы',
    page: DatabaseListViewFunction
});