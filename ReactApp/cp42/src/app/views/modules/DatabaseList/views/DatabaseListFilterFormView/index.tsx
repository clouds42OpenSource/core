import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import {
    AccountDatabaseListFilterDataForm,
    AccountDatabaseListFilterDataFormFieldNamesType
} from 'app/views/modules/DatabaseList/types';
import { SearchByFilterButton } from 'app/views/components/controls/Button';
import { TextBoxForm } from 'app/views/components/controls/forms/TextBoxForm';
import { TableFilterWrapper } from 'app/views/components/_hoc/tableFilterWrapper';
import { FilterItemWrapper } from 'app/views/components/_hoc/tableFilterWrapper/filterItemWrapper';
import React from 'react';

/**
 * Свойства для компонента фильтра
 */
type OwnProps = ReduxFormProps<AccountDatabaseListFilterDataForm> & {
    onFilterChanged: (filter: AccountDatabaseListFilterDataForm, filterFormName: string) => void;
    isFilterFormDisabled: boolean;
};

class DatabaseListFilterFormViewClass extends React.Component<OwnProps> {
    public constructor(props: OwnProps) {
        super(props);
        this.initReduxForm = this.initReduxForm.bind(this);
        this.onValueChange = this.onValueChange.bind(this);
        this.applyFilter = this.applyFilter.bind(this);
        this.handleEnterKey = this.handleEnterKey.bind(this);
    }

    public componentDidMount() {
        this.props.reduxForm.setInitializeFormDataAction(this.initReduxForm);
        document.body.addEventListener('keydown', this.handleEnterKey);
    }

    public componentWillUnmount() {
        document.body.removeEventListener('keydown', this.handleEnterKey);
    }

    /**
     * Обработать нажатие на Enter
     * @param event событие
     */
    private handleEnterKey(event: KeyboardEvent) {
        if (event.key !== 'Enter') return;

        event.preventDefault();
        this.applyFilter();
    }

    private onValueChange<TValue, TForm extends string = AccountDatabaseListFilterDataFormFieldNamesType>(formName: TForm, newValue: TValue) {
        this.props.reduxForm.updateReduxFormFields({
            [formName]: newValue
        });
    }

    private initReduxForm(): AccountDatabaseListFilterDataForm | undefined {
        return {
            accountDatabaseSearchLine: ''
        };
    }

    /**
     * Событие по нажатию кнопки поиска
     */
    private applyFilter() {
        this.props.onFilterChanged({
            ...this.props.reduxForm.getReduxFormFields(false)
        }, '');
    }

    public render() {
        const formFields = this.props.reduxForm.getReduxFormFields(false);

        return (
            <TableFilterWrapper>
                <FilterItemWrapper width="50%">
                    <TextBoxForm
                        label="Поиск"
                        formName="accountDatabaseSearchLine"
                        onValueChange={ this.onValueChange }
                        value={ formFields.accountDatabaseSearchLine }
                        isReadOnly={ this.props.isFilterFormDisabled }
                        placeholder="Введите код, название, имя аккаунта, код аккаунта, номер области..."
                    />
                </FilterItemWrapper>
                <FilterItemWrapper width="250px">
                    <SearchByFilterButton onClick={ this.applyFilter } />
                </FilterItemWrapper>
            </TableFilterWrapper>
        );
    }
}

export const DatabaseListFilterFormView = withReduxForm(DatabaseListFilterFormViewClass, {
    reduxFormName: 'DatabaseList_Filter_Form',
});