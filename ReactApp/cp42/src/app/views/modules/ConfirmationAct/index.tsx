import { FETCH_API } from 'app/api/useFetchApi';
import { AppRoutes } from 'app/AppRoutes';
import { EMessageType, useFloatMessages } from 'app/hooks';
import { useQuery } from 'app/hooks/useQuery';
import { withHeader } from 'app/views/components/_hoc/withHeader';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { useEffect } from 'react';
import { useHistory } from 'react-router';

const { putClosingDocument } = FETCH_API.CLOSING_DOCUMENTS;

const ConfirmationAct = () => {
    const history = useHistory();
    const { show } = useFloatMessages();

    const invoiceId = useQuery().get('invoiceId');
    const invoiceStatus = useQuery().get('status');

    useEffect(() => {
        (async () => {
            if (!invoiceStatus || !invoiceId) {
                history.push(AppRoutes.homeRoute);
                return;
            }

            const confirmation = await putClosingDocument(invoiceId, +invoiceStatus);

            if (confirmation.success) {
                history.push(AppRoutes.accountManagement.balanceManagement);
                show(EMessageType.success, 'Акт успешно подтвержден');
            } else {
                history.push(AppRoutes.homeRoute);
                show(EMessageType.error, confirmation.message);
            }
        })();
    }, [history, invoiceId, invoiceStatus, show]);

    return <LoadingBounce />;
};

export const ConfirmationActView = withHeader({
    title: 'Подтверждение акта',
    page: ConfirmationAct
});