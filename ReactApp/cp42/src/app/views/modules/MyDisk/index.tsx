import { Box, Button } from '@mui/material';
import { getMyDiskManagementResponseDto } from 'app/api/endpoints/myDisk/response';
import { apiHost } from 'app/api/fetch';
import { FETCH_API } from 'app/api/useFetchApi';
import { AppRoutes } from 'app/AppRoutes';
import { AccountUserGroup } from 'app/common/enums';
import { ReceiveLocalizationsDataThunk } from 'app/modules/locales/store/thunks/ReceiveLocalizationsDataThunk';
import { AppReduxStoreState } from 'app/redux/types';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { TextOut } from 'app/views/components/TextOut';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { withHeader } from 'app/views/components/_hoc/withHeader';
import { TabsControl } from 'app/views/components/controls/TabsControl';
import { MyDiskControl } from 'app/views/modules/MyDisk/tabs/MyDiskControl';
import { MyDiskTarrifs } from 'app/views/modules/MyDisk/tabs/MyDiskTarrifs';
import { LocalizationsDataModel } from 'app/web/InterlayerApiProxy/LocalesApiProxy/receiveLocalizations';
import dayjs from 'dayjs';
import 'dayjs/locale/ru';
import { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

dayjs.locale('ru');

type StateProps = {
    localizations: LocalizationsDataModel;
    currentUserGroups: AccountUserGroup[];
    isDifferentContext: boolean;
};

type DispatchProps = {
    dispatchReceiveLocalizationsThunk: () => void;
};

const MyDiskView = ({ floatMessage: { show }, dispatchReceiveLocalizationsThunk, localizations, currentUserGroups, isDifferentContext }: FloatMessageProps & StateProps & DispatchProps) => {
    const [activeTabIndex, setActiveTabIndex] = useState(0);
    const [isLoading, setIsLoading] = useState(false);
    const [myDiskManagement, setMyDiskManagement] = useState<getMyDiskManagementResponseDto | null>(null);

    const handleGetMyDiskManagement = async () => {
        setIsLoading(true);
        const response = await FETCH_API.MYDISK.getMyDiskManagement();
        setIsLoading(false);
        setMyDiskManagement(response.data);
    };

    useEffect(() => {
        if (!localizations.length) {
            dispatchReceiveLocalizationsThunk();
        }
    }, []);

    const { data, refreshData: refreshMyDiskInfo, error: myDiskError, isLoading: myDiskLoading } = FETCH_API.MYDISK.useGetMyDisk();

    useEffect(() => {
        if (myDiskLoading) {
            setIsLoading(true);
        } else if (!myDiskManagement && (currentUserGroups.includes(AccountUserGroup.CloudAdmin) || currentUserGroups.includes(AccountUserGroup.CloudSE))) {
            void handleGetMyDiskManagement();
        } else {
            setIsLoading(false);
        }
    }, [myDiskLoading]);

    useEffect(() => {
        if (myDiskError) {
            show(MessageType.Error, myDiskError);
        }
    }, [myDiskError]);

    const handleTurnRent = async () => {
        setIsLoading(true);
        const response = await FETCH_API.RENT.turnOnRent();
        if (response.success) {
            window.location.pathname = '/CreateAccountDatabase/CreateFromTemplate';
        } else {
            setIsLoading(false);
            show(MessageType.Error, response.message);
        }
    };

    const myDiskInfo = data?.rawData;

    if (isLoading && !myDiskInfo) {
        return <LoadingBounce />;
    }

    if (myDiskInfo && !myDiskInfo.serviceStatus) {
        return (
            <Box>
                <Box display="grid" gridTemplateColumns="200px 1fr" alignItems="center">
                    <img width="70px" src={ apiHost('img/services/myDisk.png') } alt="myDisk" />
                    <TextOut inDiv={ true } fontSize={ 13 } fontWeight={ 400 }>
                        Сервис Мой Диск не активен. Для активации тестового режима подключите { localizations.find(({ key }) => key === 0)?.value }
                        или <a style={ { color: '#337ab7' } } href="/CreateAccountDatabase/CreateFromTemplate">создайте информационную базу.</a>
                    </TextOut>
                </Box>
                <Button
                    variant="contained"
                    onClick={ handleTurnRent }
                    sx={ {
                        background: '#23c6c8',
                        color: '#FFFFFF',
                        minHeight: '60px',
                        minWidth: '220px',
                        width: '35%',
                        marginLeft: '20%',
                        marginTop: '6px',
                        textTransform: 'none',
                        '&:hover': {
                            background: '#23c6c8',
                        }
                    } }
                >
                    Активировать пробную версию &quot;{ localizations.find(({ key }) => key === 0)?.value }&quot;
                </Button>
            </Box>
        );
    }

    return (
        <div>
            { isLoading && <LoadingBounce /> }
            {
                !activeTabIndex && myDiskInfo?.serviceStatus.promisedPaymentIsActive &&
                <Box sx={ { color: '#31708f', background: '#d9edf7', borderRadius: '4px', border: '1px solid #bce8f1', padding: '15px', fontSize: '13px', margin: '6px 0' } }>
                    <b>Внимание </b> У Вас активирован обещанный платеж на сумму { myDiskInfo?.serviceStatus.promisedPaymentSum } { myDiskInfo.locale.currency }. Пожалуйста, погасите задолженность
                    до { dayjs(myDiskInfo?.serviceStatus.promisedPaymentExpireDate).format('D MMMM YYYY [г.]') } во избежание блокировки сервиса
                </Box>
            }
            {
                !activeTabIndex && myDiskInfo?.serviceStatus && myDiskInfo?.serviceStatus.serviceIsLocked &&
                <Box sx={ { color: '#a94442', background: '#f2dede', borderRadius: '4px', border: '1px solid #ebccd1', padding: '15px', fontSize: '13px' } }>
                    <b>Внимание </b> { myDiskInfo?.serviceStatus.serviceLockReason } Пожалуйста, <Link style={ { color: '#337ab7' } } to={ AppRoutes.accountManagement.balanceManagement }> оплатите сервис</Link>
                </Box>
            }
            {
                !activeTabIndex && (
                    <Box display="flex" alignItems="center" gap="16px">
                        <img width="70px" src={ apiHost('img/services/myDisk.png') } alt="myDisk" />
                        <TextOut inDiv={ true } fontSize={ 13 } fontWeight={ 400 }>
                            Позволяет хранить Ваши информационные базы, любые файлы и данные
                        </TextOut>
                    </Box>
                )
            }
            <TabsControl
                activeTabIndex={ activeTabIndex }
                onActiveTabIndexChanged={ setActiveTabIndex }
                headers={
                    [
                        {
                            title: 'Тарифы',
                            isVisible: true
                        },
                        {
                            title: 'Управление',
                            isVisible: (currentUserGroups.includes(AccountUserGroup.CloudAdmin) || currentUserGroups.includes(AccountUserGroup.CloudSE))
                        }
                    ]
                }
            >
                {
                    myDiskInfo &&
                    <MyDiskTarrifs
                        isDifferentContext={ isDifferentContext }
                        currentUserGroups={ currentUserGroups }
                        setIsLoading={ setIsLoading }
                        show={ show }
                        refreshMyDiskInfo={ refreshMyDiskInfo }
                        refreshMyDiskManagement={ handleGetMyDiskManagement }
                        myDiskInfo={ myDiskInfo }
                    />
                }
                {
                    myDiskInfo && myDiskManagement &&
                    <MyDiskControl
                        localizations={ localizations }
                        setIsLoading={ setIsLoading }
                        show={ show }
                        myDiskManagement={ myDiskManagement }
                        myDiskInfo={ myDiskInfo }
                    />
                }
            </TabsControl>
        </div>
    );
};

const MyDiskViewConnected = connect<StateProps, DispatchProps, {}, AppReduxStoreState>(
    state => {
        const { localizations } = state.LocalesState;
        const currentSessionSettingsState = state.Global.getCurrentSessionSettingsReducer;
        const { hasSettingsReceived } = currentSessionSettingsState.hasSuccessFor;
        const currentUserGroups = hasSettingsReceived ? state.Global.getCurrentSessionSettingsReducer.settings.currentContextInfo.currentUserGroups : [];
        const isDifferentContext = currentSessionSettingsState.settings.currentContextInfo.isDifferentContextAccount;

        return {
            localizations,
            currentUserGroups,
            isDifferentContext
        };
    },
    {
        dispatchReceiveLocalizationsThunk: ReceiveLocalizationsDataThunk.invoke,
    }
)(MyDiskView);

export const MyDiskViewPage = withHeader({
    title: 'Сервис "Мой диск"',
    page: withFloatMessages(MyDiskViewConnected)
});