import { Cached as CachedIcon, DoDisturb as DoDisturbIcon, ErrorOutline as ErrorOutlineIcon, ShoppingCart as ShoppingCartIcon } from '@mui/icons-material';
import { Box, Button, Tooltip, Typography } from '@mui/material';
import { changeTariffResponseDto, getMyDiskResponseDto, myDiskVolumeInfo } from 'app/api/endpoints/myDisk/response';
import { FETCH_API } from 'app/api/useFetchApi';
import { AppRoutes } from 'app/AppRoutes';
import { AccountUserGroup } from 'app/common/enums';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { Dialog } from 'app/views/components/controls/Dialog';
import { MyDiskPie } from 'app/views/modules/MyDisk/components/MyDiskPie';
import { MyDiskProgressBar } from 'app/views/modules/MyDisk/components/MyDiskProgressBar';
import dayjs from 'dayjs';
import 'dayjs/locale/ru';
import { ReactNode, useState } from 'react';
import { useHistory } from 'react-router';

dayjs.locale('ru');

type Props = {
    myDiskInfo: getMyDiskResponseDto;
    refreshMyDiskInfo?: () => void;
    refreshMyDiskManagement?: () => void;
    show: (type: MessageType, content: ReactNode, autoHideDuration?: number) => void;
    setIsLoading: (value: boolean) => void;
    currentUserGroups: AccountUserGroup[];
    isDifferentContext: boolean;
};

export const MyDiskTarrifs = ({ myDiskInfo, refreshMyDiskInfo, refreshMyDiskManagement, show, setIsLoading, currentUserGroups, isDifferentContext }: Props) => {
    const [totalSize, setTotalSize] = useState(myDiskInfo.myDiskVolumeInfo.availableSize / 1024);
    const [volumeInfo, setVolumeInfo] = useState<myDiskVolumeInfo>(myDiskInfo.myDiskVolumeInfo);
    const [recalculationTaskId, setRecalculationTaskId] = useState(myDiskInfo.recalculationTaskId);
    const [changedCost, setChangedCost] = useState(0);
    const [isPaymentRequired, setIsPaymentRequired] = useState(false);
    const [paymentData, setPaymentData] = useState<changeTariffResponseDto | null>(null);
    const [costForPay, setCostForPay] = useState(0);
    const history = useHistory();

    const initialTotalSize = myDiskInfo.myDiskVolumeInfo.availableSize / 1024;

    const isAllowRecalculate =
        (currentUserGroups.includes(AccountUserGroup.CloudAdmin) || currentUserGroups.includes(AccountUserGroup.Hotline) ||
        currentUserGroups.includes(AccountUserGroup.AccountSaleManager)) ||
        (!isDifferentContext && currentUserGroups.includes(AccountUserGroup.AccountAdmin));
    const isAllowChange = (currentUserGroups.includes(AccountUserGroup.CloudAdmin) || currentUserGroups.includes(AccountUserGroup.Hotline)) || (!isDifferentContext && currentUserGroups.includes(AccountUserGroup.AccountAdmin));
    const isAdmin = (currentUserGroups.includes(AccountUserGroup.CloudAdmin));

    const handleSetTotalSize = async (value: number) => {
        setIsLoading(true);
        const response = await FETCH_API.MYDISK.useChangeSize(value);
        setIsLoading(false);
        const { data } = response;
        if (data) {
            setVolumeInfo(data.myDiskVolumeInfo);
            setChangedCost(data.cost);
        }
        setTotalSize(value);
    };

    const handleChangeTariff = async () => {
        setIsLoading(true);
        const response = await FETCH_API.MYDISK.useChangeTariff(totalSize);
        const { data } = response;

        if (response.success) {
            if (data && !data.complete) {
                setPaymentData(data);

                if (data.enoughMoney) {
                    setIsPaymentRequired(true);
                } else if (data.costOftariff && !data.enoughMoney) {
                    setCostForPay(data.costOftariff);
                } else if (!data.costOftariff && !data.enoughMoney) {
                    show(MessageType.Error, data.error);
                }
            } else {
                show(MessageType.Success, 'Успешно');

                if (refreshMyDiskInfo) {
                    refreshMyDiskInfo();
                }

                if (refreshMyDiskManagement && isAdmin) {
                    refreshMyDiskManagement();
                }
            }
        }

        setIsLoading(false);
    };

    const handleStartRecalculation = async () => {
        setIsLoading(true);
        const response = await FETCH_API.MYDISK.useStartRecalculation();
        setIsLoading(false);
        const { data: taskId } = response;

        if (taskId) {
            show(MessageType.Success, 'Успешно создана задача на перерасчет диска');
            setRecalculationTaskId(taskId);
            const interval = setInterval(async () => {
                const { data: isFinished } = await FETCH_API.MYDISK.useCheckRecalculation(taskId);

                if (isFinished) {
                    setRecalculationTaskId('');
                    clearInterval(interval);
                    show(MessageType.Success, 'Перерасчет дискового пространства выполнен, диаграмма будет обновлена');

                    if (refreshMyDiskInfo) {
                        refreshMyDiskInfo();
                    }
                }
            }, 15000);
        }
    };

    const handleClearPaymentData = () => {
        setIsPaymentRequired(false);
        setPaymentData(null);
        setCostForPay(0);
    };

    const handlePayTariff = async (isPromise?: boolean) => {
        setIsLoading(true);
        const response = await FETCH_API.MYDISK.usePayTariff({ sizeGb: totalSize, byPromisedPayment: !!isPromise });
        setIsLoading(false);

        if (response.success) {
            show(MessageType.Success, 'Успешно');

            if (refreshMyDiskManagement) {
                refreshMyDiskManagement();
            }

            handleClearPaymentData();
        } else {
            show(MessageType.Error, response.message);
        }
    };

    return (
        <Box
            sx={ {
                display: 'flex',
                gap: '16px',
                flexWrap: {
                    xs: 'wrap',
                    md: 'nowrap'
                }
            } }
        >
            <Box
                sx={ {
                    flex: {
                        xs: '1',
                        sm: '0.2 1 auto'
                    }
                } }
            >
                <Box display="flex" justifyContent="space-between">
                    <Typography>
                        Объем хранилища:
                    </Typography>
                    {
                        !recalculationTaskId
                            ? (
                                <Button
                                    disabled={ !isAllowRecalculate }
                                    onClick={ handleStartRecalculation }
                                    variant="outlined"
                                    sx={ { color: '#333', border: '1px solid #333', textTransform: 'none', marginLeft: '6px' } }
                                    startIcon={ <CachedIcon /> }
                                >
                                    Обновить
                                </Button>
                            )
                            : (
                                <Tooltip title="выполняется обновление занимаемого места">
                                    <div>
                                        <Button
                                            onClick={ handleStartRecalculation }
                                            disabled={ true }
                                            variant="outlined"
                                            sx={ {
                                                color: '#333',
                                                border: '1px solid #333',
                                                textTransform: 'none',
                                                marginLeft: '6px'
                                            } }
                                            startIcon={ <CachedIcon /> }
                                        >
                                            Обновить
                                        </Button>
                                    </div>
                                </Tooltip>
                            )
                    }
                </Box>
                <MyDiskPie totalSize={ totalSize } myDiskVolumeInfo={ volumeInfo } />
                <Box display="flex" justifyContent="space-between">
                    <Box>
                        <Typography variant="h5" sx={ { color: '#f3d346' } }>
                            { (volumeInfo.usedSizeOnDisk / 1024).toFixed(1) } Гб
                        </Typography>
                        <Typography variant="caption">
                            используется
                        </Typography>
                    </Box>
                    <Box>
                        <Typography variant="h5">
                            { myDiskInfo.currentTariff } Гб
                        </Typography>
                        <Typography variant="caption">
                            Объем хранилища
                        </Typography>
                    </Box>
                </Box>
            </Box>
            {
                !myDiskInfo.accountIsVip && (
                    <Box sx={ { flex: '1' } }>
                        <Box display="flex" flexDirection="column" gap="6px">
                            <Typography variant="caption">
                                <b>Активен до:</b> { dayjs(myDiskInfo.expireDate).format('D MMMM YYYY [г.]') }
                            </Typography>
                            <Box>
                                Внимание
                            </Box>
                            <Typography variant="caption">
                                <b>Тарифный план</b> { (volumeInfo.availableSize / 1024).toFixed(0) } гб
                            </Typography>
                            <Typography variant="caption">
                                <b>Стоимость сервиса</b> { `${ changedCost || myDiskInfo.monthlyCost } ${ myDiskInfo.locale.currency }` } / месяц
                            </Typography>
                        </Box>
                        {
                            isAllowRecalculate && <MyDiskProgressBar
                                disabled={ !!costForPay || !isAllowChange }
                                totalSize={ totalSize }
                                setTotalSize={ handleSetTotalSize }
                            />
                        }
                        {
                            isAllowChange && (initialTotalSize !== totalSize) && (
                                <Box sx={ { padding: '8px' } }>
                                    {
                                        !costForPay
                                            ? (
                                                <Button
                                                    variant="contained"
                                                    onClick={ handleChangeTariff }
                                                    sx={ {
                                                        background: '#1ab394',
                                                        color: '#FFFFFF',
                                                        borderColor: '#1ab394',
                                                        textTransform: 'none',
                                                        '&:hover': {
                                                            background: '#1ab394',
                                                        }
                                                    } }
                                                >
                                                    Сохранить изменения
                                                </Button>
                                            )
                                            : (
                                                <>
                                                    <Button
                                                        variant="contained"
                                                        startIcon={ <ShoppingCartIcon /> }
                                                        onClick={ () => handlePayTariff(false) }
                                                        sx={ {
                                                            background: '#1c84c6',
                                                            color: '#FFFFFF',
                                                            borderColor: '#1c84c6',
                                                            textTransform: 'none',
                                                            marginRight: '6px',
                                                            '&:hover': {
                                                                background: '#1c84c6',
                                                            }

                                                        } }
                                                    >
                                                        Купить { `${ costForPay } ${ myDiskInfo.locale.currency }` }
                                                    </Button>
                                                    <Button
                                                        variant="contained"
                                                        startIcon={ <DoDisturbIcon /> }
                                                        onClick={ handleClearPaymentData }
                                                        sx={ {
                                                            background: 'white',
                                                            color: '#333',
                                                            borderColor: '#e7eaec',
                                                            textTransform: 'none',
                                                            '&:hover': {
                                                                background: 'white',
                                                            }
                                                        } }
                                                    >
                                                        Отменить
                                                    </Button>
                                                </>
                                            ) }
                                </Box>
                            ) }
                    </Box>
                )}
            <Dialog
                isOpen={ isPaymentRequired }
                dialogWidth="sm"
                dialogVerticalAlign="center"
                onCancelClick={ handleClearPaymentData }
                buttons={
                    [
                        {
                            content: 'Отмена',
                            kind: 'error',
                            variant: 'contained',
                            onClick: handleClearPaymentData
                        },
                        {
                            content: 'Взять обещанный платеж',
                            kind: 'success',
                            isEnabled: paymentData?.canGetPromisePayment,
                            variant: 'contained',
                            onClick: () => handlePayTariff(true)
                        },
                        {
                            content: 'Оплатить сейчас',
                            kind: 'primary',
                            variant: 'text',
                            onClick: () => history.push(`${ AppRoutes.accountManagement.balanceManagement }/replenish-balance?customPayment.amount=${ paymentData?.enoughMoney }&customPayment.description=Оплата+за+использование+сервиса+"Мой диск"+до+${ dayjs(myDiskInfo.expireDate).format('D MMMM YYYY [г.]') }`)
                        }
                    ]
                }
            >
                <Box display="flex" flexDirection="column" alignItems="center" justifyContent="center">
                    <ErrorOutlineIcon sx={ { fontSize: '88px', color: '#f8bb86' } } />
                    <Typography>
                        Уважаемый пользователь, для активации сервиса у Вас недостаточно средств в размере { paymentData?.enoughMoney } { myDiskInfo.locale.currency } { paymentData?.canGetPromisePayment ? 'Вы можете воспользоваться услугой "Обещанный платеж", Вы должны будете погасить задолженность в течение 7 дней. Или оплатить сейчас.' : '' }
                    </Typography>
                </Box>
            </Dialog>
        </Box>
    );
};