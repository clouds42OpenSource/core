import { Box, Button, Checkbox, TextField, Typography } from '@mui/material';
import { AppRoutes } from 'app/AppRoutes';
import { TextOut } from 'app/views/components/TextOut';

import { DateTimePicker, LocalizationProvider } from '@mui/x-date-pickers';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { getMyDiskManagementResponseDto, getMyDiskResponseDto } from 'app/api/endpoints/myDisk/response';
import { FETCH_API } from 'app/api/useFetchApi';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { MyDiskProgressBar } from 'app/views/modules/MyDisk/components/MyDiskProgressBar';
import { LocalizationsDataModel } from 'app/web/InterlayerApiProxy/LocalesApiProxy/receiveLocalizations/data-models';
import ruLocale from 'date-fns/locale/ru';
import { ReactNode, useEffect, useState } from 'react';
import { useHistory } from 'react-router';

type Props = {
    myDiskInfo: getMyDiskResponseDto;
    myDiskManagement: getMyDiskManagementResponseDto;
    show: (type: MessageType, content: ReactNode, autoHideDuration?: number) => void;
    setIsLoading: (value: boolean) => void;
    localizations: LocalizationsDataModel;
};

export const MyDiskControl = ({ myDiskInfo, myDiskManagement, show, setIsLoading, localizations }: Props) => {
    const [discount, setDiscount] = useState<number>(0);
    const [cost, setCost] = useState(0);
    const [totalSize, setTotalSize] = useState(0);
    const [storageCapacity, setStorageCapacity] = useState(0);
    const history = useHistory();

    useEffect(() => {
        setTotalSize(myDiskManagement.tariff);
        setCost(myDiskManagement.cost);
        setDiscount(myDiskManagement.discountGroup);
    }, [myDiskManagement]);

    useEffect(() => {
        setStorageCapacity(myDiskInfo.currentTariff);
    }, [myDiskInfo]);

    const handleSetTotalSize = async (value: number) => {
        setIsLoading(true);
        setTotalSize(value);
        const response = await FETCH_API.MYDISK.useChangeSize(value);
        setIsLoading(false);
        const { data } = response;
        if (data) {
            setCost(data.cost);
            setTotalSize(value);
        }
    };

    const handleSubmit = async () => {
        setIsLoading(true);

        const response = await FETCH_API.MYDISK.useEditMyDiskManagement({ ...myDiskManagement, tariff: totalSize, cost, discountGroup: discount });
        setIsLoading(false);
        if (response.success) {
            show(MessageType.Success, 'Успешно');
            history.push(AppRoutes.services.myDisk);
        } else {
            show(MessageType.Error, response.message);
        }
    };

    return (
        <Box sx={ { border: '1px solid #ddd', borderRadius: '4px' } }>
            <Box sx={ { background: '#f5f5f5', color: '#333', border: '1px solid #ddd', padding: '10px 15px' } }>
                Информация
            </Box>
            <Box sx={ { padding: '6px' } }>
                <Box sx={ { display: 'grid', gridTemplateColumns: '200px 1fr', alignItems: 'start', justifyItems: 'start', minWidth: '40%', gap: '6px', borderBottom: '1px solid #e7eaec', padding: '8px' } }>
                    <Typography variant="caption">
                        Оплачено до:
                    </Typography>
                    <LocalizationProvider dateAdapter={ AdapterDateFns } adapterLocale={ ruLocale }>
                        <DateTimePicker
                            sx={ {
                                '& input': {
                                    padding: '6px'
                                }
                            } }
                            value={ new Date(myDiskManagement.expireDate) }
                            disabled={ true }
                        />
                    </LocalizationProvider>
                    <TextOut style={ { color: '#ed5565', gridArea: '2/2/3/3' } }>
                        Для изменения пролонгации перейдите в сервис { localizations.find(({ key }) => key === 0)?.value }
                    </TextOut>

                </Box>
                <Box sx={ { display: 'grid', gridTemplateColumns: '200px 1fr', alignItems: 'start', justifyItems: 'start', gap: '6px', padding: '8px' } }>
                    <Typography variant="caption">
                        Блокировано:
                    </Typography>
                    <Checkbox
                        sx={ { padding: 0 } }
                        disabled={ true }
                        checked={ myDiskInfo.serviceStatus.serviceIsLocked }
                        inputProps={ { 'aria-label': 'controlled' } }
                    />
                </Box>
                <Box sx={ {
                    display: 'grid',
                    alignItems: 'start',
                    justifyItems: 'start',
                    gridTemplateColumns: '200px 1fr',
                    gap: '6px',
                    borderBottom: '1px solid #e7eaec',
                    borderTop: '1px solid #e7eaec',
                    padding: '8px'
                } }
                >
                    <Typography variant="caption">Группа скидок:</Typography>
                    <TextField
                        sx={ {
                            minWidth: '40%',
                        } }
                        value={ discount }
                        onChange={ e => setDiscount(Number(e.target.value)) }
                        type="number"
                        inputProps={ {
                            style: {
                                fontSize: '14px',
                                padding: '6px',
                                color: '#676a6c',

                            },
                        } }
                    />
                </Box>
                <Box sx={ {
                    display: 'grid',
                    alignItems: 'top',
                    gridTemplateColumns: '200px 1fr',
                    justifyItems: 'start',
                    borderBottom: '1px solid #e7eaec',
                    gap: '6px',
                    borderTop: '1px solid #e7eaec',
                    padding: '8px'
                } }
                >
                    <Typography variant="caption">Стоимость, руб.:</Typography>
                    <TextField
                        sx={ {
                            minWidth: '40%',
                        } }
                        disabled={ !myDiskInfo.accountIsVip }
                        value={ cost }
                        type="number"
                        onChange={ e => setCost(Number(e.target.value)) }
                        inputProps={ {
                            style: {
                                fontSize: '14px',
                                padding: '6px',
                                color: '#676a6c',

                            },
                        } }
                    />
                </Box>
                {
                    myDiskInfo.accountIsVip && (
                        <Box sx={ {
                            display: 'grid',
                            alignItems: 'top',
                            gridTemplateColumns: '200px 1fr',
                            justifyItems: 'start',
                            borderBottom: '1px solid #e7eaec',
                            gap: '6px',
                            borderTop: '1px solid #e7eaec',
                            padding: '8px'
                        } }
                        >
                            <Typography variant="caption">Объем хранилища, ГБ.:</Typography>
                            <TextField
                                sx={ {
                                    minWidth: '40%',
                                } }
                                disabled={ !myDiskInfo.accountIsVip }
                                value={ storageCapacity }
                                type="number"
                                onChange={ e => {
                                    setStorageCapacity(Number(e.target.value));
                                    setTimeout(() => handleSetTotalSize(Number(e.target.value)), 1000);
                                } }
                                inputProps={ {
                                    style: {
                                        fontSize: '14px',
                                        padding: '6px',
                                        color: '#676a6c',

                                    },
                                } }
                            />
                        </Box>
                    )
                }
            </Box>
            { !myDiskInfo.accountIsVip && <MyDiskProgressBar totalSize={ totalSize } setTotalSize={ handleSetTotalSize } /> }
            <Box sx={ { padding: '8px' } }>
                <Button
                    variant="contained"
                    onClick={ handleSubmit }
                    sx={ {
                        background: '#1ab394',
                        color: '#FFFFFF',
                        borderColor: '#1ab394',
                        textTransform: 'none',
                        '&:hover': {
                            background: '#1ab394',
                        }
                    } }
                >
                    Сохранить изменения
                </Button>
            </Box>
        </Box>
    );
};