import { Box, Typography } from '@mui/material';
import { myDiskVolumeInfo } from 'app/api/endpoints/myDisk/response';
import css from 'app/views/modules/MyDisk/styles.module.css';
import { ArcElement, Chart as ChartJS, Legend, Tooltip } from 'chart.js';
import ChartDataLabels from 'chartjs-plugin-datalabels';
import { useEffect, useState } from 'react';
import { Pie } from 'react-chartjs-2';

ChartJS.register(ArcElement, Tooltip, Legend, ChartDataLabels);

type Props = {
    myDiskVolumeInfo: myDiskVolumeInfo;
    totalSize: number;
};

const excludedKeys = ['availableSize', 'usedSizeOnDisk'];
const colors = ['#e6e6e6', '#dee5f1', '#dfbcb9', '#f5d7ba'];

export const MyDiskPie = ({ myDiskVolumeInfo, totalSize }: Props) => {
    const [showInfo, setShowInfo] = useState(false);
    const [values, setValues] = useState<number[]>([]);

    const getValues = () => {
        return myDiskVolumeInfo ? Object.entries(myDiskVolumeInfo)
            .filter(([key]) => !excludedKeys.includes(key))
            .map(([_, value]) => value / 1024) : [];
    };

    const getNames = () => {
        return myDiskVolumeInfo ? Object.keys(myDiskVolumeInfo).filter(key => !excludedKeys.includes(key)).map(key => {
            switch (key) {
                case 'freeSizeOnDisk':
                    return 'Свободное место';
                case 'clientFilesSize':
                    return 'Файлы';
                case 'fileDataBaseSize':
                    return 'Файловые базы';
                case 'serverDataBaseSize':
                    return 'Серверные базы';
                default:
                    return key;
            }
        }) : [];
    };

    const data = {
        labels: getNames(),
        datasets: [
            {
                data: values,
                backgroundColor: colors,
            },
        ],
    };

    useEffect(() => {
        setValues(getValues());
    }, [myDiskVolumeInfo]);

    return (
        <Box display="flex" gap="6px" flexDirection="column">
            <Box sx={ { height: '200px' } }>
                <Pie
                    onMouseEnter={ () => setShowInfo(true) }
                    onMouseLeave={ () => setShowInfo(false) }
                    data={ data }
                    options={ {
                        plugins: {
                            legend: { display: false },
                            tooltip: {
                                callbacks: {
                                    label: context => {
                                        const value = context.raw as number;
                                        return ` ${ value.toFixed(3) } Гб`;
                                    },
                                    footer: tooltip => {
                                        const value = tooltip[0].parsed;
                                        const total = totalSize >= value ? totalSize : (myDiskVolumeInfo.usedSizeOnDisk / 1024);
                                        const percentage = ((value / total) * 100).toFixed(2);
                                        return `(${ percentage }%)`;
                                    }
                                }
                            },
                            datalabels: {
                                color: '#424040',
                                font: {
                                    weight: 'bold',
                                    size: 10
                                },
                                formatter: value => {
                                    const percentage = ((value / totalSize) * 100);
                                    return percentage >= 12 ? `${ value.toFixed(1) } ГБ` : '';
                                },
                            },

                        },

                    } }
                />
            </Box>
            <Box sx={ { position: 'relative' } }>
                { showInfo && (
                    <Box className={ css.pie_info }>
                        <Typography variant="body2" sx={ { marginBottom: '6px', fontWeight: 'bold', color: 'black' } }>
                            Использовано: { (myDiskVolumeInfo.usedSizeOnDisk / 1024).toFixed(1) } Гб
                        </Typography>
                        { values.map((item, i) => {
                            return (
                                <Typography key={ i } variant="caption" sx={ { display: 'grid', gridTemplateColumns: 'auto auto 1fr', gap: '5px', justifyItems: 'end', alignItems: 'center' } }>
                                    <span style={ { background: colors[i], width: '10px', height: '10px' } } />
                                    <span>{ getNames()[i] }</span>
                                    <span>{ item.toFixed(1) } Гб</span>
                                </Typography>
                            );
                        }) }
                        <Typography variant="body2" sx={ { margin: '6px 0', fontWeight: 'bold', color: 'black' } }>
                            Всего: { (myDiskVolumeInfo.availableSize / 1024).toFixed(0) } Гб
                        </Typography>
                        <Typography variant="caption" sx={ { display: 'flex', justifyContent: 'space-between' } }>
                            <span>
                                Ваш тарифный план:
                            </span>
                            <span>
                                { (myDiskVolumeInfo.availableSize / 1024).toFixed(0) } Гб
                            </span>
                        </Typography>
                    </Box>
                ) }
            </Box>
        </Box>
    );
};