import { Box, Slider, Typography } from '@mui/material';
import { useEffect, useState } from 'react';

type Props = {
    totalSize: number;
    setTotalSize: (value: number) => void;
    disabled?: boolean;
};

export const MyDiskProgressBar = ({ totalSize, setTotalSize, disabled }: Props) => {
    const [value, setValue] = useState(totalSize);

    useEffect(() => {
        setValue(totalSize);
    }, [totalSize]);

    const getMarks = (value: number, visibleLabel = true) => {
        const marks = [];
        const step = 25;

        for (let value = 0; value <= 500; value += step) {
            if (visibleLabel) {
                marks.push({
                    value,
                    label: `${ value }`
                });
            } else {
                marks.push({
                    value
                });
            }
        }

        return marks;
    };

    function valuetext(val: number) {
        return `${ val } ГБ`;
    }

    const handleChange = (event: Event, newValue: number | number[]) => {
        if (typeof newValue === 'number') {
            setValue(newValue);
        }
    };

    const handleChangeCommited = () => {
        if (setTotalSize) {
            setTotalSize(value);
        }
    };

    return (
        <Box width="100%" display="flex" flexDirection="column" alignItems="center" padding="25px 0">
            <Box display="flex" justifyContent="space-between" width="95%">
                <Typography variant="caption" sx={ { color: '#999', background: '#e1e4e9', borderRadius: '4px' } }>
                    { value >= +25 && <span style={ { padding: '1px 5px' } }>0 ГБ</span> }
                </Typography>
                <Typography variant="caption" sx={ { color: '#999', background: '#e1e4e9', borderRadius: '4px' } }>
                    { value <= 475 && <span style={ { padding: '1px 5px' } }>500 ГБ</span> }
                </Typography>
            </Box>
            <Slider
                disabled={ disabled }
                track={ false }
                aria-labelledby="track-false-slider"
                getAriaValueText={ valuetext }
                value={ value }
                onChange={ handleChange }
                sx={ {
                    width: '95%',
                    color: '#8fbd85',
                    height: '12px',
                    padding: '3px 0',
                    '& .MuiSlider-valueLabel': {
                        background: '#8fbd85',
                        color: '#fff',
                        fontSize: '10px',
                        padding: '1px 5px'
                    },
                    '& .MuiSlider-markLabel': {
                        display: {
                            xs: 'none',
                            lg: 'block'
                        }
                    }
                } }
                min={ 10 }
                onChangeCommitted={ handleChangeCommited }
                valueLabelDisplay="on"
                valueLabelFormat={ (val: number) => `${ val } ГБ` }
                max={ 500 }
                marks={ getMarks(500) }
            />
        </Box>
    );
};