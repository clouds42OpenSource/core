import { FETCH_API } from 'app/api/useFetchApi';
import { AppRoutes } from 'app/AppRoutes';
import { EMessageType, useFloatMessages } from 'app/hooks';
import { useQuery } from 'app/hooks/useQuery';
import { withHeader } from 'app/views/components/_hoc/withHeader';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { useEffect } from 'react';
import { useHistory } from 'react-router';

const { deleteConfirmDeletion } = FETCH_API.ACCOUNTS;

const ConfirmationOfAccountDeletion = () => {
    const history = useHistory();
    const { show } = useFloatMessages();

    const code = useQuery().get('confirm-code');

    useEffect(() => {
        (async () => {
            if (!code) {
                history.push(AppRoutes.homeRoute);
                return;
            }

            const deleting = await deleteConfirmDeletion(code ?? '');

            if (deleting.success) {
                history.push(AppRoutes.signInRoute);
                show(EMessageType.success, 'Аккаунт успешно удален');
            } else {
                history.push(AppRoutes.homeRoute);
                show(EMessageType.error, deleting.message);
            }
        })();
    }, [code, history, show]);

    return <LoadingBounce />;
};

export const ConfirmationOfAccountDeletionView = withHeader({
    title: 'Подтверждение удаления аккаунта',
    page: ConfirmationOfAccountDeletion
});