import { CloudCoreProcessId } from 'app/modules/cloudCore';
import { EditDefaultSegmentParams } from 'app/modules/cloudCore/store/reducers/editDefaultSegmentReducer/params';
import { EditHoursDifferenceUkraineAndMoscowParams } from 'app/modules/cloudCore/store/reducers/editHoursDiferenceUkraineAndMoscowReducer/params';
import { EditPageNotificationParams } from 'app/modules/cloudCore/store/reducers/editPageNotificationReducer/params';
import { EditRussianLocalePaymentParams } from 'app/modules/cloudCore/store/reducers/editRussianLocalePaymentAggregatorReducer/params';
import { ReceiveDefaultConfigurationThunkParams } from 'app/modules/cloudCore/store/reducers/receiveDefaultConfigurationReducer/params';
import { EditDefaultSegmentThunk, EditHoursDifferenceUkraineAndMoscowThunk, EditPageNotificationThunk, EditRussianLocalePaymentAggregatorThunk, ReceiveDefaultConfigurationThunk } from 'app/modules/cloudCore/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { withHeader } from 'app/views/components/_hoc/withHeader';
import { IReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { RussianLocalePaymentAggregatorView } from 'app/views/modules/CloudCore/views/RussianLocalePaymentAggregatorView';
import { WatchReducerProcessActionState } from 'app/views/modules/_common/components/WatchReducerProcessActionState';
import { CloudCoreSegmentDataModel } from 'app/web/InterlayerApiProxy/CloudCoreApiProxy/receiveCloudCoreConfiguration/data-models';
import { IReducerProcessActionInfo } from 'core/redux/interfaces';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { UploadMarketBannerView } from 'app/views/modules/CloudCore/views/UploadMarketBannerView';
import { FETCH_API } from 'app/api/useFetchApi';
import { apiHost } from 'app/api';
import { Link } from '@mui/material';
import { DialogWindowCloudSettingType, RussianLocalePaymentAggregatorEnumType } from './types';
import { ConfigurationDialogsWrapperView } from './views/ConfigurationDialogsWrapperView';
import { DefaultSegmentView } from './views/DefultSegmentView';
import { HoursDeferenceUkraineAndMoscowView } from './views/HoursDiferenceUkraineAndMoscowView';
import { MainPageConfigurationView } from './views/MainPageConfigurationView';
import { PageNotificationView } from './views/PageNotificationView';

type StateProps = {
    hoursDifferenceOfUkraineAndMoscow: number;
    defaultSegment: CloudCoreSegmentDataModel;
    segments: Array<CloudCoreSegmentDataModel>;
    mainPageNotification: string;
    bannerUrl: string | null;
    russianLocalePaymentAggregator: number;
};

type DispatchProps = {
    dispatchReceiveDefaultConfigurationThunk: (args: ReceiveDefaultConfigurationThunkParams) => void;
    dispatchEditHoursDifferenceUkraineAndMoscowThunk: (args: EditHoursDifferenceUkraineAndMoscowParams) => void;
    dispatchEditDefaultSegmentThunk: (args: EditDefaultSegmentParams) => void;
    dispatchPageNotificationThunk: (args: EditPageNotificationParams) => void;
    dispatchEditRussianLocalePaymentAggregatorThunk: (args: EditRussianLocalePaymentParams) => void;
};

type AllProps = StateProps & DispatchProps & FloatMessageProps;

const { uploadMainPageBannerFile } = FETCH_API.CLOUD_CORE;

class CloudCoreClass extends Component<AllProps> {
    private dialogsComponentsRef = React.createRef<IReduxForm<{
        hoursDifferenceOfUkraineAndMoscow: string;
        segmentId: string;
        mainPageNotification: string;
        paymentAggregator: string;
        bannerFileFormData: FormData;
    }>>();

    constructor(props: AllProps) {
        super(props);

        this.onSaveEditHoursDifferenceUkraineAndMoscow = this.onSaveEditHoursDifferenceUkraineAndMoscow.bind(this);
        this.onSaveDefaultSegment = this.onSaveDefaultSegment.bind(this);
        this.onSavePageNotificationContent = this.onSavePageNotificationContent.bind(this);
        this.onSaveRussianLocalePaymentAggregator = this.onSaveRussianLocalePaymentAggregator.bind(this);
        this.onSaveMarketBannerFile = this.onSaveMarketBannerFile.bind(this);

        this.updateMainPage = this.updateMainPage.bind(this);
        this.onProcessActionStateChanged = this.onProcessActionStateChanged.bind(this);
        this.showFloatMessageOnActionState = this.showFloatMessageOnActionState.bind(this);
    }

    public componentDidMount() {
        this.updateMainPage();
    }

    public onSaveEditHoursDifferenceUkraineAndMoscow() {
        const reduxForm = this.dialogsComponentsRef.current!.getReduxForm().getReduxFormFields(false);

        if (reduxForm.hoursDifferenceOfUkraineAndMoscow === '-') {
            this.props.floatMessage.show(MessageType.Warning, 'Нельзя сохранить только минус', 3000);

            return true;
        }
        this.props.dispatchEditHoursDifferenceUkraineAndMoscowThunk({
            hours: Number(reduxForm.hoursDifferenceOfUkraineAndMoscow),
            force: true
        });

        return false;
    }

    public onSaveDefaultSegment() {
        const reduxForm = this.dialogsComponentsRef.current!.getReduxForm().getReduxFormFields(false);

        if (reduxForm.segmentId) {
            this.props.dispatchEditDefaultSegmentThunk({
                defSegmentId: reduxForm.segmentId,
                force: true
            });
        }

        return false;
    }

    public onSavePageNotificationContent() {
        const reduxForm = this.dialogsComponentsRef.current!.getReduxForm().getReduxFormFields(false);

        if (reduxForm.mainPageNotification) {
            this.props.dispatchPageNotificationThunk({
                pageNotification: reduxForm.mainPageNotification,
                force: true
            });
        }

        return false;
    }

    public onSaveRussianLocalePaymentAggregator() {
        const reduxForm = this.dialogsComponentsRef.current!.getReduxForm().getReduxFormFields(false);

        if (reduxForm.paymentAggregator) {
            this.props.dispatchEditRussianLocalePaymentAggregatorThunk({
                aggregator: Number(reduxForm.paymentAggregator),
                force: true
            });
        }

        return false;
    }

    private onSaveMarketBannerFile() {
        const reduxForm = this.dialogsComponentsRef.current!.getReduxForm().getReduxFormFields(false);

        uploadMainPageBannerFile(reduxForm.bannerFileFormData).then(({ error }) => {
            if (error) {
                this.props.floatMessage.show(MessageType.Error, `Изменения не сохранены: ${ error }`);
            } else {
                this.props.floatMessage.show(MessageType.Success, 'Изменения успешно сохранены');
                this.updateMainPage();
            }
        });

        return false;
    }

    private onProcessActionStateChanged(_: string, actionState: IReducerProcessActionInfo, error?: Error) {
        this.showFloatMessageOnActionState(actionState, error);
    }

    public updateMainPage() {
        this.props.dispatchReceiveDefaultConfigurationThunk({
            force: true
        });
    }

    private showFloatMessageOnActionState(actionState: IReducerProcessActionInfo, error?: Error) {
        if (actionState.isInSuccessState) {
            this.props.floatMessage.show(MessageType.Success, 'Изменения успешно сохранены');
            this.updateMainPage();
        }

        if (actionState.isInErrorState) {
            this.props.floatMessage.show(MessageType.Error, `Изменения не сохранены: ${ error?.message }`);
        }
    }

    public render() {
        return (
            <>
                <MainPageConfigurationView
                    configurationDialogs={
                        new Array<DialogWindowCloudSettingType>(
                            {
                                dialogComponent:
    <ConfigurationDialogsWrapperView
        buttonLinkLabel="Файл баннера главной страницы"
        body={ <UploadMarketBannerView ref={ this.dialogsComponentsRef } /> }
        titleFontSize={ 20 }
        callbackFunction={ this.onSaveMarketBannerFile }
    />,
                                defaultValue: <Link href={ apiHost((this.props.bannerUrl ?? '').replaceAll('\\', '/')) } target="_blank" rel="noreferrer">Открыть изображение</Link>
                            },
                            {
                                dialogComponent:
    <ConfigurationDialogsWrapperView
        buttonLinkLabel="Уведомления главной страницы"
        body={
            <PageNotificationView
                defaultMainPageNotification={ this.props.mainPageNotification }
                ref={ this.dialogsComponentsRef }
            />
        }
        titleFontSize={ 20 }
        callbackFunction={ this.onSavePageNotificationContent }
    />,
                                defaultValue: '...'
                            },
                            {
                                dialogComponent:
    <ConfigurationDialogsWrapperView
        buttonLinkLabel="Сегмент по умолчанию"
        body={
            <DefaultSegmentView
                ref={ this.dialogsComponentsRef }
                currentSegmentId={ this.props.defaultSegment.segmentId }
                segments={ this.props.segments }
            />
        }
        titleFontSize={ 20 }
        callbackFunction={ this.onSaveDefaultSegment }
    />,
                                defaultValue: `${ this.props.defaultSegment.defaultSegmentName }`
                            },
                            {
                                dialogComponent:
    <ConfigurationDialogsWrapperView
        buttonLinkLabel="Часовая разница Украины от Москвы"
        body={
            <HoursDeferenceUkraineAndMoscowView
                defaultHoursDifferenceOfUkraineAndMoscow={ this.props.hoursDifferenceOfUkraineAndMoscow.toString() }
                ref={ this.dialogsComponentsRef }
            />
        }
        titleFontSize={ 20 }
        callbackFunction={ this.onSaveEditHoursDifferenceUkraineAndMoscow }
    />,
                                defaultValue: `${ this.props.hoursDifferenceOfUkraineAndMoscow } час`
                            },
                            {
                                dialogComponent:
    <ConfigurationDialogsWrapperView
        buttonLinkLabel="Прием онлайн платежей для клиентов из России"
        body={
            <RussianLocalePaymentAggregatorView
                defaultPaymentAggregator={ this.props.russianLocalePaymentAggregator.toString() }
                ref={ this.dialogsComponentsRef }
            />
        }
        titleFontSize={ 20 }
        callbackFunction={ this.onSaveRussianLocalePaymentAggregator }
    />,
                                defaultValue: `${ RussianLocalePaymentAggregatorEnumType[this.props.russianLocalePaymentAggregator] }`
                            }
                        )
                    }
                />
                <WatchReducerProcessActionState
                    watch={ [{
                        reducerStateName: 'CloudCoreState',
                        processIds:
                            [
                                CloudCoreProcessId.EditHoursDifferenceUkraineAndMoscow,
                                CloudCoreProcessId.EditDefaultSegment,
                                CloudCoreProcessId.EditMainPageNotification,
                                CloudCoreProcessId.EditRussianLocalePaymentAggregator,
                                CloudCoreProcessId.EditMarketBannerFile
                            ]
                    }] }
                    onProcessActionStateChanged={ this.onProcessActionStateChanged }
                />
            </>
        );
    }
}

const CloudCoreConnected = connect<StateProps, DispatchProps, {}, AppReduxStoreState>(
    state => {
        const { CloudCoreState: {
            hoursDifferenceOfUkraineAndMoscow,
            defaultSegment,
            segments,
            mainPageNotification,
            bannerUrl,
            russianLocalePaymentAggregator
        } } = state;

        return {
            hoursDifferenceOfUkraineAndMoscow,
            defaultSegment,
            segments,
            mainPageNotification,
            russianLocalePaymentAggregator,
            bannerUrl
        };
    },
    {
        dispatchReceiveDefaultConfigurationThunk: ReceiveDefaultConfigurationThunk.invoke,
        dispatchEditHoursDifferenceUkraineAndMoscowThunk: EditHoursDifferenceUkraineAndMoscowThunk.invoke,
        dispatchEditDefaultSegmentThunk: EditDefaultSegmentThunk.invoke,
        dispatchPageNotificationThunk: EditPageNotificationThunk.invoke,
        dispatchEditRussianLocalePaymentAggregatorThunk: EditRussianLocalePaymentAggregatorThunk.invoke
    }
)(CloudCoreClass);

export const CloudCoreView = withHeader({
    title: 'Настройка облака',
    page: withFloatMessages(CloudCoreConnected)
});