import { useState } from 'react';

import { ConfigFileUploader } from 'app/views/modules/_common/components/ConfigFileUploader';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';

type TUploadMarketBannerView = ReduxFormProps<{ bannerFileFormData: FormData }>;

export const UploadMarketBannerView = withReduxForm(({ reduxForm }: TUploadMarketBannerView) => {
    const [uploadedFile, setUploadedFile] = useState<File>();

    const uploadFileHandler = (file: File) => {
        const formData = new FormData();
        formData.append('bannerImage', file);
        reduxForm.updateReduxFormFields({
            bannerFileFormData: formData
        });
        setUploadedFile(file);
    };

    const deleteFileHandler = () => {
        reduxForm.updateReduxFormFields({
            bannerFileFormData: undefined
        });
        setUploadedFile(undefined);
    };

    return (
        <ConfigFileUploader
            deleteUploadFile={ deleteFileHandler }
            limit={ 1 }
            callBack={ uploadFileHandler }
            fileTypesArr={ ['png', 'jpeg', 'jpg', 'gif'] }
            uploadedFiles={ uploadedFile ? [uploadedFile] : null }
        />
    );
}, { reduxFormName: 'UploadMarketBanner_ReduxForm' });