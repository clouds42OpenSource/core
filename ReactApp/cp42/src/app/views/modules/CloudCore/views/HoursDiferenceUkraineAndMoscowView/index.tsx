import { TextBoxForm } from 'app/views/components/controls/forms/TextBoxForm';
import React, { Component } from 'react';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';

type ownProps = {
    /**
     * Дефолтные разница часов между Украиной и Москвой
     */
    defaultHoursDifferenceOfUkraineAndMoscow: string
} &
 ReduxFormProps<{
    hoursDifferenceOfUkraineAndMoscow: string
}>;

class HoursDeferenceUkraineAndMoscowClass extends Component<ownProps> {
    public constructor(props: ownProps) {
        super(props);

        this.onValueChange = this.onValueChange.bind(this);
        this.props.reduxForm.setInitializeFormDataAction(() => ({
            hoursDifferenceOfUkraineAndMoscow: this.props.defaultHoursDifferenceOfUkraineAndMoscow
        }));
    }

    private onValueChange(formName: string, value: string) {
        let changedValue = '0';

        if (!Number.isNaN(Number(value)) || value === '-') {
            changedValue = value;
        }

        this.props.reduxForm.updateReduxFormFields({
            [formName]: changedValue
        });
    }

    public render() {
        const reduxForm = this.props.reduxForm.getReduxFormFields(false);

        return (
            <TextBoxForm
                autoFocus={ true }
                label="Например часовая разница Украины от Москвы +1 час или -1 час и тд."
                formName="hoursDifferenceOfUkraineAndMoscow"
                onValueChange={ this.onValueChange }
                value={ reduxForm.hoursDifferenceOfUkraineAndMoscow }
            />
        );
    }
}

export const HoursDeferenceUkraineAndMoscowView = withReduxForm(HoursDeferenceUkraineAndMoscowClass, { reduxFormName: 'hoursDifferenceOfUkraineAndMoscow_ReduxForm' });