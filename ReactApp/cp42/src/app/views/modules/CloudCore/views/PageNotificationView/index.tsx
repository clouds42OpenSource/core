import { RichTextEditor } from 'app/views/components/RichTextEditor';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import React, { Component } from 'react';

type PageNotificationProps = {
    /**
     * Сохраненный дефолтный контент страницы уведомления
     */
    defaultMainPageNotification: string
} & ReduxFormProps<{
    mainPageNotification: string;
}>;

class PageNotificationClass extends Component<PageNotificationProps> {
    public constructor(props: PageNotificationProps) {
        super(props);

        this.onValueChange = this.onValueChange.bind(this);
        this.props.reduxForm.setInitializeFormDataAction(() => ({
            mainPageNotification: this.props.defaultMainPageNotification
        }));
    }

    private onValueChange(formName: string, value: string) {
        this.props.reduxForm.updateReduxFormFields({
            [formName]: value
        });
    }

    public render() {
        const reduxForm = this.props.reduxForm.getReduxFormFields(false);

        return (
            <RichTextEditor
                value={ reduxForm.mainPageNotification }
                heightPercent="100%"
                language="ru"
                autoFocus={ true }
                toolBarHeight={ 200 }
                toolbarButtonList={
                    [
                        ['formatBlock'],
                        ['bold', 'underline', 'italic', 'removeFormat'],
                        ['font', 'fontSize'],
                        ['hiliteColor', 'fontColor'],
                        ['list', 'align'],
                        ['table'],
                        ['link', 'image', 'video'],
                        ['fullScreen', 'codeView']
                    ]
                }
                formName="mainPageNotification"
                onValueChange={ this.onValueChange }
            />
        );
    }
}

export const PageNotificationView = withReduxForm(PageNotificationClass, { reduxFormName: 'PageNotificationClass_redux_form' });