import { ButtonGroup } from 'app/views/components/controls/ButtonGroup';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import React, { Component } from 'react';

type OwnProps = {
    /**
     * Дефолтный тип аггрегатора
     */
    defaultPaymentAggregator: string
} & ReduxFormProps<{
    paymentAggregator: string
}>;

class RussianLocalePaymentAggregatorClass extends Component<OwnProps> {
    public constructor(props: OwnProps) {
        super(props);

        this.onClick = this.onClick.bind(this);
        this.props.reduxForm.setInitializeFormDataAction(() => ({
            paymentAggregator: this.props.defaultPaymentAggregator
        }));
    }

    private onClick(buttonIndex: number) {
        this.props.reduxForm.updateReduxFormFields({
            paymentAggregator: buttonIndex.toString()
        });
    }

    public render() {
        const reduxForm = this.props.reduxForm.getReduxFormFields(false);

        return (
            <ButtonGroup
                items={
                    [
                        {
                            index: 0,
                            text: 'Робокасса'
                        },
                        {
                            index: 1,
                            text: 'Юкаssa'
                        }
                    ]
                }
                selectedButtonIndex={ Number(reduxForm.paymentAggregator) }
                label="Прием онлайн платежей для клиентов из России произодить через:"
                onClick={ this.onClick }
            />
        );
    }
}

export const RussianLocalePaymentAggregatorView = withReduxForm(RussianLocalePaymentAggregatorClass, { reduxFormName: 'RussianLocalePaymentAggregatorView_Redux_From' });