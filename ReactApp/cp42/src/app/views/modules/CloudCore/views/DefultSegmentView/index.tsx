import { ComboBoxForm } from 'app/views/components/controls/forms/ComboBox/ComboBoxForm';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { CloudCoreSegmentDataModel } from 'app/web/InterlayerApiProxy/CloudCoreApiProxy/receiveCloudCoreConfiguration/data-models';
import { Component } from 'react';

type DefaultSegmentProps = {
    /**
     * Выбранный дефолтный сегмент
     */
    currentSegmentId: string,

    /**
     * Все сегменты
     */
    segments: Array<CloudCoreSegmentDataModel>
} & ReduxFormProps<{
    segmentId: string;
}>;

class DefaultSegmentClass extends Component<DefaultSegmentProps> {
    public constructor(props: DefaultSegmentProps) {
        super(props);

        this.onValueChange = this.onValueChange.bind(this);
        this.props.reduxForm.setInitializeFormDataAction(() => ({
            segmentId: this.props.currentSegmentId
        }));
    }

    private onValueChange(formName: string, value: string) {
        this.props.reduxForm.updateReduxFormFields({
            [formName]: value
        });
    }

    public render() {
        const reduxForm = this.props.reduxForm.getReduxFormFields(false);
        const { segments } = this.props;

        return (
            <ComboBoxForm
                formName="segmentId"
                label=""
                value={ reduxForm.segmentId }
                items={ segments.map(item => {
                    return {
                        value: item.segmentId,
                        text: item.defaultSegmentName
                    };
                }) }
                onValueChange={ this.onValueChange }
            />
        );
    }
}

export const DefaultSegmentView = withReduxForm(DefaultSegmentClass, { reduxFormName: 'DefaultSegmentClass_ReduxForm' });