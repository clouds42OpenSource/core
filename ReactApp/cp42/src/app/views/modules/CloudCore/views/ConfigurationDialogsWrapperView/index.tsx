import { TextFontSize } from 'app/views/components/TextOut/views/TextView';
import { Dialog } from 'app/views/components/controls/Dialog';
import { useState } from 'react';
import css from '../styles.module.css';

export type ConfigurationDialogsWrapperProps = {
    /**
     * Название ссылки и заголовка диалогового окна
     */
    buttonLinkLabel: string,

    /**
     * Тело диалогового окна
     */
    body: {},

    /**
     * Размер шрифта заголовка диалогового окна
     */
    titleFontSize: TextFontSize,

    /**
     * Call Back функция на нажатие сохранить
     */
    callbackFunction: () => boolean
};

export const ConfigurationDialogsWrapperView = (props: ConfigurationDialogsWrapperProps) => {
    const {
        buttonLinkLabel,
        body,
        titleFontSize,
        callbackFunction
    } = props;

    const [modal, setModal] = useState(false);
    const toggle = () => setModal(!modal);
    const callBack = () => {
        setModal(callbackFunction());
    };

    return (
        <>
            <span className={ css['custom-a'] } onClick={ toggle }>{ buttonLinkLabel }</span>
            <Dialog
                className={ css['custom-modal'] }
                title={ `Настройка облака: ${ buttonLinkLabel }` }
                isOpen={ modal }
                dialogWidth="sm"
                isTitleSmall={ true }
                titleFontSize={ titleFontSize }
                dialogVerticalAlign="top"
                onCancelClick={ toggle }
                buttons={ [{
                    kind: 'success',
                    content: 'Сохранить',
                    fontSize: 13,
                    onClick: callBack
                }] }
            >
                { body }
            </Dialog>
        </>
    );
};