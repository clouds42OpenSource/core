import { Table } from 'app/views/components/controls/Table';
import { ConfigurationDialogsWrapperView } from 'app/views/modules/CloudCore/views/ConfigurationDialogsWrapperView';
import { DialogWindowCloudSettingType } from '../../types';
import css from '../styles.module.css';

type OwnProps = {
    /**
     * Все модальные окошки для Настройки облака
     */
    configurationDialogs: Array<DialogWindowCloudSettingType>;
};

export const MainPageConfigurationView = (props: OwnProps) => {
    const dialogs = props.configurationDialogs;

    const checkValidationOfConfigurationDialogsWrapperViewArray = () => {
        dialogs.forEach(item => {
            if (item.dialogComponent.type !== ConfigurationDialogsWrapperView) {
                throw new Error('Для MainPageConfigurationView свойство configurationDialogs может принимать только массив элементов типа ConfigurationDialogsWrapperView');
            }
        });
    };

    checkValidationOfConfigurationDialogsWrapperViewArray();

    return (
        <div className={ css['custom-table-wrapper'] }>
            <Table
                dataset={ dialogs }
                keyFieldName="defaultValue"
                fieldsView={ {
                    dialogComponent: {
                        caption: 'Настройка'
                    },
                    defaultValue: {
                        caption: 'Значение настройки'
                    }
                } }
            />
        </div>
    );
};