declare const styles: {
    readonly 'custom-a': string;
    readonly 'custom-modal': string;
    readonly 'custom-table-wrapper': string;
};
export = styles;