import React from 'react';
import { ConfigurationDialogsWrapperProps, ConfigurationDialogsWrapperView } from '../views/ConfigurationDialogsWrapperView';

/**
 * Обьект для диалоговых окон настройки облака
 */
export type DialogWindowCloudSettingType =
{
    /**
     * Обьект компонента под пунктов для настройки в таблице на главной странице
     */
    dialogComponent: React.ReactComponentElement<typeof ConfigurationDialogsWrapperView, ConfigurationDialogsWrapperProps>,

    /**
     * Строка для вывода значения дефолтных данных в таблице на главной странице
     */
    defaultValue: React.ReactNode;
};