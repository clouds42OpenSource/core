import { useParams } from 'react-router';
import { LocationProps } from 'app/views/modules/ManagingServiceSubscriptions/types';
import { FETCH_API } from 'app/api/useFetchApi';
import { PageHeaderView } from 'app/views/components/_hoc/withHeader/PageHeaderView';
import { useEffect, useState } from 'react';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { GetServicesMyDatabases } from 'app/api/endpoints/managingServiceSubscriptions/response';
import { ServiceHeader } from 'app/views/modules/ManagingServicesMyDatabaseSubscription/views/ServiceHeader';
import { ServicesBillingTableView } from 'app/views/modules/ManagingServicesMyDatabaseSubscription/views/ServicesBillingTableView';
import { Box } from '@mui/system';
import { PeriodInfo } from 'app/views/modules/ManagingServicesMyDatabaseSubscription/views/PeriodInfo';
import { ServicesOptions } from 'app/views/modules/ManagingServicesMyDatabaseSubscription/views/ServicesOptions';

const { getMyDatabasesServices } = FETCH_API.MANAGING_SERVICE_SUBSCRIPTIONS;

export const ManagingServicesMyDatabaseSubscriptionView = withFloatMessages(({ floatMessage: { show } }) => {
    const { id } = useParams<LocationProps>();
    const [servicesInfo, setServicesInfo] = useState<GetServicesMyDatabases | null>(null);

    useEffect(() => {
        getMyDatabasesServices(id)
            .then(response => {
                if (response.success) {
                    setServicesInfo(response.data);
                } else {
                    show(MessageType.Error, response.message);
                }
            });
    }, [id, show]);

    const getServices = () => {
        getMyDatabasesServices(id)
            .then(response => {
                if (response.success) {
                    setServicesInfo(response.data);
                } else {
                    show(MessageType.Error, response.message);
                }
            });
    };

    return (
        <>
            {
                servicesInfo && (
                    <Box display="flex" flexDirection="column" gap="16px">
                        <PageHeaderView title={ servicesInfo.displayedName } />
                        <ServiceHeader
                            serviceType={ servicesInfo.systemServiceType }
                        />
                        <ServicesBillingTableView
                            servicesInfo={ servicesInfo }
                        />
                        <PeriodInfo
                            serviceInfo={ servicesInfo }
                        />
                        <ServicesOptions
                            id={ id }
                            serviceInfo={ servicesInfo }
                            getServicesInfo={ getServices }
                        />
                    </Box>
                )
            }
        </>
    );
});