import { Box, Checkbox, TextField } from '@mui/material';
import dayjs from 'dayjs';
import React, { useState } from 'react';
import { NewCostsRequest } from 'app/api/endpoints/managingServiceSubscriptions/request';
import { FETCH_API } from 'app/api/useFetchApi';
import { localStorageHelper } from 'app/common/helpers/LocalStorageHelper';
import { TextOut } from 'app/views/components/TextOut';
import { SuccessButton } from 'app/views/components/controls/Button';
import { MuiDateInputForm } from 'app/views/components/controls/forms/MuiDateInputForm';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import css from './style.module.css';
import { ManagingProps } from './types';

const { putServicesManagement } = FETCH_API.MANAGING_SERVICE_SUBSCRIPTIONS;

const currentDate = new Date();

export const Managing = withFloatMessages(({ id, billingServiceId, serviceIsLocked, serviceExpireDate, serviceIsServer, serviceName, serviceCost, getServicesInfo, floatMessage: { show } }: ManagingProps & FloatMessageProps) => {
    const [servicesCost, setServicesCost] = useState<NewCostsRequest>({
        key: billingServiceId,
        value: serviceCost
    });
    const [expireDate, setExpireDate] = useState(new Date(serviceExpireDate));
    const [isLoading, setIsLoading] = useState(false);
    const [isNeedRefresh, setIsNeedRefresh] = useState(false);

    const dateHandleChange = (_: string, e: Date | null) => {
        setExpireDate(e ?? currentDate);
    };

    const costHandleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setServicesCost({
            key: id,
            value: event.target.value !== '' ? parseInt(event.target.value, 10) : 0
        });
    };

    const handleSaveChange = async () => {
        setIsLoading(true);
        const servicesCostObject: { [key: string]: number } = {};

        servicesCostObject[servicesCost.key] = servicesCost.value;

        const response = await putServicesManagement({
            accountId: localStorageHelper.getContextAccountId() ?? '',
            billingServiceId,
            newCosts: servicesCostObject,
            newExpireDate: dayjs(expireDate).format('YYYY-MM-DDTHH:mm:ss')
        });

        if (response && !response.success) {
            show(MessageType.Error, response.message);
        } else {
            show(MessageType.Success, 'Изменения успешно сохранены');
            setIsNeedRefresh(true);
        }

        setIsLoading(false);
    };

    if (isNeedRefresh) {
        getServicesInfo();
        setIsNeedRefresh(false);
    }

    return (
        <>
            { isLoading && <LoadingBounce /> }
            <Box display="flex" flexDirection="column" justifyContent="space-between" gap="16px">
                <Box display="flex" flexDirection="column" gap="16px">
                    <Box display="flex" alignItems="center">
                        <TextOut className={ css.inputWidth }>
                            { serviceIsServer ? 'Стоимость 1 лицензии на доступ в серверную базу' : `Стоимость 1 лицензии на доступ в конфигурацию "${ serviceName }"` }:
                        </TextOut>
                        <TextField value={ servicesCost?.value ?? 0 } className={ css.inputWidth } onChange={ costHandleChange } />
                    </Box>
                </Box>
                <Box display="flex" alignItems="center">
                    <TextOut className={ css.inputWidth }>Оплачено до:</TextOut>
                    <div className={ css.inputWidth }>
                        <MuiDateInputForm
                            className={ css.datePecker }
                            label="Дата"
                            formName="Date"
                            value={ expireDate }
                            onValueChange={ dateHandleChange }
                            includeTime={ true }
                            isReadOnly={ true }
                        />
                    </div>
                </Box>
                <Box display="flex" alignItems="center">
                    <TextOut className={ css.inputWidth }>Блокировано:</TextOut>
                    <Checkbox checked={ serviceIsLocked } disabled={ true } className={ css.checkBox } />
                </Box>
                <SuccessButton onClick={ handleSaveChange } className={ css.buttonRight }>
                    Сохранить изменения
                </SuccessButton>
            </Box>
        </>
    );
});