export type ManagingProps = {
    id: string;
    billingServiceId: string;
    serviceExpireDate: Date;
    serviceIsLocked: boolean;
    serviceIsServer: boolean;
    serviceName: string;
    serviceCost: number;
    getServicesInfo: () => void;
};