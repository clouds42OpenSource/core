import { ServiceHeaderProps } from 'app/views/modules/ManagingServicesMyDatabaseSubscription/views/ServiceHeader/types';
import { SystemServiceType } from 'app/api/endpoints/managingServiceSubscriptions/response';
import { TextOut } from 'app/views/components/TextOut';
import { Box } from '@mui/system';

export const ServiceHeader = ({ serviceType }: ServiceHeaderProps) => {
    return (
        <Box display="flex" alignItems="center" gap="16px">
            <img src="/img/my-database-service/my-database-service-icon.png" alt="Логотип сервиса" />
            {
                serviceType === SystemServiceType.AccessToServerDatabase &&
                <TextOut fontSize={ 14 }>Клиент-серверный режим работы с 1С позволит улучшить производительность и быстродействие для баз с объемом свыше 5 Гб или при подключении к базе 5 и более пользователей</TextOut>
            }
        </Box>
    );
};