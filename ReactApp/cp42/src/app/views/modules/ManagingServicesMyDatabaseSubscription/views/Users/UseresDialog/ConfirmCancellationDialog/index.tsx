import { TextOut } from 'app/views/components/TextOut';
import { Dialog } from 'app/views/components/controls/Dialog';
import { ConfirmCancellationDialogProps } from 'app/views/modules/ManagingServicesMyDatabaseSubscription/views/Users/UseresDialog/ConfirmCancellationDialog/types';
import React from 'react';

export const ConfirmCancellationDialog = ({ isOpen, onCancelClick, onApplyClick }: ConfirmCancellationDialogProps) => {
    return (
        <Dialog
            title="Подтвердите отмену"
            isTitleSmall={ true }
            isOpen={ isOpen }
            dialogWidth="xs"
            dialogVerticalAlign="center"
            onCancelClick={ onCancelClick }
            buttons={
                [
                    {
                        content: 'Да',
                        kind: 'default',
                        onClick: onApplyClick,
                        variant: 'text'
                    },
                    {
                        content: 'Нет',
                        kind: 'warning',
                        onClick: onCancelClick
                    }
                ]
            }
        >
            <TextOut>Вы уверены что хотите сбросить все изменения?</TextOut>
        </Dialog>
    );
};