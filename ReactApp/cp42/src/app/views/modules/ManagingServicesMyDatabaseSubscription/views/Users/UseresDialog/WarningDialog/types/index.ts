export type WarningDialogProps = {
    isOpen: boolean;
    errorMessage: string;
    onCancelClick: () => void;
};