import { GetServicesMyDatabases } from 'app/api/endpoints/managingServiceSubscriptions/response';

export type PeriodInfoProps = {
    serviceInfo: GetServicesMyDatabases;
};