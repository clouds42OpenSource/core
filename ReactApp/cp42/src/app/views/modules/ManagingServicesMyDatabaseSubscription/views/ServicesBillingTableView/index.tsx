import { SystemServiceType } from 'app/api/endpoints/managingServiceSubscriptions/response';
import { TextOut } from 'app/views/components/TextOut';
import { TableCellProps } from 'app/views/components/controls/Table/types';
import { FormatTextOut } from 'app/views/modules/ManagingServicesMyDatabaseSubscription/views/FormatTextOut';
import { BillingInfoTable, ServicesBillingTableViewProps } from 'app/views/modules/ManagingServicesMyDatabaseSubscription/views/ServicesBillingTableView/types';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { useCallback, useEffect, useState } from 'react';

export const ServicesBillingTableView = ({ servicesInfo }: ServicesBillingTableViewProps) => {
    const [dataset, setDataset] = useState<BillingInfoTable[]>([]);

    useEffect(() => {
        const billingInfo: BillingInfoTable[] = [];

        billingInfo.push({
            description: servicesInfo.systemServiceType === SystemServiceType.AccessToServerDatabase
                ? 'Лицензии на доступ в серверные базы'
                : `Лицензии на доступ в конфигурация "${ servicesInfo.displayedName }"`,
            count: servicesInfo.usedLicenses,
            i: servicesInfo.iamSponsorLicenses,
            me: servicesInfo.meSponsorLicenses,
            price: servicesInfo.cost,
            totalPrice: servicesInfo.amount
        });

        if (servicesInfo.systemServiceType === SystemServiceType.AccessToServerDatabase) {
            billingInfo.push({
                description: 'Размещено серверных баз',
                count: servicesInfo.databasesCount,
                price: servicesInfo.costPerOneDatabase,
                totalPrice: servicesInfo.totalDatabasesCost
            });
        }

        billingInfo.push({
            description: 'Итого в рамках текущего периода',
            totalPrice: billingInfo.reduce((prev, curr) => prev + curr.totalPrice, 0)
        });

        setDataset(billingInfo);
    }, [servicesInfo]);

    const { iamSponsorLicenses } = servicesInfo;
    const { meSponsorLicenses } = servicesInfo;

    const getFieldsViews = useCallback((): { [fieldName in keyof BillingInfoTable]?: TableCellProps<BillingInfoTable> } => {
        if (iamSponsorLicenses > 0 || meSponsorLicenses > 0) {
            if (iamSponsorLicenses > 0 && meSponsorLicenses > 0) {
                return {
                    description: {
                        caption: 'Описание',
                        format: value => {
                            return value === 'Итого в рамках текущего периода'
                                ? <TextOut fontWeight={ 700 }>{ value }</TextOut>
                                : <TextOut>{ value }</TextOut>;
                        }
                    },
                    count: {
                        caption: 'Количество',
                        format: value => {
                            return <FormatTextOut value={ value } />;
                        }
                    },
                    i: {
                        caption: 'Я спонсирую',
                        format: value => {
                            return <FormatTextOut value={ value } />;
                        }
                    },
                    me: {
                        caption: 'Меня спонсируют',
                        format: value => {
                            return <FormatTextOut value={ value } />;
                        }
                    },
                    price: {
                        caption: `Стоимость, ${ servicesInfo.currencyCode }/мес`,
                        format: value => {
                            return <FormatTextOut value={ value } />;
                        }
                    },
                    totalPrice: {
                        caption: `Всего, ${ servicesInfo.currencyCode }/мес`,
                        format: value => {
                            return <FormatTextOut value={ value } />;
                        }
                    }
                };
            }
            if (iamSponsorLicenses > 0) {
                return {
                    description: {
                        caption: 'Описание',
                        format: value => {
                            return value === 'Итого в рамках текущего периода'
                                ? <TextOut fontWeight={ 700 }>{ value }</TextOut>
                                : <TextOut>{ value }</TextOut>;
                        }
                    },
                    count: {
                        caption: 'Количество',
                        format: value => {
                            return <FormatTextOut value={ value } />;
                        }
                    },
                    i: {
                        caption: 'Я спонсирую',
                        format: value => {
                            return <FormatTextOut value={ value } />;
                        }
                    },
                    price: {
                        caption: `Стоимость, ${ servicesInfo.currencyCode }/мес`,
                        format: value => {
                            return <FormatTextOut value={ value } />;
                        }
                    },
                    totalPrice: {
                        caption: `Всего, ${ servicesInfo.currencyCode }/мес`,
                        format: value => {
                            return <FormatTextOut value={ value } />;
                        }
                    }
                };
            }
            return {
                description: {
                    caption: 'Описание',
                    format: value => {
                        return value === 'Итого в рамках текущего периода'
                            ? <TextOut fontWeight={ 700 }>{ value }</TextOut>
                            : <TextOut>{ value }</TextOut>;
                    }
                },
                count: {
                    caption: 'Количество',
                    format: value => {
                        return <FormatTextOut value={ value } />;
                    }
                },
                me: {
                    caption: 'Меня спонсируют',
                    format: value => {
                        return <FormatTextOut value={ value } />;
                    }
                },
                price: {
                    caption: `Стоимость, ${ servicesInfo.currencyCode }/мес`,
                    format: value => {
                        return <FormatTextOut value={ value } />;
                    }
                },
                totalPrice: {
                    caption: `Всего, ${ servicesInfo.currencyCode }/мес`,
                    format: value => {
                        return <FormatTextOut value={ value } />;
                    }
                }
            };
        }
        return {
            description: {
                caption: 'Описание',
                format: value => {
                    return value === 'Итого в рамках текущего периода'
                        ? <TextOut fontWeight={ 700 }>{ value }</TextOut>
                        : <TextOut>{ value }</TextOut>;
                }
            },
            count: {
                caption: 'Количество',
                format: value => {
                    return <FormatTextOut value={ value } />;
                }
            },
            price: {
                caption: `Стоимость, ${ servicesInfo.currencyCode }/мес`,
                format: value => {
                    return <FormatTextOut value={ value } />;
                }
            },
            totalPrice: {
                caption: `Всего, ${ servicesInfo.currencyCode }/мес`,
                format: value => {
                    return <FormatTextOut value={ value } />;
                }
            }
        };
    }, [iamSponsorLicenses, meSponsorLicenses, servicesInfo.currencyCode]);

    return (<CommonTableWithFilter
        uniqueContextProviderStateId="ServicesBillingTable"
        isResponsiveTable={ true }
        tableProps={ {
            dataset,
            keyFieldName: 'description',
            fieldsView: getFieldsViews(),
        } }
    />);
};