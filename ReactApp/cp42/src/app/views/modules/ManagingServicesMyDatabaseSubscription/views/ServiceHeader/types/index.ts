import { SystemServiceType } from 'app/api/endpoints/managingServiceSubscriptions/response';

export type ServiceHeaderProps = {
    serviceType?: SystemServiceType;
};