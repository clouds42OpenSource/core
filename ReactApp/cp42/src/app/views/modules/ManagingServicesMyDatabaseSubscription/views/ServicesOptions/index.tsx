import { TabsControl } from 'app/views/components/controls/TabsControl';
import { AccountUserGroup } from 'app/common/enums';
import { useAppSelector } from 'app/hooks';
import { Users } from 'app/views/modules/ManagingServicesMyDatabaseSubscription/views/Users';
import { Managing } from 'app/views/modules/ManagingServicesMyDatabaseSubscription/views/Managing';
import { ServicesOptionsProps } from 'app/views/modules/ManagingServicesMyDatabaseSubscription/views/ServicesOptions/types';
import { SystemServiceType } from 'app/api/endpoints/managingServiceSubscriptions/response';
import React from 'react';

export const ServicesOptions = ({ id, serviceInfo, getServicesInfo }: ServicesOptionsProps) => {
    const { settings: { currentContextInfo: { currentUserGroups } } } = useAppSelector(state => state.Global.getCurrentSessionSettingsReducer);
    const isCloudAdmin = currentUserGroups.includes(AccountUserGroup.CloudAdmin);

    const renderHeaders = [
        {
            title: 'Пользователи',
            isVisible: true,
        },
        {
            title: 'Управление',
            isVisible: isCloudAdmin,
        },
    ].filter(item => item.isVisible);

    return (
        <TabsControl headers={ renderHeaders }>
            <Users
                id={ id }
                billingServiceId={ serviceInfo.billingServiceId }
                billingTable={ [serviceInfo] }
                isDemo={ false }
            />
            { isCloudAdmin &&
                <Managing
                    id={ id }
                    billingServiceId={ serviceInfo.billingServiceId }
                    serviceExpireDate={ serviceInfo.displayedExpireDate }
                    serviceIsLocked={ serviceInfo.serviceTypeStatus.serviceIsLocked }
                    serviceIsServer={ serviceInfo.systemServiceType === SystemServiceType.AccessToServerDatabase }
                    serviceName={ serviceInfo.displayedName }
                    serviceCost={ serviceInfo.cost }
                    getServicesInfo={ getServicesInfo }
                />
            }
        </TabsControl>
    );
};