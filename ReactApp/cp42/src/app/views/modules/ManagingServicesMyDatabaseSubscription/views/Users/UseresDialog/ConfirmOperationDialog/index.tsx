import { TextOut } from 'app/views/components/TextOut';
import { Dialog } from 'app/views/components/controls/Dialog';
import { ConfirmOperationDialogProps } from 'app/views/modules/ManagingServicesMyDatabaseSubscription/views/Users/UseresDialog/ConfirmOperationDialog/types';
import React from 'react';

export const ConfirmOperationDialog = ({ isOpen, precalculateCorrectStatus, applyOrBuy, onCancelClick, onApplyClick }: ConfirmOperationDialogProps) => {
    return (
        <Dialog
            title="Подтвердите операцию"
            isTitleSmall={ true }
            isOpen={ isOpen }
            dialogWidth="xs"
            dialogVerticalAlign="center"
            onCancelClick={ onCancelClick }
            buttons={
                [
                    {
                        content: 'Отмена',
                        kind: 'default',
                        onClick: onCancelClick,
                        variant: 'text'
                    },
                    {
                        content: 'Продолжить',
                        kind: 'success',
                        onClick: () => onApplyClick()
                    }
                ]
            }
        >
            { precalculateCorrectStatus &&
            <TextOut>Внимание! При отключении всех пользователей сервис будет удален из ВСЕХ информационных баз! </TextOut>
            }
            {
                applyOrBuy
                    ? <TextOut>Нажмите «Продолжить», чтобы совершить покупку на выбранные Вами услуги.</TextOut>
                    : <TextOut>Нажмите «Продолжить», что бы применить изменения.</TextOut>
            }
        </Dialog>
    );
};