import { TextOut } from 'app/views/components/TextOut';
import { FormatTextOutProps } from 'app/views/modules/ManagingServicesMyDatabaseSubscription/views/FormatTextOut/types';

export const FormatTextOut = ({ value }: FormatTextOutProps) => {
    return <TextOut textAlign="center" style={ { marginLeft: '40px' } }>{ value }</TextOut>;
};