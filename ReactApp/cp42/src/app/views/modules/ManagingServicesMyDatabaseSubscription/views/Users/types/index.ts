import { GetServicesMyDatabases } from 'app/api/endpoints/managingServiceSubscriptions/response';
import { SortingKind } from 'app/common/enums';

export type Props = {
    id: string;
    billingServiceId: string;
    billingTable: GetServicesMyDatabases[];
    isDemo: boolean;
};

export type SortingSwitch = {
    id: string;
    sortKind?: SortingKind;
};