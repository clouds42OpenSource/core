export type ConfirmCancellationDialogProps = {
    isOpen: boolean;
    onCancelClick: () => void;
    onApplyClick: () => void;
};