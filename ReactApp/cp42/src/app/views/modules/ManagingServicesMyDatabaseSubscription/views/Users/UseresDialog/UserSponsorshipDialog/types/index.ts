import { ChangeEvent } from 'react';

export type UserSponsorshipDialogProps = {
    isOpen: boolean;
    sponsorship: string;
    onCancelClick: () => void;
    onApplyClick: () => void;
    onValueChange: (event: ChangeEvent<HTMLInputElement>) => void;
};