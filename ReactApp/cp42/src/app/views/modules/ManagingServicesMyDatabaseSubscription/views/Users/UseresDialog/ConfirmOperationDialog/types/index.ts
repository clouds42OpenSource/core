export type ConfirmOperationDialogProps = {
    isOpen: boolean;
    precalculateCorrectStatus: boolean;
    applyOrBuy: boolean;
    onCancelClick: () => void;
    onApplyClick: () => void;
};