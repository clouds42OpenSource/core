import { GetServicesMyDatabases } from 'app/api/endpoints/managingServiceSubscriptions/response';

export type ServicesOptionsProps = {
    id: string;
    serviceInfo: GetServicesMyDatabases;
    getServicesInfo: () => void;
};