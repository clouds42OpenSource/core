import { Box, TextField } from '@mui/material';
import { TextOut } from 'app/views/components/TextOut';
import React from 'react';
import { Dialog } from 'app/views/components/controls/Dialog';
import { UserSponsorshipDialogProps } from 'app/views/modules/ManagingServicesMyDatabaseSubscription/views/Users/UseresDialog/UserSponsorshipDialog/types';

export const UserSponsorshipDialog = ({ isOpen, sponsorship, onCancelClick, onApplyClick, onValueChange }: UserSponsorshipDialogProps) => {
    return (
        <Dialog
            title="Спонсирование пользователя"
            isTitleSmall={ true }
            isOpen={ isOpen }
            dialogWidth="xs"
            dialogVerticalAlign="center"
            onCancelClick={ onCancelClick }
            buttons={
                [
                    {
                        content: 'Отмена',
                        kind: 'default',
                        onClick: onCancelClick,
                        variant: 'text'
                    },
                    {
                        content: 'Спонсировать',
                        kind: 'primary',
                        onClick: onApplyClick
                    }
                ]
            }
        >
            <Box display="flex" flexDirection="column" gap="16px" alignItems="center">
                <TextOut>Вы можете проспонсировать подключение к сервису для пользователя внешнего аккаунта</TextOut>
                <TextField
                    InputLabelProps={ { shrink: true } }
                    label="Введите e-mail пользователя"
                    size="small"
                    value={ sponsorship }
                    fullWidth={ true }
                    onChange={ onValueChange }
                />
            </Box>
        </Dialog>
    );
};