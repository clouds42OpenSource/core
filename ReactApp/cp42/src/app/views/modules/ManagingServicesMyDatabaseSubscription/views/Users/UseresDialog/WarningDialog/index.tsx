import { AppRoutes } from 'app/AppRoutes';
import { TextOut } from 'app/views/components/TextOut';
import { Dialog } from 'app/views/components/controls/Dialog';
import React from 'react';
import { useHistory } from 'react-router';
import { WarningDialogProps } from 'app/views/modules/ManagingServicesMyDatabaseSubscription/views/Users/UseresDialog/WarningDialog/types';

export const WarningDialog = ({ isOpen, errorMessage, onCancelClick }: WarningDialogProps) => {
    const history = useHistory();

    return (
        <Dialog
            title="Внимание"
            isTitleSmall={ true }
            isOpen={ isOpen }
            dialogWidth="xs"
            dialogVerticalAlign="center"
            onCancelClick={ onCancelClick }
            buttons={
                [
                    {
                        content: 'Закрыть',
                        kind: 'default',
                        onClick: onCancelClick,
                        variant: 'text'
                    },
                    {
                        content: 'Подключить Аренду 1С',
                        kind: 'primary',
                        onClick: () => history.push(AppRoutes.services.rentReference)
                    }
                ]
            }
        >
            <TextOut>
                <div dangerouslySetInnerHTML={ { __html: errorMessage } } />
            </TextOut>
        </Dialog>
    );
};