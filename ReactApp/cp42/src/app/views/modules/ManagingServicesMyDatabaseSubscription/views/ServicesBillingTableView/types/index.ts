import { GetServicesMyDatabases } from 'app/api/endpoints/managingServiceSubscriptions/response';

export type ServicesBillingTableViewProps = {
    servicesInfo: GetServicesMyDatabases;
};

export type BillingInfoTable = {
    description: string;
    count?: number;
    i?: number;
    me?: number;
    price?: number;
    totalPrice: number;
};