import { PeriodInfoProps } from 'app/views/modules/ManagingServicesMyDatabaseSubscription/views/PeriodInfo/types';
import { DateUtility } from 'app/utils';
import { Paper } from '@mui/material';
import { TextOut } from 'app/views/components/TextOut';

const paperStyle = {
    padding: '9.5px',
    fontSize: '14px',
    lineHeight: '1',
    boxShadow: 'none',
    border: '1px solid',
    flex: 1,
} as const;

export const PeriodInfo = ({ serviceInfo }: PeriodInfoProps) => {
    return (
        <Paper
            sx={ {
                ...paperStyle,
                color: '#31708f',
                backgroundColor: '#d9edf7',
                borderColor: '#bce8f1',
            } }
        >
            {
                serviceInfo.serviceTypeStatus.promisedPaymentIsActive
                    ? (
                        <>
                            <TextOut fontWeight={ 700 } style={ { color: 'inherit' } }>Внимание! </TextOut>
                            <TextOut style={ { color: 'inherit' } }>
                                У Вас активирован обещанный платеж на сумму { serviceInfo.serviceTypeStatus.promisedPaymentSum } { serviceInfo.currencyCode }.
                                Пожалуйста, погасите задолженность до { DateUtility.getDateWithDayMonthNameYear(serviceInfo.serviceTypeStatus.promisedPaymentExpireDate) } во избежание блокировки доступов
                            </TextOut>
                        </>
                    )
                    : <TextOut style={ { color: 'inherit' } }>Использование сервиса возможно до { DateUtility.getDateWithDayMonthNameYear(serviceInfo.displayedExpireDate) }</TextOut>
            }
        </Paper>
    );
};