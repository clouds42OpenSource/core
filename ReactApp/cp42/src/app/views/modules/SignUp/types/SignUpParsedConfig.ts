import { RegistrationConfig } from 'app/api/endpoints/auth/response/RegistrationConfig';

export type SignUpParsedConfig = {
  registrationConfig?: RegistrationConfig;
  localeName?: string;
  userSource?: string;
};

export type parsedPasswordErrors = {
  text: string;
  isValid: boolean;
}[];