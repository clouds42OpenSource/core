export enum RegistrationSource {
    Promo,
    Sauri,
    Dostavka,
    Efsol,
    Delans,
    ProfitAccount,
    AbonCenter,
    Roznica42,
    Kladovoy,
    Yashchenko,
    Market,
    MCOB
}