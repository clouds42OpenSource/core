import { msProvider } from 'app/api';
import { RegistrationConfig } from 'app/api/endpoints/auth/response/RegistrationConfig';
import Cookies from 'js-cookie';
import { useQuery } from 'app/hooks/useQuery';
import { RegistrationSource } from 'app/views/modules/SignUp/types/RegistrationSourceEnum';
import { SignUpParsedConfig } from 'app/views/modules/SignUp/types/SignUpParsedConfig';
import { useCallback, useEffect } from 'react';

export const SignUpView = () => {
    const params = useQuery();

    const parseQueryString = useCallback(() => {
        const parsedData: SignUpParsedConfig = {};

        const registrationConfig: RegistrationConfig = {
            cloudService: '',
            cloudServiceId: '',
            configuration1C: '',
            referralAccountId: '',
            registrationSource: 0
        };

        params.forEach((value, key) => {
            switch (key) {
                case 'CloudService':
                    registrationConfig.cloudService = value;
                    break;
                case 'CloudServiceId':
                    registrationConfig.cloudServiceId = value;
                    break;
                case 'Configuration1C':
                    registrationConfig.configuration1C = value;
                    break;
                case 'ReferralAccountId':
                    registrationConfig.referralAccountId = value;
                    break;
                case 'RegistrationSource':
                    registrationConfig.registrationSource = RegistrationSource[value as keyof typeof RegistrationSource];
                    break;
                case 'LocaleName':
                    parsedData.localeName = value ?? 'ru-RU';
                    break;
                case 'utm_source':
                    parsedData.userSource = value;
                    break;
                default:
                    break;
            }
        });

        if (!parsedData.userSource) {
            const match = Cookies.get('sbjs_first')?.match(/src=([^|]+)/);
            parsedData.userSource = match ? match[1] : 'Прямой переход по ссылке';
        }

        parsedData.registrationConfig = registrationConfig;

        return parsedData;
    }, [params]);

    useEffect(() => {
        const { userSource, registrationConfig, localeName } = parseQueryString();
        const userInfo = new URLSearchParams({ userinfo: JSON.stringify({ userSource, ...registrationConfig }) });

        window.location.href = msProvider(`oid/hs/ExternalRegistration/page?${ localeName ? `localeName=${ localeName }` : '' }&${ userInfo.toString() }`);
    }, [parseQueryString]);

    return null;
};