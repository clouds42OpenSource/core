import { LocaleDataForm } from 'app/views/modules/Locales/Views/types';

/**
 * Модель редактирования локали
 */
export type EditLocaleDataForm = LocaleDataForm & {

    /**
     * Адрес сайта личного кабинета
     */
    cpSiteUrl: string,

    /**
     * ID сегмента по умолчанию
     */
    defaultSegmentId: string
};