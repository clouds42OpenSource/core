import { hasComponentChangesFor } from 'app/common/functions';
import { TextBoxForm } from 'app/views/components/controls/forms';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import React from 'react';
import { LocalesFilterDataForm } from 'app/views/modules/Locales/Views/types';

type OwnProps = ReduxFormProps<LocalesFilterDataForm> & {
    onFilterChanged: (filter: LocalesFilterDataForm, filterFormName: string) => void;
    isFilterFormDisabled: boolean;
};

/**
 * Класс, описываюший фильтр поиска записей локалей
 */
class LocalesFilterFormViewClass extends React.Component<OwnProps> {
    public constructor(props: OwnProps) {
        super(props);
        this.onValueChange = this.onValueChange.bind(this);
        this.onValueApplied = this.onValueApplied.bind(this);
        this.initReduxForm = this.initReduxForm.bind(this);
        this.props.reduxForm.setInitializeFormDataAction(this.initReduxForm);
    }

    public shouldComponentUpdate(nextProps: OwnProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private onValueChange<TValue, TForm extends string>(formName: TForm, newValue: TValue) {
        this.props.reduxForm.updateReduxFormFields({
            [formName]: newValue
        });
    }

    private onValueApplied<TValue>(formName: string, value: TValue) {
        this.props.onFilterChanged({
            ...this.props.reduxForm.getReduxFormFields(false),
            [formName]: value
        }, formName);
    }

    private initReduxForm(): LocalesFilterDataForm | undefined {
        return {
            searchString: null
        };
    }

    public render() {
        const reduxForm = this.props.reduxForm.getReduxFormFields(false);

        return (<TextBoxForm
            label="Поиск"
            formName="searchString"
            onValueChange={ this.onValueChange }
            onValueApplied={ this.onValueApplied }
            value={ reduxForm.searchString ?? '' }
            isReadOnly={ this.props.isFilterFormDisabled }
            placeholder="Введите название, страну, валюту"
        />);
    }
}

/**
 * Компонент фильтра поиска записей локалей
 */
export const LocalesFilterFormView = withReduxForm(LocalesFilterFormViewClass, {
    reduxFormName: 'LocalesTabViewFilter_ReduxForm',
    resetOnUnmount: true
});