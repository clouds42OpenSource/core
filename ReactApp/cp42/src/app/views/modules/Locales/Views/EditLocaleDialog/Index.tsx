import { AppReduxStoreState } from 'app/redux/types';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { CommonDialog } from 'app/views/modules/_common/components/dialogs/CommonDialog';
import React from 'react';
import { connect } from 'react-redux';
import { EditLocaleDialogContentFormView } from 'app/views/modules/Locales/Views/EditLocaleDialog/EditLocaleDialogContentFormView';
import { LocaleItemDataModel } from 'app/web/InterlayerApiProxy/LocalesApiProxy/receiveLocales/data-models';
import { EditLocaleThunk } from 'app/modules/locales/store/thunks';
import { EditLocaleThunkParams } from 'app/modules/locales/store/reducers/editLocale/params/EditLocaleThunkParams';
import { LocalesProcessId } from 'app/modules/locales';
import { EditLocaleDataForm } from 'app/views/modules/Locales/Views/EditLocaleDialog/types';

type OwnProps = FloatMessageProps & {
    onCancelClick: () => void;
    isOpen: boolean;
    onConfirmOperationSuccess: () => void;
    selectedItem: LocaleItemDataModel;
};

type DispatchProps = {
    dispatchEditLocaleThunk: (args: EditLocaleThunkParams) => void;
};

type AllProps = OwnProps & DispatchProps;

class EditLocaleDialogClass extends React.Component<AllProps> {
    public constructor(props: AllProps) {
        super(props);

        this.onConfirmClick = this.onConfirmClick.bind(this);
    }

    private onConfirmClick<TDataModel>(formData: TDataModel) {
        const model = formData as unknown as EditLocaleDataForm;

        this.props.dispatchEditLocaleThunk({
            localeId: model.localeId,
            defaultSegmentId: model.defaultSegmentId,
            cpSiteUrl: model.cpSiteUrl,
            force: true
        });
    }

    public render() {
        return (
            <CommonDialog
                watch={ {
                    reducerStateName: 'LocalesState',
                    processId: LocalesProcessId.EditLocaleData,
                    getOnSuccessMessage: () => 'Изменения успешно сохранены.',
                    onSuccessHandler: this.props.onConfirmOperationSuccess
                } }
                dialogProps={
                    {
                        dataModel: this.props.selectedItem,
                        dialogWidth: 'sm',
                        dialogTitleFontSize: 20,
                        dialogTitle: `${ this.props.selectedItem?.country } (${ this.props.selectedItem?.name })`,
                        confirmButtonText: 'Сохранить',
                        dialogTitleTextAlign: 'center',
                        onCancelClick: this.props.onCancelClick,
                        isOpen: this.props.isOpen,
                        onConfirmClick: this.onConfirmClick
                    }
                }
            >
                { ref => <EditLocaleDialogContentFormView ref={ ref } itemToEdit={ this.props.selectedItem } /> }
            </CommonDialog>
        );
    }
}

const EditLocaleDialogConnected = connect<{}, DispatchProps, OwnProps, AppReduxStoreState>(
    null,
    {
        dispatchEditLocaleThunk: EditLocaleThunk.invoke,
    }
)(EditLocaleDialogClass);

/**
 * Модальное окно редактирования локали
 */
export const EditLocaleDialog = withFloatMessages(EditLocaleDialogConnected);