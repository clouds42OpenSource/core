import { Nullable } from 'app/common/types';

/**
 * Данные локали
 */
export type LocaleDataForm = {

    /**
     * ID локали
     */
    localeId: string,

    /**
     * Название локали (прим. ru-ru)
     */
    name: string,

    /**
     * Страна локали
     */
    country: string,

    /**
     * Код валюты
     */
    currencyCode: Nullable<number>,

    /**
     * Валюта
     */
    currency: string
};