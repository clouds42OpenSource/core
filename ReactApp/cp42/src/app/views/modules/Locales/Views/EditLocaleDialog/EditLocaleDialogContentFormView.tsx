import { Box } from '@mui/material';
import { hasComponentChangesFor } from 'app/common/functions';
import { ReceiveLocaleDataForEditingThunkParams } from 'app/modules/locales/store/reducers/receiveLocaleDataForEditing/params';
import { ReceiveLocaleDataForEditingThunk } from 'app/modules/locales/store/thunks';
import { ReceiveSegmentsShortDataThunk } from 'app/modules/segments/TabSegments/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { ComboBoxForm } from 'app/views/components/controls/forms/ComboBox/ComboBoxForm';
import { TextBoxForm } from 'app/views/components/controls/forms/TextBoxForm';
import { EditLocaleDataForm } from 'app/views/modules/Locales/Views/EditLocaleDialog/types';
import { LocaleDataModel, LocaleItemDataModel } from 'app/web/InterlayerApiProxy/LocalesApiProxy/receiveLocales/data-models';
import { KeyValueDataModel } from 'app/web/common/data-models';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';
import React from 'react';
import { connect } from 'react-redux';

type OwnProps = ReduxFormProps<EditLocaleDataForm> & {
    itemToEdit: LocaleItemDataModel;
};

type StateProps = {
    hasDataReceived: boolean;
    details: LocaleDataModel;
    segmentsData: Array<KeyValueDataModel<string, string>>
};

type DispatchProps = {
    dispatchReceiveEditLocaleCardThunk: (args?: ReceiveLocaleDataForEditingThunkParams) => void;
    dispatchReceiveSegmentsShortDataThunk: (args?: IForceThunkParam) => void;
};

type AllProps = OwnProps & StateProps & DispatchProps;

/**
 * Класс, описывающий содержимое модального окна редактирования локали
 */
class EditLocaleDialogContentFormViewClass extends React.Component<AllProps> {
    public constructor(props: AllProps) {
        super(props);
        this.onValueChange = this.onValueChange.bind(this);
        this.initReduxForm = this.initReduxForm.bind(this);
        this.props.reduxForm.setInitializeFormDataAction(this.initReduxForm);
    }

    public componentDidMount() {
        this.props.dispatchReceiveEditLocaleCardThunk({
            localeId: this.props.itemToEdit.localeId,
            force: true
        });
        this.props.dispatchReceiveSegmentsShortDataThunk();
    }

    public shouldComponentUpdate(nextProps: AllProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private onValueChange<TValue>(formName: string, newValue: TValue) {
        this.props.reduxForm.updateReduxFormFields({
            [formName]: newValue
        });
    }

    private initReduxForm(): EditLocaleDataForm | undefined {
        if (!this.props.hasDataReceived) {
            return;
        }

        return {
            ...this.props.details
        };
    }

    public render() {
        if (!this.props.hasDataReceived) {
            return null;
        }

        const reduxForm = this.props.reduxForm.getReduxFormFields(false);

        return (
            <Box display="flex" flexDirection="column" gap="16px">
                <TextBoxForm
                    formName="name"
                    label="Название"
                    value={ reduxForm.name }
                    autoFocus={ false }
                    onValueChange={ void 0 }
                    isReadOnly={ true }

                />
                <TextBoxForm
                    formName="country"
                    label="Страна"
                    value={ reduxForm.country }
                    autoFocus={ false }
                    onValueChange={ void 0 }
                    isReadOnly={ true }
                />
                <TextBoxForm
                    formName="currency"
                    label="Валюта"
                    value={ reduxForm.currency }
                    autoFocus={ false }
                    onValueChange={ void 0 }
                    isReadOnly={ true }
                />
                <TextBoxForm
                    formName="currencyCode"
                    label="Код валюты"
                    value={ reduxForm.currencyCode ? reduxForm.currencyCode.toString() : '' }
                    autoFocus={ false }
                    onValueChange={ void 0 }
                    isReadOnly={ true }
                />
                <TextBoxForm
                    formName="cpSiteUrl"
                    label="Адрес сайта личного кабинета"
                    value={ reduxForm.cpSiteUrl }
                    autoFocus={ false }
                    onValueChange={ this.onValueChange }
                />
                <ComboBoxForm
                    formName="defaultSegmentId"
                    label="Сегмент по умолчанию"
                    value={ reduxForm.defaultSegmentId }
                    items={ this.props.segmentsData.map(item => {
                        return {
                            value: item.key,
                            text: item.value
                        };
                    })
                    }
                    onValueChange={ this.onValueChange }
                    allowAutoComplete={ true }
                />
            </Box>
        );
    }
}

export const EditLocaleDialogContentFormViewConnected = connect<StateProps, DispatchProps, OwnProps, AppReduxStoreState>(
    state => {
        const localesState = state.LocalesState;
        const localeDataModel = localesState.localeData;

        const segmentsState = state.SegmentsState;
        const segmentsShortData = segmentsState.segmentsShortData.items;

        const hasDataReceived = localesState.hasSuccessFor.hasLocaleDataReceived && segmentsState.hasSuccessFor.hasSegmentsShortDataReceived;

        return {
            details: localeDataModel,
            segmentsData: segmentsShortData,
            hasDataReceived
        };
    },
    {
        dispatchReceiveEditLocaleCardThunk: ReceiveLocaleDataForEditingThunk.invoke,
        dispatchReceiveSegmentsShortDataThunk: ReceiveSegmentsShortDataThunk.invoke
    }
)(EditLocaleDialogContentFormViewClass);

/**
 * Содержимое модального окна редактирования локали
 */
export const EditLocaleDialogContentFormView = withReduxForm(EditLocaleDialogContentFormViewConnected, {
    reduxFormName: 'EditLocaleDialogContent_ReduxForm',
    resetOnUnmount: true
});