import { AppReduxStoreState } from 'app/redux/types';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { LocaleItemDataModel } from 'app/web/InterlayerApiProxy/LocalesApiProxy/receiveLocales/data-models';
import { LocalesFilterDataForm } from 'app/views/modules/Locales/Views/types';
import { LocalesFilterFormView } from 'app/views/modules/Locales/Views/filter';
import { LocalesProcessId } from 'app/modules/locales';
import { ProcessIdStateHelper } from 'app/common/helpers/ProcessIdStateHelper';
import React from 'react';
import { ReceiveLocalesThunk } from 'app/modules/locales/store/thunks';
import { ReceiveLocalesThunkParams } from 'app/modules/locales/store/reducers/receiveLocalesData/params';
import { SelectDataCommonDataModel, SortingDataModel } from 'app/web/common/data-models';
import { SortingKind } from 'app/common/enums';
import { TablePagination } from 'app/views/modules/_common/components/CommonTableWithFilter/TableView';
import { TextButton } from 'app/views/components/controls/Button';
import { TextOut } from 'app/views/components/TextOut';
import { connect } from 'react-redux';
import { hasComponentChangesFor } from 'app/common/functions';
import { EditLocaleDialog } from './EditLocaleDialog/Index';

type StateProps<TDataRow = LocaleItemDataModel> = {
    dataset: TDataRow[];
    hasLocalesDataReceived: boolean;
    isInLoadingDataProcess: boolean;
    pagination: TablePagination;
};

type DispatchProps = {
    dispatchReceiveLocalesThunk: (args: ReceiveLocalesThunkParams) => void
};

type OwnState = {
    isEditLocaleCardOpen: boolean;
    selectedItem?: LocaleItemDataModel;
};

type AllProps = StateProps & DispatchProps;

/**
 * Класс, описывающий содержимое страницы локали
 */
class LocalesViewClass extends React.Component<AllProps, OwnState> {
    private _commonTableWithFilterViewRef = React.createRef<CommonTableWithFilter<any, any>>();

    public constructor(props: AllProps) {
        super(props);

        this.onDataSelect = this.onDataSelect.bind(this);
        this.closeEditLocaleDialog = this.closeEditLocaleDialog.bind(this);
        this.openEditLocaleDialog = this.openEditLocaleDialog.bind(this);
        this.onConfirmOperationSuccessEditLocaleDialog = this.onConfirmOperationSuccessEditLocaleDialog.bind(this);

        this.state = {
            isEditLocaleCardOpen: false,
            selectedItem: void 0
        };
    }

    public shouldComponentUpdate(nextProps: AllProps, nextState: OwnState) {
        return hasComponentChangesFor(this.props, nextProps) ||
            hasComponentChangesFor(this.state, nextState);
    }

    public componentDidUpdate(prevProps: AllProps) {
        if (!this.props.hasLocalesDataReceived && prevProps.hasLocalesDataReceived !== this.props.hasLocalesDataReceived && this._commonTableWithFilterViewRef.current) {
            this._commonTableWithFilterViewRef.current.ReSelectData();
        }
    }

    private onDataSelect(args: SelectDataCommonDataModel<LocalesFilterDataForm>) {
        this.props.dispatchReceiveLocalesThunk({
            pageNumber: args.pageNumber,
            filter: args.filter,
            orderBy: args.sortingData ? this.getSortQuery(args.sortingData) : 'name.desc',
            force: true
        });
    }

    private onConfirmOperationSuccessEditLocaleDialog() {
        this.closeEditLocaleDialog();
        if (this._commonTableWithFilterViewRef.current) {
            this._commonTableWithFilterViewRef.current.ReSelectData();
        }
    }

    private getSortQuery(args: SortingDataModel) {
        switch (args.fieldName) {
            case 'name':
                return `name.${ args.sortKind ? 'desc' : 'asc' }`;
            case 'country':
                return `country.${ args.sortKind ? 'desc' : 'asc' }`;
            case 'currency':
                return `currency.${ args.sortKind ? 'desc' : 'asc' }`;
            default:
                return 'name.desc';
        }
    }

    private openEditLocaleDialog(item: LocaleItemDataModel) {
        return () => {
            this.setState({
                isEditLocaleCardOpen: true,
                selectedItem: item
            });
        };
    }

    private closeEditLocaleDialog() {
        this.setState({
            isEditLocaleCardOpen: false,
            selectedItem: undefined
        });
    }

    public render() {
        return (
            <>
                <CommonTableWithFilter
                    ref={ this._commonTableWithFilterViewRef }
                    uniqueContextProviderStateId="LocalesContextProviderStateId"
                    filterProps={ {
                        getFilterContentView: (ref, onFilterChanged) =>
                            <LocalesFilterFormView ref={ ref } isFilterFormDisabled={ this.props.isInLoadingDataProcess } onFilterChanged={ onFilterChanged } />
                    } }
                    tableProps={ {
                        dataset: this.props.dataset,
                        keyFieldName: 'localeId',
                        sorting: {
                            sortFieldName: 'name',
                            sortKind: SortingKind.Desc
                        },
                        fieldsView: {
                            name: {
                                caption: 'Название',
                                isSortable: true,
                                fieldWidth: '40%',
                                format: (_value, row) => {
                                    return (
                                        <TextButton onClick={ this.openEditLocaleDialog(row) }>
                                            <TextOut fontSize={ 13 } className="text-link">
                                                { _value }
                                            </TextOut>
                                        </TextButton>
                                    );
                                }
                            },
                            country: {
                                caption: 'Страна',
                                isSortable: true,
                                fieldWidth: '40%'
                            },
                            currency: {
                                caption: 'Валюта (код)',
                                isSortable: true,
                                fieldWidth: '20%',
                                format: (_value, row) => {
                                    return (
                                        <TextOut>
                                            { _value } ({ row.currencyCode })
                                        </TextOut>
                                    );
                                }
                            }
                        },
                        pagination: this.props.pagination
                    } }
                    onDataSelect={ this.onDataSelect }
                />
                <EditLocaleDialog
                    isOpen={ this.state.isEditLocaleCardOpen }
                    onCancelClick={ this.closeEditLocaleDialog }
                    onConfirmOperationSuccess={ this.onConfirmOperationSuccessEditLocaleDialog }
                    selectedItem={ this.state.selectedItem! }
                />
            </>
        );
    }
}

/**
 * Содержимое страницы локали
 */
export const LocalesView = connect<StateProps, DispatchProps, {}, AppReduxStoreState>(
    state => {
        const localesState = state.LocalesState;
        const localesData = localesState.locales;
        const dataset = localesData.items;

        const currentPage = localesData.metadata.pageNumber;
        const totalPages = localesData.metadata.pageCount;
        const { pageSize } = localesData.metadata;
        const recordsCount = localesData.metadata.totalItemCount;

        const { hasLocalesDataReceived } = localesState.hasSuccessFor;
        const isInLoadingDataProcess = ProcessIdStateHelper.isInProgress(localesState.reducerActions, LocalesProcessId.ReceiveLocalesData);

        return {
            dataset,
            hasLocalesDataReceived,
            isInLoadingDataProcess,
            pagination: {
                currentPage,
                totalPages,
                pageSize,
                recordsCount
            }
        };
    },
    {
        dispatchReceiveLocalesThunk: ReceiveLocalesThunk.invoke
    }
)(LocalesViewClass);