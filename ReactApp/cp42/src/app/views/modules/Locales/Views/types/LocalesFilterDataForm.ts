import { Nullable } from 'app/common/types';

/**
 * Модель Redux формы редактирования фильтра для таблицы локалей
 */
export type LocalesFilterDataForm = {

    /**
     * Строка поиска
     */
    searchString: Nullable<string>;
};