import { LocalesView } from 'app/views/modules/Locales/Views';
import React from 'react';
import { withHeader } from 'app/views/components/_hoc/withHeader';

/**
 * Класс, описывающий страницу локалей
 */
class LocalesViewClass extends React.Component {
    public render() {
        return (
            <LocalesView />
        );
    }
}

/**
 * Страница локалей
 */
export const LocalesPageView = withHeader({
    title: 'Локали',
    page: LocalesViewClass
});