import { useEffect } from 'react';
import { useHistory } from 'react-router';

import { FETCH_API } from 'app/api/useFetchApi';
import { REDUX_API } from 'app/api/useReduxApi';
import { AppRoutes } from 'app/AppRoutes';
import { localStorageHelper } from 'app/common/helpers/LocalStorageHelper';
import { useAppDispatch } from 'app/hooks';
import { LoadingBounce } from 'app/views/components/LoadingBounce';

const { postCheckToken } = FETCH_API.AUTH;
const {
    getCurrentSessionSettings,
    getAlertMessages,
    getUserPermissions,
    getNavMenu
} = REDUX_API.GLOBAL_REDUX;

const token = localStorageHelper.getJWTToken();

export const ProxyPage = () => {
    const history = useHistory();
    const dispatch = useAppDispatch();

    const redirectUrl = (path: string) => {
        if (path.includes('/billing-service') || path.includes('/users')) {
            return path;
        }

        switch (path) {
            case '/my-databases':
                return AppRoutes.accountManagement.informationBases;
            case '/create-db':
                return AppRoutes.accountManagement.createAccountDatabase.createDb;
            case '/esdl':
                return AppRoutes.services.fasta;
            case '/rent':
                return AppRoutes.services.rentReference;
            default:
                return AppRoutes.homeRoute;
        }
    };

    useEffect(() => {
        const query = new URLSearchParams(window.location.search);
        const uid = query.get('openid.auth.uid');
        const user = query.get('openid.auth.user');
        const returnTo = query.get('return_to');

        if (uid && user) {
            postCheckToken([uid, user])
                .then(() => getCurrentSessionSettings(dispatch))
                .then(() => getAlertMessages(dispatch))
                .then(() => getUserPermissions(dispatch))
                .then(() => getNavMenu(dispatch))
                .then(() => {
                    history.push(redirectUrl(returnTo ?? ''));
                });
        } else if (token) {
            history.push(AppRoutes.homeRoute);
        } else {
            history.push(`${ AppRoutes.signInRoute }?isOk=0`);
        }
    }, [dispatch, history]);

    return (
        <LoadingBounce noMargin={ true } />
    );
};