import { hasComponentChangesFor } from 'app/common/functions';
import { ReceiveTasksPageThunkParams } from 'app/modules/tasksPage/store/reducers/receiveTasksPageReducer/params';
import { ReceiveTasksPageThunk } from 'app/modules/tasksPage/store/reducers/receiveTasksPageReducer/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { TabsControl } from 'app/views/components/controls/TabsControl';
import { withHeader } from 'app/views/components/_hoc/withHeader';
import { TaskControlTabView } from 'app/views/modules/CoreWorkerTasks/tabs/TaskControlTabView';
import { TaskInQueueTabView } from 'app/views/modules/CoreWorkerTasks/tabs/TaskInQueueTabView';
import {
    WorkerAvailableTasksBagsTabView
} from 'app/views/modules/CoreWorkerTasks/tabs/WorkerAvailableTasksBagsTabView';
import {
    TasksPageTabVisibilityDataModel
} from 'app/web/InterlayerApiProxy/TasksPageApiProxy/receiveTasksPage/data-models';
import React from 'react';
import { connect } from 'react-redux';

type OwnState = {
    activeTabIndex: number;
};

type StateProps = {
    tabVisibility: TasksPageTabVisibilityDataModel;
    hasTasksPageReceived: boolean;
};

type DispatchProps = {
    dispatchReceiveTasksPageThunk: (args?: ReceiveTasksPageThunkParams) => void;
};

type AllProps = StateProps & DispatchProps;

/**
 * Класс, описывающий страницу задач воркера
 */
class CoreWorkerTasksViewClass extends React.Component<AllProps, OwnState> {
    public constructor(props: AllProps) {
        super(props);
        this.onActiveTabIndexChanged = this.onActiveTabIndexChanged.bind(this);
        this.state = {
            activeTabIndex: 0,
        };
    }

    public componentDidMount() {
        if (this.props.hasTasksPageReceived) return;
        this.props.dispatchReceiveTasksPageThunk({
            force: true
        });
    }

    public shouldComponentUpdate(_nextProps: {}, nextState: OwnState) {
        return hasComponentChangesFor(this.props, _nextProps) ||
            hasComponentChangesFor(this.state, nextState);
    }

    private onActiveTabIndexChanged(tabIndex: number) {
        this.setState({
            activeTabIndex: tabIndex
        });
    }

    public render() {
        return (
            <TabsControl
                activeTabIndex={ this.state.activeTabIndex }
                onActiveTabIndexChanged={ this.onActiveTabIndexChanged }
                headers={ [
                    {
                        title: 'Очередь задач',
                        isVisible: true,
                    },
                    {
                        title: 'Управление',
                        isVisible: this.props.tabVisibility.isControlTaskTabVisible
                    },
                    {
                        title: 'Взаимодействие воркера и задач',
                        isVisible: this.props.tabVisibility.isWorkerAvailableTasksBagsTabVisible
                    }]
                }
            >
                <TaskInQueueTabView isAddTaskButtonVisible={ this.props.tabVisibility.isAddTaskButtonVisible } />
                <TaskControlTabView canLoad={ this.state.activeTabIndex === 1 } />
                <WorkerAvailableTasksBagsTabView canLoad={ this.state.activeTabIndex === 2 } />
            </TabsControl>
        );
    }
}

const CoreWorkerTasksViewConnected = connect<StateProps, DispatchProps, {}, AppReduxStoreState>(
    state => {
        const tasksPageState = state.TasksPageState;
        const { tabVisibility } = tasksPageState;
        const { hasTasksPageReceived } = tasksPageState.hasSuccessFor;

        return {
            tabVisibility,
            hasTasksPageReceived
        };
    },
    {
        dispatchReceiveTasksPageThunk: ReceiveTasksPageThunk.invoke,
    }
)(CoreWorkerTasksViewClass);

export const CoreWorkerTasksView = withHeader({
    title: 'Задачи воркера',
    page: CoreWorkerTasksViewConnected
});