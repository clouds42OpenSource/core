import { Box } from '@mui/material';
import { hasComponentChangesFor } from 'app/common/functions';
import { ReceiveCoreWorkerTasksDataThunk } from 'app/modules/coreWorkerTasks/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { ComboBoxForm, TextBoxForm } from 'app/views/components/controls/forms';
import { AddTaskToQueueDataForm, AddTaskToQueueDataFormFieldNamesType } from 'app/views/modules/CoreWorkerTasks/tabs/TaskInQueueTabView/types/AddTaskToQueueDataForm';
import { SearchAccountsItemDataModel } from 'app/web/InterlayerApiProxy/ServiceAccountsApiProxy/searchAccounts';
import { KeyValueDataModel } from 'app/web/common/data-models';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';
import React from 'react';
import { connect } from 'react-redux';

type OwnProps = ReduxFormProps<AddTaskToQueueDataForm>;

type OwnState = {
    accounts: Array<SearchAccountsItemDataModel>
};

type StateProps = {
    availableTasks: Array<KeyValueDataModel<string, string>>;
};

type DispatchProps = {
    dispatchReceiveCoreWorkerTasks: (args?: IForceThunkParam) => void;
};

type AllProps = OwnProps & StateProps & DispatchProps;

class AddTaskInQueueDialogContentFormViewClass extends React.Component<AllProps, OwnState> {
    public constructor(props: AllProps) {
        super(props);
        this.onValueChange = this.onValueChange.bind(this);
        this.initReduxForm = this.initReduxForm.bind(this);
        this.props.reduxForm.setInitializeFormDataAction(this.initReduxForm);
    }

    public componentDidMount() {
        this.props.dispatchReceiveCoreWorkerTasks();
    }

    public shouldComponentUpdate(nextProps: AllProps, nextState: OwnState) {
        return hasComponentChangesFor(this.props, nextProps) ||
            hasComponentChangesFor(this.state, nextState);
    }

    private onValueChange<TValue, TForm extends string = AddTaskToQueueDataFormFieldNamesType>(formName: TForm, newValue: TValue) {
        this.props.reduxForm.updateReduxFormFields({
            [formName]: newValue
        });
    }

    private initReduxForm(): AddTaskToQueueDataForm | undefined {
        if (!this.props.availableTasks.length) {
            return;
        }

        return {
            comment: '',
            taskParams: '',
            taskId: this.props.availableTasks[0].key
        };
    }

    public render() {
        if (!this.props.availableTasks.length) {
            return null;
        }

        const reduxForm = this.props.reduxForm.getReduxFormFields(false);

        return (
            <Box display="flex" flexDirection="column" gap="16px">
                <ComboBoxForm
                    allowAutoComplete={ true }
                    autoFocus={ true }
                    label="Задача:"
                    formName="taskId"
                    onValueChange={ this.onValueChange }
                    value={ reduxForm.taskId }
                    items={ this.props.availableTasks.map(item => {
                        return {
                            value: item.key,
                            text: item.value
                        };
                    }) }
                />
                <TextBoxForm
                    type="textarea"
                    label="Параметры:"
                    formName="taskParams"
                    value={ reduxForm.taskParams }
                    onValueChange={ this.onValueChange }
                />
                <TextBoxForm
                    type="textarea"
                    label="Комментарий:"
                    formName="comment"
                    value={ reduxForm.comment }
                    onValueChange={ this.onValueChange }
                />
            </Box>
        );
    }
}

const AddTaskInQueueDialogContentFormViewConnected = connect<StateProps, DispatchProps, OwnProps, AppReduxStoreState>(
    state => {
        const workerTasksState = state.CoreWorkerTasksState;
        const availableTasks = workerTasksState.tasks.items;

        return {
            availableTasks
        };
    },
    {
        dispatchReceiveCoreWorkerTasks: ReceiveCoreWorkerTasksDataThunk.invoke
    }
)(AddTaskInQueueDialogContentFormViewClass);

export const AddTaskInQueueDialogContentFormView = withReduxForm(AddTaskInQueueDialogContentFormViewConnected, {
    reduxFormName: 'AddTaskInQueueDialogContent_ReduxForm',
    resetOnUnmount: true
});