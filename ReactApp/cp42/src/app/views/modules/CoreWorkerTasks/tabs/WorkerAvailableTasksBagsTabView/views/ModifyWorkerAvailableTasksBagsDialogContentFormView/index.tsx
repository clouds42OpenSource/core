import { hasComponentChangesFor } from 'app/common/functions';
import { createInt32Validator } from 'app/common/validators';
import { TextBoxForm } from 'app/views/components/controls/forms';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import {
    WorkerAvailableTasksBagItemDataModel
} from 'app/web/InterlayerApiProxy/WorkerAvailableTasksBagsApiProxy/receiveWorkerAvailableTasksBagsData';
import React from 'react';
import { Box } from '@mui/material';

type OwnProps = ReduxFormProps<WorkerAvailableTasksBagItemDataModel> & {
    itemToEdit: WorkerAvailableTasksBagItemDataModel;
};

class ModifyWorkerAvailableTasksBagsDialogContentFormViewClass extends React.Component<OwnProps> {
    public constructor(props: OwnProps) {
        super(props);
        this.onValueChange = this.onValueChange.bind(this);
        this.initReduxForm = this.initReduxForm.bind(this);
        this.props.reduxForm.setInitializeFormDataAction(this.initReduxForm);
    }

    public shouldComponentUpdate(nextProps: OwnProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private onValueChange<TValue, TForm extends string>(formName: TForm, newValue: TValue) {
        this.props.reduxForm.updateReduxFormFields({
            [formName]: newValue
        });
    }

    private initReduxForm(): WorkerAvailableTasksBagItemDataModel | undefined {
        if (!this.props.itemToEdit) {
            return;
        }

        return {
            id: this.props.itemToEdit.id,

            taskId: this.props.itemToEdit.taskId,
            taskName: this.props.itemToEdit.taskName,

            workerId: this.props.itemToEdit.workerId,
            workerName: this.props.itemToEdit.workerName,

            taskPriority: this.props.itemToEdit.taskPriority,
        };
    }

    public render() {
        const reduxForm = this.props.reduxForm.getReduxFormFields(false);

        return (
            <Box display="flex" flexDirection="column" gap="16px">
                <TextBoxForm
                    label="Воркер:"
                    formName="workerName"
                    isReadOnly={ true }
                    value={ this.props.itemToEdit.workerName }
                    onValueChange={ this.onValueChange }
                />
                <TextBoxForm
                    label="Задача:"
                    formName="taskName"
                    isReadOnly={ true }
                    value={ this.props.itemToEdit.taskName }
                    onValueChange={ this.onValueChange }
                />
                <TextBoxForm
                    autoFocus={ true }
                    label="Приоритет:"
                    formName="taskPriority"
                    maxLength={ 50 }
                    isReadOnly={ false }
                    validator={ createInt32Validator() }
                    value={ reduxForm.taskPriority?.toString() ?? '' }
                    onValueChange={ this.onValueChange }
                />
            </Box>
        );
    }
}

export const ModifyWorkerAvailableTasksBagsDialogContentFormView = withReduxForm(ModifyWorkerAvailableTasksBagsDialogContentFormViewClass, {
    reduxFormName: 'ModifyWorkerAvailableTasksBagsDialogContent_ReduxForm',
    resetOnUnmount: true
});