import { Nullable } from 'app/common/types';

/**
 * Модель Redux формы редактирования фильтра для таблицы задач в очереди
 */
export type TaskInQueueFilterDataForm = {
    /**
     * Фильтр для номера воркера
     */
    workerId: Nullable<number>,

    /**
     * Фильтр для ID задачи
     */
    taskId: Nullable<string>,

    /**
     * Фильтр для статуса задачи в очереди
     */
    status: Nullable<string>,

    /**
     * Фильтр для начального периода поиска
     */
    periodFrom: Nullable<Date>,

    /**
     * Фильтр для конечного периода поиска
     */
    periodTo: Nullable<Date>,

    /**
     * Строка поиска
     */
    searchString: Nullable<string>
};

export type TaskInQueueFilterDataFormFieldNamesType = keyof TaskInQueueFilterDataForm;