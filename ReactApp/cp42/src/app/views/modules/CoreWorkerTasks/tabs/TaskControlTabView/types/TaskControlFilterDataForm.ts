import { Nullable } from 'app/common/types';

/**
 * Фильтр поиска задачи для управления временем жизни
 */
export type TaskControlFilterDataForm = {
    /**
     * ID задачи
     */
    taskId: Nullable<string>
};

export type TaskControlFilterDataFormFieldNamesType = keyof TaskControlFilterDataForm;