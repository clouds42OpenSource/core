import { ComboBoxForm, TextBoxForm } from 'app/views/components/controls/forms';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';

import { AppReduxStoreState } from 'app/redux/types';
import { ComboboxItemModel } from 'app/views/components/controls/forms/ComboBox/models';
import { FilterItemWrapper } from 'app/views/components/_hoc/tableFilterWrapper/filterItemWrapper';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';
import { KeyValueDataModel } from 'app/web/common/data-models';
import React from 'react';
import { ReceiveCoreWorkerTasksDataThunk } from 'app/modules/coreWorkerTasks/store/thunks';
import { ReceiveWorkersInSystemDataThunk } from 'app/modules/workers/store/thunks';
import { TableFilterWrapper } from 'app/views/components/_hoc/tableFilterWrapper';
import {
    WorkerAvailableTasksBagsFilterDataForm
} from 'app/views/modules/CoreWorkerTasks/tabs/WorkerAvailableTasksBagsTabView/types';
import { WorkerDescriptionDataModel } from 'app/web/InterlayerApiProxy/WorkersApiProxy/receiveWorkersData';
import { connect } from 'react-redux';
import { createInt32Validator } from 'app/common/validators';
import { hasComponentChangesFor } from 'app/common/functions';

type OwnProps = ReduxFormProps<WorkerAvailableTasksBagsFilterDataForm> & {
    onFilterChanged: (filter: WorkerAvailableTasksBagsFilterDataForm, filterFormName: string) => void;
    isFilterFormDisabled: boolean;
};

type StateProps = {
    coreWorkerTasks: Array<KeyValueDataModel<string, string>>;
    availableWorkers: Array<WorkerDescriptionDataModel>;
};

type DispatchProps = {
    dispatchReceiveWorkersDataThunk: (args?: IForceThunkParam) => void;
    dispatchReceiveCoreWorkerTasks: (args?: IForceThunkParam) => void;
};

type AllProps = OwnProps & StateProps & DispatchProps;

class WorkerAvailableTasksBagsTabFilterFormViewClass extends React.Component<AllProps> {
    public constructor(props: AllProps) {
        super(props);
        this.onValueChange = this.onValueChange.bind(this);
        this.onValueApplied = this.onValueApplied.bind(this);
        this.initReduxForm = this.initReduxForm.bind(this);
        this.props.reduxForm.setInitializeFormDataAction(this.initReduxForm);

        this.getAvailableWorkersSelectOptions = this.getAvailableWorkersSelectOptions.bind(this);
        this.getWorkerTasksSelectOptions = this.getWorkerTasksSelectOptions.bind(this);
    }

    public componentDidMount() {
        this.props.dispatchReceiveWorkersDataThunk();
        this.props.dispatchReceiveCoreWorkerTasks();
    }

    public shouldComponentUpdate(nextProps: AllProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private onValueChange<TValue, TForm extends string>(formName: TForm, newValue: TValue) {
        this.props.reduxForm.updateReduxFormFields({
            [formName]: newValue
        });
    }

    private onValueApplied<TValue>(formName: string, value: TValue) {
        this.props.onFilterChanged({
            ...this.props.reduxForm.getReduxFormFields(false),
            [formName]: value
        }, formName);
    }

    /**
     * Получить список опций выпадающего списка для доступных воркеров
     */
    private getAvailableWorkersSelectOptions(): ComboboxItemModel<string>[] {
        return this.props.availableWorkers.map(workerItem => {
            const workerId = workerItem.id.toString();
            return {
                value: workerId,
                text: `[${ workerId }] ${ workerItem.name ?? 'core-worker' }`
            };
        });
    }

    /**
     * Получить список опций выпадающего списка для задач воркера
     */
    private getWorkerTasksSelectOptions(): ComboboxItemModel<string>[] {
        return this.props.coreWorkerTasks.map(taskItem => {
            return {
                value: taskItem.key,
                text: taskItem.value
            };
        });
    }

    private initReduxForm(): WorkerAvailableTasksBagsFilterDataForm | undefined {
        if (!this.props.coreWorkerTasks.length || !this.props.availableWorkers.length) {
            return;
        }

        return {
            taskId: null,
            workerId: null,
            taskPriority: null
        };
    }

    public render() {
        if (!this.props.coreWorkerTasks.length || !this.props.availableWorkers.length) {
            return null;
        }

        const reduxForm = this.props.reduxForm.getReduxFormFields(false);

        return (
            <TableFilterWrapper>
                <FilterItemWrapper width="350px">
                    <ComboBoxForm
                        label="Воркер"
                        formName="workerId"
                        value={ reduxForm.workerId ?? '' }
                        onValueChange={ this.onValueChange }
                        onValueApplied={ this.onValueApplied }
                        autoFocus={ true }
                        items={ [
                            { value: '', text: 'Все' },
                            ...this.getAvailableWorkersSelectOptions()
                        ] }
                        isReadOnly={ this.props.isFilterFormDisabled }
                    />
                </FilterItemWrapper>
                <FilterItemWrapper width="350px">
                    <ComboBoxForm
                        label="Задача"
                        formName="taskId"
                        onValueChange={ this.onValueChange }
                        onValueApplied={ this.onValueApplied }
                        value={ reduxForm.taskId ?? '' }
                        items={ [
                            { value: '', text: 'Все' },
                            ...this.getWorkerTasksSelectOptions()
                        ] }
                        isReadOnly={ this.props.isFilterFormDisabled }
                        allowAutoComplete={ true }
                    />
                </FilterItemWrapper>
                <FilterItemWrapper width="350px">
                    <TextBoxForm
                        label="Приоритет задачи"
                        formName="taskPriority"
                        onValueChange={ this.onValueChange }
                        onValueApplied={ this.onValueApplied }
                        value={ reduxForm.taskPriority?.toString() ?? '' }
                        validator={ createInt32Validator() }
                        isReadOnly={ this.props.isFilterFormDisabled }
                    />
                </FilterItemWrapper>
            </TableFilterWrapper>
        );
    }
}

/**
 * Компонент для фильтра вкладки "Взаимодействие воркера и задач"
 */
export const WorkerAvailableTasksBagsTabFilterFormViewConnected = connect<StateProps, DispatchProps, OwnProps, AppReduxStoreState>(
    state => {
        const workersState = state.WorkersState;
        const availableWorkers = workersState.availableWorkers.items;

        const coreWorkerTasksState = state.CoreWorkerTasksState;
        const coreWorkerTasks = coreWorkerTasksState.tasks.items;

        return {
            availableWorkers,
            coreWorkerTasks
        };
    },
    {
        dispatchReceiveWorkersDataThunk: ReceiveWorkersInSystemDataThunk.invoke,
        dispatchReceiveCoreWorkerTasks: ReceiveCoreWorkerTasksDataThunk.invoke
    }
)(WorkerAvailableTasksBagsTabFilterFormViewClass);

export const WorkerAvailableTasksBagsTabFilterFormView = withReduxForm(WorkerAvailableTasksBagsTabFilterFormViewConnected, {
    reduxFormName: 'WorkerAvailableTasksBagsTabViewFilter_ReduxForm',
    resetOnUnmount: true
});