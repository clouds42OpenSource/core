import { SortingKind } from 'app/common/enums';
import { hasComponentChangesFor } from 'app/common/functions';
import { ProcessIdStateHelper } from 'app/common/helpers/ProcessIdStateHelper';
import { WorkerAvailableTasksBagsProcessId } from 'app/modules/workerAvailableTasksBags';
import {
    ChangeWorkerTaskBagPriorityThunkParams
} from 'app/modules/workerAvailableTasksBags/store/reducers/changeWorkerTaskBagPriority/params';
import {
    ReceiveWorkerAvailableTasksBagsThunkParams
} from 'app/modules/workerAvailableTasksBags/store/reducers/receiveWorkerAvailableTasksBags/params';
import { ReceiveWorkerAvailableTasksBagsThunk } from 'app/modules/workerAvailableTasksBags/store/thunks';
import {
    ChangeWorkerTaskBagPriorityThunk
} from 'app/modules/workerAvailableTasksBags/store/thunks/ChangeWorkerTaskBagPriorityThunk';
import { AppReduxStoreState } from 'app/redux/types';
import { TextOut } from 'app/views/components/TextOut';
import { TextButton } from 'app/views/components/controls/Button';
import {
    WorkerAvailableTasksBagsFilterDataForm
} from 'app/views/modules/CoreWorkerTasks/tabs/WorkerAvailableTasksBagsTabView/types';
import {
    AssignTasksToWorkerDialog
} from 'app/views/modules/CoreWorkerTasks/tabs/WorkerAvailableTasksBagsTabView/views/AssignTasksToWorkerDialog';
import {
    ModifyWorkerAvailableTasksBagsDialogContentFormView
} from 'app/views/modules/CoreWorkerTasks/tabs/WorkerAvailableTasksBagsTabView/views/ModifyWorkerAvailableTasksBagsDialogContentFormView';
import {
    WorkerAvailableTasksBagsTabFilterFormView
} from 'app/views/modules/CoreWorkerTasks/tabs/WorkerAvailableTasksBagsTabView/views/WorkerAvailableTasksBagsTabFilterFormView';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { TablePagination } from 'app/views/modules/_common/components/CommonTableWithFilter/TableView';
import {
    WorkerAvailableTasksBagItemDataModel
} from 'app/web/InterlayerApiProxy/WorkerAvailableTasksBagsApiProxy/receiveWorkerAvailableTasksBagsData';
import { SelectDataCommonDataModel } from 'app/web/common/data-models';
import { SortingDataModel } from 'app/web/common/data-models/SortingDataModel';
import React from 'react';
import { connect } from 'react-redux';

type OwnProps = {
    canLoad: boolean;
};

type StateProps<TDataRow = WorkerAvailableTasksBagItemDataModel> = {
    dataset: TDataRow[];
    hasWorkerAvailableTasksBagsReceived: boolean;
    isInLoadingDataProcess: boolean;
    metadata: TablePagination;
};

type DispatchProps = {
    dispatchReceiveWorkerAvailableTasksBagsThunk: (args: ReceiveWorkerAvailableTasksBagsThunkParams) => void;
    dispatchChangeWorkerTaskBagPriorityThunk: (args: ChangeWorkerTaskBagPriorityThunkParams) => void;
};

type OwnState = {
    isAssignTasksToWorkerDialogOpen: boolean;
    selectedItem?: WorkerAvailableTasksBagItemDataModel;
};

type AllProps = OwnProps & StateProps & DispatchProps;

class WorkerAvailableTasksBagsTabViewClass extends React.Component<AllProps, OwnState> {
    private _commonTableWithFilterViewRef = React.createRef<CommonTableWithFilter<any, any>>();

    public constructor(props: AllProps) {
        super(props);

        this.onDataSelect = this.onDataSelect.bind(this);
        this.closeAssignTasksToWorkerDialog = this.closeAssignTasksToWorkerDialog.bind(this);
        this.openAssignTasksToWorkerDialog = this.openAssignTasksToWorkerDialog.bind(this);
        this.onConfirmOperationSuccessAssignTasksToWorkerDialog = this.onConfirmOperationSuccessAssignTasksToWorkerDialog.bind(this);

        this.state = {
            isAssignTasksToWorkerDialogOpen: false,
            selectedItem: void 0
        };
    }

    public shouldComponentUpdate(nextProps: AllProps, nextState: OwnState) {
        return hasComponentChangesFor(this.props, nextProps) ||
            hasComponentChangesFor(this.state, nextState);
    }

    public componentDidUpdate(prevProps: AllProps) {
        if (prevProps.canLoad !== this.props.canLoad && !this.props.hasWorkerAvailableTasksBagsReceived && this._commonTableWithFilterViewRef.current) {
            this._commonTableWithFilterViewRef.current.ReSelectData();
        }
    }

    private onConfirmOperationSuccessAssignTasksToWorkerDialog() {
        this.closeAssignTasksToWorkerDialog();
        if (this._commonTableWithFilterViewRef.current) {
            this._commonTableWithFilterViewRef.current.ReSelectData();
        }
    }

    private onDataSelect(args: SelectDataCommonDataModel<WorkerAvailableTasksBagsFilterDataForm>) {
        if (!this.props.canLoad) {
            return;
        }

        this.props.dispatchReceiveWorkerAvailableTasksBagsThunk({
            pageNumber: args.pageNumber,
            filter: args.filter,
            orderBy: args.sortingData ? this.getSortQuery(args.sortingData) : 'CoreWorker.CoreWorkerAddress.desc',
            force: true
        });
    }

    private getSortQuery(args: SortingDataModel) {
        switch (args.fieldName) {
            case 'workerId':
                return `CoreWorker.CoreWorkerAddress.${ args.sortKind ? 'desc' : 'asc' }`;
            case 'taskName':
                return `CoreWorkerTask.TaskName.${ args.sortKind ? 'desc' : 'asc' }`;
            case 'taskPriority':
                return `Priority.${ args.sortKind ? 'desc' : 'asc' }`;
            default:
                break;
        }
    }

    private openAssignTasksToWorkerDialog(item: WorkerAvailableTasksBagItemDataModel) {
        return () => {
            this.setState({
                isAssignTasksToWorkerDialogOpen: true,
                selectedItem: item
            });
        };
    }

    private closeAssignTasksToWorkerDialog() {
        this.setState({
            isAssignTasksToWorkerDialogOpen: false,
            selectedItem: undefined
        });
    }

    public render() {
        return (
            <>
                <CommonTableWithFilter
                    ref={ this._commonTableWithFilterViewRef }
                    uniqueContextProviderStateId="WorkerAvailableTasksBagsTabContextProviderStateId"
                    tableEditProps={
                        {
                            processProps: {
                                watch: {
                                    reducerStateName: 'WorkerAvailableTasksBagsReducerState',
                                    processIds: {
                                        modifyItemProcessId: WorkerAvailableTasksBagsProcessId.ChangeWorkerTaskBagPriority
                                    }
                                },
                                processMessages: {
                                    getOnSuccessModifyItemMessage: () => 'Изменения успешно сохранены.'
                                }
                            },
                            dialogProps: {
                                modifyDialog: {
                                    getDialogTitle: () => 'Смена приоритета для задачи',
                                    confirmModifyItemButtonText: 'Сохранить',
                                    getDialogContentFormView: (ref, item: WorkerAvailableTasksBagItemDataModel) => <ModifyWorkerAvailableTasksBagsDialogContentFormView
                                        ref={ ref }
                                        itemToEdit={ item }
                                    />,
                                    onModifyItemConfirmed: (itemToModify: WorkerAvailableTasksBagItemDataModel) => {
                                        this.props.dispatchChangeWorkerTaskBagPriorityThunk({
                                            taskId: itemToModify.taskId,
                                            workerId: itemToModify.workerId,
                                            priority: itemToModify.taskPriority,
                                            force: true
                                        });
                                    }
                                }
                            }
                        }
                    }
                    filterProps={ {
                        getFilterContentView: (ref, onFilterChanged) => this.props.canLoad
                            ? <WorkerAvailableTasksBagsTabFilterFormView ref={ ref } isFilterFormDisabled={ this.props.isInLoadingDataProcess } onFilterChanged={ onFilterChanged } />
                            : null
                    } }
                    tableProps={ {
                        dataset: this.props.dataset,
                        keyFieldName: 'id',
                        sorting: {
                            sortFieldName: 'workerId',
                            sortKind: SortingKind.Desc
                        },
                        fieldsView: {
                            workerId: {
                                caption: 'Название воркера',
                                isSortable: true,
                                fieldWidth: '40%',
                                format: (_value, row) => {
                                    return (
                                        <TextButton onClick={ this.openAssignTasksToWorkerDialog(row) }>
                                            <TextOut fontSize={ 13 } className="text-link">
                                                { row.workerName }
                                            </TextOut>
                                        </TextButton>
                                    );
                                }
                            },
                            taskName: {
                                caption: 'Название задачи',
                                isSortable: true,
                                fieldWidth: '40%'
                            },
                            taskPriority: {
                                caption: 'Приоритет задачи',
                                isSortable: true,
                                fieldWidth: '20%'
                            }
                        },
                        pagination: this.props.metadata
                    } }
                    onDataSelect={ this.onDataSelect }
                />
                <AssignTasksToWorkerDialog
                    isOpen={ this.state.isAssignTasksToWorkerDialogOpen }
                    onCancelClick={ this.closeAssignTasksToWorkerDialog }
                    onConfirmOperationSuccess={ this.onConfirmOperationSuccessAssignTasksToWorkerDialog }
                    selectedItem={ this.state.selectedItem! }
                />
            </>
        );
    }
}

export const WorkerAvailableTasksBagsTabView = connect<StateProps, DispatchProps, {}, AppReduxStoreState>(
    state => {
        const workerAvailableTasksBagsReducerState = state.WorkerAvailableTasksBagsReducerState;
        const { workerAvailableTasksBags } = workerAvailableTasksBagsReducerState;
        const dataset = workerAvailableTasksBags.items;

        const currentPage = workerAvailableTasksBags.metadata.pageNumber;
        const totalPages = workerAvailableTasksBags.metadata.pageCount;
        const { pageSize } = workerAvailableTasksBags.metadata;
        const recordsCount = workerAvailableTasksBags.metadata.totalItemCount;

        const { hasWorkerAvailableTasksBagsReceived } = workerAvailableTasksBagsReducerState.hasSuccessFor;

        const isInLoadingDataProcess = ProcessIdStateHelper.isInProgress(workerAvailableTasksBagsReducerState.reducerActions, WorkerAvailableTasksBagsProcessId.ReceiveWorkerAvailableTasksBagsData);

        return {
            dataset,
            hasWorkerAvailableTasksBagsReceived,
            isInLoadingDataProcess,
            metadata: {
                currentPage,
                totalPages,
                pageSize,
                recordsCount
            }
        };
    },
    {
        dispatchReceiveWorkerAvailableTasksBagsThunk: ReceiveWorkerAvailableTasksBagsThunk.invoke,
        dispatchChangeWorkerTaskBagPriorityThunk: ChangeWorkerTaskBagPriorityThunk.invoke,
    }
)(WorkerAvailableTasksBagsTabViewClass);