import { SortingKind } from 'app/common/enums';
import { hasComponentChangesFor } from 'app/common/functions';
import { ProcessIdStateHelper } from 'app/common/helpers/ProcessIdStateHelper';
import { CoreWorkerTasksProcessId } from 'app/modules/coreWorkerTasks';
import {
    ReceiveTasksControlDataThunkParams
} from 'app/modules/coreWorkerTasks/store/reducers/receiveTasksControlData/params/ReceiveTasksControlDataThunkParams';
import { ReceiveTasksControlDataThunk } from 'app/modules/coreWorkerTasks/store/thunks';
import { TaskControlCardProcessId } from 'app/modules/taskControlCard';
import {
    UpdateTaskControlCardThunkParams
} from 'app/modules/taskControlCard/store/reducers/updateTaskCotrolCardReducer/params/UpdateTaskControlCardParams';
import { UpdateTaskControlCardThunk } from 'app/modules/taskControlCard/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { TaskControlFilterDataForm } from 'app/views/modules/CoreWorkerTasks/tabs/TaskControlTabView/types';
import {
    ModifyTaskControlDialogContentFormView
} from 'app/views/modules/CoreWorkerTasks/tabs/TaskControlTabView/views/ModifyTaskControlDialogContentFormView';
import {
    TaskControlFilterFormView
} from 'app/views/modules/CoreWorkerTasks/tabs/TaskControlTabView/views/TaskControlFilterFormView';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { TablePagination } from 'app/views/modules/_common/components/CommonTableWithFilter/TableView';
import { SelectDataCommonDataModel } from 'app/web/common/data-models';
import { TasksControlItemDataModel } from 'app/web/InterlayerApiProxy/CoreWorkerTasksApiProxy/receiveTasksControlData';
import React from 'react';
import { connect } from 'react-redux';

type OwnProps = {
    canLoad: boolean;
};

type StateProps<TDataRow = TasksControlItemDataModel> = {
    dataset: TDataRow[];
    hasTasksControlDataReceived: boolean;
    isInLoadingDataProcess: boolean;
    metadata: TablePagination;
};

type DispatchProps = {
    dispatchReceiveTasksControlDataThunk: (args: ReceiveTasksControlDataThunkParams) => void;
    dispatchUpdateTaskControlCardThunk: (args: UpdateTaskControlCardThunkParams) => void;
};

type AllProps = OwnProps & StateProps & DispatchProps;

class TaskControlTabViewClass extends React.Component<AllProps> {
    private _commonTableWithFilterViewRef = React.createRef<CommonTableWithFilter<any, any>>();

    public constructor(props: AllProps) {
        super(props);

        this.onDataSelect = this.onDataSelect.bind(this);
    }

    public shouldComponentUpdate(nextProps: AllProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    public componentDidUpdate(prevProps: AllProps) {
        if (prevProps.canLoad !== this.props.canLoad && !this.props.hasTasksControlDataReceived && this._commonTableWithFilterViewRef.current) {
            this._commonTableWithFilterViewRef.current.ReSelectData();
        }
    }

    private onDataSelect(args: SelectDataCommonDataModel<TaskControlFilterDataForm>) {
        if (!this.props.canLoad) {
            return;
        }

        this.props.dispatchReceiveTasksControlDataThunk({
            pageNumber: args.pageNumber,
            filter: args.filter,
            force: true
        });
    }

    public render() {
        return (
            <CommonTableWithFilter
                ref={ this._commonTableWithFilterViewRef }
                uniqueContextProviderStateId="TaskControlTabContextProviderStateId"
                tableEditProps={
                    {
                        processProps: {
                            watch: {
                                reducerStateName: 'TaskControlCardState',
                                processIds: {
                                    modifyItemProcessId: TaskControlCardProcessId.UpdateTaskControlCard
                                }
                            },
                            processMessages: {
                                getOnSuccessModifyItemMessage: () => 'Данные успешно сохранены.'
                            }
                        },
                        dialogProps: {
                            modifyDialog: {
                                getDialogTitle: () => 'Редактирование задачи',
                                confirmModifyItemButtonText: 'Сохранить',
                                getDialogContentFormView: (ref, item: TasksControlItemDataModel) => <ModifyTaskControlDialogContentFormView
                                    ref={ ref }
                                    itemToEdit={ item }
                                />,
                                onModifyItemConfirmed: (itemToModify: TasksControlItemDataModel) => {
                                    this.props.dispatchUpdateTaskControlCardThunk({
                                        ...itemToModify,
                                        force: true
                                    });
                                }
                            }
                        }
                    }
                }
                filterProps={ {
                    getFilterContentView: (ref, onFilterChanged) => <TaskControlFilterFormView ref={ ref } isFilterFormDisabled={ this.props.isInLoadingDataProcess } onFilterChanged={ onFilterChanged } />
                } }
                tableProps={ {
                    dataset: this.props.dataset,
                    keyFieldName: 'taskName',
                    sorting: {
                        sortFieldName: 'taskName',
                        sortKind: SortingKind.Asc
                    },
                    fieldsView: {
                        taskName: {
                            caption: 'Название задачи',
                            isSortable: false,
                            fieldContentNoWrap: true
                        },
                        taskExecutionLifetimeInMinutes: {
                            caption: 'Время жизни выполнения задачи в минутах',
                            isSortable: false,
                            fieldContentNoWrap: true
                        }
                    },
                    pagination: this.props.metadata
                } }
                onDataSelect={ this.onDataSelect }
            />
        );
    }
}

export const TaskControlTabView = connect<StateProps, DispatchProps, {}, AppReduxStoreState>(
    state => {
        const workerTasksState = state.CoreWorkerTasksState;
        const dataset = workerTasksState.tasksControlData.items;

        const currentPage = workerTasksState.tasksControlData.metadata.pageNumber;
        const totalPages = workerTasksState.tasksControlData.metadata.pageCount;
        const { pageSize } = workerTasksState.tasksControlData.metadata;
        const recordsCount = workerTasksState.tasksControlData.metadata.totalItemCount;

        const { hasTasksControlDataReceived } = workerTasksState.hasSuccessFor;
        const isInLoadingDataProcess = ProcessIdStateHelper.isInProgress(workerTasksState.reducerActions, CoreWorkerTasksProcessId.ReceiveTasksControlData);

        return {
            dataset,
            hasTasksControlDataReceived,
            isInLoadingDataProcess,
            metadata: {
                currentPage,
                totalPages,
                pageSize,
                recordsCount
            }
        };
    },
    {
        dispatchReceiveTasksControlDataThunk: ReceiveTasksControlDataThunk.invoke,
        dispatchUpdateTaskControlCardThunk: UpdateTaskControlCardThunk.invoke,
    }
)(TaskControlTabViewClass);