import { hasComponentChangesFor } from 'app/common/functions';
import { ReceiveCoreWorkerTasksDataThunk } from 'app/modules/coreWorkerTasks/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { ComboBoxForm } from 'app/views/components/controls/forms';
import { ComboboxItemModel } from 'app/views/components/controls/forms/ComboBox/models';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { TaskControlFilterDataForm } from 'app/views/modules/CoreWorkerTasks/tabs/TaskControlTabView/types';
import {
    TaskControlFilterDataFormFieldNamesType
} from 'app/views/modules/CoreWorkerTasks/tabs/TaskControlTabView/types/TaskControlFilterDataForm';
import { KeyValueDataModel } from 'app/web/common/data-models';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';
import React from 'react';
import { connect } from 'react-redux';

/**
 * Свойства для компонента фильтра
 */
type OwnProps = ReduxFormProps<TaskControlFilterDataForm> & {
    onFilterChanged: (filter: TaskControlFilterDataForm, filterFormName: string) => void;
    isFilterFormDisabled: boolean;
};

type StateProps = {
    availableTasks: Array<KeyValueDataModel<string, string>>
};

type DispatchProps = {
    dispatchReceiveCoreWorkerTasksThunk: (args?: IForceThunkParam) => void;
};

type AllProps = OwnProps & StateProps & DispatchProps;

class TaskControlFilterFormViewClass extends React.Component<AllProps> {
    public constructor(props: AllProps) {
        super(props);
        this.initReduxForm = this.initReduxForm.bind(this);
        this.onValueChange = this.onValueChange.bind(this);
        this.onValueApplied = this.onValueApplied.bind(this);
        this.getCoreWorkerTasksOptions = this.getCoreWorkerTasksOptions.bind(this);
    }

    public componentDidMount() {
        this.props.reduxForm.setInitializeFormDataAction(this.initReduxForm);
        this.props.dispatchReceiveCoreWorkerTasksThunk();
    }

    public shouldComponentUpdate(nextProps: AllProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private onValueApplied<TValue>(formName: string, value: TValue) {
        this.props.onFilterChanged({
            ...this.props.reduxForm.getReduxFormFields(false),
            [formName]: value
        }, formName);
    }

    private onValueChange<TValue, TForm extends string = TaskControlFilterDataFormFieldNamesType>(formName: TForm, newValue: TValue) {
        this.props.reduxForm.updateReduxFormFields({
            [formName]: newValue
        });
    }

    private getCoreWorkerTasksOptions(): ComboboxItemModel<string>[] {
        const options = this.props.availableTasks.map(action => {
            return {
                value: action.key,
                text: action.value
            };
        });
        options.unshift({
            value: '',
            text: 'Все'
        });
        return options;
    }

    private initReduxForm(): TaskControlFilterDataForm | undefined {
        if (!this.props.availableTasks.length) {
            return;
        }

        return {
            taskId: ''
        };
    }

    public render() {
        if (!this.props.availableTasks.length) {
            return null;
        }

        const formFields = this.props.reduxForm.getReduxFormFields(false);

        return (
            <ComboBoxForm
                label="Задача"
                formName="taskId"
                value={ formFields.taskId }
                onValueChange={ this.onValueChange }
                onValueApplied={ this.onValueApplied }
                autoFocus={ true }
                items={ this.getCoreWorkerTasksOptions() }
                isReadOnly={ this.props.isFilterFormDisabled }
                allowAutoComplete={ true }
            />
        );
    }
}

const TaskControlFilterFormViewConnected = connect<StateProps, DispatchProps, {}, AppReduxStoreState>(
    state => {
        const tasksState = state.CoreWorkerTasksState;
        const availableTasks = tasksState.tasks.items;

        return {
            availableTasks
        };
    },
    {
        dispatchReceiveCoreWorkerTasksThunk: ReceiveCoreWorkerTasksDataThunk.invoke,
    }
)(TaskControlFilterFormViewClass);

export const TaskControlFilterFormView = withReduxForm(TaskControlFilterFormViewConnected, {
    reduxFormName: 'TaskControl_Filter_Form',
    resetOnUnmount: true
});