import { Nullable } from 'app/common/types';

/**
 * Модель Redux формы редактирования фильтра для таблицы взаимодействия воркера и задач
 */
export type WorkerAvailableTasksBagsFilterDataForm = {

    /**
     * ID воркера
     */
    workerId: Nullable<number>;

    /**
     * ID задачи
     */
    taskId: Nullable<string>;

    /**
     * Приоритет задачи
     */
    taskPriority: Nullable<number>;
};