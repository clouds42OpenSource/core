export type AddTaskToQueueDataForm = {
    /**
     * ID задачи
     */
    taskId: string,

    /**
     * Параметры задачи
     */
    taskParams: string,

    /**
     * Комментарий к задаче
     */
    comment: string
};

export type AddTaskToQueueDataFormFieldNamesType = keyof AddTaskToQueueDataForm;