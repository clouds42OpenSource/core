import { WorkerTasksBagsControlCardProcessId } from 'app/modules/workerTasksBagsControlCard';
import { ChangeTasksBagsThunkParams } from 'app/modules/workerTasksBagsControlCard/store/reducers/changeTasksBags/params';
import { ChangeWorkerTasksBagsThunk } from 'app/modules/workerTasksBagsControlCard/store/thunks/ChangeWorkerTasksBagsThunk';
import { AppReduxStoreState } from 'app/redux/types';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { WorkerTasksBagsControlDataForm } from 'app/views/modules/CoreWorkerTasks/tabs/WorkerAvailableTasksBagsTabView/types';
import { AssignTasksToWorkerDialogContentFormView } from 'app/views/modules/CoreWorkerTasks/tabs/WorkerAvailableTasksBagsTabView/views/AssignTasksToWorkerDialog/AssignTasksToWorkerDialogContentFormView';
import { CommonDialog } from 'app/views/modules/_common/components/dialogs/CommonDialog';
import { WorkerAvailableTasksBagItemDataModel } from 'app/web/InterlayerApiProxy/WorkerAvailableTasksBagsApiProxy/receiveWorkerAvailableTasksBagsData';
import React from 'react';
import { connect } from 'react-redux';

type OwnProps = FloatMessageProps & {
    onCancelClick: () => void;
    isOpen: boolean;
    onConfirmOperationSuccess: () => void;
    selectedItem: WorkerAvailableTasksBagItemDataModel;
};

type DispatchProps = {
    dispatchChangeTasksBagsThunk: (args: ChangeTasksBagsThunkParams) => void;
};

type AllProps = OwnProps & DispatchProps;

class AssignTasksToWorkerDialogClass extends React.Component<AllProps> {
    public constructor(props: AllProps) {
        super(props);

        this.onConfirmClick = this.onConfirmClick.bind(this);
    }

    private onConfirmClick<TDataModel>(formData: TDataModel) {
        const model = formData as unknown as WorkerTasksBagsControlDataForm;
        const workerTasksBagsIds = model.workerTasksBags.map(item => {
            return item.key;
        });

        if (!workerTasksBagsIds.length) {
            this.props.floatMessage.show(MessageType.Error, 'Не возможно удалить все доступные задачи воркера');
            return;
        }

        this.props.dispatchChangeTasksBagsThunk({
            coreWorkerId: model.coreWorkerId,
            workerTasksBagsIds,
            force: true
        });
    }

    public render() {
        return (
            <CommonDialog
                watch={ {
                    reducerStateName: 'WorkerTasksBagsControlCardState',
                    processId: WorkerTasksBagsControlCardProcessId.ChangeWorkerTasksBags,
                    getOnSuccessMessage: () => 'Изменения успешно сохранены.',
                    onSuccessHandler: this.props.onConfirmOperationSuccess
                } }
                dialogProps={
                    {
                        dataModel: this.props.selectedItem,
                        dialogWidth: 'md',
                        dialogTitleFontSize: 20,
                        dialogTitle: this.props.selectedItem?.workerName,
                        confirmButtonText: 'Сохранить',
                        dialogTitleTextAlign: 'center',
                        onCancelClick: this.props.onCancelClick,
                        isOpen: this.props.isOpen,
                        onConfirmClick: this.onConfirmClick
                    }
                }
            >
                { ref => <AssignTasksToWorkerDialogContentFormView ref={ ref } itemToEdit={ this.props.selectedItem } /> }
            </CommonDialog>
        );
    }
}

const AssignTasksToWorkerDialogConnected = connect<{}, DispatchProps, OwnProps, AppReduxStoreState>(
    null,
    {
        dispatchChangeTasksBagsThunk: ChangeWorkerTasksBagsThunk.invoke,
    }
)(AssignTasksToWorkerDialogClass);

export const AssignTasksToWorkerDialog = withFloatMessages(AssignTasksToWorkerDialogConnected);