import { Grid } from '@mui/material';
import { CoreWorkerTaskInQueueStatusLabels } from 'app/common/enums';
import { getSelectOptionsFromEnumLabels, hasComponentChangesFor } from 'app/common/functions';
import { ReceiveCoreWorkerTasksDataThunk } from 'app/modules/coreWorkerTasks/store/thunks';
import { ReceiveWorkersInSystemDataThunk } from 'app/modules/workers/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { DateUtility } from 'app/utils';
import { TableFilterWrapper } from 'app/views/components/_hoc/tableFilterWrapper';
import { FilterItemWrapper } from 'app/views/components/_hoc/tableFilterWrapper/filterItemWrapper';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { ComboBoxForm } from 'app/views/components/controls/forms';
import { ComboboxItemModel } from 'app/views/components/controls/forms/ComboBox/models';
import { DoubleDateInputForm } from 'app/views/components/controls/forms/DoubleDateInputView';
import { TextBoxForm } from 'app/views/components/controls/forms/TextBoxForm';
import {
    TaskInQueueFilterDataForm,
    TaskInQueueFilterDataFormFieldNamesType
} from 'app/views/modules/CoreWorkerTasks/tabs/TaskInQueueTabView/types';
import { WorkerDescriptionDataModel } from 'app/web/InterlayerApiProxy/WorkersApiProxy/receiveWorkersData';
import { KeyValueDataModel } from 'app/web/common/data-models';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';
import React from 'react';
import { connect } from 'react-redux';

/**
 * Свойства для компонента фильтра
 */
type OwnProps = ReduxFormProps<TaskInQueueFilterDataForm> & {
    onFilterChanged: (filter: TaskInQueueFilterDataForm, filterFormName: string) => void;
    isFilterFormDisabled: boolean;
};

type StateProps = {
    availableWorkers: Array<WorkerDescriptionDataModel>;
    coreWorkerTasks: Array<KeyValueDataModel<string, string>>;
};

type DispatchProps = {
    dispatchReceiveWorkersDataThunk: (args?: IForceThunkParam) => void;
    dispatchReceiveCoreWorkerTasks: (args?: IForceThunkParam) => void;
};

type AllProps = OwnProps & StateProps & DispatchProps;

class TaskInQueueFilterFormViewClass extends React.Component<AllProps> {
    public constructor(props: AllProps) {
        super(props);
        this.initReduxForm = this.initReduxForm.bind(this);
        this.onValueChange = this.onValueChange.bind(this);
        this.onValueApplied = this.onValueApplied.bind(this);
        this.getAvailableWorkersSelectOptions = this.getAvailableWorkersSelectOptions.bind(this);
        this.getWorkerTasksSelectOptions = this.getWorkerTasksSelectOptions.bind(this);
    }

    public componentDidMount() {
        this.props.reduxForm.setInitializeFormDataAction(this.initReduxForm);

        this.props.dispatchReceiveWorkersDataThunk();
        this.props.dispatchReceiveCoreWorkerTasks();
    }

    public shouldComponentUpdate(nextProps: AllProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private onValueChange<TValue, TForm extends string = TaskInQueueFilterDataFormFieldNamesType>(formName: TForm, newValue: TValue) {
        this.props.reduxForm.updateReduxFormFields({
            [formName]: newValue
        });
        if (formName === 'searchString' && newValue && (newValue as unknown as string).length < 3) {
            return false;
        }
    }

    private onValueApplied<TValue>(formName: string, value: TValue) {
        this.props.onFilterChanged({
            ...this.props.reduxForm.getReduxFormFields(false),
            [formName]: value
        }, formName);
    }

    private getAvailableWorkersSelectOptions(): ComboboxItemModel<string>[] {
        return this.props.availableWorkers.map(workerItem => {
            const workerId = workerItem.id.toString();
            return {
                value: workerId,
                text: `[${ workerId }] ${ workerItem.name ?? 'core-worker' }`
            };
        });
    }

    private getWorkerTasksSelectOptions(): ComboboxItemModel<string>[] {
        return this.props.coreWorkerTasks.map(taskItem => {
            return {
                value: taskItem.key,
                text: taskItem.value
            };
        });
    }

    private initReduxForm(): TaskInQueueFilterDataForm | undefined {
        return {
            periodFrom: DateUtility.setDayTimeToMidnight(DateUtility.getSomeMonthAgoDate(3)),
            periodTo: DateUtility.getTomorrow(new Date()),
            searchString: null,
            status: null,
            taskId: null,
            workerId: null
        };
    }

    public render() {
        if (!this.props.availableWorkers.length || !this.props.coreWorkerTasks.length) {
            return null;
        }

        const formFields = this.props.reduxForm.getReduxFormFields(false);
        return (
            <>
                <TableFilterWrapper>
                    <FilterItemWrapper width="350px">
                        <ComboBoxForm
                            label="Номер воркера"
                            formName="workerId"
                            value={ formFields.workerId ?? '' }
                            onValueChange={ this.onValueChange }
                            onValueApplied={ this.onValueApplied }
                            items={ [
                                { value: '', text: 'Все' },
                                ...this.getAvailableWorkersSelectOptions()
                            ] }
                            isReadOnly={ this.props.isFilterFormDisabled }
                        />
                    </FilterItemWrapper>
                    <FilterItemWrapper width="350px">
                        <ComboBoxForm
                            label="Задача"
                            formName="taskId"
                            onValueChange={ this.onValueChange }
                            onValueApplied={ this.onValueApplied }
                            value={ formFields.taskId ?? '' }
                            items={ [
                                { value: '', text: 'Все' },
                                ...this.getWorkerTasksSelectOptions()
                            ] }
                            isReadOnly={ this.props.isFilterFormDisabled }
                            allowAutoComplete={ true }
                        />
                    </FilterItemWrapper>
                    <FilterItemWrapper width="350px">
                        <ComboBoxForm
                            label="Статус"
                            value={ formFields.status ?? '' }
                            formName="status"
                            onValueChange={ this.onValueChange }
                            onValueApplied={ this.onValueApplied }
                            items={ getSelectOptionsFromEnumLabels(CoreWorkerTaskInQueueStatusLabels, { value: '', text: 'Все' }) }
                            isReadOnly={ this.props.isFilterFormDisabled }
                        />
                    </FilterItemWrapper>
                    <DoubleDateInputForm
                        label="Период поиска"
                        onValueChange={ this.onValueChange }
                        onValueApplied={ this.onValueApplied }
                        periodFromInputName="periodFrom"
                        periodToInputName="periodTo"
                        periodToValue={ formFields.periodTo }
                        periodFromValue={ formFields.periodFrom }
                        isReadOnly={ this.props.isFilterFormDisabled }
                    />

                </TableFilterWrapper>
                <Grid container={ true } sx={ { marginTop: '15px' } }>
                    <Grid item={ true } xs={ 6 } sm={ 9 }>
                        <TextBoxForm
                            autoFocus={ true }
                            label="В параметрах задачи содержится"
                            formName="searchString"
                            onValueChange={ this.onValueChange }
                            onValueApplied={ this.onValueApplied }
                            value={ formFields.searchString ?? '' }
                            isReadOnly={ this.props.isFilterFormDisabled }
                        />
                    </Grid>
                </Grid>
            </>
        );
    }
}

const TaskInQueueFilterFormViewConnected = connect<StateProps, DispatchProps, {}, AppReduxStoreState>(
    state => {
        const workersState = state.WorkersState;
        const availableWorkers = workersState.availableWorkers.items;

        const coreWorkerTasksState = state.CoreWorkerTasksState;
        const coreWorkerTasks = coreWorkerTasksState.tasks.items;

        return {
            availableWorkers,
            coreWorkerTasks
        };
    },
    {
        dispatchReceiveWorkersDataThunk: ReceiveWorkersInSystemDataThunk.invoke,
        dispatchReceiveCoreWorkerTasks: ReceiveCoreWorkerTasksDataThunk.invoke
    }
)(TaskInQueueFilterFormViewClass);

export const TaskInQueueFilterFormView = withReduxForm(TaskInQueueFilterFormViewConnected, {
    reduxFormName: 'TasksInQueue_Filter_Form',
    resetOnUnmount: true
});