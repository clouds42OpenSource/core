import { AccountUserGroup, MapTaskStatusToDescription, MapTaskStatusToLabelType, SortingKind } from 'app/common/enums';
import { hasComponentChangesFor } from 'app/common/functions';
import { ProcessIdStateHelper } from 'app/common/helpers/ProcessIdStateHelper';
import { TaskInQueueItemCardProcessId } from 'app/modules/taskInQueueItemCard';
import {
    AddTaskToQueueCardThunkParams
} from 'app/modules/taskInQueueItemCard/store/reducers/addTaskToQueueReducer/params';
import { AddTaskToQueueThunk } from 'app/modules/taskInQueueItemCard/store/thunks';
import { TasksProcessId } from 'app/modules/tasksTable';
import {
    ReceiveTasksInQueueDataThunkParams
} from 'app/modules/tasksTable/store/reducers/receiveTasksInQueueData/params';
import { ReceiveTasksInQueueDataThunk } from 'app/modules/tasksTable/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { DateUtility } from 'app/utils';
import { TextButton } from 'app/views/components/controls/Button';
import { StatusLabel } from 'app/views/components/controls/Labels';
import { StatusLabelTypes } from 'app/views/components/controls/Labels/views/StatusLabelView/types';
import { TextOut } from 'app/views/components/TextOut';
import { SortingDataModel } from 'app/web/common/data-models/SortingDataModel';

import {
    AddTaskToQueueDataForm,
    TaskInQueueFilterDataForm
} from 'app/views/modules/CoreWorkerTasks/tabs/TaskInQueueTabView/types';
import {
    AddTaskInQueueDialogContentFormView
} from 'app/views/modules/CoreWorkerTasks/tabs/TaskInQueueTabView/views/AddTaskInQueueDialogContentFormView';
import {
    TaskInQueueDetailsDialog
} from 'app/views/modules/CoreWorkerTasks/tabs/TaskInQueueTabView/views/TaskInQueueDetailsDialog';
import {
    TaskInQueueFilterFormView
} from 'app/views/modules/CoreWorkerTasks/tabs/TaskInQueueTabView/views/TaskInQueueFilterFormView';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { TablePagination } from 'app/views/modules/_common/components/CommonTableWithFilter/TableView';
import { SelectDataCommonDataModel } from 'app/web/common/data-models';
import { TaskInQueueItemDataModel } from 'app/web/InterlayerApiProxy/TasksApiProxy/receiveTasksData';
import React from 'react';
import { connect } from 'react-redux';

type OwnProps = {
    isAddTaskButtonVisible: boolean;
};

type StateProps<TDataRow = TaskInQueueItemDataModel> = {
    dataset: TDataRow[];
    isInLoadingDataProcess: boolean;
    metadata: TablePagination;
    currentUserGroups: AccountUserGroup[];
};

type DispatchProps = {
    dispatchReceiveTasksInQueueDataThunk: (args: ReceiveTasksInQueueDataThunkParams) => void;
    dispatchAddTaskToQueueThunk: (args: AddTaskToQueueCardThunkParams) => void;
};

type AllProps = OwnProps & StateProps & DispatchProps;

type OwnState = {
    isTaskCardVisible: boolean;
    taskInQueueId: string;
    taskName: string;
};

class TaskInQueueTabViewClass extends React.Component<AllProps, OwnState> {
    public constructor(props: AllProps) {
        super(props);

        this.onDataSelect = this.onDataSelect.bind(this);
        this.showTaskInQueueItemCard = this.showTaskInQueueItemCard.bind(this);
        this.getStatusLabelType = this.getStatusLabelType.bind(this);
        this.getTaskStatusDescription = this.getTaskStatusDescription.bind(this);
        this.closeTaskInQueueItemCard = this.closeTaskInQueueItemCard.bind(this);

        this.state = {
            isTaskCardVisible: false,
            taskInQueueId: '',
            taskName: ''
        };
    }

    public shouldComponentUpdate(nextProps: AllProps, nextState: OwnState) {
        return hasComponentChangesFor(this.props, nextProps) ||
            hasComponentChangesFor(this.state, nextState);
    }

    private onDataSelect(args: SelectDataCommonDataModel<TaskInQueueFilterDataForm>) {
        this.props.dispatchReceiveTasksInQueueDataThunk({
            pageNumber: args.pageNumber,
            filter: args.filter,
            orderBy: args.sortingData ? this.getSortQuery(args.sortingData) : 'date.desc',
            force: true
        });
    }

    private getSortQuery(args: SortingDataModel) {
        switch (args.fieldName) {
            case 'name':
                return `coreWorkerTask.taskName.${ args.sortKind ? 'desc' : 'asc' }`;
            case 'status':
                return `status.${ args.sortKind ? 'desc' : 'asc' }`;
            case 'createDateTime':
                return `createDate.${ args.sortKind ? 'desc' : 'asc' }`;
            case 'finishDateTime':
                return `editDate.${ args.sortKind ? 'desc' : 'asc' }`;
            case 'workerId':
                return `capturedWorkerId.${ args.sortKind ? 'desc' : 'asc' }`;
            default:
                return 'date.desc';
        }
    }

    private getTaskStatusDescription(status: string): string {
        return MapTaskStatusToDescription[status];
    }

    private getStatusLabelType(status: string): StatusLabelTypes {
        const type = MapTaskStatusToLabelType[status];

        if (type) {
            return type as StatusLabelTypes;
        }

        return 'success';
    }

    private closeTaskInQueueItemCard() {
        this.setState({
            isTaskCardVisible: false
        });
    }

    private showTaskInQueueItemCard(taskInQueue: TaskInQueueItemDataModel) {
        return () => {
            this.setState({
                isTaskCardVisible: true,
                taskInQueueId: taskInQueue.id,
                taskName: taskInQueue.name
            });
        };
    }

    public render() {
        return (
            <>
                <CommonTableWithFilter
                    uniqueContextProviderStateId="TaskInQueueTabContextProviderStateId"
                    isResponsiveTable={ false }
                    tableEditProps={
                        {
                            processProps: {
                                watch: {
                                    reducerStateName: 'TaskInQueueItemCardState',
                                    processIds: {
                                        addNewItemProcessId: TaskInQueueItemCardProcessId.AddTaskToQueue
                                    }
                                },
                                processMessages: {
                                    getOnSuccessAddNewItemMessage: () => 'Задача добавлена в очередь.'
                                }
                            },

                            dialogProps: {
                                addDialog:
                                    this.props.isAddTaskButtonVisible
                                        ? {
                                            dialogWidth: 'sm',
                                            dialogVerticalAlign: 'top',
                                            dialogTitleTextAlign: 'center',
                                            dialogTitleFontSize: 20,
                                            getDialogTitle: () => 'Новая задача',
                                            getDialogContentFormView: ref =>
                                                <AddTaskInQueueDialogContentFormView ref={ ref } />,
                                            confirmAddItemButtonText: 'Добавить',
                                            showAddDialogButtonText: 'Добавить задачу',
                                            onAddNewItemConfirmed: itemToAdd => {
                                                this.props.dispatchAddTaskToQueueThunk({
                                                    ...itemToAdd as unknown as AddTaskToQueueDataForm,
                                                    force: true
                                                });
                                            }
                                        }
                                        : void 0
                            }
                        }
                    }
                    filterProps={ {
                        getFilterContentView: (ref, onFilterChanged) =>
                            <TaskInQueueFilterFormView ref={ ref } isFilterFormDisabled={ this.props.isInLoadingDataProcess } onFilterChanged={ onFilterChanged } />
                    } }
                    tableProps={ {
                        dataset: this.props.dataset,
                        keyFieldName: 'id',
                        sorting: {
                            sortFieldName: 'createDateTime',
                            sortKind: SortingKind.Desc
                        },
                        fieldsView: {
                            name: {
                                caption: 'Название',
                                isSortable: true,
                                format: (value, row) => {
                                    const { currentUserGroups } = this.props;

                                    if (currentUserGroups.includes(AccountUserGroup.CloudAdmin)) {
                                        return (
                                            <TextOut style={ { cursor: 'pointer', textWrap: 'nowrap' } } onClick={ this.showTaskInQueueItemCard(row) } fontSize={ 13 } className="text-link">
                                                { value }
                                            </TextOut>
                                        );
                                    }

                                    return value;
                                }
                            },
                            status: {
                                caption: 'Статус',
                                isSortable: true,
                                format: (value: string) => (
                                    <StatusLabel
                                        text={ this.getTaskStatusDescription(value) }
                                        type={ this.getStatusLabelType(value) }
                                        style={ { height: '20px' } }
                                    />
                                )
                            },
                            createDateTime: {
                                caption: 'Дата создания',
                                isSortable: true,
                                format: (value: Date | null) => {
                                    return value ? DateUtility.dateToDayMonthYearHourMinute(value) : value;
                                }
                            },
                            finishDateTime: {
                                caption: 'Дата выполнения',
                                isSortable: true,
                                format: (value: Date | null) => {
                                    return value ? DateUtility.dateToDayMonthYearHourMinute(value) : value;
                                }
                            },
                            comment: {
                                caption: 'Комментарий',
                                isSortable: false,
                            },
                            workerId: {
                                caption: '№ воркера',
                                isSortable: true,
                                fieldWidth: '2%',
                            }
                        },
                        pagination: this.props.metadata
                    } }
                    onDataSelect={ this.onDataSelect }
                />
                <TaskInQueueDetailsDialog
                    isOpen={ this.state.isTaskCardVisible }
                    onCancelDialogButtonClick={ this.closeTaskInQueueItemCard }
                    taskInQueueItemId={ this.state.taskInQueueId }
                    taskName={ this.state.taskName }
                />
            </>
        );
    }
}

export const TaskInQueueTabView = connect<StateProps, DispatchProps, {}, AppReduxStoreState>(
    state => {
        const tasksInQueueState = state.TasksInQueueState;
        const { tasksInQueue } = tasksInQueueState;
        const dataset = tasksInQueue.items;

        const currentPage = tasksInQueue.metadata.pageNumber;
        const totalPages = tasksInQueue.metadata.pageCount;
        const { pageSize } = tasksInQueue.metadata;
        const recordsCount = tasksInQueue.metadata.totalItemCount;

        const isInLoadingDataProcess = ProcessIdStateHelper.isInProgress(tasksInQueueState.reducerActions, TasksProcessId.ReceiveTasksInQueueData);

        const { currentUserGroups } = state.Global.getCurrentSessionSettingsReducer.settings.currentContextInfo;

        return {
            dataset,
            isInLoadingDataProcess,
            currentUserGroups,
            metadata: {
                currentPage,
                totalPages,
                pageSize,
                recordsCount
            }
        };
    },
    {
        dispatchReceiveTasksInQueueDataThunk: ReceiveTasksInQueueDataThunk.invoke,
        dispatchAddTaskToQueueThunk: AddTaskToQueueThunk.invoke,
    }
)(TaskInQueueTabViewClass);