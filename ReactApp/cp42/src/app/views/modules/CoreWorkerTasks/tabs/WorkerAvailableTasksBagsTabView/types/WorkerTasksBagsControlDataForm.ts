import { KeyValueDataModel } from 'app/web/common/data-models';

export type WorkerTasksBagsControlDataForm = {
    /**
     * ID воркера
     */
    coreWorkerId: number,

    /**
     * Собственные задачи воркера
     */
    workerTasksBags: Array<KeyValueDataModel<string, string>>,

    /**
     * Доступные задачи для добавления
     */
    availableTasksForAdd: Array<KeyValueDataModel<string, string>>
};