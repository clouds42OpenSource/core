import { hasComponentChangesFor } from 'app/common/functions';
import { Dialog } from 'app/views/components/controls/Dialog';
import {
    TaskInQueueDetailsDialogContentView
} from 'app/views/modules/CoreWorkerTasks/tabs/TaskInQueueTabView/views/TaskInQueueDetailsDialog/TaskInQueueDetailsDialogContentView';
import React from 'react';

type OwnProps = {
    isOpen: boolean;
    onCancelDialogButtonClick: () => void;
    taskInQueueItemId: string;
    taskName: string;
};

export class TaskInQueueDetailsDialog extends React.Component<OwnProps> {
    public shouldComponentUpdate(nextProps: OwnProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    public render() {
        return (
            <Dialog
                title={ this.props.taskName ?? '' }
                isOpen={ this.props.isOpen }
                dialogWidth="sm"
                isTitleSmall={ true }
                titleFontWeight={ 600 }
                titleFontSize={ 20 }
                onCancelClick={ this.props.onCancelDialogButtonClick }
                dialogVerticalAlign="top"
            >
                { this.props.isOpen &&
                    <TaskInQueueDetailsDialogContentView
                        onCancelDialogButtonClick={ this.props.onCancelDialogButtonClick }
                        taskInQueueItemId={ this.props.taskInQueueItemId }
                    />
                }
            </Dialog>
        );
    }
}