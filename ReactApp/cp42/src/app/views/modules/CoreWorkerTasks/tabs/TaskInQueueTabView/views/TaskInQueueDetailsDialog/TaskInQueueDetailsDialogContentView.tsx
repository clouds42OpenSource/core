import { Box } from '@mui/material';
import { CoreWorkerTaskInQueueStatusType } from 'app/common/enums';
import {
    CoreWorkerTaskInQueueTypeEnum,
    CoreWorkerTaskInQueueTypeLabels
} from 'app/common/enums/CoreWorkerTaskInQueueTypeEnum';
import { hasComponentChangesFor } from 'app/common/functions';
import {
    ReceiveTaskInQueueItemCardThunkParams
} from 'app/modules/taskInQueueItemCard/store/reducers/receiveTaskInQueueItemCardReducer/params';
import { ReceiveTaskInQueueItemCardThunk } from 'app/modules/taskInQueueItemCard/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { DateUtility } from 'app/utils';
import { TextOut } from 'app/views/components/TextOut';
import { ContainedButton } from 'app/views/components/controls/Button';
import { JsonViewer } from 'app/views/components/controls/JsonViewer';
import {
    CancelCoreWorkerTasksQueueDialog
} from 'app/views/modules/CoreWorkerTasks/tabs/TaskInQueueTabView/views/CancelCoreWorkerTasksQueueDialog/Index';
import { ReadOnlyKeyValueView } from 'app/views/modules/_common/components/ReadOnlyKeyValueView';
import {
    TaskInQueueItemCardDataModel
} from 'app/web/InterlayerApiProxy/TaskInQueueItemCardApiProxy/receiveTaskInQueueItemCard/data-models';
import cn from 'classnames';
import React from 'react';
import { connect } from 'react-redux';
import css from './styles.module.css';

type OwnProps = {
    taskInQueueItemId: string;
    onCancelDialogButtonClick: () => void;
};

type StateProps = {
    taskInQueueItemInfo: TaskInQueueItemCardDataModel;
    canCancelCoreWorkerTasksQueue: boolean;
};

type DispatchProps = {
    dispatchReceiveTaskInQueueItemCardThunk: (args?: ReceiveTaskInQueueItemCardThunkParams) => void;
};

type OwnState = {
    isCancelCoreWorkerTasksQueueDialogVisible: boolean;
    coreWorkerTasksQueueId: string;
    isTasksQueueCancellationCompleted: boolean;
};

type AllProps = OwnProps & StateProps & DispatchProps;

class TaskInQueueDetailsDialogContentViewClass extends React.Component<AllProps, OwnState> {
    public constructor(props: AllProps) {
        super(props);

        this.getTaskTypeDescription = this.getTaskTypeDescription.bind(this);
        this.showCancelCoreWorkerTasksQueueDialog = this.showCancelCoreWorkerTasksQueueDialog.bind(this);
        this.hideCancelCoreWorkerTasksQueueDialog = this.hideCancelCoreWorkerTasksQueueDialog.bind(this);
        this.onTasksQueueCancellationCompleted = this.onTasksQueueCancellationCompleted.bind(this);

        this.state = {
            isCancelCoreWorkerTasksQueueDialogVisible: false,
            coreWorkerTasksQueueId: '',
            isTasksQueueCancellationCompleted: false
        };
    }

    public componentDidMount() {
        this.props.dispatchReceiveTaskInQueueItemCardThunk({
            taskInQueueItemId: this.props.taskInQueueItemId,
            force: true
        });
    }

    public shouldComponentUpdate(nextProps: AllProps, nextState: OwnState) {
        return hasComponentChangesFor(this.props, nextProps) ||
            hasComponentChangesFor(this.state, nextState);
    }

    private onTasksQueueCancellationCompleted() {
        this.setState({
            isTasksQueueCancellationCompleted: true,
            isCancelCoreWorkerTasksQueueDialogVisible: false
        });
        this.props.onCancelDialogButtonClick();
    }

    private getTaskTypeDescription(): string {
        const typeDescription = CoreWorkerTaskInQueueTypeLabels[this.props.taskInQueueItemInfo.taskType];
        if (typeDescription) return typeDescription;
        return CoreWorkerTaskInQueueTypeLabels[CoreWorkerTaskInQueueTypeEnum.InternalTask];
    }

    private showCancelCoreWorkerTasksQueueDialog(coreWorkerTasksQueueId: string) {
        return () => {
            this.setState({
                isCancelCoreWorkerTasksQueueDialogVisible: true,
                coreWorkerTasksQueueId
            });
        };
    }

    private hideCancelCoreWorkerTasksQueueDialog() {
        this.setState({
            isCancelCoreWorkerTasksQueueDialogVisible: false,
            coreWorkerTasksQueueId: ''
        });
    }

    public render() {
        const item = this.props.taskInQueueItemInfo;

        if (!item) {
            return null;
        }

        const startDate = item.startDate
            ? DateUtility.dateToDayMonthYearHourMinuteSeconds(item.startDate)
            : '---';

        const isCancelTasksQueueDialogButtonVisible =
            this.props.taskInQueueItemInfo.status === CoreWorkerTaskInQueueStatusType[CoreWorkerTaskInQueueStatusType.Processing] ||
            this.props.taskInQueueItemInfo.status === CoreWorkerTaskInQueueStatusType[CoreWorkerTaskInQueueStatusType.NeedRetry] ||
            this.props.taskInQueueItemInfo.status === CoreWorkerTaskInQueueStatusType[CoreWorkerTaskInQueueStatusType.Captured] ||
            this.props.taskInQueueItemInfo.status === CoreWorkerTaskInQueueStatusType[CoreWorkerTaskInQueueStatusType.New];

        const canCancelCoreWorkerTasksQueue = this.props.canCancelCoreWorkerTasksQueue &&
            isCancelTasksQueueDialogButtonVisible && !this.state.isTasksQueueCancellationCompleted;

        return (
            <Box display="flex" flexDirection="column">
                <ReadOnlyKeyValueView
                    label="Дата запуска:"
                    text={ startDate }
                />
                <ReadOnlyKeyValueView
                    label="Комментарий:"
                    text={ item.comment }
                />
                <ReadOnlyKeyValueView
                    label="Тип задачи:"
                    text={ this.getTaskTypeDescription() }
                />
                <ReadOnlyKeyValueView
                    label="Приоритет:"
                    text={ item.priority === -1 ? '---' : item.priority }
                />
                <div>
                    <TextOut fontSize={ 13 } fontWeight={ 700 }>
                        Параметры задачи
                    </TextOut>
                    <TextOut inDiv={ true } fontSize={ 13 }>
                        <JsonViewer
                            jsonData={ item.taskParams }
                            displayDataTypes={ false }
                        />
                    </TextOut>
                </div>
                {
                    canCancelCoreWorkerTasksQueue
                        ? (
                            <div className={ cn(css['cancel-core-worker-tasks-queue']) }>
                                <ContainedButton
                                    onClick={ this.showCancelCoreWorkerTasksQueueDialog(this.props.taskInQueueItemInfo.taskInQueueItemId) }
                                    kind="error"
                                >
                                    Отменить задачу
                                </ContainedButton>
                            </div>
                        )
                        : null
                }

                <CancelCoreWorkerTasksQueueDialog
                    coreWorkerTasksQueueId={ this.state.coreWorkerTasksQueueId }
                    taskName={ this.props.taskInQueueItemInfo.taskName }
                    isOpen={ this.state.isCancelCoreWorkerTasksQueueDialogVisible }
                    onTasksQueueCancellationCompleted={ this.onTasksQueueCancellationCompleted }
                    onCancelDialogButtonClick={ this.hideCancelCoreWorkerTasksQueueDialog }
                />
            </Box>
        );
    }
}

export const TaskInQueueDetailsDialogContentView = connect<StateProps, DispatchProps, OwnProps, AppReduxStoreState>(
    state => {
        const taskInQueueItemCardState = state.TaskInQueueItemCardState;
        const taskInQueueItemInfo = taskInQueueItemCardState.details;
        const { canCancelCoreWorkerTasksQueue } = state.TasksPageState.tabVisibility;

        return {
            taskInQueueItemInfo,
            canCancelCoreWorkerTasksQueue
        };
    },
    {
        dispatchReceiveTaskInQueueItemCardThunk: ReceiveTaskInQueueItemCardThunk.invoke,
    }
)(TaskInQueueDetailsDialogContentViewClass);