import React from 'react';
import {
    CancelCoreWorkerTasksQueueDataForm
} from 'app/views/modules/CoreWorkerTasks/tabs/TaskInQueueTabView/views/CancelCoreWorkerTasksQueueDialog/types/CancelCoreWorkerTasksQueueDataForm';
import { IReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { hasComponentChangesFor } from 'app/common/functions';
import { Nullable } from 'app/common/types';
import {
    CancelCoreWorkerTasksQueueButton
} from 'app/views/modules/CoreWorkerTasks/tabs/TaskInQueueTabView/views/CancelCoreWorkerTasksQueueDialog/_buttons/CancelCoreWorkerTasksQueueButton';
import { Dialog } from 'app/views/components/controls/Dialog';
import {
    CancelCoreWorkerTasksQueueDialogContentView
} from 'app/views/modules/CoreWorkerTasks/tabs/TaskInQueueTabView/views/CancelCoreWorkerTasksQueueDialog/CancelCoreWorkerTasksQueueDialogContentView';

type OwnProps = {
    isOpen: boolean;
    onCancelDialogButtonClick: () => void;
    onTasksQueueCancellationCompleted: () => void
    taskName: string;
    coreWorkerTasksQueueId: string;
};

export class CancelCoreWorkerTasksQueueDialog extends React.Component<OwnProps> {
    private dataForm = React.createRef<IReduxForm<CancelCoreWorkerTasksQueueDataForm>>();

    public constructor(props: OwnProps) {
        super(props);
        this.getReasonCancellation = this.getReasonCancellation.bind(this);
        this.getDialogButtons = this.getDialogButtons.bind(this);
    }

    public shouldComponentUpdate(nextProps: OwnProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private getDialogButtons() {
        return (
            <CancelCoreWorkerTasksQueueButton
                key="CancelCoreWorkerTasksQueueButton"
                onSuccess={ this.props.onTasksQueueCancellationCompleted }
                getReasonCancellation={ this.getReasonCancellation }
                coreWorkerTasksQueueId={ this.props.coreWorkerTasksQueueId }
                isEnabled={ true }
            />
        );
    }

    private getReasonCancellation(): Nullable<string> {
        return this.dataForm.current!.getReduxForm().getReduxFormFields(false)?.reasonCancellation;
    }

    public render() {
        return (
            <Dialog
                title={ `Отмена задачи ${ this.props.taskName }` }
                isOpen={ this.props.isOpen }
                dialogWidth="sm"
                isTitleSmall={ true }
                titleFontSize={ 14 }
                dialogVerticalAlign="top"
                onCancelClick={ this.props.onCancelDialogButtonClick }
                buttons={ this.getDialogButtons }
            >

                <CancelCoreWorkerTasksQueueDialogContentView
                    key={ this.props.coreWorkerTasksQueueId }
                    ref={ this.dataForm }
                />
            </Dialog>
        );
    }
}