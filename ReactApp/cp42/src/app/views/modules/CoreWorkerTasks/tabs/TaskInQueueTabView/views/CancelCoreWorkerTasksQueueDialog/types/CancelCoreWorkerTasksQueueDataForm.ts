/**
 *  Форма отмены запущенной задачи
 */
export type CancelCoreWorkerTasksQueueDataForm = {
    /**
     * Причина отмены задачи
     */
    reasonCancellation: string | null;
};