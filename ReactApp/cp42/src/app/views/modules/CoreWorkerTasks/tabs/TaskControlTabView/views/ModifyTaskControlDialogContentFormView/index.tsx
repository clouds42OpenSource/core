import { hasComponentChangesFor } from 'app/common/functions';
import { createInt32Validator } from 'app/common/validators';
import { TextBoxForm } from 'app/views/components/controls/forms';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { TasksControlItemDataModel } from 'app/web/InterlayerApiProxy/CoreWorkerTasksApiProxy/receiveTasksControlData';
import React from 'react';
import { Box } from '@mui/material';

type OwnProps = ReduxFormProps<TasksControlItemDataModel> & {
    itemToEdit: TasksControlItemDataModel;
};

class ModifyTaskControlDialogContentFormViewClass extends React.Component<OwnProps> {
    public constructor(props: OwnProps) {
        super(props);
        this.onValueChange = this.onValueChange.bind(this);
        this.initReduxForm = this.initReduxForm.bind(this);
        this.props.reduxForm.setInitializeFormDataAction(this.initReduxForm);
    }

    public shouldComponentUpdate(nextProps: OwnProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private onValueChange<TValue, TForm extends string>(formName: TForm, newValue: TValue) {
        this.props.reduxForm.updateReduxFormFields({
            [formName]: newValue
        });
    }

    private initReduxForm(): TasksControlItemDataModel | undefined {
        if (!this.props.itemToEdit) {
            return;
        }

        return {
            taskId: this.props.itemToEdit.taskId,
            taskExecutionLifetimeInMinutes: this.props.itemToEdit.taskExecutionLifetimeInMinutes,
            taskName: this.props.itemToEdit.taskName
        };
    }

    public render() {
        const reduxForm = this.props.reduxForm.getReduxFormFields(false);

        return (
            <Box display="flex" flexDirection="column">
                <div>
                    <TextBoxForm
                        label="Задача:"
                        formName="taskName"
                        isReadOnly={ true }
                        value={ this.props.itemToEdit.taskName }
                    />
                </div>
                <div className="m-t-md">
                    <TextBoxForm
                        autoFocus={ true }
                        label="Время жизни выполнения задачи в минутах:"
                        formName="taskExecutionLifetimeInMinutes"
                        maxLength={ 50 }
                        isReadOnly={ false }
                        validator={ createInt32Validator({ negativeNumbersCanBeEntered: true }) }
                        value={ reduxForm.taskExecutionLifetimeInMinutes.toString() }
                        onValueChange={ this.onValueChange }
                    />
                </div>
            </Box>
        );
    }
}

export const ModifyTaskControlDialogContentFormView = withReduxForm(ModifyTaskControlDialogContentFormViewClass, {
    reduxFormName: 'ModifyTaskControlDialogContent_ReduxForm',
    resetOnUnmount: true
});