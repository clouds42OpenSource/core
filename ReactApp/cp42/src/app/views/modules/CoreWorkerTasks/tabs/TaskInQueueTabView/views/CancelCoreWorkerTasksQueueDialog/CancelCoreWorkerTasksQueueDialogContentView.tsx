import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import {
    CancelCoreWorkerTasksQueueDataForm
} from 'app/views/modules/CoreWorkerTasks/tabs/TaskInQueueTabView/views/CancelCoreWorkerTasksQueueDialog/types/CancelCoreWorkerTasksQueueDataForm';
import React from 'react';
import { hasComponentChangesFor } from 'app/common/functions';
import { TextBoxForm } from 'app/views/components/controls/forms/TextBoxForm';

type OwnProps = ReduxFormProps<CancelCoreWorkerTasksQueueDataForm>;

type StateProps = {};

type AllProps = OwnProps & StateProps;

class CancelCoreWorkerTasksQueueDialogContentViewClass extends React.Component<AllProps> {
    public constructor(props: AllProps) {
        super(props);
        this.updateReduxFormField = this.updateReduxFormField.bind(this);
        this.initReduxForm = this.initReduxForm.bind(this);
    }

    public componentDidMount() {
        this.props.reduxForm.setInitializeFormDataAction(this.initReduxForm);
    }

    public shouldComponentUpdate(nextProps: OwnProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private initReduxForm(): CancelCoreWorkerTasksQueueDataForm | undefined {
        return {
            reasonCancellation: null,
        };
    }

    private updateReduxFormField<TValue>(fieldName: string, newValue: TValue) {
        this.props.reduxForm.updateReduxFormFields({ [fieldName]: newValue });
    }

    public render() {
        const reasonCancellation = this.props.reduxForm.getReduxFormFields(false).reasonCancellation ?? '';

        return (
            <div>
                <TextBoxForm
                    label="Причина отмены задачи:"
                    value={ reasonCancellation }
                    onValueChange={ this.updateReduxFormField }
                    autoFocus={ true }
                    formName="reasonCancellation"
                />
            </div>
        );
    }
}

export const CancelCoreWorkerTasksQueueDialogContentView = withReduxForm(CancelCoreWorkerTasksQueueDialogContentViewClass, {
    reduxFormName: 'Cancel_Tasks_Queue_Form'
});