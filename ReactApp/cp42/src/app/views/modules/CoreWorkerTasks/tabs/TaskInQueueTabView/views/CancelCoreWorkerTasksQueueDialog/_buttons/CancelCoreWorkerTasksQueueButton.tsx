import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { connect } from 'react-redux';
import { AppReduxStoreState } from 'app/redux/types';
import { CancelCoreWorkerTasksQueueThunk } from 'app/modules/tasksTable/store/thunks';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import {
    CancelCoreWorkerTasksQueueThunkParams
} from 'app/modules/tasksTable/store/reducers/cancelCoreWorkerTasksQueueReducer/params';
import React from 'react';
import { hasComponentChangesFor } from 'app/common/functions';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { IReducerProcessActionInfo } from 'core/redux/interfaces';
import { TasksProcessId } from 'app/modules/tasksTable';
import { ContainedButton } from 'app/views/components/controls/Button';
import { WatchReducerProcessActionState } from 'app/views/modules/_common/components/WatchReducerProcessActionState';
import { Nullable } from 'app/common/types';

type OwnProps = FloatMessageProps & {
    isEnabled?: boolean;
    onSuccess?: () => void;
    showSuccessMessage?: boolean;
    showErrorMessage?: boolean;
    getReasonCancellation: () => Nullable<string>;
    coreWorkerTasksQueueId: string;
};

type OwnState = {
    cancelCoreWorkerTasksQueueState: {
        isInProgressState: boolean;
        isInSuccessState: boolean;
        isInErrorState: boolean;
        error?: Error;
    }
};

type DispatchProps = {
    dispatchCancelCoreWorkerTasksQueueThunk: (args: CancelCoreWorkerTasksQueueThunkParams) => void;
};

type AllProps = OwnProps & DispatchProps;

class CancelCoreWorkerTasksQueueButtonClass extends React.Component<AllProps, OwnState> {
    public constructor(props: AllProps) {
        super(props);

        this.onCancelCoreWorkerTasksQueueClick = this.onCancelCoreWorkerTasksQueueClick.bind(this);
        this.onProcessActionStateChanged = this.onProcessActionStateChanged.bind(this);

        this.state = {
            cancelCoreWorkerTasksQueueState: {
                isInErrorState: false,
                isInProgressState: false,
                isInSuccessState: false,
                error: undefined
            }
        };
    }

    public shouldComponentUpdate(nextProps: OwnProps, nextState: OwnState) {
        return hasComponentChangesFor(this.props, nextProps) ||
            hasComponentChangesFor(this.state, nextState);
    }

    public componentDidUpdate(_prevProps: AllProps, prevState: OwnState) {
        if (!prevState.cancelCoreWorkerTasksQueueState.isInSuccessState && this.state.cancelCoreWorkerTasksQueueState.isInSuccessState) {
            if (this.props.showSuccessMessage ?? true) {
                this.props.floatMessage.show(MessageType.Success, 'Отмена задачи выполнена успешно!');
            }

            if (this.props.onSuccess) this.props.onSuccess();
        } else if (!prevState.cancelCoreWorkerTasksQueueState.isInErrorState && this.state.cancelCoreWorkerTasksQueueState.isInErrorState) {
            if (this.props.showErrorMessage ?? true) {
                this.props.floatMessage.show(MessageType.Error, this.state.cancelCoreWorkerTasksQueueState.error?.message);
            }
        }
    }

    private onProcessActionStateChanged(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId === TasksProcessId.CancelCoreWorkerTasksQueue) {
            this.setState({
                cancelCoreWorkerTasksQueueState: {
                    isInProgressState: actionState.isInProgressState,
                    isInSuccessState: actionState.isInSuccessState,
                    isInErrorState: actionState.isInErrorState,
                    error
                }
            });
        }
    }

    private onCancelCoreWorkerTasksQueueClick() {
        const reasonCancellation = this.props.getReasonCancellation();

        this.props.dispatchCancelCoreWorkerTasksQueueThunk({
            coreWorkerTasksQueueId: this.props.coreWorkerTasksQueueId,
            reasonCancellation: reasonCancellation ?? '',
            force: true
        });
    }

    public render() {
        return (
            <>
                <ContainedButton
                    isEnabled={ this.props.isEnabled && !this.state.cancelCoreWorkerTasksQueueState.isInProgressState }
                    showProgress={ this.state.cancelCoreWorkerTasksQueueState.isInProgressState }
                    onClick={ this.onCancelCoreWorkerTasksQueueClick }
                    kind="success"
                >
                    Сохранить
                </ContainedButton>
                <WatchReducerProcessActionState
                    watch={ [{
                        reducerStateName: 'TasksInQueueState',
                        processIds: [TasksProcessId.CancelCoreWorkerTasksQueue]
                    }] }
                    onProcessActionStateChanged={ this.onProcessActionStateChanged }
                />
            </>
        );
    }
}

const CancelCoreWorkerTasksQueueButtonConnected = connect<{}, DispatchProps, OwnProps, AppReduxStoreState>(
    null,
    {
        dispatchCancelCoreWorkerTasksQueueThunk: CancelCoreWorkerTasksQueueThunk.invoke,
    }
)(CancelCoreWorkerTasksQueueButtonClass);

export const CancelCoreWorkerTasksQueueButton = withFloatMessages(CancelCoreWorkerTasksQueueButtonConnected);