import { Box } from '@mui/material';
import { hasComponentChangesFor } from 'app/common/functions';
import {
    ReceiveWorkerTaskBagsControlCardThunkParams
} from 'app/modules/workerTasksBagsControlCard/store/reducers/receiveWorkerTasksBagsControlCard/params';
import { ReceiveWorkerTasksBagsControlCardThunk } from 'app/modules/workerTasksBagsControlCard/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { DualListBoxForm } from 'app/views/components/controls/forms/DualListBoxForm';
import { DualListItem, DualListStateListTypes } from 'app/views/components/controls/forms/DualListBoxForm/views/types';
import {
    WorkerTasksBagsControlDataForm
} from 'app/views/modules/CoreWorkerTasks/tabs/WorkerAvailableTasksBagsTabView/types';
import {
    WorkerAvailableTasksBagItemDataModel
} from 'app/web/InterlayerApiProxy/WorkerAvailableTasksBagsApiProxy/receiveWorkerAvailableTasksBagsData';
import {
    WorkerTasksBagsControlCardDataModel
} from 'app/web/InterlayerApiProxy/WorkerTasksBagsControlCardApiProxy/receiveWorkerTasksBagsControlCard/data-models';
import { KeyValueDataModel } from 'app/web/common/data-models';
import React from 'react';
import { connect } from 'react-redux';

type OwnProps = ReduxFormProps<WorkerTasksBagsControlDataForm> & {
    itemToEdit: WorkerAvailableTasksBagItemDataModel;
};

type StateProps = {
    hasTasksBagsControlCardReceived: boolean;
    details: WorkerTasksBagsControlCardDataModel;
};

type DispatchProps = {
    dispatchReceiveWorkerTaskBagsControlCardThunk: (args?: ReceiveWorkerTaskBagsControlCardThunkParams) => void;
};

type AllProps = OwnProps & StateProps & DispatchProps;

class AssignTasksToWorkerDialogContentFormViewClass extends React.Component<AllProps> {
    public constructor(props: AllProps) {
        super(props);
        this.onValueChange = this.onValueChange.bind(this);
        this.initReduxForm = this.initReduxForm.bind(this);
        this.props.reduxForm.setInitializeFormDataAction(this.initReduxForm);
        this.getDualListItems = this.getDualListItems.bind(this);
    }

    public componentDidMount() {
        this.props.dispatchReceiveWorkerTaskBagsControlCardThunk({
            coreWorkerId: this.props.itemToEdit.workerId,
            force: true
        });
    }

    public shouldComponentUpdate(nextProps: AllProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private onValueChange(formName: string, newLeftValue: Array<DualListItem<string>>, newRightValue: Array<DualListItem<string>>): boolean {
        this.props.reduxForm.updateReduxFormFields({
            workerTasksBags: this.getKeyValueData(newLeftValue),
            availableTasksForAdd: this.getKeyValueData(newRightValue)
        });

        return true;
    }

    /**
     * Получить элементы двойного выпадающего списка
     * @param items список элементов модели
     */
    private getDualListItems(items: Array<KeyValueDataModel<string, string>>): Array<DualListItem<string>> {
        return items.map(item => {
            return {
                label: item.value,
                key: item.key
            };
        });
    }

    /**
     * Получить список ключ-значение элементов задач
     * @param dualListItem список DualList элементов задач
     * @returns Тип ключ-значение списка элементов задач
     */
    private getKeyValueData(dualListItem: Array<DualListItem<string>>) {
        return dualListItem.map(item => {
            return {
                key: item.key,
                value: item.label
            };
        });
    }

    private initReduxForm(): WorkerTasksBagsControlDataForm | undefined {
        if (!this.props.hasTasksBagsControlCardReceived) {
            return;
        }

        return {
            coreWorkerId: this.props.itemToEdit.workerId,
            workerTasksBags: this.props.details.workerTasksBags,
            availableTasksForAdd: this.props.details.availableTasksForAdd
        };
    }

    public render() {
        if (!this.props.hasTasksBagsControlCardReceived) {
            return null;
        }

        const reduxForm = this.props.reduxForm.getReduxFormFields(false);

        return (
            <Box>
                <DualListBoxForm
                    allowSelectAll={ true }
                    mainList={ DualListStateListTypes.leftList }
                    rightListLabel="Доступные задачи"
                    leftListLabel="Выполняемые задачи"
                    formName="workerTasksBags"
                    leftListItems={ this.getDualListItems(reduxForm.workerTasksBags) }
                    rightListItems={ this.getDualListItems(reduxForm.availableTasksForAdd) }
                    isFormDisabled={ false }
                    onValueChange={ this.onValueChange }
                />
            </Box>
        );
    }
}

export const AssignTasksToWorkerDialogContentFormViewConnected = connect<StateProps, DispatchProps, OwnProps, AppReduxStoreState>(
    state => {
        const workerTasksBagsCardState = state.WorkerTasksBagsControlCardState;
        const tasksBagsControlCardDataModel = workerTasksBagsCardState.details;
        const { hasTasksBagsControlCardReceived } = workerTasksBagsCardState.hasSuccessFor;

        return {
            details: tasksBagsControlCardDataModel,
            hasTasksBagsControlCardReceived
        };
    },
    {
        dispatchReceiveWorkerTaskBagsControlCardThunk: ReceiveWorkerTasksBagsControlCardThunk.invoke,
    }
)(AssignTasksToWorkerDialogContentFormViewClass);

export const AssignTasksToWorkerDialogContentFormView = withReduxForm(AssignTasksToWorkerDialogContentFormViewConnected, {
    reduxFormName: 'AssignTasksToWorkerDialogContent_ReduxForm',
    resetOnUnmount: true
});