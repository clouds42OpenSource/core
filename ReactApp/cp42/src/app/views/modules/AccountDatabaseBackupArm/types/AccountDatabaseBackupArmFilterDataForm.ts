/**
 * Модель Redux формы для редактирования фильтра для таблицы Менеджера архивов
 */
export type AccountDatabaseBackupArmFilterDataForm = {
    /**
     * Строка фильтра
     */
    searchLine: string;
};