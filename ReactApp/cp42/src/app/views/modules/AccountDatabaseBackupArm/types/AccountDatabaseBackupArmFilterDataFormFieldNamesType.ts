import { AccountDatabaseBackupArmFilterDataForm } from 'app/views/modules/AccountDatabaseBackupArm/types/AccountDatabaseBackupArmFilterDataForm';

/**
 * Название полей для фильтр Filter Data Form
 */
export type AccountDatabaseBackupArmFilterDataFormFieldNamesType = keyof AccountDatabaseBackupArmFilterDataForm;