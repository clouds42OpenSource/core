import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import {
    AccountDatabaseBackupArmFilterDataForm
} from 'app/views/modules/AccountDatabaseBackupArm/types/AccountDatabaseBackupArmFilterDataForm';
import {
    AccountDatabaseBackupArmFilterDataFormFieldNamesType
} from 'app/views/modules/AccountDatabaseBackupArm/types/AccountDatabaseBackupArmFilterDataFormFieldNamesType';
import React from 'react';
import { SearchByFilterButton } from 'app/views/components/controls/Button';
import { TableFilterWrapper } from 'app/views/components/_hoc/tableFilterWrapper';
import { TextBoxForm } from 'app/views/components/controls/forms/TextBoxForm';
import { hasComponentChangesFor } from 'app/common/functions';

/**
 * Свойства для компонента фильтра
 */
type OwnProps = ReduxFormProps<AccountDatabaseBackupArmFilterDataForm> & {
    onFilterChanged: (filter: AccountDatabaseBackupArmFilterDataForm, filterFormName: string) => void;
    isFilterFormDisabled: boolean;
};

class AccountDatabaseBackupArmFilterFormViewClass extends React.Component<OwnProps> {
    public constructor(props: OwnProps) {
        super(props);
        this.initReduxForm = this.initReduxForm.bind(this);
        this.onValueChange = this.onValueChange.bind(this);
        this.onClickSearchButton = this.onClickSearchButton.bind(this);
    }

    public componentDidMount() {
        this.props.reduxForm.setInitializeFormDataAction(this.initReduxForm);
    }

    public shouldComponentUpdate(nextProps: OwnProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private onClickSearchButton() {
        this.props.onFilterChanged({
            ...this.props.reduxForm.getReduxFormFields(false)
        }, 'searchLine');
    }

    private onValueChange<TValue, TForm extends string = AccountDatabaseBackupArmFilterDataFormFieldNamesType>(formName: TForm, newValue: TValue) {
        this.props.reduxForm.updateReduxFormFields({
            [formName]: newValue
        });

        if (formName === 'searchLine' && newValue && (newValue as any as string).length < 3) {
            return false;
        }
    }

    private initReduxForm(): AccountDatabaseBackupArmFilterDataForm | undefined {
        return {
            searchLine: ''
        };
    }

    public render() {
        const formFields = this.props.reduxForm.getReduxFormFields(false);
        return (
            <TableFilterWrapper>
                <div style={ { width: '350px' } }>
                    <TextBoxForm
                        label="Поиск"
                        formName="searchLine"
                        onValueChange={ this.onValueChange }
                        value={ formFields.searchLine }
                        isReadOnly={ this.props.isFilterFormDisabled }
                        placeholder="Аккаунт/база/текст ошибки"
                    />
                </div>
                <SearchByFilterButton
                    onClick={ this.onClickSearchButton }
                />
            </TableFilterWrapper>
        );
    }
}

export const AccountDatabaseBackupArmFilterFormView = withReduxForm(AccountDatabaseBackupArmFilterFormViewClass, {
    reduxFormName: 'Account_Database_Backup_Filter_Form',
    resetOnUnmount: true
});