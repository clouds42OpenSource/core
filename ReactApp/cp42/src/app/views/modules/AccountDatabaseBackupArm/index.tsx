import { hasComponentChangesFor } from 'app/common/functions';
import { ProcessIdStateHelper } from 'app/common/helpers/ProcessIdStateHelper';
import { Nullable } from 'app/common/types';
import { useDatabaseCard } from 'app/hooks';
import { AccountDatabaseBackupArmProcessId } from 'app/modules/accountDatabaseBackupArm';
import {
    GetAccountDatabaseBackupHistoriesThunkParams
} from 'app/modules/accountDatabaseBackupArm/store/reducers/commonParams';
import { GetAccountDatabaseBackupHistoriesThunk } from 'app/modules/accountDatabaseBackupArm/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { DateUtility } from 'app/utils';
import { TextLinkButton } from 'app/views/components/controls/Button';
import { TextOut } from 'app/views/components/TextOut';
import { withHeader } from 'app/views/components/_hoc/withHeader';
import { InformationDatabaseCard } from 'app/views/modules/_common/reusable-modules/InformationDatabaseCard';
import {
    AccountDatabaseBackupArmFilterDataForm
} from 'app/views/modules/AccountDatabaseBackupArm/types/AccountDatabaseBackupArmFilterDataForm';
import {
    AccountDatabaseBackupArmFilterFormView
} from 'app/views/modules/AccountDatabaseBackupArm/views/AccountDatabaseBackupArmFilterFormView';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { AccountCard } from 'app/views/modules/_common/reusable-modules/AccountCard';
import { SelectDataCommonDataModel } from 'app/web/common/data-models';
import {
    AccountDatabaseBackupArmPaginationDataModel,
    AccountDatabaseBackupHistoryPeriodDataModel
} from 'app/web/InterlayerApiProxy/AccountDatabaseBackupArmApiProxy/getAccountDatabaseBackupHistoriesPagination/data-models';
import React, { Component, ReactNode } from 'react';
import { connect } from 'react-redux';

type StateProps = {
    accountDatabaseBackupHistory: AccountDatabaseBackupArmPaginationDataModel;
    isInLoadingDataProcess: boolean;
};

type OwnState = {
    isAccountCardVisible: boolean;
    isDatabaseCardVisible: boolean;
    databaseName: string;
    accountNumber: number;
    currentFilter?: AccountDatabaseBackupArmFilterDataForm | undefined;
};

type TProps = {
    openDatabase: (databaseNumber: string) => void;
    databaseCard: ReactNode;
};

type DispatchProps = {
    dispatchAccountDatabaseBackupHistoriesThunk: (args: GetAccountDatabaseBackupHistoriesThunkParams) => void;
};

type AllProps = StateProps & DispatchProps & TProps;

class AccountDatabaseBackupArmClass extends Component<AllProps, OwnState> {
    private _warningTableRef = React.createRef<CommonTableWithFilter<any, AccountDatabaseBackupArmFilterDataForm>>();

    private _dangerTableRef = React.createRef<CommonTableWithFilter<any, AccountDatabaseBackupArmFilterDataForm>>();

    public constructor(props: AllProps) {
        super(props);

        this.state = {
            /**
             * Поле для видимости карточки аккаунта
             */
            isAccountCardVisible: false,

            /**
             * Поле для видимости карточки информационной базы
             */
            isDatabaseCardVisible: false,

            /**
             * Номер базы
             */
            databaseName: '',

            /**
             * Номер аккаунта
             */
            accountNumber: 0
        };

        this.onDataDangerTableSelect = this.onDataDangerTableSelect.bind(this);
        this.onDataWarningTableSelect = this.onDataWarningTableSelect.bind(this);

        this.showAccountCard = this.showAccountCard.bind(this);
        this.hideAccountCard = this.hideAccountCard.bind(this);
    }

    public componentDidMount() {
        this.props.dispatchAccountDatabaseBackupHistoriesThunk({
            dangerTablePage: 1,
            warningTablePage: 1,
            searchQuery: '',
            force: true
        });
    }

    public shouldComponentUpdate(nextProps: AllProps, nextState: OwnState) {
        return hasComponentChangesFor(this.props, nextProps) ||
            hasComponentChangesFor(this.state, nextState);
    }

    private onDataDangerTableSelect(args: SelectDataCommonDataModel<AccountDatabaseBackupArmFilterDataForm>, changedFilterFormName: string | null) {
        this.props.dispatchAccountDatabaseBackupHistoriesThunk({
            dangerTablePage: args.pageNumber,
            warningTablePage: this._warningTableRef.current?.state.pageNumber! ?? 1,
            searchQuery: args.filter?.searchLine ?? '',
            force: true
        });

        if (changedFilterFormName === 'searchLine') {
            this.setState({
                currentFilter: args.filter
            });
        }
    }

    private onDataWarningTableSelect(args: SelectDataCommonDataModel<AccountDatabaseBackupArmFilterDataForm>) {
        this.props.dispatchAccountDatabaseBackupHistoriesThunk({
            dangerTablePage: this._dangerTableRef.current?.state.pageNumber! ?? 1,
            warningTablePage: args.pageNumber,
            searchQuery: this.state.currentFilter?.searchLine ?? '',
            force: true
        });
    }

    private getTableTitle(period: AccountDatabaseBackupHistoryPeriodDataModel) {
        let title = '';

        if (period.fromDays) title = `Архив отсутcтвует от ${ period.fromDays } ${ period.toDays !== undefined ? `до ${ period.toDays } дней` : 'и более дней' }`;

        return title;
    }

    private showAccountCard(accountNumber: number) {
        return () => {
            this.setState({
                isAccountCardVisible: true,
                accountNumber
            });
        };
    }

    private hideAccountCard() {
        this.setState({
            isAccountCardVisible: false,
            accountNumber: 0
        });
    }

    public render() {
        const { accountDatabaseBackupHistory: { dangerTable, warningTable, dangerTablePeriod, warningTablePeriod }, isInLoadingDataProcess } = this.props;
        const dateFormat = 'dd.MM.yyyy';

        return (
            <>
                <div>
                    <CommonTableWithFilter
                        ref={ this._dangerTableRef }
                        uniqueContextProviderStateId="AccountDatabaseBackupArmDangerTableContextProviderStateId"
                        filterProps={ {
                            getFilterContentView: (ref, onFilterChanged) => {
                                return (<AccountDatabaseBackupArmFilterFormView
                                    ref={ ref }
                                    isFilterFormDisabled={ isInLoadingDataProcess }
                                    onFilterChanged={ onFilterChanged }
                                />);
                            }
                        } }
                        tableProps={ {
                            dataset: dangerTable.records,
                            keyFieldName: 'accountDatabaseId',
                            fieldsView: {
                                v82Name: {
                                    caption: 'База (область)',
                                    fieldContentNoWrap: false,
                                    fieldWidth: 'auto',
                                    format: value => {
                                        return (
                                            <TextLinkButton onClick={ () => this.props.openDatabase(value) }>
                                                <TextOut fontSize={ 13 } className="text-link">
                                                    { value }
                                                </TextOut>
                                            </TextLinkButton>
                                        );
                                    }
                                },
                                accountIndexNumber: {
                                    caption: 'Номер аккаунта',
                                    fieldContentNoWrap: false,
                                    fieldWidth: 'auto',
                                    format: (value: number) => {
                                        return (
                                            <TextLinkButton onClick={ this.showAccountCard(value) } canSelectButtonContent={ true }>
                                                <TextOut fontSize={ 13 } className="text-link">
                                                    { value }
                                                </TextOut>
                                            </TextLinkButton>
                                        );
                                    }
                                },
                                accountDatabaseBackupHistoryCreateDateTime: {
                                    caption: 'Дата',
                                    fieldContentNoWrap: false,
                                    fieldWidth: 'auto',
                                    format: (value: Nullable<Date>) => {
                                        return DateUtility.getDateByFormat(value, dateFormat);
                                    }
                                },
                                accountDatabaseBackupHistoryMessage: {
                                    caption: 'Состояние',
                                    fieldContentNoWrap: false,
                                    fieldWidth: 'auto'
                                }
                            },
                            pagination: {
                                currentPage: dangerTable.pagination.currentPage,
                                pageSize: dangerTable.pagination.pageSize,
                                recordsCount: dangerTable.pagination.totalCount,
                                totalPages: dangerTable.pagination.pageCount
                            },
                            headerTitleTable: {
                                isRed: true,
                                text: this.getTableTitle(dangerTablePeriod)
                            }
                        } }
                        onDataSelect={ this.onDataDangerTableSelect }
                    />
                </div>
                {
                    warningTable.records.length > 0
                        ? (
                            <div className="pt-3">
                                <CommonTableWithFilter
                                    ref={ this._warningTableRef }
                                    uniqueContextProviderStateId="AccountDatabaseBackupArmWarningTableContextProviderStateId"
                                    tableProps={ {
                                        dataset: warningTable.records,
                                        keyFieldName: 'accountDatabaseId',
                                        fieldsView: {
                                            v82Name: {
                                                caption: 'База (область)',
                                                fieldContentNoWrap: false,
                                                fieldWidth: 'auto',
                                                format: (value: string, { id }) => {
                                                    return (
                                                        <TextLinkButton onClick={ () => this.props.openDatabase(id) }>
                                                            <TextOut fontSize={ 13 } className="text-link">
                                                                { value }
                                                            </TextOut>
                                                        </TextLinkButton>
                                                    );
                                                }
                                            },
                                            accountIndexNumber: {
                                                caption: 'Номер аккаунта',
                                                fieldContentNoWrap: false,
                                                fieldWidth: 'auto',
                                                format: (value: number) => {
                                                    return (
                                                        <TextLinkButton onClick={ this.showAccountCard(value) } canSelectButtonContent={ true }>
                                                            <TextOut fontSize={ 13 } className="text-link">
                                                                { value }
                                                            </TextOut>
                                                        </TextLinkButton>
                                                    );
                                                }
                                            },
                                            accountDatabaseBackupHistoryCreateDateTime: {
                                                caption: 'Дата',
                                                fieldContentNoWrap: false,
                                                fieldWidth: 'auto',
                                                format: (value: Nullable<Date>) => {
                                                    return DateUtility.getDateByFormat(value, dateFormat);
                                                }
                                            },
                                            accountDatabaseBackupHistoryMessage: {
                                                caption: 'Состояние',
                                                fieldContentNoWrap: false,
                                                fieldWidth: 'auto'
                                            }
                                        },
                                        pagination: {
                                            currentPage: warningTable.pagination.currentPage,
                                            pageSize: warningTable.pagination.pageSize,
                                            recordsCount: warningTable.pagination.totalCount,
                                            totalPages: warningTable.pagination.pageCount
                                        },
                                        headerTitleTable: {
                                            isRed: false,
                                            text: this.getTableTitle(warningTablePeriod)
                                        }
                                    } }
                                    onDataSelect={ this.onDataWarningTableSelect }
                                />
                            </div>
                        )
                        : null
                }
                <AccountCard
                    accountNumber={ this.state.accountNumber }
                    isOpen={ this.state.isAccountCardVisible }
                    onCancelDialogButtonClick={ this.hideAccountCard }
                />
                { this.props.databaseCard }
            </>
        );
    }
}

const AccountDatabaseBackupArmConnected = connect<StateProps, DispatchProps, {}, AppReduxStoreState>(
    state => {
        const accountDatabaseBackupArmState = state.AccountDatabaseBackupArmState;

        const { accountDatabaseBackupHistory } = accountDatabaseBackupArmState;
        const isInLoadingDataProcess = ProcessIdStateHelper.isInProgress(accountDatabaseBackupArmState.reducerActions, AccountDatabaseBackupArmProcessId.GetAccountDatabaseBackupHistoriesWithPagination);

        return {
            accountDatabaseBackupHistory,
            isInLoadingDataProcess
        };
    },
    {
        dispatchAccountDatabaseBackupHistoriesThunk: GetAccountDatabaseBackupHistoriesThunk.invoke
    }
)(AccountDatabaseBackupArmClass);

const AccountDatabaseBackupArmFunction = () => {
    const { openDatabaseDialogHandler, closeDatabaseDialogHandler, dialogDatabaseNumber, isOpen } = useDatabaseCard();

    return (
        <AccountDatabaseBackupArmConnected
            openDatabase={ openDatabaseDialogHandler }
            databaseCard={
                <InformationDatabaseCard
                    isOpen={ isOpen }
                    databaseNumber={ dialogDatabaseNumber }
                    onClose={ closeDatabaseDialogHandler }
                />
            }
        />
    );
};

export const AccountDatabaseBackupArmView = withHeader({
    title: 'Менеджер архивов',
    page: AccountDatabaseBackupArmFunction
});