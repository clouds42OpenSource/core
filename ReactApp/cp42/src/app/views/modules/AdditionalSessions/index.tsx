import { ErrorOutline as ErrorOutlineIcon } from '@mui/icons-material';
import { Paper } from '@mui/material';
import Stack from '@mui/material/Stack';
import { Box } from '@mui/system';
import { contextAccountId } from 'app/api';
import { TUserSessionsItem } from 'app/api/endpoints/additionalSessions/response';
import { REDUX_API } from 'app/api/useReduxApi';
import { AppRoutes } from 'app/AppRoutes';
import { AccountUserGroup } from 'app/common/enums';
import { hasComponentChangesFor } from 'app/common/functions';
import { useAppDispatch } from 'app/hooks';
import { ChangeCloudServiceResourcesThunkParams } from 'app/modules/additionalSessions/store/reducers/ChangeCloudServiceResourcesReducer/params';
import { GetCloudServiceResourcesThunkParams } from 'app/modules/additionalSessions/store/reducers/GetCloudServiceResourcesReducer/params';
import { ResetCloudServiceResourcesThunkParams } from 'app/modules/additionalSessions/store/reducers/ResetCloudServiceResourcesReducer/params';
import { ChangeCloudServiceResourcesThunk, GetCloudServiceResourcesThunk, GetSessionListThunk, ResetCloudServiceResourcesThunk } from 'app/modules/additionalSessions/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { COLORS } from 'app/utils';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { withHeader } from 'app/views/components/_hoc/withHeader';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { BaseButton } from 'app/views/components/controls/Button/BaseButton';
import { Dialog } from 'app/views/components/controls/Dialog';
import { TextBoxForm } from 'app/views/components/controls/forms';
import { TextOut } from 'app/views/components/TextOut';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { AmountDataForm } from 'app/views/modules/AdditionalSessions/types/AmountDataForm';
import { AdditionalCardView } from 'app/views/modules/AdditionalSessions/views/AdditidionalCard';
import { AdditionalSessionsTableToChangeView } from 'app/views/modules/AdditionalSessions/views/AdditionalSessionsTableToChange';
import { AdditionalSessionsTableToViewClass } from 'app/views/modules/AdditionalSessions/views/AdditionalSessionsTableToView';
import { PersonalAdditionalSessions } from 'app/views/modules/AdditionalSessions/views/PersonalAdditionalSessions';
import { SummaryTable } from 'app/views/modules/AdditionalSessions/views/SummaryTable';
import { ChangeCloudServiceResourcesDataModel } from 'app/web/InterlayerApiProxy/AdditionalSessionsApiProxy/changeCloudServiceResources/data-models';
import { CloudServiceResourcesDataModel } from 'app/web/InterlayerApiProxy/AdditionalSessionsApiProxy/getCloudServiceResources/data-models';
import { SessionListDataModel } from 'app/web/InterlayerApiProxy/AdditionalSessionsApiProxy/getSessionList/data-models';
import { GetSessionListParams } from 'app/web/InterlayerApiProxy/AdditionalSessionsApiProxy/getSessionList/input-params';
import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import css from './style.module.css';

type StateProps = {
    /**
     * Лист с дополнительными сеансами
     */
    dataset: SessionListDataModel;
    /**
     * Статус сервиса "Дополнительные сеансы" для аккаунта
     */
    cloudServiceResources?: CloudServiceResourcesDataModel;
    /**
     * Группа пользователя
     */
    currentUserGroups: AccountUserGroup[];
    /**
     * Статус покупки или изменения дополнительных сеансов
     */
    cloudService: ChangeCloudServiceResourcesDataModel;
    hasReceivedCloudService: boolean;
    accountUserSessions?: TUserSessionsItem[];
    commonSessions: number;
    hasBuyResources: boolean;
    additionalSessionsUsed: number;
    cloudServiceResourcesError?: string;
    isDifferentContextAccount: boolean;
};

type DispatchProps = {
    dispatchGetSessionListThunk: (args: GetSessionListParams) => void;
    dispatchGetCloudServiceResourcesThunk: (args: GetCloudServiceResourcesThunkParams) => void;
    dispatchChangeCloudServiceResourcesThunk: (args: ChangeCloudServiceResourcesThunkParams) => void;
    dispatchResetCloudServiceResourcesThunk: (args: ResetCloudServiceResourcesThunkParams) => void;
};

type OwnProps = {
    getPersonalSession: () => void;
};

type OwnState = {
    isVisible: boolean;
    isOpenApplyDialog: boolean;
};

type AllProps = StateProps & DispatchProps & ReduxFormProps<AmountDataForm> & OwnProps & FloatMessageProps;

const { getExtraSession } = REDUX_API.ADDITIONAL_SESSIONS_REDUX;

class AdditionalSessionsClass extends React.Component<AllProps, OwnState> {
    public constructor(props: AllProps) {
        super(props);

        this.getChangeMod = this.getChangeMod.bind(this);
        this.getViewMode = this.getViewMode.bind(this);
        this.plusAmount = this.plusAmount.bind(this);
        this.minusAmount = this.minusAmount.bind(this);
        this.onValueChange = this.onValueChange.bind(this);
        this.initReduxForm = this.initReduxForm.bind(this);
        this.showBuy = this.showBuy.bind(this);
        this.successUpdateCloudResources = this.successUpdateCloudResources.bind(this);
        this.failedUpdateCloudResources = this.failedUpdateCloudResources.bind(this);
        this.closeDialog = this.closeDialog.bind(this);
        this.renderBuyOrPayButton = this.renderBuyOrPayButton.bind(this);
        this.buyResources = this.buyResources.bind(this);
        this.closeApplyDialog = this.closeApplyDialog.bind(this);
        this.openApplyDialog = this.openApplyDialog.bind(this);

        this.props.reduxForm.setInitializeFormDataAction(this.initReduxForm);

        this.state = {
            isVisible: false,
            isOpenApplyDialog: false
        };
    }

    public componentDidMount() {
        this.props.dispatchGetSessionListThunk({
            'full-list': false,
            'group-by': 'user',
            'account-id': this.props.isDifferentContextAccount ? contextAccountId() : undefined
        });
        this.props.dispatchGetCloudServiceResourcesThunk({ force: true });
    }

    public shouldComponentUpdate(nextProps: AllProps, nextState: OwnState) {
        return hasComponentChangesFor(this.props, nextProps) || hasComponentChangesFor(this.state, nextState);
    }

    public componentDidUpdate(prevProps: Readonly<AllProps>) {
        this.props.dispatchResetCloudServiceResourcesThunk({});

        if (this.props.hasReceivedCloudService && !this.props.cloudService.complete) {
            this.setState({ isVisible: true });
        }

        if (this.props.hasBuyResources && this.props.hasBuyResources !== prevProps.hasBuyResources) {
            this.props.dispatchGetCloudServiceResourcesThunk({ force: true });
            this.props.getPersonalSession();
        }

        if (this.props.cloudServiceResourcesError && this.props.cloudServiceResourcesError !== prevProps.cloudServiceResourcesError) {
            this.props.floatMessage.show(MessageType.Error, this.props.cloudServiceResourcesError);
        }
    }

    private onValueChange(formName: string, newValue: string) {
        this.props.reduxForm.updateReduxFormFields({
            /**
             * Принимает только цифры
             */
            [formName]: /^\d+$/.test(newValue) ? newValue : '0'
        });
    }

    /**
     * Вывод таблицы для всех, кто старше пользователя
     * @private
     */
    private getChangeMod() {
        const reduxForm = this.props.reduxForm.getReduxFormFields(false);

        return (
            <>
                {
                    this.props.cloudServiceResources && (
                        <>
                            <AdditionalCardView count={ `${ this.props.additionalSessionsUsed } из ${ this.props.cloudServiceResources.usedLicenses } ` } />
                            {
                                new Date(this.props.cloudServiceResources.serviceExpireDate) < new Date()
                                    ? (
                                        <Paper style={ {
                                            padding: '8px',
                                            backgroundColor: '#F2DEDE',
                                            fontSize: '14px',
                                            lineHeight: '1.2',
                                            color: '#A94442',
                                            margin: '16px 0'
                                        } }
                                        >
                                            Внимание! Использование сервиса возможно до: { new Date(this.props.cloudServiceResources.serviceExpireDate)
                                                .toLocaleString('ru', { year: 'numeric', month: 'long', day: 'numeric', timeZone: 'UTC' }) }&nbsp;
                                            Сервис &quot;Аренда 1С&quot; заблокирован. Пожалуйста, <Link to={ AppRoutes.accountManagement.balanceManagement }>оплатите сервис.</Link>
                                        </Paper>
                                    )
                                    : null
                            }
                            {
                                this.props.cloudServiceResources.isActivatedPromisPayment
                                    ? (
                                        <Paper style={ {
                                            padding: '8px',
                                            backgroundColor: '#D9EDF7',
                                            fontSize: '14px',
                                            lineHeight: '1.2',
                                            color: '#31708F',
                                            margin: '16px 0'
                                        } }
                                        >
                                            Внимание! У Вас активирован обещанный платеж. Пожалуйста, <Link to={ AppRoutes.accountManagement.balanceManagement }>погасите</Link> задолженность до&nbsp;
                                            { new Date(this.props.cloudServiceResources.serviceExpireDate)
                                                .toLocaleString('ru', { year: 'numeric', month: 'long', day: 'numeric', timeZone: 'UTC' }) }&nbsp;
                                            во избежание блокировки сервиса
                                        </Paper>
                                    )
                                    : null
                            }
                            <CommonTableWithFilter
                                uniqueContextProviderStateId="BuyingAdditionalSessionsContextProviderStateId"
                                isResponsiveTable={ false }
                                tableProps={ {
                                    dataset: [{
                                        description: 'Изменить количество дополнительных сеансов',
                                        amount: this.props.cloudServiceResources.usedLicenses,
                                        price: '950',
                                        totalPrice: (parseInt(reduxForm.amount, 10) * 950).toString()
                                    }],
                                    keyFieldName: 'amount',
                                    isResponsiveTable: false,
                                    fieldsView: {
                                        description: {
                                            caption: '',
                                            style: { lineHeight: '35px' },
                                            fieldContentNoWrap: true
                                        },
                                        amount: {
                                            caption: 'Количество',
                                            style: { textAlign: 'center' },
                                            format: () => {
                                                return (
                                                    <Stack direction="row" alignItems="center" justifyContent="center">
                                                        <BaseButton
                                                            variant="text"
                                                            kind="error"
                                                            onClick={ this.minusAmount }
                                                            style={ { minWidth: '30px', maxHeight: '30px' } }
                                                        >
                                                            <i className="fa fa-minus" />
                                                        </BaseButton>
                                                        <TextBoxForm
                                                            formName="amount"
                                                            className={ css.textFieldLegend }
                                                            hiddenLabel={ true }
                                                            onValueChange={ this.onValueChange }
                                                            value={ reduxForm.amount }
                                                        />
                                                        <BaseButton
                                                            variant="text"
                                                            kind="turquoise"
                                                            onClick={ this.plusAmount }
                                                            style={ { minWidth: '30px', maxHeight: '30px' } }
                                                        >
                                                            <i className="fa fa-plus" />
                                                        </BaseButton>
                                                    </Stack>
                                                );
                                            }
                                        },
                                        price: {
                                            caption: `Стоимость, ${ this.props.cloudServiceResources.currency }/мес`,
                                            style: { textAlign: 'center' }
                                        },
                                        totalPrice: {
                                            caption: `Всего, ${ this.props.cloudServiceResources.currency }/мес`,
                                            style: { textAlign: 'center' }
                                        }
                                    }
                                } }
                            />
                            <PersonalAdditionalSessions />
                            <Stack direction="row" alignItems="center" justifyContent="space-between">
                                <SummaryTable commonSessions={ +reduxForm.amount } />
                                {
                                    new Date(this.props.cloudServiceResources.serviceExpireDate) > new Date()
                                        ? (
                                            <TextOut>
                                                Использование сервиса возможно до: { new Date(this.props.cloudServiceResources.serviceExpireDate)
                                                    .toLocaleString('ru', { year: 'numeric', month: 'long', day: 'numeric', timeZone: 'UTC' }) }
                                            </TextOut>
                                        )
                                        : null
                                }
                                {
                                    new Date(this.props.cloudServiceResources.serviceExpireDate) < new Date()
                                        ? this.renderBuyOrPayButton()
                                        : (
                                            <BaseButton
                                                variant="contained"
                                                kind="success"
                                                style={ { maxWidth: '130px', minWidth: 'auto' } }
                                                onClick={ this.openApplyDialog }
                                            >
                                                Применить
                                            </BaseButton>
                                        )
                                }
                            </Stack>
                            <br />
                            <AdditionalSessionsTableToChangeView
                                dataset={ this.props.dataset.sessions }
                                amount={ this.props.commonSessions }
                                serviceExpireData={ this.props.cloudServiceResources.serviceExpireDate }
                                updateDataset={ () => {
                                    this.props.dispatchGetCloudServiceResourcesThunk({ force: true });
                                } }
                                currency={ this.props.cloudServiceResources.currency }
                                visible={ this.state.isVisible }
                                closeDialog={ () => this.closeDialog() }
                                closeApplyDialog={ this.closeApplyDialog }
                                isRent1C={ new Date(this.props.cloudServiceResources.serviceExpireDate) > new Date() }
                            />
                        </>
                    )
                }
            </>
        );
    }

    /**
     * Вывод таблицы для пользователя
     * @private
     */
    private getViewMode() {
        return (
            <>
                {
                    this.props.cloudServiceResources && (
                        <>
                            <AdditionalCardView count={ ` ${ this.props.additionalSessionsUsed }из ${ this.props.cloudServiceResources.usedLicenses }` } mb="65px" />
                            {
                                new Date(this.props.cloudServiceResources.serviceExpireDate) < new Date()
                                    ? (
                                        <Paper style={ {
                                            padding: '8px',
                                            backgroundColor: '#F2DEDE',
                                            fontSize: '14px',
                                            lineHeight: '1.2',
                                            color: '#A94442',
                                            margin: '16px 0'
                                        } }
                                        >
                                            Внимание! Использование сервиса возможно до: { new Date(this.props.cloudServiceResources.serviceExpireDate)
                                                .toLocaleString('ru', { year: 'numeric', month: 'long', day: 'numeric', timeZone: 'UTC' }) }
                                            Сервис &quot;Аренда 1С&quot; заблокирован. Пожалуйста, <Link style={ { color: COLORS.link } } to={ AppRoutes.accountManagement.balanceManagement }>оплатите сервис.</Link>
                                        </Paper>
                                    )
                                    : null
                            }
                            {
                                this.props.dataset.sessions.length
                                    ? <AdditionalSessionsTableToViewClass dataset={ this.props.dataset.sessions } />
                                    : <TextOut inDiv={ true } textAlign="center" fontSize={ 13 }>нет записей</TextOut>
                            }
                        </>
                    )
                }
            </>
        );
    }

    /**
     * Плюс 1 к общему количеству дополнительных сеансов
     * @private
     */
    private plusAmount() {
        const reduxForm = this.props.reduxForm.getReduxFormFields(false);

        this.props.reduxForm.updateReduxFormFields({
            amount: (parseInt(reduxForm.amount, 10) + 1).toString()
        });
    }

    /**
     * Минус 1 к общему количеству дополнительных сеансов
     * @private
     */
    private minusAmount() {
        const reduxForm = this.props.reduxForm.getReduxFormFields(false);

        if (parseInt(reduxForm.amount, 10) > 0) {
            this.props.reduxForm.updateReduxFormFields({
                amount: (parseInt(reduxForm.amount, 10) - 1).toString()
            });
        }
    }

    private initReduxForm(): AmountDataForm | undefined {
        return {
            amount: this.props.commonSessions.toString()
        };
    }

    /**
     * В зависимости от статуса покупки показывает окно покупки или обновляет ресурсы доп сеансов
     * this.props.cloudService.complete = true - обновление ресурсов
     * this.props.cloudService.complete = false - окно покупки
     * @private
     */
    private showBuy() {
        const reduxForm = this.props.reduxForm.getReduxFormFields(false);

        this.props.dispatchChangeCloudServiceResourcesThunk({
            count: parseInt(reduxForm.amount, 10),
            accountUserSessions: this.props.accountUserSessions ?? [],
            force: true
        });
        this.closeApplyDialog();
    }

    /**
     * Обновление ресурсов дополнительных сеансов
     * @private
     */
    private successUpdateCloudResources() {
        this.props.dispatchGetCloudServiceResourcesThunk({ force: true });
    }

    /**
     * Показ модального окна с покупкой/пополнением счета/обещанным платежом
     * И обновление ресурсов дополнительных сеансов
     * @private
     */
    private failedUpdateCloudResources() {
        this.props.dispatchGetCloudServiceResourcesThunk({ force: true });
        this.setState({
            isVisible: true
        });
    }

    private closeDialog() {
        this.setState({
            isVisible: false
        });
    }

    private openApplyDialog() {
        this.setState({
            isOpenApplyDialog: true
        });
    }

    private closeApplyDialog() {
        this.setState({
            isOpenApplyDialog: false
        });
    }

    private buyResources() {
        const { amount } = this.props.reduxForm.getReduxFormFields(false);

        this.props.dispatchChangeCloudServiceResourcesThunk({
            count: parseInt(amount, 10),
            accountUserSessions: this.props.accountUserSessions ?? [],
            force: true
        });
        this.closeApplyDialog();
    }

    private renderBuyOrPayButton() {
        if (this.props.cloudServiceResources) {
            const amount = parseInt(this.props.reduxForm.getReduxFormFields(false).amount, 10);
            const { usedLicenses } = this.props.cloudServiceResources;

            if (amount === 0) {
                return (
                    <BaseButton
                        variant="contained"
                        kind="success"
                        onClick={ this.openApplyDialog }
                    >
                        Применить
                    </BaseButton>
                );
            }

            if (usedLicenses === amount) {
                return (
                    <Link
                        to={ AppRoutes.accountManagement.balanceManagement }
                        style={ { textDecoration: 'none' } }
                    >
                        <BaseButton
                            variant="contained"
                            kind="success"
                        >
                            Купить
                        </BaseButton>
                    </Link>
                );
            }

            if (usedLicenses > amount) {
                return (
                    <BaseButton
                        variant="contained"
                        kind="success"
                        onClick={ this.openApplyDialog }
                    >
                        Применить
                    </BaseButton>
                );
            }

            return (
                <Link
                    to={ AppRoutes.accountManagement.balanceManagement }
                    style={ { textDecoration: 'none' } }
                >
                    <BaseButton
                        variant="contained"
                        kind="success"
                    >
                        Купить
                    </BaseButton>
                </Link>
            );
        }
    }

    public render() {
        return (
            <>
                {
                    this.props.cloudServiceResources && (
                        <>
                            <div data-testid="additional-sessions">
                                {
                                    !this.props.currentUserGroups.includes(AccountUserGroup.Undefined) ||
                                    !this.props.currentUserGroups.includes(AccountUserGroup.AccountUser) ||
                                    !this.props.currentUserGroups.includes(AccountUserGroup.Anonymous)
                                        ? this.getChangeMod()
                                        : this.getViewMode()
                                }
                            </div>
                            <Dialog
                                title="Изменить количество дополнительных сеансов?"
                                isOpen={ this.state.isOpenApplyDialog }
                                dialogWidth="sm"
                                dialogVerticalAlign="center"
                                onCancelClick={ this.closeApplyDialog }
                                buttons={ [
                                    {
                                        content: 'Нет',
                                        kind: 'default',
                                        onClick: this.closeApplyDialog
                                    },
                                    {
                                        content: 'Да',
                                        kind: 'error',
                                        onClick: new Date(this.props.cloudServiceResources.serviceExpireDate) < new Date() ? this.buyResources : this.showBuy
                                    },
                                ] }
                            >
                                <Box display="flex" flexDirection="column" gap="16px" alignItems="center">
                                    <ErrorOutlineIcon sx={ { fontSize: '120px', color: COLORS.warning } } />
                                    <TextOut className={ css.apply }>Смена количества дополнительных сеансов может привести к завершению сеансов пользователей.</TextOut>
                                </Box>
                            </Dialog>
                        </>
                    )
                }
            </>
        );
    }
}

const AmountWithReduxForm = withReduxForm(AdditionalSessionsClass, {
    reduxFormName: 'Amount_ReduxForm',
    resetOnUnmount: true
});

const AdditionalSessionsConnect = connect<StateProps, DispatchProps, OwnProps, AppReduxStoreState>(
    state => {
        const hasSessionSettingsReceived = state.Global.getCurrentSessionSettingsReducer.hasSuccessFor.hasSettingsReceived;
        const currentUserGroups = hasSessionSettingsReceived ? state.Global.getCurrentSessionSettingsReducer.settings.currentContextInfo.currentUserGroups : [];
        const { isDifferentContextAccount } = state.Global.getCurrentSessionSettingsReducer.settings.currentContextInfo;
        const accountUserSessions = state.PersonalAdditionalSession.personalAdditionalSessionsReducer.personalAdditionalSessions.data?.userSessions;
        const commonSessions = state.PersonalAdditionalSession.personalAdditionalSessionsReducer.personalAdditionalSessions.data?.commonSessions;

        return {
            dataset: state.AdditionalSessionState.sessionsList,
            cloudServiceResources: state.AdditionalSessionState.cloudServiceResources,
            hasSessionsListReceived: state.AdditionalSessionState.hasSuccessFor.hasSessionsListReceived,
            hasCloudServicesResourcesReceived: state.AdditionalSessionState.hasSuccessFor.hasCloudServiceResources,
            currentUserGroups,
            cloudService: state.AdditionalSessionState.changeCloudServiceResources,
            hasReceivedCloudService: state.AdditionalSessionState.hasSuccessFor.hasChangeCloudServiceResources,
            accountUserSessions,
            commonSessions: commonSessions ?? 0,
            hasBuyResources: state.AdditionalSessionState.hasSuccessFor.hasBuyResources,
            additionalSessionsUsed: state.AdditionalSessionState.sessionsList.additionalSessionsUsed,
            cloudServiceResourcesError: state.AdditionalSessionState.cloudServiceResourcesError,
            isDifferentContextAccount
        };
    },
    {
        dispatchGetSessionListThunk: GetSessionListThunk.invoke,
        dispatchGetCloudServiceResourcesThunk: GetCloudServiceResourcesThunk.invoke,
        dispatchChangeCloudServiceResourcesThunk: ChangeCloudServiceResourcesThunk.invoke,
        dispatchResetCloudServiceResourcesThunk: ResetCloudServiceResourcesThunk.invoke,
    }
)(withFloatMessages(AmountWithReduxForm));

const AdditionalSessionsFunction = () => {
    const dispatch = useAppDispatch();

    return (
        <div>
            <AdditionalSessionsConnect getPersonalSession={ () => getExtraSession(dispatch) } />
        </div>
    );
};

export const AdditionalSessionsView = withHeader({
    title: 'Дополнительные сеансы',
    page: AdditionalSessionsFunction
});