export type EndingUserSessionsDataForm = {
    /**
     * Сообщение пользователю
     */
    message: string;
    /**
     * Закончить сеанс без предпреждения
     */
    warning: boolean;
};