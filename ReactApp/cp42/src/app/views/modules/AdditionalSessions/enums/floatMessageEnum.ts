export enum FloatMessageEnum {
    buyResources = 'Дополнительные сеансы успешно куплены',
    promisePayment = 'Обещанный платеж успешно взят',
    endingAdditionSession = 'Сеанс будет завершен в течение нескольких минут'
}