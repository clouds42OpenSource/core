import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { Pagination, TableHead, TextField } from '@mui/material';
import Accordion from '@mui/material/Accordion';
import AccordionDetails from '@mui/material/AccordionDetails';
import AccordionSummary from '@mui/material/AccordionSummary';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableRow from '@mui/material/TableRow';
import { TUserSessionsItem } from 'app/api/endpoints/additionalSessions/response';
import { REDUX_API } from 'app/api/useReduxApi';
import { EMessageType, useAppDispatch, useAppSelector, useFloatMessages } from 'app/hooks';
import { TextOut } from 'app/views/components/TextOut';
import { TablePagination } from 'app/views/modules/_common/components/CommonTableWithFilter/TableView';
import { PersonalSessionsInput } from 'app/views/modules/AdditionalSessions/views/PersonalSessionsInput';
import React, { useCallback, useEffect, useState } from 'react';
import style from './style.module.css';

const { getExtraSession } = REDUX_API.ADDITIONAL_SESSIONS_REDUX;

export const PersonalAdditionalSessions = () => {
    const dispatch = useAppDispatch();
    const { data, message } = useAppSelector(state => state.PersonalAdditionalSession.personalAdditionalSessionsReducer.personalAdditionalSessions);
    const { currency } = useAppSelector(state => state.AdditionalSessionState.cloudServiceResources);

    const { show } = useFloatMessages();

    const [search, setSearch] = useState('');
    const [userSessionsPagination, setUserSessionsPagination] = useState<TablePagination>({
        pageSize: 10,
        currentPage: 1,
        totalPages: 1,
        recordsCount: 0
    });

    useEffect(() => {
        void getExtraSession(dispatch);
    }, [dispatch]);

    const setPagination = useCallback((records: TUserSessionsItem[]) => {
        if (records.length) {
            setUserSessionsPagination(prev => ({
                ...prev,
                totalPages: Math.ceil(records.length / 10),
                recordsCount: records.length
            }));
        }
    }, []);

    useEffect(() => {
        if (data && data.userSessions) {
            setPagination(data.userSessions);
        }
    }, [data, setPagination]);

    useEffect(() => {
        if (message) {
            show(EMessageType.error, message);
        }
    }, [message, show]);

    const currentPageHandler = (_: React.ChangeEvent<unknown>, page: number) => {
        setUserSessionsPagination(prev => ({
            ...prev,
            currentPage: page
        }));
    };

    const onSearch = (event: React.ChangeEvent<HTMLInputElement>) => {
        const searchLogin = event.target.value;
        if (data) {
            const records = data.userSessions.filter(item => item.login.toLowerCase().includes(searchLogin));

            setPagination(records);
        }

        setSearch(searchLogin);
    };

    return (
        <Accordion className={ style.accordion }>
            <AccordionSummary
                expandIcon={ <ExpandMoreIcon /> }
                className={ style.summary }
            >
                Настроить личные дополнительные сеансы по пользователям
            </AccordionSummary>
            <AccordionDetails>
                {
                    data && data.userSessions && (
                        <TextField
                            label="Поиск пользователя"
                            value={ search }
                            onChange={ onSearch }
                            InputLabelProps={ { shrink: true } }
                            className={ style.search }
                        />
                    )
                }
                {
                    data && data.userSessions
                        ? (
                            <>
                                <TableContainer>
                                    <Table>
                                        <TableHead>
                                            <TableRow>
                                                <TableCell />
                                                <TableCell align="center" className={ style.element }>Количество</TableCell>
                                                <TableCell align="center" className={ style.element }>Всего, {currency}/мес</TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {
                                                data.userSessions
                                                    .filter(item => item.login.toLowerCase().includes(search.toLowerCase()))
                                                    .slice((userSessionsPagination.currentPage - 1) * userSessionsPagination.pageSize, userSessionsPagination.currentPage * userSessionsPagination.pageSize)
                                                    .map(row => (
                                                        <TableRow key={ row.userID }>
                                                            <TableCell className={ style.element }>{row.login}</TableCell>
                                                            <TableCell align="right" className={ style.element }><PersonalSessionsInput value={ row.sessions } item={ row } /></TableCell>
                                                            <TableCell align="center" className={ style.element }><span>{ row.sessions * 950 }</span></TableCell>
                                                        </TableRow>
                                                    ))
                                            }
                                        </TableBody>
                                    </Table>
                                </TableContainer>
                                <Pagination
                                    className={ style.pagination }
                                    page={ userSessionsPagination.currentPage }
                                    count={ userSessionsPagination.totalPages }
                                    onChange={ currentPageHandler }
                                />
                            </>
                        )
                        : <TextOut inDiv={ true } textAlign="center" fontSize={ 13 }>нет записей</TextOut>
                }

            </AccordionDetails>
        </Accordion>
    );
};