import { TUserSessionsItem } from 'app/api/endpoints/additionalSessions/response';

export type TPropsPersonalSessions = {
    item: TUserSessionsItem;
    value: number;
};