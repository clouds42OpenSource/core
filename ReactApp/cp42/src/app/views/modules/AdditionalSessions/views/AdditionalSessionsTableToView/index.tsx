import React from 'react';
import { AdditionalSessionsTableView } from 'app/views/modules/AdditionalSessions/views/AdditionalSessionsTable';
import { SessionListItem } from 'app/web/InterlayerApiProxy/AdditionalSessionsApiProxy/getSessionList/data-models';

type StateProps = {
    dataset: SessionListItem[]
};

export class AdditionalSessionsTableToViewClass extends React.Component<StateProps> {
    public constructor(props: StateProps) {
        super(props);
    }

    public render() {
        return (
            <>
                {
                    this.props.dataset.map(item => <AdditionalSessionsTableView dataset={ item.data } key={ item.group.id } />)
                }
            </>
        );
    }
}