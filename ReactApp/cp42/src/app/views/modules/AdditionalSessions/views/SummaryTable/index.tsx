import { TableHead } from '@mui/material';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableRow from '@mui/material/TableRow';
import { useAppSelector } from 'app/hooks';
import style from 'app/views/modules/AdditionalSessions/views/PersonalAdditionalSessions/style.module.css';
import React from 'react';

type TSummaryTableProps = {
    commonSessions: number;
};

export const SummaryTable = ({ commonSessions }: TSummaryTableProps) => {
    const { data } = useAppSelector(state => state.PersonalAdditionalSession.personalAdditionalSessionsReducer.personalAdditionalSessions);
    const { currency } = useAppSelector(state => state.AdditionalSessionState.cloudServiceResources);

    const getCount = () => {
        let count = 0;

        if (data) {
            const userCount = data.userSessions.reduce((prev, curr) => {
                prev += curr.sessions;

                return prev;
            }, 0);

            count = userCount + commonSessions;
        }

        return count;
    };

    return (
        <TableContainer sx={ { maxWidth: '55%' } }>
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell />
                        <TableCell align="center" className={ style.element }>Количество сеансов</TableCell>
                        <TableCell align="center" className={ style.element }>Стоимость одного сеанса</TableCell>
                        <TableCell align="center" className={ style.element }>Всего, {currency}/мес</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    <TableRow>
                        <TableCell className={ style.element }>Итого</TableCell>
                        <TableCell align="center" className={ style.element }>{ getCount() }</TableCell>
                        <TableCell align="center" className={ style.element }>950</TableCell>
                        <TableCell align="center" className={ style.element }>{ getCount() * 950 }</TableCell>
                    </TableRow>
                </TableBody>
            </Table>
        </TableContainer>
    );
};