import { OutlinedInput } from '@mui/material';
import Stack from '@mui/material/Stack';
import { useAppDispatch } from 'app/hooks';
import { personalAdditionalSessionsSlice } from 'app/modules/additionalSessions/store/reducers/PersonalAdditionalSessionsSlice';
import { BaseButton } from 'app/views/components/controls/Button/BaseButton';
import { TPropsPersonalSessions } from 'app/views/modules/AdditionalSessions/views/PersonalSessionsInput/types';
import React, { memo } from 'react';
import css from './style.module.css';

export const PersonalSessionsInput = memo(({ value, item }: TPropsPersonalSessions) => {
    const dispatch = useAppDispatch();
    const { plus, minus, onChange } = personalAdditionalSessionsSlice.actions;

    return (
        <Stack direction="row" alignItems="center" justifyContent="center">
            <BaseButton
                variant="text"
                kind="error"
                onClick={ () => dispatch(minus(item.userID)) }
                style={ { minWidth: '30px', maxHeight: '30px' } }
            >
                <i className="fa fa-minus" />
            </BaseButton>
            <OutlinedInput
                fullWidth={ true }
                className={ css.input }
                type="number"
                onChange={ event => { dispatch(onChange([item.userID, +event.target.value.replace(/[^0-9]/g, '')])); } }
                value={ `${ value }` }
            />
            <BaseButton
                variant="text"
                kind="turquoise"
                onClick={ () => dispatch(plus(item.userID)) }
                style={ { minWidth: '30px', maxHeight: '30px' } }
            >
                <i className="fa fa-plus" />
            </BaseButton>
        </Stack>
    );
});