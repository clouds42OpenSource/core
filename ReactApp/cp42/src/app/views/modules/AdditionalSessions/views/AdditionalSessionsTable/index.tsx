import { contextAccountId } from 'app/api';
import React from 'react';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { BaseButton } from 'app/views/components/controls/Button/BaseButton';
import { DataItemType } from 'app/web/InterlayerApiProxy/AdditionalSessionsApiProxy/getSessionList/data-models';
import { TextOut } from 'app/views/components/TextOut';
import { Dialog } from 'app/views/components/controls/Dialog';
import { CheckBoxForm, TextBoxForm } from 'app/views/components/controls/forms';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { EndingUserSessionsDataForm } from 'app/views/modules/AdditionalSessions/types/EndingUserSessionsDataForm';
import { EndingUserSessionParams } from 'app/web/InterlayerApiProxy/AdditionalSessionsApiProxy/endingUserSession/input-params';
import { EndingUserSessionThunk } from 'app/modules/additionalSessions/store/thunks';
import { connect } from 'react-redux';
import { AppReduxStoreState } from 'app/redux/types';
import cn from 'classnames';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { FloatMessageEnum } from 'app/views/modules/AdditionalSessions/enums/floatMessageEnum';
import { Box } from '@mui/material';
import css from './styles.module.css';

type DispatchProps = {
    dispatchEndingUserSession: (args: EndingUserSessionParams) => void;
};

type StateProps = {
    isDifferentContextAccount: boolean;
};

type OwnProps = ReduxFormProps<EndingUserSessionsDataForm> & FloatMessageProps & {
    dataset: DataItemType[];
};

type OwnState = {
    isVisible: boolean;
    idSessions: string;
};

type AllProps = StateProps & DispatchProps & OwnProps;

class AdditionalSessionTableClass extends React.Component<AllProps, OwnState> {
    public constructor(props: AllProps) {
        super(props);

        this.endingUserSession = this.endingUserSession.bind(this);
        this.onValueChange = this.onValueChange.bind(this);
        this.confirmationEndingSessions = this.confirmationEndingSessions.bind(this);
        this.initReduxForm = this.initReduxForm.bind(this);
        this.props.reduxForm.setInitializeFormDataAction(this.initReduxForm);

        this.state = {
            isVisible: false,
            idSessions: ''
        };
    }

    private onValueChange<TValue>(formName: string, newValue: TValue) {
        this.props.reduxForm.updateReduxFormFields({
            [formName]: newValue
        });
    }

    /**
     * Модальное окно завершения сеанса
     * @param sessionId передается в диспатч
     * @private
     */
    private endingUserSession(sessionId: string) {
        this.setState(prevState => ({
            isVisible: !prevState.isVisible,
            idSessions: sessionId
        }));
    }

    private initReduxForm(): EndingUserSessionsDataForm | undefined {
        return {
            message: 'Сеанс завершен администратором в связи с регламентными работами',
            warning: false
        };
    }

    /**
     * Завершение сеанса, весит на кнопке "Завершить"
     * @param id сеанса
     * @private
     */
    private confirmationEndingSessions(id: string) {
        const reduxForm = this.props.reduxForm.getReduxFormFields(false);

        this.props.dispatchEndingUserSession({
            sessionId: id,
            message: reduxForm.message,
            notify: !reduxForm.warning,
            dummy: false,
            accountId: this.props.isDifferentContextAccount ? contextAccountId() : undefined
        });

        this.setState({
            isVisible: false
        });

        this.props.floatMessage.show(MessageType.Success, FloatMessageEnum.endingAdditionSession, 1500);
    }

    public render() {
        const reduxForm = this.props.reduxForm.getReduxFormFields(false);

        return (
            <div data-testid="additional-sessions-table">
                <CommonTableWithFilter
                    uniqueContextProviderStateId="AdditionalSessionsTableToViewContextProviderStateId"
                    tableProps={ {
                        dataset: this.props.dataset,
                        keyFieldName: 'id',
                        isResponsiveTable: false,
                        fieldsView: {
                            id: {
                                caption: ' ',
                                format: () => (
                                    <div style={ { width: '20px' } } />
                                ),
                            },
                            application: {
                                caption: 'Информационная база',
                                fieldContentNoWrap: true,
                                style: { lineHeight: '35px' },
                                format: (value: string, data) => (
                                    <TextOut fontSize={ 12 }>{ data.application.name } ({ data.application.code })</TextOut>
                                )
                            },
                            start: {
                                caption: 'Начало работы',
                                fieldContentNoWrap: true,
                                format: (value: string, data) => (
                                    <TextOut fontSize={ 12 }>{ new Date(Date.parse(data.start)).toLocaleString() }</TextOut>
                                )
                            },
                            locked: {
                                caption: 'Статус сеанса',
                                format: (value: string, data) => (
                                    <TextOut fontSize={ 12 }>{
                                        !data.locked && !data.sleeping ? 'Активный'
                                            : data.locked ? 'Заблокирован' : 'Спящий'
                                    }
                                    </TextOut>
                                )
                            },
                            timestamp: {
                                caption: '',
                                format: (value: string, data) => {
                                    return (
                                        <BaseButton
                                            variant="text"
                                            kind="primary"
                                            style={ { fontSize: '12px' } }
                                            onClick={ () => this.endingUserSession(data.id) }
                                        >
                                            Завершить
                                        </BaseButton>
                                    );
                                }
                            },
                        }
                    } }
                />
                <Dialog
                    data-testid="additional-sessions-dialog"
                    isOpen={ this.state.isVisible }
                    dialogWidth="xs"
                    dialogVerticalAlign="center"
                    buttons={ [
                        {
                            content: 'Отмена',
                            kind: 'default',
                            variant: 'contained',
                            onClick: () => this.setState(prevState => ({ isVisible: !prevState.isVisible }))
                        },
                        {
                            content: 'Завершить',
                            kind: 'error',
                            variant: 'contained',
                            onClick: () => this.confirmationEndingSessions(this.state.idSessions)
                        }
                    ] }
                    onCancelClick={ () => this.setState(prevState => ({ isVisible: !prevState.isVisible })) }
                    contentClassName={ cn(css['dialog-wrapper']) }
                >
                    <Box display="flex" flexDirection="column" alignItems="center" justifyContent="space-between" width="100%" height="400px">
                        <i className="fa fa-5x fa-exclamation-circle" color="#FFCF34" />
                        <h2>Завершение сеансов</h2>
                        <TextOut>Работа пользователя завершаемого сеанса будет прекращена, все несохраненные данные будут потеряны.</TextOut>
                        <div className={ cn(css.item) }>
                            <TextBoxForm
                                formName="message"
                                label="Сообщение для пользователя"
                                type="textarea"
                                value={ reduxForm.message }
                                onValueChange={ this.onValueChange }
                            />
                        </div>
                        <CheckBoxForm
                            className={ cn(css.item) }
                            formName="warning"
                            label=""
                            isChecked={ reduxForm.warning }
                            trueText={ <TextOut fontWeight={ 400 } fontSize={ 13 }>Завершить без предупреждения</TextOut> }
                            falseText={ <TextOut fontWeight={ 400 } fontSize={ 13 }>Завершить без предупреждения</TextOut> }
                            onValueChange={ this.onValueChange }
                        />
                    </Box>
                </Dialog>
            </div>
        );
    }
}

const AdditionalSessionsTableWithReduxForm = withReduxForm(AdditionalSessionTableClass, {
    reduxFormName: 'EndingUserSession_ReduxForm',
    resetOnUnmount: true
});

const AdditionalSessionsTableConnect = connect<StateProps, DispatchProps, {}, AppReduxStoreState>(
    state => ({
        isDifferentContextAccount: state.Global.getCurrentSessionSettingsReducer.settings.currentContextInfo.isDifferentContextAccount
    }),
    {
        dispatchEndingUserSession: EndingUserSessionThunk.invoke
    }
)(AdditionalSessionsTableWithReduxForm);

export const AdditionalSessionsTableView = withFloatMessages(AdditionalSessionsTableConnect);