import React from 'react';
import { Collapse, TableCell, TableRow } from '@mui/material';
import { BaseButton } from 'app/views/components/controls/Button/BaseButton';
import { AdditionalSessionsTableView } from 'app/views/modules/AdditionalSessions/views/AdditionalSessionsTable';
import { SessionListItem } from 'app/web/InterlayerApiProxy/AdditionalSessionsApiProxy/getSessionList/data-models';

type StateProps = {
    dataset: SessionListItem
};

type OwnState = {
    open: boolean;
};

export class CollapseRow extends React.Component<StateProps, OwnState> {
    public constructor(props: StateProps) {
        super(props);

        this.state = {
            open: false
        };
    }

    private countUserAdditionalSessions = this.props.dataset.data.filter(item => (!item.sleeping && !item.locked)).length - 1;

    public render() {
        return (
            <>
                <TableRow data-testid="collapse-row">
                    <TableCell sx={ { padding: '0', textAlign: 'center', fontSize: '12px', width: '40px' } }>
                        <BaseButton variant="text" size="small" onClick={ () => this.setState(prevState => ({ open: !prevState.open })) }>
                            { this.state.open ? <i data-testid="collapse-row-button" className="fa fa-minus" /> : <i className="fa fa-plus" /> }
                        </BaseButton>
                    </TableCell>
                    <TableCell sx={ { fontSize: '12px', color: '#52AAEF' } }>{ this.props.dataset.group.name }</TableCell>
                    <TableCell sx={ { fontSize: '12px' } }>
                        { this.countUserAdditionalSessions < 0 ? 0 : this.countUserAdditionalSessions }
                    </TableCell>
                    <TableCell />
                    <TableCell />
                </TableRow>
                <TableRow>
                    <TableCell style={ { paddingBottom: 0, paddingTop: 0 } } colSpan={ 5 }>
                        <Collapse in={ this.state.open } timeout="auto" unmountOnExit={ true } data-testid="collapse-row-hidden">
                            <AdditionalSessionsTableView dataset={ this.props.dataset.data } />
                        </Collapse>
                    </TableCell>
                </TableRow>
            </>
        );
    }
}