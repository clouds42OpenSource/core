import React from 'react';
import { Box, Grid, Paper, Typography } from '@mui/material';
import cn from 'classnames';
import { GroupAdd } from '@mui/icons-material';
import css from './style.module.css';

type OwnProps = {
    count: string;
    mb?: string;
};

export class AdditionalCardView extends React.Component<OwnProps> {
    public render() {
        return (
            <Paper
                elevation={ 1 }
                className={ cn(css['card-container']) }
                style={ { marginBottom: this.props.mb } }
                data-testid="additional-card-view"
            >
                <Box>
                    <Grid container={ true } spacing={ 2 }>
                        <Grid item={ true }><GroupAdd sx={ { fontSize: '55px', color: '#676A6C' } } /></Grid>
                        <Grid item={ true }>
                            <Box sx={ { marginTop: '5px' } }>
                                <Typography
                                    sx={ { fontSize: '13px', fontWeight: 700, color: '#3A4459' } }
                                    noWrap={ true }
                                    align="center"
                                >
                                    Использовано доп.сеансов
                                </Typography>
                            </Box>
                            <Box>
                                <Typography
                                    sx={ { fontSize: '18px', fontWeight: 500, color: '#3A4459', lineHeight: '21px' } }
                                >
                                    { this.props.count }
                                </Typography>
                            </Box>
                        </Grid>
                    </Grid>
                </Box>
            </Paper>
        );
    }
}