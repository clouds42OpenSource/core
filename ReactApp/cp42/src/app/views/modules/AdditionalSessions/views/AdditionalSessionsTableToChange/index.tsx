import { TUserSessionsItem } from 'app/api/endpoints/additionalSessions/response';
import { AppRoutes } from 'app/AppRoutes';
import { CloudServiceResourcesDataModel } from 'app/web/InterlayerApiProxy/AdditionalSessionsApiProxy/getCloudServiceResources';
import React from 'react';
import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@mui/material';
import { SessionListItem } from 'app/web/InterlayerApiProxy/AdditionalSessionsApiProxy/getSessionList/data-models';
import { CollapseRow } from 'app/views/modules/AdditionalSessions/views/CollapseRow';
import { hasComponentChangesFor } from 'app/common/functions';
import { BuyCloudServiceResourcesThunkParams } from 'app/modules/additionalSessions/store/reducers/BuyCloudServiceResourcesReducer/params';
import { ResetCloudServiceResourcesThunkParams } from 'app/modules/additionalSessions/store/reducers/ResetCloudServiceResourcesReducer/params';
import { ChangeCloudServiceResourcesDataModel } from 'app/web/InterlayerApiProxy/AdditionalSessionsApiProxy/changeCloudServiceResources/data-models';
import { BuyCloudServiceResourcesThunk, ResetCloudServiceResourcesThunk } from 'app/modules/additionalSessions/store/thunks';
import { connect } from 'react-redux';
import { AppReduxStoreState } from 'app/redux/types';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { AmountDataForm } from 'app/views/modules/AdditionalSessions/types/AmountDataForm';
import { Dialog } from 'app/views/components/controls/Dialog';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { FloatMessageEnum } from 'app/views/modules/AdditionalSessions/enums/floatMessageEnum';
import { TextOut } from 'app/views/components/TextOut';
import { withRouter } from 'react-router';
import { RouteComponentProps } from 'react-router-dom';

type StateProps = {
    cloudService: ChangeCloudServiceResourcesDataModel;
    cloudServiceResources: CloudServiceResourcesDataModel;
    accountUserSessions?: TUserSessionsItem[];
};

type OwnProps = ReduxFormProps<AmountDataForm> & FloatMessageProps & {
    dataset: SessionListItem[];
    amount: number;
    serviceExpireData: string;
    updateDataset: () => void;
    currency: string;
    visible: boolean;
    closeDialog: () => void;
    closeApplyDialog: () => void;
    isRent1C: boolean
};

type DispatchProps = {
    dispatchBuyCloudServiceResourcesThunk: (args: BuyCloudServiceResourcesThunkParams) => void;
    dispatchResetCloudServiceResourcesThunk: (args: ResetCloudServiceResourcesThunkParams) => void;
};

type OwnState = {};

type AllProps = StateProps & OwnProps & DispatchProps & RouteComponentProps;

class AdditionalSessionsTableToChangeClass extends React.Component<AllProps, OwnState> {
    public constructor(props: AllProps) {
        super(props);

        this.buyResources = this.buyResources.bind(this);
        this.getPromisePayment = this.getPromisePayment.bind(this);
        this.redirectToBilling = this.redirectToBilling.bind(this);
    }

    public shouldComponentUpdate(nextProps: AllProps, nextState: OwnState) {
        return hasComponentChangesFor(this.props, nextProps) || hasComponentChangesFor(this.state, nextState);
    }

    /**
     * Взять обещанный платеж
     * @private
     */
    private getPromisePayment(amount: string, personalSum: number) {
        const reduxForm = this.props.reduxForm.getReduxFormFields(false);

        this.props.dispatchBuyCloudServiceResourcesThunk({
            isPromisePayment: true,
            costOfTariff: +this.props.cloudService.costOftariff.toString(),
            count: (parseInt(reduxForm.amount ?? amount, 10) + (personalSum ?? 0)),
            accountUserSessions: this.props.accountUserSessions ?? [],
            force: true
        });

        this.props.closeDialog();
        this.props.closeApplyDialog();

        this.props.updateDataset();

        this.props.floatMessage.show(MessageType.Info, FloatMessageEnum.promisePayment, 1500);
    }

    /**
     * Покупка дополнительных сеансов за деньги на аккаунте
     * @private
     */
    private buyResources(amount: string, personalSum: number) {
        const reduxForm = this.props.reduxForm.getReduxFormFields(false);

        this.props.dispatchBuyCloudServiceResourcesThunk({
            isPromisePayment: false,
            costOfTariff: +this.props.cloudService.costOftariff.toString(),
            count: (parseInt(reduxForm.amount ?? amount, 10) + (personalSum ?? 0)),
            accountUserSessions: this.props.accountUserSessions ?? [],
            force: true
        });

        this.props.closeDialog();
        this.props.closeApplyDialog();

        this.props.updateDataset();

        this.props.floatMessage.show(MessageType.Success, FloatMessageEnum.buyResources, 1500);
    }

    private redirectToBilling() {
        this.props.history.push(`${ AppRoutes.accountManagement.replenishBalance }?customPayment.amount=${ this.props.cloudService.enoughMoney }&customPayment.description=Оплата+за+использование+сервиса+"Дополнительные+сеансы"+до+${ new Date(this.props.serviceExpireData).toLocaleString('ru', { year: 'numeric', month: 'long', day: 'numeric', timeZone: 'UTC' }) }`);
    }

    public render() {
        const reduxForm = this.props.reduxForm.getReduxFormFields(false);
        const {
            dataset,
            amount,
            currency,
            visible,
            isRent1C
        } = this.props;

        const personalSum = this.props.accountUserSessions?.reduce((prev, curr) => {
            prev += curr.sessions;

            return prev;
        }, 0);

        return (
            <div data-testid="additional-sessions-table-to-change">
                {
                    dataset.length
                        ? (
                            <TableContainer>
                                <Table>
                                    <TableHead>
                                        <TableRow key="tableHead">
                                            <TableCell size="small" />
                                            <TableCell size="small" sx={ { fontSize: '12px', minWidth: '240px' } }>Пользователь</TableCell>
                                            <TableCell size="small" sx={ { fontSize: '12px', maxWidth: '250px' } }>Использует дополнительных сеансов</TableCell>
                                            <TableCell />
                                            <TableCell />
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {
                                            dataset.map(item => {
                                                return (
                                                    <CollapseRow dataset={ item } key={ `${ item.data[0].id }` } />
                                                );
                                            })
                                        }
                                    </TableBody>
                                </Table>
                            </TableContainer>
                        )
                        : <TextOut inDiv={ true } textAlign="center" fontSize={ 13 }>нет записей</TextOut>
                }
                <Dialog
                    isOpen={ visible && !this.props.cloudService.complete }
                    isTitleSmall={ true }
                    title="Оплата сервиса Дополнительные сеансы"
                    dialogWidth="sm"
                    dialogVerticalAlign="center"
                    buttons={ this.props.cloudService.enoughMoney
                        ? this.props.cloudService.canGetPromisePayment ? [
                            {
                                content: 'Отмена',
                                kind: 'default',
                                variant: 'contained',
                                onClick: () => this.props.closeDialog()
                            },
                            {
                                content: `Пополнить счет на (${ this.props.cloudService.enoughMoney.toFixed(2) } ${ currency })`,
                                kind: 'primary',
                                variant: 'contained',
                                onClick: this.redirectToBilling
                            },
                            {
                                content: 'Обещанный платеж',
                                kind: 'primary',
                                variant: 'contained',
                                onClick: () => this.getPromisePayment((reduxForm.amount ?? this.props.amount), personalSum ?? 0)
                            }
                        ] : [
                            {
                                content: 'Отмена',
                                kind: 'default',
                                variant: 'contained',
                                onClick: () => this.props.closeDialog()
                            },
                            {
                                content: `Пополнить счет на (${ this.props.cloudService.enoughMoney.toFixed(2) } ${ currency })`,
                                kind: 'primary',
                                variant: 'contained',
                                onClick: this.redirectToBilling
                            }
                        ]
                        : [
                            {
                                content: 'Отмена',
                                kind: 'default',
                                variant: 'contained',
                                onClick: () => this.props.closeDialog()
                            },
                            {
                                content: `Купить (${ this.props.cloudService.costOftariff.toFixed(2) } ${ currency })`,
                                kind: 'success',
                                variant: 'contained',
                                onClick: () => this.buyResources((reduxForm.amount ?? this.props.amount), personalSum ?? 0)
                            }
                        ]
                    }
                    onCancelClick={ () => {
                        this.props.closeDialog();
                        this.props.updateDataset();
                        this.props.dispatchResetCloudServiceResourcesThunk({});
                    } }
                >
                    <TextOut>Сумма { this.props.cloudService.costOftariff } { currency } будет списана с баланса за покупку {
                        isRent1C
                            ? (parseInt(reduxForm.amount ?? amount, 10) + (personalSum ?? 0)) - this.props.cloudServiceResources.usedLicenses
                            : parseInt(reduxForm.amount ?? amount, 10) + (personalSum ?? 0)
                    } дополнительных сеансов.
                    </TextOut>
                </Dialog>
            </div>
        );
    }
}

const AmountWithReduxForm = withReduxForm(AdditionalSessionsTableToChangeClass, {
    reduxFormName: 'Amount_ReduxForm',
    resetOnUnmount: true
});

const AdditionalSessionsTableToChangeConnect = connect<StateProps, DispatchProps, {}, AppReduxStoreState>(
    state => {
        const accountUserSessions = state.PersonalAdditionalSession.personalAdditionalSessionsReducer.personalAdditionalSessions.data?.userSessions;

        return {
            cloudService: state.AdditionalSessionState.changeCloudServiceResources,
            cloudServiceResources: state.AdditionalSessionState.cloudServiceResources,
            accountUserSessions
        };
    },
    {
        dispatchBuyCloudServiceResourcesThunk: BuyCloudServiceResourcesThunk.invoke,
        dispatchResetCloudServiceResourcesThunk: ResetCloudServiceResourcesThunk.invoke
    }
)(AmountWithReduxForm);

export const AdditionalSessionsTableToChangeView = withFloatMessages(withRouter(AdditionalSessionsTableToChangeConnect));