export enum ValidateErrorEnum {
    emailError = 'Неверный формат почты',
    requiredEmail = 'Обязательное поле',
    loginError = 'Ошибка при проверке логина',
    phoneError = 'Неверный формат номера телефона',
}