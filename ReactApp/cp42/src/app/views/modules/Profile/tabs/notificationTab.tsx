import { Box } from '@mui/material';
import { ETypeNotification, TNotificationSettingsItem } from 'app/api/endpoints/notification/request/postNotificationSettings';
import { FETCH_API } from 'app/api/useFetchApi';
import { EMessageType, useAppSelector, useFloatMessages } from 'app/hooks';
import { TextOut } from 'app/views/components/TextOut';
import { SuccessButton, TextButton, TextLinkButton } from 'app/views/components/controls/Button';
import { Dialog } from 'app/views/components/controls/Dialog';
import { getInitialNotificationSettingsState, getLabel } from 'app/views/modules/Profile/functions';
import { TRestrictions } from 'app/views/modules/Profile/tabs/types';
import { TypeNotification } from 'app/views/modules/Profile/views/TypeNotification';
import { QrCodeElement } from 'app/views/modules/_common/reusable-modules/QrCodeElement';
import React, { memo, useEffect, useState } from 'react';
import style from '../style.module.css';

const { changeNotificationSettings } = FETCH_API.NOTIFICATIONS;

export const NotificationTab = memo(() => {
    const { data, refreshData } = FETCH_API.PROFILE.useGetProfile();
    const { getCurrentSessionSettingsReducer: { settings: { currentContextInfo } } } = useAppSelector(state => state.Global);
    const { show } = useFloatMessages();

    const [notificationSettings, setNotificationSettings] = useState<TNotificationSettingsItem[]>(getInitialNotificationSettingsState);
    const [showWarningDialog, setShowWarningDialog] = useState(false);
    const [showQrCode, setShowQrCode] = useState(false);

    const restrictions: Record<ETypeNotification, TRestrictions> = {
        [ETypeNotification.Email]: {
            isDisable: !currentContextInfo.isEmailExists || !currentContextInfo.isEmailVerified,
            description: 'Для получения уведомлений по почте необходимо ее заполнить и подтвердить'
        },
        [ETypeNotification.Sms]: {
            isDisable: !currentContextInfo.isPhoneExists || !currentContextInfo.isPhoneVerified,
            description: 'Для получения уведомлений по смс необходимо заполнить и подтвердить телефон'
        },
        [ETypeNotification.WhatsApp]: {
            isDisable: !currentContextInfo.isPhoneExists || !currentContextInfo.isPhoneVerified,
            description: 'Для получения уведомлений в WhatsApp необходимо заполнить и подтвердить телефон'
        },
        [ETypeNotification.Telegram]: {
            isDisable: false,
            description: ''
        }
    };

    useEffect(() => {
        if (data && data.rawData && data.rawData.notificationSettings.length) {
            setNotificationSettings(prev => prev.map(item => {
                const searchElement = data.rawData.notificationSettings.find(findItem => findItem.notification.type === item.notification.type);

                return searchElement ?? item;
            }));
        }
    }, [data]);

    const changeTypeNotificationForm = (event: React.ChangeEvent<HTMLInputElement>, type: ETypeNotification) => {
        setNotificationSettings(prev => prev.map(item => {
            if (item.notification.type === type) {
                return {
                    ...item,
                    isActive: event.target.checked
                };
            }

            return {
                ...item,
                isActive: false
            };
        }));
    };

    const getDescription = (type: ETypeNotification, item: TNotificationSettingsItem) => {
        switch (type) {
            case ETypeNotification.Email:
                return <TextOut fontSize={ 14 }>{ data && data.rawData ? data.rawData.email : '' }</TextOut>;
            case ETypeNotification.Sms:
                return (
                    <TextOut fontSize={ 14 }>
                        {
                            data && data.rawData && data.rawData.phoneNumber && currentContextInfo.isPhoneExists && currentContextInfo.isPhoneVerified
                                ? `+${ data.rawData.phoneNumber }`
                                : 'Для активации уведомлений через смс введите и подтвердите номер телефона'
                        }
                    </TextOut>
                );
            case ETypeNotification.Telegram:
                return (
                    <TextOut fontSize={ 14 }>
                        Для настройки уведомлений вам достаточно активировать бота&nbsp;
                        {
                            data && data.rawData && data.rawData.notificationSettings.find(notificationSettingsItem => notificationSettingsItem.notification.type === ETypeNotification.Telegram)?.isActive
                                ? (
                                    <a
                                        href={ `https://t.me/clouds42bot?start=${ item.code }` }
                                        target="_blank"
                                        rel="noreferrer"
                                        className={ style.link }
                                    >
                                        @42CloudsBot
                                    </a>
                                )
                                : (
                                    <TextLinkButton
                                        onClick={ () => show(EMessageType.warning, 'Для активации бота сохраните настройки уведомлений') }
                                        className={ style.link }
                                    >
                                        @42CloudsBot
                                    </TextLinkButton>
                                )
                        }
                    </TextOut>
                );
            case ETypeNotification.WhatsApp:
                return (
                    <>
                        {
                            data && data.rawData && data.rawData.phoneNumber && currentContextInfo.isPhoneExists && currentContextInfo.isPhoneVerified
                                ? (
                                    <TextOut fontSize={ 14 }>
                                        Чтобы активировать уведомления в WhatsApp, перейдите по {
                                            data && data.rawData && data.rawData.notificationSettings.find(notificationSettingsItem => notificationSettingsItem.notification.type === ETypeNotification.WhatsApp)?.isActive
                                                ? (
                                                    <a
                                                        href={ `https://wa.me/77754687812?text=${ item.code }` }
                                                        target="_blank"
                                                        rel="noreferrer"
                                                        className={ style.link }
                                                    >
                                                        ссылке
                                                    </a>
                                                )
                                                : (
                                                    <TextLinkButton
                                                        onClick={ () => show(EMessageType.warning, 'Для активации бота сохраните настройки уведомлений') }
                                                        className={ style.link }
                                                    >
                                                        ссылке
                                                    </TextLinkButton>
                                                )
                                        } (WhatsApp установлен на компьютере) или отсканируйте {
                                            data && data.rawData && data.rawData.notificationSettings.find(notificationSettingsItem => notificationSettingsItem.notification.type === ETypeNotification.WhatsApp)?.isActive
                                                ? (
                                                    <TextLinkButton
                                                        onClick={ () => setShowQrCode(true) }
                                                        className={ style.link }
                                                    >
                                                        QR-код
                                                    </TextLinkButton>
                                                )
                                                : (
                                                    <TextLinkButton
                                                        onClick={ () => show(EMessageType.warning, 'Для активации бота сохраните настройки уведомлений') }
                                                        className={ style.link }
                                                    >
                                                        QR-код
                                                    </TextLinkButton>
                                                )
                                        } (для входа с телефона)
                                    </TextOut>
                                )
                                : (
                                    <TextOut fontSize={ 14 }>
                                        Для активации уведомлений по WhatsApp введите и подтвердите номер телефона
                                    </TextOut>
                                )
                        }
                        <Dialog
                            isOpen={ showQrCode }
                            onCancelClick={ () => setShowQrCode(false) }
                            dialogWidth="xs"
                            dialogVerticalAlign="center"
                        >
                            <Box display="flex" flexDirection="column" alignItems="center" gap="12px">
                                <TextOut fontSize={ 20 } fontWeight={ 500 } textAlign="center">Отсканируйте QR-код камерой вашего смартфона</TextOut>
                                <QrCodeElement text={ `https://wa.me/77754687812?text=${ item.code }` } />
                                <TextButton onClick={ () => setShowQrCode(false) }>Закрыть</TextButton>
                            </Box>
                        </Dialog>
                    </>
                );
            default:
                return null;
        }
    };

    const changeSettings = async () => {
        try {
            const response = await changeNotificationSettings(notificationSettings);

            if (response.success) {
                if (refreshData) {
                    refreshData();
                }

                show(EMessageType.success, 'Настройки уведомлений успешно сохранены!');
            } else {
                show(EMessageType.error, response.message);
            }
        } catch (err) {
            show(EMessageType.error, 'При изменение настроек уведомлений произошла ошибка. Пожалуйста, повторите позже или обратитесь в поддержку');
        }
    };

    const openWarningDialog = () => {
        setShowWarningDialog(true);
    };

    const closeWarningDialog = () => {
        setShowWarningDialog(false);
    };

    const saveNotificationSettings = () => {
        if (notificationSettings.some(item => item.isActive)) {
            void changeSettings();
        } else {
            openWarningDialog();
        }
    };

    return (
        <>
            <Box textAlign="end">
                <Box display="flex" flexDirection="column" gap="16px" pr="13px" pl="13px" mb="16px">
                    {
                        notificationSettings.map(item => {
                            if ((item.notification.type === ETypeNotification.WhatsApp || item.notification.type === ETypeNotification.Sms) && currentContextInfo.locale !== 'ru-ru') {
                                return null;
                            }

                            return (
                                <TypeNotification
                                    key={ item.id ?? item.notification.type }
                                    checkboxLabel={ getLabel(item.notification.type) }
                                    checked={ item.isActive }
                                    checkedChange={ event => changeTypeNotificationForm(event, item.notification.type) }
                                    description={ getDescription(item.notification.type, item) }
                                    isDisable={ restrictions[item.notification.type].isDisable ?? false }
                                    disabledDescription={ restrictions[item.notification.type].description ?? '' }
                                />
                            );
                        })
                    }
                </Box>
                <SuccessButton onClick={ saveNotificationSettings }>Сохранить</SuccessButton>
            </Box>
            <Dialog
                title="Внимание!"
                isOpen={ showWarningDialog }
                dialogWidth="sm"
                dialogVerticalAlign="center"
                onCancelClick={ closeWarningDialog }
                buttons={ [
                    {
                        content: 'Отменить',
                        kind: 'default',
                        onClick: closeWarningDialog,
                    },
                    {
                        content: 'Сохранить',
                        kind: 'success',
                        onClick: () => {
                            void changeSettings();
                            closeWarningDialog();
                        }
                    }
                ] }
            >
                При отключении всех способов уведомлений существует риск пропуска важного письма или напоминания!
            </Dialog>
        </>
    );
});