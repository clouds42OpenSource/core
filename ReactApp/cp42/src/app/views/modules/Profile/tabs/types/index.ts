export type TRestrictions = {
    isDisable: boolean;
    description: string;
};