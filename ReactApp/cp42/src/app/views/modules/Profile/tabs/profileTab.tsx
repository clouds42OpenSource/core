import { Box, Divider, Link } from '@mui/material';
import { Telegram, Google } from '@mui/icons-material';
import { userId } from 'app/api';
import { putProfileRequestDto } from 'app/api/endpoints/profile/request';
import { ProfileItemResponseDto } from 'app/api/endpoints/profile/response';
import { FETCH_API } from 'app/api/useFetchApi';
import { EMessageType, useAppSelector, useFloatMessages } from 'app/hooks';
import { SuccessButton } from 'app/views/components/controls/Button';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { TextOut } from 'app/views/components/TextOut';
import { CommonTableBasic, DatasetType } from 'app/views/modules/_common/components/CommonTableBasic';
import { EditProfileModal } from 'app/views/modules/Profile/views/EditProfileModal';
import { useCallback, useEffect, useState } from 'react';

import styles from '../style.module.css';

const { useGetSsoAccounts, deleteSsoAccount, useGetProfile, putProfile } = FETCH_API.PROFILE;

export const ProfileTab = () => {
    const { show } = useFloatMessages();

    const [loginUser, setLoginUser] = useState('');
    const [info, setInfo] = useState<ProfileItemResponseDto>();
    const [dataset, setDataset] = useState<DatasetType[]>([]);
    const [isOpen, setIsOpen] = useState(false);
    const [isLoading, setIsLoading] = useState(false);

    const createData = useCallback((caption: string, value: string, visible = true): DatasetType => ({ caption, value, visible }), []);

    const openModal = () => setIsOpen(true);
    const closeModal = () => setIsOpen(false);

    const { data, error, refreshData } = useGetProfile();
    const { data: ssoAccountsData, refreshData: refreshSsoAccountsData } = useGetSsoAccounts(data?.rawData.id);

    const { data: navMenuData } = useAppSelector(state => state.Global.getNavMenuReducer);

    useEffect(() => {
        window.scroll(0, 0);
    }, []);

    useEffect(() => {
        if (data && data.rawData) {
            const profileData = data.rawData;
            setDataset([
                createData('Логин', profileData.login),
                createData('Фамилия', profileData.lastName),
                createData('Имя', profileData.firstName),
                createData('Отчество', profileData.middleName ?? ''),
                createData('Электронная почта', profileData.email),
                createData('Телефон', profileData.phoneNumber)
            ]);
            setLoginUser(profileData.login);
            setInfo(profileData);
        }
    }, [data, createData]);

    const saveProfile = async (information: putProfileRequestDto) => {
        setIsLoading(true);
        await putProfile(information);
        closeModal();

        if (information.id === userId() && information.password && navMenuData?.rawData.topSideMenu) {
            window.location.href = navMenuData.rawData.topSideMenu.exitMenu.startMenu.href;
        }

        if (refreshData) {
            refreshData();
        }

        setIsLoading(false);
    };

    const removeSsoAccount = async (ssoId: string) => {
        if (data?.rawData.id) {
            const { message } = await deleteSsoAccount(data.rawData.id, ssoId);

            if (message) {
                show(EMessageType.error, message);
            } else if (refreshSsoAccountsData) {
                refreshSsoAccountsData();
            }
        }
    };

    if (error) {
        show(EMessageType.error, error);
    }

    return (
        <>
            { isLoading && <LoadingBounce /> }
            <Box display="flex" flexDirection="column" gap="15px">
                <CommonTableBasic dataset={ dataset } />
                { data?.rawData && (
                    <SuccessButton className={ styles.button } onClick={ openModal }>
                        Редактировать
                    </SuccessButton>
                ) }
                { !!ssoAccountsData?.rawData.accounts.length && (
                    <>
                        <Divider />
                        <CommonTableBasic
                            title="Привязанные аккаунты"
                            captionWidth="400px"
                            dataset={ ssoAccountsData.rawData.accounts.map(item => ({
                                caption: (
                                    <Box flexWrap="wrap" display="flex" gap="5px" alignItems="flex-end">
                                        { item.provider === 'tg' ? <Telegram /> : <Google /> }
                                        <TextOut fontWeight={ 700 }>{ item.description }</TextOut>
                                    </Box>
                                ),
                                value: (
                                    <Link onClick={ () => removeSsoAccount(item.id) }>Отвязать</Link>
                                ),
                                visible: true
                            })) }
                        />
                    </>
                ) }
            </Box>
            { info && (
                <EditProfileModal
                    isOpen={ isOpen }
                    loginUser={ loginUser }
                    onCancelClick={ closeModal }
                    info={ info }
                    save={ saveProfile }
                />
            ) }
        </>
    );
};