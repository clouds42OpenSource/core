import { ETypeNotification, TNotificationSettingsItem } from 'app/api/endpoints/notification/request/postNotificationSettings';

export const getInitialNotificationSettingsState = (): TNotificationSettingsItem[] => {
    return [
        {
            code: null,
            notification: {
                type: ETypeNotification.Email
            },
            isActive: false
        },
        {
            code: null,
            notification: {
                type: ETypeNotification.Sms
            },
            isActive: false
        },
        {
            code: null,
            notification: {
                type: ETypeNotification.Telegram
            },
            isActive: false
        },
        {
            code: null,
            notification: {
                type: ETypeNotification.WhatsApp
            },
            isActive: false
        }
    ];
};