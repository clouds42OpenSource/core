import { ETypeNotification } from 'app/api/endpoints/notification/request/postNotificationSettings';

export const getLabel = (type: ETypeNotification) => {
    switch (type) {
        case ETypeNotification.Email:
            return 'Эл. адрес (основной)';
        case ETypeNotification.Sms:
            return 'Смс';
        case ETypeNotification.Telegram:
            return 'Telegram (чат бот)';
        case ETypeNotification.WhatsApp:
            return 'WhatsApp';
        default:
            return '';
    }
};