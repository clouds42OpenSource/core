import { Box, FormControlLabel, Tooltip } from '@mui/material';
import Checkbox from '@mui/material/Checkbox';
import { TextOut } from 'app/views/components/TextOut';
import { TypeNotificationProps } from 'app/views/modules/Profile/views/TypeNotification/types';
import style from './style.module.css';

export const TypeNotification = ({ checked, checkedChange, checkboxLabel, description, disabledDescription, isDisable = false }: TypeNotificationProps) => {
    return (
        <Box
            display="flex"
            gap="16px"
            alignItems="center"
        >
            {
                isDisable
                    ? (
                        <Tooltip title={ disabledDescription }>
                            <FormControlLabel
                                disabled={ isDisable }
                                control={ <Checkbox checked={ checked } onChange={ checkedChange } size="small" /> }
                                label={ <TextOut fontWeight={ 700 } className={ style.label }>{ checkboxLabel }</TextOut> }
                                sx={ { minWidth: { xs: '250px', sm: '220px', md: '190px', lg: '170px' } } }
                            />
                        </Tooltip>
                    )
                    : (
                        <FormControlLabel
                            disabled={ isDisable }
                            control={ <Checkbox checked={ checked } onChange={ checkedChange } size="small" /> }
                            label={ <TextOut fontWeight={ 700 } className={ style.label }>{ checkboxLabel }</TextOut> }
                            sx={ { minWidth: { xs: '250px', sm: '220px', md: '190px', lg: '170px' } } }
                        />
                    )
            }
            { description }
        </Box>
    );
};