import { putProfileRequestDto } from 'app/api/endpoints/profile/request';
import { ProfileItemResponseDto } from 'app/api/endpoints/profile/response';

export type Props = {
    isOpen: boolean;
    loginUser: string;
    info: ProfileItemResponseDto;
    onCancelClick: () => void;
    save: (info: putProfileRequestDto) => void;
}