import React from 'react';

export type TypeNotificationProps = {
    checkboxLabel: string;
    checked: boolean;
    checkedChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
    description: React.ReactNode;
    isDisable?: boolean;
    disabledDescription?: string;
};