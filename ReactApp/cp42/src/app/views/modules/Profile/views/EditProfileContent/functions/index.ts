import { AuthValueModal, ValidationValue } from 'app/views/modules/Profile/views/EditProfileContent/types';

export const initAuthValue = (): AuthValueModal => ({
    currentPassword: '',
    newPassword: '',
    repeatPassword: '',
    token: ''
});

export const initVvalidation = (): ValidationValue => ({
    firstName: '',
    fullName: '',
    lastName: '',
    phone: '',
    email: '',
    password: '',
    middleName: ''
});