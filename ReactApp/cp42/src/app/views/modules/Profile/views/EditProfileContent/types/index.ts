import { putProfileRequestDto } from 'app/api/endpoints/profile/request';
import { ProfileItemResponseDto } from 'app/api/endpoints/profile/response';

export type Props = {
    profileInfo: ProfileItemResponseDto;
    getInfo: (info: putProfileRequestDto) => void;
    getStatusSave: (status: boolean) => void;
};

export type AuthValueModal = {
    currentPassword: string;
    newPassword: string;
    repeatPassword: string;
    token: string;
};

export type ValidationValue = {
    fullName: string | null;
    firstName: string | null;
    lastName: string | null;
    phone: string | null;
    email: string | null;
    password: string | null;
    middleName: string | null;
};

export type PasswordValidation = {
    text: string;
    isValid: boolean;
};