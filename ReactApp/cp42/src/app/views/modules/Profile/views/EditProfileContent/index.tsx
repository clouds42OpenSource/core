import { Visibility, VisibilityOff } from '@mui/icons-material';
import { Grid, IconButton, InputAdornment, OutlinedInput, TextField } from '@mui/material';
import { ProfileItemResponseDto } from 'app/api/endpoints/profile/response';
import { localStorageHelper } from 'app/common/helpers/LocalStorageHelper';
import { passwordValidator } from 'app/common/validators/passwordValidator';
import { ValidateErrorEnum } from 'app/views/modules/Profile/common/ValidateErrorEnum';
import { initAuthValue, initVvalidation } from 'app/views/modules/Profile/views/EditProfileContent/functions';
import { AuthValueModal, PasswordValidation, Props, ValidationValue } from 'app/views/modules/Profile/views/EditProfileContent/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { RequestKind } from 'core/requestSender/enums';
import React, { memo, useCallback, useEffect, useState } from 'react';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';

const stylePassword = {
    '& input': {
        textSecurity: 'disc',
        MozTextSecurity: 'disc',
        WebkitTextSecurity: 'disc'
    },
};

export const EditProfileContent = memo(({ profileInfo, getInfo, getStatusSave }: Props) => {
    const [showNewPassword, setShowNewPassword] = useState(false);
    const [value, setValue] = useState<ProfileItemResponseDto>(profileInfo);
    const [authValue, setAuthValue] = useState<AuthValueModal>(initAuthValue);
    const [validation, setValidation] = useState<ValidationValue>(initVvalidation);
    const [passwordValidation, setPasswordValidation] = useState<PasswordValidation[]>([]);

    const handleClickShowNewPassword = useCallback(() => setShowNewPassword(show => !show), []);
    const handleMouseDownNewPassword = useCallback((event: React.MouseEvent<HTMLButtonElement>) => event.preventDefault(), []);

    const validatePassword = useCallback((password?: string) => {
        if (password) {
            const data = [
                {
                    text: '',
                    isValid: passwordValidator(password)
                },
                {
                    text: 'пароль и его подтверждение совпадают',
                    isValid: password === authValue.newPassword,
                }
            ];
            setPasswordValidation([...data]);
        } else {
            setPasswordValidation([]);
        }

        const isPasswordValid = (authValue.newPassword)
            ? !passwordValidation.map(x => x.isValid).includes(false)
            : true;
        const isFieldsValid = Object.values(validation).every(x => x === '');

        return isPasswordValid && isFieldsValid;
    }, [authValue, passwordValidation, validation]);

    const validateAsyncForm = useCallback(async (label: string, newValue: string) => {
        const api = InterlayerApiProxy.getUserManagementApiProxy();
        const userId = localStorageHelper.getUserId();

        if (label === 'middleName' || label === 'firstName' || label === 'lastName') {
            if (!/^[а-яА-ЯІіЇїЄєҐґёЁa-zA-Z]+$/.test(newValue) && newValue !== '') {
                setValidation({ ...validation, [label]: 'Неверный формат данных' });
            } else {
                setValidation({ ...validation, [label]: '' });
            }
        }

        if (label === 'phoneNumber') {
            if (newValue === '') {
                setValidation({ ...validation, phone: '' });
            }

            try {
                const phone = await api.IsPhoneAvailable(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, { PhoneNumber: newValue, userId: userId ?? '' });
                setValidation({ ...validation, phone: phone.message ? phone.message : '' });
            } catch (error) {
                setValidation({ ...validation, phone: ValidateErrorEnum.phoneError });
            }
        }

        if (label === 'email' && newValue !== '') {
            try {
                const email = await api.IsEmailAvailable(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, { Email: newValue, userId: userId ?? '' });
                setValidation({ ...validation, email: email.message ? email.message : '' });
            } catch (error) {
                setValidation({ ...validation, email: ValidateErrorEnum.emailError });
            }
        }

        if (label === 'repeatPassword' || label === 'newPassword') {
            validatePassword(newValue);
        }
    }, [validation, validatePassword]);

    const onValueChange = useCallback(async (event: React.ChangeEvent<HTMLInputElement>) => {
        const label = event.target.id;
        const newValue = event.target.value;
        void validateAsyncForm(label, newValue);

        if (value.hasOwnProperty(label)) {
            setValue({ ...value, [label]: newValue });
        } else {
            setAuthValue({ ...authValue, [label]: newValue });
        }
    }, [value, authValue, validateAsyncForm]);

    useEffect(() => {
        getInfo({
            accountId: localStorageHelper.getAccountId() ?? '',
            id: localStorageHelper.getUserId() ?? '',
            isActivated: value.activated,
            login: value.login.trim(),
            roles: value.accountRoles,
            lastName: value.lastName,
            firstName: value.firstName,
            midName: value.middleName ?? '',
            email: (value.email ?? '').trim(),
            password: authValue.newPassword,
            confirmPassword: authValue.repeatPassword,
            phoneNumber: (value.phoneNumber ?? '').trim()
        });
        getStatusSave(
            !validation.firstName && !validation.middleName && !validation.lastName &&
            !validation.phone &&
            !validation.password &&
            (passwordValidation.filter(item => item.isValid).length === 2 || !passwordValidation.length)
        );
    }, [value, authValue, validation, getInfo, getStatusSave, passwordValidation]);

    return (
        <Grid container={ true } spacing={ { xs: 0, md: 2 } }>
            <Grid item={ true } xs={ 12 } md={ 6 }>
                <FormAndLabel label="Логин">
                    <TextField
                        fullWidth={ true }
                        id="login"
                        value={ value.login.trim() }
                        disabled={ true }
                        helperText="Смена логина невозможна. Рекомендуем удалить пользователя и создать нового"
                    />
                </FormAndLabel>
                <FormAndLabel label="Телефон">
                    <TextField
                        id="phoneNumber"
                        value={ (value.phoneNumber ?? '').trim() }
                        onChange={ onValueChange }
                        helperText={ validation.phone }
                        error={ !!validation.phone }
                        fullWidth={ true }
                    />
                </FormAndLabel>
                <FormAndLabel label="Электронная почта">
                    <TextField
                        id="email"
                        value={ (value.email ?? '').trim() }
                        onChange={ onValueChange }
                        helperText={ validation.email }
                        error={ !!validation.email }
                        fullWidth={ true }
                    />
                </FormAndLabel>
                <FormAndLabel label="Фамилия">
                    <TextField
                        id="lastName"
                        value={ value.lastName ?? '' }
                        onChange={ onValueChange }
                        helperText={ validation.lastName }
                        error={ !!validation.lastName }
                        fullWidth={ true }
                    />
                </FormAndLabel>
                <FormAndLabel label="Имя">
                    <TextField
                        id="firstName"
                        value={ value.firstName ?? '' }
                        onChange={ onValueChange }
                        helperText={ validation.firstName }
                        error={ !!validation.firstName }
                        fullWidth={ true }
                    />
                </FormAndLabel>
                <FormAndLabel label="Отчество">
                    <TextField
                        id="middleName"
                        value={ value.middleName ?? '' }
                        onChange={ onValueChange }
                        helperText={ validation.middleName }
                        error={ !!validation.middleName }
                        fullWidth={ true }
                    />
                </FormAndLabel>
            </Grid>
            <Grid item={ true } xs={ 12 } md={ 6 }>
                <FormAndLabel label="Новый пароль">
                    <OutlinedInput
                        fullWidth={ true }
                        id="newPassword"
                        value={ authValue.newPassword }
                        onChange={ onValueChange }
                        autoComplete="off"
                        error={ authValue.newPassword !== '' && !passwordValidation.find(item => item.text === '')?.isValid }
                        endAdornment={
                            <InputAdornment position="end">
                                <IconButton
                                    aria-label="toggle password visibility"
                                    onClick={ handleClickShowNewPassword }
                                    onMouseDown={ handleMouseDownNewPassword }
                                    edge="end"
                                >
                                    { showNewPassword ? <VisibilityOff /> : <Visibility /> }
                                </IconButton>
                            </InputAdornment>
                        }
                        sx={ !showNewPassword ? stylePassword : undefined }
                    />
                </FormAndLabel>
                <FormAndLabel label="Подтверждение пароля">
                    <OutlinedInput
                        fullWidth={ true }
                        id="repeatPassword"
                        value={ authValue.repeatPassword }
                        onChange={ onValueChange }
                        autoComplete="off"
                        error={ authValue.repeatPassword !== '' && !passwordValidation.find(item => item.text === 'пароль и его подтверждение совпадают')?.isValid }
                        endAdornment={
                            <InputAdornment position="end">
                                <IconButton
                                    aria-label="toggle password visibility"
                                    onClick={ handleClickShowNewPassword }
                                    onMouseDown={ handleMouseDownNewPassword }
                                    edge="end"
                                >
                                    { showNewPassword ? <VisibilityOff /> : <Visibility /> }
                                </IconButton>
                            </InputAdornment>
                        }
                        sx={ !showNewPassword ? stylePassword : undefined }
                    />
                </FormAndLabel>
            </Grid>
        </Grid>
    );
});