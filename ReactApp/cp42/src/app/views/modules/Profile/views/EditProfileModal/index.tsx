import { putProfileRequestDto } from 'app/api/endpoints/profile/request';
import { EMessageType, useFloatMessages } from 'app/hooks';
import { Dialog } from 'app/views/components/controls/Dialog';
import { EditProfileContent } from 'app/views/modules/Profile/views/EditProfileContent';
import { memo, useCallback, useState } from 'react';
import { Props } from './types';

export const EditProfileModal = memo(({ isOpen, loginUser, onCancelClick, save, info }: Props) => {
    const [saveInfo, setSaveInfo] = useState<putProfileRequestDto>();
    const [saveStatus, setSaveStatus] = useState(false);

    const { show } = useFloatMessages();

    const getInfo = useCallback((information: putProfileRequestDto) => setSaveInfo(information), []);
    const getStatusSave = useCallback((status: boolean) => setSaveStatus(status), []);

    return (
        <Dialog
            isOpen={ isOpen }
            dialogWidth="md"
            isTitleSmall={ true }
            dialogVerticalAlign="center"
            title={ `Карточка пользователя: ${ loginUser }` }
            onCancelClick={ onCancelClick }
            buttons={ [
                {
                    content: 'Сохранить',
                    kind: 'success',
                    onClick: () => {
                        save(saveInfo!);
                        if (saveInfo!.email !== '' && saveInfo!.email !== info.email) {
                            show(EMessageType.info, 'Смена почты произойдет после верификации новой почты. Проверьте письмо на почте.');
                        }
                    },
                    isEnabled: saveStatus
                }
            ] }
        >
            <EditProfileContent profileInfo={ info } getInfo={ getInfo } getStatusSave={ getStatusSave } />
        </Dialog>
    );
});