import { withHeader } from 'app/views/components/_hoc/withHeader';
import { TabsControl } from 'app/views/components/controls/TabsControl';
import { ProfileTab, NotificationTab } from 'app/views/modules/Profile/tabs';

const headerTabs = [
    {
        title: 'Профиль',
        isVisible: true,
    },
    {
        title: 'Настройка уведомлений',
        isVisible: true,
    }
];

const Profile = () => {
    return (
        <TabsControl headers={ headerTabs }>
            <ProfileTab />
            <NotificationTab />
        </TabsControl>
    );
};

export const ProfileView = withHeader({
    title: 'Профиль',
    page: Profile
});

export * from './views/EditProfileModal';