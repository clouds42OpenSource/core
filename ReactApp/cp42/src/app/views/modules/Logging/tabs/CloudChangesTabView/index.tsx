import { SortingKind } from 'app/common/enums';
import { hasComponentChangesFor } from 'app/common/functions';
import { ProcessIdStateHelper } from 'app/common/helpers/ProcessIdStateHelper';
import { LoggingProcessId } from 'app/modules/logging';
import { ReceiveCloudChangesThunkParams } from 'app/modules/logging/store/reducers/receiveCloudChangesReducer/params';
import { ReceiveCloudChangesThunk } from 'app/modules/logging/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { DateUtility } from 'app/utils';
import { TextButton } from 'app/views/components/controls/Button';
import { TextOut } from 'app/views/components/TextOut';
import { CloudChangesFilterDataForm } from 'app/views/modules/Logging/tabs/CloudChangesTabView/types';
import {
    CloudChangesFilterFormView
} from 'app/views/modules/Logging/tabs/CloudChangesTabView/views/CloudChangesFilterFormView';
import { SortingDataModel } from 'app/web/common/data-models/SortingDataModel';

import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { TablePagination } from 'app/views/modules/_common/components/CommonTableWithFilter/TableView';
import { AccountCard } from 'app/views/modules/_common/reusable-modules/AccountCard';
import { SelectDataCommonDataModel } from 'app/web/common/data-models';
import { CloudChangesItemDataModel } from 'app/web/InterlayerApiProxy/LoggingApiProxy/receiveCloudChanges';
import React from 'react';
import { connect } from 'react-redux';

type StateProps<TDataRow = CloudChangesItemDataModel> = {
    dataset: TDataRow[];
    isInLoadingDataProcess: boolean;
    metadata: TablePagination;
};

type DispatchProps = {
    dispatchReceiveCloudChangesThunk: (args: ReceiveCloudChangesThunkParams) => void;
};

type AllProps = StateProps & DispatchProps;

type OwnState = {
    isAccountCardVisible: boolean;
    accountNumber: number;
};

class CloudChangesTabViewClass extends React.Component<AllProps, OwnState> {
    public constructor(props: AllProps) {
        super(props);

        this.onDataSelect = this.onDataSelect.bind(this);
        this.showAccountCard = this.showAccountCard.bind(this);
        this.hideAccountCard = this.hideAccountCard.bind(this);

        this.state = {
            isAccountCardVisible: false,
            accountNumber: 0
        };
    }

    public shouldComponentUpdate(nextProps: AllProps, nextState: OwnState) {
        return hasComponentChangesFor(this.props, nextProps) ||
            hasComponentChangesFor(this.state, nextState);
    }

    private onDataSelect(args: SelectDataCommonDataModel<CloudChangesFilterDataForm>) {
        this.props.dispatchReceiveCloudChangesThunk({
            pageNumber: args.pageNumber,
            filter: args.filter,
            orderBy: args.sortingData ? this.getSortQuery(args.sortingData) : 'date.desc',
            force: true
        });
    }

    private getSortQuery(args: SortingDataModel) {
        switch (args.fieldName) {
            case 'accountNumber':
                return `account.indexNumber.${ args.sortKind ? 'desc' : 'asc' }`;
            case 'createCloudChangeDate':
                return `date.${ args.sortKind ? 'desc' : 'asc' }`;
            case 'cloudChangesActionName':
                return `cloudChangesAction.name.${ args.sortKind ? 'desc' : 'asc' }`;
            case 'initiatorLogin':
                return `accountUser.login.${ args.sortKind ? 'desc' : 'asc' }`;
            case 'cloudChangesActionDescription':
                return `description.${ args.sortKind ? 'desc' : 'asc' }`;
            default:
                return 'date.desc';
        }
    }

    private showAccountCard(accountNumber: number) {
        return () => {
            this.setState({
                isAccountCardVisible: true,
                accountNumber
            });
        };
    }

    private hideAccountCard() {
        this.setState({
            isAccountCardVisible: false,
            accountNumber: 0
        });
    }

    public render() {
        return (
            <>
                <CommonTableWithFilter
                    uniqueContextProviderStateId="CloudChangesContextProviderStateId"
                    filterProps={ {
                        getFilterContentView: (ref, onFilterChanged) => <CloudChangesFilterFormView ref={ ref } isFilterFormDisabled={ this.props.isInLoadingDataProcess } onFilterChanged={ onFilterChanged } />
                    } }
                    isResponsiveTable={ false }
                    tableProps={ {
                        dataset: this.props.dataset,
                        keyFieldName: 'cloudChangeId',
                        sorting: {
                            sortFieldName: 'createCloudChangeDate',
                            sortKind: SortingKind.Desc
                        },
                        fieldsView: {
                            accountNumber: {
                                caption: 'Аккаунт',
                                isSortable: true,
                                fieldContentNoWrap: false,
                                fieldWidth: '5%',
                                format: (value: number) => {
                                    return (
                                        <TextButton onClick={ this.showAccountCard(value) } canSelectButtonContent={ true }>
                                            <TextOut fontSize={ 13 } className="text-link">
                                                { value }
                                            </TextOut>
                                        </TextButton>
                                    );
                                }
                            },
                            createCloudChangeDate: {
                                caption: 'Дата и время',
                                isSortable: true,
                                fieldContentNoWrap: false,
                                fieldWidth: '10%',
                                format: (value: Date) => {
                                    return DateUtility.dateToDayMonthYearHourMinute(value);
                                }
                            },
                            cloudChangesActionName: {
                                caption: 'Действие',
                                fieldContentNoWrap: false,
                                isSortable: true,
                                fieldWidth: '20%'
                            },
                            initiatorLogin: {
                                caption: 'Инициатор',
                                fieldContentNoWrap: true,
                                isSortable: true,
                            },
                            cloudChangesActionDescription: {
                                caption: 'Описание действия',
                                fieldContentNoWrap: false,
                                fieldContentWrapEverySymbol: true,
                                isSortable: true,
                                fieldWidth: '60%'
                            }
                        },
                        pagination: this.props.metadata
                    } }
                    onDataSelect={ this.onDataSelect }
                />
                <AccountCard
                    accountNumber={ this.state.accountNumber }
                    isOpen={ this.state.isAccountCardVisible }
                    onCancelDialogButtonClick={ this.hideAccountCard }
                />
            </>
        );
    }
}

export const CloudChangesTabView = connect<StateProps, DispatchProps, {}, AppReduxStoreState>(
    state => {
        const loggingState = state.LoggingState;
        const { cloudChanges } = loggingState;
        const dataset = cloudChanges.items;

        const currentPage = cloudChanges.metadata.pageNumber;
        const totalPages = cloudChanges.metadata.pageCount;
        const { pageSize } = cloudChanges.metadata;
        const recordsCount = cloudChanges.metadata.totalItemCount;

        const isInLoadingDataProcess = ProcessIdStateHelper.isInProgress(loggingState.reducerActions, LoggingProcessId.ReceiveCloudChanges);

        return {
            dataset,
            isInLoadingDataProcess,
            metadata: {
                currentPage,
                totalPages,
                pageSize,
                recordsCount
            }
        };
    },
    {
        dispatchReceiveCloudChangesThunk: ReceiveCloudChangesThunk.invoke
    }
)(CloudChangesTabViewClass);