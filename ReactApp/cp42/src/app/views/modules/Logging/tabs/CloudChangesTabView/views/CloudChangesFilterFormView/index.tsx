import { Grid } from '@mui/material';
import { hasComponentChangesFor } from 'app/common/functions';
import { getEmptyUuid } from 'app/common/helpers';
import { ReceiveCloudChangesActionsDataThunk } from 'app/modules/logging/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { DateUtility } from 'app/utils';
import { TableFilterWrapper } from 'app/views/components/_hoc/tableFilterWrapper';
import { FilterItemWrapper } from 'app/views/components/_hoc/tableFilterWrapper/filterItemWrapper';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { CheckBoxForm, ComboBoxForm, TextBoxForm } from 'app/views/components/controls/forms';
import { ComboboxItemModel } from 'app/views/components/controls/forms/ComboBox/models';
import { DoubleDateInputForm } from 'app/views/components/controls/forms/DoubleDateInputView';
import {
    CloudChangesFilterDataForm,
    CloudChangesFilterDataFormFieldNamesType
} from 'app/views/modules/Logging/tabs/CloudChangesTabView/types';
import { CloudChangesActionDataModel } from 'app/web/InterlayerApiProxy/LoggingApiProxy/receiveCloudChangesActionsData';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';
import React from 'react';
import { connect } from 'react-redux';

/**
 * Свойства для компонента фильтра
 */
type OwnProps = ReduxFormProps<CloudChangesFilterDataForm> & {
    onFilterChanged: (filter: CloudChangesFilterDataForm, filterFormName: string) => void;
    isFilterFormDisabled: boolean;
};

type StateProps = {
    cloudChangesActions: Array<CloudChangesActionDataModel>;
    isDifferentContext: boolean;
};

type DispatchProps = {
    dispatchReceiveCloudChangesActionsDataThunk: (args?: IForceThunkParam) => void;
};

type AllProps = OwnProps & StateProps & DispatchProps;

class CloudChangesFilterFormViewClass extends React.Component<AllProps> {
    public constructor(props: AllProps) {
        super(props);
        this.initReduxForm = this.initReduxForm.bind(this);
        this.onValueChange = this.onValueChange.bind(this);
        this.onValueApplied = this.onValueApplied.bind(this);
        this.getCloudChangesActions = this.getCloudChangesActions.bind(this);
        this.renderCheckBoxes = this.renderCheckBoxes.bind(this);
    }

    public componentDidMount() {
        this.props.reduxForm.setInitializeFormDataAction(this.initReduxForm);
        this.props.dispatchReceiveCloudChangesActionsDataThunk({ showLoadingProgress: false });
    }

    public shouldComponentUpdate(nextProps: AllProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private onValueChange<TValue, TForm extends string = CloudChangesFilterDataFormFieldNamesType>(formName: TForm, newValue: TValue) {
        this.props.reduxForm.updateReduxFormFields({
            [formName]: newValue
        });

        if (formName === 'searchLine' && newValue && (newValue as unknown as string).length < 3) {
            return false;
        }
    }

    private onValueApplied<TValue>(formName: string, value: TValue) {
        this.props.onFilterChanged({
            ...this.props.reduxForm.getReduxFormFields(false),
            [formName]: value
        }, formName);
    }

    /**
     * Получить список опций выпадающего списка для действий логирования
     */
    private getCloudChangesActions(): ComboboxItemModel<string>[] {
        return this.props.cloudChangesActions.map(action => {
            return {
                value: action.id,
                text: action.name,
                group: action.group
            };
        });
    }

    private initReduxForm(): CloudChangesFilterDataForm | undefined {
        return {
            createCloudChangeDateFrom: DateUtility.getSomeMonthAgoDate(3),
            createCloudChangeDateTo: DateUtility.setDayEndTime(DateUtility.getTomorrow()),
            searchLine: '',
            showMyAccountsOnly: false,
            showVipAccountsOnly: false,
            cloudChangesActionId: getEmptyUuid()
        };
    }

    private renderCheckBoxes(formFields: CloudChangesFilterDataForm) {
        if (this.props.isDifferentContext) return null;

        return (
            <Grid container={ true } spacing={ 2 } mt={ 2 }>
                <Grid item={ true } xs={ 12 } sm={ 12 } md={ 4 } lg={ 4 } xl={ 4 }>
                    <CheckBoxForm
                        label=""
                        captionFontSize={ 13 }
                        captionFontWeight={ 600 }
                        captionAsLabel={ true }
                        falseText="Показать мои аккаунты"
                        trueText="Показать мои аккаунты"
                        formName="showMyAccountsOnly"
                        onValueChange={ this.onValueChange }
                        onValueApplied={ this.onValueApplied }
                        isChecked={ formFields.showMyAccountsOnly }
                        isReadOnly={ this.props.isFilterFormDisabled }
                    />
                </Grid>
                <Grid item={ true } xs={ 12 } sm={ 12 } md={ 4 } lg={ 4 } xl={ 4 }>
                    <CheckBoxForm
                        label=""
                        captionFontSize={ 13 }
                        captionFontWeight={ 600 }
                        captionAsLabel={ true }
                        falseText="Показать VIP аккаунты"
                        trueText="Показать VIP аккаунты"
                        formName="showVipAccountsOnly"
                        onValueChange={ this.onValueChange }
                        onValueApplied={ this.onValueApplied }
                        isChecked={ formFields.showVipAccountsOnly }
                        isReadOnly={ this.props.isFilterFormDisabled }
                    />
                </Grid>
            </Grid>
        );
    }

    public render() {
        if (!this.props.cloudChangesActions.length) {
            return null;
        }

        const formFields = this.props.reduxForm.getReduxFormFields(false);
        return (
            <>
                <TableFilterWrapper>
                    <FilterItemWrapper width="370px">
                        <ComboBoxForm
                            allowAutoComplete={ true }
                            label="Действие"
                            formName="cloudChangesActionId"
                            value={ formFields.cloudChangesActionId }
                            onValueChange={ this.onValueChange }
                            onValueApplied={ this.onValueApplied }
                            autoFocus={ true }
                            items={ this.getCloudChangesActions() }
                            isReadOnly={ this.props.isFilterFormDisabled }
                        />
                    </FilterItemWrapper>
                    <FilterItemWrapper width="370px">
                        <TextBoxForm
                            label="Поиск по таблице"
                            formName="searchLine"
                            onValueChange={ this.onValueChange }
                            onValueApplied={ this.onValueApplied }
                            value={ formFields.searchLine ?? '' }
                            maxLength={ 50 }
                            isReadOnly={ this.props.isFilterFormDisabled }
                        />
                    </FilterItemWrapper>
                    <DoubleDateInputForm
                        label="Период:"
                        onValueChange={ this.onValueChange }
                        onValueApplied={ this.onValueApplied }
                        periodFromInputName="createCloudChangeDateFrom"
                        periodToInputName="createCloudChangeDateTo"
                        periodFromValue={ formFields.createCloudChangeDateFrom }
                        periodToValue={ formFields.createCloudChangeDateTo }
                        isReadOnly={ this.props.isFilterFormDisabled }
                    />

                </TableFilterWrapper>
                { this.renderCheckBoxes(formFields) }
            </>
        );
    }
}

const CloudChangesFilterFormViewConnected = connect<StateProps, DispatchProps, OwnProps, AppReduxStoreState>(
    state => {
        const cloudChangesActions = state.LoggingState.cloudChangesActions.items;

        const currentSessionSettingsState = state.Global.getCurrentSessionSettingsReducer;
        const hasSessionSettingsReceived = currentSessionSettingsState.hasSuccessFor.hasSettingsReceived;
        const isDifferentContext = hasSessionSettingsReceived && currentSessionSettingsState.settings.currentContextInfo.isDifferentContextAccount;

        return {
            cloudChangesActions,
            isDifferentContext
        };
    },
    {
        dispatchReceiveCloudChangesActionsDataThunk: ReceiveCloudChangesActionsDataThunk.invoke
    }
)(CloudChangesFilterFormViewClass);

export const CloudChangesFilterFormView = withReduxForm(CloudChangesFilterFormViewConnected, {
    reduxFormName: 'CloudChanges_Filter_Form',
    resetOnUnmount: true
});