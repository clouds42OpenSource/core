/**
 * Модель Redux формы для редактирования фильтра для таблицы логов запуска баз и RDP
 */
export type LaunchDbAndRdpLogFilterDataForm = {
    /**
     * Номер аккаунта
     */
    accountNumber: string;
    /**
     * Логин
     */
    login: string;

    /**
     * Номер информационной базы
     */
    v82Name: string;

    /**
     * Версия Линка
     */
    linkAppVersion: string;

    /**
     * Внешний IP адрес
     */
    externalIpAddress: string;

    /**
     * Внутренний IP адрес
     */
    internalIpAddress: string;
};