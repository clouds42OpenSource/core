import { Nullable } from 'app/common/types';

/**
 * Модель Redux формы для редактирования фильтра для таблицы логов действий в облаке
 */
export type CloudChangesFilterDataForm = {
    /**
     * ID действия облака
     */
    cloudChangesActionId: string,

    /**
     * Строка поиска
     */
    searchLine: Nullable<string>,

    /**
     * Дата создания действия с
     */
    createCloudChangeDateFrom: Nullable<Date>,

    /**
     * Дата создания действия по
     */
    createCloudChangeDateTo: Nullable<Date>,

    /**
     * Показывать только мои аккаунты
     */
    showMyAccountsOnly: boolean,

    /**
     * Показывать только вип аккаунты
     */
    showVipAccountsOnly: boolean
};