import { LaunchDbAndRdpLogFilterDataForm } from 'app/views/modules/Logging/tabs/LaunchDbAndRdpLogTabView/types';

export type LaunchDbAndRdpLogFilterDataFormFieldNamesType = keyof LaunchDbAndRdpLogFilterDataForm;