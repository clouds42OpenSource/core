import { CloudChangesFilterDataForm } from 'app/views/modules/Logging/tabs/CloudChangesTabView/types/CloudChangesFilterDataForm';

export type CloudChangesFilterDataFormFieldNamesType = keyof CloudChangesFilterDataForm;