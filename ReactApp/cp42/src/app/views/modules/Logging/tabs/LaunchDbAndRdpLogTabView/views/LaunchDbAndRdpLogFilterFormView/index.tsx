import { LaunchDbAndRdpLogFilterDataForm, LaunchDbAndRdpLogFilterDataFormFieldNamesType } from 'app/views/modules/Logging/tabs/LaunchDbAndRdpLogTabView/types';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { FilterItemWrapper } from 'app/views/components/_hoc/tableFilterWrapper/filterItemWrapper';
import React from 'react';
import { SearchByFilterButton } from 'app/views/components/controls/Button';
import { TableFilterWrapper } from 'app/views/components/_hoc/tableFilterWrapper';
import { TextBoxForm } from 'app/views/components/controls/forms/TextBoxForm';
import { createInt32Validator } from 'app/common/validators';
import { hasComponentChangesFor } from 'app/common/functions';

/**
 * Свойства для компонента фильтра
 */
type OwnProps = ReduxFormProps<LaunchDbAndRdpLogFilterDataForm> & {
    /**
     * Call back функция на изменение фильтра
     */
    onFilterChanged: (filter: LaunchDbAndRdpLogFilterDataForm, filterFormName: string) => void;

    /**
     * Поле для блокировки полей фильтра
     */
    isFilterFormDisabled: boolean;

    /**
     * Поле обозначающий что вход был сделан через чужой аккаунт
     */
    isDifferentContext: boolean;
};

class LaunchDbAndRdpLogFilterFormViewClass extends React.Component<OwnProps> {
    public constructor(props: OwnProps) {
        super(props);
        this.initReduxForm = this.initReduxForm.bind(this);
        this.onValueChange = this.onValueChange.bind(this);
        this.applyFilter = this.applyFilter.bind(this);
        this.getFilterInputsFields = this.getFilterInputsFields.bind(this);
        this.handleEnterKey = this.handleEnterKey.bind(this);
    }

    public componentDidMount() {
        this.props.reduxForm.setInitializeFormDataAction(this.initReduxForm);
        document.body.addEventListener('keydown', this.handleEnterKey);
    }

    public shouldComponentUpdate(nextProps: OwnProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    public componentWillUnmount() {
        document.body.removeEventListener('keydown', this.handleEnterKey);
    }

    private handleEnterKey(event: KeyboardEvent) {
        if (event.key !== 'Enter') return;

        event.preventDefault();
        this.applyFilter();
    }

    private onValueChange<TValue, TForm extends string = LaunchDbAndRdpLogFilterDataFormFieldNamesType>(formName: TForm, newValue: TValue) {
        this.props.reduxForm.updateReduxFormFields({
            [formName]: newValue
        });

        if (formName === 'accountNumber' && newValue && (newValue as unknown as number).toString().length < 3) {
            return false;
        }
    }

    private getFilterInputsFields(formFields: LaunchDbAndRdpLogFilterDataForm) {
        return (
            <TextBoxForm
                label="Номер аккаунта"
                formName="accountNumber"
                onValueChange={ this.onValueChange }
                onValueApplied={ () => void 0 }
                value={ formFields.accountNumber }
                maxLength={ 9 }
                validator={ createInt32Validator() }
                isReadOnly={ this.props.isFilterFormDisabled }
            />
        );
    }

    private initReduxForm(): LaunchDbAndRdpLogFilterDataForm | undefined {
        return {
            accountNumber: '',
            linkAppVersion: '',
            login: '',
            v82Name: '',
            externalIpAddress: '',
            internalIpAddress: ''
        };
    }

    private applyFilter() {
        this.props.onFilterChanged({
            ...this.props.reduxForm.getReduxFormFields(false)
        }, '');
    }

    public render() {
        const formFields = this.props.reduxForm.getReduxFormFields(false);

        return (
            <TableFilterWrapper>
                {
                    this.props.isDifferentContext
                        ? null
                        : (
                            <FilterItemWrapper width="370px">
                                {
                                    this.getFilterInputsFields(formFields)
                                }
                            </FilterItemWrapper>
                        )
                }
                <FilterItemWrapper width="370px">
                    <TextBoxForm
                        label="Логин"
                        formName="login"
                        onValueChange={ this.onValueChange }
                        onValueApplied={ () => void 0 }
                        value={ formFields.login }
                        maxLength={ 50 }
                        isReadOnly={ this.props.isFilterFormDisabled }
                    />
                </FilterItemWrapper>
                <FilterItemWrapper width="370px">
                    <TextBoxForm
                        label="Номер базы"
                        formName="v82Name"
                        onValueChange={ this.onValueChange }
                        onValueApplied={ () => void 0 }
                        value={ formFields.v82Name }
                        maxLength={ 20 }
                        isReadOnly={ this.props.isFilterFormDisabled }
                    />
                </FilterItemWrapper>
                <FilterItemWrapper width="370px">
                    <TextBoxForm
                        label="Версия Линк42"
                        formName="linkAppVersion"
                        onValueChange={ this.onValueChange }
                        onValueApplied={ () => void 0 }
                        value={ formFields.linkAppVersion }
                        maxLength={ 20 }
                        isReadOnly={ this.props.isFilterFormDisabled }
                    />
                </FilterItemWrapper>
                <FilterItemWrapper width="370px">
                    <TextBoxForm
                        label="Внешний IP адрес"
                        formName="externalIpAddress"
                        onValueChange={ this.onValueChange }
                        onValueApplied={ () => void 0 }
                        value={ formFields.externalIpAddress }
                        maxLength={ 20 }
                        isReadOnly={ this.props.isFilterFormDisabled }
                    />
                </FilterItemWrapper>
                <FilterItemWrapper width="370px">
                    <TextBoxForm
                        label="Внутренний IP адрес"
                        formName="internalIpAddress"
                        onValueChange={ this.onValueChange }
                        onValueApplied={ () => void 0 }
                        value={ formFields.internalIpAddress }
                        maxLength={ 20 }
                        isReadOnly={ this.props.isFilterFormDisabled }
                    />
                </FilterItemWrapper>
                <FilterItemWrapper width="370px">
                    <SearchByFilterButton onClick={ this.applyFilter } />
                </FilterItemWrapper>
            </TableFilterWrapper>
        );
    }
}

export const LaunchDbAndRdpLogFilterFormView = withReduxForm(LaunchDbAndRdpLogFilterFormViewClass, {
    reduxFormName: 'LaunchDbAndRdpLog_Filter_Form',
    resetOnUnmount: true
});