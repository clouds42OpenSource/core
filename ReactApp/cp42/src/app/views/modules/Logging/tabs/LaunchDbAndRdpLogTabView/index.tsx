import { DbLaunchTypeStrings, getLinkAppTypeString } from 'app/common/constants';
import { DbLaunchType, DbRdpActionType, LinkAppType, SortingKind } from 'app/common/enums';
import { hasComponentChangesFor } from 'app/common/functions';
import { ProcessIdStateHelper } from 'app/common/helpers/ProcessIdStateHelper';
import { Nullable } from 'app/common/types';
import { useDatabaseCard } from 'app/hooks';
import { LoggingProcessId } from 'app/modules/logging';
import { ReceiveLaunchDbAndRdpLogThunkParams } from 'app/modules/logging/store/reducers/receiveLaunchDbAndRdpReducer/params';
import { ReceiveLaunchDbAndRdpLogThunk } from 'app/modules/logging/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { DateUtility } from 'app/utils';
import { TextButton } from 'app/views/components/controls/Button';
import { TextOut } from 'app/views/components/TextOut';
import { displayValueOrDefault } from 'app/views/components/utils/TextDisplayHelper';
import { InformationDatabaseCard } from 'app/views/modules/_common/reusable-modules/InformationDatabaseCard';
import { LaunchDbAndRdpLogFilterDataForm } from 'app/views/modules/Logging/tabs/LaunchDbAndRdpLogTabView/types';
import { LaunchDbAndRdpLogFilterFormView } from 'app/views/modules/Logging/tabs/LaunchDbAndRdpLogTabView/views/LaunchDbAndRdpLogFilterFormView';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { TablePagination } from 'app/views/modules/_common/components/CommonTableWithFilter/TableView';
import { AccountCard } from 'app/views/modules/_common/reusable-modules/AccountCard';
import { SelectDataCommonDataModel } from 'app/web/common/data-models';
import { SortingDataModel } from 'app/web/common/data-models/SortingDataModel';
import { LaunchDbAndRdpLogItemDataModel } from 'app/web/InterlayerApiProxy/LoggingApiProxy/receiveLaunchDbAndRdpLog';
import React, { ReactNode } from 'react';
import { connect } from 'react-redux';

type OwnProps = {
    canLoadLog: boolean;
};

type StateProps<TDataRow = LaunchDbAndRdpLogItemDataModel> = {
    dataset: TDataRow[];
    hasDbLaunchAndRdpLogReceived: boolean;
    isInLoadingDataProcess: boolean;
    metadata: TablePagination;
    isDifferentContext: boolean;
    differentContextAccountNumber: string;
};

type DispatchProps = {
    dispatchReceiveLaunchDbAndRdpLogThunk: (args: ReceiveLaunchDbAndRdpLogThunkParams) => void;
};

type TProps = {
    openDatabase: (databaseNumber: string) => void;
    databaseCard: ReactNode;
};

type AllProps = OwnProps & StateProps & DispatchProps & TProps;

type OwnState = {
    isDatabaseCardVisible: boolean;
    isAccountCardVisible: boolean;
    accountNumber: number;
    databaseName: string;
};

class LaunchDbAndRdpLogTabViewClass extends React.Component<AllProps, OwnState> {
    private static _noValueStr = '--';

    private _commonTableWithFilterViewRef = React.createRef<CommonTableWithFilter<any, any>>();

    public constructor(props: AllProps) {
        super(props);

        this.onDataSelect = this.onDataSelect.bind(this);
        this.showAccountCard = this.showAccountCard.bind(this);
        this.hideAccountCard = this.hideAccountCard.bind(this);
        this.changeFilterForDeferentContext = this.changeFilterForDeferentContext.bind(this);

        this.state = {
            isDatabaseCardVisible: false,
            isAccountCardVisible: false,
            databaseName: '',
            accountNumber: 0
        };
    }

    public shouldComponentUpdate(nextProps: AllProps, nextState: OwnState) {
        return hasComponentChangesFor(this.props, nextProps) ||
            hasComponentChangesFor(this.state, nextState);
    }

    public componentDidUpdate(prevProps: AllProps) {
        if (prevProps.canLoadLog !== this.props.canLoadLog && !this.props.hasDbLaunchAndRdpLogReceived && this._commonTableWithFilterViewRef.current) {
            this._commonTableWithFilterViewRef.current.ReSelectData();
        }
    }

    private onDataSelect(args: SelectDataCommonDataModel<LaunchDbAndRdpLogFilterDataForm>) {
        if (!this.props.canLoadLog) {
            return;
        }

        this.changeFilterForDeferentContext(args);

        this.props.dispatchReceiveLaunchDbAndRdpLogThunk({
            pageNumber: args.pageNumber,
            filter: args.filter,
            orderBy: args.sortingData ? this.getSortQuery(args.sortingData) : 'actionCreated.desc',
            force: true
        });
    }

    private getSortQuery(args: SortingDataModel) {
        return `${ args.fieldName }.${ args.sortKind ? 'desc' : 'asc' }`;
    }

    private getNoValueStr() {
        return LaunchDbAndRdpLogTabViewClass._noValueStr;
    }

    private getLaunchTypeString(value: DbLaunchType, data: LaunchDbAndRdpLogItemDataModel) {
        switch (value) {
            case DbLaunchType.Thin: return DbLaunchTypeStrings.dbLaunchTypeThin;
            case DbLaunchType.Thick: return DbLaunchTypeStrings.dbLaunchTypeThick;
            case DbLaunchType.ThinWeb: return DbLaunchTypeStrings.dbLaunchTypeThinWeb;
            case DbLaunchType.WebClient: {
                if (!data.linkAppVersion &&
                    data.linkAppType === LinkAppType.None &&
                    data.action === DbRdpActionType.LaunchDatabase) {
                    return DbLaunchTypeStrings.dbLaunchTypeWebClientCP;
                }
                return DbLaunchTypeStrings.dbLaunchTypeWebClient;
            }
            case DbLaunchType.Auto: return this.getNoValueStr();
            default:
                return this.getNoValueStr();
        }
    }

    private getLinkAppType(value: LinkAppType) {
        return getLinkAppTypeString(value, this.getNoValueStr());
    }

    private getLinkAppAction(value: DbRdpActionType) {
        switch (value) {
            case DbRdpActionType.LaunchDatabase: return 'Запуск базы';
            case DbRdpActionType.OpenRdp: return 'Запуск RDP';
            default: return this.getNoValueStr();
        }
    }

    private changeFilterForDeferentContext(args: SelectDataCommonDataModel<LaunchDbAndRdpLogFilterDataForm>) {
        if (this.props.isDifferentContext) {
            return args.filter!.accountNumber = this.props.differentContextAccountNumber;
        }
    }

    private showAccountCard(accountNumber: number) {
        return () => {
            this.setState({
                isAccountCardVisible: true,
                accountNumber
            });
        };
    }

    private hideAccountCard() {
        this.setState({
            isAccountCardVisible: false,
            accountNumber: 0
        });
    }

    public render() {
        return (
            <>
                <CommonTableWithFilter
                    ref={ this._commonTableWithFilterViewRef }
                    uniqueContextProviderStateId="LaunchDbAndRdpLogContextProviderStateId"
                    filterProps={ {
                        getFilterContentView: (ref, onFilterChanged) => <LaunchDbAndRdpLogFilterFormView
                            isDifferentContext={ this.props.isDifferentContext }
                            ref={ ref }
                            isFilterFormDisabled={ this.props.isInLoadingDataProcess }
                            onFilterChanged={ onFilterChanged }
                        />
                    } }
                    tableProps={ {
                        dataset: this.props.dataset,
                        keyFieldName: 'id',
                        sorting: {
                            sortFieldName: 'actionCreated',
                            sortKind: SortingKind.Desc
                        },
                        fieldsView: {
                            accountNumber: {
                                caption: 'Аккаунт',
                                isSortable: true,
                                fieldContentNoWrap: true,
                                format: (value: number) => {
                                    return (
                                        <TextButton onClick={ this.showAccountCard(value) } canSelectButtonContent={ true }>
                                            <TextOut fontSize={ 13 } className="text-link">
                                                { value }
                                            </TextOut>
                                        </TextButton>
                                    );
                                }
                            },
                            actionCreated: {
                                caption: 'Дата и время',
                                isSortable: true,
                                fieldContentNoWrap: true,
                                format: (value: Date) => {
                                    return DateUtility.dateToDayMonthYearHourMinute(value);
                                }
                            },
                            action: {
                                caption: 'Действие',
                                isSortable: true,
                                fieldContentNoWrap: true,
                                format: (value: DbRdpActionType) => {
                                    return this.getLinkAppAction(value);
                                }
                            },
                            login: {
                                caption: 'Логин',
                                fieldContentNoWrap: true,
                                isSortable: true,
                            },
                            linkAppVersion: {
                                caption: 'Версия Линк42',
                                fieldContentNoWrap: true,
                                isSortable: true,
                                format: (value: string) => {
                                    return !value ? this.getNoValueStr() : value;
                                }
                            },
                            v82Name: {
                                caption: 'Номер базы',
                                fieldContentNoWrap: true,
                                isSortable: true,
                                format: value => {
                                    return (
                                        <TextButton onClick={ () => this.props.openDatabase(value) } canSelectButtonContent={ true }>
                                            <TextOut fontSize={ 13 } className="text-link">
                                                { value }
                                            </TextOut>
                                        </TextButton>
                                    );
                                }
                            },
                            linkAppType: {
                                fieldContentNoWrap: true,
                                caption: 'Тип клиента',
                                isSortable: true,
                                format: (value: LinkAppType) => {
                                    return this.getLinkAppType(value);
                                }
                            },
                            launchType: {
                                caption: 'Режим запуска базы',
                                fieldContentNoWrap: true,
                                isSortable: true,
                                format: (value: DbLaunchType, data: LaunchDbAndRdpLogItemDataModel) => {
                                    return this.getLaunchTypeString(value, data);
                                }
                            },
                            externalIpAddress: {
                                caption: 'Внешний IP адрес',
                                fieldContentNoWrap: true,
                                isSortable: true,
                                format: (value: Nullable<string>) => {
                                    return displayValueOrDefault(value, this.getNoValueStr());
                                }
                            },
                            internalIpAddress: {
                                caption: 'Внутренний IP адрес',
                                fieldContentNoWrap: true,
                                isSortable: true,
                                format: (value: Nullable<string>) => {
                                    return displayValueOrDefault(value, this.getNoValueStr());
                                }
                            }
                        },
                        pagination: this.props.metadata
                    } }
                    onDataSelect={ this.onDataSelect }
                />
                { this.props.databaseCard }
                <AccountCard
                    accountNumber={ this.state.accountNumber }
                    isOpen={ this.state.isAccountCardVisible }
                    onCancelDialogButtonClick={ this.hideAccountCard }
                />
            </>
        );
    }
}

const LaunchDbAndRdpLogTabConnect = connect<StateProps, DispatchProps, {}, AppReduxStoreState>(
    state => {
        const loggingState = state.LoggingState;
        const { launchDbAndRdpLog } = loggingState;
        const dataset = launchDbAndRdpLog.items;

        const currentPage = launchDbAndRdpLog.metadata.pageNumber;
        const totalPages = launchDbAndRdpLog.metadata.pageCount;
        const { pageSize } = launchDbAndRdpLog.metadata;
        const recordsCount = launchDbAndRdpLog.metadata.totalItemCount;

        const { hasDbLaunchAndRdpLogReceived } = loggingState.hasSuccessFor;

        const isInLoadingDataProcess = ProcessIdStateHelper.isInProgress(loggingState.reducerActions, LoggingProcessId.ReceiveLaunchDbAndRdpLog);

        const currentSessionSettingsState = state.Global.getCurrentSessionSettingsReducer;
        const hasSessionSettingsReceived = currentSessionSettingsState.hasSuccessFor.hasSettingsReceived;
        const isDifferentContext = hasSessionSettingsReceived && currentSessionSettingsState.settings.currentContextInfo.isDifferentContextAccount;
        const differentContextAccountNumber = currentSessionSettingsState.settings.currentContextInfo.contextAccountIndex.toString();

        return {
            dataset,
            hasDbLaunchAndRdpLogReceived,
            isInLoadingDataProcess,
            metadata: {
                currentPage,
                totalPages,
                pageSize,
                recordsCount
            },
            isDifferentContext,
            differentContextAccountNumber
        };
    },
    {
        dispatchReceiveLaunchDbAndRdpLogThunk: ReceiveLaunchDbAndRdpLogThunk.invoke
    }
)(LaunchDbAndRdpLogTabViewClass);

export const LaunchDbAndRdpLogTabView = ({ canLoadLog }: OwnProps) => {
    const { openDatabaseDialogHandler, closeDatabaseDialogHandler, dialogDatabaseNumber, isOpen } = useDatabaseCard();

    return (
        <LaunchDbAndRdpLogTabConnect
            canLoadLog={ canLoadLog }
            openDatabase={ openDatabaseDialogHandler }
            databaseCard={
                <InformationDatabaseCard
                    isOpen={ isOpen }
                    databaseNumber={ dialogDatabaseNumber }
                    onClose={ closeDatabaseDialogHandler }
                />
            }
        />
    );
};