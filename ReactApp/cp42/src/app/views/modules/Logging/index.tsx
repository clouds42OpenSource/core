import { TabsControl } from 'app/views/components/controls/TabsControl';
import { withHeader } from 'app/views/components/_hoc/withHeader';
import { CloudChangesTabView } from 'app/views/modules/Logging/tabs/CloudChangesTabView';
import { LaunchDbAndRdpLogTabView } from 'app/views/modules/Logging/tabs/LaunchDbAndRdpLogTabView';
import React from 'react';

type OwnState = {
    activeTabIndex: number;
};

class LoggingViewClass extends React.Component<{}, OwnState> {
    public constructor(props: {}) {
        super(props);
        this.onActiveTabIndexChanged = this.onActiveTabIndexChanged.bind(this);
        this.state = {
            activeTabIndex: 0
        };
    }

    private onActiveTabIndexChanged(tabIndex: number) {
        this.setState({
            activeTabIndex: tabIndex
        });
    }

    public render() {
        return (
            <TabsControl
                activeTabIndex={ this.state.activeTabIndex }
                onActiveTabIndexChanged={ this.onActiveTabIndexChanged }
                headers={
                    [{
                        title: 'Изменение данных',
                        isVisible: true
                    }, {
                        title: 'Запуск баз и RDP',
                        isVisible: true
                    }] }
            >
                <CloudChangesTabView />
                <LaunchDbAndRdpLogTabView canLoadLog={ this.state.activeTabIndex === 1 } />
            </TabsControl>
        );
    }
}

export const LoggingView = withHeader({
    title: 'Логирование',
    page: LoggingViewClass
});