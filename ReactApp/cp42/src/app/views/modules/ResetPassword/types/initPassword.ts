export type InitPassword = {
    password: string;
    confirmationPassword: string;
};