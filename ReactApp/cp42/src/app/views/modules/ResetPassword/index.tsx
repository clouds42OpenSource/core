import { Visibility, VisibilityOff } from '@mui/icons-material';
import { Box, Button, IconButton, InputAdornment, OutlinedInput } from '@mui/material';
import { GetChangePasswordSessionResponse } from 'app/api/endpoints/resetPassword/response';
import { FETCH_API } from 'app/api/useFetchApi';
import { AppRoutes } from 'app/AppRoutes';
import { AppConsts } from 'app/common/constants';
import { useQuery } from 'app/hooks/useQuery';
import { COLORS } from 'app/utils';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { PageHeaderView } from 'app/views/components/_hoc/withHeader/PageHeaderView';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { TextOut } from 'app/views/components/TextOut';
import { templatesError } from 'app/views/modules/ResetPassword/const';
import { stylePassword } from 'app/views/modules/ResetPassword/const/stylePassword';
import { initPasswordErrors, initPasswordValue } from 'app/views/modules/ResetPassword/functions';
import { parsedPasswordErrors as TParsedPasswordErrors } from 'app/views/modules/SignUp/types/SignUpParsedConfig';
import { useFormik } from 'formik';
import React, { ChangeEvent, useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import { Link } from 'react-router-dom';
import * as yup from 'yup';
import styles from './style.module.css';

const { postResetPasswordLetter, getChangePasswordSession, postNewPassword } = FETCH_API.RESET_PASSWORD;

export const ResetPasswordView = withFloatMessages(({ floatMessage: { show } }: FloatMessageProps) => {
    const passwordValidationSchema = yup.object().shape({
        password: yup
            .string()
            .matches(AppConsts.passwordRegex, '')
            .required(),
        confirmationPassword: yup
            .string()
            .oneOf([yup.ref('password')], 'пароли должны совпадать')
            .required()
    });

    const formik = useFormik({
        initialValues: initPasswordValue(),
        validationSchema: passwordValidationSchema,
        onSubmit: () => setNewPassword(),
        validateOnBlur: true,
        validateOnChange: true,
    });

    const history = useHistory();
    const resetCode = useQuery().get('resetcode');

    const [email, setEmail] = useState('');
    const [userInfo, setUserInfo] = useState<GetChangePasswordSessionResponse | null>(null);
    const [parsedPasswordErrors, setParsedPasswordErrors] = useState<TParsedPasswordErrors>(initPasswordErrors);

    const [showError, setShowError] = useState(false);
    const [showPassword, setShowPassword] = useState(false);
    const [showConfirmationPassword, setShowConfirmationPassword] = useState(false);
    const [showLoading, setShowLoading] = useState(false);

    useEffect(() => {
        if (resetCode) {
            getChangePasswordSession({ resetCode }).then(response => {
                if (response.error) {
                    show(MessageType.Error, response.error);
                } else if (response.data && response.data.rawData) {
                    setUserInfo(response.data.rawData);
                }
            });
        }
    }, [resetCode, show]);

    const onMailChange = (event: ChangeEvent<HTMLInputElement>) => {
        setEmail(event.target.value);
    };

    useEffect(() => {
        try {
            yup.string().email().matches(/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/).validateSync(email);
            setShowError(false);
        } catch (error) {
            setShowError(true);
        }
    }, [email]);

    const sendResetMessage = () => {
        if (email === '') {
            setShowError(true);
        } else {
            postResetPasswordLetter(email).then(response => {
                if (response.error) {
                    show(MessageType.Error, response.error);
                } else {
                    show(MessageType.Info, 'Запрос на сброс пароля отправлен. Для сброса пароля перейдите по ссылке указанной в сообщении.');
                    history.push(AppRoutes.signInRoute);
                }
            });
        }
    };

    const setNewPassword = () => {
        setShowLoading(true);
        postNewPassword({
            login: userInfo?.login ?? '',
            loginToken: userInfo?.loginToken ?? '',
            password: formik.values.password,
            controlPassword: formik.values.confirmationPassword
        }).then(response => {
            if (response.error) {
                show(MessageType.Error, response.error);
            } else {
                show(MessageType.Success, 'Пароль успешно изменен');
                history.push(AppRoutes.signInRoute);
            }
        });
        setShowPassword(false);
    };

    const getPasswordErrors = async () => {
        try {
            await passwordValidationSchema.validate(formik.values, { abortEarly: false });
            setParsedPasswordErrors(initPasswordErrors);
        } catch (allErrors) {
            if (allErrors instanceof yup.ValidationError) {
                const passwordErrors = allErrors.inner.map(({ message }) => message);

                setParsedPasswordErrors(parsedPasswordErrors.map(item => ({
                    text: item.text,
                    isValid: !passwordErrors.some(error => error === item.text)
                })));
            }
        }
    };

    useEffect(() => {
        void getPasswordErrors();
    }, [formik.values]);

    return (
        <Box sx={ { backgroundColor: '#E5E5E5', backgroundImage: `url(${ import.meta.env.REACT_APP_CP_HOST }/img/signin/background.png)` } } display="flex" justifyContent="center" alignItems="center" height="100vh">
            { showLoading && <LoadingBounce noMargin={ true } /> }
            <PageHeaderView title="Восстановление пароля - 42Clouds" />
            <Box
                sx={ { backgroundColor: '#FFFFFF', boxShadow: '2px 2px 3px rgba(0,0,0,0.3)' } }
                display="grid"
                gridTemplateColumns="1fr"
                justifyItems="center"
                gap="8px"
                width="400px"
                maxHeight="300px"
                minHeight="250px"
                padding="32px 48px"
                borderRadius="12px"
                textAlign="center"
            >
                <Box>
                    <img height="50px" style={ { color: COLORS.black } } src={ `${ import.meta.env.REACT_APP_CP_HOST }/img/signin/logo.svg` } alt="42clouds" loading="lazy" />
                </Box>
                <TextOut fontWeight={ 700 } style={ { color: COLORS.black } } fontSize={ 16 } textAlign="center">
                    { resetCode ? 'Смена пароля' : 'Восстановление пароля' }
                </TextOut>
                <Box display="flex" flexDirection="column" textAlign="center" gap="8px" minWidth="300px">
                    { resetCode
                        ? (
                            <>
                                <OutlinedInput
                                    fullWidth={ true }
                                    id="password"
                                    placeholder="Новый пароль"
                                    value={ formik.values.password }
                                    onChange={ formik.handleChange }
                                    autoComplete="off"
                                    error={ formik.values.password !== '' && parsedPasswordErrors.find(item => item.text = '')?.isValid }
                                    className={ styles.input }
                                    endAdornment={
                                        <InputAdornment position="end">
                                            <IconButton
                                                aria-label="toggle password visibility"
                                                onClick={ () => setShowPassword(showIcon => !showIcon) }
                                                onMouseDown={ (event: React.MouseEvent<HTMLButtonElement>) => event.preventDefault() }
                                                edge="end"
                                            >
                                                { showPassword ? <VisibilityOff /> : <Visibility /> }
                                            </IconButton>
                                        </InputAdornment>
                                    }
                                    sx={ !showPassword ? stylePassword : undefined }
                                />
                                <OutlinedInput
                                    fullWidth={ true }
                                    placeholder="Подтверждение пароля"
                                    id="confirmationPassword"
                                    value={ formik.values.confirmationPassword }
                                    onChange={ formik.handleChange }
                                    autoComplete="off"
                                    className={ styles.input }
                                    error={ formik.values.confirmationPassword !== '' && formik.values.confirmationPassword !== formik.values.password }
                                    endAdornment={
                                        <InputAdornment position="end">
                                            <IconButton
                                                aria-label="toggle password visibility"
                                                onClick={ () => setShowConfirmationPassword(showIcon => !showIcon) }
                                                onMouseDown={ (event: React.MouseEvent<HTMLButtonElement>) => event.preventDefault() }
                                                edge="end"
                                            >
                                                { showConfirmationPassword ? <VisibilityOff /> : <Visibility /> }
                                            </IconButton>
                                        </InputAdornment>
                                    }
                                    sx={ !showConfirmationPassword ? stylePassword : undefined }
                                />
                            </>
                        )
                        : (
                            <>
                                <OutlinedInput
                                    type="text"
                                    placeholder="E-mail"
                                    value={ email }
                                    className={ styles.input }
                                    error={ showError && email.length > 0 }
                                    onChange={ onMailChange }
                                />
                                { showError && email.length > 0
                                    ? (
                                        <TextOut style={ { color: COLORS.error } }>
                                            { email.length ? templatesError.invalidFieldFormat : templatesError.required }
                                        </TextOut>
                                    )
                                    : <></>
                                }
                            </>
                        )
                    }
                    <Button
                        variant="contained"
                        style={ { borderRadius: '12px', boxShadow: '2px 2px 3px rgba(0,0,0,0.3)', color: COLORS.black, textTransform: 'none', fontSize: '16px' } }
                        onClick={ resetCode ? formik.submitForm : sendResetMessage }
                        disabled={ !(resetCode ? (formik.isValid && formik.values.password.length > 0 && formik.values.confirmationPassword.length > 0) : !showError && email !== '') }
                    >
                        { resetCode ? 'Сохранить новый пароль' : 'Восстановить пароль' }
                    </Button>
                    { !resetCode && (
                        <>
                            <Box display="flex" justifyContent="space-between">
                                <Link to={ AppRoutes.signInRoute } className={ styles.link }>
                                    Войти
                                </Link>
                                <Link to={ AppRoutes.signUpRoute } className={ styles.link }>
                                    Создать аккаунт
                                </Link>
                            </Box>
                            <TextOut textAlign="center">
                                Укажите именно тот электронный адрес, который вы вводили при регистрации в личном кабинете. На этот адрес вам придет письмо с ссылкой для восстановления пароля.
                            </TextOut>
                        </>
                    )
                    }
                </Box>
            </Box>
        </Box>
    );
});