export const numbersOrSpecialCharactersRegexp = /[!@#$^&*_+\-=[\];':"\\|,.<>/?]+|\d/;
export const whiteSpaceRegexp = /^(\S+$)/g;