export const stylePassword = {
    '& input': {
        textSecurity: 'disc',
        MozTextSecurity: 'disc',
        WebkitTextSecurity: 'disc'
    },
};