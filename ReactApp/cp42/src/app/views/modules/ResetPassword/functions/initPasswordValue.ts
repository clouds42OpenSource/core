import { InitPassword } from 'app/views/modules/ResetPassword/types';

export const initPasswordValue = (): InitPassword => {
    return {
        password: '',
        confirmationPassword: ''
    };
};