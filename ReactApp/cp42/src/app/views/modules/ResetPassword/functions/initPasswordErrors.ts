import { parsedPasswordErrors } from 'app/views/modules/SignUp/types/SignUpParsedConfig';

export const initPasswordErrors = (): parsedPasswordErrors => {
    return [
        {
            text: 'от 7 до 25 символов',
            isValid: true,
        },
        {
            text: 'большие латинские буквы',
            isValid: true,
        },
        {
            text: 'маленькие латинские буквы',
            isValid: true,
        },
        {
            text: 'цифры или спец символы, без пробелов',
            isValid: true,
        },
        {
            text: 'не равен логину',
            isValid: true,
        },
        {
            text: 'пароли должны совпадать',
            isValid: true,
        }
    ];
};