/* eslint-disable @typescript-eslint/no-empty-function */
import {
    Cancel as CancelIcon,
    Check as CheckIcon,
    Close as CloseIcon,
    ErrorOutline as ErrorOutlineIcon,
    KeyboardArrowDown as KeyboardArrowDownIcon,
    KeyboardArrowUp as KeyboardArrowUpIcon,
    Search as SearchIcon,
    ShoppingCart as ShoppingCartIcon,
    UnfoldMore as UnfoldMoreIcon
} from '@mui/icons-material';
import { Box, IconButton, InputAdornment, TextField, Tooltip, Typography, Link as MuiLink } from '@mui/material';
import Button from '@mui/material/Button';
import { AppRoutes } from 'app/AppRoutes';
import { tryPayAccessResponeDto, userItemModel } from 'app/api/endpoints/rent/response';
import { FETCH_API } from 'app/api/useFetchApi';
import { AccountUserGroup } from 'app/common/enums';
import { ETourId } from 'app/views/Layout/ProjectTour/enums';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { TextOut } from 'app/views/components/TextOut';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { SuccessButton } from 'app/views/components/controls/Button';
import { Dialog } from 'app/views/components/controls/Dialog';
import { Props, accessListItem } from 'app/views/modules/Rent/tabs/RentUsers/types';
import { RentUserItem } from 'app/views/modules/Rent/tabs/RentUsers/views/RentUsersItem';
import dayjs from 'dayjs';
import 'dayjs/locale/ru';
import { memo, useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import { Link } from 'react-router-dom';
import css from '../../styles.module.css';

dayjs.locale('ru');

export const RentUsers = memo(({ userList, freeRdpResources, freeWebResources, currentUserGroups, refreshData, rentLocale, expireDate, show, setIsLoading, isLoading, localizationsData }: Props) => {
    const [externalSearchValue, setExternalSearchValue] = useState<string>('');
    const [searchValue, setSearchValue] = useState('');
    const [formData, setFormData] = useState<accessListItem[]>([]);
    const [freeRdp, setFreeRdp] = useState(freeRdpResources);
    const [freeWeb, setFreeWeb] = useState(freeWebResources);
    const [isPaymentRequired, setIsPaymentRequired] = useState(false);
    const [paymentData, setPaymentData] = useState<tryPayAccessResponeDto | null>(null);
    const [paymentCost, setPaymentCost] = useState(0);
    const [externalUsers, setExternalUsers] = useState<userItemModel[]>([]);
    const [sortingData, setSortingData] = useState<{ name: keyof userItemModel, sortKind: string }>({ name: 'fullUserName', sortKind: 'desc' });
    const [filteredUsers, setFilteredUsers] = useState(userList);
    const [alertData, setAlertData] = useState<{data: userItemModel | null, callback:() => void}>({ data: null, callback: () => {} });
    const history = useHistory();
    const isAllowedChange = currentUserGroups.includes(AccountUserGroup.AccountAdmin) ||
        currentUserGroups.includes(AccountUserGroup.Hotline) ||
        currentUserGroups.includes(AccountUserGroup.AccountSaleManager) ||
        currentUserGroups.includes(AccountUserGroup.CloudAdmin);
    const isShowLink42 = currentUserGroups.includes(AccountUserGroup.Hotline) ||
        currentUserGroups.includes(AccountUserGroup.CloudSE) ||
        currentUserGroups.includes(AccountUserGroup.CloudAdmin);

    useEffect(() => {
        void calculateSum();
    }, [formData]);

    const handleFreeRdpResources = (value: string | null, cleanFormData: accessListItem[]) => {
        if (value && !cleanFormData.some(fData => fData.rdpResourceId === value)) {
            return value;
        }

        if (freeRdp[0]) {
            setFreeRdp(rdp => rdp.slice(1));
            return freeRdp[0];
        }

        return null;
    };

    const handleFreeWebResources = (value: string | null, cleanFormData: accessListItem[]) => {
        if (value && !cleanFormData.some(fData => fData.webResourceId === value)) {
            return value;
        }

        if (freeWeb[0]) {
            setFreeWeb(web => web.slice(1));
            return freeWeb[0];
        }

        return null;
    };

    const getCleanFormData = (id: string) => {
        return formData.filter(item => item.accountUserId !== id);
    };

    const clearFormData = () => {
        setFormData([]);
        setFreeRdp(freeRdpResources);
        setFreeWeb(freeWebResources);
    };

    const handleClearExternaluser = (externalUserId: string) => {
        setExternalUsers(externalUsers.filter(externalUser => externalUser.id !== externalUserId));
    };

    const handleOpenAlert = (userItem: userItemModel, callback: () => void) => {
        setAlertData({ data: userItem, callback });
    };

    const handleCloseAlert = () => {
        setAlertData({ data: null, callback: () => {} });
    };

    const handleChange = (type: string, accountId: string, rdpResourceId: string | null, webResourceId: string | null, checked: boolean, isExternal?: boolean) => {
        const cleanFormData = getCleanFormData(accountId);
        let accessItem: accessListItem;

        if (checked) {
            accessItem = {
                accountUserId: accountId,
                webResourceId: handleFreeWebResources(webResourceId, cleanFormData),
                rdpResourceId: type === 'rdp' ? handleFreeRdpResources(rdpResourceId, cleanFormData) : null,
                webResource: type === 'web',
                standartResource: type === 'rdp'
            };

            if (webResourceId) {
                setFreeWeb(freeWeb.filter(id => id !== webResourceId));
            }

            if (rdpResourceId) {
                if (type === 'web' && !cleanFormData.some(fData => fData.rdpResourceId === rdpResourceId)) {
                    let isUpdated = false;
                    setFormData([...cleanFormData.map(fData => {
                        if (fData.standartResource && !fData.rdpResourceId && !isUpdated) {
                            isUpdated = true;
                            return { ...fData, rdpResourceId };
                        }
                        return fData;
                    }), accessItem]);

                    if (!isUpdated) {
                        setFreeRdp([...freeRdp.filter(id => id !== rdpResourceId), rdpResourceId]);
                    }

                    return;
                }
                setFreeRdp(freeRdp.filter(id => id !== rdpResourceId));
                setFormData([...cleanFormData, accessItem]);
                return;
            }
            setFormData([...cleanFormData, accessItem]);
        } else {
            if (isExternal) handleClearExternaluser(accountId);
            accessItem = {
                accountUserId: accountId,
                webResourceId: null,
                rdpResourceId: null,
                webResource: false,
                standartResource: false
            };
            if (rdpResourceId && !webResourceId && !cleanFormData.some(fData => fData.rdpResourceId === rdpResourceId)) {
                let isRdpUpdated = false;

                const checkedFormData = cleanFormData.map(fData => {
                    if (fData.standartResource && !fData.rdpResourceId && !isRdpUpdated) {
                        isRdpUpdated = true;
                        return { ...fData, rdpResourceId };
                    }
                    return fData;
                });

                if (isExternal) {
                    setFormData(checkedFormData);
                } else {
                    setFormData([...checkedFormData, accessItem]);
                }

                if (!isRdpUpdated) {
                    setFreeRdp([...freeRdp.filter(id => id !== rdpResourceId), rdpResourceId]);
                }
                return;
            }
            if (webResourceId && !rdpResourceId && !cleanFormData.some(fData => fData.webResourceId === webResourceId)) {
                let isWebUpdated = false;
                const checkedFormData = cleanFormData.map(fData => {
                    if ((fData.webResource || fData.standartResource) && !fData.webResourceId && !isWebUpdated) {
                        isWebUpdated = true;
                        return { ...fData, webResourceId };
                    }
                    return fData;
                });
                if (isExternal) {
                    setFormData(checkedFormData);
                } else {
                    setFormData([...checkedFormData, accessItem]);
                }

                if (!isWebUpdated) {
                    setFreeWeb([...freeWeb.filter(id => id !== webResourceId), webResourceId]);
                }
                return;
            }
            if (webResourceId && rdpResourceId) {
                let isWebUpdated = cleanFormData.some(fData => fData.webResourceId === webResourceId);
                let isRdpUpdated = cleanFormData.some(fData => fData.rdpResourceId === rdpResourceId);
                const checkedFormData = cleanFormData.map(fData => {
                    let tempData = fData;
                    if ((fData.webResource || fData.standartResource) && !fData.webResourceId && !isWebUpdated) {
                        isWebUpdated = true;
                        tempData = { ...tempData, webResourceId };
                    }
                    if (fData.standartResource && !fData.rdpResourceId && !isRdpUpdated) {
                        isRdpUpdated = true;
                        tempData = { ...tempData, rdpResourceId };
                    }
                    return tempData;
                });
                if (isExternal) {
                    setFormData(checkedFormData);
                } else {
                    setFormData([...checkedFormData, accessItem]);
                }

                if (!isWebUpdated) {
                    setFreeWeb([...freeWeb.filter(id => id !== webResourceId), webResourceId]);
                }

                if (!isRdpUpdated) {
                    setFreeRdp([...freeRdp.filter(id => id !== rdpResourceId), rdpResourceId]);
                }
                return;
            }

            setFormData([...cleanFormData, accessItem]);
        }
    };

    const handleSubmit = async () => {
        setIsLoading(true);
        const response = await FETCH_API.RENT.tryPayAccess(formData);
        const { data } = response;
        setIsLoading(false);
        if (response.success) {
            if (!data?.errorMessage) {
                if (data && !data.complete) {
                    setPaymentData(data);
                    setIsPaymentRequired(true);
                } else {
                    show(MessageType.Success, 'Успешно');
                    if (refreshData) refreshData();
                }
            } else {
                show(MessageType.Error, data.errorMessage);
            }
        } else {
            show(MessageType.Error, response.message);
        }
    };

    const calculateSum = () => {
        const sum = formData.reduce((acc, item) => {
            const isStandardAndNotRdpAndNotWeb = item.standartResource && !item.rdpResourceId && !item.webResourceId;
            const isStandardAndNotRdp = item.standartResource && !item.rdpResourceId;
            const isStandardOrWeb = (item.webResource || item.standartResource) && !item.webResourceId;

            if (isStandardAndNotRdpAndNotWeb || isStandardAndNotRdp || isStandardOrWeb) {
                const user = userList.find(userElem => userElem.id === item.accountUserId);

                if (isStandardAndNotRdpAndNotWeb && user) {
                    return acc + user.partialUserCostStandart;
                }

                if (isStandardAndNotRdp && user) {
                    return acc + user.partialUserCostRdp;
                }

                if (isStandardOrWeb && user) {
                    return acc + user.partialUserCostWeb;
                }
            }

            return acc;
        }, 0);

        setPaymentCost(sum);
    };

    const handleCloseModal = () => {
        setIsPaymentRequired(false);
        setPaymentData(null);
    };

    const handleTryPromisePayment = async () => {
        const promiseFormData = formData.filter(fData => (fData.standartResource && (!fData.rdpResourceId || !fData.webResourceId)) || (fData.webResource && !fData.webResourceId));
        setIsLoading(true);
        const response = await FETCH_API.RENT.tryPromisePayment(promiseFormData);
        if (response.success) {
            show(MessageType.Success, 'Успешно');
            setIsLoading(false);
            if (refreshData) refreshData();
        } else {
            show(MessageType.Error, response.message);
        }
        handleCloseModal();
    };

    const handleExternalUser = async () => {
        setIsLoading(true);
        const response = await FETCH_API.RENT.tryExternalUser(externalSearchValue);
        setIsLoading(false);
        if (response.data) {
            setExternalUsers([response.data, ...externalUsers.filter((({ id }) => id !== response.data?.id))]);
        } else {
            show(MessageType.Error, response.message);
        }
    };

    const getSortingButton = (sortName: keyof userItemModel) => {
        const { name, sortKind } = sortingData;

        if (name !== sortName) {
            return (
                <Box display="flex" flexDirection="column">
                    <IconButton sx={ { fontSize: '16px', padding: 0 } } onClick={ () => setSortingData({ name: sortName, sortKind: 'desc' }) }>
                        <UnfoldMoreIcon fontSize="inherit" />
                    </IconButton>
                </Box>
            );
        }

        return (
            <Box display="flex" flexDirection="column">
                { !(name === sortName && sortKind === 'desc') &&
                    <IconButton sx={ { fontSize: '16px', padding: 0 } } onClick={ () => setSortingData({ name: sortName, sortKind: 'desc' }) }>
                        <KeyboardArrowDownIcon fontSize="inherit" />
                    </IconButton>
                }
                { !(name === sortName && sortKind === 'asc') &&
                    <IconButton sx={ { fontSize: '16px', padding: 0 } } onClick={ () => setSortingData({ name: sortName, sortKind: 'asc' }) }>
                        <KeyboardArrowUpIcon fontSize="inherit" />
                    </IconButton>
                }
            </Box>
        );
    };

    const handleFilterUsers = (value: string) => {
        const valueInLower = value.toLowerCase();
        if (!value) {
            setSearchValue('');
            setFilteredUsers(userList);
        }
        setFilteredUsers(userList.filter(userItem => userItem.fullUserName.toLowerCase().includes(valueInLower) || userItem.login.toLowerCase().includes(valueInLower)));
    };

    useEffect(() => {
        setExternalUsers([]);
        setFormData([]);
        handleFilterUsers(searchValue);
    }, [userList]);

    const getSortedUsers = () => {
        const { name, sortKind } = sortingData;

        return filteredUsers.sort((a, b) => {
            if (name === 'webResourceId') {
                const hasWebResourceA = a.webResourceId;
                const hasWebResourceB = b.webResourceId;
                const hasRdpResourceA = a.rdpResourceId;
                const hasRdpResourceB = b.rdpResourceId;

                if (sortKind === 'asc') {
                    if (hasWebResourceA && !hasRdpResourceA) {
                        return hasWebResourceB && !hasRdpResourceB ? 0 : -1;
                    }

                    if (hasWebResourceB && !hasRdpResourceB) {
                        return 1;
                    }

                    return 0;
                }

                if (sortKind === 'desc') {
                    if (hasWebResourceA && !hasRdpResourceA) {
                        return hasWebResourceB && !hasRdpResourceB ? 0 : 1;
                    }

                    if (hasWebResourceB && !hasRdpResourceB) {
                        return -1;
                    }

                    return 0;
                }
            }

            const nameA = a[name] ? String(a[name]).toLowerCase().trim() : '';
            const nameB = b[name] ? String(b[name]).toLowerCase().trim() : '';

            switch (sortKind) {
                case 'asc':
                    return nameA.localeCompare(nameB);
                case 'desc':
                    return nameB.localeCompare(nameA);
                default:
                    return 0;
            }
        });
    };

    return (
        <>
            { isLoading && <LoadingBounce /> }
            <Box sx={ { display: 'flex', gap: '15px', justifyContent: 'space-between', flexWrap: 'wrap', paddingBlock: '10px' } }>
                { isAllowedChange &&
                    <Box sx={ {
                        display: 'flex',
                        gap: '10px',
                        '@media (max-width: 800px)': {
                            flexDirection: 'column'
                        }
                    } }
                    >
                        <Box>
                            <TextField
                                sx={ {
                                    width: '100%',
                                    minWidth: '350px',
                                    '& input': { padding: '6px' },
                                    '@media (max-width: 400px)': {
                                        minWidth: '200px'
                                    }
                                } }
                                placeholder="Введите email пользователя"
                                onChange={ e => setExternalSearchValue(e.target.value) }
                                value={ externalSearchValue }
                            />
                        </Box>
                        <Box>
                            <Tooltip
                                title={
                                    <>
                                        <span>Что такое спонсирование можете узнать более подробно в </span>
                                        <a style={ { fontWeight: 'bold', textDecoration: 'underline' } } href="https://42clouds.com/ru-ru/manuals/chto-takoe-sponsirovanie-i-kak-s-nim-rabotat.html">инструкции</a>
                                    </>
                                }
                            >
                                <SuccessButton
                                    onClick={ handleExternalUser }
                                    isEnabled={ !!externalSearchValue }
                                >
                                    <SearchIcon /> Спонсировать
                                </SuccessButton>
                            </Tooltip>
                        </Box>
                    </Box>
                }
                <Box sx={ {
                    display: 'flex',
                    gap: '10px',
                    '@media (max-width: 800px)': {
                        minWidth: '200px',
                        flexDirection: 'column'
                    }
                } }
                >
                    <TextField
                        sx={ { width: '100%', minWidth: '200px', maxWidth: '350px', '& input': { padding: '6px' } } }
                        placeholder="Поиск"
                        onChange={ e => setSearchValue(e.target.value) }
                        value={ searchValue }
                        InputProps={ {
                            endAdornment: (
                                <InputAdornment position="end">
                                    <IconButton
                                        aria-label="clear"
                                        sx={ { padding: 0 } }
                                        onClick={ () => handleFilterUsers('') }
                                    >
                                        <CloseIcon />
                                    </IconButton>
                                </InputAdornment>
                            )
                        } }
                    />
                    <SuccessButton style={ { minWidth: '140px' } } onClick={ () => handleFilterUsers(searchValue) }><SearchIcon /> Поиск</SuccessButton>
                </Box>
            </Box>
            <Box className={ css['resources-list'] } data-tourid={ ETourId.rentPageTariff }>
                <Box fontSize="13px">
                    <Box>
                        Пользователь { getSortingButton('fullUserName') }
                    </Box>
                    <Box>
                        <Box display="flex" gap="3px" alignItems="center">
                            <TextOut fontWeight={ 700 }>WEB</TextOut>
                            <Tooltip title="Позволяет работать только через браузер или тонкий клиент.">
                                <i className="fa fa-question-circle-o" aria-hidden="true" />
                            </Tooltip>
                        </Box>
                        { getSortingButton('webResourceId') }
                    </Box>
                    <Box>
                        <Box display="flex" gap="3px" alignItems="center">
                            <TextOut fontWeight={ 700 }>Стандарт</TextOut>
                            <Tooltip title="Дает возможность работы также через RDP, RemoteApp, получить разовую бесплатную консультацию 1С и дает доступ к сервису Fasta">
                                <i className="fa fa-question-circle-o" aria-hidden="true" />
                            </Tooltip>
                        </Box>
                        { getSortingButton('rdpResourceId') }
                    </Box>
                </Box>
                { externalUsers && externalUsers.map(user =>
                    <RentUserItem
                        handleOpenAlert={ handleOpenAlert }
                        handleClearExternaluser={ handleClearExternaluser }
                        isAllowedChange={ isAllowedChange }
                        key={ user.id }
                        accessItem={ formData.find(({ accountUserId }) => accountUserId === user.id) }
                        handleChange={ handleChange }
                        userItem={ user }
                    />
                ) }
                { getSortedUsers().map(user =>
                    <RentUserItem
                        handleOpenAlert={ handleOpenAlert }
                        isShowLink42={ isShowLink42 }
                        isAllowedChange={ isAllowedChange }
                        key={ user.id }
                        accessItem={ formData.find(({ accountUserId }) => accountUserId === user.id) }
                        handleChange={ handleChange }
                        userItem={ user }
                    />
                ) }
            </Box>
            <Box sx={ { marginTop: '15px' } }>
                <Link to={ AppRoutes.accountManagement.informationBases } style={ { color: '#337ab7', fontSize: '13px' } }>
                    Назначить доступ к информационным базам
                </Link>
            </Box>
            { formData.length
                ? (
                    <Box sx={ { display: 'flex', justifyContent: 'flex-end', gap: '6px', marginTop: '6px', position: 'fixed', bottom: 0, left: 0, width: '100%', padding: '20px 20px 20px 0', background: 'rgba(255, 255, 255, 0.85)' } }>
                        <Button
                            variant="contained"
                            sx={ {
                                textTransform: 'initial',
                                color: 'white',
                                background: '#23c6c8',
                                marginLeft: '6px',
                                '&:hover': {
                                    background: '#23c6c8',
                                },
                            } }
                            onClick={ clearFormData }
                            startIcon={ <CancelIcon /> }
                        >
                            Отмена
                        </Button>
                        <Button
                            data-tourid={ ETourId.rentPageAcceptButton }
                            onClick={ handleSubmit }
                            disabled={ !formData.length }
                            variant="contained"
                            sx={ {
                                textTransform: 'initial',
                                color: 'white',
                                background: '#23c6c8',
                                margin: '0 20px 0 6px',
                                '&:hover': {
                                    background: '#23c6c8'
                                },
                            } }
                            startIcon={ paymentCost ? <ShoppingCartIcon /> : <CheckIcon /> }
                        >
                            { paymentCost ? `Купить ${ paymentCost } ${ rentLocale.currency }` : 'Применить' }
                        </Button>
                    </Box>
                ) : ''
            }
            <Dialog
                isOpen={ isPaymentRequired }
                dialogWidth="sm"
                dialogVerticalAlign="center"
                onCancelClick={ handleCloseModal }
                buttons={
                    [
                        {
                            content: 'Отмена',
                            kind: 'error',
                            variant: 'contained',
                            onClick: handleCloseModal
                        },
                        {
                            content: 'Взять обещанный платеж',
                            kind: 'primary',
                            isEnabled: paymentData?.canGetPromisePayment,
                            variant: 'text',
                            onClick: handleTryPromisePayment
                        },
                        {
                            content: 'Оплатить сейчас',
                            kind: 'primary',
                            variant: 'text',
                            onClick: () => history.push(`${ AppRoutes.accountManagement.balanceManagement }/replenish-balance?customPayment.amount=${ paymentData?.costForPay }
                            &customPayment.description=Оплата+за+использование+сервиса+"Аренда+1С"+до+${ dayjs(expireDate).format('D MMMM YYYY [г.]') }`)
                        }
                    ]
                }
            >
                <Box display="flex" flexDirection="column" alignItems="center" justifyContent="center">
                    <ErrorOutlineIcon sx={ { fontSize: '88px', color: '#f8bb86' } } />
                    <Typography>
                        Уважаемый пользователь, для активации сервиса у Вас недостаточно средств в размере { paymentData?.costForPay } руб.
                        { paymentData?.canGetPromisePayment ? 'Вы можете воспользоваться услугой "Обещанный платеж" или оплатить сейчас.' : '' }
                    </Typography>
                </Box>
            </Dialog>
            <Dialog
                isOpen={ !!alertData.data }
                dialogWidth="sm"
                dialogVerticalAlign="center"
                onCancelClick={ handleCloseAlert }
                buttons={
                    [
                        {
                            content: 'Отключить все',
                            kind: 'primary',
                            variant: 'contained',
                            onClick: () => {
                                alertData.callback();
                                handleCloseAlert();
                            }
                        },
                        {
                            content: 'Закрыть',
                            kind: 'primary',
                            variant: 'contained',
                            onClick: handleCloseAlert
                        }
                    ]
                }
            >
                <Box display="flex" flexDirection="column" alignItems="center" justifyContent="center">
                    <ErrorOutlineIcon sx={ { fontSize: '88px', color: '#f8bb86' } } />
                    <Typography variant="body1">
                        Внимание
                    </Typography>
                    <Typography variant="body2">
                        Для пользователя ({ alertData.data?.login }) будут отключены все зависимые от { localizationsData.find(({ key }) => key === 0)?.value } сервисы:
                        { alertData.data?.namesOfDependentActiveServices.map(({ value, dependServiceIds }) =>
                            value.map((text, i) => <MuiLink href={ `${ import.meta.env.PUBLIC_URL }/billing-service/${ dependServiceIds[i] }` } key={ text } target="_blank" rel="noreferrer">{ text }&nbsp;</MuiLink>)) }
                    </Typography>
                </Box>
            </Dialog>
        </>
    );
});