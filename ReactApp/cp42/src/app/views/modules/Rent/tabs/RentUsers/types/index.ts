import { getLocalizationsResponseDto } from 'app/api/endpoints/locale/response/getLocaleResponseDto';
import { rentLocale, userItemModel } from 'app/api/endpoints/rent/response';
import { AccountUserGroup } from 'app/common/enums';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { ReactNode } from 'react';

export type Props = {
    userList: userItemModel[];
    freeRdpResources: string[];
    freeWebResources: string[];
    refreshData?: () => void;
    expireDate: string;
    show: (type: MessageType, content: ReactNode, autoHideDuration?: number) => void;
    currentUserGroups: AccountUserGroup[];
    isLoading: boolean;
    setIsLoading: (value: boolean) => void;
    rentLocale: rentLocale;
    localizationsData: getLocalizationsResponseDto;
};

export type accessListItem = {
    accountUserId: string;
    webResourceId: string | null;
    rdpResourceId: string | null;
    webResource: boolean;
    standartResource: boolean;
};

export type RentUserItemProps = {
    userItem: userItemModel;
    accessItem?: accessListItem;
    handleChange: (type: string, guid: string, rdpResourceId: string | null, webResourceId: string | null, checked: boolean, isExternal?: boolean) => void;
    isAllowedChange: boolean;
    handleClearExternaluser?: (externalUserId: string) => void;
    isShowLink42?: boolean;
    handleOpenAlert: (data: userItemModel, callback: () => void) => void;
};