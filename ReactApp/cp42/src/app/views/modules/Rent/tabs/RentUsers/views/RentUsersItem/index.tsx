import { Circle as CircleIcon, Close as CloseIcon } from '@mui/icons-material';
import { Box, IconButton, Tooltip, Typography } from '@mui/material';
import { useTour } from '@reactour/tour';
import { TextOut } from 'app/views/components/TextOut';
import CustomizedSwitches from 'app/views/components/controls/Switch/views/SwitchViews';
import { RentUserItemProps } from 'app/views/modules/Rent/tabs/RentUsers/types';
import React, { useEffect, useState } from 'react';

export const RentUserItem = ({ userItem, handleChange, handleClearExternaluser, accessItem, isAllowedChange, isShowLink42, handleOpenAlert }: RentUserItemProps) => {
    const { setCurrentStep } = useTour();

    const [webChecked, setWebChecked] = useState<boolean>(false);
    const [rdpChecked, setRdpChecked] = useState<boolean>(false);

    const handleCheckbox = (value: boolean, type: string, isExternal?: boolean) => {
        setCurrentStep(prev => prev + 1);
        const handleChangeWrapper = () => {
            handleChange(
                type,
                userItem.id,
                (accessItem ? accessItem.rdpResourceId : userItem.rdpResourceId),
                (accessItem ? accessItem.webResourceId : userItem.webResourceId),
                value,
                isExternal
            );
        };
        if (!value && userItem.namesOfDependentActiveServices.some(({ value: userValue }) => userValue.length)) {
            handleOpenAlert(userItem, handleChangeWrapper);
        } else {
            handleChange(type, userItem.id, (accessItem ? accessItem.rdpResourceId : userItem.rdpResourceId), (accessItem ? accessItem.webResourceId : userItem.webResourceId), value, isExternal);
        }
    };

    useEffect(() => {
        if (accessItem) {
            setWebChecked(accessItem.webResource && !accessItem.standartResource);
            setRdpChecked(accessItem.standartResource);
        } else {
            setWebChecked(!!userItem.webResourceId && !userItem.rdpResourceId);
            setRdpChecked(!!userItem.rdpResourceId);
        }
    }, [accessItem, userItem]);

    const renderWebResourceCost = () => {
        if (accessItem?.webResource && !accessItem?.webResourceId) {
            return `${ userItem.partialUserCostWeb } руб`;
        }

        return '';
    };

    const renderStandartResourceCost = () => {
        if (accessItem?.standartResource && (!accessItem?.rdpResourceId && !accessItem?.webResourceId)) {
            return `${ userItem.partialUserCostStandart } руб`;
        }

        if (accessItem?.standartResource && !accessItem?.rdpResourceId) {
            return `${ userItem.partialUserCostRdp } руб`;
        }

        if (accessItem?.standartResource && accessItem.rdpResourceId && !accessItem.webResourceId) {
            return `${ userItem.partialUserCostWeb } руб`;
        }

        return '';
    };

    return (
        <Box key={ userItem.accountId }>
            <Box>
                { userItem.link42Info && isShowLink42 &&
                    <Tooltip title={ userItem.link42Info.version }>
                        <IconButton aria-label="delete">
                            <CircleIcon sx={ { color: userItem.link42Info.isCurrentVersion ? 'rgba(30, 212, 19, 0.99)' : 'red', fontSize: '12px' } } />
                        </IconButton>
                    </Tooltip>
                }
                { userItem.login } { userItem.fullUserName ? `(${ userItem.fullUserName })` : '' }

                { userItem.sponsoredLicenseAccountName &&
                    <TextOut style={ { color: 'red', fontSize: '10px', fontWeight: 'normal', marginLeft: '10px' } }>Спонсирую { userItem.sponsoredLicenseAccountName }</TextOut>
                }
                { userItem.sponsorshipLicenseAccountName &&
                    <TextOut style={ { color: 'rgba(30, 212, 19, 0.99)', fontSize: '10px', fontWeight: 'normal', marginLeft: '10px' } }>Спонсируется { userItem.sponsorshipLicenseAccountName }</TextOut>
                }

            </Box>
            <Box sx={ { display: 'flex', alignItems: 'center', gap: '10px' } }>
                <Typography
                    sx={ {
                        display: { xs: 'block', sm: 'none' },
                        marginLeft: '10px'
                    } }
                >
                    WEB
                </Typography>
                <CustomizedSwitches disabled={ !isAllowedChange } onChange={ (event: React.ChangeEvent<HTMLInputElement>) => handleCheckbox(event.target.checked, 'web') } checked={ webChecked } />
                <Typography sx={ { marginLeft: '10px' } }>
                    { renderWebResourceCost() }
                </Typography>
            </Box>
            <Box sx={ { display: 'flex', alignItems: 'center' } }>
                <Typography
                    sx={ {
                        display: { xs: 'block', sm: 'none' },
                        marginLeft: '10px'
                    } }
                >
                    Стандарт
                </Typography>
                <CustomizedSwitches disabled={ !isAllowedChange } onChange={ (event: React.ChangeEvent<HTMLInputElement>) => handleCheckbox(event.target.checked, 'rdp') } checked={ rdpChecked } />
                <Typography sx={ { marginLeft: '10px' } }>
                    { renderStandartResourceCost() }
                </Typography>
                { handleClearExternaluser && (
                    <IconButton color="primary" onClick={ () => handleCheckbox(false, 'web', true) }>
                        <CloseIcon />
                    </IconButton>
                ) }
            </Box>
        </Box>
    );
};