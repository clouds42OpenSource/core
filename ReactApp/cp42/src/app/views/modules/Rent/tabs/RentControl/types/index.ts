import { getRentManagementResponseDto, serviceResponseDto } from 'app/api/endpoints/rent/response';
import { AccountUserGroup } from 'app/common/enums';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { ReactNode } from 'react';

export type Props = {
    rentManagement: getRentManagementResponseDto;
    show: (type: MessageType, content: ReactNode, autoHideDuration?: number) => void;
    isDifferentContext: boolean;
    currentUserGroups: AccountUserGroup[];
    isBlocked: boolean;
    isLoading: boolean;
    setIsLoading: (value: boolean) => void;

}

export type RentControlFormProps = {
    service: serviceResponseDto;
    show: (type: MessageType, content: ReactNode, autoHideDuration?: number) => void;
    isDifferentContext: boolean;
    isBlocked: boolean;
    isLoading: boolean;
    currentUserGroups: AccountUserGroup[];

    setIsLoading: (value: boolean) => void;
}