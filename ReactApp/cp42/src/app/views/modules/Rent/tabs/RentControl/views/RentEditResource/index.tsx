import { Box, Button, TextField, Typography } from '@mui/material';
import { FETCH_API } from 'app/api/useFetchApi';
import { AppRoutes } from 'app/AppRoutes';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { withHeader } from 'app/views/components/_hoc/withHeader';
import { Location } from 'history';
import React, { useEffect, useState } from 'react';
import { useHistory, useLocation } from 'react-router';

const RentEditResource: React.FC<FloatMessageProps> = ({ floatMessage: { show } }) => {
    const [cost, setCost] = useState(0);
    const [isLoading, setIsLoading] = useState(false);
    const location: Location<{ id: string }> = useLocation();
    const history = useHistory();

    const { data, isLoading: isResourceLoading } = FETCH_API.RENT.useGetUserResource(location.state.id);

    const resourceData = data?.rawData;

    useEffect(() => {
        if (resourceData) {
            setCost(resourceData.cost);
        }
    }, [data]);

    useEffect(() => {
        setIsLoading(isResourceLoading);
    }, [isResourceLoading]);

    const handleSubmit = async () => {
        if (resourceData) {
            setIsLoading(true);
            const response = await FETCH_API.RENT.editUserResource({ ...resourceData, cost });

            if (response.success) {
                history.push(AppRoutes.services.rentReference);
            } else {
                show(MessageType.Error, response.message);
            }

            setIsLoading(false);
        }
    };

    return isLoading ? <LoadingBounce /> : (
        <Box sx={ { display: 'flex', flexDirection: 'column', padding: '6px', gap: '12px' } }>
            <Box>
                <Typography variant="body1" sx={ { fontWeight: 'bold' } }>
                    Логин
                </Typography>
                <Typography variant="body2">
                    { resourceData?.login }
                </Typography>
            </Box>
            <Box>
                <Typography variant="body1" sx={ { fontWeight: 'bold' } }>
                    Полное имя
                </Typography>
                <Typography variant="body2">
                    { resourceData?.name }
                </Typography>
            </Box>
            <Box>
                <Typography variant="body1" sx={ { fontWeight: 'bold' } }>
                    Тип лицензии
                </Typography>
                <Typography variant="body2">
                    { resourceData?.serviceTypeName }
                </Typography>
            </Box>
            <Box>
                <Typography variant="body1" sx={ { fontWeight: 'bold' } }>
                    Стоимость
                </Typography>
                <TextField
                    sx={ { width: '100%' } }
                    onChange={ e => {
                        const input = e.target.value;
                        if (/^\d*$/.test(input)) {
                            setCost(Number(input));
                        }
                    } }
                    value={ cost }
                    inputProps={ {
                        style: {
                            fontSize: '14px',
                            padding: '6px',
                            color: '#676a6c',
                            width: '100%'
                        }
                    } }
                />
            </Box>
            <Box sx={ { display: 'flex', gap: '6px' } }>
                <Button
                    variant="contained"
                    color="success"
                    onClick={ handleSubmit }
                    sx={ {
                        textTransform: 'initial',
                        color: 'white',
                        background: '#1ab394',
                        '&:hover': {
                            background: '#1ab394'
                        }
                    } }
                >
                    Сохранить
                </Button>
                <Button
                    onClick={ () => history.goBack() }
                    variant="outlined"
                    color="error"
                    sx={ {
                        textTransform: 'initial',
                    } }
                >
                    Отмена
                </Button>
            </Box>
        </Box>
    );
};

export const RentEditResourceView = withHeader({
    title: 'Редактирование ресурса',
    page: withFloatMessages(RentEditResource)
});