import { Box, TextField } from '@mui/material';
import React from 'react';
import { TextOut } from 'app/views/components/TextOut';

type LabeledTextFieldProps = {
    label: string;
    id: string;
    value: string;
    onChange: (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => void;
    variant?: 'outlined' | 'standard';
    multiline?: boolean;
    disabled?: boolean;
    type?: string;
    helperText?: string;
};

export const LabeledTextField = ({ label, multiline, helperText, ...props }: LabeledTextFieldProps) => (
    <Box
        sx={ {
            display: 'grid',
            gridTemplateColumns: '1fr 1fr',
            borderBottom: '1px solid #e7eaec',
            borderTop: '1px solid #e7eaec',
            padding: '8px',
            alignItems: 'center',
        } }
    >
        <TextOut>{ label }:</TextOut>
        <TextField
            multiline={ multiline }
            disabled={ props.disabled }
            sx={ {
                minWidth: '40%',
                ...(multiline && {
                    '& .MuiInputBase-multiline': {
                        padding: 0,
                    }
                })
            } }
            inputProps={ {
                style: {
                    fontSize: '13px',
                    padding: '6px',
                    color: '#676a6c',
                    ...(multiline && {
                        minHeight: '80px'
                    })
                },
            } }
            error={ !!helperText }
            helperText={ helperText }
            { ...props }
        />
    </Box>
);