import { Box, Button, Collapse } from '@mui/material';
import { AppRoutes } from 'app/AppRoutes';
import { userResource } from 'app/api/endpoints/rent/response';
import { AccountUserGroup, SortingKind } from 'app/common/enums';
import { COLORS } from 'app/utils';
import { TextButton } from 'app/views/components/controls/Button';
import { RentControlInformationFormView } from 'app/views/modules/Rent/tabs/RentControl/views/RentControlInformationFormView';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import cn from 'classnames';
import { memo, useState } from 'react';
import { Link } from 'react-router-dom';
import css from '../../styles.module.css';
import { Props } from './types';

export const RentControl = memo(({ rentManagement, show, isDifferentContext, isBlocked, currentUserGroups, isLoading, setIsLoading }: Props) => {
    const [isOpened, setIsOpened] = useState(false);
    const [sortingData, setSortingData] = useState<{ name: keyof userResource, sortKind: string }>({ name: 'name', sortKind: 'desc' });
    const isAllowedChange = currentUserGroups.includes(AccountUserGroup.CloudAdmin) || currentUserGroups.includes(AccountUserGroup.CloudSE);

    const getSortedUsers = (value: userResource[], name: keyof userResource, sortKind: string) => {
        return value.sort((a, b) => {
            const nameA = a[name] ? a[name] : '';
            const nameB = b[name] ? b[name] : '';

            switch (sortKind) {
                case 'asc':
                    switch (true) {
                        case nameA < nameB:
                            return -1;
                        case nameA > nameB:
                            return 1;
                        default:
                            return 0;
                    }
                case 'desc':
                    switch (true) {
                        case nameA < nameB:
                            return 1;
                        case nameA > nameB:
                            return -1;
                        default:
                            return 0;
                    }
                default:
                    return 0;
            }
        });
    };

    return (
        <div style={ { display: 'flex', flexDirection: 'column', gap: '10px' } }>
            { rentManagement.service &&
                <RentControlInformationFormView
                    isLoading={ isLoading }
                    setIsLoading={ setIsLoading }
                    currentUserGroups={ currentUserGroups }
                    isBlocked={ isBlocked }
                    isDifferentContext={ isDifferentContext }
                    show={ show }
                    service={ rentManagement.service }
                />
            }
            <Box sx={ { border: `1px solid ${ COLORS.border }`, borderRadius: '4px' } }>
                <Box sx={ { background: COLORS.backgroundElement, borderRadius: '4px' } }>
                    <TextButton onClick={ () => setIsOpened(!isOpened) }>
                        Список ресурсов пользователей
                    </TextButton>
                </Box>
                <Collapse in={ isOpened }>
                    <CommonTableWithFilter
                        className={ cn(css['custom-table'], css['resources-list'], css['resources-list--padding']) }
                        uniqueContextProviderStateId="rentManagement"
                        isResponsiveTable={ true }
                        isBorderStyled={ true }
                        tableProps={ {
                            dataset: getSortedUsers(rentManagement.users, sortingData.name, sortingData.sortKind),
                            keyFieldName: 'resourceId',
                            sorting: {
                                sortFieldName: 'name',
                                sortKind: SortingKind.Asc
                            },
                            fieldsView: {
                                login: {
                                    caption: 'Логин',
                                    fieldWidth: 'auto',
                                    isSortable: true,
                                },
                                name: {
                                    caption: 'Полное имя',
                                    fieldWidth: 'auto',
                                    isSortable: true,
                                },
                                cost: {
                                    caption: 'Стоимость',
                                    fieldWidth: 'auto',
                                    isSortable: true,
                                },
                                serviceTypeName: {
                                    caption: 'Тип лицензии',
                                    fieldWidth: 'auto',
                                    isSortable: true,
                                },
                                resourceId: {

                                    caption: '',
                                    fieldWidth: 'auto',
                                    format: id => (
                                        isAllowedChange && (
                                            <Link to={ { pathname: `${ AppRoutes.services.rent.editRentResourcePath }/${ id }`, state: { id } } }>
                                                <Button
                                                    variant="outlined"
                                                    sx={ {
                                                        borderColor: '#23c6c8',
                                                        color: '#23c6c8',
                                                        textTransform: 'initial',
                                                        '&:hover': {
                                                            background: '#23c6c8',
                                                            color: 'white',
                                                            boxShadow: '1px 1px 7px 0.1px',
                                                        }

                                                    } }
                                                >
                                                    Редактировать
                                                </Button>
                                            </Link>
                                        )
                                    )
                                }
                            },
                        } }
                        onDataSelect={ ({ sortingData }) => sortingData && setSortingData({ name: sortingData?.fieldName as keyof userResource, sortKind: sortingData.sortKind ? 'asc' : 'desc' }) }
                    />
                </Collapse>
            </Box>
        </div>
    );
});