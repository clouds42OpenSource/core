import { Box, Button, Checkbox, Collapse, Typography } from '@mui/material';
import { DateTimePicker, LocalizationProvider } from '@mui/x-date-pickers';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { AppRoutes } from 'app/AppRoutes';
import ruLocale from 'date-fns/locale/ru';
import dayjs from 'dayjs';
import { useHistory } from 'react-router';
import React, { useState } from 'react';

import { serviceResponseDto } from 'app/api/endpoints/rent/response';
import { FETCH_API } from 'app/api/useFetchApi';
import { COLORS, DateUtility } from 'app/utils';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { Dialog } from 'app/views/components/controls/Dialog';
import { LabeledTextField } from 'app/views/modules/Rent/tabs/RentControl/views/RentControlInformationFormView/components/LabeledTextField';
import { AccountUserGroup } from 'app/common/enums';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { RentControlFormProps } from 'app/views/modules/Rent/tabs/RentControl/types';
import { TextButton } from 'app/views/components/controls/Button';

type RentControlFormFields = {
    costOfWebLicense: number;
    costOfRdpLicense: number;
    limitOnFreeCreationDb: number;
    costOfCreatingDbOverFreeLimit: number;
    serverDatabasePlacementCost: number;
    expireDate: string;
    additionalResourceCost: number;
    additionalResourceName: string;
};

const RentControlInformationForm = ({ reduxForm, currentUserGroups, service, show, isDifferentContext, isBlocked, isLoading, setIsLoading }: RentControlFormProps & ReduxFormProps<RentControlFormFields>) => {
    const [isOpened, setIsOpened] = useState(true);
    const [isDemoProlong, setIsDemoProlong] = useState(false);
    const [demoData, setDemoData] = useState<serviceResponseDto | null>(null);
    const [demoExpiredDate, setDemoExpiredDate] = useState('');
    const [additionalResourceNameError, setAdditionalResourceNameError] = useState('');

    const isAdmin = currentUserGroups.includes(AccountUserGroup.CloudAdmin);
    const isAllowedChangeDemo = currentUserGroups.includes(AccountUserGroup.AccountSaleManager) || currentUserGroups.includes(AccountUserGroup.CloudAdmin);

    const history = useHistory();
    const initReduxForm = (): RentControlFormFields => {
        return {
            costOfWebLicense: service.costOfWebLicense,
            costOfRdpLicense: service.costOfRdpLicense,
            limitOnFreeCreationDb: service.limitOnFreeCreationDb,
            costOfCreatingDbOverFreeLimit: service.costOfCreatingDbOverFreeLimit,
            serverDatabasePlacementCost: service.serverDatabasePlacementCost,
            additionalResourceCost: service.additionalResourceCost ? service.additionalResourceCost : 0,
            additionalResourceName: service.additionalResourceName ? service.additionalResourceName : '',
            expireDate: service.expireDate,
        };
    };

    const handleOpenDemoModal = async () => {
        setIsLoading(true);
        const response = await FETCH_API.RENT.getDemoPeriod();
        setIsLoading(false);
        if (response.success) {
            setDemoData(response.data);
            setIsDemoProlong(true);
            if (response.data?.isDemoPeriod) {
                setDemoExpiredDate(response.data.expireDate);
            }
        }
    };

    const handleCloseDemoModal = () => {
        setIsDemoProlong(false);
        setDemoData(null);
    };

    const handleDemoExpiredDateChange = (date: Date | null) => {
        if (date) {
            setDemoExpiredDate(DateUtility.dateToIsoDateString(date));
        }
    };

    reduxForm.setInitializeFormDataAction(initReduxForm);

    const reduxFormFields = reduxForm.getReduxFormFields(false);

    const handleChange = (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        const { id, value } = event.target;

        if (reduxFormFields.additionalResourceName !== '') {
            setAdditionalResourceNameError('');
        }

        reduxForm.updateReduxFormFields({ [id]: value });
    };

    const handleDateTimePickerChange = (date: Date | null) => {
        if (date) {
            reduxForm.updateReduxFormFields({ expireDate: dayjs(date).format('YYYY-MM-DDTHH:mm:ss') });
        }
    };

    const handleChangeDemo = async () => {
        if (demoExpiredDate && demoData) {
            setIsLoading(true);
            const response = await FETCH_API.RENT.editDemoPeriod({ ...demoData, expireDate: demoExpiredDate });

            if (response.success) {
                show(MessageType.Success, 'Успешно');
                history.push(AppRoutes.services.rentReference);
            } else {
                setIsLoading(false);
                show(MessageType.Error, response.message);
            }
        }
    };

    const handleSubmit = async () => {
        if (!reduxFormFields.additionalResourceName && !!reduxFormFields.additionalResourceCost) {

            setAdditionalResourceNameError('Обязательное поле');
            return;
        }

        setIsLoading(true);

        const response = await FETCH_API.RENT.editRentManagement({ ...service, ...reduxFormFields });

        if (response.success) {
            show(MessageType.Success, 'Успешно');
            history.push(AppRoutes.services.rentReference);
        } else {
            show(MessageType.Error, response.message);
        }

        setIsLoading(false);
    };

    return (
        <Box sx={ { border: '1px solid #ddd', borderRadius: '4px' } }>
            { isLoading && <LoadingBounce /> }
            <Box sx={ { background: COLORS.backgroundElement, borderRadius: '4px' } }>
                <TextButton onClick={ () => setIsOpened(!isOpened) }>
                    Информация
                </TextButton>
            </Box>
            <Collapse in={ isOpened }>
                <LabeledTextField
                    label="Стоимость Web лицензии для аккаунта"
                    id="costOfWebLicense"
                    disabled={ !isAdmin }
                    value={ reduxFormFields.costOfWebLicense.toString() }
                    onChange={ handleChange }
                />
                <LabeledTextField
                    label="Стоимость Rdp лицензии для аккаунта"
                    id="costOfRdpLicense"
                    disabled={ !isAdmin }
                    value={ reduxFormFields.costOfRdpLicense.toString() }
                    onChange={ handleChange }
                />
                <LabeledTextField
                    label="Количество баз бесплатно, шт"
                    id="limitOnFreeCreationDb"
                    disabled={ !isAdmin }
                    value={ reduxFormFields.limitOnFreeCreationDb.toString() }
                    onChange={ handleChange }
                />
                <LabeledTextField
                    label="Стоимость 1 базы, свыше бесплатного лимита"
                    id="costOfCreatingDbOverFreeLimit"
                    disabled={ !isAdmin }
                    value={ reduxFormFields.costOfCreatingDbOverFreeLimit.toString() }
                    onChange={ handleChange }
                />
                <LabeledTextField
                    label="Стоимость размещения 1 серверной базы"
                    id="serverDatabasePlacementCost"
                    disabled={ !isAdmin }
                    value={ reduxFormFields.serverDatabasePlacementCost.toString() }
                    onChange={ handleChange }
                />
                { service.accountIsVip && <LabeledTextField
                    label="Дополнительные ресурсы"
                    id="additionalResourceName"
                    disabled={ !isAdmin }
                    value={ reduxFormFields.additionalResourceName.toString() }
                    onChange={ handleChange }
                    helperText={ additionalResourceNameError }
                /> }
                { service.accountIsVip && <LabeledTextField
                    type="number"
                    label="Стоимость дополнительные ресурсов"
                    id="additionalResourceCost"
                    disabled={ !isAdmin }
                    value={ reduxFormFields.additionalResourceCost.toString() }
                    onChange={ handleChange }
                /> }
                <Box sx={ { display: 'grid', alignItems: 'center', gridTemplateColumns: '1fr 1fr', borderBottom: '1px solid #e7eaec', padding: '8px' } }>
                    <Typography variant="body2">
                        Оплачено до:
                    </Typography>
                    <LocalizationProvider dateAdapter={ AdapterDateFns } adapterLocale={ ruLocale }>
                        <DateTimePicker
                            sx={ {
                                '& input': {
                                    padding: '6px',
                                    color: '#676a6c'
                                }
                            } }
                            disabled={ !isAdmin }
                            minDate={ isAdmin ? undefined : new Date() }
                            value={ new Date(reduxFormFields.expireDate) }
                            onChange={ handleDateTimePickerChange }
                        />
                    </LocalizationProvider>
                </Box>
                <Box sx={ { display: 'grid', alignItems: 'center', gridTemplateColumns: '1fr 1fr', justifyItems: 'flex-start', padding: '8px' } }>
                    <Typography variant="body2">
                        Блокировано:
                    </Typography>
                    <Checkbox
                        disabled={ true }
                        sx={ { padding: 0 } }
                        checked={ isBlocked }
                        inputProps={ { 'aria-label': 'controlled' } }
                    />
                </Box>
                <Box>
                    { isAdmin &&
                        <Button
                            variant="contained"
                            onClick={ handleSubmit }
                            sx={ {
                                textTransform: 'initial',
                                color: 'white',
                                background: '#1ab394',
                                '&:hover': {
                                    background: '#1ab394'
                                } } }
                        >
                            Сохранить изменения
                        </Button> }
                    { isDifferentContext && service.isDemoPeriod && isAllowedChangeDemo &&
                        <Button
                            onClick={ handleOpenDemoModal }
                            variant="contained"
                            sx={ {
                                textTransform: 'initial',
                                color: 'white',
                                background: '#23c6c8',
                                marginLeft: '6px',
                                '&:hover': {
                                    background: '#23c6c8',
                                }, } }
                        >
                            Продлить демо период
                        </Button> }
                </Box>
            </Collapse>
            <Dialog
                isOpen={ isDemoProlong }
                dialogWidth="xs"
                dialogVerticalAlign="center"
                titleFontSize={ 12 }
                isTitleSmall={ true }
                title="Продление демо периода"
                onCancelClick={ handleCloseDemoModal }
                buttons={
                    [
                        {
                            content: 'Закрыть',
                            kind: 'default',
                            variant: 'contained',
                            onClick: handleCloseDemoModal
                        },
                        {
                            content: 'Продлить',
                            kind: 'primary',
                            variant: 'contained',
                            onClick: handleChangeDemo
                        }
                    ]
                }
            >
                <Box display="flex" flexDirection="column" alignItems="center" justifyContent="center" gap="6px">
                    <Typography variant="body2">
                        Выберите дату окончания демо периода
                    </Typography>
                    <LocalizationProvider dateAdapter={ AdapterDateFns } adapterLocale={ ruLocale }>
                        <DateTimePicker
                            sx={ {
                                '& input': {
                                    padding: '6px'
                                }
                            } }
                            maxDate={ isAdmin ? undefined : new Date(service.maxDemoExpiredDate) }
                            value={ new Date(demoExpiredDate) }
                            onChange={ handleDemoExpiredDateChange }
                        />
                    </LocalizationProvider>
                </Box>
            </Dialog>
        </Box>
    );
};

export const RentControlInformationFormView = withReduxForm(RentControlInformationForm, {
    reduxFormName: ' AddModifyDbTemplate_ReduxForm',
    resetOnUnmount: true
});