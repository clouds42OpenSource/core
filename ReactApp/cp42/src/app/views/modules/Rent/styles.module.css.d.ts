declare const styles: {
    readonly 'custom-table': string;
    readonly 'resources-list': string;
    readonly 'services-list': string;
    readonly 'resources-list--padding': string

};
export = styles;