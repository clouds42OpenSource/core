import { Box, Button, Typography } from '@mui/material';
import cn from 'classnames';
import dayjs from 'dayjs';
import 'dayjs/locale/ru';
import React, { useCallback, useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import { useHistory } from 'react-router';
import { Link } from 'react-router-dom';

import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

import { AppRoutes } from 'app/AppRoutes';
import { apiHost, templateErrorText } from 'app/api';
import { GetCurrentSessionsSettingsResponse } from 'app/api/endpoints/global/response';
import { NavMenuItemInfo } from 'app/api/endpoints/global/response/getNavMenu';
import { getLocalizationsResponseDto } from 'app/api/endpoints/locale/response/getLocaleResponseDto';
import { getRentManagementResponseDto, getRentResponseDto } from 'app/api/endpoints/rent/response';
import { FETCH_API } from 'app/api/useFetchApi';
import { AccountUserGroup } from 'app/common/enums';
import { localStorageHelper } from 'app/common/helpers/LocalStorageHelper';
import { AvailabilityInfoDbListThunk } from 'app/modules/infoDbList/store/thunks/AvailabilityInfoDbListThunk';
import { AppReduxStoreState } from 'app/redux/types';
import { COLORS } from 'app/utils';
import { ERefreshId, ETourId } from 'app/views/Layout/ProjectTour/enums';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { TextOut } from 'app/views/components/TextOut';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { TabsControl } from 'app/views/components/controls/TabsControl';
import { CollapseContentViews } from 'app/views/modules/AccountManagement/InformationBases/views/CollapseContent';
import { RentControl } from 'app/views/modules/Rent/tabs/RentControl';
import { RentUsers } from 'app/views/modules/Rent/tabs/RentUsers';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { InputAccountId } from 'app/web/api/InfoDbProxy/request-dto/InputAccountId';
import { GetAvailabilityInfoDb } from 'app/web/api/InfoDbProxy/responce-dto/GetAvailabilityInfoDb';

import css from './styles.module.css';

type StateProps = {
    availability: GetAvailabilityInfoDb;
    settings: GetCurrentSessionsSettingsResponse;
    hasSettingsReceived: boolean;
    rentNavItem?: NavMenuItemInfo;
    currentUserGroups: AccountUserGroup[];
};

type DispatchProps = {
    dispatchAvailabilityInfoDbThunk: (args: IForceThunkParam & InputAccountId) => void;
};

type TCurrentService = {
    name: string;
    count?: number;
    price?: number;
    sponsored?: number;
    sponsorship?: number;
    sum: number;
};

dayjs.locale('ru');

const RentView: React.FC<StateProps & DispatchProps & FloatMessageProps> = ({ availability, settings, hasSettingsReceived, rentNavItem, currentUserGroups, dispatchAvailabilityInfoDbThunk, floatMessage }) => {
    const history = useHistory();
    const [isLoading, setIsLoading] = useState(false);
    const [activeTabIndex, setActiveTabIndex] = useState(0);
    const [rentManagementData, setRentManagementData] = useState<getRentManagementResponseDto | null>(null);

    const { data, refreshData, isLoading: isRentLoading } = FETCH_API.RENT.useGetRent();
    const { data: localizations, isLoading: isLocalizationsLoading } = FETCH_API.LOCALE.useGetLocaleLocalizations();

    const isDifferentContext = hasSettingsReceived && settings.currentContextInfo.isDifferentContextAccount;

    const caption = rentNavItem?.children.find((({ href }) => href === AppRoutes.services.rentReference))?.caption || '';

    const getLocalizationsNameByKey = (localizationsResponseData: getLocalizationsResponseDto, keyNumber: number) =>
        localizationsResponseData.find(({ key }) => key === keyNumber)?.value;

    const rentData = data?.rawData;
    const localizationsData = localizations?.rawData || [];

    const accountId = localStorageHelper.getContextAccountId()!;
    const accountUserId = localStorageHelper.getUserId();

    const handleSetIsLoading = (value: boolean) => {
        setIsLoading(value);
    };

    const handleGetRentManagement = async () => {
        setIsLoading(true);
        const response = await FETCH_API.RENT.getRentManagement();
        setIsLoading(false);

        if (response.success) {
            setRentManagementData(response.data);
        } else {
            floatMessage.show(MessageType.Error, response.message ?? templateErrorText);
        }
    };

    useEffect(() => {
        dispatchAvailabilityInfoDbThunk({
            accountId,
            accountUserId: accountUserId || '',
        });
    }, []);

    useEffect(() => {
        if (isRentLoading || isLocalizationsLoading) {
            setIsLoading(true);
        } else if (!rentManagementData && (currentUserGroups.includes(AccountUserGroup.CloudAdmin) ||
            currentUserGroups.includes(AccountUserGroup.AccountSaleManager) ||
            currentUserGroups.includes(AccountUserGroup.CloudSE) ||
            currentUserGroups.includes(AccountUserGroup.Hotline))) {
            void handleGetRentManagement();
        } else {
            setIsLoading(false);
        }
    }, [isRentLoading, isLocalizationsLoading]);

    const getCurrentServices = (rentResponseData: getRentResponseDto) => {
        const {
            myDatabasesServiceData,
            additionalAccountResources,
            sponsoredStandartCount,
            sponsoredWebCount,
            sponsorshipStandartCount,
            sponsorshipWebCount
        } = rentResponseData;

        const currentService: TCurrentService[] = [
            {
                name: 'Подключение "Стандарт" (удаленный рабочий стол + WEB)',
                count: rentResponseData.standartResourcesCount,
                price: (rentResponseData.rateWeb + rentResponseData.rateRdp),
                sponsored: sponsoredStandartCount,
                sponsorship: sponsorshipStandartCount,
                sum: rentResponseData.monthlyCostStandart
            },
            {
                name: `Подключение "WEB" (${ getLocalizationsNameByKey(localizationsData, 5) })`,
                count: rentResponseData.webResourcesCount,
                price: rentResponseData.rateWeb,
                sponsored: sponsoredWebCount,
                sponsorship: sponsorshipWebCount,
                sum: rentResponseData.monthlyCostWeb
            },
        ];

        if (myDatabasesServiceData.countDatabasesOverLimit) {
            currentService.push({
                name: 'Количество баз, свыше 10',
                count: myDatabasesServiceData.countDatabasesOverLimit,
                price: myDatabasesServiceData.databasePlacementCost,
                sum: myDatabasesServiceData.totalAmountForDatabases
            });
        }

        if (additionalAccountResources.needShowAdditionalResources) {
            currentService.push({
                name: additionalAccountResources.additionalResourcesName,
                count: 1,
                price: additionalAccountResources.additionalResourcesCost,
                sum: additionalAccountResources.additionalResourcesCost
            });
        }

        currentService.push({
            name: 'Итого в рамках текущего периода',
            sum: rentResponseData.monthlyCostTotal
        });

        return currentService;
    };

    const getImageUrl = (value?: string) => {
        const host = apiHost('/img/services/');

        switch (value) {
            case 'Казахстан':
                return `${ host }/1c_my_enterprise.png`;
            case 'Россия':
                return `${ host }/1c_my_enterprise.png`;
            case 'Украина':
                return `${ host }/bas_icon.png`;
            default:
                return `${ host }/1c_my_enterprise.png`;
        }
    };

    const currentServices = rentData ? getCurrentServices(rentData) : [];
    const sponsored = currentServices.reduce((prev, curr) => prev + (curr.sponsored ?? 0), 0);
    const sponsorship = currentServices.reduce((prev, curr) => prev + (curr.sponsorship ?? 0), 0);

    const handleTurnRent = useCallback(async () => {
        setIsLoading(true);
        const response = await FETCH_API.RENT.turnOnRent();

        if (response.success) {
            history.push(AppRoutes.accountManagement.createAccountDatabase.createDb);
        } else {
            floatMessage.show(MessageType.Error, response.message);
        }

        setIsLoading(false);
    }, [floatMessage, history]);

    useEffect(() => {
        if (rentData && rentData.locale.currency !== 'руб' && (rentData.serviceStatus === null || rentData.expireDate === null)) {
            void handleTurnRent();
        }
    }, [handleTurnRent, rentData]);

    if (isLoading && !data) {
        return <LoadingBounce />;
    }

    if (rentData && !rentData.serviceStatus) {
        return (
            <div>
                <Box>
                    <Helmet>
                        <title>Сервис &quot;{ caption }&quot;</title>
                    </Helmet>
                    <Box display="grid" gridTemplateColumns="200px 1fr" alignItems="center">
                        <img src={ getImageUrl(rentData?.locale.country) } alt="country" />
                        <Box>
                            <Box>
                                <Typography variant="caption">
                                    Сервис Аренда 1С не активен. Для активации тестового режима подключите пользователя или
                                    <Link style={ { color: COLORS.link } } to={ AppRoutes.accountManagement.createAccountDatabase.createDb }> создайте информационную базу.</Link>
                                </Typography>
                            </Box>
                            <Box>
                                <Typography variant="caption">
                                    Стоимость Web за пользователя:
                                    <a style={ { color: '#337ab7' } } href={ `https://42clouds.com/${ rentData.locale.name }/services/rates.html` }>{ `${ rentData.rateWeb } ${ rentData.locale.currency }/мес` }</a>
                                </Typography>
                            </Box>
                            <Box>
                                <Typography variant="caption">
                                    Стоимость Стандарт за пользователя:
                                    <a style={ { color: '#337ab7' } } href={ `https://42clouds.com/${ rentData.locale.name }/services/rates.html` }>{ `${ rentData.rateWeb + rentData.rateRdp } ${ rentData.locale.currency }/мес` }</a>
                                </Typography>
                            </Box>
                        </Box>
                    </Box>
                    <Button
                        variant="contained"
                        onClick={ handleTurnRent }
                        sx={ {
                            background: '#23c6c8',
                            color: '#FFFFFF',
                            minHeight: '60px',
                            minWidth: '220px',
                            width: '35%',
                            marginLeft: '20%',
                            marginTop: '6px',
                            textTransform: 'none',
                            '&:hover': {
                                background: '#23c6c8',
                            }
                        } }
                    >
                        Активировать пробную версию &quot;{ getLocalizationsNameByKey(localizationsData, 0) }&quot;
                    </Button>
                </Box>
            </div>
        );
    }

    return (
        <>
            { isLoading && <LoadingBounce /> }
            <Helmet>
                <title>Сервис &quot;{ caption }&quot;</title>
            </Helmet>
            <TabsControl
                headerContent={
                    <Box display="flex" flexDirection="column" gap="10px">
                        <Box
                            display="grid"
                            gridTemplateColumns="200px 1fr"
                            sx={ {
                                '@media (max-width: 800px)': {
                                    gridTemplateColumns: 'auto 1fr',
                                }
                            } }
                            alignItems="center"
                        >
                            <img src={ getImageUrl(rentData?.locale.country) } alt="country" />
                            <TextOut inDiv={ true } fontSize={ 13 } fontWeight={ 400 }>
                                Вы можете подключить своих пользователей к сервису «{ getLocalizationsNameByKey(localizationsData, 0) }».
                                В рамках доступных лицензий можно переназначать своих пользователей. В момент очередного автоматического продления сервиса неиспользованные лицензии
                                удаляются
                            </TextOut>
                        </Box>
                        { !activeTabIndex && <CollapseContentViews
                            isRent={ true }
                            localeTitle={ getLocalizationsNameByKey(localizationsData, 3) }
                            availability={ availability }
                        /> }
                        { !activeTabIndex && rentData?.serviceStatus.promisedPaymentIsActive && !rentData?.serviceStatus.serviceIsLocked && (
                            <Box sx={ { color: '#31708f', background: '#d9edf7', borderRadius: '4px', border: '1px solid #bce8f1', padding: '15px', fontSize: '13px', margin: '6px 0' } }>
                                <b>Внимание </b> У Вас активирован обещанный платеж на сумму { rentData?.serviceStatus.promisedPaymentSum } { rentData.locale.currency }.
                                Пожалуйста, погасите задолженность до { dayjs(rentData?.serviceStatus.promisedPaymentExpireDate).format('D MMMM YYYY [г.]') } во избежание блокировки сервиса
                            </Box>
                        ) }
                        { !activeTabIndex && rentData?.serviceStatus.serviceIsLocked && (
                            <Box sx={ { color: '#a94442', background: '#f2dede', borderRadius: '4px', border: '1px solid #ebccd1', padding: '15px', fontSize: '13px' } }>
                                <b>Внимание! </b> { rentData?.serviceStatus.serviceLockReason } Пожалуйста, <Link style={ { color: '#337ab7' } } to={ AppRoutes.accountManagement.balanceManagement }> оплатите сервис</Link>
                            </Box>
                        ) }
                        { !activeTabIndex && (
                            <Box sx={ { background: '#f5f5f5', border: '1px solid #e3e3e3', borderRadius: '4px', padding: '19px' } }>
                                <CommonTableWithFilter
                                    refreshId={ ERefreshId.rentInfo }
                                    tourId={ ETourId.rentPageInfo }
                                    className={ cn(css['custom-table'], css['services-list']) }
                                    uniqueContextProviderStateId="asdf"
                                    isResponsiveTable={ true }
                                    isBorderStyled={ true }
                                    tableProps={ {
                                        dataset: currentServices,
                                        keyFieldName: 'name',
                                        fieldsView:
                                            sponsored + sponsorship === 0
                                                ? {
                                                    name: {
                                                        caption: 'Услуги',
                                                        fieldWidth: '50%',
                                                    },
                                                    count: {
                                                        caption: 'Количество',
                                                        fieldWidth: 'auto'
                                                    },
                                                    price: {
                                                        caption: `Стоимость, ${ rentData?.locale.currency }/мес`,
                                                        fieldWidth: 'auto'
                                                    },
                                                    sum: {
                                                        caption: `Всего, ${ rentData?.locale.currency }/мес`,
                                                        fieldWidth: 'auto'
                                                    }
                                                }
                                                : sponsored > 0 && sponsorship > 0
                                                    ? {
                                                        name: {
                                                            caption: 'Услуги',
                                                            fieldWidth: '50%',
                                                        },
                                                        count: {
                                                            caption: 'Количество',
                                                            fieldWidth: 'auto'
                                                        },
                                                        sponsored: {
                                                            caption: 'Я спонсирую'
                                                        },
                                                        sponsorship: {
                                                            caption: 'Меня спонсируют'
                                                        },
                                                        price: {
                                                            caption: `Стоимость, ${ rentData?.locale.currency }/мес`,
                                                            fieldWidth: 'auto'
                                                        },
                                                        sum: {
                                                            caption: `Всего, ${ rentData?.locale.currency }/мес`,
                                                            fieldWidth: 'auto'
                                                        }
                                                    }
                                                    : sponsored > 0
                                                        ? {
                                                            name: {
                                                                caption: 'Услуги',
                                                                fieldWidth: '50%',
                                                            },
                                                            count: {
                                                                caption: 'Количество',
                                                                fieldWidth: 'auto'
                                                            },
                                                            sponsored: {
                                                                caption: 'Я спонсирую'
                                                            },
                                                            price: {
                                                                caption: `Стоимость, ${ rentData?.locale.currency }/мес`,
                                                                fieldWidth: 'auto'
                                                            },
                                                            sum: {
                                                                caption: `Всего, ${ rentData?.locale.currency }/мес`,
                                                                fieldWidth: 'auto'
                                                            }
                                                        }
                                                        : {
                                                            name: {
                                                                caption: 'Услуги',
                                                                fieldWidth: '50%',
                                                            },
                                                            count: {
                                                                caption: 'Количество',
                                                                fieldWidth: 'auto'
                                                            },
                                                            sponsorship: {
                                                                caption: 'Меня спонсируют'
                                                            },
                                                            price: {
                                                                caption: `Стоимость, ${ rentData?.locale.currency }/мес`,
                                                                fieldWidth: 'auto'
                                                            },
                                                            sum: {
                                                                caption: `Всего, ${ rentData?.locale.currency }/мес`,
                                                                fieldWidth: 'auto'
                                                            }
                                                        },
                                    } }
                                />
                                <TextOut>
                                    Использование сервиса возможно до: { rentData?.serviceStatus.serviceIsLocked
                                        ? dayjs(rentData?.expireDate).format('D MMMM YYYY [г.]')
                                        : rentData?.serviceStatus.promisedPaymentIsActive
                                            ? dayjs(rentData?.serviceStatus.promisedPaymentExpireDate).format('D MMMM YYYY [г.]')
                                            : dayjs(rentData?.expireDate).format('D MMMM YYYY [г.]') }
                                </TextOut>
                            </Box>
                        ) }
                    </Box>
                }
                activeTabIndex={ activeTabIndex }
                onActiveTabIndexChanged={ setActiveTabIndex }
                headers={
                    [{
                        title: 'Пользователи',
                        isVisible: !!rentData,
                    }, {
                        title: 'Управление',
                        isVisible: ((currentUserGroups.includes(AccountUserGroup.CloudAdmin) ||
                            currentUserGroups.includes(AccountUserGroup.Hotline) ||
                            currentUserGroups.includes(AccountUserGroup.AccountSaleManager) ||
                            currentUserGroups.includes(AccountUserGroup.CloudSE))) &&
                            !!rentManagementData
                    }] }
            >
                { rentData &&
                    <RentUsers
                        localizationsData={ localizationsData }
                        rentLocale={ rentData.locale }
                        setIsLoading={ handleSetIsLoading }
                        isLoading={ isLoading }
                        currentUserGroups={ currentUserGroups }
                        show={ floatMessage.show }
                        refreshData={ refreshData }
                        freeWebResources={ rentData.freeWebResources }
                        freeRdpResources={ rentData.freeRdpResources }
                        userList={ rentData.userList }
                        expireDate={ rentData?.expireDate }
                    />
                }
                { rentManagementData &&
                    <RentControl
                        setIsLoading={ handleSetIsLoading }
                        isLoading={ isLoading }
                        isBlocked={ !!rentData?.serviceStatus.serviceIsLocked }
                        currentUserGroups={ currentUserGroups }
                        isDifferentContext={ isDifferentContext }
                        show={ floatMessage.show }
                        rentManagement={ rentManagementData }
                    />
                }
            </TabsControl>
        </>
    );
};

const RentViewConnected = connect<StateProps, DispatchProps, object, AppReduxStoreState>(
    state => {
        const infoDbListState = state.InfoDbListState;
        const availability = infoDbListState.AvailabilityInfoDb;
        const { settings, hasSuccessFor } = state.Global.getCurrentSessionSettingsReducer;
        const rentNavItem = state.Global.getNavMenuReducer.data?.rawData.leftSideMenu.find(item => item.categoryKey === 'rent_1c');
        const { hasSettingsReceived } = hasSuccessFor;
        const currentUserGroups = hasSettingsReceived ? settings.currentContextInfo.currentUserGroups : [];

        return {
            availability,
            settings,
            hasSettingsReceived,
            rentNavItem,
            currentUserGroups
        };
    },
    {
        dispatchAvailabilityInfoDbThunk: AvailabilityInfoDbListThunk.invoke,
    }
)(RentView);

export const RentViewComponent = withFloatMessages(RentViewConnected);