import { ButtonsToolbarListType } from 'app/views/components/RichTextEditor/types';

export const buttonList: ButtonsToolbarListType = [
    ['bold', 'underline', 'removeFormat'],
    ['font'],
    ['fontColor'],
    ['list'],
    ['table'],
    ['link', 'image', 'video'],
    ['fullScreen', 'codeView'],
];