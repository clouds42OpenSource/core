import { TUserItem } from 'app/api/endpoints/accountUsers/response/TGetAllUserList';
import { NotificationSendTypeEnum } from 'app/api/endpoints/notification/enum';

export type TNotificationForm = {
    searchUser: string;
    themes: string;
    subject: string;
    body: string;
    logins: TUserItem[];
    isCritical: boolean;
    notificationSendType: NotificationSendTypeEnum;
};