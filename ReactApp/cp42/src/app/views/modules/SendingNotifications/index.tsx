import { Autocomplete, FormControlLabel, Radio, RadioGroup, TextField } from '@mui/material';
import Checkbox from '@mui/material/Checkbox';
import { Box } from '@mui/system';
import { TUserItem } from 'app/api/endpoints/accountUsers/response/TGetAllUserList';
import { NotificationSendTypeEnum } from 'app/api/endpoints/notification/enum';
import { TPostCreateNotification } from 'app/api/endpoints/notification/request';
import { EStateNotification } from 'app/api/endpoints/notification/response';
import { FETCH_API } from 'app/api/useFetchApi';
import { EMessageType, useFloatMessages } from 'app/hooks';
import { withHeader } from 'app/views/components/_hoc/withHeader';
import { SuccessButton } from 'app/views/components/controls/Button';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { TextOut } from 'app/views/components/TextOut';
import { TNotificationForm } from 'app/views/modules/SendingNotifications/types';
import { useFormik } from 'formik';
import React, { useCallback, useEffect, useState } from 'react';

const { getAllUserList } = FETCH_API.ACCOUNT_USERS;
const { createNotification } = FETCH_API.NOTIFICATIONS;

const initialValues: TNotificationForm = {
    searchUser: '',
    themes: '',
    subject: '',
    body: '',
    notificationSendType: 1,
    logins: [],
    isCritical: false
};

const SendingNotifications = () => {
    const { show } = useFloatMessages();

    const [allUsers, setAllUsers] = useState<TUserItem[]>([]);

    const { values, handleChange, resetForm, setFieldValue, submitForm } = useFormik<TNotificationForm>({
        initialValues,
        onSubmit: async () => {
            const dataToSend: TPostCreateNotification = {
                message: values.body,
                state: values.isCritical ? EStateNotification.critical : EStateNotification.normal,
                header: values.themes,
                ignoreMailSending: true,
                subject: values.subject,
                notificationSendType: values.notificationSendType
            };

            if (Number(values.notificationSendType) === NotificationSendTypeEnum.ByUsersIdsList) {
                dataToSend.userIds = values.logins.map(({ id }) => id);
            }

            const { success, message } = await createNotification(dataToSend);

            if (success) {
                resetForm();
                show(EMessageType.success, 'Уведомление успешно отправлено');
            } else {
                show(EMessageType.error, message);
            }
        }
    });

    const searchUser = useCallback(async () => {
        try {
            const response = await getAllUserList(values.searchUser);

            if (response.data) {
                setAllUsers(response.data.records);
            }
        } catch (err) {
            show(EMessageType.error, 'При поиске пользователей произошла ошибка. Пожалуйста, повторите позже или обратитесь в службу поддержки');
        }
    }, [show, values.searchUser]);

    useEffect(() => {
        void searchUser();
    }, [searchUser, values.searchUser]);

    const searchUserOnChange = (_: NonNullable<unknown>, newValue: TUserItem[]) => {
        if (newValue) {
            setFieldValue('logins', newValue);
        }
    };

    return (
        <>
            <Box display="flex" flexDirection="column" gap="16px" mb="16px" maxWidth="900px">
                <FormAndLabel label="Введите тему письма">
                    <TextField
                        name="themes"
                        value={ values.themes }
                        onChange={ handleChange }
                        hiddenLabel={ true }
                        fullWidth={ true }
                    />
                </FormAndLabel>
                <FormAndLabel label="Введите заголовок письма">
                    <TextField
                        name="subject"
                        value={ values.subject }
                        onChange={ handleChange }
                        hiddenLabel={ true }
                        fullWidth={ true }
                    />
                </FormAndLabel>
                <FormAndLabel label="Тело письма">
                    <TextField
                        name="body"
                        value={ values.body }
                        onChange={ handleChange }
                        hiddenLabel={ true }
                        fullWidth={ true }
                        multiline={ true }
                        rows={ 6 }
                    />
                </FormAndLabel>
                <FormAndLabel label="Получатели">
                    <RadioGroup
                        aria-label="notificationSendType"
                        name="notificationSendType"
                        value={ values.notificationSendType }
                        onChange={ handleChange }
                    >
                        <FormControlLabel
                            value={ NotificationSendTypeEnum.OnlyActiveUsers }
                            control={ <Radio /> }
                            label="Отправить всем активным пользователям"
                        />
                        {/* <FormControlLabel
                            value={ NotificationSendTypeEnum.UsersWithConnectedService }
                            control={ <Radio /> }
                            label="Пользователям с подключенным сервисом (к примеру аренда)"
                        /> */}
                        <FormControlLabel
                            value={ NotificationSendTypeEnum.OnlyAccountAdminsOrManagers }
                            control={ <Radio /> }
                            label="Администраторам/менеджерам аккаунтов"
                        />
                        <FormControlLabel
                            value={ NotificationSendTypeEnum.ByUsersIdsList }
                            control={ <Radio /> }
                            label="Указать список логинов пользователей которым надо разослать уведомления"
                        />
                    </RadioGroup>
                </FormAndLabel>
                { Number(values.notificationSendType) === NotificationSendTypeEnum.ByUsersIdsList && (
                    <FormAndLabel label="Поиск пользователей">
                        <Autocomplete
                            disablePortal={ true }
                            multiple={ true }
                            autoComplete={ false }
                            onChange={ searchUserOnChange }
                            value={ values.logins }
                            disableCloseOnSelect={ true }
                            options={ allUsers }
                            getOptionLabel={ value => value.login }
                            fullWidth={ true }
                            size="small"
                            renderInput={ params => <TextField name="searchUser" value={ values.searchUser } onChange={ handleChange } hiddenLabel={ true } { ...params } /> }
                        />
                    </FormAndLabel>
                )}
                <FormControlLabel
                    name="isCritical"
                    control={
                        <Checkbox
                            checked={ values.isCritical }
                            onChange={ handleChange }
                            size="small"
                        />
                    }
                    label={
                        <TextOut>Критическое уведомление</TextOut>
                    }
                    sx={ {
                        maxWidth: 'max-content'
                    } }
                />
            </Box>
            <SuccessButton onClick={ submitForm }>Отправить</SuccessButton>
        </>
    );
};

export const SendingNotificationsView = withHeader({
    page: SendingNotifications,
    title: 'Рассылка уведомлений'
});