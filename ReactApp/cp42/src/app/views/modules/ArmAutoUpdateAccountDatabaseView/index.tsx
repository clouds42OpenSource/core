import { TabsControl } from 'app/views/components/controls/TabsControl';
import { withHeader } from 'app/views/components/_hoc/withHeader';
import { ArmAutoUpdateAccountDatabaseTabEnumType } from 'app/views/modules/ArmAutoUpdateAccountDatabaseView/types';
import { ConnectedDatabasesOnAutoUpdateView, DatabasesInAutoUpdateQueueView, PerformedAutoUpdateForDatabasesView } from 'app/views/modules/ArmAutoUpdateAccountDatabaseView/views';
import { IndividualTab } from 'app/views/modules/ArmAutoUpdateAccountDatabaseView/views/IndividualTab';
import { Component } from 'react';

type ArmAutoUpdateAccountDatabaseState = {
    /**
     * Index активного таба
     */
    activeTabIndex: ArmAutoUpdateAccountDatabaseTabEnumType;
};

class ArmAutoUpdateAccountDatabaseClass extends Component<{}, ArmAutoUpdateAccountDatabaseState> {
    public constructor(props: {}) {
        super(props);

        this.state = {
            activeTabIndex: ArmAutoUpdateAccountDatabaseTabEnumType.Connected
        };

        this.onActiveTabIndexChanged = this.onActiveTabIndexChanged.bind(this);
        this.getTabsHeaders = this.getTabsHeaders.bind(this);
    }

    /**
     * Call back функция на получение индекса активного таба
     * @param tabIndex индекс активного таба
     */
    private onActiveTabIndexChanged(tabIndex: ArmAutoUpdateAccountDatabaseTabEnumType) {
        this.setState({
            activeTabIndex: tabIndex
        });
    }

    /**
     * Получить массив заголовков табов АРМ Автообновления
     * @returns Массив заголовков табов
     */
    private getTabsHeaders() {
        return [
            {
                title: 'Подключены',
                isVisible: true
            },
            {
                title: 'В очереди',
                isVisible: true
            },
            {
                title: 'Выполнения',
                isVisible: true
            },
            {
                title: 'Ручное обновление',
                isVisible: true
            }
        ];
    }

    public render() {
        const { activeTabIndex } = this.state;

        return (
            <TabsControl
                activeTabIndex={ activeTabIndex }
                onActiveTabIndexChanged={ this.onActiveTabIndexChanged }
                headers={ this.getTabsHeaders() }
            >
                <ConnectedDatabasesOnAutoUpdateView />
                <DatabasesInAutoUpdateQueueView isCanLoad={ activeTabIndex === ArmAutoUpdateAccountDatabaseTabEnumType.InQueue } />
                <PerformedAutoUpdateForDatabasesView isCanLoad={ activeTabIndex === ArmAutoUpdateAccountDatabaseTabEnumType.Performed } />
                <IndividualTab />
            </TabsControl>
        );
    }
}

export const ArmAutoUpdateAccountDatabaseView = withHeader({
    page: ArmAutoUpdateAccountDatabaseClass,
    title: 'АРМ Автообновления'
});