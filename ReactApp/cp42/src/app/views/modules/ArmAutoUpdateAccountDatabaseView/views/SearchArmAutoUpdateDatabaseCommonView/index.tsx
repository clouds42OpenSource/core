import { NumbersOfItemsPerPage } from 'app/common/constants';
import { SupportHistoryStateEnumType, SupportHistoryStateEnumTypeLabels } from 'app/common/enums';
import { getSelectOptionsFromEnumLabels } from 'app/common/functions';
import { Nullable } from 'app/common/types';
import { TableFilterWrapper } from 'app/views/components/_hoc/tableFilterWrapper';
import { FilterItemWrapper } from 'app/views/components/_hoc/tableFilterWrapper/filterItemWrapper';
import { OutlinedButton } from 'app/views/components/controls/Button';
import { CheckBoxForm, ComboBoxForm, DropDownListWithCheckboxes, TextBoxForm } from 'app/views/components/controls/forms';
import { DoubleDateInputForm } from 'app/views/components/controls/forms/DoubleDateInputView';
import { DropDownListWithCheckboxItem } from 'app/views/components/controls/forms/DropDownListWithCheckboxes/types';
import { Component } from 'react';
import commonCss from '../../../_common/styles/styles.module.css';

type SearchArmAutoUpdateDatabaseCommonState = {
    /**
     * Строка поиска
     */
    searchString?: string;

    /**
     * Только вип аккаунты
     */
    isVipAccountsOnly?: boolean;

    /**
     * Конфигурации 1С
     */
    configurations?: Array<string>;

    /**
     * Количество записей
     */
    numberOfItemsPerPage?: number;

    /**
     * Только те, которым необходимо авто обновление
     */
    needAutoUpdateOnly?: boolean;

    /**
     * Только те, у которых активна Аренда 1С
     */
    isActiveRent1COnly?: boolean;

    /**
     * Номер воркера
     */
    coreWorkerId?: number;

    /**
     * Статус АО
     */
    status?: SupportHistoryStateEnumType | string;

    /**
     * Время начала АО
     */
    autoUpdatePeriodFrom?: Nullable<Date>;

    /**
     * Время завершения АО
     */
    autoUpdatePeriodTo?: Nullable<Date>;
};

type SearchArmAutoUpdateDatabaseCommonProps = {
    /**
     * Флаг на видимость фильтра авто обновления
     */
    isNeedAutoUpdateOnlyVisible?: boolean;

    /**
     * Флаг на видимость фильтра авктивной Аренды 1С
     */
    isActiveRent1COnlyVisible?: boolean;

    /**
     * Флаг на видимость фильтра статуса АО
     */
    isStatusVisible?: boolean;

    /**
     * Флаг на видимость фильтра даты начала АО до завершения АО
     */
    isDateRangeVisible?: boolean;

    /**
     * Массив элементов для вывода списка чекбоксов
     */
    items: DropDownListWithCheckboxItem[];

    /**
     * Функция для поиска
     */
    onClickToSearch: () => void;
};

export class SearchArmAutoUpdateDatabaseCommonView extends Component<SearchArmAutoUpdateDatabaseCommonProps, SearchArmAutoUpdateDatabaseCommonState> {
    public constructor(props: SearchArmAutoUpdateDatabaseCommonProps) {
        super(props);

        this.state = {
            searchString: '',
            isActiveRent1COnly: true,
            isVipAccountsOnly: false,
            needAutoUpdateOnly: false,
            configurations: this.props.items.map<string>(item => item.text),
            numberOfItemsPerPage: Number(NumbersOfItemsPerPage[1].value),
            autoUpdatePeriodFrom: this.getDateWithLastMonth(),
            autoUpdatePeriodTo: this.getDateWithNextDay(),
            status: ''
        };

        this.onValueChange = this.onValueChange.bind(this);
    }

    public componentDidUpdate(nextProps: SearchArmAutoUpdateDatabaseCommonProps) {
        const { items } = this.props;

        if (nextProps.items && items && nextProps.items.length !== items.length) {
            if (items) {
                this.setState({ configurations: items.map(item => item.text) });
            }
        }
    }

    /**
     * Функция на изменения значения в полях поисковика
     * @param formName Название формы
     * @param newValue Новое значение
     */
    private onValueChange<TForm extends string, TValue>(formName: TForm, newValue: TValue) {
        this.setState({ [formName]: newValue });
    }

    /**
     * Получить дату на следующий день
     * @returns Дата на следующий день
     */
    private getDateWithNextDay() {
        const dateNow = new Date();
        dateNow.setDate(dateNow.getDate() + 1);
        dateNow.setHours(0);
        dateNow.setMinutes(0);
        dateNow.setSeconds(0);
        return dateNow;
    }

    /**
     * Получить дату с прошлым месяцем
     * @returns Дата с прошлым месяцем
     */
    private getDateWithLastMonth() {
        const dateNow = new Date();
        dateNow.setMonth(dateNow.getMonth() - 2);
        dateNow.setHours(0);
        dateNow.setMinutes(0);
        dateNow.setSeconds(0);
        return dateNow;
    }

    /**
     * Функция на изменения значения в конфигурации
     * @param _formName Название формы
     * @param newValue Новое значение
     */
    private onConfigurationsValueChange = (_formName: string, newValue: DropDownListWithCheckboxItem[]) => {
        const configurations = newValue.map<string>(item => item.text);
        this.setState({ configurations });
    };

    public render() {
        const { isActiveRent1COnlyVisible, isDateRangeVisible,
            isNeedAutoUpdateOnlyVisible, isStatusVisible, items
        } = this.props;

        return (
            <TableFilterWrapper>
                <FilterItemWrapper width="600px">
                    <TextBoxForm
                        formName="searchString"
                        label="Поиск по таблице"
                        placeholder="Введите значение из таблицы"
                        value={ this.state.searchString }
                        onValueChange={ this.onValueChange }
                    />
                </FilterItemWrapper>
                <FilterItemWrapper width="320px">
                    <ComboBoxForm
                        label="Количество записей"
                        formName="numberOfItemsPerPage"
                        items={ NumbersOfItemsPerPage }
                        value={ this.state.numberOfItemsPerPage?.toString() }
                        onValueChange={ this.onValueChange }
                    />
                </FilterItemWrapper>
                <FilterItemWrapper width="320px">
                    <DropDownListWithCheckboxes
                        label="Конфигурация"
                        formName="configurations"
                        items={ items }
                        onValueChange={ this.onConfigurationsValueChange }
                    />
                </FilterItemWrapper>

                {
                    isStatusVisible || isStatusVisible === undefined
                        ? (
                            <FilterItemWrapper width="320px">
                                <ComboBoxForm
                                    label="Статус"
                                    formName="status"
                                    items={ getSelectOptionsFromEnumLabels(SupportHistoryStateEnumTypeLabels, { value: '', text: 'Все' }) }
                                    value={ this.state.status?.toString() }
                                    onValueChange={ this.onValueChange }
                                />
                            </FilterItemWrapper>
                        )
                        : null
                }
                { (isDateRangeVisible || isDateRangeVisible === undefined) &&
                    <DoubleDateInputForm
                        label="Дата выполнения"
                        periodFromInputName="autoUpdatePeriodFrom"
                        periodToInputName="autoUpdatePeriodTo"
                        includeTime={ true }
                        onValueChange={ this.onValueChange }
                        periodFromValue={ this.state.autoUpdatePeriodFrom! }
                        periodToValue={ this.state.autoUpdatePeriodTo! }
                    />
                }
                {
                    isNeedAutoUpdateOnlyVisible || isNeedAutoUpdateOnlyVisible === undefined
                        ? (
                            <FilterItemWrapper width="320px">
                                <CheckBoxForm
                                    captionFontSize={ 13 }
                                    captionFontWeight={ 600 }
                                    captionAsLabel={ true }
                                    falseText="Требуют обновления (релиз устаревший)"
                                    trueText="Требуют обновления (релиз устаревший)"
                                    formName="needAutoUpdateOnly"
                                    label=""
                                    isChecked={ this.state.needAutoUpdateOnly }
                                    onValueChange={ this.onValueChange }
                                />
                            </FilterItemWrapper>
                        )
                        : null
                }
                <FilterItemWrapper width="320px">
                    <CheckBoxForm
                        formName="isVipAccountsOnly"
                        captionFontSize={ 13 }
                        captionFontWeight={ 600 }
                        captionAsLabel={ true }
                        falseText="Показать ВИП аккаунты"
                        trueText="Показать ВИП аккаунты"
                        label=""
                        isChecked={ this.state.isVipAccountsOnly }
                        onValueChange={ this.onValueChange }
                    />
                </FilterItemWrapper>
                {
                    isActiveRent1COnlyVisible || isActiveRent1COnlyVisible === undefined
                        ? (
                            <FilterItemWrapper width="320px">
                                <CheckBoxForm
                                    formName="isActiveRent1COnly"
                                    captionFontSize={ 13 }
                                    captionFontWeight={ 600 }
                                    captionAsLabel={ true }
                                    falseText="Показать с активной Арендой"
                                    trueText="Показать с активной Арендой"
                                    label=""
                                    isChecked={ this.state.isActiveRent1COnly }
                                    onValueChange={ this.onValueChange }
                                />
                            </FilterItemWrapper>
                        )
                        : null
                }
                <FilterItemWrapper width="320px">
                    <OutlinedButton
                        kind="success"
                        className={ commonCss['search-button'] }
                        onClick={ this.props.onClickToSearch }
                    >
                        <i className="fa fa-search mr-1" />
                        Поиск
                    </OutlinedButton>
                </FilterItemWrapper>
            </TableFilterWrapper>
        );
    }
}