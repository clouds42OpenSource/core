import { getCommonDateFormat } from 'app/common/functions';
import { GetDatabaseAutoUpdateTechnicalResultsThunkParams } from 'app/modules/armAutoUpdateAccountDatabase/store/reducers/getDatabaseAutoUpdateTechnicalResultsReducer/params';
import { Dialog } from 'app/views/components/controls/Dialog';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { SelectDataCommonDataModel, SelectDataResultCommonDataModel } from 'app/web/common';
import { DatabaseAutoUpdateTechnicalResultDataModel } from 'app/web/InterlayerApiProxy/ArmAutoUpdateAccountDatabaseApiProxy/data-models';
import React, { Component } from 'react';

type TechnicalInformationModalProps = {
    /**
     * Массив данных технического результата АО инф. базы
     */
    databaseAutoUpdateTechnicalResults: SelectDataResultCommonDataModel<DatabaseAutoUpdateTechnicalResultDataModel>;

    /**
     * Номер базы
     */
    numberOfDatabase: string;

    /**
     * Флаг для открытия/закрытия модального окошка
     */
    isOpen: boolean;

    /**
     * Id базы
     */
    accountDatabaseId: string;

    /**
     * Id задачи которая выполняла обновление
     */
    coreWorkerTasksQueueId: string;

    /**
     * Функция для закрытия модального окошка
     */
    onCancelClick: () => void;

    /**
     * Диспатч для получения данных технических результатов АО инф. базы
     */
    dispatchGetDatabaseAutoUpdateTechnicalResultsThunk: (args: GetDatabaseAutoUpdateTechnicalResultsThunkParams) => void;
};

export class TechnicalInformationModalView extends Component<TechnicalInformationModalProps> {
    public constructor(props: TechnicalInformationModalProps) {
        super(props);
    }

    private getDialogBody = () => {
        const { databaseAutoUpdateTechnicalResults: { pagination, records } } = this.props;

        return (
            <CommonTableWithFilter
                isResponsiveTable={ true }
                uniqueContextProviderStateId="TechnicalInformationModalContextProviderStateId"
                tableProps={ {
                    dataset: records,
                    keyFieldName: 'autoUpdateStartDate',
                    fieldsView: {
                        autoUpdateEndDate: {
                            caption: 'Время начала',
                            fieldWidth: 'auto',
                            fieldContentNoWrap: false,
                            format: getCommonDateFormat
                        },
                        autoUpdateStartDate: {
                            caption: 'Время завершения',
                            fieldWidth: 'auto',
                            fieldContentNoWrap: false,
                            format: getCommonDateFormat
                        },
                        debugInformation: {
                            caption: 'Техническая информация',
                            fieldWidth: 'auto',
                            fieldContentNoWrap: false
                        }
                    },
                    pagination: {
                        currentPage: pagination.currentPage,
                        pageSize: pagination.pageSize,
                        recordsCount: pagination.totalCount,
                        totalPages: pagination.pageCount
                    },
                } }
                onDataSelect={ this.onDataSelect }
            />
        );
    };

    private onDataSelect = (args: SelectDataCommonDataModel<undefined>) => {
        const { accountDatabaseId, coreWorkerTasksQueueId } = this.props;
        this.props.dispatchGetDatabaseAutoUpdateTechnicalResultsThunk({
            accountDatabaseId,
            coreWorkerTasksQueueId,
            pageNumber: args.pageNumber,
            sortFieldName: args.sortingData?.fieldName,
            sortType: args.sortingData?.sortKind,
            force: true
        });
    };

    public render() {
        const { numberOfDatabase, isOpen, onCancelClick } = this.props;

        return (
            <Dialog
                disableBackdropClick={ true }
                title={ `Техническая информация по инф. базе ${ numberOfDatabase }` }
                isOpen={ isOpen }
                dialogWidth="md"
                isTitleSmall={ true }
                titleTextAlign="left"
                titleFontSize={ 14 }
                dialogVerticalAlign="center"
                onCancelClick={ onCancelClick }
                buttons={ [
                    {
                        content: 'Закрыть',
                        kind: 'default',
                        onClick: onCancelClick,
                        variant: 'outlined'
                    }
                ] }
            >
                { this.getDialogBody() }
            </Dialog>
        );
    }
}