import { contextAccountId, userId } from 'app/api';
import { SupportHistoryStateEnumType, SupportHistoryStateEnumTypeLabels } from 'app/common/enums';
import { getCommonDateFormat } from 'app/common/functions';
import { useDatabaseCard } from 'app/hooks';
import { ArmAutoUpdateAccountDatabaseProcessId } from 'app/modules/armAutoUpdateAccountDatabase';
import { GetDatabaseAutoUpdateTechnicalResultsThunkParams } from 'app/modules/armAutoUpdateAccountDatabase/store/reducers/getDatabaseAutoUpdateTechnicalResultsReducer/params';
import { GetPerformedAutoUpdateForDatabasesDataThunkParams } from 'app/modules/armAutoUpdateAccountDatabase/store/reducers/getPerformedAutoUpdateForDatabasesReducer/params';
import { GetDatabaseAutoUpdateTechnicalResultsThunk, GetPerformedAutoUpdateForDatabasesThunk, GetWorkersThunk } from 'app/modules/armAutoUpdateAccountDatabase/store/thunks';
import { AvailabilityInfoDbListThunk } from 'app/modules/infoDbList/store/thunks/AvailabilityInfoDbListThunk';
import { AppReduxStoreState } from 'app/redux/types';
import { TextOut } from 'app/views/components/TextOut';
import { TextLinkButton } from 'app/views/components/controls/Button';
import { InformationDatabaseCard } from 'app/views/modules/_common/reusable-modules/InformationDatabaseCard';
import { TechnicalInformationModalView } from 'app/views/modules/ArmAutoUpdateAccountDatabaseView/views/PerformedAutoUpdateForDatabasesView/views';
import { SearchArmAutoUpdateDatabaseCommonView } from 'app/views/modules/ArmAutoUpdateAccountDatabaseView/views/SearchArmAutoUpdateDatabaseCommonView';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { TablePagination } from 'app/views/modules/_common/components/CommonTableWithFilter/TableView';
import { WatchReducerProcessActionState } from 'app/views/modules/_common/components/WatchReducerProcessActionState';
import { InputAccountId } from 'app/web/api/InfoDbProxy/request-dto/InputAccountId';
import { DatabaseAutoUpdateTechnicalResultDataModel, PerformedAutoUpdateForDatabasesDataModel } from 'app/web/InterlayerApiProxy/ArmAutoUpdateAccountDatabaseApiProxy/data-models';
import { SelectDataCommonDataModel, SelectDataResultCommonDataModel, SelectDataResultMetadataModel } from 'app/web/common/data-models';
import { SortingDataModel } from 'app/web/common/data-models/SortingDataModel';
import { IReducerProcessActionInfo } from 'core/redux/interfaces';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';
import React, { Component, createRef, ReactNode } from 'react';
import { connect } from 'react-redux';
import css from './styles.module.css';

type PerformedAutoUpdateForDatabasesState = {
    numberOfDatabase: string;
    isTechnicalInformationModalVisible: boolean;
    isFirstTimeLoaded: boolean;
    accountDatabaseId: string,
    coreWorkerTasksQueueId: string;
    changeField: boolean;
};

type PerformedAutoUpdateForDatabasesProps = {
    isCanLoad: boolean;
};

type StateProps = {
    performedAutoUpdateForDatabases: SelectDataResultMetadataModel<PerformedAutoUpdateForDatabasesDataModel>;
    configurationNames: string[];
    databaseAutoUpdateTechnicalResults: SelectDataResultCommonDataModel<DatabaseAutoUpdateTechnicalResultDataModel>;
    metadata: TablePagination;
};

type DispatchProps = {
    dispatchGetPerformedAutoUpdateForDatabasesThunk: (args: GetPerformedAutoUpdateForDatabasesDataThunkParams) => void;
    dispatchGetDatabaseAutoUpdateTechnicalResultsThunk: (args: GetDatabaseAutoUpdateTechnicalResultsThunkParams) => void;
    dispatchGetWorkersThunk: (args: IForceThunkParam) => void;
    dispatchAvailabilityInfoDbThunk: (args: IForceThunkParam & InputAccountId) => void;
};

type TProps = {
    openDatabase: (databaseNumber: string) => void;
    databaseCard: ReactNode;
};

type AllProps = PerformedAutoUpdateForDatabasesProps & StateProps & DispatchProps & TProps;

class PerformedAutoUpdateForDatabasesClass extends Component<AllProps, PerformedAutoUpdateForDatabasesState> {
    private _commonTableRef = createRef<CommonTableWithFilter<PerformedAutoUpdateForDatabasesDataModel, undefined>>();

    private _searchArmAutoUpdateDatabaseCommonRef = createRef<SearchArmAutoUpdateDatabaseCommonView>();

    public constructor(props: AllProps) {
        super(props);

        this.state = {
            numberOfDatabase: '',
            accountDatabaseId: '',
            coreWorkerTasksQueueId: '',
            isFirstTimeLoaded: false,
            isTechnicalInformationModalVisible: false,
            changeField: false,
        };

        this.onClickToSearch = this.onClickToSearch.bind(this);
        this.onDataSelect = this.onDataSelect.bind(this);
        this.getFormatNumberOfDatabase = this.getFormatNumberOfDatabase.bind(this);
        this.onChangeField = this.onChangeField.bind(this);
    }

    public componentDidMount() {
        this.props.dispatchAvailabilityInfoDbThunk({ accountId: contextAccountId(), accountUserId: userId(), force: true });
    }

    public componentDidUpdate() {
        const { isCanLoad } = this.props;
        const { isFirstTimeLoaded } = this.state;

        if (isCanLoad && !isFirstTimeLoaded) {
            this.props.dispatchGetWorkersThunk({ force: true });
            this.setState({ isFirstTimeLoaded: true });
        }
    }

    private onChangeField() {
        this.setState(({ changeField }) => ({
            changeField: !changeField
        }));
    }

    private onProcessActionStateChanged = (processId: string, actionState: IReducerProcessActionInfo) => {
        this.onProcessGetWorkers(processId, actionState);
        this.onProcessGetDatabaseAutoUpdateTechnicalResults(processId, actionState);
    };

    private onProcessGetDatabaseAutoUpdateTechnicalResults = (processId: string, actionState: IReducerProcessActionInfo) => {
        if (processId === ArmAutoUpdateAccountDatabaseProcessId.GetDatabaseAutoUpdateTechnicalResults && actionState.isInSuccessState) { this.setState({ isTechnicalInformationModalVisible: true }); }
    };

    private onProcessGetWorkers = (processId: string, actionState: IReducerProcessActionInfo) => {
        if (processId === ArmAutoUpdateAccountDatabaseProcessId.GetWorkers && actionState.isInSuccessState) { this._commonTableRef.current?.ReSelectData(); }
    };

    private onClickToSearch() {
        const {
            configurations, searchString,
            isVipAccountsOnly, numberOfItemsPerPage,
            autoUpdatePeriodFrom, autoUpdatePeriodTo,
            coreWorkerId, status
        } = this._searchArmAutoUpdateDatabaseCommonRef.current?.state!;

        this.props.dispatchGetPerformedAutoUpdateForDatabasesThunk({
            filter: {
                autoUpdatePeriodFrom,
                autoUpdatePeriodTo,
                coreWorkerId,
                status,
                configurations,
                isVipAccountsOnly,
                searchLine: searchString,
            },
            pageNumber: 1,
            size: numberOfItemsPerPage,
            force: true
        });
    }

    private onDataSelect(args: SelectDataCommonDataModel<undefined>) {
        const {
            configurations, searchString,
            isVipAccountsOnly, numberOfItemsPerPage,
            autoUpdatePeriodFrom, autoUpdatePeriodTo,
            coreWorkerId, status
        } = this._searchArmAutoUpdateDatabaseCommonRef.current?.state!;
        this.props.dispatchGetPerformedAutoUpdateForDatabasesThunk({
            filter: {
                autoUpdatePeriodFrom,
                autoUpdatePeriodTo,
                coreWorkerId,
                status,
                configurations,
                isVipAccountsOnly,
                searchLine: searchString,
            },
            orderBy: args.sortingData ? this.getSortQuery(args.sortingData) : 'caption.desc',
            pageNumber: args.pageNumber,
            size: numberOfItemsPerPage,
            force: true
        });
    }

    private getFormatNumberOfDatabase(numberDatabase: string) {
        return (
            <TextLinkButton onClick={ () => { this.props.openDatabase(numberDatabase); } }>
                <TextOut fontSize={ 13 } className="text-link">
                    { numberDatabase }
                </TextOut>
            </TextLinkButton>
        );
    }

    private getSortQuery(args: SortingDataModel) {
        return `${ args.fieldName }.${ args.sortKind ? 'desc' : 'asc' }`;
    }

    public render() {
        const { performedAutoUpdateForDatabases: { records },
            databaseAutoUpdateTechnicalResults } = this.props;

        return (
            <>
                <SearchArmAutoUpdateDatabaseCommonView
                    ref={ this._searchArmAutoUpdateDatabaseCommonRef }
                    onClickToSearch={ this.onClickToSearch }
                    isNeedAutoUpdateOnlyVisible={ false }
                    isActiveRent1COnlyVisible={ false }
                    items={ this.props.configurationNames.map(item => ({
                        text: item,
                        value: item,
                        checked: true
                    })) }
                />
                <CommonTableWithFilter
                    ref={ this._commonTableRef }
                    className={ css['custom-database-auto-update-performed'] }
                    isResponsiveTable={ false }
                    uniqueContextProviderStateId="PerformedAutoUpdateForDatabasesContextProviderStateId"
                    tableProps={ {
                        dataset: records,
                        keyFieldName: 'id',
                        fieldsView: {
                            autoUpdateStartDate: {
                                caption: 'Время начала',
                                fieldWidth: 'auto',
                                fieldContentNoWrap: false,
                                isSortable: true,
                                format: getCommonDateFormat
                            },
                            v82Name: {
                                caption: 'Номер базы',
                                fieldWidth: 'auto',
                                fieldContentNoWrap: false,
                                isSortable: true,
                                format: value => this.getFormatNumberOfDatabase(value)
                            },
                            caption: {
                                caption: 'Название базы',
                                fieldWidth: 'auto',
                                fieldContentNoWrap: false,
                                isSortable: true
                            },
                            configurationName: {
                                caption: 'Конфигурация',
                                fieldWidth: 'auto',
                                fieldContentNoWrap: false,
                                isSortable: true
                            },
                            status: {
                                caption: 'Статус',
                                fieldWidth: 'auto',
                                fieldContentNoWrap: false,
                                isSortable: true,
                                format: (status: SupportHistoryStateEnumType) => {
                                    return SupportHistoryStateEnumTypeLabels[status];
                                }
                            },
                            description: {
                                caption: 'Описание',
                                fieldWidth: 'auto',
                                fieldContentNoWrap: false,
                                isSortable: true
                            },
                        },
                        pagination: this.props.metadata
                    } }
                    onDataSelect={ this.onDataSelect }
                />
                { this.props.databaseCard }
                <TechnicalInformationModalView
                    databaseAutoUpdateTechnicalResults={ databaseAutoUpdateTechnicalResults }
                    dispatchGetDatabaseAutoUpdateTechnicalResultsThunk={ this.props.dispatchGetDatabaseAutoUpdateTechnicalResultsThunk }
                    isOpen={ this.state.isTechnicalInformationModalVisible }
                    onCancelClick={ () => { this.setState({ isTechnicalInformationModalVisible: false }); } }
                    numberOfDatabase={ this.state.numberOfDatabase }
                    accountDatabaseId={ this.state.accountDatabaseId }
                    coreWorkerTasksQueueId={ this.state.coreWorkerTasksQueueId }
                />
                <WatchReducerProcessActionState
                    watch={ [{
                        reducerStateName: 'ArmAutoUpdateAccountDatabaseState',
                        processIds: [
                            ArmAutoUpdateAccountDatabaseProcessId.GetWorkers,
                            ArmAutoUpdateAccountDatabaseProcessId.GetDatabaseAutoUpdateTechnicalResults
                        ]
                    }] }
                    onProcessActionStateChanged={ this.onProcessActionStateChanged }
                />
            </>
        );
    }
}

const PerformedAutoUpdateForDatabasesConnect = connect<StateProps, DispatchProps, {}, AppReduxStoreState>(
    state => {
        const armAutoUpdateAccountDatabaseState = state.ArmAutoUpdateAccountDatabaseState;
        const { performedAutoUpdateForDatabases } = armAutoUpdateAccountDatabaseState;
        const { configurationNames } = armAutoUpdateAccountDatabaseState;
        const { workers } = armAutoUpdateAccountDatabaseState;
        const { databaseAutoUpdateTechnicalResults } = armAutoUpdateAccountDatabaseState;
        const currentPage = performedAutoUpdateForDatabases.metadata.pageNumber;
        const totalPages = performedAutoUpdateForDatabases.metadata.pageCount;
        const { pageSize } = performedAutoUpdateForDatabases.metadata;
        const recordsCount = performedAutoUpdateForDatabases.metadata.totalItemCount;

        return {
            performedAutoUpdateForDatabases,
            configurationNames,
            workers,
            databaseAutoUpdateTechnicalResults,
            metadata: {
                currentPage,
                totalPages,
                pageSize,
                recordsCount
            }
        };
    },
    {
        dispatchGetPerformedAutoUpdateForDatabasesThunk: GetPerformedAutoUpdateForDatabasesThunk.invoke,
        dispatchGetDatabaseAutoUpdateTechnicalResultsThunk: GetDatabaseAutoUpdateTechnicalResultsThunk.invoke,
        dispatchGetWorkersThunk: GetWorkersThunk.invoke,
        dispatchAvailabilityInfoDbThunk: AvailabilityInfoDbListThunk.invoke
    }
)(PerformedAutoUpdateForDatabasesClass);

export const PerformedAutoUpdateForDatabasesView = ({ isCanLoad }: PerformedAutoUpdateForDatabasesProps) => {
    const { openDatabaseDialogHandler, closeDatabaseDialogHandler, dialogDatabaseNumber, isOpen } = useDatabaseCard();

    return (
        <PerformedAutoUpdateForDatabasesConnect
            isCanLoad={ isCanLoad }
            openDatabase={ openDatabaseDialogHandler }
            databaseCard={
                <InformationDatabaseCard
                    isOpen={ isOpen }
                    databaseNumber={ dialogDatabaseNumber }
                    onClose={ closeDatabaseDialogHandler }
                />
            }
        />
    );
};