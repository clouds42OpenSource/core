import { Dialog } from 'app/views/components/controls/Dialog';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { TextOut } from 'app/views/components/TextOut';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { AutoUpdateConfiguration1CDetailsDataModel } from 'app/web/InterlayerApiProxy/ArmAutoUpdateAccountDatabaseApiProxy/data-models';
import React, { Component } from 'react';
import { Box } from '@mui/material';

type NumberOfIterationsModalProps = {
    /**
     * Модель авто обновления конфигурации 1С
     */
    autoUpdateConfiguration1CDetails: AutoUpdateConfiguration1CDetailsDataModel;

    /**
     * Номер базы
     */
    numberOfDatabase: string;

    /**
     * Флаг для открытия/закрытия модального окошка
     */
    isOpen: boolean;

    /**
     * Функция для закрытия модального окошка
     */
    onCancelClick: () => void;
};

export class NumberOfIterationsModalView extends Component<NumberOfIterationsModalProps> {
    public constructor(props: NumberOfIterationsModalProps) {
        super(props);
    }

    private getDialogBody = () => {
        const { autoUpdateConfiguration1CDetails: { actualVersion, configurationName, countStepsForUpdate,
            currentVersion, updateConfigurationData }
        } = this.props;

        return (
            <Box display="flex" flexDirection="column">
                <FormAndLabel label="Конфигурация" key="configuration">
                    <TextOut fontSize={ 13 }>
                        { configurationName }
                    </TextOut>
                </FormAndLabel>
                <FormAndLabel label="Кол-во итераций" key="numberOfIterations">
                    <TextOut fontSize={ 13 }>
                        { countStepsForUpdate }
                    </TextOut>
                </FormAndLabel>
                <FormAndLabel label="Текущая версия" key="currentVersion">
                    <TextOut fontSize={ 13 }>
                        { currentVersion }
                    </TextOut>
                </FormAndLabel>
                <FormAndLabel label="Актуальная версия" key="ActualVersion">
                    <TextOut fontSize={ 13 }>
                        { actualVersion }
                    </TextOut>
                </FormAndLabel>
                <CommonTableWithFilter
                    isResponsiveTable={ true }
                    uniqueContextProviderStateId="NumberOfIterationsContextProviderStateId"
                    tableProps={ {
                        dataset: updateConfigurationData,
                        keyFieldName: 'number',
                        fieldsView: {
                            number: {
                                caption: '№ шага',
                                fieldWidth: 'auto',
                                fieldContentNoWrap: false
                            },
                            currentVersion: {
                                caption: 'С версии',
                                fieldWidth: 'auto',
                                fieldContentNoWrap: false
                            },
                            actualVersion: {
                                caption: 'На версию',
                                fieldWidth: 'auto',
                                fieldContentNoWrap: false
                            }
                        }
                    } }
                />
            </Box>
        );
    };

    public render() {
        const { numberOfDatabase, isOpen, onCancelClick } = this.props;

        return (
            <Dialog
                disableBackdropClick={ true }
                title={ `Кол-во итераций для обновления инф. базы ${ numberOfDatabase }` }
                isOpen={ isOpen }
                dialogWidth="sm"
                isTitleSmall={ true }
                titleTextAlign="left"
                titleFontSize={ 14 }
                dialogVerticalAlign="top"
                onCancelClick={ onCancelClick }
                buttons={ [
                    {
                        content: 'Закрыть',
                        kind: 'default',
                        onClick: onCancelClick,
                        variant: 'outlined'
                    }
                ] }
            >
                { this.getDialogBody() }
            </Dialog>
        );
    }
}