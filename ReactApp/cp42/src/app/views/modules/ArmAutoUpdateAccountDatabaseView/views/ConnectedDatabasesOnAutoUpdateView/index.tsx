import { contextAccountId, userId } from 'app/api';
import { getCommonDateFormat } from 'app/common/functions';
import { useDatabaseCard } from 'app/hooks';
import { ArmAutoUpdateAccountDatabaseProcessId } from 'app/modules/armAutoUpdateAccountDatabase';
import { GetConnectedDatabasesOnAutoUpdateThunkParams } from 'app/modules/armAutoUpdateAccountDatabase/store/reducers/getConnectedDatabasesOnAutoUpdatesReducer/params';
import { GetConfigurationNamesThunk, GetConnectedDatabasesOnAutoUpdatesThunk } from 'app/modules/armAutoUpdateAccountDatabase/store/thunks';
import { AvailabilityInfoDbListThunk } from 'app/modules/infoDbList/store/thunks/AvailabilityInfoDbListThunk';
import { AppReduxStoreState } from 'app/redux/types';
import { TextOut } from 'app/views/components/TextOut';
import { TextLinkButton } from 'app/views/components/controls/Button';
import { InformationDatabaseCard } from 'app/views/modules/_common/reusable-modules/InformationDatabaseCard';
import { SearchArmAutoUpdateDatabaseCommonView } from 'app/views/modules/ArmAutoUpdateAccountDatabaseView/views/SearchArmAutoUpdateDatabaseCommonView';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { TablePagination } from 'app/views/modules/_common/components/CommonTableWithFilter/TableView';
import { WatchReducerProcessActionState } from 'app/views/modules/_common/components/WatchReducerProcessActionState';
import { InputAccountId } from 'app/web/api/InfoDbProxy/request-dto/InputAccountId';
import { ConnectedDatabaseOnAutoUpdateDataModel } from 'app/web/InterlayerApiProxy/ArmAutoUpdateAccountDatabaseApiProxy/data-models';
import { SelectDataCommonDataModel, SelectDataResultMetadataModel, SortingDataModel } from 'app/web/common/data-models';
import { IReducerProcessActionInfo } from 'core/redux/interfaces';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';
import React, { Component, createRef, ReactNode } from 'react';
import { connect } from 'react-redux';

type ConnectedDatabasesOnAutoUpdateState = {
    changeField: boolean;
};

type StateProps = {
    connectedDatabasesOnAutoUpdates: SelectDataResultMetadataModel<ConnectedDatabaseOnAutoUpdateDataModel>;
    configurationNames: string[];
    metadata: TablePagination;
};

type DispatchProps = {
    dispatchGetConnectedDatabasesOnAutoUpdatesThunk: (args: GetConnectedDatabasesOnAutoUpdateThunkParams) => void;
    dispatchGetConfigurationNamesThunk: (args: IForceThunkParam) => void;
    dispatchAvailabilityInfoDbThunk: (args: IForceThunkParam & InputAccountId) => void;
};

type TProps = {
    openDatabase: (databaseNumber: string) => void;
    databaseCard: ReactNode;
};

type AllProps = StateProps & DispatchProps & TProps;

class ConnectedDatabasesOnAutoUpdateClass extends Component<AllProps, ConnectedDatabasesOnAutoUpdateState> {
    private _commonTableRef = createRef<CommonTableWithFilter<ConnectedDatabaseOnAutoUpdateDataModel, undefined>>();

    private _searchArmAutoUpdateDatabaseCommonRef = createRef<SearchArmAutoUpdateDatabaseCommonView>();

    public constructor(props: AllProps) {
        super(props);

        this.state = {
            changeField: false
        };

        this.onClickToSearch = this.onClickToSearch.bind(this);
        this.onDataSelect = this.onDataSelect.bind(this);
        this.getFormatNumberOfDatabase = this.getFormatNumberOfDatabase.bind(this);
        this.onChangeField = this.onChangeField.bind(this);
    }

    public componentDidMount() {
        this.props.dispatchGetConfigurationNamesThunk({ force: true });
        this.props.dispatchAvailabilityInfoDbThunk({ accountId: contextAccountId(), accountUserId: userId(), force: true });
    }

    private onChangeField() {
        this.setState(({ changeField }) => ({
            changeField: !changeField
        }));
    }

    private onProcessActionStateChanged = (processId: string, actionState: IReducerProcessActionInfo) => {
        this.onProcessGetConfigurationNames(processId, actionState);
    };

    private onProcessGetConfigurationNames = (processId: string, actionState: IReducerProcessActionInfo) => {
        if (processId === ArmAutoUpdateAccountDatabaseProcessId.GetConfigurationNames && actionState.isInSuccessState) {
            this.props.dispatchGetConnectedDatabasesOnAutoUpdatesThunk({
                filter: {
                    configurations: this.props.configurationNames,
                    isActiveRent1COnly: true,
                },
                pageNumber: 1,
                force: true
            });
        }
    };

    private onClickToSearch() {
        const {
            configurations, searchString, isActiveRent1COnly,
            isVipAccountsOnly, needAutoUpdateOnly, numberOfItemsPerPage
        } = this._searchArmAutoUpdateDatabaseCommonRef.current?.state!;

        this.props.dispatchGetConnectedDatabasesOnAutoUpdatesThunk({
            filter: {
                configurations,
                isActiveRent1COnly,
                isVipAccountsOnly,
                needAutoUpdateOnly,
                searchLine: searchString,
            },
            size: numberOfItemsPerPage,
            pageNumber: 1,
            force: true
        });
    }

    private onDataSelect(args: SelectDataCommonDataModel<undefined>) {
        const {
            configurations, searchString, isActiveRent1COnly,
            isVipAccountsOnly, needAutoUpdateOnly, numberOfItemsPerPage
        } = this._searchArmAutoUpdateDatabaseCommonRef.current?.state!;

        this.props.dispatchGetConnectedDatabasesOnAutoUpdatesThunk({
            filter: {
                configurations,
                isActiveRent1COnly,
                isVipAccountsOnly,
                needAutoUpdateOnly,
                searchLine: searchString,
            },
            orderBy: args.sortingData ? this.getSortQuery(args.sortingData) : 'caption.desc',
            size: numberOfItemsPerPage,
            pageNumber: args.pageNumber,
            force: true
        });
    }

    private getFormatNumberOfDatabase(numberDatabase: string) {
        return (
            <TextLinkButton onClick={ () => { this.props.openDatabase(numberDatabase); } }>
                <TextOut fontSize={ 13 } className="text-link">
                    { numberDatabase }
                </TextOut>
            </TextLinkButton>
        );
    }

    private getSortQuery(args: SortingDataModel) {
        return `${ args.fieldName }.${ args.sortKind ? 'desc' : 'asc' }`;
    }

    public render() {
        const { connectedDatabasesOnAutoUpdates: { records } } = this.props;

        return (
            <>
                <SearchArmAutoUpdateDatabaseCommonView
                    ref={ this._searchArmAutoUpdateDatabaseCommonRef }
                    onClickToSearch={ this.onClickToSearch }
                    isDateRangeVisible={ false }
                    isStatusVisible={ false }
                    items={ this.props.configurationNames.map(item => ({
                        text: item,
                        value: item,
                        checked: true
                    })) }
                />
                <CommonTableWithFilter
                    ref={ this._commonTableRef }
                    isResponsiveTable={ false }
                    uniqueContextProviderStateId="ConnectedDatabasesOnAutoUpdateContextProviderStateId"
                    tableProps={ {
                        dataset: records,
                        keyFieldName: 'id',
                        fieldsView: {
                            connectDate: {
                                caption: 'Дата подключения',
                                fieldWidth: 'auto',
                                fieldContentNoWrap: false,
                                isSortable: true,
                                format: getCommonDateFormat
                            },
                            v82Name: {
                                caption: 'Номер базы',
                                fieldWidth: 'auto',
                                fieldContentNoWrap: false,
                                isSortable: true,
                                format: value => this.getFormatNumberOfDatabase(value)
                            },
                            caption: {
                                caption: 'Название базы',
                                fieldWidth: 'auto',
                                fieldContentNoWrap: false,
                                isSortable: true
                            },
                            configurationName: {
                                caption: 'Конфигурация',
                                fieldWidth: 'auto',
                                fieldContentNoWrap: false,
                                isSortable: true
                            },
                            currentVersion: {
                                caption: 'Релиз текущий',
                                fieldWidth: 'auto',
                                fieldContentNoWrap: false,
                                isSortable: true
                            },
                            actualVersion: {
                                caption: 'Релиз актуальный',
                                fieldWidth: 'auto',
                                fieldContentNoWrap: false,
                                isSortable: true
                            },
                            platformVersion: {
                                caption: 'Релиз платформы',
                                fieldWidth: 'auto',
                                fieldContentNoWrap: false,
                                isSortable: true
                            },
                            lastSuccessAuDate: {
                                caption: 'Дата последнего успешного АО',
                                fieldWidth: 'auto',
                                fieldContentNoWrap: false,
                                isSortable: true,
                                format: getCommonDateFormat
                            }
                        },
                        pagination: this.props.metadata
                    } }
                    onDataSelect={ this.onDataSelect }
                />
                { this.props.databaseCard }
                <WatchReducerProcessActionState
                    watch={ [{
                        reducerStateName: 'ArmAutoUpdateAccountDatabaseState',
                        processIds: [
                            ArmAutoUpdateAccountDatabaseProcessId.GetConfigurationNames
                        ]
                    }] }
                    onProcessActionStateChanged={ this.onProcessActionStateChanged }
                />
            </>
        );
    }
}

const ConnectedDatabasesOnAutoUpdateConnect = connect<StateProps, DispatchProps, {}, AppReduxStoreState>(
    state => {
        const armAutoUpdateAccountDatabaseState = state.ArmAutoUpdateAccountDatabaseState;
        const { connectedDatabasesOnAutoUpdates } = armAutoUpdateAccountDatabaseState;
        const { configurationNames } = armAutoUpdateAccountDatabaseState;
        const { hasConnectedDatabasesOnAutoUpdateReceived } = armAutoUpdateAccountDatabaseState.hasSuccessFor;
        const currentPage = connectedDatabasesOnAutoUpdates.metadata.pageNumber;
        const totalPages = connectedDatabasesOnAutoUpdates.metadata.pageCount;
        const { pageSize } = connectedDatabasesOnAutoUpdates.metadata;
        const recordsCount = connectedDatabasesOnAutoUpdates.metadata.totalItemCount;

        return {
            connectedDatabasesOnAutoUpdates,
            hasConnectedDatabasesOnAutoUpdateReceived,
            configurationNames,
            metadata: {
                currentPage,
                totalPages,
                pageSize,
                recordsCount
            }
        };
    },
    {
        dispatchGetConnectedDatabasesOnAutoUpdatesThunk: GetConnectedDatabasesOnAutoUpdatesThunk.invoke,
        dispatchGetConfigurationNamesThunk: GetConfigurationNamesThunk.invoke,
        dispatchAvailabilityInfoDbThunk: AvailabilityInfoDbListThunk.invoke
    }
)(ConnectedDatabasesOnAutoUpdateClass);

export const ConnectedDatabasesOnAutoUpdateView = () => {
    const { openDatabaseDialogHandler, closeDatabaseDialogHandler, dialogDatabaseNumber, isOpen } = useDatabaseCard();

    return (
        <ConnectedDatabasesOnAutoUpdateConnect
            openDatabase={ openDatabaseDialogHandler }
            databaseCard={
                <InformationDatabaseCard
                    isOpen={ isOpen }
                    databaseNumber={ dialogDatabaseNumber }
                    onClose={ closeDatabaseDialogHandler }
                />
            }
        />
    );
};