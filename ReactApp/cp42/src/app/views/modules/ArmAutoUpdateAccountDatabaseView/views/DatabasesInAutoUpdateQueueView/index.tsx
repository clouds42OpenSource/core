import { contextAccountId, userId } from 'app/api';
import { getCommonDateFormat } from 'app/common/functions';
import { useDatabaseCard } from 'app/hooks';
import { ArmAutoUpdateAccountDatabaseProcessId } from 'app/modules/armAutoUpdateAccountDatabase';
import { GetDatabaseInAutoUpdateQueueThunkParams } from 'app/modules/armAutoUpdateAccountDatabase/store/reducers/getDatabaseInAutoUpdateQueuesReducer/params';
import { GetDatabaseInAutoUpdateQueuesThunk } from 'app/modules/armAutoUpdateAccountDatabase/store/thunks';
import { AvailabilityInfoDbListThunk } from 'app/modules/infoDbList/store/thunks/AvailabilityInfoDbListThunk';
import { AppReduxStoreState } from 'app/redux/types';
import { TextOut } from 'app/views/components/TextOut';
import { TextLinkButton } from 'app/views/components/controls/Button';
import { InformationDatabaseCard } from 'app/views/modules/_common/reusable-modules/InformationDatabaseCard';
import { NumberOfIterationsModalView } from 'app/views/modules/ArmAutoUpdateAccountDatabaseView/views/DatabasesInAutoUpdateQueueView/views';
import { SearchArmAutoUpdateDatabaseCommonView } from 'app/views/modules/ArmAutoUpdateAccountDatabaseView/views/SearchArmAutoUpdateDatabaseCommonView';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { TablePagination } from 'app/views/modules/_common/components/CommonTableWithFilter/TableView';
import { WatchReducerProcessActionState } from 'app/views/modules/_common/components/WatchReducerProcessActionState';
import { InputAccountId } from 'app/web/api/InfoDbProxy/request-dto/InputAccountId';
import { AutoUpdateConfiguration1CDetailsDataModel, DatabaseInAutoUpdateQueueDataModel } from 'app/web/InterlayerApiProxy/ArmAutoUpdateAccountDatabaseApiProxy/data-models';
import { SelectDataCommonDataModel, SelectDataResultMetadataModel, SortingDataModel } from 'app/web/common/data-models';
import { IReducerProcessActionInfo } from 'core/redux/interfaces';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';
import React, { Component, createRef, ReactNode } from 'react';
import { connect } from 'react-redux';
import css from './styles.module.css';

type DatabasesInAutoUpdateQueueState = {
    numberOfDatabase: string;
    isNumberOfIterationsModalVisible: boolean;
    isFirstTimeLoaded: boolean;
    changeField: boolean;
};

type DatabasesInAutoUpdateQueueProps = {
    isCanLoad: boolean;
};

type StateProps = {
    databaseInAutoUpdateQueues: SelectDataResultMetadataModel<DatabaseInAutoUpdateQueueDataModel>;
    configurationNames: string[];
    autoUpdateConfiguration1CDetails: AutoUpdateConfiguration1CDetailsDataModel;
    metadata: TablePagination;
};

type DispatchProps = {
    dispatchGetDatabaseInAutoUpdateQueuesThunk: (args: GetDatabaseInAutoUpdateQueueThunkParams) => void;
    dispatchAvailabilityInfoDbThunk: (args: IForceThunkParam & InputAccountId) => void;
};

type TProps = {
    openDatabase: (databaseNumber: string) => void;
    databaseCard: ReactNode;
};

type AllProps = DatabasesInAutoUpdateQueueProps & StateProps & DispatchProps & TProps;

class DatabasesInAutoUpdateQueueClass extends Component<AllProps, DatabasesInAutoUpdateQueueState> {
    private _commonTableRef = createRef<CommonTableWithFilter<DatabaseInAutoUpdateQueueDataModel, undefined>>();

    private _searchArmAutoUpdateDatabaseCommonRef = createRef<SearchArmAutoUpdateDatabaseCommonView>();

    public constructor(props: AllProps) {
        super(props);

        this.state = {
            numberOfDatabase: '',
            isFirstTimeLoaded: false,
            isNumberOfIterationsModalVisible: false,
            changeField: false,
        };

        this.onClickToSearch = this.onClickToSearch.bind(this);
        this.onDataSelect = this.onDataSelect.bind(this);
        this.getFormatNumberOfDatabase = this.getFormatNumberOfDatabase.bind(this);
        this.onChangeField = this.onChangeField.bind(this);
    }

    public componentDidMount() {
        this.props.dispatchAvailabilityInfoDbThunk({ accountId: contextAccountId(), accountUserId: userId(), force: true });
    }

    public componentDidUpdate() {
        const { isCanLoad } = this.props;
        const { isFirstTimeLoaded } = this.state;

        if (isCanLoad && !isFirstTimeLoaded) {
            this._commonTableRef.current?.ReSelectData();
            this.setState({ isFirstTimeLoaded: true });
        }
    }

    private onChangeField() {
        this.setState(({ changeField }) => ({
            changeField: !changeField
        }));
    }

    private onProcessActionStateChanged = (processId: string, actionState: IReducerProcessActionInfo) => {
        this.onProcessGetAutoUpdateConfiguration1CDetails(processId, actionState);
    };

    private onProcessGetAutoUpdateConfiguration1CDetails = (processId: string, actionState: IReducerProcessActionInfo) => {
        if (processId === ArmAutoUpdateAccountDatabaseProcessId.GetAutoUpdateConfiguration1CDetails && actionState.isInSuccessState) this.setState({ isNumberOfIterationsModalVisible: true });
    };

    private onClickToSearch() {
        const {
            configurations, searchString,
            isVipAccountsOnly, numberOfItemsPerPage
        } = this._searchArmAutoUpdateDatabaseCommonRef.current?.state!;

        this.props.dispatchGetDatabaseInAutoUpdateQueuesThunk({
            filter: {
                configurations,
                isVipAccountsOnly,
                searchLine: searchString,
            },
            pageNumber: 1,
            size: numberOfItemsPerPage,
            force: true
        });
    }

    private onDataSelect(args: SelectDataCommonDataModel<undefined>) {
        const {
            configurations, searchString,
            isVipAccountsOnly, numberOfItemsPerPage
        } = this._searchArmAutoUpdateDatabaseCommonRef.current?.state!;

        this.props.dispatchGetDatabaseInAutoUpdateQueuesThunk({
            filter: {
                configurations,
                isVipAccountsOnly,
                searchLine: searchString,
            },
            orderBy: args.sortingData ? this.getSortQuery(args.sortingData) : 'caption.desc',
            pageNumber: args.pageNumber,
            size: numberOfItemsPerPage,
            force: true
        });
    }

    private getFormatNumberOfDatabase(numberDatabase: string) {
        return (
            <TextLinkButton onClick={ () => { this.props.openDatabase(numberDatabase); } }>
                <TextOut fontSize={ 13 } className="text-link">
                    { numberDatabase }
                </TextOut>
            </TextLinkButton>
        );
    }

    private getSortQuery(args: SortingDataModel) {
        return `${ args.fieldName }.${ args.sortKind ? 'desc' : 'asc' }`;
    }

    public render() {
        const { databaseInAutoUpdateQueues: { records },
            autoUpdateConfiguration1CDetails
        } = this.props;

        return (
            <>
                <SearchArmAutoUpdateDatabaseCommonView
                    ref={ this._searchArmAutoUpdateDatabaseCommonRef }
                    onClickToSearch={ this.onClickToSearch }
                    isDateRangeVisible={ false }
                    isStatusVisible={ false }
                    isNeedAutoUpdateOnlyVisible={ false }
                    isActiveRent1COnlyVisible={ false }
                    items={ this.props.configurationNames.map(item => ({
                        text: item,
                        value: item,
                        checked: true
                    })) }
                />
                <CommonTableWithFilter
                    ref={ this._commonTableRef }
                    className={ css['custom-database-auto-update-queue'] }
                    isResponsiveTable={ false }
                    uniqueContextProviderStateId="DatabaseAutoUpdateQueueContextProviderStateId"
                    tableProps={ {
                        dataset: records,
                        keyFieldName: 'id',
                        fieldsView: {
                            addedDate: {
                                caption: 'Дата добавления',
                                fieldWidth: 'auto',
                                fieldContentNoWrap: false,
                                isSortable: true,
                                format: getCommonDateFormat
                            },
                            v82Name: {
                                caption: 'Номер базы',
                                fieldWidth: 'auto',
                                fieldContentNoWrap: false,
                                isSortable: true,
                                format: value => this.getFormatNumberOfDatabase(value)
                            },
                            caption: {
                                caption: 'Название базы',
                                fieldWidth: 'auto',
                                fieldContentNoWrap: false,
                                isSortable: true
                            },
                            configurationName: {
                                caption: 'Конфигурация',
                                fieldWidth: 'auto',
                                fieldContentNoWrap: false,
                                isSortable: true
                            },
                            currentVersion: {
                                caption: 'Релиз текущий',
                                fieldWidth: 'auto',
                                fieldContentNoWrap: false,
                                isSortable: true
                            },
                            actualVersion: {
                                caption: 'Релиз актуальный',
                                fieldWidth: 'auto',
                                fieldContentNoWrap: false,
                                isSortable: true
                            },
                            platformVersion: {
                                caption: 'Релиз платформы',
                                fieldWidth: 'auto',
                                fieldContentNoWrap: false,
                                isSortable: true
                            },
                        },
                        pagination: this.props.metadata
                    } }
                    onDataSelect={ this.onDataSelect }
                />
                { this.props.databaseCard }
                <NumberOfIterationsModalView
                    autoUpdateConfiguration1CDetails={ autoUpdateConfiguration1CDetails }
                    numberOfDatabase={ this.state.numberOfDatabase }
                    isOpen={ this.state.isNumberOfIterationsModalVisible }
                    onCancelClick={ () => { this.setState({ isNumberOfIterationsModalVisible: false }); } }
                />
                <WatchReducerProcessActionState
                    watch={ [{
                        reducerStateName: 'ArmAutoUpdateAccountDatabaseState',
                        processIds: [
                            ArmAutoUpdateAccountDatabaseProcessId.GetAutoUpdateConfiguration1CDetails
                        ]
                    }] }
                    onProcessActionStateChanged={ this.onProcessActionStateChanged }
                />
            </>
        );
    }
}

const DatabasesInAutoUpdateQueueConnect = connect<StateProps, DispatchProps, {}, AppReduxStoreState>(
    state => {
        const armAutoUpdateAccountDatabaseState = state.ArmAutoUpdateAccountDatabaseState;
        const { databaseInAutoUpdateQueues } = armAutoUpdateAccountDatabaseState;
        const { configurationNames } = armAutoUpdateAccountDatabaseState;
        const autoUpdateConfiguration1CDetails = armAutoUpdateAccountDatabaseState.autoUpdateConfiguration1CDetailsDataModel;
        const currentPage = databaseInAutoUpdateQueues.metadata.pageNumber;
        const totalPages = databaseInAutoUpdateQueues.metadata.pageCount;
        const { pageSize } = databaseInAutoUpdateQueues.metadata;
        const recordsCount = databaseInAutoUpdateQueues.metadata.totalItemCount;

        return {
            databaseInAutoUpdateQueues,
            configurationNames,
            autoUpdateConfiguration1CDetails,
            metadata: {
                currentPage,
                totalPages,
                pageSize,
                recordsCount
            }
        };
    },
    {
        dispatchGetDatabaseInAutoUpdateQueuesThunk: GetDatabaseInAutoUpdateQueuesThunk.invoke,
        dispatchAvailabilityInfoDbThunk: AvailabilityInfoDbListThunk.invoke
    }
)(DatabasesInAutoUpdateQueueClass);

export const DatabasesInAutoUpdateQueueView = ({ isCanLoad }: DatabasesInAutoUpdateQueueProps) => {
    const { openDatabaseDialogHandler, closeDatabaseDialogHandler, dialogDatabaseNumber, isOpen } = useDatabaseCard();

    return (
        <DatabasesInAutoUpdateQueueConnect
            isCanLoad={ isCanLoad }
            openDatabase={ openDatabaseDialogHandler }
            databaseCard={
                <InformationDatabaseCard
                    isOpen={ isOpen }
                    databaseNumber={ dialogDatabaseNumber }
                    onClose={ closeDatabaseDialogHandler }
                />
            }
        />
    );
};