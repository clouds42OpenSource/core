import { FormControlLabel } from '@mui/material';
import Checkbox from '@mui/material/Checkbox';
import { TGetSoloUpdateQueueView } from 'app/api/endpoints/autoUpdate/response';
import { FETCH_API } from 'app/api/useFetchApi';
import { EMessageType, useFloatMessages } from 'app/hooks';
import { TextOut } from 'app/views/components/TextOut';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import dayjs from 'dayjs';
import React, { useEffect, useState } from 'react';

const { getSoloUpdateQueue } = FETCH_API.AUTO_UPDATE;

export const IndividualTab = () => {
    const { show } = useFloatMessages();

    const [soloUpdateQueue, setSoloUpdateQueue] = useState<TGetSoloUpdateQueueView[]>([]);
    const [showVip, setShowVip] = useState(false);

    useEffect(() => {
        (async () => {
            const response = await getSoloUpdateQueue(showVip);

            if (response.success && response.data) {
                setSoloUpdateQueue(response.data);
            } else {
                show(EMessageType.error, response.message);
            }
        })();
    }, [show, showVip]);

    return (
        <>
            <FormControlLabel
                control={
                    <Checkbox
                        checked={ showVip }
                        onChange={ event => setShowVip(event.target.checked) }
                        size="small"
                    />
                }
                label={
                    <TextOut fontWeight={ 700 }>Показать ВИП аккаунты</TextOut>
                }
            />
            <CommonTableWithFilter
                uniqueContextProviderStateId="soloUpdateQueue"
                tableProps={ {
                    dataset: soloUpdateQueue,
                    keyFieldName: 'name',
                    fieldsView: {
                        number: {
                            caption: 'Номер'
                        },
                        name: {
                            caption: 'Имя'
                        },
                        addDate: {
                            caption: 'Дата',
                            format: value => <TextOut>{ dayjs(value).format('DD.MM.YYYY HH:mm:ss') }</TextOut>
                        }
                    }
                } }
            />
        </>
    );
};