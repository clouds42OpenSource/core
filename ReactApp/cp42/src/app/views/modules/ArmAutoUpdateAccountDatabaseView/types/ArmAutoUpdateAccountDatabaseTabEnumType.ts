/**
 * Enum табов Арм автообновления
 */
export enum ArmAutoUpdateAccountDatabaseTabEnumType {
    /**
     * Таб подключены
     */
    Connected,

    /**
     * Таб в очереди
     */
    InQueue,

    /**
     * Таб выполнения
     */
    Performed
}