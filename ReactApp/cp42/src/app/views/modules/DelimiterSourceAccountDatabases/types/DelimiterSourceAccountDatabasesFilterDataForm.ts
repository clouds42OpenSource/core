/**
 * Модель Redux формы для редактирования фильтра для таблицы материнских баз разделителей
 */
export type DelimiterSourceAccountDatabasesFilterDataForm = {
    /**
     * Строка фильтра
     */
    searchLine: string;
};