import { hasComponentChangesFor } from 'app/common/functions';
import { TextBoxForm } from 'app/views/components/controls/forms';
import { DynAutocompleteComboBoxForm } from 'app/views/components/controls/forms/ComboBox/DynAutocompleteComboBoxForm';
import { ComboboxItemModel } from 'app/views/components/controls/forms/ComboBox/models';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import {
    SearchAccountDatabaseDataModel
} from 'app/web/InterlayerApiProxy/DelimiterSourceAccountDatabasesApiProxy/searchAccountDatabases';
import { RequestKind } from 'core/requestSender/enums';
import React from 'react';

type OwnProps = {
    label: string;
    autoFocus: boolean;
    formName: string;
    readOnlyV82Name: string;
    accountDatabaseId: string;
    isInEditMode: boolean;
    onValueChange: <TValue>(fieldName: string, newValue?: TValue, prevValue?: TValue) => boolean | void;
};

export class DatabaseV82NameView extends React.Component<OwnProps> {
    public constructor(props: OwnProps) {
        super(props);
        this.searchAccountDatabases = this.searchAccountDatabases.bind(this);
        this.onValueChange = this.onValueChange.bind(this);
        this.foundAccountDatabaseToComboboxItemModel = this.foundAccountDatabaseToComboboxItemModel.bind(this);
    }

    public shouldComponentUpdate(nextProps: OwnProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private onValueChange(formName: string, originalItems: Array<SearchAccountDatabaseDataModel>, newValue: string, prevValue: string) {
        this.props.onValueChange(formName, newValue, prevValue);

        const accountDatabaseV82Name = originalItems.find(item => item.accountDatabaseId === newValue)?.accountDatabaseV82Name;
        this.props.onValueChange('accountDatabaseV82Name', accountDatabaseV82Name);
    }

    private foundAccountDatabaseToComboboxItemModel(item: SearchAccountDatabaseDataModel): ComboboxItemModel<string> {
        return {
            value: item.accountDatabaseId,
            text: item.accountDatabaseCaption
        };
    }

    private searchAccountDatabases(searchLine: string, setItems: (items: Array<SearchAccountDatabaseDataModel>) => void) {
        const api = InterlayerApiProxy.getDelimiterSourceAccountDatabasesApi();
        api.searchAccountDatabases(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, { searchLine }).then(foundAccountDatabases => { setItems(foundAccountDatabases); });
    }

    public render() {
        const { isInEditMode, accountDatabaseId, autoFocus, formName, label, readOnlyV82Name } = this.props;
        return isInEditMode
            ? (
                <DynAutocompleteComboBoxForm
                    autoFocus={ autoFocus }
                    formName={ formName }
                    label={ label }
                    value={ accountDatabaseId }
                    onValueChange={ this.onValueChange }
                    placeholder="Введите название базы, номер базы"
                    onGetItems={ this.searchAccountDatabases }
                    originalItemToComboboxItemModel={ this.foundAccountDatabaseToComboboxItemModel }
                />
            )
            : (
                <TextBoxForm
                    isReadOnly={ true }
                    label={ label }
                    value={ readOnlyV82Name }
                    formName={ formName }
                />
            );
    }
}