import { hasComponentChangesFor } from 'app/common/functions';
import { TextBoxForm } from 'app/views/components/controls/forms';
import { AutocompleteComboBoxForm } from 'app/views/components/controls/forms/ComboBox/AutocompleteComboBoxForm';
import { KeyValueDataModel } from 'app/web/common/data-models';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { RequestKind } from 'core/requestSender/enums';
import React from 'react';

type OwnProps = {
    label: string;
    formName: string;
    readOnlyDbTemplateDelimiterCode: string;
    editDbTemplateDelimiterCode: string;
    isInEditMode: boolean;
    onValueChange: <TValue>(fieldName: string, newValue?: TValue, prevValue?: TValue) => boolean | void;
};

type OwnState = {
    dbTeplates: KeyValueDataModel<string, string>[];
};

export class DbTemplateDelimiterCodeView extends React.Component<OwnProps, OwnState> {
    public constructor(props: OwnProps) {
        super(props);

        this.state = {
            dbTeplates: []
        };
    }

    public async componentDidMount() {
        if (!this.props.isInEditMode) { return; }

        const api = InterlayerApiProxy.getDelimiterSourceAccountDatabasesApi();
        const dbTeplates = await api.getDbTemplateDelimiters(RequestKind.SEND_BY_ROBOT);

        this.setState(() => {
            return {
                dbTeplates
            };
        }, () => {
            this.props.onValueChange('dbTemplateDelimiterCode', this.state.dbTeplates[0].key);
        });
    }

    public shouldComponentUpdate(nextProps: OwnProps, nextState: OwnState) {
        return hasComponentChangesFor(this.props, nextProps) ||
            hasComponentChangesFor(this.state, nextState);
    }

    public render() {
        return this.props.isInEditMode
            ? this.state.dbTeplates.length && this.props.editDbTemplateDelimiterCode
                ? (
                    <AutocompleteComboBoxForm
                        formName={ this.props.formName }
                        label={ this.props.label }
                        value={ this.props.editDbTemplateDelimiterCode }
                        items={ this.state.dbTeplates.map(item => {
                            return {
                                value: item.key,
                                text: item.value
                            };
                        }) }
                        onValueChange={ this.props.onValueChange }
                    />
                )
                : null
            : (
                <TextBoxForm
                    isReadOnly={ true }
                    label={ this.props.label }
                    formName={ this.props.formName }
                    value={ this.props.readOnlyDbTemplateDelimiterCode }
                />
            );
    }
}