import { hasComponentChangesFor } from 'app/common/functions';
import { TextBoxForm } from 'app/views/components/controls/forms';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import {
    DatabaseV82NameView
} from 'app/views/modules/DelimiterSourceAccountDatabases/views/AddModifyDelimiterSourceAccountDatabaseDialogContentFormView/partial-elements/DatabaseV82NameView';
import {
    DbTemplateDelimiterCodeView
} from 'app/views/modules/DelimiterSourceAccountDatabases/views/AddModifyDelimiterSourceAccountDatabaseDialogContentFormView/partial-elements/DbTemplateDelimiterCodeView';
import {
    DelimiterSourceAccountDatabaseItemDataModel
} from 'app/web/InterlayerApiProxy/DelimiterSourceAccountDatabasesApiProxy/receiveDelimiterSourceAccountDatabases';
import {
    delimiterSourceAccountDatabaseItemGenerateIdFor
} from 'app/web/InterlayerApiProxy/DelimiterSourceAccountDatabasesApiProxy/receiveDelimiterSourceAccountDatabases/functions';
import React from 'react';
import { Box } from '@mui/material';

type OwnProps = ReduxFormProps<DelimiterSourceAccountDatabaseItemDataModel> & {
    isEditMode: boolean;
    itemToEdit: DelimiterSourceAccountDatabaseItemDataModel;
};

class AddModifyDelimiterSourceAccountDatabaseDialogContentFormViewClass extends React.Component<OwnProps> {
    public constructor(props: OwnProps) {
        super(props);
        this.onValueChange = this.onValueChange.bind(this);
        this.initReduxForm = this.initReduxForm.bind(this);

        this.props.reduxForm.setInitializeFormDataAction(this.initReduxForm);
    }

    public shouldComponentUpdate(nextProps: OwnProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private onValueChange<TValue>(formName: string, newValue: TValue) {
        this.props.reduxForm.updateReduxFormFields({
            [formName]: newValue
        });
    }

    private initReduxForm(): DelimiterSourceAccountDatabaseItemDataModel | undefined {
        if (!this.props.itemToEdit) {
            return;
        }

        const item: DelimiterSourceAccountDatabaseItemDataModel = {
            id: '',
            accountDatabaseId: this.props.itemToEdit.accountDatabaseId,
            databaseOnDelimitersPublicationAddress: this.props.itemToEdit.databaseOnDelimitersPublicationAddress,
            dbTemplateDelimiterCode: this.props.itemToEdit.dbTemplateDelimiterCode,
            accountDatabaseV82Name: this.props.itemToEdit.accountDatabaseV82Name
        };

        delimiterSourceAccountDatabaseItemGenerateIdFor(item);
        return item;
    }

    public render() {
        if (!this.props.itemToEdit) {
            return null;
        }

        const reduxForm = this.props.reduxForm.getReduxFormFields(false);
        const databaseOnDelimitersPublicationAddress =
            reduxForm.databaseOnDelimitersPublicationAddress
                ? reduxForm.databaseOnDelimitersPublicationAddress
                : this.props.itemToEdit.databaseOnDelimitersPublicationAddress;

        return (
            <Box display="flex" flexDirection="column" gap="16px">
                <DatabaseV82NameView
                    autoFocus={ this.props.isEditMode }
                    formName="accountDatabaseId"
                    isInEditMode={ this.props.isEditMode }
                    label="Информационная база"
                    accountDatabaseId={ reduxForm.accountDatabaseId }
                    readOnlyV82Name={ this.props.itemToEdit.accountDatabaseV82Name }
                    onValueChange={ this.onValueChange }
                />
                <DbTemplateDelimiterCodeView
                    formName="dbTemplateDelimiterCode"
                    isInEditMode={ this.props.isEditMode }
                    label="Конфигурация базы на разделителях"
                    readOnlyDbTemplateDelimiterCode={ this.props.itemToEdit.dbTemplateDelimiterCode }
                    editDbTemplateDelimiterCode={ reduxForm.dbTemplateDelimiterCode }
                    onValueChange={ this.onValueChange }
                />
                <TextBoxForm
                    autoFocus={ !this.props.isEditMode }
                    formName="databaseOnDelimitersPublicationAddress"
                    label="Адрес публикации базы на разделителях"
                    value={ databaseOnDelimitersPublicationAddress }
                    onValueChange={ this.onValueChange }
                />
            </Box>
        );
    }
}

export const AddModifyDelimiterSourceAccountDatabaseDialogContentFormView = withReduxForm(AddModifyDelimiterSourceAccountDatabaseDialogContentFormViewClass, {
    reduxFormName: 'AddModifyDelimiterSourceAccountDatabase_ReduxForm',
    resetOnUnmount: true
});