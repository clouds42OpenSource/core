import { SortingKind } from 'app/common/enums';
import { hasComponentChangesFor } from 'app/common/functions';
import { ProcessIdStateHelper } from 'app/common/helpers/ProcessIdStateHelper';
import { DelimiterSourceAccountDatabasesProcessId } from 'app/modules/delimiterSourceAccountDatabases';
import {
    DeleteDelimiterSourceAccountDatabaseThunkParams
} from 'app/modules/delimiterSourceAccountDatabases/store/reducers/deleteDelimiterSourceAccountDatabaseReducer/params';
import {
    InsertDelimiterSourceAccountDatabaseThunkParams
} from 'app/modules/delimiterSourceAccountDatabases/store/reducers/insertDelimiterSourceAccountDatabaseReducer/params';
import {
    ReceiveDelimiterSourceAccountDatabasesThunkParams
} from 'app/modules/delimiterSourceAccountDatabases/store/reducers/receiveDelimiterSourceAccountDatabasesReducer/params';
import {
    UpdateDelimiterSourceAccountDatabaseThunkParams
} from 'app/modules/delimiterSourceAccountDatabases/store/reducers/updateDelimiterSourceAccountDatabaseReducer/params';
import {
    DeleteDelimiterSourceAccountDatabaseThunk,
    InsertDelimiterSourceAccountDatabaseThunk,
    ReceiveDelimiterSourceAccountDatabasesThunk,
    UpdateDelimiterSourceAccountDatabaseThunk
} from 'app/modules/delimiterSourceAccountDatabases/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { withHeader } from 'app/views/components/_hoc/withHeader';
import { DelimiterSourceAccountDatabasesFilterDataForm } from 'app/views/modules/DelimiterSourceAccountDatabases/types';
import {
    AddModifyDelimiterSourceAccountDatabaseDialogContentFormView
} from 'app/views/modules/DelimiterSourceAccountDatabases/views/AddModifyDelimiterSourceAccountDatabaseDialogContentFormView';
import {
    DelimiterSourceAccountDatabasesFilterFormView
} from 'app/views/modules/DelimiterSourceAccountDatabases/views/DelimiterSourceAccountDatabasesFilterFormView';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { TablePagination } from 'app/views/modules/_common/components/CommonTableWithFilter/TableView';
import { SelectDataCommonDataModel, SortingDataModel } from 'app/web/common/data-models';
import {
    DelimiterSourceAccountDatabaseItemDataModel
} from 'app/web/InterlayerApiProxy/DelimiterSourceAccountDatabasesApiProxy/receiveDelimiterSourceAccountDatabases';
import React from 'react';
import { connect } from 'react-redux';

type StateProps<TDataRow = DelimiterSourceAccountDatabaseItemDataModel> = {
    dataset: TDataRow[];
    isInLoadingDataProcess: boolean;
    pagination: TablePagination;
};

type DispatchProps = {
    dispatchReceiveDelimiterSourceAccountDatabasesThunk: (args: ReceiveDelimiterSourceAccountDatabasesThunkParams) => void;
    dispatchDeleteDelimiterSourceAccountDatabaseThunk: (args: DeleteDelimiterSourceAccountDatabaseThunkParams) => void;
    dispatchInsertDelimiterSourceAccountDatabaseThunk: (args: InsertDelimiterSourceAccountDatabaseThunkParams) => void;
    dispatchUpdateDelimiterSourceAccountDatabaseThunk: (args: UpdateDelimiterSourceAccountDatabaseThunkParams) => void;
};

type AllProps = StateProps & DispatchProps;

class DelimiterSourceAccountDatabasesClass extends React.Component<AllProps> {
    public constructor(props: AllProps) {
        super(props);
        this.onDataSelect = this.onDataSelect.bind(this);
    }

    public shouldComponentUpdate(nextProps: AllProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private onDataSelect(args: SelectDataCommonDataModel<DelimiterSourceAccountDatabasesFilterDataForm>) {
        this.props.dispatchReceiveDelimiterSourceAccountDatabasesThunk({
            pageNumber: args.pageNumber,
            filter: args.filter,
            orderBy: args.sortingData ? this.getSortQuery(args.sortingData) : 'name.desc',
            force: true
        });
    }

    private getSortQuery(args: SortingDataModel) {
        switch (args.fieldName) {
            case 'accountDatabaseV82Name':
                return `accountDatabase.V82Name.${ args.sortKind ? 'desc' : 'asc' }`;
            case 'dbTemplateDelimiterCode':
                return `dbTemplateDelimiterCode.${ args.sortKind ? 'desc' : 'asc' }`;
            case 'databaseOnDelimitersPublicationAddress':
                return `databaseOnDelimitersPublicationAddress.${ args.sortKind ? 'desc' : 'asc' }`;
            default:
                return 'name.desc';
        }
    }

    public render() {
        return (
            <CommonTableWithFilter
                uniqueContextProviderStateId="DelimiterSourceAccountDatabasesContextProviderStateId"
                tableEditProps={ {
                    processProps: {
                        watch: {
                            reducerStateName: 'DelimiterSourceAccountDatabasesState',
                            processIds: {
                                addNewItemProcessId: DelimiterSourceAccountDatabasesProcessId.InsertDelimiterSourceAccountDatabase,
                                modifyItemProcessId: DelimiterSourceAccountDatabasesProcessId.UpdateDelimiterSourceAccountDatabase,
                                deleteItemProcessId: DelimiterSourceAccountDatabasesProcessId.DeleteDelimiterSourceAccountDatabase
                            }
                        },
                        processMessages: {
                            getOnSuccessAddNewItemMessage: () => 'Добавление новой материнской базы успешно завершено.',
                            getOnSuccessModifyItemMessage: () => 'Редактирование материнской базы успешно завершено.',
                            getOnSuccessDeleteItemMessage: () => 'Удаление материнской базы успешно завершено. '
                        }
                    },
                    dialogProps: {
                        deleteDialog: {
                            getDialogTitle: () => 'Удаление материнской базы',
                            getDialogContentView: itemToDelete => `Вы действительно хотите удалить материнскую базу "${ itemToDelete.accountDatabaseV82Name }"?`,
                            confirmDeleteItemButtonText: 'Удалить',
                            onDeleteItemConfirmed: itemToDelete => {
                                this.props.dispatchDeleteDelimiterSourceAccountDatabaseThunk({
                                    accountDatabaseId: itemToDelete.accountDatabaseId,
                                    dbTemplateDelimiterCode: itemToDelete.dbTemplateDelimiterCode,
                                    force: true
                                });
                            }
                        },
                        addDialog: {
                            dialogWidth: 'sm',
                            getDialogTitle: () => 'Добавление материнской базы',
                            getDialogContentFormView: ref =>
                                <AddModifyDelimiterSourceAccountDatabaseDialogContentFormView
                                    ref={ ref }
                                    isEditMode={ true }
                                    itemToEdit={
                                        {
                                            id: '',
                                            accountDatabaseId: '',
                                            accountDatabaseV82Name: '',
                                            databaseOnDelimitersPublicationAddress: '',
                                            dbTemplateDelimiterCode: ''
                                        }
                                    }
                                />,
                            confirmAddItemButtonText: 'Добавить',
                            showAddDialogButtonText: 'Добавить материнскую базу',
                            onAddNewItemConfirmed: itemToAdd => {
                                this.props.dispatchInsertDelimiterSourceAccountDatabaseThunk({
                                    id: '',
                                    accountDatabaseId: itemToAdd.accountDatabaseId,
                                    dbTemplateDelimiterCode: itemToAdd.dbTemplateDelimiterCode,
                                    accountDatabaseV82Name: itemToAdd.accountDatabaseV82Name,
                                    databaseOnDelimitersPublicationAddress: itemToAdd.databaseOnDelimitersPublicationAddress,
                                    force: true
                                });
                            }
                        },
                        modifyDialog: {
                            getDialogTitle: () => 'Редактирование материнской базы',
                            confirmModifyItemButtonText: 'Сохранить',
                            getDialogContentFormView: (ref, item) => <AddModifyDelimiterSourceAccountDatabaseDialogContentFormView
                                ref={ ref }
                                isEditMode={ false }
                                itemToEdit={ item }
                            />,
                            onModifyItemConfirmed: itemToModify => {
                                this.props.dispatchUpdateDelimiterSourceAccountDatabaseThunk({
                                    ...itemToModify,
                                    force: true
                                });
                            }
                        }
                    }
                } }
                filterProps={ {
                    getFilterContentView: (ref, onFilterChanged) => <DelimiterSourceAccountDatabasesFilterFormView ref={ ref } isFilterFormDisabled={ this.props.isInLoadingDataProcess } onFilterChanged={ onFilterChanged } />
                } }
                tableProps={ {
                    dataset: this.props.dataset,
                    keyFieldName: 'id',
                    sorting: {
                        sortFieldName: 'dbTemplateDelimiterCode',
                        sortKind: SortingKind.Asc
                    },
                    fieldsView: {
                        accountDatabaseV82Name: {
                            caption: 'Название информационной базы',
                            fieldContentNoWrap: false,
                            isSortable: true,
                            fieldWidth: 'auto'
                        },
                        dbTemplateDelimiterCode: {
                            caption: 'Код конфигурации базы на разделителях',
                            fieldContentNoWrap: false,
                            isSortable: true,
                            fieldWidth: 'auto'
                        },
                        databaseOnDelimitersPublicationAddress: {
                            caption: 'Адрес публикации базы на разделителях',
                            fieldContentNoWrap: false,
                            isSortable: true,
                            fieldWidth: 'auto'
                        }
                    },
                    pagination: this.props.pagination
                } }
                onDataSelect={ this.onDataSelect }
            />
        );
    }
}

const DelimiterSourceAccountDatabasesConnected = connect<StateProps, DispatchProps, {}, AppReduxStoreState>(
    state => {
        const { DelimiterSourceAccountDatabasesState } = state;
        const { delimiterSourceAccountDatabases } = DelimiterSourceAccountDatabasesState;
        const dataset = delimiterSourceAccountDatabases.records;

        const currentPage = delimiterSourceAccountDatabases.metadata.pageNumber;
        const totalPages = delimiterSourceAccountDatabases.metadata.pageCount;
        const { pageSize } = delimiterSourceAccountDatabases.metadata;
        const recordsCount = delimiterSourceAccountDatabases.metadata.totalItemCount;

        const isInLoadingDataProcess = ProcessIdStateHelper.isInProgress(DelimiterSourceAccountDatabasesState.reducerActions, DelimiterSourceAccountDatabasesProcessId.ReceiveDelimiterSourceAccountDatabases);

        return {
            dataset,
            isInLoadingDataProcess,
            pagination: {
                currentPage,
                totalPages,
                pageSize,
                recordsCount
            }
        };
    },
    {
        dispatchReceiveDelimiterSourceAccountDatabasesThunk: ReceiveDelimiterSourceAccountDatabasesThunk.invoke,
        dispatchDeleteDelimiterSourceAccountDatabaseThunk: DeleteDelimiterSourceAccountDatabaseThunk.invoke,
        dispatchInsertDelimiterSourceAccountDatabaseThunk: InsertDelimiterSourceAccountDatabaseThunk.invoke,
        dispatchUpdateDelimiterSourceAccountDatabaseThunk: UpdateDelimiterSourceAccountDatabaseThunk.invoke
    }
)(DelimiterSourceAccountDatabasesClass);

export const DelimiterSourceAccountDatabases = withHeader({
    title: 'Управление материнскими базами',
    page: DelimiterSourceAccountDatabasesConnected
});