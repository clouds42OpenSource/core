import { useEffect } from 'react';

import { withHeader } from 'app/views/components/_hoc/withHeader';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { TextOut } from 'app/views/components/TextOut';
import { AccountInfo, AccountAdministrators, Banner, Market } from 'app/views/modules/Main/modules';
import { useAppDispatch, useAppSelector } from 'app/hooks';
import { REDUX_API } from 'app/api/useReduxApi';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { LoadingBounce } from 'app/views/components/LoadingBounce';

import styles from './style.module.css';

const { getMainPageInfo } = REDUX_API.MAIN_PAGE_REDUX;

const Main = withFloatMessages(({ floatMessage: { show } }) => {
    const dispatch = useAppDispatch();
    const { error, isLoading } = useAppSelector(state => state.MainPageState.mainPageReducer);

    useEffect(() => {
        getMainPageInfo(dispatch);
    }, [dispatch]);

    useEffect(() => {
        if (error) {
            show(MessageType.Error, error);
        }
    }, [error, show]);

    return (
        <>
            { isLoading && <LoadingBounce /> }
            <div className={ styles.container }>
                <TextOut fontWeight={ 500 } fontSize={ 16 }>Добро пожаловать в личный кабинет «42Clouds»!</TextOut>
                <AccountInfo />
                <AccountAdministrators />
                <Banner />
                <Market />
            </div>
        </>
    );
});

export const MainView = withHeader({
    page: Main,
    title: 'Главная страница'
});