import { useAppSelector } from 'app/hooks';
import { TextOut } from 'app/views/components/TextOut';
import { SuccessButton } from 'app/views/components/controls/Button';
import { apiHost } from 'app/api';

import styles from './style.module.css';

export const Market = () => {
    const { data } = useAppSelector(state => state.MainPageState.mainPageReducer);

    const clickButtonHandler = () => {
        if (data) {
            window.open(data.market.marketUrlWithSource, '_blank');
        }
    };

    return (
        <div className={ styles.container }>
            <img className={ styles.image } src={ apiHost((data?.market.bannerUrl ?? '').replaceAll('\\', '/')) } alt="market42" />
            <div className={ styles.marketInfo }>
                <TextOut fontSize={ 20 } fontWeight={ 500 }>
                    Маркет 42. Магазин обработок для 1С. Тестируйте бесплатно!
                </TextOut>
                <TextOut>
                    Упростите работу в 1С с помощью внешних обработок.
                    Подключайте обработку к 1С в 3 клика, тестируйте бесплатно и потом принимайте решение о покупке.
                </TextOut>
                <SuccessButton onClick={ clickButtonHandler }>Узнать подробнее</SuccessButton>
            </div>
        </div>
    );
};