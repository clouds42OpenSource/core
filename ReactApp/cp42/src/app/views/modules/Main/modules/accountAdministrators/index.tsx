import React from 'react';

import { useAppSelector } from 'app/hooks';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { TextOut } from 'app/views/components/TextOut';

import styles from './style.module.css';

export const AccountAdministrators = () => {
    const { data } = useAppSelector(state => state.MainPageState.mainPageReducer);

    return (
        <div className={ styles.container }>
            <TextOut fontWeight={ 500 } fontSize={ 14 }>Администраторы аккаунта</TextOut>
            <CommonTableWithFilter
                uniqueContextProviderStateId="accountAdministratorsInfo"
                isBorderStyled={ true }
                isResponsiveTable={ true }
                tableProps={ {
                    emptyText: 'Не удалось получить список администраторов аккаунта',
                    dataset: data?.accountUser.accountAdministrators ?? [],
                    keyFieldName: 'email',
                    fieldsView: {
                        lastName: {
                            caption: 'ФИО',
                            format: (_: string | null, rowData) =>
                                <>{ rowData.firstName } { rowData.middleName } { rowData.lastName }</>
                        },
                        phoneNumber: {
                            caption: 'Телефон'
                        },
                        email: {
                            caption: 'E-Mail'
                        },
                    }
                } }
            />
        </div>
    );
};