import { useAppSelector } from 'app/hooks';

import styles from './style.module.css';

export const Banner = () => {
    const { data } = useAppSelector(state => state.MainPageState.mainPageReducer);

    return (
        <div className={ styles.container } dangerouslySetInnerHTML={ { __html: data?.market.mainPageNotification ?? '' } } />
    );
};