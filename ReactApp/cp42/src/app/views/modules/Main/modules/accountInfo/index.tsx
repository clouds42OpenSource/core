import { Box } from '@mui/material';
import LinearProgress from '@mui/material/LinearProgress';
import { TAcf } from 'app/api/endpoints/main/response';
import { COLORS, encodeToBase64 } from 'app/utils';
import { useEffect, useState } from 'react';

import { putProfileRequestDto } from 'app/api/endpoints/profile/request';
import { FETCH_API } from 'app/api/useFetchApi';
import { useAppSelector } from 'app/hooks';
import { TextOut } from 'app/views/components/TextOut';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { SuccessButton } from 'app/views/components/controls/Button';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { EditProfileModal } from 'app/views/modules/Profile';

import { userId } from 'app/api';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import styles from './style.module.css';

const { useGetProfile, putProfile } = FETCH_API.PROFILE;
const { getUserAchievement } = FETCH_API.MAIN_PAGE;

export const AccountInfo = withFloatMessages(({ floatMessage: { show } }) => {
    const [isOpen, setIsOpen] = useState(false);
    const [isProfileSaveLoading, setIsProfileSaveLoading] = useState(false);
    const [achievements, setAchievements] = useState<TAcf>();

    const {
        MainPageState: { mainPageReducer: { data } },
    } = useAppSelector(state => state);
    const { topSideMenu } = useAppSelector(state => state.Global.getNavMenuReducer.data!.rawData);
    const { data: profileData, error, refreshData, isLoading } = useGetProfile();

    const idForReview = encodeToBase64(profileData?.rawData.login ?? '');

    useEffect(() => {
        (async () => {
            if (idForReview) {
                const { success, data: achievementsData } = await getUserAchievement(idForReview);

                if (success && achievementsData && achievementsData.length) {
                    setAchievements(achievementsData[0].acf);
                }
            }
        })();
    }, [idForReview]);

    const saveProfileHandler = async (profileInfo: putProfileRequestDto) => {
        setIsProfileSaveLoading(true);
        const { message } = await putProfile(profileInfo);

        if (message) {
            show(MessageType.Error, message);
        } else {
            closeModalHandler();

            if (profileInfo.id === userId() && profileInfo.password && topSideMenu) {
                window.location.href = topSideMenu.exitMenu.startMenu.href;
            }

            if (refreshData) {
                refreshData();
            }
        }

        setIsProfileSaveLoading(false);
    };

    const closeModalHandler = () => {
        setIsOpen(false);
    };

    const openModalHandler = () => {
        setIsOpen(true);
    };

    useEffect(() => {
        if (error) {
            show(MessageType.Error, error);
        }
    }, [error, show]);

    return (
        <>
            { (isLoading || isProfileSaveLoading) && <LoadingBounce /> }
            <Box display="flex">
                <div className={ styles.container }>
                    <div className={ styles.info }>
                        <FormAndLabel label={ <TextOut fontWeight={ 500 } fontSize={ 14 }>Данные о пользователе</TextOut> } className={ styles.formAndLabel }>
                            <TextOut>Ваш логин: { profileData?.rawData.login }</TextOut>
                            <TextOut>Ваша электронная почта: { profileData?.rawData.email } </TextOut>
                            <TextOut>Id для отзыва: { idForReview }</TextOut>
                        </FormAndLabel>
                        <FormAndLabel label={ <TextOut fontWeight={ 500 } fontSize={ 14 }>Данные о компании</TextOut> } className={ styles.formAndLabel }>
                            <TextOut>Название: { data?.accountUser.accountCaption } </TextOut>
                            <TextOut>ИНН: { data?.accountUser.accountInn } </TextOut>
                        </FormAndLabel>
                    </div>
                    <SuccessButton className={ styles.editButton } onClick={ openModalHandler }><i className="fa fa-edit" /> Редактировать</SuccessButton>
                </div>
                { achievements && (
                    <Box display="flex" flexDirection="column">
                        <img src={ achievements.user_achievement } alt={ achievements.user_status } loading="lazy" className={ styles.achievements } />
                        <TextOut textAlign="center" fontWeight={ 600 }>{ achievements.user_status }</TextOut>
                        <Box display="flex" alignItems="center" mt={ 1 }>
                            <Box position="relative" width="100%">
                                <LinearProgress
                                    sx={ {
                                        height: 10,
                                        borderRadius: 1
                                    } }
                                    value={ ((achievements.next_status_score - achievements.next_rate_score) / achievements.next_status_score) * 100 }
                                    variant="determinate"
                                />
                                <Box
                                    sx={ {
                                        position: 'absolute',
                                        left: '50%',
                                        top: '50%',
                                        transform: 'translate(-50%, -50.5%)',
                                        cursor: 'default'
                                    } }
                                >
                                    <TextOut fontSize={ 11 } style={ { color: COLORS.white } }>
                                        { achievements.next_status_score - achievements.next_rate_score }/{ achievements.next_status_score }
                                    </TextOut>
                                </Box>
                            </Box>
                        </Box>
                    </Box>
                ) }
            </Box>
            { profileData &&
                <EditProfileModal
                    isOpen={ isOpen }
                    loginUser={ profileData.rawData.login }
                    info={ profileData.rawData }
                    onCancelClick={ closeModalHandler }
                    save={ saveProfileHandler }
                />
            }
        </>
    );
});