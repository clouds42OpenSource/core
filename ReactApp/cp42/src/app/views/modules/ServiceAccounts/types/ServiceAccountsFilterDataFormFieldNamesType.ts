import { ServiceAccountsFilterDataForm } from 'app/views/modules/ServiceAccounts/types';

export type ServiceAccountsFilterDataFormFieldNamesType = keyof ServiceAccountsFilterDataForm;