import { SortingKind } from 'app/common/enums';
import { hasComponentChangesFor } from 'app/common/functions';
import { ProcessIdStateHelper } from 'app/common/helpers/ProcessIdStateHelper';
import { ServiceAccountsProcessId } from 'app/modules/serviceAccounts/ServiceAccountsProcessId';
import {
    DeleteServiceAccountThunkParams
} from 'app/modules/serviceAccounts/store/reducers/deleteServiceAccountReducer/params';
import {
    InsertServiceAccountThunkParams
} from 'app/modules/serviceAccounts/store/reducers/insertServiceAccountReducer/params';
import {
    ReceiveServiceAccountsThunkParams
} from 'app/modules/serviceAccounts/store/reducers/receiveServiceAccountsReducer/params';
import {
    DeleteServiceAccountThunk,
    InsertServiceAccountThunk,
    ReceiveServiceAccountsThunk
} from 'app/modules/serviceAccounts/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { DateUtility } from 'app/utils';
import { withHeader } from 'app/views/components/_hoc/withHeader';
import { ServiceAccountsFilterDataForm } from 'app/views/modules/ServiceAccounts/types';
import {
    AddServiceAccountDialogContentFormView
} from 'app/views/modules/ServiceAccounts/views/AddServiceAccountDialogContentFormView';
import { ServiceAccountsFilterFormView } from 'app/views/modules/ServiceAccounts/views/ServiceAccountsFilterFormView';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { TablePagination } from 'app/views/modules/_common/components/CommonTableWithFilter/TableView';
import { ServiceAccountItemDataModel } from 'app/web/InterlayerApiProxy/ServiceAccountsApiProxy/receiveServiceAccounts';
import { SelectDataCommonDataModel } from 'app/web/common/data-models';
import React from 'react';
import { connect } from 'react-redux';

import { SortingDataModel } from 'app/web/common/data-models/SortingDataModel';

type StateProps<TDataRow = ServiceAccountItemDataModel> = {
    dataset: TDataRow[];
    isInLoadingDataProcess: boolean;
    metadata: TablePagination;
};

type DispatchProps = {
    dispatchReceiveServiceAccountsThunk: (args: ReceiveServiceAccountsThunkParams) => void;
    dispatchDeleteServiceAccountThunk: (args: DeleteServiceAccountThunkParams) => void;
    dispatchInsertServiceAccountThunk: (args: InsertServiceAccountThunkParams) => void;

};

type AllProps = StateProps & DispatchProps;

class ServiceAccountsClass extends React.Component<AllProps> {
    public constructor(props: AllProps) {
        super(props);
        this.onDataSelect = this.onDataSelect.bind(this);
    }

    public shouldComponentUpdate(nextProps: AllProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private onDataSelect(args: SelectDataCommonDataModel<ServiceAccountsFilterDataForm>) {
        this.props.dispatchReceiveServiceAccountsThunk({
            pageNumber: args.pageNumber,
            filter: args.filter,
            orderBy: args.sortingData ? this.getSortQuery(args.sortingData) : 'creationDateTime.desc',
            force: true
        });
    }

    private getSortQuery(args: SortingDataModel) {
        switch (args.fieldName) {
            case 'accountIndexNumber':
                return `account.indexNumber.${ args.sortKind ? 'desc' : 'asc' }`;
            case 'accountCaption':
                return `account.accountCaption.${ args.sortKind ? 'desc' : 'asc' }`;
            case 'accountUserInitiatorName':
                return `accountUserInitiatorName.${ args.sortKind ? 'desc' : 'asc' }`;
            case 'creationDateTime':
                return `creationDateTime.${ args.sortKind ? 'desc' : 'asc' }`;
            default:
                break;
        }
    }

    public render() {
        return (
            <CommonTableWithFilter
                uniqueContextProviderStateId="ServiceAccountsContextProviderStateId"
                tableEditProps={ {
                    processProps: {
                        watch: {
                            reducerStateName: 'ServiceAccountsState',
                            processIds: {
                                addNewItemProcessId: ServiceAccountsProcessId.InsertServiceAccount,
                                deleteItemProcessId: ServiceAccountsProcessId.DeleteServiceAccount
                            }
                        },
                        processMessages: {
                            getOnSuccessAddNewItemMessage: () => 'Добавление нового служебного акаунта успешно завершено.',
                            getOnSuccessDeleteItemMessage: () => 'Удаление служебного акаунта успешно завершено.'
                        }
                    },
                    dialogProps: {
                        deleteDialog: {
                            getDialogTitle: () => 'Удаление служебного аккаунта',
                            getDialogContentView: itemToDelete => `Вы действительно хотите удалить служебный аккаунт "${ itemToDelete.accountCaption }" ?`,
                            confirmDeleteItemButtonText: 'Удалить',
                            onDeleteItemConfirmed: itemToDelete => {
                                this.props.dispatchDeleteServiceAccountThunk({
                                    accountId: itemToDelete.accountId,
                                    force: true
                                });
                            }
                        },
                        addDialog: {
                            getDialogTitle: () => 'Добавление служебного аккаунта',
                            getDialogContentFormView: ref => <AddServiceAccountDialogContentFormView ref={ ref } />,
                            confirmAddItemButtonText: 'Добавить',
                            showAddDialogButtonText: 'Добавить служебный аккаунт',
                            onAddNewItemConfirmed: itemToAdd => {
                                this.props.dispatchInsertServiceAccountThunk({
                                    accountId: itemToAdd.accountId,
                                    accountCaption: itemToAdd.accountCaption,
                                    accountIndexNumber: itemToAdd.accountIndexNumber,
                                    force: true
                                });
                                this.onDataSelect({
                                    pageNumber: 1
                                });
                            }
                        }
                    }
                } }
                filterProps={ {
                    getFilterContentView: (ref, onFilterChanged) => <ServiceAccountsFilterFormView ref={ ref } isFilterFormDisabled={ this.props.isInLoadingDataProcess } onFilterChanged={ onFilterChanged } />
                } }
                tableProps={ {
                    dataset: this.props.dataset,
                    keyFieldName: 'accountId',
                    sorting: {
                        sortFieldName: 'creationDateTime',
                        sortKind: SortingKind.Desc
                    },
                    fieldsView: {
                        accountIndexNumber: {
                            caption: '№',
                            fieldContentNoWrap: false,
                            isSortable: true,
                            fieldWidth: 'auto'
                        },
                        accountCaption: {
                            caption: 'Аккаунт',
                            fieldContentNoWrap: false,
                            isSortable: true,
                            fieldWidth: 'auto'
                        },
                        accountUserInitiatorName: {
                            caption: 'Инициатор создания',
                            fieldContentNoWrap: false,
                            isSortable: true,
                            fieldWidth: 'auto'
                        },
                        creationDateTime: {
                            caption: 'Дата создания',
                            fieldContentNoWrap: false,
                            isSortable: true,
                            fieldWidth: 'auto',
                            format: (value: Date) => {
                                return DateUtility.dateToDayMonthYear(value);
                            }
                        }
                    },
                    pagination: this.props.metadata
                } }
                onDataSelect={ this.onDataSelect }
            />
        );
    }
}

const ServiceAccountsConnected = connect<StateProps, DispatchProps, {}, AppReduxStoreState>(
    state => {
        const serviceAccountsState = state.ServiceAccountsState;
        const dataset = serviceAccountsState.serviceAccounts.records;

        const currentPage = serviceAccountsState.serviceAccounts.metadata.pageNumber;
        const totalPages = serviceAccountsState.serviceAccounts.metadata.pageCount;
        const { pageSize } = serviceAccountsState.serviceAccounts.metadata;
        const recordsCount = serviceAccountsState.serviceAccounts.metadata.totalItemCount;

        const isInLoadingDataProcess = ProcessIdStateHelper.isInProgress(serviceAccountsState.reducerActions, ServiceAccountsProcessId.ReceiveServiceAccounts);

        return {
            dataset,
            isInLoadingDataProcess,
            metadata: {
                currentPage,
                totalPages,
                pageSize,
                recordsCount
            }
        };
    },
    {
        dispatchReceiveServiceAccountsThunk: ReceiveServiceAccountsThunk.invoke,
        dispatchDeleteServiceAccountThunk: DeleteServiceAccountThunk.invoke,
        dispatchInsertServiceAccountThunk: InsertServiceAccountThunk.invoke
    }
)(ServiceAccountsClass);

export const ServiceAccountsView = withHeader({
    title: 'Управление служебными аккаунтами',
    page: ServiceAccountsConnected
});