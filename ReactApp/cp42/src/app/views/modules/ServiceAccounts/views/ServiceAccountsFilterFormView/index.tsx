import { hasComponentChangesFor } from 'app/common/functions';
import { TextBoxForm } from 'app/views/components/controls/forms/TextBoxForm';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import {
    ServiceAccountsFilterDataForm,
    ServiceAccountsFilterDataFormFieldNamesType
} from 'app/views/modules/ServiceAccounts/types';
import React from 'react';

/**
 * Свойства для компонента фильтра
 */
type OwnProps = ReduxFormProps<ServiceAccountsFilterDataForm> & {
    onFilterChanged: (filter: ServiceAccountsFilterDataForm, filterFormName: string) => void;
    isFilterFormDisabled: boolean;
};

class ServiceAccountsFilterFormViewClass extends React.Component<OwnProps> {
    public constructor(props: OwnProps) {
        super(props);
        this.initReduxForm = this.initReduxForm.bind(this);
        this.onValueChange = this.onValueChange.bind(this);
        this.onValueApplied = this.onValueApplied.bind(this);
    }

    public componentDidMount() {
        this.props.reduxForm.setInitializeFormDataAction(this.initReduxForm);
    }

    public shouldComponentUpdate(nextProps: OwnProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private onValueChange<TValue, TForm extends string = ServiceAccountsFilterDataFormFieldNamesType>(formName: TForm, newValue: TValue) {
        this.props.reduxForm.updateReduxFormFields({
            [formName]: newValue
        });

        if (formName === 'searchLine' && newValue && (newValue as any as string).length < 3) {
            return false;
        }
    }

    private onValueApplied<TValue>(formName: string, value: TValue) {
        this.props.onFilterChanged({
            ...this.props.reduxForm.getReduxFormFields(false),
            [formName]: value
        }, formName);
    }

    private initReduxForm(): ServiceAccountsFilterDataForm | undefined {
        return {
            searchLine: ''
        };
    }

    public render() {
        const formFields = this.props.reduxForm.getReduxFormFields(false);

        return (
            <TextBoxForm
                label="Поиск"
                formName="searchLine"
                onValueChange={ this.onValueChange }
                onValueApplied={ this.onValueApplied }
                value={ formFields.searchLine }
                isReadOnly={ this.props.isFilterFormDisabled }
                placeholder="Введите номер или название аккаунта"
            />
        );
    }
}

export const ServiceAccountsFilterFormView = withReduxForm(ServiceAccountsFilterFormViewClass, {
    reduxFormName: 'ServiceAcccounts_Filter_Form',
    resetOnUnmount: true
});