import { hasComponentChangesFor } from 'app/common/functions';
import { DynAutocompleteComboBoxForm } from 'app/views/components/controls/forms/ComboBox/DynAutocompleteComboBoxForm';
import { ComboboxItemModel } from 'app/views/components/controls/forms/ComboBox/models';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ServiceAccountItemDataModel } from 'app/web/InterlayerApiProxy/ServiceAccountsApiProxy/receiveServiceAccounts';
import { SearchAccountsItemDataModel } from 'app/web/InterlayerApiProxy/ServiceAccountsApiProxy/searchAccounts';
import { RequestKind } from 'core/requestSender/enums';
import React from 'react';

type OwnProps = ReduxFormProps<ServiceAccountItemDataModel>;

type OwnState = {
    accounts: Array<SearchAccountsItemDataModel>
};

class AddServiceAccountDialogContentFormViewClass extends React.Component<OwnProps, OwnState> {
    public constructor(props: OwnProps) {
        super(props);
        this.onValueChange = this.onValueChange.bind(this);
        this.searchAccounts = this.searchAccounts.bind(this);
        this.initReduxForm = this.initReduxForm.bind(this);
        this.props.reduxForm.setInitializeFormDataAction(this.initReduxForm);
    }

    public shouldComponentUpdate(nextProps: OwnProps, nextState: OwnState) {
        return hasComponentChangesFor(this.props, nextProps) ||
            hasComponentChangesFor(this.state, nextState);
    }

    private onValueChange(formName: string, originalItems: Array<SearchAccountsItemDataModel>, newValue: string) {
        const newValues: Partial<ServiceAccountItemDataModel & { [key: string]: any }> = {};

        const account = originalItems.find(item => item.accountId === newValue);

        newValues[formName] = newValue;
        newValues.accountIndexNumber = account?.accountIndexNumber;
        newValues.accountCaption = account?.accountCaption;

        this.props.reduxForm.updateReduxFormFields(newValues);
    }

    private foundAccountToComboboxItemModel(item: SearchAccountsItemDataModel): ComboboxItemModel<string> {
        return {
            value: item.accountId,
            text: item.accountCaption
        };
    }

    private initReduxForm(): ServiceAccountItemDataModel | undefined {
        return {
            accountId: '',
            accountCaption: '',
            accountIndexNumber: 0,
            accountUserInitiatorName: '',
            creationDateTime: new Date()
        };
    }

    private searchAccounts(searchLine: string, setItems: (items: Array<SearchAccountsItemDataModel>) => void) {
        const api = InterlayerApiProxy.getServiceAccountsApi();
        api.searchAccounts(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, { searchLine }).then(foundAccounts => { setItems(foundAccounts); });
    }

    public render() {
        const reduxForm = this.props.reduxForm.getReduxFormFields(false);

        return (
            <DynAutocompleteComboBoxForm
                autoFocus={ true }
                formName="accountId"
                label="Аккаунт"
                value={ reduxForm.accountId }
                onValueChange={ this.onValueChange }
                placeholder="Введите название, ID аккаунта"
                onGetItems={ this.searchAccounts }
                originalItemToComboboxItemModel={ this.foundAccountToComboboxItemModel }
            />
        );
    }
}

export const AddServiceAccountDialogContentFormView = withReduxForm(AddServiceAccountDialogContentFormViewClass, {
    reduxFormName: 'AddServiceAccount_ReduxForm',
    resetOnUnmount: true
});