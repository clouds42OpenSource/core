import { Box, Typography } from '@mui/material';
import { FETCH_API } from 'app/api/useFetchApi';
import { AppRoutes } from 'app/AppRoutes';
import { useQuery } from 'app/hooks/useQuery';
import { withHeader } from 'app/views/components/_hoc/withHeader';
import { SuccessButton } from 'app/views/components/controls/Button';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

const { getConfirmEmail } = FETCH_API.VERIFICATION_CODE;

const EmailVerify = () => {
    const code = useQuery().get('code');

    const [isLoading, setIsLoading] = useState(true);
    const [isSuccess, setIsSuccess] = useState(false);
    const [message, setMessage] = useState<string | null>(null);

    useEffect(() => {
        if (code) {
            getConfirmEmail(code, true)
                .then(response => {
                    if (response.success) {
                        setIsSuccess(true);
                    } else {
                        setMessage(response.message);
                    }
                })
                .then(() => setIsLoading(false));
        }
    });

    return (
        <Box minHeight="100vh" color="#333333">
            {
                isLoading
                    ? <LoadingBounce />
                    : (
                        <Box padding="40px">
                            <img height="50px" src={ `${ import.meta.env.REACT_APP_API_HOST }/img/registration/logo.png` } alt="42clouds" />
                            <Box display="flex" marginTop="40px" flexDirection="column" maxWidth="550px" gap="20px">
                                <Typography variant="h4" fontWeight="bold">
                                    { isSuccess ? 'Вы подтвердили почту!' : `Ошибка подтверждения почты (${ message })` }
                                </Typography>
                                <Typography variant="subtitle1">
                                    { !isSuccess && 'Попробуйте еще раз или обратитесь в тех поддержку '}
                                    <a style={ { color: '#1AB39F' } } href="mailto:support@42clouds.com">support@42clouds.com</a>
                                </Typography>
                                <Link to={ AppRoutes.homeRoute }>
                                    <SuccessButton>
                                        Перейти на главную
                                    </SuccessButton>
                                </Link>
                            </Box>
                        </Box>
                    )
            }
        </Box>
    );
};

export const EmailVerifyView = withHeader({
    title: 'Подтверждение почты',
    page: EmailVerify
});