type TConfig = {
    code: string;
    'short-name': string;
    name: string;
};

type TConfigs = { [k: string]: TConfig };

export const CONFIGS: TConfigs = {
    bp: {
        code: 'bp',
        'short-name': 'БП',
        name: 'Бухгалтерия Предприятия',
    },
    ut: {
        code: 'ut',
        'short-name': 'УТ',
        name: 'Управление Торговлей',
    },
    unf: {
        code: 'unf',
        'short-name': 'УНФ',
        name: 'Управление Небольшой Фирмой',
    },
    rt: {
        code: 'rt',
        'short-name': 'РТ',
        name: 'Розница',
    },
    zup: {
        code: 'zup',
        'short-name': 'ЗУП',
        name: 'Зарплата И Управление Персоналом',
    },
    ka: {
        code: 'ka',
        'short-name': 'КА',
        name: 'Комплексная Автоматизация',
    }
} as const;