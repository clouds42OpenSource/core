import { AccountUserGroup } from 'app/common/enums';

export const ADMIN_ROLES = [AccountUserGroup.AccountAdmin, AccountUserGroup.CloudAdmin, AccountUserGroup.Hotline];