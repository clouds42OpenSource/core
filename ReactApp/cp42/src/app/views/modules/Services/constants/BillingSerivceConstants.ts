export type TBillingServiceConstants = {
    [k: number]: string;
};

export const INTERNAL_CLOUD_SERVICE: TBillingServiceConstants = {
    0: '-- Не выбрано --',
    1: 'Деланс',
    2: 'Саюри',
    3: 'Кладовой',
    4: 'Управление бухгалтерией',
    5: 'Сервис управления и оптимизации налогов',
    6: 'Автоматическое подведение итогов встречи в телеграмм'
} as const;