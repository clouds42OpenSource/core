type TStatus = {
    label: string;
    value: string;
    disabled: boolean;
    checked: boolean;
};

type TStatuses = {
    [k: string]: TStatus;
};

export const SERVICE_STATUSES: TStatuses = {
    ВПроцессеУстановки: {
        label: 'В процессе установки',
        value: 'ВПроцессеУстановки',
        disabled: true,
        checked: false,
    },
    ОшибкаУстановки: {
        label: 'Ошибка установки',
        value: 'ОшибкаУстановки',
        disabled: true,
        checked: false,
    },
    Установлен: {
        label: 'Установлен',
        value: 'Установлен',
        disabled: false,
        checked: true,
    },
    НеУстановлен: {
        label: 'Не установлен',
        value: 'НеУстановлен',
        disabled: false,
        checked: false,
    },
    Удален: {
        label: 'Удален',
        value: 'Удален',
        disabled: true,
        checked: false,
    },
    ВПроцессеУдаления: {
        label: 'В процессе удаления',
        value: 'ВПроцессеУдаления',
        disabled: true,
        checked: false,
    }
} as const;