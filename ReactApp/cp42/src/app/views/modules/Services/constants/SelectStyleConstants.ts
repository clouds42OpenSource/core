export const SELECT_STYLE_CONSTANT = {
    '& legend': { display: 'none' },
    '& fieldset': { top: 0 },
    '.MuiOutlinedInput-notchedOutline': {
        transitionDuration: '0.2s',
    },
    '&.Mui-focused .MuiOutlinedInput-notchedOutline': {
        border: '1px solid #1ab394',
    },
    '&:hover .MuiOutlinedInput-notchedOutline': {
        border: '1px solid #1ab394',
    },
} as const;

export const SELECT_MENU_PROPS = {
    PaperProps: {
        style: {
            width: 250,
            maxHeight: 48 * 4.5 + 8
        }
    }
} as const;