type TDay = {
    label: string;
    formName: string;
};

export const DAYS: TDay[] = [
    { label: 'Пн', formName: '1' },
    { label: 'Вт', formName: '2' },
    { label: 'Ср', formName: '3' },
    { label: 'Чт', formName: '4' },
    { label: 'Пт', formName: '5' },
    { label: 'Сб', formName: '6' },
    { label: 'Вс', formName: '7' },
];