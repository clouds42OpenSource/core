import { AppRoutes } from 'app/AppRoutes';

export const SERVICES_ROUTES = {
    ServiceCardPageUrl: AppRoutes.services.myServices,
    EditServicePageUrl: AppRoutes.services.editServicePath
} as const;