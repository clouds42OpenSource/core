import { TFileInformationResponse, TServiceInfoResponse, TConfigurationsResponse } from 'app/web/api/ServicesProxy/response-dto';

export const ServiceInfoToFileInfo = (service: TServiceInfoResponse, configs: TConfigurationsResponse[]) => {
    const configurationKeys = Object.keys(service['configurations-min-versions']);
    const configurations = configurationKeys.map(key => configs.find(config => config.code === key));

    return {
        comment: service.comment,
        type: service.type,
        name: service.name,
        synonym: service.synonym,
        version: service.version,
        isNotSafe: service['unsafe-mode'] === true,
        commands: service.commands,
        configurations
    } as TFileInformationResponse;
};