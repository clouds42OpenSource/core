import { TVersionStatus } from 'app/views/modules/Services/types/globalServicesTypes';
import React, { memo } from 'react';
import { EFilterStatus } from 'app/views/modules/Services/views/EditServiceCard/ServiceVersions/index';

type TProps = {
    versionStatus: TVersionStatus;
};

export const VersionStatus = memo(({ versionStatus }: TProps) => {
    switch (versionStatus) {
        case EFilterStatus.Published:
            return <div style={ { color: '#1ab394' } }>{ versionStatus }</div>;
        case EFilterStatus.Waiting:
        case EFilterStatus.Processing:
            return <div style={ { color: '#f8ac59' } }>{ versionStatus }</div>;
        case EFilterStatus.Blocked:
        case EFilterStatus.Compatibility:
        case EFilterStatus.Declined:
            return <div style={ { color: '#ed5565' } }>{ versionStatus }</div>;
        default:
            return <div>{ versionStatus }</div>;
    }
});