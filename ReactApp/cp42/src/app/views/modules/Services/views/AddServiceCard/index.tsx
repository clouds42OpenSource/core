import { Link as MuiLink } from '@mui/material';
import { TourProps, withTour } from '@reactour/tour';
import { TGetServicesAudience } from 'app/api/endpoints/services/response';
import { AccountUserGroup } from 'app/common/enums';
import { getEmptyUuid } from 'app/common/helpers';

import uuid from 'app/common/helpers/GenerateUuidHelper/uuid';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { withHeader } from 'app/views/components/_hoc/withHeader';
import { ContainedButton, SuccessButton } from 'app/views/components/controls/Button';
import { CheckBoxForm } from 'app/views/components/controls/forms';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { TextOut } from 'app/views/components/TextOut';
import { EPartnersSteps, EPartnersTourId, ERefreshId } from 'app/views/Layout/ProjectTour/enums';
import { ConfigFileUploader } from 'app/views/modules/_common/components/ConfigFileUploader';
import { ImageCropper } from 'app/views/modules/_common/components/ImageCropper';
import { ImageGalleryUpload } from 'app/views/modules/_common/components/ImageGalleryUpload';
import { CONFIGS } from 'app/views/modules/Services/constants/ConfigConstants';
import { SERVICES_ROUTES } from 'app/views/modules/Services/constants/RoutesConstants';
import { TErrorMSType, TErrorType } from 'app/views/modules/Services/types/ErrorTypes';
import { BillingViewer, TBillingViewerState } from 'app/views/modules/Services/views/AddServiceCard/components/BillingViewer';
import { ConfigFileViewer } from 'app/views/modules/Services/views/AddServiceCard/components/ConfigFileViewer';
import { InputField } from 'app/views/modules/Services/views/AddServiceCard/components/InputField';
import { InstructionFileViewer } from 'app/views/modules/Services/views/AddServiceCard/components/InstructionFileViewer';
import { RichTextEditorField } from 'app/views/modules/Services/views/AddServiceCard/components/RichTextEditorField';
import { TextField } from 'app/views/modules/Services/views/AddServiceCard/components/TextField';
import styles from 'app/views/modules/Services/views/AddServiceCard/style.module.css';
import { ServicePreview } from 'app/views/modules/Services/views/ServicePreview';
import { TConfigurationsResponse, TFileInformationResponse, TModerationList, TServiceInformation, TServiceInformationDraftFiles } from 'app/web/api/ServicesProxy/response-dto';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { RequestKind } from 'core/requestSender/enums';
import React from 'react';
import { Link, Redirect } from 'react-router-dom';

type TField = {
    name: string;
    description: string;
};

type OwnProps = {
    isServiceHasBilling?: boolean;
    isEditMode: boolean;
    serviceId?: string;
    isModifyPage: boolean;
    isModeratePage: boolean;
    viewModeInfo?: TModerationList;
    refreshServiceData?: () => void;
    currentUserGroups?: AccountUserGroup[];
};

type TCreateServiceProps = FloatMessageProps & OwnProps;

export type TCreateServiceState = {
    fields: TField;
    isForMarket: boolean;
    hybrid: boolean;
    isMultipleFiles: boolean;
    configFiles: TFileInformationResponse[];
    screenshotsId: string[];
    logoId: string;
    richTextDescription: string;
    instruction: string;
    instructionUrl: string | null;
    isLoading: boolean;
    serviceInformation?: TServiceInformation;
    industry: string[];
    draft: boolean;
    isNeedToRefresh?: boolean;
    tags: string[];
    audience: TGetServicesAudience[] | string[];
    onSubmitPreview: boolean;
};

export class CreateService extends React.Component<TCreateServiceProps & Partial<TourProps>, TCreateServiceState> {
    public static defaultProps: TCreateServiceProps = {
        isEditMode: true,
        floatMessage: {
            show: () => void 0
        },
        isModifyPage: false,
        isModeratePage: false,
        currentUserGroups: []
    };

    private readonly nameRegex = /[/\\"']/;

    private readonly billingViewerRef;

    constructor(props: TCreateServiceProps & Partial<TourProps>) {
        super(props);
        this.billingViewerRef = React.createRef<BillingViewer>();
        this.state = {
            isForMarket: false,
            isMultipleFiles: false,
            hybrid: false,
            configFiles: [],
            screenshotsId: [],
            logoId: '',
            richTextDescription: '',
            fields: {
                name: '',
                description: ''
            },
            instruction: '',
            instructionUrl: null,
            isLoading: false,
            industry: [],
            draft: false,
            audience: [],
            tags: [],
            onSubmitPreview: false
        };
        this.getFileInformation = this.getFileInformation.bind(this);
        this.uploadFile = this.uploadFile.bind(this);
        this.imageGalleryUploadCallback = this.imageGalleryUploadCallback.bind(this);
        this.imageCropperCallback = this.imageCropperCallback.bind(this);
        this.marketCheckboxHandler = this.marketCheckboxHandler.bind(this);
        this.multipleFilesCheckboxHandler = this.multipleFilesCheckboxHandler.bind(this);
        this.configFileUploaderCallback = this.configFileUploaderCallback.bind(this);
        this.richTextEditorHandler = this.richTextEditorHandler.bind(this);
        this.inputNameHandler = this.inputNameHandler.bind(this);
        this.inputDescriptionHandler = this.inputDescriptionHandler.bind(this);
        this.instructionFileUploaderCallback = this.instructionFileUploaderCallback.bind(this);
        this.createNewService = this.createNewService.bind(this);
        this.configFileConfigurationsHandler = this.configFileConfigurationsHandler.bind(this);
        this.modifyService = this.modifyService.bind(this);
        this.getAllConfigs = this.getAllConfigs.bind(this);
        this.imageGalleryDeleteCallback = this.imageGalleryDeleteCallback.bind(this);
        this.checkFieldValues = this.checkFieldValues.bind(this);
        this.deleteInstruction = this.deleteInstruction.bind(this);
        this.checkBillingFieldValues = this.checkBillingFieldValues.bind(this);
        this.changeServiceStatus = this.changeServiceStatus.bind(this);
        this.receiveData = this.receiveData.bind(this);
        this.hybridCheckboxHandler = this.hybridCheckboxHandler.bind(this);
        this.handleIsNewHybrid = this.handleIsNewHybrid.bind(this);
        this.mappedFileData = this.mappedFileData.bind(this);
        this.instructionTextEditorHandler = this.instructionTextEditorHandler.bind(this);
        this.onSubmitPreview = this.onSubmitPreview.bind(this);
    }

    public componentDidMount() {
        this.receiveData();
    }

    public componentDidUpdate(_: Readonly<TCreateServiceProps>, prevState: Readonly<TCreateServiceState>) {
        if (!prevState.isForMarket && this.state.isForMarket && this.state.configFiles.find(configFile => configFile['market-service-already-exists'])) {
            this.props.floatMessage.show(MessageType.Warning, 'Данный сервис уже существует в Маркет42. Вы можете добавить его только как личный');
        }
    }

    private handleIsNewHybrid(isNewHybrid = false) {
        this.setState({ hybrid: isNewHybrid });
    }

    private getAllConfigs() {
        const allConfigs: TConfigurationsResponse[] = [];
        this.state.configFiles.forEach(configFile => allConfigs.push(...configFile.configurations));
        return allConfigs;
    }

    private async getFileInformation(file: File, isService: boolean) {
        const { show } = this.props.floatMessage;
        const { configFiles } = this.state;
        const uploadedFileId = await this.uploadFile(file, 'service');
        let fileInformation: TFileInformationResponse | null = null;

        try {
            if (uploadedFileId && isService) {
                this.setState({ isLoading: true });
                fileInformation = await InterlayerApiProxy.getServicesApiProxy().receiveConfigInfoFromMs(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, uploadedFileId);
            } else if (uploadedFileId && !isService) {
                return uploadedFileId;
            }

            if (fileInformation) {
                if ('message' in fileInformation) {
                    this.setState({ isLoading: false });

                    return show(MessageType.Error, fileInformation.message);
                }

                if (fileInformation.warning && !fileInformation['allow-loading']) {
                    this.setState({ isLoading: false });

                    return show(MessageType.Error, fileInformation.warning);
                }

                if (!fileInformation.name || !fileInformation.synonym || !fileInformation.version) {
                    this.setState({ isLoading: false });

                    return show(MessageType.Error, 'Было обнаружено пустое поле в файле разработки сервиса');
                }

                if (configFiles.length > 0 && configFiles[0].name !== fileInformation.name) {
                    if (fileInformation.type === 'cfe') {
                        this.setState({ isLoading: false });
                        return show(MessageType.Error, ' Название не совпадает с названием ранее загруженных версий файлов сервиса');
                    }

                    show(MessageType.Warning, ' Название не совпадает с названием ранее загруженных версий файлов сервиса');
                }

                if (fileInformation.warning && fileInformation['service-already-exists']) {
                    this.setState({ isLoading: false });

                    return show(MessageType.Error, fileInformation.warning);
                }

                if (this.state.isForMarket && fileInformation['market-service-already-exists']) {
                    show(MessageType.Warning, 'Данный сервис уже существует в Маркет42. Вы можете добавить его только как личный');
                }

                if (fileInformation.warning) {
                    show(MessageType.Info, fileInformation.warning);
                }

                this.setState({ isLoading: false });

                return {
                    ...fileInformation,
                    fileId: uploadedFileId,
                    configurations: [],
                    chosenCommands: []
                } as TFileInformationResponse;
            }

            show(MessageType.Info, 'Не удалось получить информацию о файле конфигурации');
            this.setState({ isLoading: false });

            return null;
        } catch (err: unknown) {
            show(MessageType.Error, 'Произошла непредвиденная ошибка');
        }
    }

    private uploadFile(file: File, type: 'service' | 'service_attachment') {
        this.setState({ isLoading: true });
        return InterlayerApiProxy.getServicesApiProxy().uploadFileToMs(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, file, type)
            .then(result => {
                this.setState({ isLoading: false });
                return result;
            });
    }

    private async mappedFileData(draftFiles: TServiceInformationDraftFiles[]) {
        const result = await InterlayerApiProxy.getServicesApiProxy().receiveConfigsFromMs(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY);

        try {
            const mappedFiles: TFileInformationResponse[] = (draftFiles ?? []).map(draftFile => ({
                type: draftFile.Type,
                name: draftFile.Name,
                synonym: draftFile.Synonym,
                version: draftFile.Version,
                warning: '',
                'allow-loading': true,
                'service-already-exists': false,
                'market-service-already-exists': false,
                'last-version-configurations': [],
                'unsafe-mode': draftFile.UnsafeMode ?? false,
                isNotSafe: draftFile.UnsafeMode ?? false,
                commands: draftFile.Commands ? draftFile.Commands.map(command => ({
                    name: command.Name,
                    presentation: command.Presentation,
                    'repeat-period-in-day': 0,
                    'days-repeat-period': 0,
                    'begin-time': '',
                    'end-time': '',
                    'week-days': [],
                })) : [],
                fileId: draftFile.FileId,
                configurations: draftFile.ConfigurationsMinVersions ? Object.entries(draftFile.ConfigurationsMinVersions).map(([code, version]) => {
                    const configInfo = result?.find(config => config.code === code) ?? CONFIGS[code];

                    return {
                        ...configInfo,
                        version
                    };
                }) : [],
                comment: draftFile.Comment,
                chosenCommands: draftFile.Commands ? draftFile.Commands.flatMap(command => {
                    if (command.Active) {
                        return {
                            name: command.Name,
                            presentation: command.Presentation,
                            'repeat-period-in-day': command.RepeatPeriodInDay!,
                            'days-repeat-period': command.DaysRepeatPeriod!,
                            'begin-time': command.BeginTime!,
                            'end-time': command.EndTime!,
                            'week-days': command.WeekDays!,
                        };
                    }

                    return [];
                }) : [],
                draft: true,
            }));

            this.setState({ configFiles: mappedFiles, isMultipleFiles: mappedFiles.length > 1 });
        } catch (err: unknown) {
            this.props.floatMessage.show(MessageType.Error, 'Произошла непредвиденная ошибка');
        }
    }

    private checkFieldValues(isModify: boolean) {
        let stringBuilder = '';
        const { show } = this.props.floatMessage;
        const { configFiles, fields: { name: serviceName, description }, isForMarket } = this.state;

        if ((!isModify && configFiles.length === 0) || serviceName.length < 3) {
            stringBuilder += 'Не загружен ни один конфигурационный файл или название сервиса содержит менее 3 символов.\n';
        }

        if (!isModify) {
            for (const { configurations, name, } of configFiles) {
                if (configurations.length === 0) {
                    stringBuilder += `Не заполнены совместимые конфигурации в файле ${ name }.\n`;
                }

                for (const { version, name: configName } of configurations) {
                    if (version) {
                        const isMatch = !!version.match(/^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$/);
                        if (!isMatch) {
                            stringBuilder += `Неверно заполнена минимальная версия конфигурации ${ configName } в файле ${ name }.\n`;
                        }
                    }
                }
            }
        }

        if (isForMarket) {
            const { logoId, instruction, richTextDescription, instructionUrl } = this.state;
            const billingData = this.billingViewerRef.current?.state;

            if (!description) {
                stringBuilder += 'Не заполнено краткое описание сервиса.\n';
            }

            if (!richTextDescription) {
                stringBuilder += 'Не заполнено поле возможностей сервиса.\n';
            }

            if (!instruction && !instructionUrl) {
                stringBuilder += 'Не заполнено поле инструкция сервиса.\n';
            }

            if (!billingData?.chosenAudience.length) {
                stringBuilder += 'Не выбрана категория "Для кого".\n';
            }

            if (!billingData?.chosenAudience.length) {
                stringBuilder += 'Не выбраны теги для сервиса.\n';
            }

            if (!logoId || logoId === getEmptyUuid()) {
                stringBuilder += 'Не загружен логотип сервиса.\n';
            }

            for (const { configurations, name, } of configFiles) {
                if (!configurations.length) {
                    stringBuilder += `Не заполнены совместимые конфигурации в файле ${ name }.\n`;
                }
            }
        }

        if (stringBuilder) {
            show(MessageType.Error, stringBuilder);
            return false;
        }

        return true;
    }

    private async checkBillingFieldValues(billingData: Readonly<TBillingViewerState>, serviceId: string, isCreate: boolean, hybrid: boolean, name?: string) {
        const { show } = this.props.floatMessage;
        let countConnectionType = 0;

        for (const serviceType of billingData.BillingServiceTypes) {
            if (serviceType.ConnectionToDatabaseType === 2) {
                countConnectionType++;
            }
        }

        if (billingData.BillingServiceTypes.length === countConnectionType) {
            show(MessageType.Error, 'Сервис должен иметь хотя бы одну услугу с типом подключения к информационной базе 1С');
            return false;
        }

        for (const serviceType of billingData.BillingServiceTypes) {
            if (this.nameRegex.test(serviceType.Name)) {
                show(MessageType.Error, `В названии услуги ${ serviceType.Name } указаны недопустимые символы / \\ ' "`);
                return false;
            }
        }

        return InterlayerApiProxy.getServicesApiProxy().receiveBillingValidation(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, billingData, serviceId, isCreate, hybrid, name)
            .then(result => {
                if (result && result.Success) {
                    return true;
                }
                show(MessageType.Error, result?.Message ?? 'Неверно заполнены поля биллинга сервиса');
                return false;
            });
    }

    private async createNewService(needCheckFields = true, isDraft = false) {
        const newServiceId = uuid();
        const { show } = this.props.floatMessage;
        const { isForMarket, hybrid, fields: { name } } = this.state;
        const billingData = isForMarket ? this.billingViewerRef.current?.state : undefined;

        if (needCheckFields && !this.checkFieldValues(false)) {
            return;
        }

        this.setState({ isLoading: true });

        if (needCheckFields && billingData) {
            const validationResult = await this.checkBillingFieldValues(billingData, newServiceId, true, hybrid, name);

            if (!validationResult) {
                this.setState({ isLoading: false });

                return;
            }
        }

        InterlayerApiProxy.getServicesApiProxy().createServiceCard(
            RequestKind.SEND_BY_USER_ASYNCHRONOUSLY,
            {
                ...this.state,
                draft: isDraft,
                industry: billingData?.chosenIndustry ?? [],
                tags: billingData?.chosenTags ?? [],
                audience: billingData?.chosenAudience ?? this.state.serviceInformation?.TargetAudience ?? []
            },
            billingData,
            isForMarket,
            this.state.serviceInformation?.ServiceID ?? newServiceId
        )
            .then(result => {
                if (result && !result.hasOwnProperty('Code') && !result.hasOwnProperty('error')) {
                    window.location.href = `${ SERVICES_ROUTES.ServiceCardPageUrl }/${ newServiceId }`;
                } else {
                    show(MessageType.Error, (result as TErrorType)?.Description ?? (result as TErrorMSType)?.message ?? 'Не удалось создать сервис');
                }

                this.setState({ isLoading: false });
            });
    }

    private async modifyService(needCheckFields = true, isDraft = false) {
        const { serviceInformation, isForMarket, hybrid, fields: { name } } = this.state;
        const { refreshServiceData, isServiceHasBilling, floatMessage: { show } } = this.props;
        const billingData = this.state.isForMarket ? this.billingViewerRef.current?.state : undefined;

        if (needCheckFields && !this.checkFieldValues(true)) {
            return;
        }

        this.setState({ isLoading: true });

        if (needCheckFields && billingData) {
            const validationResult = await this.checkBillingFieldValues(billingData, serviceInformation!.ServiceID, !(this.props.isServiceHasBilling), hybrid, name);

            if (!validationResult) {
                this.setState({ isLoading: false });

                return;
            }
        }

        InterlayerApiProxy.getServicesApiProxy().modifyServiceCard(
            RequestKind.SEND_BY_USER_ASYNCHRONOUSLY,
            {
                ...this.state,
                industry: billingData?.chosenIndustry ?? this.state.serviceInformation?.Industries ?? [],
                tags: billingData?.chosenTags ?? this.state.serviceInformation?.Tags ?? [],
                audience: billingData?.chosenAudience ?? this.state.serviceInformation?.TargetAudience ?? [],
                draft: isDraft
            },
            serviceInformation!.ServiceID,
            billingData,
            isServiceHasBilling
        )
            .then(result => {
                if (typeof result === 'string') {
                    this.setState({ isLoading: false });
                }

                if (result && !result.hasOwnProperty('Code') && !result.hasOwnProperty('error')) {
                    if (isForMarket && !isDraft) {
                        show(MessageType.Success, 'Заявка на модерацию успешно создана');
                    } else {
                        show(MessageType.Success, 'Данные успешно сохранены');
                    }

                    if (refreshServiceData) {
                        this.setState({ isNeedToRefresh: true });
                    }
                } else {
                    show(
                        MessageType.Error,
                        (result as TErrorType)?.Description ??
                        (result as TErrorMSType)?.message ??
                        'Не удалось обновить данные сервиса'
                    );
                }

                this.setState({ isLoading: false });
            });
    }

    private changeServiceStatus() {
        const { serviceId, floatMessage: { show }, isServiceHasBilling } = this.props;

        if (serviceId && isServiceHasBilling) {
            this.setState({ isLoading: true });

            InterlayerApiProxy.getServicesApiProxy().changeServiceActivity(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, {
                ServiceId: serviceId,
                IsActive: !this.state.isForMarket
            }).then(result => {
                if (result && result.Result && !result.hasOwnProperty('Code')) {
                    show(MessageType.Success, 'Доступность в Маркет 42 успешно изменена');
                    this.receiveData(true);
                } else {
                    show(MessageType.Error, (result as unknown as TErrorType)?.Description ?? 'Не удалось сменить статус активности сервиса');
                    this.setState({ isLoading: false });
                }
            });
        } else {
            this.marketCheckboxHandler();
        }
    }

    private imageCropperCallback(file: File) {
        this.uploadFile(file, 'service_attachment').then(logoId => {
            if (logoId) {
                this.setState({ logoId });
            } else {
                this.props.floatMessage.show(MessageType.Error, 'Не удалось сохранить логотип сервиса');
            }
        });
    }

    private receiveData(isNeedToRefresh = false) {
        const { isEditMode, viewModeInfo, serviceId, floatMessage: { show } } = this.props;
        if ((!isEditMode && !viewModeInfo) || isNeedToRefresh) {
            const serviceApi = InterlayerApiProxy.getServicesApiProxy();
            this.setState({ isLoading: true });
            serviceApi.receiveServiceInformation(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, serviceId!)
                .then(receivedInfo => {
                    if (receivedInfo) {
                        this.setState({ serviceInformation: receivedInfo.serviceInformation, ...receivedInfo.mappedServiceInformation });
                        void this.mappedFileData(receivedInfo.serviceInformation.DraftFiles);
                    } else {
                        show(MessageType.Error, 'Не удалось получить информацию о сервисе');
                    }
                });
        }

        if (viewModeInfo) {
            this.setState({
                draft: false,
                hybrid: false,
                industry: [],
                isForMarket: true,
                isMultipleFiles: false,
                configFiles: [],
                screenshotsId: [],
                logoId: '',
                richTextDescription: viewModeInfo['service-description'],
                fields: {
                    name: viewModeInfo['service-name'],
                    description: viewModeInfo['service-short-description']
                },
                instructionUrl: viewModeInfo['instruction-url'],
                instruction: viewModeInfo.instruction,
                isLoading: false,
                serviceInformation: {
                    Hybrid: false,
                    ServiceID: serviceId!,
                    ServiceName: viewModeInfo['service-name'],
                    ServiceShortDescription: viewModeInfo['service-short-description'],
                    ServiceDescription: viewModeInfo['service-description'],
                    Private: false,
                    ServiceIcon: viewModeInfo['service-icon'],
                    ServiceIconID: viewModeInfo['service-icon'],
                    ServiceScreenshots: viewModeInfo['service-screenshots'],
                    InstructionURL: viewModeInfo['instruction-url'],
                    Instruction: viewModeInfo.instruction,
                    InstructionID: viewModeInfo['instruction-url'],
                    Industries: viewModeInfo.industries,
                    Draft: viewModeInfo.draft,
                    DraftFiles: [],
                    Tags: viewModeInfo.tags,
                    TargetAudience: viewModeInfo['target-audience'],
                }
            });
        }

        this.setState({ isLoading: false });
    }

    private imageGalleryUploadCallback(ev: React.ChangeEvent<HTMLDivElement>) {
        const image = (ev.target as HTMLInputElement).files![0];
        return this.uploadFile(image, 'service_attachment').then(screenshotId => {
            if (screenshotId) {
                this.setState(prevState => ({ screenshotsId: [...prevState.screenshotsId, screenshotId] }));
                return true;
            }
            this.props.floatMessage.show(MessageType.Error, 'Не удалось сохранить скриншот сервиса');
            return false;
        });
    }

    private imageGalleryDeleteCallback(index: number) {
        const { screenshotsId, serviceInformation } = this.state;

        if (serviceInformation?.ServiceScreenshots) {
            serviceInformation.ServiceScreenshots.splice(index, 1);
            this.setState({ serviceInformation });
        }

        screenshotsId.splice(index, 1);
        this.setState({ screenshotsId });
    }

    private async configFileUploaderCallback(file: File, isService: boolean) {
        const configInformation = await this.getFileInformation(file, isService);

        if (typeof configInformation !== 'string' && configInformation) {
            this.setState(prevState => ({ configFiles: [...prevState.configFiles, configInformation] }));
        }
    }

    private instructionFileUploaderCallback(file: File) {
        this.uploadFile(file, 'service_attachment').then(result => {
            if (result) {
                this.setState({ instructionUrl: result });
            } else {
                this.props.floatMessage.show(MessageType.Error, 'Не удалось сохранить инструкцию сервиса');
            }
        });
    }

    private deleteInstruction() {
        this.setState({ instructionUrl: null });
    }

    private inputNameHandler(ev: React.ChangeEvent<HTMLInputElement>) {
        this.setState(prevState => ({ fields: { name: ev.target.value, description: prevState.fields.description } }));
    }

    private inputDescriptionHandler(ev: React.ChangeEvent<HTMLInputElement>) {
        this.setState(prevState => ({ fields: { name: prevState.fields.name, description: ev.target.value } }));
    }

    private marketCheckboxHandler() {
        if (this.props.meta === 'partners' && this.props.currentStep === EPartnersSteps.createServiceCheckbox && this.props.isOpen && this.props.setCurrentStep) {
            this.props.setCurrentStep(prev => prev + 1);
        }

        if (this.state.isForMarket) {
            this.setState({ hybrid: false });
        }

        this.setState(prevState => ({ isForMarket: !prevState.isForMarket }));
    }

    private hybridCheckboxHandler() {
        this.setState(prevState => ({ hybrid: !prevState.hybrid }));
    }

    private multipleFilesCheckboxHandler() {
        const { isMultipleFiles, configFiles } = this.state;

        if (isMultipleFiles && configFiles.length > 1) {
            this.setState({ configFiles: [configFiles[0]] });
        }

        this.setState({ isMultipleFiles: !isMultipleFiles });
    }

    private richTextEditorHandler(_: string, value: string) {
        this.setState({ richTextDescription: value });
    }

    private instructionTextEditorHandler(_: string, value: string) {
        this.setState({ instruction: value === '<p><br></p>' || value === '<p><br/></p>' ? '' : value });
    }

    private configFileConfigurationsHandler(configFiles: TFileInformationResponse[]) {
        this.setState({ configFiles });
    }

    private onSubmitPreview() {
        this.setState(prevState => ({ onSubmitPreview: !prevState.onSubmitPreview }));
    }

    public render() {
        const { isForMarket, isLoading, fields, serviceInformation, isMultipleFiles, configFiles, richTextDescription, instruction, instructionUrl, hybrid, draft } = this.state;
        const { isEditMode, isModifyPage, serviceId, floatMessage, isModeratePage, viewModeInfo, isServiceHasBilling } = this.props;
        const { minWidthLimit, minHeightLimit, maxHeightLimit, maxWidthLimit } = ImageCropper.defaultProps;
        const isCreatePageForBilling = (!isModifyPage && !isModeratePage) || !isServiceHasBilling;
        const fileTypes = configFiles.length > 0 ? configFiles[0].type === 'cfe' ? ['cfe'] : ['zip', 'epf', 'erf'] : ['zip', 'cfe', 'epf', 'erf'];

        if (this.state.isNeedToRefresh) {
            return <Redirect to={ window.location.pathname } />;
        }

        return (
            <div className={ styles.mainForm } data-refreshid={ ERefreshId.addServiceCard }>
                { isLoading && <LoadingBounce /> }
                <div className={ styles.childrenContainer } data-tourid={ EPartnersTourId.createServiceCheckbox }>
                    <CheckBoxForm
                        onValueChange={ this.marketCheckboxHandler }
                        isChecked={ isForMarket }
                        formName="isMarketService"
                        label="Сервис доступен в Маркет42"
                        className={ styles.mainForm__checkbox }
                        isReadOnly={ !isEditMode }
                    />
                    { this.props.children }
                </div>
                { isForMarket &&
                    <CheckBoxForm
                        onValueChange={ this.hybridCheckboxHandler }
                        isChecked={ hybrid }
                        formName="hybrid"
                        label="Гибридный режим"
                        className={ styles.mainForm__checkbox }
                        isReadOnly={ !isEditMode }
                    />
                }
                <InputField tourId={ EPartnersTourId.createServiceTitle } label="Название" fieldValue={ fields.name } changeHandler={ this.inputNameHandler } maxValue={ 100 } disabled={ !isEditMode } />
                <InputField label="Краткое описание" fieldValue={ fields.description } changeHandler={ this.inputDescriptionHandler } maxValue={ 200 } disabled={ !isEditMode } />
                { isForMarket &&
                    <RichTextEditorField label="Возможности" richTextEditorHandler={ this.richTextEditorHandler } disabled={ !isEditMode } value={ richTextDescription } />
                }
                <TextField
                    label="Логотип"
                    description={
                        `Загрузите изображение в формате .jpg, .jpeg, .png и разрешением не менее ${ minWidthLimit }
                        *${ minHeightLimit } и не более ${ maxWidthLimit }*${ maxHeightLimit }`
                    }
                    isDescriptionVisible={ isEditMode }
                />
                <ImageCropper floatMessage={ floatMessage } isEditMode={ isEditMode } image={ serviceInformation?.ServiceIcon } callBack={ this.imageCropperCallback } />
                <TextField
                    label="Изображение сервиса"
                    description="Вы можете добавить до 10 изображений вашего сервиса в формате .jpg, .jpeg, .png и разрешением от 640*480 до 1920*1080."
                    isDescriptionVisible={ isEditMode }
                />
                <ImageGalleryUpload
                    callBackDelete={ this.imageGalleryDeleteCallback }
                    images={ serviceInformation?.ServiceScreenshots }
                    isEditMode={ isEditMode }
                    callBack={ this.imageGalleryUploadCallback }
                    imageLimit={ 10 }
                    floatMessage={ floatMessage }
                />
                { (!isModifyPage || this.state.serviceInformation?.Draft) &&
                    <div className={ styles.uploadFileContainer } data-tourid={ EPartnersTourId.createServiceFile }>
                        { isEditMode && (
                            <FormAndLabel label="Файл разработки вашего сервиса">
                                <TextOut>
                                    Вы можете загрузить расширение .cfe, .epf или сделать комплект поставки .zip
                                    (для обработок, отчетов и печатных форм).
                                    Комплект поставки вам поможет сделать наша
                                </TextOut>
                                <MuiLink href="https://42clouds.com/ru-ru/techdocs/podgotovka-dopolnitelnyh-otchetov/" target="_blank"> инструкция</MuiLink>
                            </FormAndLabel>
                        ) }
                        <div className={ styles.fileSettingsContainer }>
                            <CheckBoxForm
                                onValueChange={ this.multipleFilesCheckboxHandler }
                                isChecked={ isMultipleFiles }
                                formName="isMultipleFiles"
                                label="У меня разные файлы для разных конфигураций"
                                className={ styles.mainForm__checkbox }
                            />
                            <MuiLink
                                className={ styles.href }
                                href="https://42clouds.com/ru-ru/techdocs/reporting-and-processing-requirements/"
                                rel="noreferrer"
                                target="_blank"
                            >
                                Требования к дополнительным отчётам и обработкам
                            </MuiLink>
                        </div>

                        { (configFiles.length === 0 || (configFiles.length < 6 && isMultipleFiles)) &&
                            <ConfigFileUploader callBack={ file => this.configFileUploaderCallback(file, true) } fileTypesArr={ fileTypes } />
                        }
                        <ConfigFileViewer
                            allConfigs={ this.getAllConfigs() }
                            isEditable={ true }
                            configFiles={ configFiles }
                            callBack={ this.configFileConfigurationsHandler }
                        />
                    </div>
                }
                { isForMarket &&
                    <BillingViewer
                        isCreatePage={ isCreatePageForBilling }
                        requestId={ viewModeInfo?.['proposal-id'] }
                        floatMessage={ floatMessage }
                        serviceName={ fields.name }
                        ref={ this.billingViewerRef }
                        serviceId={ serviceId }
                        isEditable={ isEditMode }
                        handleIsNewHybrid={ this.handleIsNewHybrid }
                        industry={ serviceInformation?.Industries }
                        serviceInformation={ serviceInformation }
                    />
                }
                {
                    isForMarket &&
                    ((!isEditMode && instruction) || draft || isEditMode) &&
                    <RichTextEditorField label="Инструкция" richTextEditorHandler={ this.instructionTextEditorHandler } disabled={ !isEditMode } value={ instruction } />
                }
                {
                    isForMarket &&
                    (!isEditMode && !instruction) &&
                    !isEditMode &&
                    !draft &&
                    <InstructionFileViewer
                        instruction={ instructionUrl }
                        isMode={ !!viewModeInfo }
                        isModeratePage={ isModeratePage }
                        isModifyPage={ isModifyPage }
                        isEditMode={ isEditMode }
                        fileUploadHandler={ this.instructionFileUploaderCallback }
                        deleteFileHandler={ this.deleteInstruction }
                        serviceInformation={ serviceInformation }
                    />
                }
                { !isModeratePage &&
                    <>
                        <TextOut>
                            По нажатию кнопки { isModifyPage ? '“Сохранить изменения“' : '“Отправить на аудит”' } ваш сервис будет отправлен нам на проверку.
                        </TextOut>
                        <TextOut>
                            В течение 3-х рабочих дней наши специалисты проведут тестирование системы с учетом внесенных изменений вашим файлом разработки.
                        </TextOut>
                    </>
                }
                <div className={ styles.buttonsContainer }>
                    { !isModeratePage &&
                        <Link
                            className={ styles.backLink }
                            to={ isModifyPage && !this.state.serviceInformation?.Draft ? `${ SERVICES_ROUTES.ServiceCardPageUrl }/${ serviceId }` : SERVICES_ROUTES.ServiceCardPageUrl }
                        >
                            <ContainedButton>Закрыть</ContainedButton>
                        </Link>
                    }
                    { !isModeratePage && this.state.isForMarket && ((isModifyPage && this.state.serviceInformation?.Draft) || !isModifyPage) &&
                        <ContainedButton
                            tourId={ EPartnersTourId.createServiceDraft }
                            isEnabled={ isEditMode && !(isForMarket && configFiles.find(configFile => configFile['market-service-already-exists'])) }
                            onClick={ () => (isModifyPage ? this.modifyService(false, true) : this.createNewService(false, true)) }
                        >
                            Сохранить как черновик
                        </ContainedButton>
                    }
                    {
                        (
                            isEditMode || ((this.props.currentUserGroups?.includes(AccountUserGroup.CloudAdmin) || this.props.currentUserGroups?.includes(AccountUserGroup.Hotline)) && this.props.viewModeInfo)
                        ) && this.state.isForMarket &&
                        <ServicePreview onSubmit={ this.onSubmitPreview } service={ this.state.serviceInformation } createService={ this.state } billingViewerRef={ this.billingViewerRef.current?.state } />
                    }
                    { !isModeratePage && (
                        isModifyPage
                            ? (
                                <SuccessButton isEnabled={ isEditMode } onClick={ () => this.modifyService() }>
                                    { this.state.serviceInformation?.Draft ? 'Отправить на аудит' : 'Сохранить изменения' }
                                </SuccessButton>
                            )
                            : (
                                <SuccessButton
                                    tourId={ EPartnersTourId.createServiceConclusion }
                                    isEnabled={ isEditMode && !(isForMarket && configFiles.find(configFile => configFile['market-service-already-exists'])) }
                                    onClick={ () => this.createNewService() }
                                >
                                    Отправить на аудит
                                </SuccessButton>
                            )
                    ) }
                </div>
            </div>
        );
    }
}

export const AddServicePageView = withHeader({
    title: 'Создание сервиса',
    page: withFloatMessages(withTour(CreateService)),
});