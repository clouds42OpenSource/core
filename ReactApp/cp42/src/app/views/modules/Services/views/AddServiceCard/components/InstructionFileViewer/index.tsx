import React from 'react';
import { TextField } from 'app/views/modules/Services/views/AddServiceCard/components/TextField';
import { ConfigFileUploader } from 'app/views/modules/_common/components/ConfigFileUploader';
import DownloadOutlinedIcon from '@mui/icons-material/DownloadOutlined';
import { TServiceInformation } from 'app/web/api/ServicesProxy/response-dto';
import DeleteIcon from '@mui/icons-material/Delete';
import styles from './style.module.css';

type TProps = {
    instruction: string | null;
    isMode: boolean;
    isModeratePage: boolean;
    isModifyPage: boolean;
    isEditMode: boolean;
    fileUploadHandler: (file: File) => void;
    deleteFileHandler: () => void;
    serviceInformation?: TServiceInformation;
};

export const InstructionFileViewer = ({
    instruction,
    isMode,
    isModeratePage,
    isEditMode,
    isModifyPage,
    serviceInformation,
    fileUploadHandler,
    deleteFileHandler
}: TProps) => {
    if (!instruction && !isMode && !isModeratePage) {
        return (
            <div className={ styles.instructionFileContainer }>
                <TextField
                    label="Инструкция по использованию вашего сервиса"
                    description="Инструкция поможет самостоятельно разобраться в использовании сервиса. Приложите файл в формате .pdf."
                    isDescriptionVisible={ isEditMode }
                />
                <ConfigFileUploader callBack={ fileUploadHandler } fileTypesArr={ ['pdf'] } />
            </div>
        );
    }

    if ((serviceInformation && serviceInformation.InstructionURL) || instruction) {
        return (
            <div className={ styles.uploadedInstructionContainer }>
                <label>Инструкция успешно загружена</label>
                <div className={ styles.iconsContainer }>
                    { isModifyPage && serviceInformation?.InstructionURL &&
                    <DownloadOutlinedIcon className={ styles.openIcon } onClick={ () => window.open(serviceInformation.InstructionURL) } />
                    }
                    { isEditMode && <DeleteIcon className={ styles.openIcon } onClick={ deleteFileHandler } /> }
                </div>
            </div>
        );
    }

    return null;
};