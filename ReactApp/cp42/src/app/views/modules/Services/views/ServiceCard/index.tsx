import { ELogEvent } from 'app/api/endpoints/global/enum';
import { FETCH_API } from 'app/api/useFetchApi';
import { COLORS } from 'app/utils';
import { AutoInstallService } from 'app/views/modules/_common/shared/AutoInstallService';
import React from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import { LoadingButton } from '@mui/lab';

import { ContainedButton } from 'app/views/components/controls/Button';
import { TServiceDatabase, TServiceInformation } from 'app/web/api/ServicesProxy/response-dto';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { RequestKind } from 'core/requestSender/enums';
import { withHeader } from 'app/views/components/_hoc/withHeader';
import { TServiceStatus } from 'app/views/modules/Services/types/globalServicesTypes';
import CustomizedSwitches from 'app/views/components/controls/Switch/views/SwitchViews';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { SERVICE_STATUSES } from 'app/views/modules/Services/constants/StatusConstants';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { TimerHelper } from 'app/common/helpers/TimerHelper';
import { AppReduxStoreState } from 'app/redux/types';
import { AccountUserGroup } from 'app/common/enums';
import { IMAGE_TEMPLATE } from 'app/views/modules/Services/constants/ImageTemplateConstants';
import { TErrorMSType } from 'app/views/modules/Services/types/ErrorTypes';
import { PageHeaderView } from 'app/views/components/_hoc/withHeader/PageHeaderView';
import { Dialog } from 'app/views/components/controls/Dialog';
import { TextOut } from 'app/views/components/TextOut';
import { SERVICES_ROUTES } from 'app/views/modules/Services/constants/RoutesConstants';

import styles from 'app/views/modules/Services/views/ServiceCard/style.module.css';

interface IRouterProps {
    id: string;
}

type TServiceCardState = {
    databasesList: TServiceDatabase[];
    serviceInfo?: TServiceInformation;
    isLoading: boolean;
    isModalOpen: boolean;
};

type TReduxProps = {
    currentUserGroup: AccountUserGroup[];
};

type TServiceCardProps = RouteComponentProps<IRouterProps> & FloatMessageProps & TReduxProps;

const { sendLogEvent } = FETCH_API.GLOBAL;

class ServiceCardClass extends React.Component<TServiceCardProps, TServiceCardState> {
    private readonly id: string;

    private intervalId: NodeJS.Timeout | number;

    constructor(props: TServiceCardProps) {
        super(props);
        this.state = {
            databasesList: [],
            isLoading: true,
            isModalOpen: false
        };
        this.id = this.props.match.params.id;
        this.intervalId = setInterval(() => void 0, TimerHelper.waitTimeRefreshData);
        this.receiveDatabases = this.receiveDatabases.bind(this);
        this.updateStatus = this.updateStatus.bind(this);
        this.receiveInformation = this.receiveInformation.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.handleOpenModal = this.handleOpenModal.bind(this);
    }

    public componentDidMount() {
        this.receiveInformation();
        this.receiveDatabases();

        if (this.intervalId) {
            clearInterval(this.intervalId);
        }

        this.intervalId = setInterval(() => this.receiveDatabases(), TimerHelper.waitTimeForRefreshDatabaseInformation);
    }

    public componentWillUnmount() {
        clearInterval(this.intervalId);
    }

    private handleOpenModal(isOpen: boolean) {
        return () => this.setState({ isModalOpen: isOpen });
    }

    private handleDelete() {
        this.setState({ isLoading: true });
        const { floatMessage, history } = this.props;

        InterlayerApiProxy.getServicesApiProxy().deleteServiceInfo(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, this.id)
            .then(result => {
                if (result && !Object.hasOwn(result, 'error')) {
                    this.setState({ isLoading: false });
                    void sendLogEvent(ELogEvent.DeletePrivateService, `Удален сервис "${ this.state.serviceInfo?.ServiceName ?? '' }".`);
                    history.push(SERVICES_ROUTES.ServiceCardPageUrl);
                } else {
                    floatMessage.show(MessageType.Error, (result as unknown as TErrorMSType)?.message ?? 'Не удалось удалить сервис');
                    this.setState({ isLoading: false });
                }
            });
    }

    private receiveDatabases() {
        this.setState({ isLoading: true });
        const { floatMessage: { show } } = this.props;

        InterlayerApiProxy.getServicesApiProxy().receiveServiceDatabases(this.id)
            .then(result => {
                if (result) {
                    this.setState({ databasesList: result.Result });
                } else {
                    show(MessageType.Error, 'Не удалось получить список баз сервиса');
                }

                this.setState({ isLoading: false });
            });
    }

    private receiveInformation() {
        const { floatMessage: { show }, history: { goBack } } = this.props;

        InterlayerApiProxy.getServicesApiProxy().receiveServiceInfo(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, this.id)
            .then(result => {
                if (result) {
                    this.setState({ serviceInfo: result.Result });
                } else {
                    show(MessageType.Error, 'Не удалось получить информацию о сервисе');
                    setTimeout(() => {
                        goBack();
                    }, 1000);
                }
            });
    }

    private updateStatus(value: string, data: TServiceDatabase) {
        const { databasesList } = this.state;
        const { show } = this.props.floatMessage;
        const index = databasesList.indexOf(data);
        const dbList = databasesList;
        const backupValue = dbList[index].AccountDatabaseServiceState;
        this.setState({ isLoading: true });

        switch (value) {
            case 'НеУстановлен':
                dbList[index].AccountDatabaseServiceState = 'ВПроцессеУстановки';
                this.setState({ databasesList: dbList });
                InterlayerApiProxy.getServicesApiProxy().installServiceIntoApplication(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, data.AccountDatabaseID, this.id)
                    .then(result => {
                        if (!result) {
                            dbList[index].AccountDatabaseServiceState = backupValue;
                            this.setState({ databasesList: dbList });
                            show(MessageType.Error, 'Не удалось сменить статус');
                        } else {
                            void sendLogEvent(ELogEvent.InstallServiceInDatabase, `Изменен статус сервиса "${ this.state.serviceInfo?.ServiceName ?? '' }" в базе "${ dbList[index].AccountDatabaseName }".`);
                        }
                    });
                break;
            case 'Установлен':
                dbList[index].AccountDatabaseServiceState = 'ВПроцессеУдаления';
                this.setState({ databasesList: dbList });
                InterlayerApiProxy.getServicesApiProxy().deleteServiceFromApplication(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, data.AccountDatabaseID, this.id)
                    .then(result => {
                        if (!result) {
                            dbList[index].AccountDatabaseServiceState = backupValue;
                            this.setState({ databasesList: dbList });
                            show(MessageType.Error, 'Не удалось сменить статус');
                        } else {
                            void sendLogEvent(ELogEvent.InstallServiceInDatabase, `Изменен статус сервиса "${ this.state.serviceInfo?.ServiceName ?? '' }" в базе "${ dbList[index].AccountDatabaseName }".`);
                        }
                    });
                break;
            default:
                break;
        }

        this.setState({ isLoading: false });
    }

    public render() {
        const { isLoading, serviceInfo, databasesList, isModalOpen } = this.state;
        const isEnabled = !!serviceInfo?.ServiceID;
        const isAllowedDelete = this.props.currentUserGroup.some(group => AccountUserGroup.CloudAdmin === group || (AccountUserGroup.AccountAdmin === group && this.state.serviceInfo?.Private));

        return (
            <>
                <Dialog
                    isOpen={ isModalOpen }
                    dialogWidth="xs"
                    title="Удаление сервиса"
                    isTitleSmall={ true }
                    onCancelClick={ this.handleOpenModal(false) }
                    className={ styles.dialog }
                    buttons={ [
                        {
                            content: 'Отмена',
                            variant: 'contained',
                            onClick: this.handleOpenModal(false),
                            kind: 'default'
                        },
                        {
                            content: 'Ок',
                            variant: 'contained',
                            onClick: () => this.handleDelete(),
                            kind: 'success'
                        }
                    ] }
                >
                    <TextOut>Вы действительно хотите удалить сервис? После удаления все заявки на модерацию этого сервиса будут автоматически отклонены.</TextOut>
                </Dialog>
                <div className={ styles.mainContainer }>
                    <PageHeaderView title={ `${ serviceInfo?.ServiceName ?? 'Сервис' }` } />
                    { isLoading && <LoadingBounce /> }
                    <div className={ styles.cardHeader }>
                        <img className={ styles.logo } src={ serviceInfo?.ServiceIcon ?? IMAGE_TEMPLATE } alt="service icon" />
                        <div className={ styles.cardView }>
                            <div className={ styles.cardDescription }>
                                <span>{ serviceInfo?.ServiceShortDescription }</span>
                            </div>
                            <div className={ styles.serviceActionButtons }>
                                { isEnabled
                                    ? (
                                        <Link to={ `${ SERVICES_ROUTES.EditServicePageUrl }/${ this.id }` } style={ { textDecoration: 'none' } }>
                                            <ContainedButton className={ styles.button } kind="success" isEnabled={ isEnabled }>Карточка сервиса</ContainedButton>
                                        </Link>
                                    )
                                    : (
                                        <ContainedButton className={ styles.button } kind="success" isEnabled={ false }>Карточка сервиса</ContainedButton>
                                    )
                                }
                                { isAllowedDelete && <ContainedButton className={ styles.button } isEnabled={ isEnabled } kind="error" onClick={ this.handleOpenModal(true) }>Удалить сервис</ContainedButton> }
                                <Link to={ SERVICES_ROUTES.ServiceCardPageUrl } style={ { textDecoration: 'none', color: 'inherit' } }>
                                    <ContainedButton className={ styles.button }>Закрыть</ContainedButton>
                                </Link>
                            </div>
                        </div>
                    </div>
                    <AutoInstallService id={ this.id } />
                    <CommonTableWithFilter
                        isResponsiveTable={ true }
                        isBorderStyled={ true }
                        uniqueContextProviderStateId="serviceCardTableContextProviderTextId"
                        className={ styles.tableContainer }
                        tableProps={ {
                            dataset: databasesList,
                            emptyText: 'Нет версий прошедших аудит или баз для установки сервиса',
                            keyFieldName: 'AccountDatabaseID',
                            isResponsiveTable: false,
                            fieldsView: {
                                AccountDatabaseName: {
                                    caption: 'Название',
                                    fieldWidth: '25%',
                                    fieldContentNoWrap: false,
                                    format: (value: string, _) =>
                                        <span className={ styles.tableElement + styles.bold }>{ value }</span>
                                },
                                ConfigurationName: {
                                    caption: 'Конфигурация',
                                    fieldWidth: '30%',
                                    fieldContentNoWrap: false,
                                    format: (value: string, _) =>
                                        <span className={ styles.tableElement }>{ value }</span>
                                },
                                LastActivityDate: {
                                    caption: 'Активность',
                                    fieldWidth: '25%',
                                    fieldContentNoWrap: false,
                                    format: (value: string, _) =>
                                        <span className={ styles.tableElement }>
                                            { new Date(value).toLocaleDateString('ru-RU', { year: 'numeric', month: 'numeric', day: 'numeric', hour: '2-digit', minute: '2-digit' }) }
                                        </span>
                                },
                                AccountDatabaseServiceState: {
                                    caption: 'Статус',
                                    fieldWidth: '20%',
                                    fieldContentNoWrap: false,
                                    format: (value: TServiceStatus, data) => {
                                        if (SERVICE_STATUSES[value]) {
                                            if (!SERVICE_STATUSES[value].disabled) {
                                                return (
                                                    <CustomizedSwitches
                                                        checked={ SERVICE_STATUSES[value].checked }
                                                        onChange={ () => this.updateStatus(SERVICE_STATUSES[value].value, data) }
                                                        disabled={ SERVICE_STATUSES[value].disabled }
                                                    />
                                                );
                                            }

                                            if (value === 'ВПроцессеУстановки' || value === 'ВПроцессеУдаления') {
                                                return (
                                                    <LoadingButton loading={ true } variant="text" className={ styles.loading }>{ SERVICE_STATUSES[value].label }</LoadingButton>
                                                );
                                            }

                                            return (
                                                <TextOut fontWeight={ 700 } style={ { color: COLORS.error } }>{ SERVICE_STATUSES[value].label }</TextOut>
                                            );
                                        }

                                        return null;
                                    }
                                }
                            }
                        } }
                    />
                </div>
            </>
        );
    }
}

const ServiceCardConnected = connect<TReduxProps, {}, {}, AppReduxStoreState>(
    state => ({ currentUserGroup: state.Global.getCurrentSessionSettingsReducer.settings.currentContextInfo.currentUserGroups }), {}
)(ServiceCardClass);

export const ServiceCardPageView = withHeader({
    title: 'Сервис',
    page: withRouter(withFloatMessages(ServiceCardConnected)),
});