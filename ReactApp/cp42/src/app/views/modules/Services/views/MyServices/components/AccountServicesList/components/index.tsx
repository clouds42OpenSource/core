import React from 'react';
import { useHistory } from 'react-router';

import { REDUX_API } from 'app/api/useReduxApi';
import { AppRoutes } from 'app/AppRoutes';
import { useAppDispatch, useAppSelector } from 'app/hooks';
import { SuccessButton } from 'app/views/components/controls/Button';
import { TPartnersHistory } from 'app/views/modules/ToPartners/config/types';

export const CreateServiceButton = () => {
    const dispatch = useAppDispatch();
    const history = useHistory<TPartnersHistory>();

    const { data } = useAppSelector(state => state.PartnersState.agencyAgreementStatusReducer);

    const redirectHandler = () => {
        REDUX_API.PARTNERS_REDUX.getAgencyAgreementStatus(dispatch);

        if (data && !data.rawData) {
            history.push(AppRoutes.services.createService);
        }
    };

    return (
        <SuccessButton onClick={ redirectHandler }>Создать сервис</SuccessButton>
    );
};