import { ExpandMore } from '@mui/icons-material';
import DeleteIcon from '@mui/icons-material/Delete';
import {
    Accordion,
    AccordionDetails,
    AccordionSummary,
    Box,
    Chip,
    Divider,
    FormControl,
    FormControlLabel,
    FormGroup,
    InputLabel,
    MenuItem,
    OutlinedInput,
    Radio,
    RadioGroup,
    Select,
    SelectChangeEvent,
    ToggleButton,
    ToggleButtonGroup
} from '@mui/material';
import { TGetServicesAudience } from 'app/api/endpoints/services/response';
import { FETCH_API } from 'app/api/useFetchApi';
import uuid from 'app/common/helpers/GenerateUuidHelper/uuid';
import { localStorageHelper } from 'app/common/helpers/LocalStorageHelper';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { ContainedButton, SuccessButton } from 'app/views/components/controls/Button';
import { TextOut } from 'app/views/components/TextOut';
import { EPartnersTourId } from 'app/views/Layout/ProjectTour/enums';
import { SELECT_MENU_PROPS, SELECT_STYLE_CONSTANT } from 'app/views/modules/Services/constants/SelectStyleConstants';
import styles from 'app/views/modules/Services/views/AddServiceCard/components/BillingViewer/style.module.css';
import { InputField } from 'app/views/modules/Services/views/AddServiceCard/components/InputField';
import { TagsSelect } from 'app/views/modules/Services/views/AddServiceCard/components/TagsSelect';
import { TBillingServiceType, TServiceBillingConnectionTypeResponse, TServiceBillingDataResponse, TServiceIndustriesResponse, TServiceInformation } from 'app/web/api/ServicesProxy/response-dto';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { RequestKind } from 'core/requestSender/enums';
import React from 'react';

type TBillingViewerProps = FloatMessageProps & {
    isEditable: boolean;
    serviceId?: string;
    serviceName: string;
    industry?: string[];
    serviceInformation?: TServiceInformation;
    requestId?: string;
    isCreatePage: boolean;
    handleIsNewHybrid: (isNewHybrid: boolean) => void
};

export type TBillingViewerState = TServiceBillingDataResponse & TServiceBillingConnectionTypeResponse & {
    industries: TServiceIndustriesResponse[];
    tags: string[];
    audience: TGetServicesAudience[];
    chosenIndustry: string[];
    chosenAudience: string[];
    chosenTags: string[];
};

const { getServicesTags, getServicesAudience } = FETCH_API.SERVICES;

export class BillingViewer extends React.Component<TBillingViewerProps, TBillingViewerState> {
    private readonly messageErrors = {
        billingErr: 'Не удалось получить информацию о биллинге сервиса',
        connectionErr: 'Не удалось получить информацию о типах подключения',
        industryErr: 'Не удалось получить список отраслей'
    };

    constructor(props: TBillingViewerProps) {
        super(props);
        this.state = {
            AccountId: localStorageHelper.getAccountId() ?? '',
            AvailabilityChangeServiceRequestOnModeration: true,
            BeforeDeletingServiceTypesMonthsCount: 1,
            BeforeNextEditServiceCostMonthsCount: 3,
            BillingServiceStatus: 2,
            BillingServiceTypes: [],
            CanEditServiceCost: true,
            Id: '',
            InternalCloudService: null,
            IsActive: false,
            IsExistsChangeServiceRequest: true,
            Key: '',
            LinkToActivateDemoPeriod: '',
            MessageStatingThatServiceIsBlocked: '',
            Name: '',
            NextPossibleEditServiceCostDate: '',
            ReferralLinkForMarket: '',
            ServiceOwnerEmail: '',
            MyEntUserWebServiceTypeId: '',
            MyEntUserServiceTypeId: '',
            industries: [],
            audience: [],
            tags: [],
            chosenIndustry: props.industry ?? [],
            chosenAudience: (props.serviceInformation?.TargetAudience ?? []) as string[],
            chosenTags: props.serviceInformation?.Tags ?? [],
            IsHybridService: false
        };
        this.renderBillingAdditionalServices = this.renderBillingAdditionalServices.bind(this);
        this.addBillingAdditionalService = this.addBillingAdditionalService.bind(this);
        this.deleteBillingAdditionalService = this.deleteBillingAdditionalService.bind(this);
        this.billingAdditionalServiceNameHandler = this.billingAdditionalServiceNameHandler.bind(this);
        this.billingAdditionalServiceTypeHandler = this.billingAdditionalServiceTypeHandler.bind(this);
        this.billingAdditionalServiceConnectionTypeHandler = this.billingAdditionalServiceConnectionTypeHandler.bind(this);
        this.billingAdditionalServicePriceHandler = this.billingAdditionalServicePriceHandler.bind(this);
        this.renderFirstBillingInfo = this.renderFirstBillingInfo.bind(this);
        this.billingAdditionalServiceDescriptionHandler = this.billingAdditionalServiceDescriptionHandler.bind(this);
        this.renderServicesAdditionalCombination = this.renderServicesAdditionalCombination.bind(this);
        this.renderSelectServicesAdditionalCombination = this.renderSelectServicesAdditionalCombination.bind(this);
        this.countServicePrice = this.countServicePrice.bind(this);
        this.addServiceTypeRelation = this.addServiceTypeRelation.bind(this);
        this.removeServiceTypeRelation = this.removeServiceTypeRelation.bind(this);
        this.onIndustrySelect = this.onIndustrySelect.bind(this);
        this.checkOptions = this.checkOptions.bind(this);
        this.getBillingInfo = this.getBillingInfo.bind(this);
        this.onAudienceSelect = this.onAudienceSelect.bind(this);
        this.onTagsSelect = this.onTagsSelect.bind(this);
    }

    public componentDidMount() {
        const { receiveBillingModeration, receiveBillingConnectionType, receiveServiceIndustries } = InterlayerApiProxy.getServicesApiProxy();
        const { floatMessage: { show }, requestId, industry } = this.props;
        const { billingErr, connectionErr, industryErr } = this.messageErrors;

        try {
            this.getBillingInfo();

            if (requestId) {
                receiveBillingModeration(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, requestId)
                    .then(receivedBillingModeration => {
                        if (receivedBillingModeration && typeof receivedBillingModeration !== 'string') {
                            this.setState(receivedBillingModeration.NewServiceData ?? receivedBillingModeration.CurrentServiceData);
                            const isNewHybrid = receivedBillingModeration.NewServiceData
                                ? receivedBillingModeration.NewServiceData.IsHybridService
                                : receivedBillingModeration.CurrentServiceData.IsHybridService;

                            this.props.handleIsNewHybrid(isNewHybrid);
                        } else {
                            show(MessageType.Error, receivedBillingModeration);
                        }
                    });
            }

            receiveBillingConnectionType(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY)
                .then(result => {
                    if (result) {
                        this.setState({ ...result });
                    } else {
                        show(MessageType.Error, connectionErr);
                    }
                });

            receiveServiceIndustries(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY)
                .then(result => {
                    if (result) {
                        this.setState({ industries: result });

                        if (industry) {
                            this.setState({ chosenIndustry: result.filter(industryObj => industry.includes(industryObj.id)).map(ind => ind.id) ?? [] });
                        }
                    } else {
                        show(MessageType.Error, industryErr);
                    }
                });

            getServicesTags()
                .then(result => {
                    if (result.success && result.data) {
                        this.setState({ tags: result.data.Tags });
                    } else {
                        show(MessageType.Error, result.message);
                    }
                });

            getServicesAudience()
                .then(result => {
                    if (result.success && result.data) {
                        this.setState({ audience: result.data });
                    } else {
                        show(MessageType.Error, result.message);
                    }
                });
        } catch (err: unknown) {
            show(MessageType.Error, 'Произошла непредвиденная ошибка');
        }
    }

    public componentDidUpdate(prevProps: Readonly<TBillingViewerProps>, _: Readonly<TBillingViewerState>) {
        const { MyEntUserWebServiceTypeId, MyEntUserServiceTypeId } = this.state;

        if (this.state.BillingServiceTypes.length === 0 && MyEntUserWebServiceTypeId && MyEntUserServiceTypeId) {
            this.addBillingAdditionalService({ MyEntUserWebServiceTypeId, MyEntUserServiceTypeId });
        }

        if (this.props.isCreatePage !== prevProps.isCreatePage) {
            this.getBillingInfo();
        }
    }

    private getBillingInfo() {
        const { serviceId, requestId, isCreatePage, floatMessage: { show } } = this.props;

        if (serviceId && !requestId && !isCreatePage) {
            InterlayerApiProxy.getServicesApiProxy().receiveBillingServiceData(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, serviceId ?? '')
                .then(receivedBilling => {
                    if (receivedBilling && typeof receivedBilling !== 'string') {
                        this.setState(receivedBilling);
                    } else {
                        show(MessageType.Error, receivedBilling);
                    }
                });
        }
    }

    private onIndustrySelect(ev: SelectChangeEvent<string[]>) {
        if (Array.isArray(ev.target.value)) {
            this.setState({ chosenIndustry: ev.target.value });
        }
    }

    private onAudienceSelect(ev: SelectChangeEvent<string[]>) {
        if (Array.isArray(ev.target.value)) {
            this.setState({ chosenAudience: ev.target.value });
        }
    }

    private onTagsSelect(ev: SelectChangeEvent<string[]>) {
        if (Array.isArray(ev.target.value)) {
            this.setState({ chosenTags: ev.target.value });
        }
    }

    private deleteBillingAdditionalService(index: number) {
        const deletedItem = this.state.BillingServiceTypes.find((_, i) => i === index);

        if (deletedItem && !deletedItem.isNew) {
            this.props.floatMessage.show(MessageType.Warning, `Внимание!
            Все администраторы аккаунтов, которые оплачивают или тестируют на демо-периоде ваш сервис, получат уведомление об удалении услуги.
            Фактическое удаление услуги будет выполняться спустя 1 месяц от даты списания клиента.
            Для клиентов, которые уже перестали пользоваться вашим сервисом, удаление услуги будет выполнено сразу же после успешной модерации.`, 10000);
        }

        this.setState({ BillingServiceTypes: this.state.BillingServiceTypes.filter((type, i) => {
            if (deletedItem && type.ServiceTypeRelations.includes(deletedItem.Id)) {
                type.ServiceTypeRelations = type.ServiceTypeRelations.filter(relation => relation !== deletedItem.Id);
            }

            return i !== index;
        })
        });
    }

    private billingAdditionalServiceNameHandler(ev: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>, index: number) {
        const extensionsFromState = this.state.BillingServiceTypes;
        extensionsFromState[index].Name = ev.target.value;
        this.setState({ BillingServiceTypes: extensionsFromState });
    }

    private billingAdditionalServiceDescriptionHandler(ev: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>, index: number) {
        const extensionsFromState = this.state.BillingServiceTypes;
        extensionsFromState[index].Description = ev.target.value;
        this.setState({ BillingServiceTypes: extensionsFromState });
    }

    private billingAdditionalServiceTypeHandler(index: number) {
        const extensionsFromState = this.state.BillingServiceTypes;
        extensionsFromState[index].BillingType = this.state.BillingServiceTypes[index].BillingType === 0 ? 1 : 0;
        this.setState({ BillingServiceTypes: extensionsFromState });
    }

    private billingAdditionalServiceConnectionTypeHandler(ev: React.ChangeEvent<HTMLInputElement>, index: number) {
        const extensionsFromState = this.state.BillingServiceTypes;
        extensionsFromState[index].ConnectionToDatabaseType = parseInt(ev.target.value, 10) as 0 | 1 | 2;

        switch (ev.target.value) {
            case '1':
                extensionsFromState[index].DependServiceTypeId = this.state.MyEntUserWebServiceTypeId;
                break;
            case '0':
                extensionsFromState[index].DependServiceTypeId = this.state.MyEntUserServiceTypeId;
                break;
            default:
                extensionsFromState[index].DependServiceTypeId = '';
        }

        this.setState({ BillingServiceTypes: extensionsFromState });
    }

    private billingAdditionalServicePriceHandler(ev: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>, index: number) {
        const extensionsFromState = this.state.BillingServiceTypes;
        const cost = parseInt(ev.target.value, 10);
        extensionsFromState[index].ServiceTypeCost = cost < 0 || isNaN(cost) ? 0 : cost;
        this.setState({ BillingServiceTypes: extensionsFromState });
    }

    private addBillingAdditionalService(serviceTypeId?: TServiceBillingConnectionTypeResponse) {
        const { BillingServiceTypes } = this.state;
        const { serviceName } = this.props;

        const field = serviceName ? `${ serviceName } ${ BillingServiceTypes.length + 1 }` : `Новая услуга ${ BillingServiceTypes.length + 1 }`;
        const newId = uuid();
        const newAdditionalService: TBillingServiceType = {
            BillingType: 1,
            ConnectionToDatabaseType: 1,
            DependServiceTypeId: serviceTypeId?.MyEntUserWebServiceTypeId ?? this.state.MyEntUserWebServiceTypeId,
            Description: field,
            Id: newId,
            Key: newId,
            Name: field,
            ServiceTypeCost: 0,
            ServiceTypeRelations: [],
            isNew: true
        };
        this.setState({ BillingServiceTypes: [...BillingServiceTypes, newAdditionalService] });
    }

    private addServiceTypeRelation(event: SelectChangeEvent<string[]>, index: number) {
        const billingServices = this.state.BillingServiceTypes;
        billingServices[index].ServiceTypeRelations = event.target.value as string[];
        this.setState({ BillingServiceTypes: billingServices });
    }

    private removeServiceTypeRelation(serviceIndex: number, relationIndex: number) {
        const billingServices = this.state.BillingServiceTypes;
        billingServices[serviceIndex].ServiceTypeRelations.splice(relationIndex, 1);
        this.setState({ BillingServiceTypes: billingServices });
    }

    private renderBillingAdditionalServices() {
        const formControlLabel = (value: string, label: string, disabled: boolean) => (
            <FormControlLabel
                disabled={ !this.props.isEditable || disabled }
                value={ value }
                control={ <Radio /> }
                label={ label }
            />
        );

        const isEditable = this.state.CanEditServiceCost && this.props.isEditable;

        return this.state.BillingServiceTypes.map((extend, index) => (
            <Accordion key={ extend.Id } className={ styles.customAccordion } defaultExpanded={ true }>
                <AccordionSummary expandIcon={ <ExpandMore /> } className={ styles.customAccordionSummary }>
                    <label style={ { height: '10px' } }>{ extend.Name }</label>
                </AccordionSummary>
                <AccordionDetails className={ styles.customAccordionDetails }>
                    <InputField
                        label="Название услуги"
                        fieldValue={ extend.Name }
                        changeHandler={ ev => this.billingAdditionalServiceNameHandler(ev, index) }
                        maxValue={ 100 }
                        disabled={ !isEditable }
                    />
                    <InputField
                        label="Описание услуги"
                        fieldValue={ extend.Description }
                        changeHandler={ ev => this.billingAdditionalServiceDescriptionHandler(ev, index) }
                        maxValue={ 200 }
                        disabled={ !isEditable }
                    />
                    <FormGroup>
                        <label>Тип биллинга</label>
                        <ToggleButtonGroup
                            disabled={ !isEditable || (!extend.isNew && this.state.BillingServiceStatus !== 1) }
                            exclusive={ true }
                            value={ extend.BillingType.toString() }
                            onChange={ _ => this.billingAdditionalServiceTypeHandler(index) }
                            className={ styles.billingButtonGroup }
                        >
                            <ToggleButton value="0">По пользователям</ToggleButton>
                            <ToggleButton value="1">На аккаунт</ToggleButton>
                        </ToggleButtonGroup>
                    </FormGroup>
                    <FormControl>
                        <label>Необходимый тип подключения к информационной базе 1C</label>
                        <RadioGroup
                            className={ styles.connectionContainer }
                            value={ extend.ConnectionToDatabaseType }
                            onChange={ ev => this.billingAdditionalServiceConnectionTypeHandler(ev, index) }
                        >
                            { formControlLabel('0', 'Web и RDP подключение', (!extend.isNew && this.state.BillingServiceStatus !== 1)) }
                            { formControlLabel('1', 'Web или RDP подключение', (!extend.isNew && this.state.BillingServiceStatus !== 1)) }
                            { formControlLabel('2', 'Без подключения к базе', (!extend.isNew && this.state.BillingServiceStatus !== 1)) }
                        </RadioGroup>
                    </FormControl>
                    <InputField
                        label="Стоимость сервиса (руб/мес)"
                        fieldValue={ extend.ServiceTypeCost.toString() }
                        changeHandler={ ev => this.billingAdditionalServicePriceHandler(ev, index) }
                        disabled={ !isEditable }
                    />
                    <ContainedButton
                        isEnabled={ isEditable }
                        kind="error"
                        onClick={ () => this.deleteBillingAdditionalService(index) }
                        style={ { width: 'fit-content' } }
                    >
                        Удалить
                    </ContainedButton>
                </AccordionDetails>
            </Accordion>
        ));
    }

    private countServicePrice(service: TBillingServiceType) {
        try {
            let price = service.ServiceTypeCost;
            const services = this.state.BillingServiceTypes;

            if (service.ServiceTypeRelations) {
                for (const current of service.ServiceTypeRelations) {
                    const foundedService = services.find(elem => elem.Id === current);

                    if (foundedService) {
                        price += this.countServicePrice(foundedService);
                    }
                }
            }

            return price;
        } catch (err: unknown) {
            return 0;
        }
    }

    private renderFirstBillingInfo() {
        if (this.state && this.state.BillingServiceTypes.length === 1) {
            const { BillingType, ServiceTypeCost, isNew } = this.state.BillingServiceTypes[0];
            const isDisabled = !this.state.CanEditServiceCost || !this.props.isEditable;

            return (
                <>
                    <FormGroup>
                        <label>Тип биллинга</label>
                        <ToggleButtonGroup
                            disabled={ isDisabled || !isNew }
                            exclusive={ true }
                            value={ BillingType.toString() }
                            onChange={ _ => this.billingAdditionalServiceTypeHandler(0) }
                            className={ styles.billingButtonGroup }
                        >
                            <ToggleButton color="primary" value="0">По пользователям</ToggleButton>
                            <ToggleButton color="primary" value="1">На аккаунт</ToggleButton>
                        </ToggleButtonGroup>
                    </FormGroup>
                    <InputField
                        label="Стоимость сервиса (руб/мес)"
                        fieldValue={ ServiceTypeCost.toString() }
                        changeHandler={ ev => this.billingAdditionalServicePriceHandler(ev, 0) }
                        disabled={ isDisabled }
                    />
                </>
            );
        }

        return null;
    }

    private checkOptions(service: TBillingServiceType) {
        return this.state.BillingServiceTypes.flatMap(option => {
            if ((option.ServiceTypeRelations && option.ServiceTypeRelations.length > 0) || option.Id === service.Id || service.ServiceTypeRelations.includes(option.Id) || option.BillingType === 1) {
                return [];
            }

            return option;
        });
    }

    private renderSelectServicesAdditionalCombination(currentService: TBillingServiceType) {
        const optionValues = this.checkOptions(currentService);
        const options = optionValues.map(option => <MenuItem key={ option.Id } value={ option.Id }>{ option.Name }</MenuItem>);

        if (options.length === 0) {
            options.push(<MenuItem key="no_founds" disabled={ true }>Совпадений не найдено</MenuItem>);
        }

        return options as JSX.Element[];
    }

    private renderServicesAdditionalCombination() {
        const isEditable = this.props.isEditable && this.state.CanEditServiceCost && this.props.isCreatePage;
        const services = this.state.BillingServiceTypes;

        return services.map(({ Id, Name, ServiceTypeRelations, BillingType }, index, array) => {
            if (BillingType === 1) {
                return [];
            }

            return (
                <div key={ Id } className={ styles.extendServiceItem }>
                    <div className={ styles.serviceItem }>
                        <div className={ styles.item }>{ Name }</div>
                        <FormControl className={ styles.itemBig }>
                            <Select
                                disabled={ !isEditable }
                                className={ styles.customSelect }
                                sx={ SELECT_STYLE_CONSTANT }
                                MenuProps={ SELECT_MENU_PROPS }
                                input={ <OutlinedInput label="Chip" /> }
                                onChange={ event => this.addServiceTypeRelation(event, index) }
                                value={ ServiceTypeRelations }
                                multiple={ !!ServiceTypeRelations }
                                renderValue={ selected => (
                                    <Box className={ styles.selectValue_box }>
                                        { selected.map((elem, i) => (
                                            services.find(obj => obj.Id === elem) && <Chip
                                                key={ elem }
                                                deleteIcon={ <DeleteIcon /> }
                                                onClick={ () => this.removeServiceTypeRelation(index, i) }
                                                onDelete={ () => this.removeServiceTypeRelation(index, i) }
                                                label={ services.find(obj => obj.Id === elem)?.Name ?? '' }
                                                onMouseDown={ event => event.stopPropagation() }
                                                className={ styles.selected }
                                                disabled={ !isEditable }
                                            />
                                        )) }
                                    </Box>
                                ) }
                            >
                                { this.renderSelectServicesAdditionalCombination(array[index]) }
                            </Select>
                        </FormControl>
                        <div className={ styles.item }>{ this.countServicePrice(array[index]) }</div>
                    </div>
                    <Divider className={ styles.divider } />
                </div>
            );
        }) as JSX.Element[];
    }

    public render() {
        const { chosenIndustry, chosenAudience, chosenTags, industries, BillingServiceTypes, CanEditServiceCost, NextPossibleEditServiceCostDate, audience, tags } = this.state;
        const isEditable = this.props.isEditable && CanEditServiceCost;

        const defaultIndustryValue = industries.filter(({ id }) => chosenIndustry.includes(id)).map(def => def.id);
        const defaultAudienceValue = audience.filter(({ id }) => chosenAudience.includes(id)).map(def => def.id);
        const defaultTagsValue = tags.filter(tag => chosenTags.includes(tag));

        const renderIndustryValue = (
            <Box display="flex" gap="5px">
                { industries.map(({ id, name }) => chosenIndustry.includes(id) && <Chip key={ id } label={ name } className={ styles.selected } />) }
            </Box>
        );
        const renderAudienceValue = (
            <Box display="flex" gap="5px">
                { audience.map(({ id, name }) => chosenAudience.includes(id) && <Chip key={ id } label={ name } className={ styles.selected } />) }
            </Box>
        );

        const headerFontWeightAdditionalCombination = 600;
        const billingServiceType = BillingServiceTypes?.filter(type => type.BillingType !== 1) ?? [];
        const isMultipleBilling = this.state.BillingServiceTypes.length > 1;

        return (
            <div className={ styles.billingContainer } data-tourid={ EPartnersTourId.createServiceCost }>
                { !CanEditServiceCost && this.props.isEditable && NextPossibleEditServiceCostDate &&
                    <div className={ styles.infoSection }>
                        <span>Редактировать услуги биллинга возможно не чаще, чем раз в 3 месяца.</span>
                        <span>Следующая возможность появится { new Date(NextPossibleEditServiceCostDate).toLocaleString() }</span>
                    </div>
                }
                { this.renderFirstBillingInfo() }
                { isMultipleBilling &&
                    <div className={ styles.billingInfo }>
                        <label>Создание услуг</label>
                        <TextOut>В этом разделе вы можете создать несколько услуг в рамках сервиса</TextOut>
                        <TextOut>Для каждой услуги у вас будет возможность задать настройки и стоимость</TextOut>
                    </div>
                }
                <Accordion className={ styles.customAccordion }>
                    <AccordionSummary expandIcon={ <ExpandMore /> } className={ styles.customAccordionSummary }>
                        <TextOut fontWeight={ 700 }>Расширенные параметры</TextOut>
                    </AccordionSummary>
                    <AccordionDetails className={ styles.customAccordionDetails }>
                        { this.renderBillingAdditionalServices() }
                        <SuccessButton
                            isEnabled={ isEditable }
                            onClick={ () => this.addBillingAdditionalService() }
                            style={ { width: 'fit-content' } }
                        >
                            Добавить услугу
                        </SuccessButton>
                    </AccordionDetails>
                </Accordion>
                { ((billingServiceType.length > 1 && this.props.isCreatePage) ||
                        (!this.props.isCreatePage &&
                            billingServiceType.length > 1 &&
                            billingServiceType.filter(({ ServiceTypeRelations }) => ServiceTypeRelations && ServiceTypeRelations.length > 0).length > 0)) &&
                            <div>
                                <div className={ styles.tableHeader }>
                                    <TextOut fontWeight={ headerFontWeightAdditionalCombination } className={ styles.item }>Комбинирование услуг</TextOut>
                                    <TextOut fontWeight={ headerFontWeightAdditionalCombination } className={ styles.itemBig }> Составляющие услуги</TextOut>
                                    <TextOut fontWeight={ headerFontWeightAdditionalCombination } className={ styles.item }>Стоимость</TextOut>
                                </div>
                                <div className={ styles.list }>
                                    <Divider className={ styles.divider } />
                                    { this.renderServicesAdditionalCombination() }
                                </div>
                            </div>
                }
                <div>
                    <InputLabel>Категории</InputLabel>
                    <Select
                        disabled={ !this.props.isEditable }
                        className={ styles.selectIndustries }
                        value={ defaultIndustryValue ?? '' }
                        sx={ SELECT_STYLE_CONSTANT }
                        input={ <OutlinedInput label="Chip" /> }
                        multiple={ true }
                        onChange={ this.onIndustrySelect }
                        renderValue={ () => renderIndustryValue }
                    >
                        { industries.map(({ id, name }) => (
                            <MenuItem value={ id } key={ id }>
                                { name }
                            </MenuItem>
                        )) }
                    </Select>
                </div>
                <div>
                    <InputLabel>Для кого</InputLabel>
                    <Select
                        disabled={ !this.props.isEditable }
                        className={ styles.selectIndustries }
                        value={ defaultAudienceValue ?? '' }
                        sx={ SELECT_STYLE_CONSTANT }
                        input={ <OutlinedInput label="Chip" /> }
                        multiple={ true }
                        onChange={ this.onAudienceSelect }
                        renderValue={ () => renderAudienceValue }
                    >
                        { audience.map(({ id, name }) => (
                            <MenuItem value={ id } key={ id }>
                                { name }
                            </MenuItem>
                        )) }
                    </Select>
                </div>
                <div>
                    <InputLabel>Теги</InputLabel>
                    <TagsSelect
                        disabled={ !this.props.isEditable }
                        options={ tags.map(tag => ({ title: tag, inputValue: tag })) }
                        value={ defaultTagsValue.map(tag => ({ title: tag, inputValue: tag })) }
                        onChange={ newValue => this.setState({ chosenTags: newValue.map(tag => tag.inputValue ?? tag.title) }) }
                        setOptions={ options => this.setState({ tags: options }) }
                    />
                </div>
            </div>
        );
    }
}