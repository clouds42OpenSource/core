import React, { useCallback, useEffect, useState } from 'react';

import { InputField } from 'app/views/modules/Services/views/AddServiceCard/components/InputField';
import { Dialog } from 'app/views/components/controls/Dialog';
import { ContainedButton, SuccessButton } from 'app/views/components/controls/Button';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FETCH_API } from 'app/api/useFetchApi';

import { TRecord } from 'app/api/endpoints/services/response';
import styles from './style.module.css';

type TMarketingViewDialog = FloatMessageProps & {
    serviceId: string;
    isOpen: boolean;
    cancelClickHandler: (record: TRecord | null) => void;
    record: TRecord | null;
    refreshData?: () => void;
    defaultTitle?: string;
    defaultDescription?: string;
};

const { deleteMarketPromo, createMarketPromo } = FETCH_API.SERVICES;

const PromoProposal = ({ isOpen, cancelClickHandler, serviceId, floatMessage: { show }, record, refreshData, defaultTitle, defaultDescription }: TMarketingViewDialog) => {
    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');
    const [script, setScript] = useState('');

    const removeSavedData = useCallback(() => {
        setTitle(defaultTitle ?? '');
        setDescription(defaultDescription ?? '');
        setScript('');
    }, [defaultDescription, defaultTitle]);

    useEffect(() => {
        if (record) {
            setTitle(record.title);
            setDescription(record.description);
            setScript(record.query);
        } else {
            removeSavedData();
        }
    }, [defaultDescription, defaultTitle, record, removeSavedData]);

    const titleHandler = (ev: React.ChangeEvent<HTMLInputElement>) => {
        setTitle(ev.target.value);
    };

    const descriptionHandler = (ev: React.ChangeEvent<HTMLInputElement>) => {
        setDescription(ev.target.value);
    };

    const scriptHandler = (ev: React.ChangeEvent<HTMLTextAreaElement>) => {
        setScript(ev.target.value);
    };

    const getRefreshData = () => {
        if (refreshData) {
            refreshData();
        }
    };

    const createPromoProposal = async () => {
        if (title.length < 1 || description.length < 1 || script.length < 1) {
            return show(MessageType.Error, 'Необходимо заполнить все поля');
        }

        const { error } = await createMarketPromo({ 'service-id': serviceId, query: script, title, description });

        if (error) {
            return show(MessageType.Error, error);
        }

        show(MessageType.Success, 'Заявка на рекламу успешно создана');
        removeSavedData();
        getRefreshData();
        cancelClickHandler(null);
    };

    const deleteMarketPromoHandler = async () => {
        const { error: deleteError } = await deleteMarketPromo(record!.id, {});

        if (deleteError) {
            return show(MessageType.Error, deleteError);
        }

        show(MessageType.Success, 'Заявка на рекламу успешно удалена');
        getRefreshData();
        cancelClickHandler(null);
    };

    const getButtons = () => (
        <div className={ styles.buttonContainer }>
            { record && record.status !== 'deleted' && <ContainedButton kind="error" onClick={ deleteMarketPromoHandler }>Удалить</ContainedButton> }
            <ContainedButton onClick={ () => cancelClickHandler(null) }>Закрыть</ContainedButton>
            { !record && <SuccessButton onClick={ createPromoProposal }>Отправить</SuccessButton> }
        </div>
    );

    return (
        <Dialog
            isOpen={ isOpen }
            dialogWidth="lg"
            onCancelClick={ () => cancelClickHandler(null) }
            title={ record ? 'Просмотр заявки' : 'Добавить заявку' }
            buttons={ getButtons }
            isTitleSmall={ true }
        >
            <div className={ styles.dialogContainer }>
                <InputField changeHandler={ titleHandler } disabled={ !!record } label="Заголовок" maxValue={ 150 } fieldValue={ title } />
                <InputField changeHandler={ descriptionHandler } disabled={ !!record } label="Описание" maxValue={ 300 } fieldValue={ description } />
                <div className={ styles.scriptContainer }>
                    <label>Запрос</label>
                    <textarea value={ script } onChange={ scriptHandler } className={ styles.scriptArea } disabled={ !!record } />
                </div>
                { record &&
                    <>
                        { record.result && <InputField disabled={ true } label="Результат" fieldValue={ record.result } /> }
                    </>
                }
            </div>
        </Dialog>
    );
};

export const PromoProposalDialog = withFloatMessages(PromoProposal);