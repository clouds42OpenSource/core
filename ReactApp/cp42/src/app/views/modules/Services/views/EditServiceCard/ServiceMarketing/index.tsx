import React, { useState } from 'react';
import CheckIcon from '@mui/icons-material/Check';

import { FETCH_API } from 'app/api/useFetchApi';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { TRecord, TStatus } from 'app/api/endpoints/services/response';
import { Chip, Link, MenuItem, Pagination, Select, Tooltip } from '@mui/material';
import { SuccessButton } from 'app/views/components/controls/Button';
import { SelectChangeEvent } from '@mui/material/Select';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { PromoProposalDialog } from './components/MarketingViewDialog';

import styles from './style.module.css';

type TServiceMarketing = FloatMessageProps & {
    serviceId: string;
    isAvailable: boolean;
    defaultTitle?: string;
    defaultDescription?: string;
};

const ServiceMarketingView = ({ floatMessage: { show }, serviceId, isAvailable, defaultTitle, defaultDescription }: TServiceMarketing) => {
    const [currentPage, setCurrentPage] = useState(1);
    const [currentStatus, setCurrentStatus] = useState<TStatus>('');
    const [isDialogOpen, setIsDialogOpen] = useState(false);
    const [currentRecord, setCurrentRecord] = useState<TRecord | null>(null);

    const { error, isLoading, data, refreshData } = FETCH_API.SERVICES.useGetMarketPromo({ page: currentPage, 'service-id': serviceId, status: currentStatus });

    const tableDataset = data?.rawData.records ?? [] as TRecord[];
    const isPaginationDisplay = data?.rawData.pagination && data.rawData.records.length > 0;

    const selectStatusHandler = (event: SelectChangeEvent) => {
        setCurrentStatus(event.target.value as TStatus);
    };

    const dialogHandler = (record: TRecord | null) => {
        setIsDialogOpen(!isDialogOpen);
        setCurrentRecord(record);
    };

    const setChipStatus = (value: TStatus) => {
        switch (value) {
            case 'accepted':
                return <Chip color="primary" label="Одобрена" />;
            case 'declined':
                return <Chip className={ styles.chip } color="warning" label="Отклонена" />;
            case 'pending':
                return <Chip color="info" label="Обрабатывается" />;
            case 'deleted':
                return <Chip color="error" label="Удалена автором" />;
            default:
                break;
        }
    };

    const paginationHandler = (_: React.ChangeEvent<unknown>, page: number) => {
        setCurrentPage(page);
    };

    if (error) {
        show(MessageType.Error, error);
    }

    return (
        <div className={ styles.pageContainer }>
            { isLoading && <LoadingBounce /> }
            <PromoProposalDialog
                defaultTitle={ defaultTitle }
                defaultDescription={ defaultDescription }
                refreshData={ refreshData }
                isOpen={ isDialogOpen }
                cancelClickHandler={ dialogHandler }
                serviceId={ serviceId }
                record={ currentRecord }
            />
            <div className={ styles.controlContainer }>
                <Link className={ styles.href } href="https://42clouds.com/ru-ru/articles/targetirovannaya-reklama-servisov-market42.html" rel="noreferrer" target="_blank">
                    Подробнее о размещении таргетированной  рекламы в базах 1С
                </Link>
                <div className={ styles.leftControlContainer }>
                    { isAvailable && <SuccessButton onClick={ () => dialogHandler(null) }>Добавить заявку</SuccessButton> }
                    <FormAndLabel label="Статус">
                        <Select fullWidth={ true } placeholder="Все" value={ currentStatus } onChange={ selectStatusHandler }>
                            <MenuItem value="">Все</MenuItem>
                            <MenuItem value="accepted">Одобрена</MenuItem>
                            <MenuItem value="declined">Отклонена</MenuItem>
                            <MenuItem value="pending">Обрабатывается</MenuItem>
                            <MenuItem value="deleted">Удалена автором</MenuItem>
                        </Select>
                    </FormAndLabel>
                </div>
            </div>
            <CommonTableWithFilter
                isResponsiveTable={ true }
                isBorderStyled={ true }
                uniqueContextProviderStateId="ServiceMarketingListTableContextProviderTextId"
                tableProps={ {
                    dataset: tableDataset,
                    keyFieldName: 'id',
                    isResponsiveTable: false,
                    emptyText: 'Нет заявок на рекламу сервиса',
                    fieldsView: {
                        created: {
                            caption: 'Дата создания',
                            fieldWidth: '30%',
                            format: (value: string) => new Date(value).toLocaleString().replaceAll(',', '')
                        },
                        title: {
                            caption: 'Заголовок',
                            fieldWidth: '35%'
                        },
                        active: {
                            caption: 'Опубликована',
                            fieldWidth: '10%',
                            format: (value: boolean) => value ? <CheckIcon /> : null,
                        },
                        status: {
                            caption: 'Статус',
                            fieldWidth: '20%',
                            format: setChipStatus
                        },
                        id: {
                            caption: 'Действия',
                            fieldWidth: '15%',
                            format: (value: string) =>
                                <Tooltip title="Просмотр заявки" onClick={ () => dialogHandler(tableDataset.find(record => record.id === value) ?? null) }>
                                    <i className={ `fa fa-eye ${ styles.icon }` } aria-hidden="true" />
                                </Tooltip>
                        }
                    }
                } }
            />
            { isPaginationDisplay &&
                <Pagination count={ data.rawData.pagination.totalPages } onChange={ paginationHandler } shape="rounded" className={ styles.pagination } />
            }
        </div>
    );
};

export const ServiceMarketing = withFloatMessages(ServiceMarketingView);