import { wpMarketHost } from 'app/api';
import { FETCH_API } from 'app/api/useFetchApi';
import { SuccessButton } from 'app/views/components/controls/Button';
import { TCreateServiceState } from 'app/views/modules/Services/views/AddServiceCard';
import { TBillingViewerState } from 'app/views/modules/Services/views/AddServiceCard/components/BillingViewer';
import { TServiceInformation } from 'app/web/api/ServicesProxy/response-dto';
import React, { useCallback, useEffect, useState } from 'react';

type TServicePreviewProps = {
    onSubmit: () => void;
    service?: TServiceInformation;
    createService?: TCreateServiceState;
    billingViewerRef?: TBillingViewerState;
};

const linkRegex = /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g;

const { getImageUrl } = FETCH_API.SERVICES;

export const ServicePreview = ({ onSubmit, service, createService, billingViewerRef }: TServicePreviewProps) => {
    const [logoUrlState, setLogoUrlState] = useState('');
    const [screenshotsUrlState, setScreenshotsUrlState] = useState<string[]>([]);

    const onSubmitForm = () => {
        onSubmit();

        const form = document?.getElementById('previewServiceForm') as HTMLFormElement | null;

        if (form) {
            form.target = '_blank';

            queueMicrotask(() => form.submit());
        }
    };

    const audience = billingViewerRef?.audience.reduce((audiences, item) => {
        if (billingViewerRef?.chosenAudience.includes(item.id)) {
            audiences.push(item.name);
        }

        return audiences;
    }, [] as string[]).join('|');

    const industrie = billingViewerRef?.industries.reduce((industries, item) => {
        if (billingViewerRef?.chosenIndustry.includes(item.id)) {
            industries.push(item.name);
        }

        return industries;
    }, [] as string[]).join('|');

    const logoUrl = useCallback(async () => {
        if (createService?.logoId && (service?.ServiceIconID !== createService.logoId)) {
            const url = await getImageUrl(createService?.logoId ?? '');

            return url.data?.url ?? '';
        }

        if (service?.ServiceIcon.match(linkRegex)) {
            return service?.ServiceIcon;
        }

        return '';
    }, [createService?.logoId, service?.ServiceIcon, service?.ServiceIconID]);

    useEffect(() => {
        (async () => {
            const logo = await logoUrl();

            setLogoUrlState(logo);
        })();
    }, [createService, logoUrl]);

    const screenshots = useCallback(async () => {
        const screenshotsList = [
            ...(service?.ServiceScreenshots ?? []),
            ...(createService?.screenshotsId?.filter(id => !(service?.ServiceScreenshots ?? []).some(screenshot => screenshot.includes(id))) ?? [])
        ];

        return Promise.all(screenshotsList.map(async screenshot => {
            if (!screenshot.match(linkRegex)) {
                const url = await getImageUrl(screenshot);
                return url.data?.url ?? '';
            }
            return screenshot;
        }));
    }, [createService?.screenshotsId, service?.ServiceScreenshots]);

    useEffect(() => {
        (async () => {
            const screenshotsList = await screenshots();

            setScreenshotsUrlState(screenshotsList);
        })();
    }, [service, createService, screenshots]);

    return (
        <form id="previewServiceForm" action={ wpMarketHost('/service/market-service-preview') } method="POST">
            <input type="text" name="title" hidden={ true } value={ service?.ServiceName ?? createService?.fields.name ?? '' } />
            <input type="text" name="short_description" hidden={ true } value={ service?.ServiceShortDescription ?? createService?.fields.description ?? '' } />
            <input type="text" name="is_hybrid" hidden={ true } value={ `${ service?.Hybrid ?? createService?.hybrid ?? false }` } />
            <input type="text" name="img_url" hidden={ true } value={ logoUrlState } />
            <input
                type="number"
                name="price"
                hidden={ true }
                value={
                    service?.ServiceCost ??
                    `${ billingViewerRef?.BillingServiceTypes.reduce((cost, billingService) => {
                        cost += billingService.ServiceTypeCost;

                        return cost;
                    }, 0) }` ??
                    ''
                }
            />
            <input type="text" name="categories" hidden={ true } value={ industrie ?? '' } />
            <input type="text" name="tags" hidden={ true } value={ (service?.Tags ?? billingViewerRef?.chosenTags ?? [])?.join('|') } />
            <input type="text" name="license_type" hidden={ true } value={ (billingViewerRef?.BillingServiceTypes.map(item => item.BillingType) ?? []).join('|') } />
            <input type="text" name="audiences" hidden={ true } value={ audience ?? '' } />
            <input type="text" name="description" hidden={ true } value={ service?.ServiceDescription ?? createService?.richTextDescription ?? '' } />
            <input type="text" name="video_url" hidden={ true } value={ service?.Video ?? '' } />
            <input type="text" name="instruction_pdf" hidden={ true } value={ service?.InstructionURL ?? createService?.instructionUrl ?? '' } />
            <input type="text" name="instruction" hidden={ true } value={ service?.Instruction ?? createService?.instruction ?? '' } />
            <input type="text" name="screenshots" hidden={ true } value={ screenshotsUrlState.join('|') } />
            <SuccessButton onClick={ onSubmitForm }>Предпросмотр</SuccessButton>
        </form>
    );
};