import React from 'react';
import { TextOut } from 'app/views/components/TextOut';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';

type TProps = {
    label: string;
    description: string;
    isDescriptionVisible: boolean;
};

export const TextField = ({ label, isDescriptionVisible, description }: TProps) => {
    return (
        <FormAndLabel label={ label }>
            { isDescriptionVisible &&
                <TextOut inDiv={ true }>{ description }</TextOut>
            }
        </FormAndLabel>
    );
};