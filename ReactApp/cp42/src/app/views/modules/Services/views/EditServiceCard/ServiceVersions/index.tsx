import { Box, MenuItem, Pagination, Select } from '@mui/material';
import OutlinedInput from '@mui/material/OutlinedInput';
import { SelectChangeEvent } from '@mui/material/Select';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';

import { SuccessButton } from 'app/views/components/controls/Button';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { dateSort } from 'app/views/modules/Services/utils/dateSort';
import { ConfigAccordion } from 'app/views/modules/Services/views/EditServiceCard/ServiceVersions/components/ConfigAccordion';
import { ConfigFileContainer } from 'app/views/modules/Services/views/EditServiceCard/ServiceVersions/components/ConfigUploader';
import { TConfigurationsResponse, TServiceInfoResponse } from 'app/web/api/ServicesProxy/response-dto';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { RequestKind } from 'core/requestSender/enums';
import React, { ChangeEvent, memo, useCallback, useEffect, useRef, useState } from 'react';
import { useParams } from 'react-router';

type TProps = FloatMessageProps & {
    isAllowedEdit: boolean;
};

export enum EFilterStatus {
    All = 'Все',
    Published = 'Опубликована',
    Blocked = 'Заблокирована',
    Waiting = 'Ожидает аудита',
    Processing = 'Обработка файла',
    Declined = 'Отклонена',
    Compatibility = 'Проблемы совместимости'
}

const PAGE_SIZE = 5;
const { receiveServiceVersionInformation, receiveConfigsFromMs } = InterlayerApiProxy.getServicesApiProxy();

export const ServiceFileVersionModule = memo(({ floatMessage: { show }, isAllowedEdit }: TProps) => {
    const [files, setFiles] = useState<TServiceInfoResponse[]>([]);
    const [filesBackup, setFilesBackup] = useState<TServiceInfoResponse[]>([]);

    const [loading, setLoading] = useState(false);
    const [newVersionPanel, setNewVersionPanel] = useState(false);
    const [configs, setConfigs] = useState<TConfigurationsResponse[] | null>(null);
    const [page, setPage] = useState(1);

    const search = useRef('');
    const [status, setStatus] = useState(EFilterStatus.All);

    const params = useParams<{ id: string }>();

    const getVersionFiles = useCallback(async (id: string) => {
        setLoading(true);
        let result = await receiveServiceVersionInformation(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, id);

        if (result) {
            result = result.sort((first, second) =>
                dateSort(first['creation-date'], second['creation-date'], '.'));

            setFiles(result);
            setFilesBackup(result);
        } else {
            show(MessageType.Error, 'Не удалось получить версии файлов конфигураций');
        }

        setLoading(false);
    }, [show]);

    useEffect(() => {
        (async () => {
            const result = await receiveConfigsFromMs(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY);

            if (result && 'message' in result) {
                show(MessageType.Error, result.message);
            } else {
                setConfigs(result);
            }

            if (params.id) {
                void getVersionFiles(params.id);
            }
        })();
    }, [getVersionFiles, params.id, show]);

    const openNewVersionPanel = () => {
        setNewVersionPanel(!newVersionPanel);
    };

    const onSearch = useCallback(() => {
        const regex = search.current.toLowerCase();
        const arr = filesBackup.filter(item =>
            (
                status === EFilterStatus.All || (item?.['version-status'] && item['version-status'] === status)
            ) &&
            (
                item.name.toLowerCase().includes(regex) ||
                item.version.toLowerCase().includes(regex) ||
                item.synonym.toLowerCase().includes(regex)
            )
        );
        setFiles(arr);
    }, [filesBackup, search, status]);

    const loadingHandler = useCallback((isLoading: boolean) => {
        setLoading(isLoading);
    }, []);

    const setPageHandler = (_: ChangeEvent<unknown>, pageCount: number) => {
        setPage(pageCount);
    };

    const setStatusHandler = (ev: SelectChangeEvent) => {
        setStatus(ev.target.value as EFilterStatus);
    };

    const setSearchHandler = (ev: ChangeEvent<HTMLInputElement>) => {
        search.current = ev.target.value;
    };

    return (
        <>
            { loading && <LoadingBounce /> }
            { files && configs &&
                <Box display="flex" flexDirection="column" gap="15px">
                    <Box display="flex" justifyContent="space-between" flexWrap="wrap" alignItems="flex-end">
                        { isAllowedEdit &&
                            <SuccessButton onClick={ openNewVersionPanel }>
                                { !newVersionPanel ? 'Добавить новую версию' : 'Скрыть' }
                            </SuccessButton>
                        }
                        <Box display="flex" gap="10px" alignItems="flex-end">
                            <FormAndLabel label="Поиск">
                                <OutlinedInput fullWidth={ true } placeholder="Введите название" onChange={ setSearchHandler } />
                            </FormAndLabel>
                            <FormAndLabel label="Статус версий">
                                <Select variant="outlined" fullWidth={ true } onChange={ setStatusHandler } value={ status }>
                                    <MenuItem value={ EFilterStatus.All }>{ EFilterStatus.All }</MenuItem>
                                    <MenuItem value={ EFilterStatus.Published }>{ EFilterStatus.Published }</MenuItem>
                                    <MenuItem value={ EFilterStatus.Blocked }>{ EFilterStatus.Blocked }</MenuItem>
                                    <MenuItem value={ EFilterStatus.Waiting }>{ EFilterStatus.Waiting }</MenuItem>
                                    <MenuItem value={ EFilterStatus.Processing }>{ EFilterStatus.Processing }</MenuItem>
                                    <MenuItem value={ EFilterStatus.Declined }>{ EFilterStatus.Declined }</MenuItem>
                                    <MenuItem value={ EFilterStatus.Compatibility }>{ EFilterStatus.Compatibility }</MenuItem>
                                </Select>
                            </FormAndLabel>
                            <SuccessButton onClick={ onSearch }>
                                Поиск
                            </SuccessButton>
                        </Box>
                    </Box>
                    <ConfigFileContainer
                        getVersionFiles={ getVersionFiles }
                        id={ params.id }
                        isOpen={ newVersionPanel }
                        setLoading={ loadingHandler }
                        files={ files }
                    />
                    <Box display="flex" flexDirection="column" gap="20px">
                        { files.slice((page - 1) * PAGE_SIZE, page * PAGE_SIZE).map(value => (
                            <ConfigAccordion
                                configs={ configs }
                                key={ value['creation-date'] }
                                file={ value }
                                id={ params.id }
                                setLoading={ loadingHandler }
                                getVersionFiles={ getVersionFiles }
                                isAllowedEdit={ isAllowedEdit }
                                files={ files }
                                filesBackup={ filesBackup }
                            />
                        )) }
                    </Box>
                    { Math.ceil(files.length / PAGE_SIZE) > 1 && (
                        <Pagination
                            page={ page }
                            count={ Math.ceil(files.length / PAGE_SIZE) }
                            onChange={ setPageHandler }
                        />
                    ) }
                </Box>
            }
        </>
    );
});