import React from 'react';

import { TCommand, TConfigurationsResponse, TFileInformationResponse } from 'app/web/api/ServicesProxy/response-dto';
import { CheckBoxForm } from 'app/views/components/controls/forms';
import { ContainedButton } from 'app/views/components/controls/Button';
import styles from 'app/views/modules/Services/views/AddServiceCard/components/ConfigFileViewer/style.module.css';
import { Link, SelectChangeEvent, TextField } from '@mui/material';
import { FileCard } from 'app/views/modules/Services/views/AddServiceCard/components/FileCard';
import { Configuration } from 'app/views/modules/Services/views/AddServiceCard/components/Configuration';
import { SelectTasks } from 'app/views/modules/Services/views/AddServiceCard/components/SelectTasks';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { TextOut } from 'app/views/components/TextOut';

type TConfigFileProps = {
    configFiles: TFileInformationResponse[];
    callBack: (selectedConfigs: TFileInformationResponse[]) => void;
    isEditable: boolean;
    comment?: string;
    allConfigs: TConfigurationsResponse[];
    linkToAuditGoogleSheet?: string;
    versionId?: string;
};

type TConfigFileState = {

};

export class ConfigFileViewer extends React.Component<TConfigFileProps, TConfigFileState> {
    constructor(props: TConfigFileProps) {
        super(props);
        this.textareaHandler = this.textareaHandler.bind(this);
        this.checkboxHandler = this.checkboxHandler.bind(this);
        this.selectTask = this.selectTask.bind(this);
        this.deleteTask = this.deleteTask.bind(this);
        this.addWeekDays = this.addWeekDays.bind(this);
        this.addTimeTableInfo = this.addTimeTableInfo.bind(this);
    }

    public componentDidMount() {
        if (this.props.isEditable) {
            const { configFiles } = this.props;

            configFiles.forEach(configFile => {
                if (configFile.commands) {
                    for (const command of configFile.commands) {
                        if (command['week-days'] !== undefined) {
                            if (configFile.chosenCommands) {
                                configFile.chosenCommands.push(command);
                            } else {
                                configFile.chosenCommands = [command];
                            }
                        }
                    }
                }
            });

            this.props.callBack(configFiles);
        }
    }

    private checkboxHandler(index: number) {
        const { configFiles } = this.props;
        configFiles[index].isNotSafe = !configFiles[index].isNotSafe;
        this.props.callBack(configFiles);
    }

    private textareaHandler(index: number, event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) {
        const { configFiles } = this.props;
        configFiles[index].comment = event.target.value;
        this.props.callBack(configFiles);
    }

    private deleteConfig(index: number) {
        let { configFiles } = this.props;
        configFiles = configFiles.splice(index, index);
        this.props.callBack(configFiles);
    }

    private selectTask(event: SelectChangeEvent<TCommand[]>, _: React.ReactNode, index: number) {
        const { value } = event.target;
        const { configFiles } = this.props;

        if (typeof value === 'string') {
            configFiles[index].chosenCommands.push(configFiles[index].commands.find(cmd => cmd.name === event.target.value)!);
        }

        this.props.callBack(configFiles);
    }

    private deleteTask(_: React.MouseEvent<HTMLDivElement, MouseEvent>, elem: string, index: number) {
        const { configFiles } = this.props;
        const commands = configFiles[index].chosenCommands;
        const indexTarget = commands.map(searchElement => searchElement.name).indexOf(elem);
        commands.splice(indexTarget, 1);
        configFiles[index].chosenCommands = commands;
        this.props.callBack(configFiles);
    }

    private addWeekDays(ev: React.MouseEvent<HTMLDivElement, MouseEvent>, index: number, i: number) {
        const eventTarget = ev as unknown as React.ChangeEvent<HTMLInputElement>;
        const { configFiles } = this.props;
        const command = configFiles[index].chosenCommands[i];
        const value = parseInt(eventTarget.target.name, 10);

        if (command['week-days'] === undefined) {
            command['week-days'] = new Set<number>();
        } else if (Array.isArray(command['week-days'])) {
            command['week-days'] = new Set<number>([...command['week-days'] as Array<number>]);
        }

        if (command['week-days'].has(value)) {
            command['week-days'].delete(value);
        } else {
            command['week-days'].add(value);
        }

        this.props.callBack(configFiles);
    }

    private addTimeTableInfo(ev: React.ChangeEvent<HTMLInputElement>, index: number, i: number) {
        const { id, value } = ev.target;
        const { configFiles } = this.props;
        const command = configFiles[index].chosenCommands[i] ?? configFiles[index].commands;

        if (id === 'begin-time' || id === 'end-time') {
            configFiles[index].chosenCommands[i] = {
                ...command,
                [id]: value
            };
        } else if (id === 'repeat-period-in-day' || id === 'days-repeat-period') {
            configFiles[index].chosenCommands[i] = {
                ...command,
                [id]: parseInt(value, 10)
            };
        }

        this.props.callBack(configFiles);
    }

    public render() {
        const { versionId, configFiles, isEditable, comment, allConfigs, callBack, linkToAuditGoogleSheet } = this.props;

        if (configFiles) {
            return configFiles.map((file, index) => {
                return (
                    <div className={ styles.container } key={ versionId ?? file.fileId }>
                        <div className={ styles.cardHeader }>
                            <TextOut fontWeight={ 700 } fontSize={ 14 }>{ file.name }</TextOut>
                        </div>
                        <div className={ styles.cardBody }>
                            <FileCard fileInfo={ file } versionId={ versionId } />
                            { file.type === 'cfe' &&
                            <CheckBoxForm
                                isChecked={ file.isNotSafe }
                                formName="isSafe"
                                label="Установить в небезопасном режиме"
                                className={ styles.container__checkbox }
                                onValueChange={ () => this.checkboxHandler(index) }
                                isReadOnly={ !isEditable }
                            />
                            }
                            <FormAndLabel label="Совместимые конфигурации">
                                <Configuration
                                    isEditable={ isEditable }
                                    index={ index }
                                    file={ file }
                                    allConfigs={ allConfigs }
                                    callBack={ callBack }
                                    configFiles={ configFiles }
                                />
                            </FormAndLabel>
                            { file.type === 'cfe' && file.commands && file.commands.length > 0 &&
                            <FormAndLabel label="Регламентные задания">
                                <SelectTasks
                                    isEditable={ isEditable }
                                    index={ index }
                                    configFiles={ configFiles }
                                    selectTask={ this.selectTask }
                                    deleteTask={ this.deleteTask }
                                    addTimeTableInfo={ this.addTimeTableInfo }
                                    addWeekDays={ this.addWeekDays }
                                />
                            </FormAndLabel>
                            }
                            <FormAndLabel label="Комментарии для аудитора">
                                <TextField
                                    className={ styles.textarea }
                                    minRows={ 4 }
                                    disabled={ !isEditable }
                                    value={ file.comment }
                                    onChange={ event => this.textareaHandler(index, event) }
                                    fullWidth={ true }
                                    multiline={ true }
                                />
                            </FormAndLabel>
                            { !isEditable && comment &&
                            <FormAndLabel label="Комментарий аудита">
                                <TextField
                                    className={ styles.textarea }
                                    fullWidth={ true }
                                    minRows={ 4 }
                                    disabled={ !isEditable }
                                    value={ comment }
                                    multiline={ true }
                                />
                            </FormAndLabel>
                            }
                            { linkToAuditGoogleSheet && (
                                <Link href={ linkToAuditGoogleSheet } target="_blank" rel="noreferrer">Результаты автоматической проверки</Link>
                            ) }
                            { isEditable && <ContainedButton kind="error" onClick={ () => this.deleteConfig(index) }>Удалить</ContainedButton> }
                        </div>
                    </div>
                );
            });
        }

        return null;
    }
}