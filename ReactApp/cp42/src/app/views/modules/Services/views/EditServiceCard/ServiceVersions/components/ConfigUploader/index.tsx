import { ConfigFileViewer } from 'app/views/modules/Services/views/AddServiceCard/components/ConfigFileViewer';
import styles from 'app/views/modules/Services/views/EditServiceCard/ServiceVersions/style.module.css';
import { SuccessButton } from 'app/views/components/controls/Button';
import { ConfigFileUploader } from 'app/views/modules/_common/components/ConfigFileUploader';
import React, { memo, useCallback, useState } from 'react';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { RequestKind } from 'core/requestSender/enums';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { TFileInformationResponse, TServiceInfoResponse } from 'app/web/api/ServicesProxy/response-dto';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';

type TProps = FloatMessageProps & {
    id: string;
    isOpen: boolean;
    setLoading: (isLoading: boolean) => void;
    files: TServiceInfoResponse[];
    getVersionFiles: (id: string) => Promise<void>;
};

export const ConfigFileContainer = withFloatMessages(memo(({ id, isOpen, floatMessage: { show }, setLoading, files, getVersionFiles }: TProps) => {
    const [newVersion, setNewVersion] = useState<TFileInformationResponse | null>(null);

    const uploadFile = useCallback((file: File) => {
        setLoading(true);
        return InterlayerApiProxy.getServicesApiProxy().uploadFileToMs(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, file, 'service')
            .then(result => result)
            .catch(_ => null)
            .finally(() => {
                setLoading(false);
            });
    }, [setLoading]);

    const configUpload = useCallback(async (file: File) => {
        const uploadedFileId = await uploadFile(file);
        let fileInformation: TFileInformationResponse | null = null;

        if (uploadedFileId) {
            setLoading(true);
            fileInformation = await InterlayerApiProxy.getServicesApiProxy().receiveConfigInfoFromMs(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, uploadedFileId, id);
        } else {
            show(MessageType.Error, 'Не удалось загрузить файл');
        }

        if (fileInformation) {
            if ('message' in fileInformation) {
                setLoading(false);
                return show(MessageType.Error, fileInformation.message);
            }

            if (fileInformation.warning && !fileInformation['allow-loading']) {
                setLoading(false);
                return show(MessageType.Error, fileInformation.warning);
            }

            if (files.length > 0 && files[0].name !== fileInformation.name) {
                if (fileInformation.type === 'cfe') {
                    setLoading(false);
                    return show(MessageType.Error, ' Название не совпадает с названием ранее загруженных версий файлов сервиса');
                }

                show(MessageType.Warning, ' Название не совпадает с названием ранее загруженных версий файлов сервиса');
            }

            if (fileInformation.warning) {
                show(MessageType.Info, fileInformation.warning);
            }

            setNewVersion({
                ...fileInformation,
                fileId: uploadedFileId,
                fileName: file.name,
                configurations: [],
                chosenCommands: []
            });
        }

        setLoading(false);
    }, [files, setLoading, show, uploadFile]);

    const saveNewConfigFile = () => {
        if (newVersion && newVersion.configurations.length === 0) {
            show(MessageType.Error, 'Не заполнены совместимые конфигурации');
        } else if (newVersion) {
            setLoading(true);
            InterlayerApiProxy.getServicesApiProxy().createNewServiceVersion(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, newVersion, id)
                .then(async result => {
                    if (result) {
                        setNewVersion(null);
                        await getVersionFiles(id);
                    } else {
                        show(MessageType.Error, 'Не удалось отправить на аудит новую версию');
                    }
                })
                .finally(() => {
                    setLoading(false);
                });
        } else {
            show(MessageType.Error, 'Не найдена информация о загруженном файле');
        }
    };

    const ConfigFileViewerCallback = useCallback((selectedConfigs: TFileInformationResponse[]) => {
        setNewVersion(selectedConfigs[0]);
    }, []);

    if (isOpen) {
        if (newVersion) {
            return (
                <div className={ styles.configFileViewerContainer }>
                    <ConfigFileViewer
                        configFiles={ [{ ...newVersion }] }
                        callBack={ ConfigFileViewerCallback }
                        isEditable={ true }
                        allConfigs={ newVersion.configurations }
                    />
                    <div className={ styles.buttonContainer }>
                        <SuccessButton onClick={ saveNewConfigFile }>Отправить на аудит</SuccessButton>
                    </div>
                </div>
            );
        }

        return (
            <div className={ styles.configFileUploaderContainer }>
                <ConfigFileUploader
                    callBack={ configUpload }
                    fileTypesArr={
                        files.length > 0
                            ? files[0].type === 'cfe' ? ['cfe'] : ['zip', 'epf', 'erf']
                            : ['zip', 'epf', 'erf', 'cfe']
                    }
                />
            </div>
        );
    }

    return null;
}));