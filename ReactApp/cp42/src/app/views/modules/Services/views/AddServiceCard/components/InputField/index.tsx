import { EPartnersTourId, ETourId } from 'app/views/Layout/ProjectTour/enums';
import React from 'react';
import { OutlinedInput } from '@mui/material';

import { MaxLengthLabel } from 'app/views/modules/Services/views/AddServiceCard/components/MaxLengthLabel';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';

type TProps = {
    label?: string;
    fieldValue?: string;
    changeHandler?: (ev: React.ChangeEvent<HTMLInputElement>) => void;
    maxValue?: number;
    disabled: boolean;
    placeholder?: string;
    background?: 'white';
    tourId?: EPartnersTourId | ETourId;
};

export const InputField = ({ label, fieldValue, changeHandler, maxValue, disabled, placeholder, background, tourId }: TProps) => {
    return (
        <FormAndLabel label={ label } tourId={ tourId }>
            <OutlinedInput
                value={ fieldValue }
                onChange={ changeHandler }
                inputProps={ { maxLength: maxValue } }
                fullWidth={ true }
                style={ { background } }
                disabled={ disabled }
                placeholder={ placeholder }
            />
            { maxValue && fieldValue && <MaxLengthLabel value={ fieldValue.length } maxLength={ maxValue } /> }
        </FormAndLabel>
    );
};