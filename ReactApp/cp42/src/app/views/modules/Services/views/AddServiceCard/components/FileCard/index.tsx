import { FETCH_API } from 'app/api/useFetchApi';
import React, { memo } from 'react';
import FileDownloadIcon from '@mui/icons-material/FileDownload';
import OutlinedInput from '@mui/material/OutlinedInput';

import { TFileInformationResponse } from 'app/web/api/ServicesProxy/response-dto';
import { TextOut } from 'app/views/components/TextOut';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { useLocation } from 'react-router';

import styles from './style.module.css';

type TProps = {
    fileInfo: TFileInformationResponse;
    versionId?: string;
};

export const FileCard = memo(({ fileInfo: { type, name, synonym, version }, versionId }: TProps) => {
    const { pathname } = useLocation();

    const downloadVersionHandler = async () => {
        if (versionId) {
            const { data, message } = await FETCH_API.SERVICES.getVersionUrl(pathname.split('/')[2], versionId);

            if (!message && data && 'url' in data) {
                window.open(data.url, '_blank');
            }
        }
    };

    return (
        <>
            <TextOut fontWeight={ 700 }>Файл разработки (.{ type })</TextOut>
            <div className={ styles.fileSection }>
                <div className={ styles.fileContainer }>
                    <div className={ styles.fileContainer_topBlock }>
                        <TextOut fontWeight={ 700 }>Название файла:</TextOut>
                    </div>
                    <div className={ styles.fileContainer_bottomBlock }>
                        <TextOut fontWeight={ 700 }>{ `${ name }.${ type }` }</TextOut>
                        <FileDownloadIcon className={ styles.icon } onClick={ downloadVersionHandler } />
                    </div>
                </div>
            </div>
            <div className={ styles.cardContainer }>
                <FormAndLabel label="Название">
                    <OutlinedInput disabled={ true } value={ name } fullWidth={ true } />
                </FormAndLabel>
                <FormAndLabel label="Синоним">
                    <OutlinedInput disabled={ true } value={ synonym } fullWidth={ true } />
                </FormAndLabel>
                <FormAndLabel label="Версия">
                    <OutlinedInput disabled={ true } value={ version } fullWidth={ true } />
                </FormAndLabel>
            </div>
        </>
    );
});