import React, { memo, useMemo, useState } from 'react';

import { INTERNAL_CLOUD_SERVICE } from 'app/views/modules/Services/constants/BillingSerivceConstants';
import { TServiceBillingDataResponse } from 'app/web/api/ServicesProxy/response-dto';
import { SuccessButton } from 'app/views/components/controls/Button';

import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { RequestKind } from 'core/requestSender/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { TErrorType } from 'app/views/modules/Services/types/ErrorTypes';
import { MenuItem, Select } from '@mui/material';
import { SelectChangeEvent } from '@mui/material/Select';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import styles from './style.module.css';

type TServiceManagingProps = FloatMessageProps & {
    billingInfo: TServiceBillingDataResponse;
    serviceId: string;
    isAllowedEdit: boolean;
    refreshData: () => void;
};

export const ServiceManaging = memo(({ billingInfo, serviceId, floatMessage: { show }, isAllowedEdit, refreshData }: TServiceManagingProps) => {
    const { InternalCloudService, ReferralLinkForMarket, BillingServiceStatus } = billingInfo;
    const [internalCloudService, setInternalCloudService] = useState<string | null>(InternalCloudService);
    const [billingServiceStatus, setBillingServiceStatus] = useState(BillingServiceStatus);

    const onSelectInternalCloudService = (event: SelectChangeEvent) => {
        setInternalCloudService(event.target.value);
    };

    const onSelectBillingServiceStatus = (event: SelectChangeEvent<1 | 2 | 3>) => {
        setBillingServiceStatus(event.target.value as 1 | 2 | 3);
    };

    const renderArrayOfOption = useMemo(() => {
        const result: React.JSX.Element[] = [];

        for (let i = 0; i < 7; ++i) {
            result.push(<MenuItem value={ i } key={ INTERNAL_CLOUD_SERVICE[i] }>{ INTERNAL_CLOUD_SERVICE[i] }</MenuItem>);
        }

        return result;
    }, []);

    const saveChanges = (_: React.MouseEvent<Element, MouseEvent>) => {
        InterlayerApiProxy.getServicesApiProxy().reworkServiceBillingManaging(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, {
            Id: serviceId,
            InternalCloudService: internalCloudService === null ? internalCloudService : parseInt(internalCloudService, 10),
            BillingServiceStatus: billingServiceStatus
        }).then(result => {
            if (result && !result.hasOwnProperty('Code')) {
                show(MessageType.Success, 'Данные успешно сохранены');
                refreshData();
            } else {
                show(MessageType.Error, (result as TErrorType)?.Description ?? 'Произошла ошибка при сохранении данных');
                setBillingServiceStatus(BillingServiceStatus);
                setInternalCloudService(InternalCloudService);
            }
        });
    };

    return (
        <div className={ styles.container }>
            <FormAndLabel label="Статус">
                <Select disabled={ !isAllowedEdit } fullWidth={ true } value={ billingServiceStatus } onChange={ onSelectBillingServiceStatus }>
                    <MenuItem value={ 2 }>На модерацию</MenuItem>
                    <MenuItem value={ 3 }>Готов к использованию</MenuItem>
                </Select>
            </FormAndLabel>
            <FormAndLabel label="Внутренний облачный сервис">
                <Select disabled={ !isAllowedEdit } fullWidth={ true } onChange={ onSelectInternalCloudService } value={ internalCloudService ?? '0' }>
                    { renderArrayOfOption }
                </Select>
            </FormAndLabel>
            <FormAndLabel label="Реферальная ссылка для маркета">
                <a href={ ReferralLinkForMarket }>{ ReferralLinkForMarket }</a>
            </FormAndLabel>
            <div className={ styles.buttonContainer }>
                <SuccessButton isEnabled={ isAllowedEdit } onClick={ saveChanges }>Сохранить изменения</SuccessButton>
            </div>
        </div>
    );
});