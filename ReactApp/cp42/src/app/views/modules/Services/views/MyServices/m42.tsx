import React from 'react';

import { Market42ServicesList } from 'app/views/modules/Services/views/MyServices/components/Market42ServicesList';
import { withHeader } from 'app/views/components/_hoc/withHeader';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';

import styles from './style.module.css';

export const M42 = withFloatMessages(({ floatMessage }) => {
    return (
        <div className={ styles.pageContainer }>
            <Market42ServicesList floatMessage={ floatMessage } />
        </div>
    );
});

export const M42PageView = withHeader({
    title: 'Сервисы Маркет42',
    page: M42,
});