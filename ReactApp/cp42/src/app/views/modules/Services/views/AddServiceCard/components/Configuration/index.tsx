import React, { memo } from 'react';
import { SelectWithChipClass } from 'app/views/modules/Services/views/AddServiceCard/components/SelectConfigs';
import { Chip } from '@mui/material';
import { TConfigurationsResponse, TFileInformationResponse } from 'app/web/api/ServicesProxy/response-dto';
import styles from './style.module.css';

type TProps = {
    isEditable: boolean;
    index: number;
    file: TFileInformationResponse;
    allConfigs: TConfigurationsResponse[];
    callBack: (selectedConfigs: TFileInformationResponse[]) => void;
    configFiles: TFileInformationResponse[];
};

export const Configuration = memo(({ isEditable, index, file, configFiles, allConfigs, callBack }: TProps) => {
    if (isEditable) {
        return (
            <SelectWithChipClass
                allConfigs={ allConfigs }
                callBack={ callBack }
                index={ index }
                configFiles={ configFiles }
                isEditable={ isEditable }
            />
        );
    }

    return (
        <div className={ styles.configsContainer }>
            { file.configurations.map(({ name }) => <Chip className={ styles.chip } label={ name } key={ name } variant="outlined" color="primary" />) }
        </div>
    );
});