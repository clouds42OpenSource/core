import { TextOut } from 'app/views/components/TextOut';
import { CreateServiceButton } from 'app/views/modules/Services/views/MyServices/components/AccountServicesList/components';
import React from 'react';
import { Divider, Pagination } from '@mui/material';
import { Link } from 'react-router-dom';

import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { RequestKind } from 'core/requestSender/enums';
import { TAccountServicesResponse } from 'app/web/api/ServicesProxy/response-dto';
import styles from 'app/views/modules/Services/views/MyServices/style.module.css';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { connect } from 'react-redux';
import { AccountUserGroup } from 'app/common/enums';
import { AppReduxStoreState } from 'app/redux/types';
import { ADMIN_ROLES } from 'app/views/modules/Services/constants/UserRoleConstants';
import { InputField } from 'app/views/modules/Services/views/AddServiceCard/components/InputField';
import { SERVICES_ROUTES } from 'app/views/modules/Services/constants/RoutesConstants';

type TAccountAddedServicesListState = {
    loading: boolean;
    pages: number;
    servicesListBackup: TAccountServicesResponse[];
    servicesList: TAccountServicesResponse[];
    currentList: TAccountServicesResponse[];
};

type TReduxProps = {
    currentUserGroup: AccountUserGroup[];
};

type TAccountAddedServicesListProps = FloatMessageProps & TReduxProps & {
    itemsAtList: number;
};

class AccountAddedServicesList extends React.Component<TAccountAddedServicesListProps, TAccountAddedServicesListState> {
    public static defaultProps: TAccountAddedServicesListProps = {
        itemsAtList: 10,
        floatMessage: {
            show: () => void 0
        },
        currentUserGroup: []
    };

    constructor(props: TAccountAddedServicesListProps) {
        super(props);
        this.state = {
            servicesListBackup: [],
            servicesList: [],
            currentList: [],
            pages: 0,
            loading: true,
        };
        this.onPaginate = this.onPaginate.bind(this);
        this.renderServicesList = this.renderServicesList.bind(this);
        this.onSearch = this.onSearch.bind(this);
    }

    public componentDidMount() {
        const { itemsAtList, floatMessage: { show } } = this.props;

        InterlayerApiProxy.getServicesApiProxy().receiveAccountServices(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY)
            .then(result => {
                if (result && !Object.hasOwn(result, 'error')) {
                    this.setState({
                        servicesListBackup: result,
                        servicesList: result,
                        currentList: result.slice(0, itemsAtList),
                        pages: Math.ceil(result.length / itemsAtList)
                    });
                } else {
                    show(MessageType.Error, 'Не удалось получить список сервисов с аккаунта');
                }

                this.setState({ loading: false });
            });
    }

    public componentDidUpdate(prevProps: Readonly<TAccountAddedServicesListProps>, { servicesList: prevServicesList }: Readonly<TAccountAddedServicesListState>) {
        const { servicesList } = this.state;
        const { itemsAtList } = this.props;

        if (prevServicesList.length !== servicesList.length) {
            this.setState({
                currentList: servicesList.slice(0, itemsAtList),
                pages: Math.ceil(servicesList.length / itemsAtList)
            });
        }
    }

    private onSearch(ev: React.ChangeEvent<HTMLInputElement>) {
        const regex = ev.target.value.toLowerCase();
        const arr = this.state.servicesListBackup.filter(item => item['service-name'].toLowerCase().includes(regex));
        this.setState({ servicesList: arr });
    }

    private onPaginate(_: React.ChangeEvent<unknown>, page: number) {
        const { itemsAtList } = this.props;

        this.setState(prevState => ({
            currentList: prevState.servicesList.slice((page - 1) * itemsAtList, page * itemsAtList),
        }));
    }

    private renderServicesList() {
        const { currentList } = this.state;

        return currentList.map(service => (
            <div className={ styles.extendServiceItem } key={ service['service-id'] + service['service-version'] }>
                <div className={ styles.serviceItem }>
                    { service.draft
                        ? (
                            <Link to={ `${ SERVICES_ROUTES.EditServicePageUrl }/${ service['service-id'] }` } className={ styles.serviceName }>
                                { service['service-name'] } <TextOut fontSize={ 10 } className={ styles.warning }>(Черновик)</TextOut>
                            </Link>
                        )
                        : (
                            <Link to={ `${ SERVICES_ROUTES.ServiceCardPageUrl }/${ service['service-id'] }` } className={ styles.serviceName }>
                                { service['service-name'] }
                            </Link>
                        )
                    }
                    <div className={ styles.serviceVersion }>
                        { service.available && <span>{ service['service-version'] }</span> }
                    </div>
                </div>
                <Divider className={ styles.divider } />
            </div>
        ));
    }

    public render() {
        const { loading, currentList, pages, servicesListBackup } = this.state;
        const isAllowed = this.props.currentUserGroup.some(group => ADMIN_ROLES.includes(group));

        return (
            <div className={ styles.container }>
                { loading && <LoadingBounce /> }
                <div className={ styles.containerHeader }>
                    <h3>Сервисы добавленные в рамках аккаунта</h3>
                    <InputField changeHandler={ this.onSearch } disabled={ false } placeholder="Поиск" background="white" />
                </div>
                { servicesListBackup.length > 0 &&
                    <div>
                        <div className={ styles.tableHeader }>
                            <span>Название:</span>
                            <span>Доступная версия:</span>
                        </div>
                        <div className={ styles.servicesList }>
                            <Divider className={ styles.dividerBold } />
                            { this.renderServicesList() }
                        </div>
                    </div>
                }
                <div className={ isAllowed ? styles.bottomContainer : styles.bottomContainerDisabled }>
                    { isAllowed && <CreateServiceButton /> }
                    { currentList && <Pagination count={ pages } onChange={ this.onPaginate } shape="rounded" className={ styles.pagination } /> }
                </div>
            </div>
        );
    }
}

export const AccountServicesList = connect<TReduxProps, {}, {}, AppReduxStoreState>(
    state => ({ currentUserGroup: state.Global.getCurrentSessionSettingsReducer.settings.currentContextInfo.currentUserGroups }), {}
)(AccountAddedServicesList);