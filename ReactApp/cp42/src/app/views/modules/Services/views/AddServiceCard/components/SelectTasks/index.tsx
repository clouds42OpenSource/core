import { DAYS } from 'app/views/modules/Services/constants/DaysConstants';
import { CheckBoxForm } from 'app/views/components/controls/forms';
import React from 'react';
import { Accordion, AccordionDetails, AccordionSummary, Box, Chip, FormControl, MenuItem, OutlinedInput, Select, SelectChangeEvent } from '@mui/material';
import { SELECT_MENU_PROPS, SELECT_STYLE_CONSTANT } from 'app/views/modules/Services/constants/SelectStyleConstants';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { TextOut } from 'app/views/components/TextOut';
import { TCommand, TFileInformationResponse } from 'app/web/api/ServicesProxy/response-dto';
import styles from './style.module.css';

type TProps = {
    index: number;
    configFiles: TFileInformationResponse[];
    selectTask: (event: SelectChangeEvent<TCommand[]>, _: React.ReactNode, index: number) => void;
    deleteTask: (_: React.MouseEvent<HTMLDivElement, MouseEvent>, elem: string, index: number) => void;
    addTimeTableInfo: (ev: React.ChangeEvent<HTMLInputElement>, index: number, i: number) => void;
    addWeekDays: (ev: React.MouseEvent<HTMLDivElement, MouseEvent>, index: number, i: number) => void;
    isEditable: boolean;
};

export const SelectTasks = ({ index, configFiles, selectTask, deleteTask, addTimeTableInfo, addWeekDays, isEditable }: TProps) => {
    const { chosenCommands, commands } = configFiles[index];

    const checkWeekDay = (value: number, name: string) => {
        if (isEditable) {
            const command = chosenCommands.find(c => c.name === name)?.['week-days'];

            if (command) {
                return !!Array.from(command.values()).find((elem: number) => elem === value);
            }
        } else {
            const command = commands.find(c => c.name === name)?.['week-days'];

            if (command) {
                return !!Array.from(command.values()).find((elem: number) => elem === value);
            }
        }
    };

    return (
        <FormControl sx={ { width: '100%' } }>
            <Select
                className={ styles.select }
                disabled={ !isEditable }
                MenuProps={ SELECT_MENU_PROPS }
                sx={ SELECT_STYLE_CONSTANT }
                multiple={ false }
                value={ isEditable ? chosenCommands : commands }
                input={ <OutlinedInput label="Chip" /> }
                inputProps={ { shrink: false } }
                onChange={ (event, child) => selectTask(event, child, index) }
                renderValue={ selected => (
                    <Box className={ styles.selectValue_box }>
                        { selected.map((elem, i) => {
                            return (
                                <div key={ elem.name } style={ { display: 'flex', flexDirection: 'row', gap: '5px' } }>
                                    <Chip
                                        label={ commands.find(c => c.name === elem.name)?.presentation ?? '' }
                                        onMouseDown={ event => event.stopPropagation() }
                                        className={ styles.selected }
                                        onClick={ ev => deleteTask(ev, elem.name, index) }
                                        disabled={ !isEditable }
                                    />
                                    <Accordion className={ styles.timeTable } onMouseDown={ event => event.stopPropagation() }>
                                        <AccordionSummary className={ styles.timeTableSummary } expandIcon={ <ExpandMoreIcon /> }>
                                            <TextOut>Расписание</TextOut>
                                        </AccordionSummary>
                                        <AccordionDetails className={ styles.timeTable__container }>
                                            <div style={ { display: 'flex', gap: '5px' } }>
                                                <div className={ styles.timeTable__formRepeat }>
                                                    <TextOut>Повторять каждые: </TextOut>
                                                    <input
                                                        disabled={ !isEditable }
                                                        type="number"
                                                        defaultValue={ elem['days-repeat-period'] }
                                                        className={ styles.timeTable__input }
                                                        id="days-repeat-period"
                                                        onChange={ ev => addTimeTableInfo(ev, index, i) }
                                                    />
                                                    <TextOut>(дн.)</TextOut>
                                                    <TextOut>Повторять через: </TextOut>
                                                    <input
                                                        disabled={ !isEditable }
                                                        type="number"
                                                        defaultValue={ elem['repeat-period-in-day'] }
                                                        className={ styles.timeTable__input }
                                                        id="repeat-period-in-day"
                                                        onChange={ ev => addTimeTableInfo(ev, index, i) }
                                                    />
                                                    <TextOut>(сек.)</TextOut>
                                                </div>
                                                <div className={ styles.timeTable__formTime }>
                                                    <TextOut>Время начала: </TextOut>
                                                    <input
                                                        disabled={ !isEditable }
                                                        maxLength={ 5 }
                                                        type="time"
                                                        defaultValue={ elem['begin-time'] }
                                                        className={ styles.timeTable__input }
                                                        id="begin-time"
                                                        onChange={ ev => addTimeTableInfo(ev, index, i) }
                                                    />
                                                    <TextOut>Время окончания: </TextOut>
                                                    <input
                                                        disabled={ !isEditable }
                                                        maxLength={ 5 }
                                                        type="time"
                                                        defaultValue={ elem['end-time'] }
                                                        className={ styles.timeTable__input }
                                                        id="end-time"
                                                        onChange={ ev => addTimeTableInfo(ev, index, i) }
                                                    />
                                                </div>
                                            </div>
                                            <div style={ { display: 'flex' } } onClick={ ev => addWeekDays(ev, index, i) }>
                                                { DAYS.map(({ label, formName }, j) =>
                                                    <CheckBoxForm
                                                        isReadOnly={ !isEditable }
                                                        key={ label }
                                                        isChecked={ checkWeekDay(++j, elem.name) }
                                                        className={ styles.timeTable__checkbox }
                                                        label={ label }
                                                        formName={ formName }
                                                    />
                                                ) }
                                            </div>
                                        </AccordionDetails>
                                    </Accordion>
                                </div>
                            );
                        }) }
                    </Box>
                ) }
            >
                { isEditable && commands.flatMap(({ name }, i) => (
                    chosenCommands.find(cmd => cmd.name === commands[i].name)
                        ? []
                        : (
                            <MenuItem key={ name } value={ name }>
                                { commands[i].presentation }
                            </MenuItem>
                        )
                )) }
                { isEditable && commands.length === chosenCommands.length && <MenuItem disabled={ true }>Доступных заданий не найдено</MenuItem> }
            </Select>
        </FormControl>
    );
};