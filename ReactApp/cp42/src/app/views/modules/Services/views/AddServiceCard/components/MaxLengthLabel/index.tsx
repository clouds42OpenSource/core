import React from 'react';

type TMaxLengthLabelProps = {
    value: number;
    maxLength: number;
};

export const MaxLengthLabel = ({ maxLength, value }: TMaxLengthLabelProps) => (
    <div style={ { fontSize: '12px', marginTop: '5px' } }>
        Осталось символов: { maxLength - value }
    </div>
);