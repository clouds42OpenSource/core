import { Box } from '@mui/system';
import { AccountUserGroup } from 'app/common/enums';
import { AppReduxStoreState } from 'app/redux/types';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { withHeader } from 'app/views/components/_hoc/withHeader';
import { PageHeaderView } from 'app/views/components/_hoc/withHeader/PageHeaderView';
import { ContainedButton, SuccessButton } from 'app/views/components/controls/Button';
import { TabsControl } from 'app/views/components/controls/TabsControl';
import { SERVICES_ROUTES } from 'app/views/modules/Services/constants/RoutesConstants';
import { ADMIN_ROLES } from 'app/views/modules/Services/constants/UserRoleConstants';
import { EditServicePageTabEnum } from 'app/views/modules/Services/enums/EditServicePageTabEnum';
import { TErrorMSType } from 'app/views/modules/Services/types/ErrorTypes';
import { CreateService } from 'app/views/modules/Services/views/AddServiceCard';
import { ServiceManaging } from 'app/views/modules/Services/views/EditServiceCard/ServiceManaging';
import { ServiceMarketing } from 'app/views/modules/Services/views/EditServiceCard/ServiceMarketing';
import { ServiceModeration } from 'app/views/modules/Services/views/EditServiceCard/ServiceModeration';
import { ServiceFileVersionModule } from 'app/views/modules/Services/views/EditServiceCard/ServiceVersions';
import { TServiceBillingDataResponse, TServiceInformation } from 'app/web/api/ServicesProxy/response-dto';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { RequestKind } from 'core/requestSender/enums';
import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { RouteComponentProps } from 'react-router-dom';
import style from './style.module.css';

interface IRouterProps {
    /**
     * ID сервиса
     */
    id: string;
}

type TReduxProps = {
    currentUserGroup: AccountUserGroup[];
};

type TEditServicePageProps = RouteComponentProps<IRouterProps> & FloatMessageProps & TReduxProps;

type TEditServicePageState = {
    tabIndex: number;
    isEditMode: boolean;
    billingInfo: TServiceBillingDataResponse | null;
    serviceInfo: TServiceInformation | null;
};

class EditServicePage extends React.Component<TEditServicePageProps, TEditServicePageState> {
    private readonly id: string;

    constructor(props: TEditServicePageProps) {
        super(props);
        this.id = this.props.match.params.id;
        this.state = {
            tabIndex: EditServicePageTabEnum.Info,
            isEditMode: false,
            billingInfo: null,
            serviceInfo: null
        };
        this.handleTabChange = this.handleTabChange.bind(this);
        this.buttonHandler = this.buttonHandler.bind(this);
        this.getData = this.getData.bind(this);
        this.renderTab = this.renderTab.bind(this);
        this.deleteDraft = this.deleteDraft.bind(this);
    }

    public componentDidMount() {
        this.getData();
    }

    private handleTabChange(newValue: number) {
        if (newValue > 0) {
            this.setState({ isEditMode: false });
        }

        this.setState({ tabIndex: newValue });
    }

    private getData() {
        const { show } = this.props.floatMessage;
        const api = InterlayerApiProxy.getServicesApiProxy();

        try {
            api.receiveServiceInfo(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, this.id)
                .then(receivedInfo => {
                    if (receivedInfo && !('error' in receivedInfo)) {
                        this.setState({ serviceInfo: receivedInfo.Result });

                        if (receivedInfo.Result.Draft) {
                            this.setState({ isEditMode: true });
                        }
                    } else {
                        show(MessageType.Error, (receivedInfo as unknown as TErrorMSType).message);

                        if ((receivedInfo as unknown as TErrorMSType).error === '409-card-deleted') {
                            this.props.history.push(SERVICES_ROUTES.ServiceCardPageUrl);
                        }
                    }
                });

            api.receiveBillingServiceData(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, this.id)
                .then(receivedBilling => {
                    if (receivedBilling && typeof receivedBilling !== 'string' && !('Code' in receivedBilling)) {
                        this.setState({ billingInfo: receivedBilling });
                    }
                });

            if (this.state.isEditMode) {
                this.buttonHandler();
            }
        } catch (err: unknown) {
            show(MessageType.Error, 'Произошла непредвиденная ошибка');
        }
    }

    private buttonHandler() {
        this.setState(prevState => ({ isEditMode: !prevState.isEditMode }));
    }

    private deleteDraft() {
        const { floatMessage, history } = this.props;

        InterlayerApiProxy.getServicesApiProxy().deleteServiceInfo(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, this.id)
            .then(result => {
                if (result && !result.hasOwnProperty('error')) {
                    history.push(SERVICES_ROUTES.ServiceCardPageUrl);
                } else {
                    floatMessage.show(MessageType.Error, (result as unknown as TErrorMSType)?.message ?? 'Не удалось удалить черновик');
                }
            });
    }

    private renderTab(children: React.ReactNode, index: number) {
        const { tabIndex } = this.state;

        return (
            <div hidden={ tabIndex !== index } id={ index.toString() }>
                { tabIndex === index && <Box sx={ { py: 3 } }>{ children }</Box> }
            </div>
        );
    }

    public render() {
        const { isEditMode, billingInfo, serviceInfo } = this.state;
        const { floatMessage, currentUserGroup } = this.props;
        const isAllowedEdit = currentUserGroup.some(group => ADMIN_ROLES.includes(group));
        const isAllowedModerate = !!currentUserGroup.find(group => group === AccountUserGroup.CloudAdmin) && !serviceInfo?.Draft;
        const isAllowedAcceptProposals = isAllowedModerate || !!currentUserGroup.find(group => group === AccountUserGroup.Hotline);
        const pageAvailable = billingInfo && !serviceInfo?.Draft;

        const headers = [{
            title: 'Общее',
            isVisible: true
        }, {
            title: 'Версии файлов',
            isVisible: !serviceInfo?.Draft
        }];

        if (pageAvailable) {
            headers.push({
                title: 'Реклама',
                isVisible: !serviceInfo?.Draft
            }, {
                title: 'Заявки на модерацию',
                isVisible: !serviceInfo?.Draft
            });

            if (isAllowedModerate) {
                headers.push({
                    title: 'Управление',
                    isVisible: !serviceInfo?.Draft
                });
            }
        }

        return (
            <div>
                { serviceInfo &&
                    <>
                        <PageHeaderView title={ serviceInfo.ServiceName } />
                        <TabsControl headers={ headers } onActiveTabIndexChanged={ this.handleTabChange } />
                        { this.renderTab(
                            <CreateService
                                isServiceHasBilling={ !!billingInfo }
                                isModifyPage={ true }
                                isEditMode={ isEditMode }
                                serviceId={ this.id }
                                floatMessage={ floatMessage }
                                refreshServiceData={ this.getData }
                            >
                                <div className={ style.buttonContainer }>
                                    { serviceInfo.Draft &&
                                        <ContainedButton kind="error" onClick={ this.deleteDraft }>Удалить черновик</ContainedButton>
                                    }
                                    { !serviceInfo.Draft && !isEditMode && isAllowedEdit &&
                                        <SuccessButton className={ style.button } onClick={ this.buttonHandler }>
                                            Редактировать сервис
                                        </SuccessButton>
                                    }
                                </div>
                            </CreateService>, EditServicePageTabEnum.Info)
                        }
                        { this.renderTab(
                            <ServiceFileVersionModule
                                floatMessage={ floatMessage }
                                isAllowedEdit={ isAllowedEdit }
                            />, EditServicePageTabEnum.Versions)
                        }
                        { pageAvailable && this.renderTab(
                            <ServiceMarketing
                                isAvailable={ !serviceInfo.Private }
                                serviceId={ this.id }
                                defaultTitle={ serviceInfo.ServiceName }
                                defaultDescription={ serviceInfo.ServiceShortDescription }
                            />, EditServicePageTabEnum.Marketing)
                        }
                        { pageAvailable && this.renderTab(
                            <ServiceModeration
                                serviceId={ this.id }
                                isNotAllowedEdit={ !isAllowedAcceptProposals }
                            />, EditServicePageTabEnum.Moderation)
                        }
                        { pageAvailable && isAllowedModerate && billingInfo && this.renderTab(
                            <ServiceManaging
                                refreshData={ this.getData }
                                floatMessage={ floatMessage }
                                billingInfo={ billingInfo }
                                serviceId={ this.id }
                                isAllowedEdit={ isAllowedEdit }
                            />, EditServicePageTabEnum.Billing)
                        }
                    </>
                }
            </div>
        );
    }
}

const EditServicePageConnect = connect<TReduxProps, {}, {}, AppReduxStoreState>(
    state => ({ currentUserGroup: state.Global.getCurrentSessionSettingsReducer.settings.currentContextInfo.currentUserGroups }), {}
)(EditServicePage);

export const EditServicePageView = withHeader({
    title: 'Карточка сервиса',
    page: withRouter(withFloatMessages(EditServicePageConnect))
});