import { Divider, Pagination } from '@mui/material';
import { AppRoutes } from 'app/AppRoutes';
import { SortingKind } from 'app/common/enums';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { SuccessButton } from 'app/views/components/controls/Button';
import { getSortaLabelElement } from 'app/views/components/controls/Table/functions';
import { InputField } from 'app/views/modules/Services/views/AddServiceCard/components/InputField';
import styles from 'app/views/modules/Services/views/MyServices/style.module.css';
import { EMarket42ServiceElementState } from 'app/web/api/ServicesProxy/response-dto';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import cn from 'classnames';
import { RequestKind } from 'core/requestSender/enums';
import React from 'react';
import { Link } from 'react-router-dom';

type TListItem = {
    serviceId: string;
    serviceName: string;
    state: EMarket42ServiceElementState;
};

type TMarket42ServicesListProps = FloatMessageProps & {
    itemsAtList: number;
};

type TMarket42ServicesListState = {
    servicesListBackup: TListItem[];
    servicesList: TListItem[];
    currentList: TListItem[];
    pages: number;
    currentPage: number;
    sortKind?: SortingKind | undefined
};

export class Market42ServicesList extends React.Component<TMarket42ServicesListProps, TMarket42ServicesListState> {
    public static defaultProps: TMarket42ServicesListProps = {
        itemsAtList: 10,
        floatMessage: {
            show: () => void 0
        }
    };

    constructor(props: TMarket42ServicesListProps) {
        super(props);
        this.state = {
            servicesListBackup: [],
            servicesList: [],
            currentList: [],
            pages: 0,
            currentPage: 1
        };
        this.loadMarket42Service = this.loadMarket42Service.bind(this);
        this.renderListItem = this.renderListItem.bind(this);
        this.linkToMarket = this.linkToMarket.bind(this);
        this.onPaginate = this.onPaginate.bind(this);
        this.onSearch = this.onSearch.bind(this);
        this.sorting = this.sorting.bind(this);
        this.getStateText = this.getStateText.bind(this);
    }

    public componentDidMount() {
        this.loadMarket42Service()
            .then(result => {
                if (result && !Object.hasOwn(result, 'Code')) {
                    const { itemsAtList } = this.props;
                    const mappedResult: TListItem[] = [];

                    result.Table.Row.forEach((p, i) => mappedResult[i] = { serviceId: p.Key, serviceName: p.Representation, state: p.State });

                    this.setState({
                        servicesListBackup: mappedResult,
                        servicesList: mappedResult,
                        currentList: mappedResult.slice(0, itemsAtList),
                        pages: Math.ceil(mappedResult.length / itemsAtList)
                    });
                } else {
                    this.props.floatMessage.show(MessageType.Error, 'Не удалось получить список сервисов с Маркет 42');
                }
            });
    }

    public componentDidUpdate(prevProps: Readonly<TMarket42ServicesListProps>, { servicesList: servicesListPrev, sortKind: prevSortKind }: Readonly<TMarket42ServicesListState>) {
        const { itemsAtList } = this.props;
        const { servicesList, sortKind } = this.state;

        if (servicesListPrev.length !== servicesList.length) {
            this.setState({
                currentList: servicesList.slice(0, itemsAtList),
                pages: Math.ceil(servicesList.length / itemsAtList)
            });
        }

        if (prevSortKind !== sortKind) {
            this.setState(prev => ({
                currentList: prev.servicesList
                    .sort((a, b) => {
                        const firstElement = a.state;
                        const secondElement = b.state;

                        if (sortKind) {
                            if (firstElement > secondElement) {
                                return 1;
                            }

                            if (firstElement < secondElement) {
                                return -1;
                            }

                            return 0;
                        }

                        if (firstElement > secondElement) {
                            return -1;
                        }

                        if (firstElement < secondElement) {
                            return 1;
                        }

                        return 0;
                    }).slice((this.state.currentPage - 1) * itemsAtList, this.state.currentPage * itemsAtList)
            }));
        }
    }

    private onSearch(ev: React.ChangeEvent<HTMLInputElement>) {
        const regex = ev.target.value.toLowerCase();
        const arr = this.state.servicesListBackup.filter(item => item.serviceName.toLowerCase().includes(regex));
        this.setState({ servicesList: arr });
    }

    private onPaginate(_: React.ChangeEvent<unknown>, page: number) {
        const { itemsAtList } = this.props;
        this.setState({
            currentList: this.state.servicesList.slice((page - 1) * itemsAtList, page * itemsAtList),
            currentPage: page
        });
    }

    private getStateText(state: EMarket42ServiceElementState) {
        switch (state) {
            case EMarket42ServiceElementState.Enabled:
                return 'Включен';
            case EMarket42ServiceElementState.Disabled:
                return 'Выключен';
            case EMarket42ServiceElementState.Demo:
                return 'Активно демо';
            case EMarket42ServiceElementState.DemoDisabled:
                return 'Неактивно демо';
            default:
                return 'Отключен';
        }
    }

    private sorting() {
        const { sortKind } = this.state;

        if (sortKind !== undefined) {
            this.setState(prev => ({
                sortKind: prev.sortKind ? SortingKind.Asc : SortingKind.Desc
            }));
        } else {
            this.setState({
                sortKind: SortingKind.Asc
            });
        }
    }

    private linkToMarket() {
        window.open('https://42clouds.com/ru-ru/market/', '_blank');
    }

    private loadMarket42Service() {
        return InterlayerApiProxy.getServicesApiProxy().receiveMarket42Services(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY);
    }

    private renderListItem(itemList: TListItem[]) {
        return itemList.map(item => (
            <div key={ JSON.stringify(item) } className={ styles.extendServiceItem }>
                <div className={ styles.serviceItem }>
                    <Link to={ `${ AppRoutes.services.managingServiceDescriptions }/${ item.serviceId }` } className={ styles.serviceName }>{ item.serviceName }</Link>
                    <span
                        className={
                            cn(
                                styles.status,
                                {
                                    [styles.success]: item.state === EMarket42ServiceElementState.Enabled,
                                    [styles.warning]: item.state === EMarket42ServiceElementState.Demo,
                                    [styles.error]: item.state === EMarket42ServiceElementState.Disabled || item.state === EMarket42ServiceElementState.DemoDisabled
                                }
                            )
                        }
                    >
                        { this.getStateText(item.state) }
                    </span>
                </div>
                <Divider className={ styles.divider } />
            </div>
        ));
    }

    public render() {
        const { currentList, pages, servicesListBackup, sortKind } = this.state;

        return (
            <div className={ styles.container }>
                <div className={ styles.containerHeader }>
                    <h3>Маркет 42</h3>
                    <InputField changeHandler={ this.onSearch } disabled={ false } placeholder="Поиск" background="white" />
                </div>
                { servicesListBackup.length > 0 &&
                    <div>
                        <div className={ styles.tableHeader }>
                            <span>Название:</span>
                            <span className={ styles.status } onClick={ this.sorting }>
                                Статус
                                { getSortaLabelElement({ isSortable: true, sortKind }) }
                            </span>
                        </div>
                        <div className={ styles.servicesList }>
                            <Divider className={ styles.dividerBold } />
                            { this.renderListItem(currentList) }
                        </div>
                    </div>
                }
                <div className={ styles.bottomContainer }>
                    <SuccessButton className={ styles.successButton } onClick={ this.linkToMarket }>Добавить сервис</SuccessButton>
                    <Pagination count={ pages } onChange={ this.onPaginate } shape="rounded" className={ styles.pagination } />
                </div>
            </div>
        );
    }
}