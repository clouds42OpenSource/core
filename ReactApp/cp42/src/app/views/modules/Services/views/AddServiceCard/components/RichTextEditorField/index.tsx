import React, { memo } from 'react';
import styles from 'app/views/modules/Services/views/AddServiceCard/style.module.css';
import { RichTextEditor } from 'app/views/components/RichTextEditor';
import { ButtonsToolbarListType } from 'app/views/components/RichTextEditor/types';

type TProps = {
    label: string;
    richTextEditorHandler: (name: string, value: string) => void;
    disabled: boolean;
    value: string;
};

const buttonList: ButtonsToolbarListType = [
    ['undo', 'redo'],
    ['formatBlock'],
    ['bold', 'underline', 'italic'],
    ['outdent', 'indent'],
    ['align', 'horizontalRule', 'list', 'table'],
    ['link', 'image', 'video'],
    ['codeView'],
    ['math']
];

export const RichTextEditorField = memo(({ label, richTextEditorHandler, value, disabled }: TProps) => {
    return (
        <div className={ styles.richTextDescriptionContainer }>
            <label>{ label }</label>
            <RichTextEditor
                showToolbar={ !disabled }
                disable={ disabled }
                value={ value }
                formName="marketDescription"
                heightPercent="100%"
                toolBarHeight={ 200 }
                autoFocus={ false }
                language="ru"
                toolbarButtonList={ buttonList }
                onValueChange={ richTextEditorHandler }
                showPathLabel={ false }
            />
        </div>
    );
});