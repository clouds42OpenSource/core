import { Autocomplete, Box, Chip, createFilterOptions, FilterOptionsState, TextField } from '@mui/material';
import { SELECT_STYLE_CONSTANT } from 'app/views/modules/Services/constants/SelectStyleConstants';
import styles from 'app/views/modules/Services/views/AddServiceCard/components/BillingViewer/style.module.css';
import React, { useState } from 'react';

type TOptionType = {
    title: string;
    inputValue?: string;
}

const filter = createFilterOptions<TOptionType>();

type TCustomSelectProps = {
    options: TOptionType[];
    value: TOptionType[];
    onChange: (value: TOptionType[]) => void;
    setOptions: (options: string[]) => void;
    disabled?: boolean;
}

export const TagsSelect = ({ options, value, onChange, setOptions, disabled }: TCustomSelectProps) => {
    const [inputValue, setInputValue] = useState('');

    const onChangeAutocomplete = (_: React.SyntheticEvent, newValue: TOptionType[]) => {
        const newItem = newValue[newValue.length - 1];
        const isOptionPresent = (option: TOptionType) => option.title === newItem.title || option.inputValue === newItem.inputValue;

        if (!options.find(isOptionPresent)) {
            const newOpt = { title: newItem.inputValue ?? '' };
            setOptions([...options, newOpt].map(item => item.title));
            onChange([...value, newOpt]);
        } else if (value.find(isOptionPresent)) {
            onChange(value.filter(item => item.title !== newItem.title || item.inputValue !== newItem.inputValue));
        } else {
            onChange(newValue as TOptionType[]);
        }
    };

    const filterOptions = (options: TOptionType[], params: FilterOptionsState<TOptionType>) => {
        const filtered = filter(options, params);

        if (params.inputValue !== '') {
            filtered.push({
                inputValue: params.inputValue,
                title: `Добавить "${ params.inputValue }"`,
            });
        }

        return filtered.filter(option =>
            option.title.toLowerCase().includes(params.inputValue.toLowerCase())
        );
    };

    const getOptionLabel = (option: TOptionType) => option.inputValue ?? option.title;

    const renderOptionView = (props: React.HTMLAttributes<HTMLLIElement>, option: TOptionType) => {
        const selected = value.find(item => option.title === item.title);

        return (
            <li { ...props } style={ { backgroundColor: selected ? 'rgba(26, 179, 148, 0.08)' : '' } }>{ option.title }</li>
        );
    };

    const renderTags = (tagValue: TOptionType[]) => (
        <Box sx={ { display: 'flex', flexWrap: 'wrap', gap: '5px' } }>
            {
                tagValue.map(option => (
                    <Chip label={ option.title } key={ option.title } className={ styles.selected } />
                ))
            }
        </Box>
    );

    const handleInputChange = (_: React.SyntheticEvent, newInputValue: string) => {
        const sanitizedValue = newInputValue.replace(/,/g, '').slice(0, 150);
        setInputValue(sanitizedValue);
    };

    return (
        <Autocomplete
            disabled={ disabled }
            disableCloseOnSelect={ true }
            disableClearable={ true }
            multiple={ true }
            value={ value }
            onChange={ onChangeAutocomplete }
            onInputChange={ handleInputChange }
            inputValue={ inputValue }
            filterOptions={ filterOptions }
            options={ options }
            getOptionLabel={ getOptionLabel }
            renderOption={ renderOptionView }
            renderTags={ renderTags }
            renderInput={ params => <TextField { ...params } variant="outlined" /> }
            sx={ SELECT_STYLE_CONSTANT }
        />
    );
};