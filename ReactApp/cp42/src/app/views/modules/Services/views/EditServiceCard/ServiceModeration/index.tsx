import { Chip, Link, MenuItem, Select, SelectChangeEvent, Tooltip } from '@mui/material';
import { AccountUserGroup } from 'app/common/enums';
import { useAppSelector } from 'app/hooks';
import { TextOut } from 'app/views/components/TextOut';
import { memo, useCallback, useEffect, useState } from 'react';

import { TDatePicker } from 'app/common/types/app-types';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { SuccessButton } from 'app/views/components/controls/Button';
import { DoubleDateInputForm } from 'app/views/components/controls/forms/DoubleDateInputView';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { ModerationView } from 'app/views/modules/Services/views/EditServiceCard/ServiceModeration/components/ModerationView';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { TModerationList, TModerationListPagination } from 'app/web/api/ServicesProxy/response-dto';
import { SelectDataCommonDataModel } from 'app/web/common';
import { RequestKind } from 'core/requestSender/enums';

import dayjs from 'dayjs';
import styles from './style.module.css';

type TServiceModeration = FloatMessageProps & {
    serviceId?: string;
    isNotAllowedEdit?: boolean;
};

enum EModerationRequestType {
    undefined = 'Все',
    create = 'Создание',
    moderate = 'Редактирование'
}

enum ServiceModerationStatusEnum {
    Undefined = 'Все',
    OnModeration = 'На модерации',
    Accepted = 'Принята',
    Rejected = 'Отклонена'
}

const ServiceModerationComponent = ({ serviceId, floatMessage, isNotAllowedEdit }: TServiceModeration) => {
    const { show } = floatMessage;
    const { currentUserGroups } = useAppSelector(state => state.Global.getCurrentSessionSettingsReducer.settings.currentContextInfo);

    const [moderationList, setModerationList] = useState<TModerationList[]>([]);
    const [moderationInfo, setModerationInfo] = useState<TModerationList | null>(null);
    const [isLoading, setIsLoading] = useState(true);

    const [paginationParams, setPaginationParams] = useState<TModerationListPagination | null>(null);
    const [period, setPeriod] = useState<TDatePicker>({ from: null, to: null });
    const [status, setStatus] = useState<ServiceModerationStatusEnum>(ServiceModerationStatusEnum.Undefined);
    const [requestType, setRequestType] = useState<EModerationRequestType>(EModerationRequestType.undefined);

    const validateModerationList = useCallback((list: TModerationList[] | null) => {
        if (list) {
            setModerationList(list);
        } else {
            show(MessageType.Error, 'Не удалось получить список заявок на модерацию');
        }

        setIsLoading(false);
    }, [show]);

    const getData = useCallback(() => {
        setIsLoading(true);

        if (serviceId) {
            InterlayerApiProxy.getServicesApiProxy().receiveModerationList(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, serviceId)
                .then(list => {
                    validateModerationList(list?.Result ?? null);

                    if (list) {
                        setPaginationParams(list.pagination);
                    }
                });
        } else {
            InterlayerApiProxy.getServicesApiProxy().receiveModerationListAll(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, { page: 1 })
                .then(list => {
                    validateModerationList(list?.Result ?? null);

                    if (list) {
                        setPaginationParams(list.pagination);
                    }
                });
        }
    }, [serviceId, validateModerationList]);

    useEffect(() => {
        getData();
    }, [getData]);

    const handleModerationInfo = (data: TModerationList) => {
        setModerationInfo(data);
    };

    const callBackCancel = useCallback(() => {
        setModerationInfo(null);
        getData();
    }, [getData]);

    const periodHandler = (formName: string, newValue: Date) => {
        if (formName === 'from') {
            setPeriod({ ...period, from: newValue });
        } else {
            setPeriod({ ...period, to: newValue });
        }
    };

    const checkCommentHandler = (data: TModerationList) => {
        switch (data['proposal-status']) {
            case ServiceModerationStatusEnum.OnModeration:
                return show(MessageType.Info, 'Вашу заявку еще не проверили');
            case ServiceModerationStatusEnum.Accepted:
                return show(MessageType.Success, data.comment);
            case ServiceModerationStatusEnum.Rejected:
                return show(MessageType.Warning, data.comment);
            default:
        }
    };

    const statusHandler = (ev: SelectChangeEvent<ServiceModerationStatusEnum>) => {
        setStatus(ev.target.value as ServiceModerationStatusEnum);
    };

    const requestTypeHandler = (ev: SelectChangeEvent<EModerationRequestType>) => {
        setRequestType(ev.target.value as EModerationRequestType);
    };

    const onDataSelectHandler = (args: SelectDataCommonDataModel<void>) => {
        setIsLoading(true);
        InterlayerApiProxy.getServicesApiProxy().receiveModerationListAll(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, {
            page: args.pageNumber,
            filterProposalStatus: status === ServiceModerationStatusEnum.Undefined ? '' : status,
            filterProposalType: requestType === EModerationRequestType.undefined ? '' : requestType,
            fromDate: period.from ? dayjs(period.from).format('DD.MM.YYYY') : '',
            toDate: period.to ? dayjs(period.to).format('DD.MM.YYYY') : '',
            sortFieldName: args.sortingData?.fieldName,
            sortType: args.sortingData?.sortKind
        }).then(result => {
            validateModerationList(result?.Result ?? null);

            if (result) {
                setPaginationParams(result.pagination);
            }
        }).catch((err: unknown) => {
            show(MessageType.Error, err);
        }).finally(() => {
            setIsLoading(false);
        });
    };

    const onSearchHandler = () => {
        onDataSelectHandler({ pageNumber: 1 });
    };

    if (!moderationInfo) {
        return (
            <>
                { isLoading && <LoadingBounce /> }
                <div className={ styles.container }>
                    { !serviceId &&
                        <div className={ styles.header }>
                            <FormAndLabel label="Дата создания">
                                <DoubleDateInputForm
                                    isNeedToStretch={ true }
                                    periodFromValue={ period.from }
                                    periodToValue={ period.to }
                                    periodFromInputName="from"
                                    periodToInputName="to"
                                    onValueChange={ periodHandler }
                                />
                            </FormAndLabel>
                            <FormAndLabel label="Статус">
                                <Select value={ status } fullWidth={ true } onChange={ statusHandler }>
                                    <MenuItem value={ ServiceModerationStatusEnum.Undefined }>Все</MenuItem>
                                    <MenuItem value={ ServiceModerationStatusEnum.Rejected }>Отклонен</MenuItem>
                                    <MenuItem value={ ServiceModerationStatusEnum.OnModeration }>На модерации</MenuItem>
                                    <MenuItem value={ ServiceModerationStatusEnum.Accepted }>Принят</MenuItem>
                                </Select>
                            </FormAndLabel>
                            <FormAndLabel label="Тип заявки">
                                <Select value={ requestType } fullWidth={ true } onChange={ requestTypeHandler }>
                                    <MenuItem value={ EModerationRequestType.undefined }>Все</MenuItem>
                                    <MenuItem value={ EModerationRequestType.moderate }>Редактирование</MenuItem>
                                    <MenuItem value={ EModerationRequestType.create }>Создание</MenuItem>
                                </Select>
                            </FormAndLabel>
                            <SuccessButton onClick={ onSearchHandler }>Найти</SuccessButton>
                        </div>
                    }
                    <CommonTableWithFilter
                        isResponsiveTable={ true }
                        isBorderStyled={ true }
                        uniqueContextProviderStateId="ModerationListTableContextProviderTextId"
                        onDataSelect={ onDataSelectHandler }
                        tableProps={ {
                            pagination: paginationParams ? {
                                totalPages: paginationParams['total-pages'],
                                currentPage: paginationParams['page-number'],
                                pageSize: paginationParams['page-size'],
                                recordsCount: paginationParams['total-items']
                            } : undefined,
                            dataset: moderationList,
                            keyFieldName: 'proposal-id',
                            fieldsView: {
                                date: {
                                    caption: 'Дата создания',
                                    isSortable: !serviceId
                                },
                                'service-name': {
                                    caption: 'Название',
                                    format: (value: string, data) => {
                                        if (isNotAllowedEdit || (currentUserGroups.includes(AccountUserGroup.AccountSaleManager) && currentUserGroups.length === 1)) {
                                            return (
                                                <TextOut>{ value }</TextOut>
                                            );
                                        }

                                        return (
                                            <Link onClick={ () => handleModerationInfo(data) }>{ value }</Link>
                                        );
                                    },
                                    isSortable: !serviceId
                                },
                                type: {
                                    caption: 'Тип заявки',
                                    isSortable: !serviceId
                                },
                                'proposal-status': {
                                    caption: 'Статус',
                                    format: (value: string, _) => {
                                        switch (value) {
                                            case ServiceModerationStatusEnum.OnModeration:
                                                return <Chip className={ styles.statusChip } label={ value } color="info" />;
                                            case ServiceModerationStatusEnum.Accepted:
                                                return <Chip className={ styles.statusChip } label={ value } color="primary" />;
                                            case ServiceModerationStatusEnum.Rejected:
                                                return <Chip className={ styles.statusChip } label={ value } color="warning" />;
                                            default:
                                                return <div>{ value }</div>;
                                        }
                                    },
                                    isSortable: !serviceId
                                },
                                'service-available': !serviceId ? {
                                    caption: 'Сервис доступен',
                                    isSortable: !serviceId,
                                    format: (value: boolean) =>
                                        <Chip
                                            className={ styles.statusChip }
                                            label={ value ? 'Доступен' : 'Не доступен' }
                                            color={ value ? 'primary' : 'error' }
                                        />
                                } : {
                                    caption: '',
                                    fieldWidth: '1px'
                                },
                                'proposal-id': {
                                    caption: 'Действия',
                                    format: (_: string, data) =>
                                        <Tooltip title="Комментарий" onClick={ () => checkCommentHandler(data) }>
                                            <i className={ `fa fa-eye ${ styles.icon }` } aria-hidden="true" />
                                        </Tooltip>
                                }
                            }
                        } }
                    />
                </div>
            </>
        );
    }

    return (
        <ModerationView
            moderationInfo={ moderationInfo }
            serviceId={ serviceId ?? moderationInfo['service-id'] ?? '' }
            floatMessage={ floatMessage }
            callBackCancel={ callBackCancel }
        />
    );
};

export const ServiceModeration = memo(withFloatMessages(ServiceModerationComponent));