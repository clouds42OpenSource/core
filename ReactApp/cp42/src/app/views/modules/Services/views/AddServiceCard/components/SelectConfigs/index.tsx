import * as React from 'react';
import { Box, Chip, FormControl, MenuItem, OutlinedInput, Select, SelectChangeEvent } from '@mui/material';
import DeleteIcon from '@mui/icons-material/Delete';

import { TConfigurationsResponse, TFileInformationResponse } from 'app/web/api/ServicesProxy/response-dto';
import styles from 'app/views/modules/Services/views/AddServiceCard/components/SelectConfigs/style.module.css';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { RequestKind } from 'core/requestSender/enums';
import { SELECT_MENU_PROPS, SELECT_STYLE_CONSTANT } from 'app/views/modules/Services/constants/SelectStyleConstants';

type TSelectWithChipProps = {
    configFiles: TFileInformationResponse[];
    callBack: (selectedConfigs: TFileInformationResponse[]) => void;
    index: number;
    isEditable: boolean;
    allConfigs: TConfigurationsResponse[];
};

type TSelectWithChipState = {
    configs: TConfigurationsResponse[];
};

export class SelectWithChipClass extends React.Component<TSelectWithChipProps, TSelectWithChipState> {
    constructor(props: TSelectWithChipProps) {
        super(props);
        this.state = {
            configs: []
        };
        this.renderSelectValue = this.renderSelectValue.bind(this);
        this.selectConfig = this.selectConfig.bind(this);
        this.deleteSelectedValue = this.deleteSelectedValue.bind(this);
        this.changeConfigVersion = this.changeConfigVersion.bind(this);
    }

    public static getDerivedStateFromProps(props: TSelectWithChipProps, state: TSelectWithChipState): TSelectWithChipState {
        return {
            configs: state.configs.filter(obj => !props.allConfigs.some(secondObj => secondObj.code === obj.code))
        };
    }

    public componentDidMount() {
        if (this.props.isEditable) {
            InterlayerApiProxy.getServicesApiProxy().receiveConfigsFromMs(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY)
                .then(result => {
                    if (result) {
                        this.setState({ configs: result });

                        const lastConfigs = this.props.configFiles[0]['last-version-configurations'];

                        if (lastConfigs && lastConfigs.length > 0) {
                            const { configFiles } = this.props;

                            lastConfigs.forEach(lastConfig => {
                                const foundConfig = result.find(config => config.code === lastConfig);

                                if (foundConfig && !configFiles[0].configurations.find(configs => configs.code === foundConfig.code)) {
                                    configFiles[0].configurations.push(foundConfig);
                                }
                            });

                            this.props.callBack(configFiles);
                        }
                    }
                });
        }
    }

    private deleteSelectedValue(index: number) {
        const { configFiles } = this.props;
        const deletedConfig = configFiles[this.props.index].configurations[index];
        configFiles[this.props.index].configurations.splice(index, 1);
        this.props.callBack(configFiles);
        this.setState(prevState => ({ configs: [...prevState.configs, deletedConfig] }));
    }

    private selectConfig(event: SelectChangeEvent<TConfigurationsResponse[] | number[]>) {
        const { configFiles, index, callBack } = this.props;
        const i = event.target.value[configFiles[index].configurations.length];

        if (typeof i === 'number') {
            configFiles[index].configurations.push(this.state.configs[i]);
            callBack(configFiles);
        }
    }

    private changeConfigVersion(event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>, i: number) {
        const { configFiles, index, callBack } = this.props;
        configFiles[index].configurations[i].version = event.currentTarget.value;
        callBack(configFiles);
    }

    private renderSelectValue(selected: TConfigurationsResponse[]) {
        const { isEditable } = this.props;

        return (
            <Box className={ styles.selectValue_box }>
                { selected.map(({ name, version }, index) => (
                    <div key={ index } className={ styles.selectedValueContainer }>
                        <Chip
                            label={ name }
                            onMouseDown={ event => event.stopPropagation() }
                            className={ styles.selectedChip }
                            onClick={ () => this.deleteSelectedValue(index) }
                            disabled={ !isEditable }
                            variant="outlined"
                            color="primary"
                            deleteIcon={ <DeleteIcon /> }
                            onDelete={ () => this.deleteSelectedValue(index) }
                        />
                        <input
                            className={ styles.selected }
                            value={ version }
                            onMouseDown={ event => event.stopPropagation() }
                            onChange={ event => this.changeConfigVersion(event, index) }
                            placeholder="Минимальная версия конфигурации (не обязательно)"
                            disabled={ !isEditable }
                        />
                    </div>
                )) }
            </Box>
        );
    }

    public render() {
        const { configFiles, index } = this.props;
        const { configs } = this.state;

        return (
            <FormControl sx={ { width: '100%' } }>
                <Select
                    sx={ SELECT_STYLE_CONSTANT }
                    MenuProps={ SELECT_MENU_PROPS }
                    multiple={ true }
                    value={ configFiles[index].configurations }
                    input={ <OutlinedInput label="Chip" /> }
                    onChange={ this.selectConfig }
                    renderValue={ selected => this.renderSelectValue(selected) }
                >
                    { configs.map((config, i) => (
                        <MenuItem key={ i } value={ i }>
                            { config.name }
                        </MenuItem>
                    )) }
                    { configs.length === 0 &&
                        <MenuItem disabled={ true }>
                            Совместимых конфигураций не найдено
                        </MenuItem>
                    }
                </Select>
            </FormControl>
        );
    }
}