import { REDUX_API } from 'app/api/useReduxApi';
import { useAppDispatch, useAppSelector } from 'app/hooks';
import { AgencyAgreementDialog } from 'app/views/modules/_common/shared/AgencyAgreementDialog';
import React, { useEffect, useState } from 'react';

import { AccountServicesList } from 'app/views/modules/Services/views/MyServices/components/AccountServicesList';
import { withHeader } from 'app/views/components/_hoc/withHeader';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';

import styles from './style.module.css';

export const ExtensionsAndProcessing = withFloatMessages(({ floatMessage }) => {
    const dispatch = useAppDispatch();
    const { data } = useAppSelector(state => state.PartnersState.agencyAgreementStatusReducer);

    const [isOpen, setIsOpen] = useState(false);

    useEffect(() => {
        REDUX_API.PARTNERS_REDUX.getAgencyAgreementStatus(dispatch);
    }, [dispatch]);

    useEffect(() => {
        if (data) {
            setIsOpen(data.rawData);
        }
    }, [data]);

    return (
        <>
            <AgencyAgreementDialog needGetSummaryData={ true } isOpen={ isOpen } setIsOpen={ setIsOpen } />
            <div className={ styles.pageContainer }>
                <AccountServicesList floatMessage={ floatMessage } />
            </div>
        </>
    );
});

export const ExtensionsAndProcessingPageView = withHeader({
    title: 'Расширения и обработки',
    page: ExtensionsAndProcessing,
});