import { Divider } from '@mui/material';
import { useAppSelector } from 'app/hooks';
import React, { memo, useCallback, useState } from 'react';

import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { ContainedButton, SuccessButton } from 'app/views/components/controls/Button';
import { Dialog } from 'app/views/components/controls/Dialog';
import { ModerationBillingStatusEnum } from 'app/views/modules/Services/enums/ModerationBillingStatusEnum';
import { CreateService } from 'app/views/modules/Services/views/AddServiceCard';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { TModerationList } from 'app/web/api/ServicesProxy/response-dto';
import { RequestKind } from 'core/requestSender/enums';

import styles from './style.module.css';

type TModerationView = FloatMessageProps & {
    moderationInfo: TModerationList;
    serviceId: string;
    callBackCancel: () => void;
};

type TModal = {
    isOpened: boolean;
    isAccepted: boolean | null;
};

const api = InterlayerApiProxy.getServicesApiProxy();

export const ModerationView = memo(({ moderationInfo, serviceId, floatMessage, callBackCancel }: TModerationView) => {
    const { currentUserGroups } = useAppSelector(state => state.Global.getCurrentSessionSettingsReducer.settings.currentContextInfo);

    const [isOpen, setIsOpen] = useState<TModal>({
        isOpened: false,
        isAccepted: null
    });
    const [commentSection, setCommentSection] = useState('');

    const handleCommentSection = (ev: React.ChangeEvent<HTMLTextAreaElement>) => {
        setCommentSection(ev.target.value);
    };

    const setOpenModal = (value: TModal) => {
        setIsOpen(value);
    };

    const setModerationStatus = useCallback((accepted: boolean) => {
        if (commentSection.length < 4) {
            return floatMessage.show(MessageType.Error, 'Комментарий по заявке должен быть больше 4х символов');
        }

        api.setModerationStatus(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, moderationInfo['proposal-id'], {
            accepted,
            comment: commentSection
        }).then(result => {
            if (result && !result?.error) {
                api.setBillingModerationStatus(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, {
                    ServiceRequestId: moderationInfo['proposal-id'],
                    Status: accepted ? ModerationBillingStatusEnum.Implemented : ModerationBillingStatusEnum.Rejected,
                    ModeratorComment: commentSection
                }).then(res => {
                    if (res) {
                        floatMessage.show(MessageType.Success, 'Статус заявки успешно изменен');
                        callBackCancel();
                    } else {
                        floatMessage.show(MessageType.Error, 'Не удалось сменить статус заявки на модерацию');
                    }
                });
            } else {
                floatMessage.show(MessageType.Error, 'Не удалось сменить статус заявки на модерацию');
            }

            setIsOpen({ isAccepted: null, isOpened: false });
        });
    }, [callBackCancel, commentSection, floatMessage, moderationInfo]);

    return (
        <>
            <Dialog
                isOpen={ isOpen.isOpened }
                dialogWidth="xs"
                onCancelClick={ () => setOpenModal({ isOpened: false, isAccepted: null }) }
                className={ styles.dialog }
                title={ isOpen.isAccepted ? 'Принятие заявки' : 'Отклонение заявки' }
                titleFontSize={ 20 }
                titleTextAlign="left"
                isTitleSmall={ true }
            >
                <label>Сообщение для автора сервиса</label>
                <textarea value={ commentSection } onChange={ handleCommentSection } className={ styles.commentSection } />
                <div className={ styles.buttonContainer }>
                    <SuccessButton onClick={ () => setModerationStatus(isOpen.isAccepted ?? false) }>Ок</SuccessButton>
                </div>
            </Dialog>
            <div>
                <div className={ moderationInfo.type === 'Редактирование' ? styles.serviceInfoContainer : undefined }>
                    { moderationInfo.type === 'Редактирование' &&
                        <div>
                            <label className={ styles.title }>Текущие данные</label>
                            <Divider className={ styles.divider } />
                            <CreateService
                                isServiceHasBilling={ true }
                                isEditMode={ false }
                                isModifyPage={ true }
                                isModeratePage={ true }
                                serviceId={ serviceId }
                                floatMessage={ floatMessage }
                            />
                        </div>
                    }
                    <div>
                        <label className={ styles.title }>Новые данные</label>
                        <Divider className={ styles.divider } />
                        <CreateService
                            isServiceHasBilling={ true }
                            viewModeInfo={ moderationInfo }
                            isEditMode={ false }
                            serviceId={ serviceId }
                            isModifyPage={ true }
                            isModeratePage={ true }
                            floatMessage={ floatMessage }
                            currentUserGroups={ currentUserGroups }
                        />
                    </div>
                </div>
                <div className={ styles.buttonContainer }>
                    <ContainedButton onClick={ callBackCancel }>Закрыть</ContainedButton>
                    { moderationInfo['proposal-status'] === 'На модерации' &&
                        <>
                            <ContainedButton kind="error" onClick={ () => setOpenModal({ isOpened: true, isAccepted: false }) }>Отклонить</ContainedButton>
                            <SuccessButton onClick={ () => setOpenModal({ isOpened: true, isAccepted: true }) }>Принять</SuccessButton>
                        </>
                    }
                </div>
            </div>
        </>
    );
});