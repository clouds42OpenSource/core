import { ServiceInfoToFileInfo } from 'app/views/modules/Services/mappings';
import { Accordion, AccordionDetails, AccordionSummary } from '@mui/material';
import styles from 'app/views/modules/Services/views/EditServiceCard/ServiceVersions/style.module.css';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { VersionStatus } from 'app/views/modules/Services/views/EditServiceCard/ServiceVersions/components/VersionStatus';
import { ConfigFileViewer } from 'app/views/modules/Services/views/AddServiceCard/components/ConfigFileViewer';
import { SuccessButton } from 'app/views/components/controls/Button';
import React, { memo, useCallback, useMemo, useState } from 'react';
import { TConfigurationsResponse, TServiceInfoResponse } from 'app/web/api/ServicesProxy/response-dto';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { RequestKind } from 'core/requestSender/enums';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { TErrorMSType } from 'app/views/modules/Services/types/ErrorTypes';

type TProps = FloatMessageProps & {
    file: TServiceInfoResponse;
    id: string;
    setLoading: (isLoading: boolean) => void;
    getVersionFiles: (id: string) => Promise<void>;
    isAllowedEdit: boolean;
    files: TServiceInfoResponse[];
    filesBackup: TServiceInfoResponse[];
    configs: TConfigurationsResponse[];
};

export const ConfigAccordion = withFloatMessages(memo((
    {
        file,
        id,
        setLoading,
        floatMessage: { show },
        getVersionFiles,
        isAllowedEdit,
        files,
        filesBackup,
        configs
    }: TProps) => {
    const [expanded, setExpanded] = useState(false);

    const versionId = useMemo(() => file['version-id'], [file]);
    const fileInfo = useMemo(() => ServiceInfoToFileInfo(file, configs), [configs, file]);

    const isPublished = useCallback((f: TServiceInfoResponse) => f['version-status'] === 'Опубликована', []);

    const changeExpandedAccordion = () => {
        setExpanded(prev => !prev);
    };

    const reworkVersion = () => {
        setLoading(true);
        InterlayerApiProxy.getServicesApiProxy().reworkServiceVersion(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, versionId, id)
            .then(async result => {
                if (result && !('error' in result)) {
                    await getVersionFiles(id);
                } else {
                    show(MessageType.Error, (result as TErrorMSType)?.message ?? 'Не удалось отправить на доработку файл конфигураций');
                }
                setLoading(false);
            });
    };

    const deleteVersion = () => {
        setLoading(true);
        InterlayerApiProxy.getServicesApiProxy().deleteServiceVersion(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, versionId, id)
            .then(async result => {
                if (result) {
                    await getVersionFiles(id);
                } else {
                    show(MessageType.Error, 'Не удалось отозвать версию');
                }
                setLoading(false);
            });
    };

    return (
        <Accordion className={ styles.accordion } key={ file['publication-date'] } expanded={ expanded } onChange={ changeExpandedAccordion }>
            <AccordionSummary className={ styles.accordionSummaryContainer } expandIcon={ <span><ExpandMoreIcon /></span> }>
                <div className={ styles.accordionSummaryContent }>
                    <div>{ file.name }</div>
                    <div>{ file.synonym }</div>
                    <div>{ file.version }</div>
                    <VersionStatus versionStatus={ file['version-status'] } />
                </div>
            </AccordionSummary>
            { expanded && (
                <AccordionDetails sx={ { display: 'flex', flexDirection: 'column', gap: '10px' } }>
                    <ConfigFileViewer
                        allConfigs={ [] }
                        configFiles={ [fileInfo] }
                        callBack={ () => { /* empty */ } }
                        isEditable={ false }
                        comment={ file['audit-result-comment'] }
                        linkToAuditGoogleSheet={ file['audit-google-sheet'] }
                        versionId={ file['version-id'] }
                    />
                    {
                        isAllowedEdit &&
                        isPublished(file) &&
                        files.filter(f => isPublished(f)).length > 1 &&
                        filesBackup.find(f => isPublished(f))?.['version-id'] === versionId && (
                            <SuccessButton className={ styles.successButton } onClick={ deleteVersion }>Отозвать версию</SuccessButton>
                        )
                    }
                    { file['rework-available'] && isAllowedEdit &&
                        <SuccessButton className={ styles.successButton } onClick={ reworkVersion }>Отправить на доработку</SuccessButton>
                    }
                </AccordionDetails>
            ) }
        </Accordion>
    );
}));