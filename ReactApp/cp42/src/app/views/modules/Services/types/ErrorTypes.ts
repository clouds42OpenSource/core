export type TErrorType = {
    Code: number;
    DebugInfo: string;
    Description: string;
};

export type TErrorMSType = {
    detail: string;
    error: string;
    message: string;
};