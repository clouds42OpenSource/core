export enum EditServicePageTabEnum {
    Info,
    Versions,
    Marketing,
    Moderation,
    Billing
}