import '@testing-library/jest-dom/extend-expect';
import { dateSort } from './dateSort';

describe('DateSort', () => {
    it('counts date', () => {
        const differenceInMS = dateSort('11.11.2011 10:22', '11.11.2011 10:23', '.');
        expect(differenceInMS).toEqual(60000);
    });

    it('counts date with error', () => {
        const differenceInMS = dateSort('abc.11.2011', '12.11.2011', '.');
        expect(differenceInMS).toEqual(NaN);
    });
});