export const dateSort = (firstDate: string, secondDate: string, separator: string) => {
    const [fDay, fMonth, fYear, fTime] = firstDate.replaceAll(' ', separator).split(separator);
    const [sDay, sMonth, sYear, sTime] = secondDate.replaceAll(' ', separator).split(separator);

    return Date.parse(`${ sYear }-${ sMonth }-${ sDay } ${ sTime }`) - Date.parse(`${ fYear }-${ fMonth }-${ fDay } ${ fTime }`);
};