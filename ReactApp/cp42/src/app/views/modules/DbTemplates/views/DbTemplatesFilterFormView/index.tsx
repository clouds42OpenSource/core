import {
    DbTemplatesFilterDataForm,
    DbTemplatesFilterDataFormFieldNamesType
} from 'app/views/modules/DbTemplates/types';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { FilterItemWrapper } from 'app/views/components/_hoc/tableFilterWrapper/filterItemWrapper';
import React from 'react';
import { TableFilterWrapper } from 'app/views/components/_hoc/tableFilterWrapper';
import { TextBoxForm } from 'app/views/components/controls/forms/TextBoxForm';
import { hasComponentChangesFor } from 'app/common/functions';

/**
 * Свойства для компонента фильтра
 */
type OwnProps = ReduxFormProps<DbTemplatesFilterDataForm> & {
    onFilterChanged: (filter: DbTemplatesFilterDataForm, filterFormName: string) => void;
    isFilterFormDisabled: boolean;
};

class DbTemplatesFilterFormViewClass extends React.Component<OwnProps> {
    public constructor(props: OwnProps) {
        super(props);
        this.initReduxForm = this.initReduxForm.bind(this);
        this.onValueChange = this.onValueChange.bind(this);
        this.onValueApplied = this.onValueApplied.bind(this);
    }

    public componentDidMount() {
        this.props.reduxForm.setInitializeFormDataAction(this.initReduxForm);
    }

    public shouldComponentUpdate(nextProps: OwnProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private onValueChange<TValue, TForm extends string = DbTemplatesFilterDataFormFieldNamesType>(formName: TForm, newValue: TValue) {
        this.props.reduxForm.updateReduxFormFields({
            [formName]: newValue
        });
        if ((formName === 'dbTemplateName' || formName === 'dbTemplateDescription') && newValue && (newValue as any as string).length < 3) {
            return false;
        }
    }

    private onValueApplied<TValue>(formName: string, value: TValue) {
        this.props.onFilterChanged({
            ...this.props.reduxForm.getReduxFormFields(false),
            [formName]: value
        }, formName);
    }

    private initReduxForm(): DbTemplatesFilterDataForm | undefined {
        return {
            dbTemplateName: '',
            dbTemplateDescription: ''
        };
    }

    public render() {
        const formFields = this.props.reduxForm.getReduxFormFields(false);

        return (
            <TableFilterWrapper>
                <FilterItemWrapper width="350px">
                    <TextBoxForm
                        autoFocus={ true }
                        label="Имя шаблона"
                        formName="dbTemplateName"
                        onValueChange={ this.onValueChange }
                        onValueApplied={ this.onValueApplied }
                        value={ formFields.dbTemplateName }
                        isReadOnly={ this.props.isFilterFormDisabled }
                    />
                </FilterItemWrapper>
                <FilterItemWrapper width="350px">
                    <TextBoxForm
                        label="Описание"
                        formName="dbTemplateDescription"
                        onValueChange={ this.onValueChange }
                        onValueApplied={ this.onValueApplied }
                        value={ formFields.dbTemplateDescription }
                        isReadOnly={ this.props.isFilterFormDisabled }
                    />
                </FilterItemWrapper>
            </TableFilterWrapper>
        );
    }
}

export const DbTemplatesFilterFormView = withReduxForm(DbTemplatesFilterFormViewClass, {
    reduxFormName: 'DbTemplates_Filter_Form',
    resetOnUnmount: true
});