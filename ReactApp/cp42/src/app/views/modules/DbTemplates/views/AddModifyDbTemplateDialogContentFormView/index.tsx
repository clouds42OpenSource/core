import { Box } from '@mui/material';
import { PlatformTypeDescriptionExtensions } from 'app/common/enums';
import { getComboBoxFormOptionsFromKeyValueDataModel, hasComponentChangesFor } from 'app/common/functions';
import { getEmptyUuid } from 'app/common/helpers';
import { createInt32Validator } from 'app/common/validators';
import { ReceiveDbTemplateDataToEditThunkParams } from 'app/modules/DbTemplates/reducers/receiveDbTemplateDataToEditReducer/params';
import { ReceiveDbTemplateDataToEditThunk } from 'app/modules/DbTemplates/thunks/ReceiveDbTemplateDataToEditThunk';
import { GetDbTemplatesThunk } from 'app/modules/dbTemplateUpdate/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { TextOut } from 'app/views/components/TextOut';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { CheckBoxForm, ComboBoxForm, TextBoxForm } from 'app/views/components/controls/forms';
import { DbTemplateItemDataModel } from 'app/web/InterlayerApiProxy/DbTemplateApiProxy/receiveDbTemplates/data-models/DbTemplateItemDataModel';
import { KeyValueDataModel } from 'app/web/common/data-models';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';
import React from 'react';
import { connect } from 'react-redux';
import { ReceiveLocalesThunk } from 'app/modules/locales/store/thunks';
import { ReceiveLocalesThunkParams } from 'app/modules/locales/store/reducers/receiveLocalesData/params';
import { LocaleItemDataModel } from 'app/web/InterlayerApiProxy/LocalesApiProxy/receiveLocales';

type OwnProps = ReduxFormProps<DbTemplateItemDataModel> & {
    templateId: string;
    configuration1CName: string[];
};

type StateProps = {
    itemToEdit?: DbTemplateItemDataModel;
    locales: LocaleItemDataModel[];
    templates: KeyValueDataModel<string, string>[];
};

type DispatchProps = {
    dispatchReceiveDbTemplateDataToEditThunk: (args?: ReceiveDbTemplateDataToEditThunkParams) => void;
    dispatchGetDbTemplatesThunk: (args: IForceThunkParam) => void;
    dispatchReceiveLocalesThunk: (args: ReceiveLocalesThunkParams) => void;
};

type AllProps = OwnProps & StateProps & DispatchProps;

class AddModifyDbTemplateDialogContentFormViewClass extends React.Component<AllProps> {
    public constructor(props: AllProps) {
        super(props);
        this.onValueChange = this.onValueChange.bind(this);
        this.initReduxForm = this.initReduxForm.bind(this);

        this.props.reduxForm.setInitializeFormDataAction(this.initReduxForm);
    }

    public componentDidMount() {
        if (this.props.templateId) {
            this.props.dispatchReceiveDbTemplateDataToEditThunk({
                dbTemplateId: this.props.templateId,
                force: true
            });
        }
        this.props.dispatchReceiveLocalesThunk({ pageNumber: 1, force: true });
        this.props.dispatchGetDbTemplatesThunk({ force: true });
    }

    public shouldComponentUpdate(nextProps: AllProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private onValueChange<TValue>(formName: string, newValue: TValue) {
        this.props.reduxForm.updateReduxFormFields({
            [formName]: newValue
        });
    }

    private initReduxForm(): DbTemplateItemDataModel | undefined {
        if (!this.props.itemToEdit || !this.props.templateId) {
            return {
                templateId: getEmptyUuid(),
                templateName: '',
                templateDescription: '',
                order: 0,
                platform: 0,
                platformType: 0,
                localeId: '',
                demoTemplateId: '',
                canWebPublish: false,
                configuration1CName: '',
                adminLogin: '',
                adminPassword: '',
                needUpdate: false
            };
        }

        return {
            templateId: this.props.itemToEdit.templateId,
            templateName: this.props.itemToEdit.templateName,
            localeId: this.props.itemToEdit?.localeId ?? '',
            demoTemplateId: this.props.itemToEdit?.demoTemplateId ?? '',
            order: this.props.itemToEdit.order,
            templateDescription: this.props.itemToEdit.templateDescription,
            canWebPublish: this.props.itemToEdit.canWebPublish,
            configuration1CName: this.props.itemToEdit.configuration1CName,
            platform: this.props.itemToEdit.platform,
            platformType: this.props.itemToEdit.platformType,
            needUpdate: this.props.itemToEdit.needUpdate,
            adminLogin: this.props.itemToEdit.adminLogin,
            adminPassword: this.props.itemToEdit.adminPassword
        };
    }

    public render() {
        const reduxForm = this.props.reduxForm.getReduxFormFields(false);
        const configuration1CName = this.props.configuration1CName.map(item => {
            return {
                key: item,
                value: item
            };
        });

        return (
            <Box display="flex" flexDirection="column" gap="16px">
                <TextBoxForm
                    formName="templateName"
                    label="Имя шаблона"
                    value={ reduxForm.templateName }
                    autoFocus={ true }
                    onValueChange={ this.onValueChange }
                />
                { !reduxForm.templateName && <TextOut style={ { color: 'red' } }>Имя шаблона обязательно</TextOut> }
                <TextBoxForm
                    formName="templateDescription"
                    label="Описание"
                    value={ reduxForm.templateDescription }
                    onValueChange={ this.onValueChange }
                />
                { !reduxForm.templateDescription && <TextOut style={ { color: 'red' } }>Описание обязательно</TextOut> }
                <TextBoxForm
                    formName="order"
                    label="Порядковый номер"
                    value={ reduxForm.order?.toString() ?? '' }
                    validator={ createInt32Validator({
                        negativeNumbersCanBeEntered: false
                    }) }
                    onValueChange={ this.onValueChange }
                />
                <ComboBoxForm
                    formName="platformType"
                    label="Платформа"
                    value={ reduxForm.platformType }
                    items={ getComboBoxFormOptionsFromKeyValueDataModel(PlatformTypeDescriptionExtensions.getAllDescriptions()) }
                    onValueChange={ this.onValueChange }
                />
                <ComboBoxForm
                    formName="localeId"
                    label="Локаль"
                    value={ reduxForm.localeId }
                    items={ this.props.locales.map(item => {
                        return {
                            value: item.localeId,
                            text: item.name
                        };
                    }) }
                    onValueChange={ this.onValueChange }
                />
                <ComboBoxForm
                    formName="demoTemplateId"
                    label="Шаблон с демо данными"
                    value={ reduxForm.demoTemplateId }
                    items={ this.props.templates.map(item => {
                        return {
                            value: item.key,
                            text: item.value
                        };
                    }) }
                    onValueChange={ this.onValueChange }
                />
                <ComboBoxForm
                    formName="configuration1CName"
                    label="Имя конфигурации 1С"
                    value={ reduxForm.configuration1CName }
                    items={ configuration1CName.map(item => {
                        return {
                            value: item.key,
                            text: item.value
                        };
                    }) }
                    onValueChange={ this.onValueChange }
                    allowAutoComplete={ true }
                />
                { !reduxForm.configuration1CName && <TextOut style={ { color: 'red' } }>Требуется платформа</TextOut> }
                <CheckBoxForm
                    formName="canWebPublish"
                    label="Можно публиковать на web"
                    isChecked={ reduxForm.canWebPublish }
                    trueText="(да)"
                    falseText="(нет)"
                    onValueChange={ this.onValueChange }
                />
                <TextBoxForm
                    formName="adminLogin"
                    label="Логин администратора"
                    value={ reduxForm.adminLogin }
                    onValueChange={ this.onValueChange }
                />
                <TextBoxForm
                    formName="adminPassword"
                    label="Пароль администратора"
                    value={ reduxForm.adminPassword }
                    onValueChange={ this.onValueChange }
                />
                <CheckBoxForm
                    formName="needUpdate"
                    label="Необходимо автообновление"
                    isChecked={ reduxForm.needUpdate }
                    trueText="(да)"
                    falseText="(нет)"
                    onValueChange={ this.onValueChange }
                />
            </Box>
        );
    }
}

const AddModifyDbTemplateDialogContentFormViewConnected = connect<StateProps, DispatchProps, {}, AppReduxStoreState>(
    state => {
        const dbTemplatesState = state.DbTemplatesState;
        const localesState = state.LocalesState;
        const dbTemplateUpdateState = state.DbTemplateUpdateState;
        const { currentTemplateDataEdit } = dbTemplatesState;

        const itemToEdit = currentTemplateDataEdit?.dbTemplateToEdit;

        const locales = localesState.locales.items;
        const templates = dbTemplateUpdateState.dbTemplates;
        const emptyOptionLocale: LocaleItemDataModel = { name: '', country: '', localeId: '', currency: '', currencyCode: null };
        const emptyOptionTemplate = { key: '', value: '' };

        if (locales.length > 0 && locales.every(item => item.localeId !== emptyOptionLocale.localeId) && templates.length > 0 && templates.every(item => item.key !== emptyOptionTemplate.key)) {
            locales.unshift(emptyOptionLocale);
            templates.unshift(emptyOptionTemplate);
        }

        return {
            itemToEdit,
            locales,
            templates
        };
    },
    {
        dispatchReceiveDbTemplateDataToEditThunk: ReceiveDbTemplateDataToEditThunk.invoke,
        dispatchGetDbTemplatesThunk: GetDbTemplatesThunk.invoke,
        dispatchReceiveLocalesThunk: ReceiveLocalesThunk.invoke
    }
)(AddModifyDbTemplateDialogContentFormViewClass);

export const AddModifyDbTemplateDialogContentFormView = withReduxForm(AddModifyDbTemplateDialogContentFormViewConnected, {
    reduxFormName: ' AddModifyDbTemplate_ReduxForm',
    resetOnUnmount: true
});