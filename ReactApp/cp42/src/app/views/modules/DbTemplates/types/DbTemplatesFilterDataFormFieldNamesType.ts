import { DbTemplatesFilterDataForm } from 'app/views/modules/DbTemplates/types/DbTemplatesFilterDataForm';

export type DbTemplatesFilterDataFormFieldNamesType = keyof DbTemplatesFilterDataForm;