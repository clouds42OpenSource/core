/**
 * Модель Redux формы для редактирования фильтра для таблицы шаблонов
 */
export type DbTemplatesFilterDataForm = {
    /**
     * Название шаблона
     */
    dbTemplateName: string;

    /**
     * Описание шаблона
     */
    dbTemplateDescription: string;
};