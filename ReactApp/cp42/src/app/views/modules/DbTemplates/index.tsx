import { SortingKind } from 'app/common/enums';
import { hasComponentChangesFor } from 'app/common/functions';
import { ProcessIdStateHelper } from 'app/common/helpers/ProcessIdStateHelper';
import { DbTemplatesProcessId } from 'app/modules/DbTemplates/IndustryProcessId';
import { AddDbTemplateThunkParams } from 'app/modules/DbTemplates/reducers/addDbTemplateReducer/params';
import { DeleteDbTemplateThunkParams } from 'app/modules/DbTemplates/reducers/deleteDbTemplateReducer/params';
import { GetDbTemplateConfiguration1CNameThunkParams } from 'app/modules/DbTemplates/reducers/getDbTemplateConfiguration1CNameReducer/params';
import { ReceiveDbTemplatesThunkParams } from 'app/modules/DbTemplates/reducers/receiveDbTemplatesReducer/params';
import { UpdateDbTemplateThunkParams } from 'app/modules/DbTemplates/reducers/updateDbTemplateReducer/params';
import { AddDbTemplateThunk, DeleteDbTemplateThunk, GetDbTemplateConfiguration1CNameThunk, ReceiveDbTemplatesThunk, UpdateDbTemplateThunk } from 'app/modules/DbTemplates/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { TextOut } from 'app/views/components/TextOut';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { withHeader } from 'app/views/components/_hoc/withHeader';
import { DbTemplatesFilterDataForm } from 'app/views/modules/DbTemplates/types';
import { AddModifyDbTemplateDialogContentFormView } from 'app/views/modules/DbTemplates/views/AddModifyDbTemplateDialogContentFormView';
import { DbTemplatesFilterFormView } from 'app/views/modules/DbTemplates/views/DbTemplatesFilterFormView';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { TablePagination } from 'app/views/modules/_common/components/CommonTableWithFilter/TableView';
import { DbTemplateSelectItemDataModel } from 'app/web/InterlayerApiProxy/DbTemplateApiProxy/receiveDbTemplates';
import { DbTemplateItemDataModel } from 'app/web/InterlayerApiProxy/DbTemplateApiProxy/receiveDbTemplates/data-models/DbTemplateItemDataModel';
import { SelectDataCommonDataModel, SortingDataModel } from 'app/web/common/data-models';
import React from 'react';
import { connect } from 'react-redux';

type StateProps<TDataRow = DbTemplateSelectItemDataModel> = {
    dataset: TDataRow[];
    isInLoadingDataProcess: boolean;
    pagination: TablePagination;
    dbTemplateConfiguration1CName: string[];
};

type DispatchProps = {
    dispatchReceiveDbTemplatesThunk: (args: ReceiveDbTemplatesThunkParams) => void;
    dispatchUpdateDbTemplateThunk: (args: UpdateDbTemplateThunkParams) => void;
    dispatchAddDbTemplateThunk: (args: AddDbTemplateThunkParams) => void;
    dispatchDeleteDbTemplateThunk: (args: DeleteDbTemplateThunkParams) => void;
    dispatchDbTemplateConfiguration1CNameThunk: (args: GetDbTemplateConfiguration1CNameThunkParams) => void;
};

type AllProps = StateProps & DispatchProps & FloatMessageProps;

class DbTemplatesClass extends React.Component<AllProps> {
    public constructor(props: AllProps) {
        super(props);
        this.onDataSelect = this.onDataSelect.bind(this);
        this.getSortQuery = this.getSortQuery.bind(this);
    }

    public componentDidMount() {
        this.props.dispatchDbTemplateConfiguration1CNameThunk({ force: true });
    }

    public shouldComponentUpdate(nextProps: AllProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private onDataSelect(args: SelectDataCommonDataModel<DbTemplatesFilterDataForm>) {
        this.props.dispatchReceiveDbTemplatesThunk({
            pageNumber: args.pageNumber,
            filter: args.filter,
            orderBy: args.sortingData ? this.getSortQuery(args.sortingData) : 'name.desc',
            force: true
        });
    }

    private getSortQuery(args: SortingDataModel) {
        switch (args.fieldName) {
            case 'dbTemplateName':
                return `name.${ args.sortKind ? 'desc' : 'asc' }`;
            case 'dbTemplateDescription':
                return `defaultCaption.${ args.sortKind ? 'desc' : 'asc' }`;
            default:
                return 'name.desc';
        }
    }

    public render() {
        return (
            <>
                <div>
                    <TextOut fontSize={ 13 } fontWeight={ 400 }>
                        На этой странице вы можете управлять справочником шаблонов.
                    </TextOut>
                </div>
                <div style={ {
                    marginTop: '10px'
                } }
                >
                    <TextOut fontSize={ 12 } fontWeight={ 600 }>
                        Управление шаблонами
                    </TextOut>
                </div>
                <br />
                <CommonTableWithFilter
                    uniqueContextProviderStateId="DbTemplatesContextProviderStateId"
                    tableEditProps={ {
                        processProps: {
                            watch: {
                                reducerStateName: 'DbTemplatesState',
                                processIds: {
                                    addNewItemProcessId: DbTemplatesProcessId.AddDbTemplate,
                                    modifyItemProcessId: DbTemplatesProcessId.UpdateDbTemplate,
                                    deleteItemProcessId: DbTemplatesProcessId.DeleteDbTemplate
                                }
                            },
                            processMessages: {
                                getOnSuccessAddNewItemMessage: () => 'Добавление нового шаблона успешно завершено.',
                                getOnSuccessModifyItemMessage: () => 'Редактирование шаблона успешно завершено.',
                                getOnSuccessDeleteItemMessage: item => `Удаление шаблона "${ item.dbTemplateName }" успешно завершено.`
                            }
                        },
                        dialogProps: {
                            deleteDialog: {
                                getDialogTitle: item => `Удаление шаблона "${ item.dbTemplateName }"`,
                                getDialogContentView: itemToDelete => `Вы действительно хотите удалить шаблон "${ itemToDelete.dbTemplateName }"?`,
                                confirmDeleteItemButtonText: 'Удалить',
                                onDeleteItemConfirmed: itemToDelete => {
                                    this.props.dispatchDeleteDbTemplateThunk({
                                        dbTemplateId: itemToDelete.dbTemplateId,
                                        force: true
                                    });
                                }
                            },
                            addDialog: {
                                dialogWidth: 'sm',
                                getDialogTitle: () => 'Добавление нового шаблона',
                                getDialogContentFormView: ref =>
                                    <AddModifyDbTemplateDialogContentFormView
                                        configuration1CName={ this.props.dbTemplateConfiguration1CName }
                                        ref={ ref }
                                        templateId=""
                                    />,
                                confirmAddItemButtonText: 'Добавить',
                                showAddDialogButtonText: 'Добавить шаблон',
                                onAddNewItemConfirmed: itemToAdd => {
                                    const item = (itemToAdd as unknown as DbTemplateItemDataModel);
                                    if (item.templateName && item.templateDescription && item.configuration1CName) {
                                        this.props.dispatchAddDbTemplateThunk({
                                            ...itemToAdd as unknown as DbTemplateItemDataModel,
                                            force: true
                                        });
                                    } else {
                                        this.props.floatMessage.show(MessageType.Warning, 'Заполните обязательные поля');
                                    }
                                }
                            },
                            modifyDialog: {
                                getDialogTitle: () => 'Редактирование шаблона',
                                dialogWidth: 'sm',
                                confirmModifyItemButtonText: 'Сохранить',
                                getDialogContentFormView: (ref, item) => <AddModifyDbTemplateDialogContentFormView
                                    configuration1CName={ this.props.dbTemplateConfiguration1CName }
                                    ref={ ref }
                                    templateId={ item.dbTemplateId }
                                />,
                                onModifyItemConfirmed: itemToModify => {
                                    const item = (itemToModify as unknown as DbTemplateItemDataModel);
                                    if (item.templateName && item.templateDescription && item.configuration1CName) {
                                        this.props.dispatchUpdateDbTemplateThunk({
                                            ...itemToModify as unknown as DbTemplateItemDataModel,
                                            force: true
                                        });
                                    } else {
                                        this.props.floatMessage.show(MessageType.Warning, 'Заполните обязательные поля');
                                    }
                                }
                            }
                        }
                    } }
                    filterProps={ {
                        getFilterContentView: (ref, onFilterChanged) => <DbTemplatesFilterFormView ref={ ref } isFilterFormDisabled={ this.props.isInLoadingDataProcess } onFilterChanged={ onFilterChanged } />
                    } }
                    tableProps={ {
                        dataset: this.props.dataset,
                        keyFieldName: 'dbTemplateId',
                        sorting: {
                            sortFieldName: 'dbTemplateName',
                            sortKind: SortingKind.Asc
                        },
                        fieldsView: {
                            dbTemplateName: {
                                caption: 'Имя шаблона',
                                fieldContentNoWrap: false,
                                isSortable: true,
                                fieldWidth: 'auto'
                            },
                            dbTemplateDescription: {
                                caption: 'Описание',
                                fieldContentNoWrap: false,
                                isSortable: true,
                                fieldWidth: 'auto'
                            },
                            platform: {
                                caption: 'Платформа',
                                fieldContentNoWrap: true,
                                isSortable: false
                            }
                        },
                        pagination: this.props.pagination
                    } }
                    onDataSelect={ this.onDataSelect }
                />
            </>
        );
    }
}

const DbTemplatesConnected = connect<StateProps, DispatchProps, {}, AppReduxStoreState>(
    state => {
        const dbTemplatesState = state.DbTemplatesState;
        const { dbTemplates } = dbTemplatesState;
        const dataset = dbTemplates.records;
        const { dbTemplateConfiguration1CName } = dbTemplatesState;

        const currentPage = dbTemplates.metadata.pageNumber;
        const totalPages = dbTemplates.metadata.pageCount;
        const { pageSize } = dbTemplates.metadata;
        const recordsCount = dbTemplates.metadata.totalItemCount;

        const isInLoadingDataProcess = ProcessIdStateHelper.isInProgress(dbTemplatesState.reducerActions, DbTemplatesProcessId.ReceiveDbTemplates);

        return {
            dataset,
            isInLoadingDataProcess,
            dbTemplateConfiguration1CName,
            pagination: {
                currentPage,
                totalPages,
                pageSize,
                recordsCount
            }
        };
    },
    {
        dispatchReceiveDbTemplatesThunk: ReceiveDbTemplatesThunk.invoke,
        dispatchUpdateDbTemplateThunk: UpdateDbTemplateThunk.invoke,
        dispatchAddDbTemplateThunk: AddDbTemplateThunk.invoke,
        dispatchDeleteDbTemplateThunk: DeleteDbTemplateThunk.invoke,
        dispatchDbTemplateConfiguration1CNameThunk: GetDbTemplateConfiguration1CNameThunk.invoke
    }
)(DbTemplatesClass);

const DbTemplateFloatMessage = withFloatMessages(DbTemplatesConnected);

export const DbTemplatesView = withHeader({
    title: 'Справочник шаблонов файловых баз',
    page: DbTemplateFloatMessage
});