import { SortingKind } from 'app/common/enums';
import { hasComponentChangesFor } from 'app/common/functions';
import { ProcessIdStateHelper } from 'app/common/helpers/ProcessIdStateHelper';
import { IndustryProcessId } from 'app/modules/industry/IndustryProcessId';
import { AddIndustryThunkParams } from 'app/modules/industry/store/reducers/addIndustryReducer/params';
import { DeleteIndustryThunkParams } from 'app/modules/industry/store/reducers/deleteIndustryReducer/params';
import { ReceiveIndustryThunkParams } from 'app/modules/industry/store/reducers/receiveIndustryReducer/params';
import { UpdateIndustryThunkParams } from 'app/modules/industry/store/reducers/updateIndustryReducer/params';
import {
    AddIndustryThunk,
    DeleteIndustryThunk,
    ReceiveIndustryThunk,
    UpdateIndustryThunk
} from 'app/modules/industry/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { TextOut } from 'app/views/components/TextOut';
import { withHeader } from 'app/views/components/_hoc/withHeader';
import { IndustryFilterDataForm } from 'app/views/modules/Industry/types';
import {
    AddModifyIndustryDialogContentFormView
} from 'app/views/modules/Industry/views/AddModifyIndustryDialogContentFormView';
import { IndustryFilterFormView } from 'app/views/modules/Industry/views/IndustryFilterFormView';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { TablePagination } from 'app/views/modules/_common/components/CommonTableWithFilter/TableView';
import { SelectDataCommonDataModel, SortingDataModel } from 'app/web/common/data-models';
import { IndustryItemDataModel } from 'app/web/InterlayerApiProxy/IndustryApiProxy/receiveIndustry';
import React from 'react';
import { connect } from 'react-redux';

type StateProps<TDataRow = IndustryItemDataModel> = {
    dataset: TDataRow[];
    isInLoadingDataProcess: boolean;
    pagination: TablePagination;
};

type DispatchProps = {
    dispatchReceiveIndustryThunk: (args: ReceiveIndustryThunkParams) => void;
    dispatchUpdateIndustryThunk: (args: UpdateIndustryThunkParams) => void;
    dispatchAddIndustryThunk: (args: AddIndustryThunkParams) => void;
    dispatchDeleteIndustryThunk: (args: DeleteIndustryThunkParams) => void;
};

type AllProps = StateProps & DispatchProps;

class IndustryClass extends React.Component<AllProps> {
    public constructor(props: AllProps) {
        super(props);
        this.onDataSelect = this.onDataSelect.bind(this);
        this.getSortQuery = this.getSortQuery.bind(this);
    }

    public shouldComponentUpdate(nextProps: AllProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private onDataSelect(args: SelectDataCommonDataModel<IndustryFilterDataForm>) {
        this.props.dispatchReceiveIndustryThunk({
            pageNumber: args.pageNumber,
            filter: args.filter,
            orderBy: args.sortingData ? this.getSortQuery(args.sortingData) : 'name.desc',
            force: true
        });
    }

    private getSortQuery(args: SortingDataModel) {
        switch (args.fieldName) {
            case 'industryName':
                return `name.${ args.sortKind ? 'desc' : 'asc' }`;
            case 'industryDescription':
                return `description.${ args.sortKind ? 'desc' : 'asc' }`;
            default:
                return 'name.desc';
        }
    }

    public render() {
        return (
            <>
                <div>
                    <TextOut fontSize={ 13 } fontWeight={ 400 }>
                        На этой странице вы можете управлять отраслями.
                    </TextOut>
                </div>
                <div style={ {
                    marginTop: '10px'
                } }
                >
                    <TextOut fontSize={ 12 } fontWeight={ 600 }>
                        Управление отраслями
                    </TextOut>
                </div>
                <br />

                <CommonTableWithFilter
                    uniqueContextProviderStateId="IndustryContextProviderStateId"
                    tableEditProps={ {
                        processProps: {
                            watch: {
                                reducerStateName: 'IndustryState',
                                processIds: {
                                    addNewItemProcessId: IndustryProcessId.AddIndustry,
                                    modifyItemProcessId: IndustryProcessId.UpdateIndustry,
                                    deleteItemProcessId: IndustryProcessId.DeleteIndustry
                                }
                            },
                            processMessages: {
                                getOnSuccessAddNewItemMessage: () => 'Добавление новой отрасли успешно завершено.',
                                getOnSuccessModifyItemMessage: () => 'Редактирование отрасли успешно завершено.',
                                getOnSuccessDeleteItemMessage: item => `Удаление отрасли "${ item.industryName }" успешно завершено.`
                            }
                        },

                        dialogProps: {
                            deleteDialog: {
                                getDialogTitle: item => `Удаление отрасли "${ item.industryName }"`,
                                getDialogContentView: itemToDelete => `Вы действительно хотите удалить отрасль "${ itemToDelete.industryName }"?`,
                                confirmDeleteItemButtonText: 'Удалить',
                                onDeleteItemConfirmed: itemToDelete => {
                                    this.props.dispatchDeleteIndustryThunk({
                                        industryId: itemToDelete.industryId,
                                        force: true
                                    });
                                }
                            },
                            addDialog: {
                                dialogWidth: 'sm',
                                getDialogTitle: () => 'Добавление новой отрасли',
                                getDialogContentFormView: ref =>
                                    <AddModifyIndustryDialogContentFormView
                                        ref={ ref }
                                        itemToEdit={
                                            {
                                                industryId: '',
                                                industryName: '',
                                                industryDescription: '',
                                            }
                                        }
                                    />,
                                confirmAddItemButtonText: 'Добавить',
                                showAddDialogButtonText: 'Добавить отрасль',
                                onAddNewItemConfirmed: itemToAdd => {
                                    this.props.dispatchAddIndustryThunk({
                                        industryName: itemToAdd.industryName,
                                        industryDescription: itemToAdd.industryDescription,
                                        force: true
                                    });
                                }
                            },
                            modifyDialog: {
                                getDialogTitle: () => 'Редактирование отрасли',
                                dialogWidth: 'sm',
                                confirmModifyItemButtonText: 'Сохранить',
                                getDialogContentFormView: (ref, item) => <AddModifyIndustryDialogContentFormView
                                    ref={ ref }
                                    itemToEdit={ item }
                                />,
                                onModifyItemConfirmed: itemToModify => {
                                    this.props.dispatchUpdateIndustryThunk({
                                        industryId: itemToModify.industryId,
                                        industryName: itemToModify.industryName,
                                        industryDescription: itemToModify.industryDescription,
                                        force: true
                                    });
                                }
                            }
                        }
                    } }
                    filterProps={ {
                        getFilterContentView: (ref, onFilterChanged) => <IndustryFilterFormView ref={ ref } isFilterFormDisabled={ this.props.isInLoadingDataProcess } onFilterChanged={ onFilterChanged } />
                    } }
                    tableProps={ {
                        dataset: this.props.dataset,
                        keyFieldName: 'industryId',
                        sorting: {
                            sortFieldName: 'industryName',
                            sortKind: SortingKind.Asc
                        },
                        fieldsView: {
                            industryName: {
                                caption: 'Название отрасли',
                                fieldContentNoWrap: false,
                                isSortable: true,
                                fieldWidth: 'auto'
                            },
                            industryDescription: {
                                caption: 'Описание отрасли',
                                fieldContentNoWrap: false,
                                isSortable: false,
                                fieldWidth: 'auto'
                            }
                        },
                        pagination: this.props.pagination
                    } }
                    onDataSelect={ this.onDataSelect }
                />
            </>
        );
    }
}

const IndustryConnected = connect<StateProps, DispatchProps, {}, AppReduxStoreState>(
    state => {
        const industryState = state.IndustryState;
        const industry = industryState.Industry;
        const dataset = industry.records;

        const currentPage = industry.metadata.pageNumber;
        const totalPages = industry.metadata.pageCount;
        const { pageSize } = industry.metadata;
        const recordsCount = industry.metadata.totalItemCount;

        const isInLoadingDataProcess = ProcessIdStateHelper.isInProgress(industryState.reducerActions, IndustryProcessId.ReceiveIndustry);

        return {
            dataset,
            isInLoadingDataProcess,
            pagination: {
                currentPage,
                totalPages,
                pageSize,
                recordsCount
            }
        };
    },
    {
        dispatchReceiveIndustryThunk: ReceiveIndustryThunk.invoke,
        dispatchUpdateIndustryThunk: UpdateIndustryThunk.invoke,
        dispatchAddIndustryThunk: AddIndustryThunk.invoke,
        dispatchDeleteIndustryThunk: DeleteIndustryThunk.invoke
    }
)(IndustryClass);

export const IndustryView = withHeader({
    title: 'Страница отраслей',
    page: IndustryConnected
});