import { hasComponentChangesFor } from 'app/common/functions';
import { TextBoxForm } from 'app/views/components/controls/forms';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { IndustryItemDataModel } from 'app/web/InterlayerApiProxy/IndustryApiProxy/receiveIndustry';
import React from 'react';
import { Box } from '@mui/material';

type OwnProps = ReduxFormProps<IndustryItemDataModel> & {
    itemToEdit: IndustryItemDataModel;
};

class AddModifyIndustryDialogContentFormViewClass extends React.Component<OwnProps> {
    public constructor(props: OwnProps) {
        super(props);
        this.onValueChange = this.onValueChange.bind(this);
        this.initReduxForm = this.initReduxForm.bind(this);

        this.props.reduxForm.setInitializeFormDataAction(this.initReduxForm);
    }

    public shouldComponentUpdate(nextProps: OwnProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private onValueChange<TValue>(formName: string, newValue: TValue) {
        this.props.reduxForm.updateReduxFormFields({
            [formName]: newValue
        });
    }

    private initReduxForm(): IndustryItemDataModel | undefined {
        if (!this.props.itemToEdit) {
            return;
        }
        return {
            industryId: this.props.itemToEdit.industryId,
            industryName: this.props.itemToEdit.industryName,
            industryDescription: this.props.itemToEdit.industryDescription
        };
    }

    public render() {
        if (!this.props.itemToEdit) {
            return null;
        }

        const reduxForm = this.props.reduxForm.getReduxFormFields(false);

        return (
            <Box display="flex" flexDirection="column" gap="16px">
                <TextBoxForm
                    formName="industryName"
                    label="Название отрасли"
                    value={ reduxForm.industryName }
                    autoFocus={ true }
                    onValueChange={ this.onValueChange }
                />
                <TextBoxForm
                    formName="industryDescription"
                    label="Описание отрасли"
                    value={ reduxForm.industryDescription }
                    onValueChange={ this.onValueChange }
                    type="textarea"
                />
            </Box>
        );
    }
}

export const AddModifyIndustryDialogContentFormView = withReduxForm(AddModifyIndustryDialogContentFormViewClass, {
    reduxFormName: ' AddModifyIndustry_ReduxForm',
    resetOnUnmount: true
});