import { hasComponentChangesFor } from 'app/common/functions';
import { TextBoxForm } from 'app/views/components/controls/forms/TextBoxForm';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { IndustryFilterDataForm, IndustryFilterDataFormFieldNamesType } from 'app/views/modules/Industry/types';
import React from 'react';

/**
 * Свойства для компонента фильтра
 */
type OwnProps = ReduxFormProps<IndustryFilterDataForm> & {
    onFilterChanged: (filter: IndustryFilterDataForm, filterFormName: string) => void;
    isFilterFormDisabled: boolean;
};

class IndustryFilterFormViewClass extends React.Component<OwnProps> {
    public constructor(props: OwnProps) {
        super(props);
        this.initReduxForm = this.initReduxForm.bind(this);
        this.onValueChange = this.onValueChange.bind(this);
        this.onValueApplied = this.onValueApplied.bind(this);
    }

    public componentDidMount() {
        this.props.reduxForm.setInitializeFormDataAction(this.initReduxForm);
    }

    public shouldComponentUpdate(nextProps: OwnProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private onValueChange<TValue, TForm extends string = IndustryFilterDataFormFieldNamesType>(formName: TForm, newValue: TValue) {
        this.props.reduxForm.updateReduxFormFields({
            [formName]: newValue
        });
        if (formName === 'name' && newValue && (newValue as any as string).length < 3) {
            return false;
        }
    }

    private onValueApplied<TValue>(formName: string, value: TValue) {
        this.props.onFilterChanged({
            ...this.props.reduxForm.getReduxFormFields(false),
            [formName]: value
        }, formName);
    }

    private initReduxForm(): IndustryFilterDataForm | undefined {
        return {
            name: ''
        };
    }

    public render() {
        const formFields = this.props.reduxForm.getReduxFormFields(false);

        return (
            <TextBoxForm
                autoFocus={ true }
                label="Название отрасли"
                formName="name"
                onValueChange={ this.onValueChange }
                onValueApplied={ this.onValueApplied }
                value={ formFields.name }
                isReadOnly={ this.props.isFilterFormDisabled }
            />
        );
    }
}

export const IndustryFilterFormView = withReduxForm(IndustryFilterFormViewClass, {
    reduxFormName: 'Industry_Filter_Form',
    resetOnUnmount: true
});