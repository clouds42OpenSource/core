/**
 * Модель Redux формы для редактирования фильтра для таблицы отрасли
 */
export type IndustryFilterDataForm = {
    /**
     * Название отрасли
     */
    name: string;
};