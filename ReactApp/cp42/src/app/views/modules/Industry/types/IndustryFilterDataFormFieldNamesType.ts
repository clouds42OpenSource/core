import { IndustryFilterDataForm } from 'app/views/modules/Industry/types';

export type IndustryFilterDataFormFieldNamesType = keyof IndustryFilterDataForm;