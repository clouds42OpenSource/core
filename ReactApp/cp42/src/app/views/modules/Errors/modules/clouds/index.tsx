import React from 'react';
import styles from './style.module.css';

type TClouds = {
    children: React.ReactNode;
    isBackgroundInvisible?: boolean;
};

export const Clouds = ({ children, isBackgroundInvisible }: TClouds) => {
    return (
        <div className={ isBackgroundInvisible ? styles.containerInvisible : styles.container }>
            <div className={ `${ styles.cloud } ${ styles.x1 }` } />
            <div className={ `${ styles.cloud } ${ styles.x2 }` } />
            <div className={ `${ styles.cloud } ${ styles.x3 }` } />
            <div className={ `${ styles.cloud } ${ styles.x4 }` } />
            <div className={ `${ styles.cloud } ${ styles.x5 }` } />
            <div className={ `${ styles.cloud } ${ styles.x6 }` } />
            { children }
        </div>
    );
};