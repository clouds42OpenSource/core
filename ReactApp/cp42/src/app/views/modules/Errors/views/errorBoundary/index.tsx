import { Link } from '@mui/material';
import React from 'react';

import { TextOut } from 'app/views/components/TextOut';
import { AppRoutes } from 'app/AppRoutes';
import { Clouds } from '../../modules';

import styles from '../style.module.css';

export const Boundary = () => {
    const onClickHandler = () => {
        window.location.href = AppRoutes.homeRoute;
    };

    return (
        <Clouds>
            <div className={ styles.contentClouds }>
                <img src={ `${ import.meta.env.PUBLIC_URL }/img/errors/mistake2.png` } alt="errorBoundary" />
                <TextOut fontWeight={ 700 } fontSize={ 26 } textAlign="center" className={ styles.textoutWhite }>Что-то пошло не так.</TextOut>
                <TextOut fontWeight={ 700 } fontSize={ 20 } textAlign="center" className={ styles.textoutWhite }>
                    Просим прощения за доставленные неудобства. Попробуйте повторить опрецию заново или обратитесь в тех-поддержку через наш <a className={ styles.link } href="https://t.me/clouds42bot">телеграм-бот</a>.
                </TextOut>
                <Link onClick={ onClickHandler } className={ styles.link }>Вернуться на главную</Link>
            </div>
        </Clouds>
    );
};