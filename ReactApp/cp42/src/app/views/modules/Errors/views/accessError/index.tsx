import { Link } from '@mui/material';
import { useHistory } from 'react-router';

import { TextOut } from 'app/views/components/TextOut';
import { AppRoutes } from 'app/AppRoutes';

import styles from '../style.module.css';

export const AccessError = () => {
    const history = useHistory();

    const onClickHandler = () => {
        history.push(AppRoutes.homeRoute);
    };

    return (
        <div className={ styles.content }>
            <img src={ `${ import.meta.env.PUBLIC_URL }/img/errors/cry-cloud.png` } alt="error-access" />
            <TextOut fontWeight={ 700 } fontSize={ 20 } textAlign="center" className={ styles.textout }>Недостаточно прав для просмотра данной страницы.</TextOut>
            <Link onClick={ onClickHandler }>Вернуться на главную</Link>
        </div>
    );
};