import React from 'react';

import { TextOut } from 'app/views/components/TextOut';
import { AppRoutes } from 'app/AppRoutes';
import { Clouds } from '../../modules';

import styles from '../style.module.css';

export const NoMoneyNoHoney = () => {
    const onClickHandler = () => {
        window.location.href = AppRoutes.accountManagement.balanceManagement;
    };

    return (
        <Clouds>
            <div className={ styles.contentClouds }>
                <h1 className={ styles.title }>ОЙ</h1>
                <TextOut fontWeight={ 700 } fontSize={ 32 } className={ styles.textoutWhite }>Сервис Аренда 1С отключен за неуплату.</TextOut>
                <TextOut fontWeight={ 700 } fontSize={ 26 } className={ styles.noMoneyNoHoneyDescription }>
                    На балансе недостаточно средств для продления сервиса.
                </TextOut>
                <TextOut fontWeight={ 700 } fontSize={ 26 } className={ styles.noMoneyNoHoneyDescription }>
                    Для доступа к сервису необходимо пополнить баланс.
                </TextOut>
                <TextOut fontWeight={ 700 } fontSize={ 32 } className={ styles.textoutWhite }>
                    <span className={ styles.link } onClick={ onClickHandler }>Оплатить</span>
                </TextOut>
            </div>
        </Clouds>
    );
};