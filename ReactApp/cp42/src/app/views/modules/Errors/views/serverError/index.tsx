import { Link } from '@mui/material';
import React from 'react';

import { TextOut } from 'app/views/components/TextOut';
import { AppRoutes } from 'app/AppRoutes';
import { Clouds } from '../../modules';

import styles from '../style.module.css';

export const ServerError = () => {
    const onClickHandler = () => {
        window.location.href = AppRoutes.homeRoute;
    };

    return (
        <Clouds>
            <div className={ styles.contentClouds }>
                <img src={ `${ import.meta.env.PUBLIC_URL }/img/errors/mistake2.png` } alt="error404" />
                <TextOut fontWeight={ 700 } fontSize={ 26 } textAlign="center" className={ styles.textoutWhite }>Сервер временно недоступен.</TextOut>
                <TextOut fontWeight={ 700 } fontSize={ 20 } textAlign="center" className={ styles.textoutWhite }>Просим прощения за доставленные неудобства. Скоро мы все починим.</TextOut>
                <TextOut textAlign="center" fontSize={ 16 } className={ styles.textoutWhite }>
                    Обратиться в тех-поддержку можно через наш <a className={ styles.link } href="https://t.me/clouds42bot">телеграм-бот</a>
                </TextOut>
                <Link onClick={ onClickHandler } className={ styles.link }>Вернуться на главную</Link>
            </div>
        </Clouds>
    );
};