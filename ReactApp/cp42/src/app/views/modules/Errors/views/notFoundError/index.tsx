import { Link } from '@mui/material';
import React from 'react';
import { useHistory } from 'react-router';

import { TextOut } from 'app/views/components/TextOut';
import { AppRoutes } from 'app/AppRoutes';

import styles from '../style.module.css';

export const NotFoundError = () => {
    const history = useHistory();

    const onClickHandler = () => {
        history.push(AppRoutes.homeRoute);
    };

    return (
        <div className={ styles.content }>
            <img src={ `${ import.meta.env.PUBLIC_URL }/img/errors/mistake.png` } alt="error404" />
            <TextOut fontWeight={ 700 } fontSize={ 20 } textAlign="center" className={ styles.textout }>Ошибка 404. Такой страницы не существует.</TextOut>
            <TextOut fontWeight={ 700 } fontSize={ 16 } textAlign="center" className={ styles.textout }>Возможно, вы ошиблись набирая адрес, или данная страница удалена.</TextOut>
            <Link onClick={ onClickHandler } className={ styles.link }>Вернуться на главную</Link>
        </div>
    );
};