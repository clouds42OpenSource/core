import { SortingKind } from 'app/common/enums';
import { hasComponentChangesFor } from 'app/common/functions';
import { CloudServicesProcessId } from 'app/modules/cloudServices';
import { AddCloudServiceThunkParams } from 'app/modules/cloudServices/store/reducers/addCloudServiceReducer/params';
import {
    ReceiveCloudServicesThunkParams
} from 'app/modules/cloudServices/store/reducers/receiveCloudServicesReducer/params';
import {
    UpdateCloudServiceThunkParams
} from 'app/modules/cloudServices/store/reducers/updateCloudServiceReducer/params';
import { ReceiveCloudServicesThunk, UpdateCloudServiceThunk } from 'app/modules/cloudServices/store/thunks';
import { AddCloudServiceThunk } from 'app/modules/cloudServices/store/thunks/AddCloudServiceThunk';
import { AppReduxStoreState } from 'app/redux/types';
import { withHeader } from 'app/views/components/_hoc/withHeader';
import {
    AddModifyCloudServiceDialogContentFormView
} from 'app/views/modules/CloudServices/views/AddModifyCloudServiceDialogContentFormView';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { CloudServiceItemDataModel } from 'app/web/InterlayerApiProxy/CloudServicesApiProxy/receiveCloudServices';
import React from 'react';
import { connect } from 'react-redux';

type StateProps<TDataRow = CloudServiceItemDataModel> = {
    dataset: TDataRow[];
};

type DispatchProps = {
    dispatchReceiveCloudServicesThunk: (args?: ReceiveCloudServicesThunkParams) => void;
    dispatchUpdateCloudServiceThunk: (args: UpdateCloudServiceThunkParams) => void;
    dispatchAddCloudServiceThunk: (args: AddCloudServiceThunkParams) => void;
};

type AllProps = StateProps & DispatchProps;

class CloudServicesPageViewClass extends React.Component<AllProps> {
    public constructor(props: AllProps) {
        super(props);
        this.onDataSelect = this.onDataSelect.bind(this);
    }

    public shouldComponentUpdate(nextProps: AllProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private onDataSelect() {
        this.props.dispatchReceiveCloudServicesThunk();
    }

    public render() {
        return (
            <CommonTableWithFilter
                uniqueContextProviderStateId="CloudServicesStateContextProviderStateId"
                tableEditProps={ {
                    processProps: {
                        watch: {
                            reducerStateName: 'CloudServicesState',
                            processIds: {
                                addNewItemProcessId: CloudServicesProcessId.AddCloudService,
                                modifyItemProcessId: CloudServicesProcessId.UpdateCloudService
                            }
                        },
                        processMessages: {
                            getOnSuccessAddNewItemMessage: () => 'Добавление новой службы Clouds42 успешно завершено.',
                            getOnSuccessModifyItemMessage: () => 'Данные для службы Clouds42 сохранены.'
                        }
                    },
                    dialogProps: {
                        addDialog: {
                            dialogWidth: 'md',
                            dialogVerticalAlign: 'top',
                            getDialogTitle: () => 'Добавление новой службы',
                            getDialogContentFormView: ref =>
                                <AddModifyCloudServiceDialogContentFormView
                                    ref={ ref }
                                    itemToEdit={
                                        {
                                            id: '',
                                            jsonWebToken: '',
                                            cloudServiceId: '',
                                            serviceCaption: ''
                                        }
                                    }
                                />,
                            confirmAddItemButtonText: 'Добавить',
                            showAddDialogButtonText: 'Добавить новую службу',
                            onAddNewItemConfirmed: itemToAdd => {
                                this.props.dispatchAddCloudServiceThunk({
                                    ...itemToAdd,
                                    force: true
                                });
                            }
                        },
                        modifyDialog: {
                            dialogWidth: 'md',
                            isDialogTitleSmall: false,
                            dialogVerticalAlign: 'stretch',
                            getDialogTitle: () => 'Редактирование данных службы',
                            confirmModifyItemButtonText: 'Сохранить',
                            getDialogContentFormView: (ref, item) => <AddModifyCloudServiceDialogContentFormView
                                ref={ ref }
                                itemToEdit={ item }
                            />,
                            onModifyItemConfirmed: itemToModify => {
                                this.props.dispatchUpdateCloudServiceThunk({
                                    ...itemToModify,
                                    force: true
                                });
                            }
                        }
                    }
                } }
                tableProps={ {
                    dataset: this.props.dataset,
                    keyFieldName: 'id',
                    sorting: {
                        sortFieldName: 'id',
                        sortKind: SortingKind.Asc
                    },
                    fieldsView: {
                        id: {
                            caption: 'ID',
                            fieldContentNoWrap: true
                        },
                        cloudServiceId: {
                            caption: 'Имя службы',
                            fieldContentNoWrap: true
                        },
                        serviceCaption: {
                            caption: 'Описание',
                            fieldContentNoWrap: true
                        }
                    }
                } }
                onDataSelect={ this.onDataSelect }
            />
        );
    }
}

const CloudServicesPageViewConnected = connect<StateProps, DispatchProps, {}, AppReduxStoreState>(
    state => {
        const cloudServicesState = state.CloudServicesState;
        const dataset = cloudServicesState.cloudServices;

        return {
            dataset
        };
    },
    {
        dispatchUpdateCloudServiceThunk: UpdateCloudServiceThunk.invoke,
        dispatchAddCloudServiceThunk: AddCloudServiceThunk.invoke,
        dispatchReceiveCloudServicesThunk: ReceiveCloudServicesThunk.invoke
    }
)(CloudServicesPageViewClass);

export const CloudServicesPageView = withHeader({
    title: 'Службы Clouds 42',
    page: CloudServicesPageViewConnected
});