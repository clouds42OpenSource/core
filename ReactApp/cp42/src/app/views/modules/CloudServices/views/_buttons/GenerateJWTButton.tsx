import RefreshIcon from '@mui/icons-material/Refresh';
import { hasComponentChangesFor } from 'app/common/functions';
import { TextButton } from 'app/views/components/controls/Button';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import cn from 'classnames';
import { RequestKind } from 'core/requestSender/enums';
import React from 'react';
import css from '../styles.module.css';

type OwnProps = FloatMessageProps & {
    cloudServiceId: string;
    onGeneratedJsonWebToken: (jsonWebToken: string) => void;
};

type OwnState = {
    isInGenerationOfTokenState: boolean;
};

export class GenerateJWTButtonClass extends React.Component<OwnProps, OwnState> {
    public constructor(props: OwnProps) {
        super(props);
        this.generateNewJsonWebToken = this.generateNewJsonWebToken.bind(this);

        this.state = {
            isInGenerationOfTokenState: false
        };
    }

    public shouldComponentUpdate(nextProps: OwnProps, nextState: OwnState) {
        return hasComponentChangesFor(this.props, nextProps) ||
            hasComponentChangesFor(this.state, nextState);
    }

    private async generateNewJsonWebToken() {
        const cloudServicesApi = InterlayerApiProxy.getCloudServicesApi();

        this.setState({
            isInGenerationOfTokenState: true
        });

        try {
            const response = await cloudServicesApi.generateJWTForCloudService(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, {
                cloudServiceId: this.props.cloudServiceId
            });

            this.setState({
                isInGenerationOfTokenState: false
            }, () => {
                if (this.props.onGeneratedJsonWebToken) {
                    this.props.onGeneratedJsonWebToken(response.jsonWebToken);
                }
            });
        } catch (error: any) {
            this.setState({
                isInGenerationOfTokenState: false
            });

            this.props.floatMessage.show(MessageType.Error, error.message);
        }
    }

    public render() {
        return (
            <TextButton
                className={ cn(css['generate-jwt-button']) }
                kind="success"
                title="Обновить токен"
                showProgress={ this.state.isInGenerationOfTokenState }
                isEnabled={ !this.state.isInGenerationOfTokenState }
                onClick={ this.generateNewJsonWebToken }
            >
                <RefreshIcon />
            </TextButton>
        );
    }
}

export const GenerateJWTButton = withFloatMessages(GenerateJWTButtonClass);