import { hasComponentChangesFor } from 'app/common/functions';
import { TableFilterWrapper } from 'app/views/components/_hoc/tableFilterWrapper';
import { FilterItemWrapper } from 'app/views/components/_hoc/tableFilterWrapper/filterItemWrapper';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { TextBoxForm } from 'app/views/components/controls/forms';
import { TextOut } from 'app/views/components/TextOut';
import { CloudServiceEditDataForm } from 'app/views/modules/CloudServices/types';
import { GenerateJWTButton } from 'app/views/modules/CloudServices/views/_buttons';
import { CloudServiceItemDataModel } from 'app/web/InterlayerApiProxy/CloudServicesApiProxy/receiveCloudServices';
import cn from 'classnames';
import jwt_decode from 'jwt-decode';
import React from 'react';
import css from '../styles.module.css';

type OwnProps = ReduxFormProps<CloudServiceEditDataForm> & {
    itemToEdit: CloudServiceItemDataModel;
};

const jwtSystemClaims = ['nbf', 'exp', 'iss', 'aud'];

class AddModifyCloudServiceDialogContentFormViewClass extends React.Component<OwnProps> {
    public constructor(props: OwnProps) {
        super(props);
        this.getJsonWebTokenClaims = this.getJsonWebTokenClaims.bind(this);
        this.getJsonWebToken = this.getJsonWebToken.bind(this);
        this.initReduxForm = this.initReduxForm.bind(this);
        this.onGeneratedJsonWebToken = this.onGeneratedJsonWebToken.bind(this);
        this.onValueChange = this.onValueChange.bind(this);
        this.getCloudServiceId = this.getCloudServiceId.bind(this);
    }

    public componentDidMount() {
        this.props.reduxForm.setInitializeFormDataAction(this.initReduxForm);
    }

    public shouldComponentUpdate(nextProps: OwnProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private onValueChange<TValue>(formName: string, newValue: TValue) {
        this.props.reduxForm.updateReduxFormFields({
            [formName]: newValue,
            jsonWebToken: this.props.itemToEdit.jsonWebToken
        });
    }

    private onGeneratedJsonWebToken(jsonWebToken: string) {
        this.props.reduxForm.updateReduxFormFields({
            cloudServiceId: this.props.itemToEdit.cloudServiceId,
            serviceCaption: this.props.itemToEdit.serviceCaption,
            jsonWebToken
        });
    }

    private getJsonWebTokenClaims(): { [key: string]: string } | null {
        const jsonWebToken = this.getJsonWebToken();

        if (!jsonWebToken) {
            return null;
        }

        try {
            return jwt_decode(jsonWebToken);
        } catch {
            return null;
        }
    }

    private getTokenLifeTimeInMinutes(jstClaims: { [key: string]: string }) {
        const nbf = parseInt(jstClaims.nbf, 10);
        const exp = parseInt(jstClaims.exp, 10);

        return (exp - nbf) / 60;
    }

    private getJsonWebToken() {
        return this.props.reduxForm.getReduxFormFields(true).jsonWebToken ?? this.props.itemToEdit.jsonWebToken;
    }

    private getCloudServiceId() {
        return this.props.reduxForm.getReduxFormFields(true)?.cloudServiceId ?? this.props.itemToEdit.cloudServiceId;
    }

    private getTokenIssuer(jstClaims: { [key: string]: string }) {
        return jstClaims.iss;
    }

    private getDateForTokenTicks(ticks: number) {
        const d = new Date(0);
        d.setUTCSeconds(ticks);
        return d.toLocaleString();
    }

    private createSystemClaimsTokenInfoData(jwtClaims: { [key: string]: string } | null) {
        if (!jwtClaims) {
            return null;
        }

        return (
            <>
                { this.createHeader('Системные данные токена') }
                { this.createTokenData('Издатель токена', this.getTokenIssuer(jwtClaims)) }
                { this.createTokenData('Время жизни токена', `${ this.getTokenLifeTimeInMinutes(jwtClaims) } минут[а\\ы]`) }
                { this.createTokenData('Начало действия токена', this.getDateForTokenTicks(parseInt(jwtClaims.nbf, 10)).toString()) }
                { this.createTokenData('Конец действия токена', this.getDateForTokenTicks(parseInt(jwtClaims.exp, 10)).toString()) }
                { this.createHorizontalLine() }
                { this.createLineBreak() }
            </>
        );
    }

    private initReduxForm(): CloudServiceEditDataForm | undefined {
        if (!this.props.itemToEdit) {
            return;
        }

        return {
            id: this.props.itemToEdit.id,
            jsonWebToken: this.props.itemToEdit.jsonWebToken ?? '',
            cloudServiceId: this.props.itemToEdit.cloudServiceId,
            serviceCaption: this.props.itemToEdit.serviceCaption
        };
    }

    private createTokenInfoData() {
        const jsonWebToken = this.getJsonWebToken();

        return (
            <>
                <tr>
                    <td colSpan={ 2 }>
                        <TableFilterWrapper>
                            <FilterItemWrapper width="45%">
                                <TextBoxForm
                                    formName="cloudServiceId"
                                    type="text"
                                    autoFocus={ this.props.itemToEdit.id === '' }
                                    label="Имя службы"
                                    isReadOnly={ this.props.itemToEdit.id !== '' }
                                    onValueChange={ this.onValueChange }
                                    value={ this.getCloudServiceId() }
                                />
                            </FilterItemWrapper>
                            <FilterItemWrapper width="45%">
                                <TextBoxForm
                                    formName="serviceCaption"
                                    autoFocus={ this.props.itemToEdit.id !== '' }
                                    type="text"
                                    label="Описание"
                                    onValueChange={ this.onValueChange }
                                    value={ this.props.reduxForm.getReduxFormFields(true)?.serviceCaption ?? this.props.itemToEdit.serviceCaption }
                                />
                            </FilterItemWrapper>
                        </TableFilterWrapper>
                    </td>
                </tr>
                {
                    this.props.itemToEdit.id
                        ? (
                            <tr>
                                <td colSpan={ 2 }>
                                    <TextBoxForm
                                        className={ cn(css['json-view-jwt']) }
                                        formName="jsonWebToken"
                                        type="textarea"
                                        label={ <>Токен<GenerateJWTButton cloudServiceId={ this.getCloudServiceId() } onGeneratedJsonWebToken={ this.onGeneratedJsonWebToken } /></> }
                                        isReadOnly={ true }
                                        value={ jsonWebToken || 'Нет токена, нажмите кнопку обновить напротив слова "Токен", что бы сгенерировать новый.' }
                                    />
                                </td>
                            </tr>
                        )
                        : null
                }
                {
                    this.props.itemToEdit.id
                        ? this.createLineBreak()
                        : null
                }
            </>
        );
    }

    private createUserClaimsTokenInfoData(jwtClaims: { [key: string]: string } | null) {
        if (!jwtClaims) {
            return null;
        }

        const keys = Object.keys(jwtClaims);
        return (
            <>
                { this.createHeader('Пользовательские данные токена') }
                {
                    keys.map(key => {
                        if (jwtSystemClaims.indexOf(key) < 0) {
                            return this.createTokenData(key, jwtClaims[key]);
                        }
                        return null;
                    })
                }
                { this.createHorizontalLine() }
            </>
        );
    }

    private createLineBreak() {
        return (
            <tr>
                <td>
                    <br />
                </td>
            </tr>
        );
    }

    private createHorizontalLine() {
        return (
            <tr>
                <td colSpan={ 2 }>
                    <hr className={ cn(css['json-view-data_hr__separator']) } />
                </td>
            </tr>
        );
    }

    private createTokenData(key: string, value?: string | undefined) {
        return (
            <tr key={ key } className="table-row-highlight-on-hover">
                <td colSpan={ value ? 1 : 2 } className={ cn(css['json-view-data']) }>
                    <TextOut fontSize={ 13 } fontWeight={ 400 }>
                        { key }
                    </TextOut>
                </td>
                {
                    value
                        ? (
                            <td className={ cn(css['json-view-data']) }>
                                <TextOut fontSize={ 13 } fontWeight={ 400 }>
                                    { value }
                                </TextOut>
                            </td>
                        )
                        : null
                }
            </tr>
        );
    }

    private createHeader(title: React.ReactNode) {
        return (
            <>
                <tr>
                    <td colSpan={ 2 }>
                        <TextOut fontSize={ 13 } fontWeight={ 600 }>
                            { title }
                        </TextOut>
                    </td>
                </tr>
                { this.createHorizontalLine() }
            </>
        );
    }

    public render() {
        if (!this.props.itemToEdit) {
            return null;
        }

        const jwtClaims = this.getJsonWebTokenClaims();
        return (
            <table className={ cn(css['json-view-data-table']) }>
                <tbody>
                    { this.createTokenInfoData() }
                    { this.createSystemClaimsTokenInfoData(jwtClaims) }
                    { this.createUserClaimsTokenInfoData(jwtClaims) }
                </tbody>
            </table>
        );
    }
}

export const AddModifyCloudServiceDialogContentFormView = withReduxForm(AddModifyCloudServiceDialogContentFormViewClass, {
    reduxFormName: 'CloudService_Item_Edit_Form',
    resetOnUnmount: true
});