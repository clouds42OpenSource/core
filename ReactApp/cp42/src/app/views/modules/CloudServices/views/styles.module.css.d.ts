declare const styles: {
  readonly 'generate-jwt-button': string;
  readonly 'json-view-jwt': string;
  readonly 'json-view-data-table': string;
  readonly 'json-view-data': string;
  readonly 'json-view-data_hr__separator': string;
};
export = styles;