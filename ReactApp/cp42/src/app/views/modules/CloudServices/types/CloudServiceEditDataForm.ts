import { CloudServiceItemDataModel } from 'app/web/InterlayerApiProxy/CloudServicesApiProxy/receiveCloudServices';

/**
 * Модель Redux формы для редактирования справочника CloudServices
 */
export type CloudServiceEditDataForm = CloudServiceItemDataModel;