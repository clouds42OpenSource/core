import { CloudServiceEditDataForm } from 'app/views/modules/CloudServices/types/CloudServiceEditDataForm';

export type CloudServiceEditDataFormFieldNamesType = keyof CloudServiceEditDataForm;