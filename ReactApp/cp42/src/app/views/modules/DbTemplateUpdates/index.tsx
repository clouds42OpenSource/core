import { DbTemplateUpdateStateEnumType } from 'app/common/enums';
import { Nullable } from 'app/common/types';
import { DbTemplateUpdateProcessId } from 'app/modules/dbTemplateUpdate';
import { ApplyUpdatesParams } from 'app/modules/dbTemplateUpdate/store/reducers/applyUpdatesReducer/params';
import { DeleteUpdatesParams } from 'app/modules/dbTemplateUpdate/store/reducers/deleteUpdatesReducer/params';
import { GetDbTemplateUpdatesParams } from 'app/modules/dbTemplateUpdate/store/reducers/getDbTemplateUpdatesReducer/params';
import { RejectUpdatesParams } from 'app/modules/dbTemplateUpdate/store/reducers/rejectUpdatesReducer/params';
import { GetDbTemplateUpdatesThunk } from 'app/modules/dbTemplateUpdate/store/thunks';
import { ApplyUpdatesThunk } from 'app/modules/dbTemplateUpdate/store/thunks/ApplyUpdatesThunk';
import { DeleteUpdatesThunk } from 'app/modules/dbTemplateUpdate/store/thunks/DeleteUpdatesThunk';
import { RejectUpdatesThunk } from 'app/modules/dbTemplateUpdate/store/thunks/RejectUpdatesThunk';
import { AppReduxStoreState } from 'app/redux/types';
import { DateUtility } from 'app/utils';
import { ContainedButton } from 'app/views/components/controls/Button';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { withHeader } from 'app/views/components/_hoc/withHeader';
import { DbTemplateUpdatesButtonsEnumType } from 'app/views/modules/DbTemplateUpdates/types';
import { SearchDbTemplateUpdateView } from 'app/views/modules/DbTemplateUpdates/views/SearchDbTemplateUpdateView';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { WatchReducerProcessActionState } from 'app/views/modules/_common/components/WatchReducerProcessActionState';
import { DbTemplateUpdatesDataModel } from 'app/web/InterlayerApiProxy/DbTemplateUpdateApiProxy/getTemplateUpdates/data-models';
import { IReducerProcessActionInfo } from 'core/redux/interfaces';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Dialog } from 'app/views/components/controls/Dialog';
import ErrorOutlineIcon from '@mui/icons-material/ErrorOutline';
import { Box } from '@mui/material';
import css from './styles.module.css';

type OwnState = {
    /**
     * Заголовок окна подтверждения
     */
    dialogMessage: string,

    /**
     * ID обновлений шаблона баз
     */
    dbTemplateUpdateId: string,

    /**
     * Тип кнопки действия над обновлением шаблона
     */
    dbTemplateUpdatesButtonType: Nullable<DbTemplateUpdatesButtonsEnumType>,
    /**
     * Состояние модального окна
     */
    isOpen: boolean
};

type StateProps = {
    /**
     * Массив обновленний шаблона баз
     */
    dataset: Array<DbTemplateUpdatesDataModel>;
};

type DispatchProps = {
    dispatchGetDbTemplateUpdatesThunk: (args: GetDbTemplateUpdatesParams) => void;
    dispatchApplyUpdatesThunk: (args: ApplyUpdatesParams) => void;
    dispatchRejectUpdatesThunk: (args: RejectUpdatesParams) => void;
    dispatchDeleteUpdatesThunk: (args: DeleteUpdatesParams) => void;
};

type AllProps = StateProps & DispatchProps & FloatMessageProps;

class DbTemplateUpdatesClass extends Component<AllProps, OwnState> {
    private _searchButtonRef = React.createRef<HTMLButtonElement>();

    public constructor(props: AllProps) {
        super(props);

        this.state = {
            dialogMessage: '',
            dbTemplateUpdateId: '',
            dbTemplateUpdatesButtonType: null,
            isOpen: false
        };

        this.getDbTemplateUpdatesButtonsFormat = this.getDbTemplateUpdatesButtonsFormat.bind(this);
        this.onClickDbTemplateUpdatesButton = this.onClickDbTemplateUpdatesButton.bind(this);
        this.onProcessActionStateChanged = this.onProcessActionStateChanged.bind(this);
        this.onClickDialogButtonConfirm = this.onClickDialogButtonConfirm.bind(this);
        this.successOrFaildStateNotification = this.successOrFaildStateNotification.bind(this);
        this.closeDialog = this.closeDialog.bind(this);
    }

    public componentDidMount() {
        this.props.dispatchGetDbTemplateUpdatesThunk({
            force: true,
            validateState: DbTemplateUpdateStateEnumType.New
        });
    }

    private onClickDialogButtonConfirm() {
        const { dbTemplateUpdateId, dbTemplateUpdatesButtonType } = this.state;

        if (dbTemplateUpdatesButtonType !== null) {
            switch (dbTemplateUpdatesButtonType) {
                case DbTemplateUpdatesButtonsEnumType.ApplyButton:
                    this.props.dispatchApplyUpdatesThunk({
                        force: true,
                        dbTemplateUpdateId
                    });
                    break;
                case DbTemplateUpdatesButtonsEnumType.RejectButton:
                    this.props.dispatchRejectUpdatesThunk({
                        force: true,
                        dbTemplateUpdateId
                    });
                    break;
                case DbTemplateUpdatesButtonsEnumType.DeleteButton:
                    this.props.dispatchDeleteUpdatesThunk({
                        force: true,
                        dbTemplateUpdateId
                    });
                    break;
                default:
                    break;
            }
        }
        this.setState({
            isOpen: false
        });
    }

    private onClickDbTemplateUpdatesButton(dbTemplateUpdateId: string, buttonType: DbTemplateUpdatesButtonsEnumType) {
        let text = '';

        switch (buttonType) {
            case DbTemplateUpdatesButtonsEnumType.ApplyButton:
                text = 'принять';
                break;
            case DbTemplateUpdatesButtonsEnumType.RejectButton:
                text = 'отклонить';
                break;
            case DbTemplateUpdatesButtonsEnumType.DeleteButton:
                text = 'удалить';
                break;
            default:
                break;
        }

        this.setState({
            dbTemplateUpdateId,
            dialogMessage: `Вы действительно хотите ${ text } полученные обновления?`,
            dbTemplateUpdatesButtonType: buttonType,
            isOpen: true
        });
    }

    private async onProcessActionStateChanged(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId === DbTemplateUpdateProcessId.ApplyUpdates) {
            if (actionState.isInSuccessState) {
                this.successOrFaildStateNotification('Шаблон обновлен', true);
            }
            if (actionState.isInErrorState) {
                this.successOrFaildStateNotification(`Ошибка обновления шаблона: ${ error?.message }`, false);
            }
        }
        if (processId === DbTemplateUpdateProcessId.RejectUpdates) {
            if (actionState.isInSuccessState) {
                this.successOrFaildStateNotification('Обновление отклонено', true);
            }
            if (actionState.isInErrorState) {
                this.successOrFaildStateNotification(`Ошибка отклонения обновления шаблона: ${ error?.message }`, false);
            }
        }
        if (processId === DbTemplateUpdateProcessId.DeleteUpdates) {
            if (actionState.isInSuccessState) {
                this.successOrFaildStateNotification('Обновление удалено', true);
            }
            if (actionState.isInErrorState) {
                this.successOrFaildStateNotification(`Ошибка удаления шаблона: ${ error?.message }`, false);
            }
        }
    }

    private getDbTemplateUpdatesButtonsFormat(dbTemplateUpdateId: string, data: DbTemplateUpdatesDataModel) {
        return (
            <Box display="flex" flexDirection="column" gap="5px" marginY="3px">
                {
                    data.validateState === DbTemplateUpdateStateEnumType.New || data.validateState === DbTemplateUpdateStateEnumType.Rejected
                        ? (
                            <ContainedButton
                                onClick={ () => { this.onClickDbTemplateUpdatesButton(dbTemplateUpdateId, DbTemplateUpdatesButtonsEnumType.ApplyButton); } }
                                kind="primary"
                                size="small"
                                className={ css['button-actions'] }
                            >
                                Принять
                            </ContainedButton>
                        )
                        : null
                }
                {
                    data.validateState === DbTemplateUpdateStateEnumType.New
                        ? (
                            <ContainedButton
                                onClick={ () => { this.onClickDbTemplateUpdatesButton(dbTemplateUpdateId, DbTemplateUpdatesButtonsEnumType.RejectButton); } }
                                kind="warning"
                                size="small"
                                className={ css['button-actions'] }
                            >
                                Отклонить
                            </ContainedButton>
                        )
                        : null
                }
                {
                    data.validateState === DbTemplateUpdateStateEnumType.New || data.validateState === DbTemplateUpdateStateEnumType.Rejected
                        ? (
                            <ContainedButton
                                onClick={ () => { this.onClickDbTemplateUpdatesButton(dbTemplateUpdateId, DbTemplateUpdatesButtonsEnumType.DeleteButton); } }
                                kind="error"
                                size="small"
                                className={ css['button-actions'] }
                            >
                                Удалить
                            </ContainedButton>
                        )
                        : null
                }
            </Box>
        );
    }

    private closeDialog() {
        this.setState({
            isOpen: false
        });
    }

    private successOrFaildStateNotification(message: string, isSuccess: boolean) {
        if (isSuccess) {
            this.props.floatMessage.show(MessageType.Success, message, 3000);
            this._searchButtonRef.current?.click();
        } else {
            this.props.floatMessage.show(MessageType.Error, message, 3000);
        }
    }

    public render() {
        const { dataset } = this.props;

        return (
            <>
                <SearchDbTemplateUpdateView buttonRef={ this._searchButtonRef } />
                <CommonTableWithFilter
                    uniqueContextProviderStateId="DbTemplateUpdatesContextProviderStateId"
                    tableProps={ {
                        dataset,
                        keyFieldName: 'id',
                        fieldsView: {
                            defaultCaption: {
                                fieldContentNoWrap: false,
                                fieldWidth: 'auto',
                                caption: 'Шаблон'
                            },
                            updateVersion: {
                                fieldContentNoWrap: false,
                                fieldWidth: 'auto',
                                caption: 'Обновленная версия'
                            },
                            updateVersionDate: {
                                fieldContentNoWrap: false,
                                fieldWidth: 'auto',
                                caption: 'Дата обновлений',
                                format: (date: Date | null) => {
                                    return DateUtility.getDateByFormat(date, 'dd.MM.yyyy HH:mm:ss');
                                }
                            },
                            updateTemplatePath: {
                                fieldContentNoWrap: false,
                                fieldWidth: 'auto',
                                caption: 'Путь до обновленного шаблона'
                            },
                            id: {
                                fieldContentNoWrap: false,
                                fieldWidth: 'auto',
                                caption: '',
                                format: this.getDbTemplateUpdatesButtonsFormat
                            }
                        }
                    } }
                />
                <WatchReducerProcessActionState
                    watch={ [{
                        reducerStateName: 'DbTemplateUpdateState',
                        processIds: [
                            DbTemplateUpdateProcessId.ApplyUpdates,
                            DbTemplateUpdateProcessId.RejectUpdates,
                            DbTemplateUpdateProcessId.DeleteUpdates
                        ]
                    }] }
                    onProcessActionStateChanged={ this.onProcessActionStateChanged }
                />
                <Dialog
                    isOpen={ this.state.isOpen }
                    dialogWidth="sm"
                    dialogVerticalAlign="center"
                    onCancelClick={ this.closeDialog }
                    buttons={
                        [
                            {
                                content: 'Да',
                                kind: this.state.dbTemplateUpdatesButtonType === DbTemplateUpdatesButtonsEnumType.ApplyButton
                                    ? 'success'
                                    : 'error',
                                variant: 'contained',
                                onClick: this.onClickDialogButtonConfirm
                            },
                            {
                                content: 'Нет',
                                kind: 'primary',
                                variant: 'text',
                                onClick: this.closeDialog
                            }
                        ]
                    }
                >
                    <Box display="flex" flexDirection="column" alignItems="center" justifyContent="center">
                        <ErrorOutlineIcon color="warning" sx={ { width: '5em', height: '5em' } } />
                        { this.state.dialogMessage }
                    </Box>
                </Dialog>
            </>
        );
    }
}

export const DbTemplateUpdatesConnected = connect<StateProps, DispatchProps, {}, AppReduxStoreState>(
    state => {
        const dbTemplateUpdateState = state.DbTemplateUpdateState;
        const dataset = dbTemplateUpdateState.dbTemplateUpdates;

        return {
            dataset
        };
    },
    {
        dispatchGetDbTemplateUpdatesThunk: GetDbTemplateUpdatesThunk.invoke,
        dispatchApplyUpdatesThunk: ApplyUpdatesThunk.invoke,
        dispatchRejectUpdatesThunk: RejectUpdatesThunk.invoke,
        dispatchDeleteUpdatesThunk: DeleteUpdatesThunk.invoke
    }
)(DbTemplateUpdatesClass);

export const DbTemplateUpdatesView = withHeader({
    title: 'Обновленные файловые шаблоны',
    page: withFloatMessages(DbTemplateUpdatesConnected)
});