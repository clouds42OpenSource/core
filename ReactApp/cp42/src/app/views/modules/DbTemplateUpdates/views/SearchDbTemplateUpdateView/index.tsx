import { DbTemplateUpdateStateEnumType } from 'app/common/enums';
import { GetDbTemplateUpdatesParams } from 'app/modules/dbTemplateUpdate/store/reducers/getDbTemplateUpdatesReducer/params';
import { GetDbTemplatesThunk, GetDbTemplateUpdatesThunk } from 'app/modules/dbTemplateUpdate/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { TableFilterWrapper } from 'app/views/components/_hoc/tableFilterWrapper';
import { FilterItemWrapper } from 'app/views/components/_hoc/tableFilterWrapper/filterItemWrapper';
import { SearchByFilterButton } from 'app/views/components/controls/Button';
import { ComboBoxForm } from 'app/views/components/controls/forms';
import { ComboboxItemModel } from 'app/views/components/controls/forms/ComboBox/models';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';
import React, { Component } from 'react';
import { connect } from 'react-redux';

type OwnState = {
    /**
     * ID шаблона
     */
    searchTemplateId?: string,

    /**
     * Статус для обновленного шаблона инф. базы
     */
    searchDbTemplateUpdateState: DbTemplateUpdateStateEnumType
};

type OwnProps = {
    /**
     * Ref на кнопку поиска
     */
    buttonRef?: React.RefObject<HTMLButtonElement>;
};

type StateProps = {
    /**
     * Массив всех шаблонов баз для выпадающего листа поиска
     */
    dbTemplates: ComboboxItemModel<string>[];
};

type DispatchProps = {
    dispatchGetDbTemplatesThunk: (args: IForceThunkParam) => void;
    dispatchGetDbTemplateUpdatesThunk: (args: GetDbTemplateUpdatesParams) => void;
};

type AllProps = StateProps & DispatchProps & OwnProps;

class SearchDbTemplateUpdateClass extends Component<AllProps, OwnState> {
    public constructor(props: AllProps) {
        super(props);

        this.state = {
            searchTemplateId: undefined,
            searchDbTemplateUpdateState: DbTemplateUpdateStateEnumType.New
        };

        this.onValueChange = this.onValueChange.bind(this);
        this.onClickSearchButton = this.onClickSearchButton.bind(this);
    }

    public componentDidMount() {
        this.props.dispatchGetDbTemplatesThunk({
            force: true,
            showLoadingProgress: false
        });
    }

    private onClickSearchButton() {
        const { searchTemplateId, searchDbTemplateUpdateState } = this.state;

        this.props.dispatchGetDbTemplateUpdatesThunk({
            force: true,
            validateState: searchDbTemplateUpdateState,
            templateId: searchTemplateId
        });
    }

    private onValueChange(formName: string, newValue: string | DbTemplateUpdateStateEnumType) {
        if (formName === 'dbTemplates') {
            this.setState({
                searchTemplateId: newValue as string
            });
        }
        if (formName === 'dbTemplateUpdateState' && newValue) {
            this.setState({
                searchDbTemplateUpdateState: newValue as DbTemplateUpdateStateEnumType
            });
        }
    }

    private getDbTemplateUpdatesState() {
        const dbTemplateTypeText = ['Новое', 'Принятое', 'Отклоненное'];
        return Object.keys(DbTemplateUpdateStateEnumType).filter(key => !Number.isNaN(+key)).map<ComboboxItemModel<string>>(key => ({
            text: dbTemplateTypeText[Number(key)],
            value: key
        }));
    }

    public render() {
        return (
            <div style={ { paddingBottom: '16px' } }>
                <TableFilterWrapper>
                    <FilterItemWrapper width="370px">
                        <ComboBoxForm
                            items={ this.props.dbTemplates }
                            formName="dbTemplates"
                            label=""
                            value={ this.state.searchTemplateId ?? '1' }
                            onValueChange={ this.onValueChange }
                            allowAutoComplete={ true }
                        />
                    </FilterItemWrapper>
                    <FilterItemWrapper width="370px">
                        <ComboBoxForm
                            items={ this.getDbTemplateUpdatesState() }
                            formName="dbTemplateUpdateState"
                            label=""
                            value={ Number(this.state.searchDbTemplateUpdateState).toString() }
                            onValueChange={ this.onValueChange }
                        />
                    </FilterItemWrapper>
                    <SearchByFilterButton
                        onClick={ this.onClickSearchButton }
                        buttonRef={ this.props.buttonRef }
                    />
                </TableFilterWrapper>
            </div>
        );
    }
}

export const SearchDbTemplateUpdateView = connect<StateProps, DispatchProps, OwnProps, AppReduxStoreState>(
    state => {
        const dbTemplateUpdateState = state.DbTemplateUpdateState;
        const dbTemplates = dbTemplateUpdateState.dbTemplates.map<ComboboxItemModel<string>>(item =>
            ({
                text: item.value,
                value: item.key
            }));
        dbTemplates.unshift({ text: '', value: '' });

        return {
            dbTemplates
        };
    },
    {
        dispatchGetDbTemplatesThunk: GetDbTemplatesThunk.invoke,
        dispatchGetDbTemplateUpdatesThunk: GetDbTemplateUpdatesThunk.invoke
    }
)(SearchDbTemplateUpdateClass);