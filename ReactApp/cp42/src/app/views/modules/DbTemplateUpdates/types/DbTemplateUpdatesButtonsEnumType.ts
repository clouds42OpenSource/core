/**
 * Enum типа кнопки действия над обновлениями шаблона баз
 */
export enum DbTemplateUpdatesButtonsEnumType {
    /**
     * Тип кнопки принятия
     */
    ApplyButton,

    /**
     * Тип кнопки отклоненения
     */
    RejectButton,

    /**
     * Тип кнопки удаления
     */
    DeleteButton
}