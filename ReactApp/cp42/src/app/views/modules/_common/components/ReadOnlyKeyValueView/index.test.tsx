import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { ReadOnlyKeyValueView } from 'app/views/modules/_common/components/ReadOnlyKeyValueView';

describe('<ReadOnlyKeyValueView />', () => {
    it('renders', () => {
        render(<ReadOnlyKeyValueView label="test_label" text="test_text" />);
        expect(screen.getByText(/test_label/i)).toBeInTheDocument();
        expect(screen.getByText(/test_text/i)).toBeInTheDocument();
    });
});