import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import '@testing-library/jest-dom/extend-expect';
import { CommonPasswordFieldView } from 'app/views/modules/_common/components/ComponentForPasswordField';

describe('<CommonPasswordFieldView />', () => {
    const onValueChangeMock: jest.Mock = jest.fn();
    let container: Element;

    beforeEach(() => {
        container = render(<CommonPasswordFieldView
            formName="test_form_name"
            onValueChange={ onValueChangeMock }
        />).container;
    });

    it('renders', () => {
        expect(screen.getByTestId(/common-password-field-view/i)).toBeInTheDocument();
    });

    it('has right form name', () => {
        expect(container.querySelector('[name=test_form_name]')).toBeInTheDocument();
    });

    it('have onChangeValue method', async () => {
        const inputElement: HTMLInputElement | null = container.querySelector('[name=test_form_name]');
        await userEvent.click(inputElement!);
        await userEvent.type(inputElement!, 'new_test_value');
        expect(onValueChangeMock).toBeCalled();
    });
});