import React from 'react';
import AddCircleIcon from '@mui/icons-material/AddCircle';

import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import styles from './style.module.css';

type TImageGalleryUploadProps = FloatMessageProps & {
    imageLimit: number;
    callBack: (event: React.ChangeEvent<HTMLDivElement>) => Promise<boolean>;
    callBackDelete: (index: number) => void;
    isEditMode: boolean;
    images?: string[];
};

type TImageGalleryUploadState = {
    images: File[];
};

export class ImageGalleryUpload extends React.Component<TImageGalleryUploadProps, TImageGalleryUploadState> {
    private readonly inputReference;

    constructor(props: TImageGalleryUploadProps) {
        super(props);
        this.state = {
            images: [],
        };
        this.inputReference = React.createRef<HTMLInputElement>();
        this.renderImages = this.renderImages.bind(this);
        this.renderImageUploader = this.renderImageUploader.bind(this);
        this.onUpload = this.onUpload.bind(this);
        this.triggerInputChange = this.triggerInputChange.bind(this);
        this.onDelete = this.onDelete.bind(this);
    }

    private onUpload(ev: React.ChangeEvent<HTMLInputElement>) {
        try {
            const { files } = ev.target;

            if (files && files[0]) {
                const reader = new FileReader();
                reader.readAsDataURL(files[0]);

                reader.onload = e => {
                    const image = new Image();
                    image.src = e.target!.result as string;

                    image.onload = () => {
                        if (image.width <= 1920 && image.height <= 1080 && image.width >= 640 && image.height >= 480) {
                            this.props.callBack(ev).then(result => {
                                if (result) {
                                    this.setState(prevState => ({ images: [...prevState.images, files[0]] }));
                                }
                            });
                        } else {
                            this.props.floatMessage.show(MessageType.Error, 'Неподходящий размер изображения');
                        }
                    };
                };
            }
        } catch (e) {}
    }

    private onDelete(index: number, isUploadedNow: boolean) {
        const { images: propsImages } = this.props;

        if (isUploadedNow) {
            const { images } = this.state;
            images.splice(index, 1);
            this.setState({ images });
            this.props.callBackDelete((propsImages ? propsImages.length : 0) + index);
        } else {
            this.props.callBackDelete(index);
        }
    }

    private triggerInputChange() {
        const inputElementRef: HTMLInputElement | null = this.inputReference.current;

        if (inputElementRef) {
            inputElementRef.click();
        }
    }

    private renderImages(images: File[] | string[]) {
        const { isEditMode } = this.props;

        if (images.length > 0) {
            if (typeof images[0] === 'string') {
                return (images as string[]).map((image, index) =>
                    <img
                        key={ image }
                        alt={ image }
                        src={ image }
                        className={ isEditMode ? styles.serviceImageClick : styles.serviceImage }
                        onClick={ () => this.onDelete(index, false) }
                    />
                );
            }

            return (images as File[]).map((image, index) =>
                <img
                    key={ image.lastModified }
                    alt={ image.name }
                    src={ URL.createObjectURL(image) }
                    className={ isEditMode ? styles.serviceImageClick : styles.serviceImage }
                    onClick={ () => this.onDelete(index, true) }
                />
            );
        }

        return null;
    }

    private renderImageUploader(imagesCount: number) {
        const acceptedFileTypes = 'image/png, image/jpg, image/jpeg';

        if (imagesCount < this.props.imageLimit) {
            return (
                <div className={ styles.uploader } onClick={ this.triggerInputChange }>
                    <AddCircleIcon />
                    <input type="file" ref={ this.inputReference } accept={ acceptedFileTypes } hidden={ true } onChange={ this.onUpload } />
                </div>
            );
        }

        return null;
    }

    public render() {
        const { isEditMode, images, imageLimit } = this.props;
        const { images: stateImages } = this.state;
        const imageCount = stateImages.length + (images?.length ?? 0);

        if (isEditMode) {
            return (
                <div>
                    <div className={ styles.container }>
                        { images && this.renderImages(images) }
                        { this.renderImages(stateImages) }
                        { this.renderImageUploader(imageCount) }
                    </div>
                    <label style={ { fontSize: '12px' } }>
                        Лимит изображений: { imageLimit - imageCount }
                    </label>
                </div>
            );
        }

        if (images && images.length > 0) {
            return (
                <div className={ styles.container }>
                    { images?.map((image, index) => <img key={ index } alt={ image } src={ image } className={ styles.serviceImage } />) }
                </div>
            );
        }

        return <div className={ styles.serviceTemplate } />;
    }
}