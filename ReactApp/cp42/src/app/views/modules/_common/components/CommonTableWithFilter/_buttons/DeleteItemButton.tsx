import DeleteIcon from '@mui/icons-material/Delete';
import { hasComponentChangesFor } from 'app/common/functions';
import { ContainedButton, OutlinedButton } from 'app/views/components/controls/Button';

import { CommonTableWithFilterContext } from 'app/views/modules/_common/components/CommonTableWithFilter/context/CommonTableWithFilterContext';
import cn from 'classnames';
import React from 'react';
import css from '../styles.module.css';

type OwnProps<TItem> = {
    Item: TItem;
    uniqueFieldName: keyof TItem;
    uniqueContextProviderStateId: string;
};

export class DeleteItemButton<TItem> extends React.Component<OwnProps<TItem>> {
    public context!: React.ContextType<typeof CommonTableWithFilterContext>;

    public constructor(props: OwnProps<TItem>) {
        super(props);
        this.showDeleteConfirmDialogFor = this.showDeleteConfirmDialogFor.bind(this);
    }

    public shouldComponentUpdate(nextProps: OwnProps<TItem>) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private showDeleteConfirmDialogFor() {
        this.context.showDeleteItemConfirmDialog(this.props.uniqueContextProviderStateId, this.props.Item);
    }

    public render() {
        const { uniqueFieldName } = this.props;
        const selectedItem = this.context.getSelectedItem(this.props.uniqueContextProviderStateId);

        const ButtonView = this.context.getIsDeleteItemConfirmDialogOpen(this.props.uniqueContextProviderStateId) &&
        selectedItem[uniqueFieldName] === this.props.Item[uniqueFieldName]
            ? ContainedButton
            : OutlinedButton;

        return (
            <ButtonView kind="error" title="Удалить" className={ cn(css['delete-row-button']) } onClick={ this.showDeleteConfirmDialogFor }>
                <DeleteIcon />
            </ButtonView>
        );
    }
}

DeleteItemButton.contextType = CommonTableWithFilterContext;