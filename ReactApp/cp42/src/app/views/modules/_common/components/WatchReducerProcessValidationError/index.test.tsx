import { render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import configureStore, { MockStoreEnhanced } from 'redux-mock-store';
import { Provider } from 'react-redux';
import { WatchReducerProcessValidationError } from 'app/views/modules/_common/components/WatchReducerProcessValidationError';

describe('<WatchReducerProcessValidationError />', () => {
    const onProcessValidationErrorMock: jest.Mock = jest.fn();
    const mockStore = configureStore();
    let store: MockStoreEnhanced<unknown>;
    const initialState = {
        ReduxForms: {
            stateName: '123',
            reducerActions: {
                NONE: {
                    hasProcessActionStateChanged: true,
                    processActionState: {
                        isInErrorState: false,
                        isInSuccessState: true,
                        isInProgressState: false
                    },
                    process: {
                        error: undefined
                    }
                }
            }
        }
    } as const;

    beforeEach(() => {
        store = mockStore(initialState);
    });

    it('renders', () => {
        const { container } = render(
            <Provider store={ store }>
                <WatchReducerProcessValidationError
                    reducerStateName="ReduxForms"
                    whatchProcesses={ ['NONE', 'NONE'] }
                    onProcessValidationError={ onProcessValidationErrorMock }
                />
            </Provider>
        );
        expect(container.firstChild).toBeNull();
    });
});