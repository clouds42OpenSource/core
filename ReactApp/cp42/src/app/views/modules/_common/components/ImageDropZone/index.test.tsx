import '@testing-library/jest-dom/extend-expect';
import { render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { ImageDropZone } from 'app/views/modules/_common/components/ImageDropZone';

describe('<ImageDropZone />', () => {
    const onUploadedMock: jest.Mock = jest.fn();
    const customValidationMock: jest.Mock = jest.fn();
    global.URL.revokeObjectURL = jest.fn();
    global.URL.createObjectURL = jest.fn();
    let container: Element;

    beforeEach(() => {
        container = render(<ImageDropZone
            onUploaded={ onUploadedMock }
            customValidation={ customValidationMock }
            acceptTypes={ ['.png'] }
        />).container;
    });

    it('should accept file and triggers custom validation', async () => {
        const inputElement: HTMLInputElement | null = container.querySelector('[type=file]');
        const file = new File(['file_data'], 'test_image.png');
        await userEvent.upload(inputElement!, file);
        expect(onUploadedMock).toBeCalled();
        expect(customValidationMock).toBeCalled();
    });

    it('should not accept file', async () => {
        const inputElement: HTMLInputElement | null = container.querySelector('[type=file]');
        const file = new File(['file_data'], 'test_image.jpg');
        await userEvent.upload(inputElement!, file);
        expect(onUploadedMock).not.toBeCalled();
    });
});