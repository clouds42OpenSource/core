import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { CommonTableBasic } from 'app/views/modules/_common/components/CommonTableBasic';

describe('<CommonTableBasic />', () => {
    it('renders', () => {
        render(<CommonTableBasic dataset={ [{ caption: 'test_caption', value: 0, visible: true }] } />);
        expect(screen.getByTestId(/common-table-basic/i)).toBeInTheDocument();
    });

    it('renders with boolean value', () => {
        const { container } = render(<CommonTableBasic dataset={ [{ caption: 'test_caption', value: true, visible: true }] } />);
        expect(screen.getByText(/test_caption/i)).toBeInTheDocument();
        expect(container.querySelector('.fa-check-square-o')!).toBeInTheDocument();
    });
});