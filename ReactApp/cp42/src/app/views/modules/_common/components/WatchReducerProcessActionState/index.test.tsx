import { WatchReducerProcessActionState } from 'app/views/modules/_common/components/WatchReducerProcessActionState';
import { render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import configureStore, { MockStoreEnhanced } from 'redux-mock-store';
import { Provider } from 'react-redux';

describe('WatchReducerProcessActionState', () => {
    const onProcessActionStateChangedMock: jest.Mock = jest.fn();
    const mockStore = configureStore();
    let store: MockStoreEnhanced<unknown>;
    const initialState = {
        ReduxForms: {
            stateName: 'test_state_name',
            reducerActions: {
                NONE: {
                    hasProcessActionStateChanged: true,
                    processActionState: {
                        isInErrorState: false,
                        isInSuccessState: true,
                        isInProgressState: false
                    },
                    process: {
                        error: undefined
                    }
                }
            }
        }
    } as const;

    beforeEach(() => {
        store = mockStore(initialState);
    });

    it('renders', () => {
        render(
            <Provider store={ store }>
                <WatchReducerProcessActionState
                    onProcessActionStateChanged={ onProcessActionStateChangedMock }
                    watch={ [{
                        reducerStateName: 'ReduxForms',
                        processIds: ['NONE', 'NONE']
                    }] }
                />
            </Provider>
        );
    });
});