import { hasComponentChangesFor } from 'app/common/functions';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { CommonTableWithFilterContext } from 'app/views/modules/_common/components/CommonTableWithFilter/context/CommonTableWithFilterContext';
import { WatchReducerProcessActionState } from 'app/views/modules/_common/components/WatchReducerProcessActionState';
import { ValidationError } from 'app/web/request-submitter/common/RequestSender/ValidationError';
import { IReducerProcessActionInfo } from 'core/redux/interfaces';
import React from 'react';
import { ContextWatchState } from './ContextStateValue';

export type OwnProps = FloatMessageProps;

type OwnState = {
    hasContextChanged: boolean;
};

class CommonTableWithFilterContextProviderClass<TItem> extends React.Component<OwnProps, OwnState> {
    public constructor(props: OwnProps) {
        super(props);
        this.getSelectedItem = this.getSelectedItem.bind(this);

        this.getIsAddNewConfirmDialogOpen = this.getIsAddNewConfirmDialogOpen.bind(this);
        this.showAddNewConfirmDialog = this.showAddNewConfirmDialog.bind(this);
        this.hideAddNewConfirmDialog = this.hideAddNewConfirmDialog.bind(this);

        this.getIsModifyItemConfirmDialogOpen = this.getIsModifyItemConfirmDialogOpen.bind(this);
        this.showModifyConfirmDialogFor = this.showModifyConfirmDialogFor.bind(this);
        this.hideModifyConfirmDialog = this.hideModifyConfirmDialog.bind(this);

        this.getIsDeleteItemConfirmDialogOpen = this.getIsDeleteItemConfirmDialogOpen.bind(this);
        this.showDeleteConfirmDialogFor = this.showDeleteConfirmDialogFor.bind(this);
        this.hideDeleteConfirmDialog = this.hideDeleteConfirmDialog.bind(this);

        this.onProcessActionStateChanged = this.onProcessActionStateChanged.bind(this);

        this.onContextStateChanged = this.onContextStateChanged.bind(this);

        ContextWatchState.setContextStateChangedEvent(this.onContextStateChanged);

        this.state = {
            hasContextChanged: false
        };
    }

    public shouldComponentUpdate(nextProps: OwnProps, nextState: OwnState) {
        return hasComponentChangesFor(this.props, nextProps) ||
            hasComponentChangesFor(this.state, nextState);
    }

    public componentDidUpdate() {
        if (this.state.hasContextChanged) {
            this.setState({
                hasContextChanged: false
            });
        }
    }

    private onContextStateChanged() {
        this.setState({
            hasContextChanged: true
        });
    }

    private onProcessActionStateChanged(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        const uniqueContextProviderStateId = ContextWatchState.getUniqueContextProviderStateIdFromProcessId(processId);

        if (!uniqueContextProviderStateId) return;
        const processIds = ContextWatchState.getProcessIds(uniqueContextProviderStateId);

        const { addNewItemProcessId: insertProcessId, modifyItemProcessId: updateProcessId, deleteItemProcessId: deleteProcessId } = processIds;

        if (insertProcessId && processId === insertProcessId) {
            if (actionState.isInSuccessState) {
                const addMsgContent = ContextWatchState.getOnSuccessAddNewItemMessage(uniqueContextProviderStateId);
                this.props.floatMessage.show(MessageType.Success, addMsgContent);
                this.hideAddNewConfirmDialog(uniqueContextProviderStateId);
            }
        } else
            if (updateProcessId && processId === updateProcessId) {
                if (actionState.isInSuccessState) {
                    const modifySelectedItem = this.getSelectedItem(uniqueContextProviderStateId);
                    const modifyMsgContent = ContextWatchState.getOnSuccessModifyItemMessage(uniqueContextProviderStateId, modifySelectedItem);
                    this.props.floatMessage.show(MessageType.Success, modifyMsgContent);
                    this.hideModifyConfirmDialog(uniqueContextProviderStateId);
                }
            } else

                if (deleteProcessId && processId === deleteProcessId) {
                    if (actionState.isInSuccessState) {
                        const deleteSelectedItem = this.getSelectedItem(uniqueContextProviderStateId);
                        const deleteMsgContent = ContextWatchState.getOnSuccessDeleteItemMessage(uniqueContextProviderStateId, deleteSelectedItem);
                        this.props.floatMessage.show(MessageType.Success, deleteMsgContent);
                        this.hideDeleteConfirmDialog(uniqueContextProviderStateId);
                    }
                }
        if (actionState.isInErrorState) {
            if (error instanceof ValidationError) {
                const errorKeys = Object.keys(error.errors);
                const errors = errorKeys.map(errorKey => error.errors[errorKey]?.join(','));

                const message = (
                    <>
                        <div>Ошибка валидации:</div>
                        <ol> { errors.map(errorItem => <li key={ errorItem }>{ errorItem }</li>) } </ol>
                    </>
                );

                this.props.floatMessage.show(MessageType.Error, message);
            } else {
                this.props.floatMessage.show(MessageType.Error, error?.message);
            }
        }
    }

    private getSelectedItem(uniqueContextProviderStateId: string) {
        const contextState = ContextWatchState.getContextProviderState(uniqueContextProviderStateId);
        return contextState?.dialogState?.selectedItem;
    }

    private getIsAddNewConfirmDialogOpen(uniqueContextProviderStateId: string) {
        const contextState = ContextWatchState.getContextProviderState(uniqueContextProviderStateId);
        return contextState?.dialogState?.isAddNewConfirmDialogOpen === true;
    }

    private getIsModifyItemConfirmDialogOpen(uniqueContextProviderStateId: string) {
        const contextState = ContextWatchState.getContextProviderState(uniqueContextProviderStateId);
        return contextState?.dialogState?.isModifyItemConfirmDialogOpen === true;
    }

    private getIsDeleteItemConfirmDialogOpen(uniqueContextProviderStateId: string) {
        const contextState = ContextWatchState.getContextProviderState(uniqueContextProviderStateId);
        return contextState?.dialogState?.isDeleteItemConfirmDialogOpen === true;
    }

    private showAddNewConfirmDialog(uniqueContextProviderStateId: string, _item: TItem) {
        ContextWatchState.setIsAddNewConfirmDialogOpenState(uniqueContextProviderStateId, true);
    }

    private hideAddNewConfirmDialog(uniqueContextProviderStateId: string) {
        ContextWatchState.setIsAddNewConfirmDialogOpenState(uniqueContextProviderStateId, false);
    }

    private showModifyConfirmDialogFor(uniqueContextProviderStateId: string, item: TItem) {
        ContextWatchState.setIsModifyItemConfirmDialogOpenState(uniqueContextProviderStateId, true, item);
    }

    private hideModifyConfirmDialog(uniqueContextProviderStateId: string) {
        ContextWatchState.setIsModifyItemConfirmDialogOpenState(uniqueContextProviderStateId, false, null);
    }

    private showDeleteConfirmDialogFor(uniqueContextProviderStateId: string, item: TItem) {
        ContextWatchState.setIsDeleteItemConfirmDialogOpenState(uniqueContextProviderStateId, true, item);
    }

    private hideDeleteConfirmDialog(uniqueContextProviderStateId: string) {
        ContextWatchState.setIsDeleteItemConfirmDialogOpenState(uniqueContextProviderStateId, false, null);
    }

    public render() {
        return (
            <CommonTableWithFilterContext.Provider
                value={ {
                    getSelectedItem: this.getSelectedItem,

                    getIsAddNewConfirmDialogOpen: this.getIsAddNewConfirmDialogOpen,
                    showAddNewConfirmDialog: this.showAddNewConfirmDialog,
                    hideAddNewConfirmDialog: this.hideAddNewConfirmDialog,

                    getIsModifyItemConfirmDialogOpen: this.getIsModifyItemConfirmDialogOpen,
                    showModifyItemConfirmDialog: this.showModifyConfirmDialogFor,
                    hideModifyItemConfirmDialog: this.hideModifyConfirmDialog,

                    getIsDeleteItemConfirmDialogOpen: this.getIsDeleteItemConfirmDialogOpen,
                    showDeleteItemConfirmDialog: this.showDeleteConfirmDialogFor,
                    hideDeleteItemConfirmDialog: this.hideDeleteConfirmDialog
                } }
            >
                <WatchReducerProcessActionState
                    watch={ ContextWatchState.getWatchProcesses() }
                    onProcessActionStateChanged={ this.onProcessActionStateChanged }
                />
                { this.props.children }
            </CommonTableWithFilterContext.Provider>
        );
    }
}

export const CommonTableWithFilterContextProvider = withFloatMessages(CommonTableWithFilterContextProviderClass);