import { Box, ClickAwayListener, Fade, Popper } from '@mui/material';
import { SuccessButton } from 'app/views/components/controls/Button';
import { TooltipPlacementTypes } from 'app/views/components/controls/forms/Tooltips/types';
import React, { useState } from 'react';
import cn from 'classnames';
import style from './style.module.css';

type TProps = {
    children: React.ReactElement<any, any>;
    buttonText?: React.ReactNode;
    classNameButton?: string;
    isHoverOpen?: boolean;
    placement?: TooltipPlacementTypes;
};

export const PopperButton = ({ children, buttonText, classNameButton, isHoverOpen, placement }: TProps) => {
    const [isOpen, setIsOpen] = useState(false);
    const [anchorEl, setAnchorEl] = useState<HTMLElement | null>(null);

    const buttonHandler = (event: React.MouseEvent<HTMLElement>) => {
        if (anchorEl) {
            setAnchorEl(null);
            setIsOpen(false);
        } else {
            setAnchorEl(event.currentTarget);
            setIsOpen(true);
        }
    };

    const onClickAway = () => {
        setAnchorEl(null);
        setIsOpen(false);
    };

    return (
        <Box position="relative" onMouseLeave={ isHoverOpen ? onClickAway : undefined }>
            <SuccessButton
                onClick={ buttonHandler }
                className={ cn(style.button, classNameButton) }
                onMouseEnter={ isHoverOpen ? buttonHandler : undefined }
            >
                { buttonText ?? <i className="fa fa-bars" /> }
            </SuccessButton>
            <Popper
                placement={ placement ?? 'bottom-end' }
                open={ isOpen }
                anchorEl={ anchorEl }
                transition={ true }
                className={ style.popper }
            >
                { ({ TransitionProps }) => (
                    <ClickAwayListener onClickAway={ onClickAway }>
                        <Fade { ...TransitionProps }>
                            { children }
                        </Fade>
                    </ClickAwayListener>
                ) }
            </Popper>
        </Box>
    );
};