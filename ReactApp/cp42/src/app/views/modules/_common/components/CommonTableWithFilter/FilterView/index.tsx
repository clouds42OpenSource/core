import { Grid } from '@mui/material';
import { hasComponentChangesFor } from 'app/common/functions';
import { AddNewItemButton } from 'app/views/modules/_common/components/CommonTableWithFilter/_buttons/AddNewItemButton';
import cn from 'classnames';
import React from 'react';
import css from '../styles.module.css';

/**
 * Свойства для компонента фильтра
 */
type OwnProps = {
    addNewItemButtonCaption?: string;
    showAddNewItemCommand: boolean;
    uniqueContextProviderStateId: string;
};

export class FilterView extends React.Component<OwnProps> {
    public constructor(props: OwnProps) {
        super(props);
    }

    public shouldComponentUpdate(nextProps: OwnProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    public render() {
        return (
            this.props.children
                ? (
                    <div className={ cn(css['filter-view']) } data-testid="filter-view">
                        { this.props.children }
                        {
                            this.props.showAddNewItemCommand
                                ? (
                                    <Grid container={ true } spacing={ 2 }>
                                        <Grid item={ true } xs={ 12 }>
                                            <AddNewItemButton
                                                uniqueContextProviderStateId={ this.props.uniqueContextProviderStateId }
                                                buttonCaption={ this.props.addNewItemButtonCaption }
                                            />
                                        </Grid>
                                    </Grid>
                                )
                                : null
                        }
                    </div>
                )
                : null
        );
    }
}