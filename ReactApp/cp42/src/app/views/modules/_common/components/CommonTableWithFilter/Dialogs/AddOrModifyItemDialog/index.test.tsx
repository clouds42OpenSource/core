import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import userEvent from '@testing-library/user-event';
import { AddOrModifyItemDialog } from 'app/views/modules/_common/components/CommonTableWithFilter/Dialogs/AddOrModifyItemDialog';

describe('<AddOrModifyItemDialog />', () => {
    const onCancelClickMock: jest.Mock = jest.fn();
    const childrenMock: jest.Mock = jest.fn();

    beforeEach(() => {
        render(
            <AddOrModifyItemDialog
                isOpen={ true }
                selectedItem="test"
                onCancelClick={ onCancelClickMock }
            >
                {childrenMock}
            </AddOrModifyItemDialog>
        );
    });

    it('renders', () => {
        expect(screen.getByTestId(/dialog-title-view/i)).toBeInTheDocument();
    });

    it('triggers onCancelClick method', async () => {
        const buttonElement = screen.getByTitle(/Закрыть/i);
        await userEvent.click(buttonElement);
        expect(onCancelClickMock).toBeCalled();
    });
});