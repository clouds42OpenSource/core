import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { EPartnersTourId } from 'app/views/Layout/ProjectTour/enums';
import React from 'react';
import Cropper from 'react-cropper';
import 'cropperjs/dist/cropper.css';

import { SuccessButton } from 'app/views/components/controls/Button';
import { IMAGE_TEMPLATE } from 'app/views/modules/Services/constants/ImageTemplateConstants';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';

import styles from './style.module.css';

type TImageCropperProps = FloatMessageProps & {
    callBack: (file: File) => void;
    minHeightLimit: number;
    minWidthLimit: number;
    maxHeightLimit: number;
    maxWidthLimit: number;
    image?: string;
    isEditMode: boolean;
};

type TImageCropperState = {
    image: string;
    isCroppedImageSaved: boolean;
    cropperInstance: Cropper | null;
};

export class ImageCropper extends React.Component<TImageCropperProps, TImageCropperState> {
    public static defaultProps = {
        minWidthLimit: 350,
        minHeightLimit: 350,
        maxHeightLimit: 1080,
        maxWidthLimit: 1920,
        floatMessage: {
            show: () => void 0
        }
    };

    private readonly inputReference;

    constructor(props: TImageCropperProps) {
        super(props);
        this.state = {
            image: '',
            isCroppedImageSaved: false,
            cropperInstance: null,
        };
        this.inputReference = React.createRef<HTMLInputElement>();
        this.onChange = this.onChange.bind(this);
        this.setCropperInstance = this.setCropperInstance.bind(this);
        this.triggerInputChange = this.triggerInputChange.bind(this);
        this.saveCroppedImage = this.saveCroppedImage.bind(this);
        this.dataURItoBlob = this.dataURItoBlob.bind(this);
    }

    private async onChange(ev: React.ChangeEvent<HTMLInputElement>) {
        const { minWidthLimit, maxWidthLimit, minHeightLimit, maxHeightLimit, floatMessage: { show } } = this.props;

        try {
            const target = ev.target.files;

            if (target) {
                const reader = new FileReader();
                const image = new Image();

                image.onload = () => {
                    const { width, height } = image;

                    if (width >= minWidthLimit && width <= maxWidthLimit && height >= minHeightLimit && height <= maxHeightLimit) {
                        this.setState({ image: reader.result as string, isCroppedImageSaved: false });
                    } else {
                        show(MessageType.Error, 'Неподходящее разрешение изображения');
                    }
                };

                reader.onloadend = ended => {
                    image.src = ended.target?.result as string;
                };

                reader.readAsDataURL(target[0]);
            }
        } catch (err: unknown) {

        }
    }

    private setCropperInstance(instance: Cropper) {
        this.setState({ cropperInstance: instance });
    }

    private triggerInputChange() {
        const { cropperInstance } = this.state;
        const inputElement: HTMLInputElement | null = this.inputReference.current;

        if (inputElement && cropperInstance) {
            cropperInstance.enable();
            inputElement.click();
        }
    }

    private dataURItoBlob(dataURI: string): Blob {
        const byteString = atob(dataURI.split(',')[1]);
        const mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
        const ab = new ArrayBuffer(byteString.length);
        const ia = new Uint8Array(ab);

        for (let i = 0; i < byteString.length; i += 1) {
            ia[i] = byteString.charCodeAt(i);
        }

        return new Blob([ia], { type: mimeString });
    }

    private saveCroppedImage() {
        const { cropperInstance, image } = this.state;

        if (cropperInstance && image.length > 0) {
            this.props.callBack(new File([this.dataURItoBlob(cropperInstance.getCroppedCanvas().toDataURL())], 'logo'));
            this.setState({ isCroppedImageSaved: true });
            cropperInstance.disable();
        }
    }

    public render() {
        const { image: propsImage, isEditMode } = this.props;
        const { image, isCroppedImageSaved } = this.state;

        if (isEditMode) {
            return (
                <div className={ styles.container } data-tourid={ EPartnersTourId.createServiceLogo }>
                    <Cropper
                        className={ styles.cropper }
                        zoomTo={ 0.5 }
                        preview="[id=image-preview]"
                        src={ image !== '' ? image : propsImage }
                        viewMode={ 1 }
                        aspectRatio={ 1 }
                        minCropBoxHeight={ 40 }
                        minCropBoxWidth={ 40 }
                        autoCropArea={ 1 }
                        onInitialized={ this.setCropperInstance }
                        guides={ true }
                    />
                    <FormAndLabel label="Предпросмотр:">
                        <div className={ styles.imagePreviewContainer }>
                            <div className={ styles.imagePreview } id="image-preview" />
                            <SuccessButton onClick={ this.triggerInputChange }>Загрузить изображение</SuccessButton>
                            <SuccessButton isEnabled={ image.length > 0 && !isCroppedImageSaved } onClick={ this.saveCroppedImage }>Сохранить</SuccessButton>
                            <input type="file" accept="image/png, image/jpg, image/jpeg" hidden={ true } onChange={ this.onChange } ref={ this.inputReference } />
                        </div>
                    </FormAndLabel>
                </div>
            );
        }

        return <img src={ propsImage ?? IMAGE_TEMPLATE } className={ styles.cropper } alt="Логотип сервиса" />;
    }
}