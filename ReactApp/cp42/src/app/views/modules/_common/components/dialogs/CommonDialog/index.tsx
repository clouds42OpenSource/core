import { hasComponentChangesFor } from 'app/common/functions';
import { AppReduxStoreStateNames } from 'app/redux/types';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { IReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { BaseConfirmDialog, BaseConfirmDialogProps } from 'app/views/modules/_common/components/dialogs/BaseConfirmDialog';
import { WatchReducerProcessActionState } from 'app/views/modules/_common/components/WatchReducerProcessActionState';
import { IReducerProcessActionInfo } from 'core/redux/interfaces';
import { ProcessIdType } from 'core/redux/types';
import React from 'react';

type OwnProps<TDataModel> = FloatMessageProps & {
    dialogProps: BaseConfirmDialogProps<TDataModel>;
    watch?: {
        reducerStateName: AppReduxStoreStateNames;
        processId: ProcessIdType;
        getOnSuccessMessage?: (modifiedItem: TDataModel) => React.ReactNode;
        onSuccessHandler?: (modifiedItem: TDataModel) => void;
    };
    children: (ref: React.Ref<IReduxForm<TDataModel>>, dataModel?: TDataModel) => React.ReactNode;
};
class CommonDialogClass<TDataModel> extends React.Component<OwnProps<TDataModel>> {
    private _dialogContentViewFormRef = React.createRef<IReduxForm<TDataModel>>();

    public constructor(props: OwnProps<TDataModel>) {
        super(props);
        this.onProcessActionStateChanged = this.onProcessActionStateChanged.bind(this);
        this.getFormData = this.getFormData.bind(this);
    }

    public shouldComponentUpdate(nextProps: OwnProps<TDataModel>) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private onProcessActionStateChanged(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId !== this.props.watch!.processId) {
            return;
        }

        if (actionState.isInSuccessState) {
            const modifiedData = this.getFormData();
            const addMsgContent =
                this.props.watch!.getOnSuccessMessage
                    ? this.props.watch!.getOnSuccessMessage(modifiedData)
                    : 'Операция завершилась успешно.';

            this.props.floatMessage.show(MessageType.Success, addMsgContent);

            if (this.props.watch!.onSuccessHandler) {
                this.props.watch!.onSuccessHandler(modifiedData);
            }
        }

        if (actionState.isInErrorState) {
            this.props.floatMessage.show(MessageType.Error, error?.message);
        }
    }

    private getFormData(): TDataModel {
        return this._dialogContentViewFormRef.current!.getReduxForm().getReduxFormFields(false);
    }

    public render() {
        return (
            <>
                <BaseConfirmDialog
                    { ...this.props.dialogProps }
                    dialogContentViewFormRef={ this._dialogContentViewFormRef }
                >
                    { this.props.children }
                </BaseConfirmDialog>
                {
                    this.props.watch
                        ? (
                            <WatchReducerProcessActionState
                                watch={ [{
                                    processIds: [this.props.watch.processId],
                                    reducerStateName: this.props.watch.reducerStateName
                                }] }
                                onProcessActionStateChanged={ this.onProcessActionStateChanged }
                            />
                        )
                        : null
                }

            </>
        );
    }
}

export const CommonDialog = withFloatMessages(CommonDialogClass);