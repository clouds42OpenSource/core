import * as React from 'react';
import Stack from '@mui/material/Stack';
import Snackbar from '@mui/material/Snackbar';
import MuiAlert, { AlertProps } from '@mui/material/Alert';

const Alert = React.forwardRef<HTMLDivElement, AlertProps>((
    props,
    ref,
) => {
    return <MuiAlert elevation={ 6 } ref={ ref } variant="filled" { ...props } />;
});

type TNotification = {
  msg: string;
  severity?: 'error' | 'info' | 'success'
}

const Notification: React.FC<TNotification> = ({ msg, severity = 'info' }) => {
    const [open, setOpen] = React.useState(true);

    const handleClose = (event?: React.SyntheticEvent | Event, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpen(false);
    };

    return (
        <Stack spacing={ 2 } sx={ { width: '100%' } }>
            <Snackbar
                anchorOrigin={ {
                    vertical: 'top',
                    horizontal: 'center',
                } }
                open={ open }
                autoHideDuration={ 8000 }
                onClose={ handleClose }
            >
                <Alert onClose={ handleClose } sx={ { width: '100%' } } severity={ severity }>{msg}</Alert>
            </Snackbar>
        </Stack>
    );
};

export default Notification;