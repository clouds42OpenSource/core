import { AppReduxStoreStateNames } from 'app/redux/types';
import { ProcessIdType } from 'core/redux/types';
import React from 'react';

type AddModifyDeleteItemProcessIds = {
    addNewItemProcessId?: ProcessIdType;
    modifyItemProcessId?: ProcessIdType;
    deleteItemProcessId?: ProcessIdType;
};

type AddModifyDeleteItemActionMessages<TItem> = {
    getOnSuccessAddNewItemMessage?: () => React.ReactNode;
    getOnSuccessModifyItemMessage?: (itemToModify: TItem) => React.ReactNode;
    getOnSuccessDeleteItemMessage?: (itemToDelete: TItem) => React.ReactNode;
};

type DialogState<TItem> = {
    isAddNewConfirmDialogOpen: boolean;
    isModifyItemConfirmDialogOpen: boolean;
    isDeleteItemConfirmDialogOpen: boolean;
    selectedItem: TItem | null;
};

export type TContextStateItem<TItem> = {
    watch: {
        reducerStateName: AppReduxStoreStateNames;
        processIds: AddModifyDeleteItemProcessIds;
    };
    processMessages: AddModifyDeleteItemActionMessages<TItem>;
};

export type TProcessProps<TItem> = TContextStateItem<TItem>;

type TContextInternalState<TItem> = TContextStateItem<TItem> & {
    dialogState: Partial<DialogState<TItem>>
};

type TContextState = Partial<{
    [uniqueContextProviderStateId: string]: TContextInternalState<any>;
}>;

type TWatchProcess = Array<{
    processIds: Array<ProcessIdType>;
    reducerStateName: AppReduxStoreStateNames;
}>;

class ContextStateClass {
    private _state: TContextState = {};

    private _onStateChanged?: () => void;

    public constructor() {
        this.invokeStateChanged = this.invokeStateChanged.bind(this);

        this.getContextProviderState = this.getContextProviderState.bind(this);
        this.registerContextState = this.registerContextState.bind(this);
        this.unRegisterContextState = this.unRegisterContextState.bind(this);

        this.getOnSuccessAddNewItemMessage = this.getOnSuccessAddNewItemMessage.bind(this);
        this.getOnSuccessModifyItemMessage = this.getOnSuccessModifyItemMessage.bind(this);
        this.getOnSuccessDeleteItemMessage = this.getOnSuccessDeleteItemMessage.bind(this);

        this.setIsAddNewConfirmDialogOpenState = this.setIsAddNewConfirmDialogOpenState.bind(this);
        this.setIsModifyItemConfirmDialogOpenState = this.setIsModifyItemConfirmDialogOpenState.bind(this);
        this.setIsDeleteItemConfirmDialogOpenState = this.setIsDeleteItemConfirmDialogOpenState.bind(this);

        this.getProcessIds = this.getProcessIds.bind(this);
        this.getWatchProcesses = this.getWatchProcesses.bind(this);
        this.getUniqueContextProviderStateIdFromProcessId = this.getUniqueContextProviderStateIdFromProcessId.bind(this);
    }

    private invokeStateChanged() {
        if (this._onStateChanged) this._onStateChanged();
    }

    public registerContextState<TItem>(uniqueContextProviderStateId: string, state: TContextStateItem<TItem>) {
        if (this._state[uniqueContextProviderStateId]) {
            return;
        }
        this._state[uniqueContextProviderStateId] = {
            ...state,
            dialogState: {}
        };
        this.invokeStateChanged();
    }

    public unRegisterContextState(uniqueContextProviderStateId: string) {
        if (!this._state[uniqueContextProviderStateId]) {
            return;
        }
        this._state[uniqueContextProviderStateId] = void 0;
        this.invokeStateChanged();
    }

    public getContextProviderState(uniqueContextProviderStateId: string) {
        return this._state[uniqueContextProviderStateId];
    }

    public getOnSuccessAddNewItemMessage(uniqueContextProviderStateId: string) {
        const itemState = this.getContextProviderState(uniqueContextProviderStateId);

        if (!itemState || !itemState.processMessages?.getOnSuccessAddNewItemMessage) {
            return 'Добавление успешно завершено';
        }

        return itemState.processMessages.getOnSuccessAddNewItemMessage();
    }

    public getOnSuccessModifyItemMessage<TItem>(uniqueContextProviderStateId: string, itemToModify: TItem) {
        const itemState = this.getContextProviderState(uniqueContextProviderStateId);

        if (!itemState || !itemState.processMessages?.getOnSuccessModifyItemMessage) {
            return 'Редактирование успешно завершено';
        }

        return itemState.processMessages.getOnSuccessModifyItemMessage(itemToModify);
    }

    public getOnSuccessDeleteItemMessage<TItem>(uniqueContextProviderStateId: string, itemToDelete: TItem) {
        const itemState = this.getContextProviderState(uniqueContextProviderStateId);

        if (!itemState || !itemState.processMessages?.getOnSuccessDeleteItemMessage) {
            return 'Удаление успешно завершено';
        }

        return itemState.processMessages.getOnSuccessDeleteItemMessage(itemToDelete);
    }

    public getProcessIds(uniqueContextProviderStateId: string) {
        const itemState = this.getContextProviderState(uniqueContextProviderStateId);

        if (!itemState) { return {}; }

        return itemState.watch.processIds;
    }

    public getWatchProcesses(): TWatchProcess {
        const items: TWatchProcess = [];

        const uniqueContextProviderStateIds = Object.keys(this._state);
        for (const uniqueContextProviderStateId of uniqueContextProviderStateIds) {
            const itemState = this._state[uniqueContextProviderStateId];

            if (!itemState) { continue; }

            const itemStateProcessIds = Object.values(itemState.watch.processIds) as string[];

            items.push({
                reducerStateName: itemState.watch.reducerStateName,
                processIds: itemStateProcessIds
            });
        }

        return items;
    }

    public getUniqueContextProviderStateIdFromProcessId(processId: ProcessIdType) {
        const uniqueContextProviderStateIds = Object.keys(this._state);
        for (const uniqueContextProviderStateId of uniqueContextProviderStateIds) {
            const itemState = this._state[uniqueContextProviderStateId];

            if (!itemState) { continue; }

            const itemStateProcessIds = Object.values(itemState.watch.processIds);

            if (itemStateProcessIds.indexOf(processId) >= 0) {
                return uniqueContextProviderStateId;
            }
        }
    }

    public setIsAddNewConfirmDialogOpenState(uniqueContextProviderStateId: string, isOpen: boolean) {
        const itemState = this.getContextProviderState(uniqueContextProviderStateId);

        if (!itemState) { return; }

        itemState.dialogState.isAddNewConfirmDialogOpen = isOpen;
        itemState.dialogState.selectedItem = null;

        this.invokeStateChanged();
    }

    public setIsModifyItemConfirmDialogOpenState<TItem>(uniqueContextProviderStateId: string, isOpen: boolean, selectedItem: TItem) {
        const itemState = this.getContextProviderState(uniqueContextProviderStateId);

        if (!itemState) { return; }

        itemState.dialogState.isModifyItemConfirmDialogOpen = isOpen;
        itemState.dialogState.selectedItem = selectedItem;

        this.invokeStateChanged();
    }

    public setIsDeleteItemConfirmDialogOpenState<TItem>(uniqueContextProviderStateId: string, isOpen: boolean, selectedItem: TItem) {
        const itemState = this.getContextProviderState(uniqueContextProviderStateId);

        if (!itemState) { return; }

        itemState.dialogState.isDeleteItemConfirmDialogOpen = isOpen;
        itemState.dialogState.selectedItem = selectedItem;

        this.invokeStateChanged();
    }

    public setContextStateChangedEvent(handler: () => void) {
        this._onStateChanged = handler;
    }
}

const ContextWatchState = new ContextStateClass();

export { ContextWatchState };