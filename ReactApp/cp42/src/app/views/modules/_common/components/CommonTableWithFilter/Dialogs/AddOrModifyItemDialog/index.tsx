import { hasComponentChangesFor } from 'app/common/functions';
import { IReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { DialogVerticalAlign, DialogWidth } from 'app/views/components/controls/Dialog/types';
import { TextFontSize, TextFontWeight, TextHorizontalAlign } from 'app/views/components/TextOut/views/TextView';
import { BaseConfirmDialog } from 'app/views/modules/_common/components/dialogs/BaseConfirmDialog';
import React from 'react';

type OwnProps<TItem> = {
    isOpen: boolean;
    selectedItem: TItem;
    dialogTitle?: string;

    dialogTitleFontSize?: TextFontSize;
    dialogTitleFontWeight?: TextFontWeight;
    dialogTitleTextAlign?: TextHorizontalAlign;
    isDialogTitleSmall?: boolean;

    successButtonText?: string;
    dialogWidth?: DialogWidth;
    dialogVerticalAlign?: DialogVerticalAlign;

    onCancelClick: () => void;
    cancelButtonText?: string;
    onSuccessClick?: (formData: TItem) => void;
    children: (ref: React.Ref<IReduxForm<TItem>>, item: TItem) => React.ReactNode;
};
export class AddOrModifyItemDialog<TItem> extends React.Component<OwnProps<TItem>> {
    private addOrModifyContentViewFormRef = React.createRef<IReduxForm<TItem>>();

    public constructor(props: OwnProps<TItem>) {
        super(props);
        this.getFormData = this.getFormData.bind(this);
    }

    public shouldComponentUpdate(nextProps: OwnProps<TItem>) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private getFormData(): TItem {
        return this.addOrModifyContentViewFormRef.current!.getReduxForm().getReduxFormFields(false);
    }

    public render() {
        return (
            <BaseConfirmDialog<TItem>
                data-testid="add-or-modify-item-dialog"
                dataModel={ this.props.selectedItem }
                isOpen={ this.props.isOpen }
                dialogVerticalAlign={ this.props.dialogVerticalAlign }
                dialogWidth={ this.props.dialogWidth }
                isDialogTitleSmall={ this.props.isDialogTitleSmall }
                dialogTitleTextAlign={ this.props.dialogTitleTextAlign }
                dialogTitleFontSize={ this.props.dialogTitleFontSize }
                dialogTitleFontWeight={ this.props.dialogTitleFontWeight }
                dialogTitle={ this.props.dialogTitle }
                onCancelClick={ this.props.onCancelClick }
                cancelButtonText={ this.props.cancelButtonText }
                confirmButtonText={ this.props.successButtonText }
                onConfirmClick={ this.props.onSuccessClick }
                dialogContentViewFormRef={ this.addOrModifyContentViewFormRef }
            >
                { this.props.children }
            </BaseConfirmDialog>
        );
    }
}