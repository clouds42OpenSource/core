import { COLORS } from 'app/utils';
import { TextOut } from 'app/views/components/TextOut';
import { ShowErrorMessageProps } from 'app/views/modules/_common/components/ShowErrorMessage/types';
import { PureComponent } from 'react';

/**
 * Для показа сообщения об ошибке
 */
export class ShowErrorMessage extends PureComponent<ShowErrorMessageProps> {
    public render() {
        const { errors, formName } = this.props;

        if (errors[formName]) {
            return <TextOut fontSize={ 10 } data-testid="show-error-message" style={ { color: COLORS.error, lineHeight: 'initial' } }>{ errors[formName] }</TextOut>;
        }

        return null;
    }
}