import { hasComponentChangesFor } from 'app/common/functions';
import { TextButton } from 'app/views/components/controls/Button';
import { DialogMessage } from 'app/views/components/controls/DialogMessage';
import { TextOut } from 'app/views/components/TextOut';
import { DeleteItemConfirmButton } from 'app/views/modules/_common/components/CommonTableWithFilter/Dialogs/DeleteItemDialogMessage/_buttons/DeleteItemConfirmButton';
import React from 'react';

type OwnProps<TItem> = {
    isOpen: boolean;
    selectedItem: TItem;
    dialogTitle: string;
    deleteButtonText: string;
    onNoClick: () => void;
    onYesClick: (itemToDelete: TItem) => void;
};

export class DeleteItemDialogMessage<TItem> extends React.Component<OwnProps<TItem>> {
    public constructor(props: OwnProps<TItem>) {
        super(props);
        this.getDialogButtons = this.getDialogButtons.bind(this);
    }

    public shouldComponentUpdate(nextProps: OwnProps<TItem>) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private getDialogButtons() {
        return (
            <>
                <DeleteItemConfirmButton
                    caption={ this.props.deleteButtonText }
                    itemToDelete={ this.props.selectedItem }
                    onDeleteConfirmed={ this.props.onYesClick }
                />
                <TextButton kind="default" onClick={ this.props.onNoClick } autoFocus={ true }>
                    <TextOut fontSize={ 12 } fontWeight={ 400 } inheritColor={ true }>
                        { 'Отмена'.toUpperCase() }
                    </TextOut>
                </TextButton>
            </>
        );
    }

    public render() {
        const dialogTitle = this.props.dialogTitle ?? 'Удаление';
        return (
            <DialogMessage
                data-testid="delete-item-dialog-message"
                isOpen={ this.props.isOpen }
                dialogWidth="sm"
                isTitleSmall={ true }
                titleTextAlign="left"
                titleFontSize={ 14 }
                title={ dialogTitle }
                onCancelClick={ this.props.onNoClick }
                buttons={ this.getDialogButtons }
            >
                <TextOut fontSize={ 13 }>
                    { this.props.children }
                </TextOut>
            </DialogMessage>
        );
    }
}