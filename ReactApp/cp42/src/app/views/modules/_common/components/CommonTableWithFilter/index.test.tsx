import { cleanup, render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';

describe('<CommonTableWithFilter />', () => {
    const onDataSelectMock: jest.Mock = jest.fn();

    it('renders without data', () => {
        render(<CommonTableWithFilter
            uniqueContextProviderStateId="test_context"
            tableProps={ { keyFieldName: '', dataset: [], fieldsView: null as never } }
            onDataSelect={ onDataSelectMock }
        />);
        expect(screen.getByText(/нет записей/i)).toBeInTheDocument();
    });

    it('renders with data', () => {
        render(<CommonTableWithFilter
            uniqueContextProviderStateId="test_context"
            tableProps={ {
                keyFieldName: 'id',
                dataset: [
                    { id: 1, value: 'test_value_1' },
                    { id: 2, value: 'test_value_2' },
                ],
                fieldsView: {
                    value: {
                        format: (value, data) =>
                            <div>
                                <div>{ data.id }</div>
                                <div>{ data.value }</div>
                            </div>
                    }
                }
            } }
            onDataSelect={ onDataSelectMock }
        />);
        expect(screen.queryByText(/нет записей/i)).not.toBeInTheDocument();
        expect(screen.getByText(/test_value_1/i)).toBeInTheDocument();
    });

    it('renders filterProps method', () => {
        render(<CommonTableWithFilter
            uniqueContextProviderStateId="test_context"
            filterProps={ { getFilterContentView: () => <div>test_filter_prop</div> } }
            tableProps={ {
                keyFieldName: 'id',
                dataset: [
                    { id: 1, value: 'test_value_1' },
                    { id: 2, value: 'test_value_2' },
                ],
                fieldsView: {
                    value: {
                        format: (value, data) =>
                            <div>
                                <div>{ data.id }</div>
                                <div>{ data.value }</div>
                            </div>
                    }
                }
            } }
            onDataSelect={ onDataSelectMock }
        />);
        expect(screen.getByText(/test_filter_prop/i)).toBeInTheDocument();
    });

    afterEach(cleanup);
});