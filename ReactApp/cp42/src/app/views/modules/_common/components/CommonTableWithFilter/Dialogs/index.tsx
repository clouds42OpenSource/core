import { hasComponentChangesFor } from 'app/common/functions';
import { IReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { DialogVerticalAlign, DialogWidth } from 'app/views/components/controls/Dialog/types';
import { TextFontSize, TextFontWeight, TextHorizontalAlign } from 'app/views/components/TextOut/views/TextView';
import { CommonTableWithFilterContext } from 'app/views/modules/_common/components/CommonTableWithFilter/context/CommonTableWithFilterContext';
import { AddOrModifyItemDialog } from 'app/views/modules/_common/components/CommonTableWithFilter/Dialogs/AddOrModifyItemDialog';
import { DeleteItemDialogMessage } from 'app/views/modules/_common/components/CommonTableWithFilter/Dialogs/DeleteItemDialogMessage';
import React from 'react';

export type CommonTableWithFilterViewDialogsProps<TItem> = {
    deleteDialog?: {
        getDialogTitle: (itemToDelete: TItem) => string;
        onDeleteItemConfirmed: (itemToDelete: TItem) => void;
        getDialogContentView: (itemToDelete: TItem) => React.ReactNode;
        confirmDeleteItemButtonText: string;
    },
    addDialog?: {
        getDialogTitle: (itemToAdd: TItem) => string;
        onAddNewItemConfirmed: (itemToAdd: TItem) => void;
        getDialogContentFormView: (ref: React.Ref<IReduxForm<TItem>>) => React.ReactNode;
        confirmAddItemButtonText: string;
        showAddDialogButtonText: string;
        dialogWidth?: DialogWidth;
        dialogVerticalAlign?: DialogVerticalAlign;
        dialogTitleFontSize?: TextFontSize;
        dialogTitleFontWeight?: TextFontWeight;
        dialogTitleTextAlign?: TextHorizontalAlign;
        isDialogTitleSmall?: boolean;
        cancelButtonText?: string;
    },
    modifyDialog?: {
        getDialogTitle: (itemToModify: TItem) => string;
        onModifyItemConfirmed?: (itemToModify: TItem) => void;
        getDialogContentFormView: (ref: React.Ref<IReduxForm<TItem>>, item: TItem) => React.ReactNode;
        confirmModifyItemButtonText: string;
        dialogWidth?: DialogWidth;
        dialogVerticalAlign?: DialogVerticalAlign;
        dialogTitleFontSize?: TextFontSize;
        dialogTitleFontWeight?: TextFontWeight;
        dialogTitleTextAlign?: TextHorizontalAlign;
        isDialogTitleSmall?: boolean;
        cancelButtonText?: string;
    }
};

type OwnProps<TItem> = CommonTableWithFilterViewDialogsProps<TItem> & {
    uniqueContextProviderStateId: string;
};

export class Dialogs<TItem> extends React.Component<OwnProps<TItem>> {
    public context!: React.ContextType<typeof CommonTableWithFilterContext>;

    public constructor(props: OwnProps<TItem>) {
        super(props);

        this.generateDeleteItemDialog = this.generateDeleteItemDialog.bind(this);
        this.hideDeleteItemConfirmDialog = this.hideDeleteItemConfirmDialog.bind(this);

        this.generateAddItemDialog = this.generateAddItemDialog.bind(this);
        this.hideAddNewConfirmDialog = this.hideAddNewConfirmDialog.bind(this);

        this.generateModifyItemDialog = this.generateModifyItemDialog.bind(this);
        this.hideModifyItemConfirmDialog = this.hideModifyItemConfirmDialog.bind(this);
    }

    public shouldComponentUpdate(nextProps: OwnProps<TItem>) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private generateDeleteItemDialog() {
        if (!this.props.deleteDialog) {
            return null;
        }

        const { uniqueContextProviderStateId } = this.props;
        const selectedItem = this.context.getSelectedItem(uniqueContextProviderStateId);
        const isDialogOpen = this.context.getIsDeleteItemConfirmDialogOpen(uniqueContextProviderStateId);
        const dialogTitle = isDialogOpen ? this.props.deleteDialog.getDialogTitle(selectedItem) : '';
        const dialogContent = isDialogOpen ? this.props.deleteDialog.getDialogContentView(selectedItem) : null;

        return (
            <DeleteItemDialogMessage
                selectedItem={ selectedItem }
                isOpen={ isDialogOpen }
                onNoClick={ this.hideDeleteItemConfirmDialog }
                dialogTitle={ dialogTitle }
                onYesClick={ this.props.deleteDialog.onDeleteItemConfirmed }
                deleteButtonText={ this.props.deleteDialog.confirmDeleteItemButtonText }
            >
                { dialogContent }
            </DeleteItemDialogMessage>
        );
    }

    private hideDeleteItemConfirmDialog() {
        this.context.hideDeleteItemConfirmDialog(this.props.uniqueContextProviderStateId);
    }

    private generateAddItemDialog() {
        if (!this.props.addDialog) {
            return null;
        }
        const { uniqueContextProviderStateId } = this.props;
        const selectedItem = this.context.getSelectedItem(uniqueContextProviderStateId);
        const isDialogOpen = this.context.getIsAddNewConfirmDialogOpen(uniqueContextProviderStateId);
        const dialogTitle = isDialogOpen ? this.props.addDialog.getDialogTitle(selectedItem) : '';
        const dialogContent = this.props.addDialog.getDialogContentFormView;

        return (
            <AddOrModifyItemDialog
                selectedItem={ selectedItem }
                isOpen={ isDialogOpen }
                onCancelClick={ this.hideAddNewConfirmDialog }
                dialogTitle={ dialogTitle }
                onSuccessClick={ this.props.addDialog.onAddNewItemConfirmed }
                successButtonText={ this.props.addDialog.confirmAddItemButtonText }
                dialogWidth={ this.props.addDialog.dialogWidth }
                dialogVerticalAlign={ this.props.addDialog.dialogVerticalAlign }
                dialogTitleFontSize={ this.props.addDialog.dialogTitleFontSize }
                dialogTitleFontWeight={ this.props.addDialog.dialogTitleFontWeight }
                dialogTitleTextAlign={ this.props.addDialog.dialogTitleTextAlign }
                isDialogTitleSmall={ this.props.addDialog.isDialogTitleSmall }
                cancelButtonText={ this.props.addDialog.cancelButtonText }
            >
                { dialogContent }
            </AddOrModifyItemDialog>
        );
    }

    private hideAddNewConfirmDialog() {
        this.context.hideAddNewConfirmDialog(this.props.uniqueContextProviderStateId);
    }

    private generateModifyItemDialog() {
        if (!this.props.modifyDialog) {
            return null;
        }
        const { uniqueContextProviderStateId } = this.props;
        const selectedItem = this.context.getSelectedItem(uniqueContextProviderStateId);
        const isDialogOpen = this.context.getIsModifyItemConfirmDialogOpen(uniqueContextProviderStateId);
        const dialogTitle = isDialogOpen ? this.props.modifyDialog.getDialogTitle(selectedItem) : '';
        const dialogContent = this.props.modifyDialog.getDialogContentFormView;

        return (
            <AddOrModifyItemDialog
                selectedItem={ selectedItem }
                isOpen={ isDialogOpen }
                onCancelClick={ this.hideModifyItemConfirmDialog }
                dialogTitle={ dialogTitle }
                onSuccessClick={ this.props.modifyDialog.onModifyItemConfirmed }
                successButtonText={ this.props.modifyDialog.confirmModifyItemButtonText }
                dialogWidth={ this.props.modifyDialog.dialogWidth }
                dialogVerticalAlign={ this.props.modifyDialog.dialogVerticalAlign }
                dialogTitleFontSize={ this.props.modifyDialog.dialogTitleFontSize }
                dialogTitleFontWeight={ this.props.modifyDialog.dialogTitleFontWeight }
                dialogTitleTextAlign={ this.props.modifyDialog.dialogTitleTextAlign }
                isDialogTitleSmall={ this.props.modifyDialog.isDialogTitleSmall }
                cancelButtonText={ this.props.modifyDialog.cancelButtonText }
            >
                { dialogContent }
            </AddOrModifyItemDialog>
        );
    }

    private hideModifyItemConfirmDialog() {
        this.context.hideModifyItemConfirmDialog(this.props.uniqueContextProviderStateId);
    }

    public render() {
        return (
            <>
                { this.generateDeleteItemDialog() }
                { this.generateAddItemDialog() }
                { this.generateModifyItemDialog() }
            </>
        );
    }
}

Dialogs.contextType = CommonTableWithFilterContext;