import ContentCopyIcon from '@mui/icons-material/ContentCopy';
import { IconButton, InputAdornment, OutlinedInput } from '@mui/material';
import { EMessageType, useFloatMessages } from 'app/hooks';

type TProps = {
    value: string;
    successMessage?: string;
};

export const CopyTextInput = ({ value, successMessage }: TProps) => {
    const { show } = useFloatMessages();

    const copy = () => {
        void navigator.clipboard.writeText(value);

        show(EMessageType.success, successMessage ?? 'Текст скопирован!');
    };

    return (
        <OutlinedInput
            value={ value }
            fullWidth={ true }
            endAdornment={
                <InputAdornment position="end">
                    <IconButton onClick={ copy }>
                        <ContentCopyIcon style={ { fontSize: '14px' } } />
                    </IconButton>
                </InputAdornment>
            }
            sx={ { '& fieldset': { border: 'none' } } }
        />
    );
};