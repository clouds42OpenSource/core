declare const styles: {
    readonly 'dropZone-block-text-style': string;
    readonly 'dropZone-block-text': string;
    readonly 'dropZone-block-text-hover': string;
    readonly 'dropZone-narrow-file': string;
    readonly 'advertising-banner-display-hover': string;
    readonly 'refreshIcon': string;
    readonly 'img': string;
};
export = styles;