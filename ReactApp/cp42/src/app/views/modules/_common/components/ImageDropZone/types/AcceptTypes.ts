/**
 * Доступные типы для image drop zone
 */
export type AcceptTypes = '.jpeg' | '.jpg' | '.png' | '.gif';