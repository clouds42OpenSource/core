declare const styles: {
    readonly 'input-group-addon-eye': string;
    readonly 'text-box': string;
};

export = styles;