import { hasComponentChangesFor } from 'app/common/functions';
import { ContainedButton } from 'app/views/components/controls/Button';
import { ButtonKind } from 'app/views/components/controls/Button/types';
import { TextOut } from 'app/views/components/TextOut';
import React from 'react';

type OwnProps = {
    onClick?: () => void;
    kind?: ButtonKind;
    caption?: string;
    icon?: string;
    confirmButtonDisabled?: boolean;
};

export class ConfirmButton extends React.Component<OwnProps> {
    public constructor(props: OwnProps) {
        super(props);
    }

    public shouldComponentUpdate(nextProps: OwnProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    public render() {
        const caption = this.props.caption ?? 'ОК';
        const kind = this.props.kind ?? 'success';
        return (
            <ContainedButton
                kind={ kind }
                isEnabled={ this.props.confirmButtonDisabled }
                onClick={ this.props.onClick }
            >
                <TextOut fontSize={ 12 } fontWeight={ 400 } inheritColor={ true }>
                    { this.props.icon && <><i className={ this.props.icon } />&nbsp;</> }
                    { caption.toUpperCase() }
                </TextOut>
            </ContainedButton>
        );
    }
}