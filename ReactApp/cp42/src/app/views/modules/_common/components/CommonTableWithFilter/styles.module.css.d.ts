declare const styles: {
    readonly 'filter-view': string;
    readonly 'delete-row-button': string;
    readonly 'modify-row-button': string;
    readonly 'add-button': string;
};
export = styles;