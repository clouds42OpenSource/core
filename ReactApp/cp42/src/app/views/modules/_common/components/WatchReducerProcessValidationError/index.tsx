import { AppReduxStoreStateNames } from 'app/redux/types';
import { WatchReducerProcessActionState } from 'app/views/modules/_common/components/WatchReducerProcessActionState';
import { ValidationError } from 'app/web/request-submitter/common/RequestSender/ValidationError';
import { IReducerProcessActionInfo } from 'core/redux/interfaces';
import { ProcessIdType } from 'core/redux/types';
import React from 'react';

type OwnProps = {
    reducerStateName: AppReduxStoreStateNames;
    whatchProcesses: ProcessIdType[];
    onProcessValidationError: (processId: ProcessIdType, error?: ValidationError) => void;
};

export class WatchReducerProcessValidationError extends React.Component<OwnProps> {
    public constructor(props: OwnProps) {
        super(props);
        this.onProcessActionStateChanged = this.onProcessActionStateChanged.bind(this);
    }

    private onProcessActionStateChanged(processId: ProcessIdType, actionState: IReducerProcessActionInfo, error?: Error) {
        if (!this.props.onProcessValidationError) { return; }
        if (actionState.isInErrorState) {
            if (error instanceof ValidationError) {
                this.props.onProcessValidationError(processId, error);
            }
        } else if (actionState.isInSuccessState) {
            this.props.onProcessValidationError(processId, undefined);
        }
    }

    public render() {
        return (
            <WatchReducerProcessActionState
                watch={ [{
                    reducerStateName: this.props.reducerStateName,
                    processIds: this.props.whatchProcesses
                }] }
                onProcessActionStateChanged={ this.onProcessActionStateChanged }
            />
        );
    }
}