import { hasComponentChangesFor } from 'app/common/functions';
import { IReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { TextButton } from 'app/views/components/controls/Button';
import { ButtonKind } from 'app/views/components/controls/Button/types';
import { Dialog } from 'app/views/components/controls/Dialog';
import { DialogVerticalAlign, DialogWidth } from 'app/views/components/controls/Dialog/types';
import { TextOut } from 'app/views/components/TextOut';
import { TextFontSize, TextFontWeight, TextHorizontalAlign } from 'app/views/components/TextOut/views/TextView';
import { ConfirmButton } from 'app/views/modules/_common/components/dialogs/BaseConfirmDialog/_buttons/ConfirmButton';
import React from 'react';

export type BaseConfirmDialogProps<TDataModel> = {

    dataModel: TDataModel;
    isOpen: boolean;

    dialogWidth?: DialogWidth;
    dialogVerticalAlign?: DialogVerticalAlign;

    dialogTitle?: string;
    dialogTitleFontSize?: TextFontSize;
    dialogTitleFontWeight?: TextFontWeight;
    dialogTitleTextAlign?: TextHorizontalAlign;
    isDialogTitleSmall?: boolean;

    confirmButtonText?: string;
    confirmButtonKind?: ButtonKind;

    cancelButtonText?: string;

    onCancelClick: () => void;
    onConfirmClick?: (formData: TDataModel) => void;
};

type OwnProps<TDataModel> = BaseConfirmDialogProps<TDataModel> & {
    children: (ref: React.Ref<IReduxForm<TDataModel>>, dataModel: TDataModel) => React.ReactNode;
    dialogContentViewFormRef: React.RefObject<IReduxForm<TDataModel>>;
};
export class BaseConfirmDialog<TDataModel> extends React.Component<OwnProps<TDataModel>> {
    public constructor(props: OwnProps<TDataModel>) {
        super(props);
        this.getFormData = this.getFormData.bind(this);
        this.getDialogButtons = this.getDialogButtons.bind(this);
    }

    public shouldComponentUpdate(nextProps: OwnProps<TDataModel>) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private getDialogButtons() {
        return (
            <>
                <TextButton kind="default" onClick={ this.props.onCancelClick }>
                    <TextOut fontSize={ 12 } fontWeight={ 400 } inheritColor={ true }>
                        { (this.props.cancelButtonText ?? 'Отмена').toUpperCase() }
                    </TextOut>
                </TextButton>
                {
                    this.props.onConfirmClick
                        ? (
                            <ConfirmButton
                                getFormData={ this.getFormData }
                                caption={ this.props.confirmButtonText }
                                onClick={ this.props.onConfirmClick }
                                kind={ this.props.confirmButtonKind }
                            />
                        )
                        : null
                }
            </>
        );
    }

    private getFormData(): TDataModel {
        return this.props.dialogContentViewFormRef.current!.getReduxForm().getReduxFormFields(false);
    }

    public render() {
        const dialogWidth = this.props.dialogWidth ?? 'sm';
        const dialogVerticalAlign = this.props.dialogVerticalAlign ?? 'top';
        const isDialogTitleSmall = this.props.isDialogTitleSmall ?? true;

        const dialogTitle = this.props.dialogTitle ?? 'Диалог';
        const dialogTitleFontSize = this.props.dialogTitleFontSize ?? (isDialogTitleSmall ? 14 : 26);
        const dialogTitleFontWeight = this.props.dialogTitleFontWeight ?? 600;
        const dialogTitleTextAlign = this.props.dialogTitleTextAlign ?? (isDialogTitleSmall ? 'left' : 'center');

        return (
            <Dialog
                data-testid="base-confirm-dialog"
                isOpen={ this.props.isOpen }
                dialogVerticalAlign={ dialogVerticalAlign }
                dialogWidth={ dialogWidth }
                isTitleSmall={ isDialogTitleSmall }
                titleTextAlign={ dialogTitleTextAlign }
                titleFontSize={ dialogTitleFontSize }
                titleFontWeight={ dialogTitleFontWeight }
                title={ dialogTitle }
                onCancelClick={ this.props.onCancelClick }
                buttons={ this.getDialogButtons }
            >
                { this.props.isOpen ? this.props.children(this.props.dialogContentViewFormRef, this.props.dataModel) : null }
            </Dialog>
        );
    }
}