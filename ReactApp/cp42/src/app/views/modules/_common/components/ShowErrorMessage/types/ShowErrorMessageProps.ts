import { IErrorsType } from 'app/common/interfaces';

export type ShowErrorMessageProps = {
    errors: IErrorsType;
    formName: string;
};