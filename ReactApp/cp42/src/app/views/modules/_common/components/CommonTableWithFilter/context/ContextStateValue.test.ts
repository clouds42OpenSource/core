import '@testing-library/react';
import { ContextWatchState } from 'app/views/modules/_common/components/CommonTableWithFilter/context/ContextStateValue';

describe('ContextStateValue', () => {
    const contextWatchStateData = {
        stateId: 'test_context',
        successAddNewItemMessage: 'getOnSuccessAddNewItemMessage',
        successModifyItemMessage: 'getOnSuccessModifyItemMessage',
        successDeleteItemMessage: 'getOnSuccessDeleteItemMessage',
        stateName: 'ProcessFlowState',
        newItemProcessId: 'addNewItemProcessId',
        modifyItemProcessId: 'modifyItemProcessId',
        deleteItemProcessId: 'deleteItemProcessId'
    } as const;

    beforeAll(() => {
        ContextWatchState.registerContextState<string>(
            contextWatchStateData.stateId,
            { watch: {
                reducerStateName: contextWatchStateData.stateName,
                processIds: {
                    addNewItemProcessId: contextWatchStateData.newItemProcessId,
                    modifyItemProcessId: contextWatchStateData.modifyItemProcessId,
                    deleteItemProcessId: contextWatchStateData.deleteItemProcessId
                } },
            processMessages: {
                getOnSuccessAddNewItemMessage: () => contextWatchStateData.successAddNewItemMessage,
                getOnSuccessModifyItemMessage: () => contextWatchStateData.successModifyItemMessage,
                getOnSuccessDeleteItemMessage: () => contextWatchStateData.successDeleteItemMessage
            }
            }
        );
    });

    it('adds item', () => {
        const addNewItemMessage = ContextWatchState.getOnSuccessAddNewItemMessage(contextWatchStateData.stateId);
        expect(contextWatchStateData.successAddNewItemMessage).toEqual(addNewItemMessage);
    });

    it('deletes item', () => {
        const deleteItemMessage = ContextWatchState.getOnSuccessDeleteItemMessage<string>(contextWatchStateData.stateId, 'item');
        expect(contextWatchStateData.successDeleteItemMessage).toEqual(deleteItemMessage);
    });

    it('modifies item', () => {
        const deleteItemMessage = ContextWatchState.getOnSuccessModifyItemMessage<string>(contextWatchStateData.stateId, 'item');
        expect(contextWatchStateData.successModifyItemMessage).toEqual(deleteItemMessage);
    });

    it('returns item state', () => {
        const contextProviderState = ContextWatchState.getContextProviderState(contextWatchStateData.stateId);
        expect(contextProviderState!.watch.reducerStateName).toEqual('ProcessFlowState');
    });

    it('returns processes id', () => {
        const processId = ContextWatchState.getProcessIds(contextWatchStateData.stateId);
        expect(
            processId.addNewItemProcessId === contextWatchStateData.newItemProcessId &&
            processId.modifyItemProcessId === contextWatchStateData.modifyItemProcessId &&
            processId.deleteItemProcessId === contextWatchStateData.deleteItemProcessId
        ).toBeTruthy();
    });

    it('returns watch processes', () => {
        const watchProcess = ContextWatchState.getWatchProcesses();
        expect(watchProcess[0].reducerStateName).toEqual(contextWatchStateData.stateName);
    });

    it('returns unique context from process id', () => {
        const uniqueContextFromProcessId = ContextWatchState.getUniqueContextProviderStateIdFromProcessId(contextWatchStateData.newItemProcessId);
        expect(uniqueContextFromProcessId).toEqual(contextWatchStateData.stateId);
    });
});