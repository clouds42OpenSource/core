/**
 * Тип кастомного файла
 */
export type CustomFile = {
    /**
     * Название файла
     */
    name: string,
    /**
     * Превью файла
     */
    preview: string
}