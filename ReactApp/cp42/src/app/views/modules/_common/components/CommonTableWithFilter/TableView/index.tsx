import { ERefreshId } from 'app/views/Layout/ProjectTour/enums';
import React from 'react';

import { SortingKind } from 'app/common/enums';
import { hasComponentChangesFor } from 'app/common/functions';
import { Table } from 'app/views/components/controls/Table';
import { TableCellProps, TableProps } from 'app/views/components/controls/Table/types';
import { DeleteItemButton, ModifyItemButton } from 'app/views/modules/_common/components/CommonTableWithFilter/_buttons';

export type TablePagination = {
    selectIsVisible?: boolean;
    selectCount?: string[];
    onSelectCount?: (pageSize: number) => void;
    hideRecordsCount?: boolean;
    totalPages: number;
    currentPage: number;
    pageSize: number;
    recordsCount: number;
    leftAlignContent?: React.ReactNode;
};

/**
 * Обьект для заголовка таблицы
 */
export type HeaderTitleTable = {
    /**
     * Поле для переключения цвета заголовка true = красный, false = желтый
     */
    isRed?: boolean;

    /**
     * Текст заголовка
     */
    text: string;
};

export type TableViewProps<TDataRow, TRowFields = keyof TDataRow> = {
    /**
     * Содержит название поля с уникальным значением
     */
    keyFieldName: TRowFields;
    /**
     * Содержит текст если таблица пуста
     */
    emptyText?: string;

    sorting?: {
        /**
         * Содержит название поля по которому сортировать
         */
        sortFieldName: TRowFields;

        /**
         * Вид сортировки
         */
        sortKind: SortingKind;
    };

    /**
     * Mapping между полем элемента данных и названием колонки
     */
    fieldsView: { [fieldName in keyof TDataRow]?: TableCellProps<TDataRow> };

    /**
     * Набор данных
     */
    dataset: TDataRow[];

    /**
     * Контент для кнопки редактирования
     */
    modifyButtonContent?: React.ReactNode;

    /**
     * Всплывающая подсказка для кнопки редактирования
     */
    modifyButtonTitle?: string;

    /**
     * Информация о страницах данных в таблице
     */
    pagination?: TablePagination;

    /**
     * Обьект для заголовка таблицы
     */
    headerTitleTable?: HeaderTitleTable;

    /**
     * Флаг для переключения между адаптивной таблицей и стандартной
     */
    isResponsiveTable?: boolean;
    /**
     * Если true фон строки серого цвета
     */
    isHistoricalRow?: TRowFields;

    /**
     * Добавляет к ключу строки ее ID
     */
    isIndexToKeyFieldName?: boolean;
};

/**
 * Свойства этого компонента
 */
type OwnProps<TDataRow> = TableProps<TDataRow> & {
    /**
     * Содержит текст если таблица пуста
     */
    emptyText?: string;

    /**
     * флаг для показа кнопку удаления
     */
    showDeleteItemCommand: boolean;

    /**
     * флаг для показа кнопку редактирования
     */
    showModifyItemCommand: boolean;

    /**
     * Уникальный ID провайдера состояния
     */
    uniqueContextProviderStateId: string;

    /**
     * Контент для кнопки редактирования
     */
    modifyButtonContent?: React.ReactNode;

    /**
     * Всплывающая подсказка для кнопки редактирования
     */
    modifyButtonTitle?: string;

    maxTableHeight?: string;

    /**
     * ID for triggering rerender onboarding component
     */
    refreshId?: ERefreshId;
    filterData?: any;
};

export class TableView<TDataRow> extends React.Component<OwnProps<TDataRow>> {
    public constructor(props: OwnProps<TDataRow>) {
        super(props);
        this.createRowActions = this.createRowActions.bind(this);
    }

    public shouldComponentUpdate(nextProps: OwnProps<TDataRow>) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private createRowActions(row: TDataRow) {
        return (
            <>
                {
                    this.props.showDeleteItemCommand
                        ? <DeleteItemButton uniqueContextProviderStateId={ this.props.uniqueContextProviderStateId } Item={ row } uniqueFieldName={ this.props.keyFieldName } />
                        : null

                }
                {
                    this.props.showModifyItemCommand
                        ? (
                            <ModifyItemButton
                                content={ this.props.modifyButtonContent }
                                title={ this.props.modifyButtonTitle }
                                uniqueContextProviderStateId={ this.props.uniqueContextProviderStateId }
                                Item={ row }
                                uniqueFieldName={ this.props.keyFieldName }
                            />
                        )
                        : null
                }
            </>
        );
    }

    public render() {
        return (
            <Table
                { ...this.props }
                createRowActions={
                    this.props.showDeleteItemCommand || this.props.showModifyItemCommand
                        ? this.createRowActions
                        : undefined
                }
            />
        );
    }
}