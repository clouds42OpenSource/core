import { hasComponentChangesFor } from 'app/common/functions';
import { ContainedButton } from 'app/views/components/controls/Button';
import { ButtonKind } from 'app/views/components/controls/Button/types';
import { TextOut } from 'app/views/components/TextOut';
import React from 'react';

type OwnProps<TReduxFormData> = {
    getFormData: () => TReduxFormData;
    onClick?: (formData: TReduxFormData) => void;
    kind?: ButtonKind;
    caption?: string;
};

export class ConfirmButton<TReduxFormData> extends React.Component<OwnProps<TReduxFormData>> {
    public constructor(props: OwnProps<TReduxFormData>) {
        super(props);
        this.onApplyChanges = this.onApplyChanges.bind(this);
    }

    public shouldComponentUpdate(nextProps: OwnProps<TReduxFormData>) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private onApplyChanges() {
        if (this.props.onClick) {
            const formData = this.props.getFormData();
            this.props.onClick(formData);
        }
    }

    public render() {
        const caption = this.props.caption ?? 'ОК';
        const kind = this.props.kind ?? 'success';
        return (
            <ContainedButton
                kind={ kind }
                onClick={ this.onApplyChanges }
            >
                <TextOut fontSize={ 12 } fontWeight={ 400 } inheritColor={ true }>
                    { caption.toUpperCase() }
                </TextOut>
            </ContainedButton>
        );
    }
}