import { Box, Theme } from '@mui/material';
import { SxProps } from '@mui/system';
import React from 'react';
import CloudUploadIcon from '@mui/icons-material/CloudUpload';
import { FileUploader } from 'react-drag-drop-files';

import Tooltip from '@mui/material/Tooltip';
import style from './style.module.css';

type TConfigFileUploaderProps = {
    callBack: (file: File) => void;
    fileTypesArr: string[];
    uploadedFiles?: File[] | null;
    deleteUploadFile?: (fileName: string, index: number) => void;
    openFile?: (fileName: string, index: number) => void;
    onTypeError?: () => void;
    required?: boolean;
    limit?: number;
    disabled?: boolean;
    text?: string;
    uploadFileStyle?: SxProps<Theme>;
    className?: string;
};

export class ConfigFileUploader extends React.Component<TConfigFileUploaderProps> {
    public static defaultProps = {
        fileTypesArr: ['zip', 'cfe', 'epf', 'erf']
    };

    private deleteHandler(ev: React.MouseEvent<HTMLDivElement>, fileName: string, index: number) {
        ev.stopPropagation();

        if (this.props.deleteUploadFile) {
            this.props.deleteUploadFile(fileName, index);
        }
    }

    private openHandler(ev: React.MouseEvent<HTMLDivElement>, fileName: string, index: number) {
        ev.stopPropagation();

        if (this.props.openFile) {
            this.props.openFile(fileName, index);
        }
    }

    public render() {
        const { callBack, uploadedFiles, fileTypesArr, required, limit, disabled, onTypeError, text, uploadFileStyle, className = '' } = this.props;
        const isHidden = !!(limit && uploadedFiles && uploadedFiles.length >= limit);

        return (
            <>
                { !isHidden && !disabled &&
                    <div className={ `${ style.uploader } ${ className }` }>
                        <FileUploader
                            handleChange={ callBack }
                            name="file"
                            types={ fileTypesArr }
                            required={ required }
                            disabled={ disabled }
                            onTypeError={ onTypeError }
                        >
                            <CloudUploadIcon className={ style.image } />
                            { text ?? 'Перетащите файлы в эту область или нажмите на неё для того чтобы выбрать.' }
                        </FileUploader>
                    </div>
                }
                { uploadedFiles &&
                    <div className={ style.uploadedFileContainer }>
                        { uploadedFiles.map((file, index) =>
                            <div key={ file.name }>
                                { disabled
                                    ? (
                                        <Tooltip arrow={ true } title="Открыть файл">
                                            <Box sx={ uploadFileStyle } className={ style.uploadedFile } onClick={ ev => this.openHandler(ev, file.name, index) }>
                                                { file.name }
                                            </Box>
                                        </Tooltip>
                                    )
                                    : (
                                        <Tooltip arrow={ true } title="Удалить файл">
                                            <Box sx={ uploadFileStyle } className={ style.uploadedFile } onClick={ ev => this.deleteHandler(ev, file.name, index) }>
                                                { file.name }
                                            </Box>
                                        </Tooltip>
                                    )
                                }
                            </div>
                        ) }
                    </div>
                }
            </>
        );
    }
}