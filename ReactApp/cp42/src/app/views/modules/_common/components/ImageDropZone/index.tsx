import { CustomFile, ImageDropZoneProps } from 'app/views/modules/_common/components/ImageDropZone/types';
import React, { Component } from 'react';
import Dropzone, { FileRejection } from 'react-dropzone';
import css from './styles.module.css';

type OwnState = {
    /**
     * Файл был загружен
     */
    isFileDroped: boolean;

    /**
     * Новый файл
     */
    newFile: CustomFile;

    /**
     * Компонент текста для дроп зоны
     */
    textElement: React.JSX.Element;
};

export class ImageDropZone extends Component<ImageDropZoneProps, OwnState> {
    public constructor(props: ImageDropZoneProps) {
        super(props);

        this.state = {
            isFileDroped: false,
            newFile: {} as CustomFile,
            textElement: <p>Перетащите изображение для футера в эту область или нажмите на нее.<br />Размер изображения должен быть (520X266)</p>
        };

        this.onDropedFile = this.onDropedFile.bind(this);
        this.getTextFileDropZone = this.getTextFileDropZone.bind(this);
        this.forUploadingAcceptedFile = this.forUploadingAcceptedFile.bind(this);
        this.forShowErrorMessageRejectedFile = this.forShowErrorMessageRejectedFile.bind(this);
    }

    public componentDidMount() {
        const { defaultFile } = this.props;

        if (defaultFile) {
            if (defaultFile instanceof File) {
                this.setState({
                    newFile: {
                        name: defaultFile.name,
                        preview: URL.createObjectURL(defaultFile)
                    },
                    isFileDroped: true
                });
            } else {
                this.setState({
                    newFile: {
                        name: 'no content',
                        preview: defaultFile
                    },
                    isFileDroped: true
                });
            }
        }
    }

    public componentWillUnmount() {
        URL.revokeObjectURL(this.state.newFile.preview);
    }

    /**
     * Для загруженных файлов, для вывода сообщения об ошибке либо подгрузке файла
     * @param acceptedFiles принятые файлы
     * @param fileRejections не принятые файлы
     */
    private onDropedFile(acceptedFiles: File[], fileRejections: FileRejection[]) {
        this.forUploadingAcceptedFile(acceptedFiles);
        this.forShowErrorMessageRejectedFile(fileRejections);
    }

    /**
     * Функция для получения сообщения об ошибке непринятого файла по коду ошибки
     * @param fileRejections не принятые файлы
     * @returns сообщение об ошибке
     */
    private getErrorTextByCode(fileRejections: FileRejection[]) {
        let errorText = {};

        if (fileRejections[0].errors[0].code === 'file-invalid-type') {
            errorText = <>Ваш файл { fileRejections[0].file.name } имеет неверный формат.<br />Допустимые форматы файла { this.props.acceptTypes }</>;
        } else {
            errorText = fileRejections[0].errors[0].message;
        }

        return errorText;
    }

    /**
     * Метод для вывода текста с информацией на превью
     * @returns JSX.Element для вывода текста с информацией
     */
    private getTextFileDropZone(): React.JSX.Element {
        const { isFileDroped, textElement } = this.state;

        return (
            !isFileDroped
                ? (
                    <div className={ css['dropZone-block-text'] }>
                        <img alt="Нет изображения" src={ `${ import.meta.env.PUBLIC_URL }/img/Icon_Upload.png` } className="p-t-20" />
                        <div className={ css['dropZone-block-text-style'] }>
                            { textElement }
                        </div>
                    </div>
                )
                : (
                    <div className={ css['dropZone-block-text-hover'] }>
                        <div className={ css['advertising-banner-display-hover'] }>
                            <em className={ `fa fa-refresh ${ css.refreshIcon }` } />
                        </div>
                    </div>
                )
        );
    }

    /**
     * Функция для подгрузки разрешенного файла
     * @param acceptedFiles принятые файлы
     */
    private forUploadingAcceptedFile(acceptedFiles: File[]) {
        if (acceptedFiles.length > 0) {
            this.setState({
                newFile: {
                    name: acceptedFiles[0].name,
                    preview: URL.createObjectURL(acceptedFiles[0])
                },
                isFileDroped: true
            });
            this.props.onUploaded(acceptedFiles[0]);
        }
    }

    /**
     * Функция для вывода сообщения об ошибке непринятых файлов
     * @param fileRejections не принятые файлы
     */
    private forShowErrorMessageRejectedFile(fileRejections: FileRejection[]) {
        if (fileRejections.length > 0) {
            this.setState({
                textElement: <p className="text-danger">{ this.getErrorTextByCode(fileRejections) }</p>
            });
        }
    }

    public render() {
        const { newFile } = this.state;

        return (
            <Dropzone
                data-testid="image-drop-zone"
                accept={ {
                    'image/gif': this.props.acceptTypes,
                    'image/jpeg': this.props.acceptTypes,
                    'image/png': this.props.acceptTypes
                } }
                onDrop={ this.onDropedFile }
                validator={ this.props.customValidation }
            >
                { ({ getRootProps, getInputProps }) => (
                    <section>
                        <div { ...getRootProps({ className: css['dropZone-narrow-file'] }) }>
                            <input { ...getInputProps() } />
                            { this.getTextFileDropZone() }
                            <div key={ newFile.name } style={ { display: 'flex', justifyContent: 'center' } }>
                                <img
                                    alt=""
                                    src={ newFile.preview }
                                    className={ css.img }
                                />
                            </div>
                        </div>
                    </section>
                ) }
            </Dropzone>
        );
    }
}