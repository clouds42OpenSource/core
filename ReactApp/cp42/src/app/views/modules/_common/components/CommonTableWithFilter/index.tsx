import { SortingKind } from 'app/common/enums';
import { hasComponentChangesFor } from 'app/common/functions';
import { IReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { EPartnersTourId, ERefreshId, ETourId } from 'app/views/Layout/ProjectTour/enums';
import { ContextWatchState, TProcessProps } from 'app/views/modules/_common/components/CommonTableWithFilter/context/ContextStateValue';
import { CommonTableWithFilterViewDialogsProps, Dialogs } from 'app/views/modules/_common/components/CommonTableWithFilter/Dialogs';
import { FilterView } from 'app/views/modules/_common/components/CommonTableWithFilter/FilterView';
import { TableView, TableViewProps } from 'app/views/modules/_common/components/CommonTableWithFilter/TableView';
import { SelectDataCommonDataModel } from 'app/web/common/data-models';
import React from 'react';

export interface ICommonTableWithFilter {
    ReSelectData: () => void;
}

export type SelectDataInitiator = 'initial' | 'filter-changed' | 'page-changed' | 'sorting-changed';

type OwnProps<TItem, TFilterReduxFormData> = {
    /**
     * Уникальный ID состояния контекста провайдера
     */
    uniqueContextProviderStateId: string;
    /**
     * Свойства таблицы редактирования
     */
    tableEditProps?: {
        processProps: TProcessProps<TItem>;
        dialogProps: CommonTableWithFilterViewDialogsProps<TItem>;
    };
    /**
     * Свойства фильтра
     */
    filterProps?: {
        getFilterContentView: (ref: React.Ref<IReduxForm<TFilterReduxFormData>>, onFilterChanged: (filterData: TFilterReduxFormData, filterFormName: string) => void) => React.ReactNode;
    },
    /**
     * Свойства таблицы
     */
    tableProps: TableViewProps<TItem>;
    /**
     * Call back функция при переходе на следующую страницу или при смене фильтра, поиска
     */
    onDataSelect?: (args: SelectDataCommonDataModel<TFilterReduxFormData>, filterFormName: string | null, initiator: SelectDataInitiator, pageSize?:number) => void;
    /**
     * Название класса
     */
    className?: string;
    /**
     * Флаг для переключения между адаптивной таблицей и стандартной
     */
    isResponsiveTable?: boolean;
    /**
     * Стиль с рамками для адаптивной таблицы
     */
    isBorderStyled?: boolean;
    maxTableHeight?: string;
    /**
     * Флаг для хедера таблицы для переноса текста
     * True будет перенос, False не будет переноса
     */
    isHeaderWrap?: boolean;
    doubleTable?: boolean;
    /**
     * ID for highlighted component in onboarding
     */
    tourId?: ETourId | EPartnersTourId;
    /**
     * ID for triggering rerender onboarding component
     */
    refreshId?: ERefreshId;
};

type Sorting<TItem> = {
    /**
     * Вид сортировки
     */
    sortKind: SortingKind;
    /**
     * По какому полю сортировать
     */
    fieldName: keyof TItem;
};

type OwnState<TItem, TFilterReduxFormData> = {
    /**
     * Информация о сортировке
     */
    sorting?: Sorting<TItem>;
    pageNumber: number;
    pageSize: number;
    initiator: SelectDataInitiator;
    filterFormName: string | null;
    filterData?: TFilterReduxFormData;
};

export class CommonTableWithFilter<
    TItem = NonNullable<unknown>,
    TFilterReduxFormData = NonNullable<unknown>> extends React.Component<OwnProps<TItem,
    TFilterReduxFormData>,
    OwnState<TItem, TFilterReduxFormData>
> implements ICommonTableWithFilter {
    private _filterViewRef = React.createRef<IReduxForm<TFilterReduxFormData>>();

    constructor(props: OwnProps<TItem, TFilterReduxFormData>) {
        super(props);
        this.getFilterData = this.getFilterData.bind(this);
        this.onFilterChanged = this.onFilterChanged.bind(this);
        this.onDataSelect = this.onDataSelect.bind(this);
        this.onPageChanged = this.onPageChanged.bind(this);
        this.onSelectCount = this.onSelectCount.bind(this);
        this.onHeaderSortClick = this.onHeaderSortClick.bind(this);
        this.renderTableWithFilterOnly = this.renderTableWithFilterOnly.bind(this);
        this.renderWithTableModifyDeleteItem = this.renderWithTableModifyDeleteItem.bind(this);
        this.state = {
            pageSize: 10,
            pageNumber: 1,
            sorting: this.props.tableProps.sorting
                ? {
                    fieldName: this.props.tableProps.sorting.sortFieldName,
                    sortKind: this.props.tableProps.sorting.sortKind
                } : undefined,
            initiator: 'initial',
            filterFormName: null,
            filterData: undefined
        };
        if (props.tableEditProps) {
            ContextWatchState.registerContextState(props.uniqueContextProviderStateId, props.tableEditProps.processProps);
        }
    }

    public shouldComponentUpdate(nextProps: OwnProps<TItem, TFilterReduxFormData>, nextState: OwnState<TItem, TFilterReduxFormData>) {
        return hasComponentChangesFor(this.props, nextProps) ||
            hasComponentChangesFor(this.state, nextState);
    }

    public componentWillUnmount() {
        if (this.props.tableEditProps) {
            ContextWatchState.unRegisterContextState(this.props.uniqueContextProviderStateId);
        }
    }

    private onPageChanged(currentPage: number) {
        this.setState({ pageNumber: currentPage, initiator: 'page-changed' }, () => {
            this.onDataSelect(this.state.initiator, currentPage, null, this.state.filterData, this.state.sorting, this.state.pageSize);
        });
    }

    private onSelectCount(pageSize: number) {
        this.setState({ pageSize, initiator: 'page-changed' }, () => {
            this.onDataSelect(this.state.initiator, 1, null, this.state.filterData, this.state.sorting, this.state.pageSize);
        });
    }

    private onHeaderSortClick(sortKind: SortingKind, fieldName: keyof TItem, isInitial: boolean) {
        const sorting: Sorting<TItem> = {
            sortKind,
            fieldName
        };
        this.setState({ sorting, initiator: isInitial ? 'initial' : 'sorting-changed', filterData: this.getFilterData() }, () => {
            this.onDataSelect(this.state.initiator, this.state.pageNumber, null, this.getFilterData(), sorting, this.state.pageSize);
        });
    }

    private onFilterChanged(filterData: TFilterReduxFormData, filterFormName: string) {
        this.setState({ initiator: 'filter-changed', filterFormName, filterData }, () => {
            this.onDataSelect(this.state.initiator, 1, this.state.filterFormName, filterData, this.state.sorting, this.state.pageSize);
        });
    }

    private onDataSelect(initiator: SelectDataInitiator, pageNumber: number, filterFormName: string | null, filter?: TFilterReduxFormData, sorting?: Sorting<TItem>, size?: number) {
        if (this.props.onDataSelect) {
            this.props.onDataSelect({
                filter,
                pageNumber,
                size,
                sortingData: sorting
                    ? {
                        fieldName: sorting.fieldName as string,
                        sortKind: sorting.sortKind
                    }
                    : undefined
            }, filterFormName, initiator);
        }
    }

    private getFilterData() {
        return this._filterViewRef.current
            ? this._filterViewRef.current.getReduxForm().getReduxFormFields(false)
            : void 0;
    }

    public ReSelectData() {
        this.onDataSelect(this.state.initiator, this.state.pageNumber, null, this.getFilterData(), this.state.sorting, this.state.pageSize);
    }

    private renderTableWithFilterOnly() {
        return (
            <>
                <FilterView
                    uniqueContextProviderStateId={ this.props.uniqueContextProviderStateId }
                    showAddNewItemCommand={ !!this.props.tableEditProps?.dialogProps.addDialog }
                    addNewItemButtonCaption={ this.props.tableEditProps?.dialogProps.addDialog?.showAddDialogButtonText }
                >
                    {
                        this.props.filterProps
                            ? this.props.filterProps.getFilterContentView(this._filterViewRef, this.onFilterChanged)
                            : true
                    }
                </FilterView>
                <TableView
                    refreshId={ this.props.refreshId }
                    maxTableHeight={ this.props.maxTableHeight }
                    isBorderStyled={ this.props.isBorderStyled }
                    emptyText={ this.props.tableProps.emptyText }
                    isHistoricalRow={ this.props.tableProps.isHistoricalRow }
                    isIndexToKeyFieldName={ this.props.tableProps.isIndexToKeyFieldName }
                    isHeaderWrap={ this.props.isHeaderWrap }
                    isResponsiveTable={ this.props.isResponsiveTable }
                    className={ this.props.className }
                    uniqueContextProviderStateId={ this.props.uniqueContextProviderStateId }
                    dataset={ this.props.tableProps.dataset }
                    fieldsView={ this.props.tableProps.fieldsView }
                    keyFieldName={ this.props.tableProps.keyFieldName }
                    sortFieldName={ this.state.sorting?.fieldName }
                    sortKind={ this.state.sorting?.sortKind }
                    pagination={ this.props.tableProps.pagination
                        ? {
                            hideRecordsCount: this.props.tableProps.pagination.hideRecordsCount!,
                            selectIsVisible: this.props.tableProps.pagination.selectIsVisible!,
                            selectCount: this.props.tableProps.pagination.selectCount!,
                            onSelectCount: this.onSelectCount,
                            currentPage: this.props.tableProps.pagination.currentPage,
                            pageSize: this.props.tableProps.pagination.pageSize,
                            recordsCount: this.props.tableProps.pagination.recordsCount,
                            totalPages: this.props.tableProps.pagination.totalPages,
                            onPageChanged: this.onPageChanged,
                            leftAlignContent: this.props.tableProps.pagination.leftAlignContent
                        }
                        : undefined
                    }
                    headerTitleTable={ this.props.tableProps.headerTitleTable
                        ? {
                            isRed: this.props.tableProps.headerTitleTable.isRed,
                            text: this.props.tableProps.headerTitleTable.text
                        }
                        : undefined
                    }
                    onHeaderSortClick={ this.onHeaderSortClick }
                    showDeleteItemCommand={ false }
                    showModifyItemCommand={ false }
                    filterData={ this.getFilterData() }
                />
            </>
        );
    }

    private renderWithTableModifyDeleteItem() {
        const tableEditProps = this.props.tableEditProps!;

        return (
            <>
                <FilterView
                    uniqueContextProviderStateId={ this.props.uniqueContextProviderStateId }
                    showAddNewItemCommand={ !!tableEditProps.dialogProps.addDialog }
                    addNewItemButtonCaption={ tableEditProps.dialogProps.addDialog?.showAddDialogButtonText }
                >
                    {
                        this.props.filterProps
                            ? this.props.filterProps.getFilterContentView(this._filterViewRef, this.onFilterChanged)
                            : true
                    }
                </FilterView>
                <TableView
                    maxTableHeight={ this.props.maxTableHeight }
                    isBorderStyled={ this.props.isBorderStyled }
                    emptyText={ this.props.tableProps.emptyText }
                    isIndexToKeyFieldName={ this.props.tableProps.isIndexToKeyFieldName }
                    isHistoricalRow={ this.props.tableProps.isHistoricalRow }
                    isHeaderWrap={ this.props.isHeaderWrap }
                    isResponsiveTable={ this.props.isResponsiveTable }
                    className={ this.props.className }
                    uniqueContextProviderStateId={ this.props.uniqueContextProviderStateId }
                    dataset={ this.props.tableProps.dataset }
                    fieldsView={ this.props.tableProps.fieldsView }
                    keyFieldName={ this.props.tableProps.keyFieldName }
                    sortFieldName={ this.state.sorting?.fieldName }
                    sortKind={ this.state.sorting?.sortKind }
                    pagination={ this.props.tableProps.pagination
                        ? {
                            hideRecordsCount: this.props.tableProps.pagination.hideRecordsCount!,
                            selectIsVisible: this.props.tableProps.pagination.selectIsVisible!,
                            selectCount: this.props.tableProps.pagination.selectCount!,
                            onSelectCount: this.onSelectCount,
                            currentPage: this.props.tableProps.pagination.currentPage,
                            pageSize: this.props.tableProps.pagination.pageSize,
                            recordsCount: this.props.tableProps.pagination.recordsCount,
                            totalPages: this.props.tableProps.pagination.totalPages,
                            onPageChanged: this.onPageChanged,
                            leftAlignContent: this.props.tableProps.pagination.leftAlignContent
                        }
                        : undefined
                    }
                    onHeaderSortClick={ this.onHeaderSortClick }
                    showDeleteItemCommand={ !!tableEditProps.dialogProps.deleteDialog }
                    showModifyItemCommand={ !!tableEditProps.dialogProps.modifyDialog }
                    modifyButtonContent={ this.props.tableProps.modifyButtonContent }
                    modifyButtonTitle={ this.props.tableProps.modifyButtonTitle }
                    headerTitleTable={ this.props.tableProps.headerTitleTable
                        ? {
                            isRed: this.props.tableProps.headerTitleTable.isRed,
                            text: this.props.tableProps.headerTitleTable.text
                        }
                        : undefined
                    }
                />
                <Dialogs
                    { ...tableEditProps.dialogProps }
                    uniqueContextProviderStateId={ this.props.uniqueContextProviderStateId }
                />
            </>
        );
    }

    public render() {
        const isTableWithFilterOnly = this.props.tableEditProps === void 0;

        return (
            <div data-tourid={ this.props.tourId } data-refreshid={ this.props.refreshId }>
                { isTableWithFilterOnly ? this.renderTableWithFilterOnly() : this.renderWithTableModifyDeleteItem() }
            </div>
        );
    }
}