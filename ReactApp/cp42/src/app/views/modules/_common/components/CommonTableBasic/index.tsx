import { TextOut } from 'app/views/components/TextOut';
import { memo, ReactNode } from 'react';
import { Table, TableBody, TableCell, TableContainer, TableRow, Box } from '@mui/material';

import { COLORS } from 'app/utils';
import { EPartnersTourId, ERefreshId, ETourId } from 'app/views/Layout/ProjectTour/enums';

export type DatasetType = {
    caption: NonNullable<ReactNode>;
    value: ReactNode;
    visible: boolean;
};

type TProps = {
    dataset: DatasetType[];
    tourId?: ETourId | EPartnersTourId;
    refreshId?: ERefreshId;
    captionWidth?: string;
    title?: string;
};

type TValueCell = {
    value: ReactNode;
};

const ValueCell = ({ value }: TValueCell) => {
    const getValue = () => {
        switch (typeof value) {
            case 'boolean':
                return value ? <i className="fa fa-check-square-o text-success" /> : <i className="fa fa-ban text-muted" />;
            default:
                return value;
        }
    };

    return (
        <TableCell sx={ { fontSize: '13px' } } align="left">
            { getValue() }
        </TableCell>
    );
};

export const CommonTableBasic = memo(({ dataset, tourId, refreshId, captionWidth = '200px', title }: TProps) => {
    return (
        <Box display="flex" flexDirection="column" gap="10px">
            { title && (<TextOut fontSize={ 14 } fontWeight={ 700 }>{ title }</TextOut>) }
            <TableContainer data-testid="common-table-basic" data-tourid={ tourId }>
                <Table sx={ { '.MuiTableBody-root > .MuiTableRow-root:hover': { backgroundColor: COLORS.white } } }>
                    <TableBody data-refreshid={ refreshId }>
                        { dataset.map(({ value, caption, visible }) => (visible && (
                            <TableRow key={ caption.toString() } sx={ { '&:last-child td, &:last-child th': { border: 0 }, width: '100%' } }>
                                <TableCell sx={ { fontSize: '13px', fontWeight: 'bold', width: captionWidth } } variant="head">{ caption }</TableCell>
                                <ValueCell value={ value } />
                            </TableRow>
                        ))) }
                    </TableBody>
                </Table>
            </TableContainer>
        </Box>
    );
});