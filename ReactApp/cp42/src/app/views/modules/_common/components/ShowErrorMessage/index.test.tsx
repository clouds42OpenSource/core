import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { ShowErrorMessage } from 'app/views/modules/_common/components/ShowErrorMessage';

describe('<ShowErrorMessage />', () => {
    it('renders', () => {
        render(<ShowErrorMessage errors={ { test_form_name: 'test_value' } } formName="test_form_name" />);
        expect(screen.getByText('test_value')).toBeInTheDocument();
    });

    it('does not renders', () => {
        render(<ShowErrorMessage errors={ { test_form_name: 'test_value' } } formName="test_form_name_1" />);
        expect(screen.queryByTestId(/show-error-message/i)).not.toBeInTheDocument();
    });
});