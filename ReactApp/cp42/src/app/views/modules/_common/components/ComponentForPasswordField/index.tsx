import { Box, Grid } from '@mui/material';
import { TextBoxForm } from 'app/views/components/controls/forms';
import { TextBoxFormType } from 'app/views/components/controls/forms/TextBoxForm/types';
import { TextOut } from 'app/views/components/TextOut';
import React, { Component } from 'react';
import css from './styles.module.css';

type PasswordFieldProps = {
    /**
     * Название формы поля
     */
    formName: string;

    /**
     * Заголовок поля
     */
    label?: string;

    /**
     * Плейсхолдер для подсказки
     */
    placeholder?: string;

    /**
     * Значение
     */
    value?: string;

    /**
     * Функция на изменения значения
     */
    onValueChange?: (formName: string, newValue: string) => void;
    fullWidth?: boolean;
    paddingRight?: string;
    error?: boolean;
    disable?: boolean;
    isReadOnly?: boolean;
    autoComplete?: string;
    onBlur?: (ev: React.FocusEvent<HTMLInputElement>) => void;
};

type PasswordFieldState = {
    /**
     * Тип поля
     */
    passwordInputType: TextBoxFormType;

    /**
     * Класс иконки для кнопки вскрытия и показа
     */
    classNameHideVisibleButton: 'fa-eye-slash' | 'fa-eye';
};

export class CommonPasswordFieldView extends Component<PasswordFieldProps, PasswordFieldState> {
    public constructor(props: PasswordFieldProps) {
        super(props);

        this.state = {
            classNameHideVisibleButton: 'fa-eye-slash',
            passwordInputType: 'password'
        };

        this.hideVisiblePassword = this.hideVisiblePassword.bind(this);
    }

    /**
     * Функция для скрытия и показа пароля
     */
    private hideVisiblePassword() {
        let { passwordInputType, classNameHideVisibleButton } = this.state;

        if (passwordInputType === 'password') {
            passwordInputType = 'text';
            classNameHideVisibleButton = 'fa-eye';
        } else {
            passwordInputType = 'password';
            classNameHideVisibleButton = 'fa-eye-slash';
        }

        this.setState({
            passwordInputType,
            classNameHideVisibleButton
        });
    }

    public render() {
        const { passwordInputType, classNameHideVisibleButton } = this.state;
        const { formName, label, onValueChange, placeholder, value, fullWidth, isReadOnly, paddingRight, disable, autoComplete, onBlur } = this.props;

        return (
            <Box gap="1px" display="flex" data-testid="common-password-field-view" className={ css['text-box'] } style={ { paddingRight } }>
                {
                    isReadOnly
                        ? (
                            <TextOut>{ label }: {value}</TextOut>
                        )
                        : (
                            <TextBoxForm
                                fullWidth={ fullWidth }
                                autoComplete={ autoComplete ?? 'new-password' }
                                formName={ formName }
                                label={ label }
                                type={ passwordInputType }
                                placeholder={ placeholder }
                                value={ value }
                                onValueChange={ onValueChange }
                                error={ this.props.error }
                                disabled={ disable }
                                onBlur={ onBlur }
                            />
                        )
                }
                {
                    !isReadOnly && (
                        <Grid item={ true } xs={ 1 } md={ 1 } lg={ 1 }>
                            <span className={ css['input-group-addon-eye'] } onClick={ this.hideVisiblePassword }>
                                <i className={ `fa ${ classNameHideVisibleButton }` } aria-hidden="true" />
                            </span>
                        </Grid>
                    )
                }
            </Box>
        );
    }
}