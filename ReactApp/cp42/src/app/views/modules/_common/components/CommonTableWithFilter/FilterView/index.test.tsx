import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { FilterView } from 'app/views/modules/_common/components/CommonTableWithFilter/FilterView';

describe('<FilterView />', () => {
    it('does not renders', () => {
        render(<FilterView showAddNewItemCommand={ false } uniqueContextProviderStateId="test" />);
        expect(screen.queryByTestId(/filter-view/i)).not.toBeInTheDocument();
    });

    it('renders', () => {
        render(<FilterView showAddNewItemCommand={ false } uniqueContextProviderStateId="test">test_text</FilterView>);
        expect(screen.getByTestId(/filter-view/i)).toBeInTheDocument();
    });

    it('render with error', () => {
        expect(() => {
            render(
                <FilterView
                    showAddNewItemCommand={ true }
                    uniqueContextProviderStateId="test"
                    addNewItemButtonCaption="test_caption"
                >
                    test_text
                </FilterView>
            );
        }).toThrowError();
    });
});