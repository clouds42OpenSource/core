import { hasComponentChangesFor } from 'app/common/functions';
import { ContainedButton, OutlinedButton } from 'app/views/components/controls/Button';
import {
    CommonTableWithFilterContext
} from 'app/views/modules/_common/components/CommonTableWithFilter/context/CommonTableWithFilterContext';
import cn from 'classnames';
import React from 'react';
import css from '../styles.module.css';

type OwnProps = {
    buttonCaption?: string;
    uniqueContextProviderStateId: string;
};

type OwnState = {
    uniqueContextProviderStateId: string;
};

export class AddNewItemButton extends React.Component<OwnProps, OwnState> {
    public context!: React.ContextType<typeof CommonTableWithFilterContext>;

    public constructor(props: OwnProps) {
        super(props);
        this.showAddNewConfirmDialog = this.showAddNewConfirmDialog.bind(this);
        this.state = {
            uniqueContextProviderStateId: props.uniqueContextProviderStateId
        };
    }

    public shouldComponentUpdate(nextProps: OwnProps, nextState: OwnState) {
        return hasComponentChangesFor(this.props, nextProps) ||
            hasComponentChangesFor(this.state, nextState);
    }

    private showAddNewConfirmDialog() {
        this.context.showAddNewConfirmDialog(this.props.uniqueContextProviderStateId, void 0);
    }

    public render() {
        const buttonCaption = this.props.buttonCaption ?? 'Добавить';
        const ButtonView = this.context.getIsAddNewConfirmDialogOpen(this.props.uniqueContextProviderStateId)
            ? ContainedButton
            : OutlinedButton;

        return (
            <ButtonView kind="success" onClick={ this.showAddNewConfirmDialog } className={ cn(css['add-button']) }>
                <i className="fa fa-plus-circle" />&nbsp;{ buttonCaption }
            </ButtonView>
        );
    }
}

AddNewItemButton.contextType = CommonTableWithFilterContext;