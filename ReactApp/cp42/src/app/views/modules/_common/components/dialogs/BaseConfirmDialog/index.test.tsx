import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import '@testing-library/jest-dom/extend-expect';
import { BaseConfirmDialog } from 'app/views/modules/_common/components/dialogs/BaseConfirmDialog';
import React from 'react';
import { IReduxForm } from 'app/views/components/_hoc/withReduxForm';

describe('<BaseConfirmDialog />', () => {
    const onCancelClickMock: jest.Mock = jest.fn();
    const childrenMock: jest.Mock = jest.fn();

    beforeEach(() => {
        render(
            <BaseConfirmDialog<string>
                dataModel="test_data_model"
                isOpen={ true }
                onCancelClick={ onCancelClickMock }
                dialogContentViewFormRef={ React.createRef<IReduxForm<string>>() }
            >
                {childrenMock}
            </BaseConfirmDialog>
        );
    });

    it('renders', () => {
        expect(screen.getByTestId(/dialog-view/i)).toBeInTheDocument();
    });

    it('should triggers onClick method', async () => {
        const buttonElement: HTMLElement | null = (await screen.findAllByTestId(/button-view/i))[1];
        await userEvent.click(buttonElement);
        expect(onCancelClickMock).toBeCalled();
    });
});