import { hasComponentChangesFor } from 'app/common/functions';
import { ContainedButton } from 'app/views/components/controls/Button';
import { TextOut } from 'app/views/components/TextOut';
import React from 'react';

type OwnProps<TItem> = {
    caption: string;
    itemToDelete: TItem;
    onDeleteConfirmed: (itemToDelete: TItem) => void;
};

export class DeleteItemConfirmButton<TItem> extends React.Component<OwnProps<TItem>> {
    public constructor(props: OwnProps<TItem>) {
        super(props);
        this.onDeleteItem = this.onDeleteItem.bind(this);
    }

    public shouldComponentUpdate(nextProps: OwnProps<TItem>) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private onDeleteItem() {
        if (this.props.onDeleteConfirmed) {
            this.props.onDeleteConfirmed(this.props.itemToDelete);
        }
    }

    public render() {
        const caption = this.props.caption ?? 'Удалить';
        return (
            <ContainedButton
                kind="error"
                onClick={ this.onDeleteItem }
            >
                <TextOut fontSize={ 12 } fontWeight={ 400 } inheritColor={ true }>
                    { caption.toUpperCase() }
                </TextOut>
            </ContainedButton>
        );
    }
}