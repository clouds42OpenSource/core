import { hasComponentChangesFor } from 'app/common/functions';
import { ContainedButton } from 'app/views/components/controls/Button';
import { ButtonKind } from 'app/views/components/controls/Button/types';
import { Dialog } from 'app/views/components/controls/Dialog';
import { DialogVerticalAlign, DialogWidth } from 'app/views/components/controls/Dialog/types';
import { TextOut } from 'app/views/components/TextOut';
import { TextFontSize, TextFontWeight, TextHorizontalAlign } from 'app/views/components/TextOut/views/TextView';
import { ConfirmButton } from 'app/views/modules/_common/components/dialogs/BaseDialog/_buttons/ConfirmButton';
import React from 'react';

export type BaseDialogProps = {
    isOpen: boolean;

    dialogWidth?: DialogWidth;
    dialogVerticalAlign?: DialogVerticalAlign;

    dialogTitle?: string;
    dialogTitleFontSize?: TextFontSize;
    dialogTitleFontWeight?: TextFontWeight;
    dialogTitleTextAlign?: TextHorizontalAlign;
    isDialogTitleSmall?: boolean;
    confirmButtonText?: string;
    confirmButtonKind?: ButtonKind;
    cancelButtonKind?: ButtonKind;
    children?: React.ReactNode;
    cancelButtonText?: string;

    onCancelClick: () => void;
    onConfirmClick?: () => void;
    cancelIcon?: string;
    confirmIcon?: string;
    cancelButtonDisabled?: boolean;
    confirmButtonDisabled?: boolean;
};

export class BaseDialog extends React.Component<BaseDialogProps> {
    public constructor(props:BaseDialogProps) {
        super(props);
        this.getDialogButtons = this.getDialogButtons.bind(this);
    }

    public shouldComponentUpdate(nextProps: BaseDialogProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private getDialogButtons() {
        return (
            <>
                <ContainedButton isEnabled={ this.props.cancelButtonDisabled } kind={ this.props.cancelButtonKind || 'default' } onClick={ this.props.onCancelClick }>
                    <TextOut fontSize={ 12 } fontWeight={ 400 } inheritColor={ true }>
                        { this.props.cancelIcon && <><i className={ this.props.cancelIcon } />&nbsp;</> }
                        { (this.props.cancelButtonText ?? 'Отмена').toUpperCase() }
                    </TextOut>
                </ContainedButton>
                {
                    this.props.onConfirmClick
                        ? (
                            <ConfirmButton
                                caption={ this.props.confirmButtonText }
                                icon={ this.props.confirmIcon }
                                onClick={ this.props.onConfirmClick }
                                kind={ this.props.confirmButtonKind }
                                confirmButtonDisabled={ this.props.confirmButtonDisabled }
                            />
                        )
                        : null
                }
            </>
        );
    }

    public render() {
        const dialogWidth = this.props.dialogWidth ?? 'sm';
        const dialogVerticalAlign = this.props.dialogVerticalAlign ?? 'top';
        const isDialogTitleSmall = this.props.isDialogTitleSmall ?? true;

        const dialogTitle = this.props.dialogTitle ?? 'Диалог';
        const dialogTitleFontSize = this.props.dialogTitleFontSize ?? (isDialogTitleSmall ? 14 : 26);
        const dialogTitleFontWeight = this.props.dialogTitleFontWeight ?? 600;
        const dialogTitleTextAlign = this.props.dialogTitleTextAlign ?? (isDialogTitleSmall ? 'left' : 'center');

        return (
            <Dialog
                isOpen={ this.props.isOpen }
                dialogVerticalAlign={ dialogVerticalAlign }
                dialogWidth={ dialogWidth }
                isTitleSmall={ isDialogTitleSmall }
                titleTextAlign={ dialogTitleTextAlign }
                titleFontSize={ dialogTitleFontSize }
                titleFontWeight={ dialogTitleFontWeight }
                title={ dialogTitle }
                onCancelClick={ this.props.onCancelClick }
                buttons={ this.getDialogButtons }
            >
                { this.props.children }
            </Dialog>
        );
    }
}