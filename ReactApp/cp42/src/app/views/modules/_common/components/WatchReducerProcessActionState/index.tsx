import { hasComponentChangesFor } from 'app/common/functions';
import { AppReduxStoreState, AppReduxStoreStateNames } from 'app/redux/types';
import { IReducerProcessActionInfo, IReducerState } from 'core/redux/interfaces';
import { ProcessIdType } from 'core/redux/types';
import React from 'react';
import { connect } from 'react-redux';

type OwnProps = {
    onProcessActionStateChanged: (processId: ProcessIdType, actionState: IReducerProcessActionInfo, error?: Error) => void;
    watch?: {
        processIds: Array<ProcessIdType>;
        reducerStateName: AppReduxStoreStateNames;
    }[]
};

type ReducerActionStateInfo = Partial<Record<string, {
    isInProgressState: boolean;
    isInSuccessState: boolean;
    isInErrorState: boolean;
    error?: Error;
}>>;

type OwnState = {
    reducerActionState: ReducerActionStateInfo,
    hasChanges: boolean;
};

type StateProps = {
    reducerActionState: ReducerActionStateInfo
};

type AllProps = OwnProps & StateProps;

class WatchReducerProcessActionStateClass extends React.Component<AllProps, OwnState> {
    public constructor(props: AllProps) {
        super(props);
        this.raiseOnProcessActionStateChanged = this.raiseOnProcessActionStateChanged.bind(this);
        this.state = {
            reducerActionState: {},
            hasChanges: false
        };
    }

    public static getDerivedStateFromProps(nextProps: AllProps, prevState: OwnState): Partial<OwnState> | null {
        const hasChanges = hasComponentChangesFor(prevState.reducerActionState, nextProps.reducerActionState);

        if (!hasChanges) {
            return null;
        }

        return {
            reducerActionState: {
                ...nextProps.reducerActionState,
            },
            hasChanges: true
        };
    }

    public shouldComponentUpdate(_nextProps: OwnProps, nextState: OwnState) {
        return nextState.hasChanges;
    }

    public componentDidUpdate(_prevProps: AllProps, prevState: OwnState) {
        if (this.state.hasChanges) {
            this.raiseOnProcessActionStateChanged(this.state.reducerActionState, prevState.reducerActionState);
        }
    }

    private raiseOnProcessActionStateChanged(currentReducerActionState: ReducerActionStateInfo, prevReducerActionState: ReducerActionStateInfo) {
        const processIds = Object.keys(currentReducerActionState);

        for (const processId of processIds) {
            const { isInProgressState, isInSuccessState, isInErrorState, error } = currentReducerActionState[processId]!;

            if (!prevReducerActionState[processId]) {
                this.props.onProcessActionStateChanged(processId, {
                    isInProgressState,
                    isInSuccessState,
                    isInErrorState,
                }, error);
                continue;
            }

            const { isInProgressState: isPrevInProgressState, isInSuccessState: isPrevInSuccessState, isInErrorState: isPrevInErrorState } = prevReducerActionState[processId]!;

            if (isInProgressState !== isPrevInProgressState ||
                isInSuccessState !== isPrevInSuccessState ||
                isInErrorState !== isPrevInErrorState) {
                this.props.onProcessActionStateChanged(processId, {
                    isInProgressState,
                    isInSuccessState,
                    isInErrorState,
                }, error);
                continue;
            }
        }

        this.setState({
            hasChanges: false
        });
    }

    public render() {
        return null;
    }
}

function getReducerProcessInfo(storeState: AppReduxStoreState, reducerStateName: AppReduxStoreStateNames, processIds: Array<ProcessIdType>) {
    const reducerState = storeState[reducerStateName] as IReducerState<ProcessIdType>;

    for (const processId of processIds) {
        const reducerActions = reducerState.reducerActions[processId];

        if (!reducerActions?.hasProcessActionStateChanged) {
            continue;
        }

        const { isInErrorState, isInSuccessState, isInProgressState } = reducerActions.processActionState;
        const { error } = reducerActions.process;
        return {
            processId,
            isInSuccessState,
            isInProgressState,
            isInErrorState,
            error
        };
    }
}

function getChangedReducerActionState(storeState: AppReduxStoreState, props: OwnProps) {
    const newState: ReducerActionStateInfo = {};

    for (const item of props.watch!) {
        const reducerProcessInfo = getReducerProcessInfo(storeState, item.reducerStateName, item.processIds);

        if (!reducerProcessInfo) { continue; }

        const { isInProgressState, isInSuccessState, isInErrorState, error } = reducerProcessInfo;

        newState[reducerProcessInfo.processId] = {
            isInProgressState,
            isInSuccessState,
            isInErrorState,
            error
        };
    }
    return newState;
}

export const WatchReducerProcessActionState = connect<StateProps, {}, OwnProps, AppReduxStoreState>(
    (state, props) => {
        return {
            reducerActionState: getChangedReducerActionState(state, props)
        };
    }
)(WatchReducerProcessActionStateClass);