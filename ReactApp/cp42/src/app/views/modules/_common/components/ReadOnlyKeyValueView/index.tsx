import { hasComponentChangesFor } from 'app/common/functions';
import { TextOut } from 'app/views/components/TextOut';
import cn from 'classnames';
import React from 'react';
import css from './styles.module.css';

type OwnProps = {
    label: string;
    text: React.ReactNode;
};

export class ReadOnlyKeyValueView extends React.Component<OwnProps> {
    public shouldComponentUpdate(nextProps: OwnProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    public render() {
        return (
            <div className={ cn(css['field-container']) } data-testid="read-only-key-value-view">
                <TextOut fontSize={ 13 } fontWeight={ 600 }>
                    { this.props.label }
                </TextOut>
                <TextOut inDiv={ true } fontSize={ 13 }>
                    { this.props.text }
                </TextOut>
            </div>
        );
    }
}