import { CommonTableWithFilterContextState } from 'app/views/modules/_common/components/CommonTableWithFilter/context/CommonTableWithFilterContextState';
import React from 'react';

export const CommonTableWithFilterContext = React.createContext<CommonTableWithFilterContextState<any>>((void 0) as any);
CommonTableWithFilterContext.displayName = 'CommonTableWithFilterContext';