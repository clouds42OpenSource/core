import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { TableView } from 'app/views/modules/_common/components/CommonTableWithFilter/TableView';

describe('<TableView />', () => {
    let container: HTMLElement;

    beforeAll(() => {
        container = render(<TableView
            keyFieldName="caption"
            fieldsView={ {} }
            dataset={ [{ caption: 'test_caption_0', value: 0, visible: true }, { caption: 'test_caption_1', value: 1, visible: true }] }
            showDeleteItemCommand={ false }
            showModifyItemCommand={ false }
            uniqueContextProviderStateId="test_state_id"
        />).container;
    });

    it('renders', () => {
        expect(screen.getByTestId(/table-view/i)).toBeInTheDocument();
    });
});