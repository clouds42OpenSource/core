interface IMethods {
    addNew: any;
    modifyItem: any;
    deleteItem: any;
}

export type CommonTableWithFilterContextState<TItem> = Readonly<
    {
        getSelectedItem: (uniqueContextProviderStateId: string) => TItem | null;
    } & {
        [K in keyof IMethods as `getIs${ Capitalize<string & K> }ConfirmDialogOpen`]: (uniqueContextProviderStateId: string) => boolean;
    } & {
        [K in keyof IMethods as `show${ Capitalize<string & K> }ConfirmDialog`]: (uniqueContextProviderStateId: string, item: TItem) => void;
    } & {
        [K in keyof IMethods as `hide${ Capitalize<string & K> }ConfirmDialog`]: (uniqueContextProviderStateId: string) => void;
    }>;