import { AcceptTypes } from 'app/views/modules/_common/components/ImageDropZone/types';

/**
 * Cвойства image drop zone
 */
export type ImageDropZoneProps = {
    /**
     * Принимаемые типы файлов
     */
    acceptTypes: AcceptTypes[];

    /**
     * Call back Функция на загрузку файла
     */
    onUploaded: (acceptedFiles: File) => void;

    /**
     * Дефолтный файл по умолчанию
     */
    defaultFile?: File | string;

    /**
     * Кастомная валидация
     */
    customValidation?: (file: File) => {
        code: string;
        message: string;
    } | null;
}