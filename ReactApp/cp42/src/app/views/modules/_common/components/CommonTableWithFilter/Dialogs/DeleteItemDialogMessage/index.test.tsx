import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import userEvent from '@testing-library/user-event';
import { DeleteItemDialogMessage } from 'app/views/modules/_common/components/CommonTableWithFilter/Dialogs/DeleteItemDialogMessage';

describe('<DeleteItemDialogMessage />', () => {
    const onNoClickMock: jest.Mock = jest.fn();
    const onYesClickMock: jest.Mock = jest.fn();

    beforeEach(() => {
        render(<DeleteItemDialogMessage
            isOpen={ true }
            selectedItem="test"
            dialogTitle="test_dialog_title"
            deleteButtonText="test_delete_button_text"
            onNoClick={ onNoClickMock }
            onYesClick={ onYesClickMock }
        />);
    });

    it('renders', () => {
        expect(screen.getByText(/test_dialog_title/i)).toBeInTheDocument();
    });

    it('triggers onNoClick method', async () => {
        const buttonElement: HTMLElement = (await screen.findAllByTestId(/button-view/i))[2];
        await userEvent.click(buttonElement);
        expect(onNoClickMock).toBeCalled();
    });

    it('triggers onYesClick method', async () => {
        const buttonElement: HTMLElement = (await screen.findAllByTestId(/button-view/i))[1];
        await userEvent.click(buttonElement);
        expect(onYesClickMock).toBeCalled();
    });
});