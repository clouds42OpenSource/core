import { Box, Input, Stack, styled, TextField } from '@mui/material';
import { VERIFICATION_CODE } from 'app/api/endpoints';
import { REDUX_API } from 'app/api/useReduxApi';
import { EMessageType, useFloatMessages } from 'app/hooks';
import { AppReduxStoreState } from 'app/redux/types';
import { COLORS } from 'app/utils';
import { SuccessButton } from 'app/views/components/controls/Button';
import { TextOut } from 'app/views/components/TextOut';
import { FocusEvent, FormEvent, KeyboardEvent, ReactNode, useCallback, useEffect, useRef, useState } from 'react';
import { connect } from 'react-redux';
import * as yup from 'yup';

type DispatchProps = {
    getCurrentSettings: () => void;
}

const VerificationInput = styled(Input)(() => ({
    width: '2rem',
    fontSize: '1.4rem',
    fontWeight: '600',
    input: { textAlign: 'center ' },
    appearance: 'textfield',
    'input::-webkit-outer-spin-button, input::-webkit-inner-spin-button': {
        appearance: 'none',
        margin: 0
    }
}));

type InputOrNull = HTMLInputElement | null;

interface VerificationProps {
    type: 'phone' | 'mail' | string;
    description: ReactNode;
    disableResendCode: boolean;
    countSecond: number;
    getNewCode: () => void;
    closeDialog: () => void;
    length?: number;
}

type AllProps = VerificationProps & DispatchProps;

const schema = yup
    .array()
    .required()
    .of(yup.number().required())
    .when('$length', (len, s) => (len ? s.length(len[0]) : s));

const { getConfirmPhone, getConfirmEmail } = VERIFICATION_CODE;
const { getCurrentSessionSettings } = REDUX_API.GLOBAL_REDUX;

const VerificationFormFunction = ({ type, description, getNewCode, disableResendCode, countSecond, closeDialog, getCurrentSettings, length = 6 }: AllProps) => {
    const [isSubmitted, setIsSubmitted] = useState(false);
    const [isValid, setIsValid] = useState(true);
    const [code, setCode] = useState<string[]>(Array(length).fill(''));
    const [emailCode, setEmailCode] = useState('');
    const [isDisableSubmitButton, setIsDisableSubmitButton] = useState(true);

    const { show } = useFloatMessages();

    const update = useCallback((index: number, val: string) => {
        return setCode(prevState => {
            const slice = prevState.slice();
            slice[index] = val;
            return slice;
        });
    }, []);

    const formRef = useRef<HTMLFormElement>(null);

    const handleKeyDown = (event: KeyboardEvent<HTMLInputElement>) => {
        const index = parseInt(event.currentTarget.dataset.index as string, 10);
        const form = formRef.current;

        if (Number.isNaN(index) || form === null) {
            return;
        }

        const prevIndex = index - 1;
        const nextIndex = index + 1;
        const prevInput: InputOrNull = form.querySelector(`.input-${ prevIndex }`);
        const nextInput: InputOrNull = form.querySelector(`.input-${ nextIndex }`);

        // eslint-disable-next-line default-case
        switch (event.key) {
            case 'Backspace':
                if (code[index]) {
                    update(index, '');
                } else if (prevInput) {
                    prevInput.select();
                }

                break;
            case 'ArrowRight':
                event.preventDefault();

                if (nextInput) {
                    nextInput.focus();
                }

                break;
            case 'ArrowLeft':
                event.preventDefault();

                if (prevInput) {
                    prevInput.focus();
                }

                break;
        }
    };

    const handleChange = (event: FormEvent<HTMLInputElement>) => {
        const { value } = event.currentTarget;
        const index = parseInt(event.currentTarget.dataset.index as string, 10);
        const form = formRef.current;

        if (Number.isNaN(index) || form === null) {
            return;
        }

        let nextIndex = index + 1;
        let nextInput: InputOrNull = form.querySelector(`.input-${ nextIndex }`);

        update(index, value[0] || '');

        if (value.length === 1) {
            nextInput?.focus();
        } else if (index < length - 1) {
            const split = value.slice(index + 1, length).split('');

            split.forEach(val => {
                update(nextIndex, val);
                nextInput?.focus();
                nextInput = form.querySelector(`.input-${ ++nextIndex }`);
            });
        }
    };

    const handleFocus = (event: FocusEvent<HTMLInputElement>) => {
        event.currentTarget.select();
    };

    useEffect(() => {
        if (isSubmitted) {
            setIsValid(schema.isValidSync(code, { context: { length } }));
        }
    }, [code, isSubmitted, length]);

    useEffect(() => {
        if (code.filter(item => item === '').length === 0) {
            setIsDisableSubmitButton(false);
        }

        if (type === 'mail' && emailCode !== '') {
            setIsDisableSubmitButton(false);
        }
    }, [code, emailCode, type]);

    const handleSubmit = async (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        setIsSubmitted(true);
        try {
            if (type === 'phone') {
                const data = await schema.validate(code, { context: { length } });

                getConfirmPhone(data?.join('')).then(response => {
                    if (response.success) {
                        show(EMessageType.success, response.data);
                        closeDialog();
                        getCurrentSettings();
                    } else {
                        show(EMessageType.error, response.message);
                    }
                });
            }
            if (type === 'mail') {
                getConfirmEmail(emailCode).then(response => {
                    if (response.success) {
                        show(EMessageType.success, response.data);
                        closeDialog();
                        getCurrentSettings();
                    } else {
                        show(EMessageType.error, response.message);
                    }
                });
            }
        } catch (e: unknown) {
            setIsValid(false);
        }
    };

    return (
        <Box
            display="flex"
            gap="5px"
            flexDirection="column"
            alignItems="center"
            component="form"
            ref={ formRef }
            onSubmit={ handleSubmit }
            noValidate={ true }
        >
            <TextOut>{ description }</TextOut>
            {
                type === 'mail'
                    ? (
                        <TextField
                            fullWidth={ true }
                            value={ emailCode }
                            onChange={ event => setEmailCode(event.target.value) }
                        />
                    )
                    : (
                        <Stack
                            component="fieldset"
                            border="none"
                            direction="row"
                            spacing={ 1.2 }
                            justifyContent="center"
                        >
                            {
                                code.map((value, i) => (
                                    <VerificationInput
                                        key={ i }
                                        value={ value }
                                        error={ isSubmitted && !isValid }
                                        inputProps={ {
                                            type: 'number',
                                            className: `input-${ i }`,
                                            'aria-label': `Number ${ i + 1 }`,
                                            'data-index': i,
                                            pattern: '[0-9]*',
                                            inputtype: 'numeric',
                                            onChange: handleChange,
                                            onKeyDown: handleKeyDown,
                                            onFocus: handleFocus
                                        } }
                                        style={ { color: isSubmitted && !isValid ? COLORS.error : COLORS.main } }
                                    />
                                ))
                            }
                        </Stack>
                    )
            }
            <SuccessButton type="submit" isEnabled={ !isDisableSubmitButton }>
                Подтвердить
            </SuccessButton>
            <TextOut>
                {
                    countSecond === 0 && (
                        <>
                            Не пришел код?
                            <TextOut
                                style={ { color: disableResendCode ? COLORS.disabled : COLORS.link, cursor: disableResendCode ? 'default' : 'pointer' } }
                                onClick={
                                    disableResendCode
                                        ? undefined
                                        : () => {
                                            getNewCode();
                                            show(EMessageType.success, 'Сообщение с кодом успешно отправлено');
                                        }
                                }
                            >
                                &nbsp;Получить новый
                            </TextOut>
                        </>
                    )
                }
                {
                    countSecond > 0 && <TextOut>Можно будет повторить через { countSecond } секунд</TextOut>
                }
            </TextOut>
        </Box>
    );
};

export const VerificationForm = connect<object, DispatchProps, NonNullable<unknown>, AppReduxStoreState>(
    null,
    dispatch => ({
        getCurrentSettings: () => getCurrentSessionSettings(dispatch)
    })
)(VerificationFormFunction);