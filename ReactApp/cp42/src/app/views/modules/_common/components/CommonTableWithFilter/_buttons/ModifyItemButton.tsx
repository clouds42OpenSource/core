import EditIcon from '@mui/icons-material/Edit';
import { hasComponentChangesFor } from 'app/common/functions';
import { ContainedButton, OutlinedButton } from 'app/views/components/controls/Button';

import { CommonTableWithFilterContext } from 'app/views/modules/_common/components/CommonTableWithFilter/context/CommonTableWithFilterContext';
import cn from 'classnames';
import React from 'react';
import css from '../styles.module.css';

type OwnProps<TItem> = {
    Item: TItem;
    uniqueFieldName: keyof TItem;
    uniqueContextProviderStateId: string;
    content?: React.ReactNode;
    title?: string;
};

export class ModifyItemButton<TItem> extends React.Component<OwnProps<TItem>> {
    public context!: React.ContextType<typeof CommonTableWithFilterContext>;

    public constructor(props: OwnProps<TItem>) {
        super(props);
        this.showModifyConfirmDialogFor = this.showModifyConfirmDialogFor.bind(this);
    }

    public shouldComponentUpdate(nextProps: OwnProps<TItem>) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private showModifyConfirmDialogFor() {
        this.context.showModifyItemConfirmDialog(this.props.uniqueContextProviderStateId, this.props.Item);
    }

    public render() {
        const { uniqueFieldName } = this.props;
        const selectedItem = this.context.getSelectedItem(this.props.uniqueContextProviderStateId);

        const ButtonView = this.context.getIsModifyItemConfirmDialogOpen(this.props.uniqueContextProviderStateId) &&
        selectedItem && selectedItem[uniqueFieldName] === this.props.Item[uniqueFieldName]
            ? ContainedButton
            : OutlinedButton;

        return (
            <ButtonView kind="success" title={ this.props.title ?? 'Редактировать' } className={ cn(css['modify-row-button']) } onClick={ this.showModifyConfirmDialogFor }>
                { this.props.content ?? <EditIcon /> }
            </ButtonView>
        );
    }
}

ModifyItemButton.contextType = CommonTableWithFilterContext;