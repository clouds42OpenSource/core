import { AppRoutes } from 'app/AppRoutes';
import { Dialog } from 'app/views/components/controls/Dialog';
import { DialogButtonType } from 'app/views/components/controls/Dialog/views/DialogActionsView/Index';
import { TextOut } from 'app/views/components/TextOut';
import { memo, useCallback, useMemo } from 'react';
import { useHistory } from 'react-router';

import styles from '../style.module.css';

type TPaymentDialog = {
    isOpen: boolean;
    onCancelClick: () => void;
    isPromisePaymentAvailable?: boolean;
    sum?: number;
    title?: string;
    currency?: string;
    apply: (canUsePromisePayment?: boolean) => void;
};

export const PaymentDialog = memo(({ isOpen, onCancelClick, isPromisePaymentAvailable, sum, title = 'Недостаточно средств для оплаты услуги', currency, apply }: TPaymentDialog) => {
    const history = useHistory();

    const redirectToBilling = useCallback(() => {
        history.push(AppRoutes.accountManagement.balanceManagement);
    }, [history]);

    const getButtons = useMemo(() => {
        const buttons: DialogButtonType = [
            {
                content: 'Отменить',
                onClick: onCancelClick,
                kind: 'default',
                variant: 'contained'
            }
        ];

        if (isPromisePaymentAvailable) {
            buttons.push({
                content: `Взять обещанный платеж ${ sum ? `(${ sum.toFixed(2) } ${ currency ?? '' })` : '' }`,
                onClick: () => {
                    apply(true);
                    onCancelClick();
                },
                kind: 'success',
                variant: 'outlined'
            });
        }

        buttons.push({
            content: 'Оплатить сейчас',
            onClick: redirectToBilling,
            kind: 'success',
            variant: 'contained'
        });

        return buttons;
    }, [apply, currency, isPromisePaymentAvailable, onCancelClick, redirectToBilling, sum]);

    return (
        <Dialog
            dialogVerticalAlign="bottom"
            title={ title }
            maxHeight="450px"
            onCancelClick={ onCancelClick }
            dialogWidth="md"
            isOpen={ isOpen }
            buttons={ getButtons }
            contentClassName={ styles.content }
        >
            <TextOut>Внимание! Уважаемый пользователь, для оплаты доступа у Вас недостаточно средств в размере { sum ?? 0 } { currency }</TextOut>
            { isPromisePaymentAvailable &&
                <TextOut>Вы можете воспользоваться услугой «Обещанный платеж». Вы должны будете погасить задолженность в течение 7 дней.</TextOut>
            }
        </Dialog>
    );
});