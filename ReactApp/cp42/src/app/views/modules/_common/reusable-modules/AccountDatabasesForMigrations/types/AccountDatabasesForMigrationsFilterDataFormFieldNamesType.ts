import { AccountDatabasesForMigrationsFilterDataForm } from 'app/views/modules/_common/reusable-modules/AccountDatabasesForMigrations/types/AccountDatabasesForMigrationsFilterDataForm';

/**
 * Названия свойств для фильтра выбора информационных баз
 */
export type AccountDatabasesForMigrationsFilterDataFormFieldNamesType = keyof AccountDatabasesForMigrationsFilterDataForm;