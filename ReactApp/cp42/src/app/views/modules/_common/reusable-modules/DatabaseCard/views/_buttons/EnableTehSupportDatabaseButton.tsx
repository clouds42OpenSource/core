import { hasComponentChangesFor } from 'app/common/functions';
import { DatabaseCardProcessId } from 'app/modules/databaseCard';
import { EnableDatabaseSupportThunkParams } from 'app/modules/databaseCard/store/reducers/enableDatabaseSupportReducer/params';
import { EnableDatabaseSupportThunk } from 'app/modules/databaseCard/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { ContainedButton } from 'app/views/components/controls/Button';
import { WatchReducerProcessActionState } from 'app/views/modules/_common/components/WatchReducerProcessActionState';
import cn from 'classnames';
import { IReducerProcessActionInfo } from 'core/redux/interfaces';
import React from 'react';
import { connect } from 'react-redux';
import css from '../styles.module.css';

type OwnProps = FloatMessageProps & {
    username: string;
    password: string;
    databaseId: string;
    onSuccess?: () => void;
};

type DispatchProps = {
    dispatchEnableDatabaseSupportThunk: (args?: EnableDatabaseSupportThunkParams) => void;

};

type OwnState = {
    enableDatabaseTehSupportState: {
        isInProgressState: boolean;
        isInSuccessState: boolean;
        isInErrorState: boolean;
        error?: Error;
    }
};
type AllProps = OwnProps & DispatchProps;

class EnableTehSupportDatabaseButtonClass extends React.Component<AllProps, OwnState> {
    constructor(props: AllProps) {
        super(props);
        this.onEnableDatabaseSupport = this.onEnableDatabaseSupport.bind(this);
        this.onProcessActionStateChanged = this.onProcessActionStateChanged.bind(this);
        this.state = {
            enableDatabaseTehSupportState: {
                isInErrorState: false,
                isInProgressState: false,
                isInSuccessState: false,
                error: undefined
            }
        };
    }

    public shouldComponentUpdate(nextProps: AllProps, nextState: OwnState) {
        return hasComponentChangesFor(this.props, nextProps) ||
            hasComponentChangesFor(this.state, nextState);
    }

    public componentDidUpdate(_prevProps: AllProps, prevState: OwnState) {
        if (this.state.enableDatabaseTehSupportState.isInErrorState && !prevState.enableDatabaseTehSupportState.isInErrorState) {
            this.props.floatMessage.show(MessageType.Error, this.state.enableDatabaseTehSupportState.error?.message);
        } else
            if (this.state.enableDatabaseTehSupportState.isInSuccessState && !prevState.enableDatabaseTehSupportState.isInSuccessState) {
                if (this.props.onSuccess) this.props.onSuccess();
            }
    }

    private onEnableDatabaseSupport() {
        this.props.dispatchEnableDatabaseSupportThunk({
            databaseId: this.props.databaseId,
            login: this.props.username,
            password: this.props.password,
            force: true
        });
    }

    private onProcessActionStateChanged(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId === DatabaseCardProcessId.EnableDatabaseSupport) {
            this.setState({
                enableDatabaseTehSupportState: {
                    isInProgressState: actionState.isInProgressState,
                    isInSuccessState: actionState.isInSuccessState,
                    isInErrorState: actionState.isInErrorState,
                    error
                }
            });
        }
    }

    public render() {
        const isEnable = !!this.props.username && !!this.props.password && !this.state.enableDatabaseTehSupportState.isInProgressState;
        return (
            <>
                <ContainedButton
                    isEnabled={ isEnable }
                    showProgress={ this.state.enableDatabaseTehSupportState.isInProgressState }
                    kind="primary"
                    className={ cn(css.btn) }
                    onClick={ this.onEnableDatabaseSupport }
                >
                    Подключить
                </ContainedButton>
                <WatchReducerProcessActionState
                    watch={ [{
                        reducerStateName: 'DatabaseCardState',
                        processIds: [DatabaseCardProcessId.EnableDatabaseSupport]
                    }] }
                    onProcessActionStateChanged={ this.onProcessActionStateChanged }
                />
            </>
        );
    }
}

const EnableTehSupportDatabaseButtonConnected = connect<NonNullable<unknown>, DispatchProps, OwnProps, AppReduxStoreState>(
    null,
    {
        dispatchEnableDatabaseSupportThunk: EnableDatabaseSupportThunk.invoke
    }
)(EnableTehSupportDatabaseButtonClass);

export const EnableTehSupportDatabaseButton = withFloatMessages(EnableTehSupportDatabaseButtonConnected);