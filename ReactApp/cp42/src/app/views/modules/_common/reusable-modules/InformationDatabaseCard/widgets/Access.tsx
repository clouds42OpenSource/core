import { Box, Chip, Pagination, Tooltip } from '@mui/material';
import { TPermissions } from 'app/api/endpoints/accountUsers/request';
import { TGetAccountProfileResponse, TGetGivenAccountProfilesResponse } from 'app/api/endpoints/accountUsers/response';
import { TLocale } from 'app/api/endpoints/global/response';
import { TAccountUserDataModelsListItem, TGetAccessPriceRequest, TGetAccessRequest } from 'app/api/endpoints/informationBases/request';
import { TDatabaseAccesses, TGetAvailability, TGetExternalUser, TGetInformationBaseInfoResponse } from 'app/api/endpoints/informationBases/response';
import { REDUX_API } from 'app/api/useReduxApi';
import { AccountUserGroup } from 'app/common/enums';
import { AccountDatabaseAccessState } from 'app/common/enums/AccountDatabaseAccessStateEnum';
import { configurationProfilesToRole } from 'app/common/functions';
import { getEmptyUuid } from 'app/common/helpers';
import { TimerHelper } from 'app/common/helpers/TimerHelper';
import { getGivenAccountProfilesListSlice, TChangeRequestDataPayload } from 'app/modules/accountUsers';
import { informationBasesList } from 'app/modules/informationBases/reducers/informationBasesList';
import { TAppDispatch } from 'app/redux';
import { AppReduxStoreState } from 'app/redux/types';
import { COLORS } from 'app/utils';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { IReduxForm, ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { ClockLoading } from 'app/views/components/controls';
import { CheckBoxForm, DropDownListWithCheckboxes } from 'app/views/components/controls/forms';
import { DropDownListWithCheckboxItem } from 'app/views/components/controls/forms/DropDownListWithCheckboxes/types';
import CustomizedSwitches from 'app/views/components/controls/Switch/views/SwitchViews';
import { TextOut } from 'app/views/components/TextOut';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { TableViewProps } from 'app/views/modules/_common/components/CommonTableWithFilter/TableView';
import { DbStateLineAccess } from 'app/views/modules/_common/reusable-modules/InformationDatabaseCard/features';
import { AccessInfoDbListFilterForm } from 'app/views/modules/_common/reusable-modules/InformationDatabaseCard/features/AccessInfoDbListFilterForm';
import { AccountDatabaseListFilterDataFormFieldNamesType } from 'app/views/modules/DatabaseList/types';
import React from 'react';
import { connect } from 'react-redux';
import style from './styles.module.css';

export type InfoDbAccessFilterDataForm = {
    searchLineUser: string;
    searchUser: string;
};

type StateProps = {
    valueCheck: string[];
    users: TGetExternalUser[];
    accessInfoDb: TDatabaseAccesses[];
    informationBase?: TGetInformationBaseInfoResponse;
    availability?: TGetAvailability;
    profilesData: TGetAccountProfileResponse[];
    givenProfilesData: TGetGivenAccountProfilesResponse | null;
    requestData: TPermissions[];
    locale: TLocale;
    currentUserGroups: AccountUserGroup[];
};

type DispatchProps = {
    dispatchGetAccountProfileList: (locale?: TLocale) => void;
    dispatchGetGivenAccountProfilesList: (userId?: string, databaseId?: string, needShowLoading?: boolean) => void;
    dispatchChangeProfileRequestData: (data: TChangeRequestDataPayload) => void;
    dispatchChangeValueCheck: (data: string[]) => void;
    dispatchUsersList: (data: TGetExternalUser[]) => void;
    dispatchGetAccess: (data: TGetAccessRequest) => void;
    dispatchGetAccessPrice: (data: TGetAccessPriceRequest) => void;
    dispatchOnResetAddUser: (isClose?: boolean) => void;
};

type GetProps = ReduxFormProps<InfoDbAccessFilterDataForm> & {
    showAccessOrPayDialog: () => void;
};

type OwnState = {
    isFirstShowWarning: boolean;
    currentPage: number;
    tabValue: number;
    isOpenProfileSelect: boolean;
};

type AllProps = GetProps & StateProps & DispatchProps & FloatMessageProps;

const pageSize = 10;

const { getAccountProfilesList, getGivenAccountProfilesList } = REDUX_API.ACCOUNT_USERS_REDUX;
const { getAccessPrice, getAccess } = REDUX_API.INFORMATION_BASES_REDUX;
const { changeValueCheck, changeUsersList, onResetAddUser } = informationBasesList.actions;
const { changeRequestData } = getGivenAccountProfilesListSlice.actions;

export class AccessViewClass extends React.Component<AllProps, OwnState> {
    private intervalId: NodeJS.Timeout | number;

    private readonly emptyUuid = getEmptyUuid();

    private readonly isOnDelimiters = !!this.props.informationBase?.database.isDbOnDelimiters;

    public constructor(props: AllProps) {
        super(props);
        this.state = {
            tabValue: 0,
            currentPage: 1,
            isFirstShowWarning: true,
            isOpenProfileSelect: false
        };

        this.intervalId = setInterval(() => void 0, TimerHelper.waitTimeRefreshData);

        this.onValueChange = this.onValueChange.bind(this);
        this.onTabChange = this.onTabChange.bind(this);
        this.checkStateAccess = this.checkStateAccess.bind(this);
        this.getFilterContentView = this.getFilterContentView.bind(this);
        this.profileChangeHandler = this.profileChangeHandler.bind(this);
        this.currentPageHandler = this.currentPageHandler.bind(this);
        this.getProfile = this.getProfile.bind(this);
        this.addUsers = this.addUsers.bind(this);
        this.onValueChangeCheckBox = this.onValueChangeCheckBox.bind(this);
        this.onValueChangeAll = this.onValueChangeAll.bind(this);
        this.calculateCost = this.calculateCost.bind(this);
    }

    public componentDidMount(): void {
        this.checkStateAccess();

        this.props.dispatchGetAccess({ databaseId: this.props.informationBase?.database.id ?? '' });

        if (this.isOnDelimiters) {
            this.props.dispatchGetAccountProfileList(this.props.locale);
            this.props.dispatchGetGivenAccountProfilesList('', this.props.informationBase?.database.id);
        }
    }

    public componentDidUpdate(prevProps: AllProps) {
        if (
            (!prevProps.valueCheck.length && this.props.valueCheck.length) ||
            (
                this.isOnDelimiters &&
                this.props.givenProfilesData &&
                this.props.givenProfilesData.permissions.find(permission => permission.state === 'Processing')
            )
        ) {
            clearInterval(this.intervalId);
            this.checkStateAccess();
        }

        if (prevProps.valueCheck.length !== this.props.valueCheck.length && !this.isOnDelimiters) {
            void this.calculateCost();
        }
    }

    public componentWillUnmount(): void {
        clearInterval(this.intervalId);
    }

    private onTabChange(value: number) {
        this.props.dispatchOnResetAddUser();
        this.setState({ tabValue: value, currentPage: 1 });
        this.props.dispatchGetAccess({
            databaseId: this.props.informationBase?.database.id ?? '',
            searchString: this.props.reduxForm.getReduxFormFields(false).searchLineUser,
            usersByAccessFilterType: value
        });
    }

    private onValueChange<TValue, TForm extends string = AccountDatabaseListFilterDataFormFieldNamesType>(formName: TForm, newValue: TValue) {
        this.props.reduxForm.updateReduxFormFields({
            [formName]: newValue
        });
        this.setState({ currentPage: 1 });
    }

    private onValueChangeCheckBox(value: string) {
        const isValueInclude = this.props.valueCheck.includes(value);

        this.props.dispatchChangeValueCheck(!isValueInclude
            ? [...this.props.valueCheck, value]
            : this.props.valueCheck.filter(check => check !== value)
        );
    }

    private onValueChangeAll() {
        this.props.dispatchChangeValueCheck(this.props.accessInfoDb.length === this.props.valueCheck.length
            ? []
            : this.props.accessInfoDb.map(access => access.userId)
        );
    }

    private getFilterContentView(ref: React.Ref<IReduxForm<object>>, onFilterChanged: (filterData: object, filterFormName: string) => void) {
        return (
            <AccessInfoDbListFilterForm
                addUsers={ this.addUsers }
                ref={ ref }
                isFilterFormDisabled={ false }
                onFilterChanged={ onFilterChanged }
                onTabChange={ this.onTabChange }
                onValueChange={ this.onValueChange }
                tabValue={ this.state.tabValue }
                isOnDelimiters={ this.isOnDelimiters }
            />
        );
    }

    private getProfile(_: boolean, rowData: TDatabaseAccesses) {
        const { profilesData, givenProfilesData, informationBase, requestData } = this.props;

        const isInProcessing = !!givenProfilesData?.permissions.find(givenProfile => givenProfile.id === rowData.userId && givenProfile.state === 'Processing');
        const isInError = !!givenProfilesData?.permissions.find(givenProfile => givenProfile.id === rowData.userId && givenProfile.state === 'Error');
        const changeDatabaseStatusInfo = this.props.requestData.find(requestInfo => requestInfo['user-id'] === rowData.userId);
        const givenProfileInfo = givenProfilesData?.permissions.find(givenProfile => givenProfile.id === rowData.userId);
        const isSwitchChecked = !!(changeDatabaseStatusInfo?.['allow-backup'] ?? givenProfileInfo?.allowBackup);

        return (
            <Box display="flex" flexDirection="column" gap="5px" marginY="5px">
                <DropDownListWithCheckboxes
                    hiddenSelectAll={ true }
                    handleOpenSelect={ () => {
                        this.setState({ isOpenProfileSelect: true });
                        this.onValueChangeCheckBox(rowData.userId);
                    } }
                    handleCloseSelect={ () => {
                        this.setState({ isOpenProfileSelect: false });
                        if (requestData.find(user => user['user-id'] === rowData.userId)) {
                            void this.calculateCost();
                            this.props.showAccessOrPayDialog();
                        }
                    } }
                    noUpdateValueChangeHandler={ true }
                    deepCompare={ true }
                    disabled={ isInProcessing }
                    selectAllText="Выбрать все пользовательские роли"
                    additionalElement={
                        <Box display="flex" justifyContent="space-between" alignItems="center" width="100%">
                            <TextOut fontWeight={ 500 }>Разрешить выгрузку базы</TextOut>
                            <CustomizedSwitches
                                onChange={ () => {
                                    if (this.props.requestData.find(data => data['user-id'] === rowData.userId)) {
                                        this.props.dispatchChangeProfileRequestData({ userId: rowData.userId, databaseId: informationBase?.database.id ?? '', allowBackup: !isSwitchChecked });
                                    } else {
                                        this.props.dispatchChangeProfileRequestData({ userId: rowData.userId, profiles: givenProfileInfo?.profiles, databaseId: informationBase?.database.id ?? '', allowBackup: !isSwitchChecked });
                                    }
                                } }
                                checked={ isSwitchChecked }
                                disabled={
                                    isInError ||
                                    !(changeDatabaseStatusInfo ? changeDatabaseStatusInfo?.profiles.find(profile => profile.admin) : givenProfileInfo?.profiles.find(profile => profile.admin)) ||
                                    isInProcessing
                                }
                            />
                        </Box>
                    }
                    renderValue={ selected => (
                        <Box sx={ { display: 'flex', flexWrap: 'wrap', gap: 0.5 } }>
                            {
                                selected.map(({ value, text }) => {
                                    const stateProfile = givenProfileInfo?.profiles.find(profile => profile.id === value);

                                    return !stateProfile?.error
                                        ? <Chip key={ value } label={ text } />
                                        : (
                                            <Tooltip key={ value } placement="top" title={ stateProfile.description }>
                                                <Chip label={ text } sx={ { color: COLORS.error } } />
                                            </Tooltip>
                                        );
                                })
                            }
                        </Box>
                    ) }
                    items={
                        profilesData.flatMap(profile => {
                            if (profile.code === informationBase?.database.configurationCode) {
                                return profile.profiles.flatMap(pr => {
                                    const currentProfile = {
                                        text: pr.name,
                                        value: pr.id,
                                        checked: rowData.hasAccess && changeDatabaseStatusInfo
                                            ? !!changeDatabaseStatusInfo.profiles.find(profileInfo => profileInfo.id === this.emptyUuid ? profileInfo.name === pr.name : profileInfo.id === pr.id)
                                            : !!givenProfileInfo?.profiles.find(profileInfo => profileInfo.id === this.emptyUuid ? profileInfo.name === pr.name : profileInfo.id === pr.id) ||
                                            !!this.props.requestData.find(data => data['user-id'] === rowData.userId)?.profiles.find(({ id, name }) => id === this.emptyUuid ? name === pr.name : id === pr.id)
                                    };

                                    if (!pr.predefined) {
                                        if (pr.databases.includes(informationBase?.database.id)) {
                                            return currentProfile;
                                        }

                                        return [];
                                    }

                                    return currentProfile;
                                });
                            }

                            return [];
                        })
                    }
                    label=""
                    formName="profiles"
                    onValueChange={ (_formName, items) => this.profileChangeHandler(items, rowData.userId) }
                />
                {
                    isInProcessing && (
                        <Box display="flex" gap="5px">
                            <ClockLoading />
                            <TextOut fontSize={ 10 } style={ { color: COLORS.warning } }>В процессе редактирования</TextOut>
                        </Box>
                    )
                }
                { isInError && <TextOut fontSize={ 10 } style={ { color: COLORS.error } }>Ошибка редактирования</TextOut> }
            </Box>
        );
    }

    private addUsers(user: TGetExternalUser) {
        this.props.dispatchUsersList([...this.props.users, user]);
        this.props.dispatchChangeValueCheck(this.isOnDelimiters ? this.props.valueCheck : [...this.props.valueCheck, user.userId ?? '']);
    }

    private checkStateAccess() {
        if (
            this.props.accessInfoDb.filter(i => i.state === AccountDatabaseAccessState.ProcessingGrant || i.state === AccountDatabaseAccessState.ProcessingDelete).length ||
            (
                this.props.givenProfilesData &&
                this.props.givenProfilesData.permissions.find(permission => permission.state === 'Processing')
            )
        ) {
            this.intervalId = setInterval(() => {
                if (this.isOnDelimiters && !this.state.isOpenProfileSelect) {
                    this.props.dispatchGetGivenAccountProfilesList('', this.props.informationBase?.database.id, false);
                }
            }, TimerHelper.waitTimeRefreshData);
        } else {
            clearInterval(this.intervalId);
        }
    }

    private async calculateCost() {
        const userWithHasAccessId = this.props.accessInfoDb.reduce((userWithHasAccess: string[], user) => {
            if (user.hasAccess) {
                userWithHasAccess.push(user.userId);
            }

            return userWithHasAccess;
        }, []);

        const removedAccessUserList = userWithHasAccessId.reduce<TAccountUserDataModelsListItem[]>((userList, userId) => {
            if (!this.props.valueCheck.includes(userId)) {
                userList.push({
                    userId,
                    hasAccess: false,
                    state: 4,
                    hasLicense: false
                });
            }

            return userList;
        }, []);

        const accountUserDataModelsList = this.props.valueCheck.reduce<TAccountUserDataModelsListItem[]>((userList, userId) => {
            if (!userWithHasAccessId.includes(userId)) {
                userList.push({
                    userId,
                    hasAccess: true,
                    state: 4,
                    hasLicense: false
                });
            }

            return userList;
        }, []);

        if (
            this.props.currentUserGroups.includes(AccountUserGroup.AccountAdmin) ||
            this.props.currentUserGroups.includes(AccountUserGroup.Hotline) ||
            this.props.currentUserGroups.includes(AccountUserGroup.CloudAdmin) ||
            this.props.currentUserGroups.includes(AccountUserGroup.CloudSE)
        ) {
            this.props.dispatchGetAccessPrice({
                accountUserDataModelsList: removedAccessUserList.concat(accountUserDataModelsList),
                serviceTypesIdsList: !this.props.informationBase?.database.isFile && !this.props.informationBase?.database.isDbOnDelimiters
                    ? [this.props.informationBase?.database.myDatabasesServiceTypeId ?? '', this.props.availability?.accessToServerDatabaseServiceTypeId ?? '']
                    : [this.props.informationBase?.database.myDatabasesServiceTypeId ?? '']
            });
        }
    }

    private profileChangeHandler(items: DropDownListWithCheckboxItem[], userId: string) {
        const { dispatchChangeProfileRequestData, profilesData, informationBase, requestData, givenProfilesData } = this.props;

        if (profilesData) {
            const configurationProfiles = profilesData.find(({ code }) => informationBase?.database.configurationCode === code);

            if (configurationProfiles) {
                const { adminRole, userRole } = configurationProfilesToRole(configurationProfiles.profiles, items);

                const prevProfiles = requestData?.find(data => data['user-id'] === userId)?.profiles ?? givenProfilesData?.permissions.find(el => el.id === userId)?.profiles;

                if (items.length > 1 && this.state.isFirstShowWarning && adminRole.length && userRole.length) {
                    this.props.floatMessage.show(MessageType.Info, 'Установка профиля доступа "Администратор" одновременно с другими профилями через личный кабинет не поддерживается');
                    this.setState({ isFirstShowWarning: false });
                }

                if (prevProfiles && prevProfiles.some(profile => profile.admin)) {
                    dispatchChangeProfileRequestData({ profiles: userRole.length ? userRole : adminRole, userId, databaseId: informationBase?.database.id ?? '' });
                } else {
                    dispatchChangeProfileRequestData({ profiles: adminRole.length ? adminRole : userRole, userId, databaseId: informationBase?.database.id ?? '' });
                }
            }
        }
    }

    private currentPageHandler(_: React.ChangeEvent<unknown>, page: number) {
        this.setState({ currentPage: page });
    }

    public render() {
        const { currentPage } = this.state;
        const { givenProfilesData, profilesData, valueCheck, users } = this.props;
        const reduxForm = this.props.reduxForm.getReduxFormFields(false);
        const dataset = reduxForm.searchLineUser
            ? [...users, ...this.props.accessInfoDb].filter(item =>
                item.userLogin?.toLowerCase().includes(reduxForm.searchLineUser.toLowerCase()) ||
                item.userEmail?.toLowerCase().includes(reduxForm.searchLineUser.toLowerCase()) ||
                item.userFullName?.toLowerCase().includes(reduxForm.searchLineUser.toLowerCase())
            )
            : [...users, ...this.props.accessInfoDb];

        const tableProps: TableViewProps<TDatabaseAccesses | TGetExternalUser> = {
            dataset: dataset.slice(pageSize * (currentPage - 1), pageSize * currentPage),
            keyFieldName: 'userId',
            fieldsView: {
                userId: {
                    caption: <div />,
                    format: () => <div />
                },
                userLogin: {
                    caption: 'Логин (ФИО)',
                    fieldContentNoWrap: false,
                    isSortable: false,
                    format: (value: string, item: TDatabaseAccesses) =>
                        <Box>
                            <TextOut inDiv={ true }>{ value }</TextOut>
                            <TextOut inDiv={ true }>{ item.userFullName ? `(${ item.userFullName })` : '' }</TextOut>
                            <DbStateLineAccess extensionAccess={ item.state } />
                        </Box>
                },
                userEmail: {
                    caption: 'Эл. адрес',
                    fieldContentNoWrap: false,
                    isSortable: false,
                    format: (value: string, item: TDatabaseAccesses) =>
                        <>
                            <TextOut inDiv={ true } fontSize={ 13 }>
                                { value }
                            </TextOut>
                            {
                                (Object.hasOwn(item, 'accessCost') || item.isExternalAccess) && (
                                    <TextOut inDiv={ true } style={ { color: COLORS.error, fontSize: '10px' } }>
                                        { item.accountInfo }
                                    </TextOut>
                                )
                            }
                        </>
                }
            }
        };

        if (!this.isOnDelimiters) {
            tableProps.fieldsView.userId = {
                caption: <CheckBoxForm
                    className={ style.check }
                    formName="isDefault"
                    isReadOnly={
                        !this.props.availability?.permissionsForWorkingWithDatabase.hasPermissionForDisplayPayments ||
                        (this.props.currentUserGroups.length === 1 && this.props.currentUserGroups.includes(AccountUserGroup.AccountSaleManager))
                    }
                    label=""
                    isChecked={ ([...this.props.accessInfoDb, ...users].length === valueCheck.length) }
                    onValueChange={ () => this.onValueChangeAll() }
                />,
                fieldContentNoWrap: false,
                fieldWidth: '3%',
                format: (value: string, item: TDatabaseAccesses) =>
                    <CheckBoxForm
                        className={ style.check }
                        formName={ value }
                        isReadOnly={
                            item.state === AccountDatabaseAccessState.ProcessingGrant || item.state === AccountDatabaseAccessState.ProcessingDelete ||
                            !this.props.availability?.permissionsForWorkingWithDatabase.hasPermissionForDisplayPayments ||
                            (this.props.currentUserGroups.length === 1 && this.props.currentUserGroups.includes(AccountUserGroup.AccountSaleManager))
                        }
                        label=""
                        isChecked={ valueCheck.includes(value) }
                        onValueChange={ () => this.onValueChangeCheckBox(value) }
                    />
            };
        }

        if (this.isOnDelimiters && givenProfilesData && profilesData) {
            tableProps.fieldsView.hasAccess = {
                caption: 'Профиль доступа',
                fieldWidth: '50%',
                format: this.getProfile
            };
        }

        return (
            <>
                <CommonTableWithFilter
                    uniqueContextProviderStateId="AccessInfoDbListContextProviderStateId"
                    filterProps={ { getFilterContentView: this.getFilterContentView } }
                    tableProps={ tableProps }
                />
                <Box gap="5px" justifyContent="flex-end" display="flex" marginTop="10px">
                    <Pagination page={ currentPage } count={ Math.ceil(dataset.length / pageSize) } onChange={ this.currentPageHandler } />
                </Box>
            </>
        );
    }
}

export const AccessViewConnect = connect<StateProps, DispatchProps, GetProps, AppReduxStoreState>(
    state => {
        const { informationBase, availability, valueCheck, users, accessInfoDb, accessInfoInit } = state.InformationBases.informationBasesList;
        const { accountProfilesListReducer: { data: profilesData }, givenAccountProfilesListReducer: { data: givenProfilesData, requestData } } = state.AccountUsersState;
        const { locale, currentUserGroups } = state.Global.getCurrentSessionSettingsReducer.settings.currentContextInfo;

        return {
            informationBase,
            availability,
            profilesData: profilesData ?? [],
            givenProfilesData,
            requestData,
            locale,
            currentUserGroups,
            valueCheck,
            users,
            accessInfoDb,
            accessInfoInit
        };
    },
    (dispatch: TAppDispatch) => {
        return {
            dispatchChangeProfileRequestData: (data: TChangeRequestDataPayload) => dispatch(changeRequestData(data)),
            dispatchGetAccountProfileList: (locale?: TLocale) => getAccountProfilesList(dispatch, locale),
            dispatchGetGivenAccountProfilesList: (userId?: string, databaseId?: string, needShowLoading?: boolean) => getGivenAccountProfilesList(dispatch, userId, databaseId, needShowLoading),
            dispatchChangeValueCheck: (data: string[]) => dispatch(changeValueCheck(data)),
            dispatchUsersList: (data: TGetExternalUser[]) => dispatch(changeUsersList(data)),
            dispatchGetAccessPrice: (data: TGetAccessPriceRequest) => getAccessPrice(dispatch, data),
            dispatchGetAccess: (data: TGetAccessRequest) => getAccess(dispatch, data),
            dispatchOnResetAddUser: (isClose?: boolean) => dispatch(onResetAddUser({ isClose })),
        };
    }
)(withFloatMessages(AccessViewClass));

export const Access = withReduxForm(AccessViewConnect, {
    reduxFormName: 'Access_Info_Db_List_Filter_Form',
    resetOnUnmount: true
});