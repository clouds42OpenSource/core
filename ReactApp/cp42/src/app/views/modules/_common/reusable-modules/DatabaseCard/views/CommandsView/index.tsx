import { Box } from '@mui/material';
import { hasComponentChangesFor } from 'app/common/functions';
import { AppReduxStoreState } from 'app/redux/types';
import { CancelPublishDatabaseButton } from 'app/views/modules/_common/reusable-modules/DatabaseCard/views/_buttons/CancelPublishDatabaseButton';
import { DeleteDatabaseButton } from 'app/views/modules/_common/reusable-modules/DatabaseCard/views/_buttons/DeleteDatabaseButton';
import { PublishDatabaseButton } from 'app/views/modules/_common/reusable-modules/DatabaseCard/views/_buttons/PublishDatabaseButton';
import { DatabaseCardCommandVisibilityDataModel } from 'app/web/InterlayerApiProxy/DatabaseCardApiProxy/receiveDatabaseCard/data-models';
import React from 'react';
import { connect } from 'react-redux';

type StateProps = {
    commandVisibility: DatabaseCardCommandVisibilityDataModel;
    usedWebServices: boolean;
    databaseName: string;
    databaseId: string;
};

type AllProps = StateProps;

class CommandsClass extends React.Component<AllProps> {
    public constructor(props: AllProps) {
        super(props);
        this.renderPublishDatabaseButton = this.renderPublishDatabaseButton.bind(this);
        this.renderCancelPublishDatabaseButton = this.renderCancelPublishDatabaseButton.bind(this);
        this.renderDeleteDatabaseButton = this.renderDeleteDatabaseButton.bind(this);
    }

    public shouldComponentUpdate(nextProps: AllProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private renderPublishDatabaseButton() {
        const { isPublishDatabaseCommandVisible, isDatabasePublishingInfoVisible } = this.props.commandVisibility;

        return (isPublishDatabaseCommandVisible || isDatabasePublishingInfoVisible
            ? (
                <PublishDatabaseButton
                    databaseId={ this.props.databaseId }
                    isInPublishingState={ isDatabasePublishingInfoVisible }
                />
            )
            : null
        );
    }

    private renderCancelPublishDatabaseButton() {
        const { isCancelPublishDatabaseCommandVisible, isCanacelingDatabasePublishingInfoVisible } = this.props.commandVisibility;

        return isCancelPublishDatabaseCommandVisible || isCanacelingDatabasePublishingInfoVisible
            ? (
                <CancelPublishDatabaseButton
                    usedWebServices={ this.props.usedWebServices }
                    databaseId={ this.props.databaseId }
                    isInCancelingPublishedDatabaseState={ isCanacelingDatabasePublishingInfoVisible }
                />
            )
            : null;
    }

    private renderDeleteDatabaseButton() {
        const { isDeleteDatabaseCommandVisible } = this.props.commandVisibility;

        return isDeleteDatabaseCommandVisible
            ? (
                <DeleteDatabaseButton
                    databaseId={ this.props.databaseId }
                    databaseName={ this.props.databaseName }
                />
            )
            : null;
    }

    public render() {
        return (
            <Box display="flex" flexDirection="row" gap="16px">
                { this.renderPublishDatabaseButton() }
                { this.renderCancelPublishDatabaseButton() }
                { this.renderDeleteDatabaseButton() }
            </Box>
        );
    }
}

export const CommandsView = connect<StateProps, NonNullable<unknown>, NonNullable<unknown>, AppReduxStoreState>(
    state => {
        const databaseCardState = state.DatabaseCardState;
        const { commandVisibility } = databaseCardState;
        const databaseName = databaseCardState.commonDetails.v82Name;
        const databaseId = databaseCardState.commonDetails.id;
        const { usedWebServices } = databaseCardState.commonDetails;
        return {
            commandVisibility,
            databaseName,
            databaseId,
            usedWebServices
        };
    }
)(CommandsClass);