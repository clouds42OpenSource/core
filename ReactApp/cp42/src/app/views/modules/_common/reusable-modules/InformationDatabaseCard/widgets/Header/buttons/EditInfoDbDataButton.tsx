import { contextAccountId } from 'app/api';
import { FETCH_API } from 'app/api/useFetchApi';
import { REDUX_API } from 'app/api/useReduxApi';
import { getErrorMessage } from 'app/common/functions/getErrorMessage';
import { EMessageType, useAppDispatch, useAppSelector, useFloatMessages, useGetCurrency } from 'app/hooks';
import { getGivenAccountProfilesListSlice } from 'app/modules/accountUsers';
import { informationBasesList } from 'app/modules/informationBases';
import { ContainedButton } from 'app/views/components/controls/Button';
import { Dialog } from 'app/views/components/controls/Dialog';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { BuyAccessDbDialogMessage } from 'app/views/modules/AccountManagement/InformationBases/views/Modal/BuyAccessDbDialogMessage';
import { DatabaseAccessesDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/AccessInfoDbDataModel';
import React, { memo, useEffect, useMemo, useState } from 'react';

type TProps = {
    showAccessOrPayDialog?: boolean;
    cancelClickShowAccessOrPayDialog?: () => void;
};

const { changeDatabaseAccessOnDelimiters } = FETCH_API.ACCOUNT_USERS;
const { tryPayAccesses, putAccessUsers } = FETCH_API.INFORMATION_BASES;
const { getAccess } = REDUX_API.INFORMATION_BASES_REDUX;
const { getGivenAccountProfilesList } = REDUX_API.ACCOUNT_USERS_REDUX;

const { clear } = getGivenAccountProfilesListSlice.actions;
const { onResetAddUser } = informationBasesList.actions;

export const EditInfoDbDataButton = memo(({ cancelClickShowAccessOrPayDialog, showAccessOrPayDialog }: TProps) => {
    const { show } = useFloatMessages();
    const currency = useGetCurrency();

    const dispatch = useAppDispatch();
    const { informationBasesList: { informationBase, availability, accessInfoDb, valueCheck, users, accessPrice } } = useAppSelector(state => state.InformationBases);
    const { requestData: requestDataForDelimiters } = useAppSelector(state => state.AccountUsersState.givenAccountProfilesListReducer);

    const [open, setOpen] = useState(false);
    const [notEnoughMoney, setNotEnoughMoney] = useState(0);
    const [canUsePromisePayment, setCanUsePromisePayment] = useState(false);
    const [loading, setLoading] = useState(false);

    const cost = useMemo(() => accessPrice?.reduce((price, item) => {
        price += item.accessCost;

        return price;
    }, 0), [accessPrice]);

    const sendUserIdAdd = (check: string[], accessDb: DatabaseAccessesDataModel[]) => {
        const userWithHasAccessId = accessDb.flatMap(i => {
            if (i.hasAccess) {
                return i.userId;
            }

            return [];
        });

        return check.filter(i => !userWithHasAccessId.includes(i));
    };

    const sendUserIdRemove = (check: string[], accessDb: DatabaseAccessesDataModel[]) => {
        return accessDb.flatMap(i => {
            if (!check.includes(i.userId) && i.hasAccess) {
                return i.userId;
            }

            return [];
        });
    };

    const changeDialogMessage = () => {
        setOpen(prev => !prev);
    };

    const addIdForPay = () => {
        return (!informationBase?.database.isFile && !informationBase?.database.isDbOnDelimiters)
            ? sendUserIdAdd(valueCheck, accessInfoDb).map(id => {
                return {
                    billingServiceTypeId: availability?.accessToServerDatabaseServiceTypeId ?? '',
                    sponsorship: {
                        i: !accessInfoDb.map(item => item.userId).includes(id),
                        label: '',
                        me: false
                    },
                    status: true,
                    subject: id
                };
            })
            : [];
    };

    const addOrRemoveAccess = async (isPromisePayment?: boolean) => {
        try {
            setLoading(true);
            const ids = sendUserIdAdd(valueCheck, accessInfoDb);

            if (ids.length && cost && cost !== 0) {
                const tryPay = await tryPayAccesses({
                    accountId: contextAccountId(),
                    billingServiceId: availability?.myDatabaseServiceId ?? '',
                    changedUsers: [...ids.map(id => {
                        return {
                            billingServiceTypeId: informationBase?.database.myDatabasesServiceTypeId ?? '',
                            sponsorship: {
                                i: !accessInfoDb.map(item => item.userId).includes(id),
                                label: '',
                                me: false
                            },
                            status: true,
                            subject: id
                        };
                    }
                    ), ...addIdForPay()],
                    usePromisePayment: isPromisePayment ?? false
                });

                if (tryPay.success && tryPay.data) {
                    if (!tryPay.data.isComplete) {
                        setOpen(prev => !prev);
                        setNotEnoughMoney(tryPay.data?.notEnoughMoney ?? 0);
                        setCanUsePromisePayment(tryPay.data?.canUsePromisePayment ?? false);
                        setLoading(false);

                        return;
                    }
                } else {
                    show(EMessageType.error, tryPay.message);
                }

                setOpen(false);
                if (cancelClickShowAccessOrPayDialog) {
                    cancelClickShowAccessOrPayDialog();
                }
            }

            if (informationBase && informationBase.database && informationBase.database.isDbOnDelimiters) {
                const { v82Name, templateName, id } = informationBase.database;

                const { data: accessData } = await changeDatabaseAccessOnDelimiters(
                    { permissions: requestDataForDelimiters },
                    requestDataForDelimiters.map(data => {
                        const internalUser = accessInfoDb.find(accessInfo => accessInfo.userId === data['user-id']);
                        const params = internalUser ?? users.find(user => user.userId === data['user-id']);

                        return {
                            databaseName: templateName,
                            databaseId: id,
                            v82Name,
                            userId: params?.userId ?? data['user-id'],
                            userName: params?.userLogin ?? '',
                            isExternal: !internalUser
                        };
                    })
                );

                if (accessData && accessData.results) {
                    accessData.results.forEach(statusResult => {
                        if (statusResult.status === 'failure') {
                            show(EMessageType.warning, statusResult.error ?? `Не удалось отредактировать права в базу ${ statusResult['application-id'] }`);
                        }
                    });
                }

                if (accessData && accessData.message) {
                    show(EMessageType.warning, accessData.message);
                } else {
                    show(EMessageType.success, 'Изменение доступов будет выполнено в течение 1 минуты. Пожалуйста, подождите');
                }

                setLoading(false);
            } else {
                const resultAdd = await putAccessUsers({
                    databaseId: informationBase?.database.id ?? '',
                    usersId: sendUserIdAdd(valueCheck, accessInfoDb) || [],
                    giveAccess: true
                });
                const resultDelete = await putAccessUsers({
                    databaseId: informationBase?.database.id ?? '',
                    usersId: sendUserIdRemove(valueCheck, accessInfoDb),
                    giveAccess: false
                });

                if (!resultAdd.data?.filter(i => !i.success).length && !resultDelete.data?.filter(i => !i.success).length) {
                    show(EMessageType.success, 'Изменение доступов будет выполнено в течение 1 минуты. Пожалуйста, подождите', 2000);
                }

                if (resultAdd.data?.filter(i => !i.success).length) {
                    show(EMessageType.error, resultAdd.data?.filter(i => !i.success)[0].message);
                }

                if (resultDelete.data?.filter(i => !i.success).length) {
                    show(EMessageType.error, resultDelete.data?.filter(i => !i.success)[0].message);
                }
            }

            setTimeout(() => {
                if (!informationBase?.database.isDbOnDelimiters) {
                    dispatch(onResetAddUser({}));
                }

                void getAccess(dispatch, { databaseId: informationBase?.database.id ?? '' });

                getGivenAccountProfilesList(dispatch, '', informationBase?.database.id);
            }, 0);
        } catch (e) {
            show(EMessageType.error, getErrorMessage(e));
        }

        setOpen(false);
        dispatch(onResetAddUser({}));

        setLoading(false);
    };

    useEffect(() => {
        if (cost === 0 && informationBase?.database.isDbOnDelimiters) {
            if (cancelClickShowAccessOrPayDialog) {
                cancelClickShowAccessOrPayDialog();
            }

            void addOrRemoveAccess(false);
        }
    }, [cost, informationBase?.database.isDbOnDelimiters]);

    return (
        <>
            { loading && <LoadingBounce /> }
            { informationBase?.database.isDbOnDelimiters && !!cost && (
                <Dialog
                    isOpen={ showAccessOrPayDialog ?? false }
                    dialogVerticalAlign="center"
                    dialogWidth="sm"
                    buttons={ [
                        {
                            onClick: () => {
                                if (cancelClickShowAccessOrPayDialog) {
                                    cancelClickShowAccessOrPayDialog();
                                }
                                dispatch(onResetAddUser({ isClose: true }));
                                dispatch(clear());
                            },
                            variant: 'contained',
                            kind: 'default',
                            content: 'Отменить',
                        },
                        {
                            onClick: () => addOrRemoveAccess(false),
                            variant: 'contained',
                            kind: 'success',
                            content: `Купить (${ cost } ${ currency }.)`,
                        }
                    ] }
                >
                    Для предоставления пользователю {
                        users.find(user => user.userId === requestDataForDelimiters[0]['user-id'])?.userFullName ?? ''
                    } доступа в базу &quot;{ informationBase?.database.v82Name }&quot; необходимо оплатить { cost } { currency }
                </Dialog>
            ) }
            { !informationBase?.database.isDbOnDelimiters && (
                <ContainedButton
                    isEnabled={ true }
                    kind="success"
                    onClick={ () => addOrRemoveAccess(false) }
                >
                    <span>{ cost ? `Купить (${ cost } ${ currency }. )` : 'Сохранить' }</span>
                </ContainedButton>
            ) }
            <BuyAccessDbDialogMessage
                loading={ loading }
                usePromise={ true }
                isOpen={ open }
                onNoClick={ changeDialogMessage }
                onYesClick={ () => {
                    setLoading(true);
                    void addOrRemoveAccess(true);
                } }
                notEnoughMoney={ notEnoughMoney }
                canUsePromisePayment={ canUsePromisePayment }
            />
        </>
    );
});