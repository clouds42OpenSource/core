import { AccountUserGroup } from 'app/common/enums';
import { getErrorMessage } from 'app/common/functions/getErrorMessage';
import { AppReduxStoreState } from 'app/redux/types';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { InfoDbCardEditDatabaseDataForm } from 'app/views/modules/AccountManagement/InformationBases/types/InfiDbCardEditDatabaseDataForm';
import { GetAvailabilityInfoDb } from 'app/web/api/InfoDbProxy/responce-dto/GetAvailabilityInfoDb';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { InfoDbListItemDataModel } from 'app/web/InterlayerApiProxy/InfoDbApiProxy/receiveDatabaseList/data-models';
import { InfoDbCardAccountDatabaseInfoDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/InfoDbCardAccountDatabaseInfoDataModel';
import { RightsInfoDbCardDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getRightsInfoDbCard/data-models/InfoDbCardAccountDatabaseInfoDataModel';
import { RequestKind } from 'core/requestSender/enums';
import React from 'react';
import { connect } from 'react-redux';
import { FieldsCommonCardView } from './FieldsCommonCardView';

type OwnProps = FloatMessageProps & ReduxFormProps<InfoDbCardEditDatabaseDataForm>;

type StateProps = {
    commonDetails: InfoDbCardAccountDatabaseInfoDataModel;
    availability: GetAvailabilityInfoDb;
    currentUserGroups: AccountUserGroup[];
};

type GetProps = {
    permissions: RightsInfoDbCardDataModel;
    item?: InfoDbListItemDataModel | null;
    isOther: boolean;
    onChangeField: () => void;
    isAdmin?: boolean;
};

type AllProps = OwnProps & StateProps & GetProps;

type OwnState = {
    errorV82Name: string;
    changeWebServices: boolean;
};

class CommonTabViewClass extends React.Component<AllProps, OwnState> {
    public constructor(props: AllProps) {
        super(props);
        this.state = {
            errorV82Name: '',
            changeWebServices: false
        };

        this.initReduxForm = this.initReduxForm.bind(this);
        this.onValueChange = this.onValueChange.bind(this);
        this.onChangeV82Name = this.onChangeV82Name.bind(this);
        this.onValueChangeWebServices = this.onValueChangeWebServices.bind(this);
        this.onValueChangeImprovements = this.onValueChangeImprovements.bind(this);
        this.onValueChangePlatformInfoDb = this.onValueChangePlatformInfoDb.bind(this);
        props.reduxForm.setInitializeFormDataAction(this.initReduxForm);
    }

    private async onChangeV82Name() {
        const infoDbCardApi = InterlayerApiProxy.getInfoDbApi();

        if (
            this.props.commonDetails.caption !== this.props.reduxForm.getReduxFormFields(false).v82Name &&
            this.props.reduxForm.getReduxFormFields(false).v82Name &&
            this.props.commonDetails.v82Name !== this.props.reduxForm.getReduxFormFields(false).v82Name
        ) {
            try {
                this.setState({
                    errorV82Name: ''
                });
                const res = await infoDbCardApi.editInfoDbList(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, {
                    databaseId: this.props.commonDetails.id,
                    v82Name: this.props.reduxForm.getReduxFormFields(false).v82Name,
                });

                if (!res.Success) {
                    this.setState({
                        errorV82Name: res.Message!
                    });

                    return;
                }

                this.props.reduxForm.resetReduxForm();
            } catch (e) {
                this.props.floatMessage.show(MessageType.Error, getErrorMessage(e));
            }

            this.props.onChangeField();
        }

        if (this.props.commonDetails.caption === this.props.reduxForm.getReduxFormFields(false).v82Name) {
            this.props.floatMessage.show(MessageType.Error, 'Номер базы не может совпадать с названием базы');
        }
    }

    private async onValueChangeWebServices() {
        const infoDbCardApi = InterlayerApiProxy.getInfoDbApi();

        this.setState({
            changeWebServices: true
        });

        await infoDbCardApi.editInfoDbList(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, {
            databaseId: this.props.commonDetails.id,
            usedWebServices: !this.props.reduxForm.getReduxFormFields(false).usedWebServices
        });

        this.props.onChangeField();

        this.setState({
            changeWebServices: false
        });
    }

    private async onValueChangeImprovements() {
        const infoDbCardApi = InterlayerApiProxy.getInfoDbApi();

        await infoDbCardApi.editInfoDbList(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, {
            databaseId: this.props.commonDetails.id
        });

        this.props.onChangeField();
    }

    private async onValueChangePlatformInfoDb() {
        const infoDbCardApiCard = InterlayerApiProxy.getInfoDbCardApi();

        await infoDbCardApiCard.editPlatformInfoDbCard(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, {
            databaseId: this.props.commonDetails.id,
            platformType: this.props.reduxForm.getReduxFormFields(false).platformType,
            distributionType: this.props.reduxForm.getReduxFormFields(false).distributionType,
        });

        this.props.onChangeField();
    }

    private async onValueChange<TValue>(fieldName: string, newValue?: TValue, _prevValue?: TValue) {
        const infoDbCardApi = InterlayerApiProxy.getInfoDbApi();
        const infoDbCardApi1 = InterlayerApiProxy.getInfoDbCardApi();

        try {
            if (fieldName === 'platformType') {
                await infoDbCardApi1.editPlatformInfoDbCard(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, {
                    databaseId: this.props.commonDetails.id,
                    platformType: +newValue!,
                    distributionType: this.props.reduxForm.getReduxFormFields(false).distributionType
                });
                this.props.onChangeField();
            } else if (fieldName === 'distributionType') {
                await infoDbCardApi1.editPlatformInfoDbCard(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, {
                    databaseId: this.props.commonDetails.id,
                    platformType: this.props.reduxForm.getReduxFormFields(false).platformType,
                    distributionType: +newValue!
                });
                this.props.onChangeField();
            } else if (fieldName !== 'v82Name') {
                await infoDbCardApi.editInfoDbList(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, {
                    databaseId: this.props.commonDetails.id,
                    [fieldName]: newValue,
                });
                this.props.onChangeField();
            }
        } catch (e) {
            this.props.floatMessage.show(MessageType.Error, getErrorMessage(e));
        }

        this.props.reduxForm.updateReduxFormFields({
            [fieldName]: newValue
        });
    }

    private initReduxForm(): InfoDbCardEditDatabaseDataForm | undefined {
        return {
            databaseId: this.props.commonDetails.id,
            v82Name: this.props.commonDetails.v82Name,
            databaseCaption: this.props.commonDetails.caption,
            platformType: this.props.commonDetails.platformType,
            distributionType: this.props.commonDetails.distributionType,
            databaseState: this.props.commonDetails.databaseState,
            usedWebServices: this.props.commonDetails.usedWebServices,
            databaseTemplateId: this.props.commonDetails.dbTemplate,
            restoreModel: this.props.commonDetails.restoreModelType,
        };
    }

    public render() {
        return (
            <FieldsCommonCardView
                availability={ this.props.availability }
                onChangeField={ this.props.onChangeField }
                onChangeV82Name={ this.onChangeV82Name }
                permissions={ this.props.permissions }
                reduxForm={ this.props.reduxForm.getReduxFormFields(false) }
                item={ this.props.item }
                commonDetails={ this.props.commonDetails }
                onValueChange={ this.onValueChange }
                onValueChangeWebServices={ this.onValueChangeWebServices }
                errorV82Name={ this.state.errorV82Name }
                changeWebServices={ this.state.changeWebServices }
                isOther={ this.props.isOther }
                isAdmin={ this.props.isAdmin }
                currentUserGroups={ this.props.currentUserGroups }
            />
        );
    }
}

export const CommonTabViewConnected = connect<StateProps, NonNullable<unknown>, OwnProps, AppReduxStoreState>(
    state => {
        const databaseCardState = state.InfoDbCardState;
        const infoDbListState = state.InfoDbListState;
        const { commonDetails } = databaseCardState;
        const availability = infoDbListState.AvailabilityInfoDb;
        const { hasDatabaseCardReceived } = databaseCardState.hasSuccessFor;
        const { hasDatabaseCardRightsReceived } = databaseCardState.hasSuccessFor;
        const hasSessionSettingsReceived = state.Global.getCurrentSessionSettingsReducer.hasSuccessFor.hasSettingsReceived;
        const currentUserGroups = hasSessionSettingsReceived ? state.Global.getCurrentSessionSettingsReducer.settings.currentContextInfo.currentUserGroups : [];

        return {
            commonDetails,
            hasDatabaseCardReceived,
            hasDatabaseCardRightsReceived,
            availability,
            currentUserGroups
        };
    },
    {}
)(CommonTabViewClass);

export const CommonTabViewFloat = withFloatMessages(CommonTabViewConnected);

export const CommonTabView = withReduxForm(CommonTabViewFloat, {
    reduxFormName: 'InfoDbCard_EditDatabase_Form',
    resetOnUnmount: true
});