import { Box } from '@mui/material';
import { SuccessButton } from 'app/views/components/controls/Button';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { TextOut } from 'app/views/components/TextOut';
import { DESCRIPTION_SUPPORT_SERVICES, FORM_TITLE, PAGE_TITLE, SUPPORT_LOGO } from 'app/views/modules/_common/reusable-modules/BoxWithPicture/constants';
import { FormEvent, ReactNode } from 'react';
import { useLocation } from 'react-router';
import style from './style.module.css';

type TBoxWithPictureProps = {
    children: ReactNode;
    handleSubmit: (e: FormEvent<HTMLFormElement>) => void;
    isLoading?: boolean;
}

export const BoxWithPicture = ({ children, handleSubmit, isLoading }: TBoxWithPictureProps) => {
    const { pathname } = useLocation();

    const logoSrc = SUPPORT_LOGO[pathname];

    return (
        <Box display="flex" gap="16px">
            { isLoading && <LoadingBounce /> }
            <Box flex="1">
                <Box display="flex" flexDirection="column" gap="16px">
                    <TextOut fontSize={ 26 } fontWeight={ 600 }>{ PAGE_TITLE[pathname] }</TextOut>
                    { DESCRIPTION_SUPPORT_SERVICES[pathname].split('\n').map((subscription, index) => <TextOut key={ `subscription-${ index }` } inDiv={ true }>{ subscription }</TextOut>) }
                    <TextOut fontSize={ 16 } fontWeight={ 500 }>{ FORM_TITLE[pathname] ?? '' }</TextOut>
                    <TextOut fontSize={ 12 }>График работы отдела: по будням с 9:00 по 18:00 МСК.</TextOut>
                    <form onSubmit={ handleSubmit }>
                        { children }
                        <SuccessButton type="submit" style={ { marginTop: '8px' } }>Отправить заявку</SuccessButton>
                    </form>
                </Box>
            </Box>
            {
                logoSrc && (
                    <img
                        className={ style.picture }
                        src={ logoSrc }
                        alt="support service logo"
                        loading="lazy"
                    />
                )
            }
        </Box>
    );
};