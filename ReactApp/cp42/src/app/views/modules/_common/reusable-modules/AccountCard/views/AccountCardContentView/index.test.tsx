import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { getNewStore } from 'app/utils/StoreUtility';
import { AccountCardContentView } from 'app/views/modules/_common/reusable-modules/AccountCard/views/AccountCardContentView';
import React from 'react';
import { Provider } from 'react-redux';
import { SnackbarProvider } from 'notistack';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { IReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { AccountCardEditAccountDataForm, AccountCardEditAccountSegmentForm } from 'app/views/modules/_common/reusable-modules/AccountCard/types';

describe('<AccountCardContentView />', () => {
    const mockStore = configureStore([thunk]);

    beforeEach(() => {
        render(
            <SnackbarProvider>
                <Provider store={ mockStore(getNewStore().getState()) }>
                    <AccountCardContentView
                        commonTabRef={ React.createRef<IReduxForm<AccountCardEditAccountDataForm>>() }
                        changeSegmentTabRef={ React.createRef<IReduxForm<AccountCardEditAccountSegmentForm>>() }
                        accountIndexNumber={ 1 }
                    />
                </Provider>
            </SnackbarProvider>
        );
    });

    it('renders', () => {
        expect(screen.getByTestId(/tab-view__default-tabs__content/i)).toBeInTheDocument();
    });
});