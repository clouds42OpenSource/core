import { Box } from '@mui/material';
import { EAccountDatabaseAccessState } from 'app/api/endpoints/accountUsers/enum';
import { FETCH_API } from 'app/api/useFetchApi';
import { REDUX_API } from 'app/api/useReduxApi';
import { TimerHelper } from 'app/common/helpers/TimerHelper';
import { EMessageType, useAppDispatch, useAppSelector, useFloatMessages } from 'app/hooks';
import { COLORS } from 'app/utils';
import { SuccessButton } from 'app/views/components/controls/Button';
import { ClockLoading } from 'app/views/components/controls/ClockLoading';
import { CheckBoxForm } from 'app/views/components/controls/forms';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { TextOut } from 'app/views/components/TextOut';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import React, { useEffect, useMemo, useState } from 'react';

import styles from './styles.module.css';

const { getInformationBases } = REDUX_API.INFORMATION_BASES_REDUX;
const { getExternalUsersList } = REDUX_API.ACCOUNT_DATABASES_REDUX;
const { putDatabaseAccess } = FETCH_API.ACCOUNT_USERS;

type TAccessIsOtherTabView = {
    onCloseHandler: () => void;
};

export const AccessIsOther = ({ onCloseHandler }: TAccessIsOtherTabView) => {
    const dispatch = useAppDispatch();
    const { show } = useFloatMessages();

    const { informationBase, otherParams } = useAppSelector(state => state.InformationBases.informationBasesList);
    const { id } = informationBase?.database ?? {};
    const { isLoading, data, error } = useAppSelector(state => state.AccountDatabasesState.getExternalUsersListReducer);

    const [userIdArray, setUserIdArray] = useState<string[]>([]);

    const dataset = useMemo(() => data?.databaseAccesses ?? [], [data]);

    useEffect(() => {
        if (id) {
            getExternalUsersList(dispatch, id);
        }
    }, [dispatch, id]);

    useEffect(() => {
        if (error) {
            show(EMessageType.error, error);
        }
    }, [error, show]);

    useEffect(() => {
        if (data?.databaseAccesses.find(database => database.state === EAccountDatabaseAccessState.processingDelete || database.state === EAccountDatabaseAccessState.processingGrant)) {
            const interval = setTimeout(() => {
                getExternalUsersList(dispatch, id ?? '', false);
            }, TimerHelper.waitTimeRefreshDatabaseState);

            return () => clearTimeout(interval);
        }

        if (data && data.databaseAccesses.length === 0) {
            onCloseHandler();
            void getInformationBases(dispatch, otherParams, false);
        }
    }, [data, dispatch, id, onCloseHandler, otherParams]);

    const checkboxHandler = (_: string, value: boolean, __: boolean, userId: string) => {
        if (!value) {
            setUserIdArray([...userIdArray, userId]);
        } else {
            const index = userIdArray.indexOf(userId);

            if (index !== -1) {
                setUserIdArray(prevState => {
                    prevState.splice(index, 1);

                    return [...prevState];
                });
            }
        }
    };

    const removeAccessHandler = async () => {
        const resultArray = userIdArray.map(userId => {
            return putDatabaseAccess({
                usersId: userId,
                giveAccess: false,
                databaseIds: [id ?? '']
            });
        });

        for await (const { data: accessData } of resultArray) {
            if (accessData && accessData.length > 0) {
                const { success, message, databasesName } = accessData[0];

                if (!success) {
                    show(EMessageType.warning, `${ message } ${ databasesName ? `(${ databasesName })` : '' }`);
                }
            }
        }

        setUserIdArray([]);
        getExternalUsersList(dispatch, id ?? '');
    };

    return (
        <>
            { isLoading && <LoadingBounce /> }
            <div className={ styles.container }>
                <CommonTableWithFilter
                    uniqueContextProviderStateId="accessItOtherList"
                    tableProps={ {
                        dataset,
                        keyFieldName: 'userId',
                        fieldsView: {
                            userId: {
                                fieldWidth: '4%',
                                caption: 'Доступ',
                                format: (value: string, { hasAccess }) =>
                                    <CheckBoxForm
                                        className={ styles.checkBoxFormWidth }
                                        label=""
                                        formName=""
                                        isChecked={ hasAccess && !userIdArray.includes(value) }
                                        isReadOnly={ !!dataset.find(database => database.state === EAccountDatabaseAccessState.processingDelete || database.state === EAccountDatabaseAccessState.processingGrant) }
                                        onValueChange={ (formName, newValue, prevValue) => checkboxHandler(formName, newValue, prevValue, value) }
                                    />
                            },
                            userLogin: {
                                caption: 'Логин',
                                format: (value: string, { state }) =>
                                    <Box display="flex" flexDirection="column" gap="5px">
                                        <TextOut>{ value }</TextOut>
                                        { state === EAccountDatabaseAccessState.processingDelete &&
                                            <Box display="flex" gap="5px">
                                                <ClockLoading />
                                                <TextOut fontSize={ 10 } style={ { color: COLORS.error } }>В процессе удаления</TextOut>
                                            </Box>
                                        }
                                        { state === EAccountDatabaseAccessState.processingGrant &&
                                            <Box display="flex" gap="5px">
                                                <ClockLoading />
                                                <TextOut fontSize={ 10 } style={ { color: COLORS.main } }>В процессе выдачи</TextOut>
                                            </Box>
                                        }
                                    </Box>
                            },
                            userFullName: {
                                caption: 'ФИО',
                            },
                            userEmail: {
                                caption: 'Эл. адрес',
                            }
                        }
                    } }
                />
                <div className={ styles.footer }>
                    <SuccessButton onClick={ removeAccessHandler } isEnabled={ userIdArray.length > 0 }>Применить</SuccessButton>
                </div>
            </div>
        </>
    );
};