import { Box } from '@mui/system';
import { TBackupsItem } from 'app/api/endpoints/applications/response';
import { FETCH_API } from 'app/api/useFetchApi';
import { AppRoutes } from 'app/AppRoutes';
import { EMessageType, useAppSelector, useFloatMessages } from 'app/hooks';
import { NumberUtility } from 'app/utils';
import { TextOut } from 'app/views/components/TextOut';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { PopperButton } from 'app/views/modules/_common/components/PopperButton';
import dayjs from 'dayjs';
import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import style from './style.module.css';

const { getBackups, getDownloadFile } = FETCH_API.APPLICATIONS;

export const DelimitersArchivalCopies = () => {
    const { show } = useFloatMessages();

    const { database } = useAppSelector(state => state.InformationBases.informationBasesList.informationBase) || {};
    const { from, to } = useAppSelector(state => state.InformationBases.archivalCopiesFilter);

    const [dataset, setDataset] = useState<TBackupsItem[]>([]);

    useEffect(() => {
        (async () => {
            const { success, data, message } = await getBackups(
                database?.id ?? '',
                {
                    from_date: from ? dayjs(from).format('YYYY-MM-DD') : '',
                    to_date: to ? dayjs(to).format('YYYY-MM-DD') : ''
                }
            );

            if (success && data) {
                setDataset(data.backups);
            } else {
                show(EMessageType.error, message);
            }
        })();
    }, [database?.id, from, show, to]);

    const download = async (fileId: string, eternalBackupUrl: string) => {
        if (fileId !== '') {
            const { responseBody, status, headers } = await getDownloadFile(fileId);

            if (status === 204) {
                const retry = headers.get('Retry-After') ?? '0';

                show(EMessageType.info, `Пожалуйста, подождите. Файл подготавливается. Примерное время ожидания ${ retry } секунд.`);
                setTimeout(download, parseInt(`${ retry }000`, 10), fileId, eternalBackupUrl);
            }

            if (status === 200) {
                window.open(responseBody.data?.url);
            }
        } else {
            window.open(eternalBackupUrl, '_blank');
        }
    };

    return (
        <CommonTableWithFilter
            uniqueContextProviderStateId="delimitersDatabaseArchivalCopies"
            tableProps={ {
                dataset,
                keyFieldName: 'id',
                fieldsView: {
                    date: {
                        caption: 'Дата и время',
                        format: date => dayjs(date).format('DD.MM.YYYY HH:mm')
                    },
                    appVersion: {
                        caption: 'Версия конфигурации'
                    },
                    fileSize: {
                        caption: 'Размер',
                        format: fileSize => NumberUtility.bytesToMegabytes(fileSize)
                    },
                    isOndemand: {
                        caption: 'Вид',
                        format: (isOndemand, item) => item.type === 'dt' ? 'Выгрузка dt' : (isOndemand ? 'Ручная' : 'Регламентная')
                    },
                    fileId: {
                        caption: '',
                        format: (fileId, item) => (
                            <PopperButton>
                                <div>
                                    {
                                        database?.state && fileId && item.type !== 'dt' && (
                                            <>
                                                <Link
                                                    to={ {
                                                        pathname: AppRoutes.accountManagement.createAccountDatabase.loadingDatabase,
                                                        state: {
                                                            isZip: true,
                                                            fileId,
                                                            caption: database.caption,
                                                            date: item.date
                                                        }
                                                    } }
                                                    className={ style.link }
                                                >
                                                    Восстановить
                                                </Link>
                                                <Box
                                                    className={ style.link }
                                                    onClick={ () => download(fileId, item.eternalBackupUrl) }
                                                >
                                                    <TextOut fontSize={ 16 }>Скачать</TextOut>
                                                </Box>
                                            </>
                                        )
                                    }
                                </div>
                            </PopperButton>
                        )
                    }
                }
            } }
        />
    );
};