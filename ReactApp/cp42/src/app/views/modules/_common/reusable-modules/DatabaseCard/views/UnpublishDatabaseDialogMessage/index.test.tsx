import '@testing-library/jest-dom/extend-expect';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { UnpublishDatabaseDialogMessage } from 'app/views/modules/_common/reusable-modules/DatabaseCard/views/UnpublishDatabaseDialogMessage';

describe('<UnpublishDatabaseDialogMessage />', () => {
    const onNoClickMock: jest.Mock = jest.fn();
    const onYesClickMock: jest.Mock = jest.fn();

    beforeEach(() => {
        render(<UnpublishDatabaseDialogMessage
            isOpen={ true }
            onNoClick={ onNoClickMock }
            onYesClick={ onYesClickMock }
        />);
    });

    it('renders', () => {
        expect(screen.getByText(/Подтверждение отмены публикации/i)).toBeInTheDocument();
        expect(screen.getByRole(/dialog/i)).toBeInTheDocument();
    });

    it('has onYesClick method', async () => {
        await userEvent.click(screen.getAllByTestId(/button-view/i)[1]);
        expect(screen.getByText('Да')).toBeInTheDocument();
        expect(onYesClickMock).toBeCalled();
    });

    it('has onNoClickMock method', async () => {
        await userEvent.click(screen.getAllByTestId(/button-view/i)[0]);
        expect(screen.getByText('Нет')).toBeInTheDocument();
        expect(onNoClickMock).toBeCalled();
    });
});