import { SelectedDatabasesToMigrateContextState } from 'app/views/modules/_common/reusable-modules/AccountDatabasesForMigrations/context/SelectedDatabasesToMigrateContextState';
import React from 'react';

/**
 * Контекст для хранения информации о выбранных базах для миграции
 */
export const SelectedDatabasesToMigrateContext = React.createContext<SelectedDatabasesToMigrateContextState>(void 0 as any);
SelectedDatabasesToMigrateContext.displayName = 'SelectedDatabasesToMigrateContext';