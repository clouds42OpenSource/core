import { Dialog } from 'app/views/components/controls/Dialog';
import { TextOut } from 'app/views/components/TextOut';
import React from 'react';

type TProps = {
    isOpen: boolean;
    onNoClick: () => void;
    onYesClick: () => void;
};

export const ChangePlatformDbDialog = ({ isOpen, onYesClick, onNoClick }: TProps) => {
    return (
        <Dialog
            title="Смена платформы для информационной базы"
            isTitleSmall={ true }
            isOpen={ isOpen }
            dialogWidth="sm"
            onCancelClick={ onNoClick }
            buttons={ [{
                kind: 'primary',
                content: 'Подтвердить',
                onClick: onYesClick
            },
            {
                kind: 'default',
                content: 'Отмена',
                onClick: onNoClick
            }
            ] }
        >
            <TextOut>
                Вы действительно хотите сменить платформу для текущей информационной базы?
            </TextOut>
        </Dialog>
    );
};