import { Box, Grid } from '@mui/material';
import { FETCH_API } from 'app/api/useFetchApi';
import { hasComponentChangesFor } from 'app/common/functions';
import { createInnValidator } from 'app/common/validators';
import { createEmailValidator } from 'app/common/validators/createEmailValidator';
import { AccountCardProcessId } from 'app/modules/accountCard';
import { ReceiveAccountCardThunkParams } from 'app/modules/accountCard/store/reducers/receiveAccountCardReducer/params';
import { DateUtility, NumberUtility } from 'app/utils';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { ButtonGroup } from 'app/views/components/controls/ButtonGroup';
import { ButtonGroupItem } from 'app/views/components/controls/ButtonGroup/types';
import { CheckBoxForm, TagInputForm, TextBoxForm } from 'app/views/components/controls/forms';
import { ComboBoxForm } from 'app/views/components/controls/forms/ComboBox/ComboBoxForm';
import { ComboboxItemModel } from 'app/views/components/controls/forms/ComboBox/models';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { TextOut } from 'app/views/components/TextOut';
import { ReadOnlyKeyValueView } from 'app/views/modules/_common/components/ReadOnlyKeyValueView';
import { WatchReducerProcessValidationError } from 'app/views/modules/_common/components/WatchReducerProcessValidationError';
import { AccountCardEditAccountDataForm } from 'app/views/modules/_common/reusable-modules/AccountCard/types';
import {
    AccountCardAccountInfoDataModel,
    AccountCardCommonDataDataModel,
    AccountCardEditableFieldsDataModel,
    AccountCardLocaleInfoItemDataModel,
    AccountCardVisibleFieldsDataModel
} from 'app/web/InterlayerApiProxy/AccountCardApiProxy/receiveAccountCard';
import { SuppliersPaginationDataModel } from 'app/web/InterlayerApiProxy/SuppliersApiProxy/getSuppliersWithPagination';
import { ValidationError } from 'app/web/request-submitter/common/RequestSender/ValidationError';
import cn from 'classnames';
import { ProcessIdType } from 'core/redux/types';
import React from 'react';
import css from '../../styles.module.css';

type OwnProps = FloatMessageProps & {
    accountInfo: AccountCardAccountInfoDataModel;
    commonInfo: AccountCardCommonDataDataModel;
    localeArr: AccountCardLocaleInfoItemDataModel[];
    editableFields: AccountCardEditableFieldsDataModel;
    visibleFields: AccountCardVisibleFieldsDataModel;
    onFieldChange: <TValue>(fieldName: string, newValue: TValue, prevValue: TValue) => boolean | void;
    accountDataForm: AccountCardEditAccountDataForm;
    dispatchReceiveAccountCardThunk: (args?: ReceiveAccountCardThunkParams) => void;
    isReadonly?: boolean;
    isCloudAdmin?: boolean;
    isCloudEngineer?: boolean;
    suppliers?: SuppliersPaginationDataModel;
};

type OwnState = {
    isLoading: boolean;
    validationError?: ValidationError;
};

const { editAccountSupplier } = FETCH_API.SUPPLIER_REFERENCE;

class ViewClass extends React.Component<OwnProps, OwnState> {
    private readonly deploymentWaveButtons: ButtonGroupItem[] = [
        {
            index: 0,
            text: 'Не выбрано'
        },
        {
            index: 1,
            text: 'Первая'
        },
        {
            index: 2,
            text: 'Вторая'
        },
        {
            index: 3,
            text: 'Третья'
        }
    ];

    public constructor(props: OwnProps) {
        super(props);
        this.onProcessValidationError = this.onProcessValidationError.bind(this);
        this.changeSupplier = this.changeSupplier.bind(this);
        this.state = {
            isLoading: false,
            validationError: undefined
        };
    }

    public shouldComponentUpdate(nextProps: OwnProps, nextState: OwnState) {
        return hasComponentChangesFor(this.props, nextProps) ||
            hasComponentChangesFor(this.state, nextState);
    }

    private onProcessValidationError(_processId: ProcessIdType, error?: ValidationError) {
        if (this.state.validationError === error) { return; }

        this.setState({
            validationError: error
        });
    }

    private changeSupplier(supplierId: string) {
        const { floatMessage: { show }, dispatchReceiveAccountCardThunk } = this.props;
        this.setState({ isLoading: true });

        editAccountSupplier(supplierId, this.props.accountInfo.id).then(({ error }) => {
            if (error) {
                show(MessageType.Error, error);
            } else {
                show(MessageType.Success, 'Поставщик успешно изменен');
                dispatchReceiveAccountCardThunk({ accountNumber: this.props.accountInfo.indexNumber, force: true, showLoadingProgress: true });
            }

            this.setState({ isLoading: false });
        });
    }

    public render() {
        const { isReadonly, isCloudAdmin, suppliers, accountInfo } = this.props;
        let supplierItems: ComboboxItemModel<string>[] = [];

        if (isCloudAdmin && suppliers) {
            const currentLocale = suppliers.records.find(record => record.id === accountInfo.supplerId)?.localeName;
            supplierItems = suppliers.records.flatMap(record => {
                if (record.localeName === currentLocale) {
                    return { value: record.id, text: record.name };
                }

                return [];
            });
        }

        return (
            <>
                { this.state.isLoading && <LoadingBounce /> }
                <div className={ cn(css['main-container']) } data-testid="view__main-container">
                    <Grid container={ true } spacing={ 2 }>
                        <Grid item={ true } xs={ 12 } sm={ 6 } md={ 6 }>
                            <Box display="flex" flexDirection="column" gap="10px">
                                { this.props.editableFields.isAccountCaptionEditable
                                    ? (
                                        <TextBoxForm
                                            autoFocus={ false }
                                            initialErrors={ this.state.validationError ? this.state.validationError?.errors['Account.AccountCaption'] : undefined }
                                            label="Наименование:"
                                            formName="accountCaption"
                                            value={ this.props.accountDataForm.accountCaption ?? this.props.accountInfo.accountCaption }
                                            maxLength={ 50 }
                                            isReadOnly={ !!isReadonly }
                                            onValueChange={ this.props.onFieldChange }
                                        />
                                    )
                                    : (
                                        <ReadOnlyKeyValueView
                                            label="Наименование:"
                                            text={ this.props.accountInfo.accountCaption }
                                        />
                                    )
                                }
                                { this.props.editableFields.isInnEditable
                                    ? (
                                        <TextBoxForm
                                            autoFocus={ false }
                                            initialErrors={ this.state.validationError ? this.state.validationError?.errors['Account.Inn'] : undefined }
                                            label="Инн:"
                                            formName="inn"
                                            value={ this.props.accountDataForm.inn ?? this.props.accountInfo.inn }
                                            maxLength={ 50 }
                                            isReadOnly={ !!isReadonly }
                                            validator={ createInnValidator() }
                                            onValueChange={ this.props.onFieldChange }
                                        />
                                    )
                                    : (
                                        <ReadOnlyKeyValueView
                                            label="Инн:"
                                            text={ this.props.accountInfo.inn }
                                        />
                                    )
                                }
                                { this.props.isCloudAdmin && supplierItems.length > 0 &&
                                    <ComboBoxForm
                                        onValueChange={ (_, supplierId) => this.changeSupplier(supplierId ?? '') }
                                        label="Поставщик:"
                                        formName="suppliers"
                                        value={ accountInfo.supplerId }
                                        items={ supplierItems }
                                    />
                                }
                            </Box>
                            <ReadOnlyKeyValueView
                                label="Используемое место на диске:"
                                text={ NumberUtility.megabytesToClassicFormat('0 Гб', this.props.commonInfo.usedSizeOnDisk) }
                            />
                            <ReadOnlyKeyValueView
                                label="Дата регистрации:"
                                text={ this.props.accountInfo.registrationDate ? DateUtility.dateToDayMonthYear(this.props.accountInfo.registrationDate) : '' }
                            />
                            <ReadOnlyKeyValueView
                                label="Источник регистрации:"
                                text={ this.props.accountInfo.userSource ?? '-' }
                            />
                            <ReadOnlyKeyValueView
                                label="Стоимость сервисов:"
                                text={ `${ NumberUtility.numberToMoneyFormat(this.props.commonInfo.serviceTotalSum) } ${ this.props.accountInfo.localeCurrency }` }
                            />
                            { this.props.visibleFields.canShowSaleManagerInfo &&
                                <>
                                    <ReadOnlyKeyValueView
                                        label="Тип:"
                                        text={ this.props.commonInfo.accountType }
                                    />
                                    <ReadOnlyKeyValueView
                                        label="Сейлc менеджер:"
                                        text={ this.props.commonInfo.accountSaleManagerCaption }
                                    />
                                    { this.props.editableFields.isLocaleEditable
                                        ? (
                                            <ComboBoxForm
                                                isReadOnly={ !!isReadonly }
                                                label="Месторасположение:"
                                                formName="localeId"
                                                onValueChange={ this.props.onFieldChange }
                                                value={ this.props.accountDataForm.localeId ?? this.props.accountInfo.localeId }
                                                items={ this.props.localeArr.map(item => {
                                                    return {
                                                        value: item.localeId,
                                                        text: item.localeText
                                                    };
                                                }) }
                                            />
                                        )
                                        : (
                                            <ReadOnlyKeyValueView
                                                label="Месторасположение:"
                                                text={ this.props.localeArr.find(item => item.localeId === this.props.accountInfo.localeId)?.localeText ?? '' }
                                            />
                                        )
                                    }
                                </>
                            }
                        </Grid>
                        <Grid item={ true } xs={ 12 } sm={ 6 } md={ 6 }>
                            { this.props.visibleFields.canShowFullInfo &&
                                <Box display="flex" flexDirection="column">
                                    <TextBoxForm
                                        autoFocus={ false }
                                        type="textarea"
                                        label="Описание:"
                                        formName="description"
                                        isReadOnly={ !!isReadonly }
                                        value={ this.props.accountDataForm.description ?? this.props.accountInfo.description }
                                        onValueChange={ this.props.onFieldChange }
                                    />
                                    <ReadOnlyKeyValueView
                                        label="Текущий баланс:"
                                        text={ `${ NumberUtility.numberToMoneyFormat(this.props.commonInfo.balance) } ${ this.props.accountInfo.localeCurrency }` }
                                    />
                                    <ReadOnlyKeyValueView
                                        label="Ссылка на склеп:"
                                        text={ this.props.accountInfo.cloudStorageWebLink }
                                    />
                                    <CheckBoxForm
                                        isReadOnly={ !!isReadonly }
                                        formName="isVip"
                                        autoFocus={ false }
                                        label="VIP аккаунт:"
                                        trueText={ <TextOut fontWeight={ 600 } fontSize={ 12 }>(да)</TextOut> }
                                        falseText={ <TextOut fontWeight={ 600 } fontSize={ 12 }>(нет)</TextOut> }
                                        isChecked={ this.props.accountDataForm.isVip ?? this.props.accountInfo.isVip }
                                        onValueChange={ this.props.onFieldChange }
                                    />
                                    <ReadOnlyKeyValueView
                                        label="Номер аккаунта:"
                                        text={ this.props.accountInfo.indexNumber }
                                    />
                                    <ReadOnlyKeyValueView
                                        label="Последняя активность:"
                                        text={ this.props.commonInfo.lastActivity ? DateUtility.dateToDayMonthYear(this.props.commonInfo.lastActivity) : '' }
                                    />
                                    <ReadOnlyKeyValueView
                                        label="Идентификатор:"
                                        text={ this.props.accountInfo.id }
                                    />
                                    <ReadOnlyKeyValueView
                                        label="Сегмент:"
                                        text={ this.props.commonInfo.segmentName }
                                    />
                                </Box>
                            }
                        </Grid>
                    </Grid>
                    <Box marginTop="10px">
                        <TagInputForm
                            isReadOnly={ !!isReadonly }
                            validator={ createEmailValidator() }
                            onTagsChanged={ this.props.onFieldChange }
                            placeholder="Введите Email"
                            tags={ this.props.accountDataForm.emails ?? this.props.commonInfo.emails }
                            formName="emails"
                            label="Email для отправки писем:"
                        />
                    </Box>
                    { (this.props.isCloudAdmin || this.props.isCloudEngineer) && (
                        <Box marginTop="20px">
                            <ButtonGroup
                                onClick={ index => this.props.onFieldChange('deployment', index, 0) }
                                items={ this.deploymentWaveButtons }
                                selectedButtonIndex={ this.props.accountInfo.deployment ?? 0 }
                                label="Волна внедрения изменений"
                            />
                        </Box>
                    ) }
                </div>
                <WatchReducerProcessValidationError
                    reducerStateName="AccountCardState"
                    whatchProcesses={ [AccountCardProcessId.UpdateAccountCard] }
                    onProcessValidationError={ this.onProcessValidationError }
                />
            </>
        );
    }
}

export const View = withFloatMessages(ViewClass);