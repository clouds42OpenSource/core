import '@testing-library/jest-dom/extend-expect';
import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';

import { AuthenticationPassedView } from 'app/views/modules/_common/reusable-modules/DatabaseCard/views/tabs/AuthenticationTabView/AuthenticationPassedView';
import { ReduxFormsReducerState } from 'app/redux/redux-forms/store/reducers/ReduxFormsReducerState';
import { getNewStore } from 'app/utils/StoreUtility';
import { DatabaseCardReducerState } from 'app/modules/databaseCard/store/reducers';
import { SupportState } from 'app/common/enums';
import { SnackbarProvider } from 'notistack';

type TInitialState = {
    DatabaseCardState: DatabaseCardReducerState;
    ReduxForms: ReduxFormsReducerState<string>;
};

describe('<AuthenticationPassedView />', () => {
    const store = getNewStore();
    const initialState: TInitialState = {
        DatabaseCardState: store.getState().DatabaseCardState,
        ReduxForms: store.getState().ReduxForms
    };
    initialState.DatabaseCardState.tehSupportInfo = {
        isInConnectingState: false,
        lastHistoryDate: undefined,
        supportState: SupportState.AutorizationSuccess,
        supportStateDescription: 'test_description'

    };

    beforeAll(() => {
        render(
            <Provider store={ configureStore()(initialState) }>
                <SnackbarProvider>
                    <AuthenticationPassedView databaseId="test_database_id" />
                </SnackbarProvider>
            </Provider>
        );
    });

    it('renders', () => {
        expect(screen.getByTestId(/authentication-passed-view/i)).toBeInTheDocument();
        expect(screen.getByText(/ТиИ еще не проводилось/i)).toBeInTheDocument();
    });
});