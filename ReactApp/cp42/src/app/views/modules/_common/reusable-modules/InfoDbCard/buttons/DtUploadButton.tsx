import { RequestType } from 'app/api/endpoints/eternalBackup/request';
import { ELogEvent } from 'app/api/endpoints/global/enum';
import { FETCH_API } from 'app/api/useFetchApi';
import { EMessageType, useFloatMessages } from 'app/hooks';
import { OutlinedButton } from 'app/views/components/controls/Button';
import { UploadDtDialogMessage } from 'app/views/modules/AccountManagement/InformationBases/views/Modal/UploadDtDialogMessage';
import { useState } from 'react';

type TProps = {
    databaseId: string;
    v82name: string;
};

const { ETERNAL_BACKUPS: { postBackups }, GLOBAL: { sendLogEvent } } = FETCH_API;

export const DtUploadButton = ({ databaseId, v82name }: TProps) => {
    const { show } = useFloatMessages();

    const [isEnabled, setIsEnabled] = useState(true);
    const [isModalOpen, setIsModalOpened] = useState(false);

    const handleCloseModal = () => {
        setIsModalOpened(false);
    };

    const handleUploadDt = async (id: string) => {
        setIsEnabled(false);
        const response = await postBackups({
            databaseId: id,
            force: true,
            type: RequestType.dt,
        });
        setIsModalOpened(false);

        if (response.success) {
            void sendLogEvent(ELogEvent.uploadDt, `Процесс создания файла DT для базы ${ v82name } инициализирован`);
            show(EMessageType.success, 'Процесс создания файла DT успешно инициирован. По завершении вы получите уведомление на указанный электронный адрес.');
        } else {
            show(EMessageType.error, response.message ?? 'Произошла ошибка при создании выгрузки dt');
        }
    };

    return (
        <>
            <OutlinedButton isEnabled={ isEnabled } kind="success" onClick={ () => setIsModalOpened(true) }>
                Создать выгрузку dt
            </OutlinedButton>
            <UploadDtDialogMessage
                isOpen={ isModalOpen }
                databaseName={ v82name }
                onNoClick={ handleCloseModal }
                onYesClick={ () => handleUploadDt(databaseId) }
            />
        </>
    );
};