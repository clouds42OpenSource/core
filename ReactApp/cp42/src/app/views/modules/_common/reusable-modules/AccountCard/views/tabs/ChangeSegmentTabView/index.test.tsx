import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import { SnackbarProvider } from 'notistack';
import { ChangeSegmentTabView } from 'app/views/modules/_common/reusable-modules/AccountCard/views/tabs/ChangeSegmentTabView';

describe('<ChangeSegmentTabView />', () => {
    const mockStore = configureStore([thunk]);

    const getInitialState = (hasAccountCardReceived: boolean) => ({
        AccountCardState: {
            reducerActions: {
                CHANGE_ACCOUNT_SEGMENT: {
                    hasProcessActionStateChanged: true,
                    processActionState: {
                        isInErrorState: false,
                        isInSuccessState: true,
                        isInProgressState: false
                    },
                    process: {
                        error: undefined
                    }
                },
            },
            tabVisibility: {
                isAccountCardVisible: true,
                isMigrateDatabaseTabVisible: true,
                isChangeSegmentTabVisible: true,
                isAccountCaptionEditable: true
            },
            segments: [{
                segmentId: 'test_segment_id',
                segmentText: 'test_segment_text'
            }],
            commonData: {
                segmentName: 'test_segment_name'
            },
            hasSuccessFor: {
                hasAccountCardReceived
            },
            accountInfo: {
                id: 'test_id'
            }
        },
        ReduxForms: {
            process: {
                showLoadingProgress: false
            },
            AccountCard_EditAccountSegment_Form: {
                isInitialized: true,
                fields: 'test_field',
            }
        }
    } as const);

    it('renders', async () => {
        render(
            <SnackbarProvider>
                <Provider store={ mockStore(getInitialState(true)) }>
                    <ChangeSegmentTabView />
                </Provider>
            </SnackbarProvider>
        );
        expect(screen.getByTestId(/change-segment-tab-view/i)).toBeInTheDocument();
    });

    it('does not renders', async () => {
        const { container } = render(
            <SnackbarProvider>
                <Provider store={ mockStore(getInitialState(false)) }>
                    <ChangeSegmentTabView />
                </Provider>
            </SnackbarProvider>
        );
        expect(container.firstChild).toBeNull();
    });
});