declare const styles: {
    readonly 'table-container': string;
    readonly 'copy-icon': string;
    readonly 'backupPathContainer': string;
    readonly 'alert-wrapper': string;
    readonly 'alert-msg': string;
    readonly 'alert-text': string;
    readonly 'btn': string;
    readonly 'btn-wrapper': string;
};
export = styles;