import { Box } from '@mui/material';
import { Dialog } from 'app/views/components/controls/Dialog';
import { TextOut } from 'app/views/components/TextOut';
import cn from 'classnames';
import React from 'react';
import css from 'app/views/modules/_common/reusable-modules/InformationDatabaseCard/widgets/Header/modals/styles.module.css';

type TProps = {
    isOpen: boolean;
    databaseName: string;
    onNoClick: () => void;
    onYesClick: () => void;
};

export const UploadDtDialog = ({ isOpen, databaseName, onNoClick, onYesClick }: TProps) => {
    return (
        <Dialog
            isOpen={ isOpen }
            onCancelClick={ onNoClick }
            dialogWidth="xs"
            dialogVerticalAlign="center"
            title={ `Создание выгрузки DT для базы ${ databaseName }` }
            isTitleSmall={ true }
            buttons={ [
                {
                    variant: 'outlined',
                    kind: 'default',
                    onClick: onNoClick,
                    content: 'Отмена'
                },
                {
                    kind: 'error',
                    onClick: onYesClick,
                    content: 'Да'
                }
            ] }
        >
            <Box display="flex" flexDirection="column" gap="5px">
                <TextOut>
                    Пожалуйста, обратите внимание, что во время формирования файла выгрузки данных текущая база будет временно недоступна до завершения процедуры.
                </TextOut>
                <TextOut>
                    Также хотели бы напомнить, что возможность создания такой выгрузки предоставляется лишь один раз в сутки для каждой базы данных.
                </TextOut>
                <TextOut>
                    Запустить процесс прямо сейчас?
                </TextOut>
            </Box>
        </Dialog>
    );
};