import { emailRegex, phoneRegex, requiredMessage } from 'app/views/modules/_common/reusable-modules/BoxWithPicture/schemas';
import { transformPhoneValue } from 'app/views/modules/ToPartners/helpers';
import * as Yup from 'yup';

export const reconciliationStatementSchema = Yup.object().shape({
    inn: Yup.string().min(10, 'Неверный формат').max(12, 'Неверный формат').required(requiredMessage),
    kpp: Yup.string().min(9, 'Неверный формат').max(9, 'Неверный формат'),
    from: Yup.date().required(requiredMessage),
    to: Yup.date().required(requiredMessage),
    send: Yup.string().required(requiredMessage),
    email: Yup.string().matches(emailRegex, 'Неверный формат почты').required(requiredMessage),
    phone: Yup.string().transform(transformPhoneValue).matches(phoneRegex, 'Неверный формат телефона').required(requiredMessage),
});

export const reconciliationStatementInitialValues = {
    inn: '',
    kpp: '',
    from: undefined,
    to: undefined,
    send: '',
    email: '',
    phone: ''
};