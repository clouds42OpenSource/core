import { hasComponentChangesFor } from 'app/common/functions';
import { AccountDatabasesForMigrationProcessId } from 'app/modules/accountDatabasesForMigrations/AccountDatabasesForMigrationProcessId';
import { MigrateSelectedDatabasesThunkParams } from 'app/modules/accountDatabasesForMigrations/store/reducers/migrateSelectedDatabasesReducer/params';
import { MigrateSelectedDatabasesThunk } from 'app/modules/accountDatabasesForMigrations/store/thunks/MigrateSelectedDatabasesThunk';
import { AppReduxStoreState } from 'app/redux/types';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { ContainedButton } from 'app/views/components/controls/Button';
import { DialogMessage } from 'app/views/components/controls/DialogMessage';
import { TextOut } from 'app/views/components/TextOut';
import { WatchReducerProcessActionState } from 'app/views/modules/_common/components/WatchReducerProcessActionState';
import { SelectedDatabasesToMigrateContext } from 'app/views/modules/_common/reusable-modules/AccountDatabasesForMigrations/context/SelectedDatabasesToMigrateContext';
import { KeyValueDataModel } from 'app/web/common/data-models';
import { IReducerProcessActionInfo } from 'core/redux/interfaces';
import React from 'react';
import { connect } from 'react-redux';

type OwnProps = FloatMessageProps;

type DispatchProps = {
    dispatchMigrateSelectedDatabasesThunk: (args: MigrateSelectedDatabasesThunkParams) => void;
};

type StateProps = {
    avalableFileStorages: KeyValueDataModel<string, string>[];
};

type AllProps = OwnProps & DispatchProps & StateProps;

type OwnState = {
    isConfirmMigrateDialogOpen: boolean;
};

export class MigrateSelectedAccountDatabasesButtonClass extends React.Component<AllProps, OwnState> {
    public context!: React.ContextType<typeof SelectedDatabasesToMigrateContext>;

    constructor(props: AllProps) {
        super(props);
        this.migrateDatabases = this.migrateDatabases.bind(this);
        this.onProcessActionStateChanged = this.onProcessActionStateChanged.bind(this);
        this.closeConfirmMigrateDialog = this.closeConfirmMigrateDialog.bind(this);
        this.showConfirmMigrateDialog = this.showConfirmMigrateDialog.bind(this);
        this.state = {
            isConfirmMigrateDialogOpen: false
        };
    }

    public shouldComponentUpdate(nextProps: AllProps, nextState: OwnState) {
        return hasComponentChangesFor(this.props, nextProps) ||
            hasComponentChangesFor(this.state, nextState);
    }

    private onProcessActionStateChanged(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId !== AccountDatabasesForMigrationProcessId.MigrateSelectedDatabases) {
            return;
        }

        if (actionState.isInSuccessState) {
            this.props.floatMessage.show(MessageType.Success, 'Задача по переносу баз стартовала.');
            this.context.reset();
        }

        if (actionState.isInErrorState) {
            this.props.floatMessage.show(MessageType.Error, error?.message);
        }
    }

    private showConfirmMigrateDialog() {
        if (!this.context.selectedStorageId) {
            this.props.floatMessage.show(MessageType.Warning, 'Не выбрано хранилище для переноса.');
            return;
        }

        if (!this.context.selectedDatabaseNumbers.length) {
            this.props.floatMessage.show(MessageType.Warning, 'Не выбрано ни одной базы для переноса.');
            return;
        }

        this.setState({
            isConfirmMigrateDialogOpen: true
        });
    }

    private closeConfirmMigrateDialog() {
        this.setState({
            isConfirmMigrateDialogOpen: false
        });
    }

    private migrateDatabases() {
        this.props.dispatchMigrateSelectedDatabasesThunk({
            selectedDatabaseNumbers: this.context.selectedDatabaseNumbers,
            selectedStorageId: this.context.selectedStorageId,
            force: true
        });
    }

    public render() {
        return (
            <>
                <ContainedButton kind="primary" onClick={ this.showConfirmMigrateDialog }>
                    Перенести
                </ContainedButton>
                <WatchReducerProcessActionState
                    watch={ [{
                        reducerStateName: 'AccountDatabasesForMigrationState',
                        processIds: [AccountDatabasesForMigrationProcessId.MigrateSelectedDatabases]
                    }] }
                    onProcessActionStateChanged={ this.onProcessActionStateChanged }
                />
                <DialogMessage
                    dialogWidth="sm"
                    titleFontSize={ 14 }
                    titleTextAlign="left"
                    isTitleSmall={ true }
                    title="Перенос информационных баз"
                    onCancelClick={ this.closeConfirmMigrateDialog }
                    isOpen={ this.state.isConfirmMigrateDialogOpen }
                    buttons={
                        [
                            {
                                kind: 'primary',
                                content: 'перенести'.toUpperCase(),
                                onClick: this.migrateDatabases
                            },
                            {
                                kind: 'default',
                                content: 'отмена'.toUpperCase(),
                                variant: 'text',
                                onClick: this.closeConfirmMigrateDialog
                            }
                        ]
                    }
                >
                    <TextOut fontWeight={ 400 } fontSize={ 13 }>
                        Вы действительно хотите перенести ? <br />
                        На&nbsp;
                        <TextOut fontWeight={ 600 }>
                            &quot;{ this.props.avalableFileStorages.find(item => item.key === this.context.selectedStorageId)?.value }&quot;
                        </TextOut>
                        &nbsp;выбранные базы:
                        <ol>
                            { this.context.selectedDatabaseNumbers.map(item => {
                                return (
                                    <TextOut key={ item } fontWeight={ 600 }>
                                        <li>{ item }</li>
                                    </TextOut>
                                );
                            }) }
                        </ol>
                    </TextOut>
                </DialogMessage>
            </>
        );
    }
}

MigrateSelectedAccountDatabasesButtonClass.contextType = SelectedDatabasesToMigrateContext;

const MigrateSelectedAccountDatabasesButtonConnected = connect<StateProps, DispatchProps, OwnProps, AppReduxStoreState>(
    state => {
        const accountDatabasesForMigrationState = state.AccountDatabasesForMigrationState;
        const { avalableFileStorages } = accountDatabasesForMigrationState.accountDatabasesMigrationInfo;

        return {
            avalableFileStorages
        };
    },
    {
        dispatchMigrateSelectedDatabasesThunk: MigrateSelectedDatabasesThunk.invoke
    }
)(MigrateSelectedAccountDatabasesButtonClass);

export const MigrateSelectedAccountDatabasesButton = withFloatMessages(MigrateSelectedAccountDatabasesButtonConnected);