import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import configureStore, { MockStoreEnhanced } from 'redux-mock-store';
import { Provider } from 'react-redux';
import { DatabaseState } from 'app/common/enums';
import { AccountDatabasesFilterFormView } from 'app/views/modules/_common/reusable-modules/AccountDatabasesForMigrations/views/AccountDatabasesFilterFormView';
import thunk from 'redux-thunk';

describe('<AccountDatabasesFilterFormView />', () => {
    const mockStore = configureStore([thunk]);
    const onFilterChangedMock: jest.Mock = jest.fn();
    let store: MockStoreEnhanced<unknown>;
    const initialState = {
        AccountDatabasesForMigrationState: {
            accountDatabasesMigrationInfo: {
                fileStorageAccountDatabasesSummary: {
                    fileStorage: 'test_file_storage',
                    pathStorage: 'test_path_storage',
                    databaseCount: 1,
                    totatlSizeOfDatabases: 2,
                    isTotalInfo: true
                },
                databases: {
                    records: [{
                        accountDatabaseId: 'test_account_database_id',
                        v82Name: 'test_v82_name',
                        path: 'test_path',
                        size: 1,
                        status: DatabaseState.Ready,
                        isPublishDatabase: true,
                        isPublishServices: true,
                        isFile: true
                    }]
                }
            }
        },
        ReduxForms: {
            AccountDatabasesMigrations_Filter_Form: {},
            process: {
                showLoadingProgress: false
            },
        }
    } as const;

    beforeEach(() => {
        store = mockStore(initialState);
    });

    it('renders', () => {
        render(
            <Provider store={ store }>
                <AccountDatabasesFilterFormView
                    accountIndexNumber={ 5 }
                    onFilterChanged={ onFilterChangedMock }
                    isFilterFormDisabled={ false }
                />
            </Provider>
        );
        expect(screen.getByTestId(/account-databases-filter-form-view/i)).toBeInTheDocument();
    });
});