import { emailRegex, phoneRegex, requiredMessage } from 'app/views/modules/_common/reusable-modules/BoxWithPicture/schemas';
import { transformPhoneValue } from 'app/views/modules/ToPartners/helpers';
import * as Yup from 'yup';

export const edoSchema = Yup.object().shape({
    inn: Yup.string().min(10, 'Неверный формат').max(12, 'Неверный формат').required(requiredMessage),
    kpp: Yup.string().min(9, 'Неверный формат').max(9, 'Неверный формат'),
    region: Yup.string().required(requiredMessage),
    eds: Yup.string().required(requiredMessage),
    postpone: Yup.string().required(requiredMessage),
    phone: Yup.string().transform(transformPhoneValue).matches(phoneRegex, 'Неверный формат телефона').required(requiredMessage),
    email: Yup.string().matches(emailRegex, 'Неверный формат почты').required(requiredMessage),
});

export const edoInitialValues = {
    inn: '',
    kpp: '',
    region: '',
    eds: '',
    postpone: '',
    phone: '',
    email: ''
};