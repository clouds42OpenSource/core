import { RequestType } from 'app/api/endpoints/eternalBackup/request';
import { ELogEvent } from 'app/api/endpoints/global/enum';
import { FETCH_API } from 'app/api/useFetchApi';
import { AccountUserGroup, DatabaseState } from 'app/common/enums';
import { EMessageType, useAppSelector, useFloatMessages } from 'app/hooks';
import { OutlinedButton } from 'app/views/components/controls/Button';
import { useState } from 'react';

import { UploadDtDialog } from '../modals';

const { ETERNAL_BACKUPS: { postBackups }, GLOBAL: { sendLogEvent } } = FETCH_API;

export const DtUpload = () => {
    const { show } = useFloatMessages();

    const { getCurrentSessionSettingsReducer: { settings: { currentContextInfo: { currentUserGroups } } } } = useAppSelector(state => state.Global);
    const { informationBase } = useAppSelector(state => state.InformationBases.informationBasesList);
    const { id: databaseId, v82Name, isDbOnDelimiters, state } = informationBase?.database ?? {};

    const [isEnabled, setIsEnabled] = useState(true);
    const [isModalOpen, setIsModalOpened] = useState(false);

    const handleCloseModal = () => {
        setIsModalOpened(false);
    };

    const handleUploadDt = async (id: string) => {
        setIsEnabled(false);
        const response = await postBackups({ databaseId: id, force: true, type: RequestType.dt });
        setIsModalOpened(false);

        if (response.success) {
            void sendLogEvent(ELogEvent.uploadDt, `Процесс создания файла DT для базы ${ v82Name } инициализирован`);
            show(EMessageType.success, 'Процесс создания файла DT успешно инициирован. По завершении вы получите уведомление на указанный электронный адрес.');
        } else {
            show(EMessageType.error, response.message ?? 'Произошла ошибка при создании выгрузки dt');
        }
    };

    if (!(
        isDbOnDelimiters &&
        !((currentUserGroups.includes(AccountUserGroup.AccountUser) || currentUserGroups.includes(AccountUserGroup.AccountSaleManager)) && currentUserGroups.length === 1) &&
        state === DatabaseState.Ready
    )) {
        return null;
    }

    return (
        <>
            <OutlinedButton isEnabled={ isEnabled } kind="success" onClick={ () => setIsModalOpened(true) }>
                Создать выгрузку dt
            </OutlinedButton>
            <UploadDtDialog
                isOpen={ isModalOpen }
                databaseName={ v82Name ?? '' }
                onNoClick={ handleCloseModal }
                onYesClick={ () => handleUploadDt(databaseId ?? '') }
            />
        </>
    );
};