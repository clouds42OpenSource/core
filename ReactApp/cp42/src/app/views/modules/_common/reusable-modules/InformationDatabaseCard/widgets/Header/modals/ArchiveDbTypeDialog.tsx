import { AppRoutes } from 'app/AppRoutes';
import { EMessageType, useFloatMessages } from 'app/hooks';
import { Dialog } from 'app/views/components/controls/Dialog';
import { TextOut } from 'app/views/components/TextOut';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { RequestKind } from 'core/requestSender/enums';
import { ReactNode } from 'react';

type TProps = {
    isOpen: boolean;
    completeStatus: boolean;
    canUsePromisePayment: boolean;
    cost: number;
    currency: string;
    notEnoughMoney: number;
    databaseId: string;
    serviceId: string;
    message: ReactNode;
    typeOperation: boolean;
    closeDialog: () => void;
};

const { ArchiveOrBackup } = InterlayerApiProxy.getInfoDbCardApi();

export const ArchiveDbTypeDialog = ({ isOpen, closeDialog, typeOperation, cost, currency, databaseId, message, serviceId, notEnoughMoney, completeStatus, canUsePromisePayment }: TProps) => {
    const { show } = useFloatMessages();

    const archiveBuy = (isPromisePayment: boolean) => {
        try {
            void ArchiveOrBackup(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, { cost, currency, databaseId, operation: typeOperation ? 1 : 2, serviceId, usePromisePayment: isPromisePayment });
            closeDialog();
        } catch (err: unknown) {
            show(EMessageType.error, err);
        }
    };

    return (
        <Dialog
            dialogWidth="sm"
            isOpen={ isOpen }
            onCancelClick={ closeDialog }
            dialogVerticalAlign="center"
            buttons={ completeStatus
                ? [
                    {
                        kind: 'default',
                        variant: 'contained',
                        content: 'Отмена',
                        onClick: closeDialog
                    },
                    {
                        kind: 'success',
                        variant: 'contained',
                        content: typeOperation
                            ? `Переместить в архив (${ cost } ${ currency })`
                            : `Создать бекап (${ cost } ${ currency })`,
                        onClick: () => archiveBuy(false)
                    }
                ]
                : canUsePromisePayment
                    ? [
                        {
                            kind: 'default',
                            variant: 'contained',
                            content: 'Отмена',
                            onClick: closeDialog
                        },
                        {
                            kind: 'primary',
                            variant: 'contained',
                            content: `Пополнить счет на (${ notEnoughMoney } ${ currency })`,
                            onClick: () => window.open(
                                `${ window.location.origin }${ AppRoutes.accountManagement.balanceManagement }/replenish-balance?customPayment.amount=${ notEnoughMoney }&customPayment.description=Оплата+за+перенос+ИБ+в+архив+`,
                                '_blank'
                            )
                        },
                        {
                            kind: 'primary',
                            variant: 'contained',
                            content: 'Обещанный платеж',
                            onClick: () => archiveBuy(true)
                        }
                    ]
                    : [
                        {
                            kind: 'default',
                            variant: 'contained',
                            content: 'Отмена',
                            onClick: closeDialog
                        },
                        {
                            kind: 'primary',
                            variant: 'contained',
                            content: `Пополнить счет на (${ notEnoughMoney } ${ currency })`,
                            onClick: () => window.open(
                                `${ window.location.origin }${ AppRoutes.accountManagement.balanceManagement }/replenish-balance?customPayment.amount=${ notEnoughMoney }&customPayment.description=Оплата+за+перенос+ИБ+в+архив+`,
                                '_blank'
                            )
                        }
                    ]
            }
        >
            <TextOut fontSize={ 14 } style={ { wordBreak: 'normal' } }>
                { message } Стоимость { cost } { currency }.
            </TextOut>
        </Dialog>
    );
};