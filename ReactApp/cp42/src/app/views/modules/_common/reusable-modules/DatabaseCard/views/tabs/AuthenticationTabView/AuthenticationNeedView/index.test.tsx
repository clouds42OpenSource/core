import '@testing-library/jest-dom/extend-expect';
import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import { SnackbarProvider } from 'notistack';
import thunk from 'redux-thunk';

import { AuthenticationNeedView } from 'app/views/modules/_common/reusable-modules/DatabaseCard/views/tabs/AuthenticationTabView/AuthenticationNeedView';
import { getNewStore } from 'app/utils/StoreUtility';
import { DatabaseCardReducerState } from 'app/modules/databaseCard/store/reducers';
import { ReduxFormsReducerState } from 'app/redux/redux-forms/store/reducers/ReduxFormsReducerState';
import { SupportState } from 'app/common/enums';

type TInitialState = {
    DatabaseCardState: DatabaseCardReducerState;
    ReduxForms: ReduxFormsReducerState<string>;
};

describe('<AuthenticationNeedView />', () => {
    const store = getNewStore();
    const initialState: TInitialState = {
        DatabaseCardState: store.getState().DatabaseCardState,
        ReduxForms: store.getState().ReduxForms
    };

    initialState.DatabaseCardState.tehSupportInfo = {
        isInConnectingState: true,
        supportState: SupportState.AutorizationSuccess,
        supportStateDescription: 'test_description'
    };

    beforeAll(() => {
        render(
            <Provider store={ configureStore([thunk])(initialState) }>
                <SnackbarProvider>
                    <AuthenticationNeedView databaseId="test_database_id" />
                </SnackbarProvider>
            </Provider>
        );
    });

    it('renders', () => {
        expect(screen.getByTestId(/authentication-need-view/i)).toBeInTheDocument();
    });
});