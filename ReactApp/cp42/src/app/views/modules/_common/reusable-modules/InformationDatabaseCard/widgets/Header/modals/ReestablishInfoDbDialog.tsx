import { Dialog } from 'app/views/components/controls/Dialog';
import { TextOut } from 'app/views/components/TextOut';

type TProps = {
    isOpen: boolean,
    onNoClick: () => void,
    onYesClick: () => void,
};

export const ReestablishInfoDbDialog = ({ isOpen, onNoClick, onYesClick }: TProps) => {
    return (
        <Dialog
            title="Восстановление информационной базы"
            isTitleSmall={ true }
            isOpen={ isOpen }
            dialogWidth="xs"
            onCancelClick={ onNoClick }
            buttons={ [
                {
                    kind: 'primary',
                    content: 'Подтвердить',
                    onClick: onYesClick
                },
                {
                    kind: 'default',
                    content: 'Отмена',
                    onClick: onNoClick
                }] }
        >
            <TextOut>
                Вы действительно хотите восстановить базу?
            </TextOut>
        </Dialog>
    );
};