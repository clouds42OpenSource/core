import { DbPublishState } from 'app/common/enums';
import { hasComponentChangesFor } from 'app/common/functions';
import { getErrorMessage } from 'app/common/functions/getErrorMessage';
import { OutlinedButton } from 'app/views/components/controls/Button';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { CancelPublishInfoDbDialogMessage } from 'app/views/modules/AccountManagement/InformationBases/views/Modal/CancelPublishInfoDbDialogMessage';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { RequestKind } from 'core/requestSender/enums';
import React from 'react';

type OwnState = {
    open: boolean
};

type OwnProps = FloatMessageProps & {
    usedWebServices: boolean;
    databaseId: string;
    isInPublishingState: number;
    onChangeField: () => void;
    canWebPublish: boolean;
};

type AllProps = OwnProps;

export class PublishInfoDbButtonClass extends React.Component<AllProps, OwnState> {
    constructor(props: AllProps) {
        super(props);
        this.renderPublishInfoDb = this.renderPublishInfoDb.bind(this);
        this.publishDatabase = this.publishDatabase.bind(this);
        this.onChangeDialogMessage = this.onChangeDialogMessage.bind(this);
        this.state = {
            open: false,
        };
    }

    public shouldComponentUpdate(nextProps: AllProps, nextState: OwnState) {
        return hasComponentChangesFor(this.props, nextProps) || hasComponentChangesFor(this.state, nextState);
    }

    private onChangeDialogMessage() {
        this.setState(prevState => ({
            open: !prevState.open
        }));
    }

    private async publishDatabase() {
        const infoDbCardApi = InterlayerApiProxy.getInfoDbCardApi();
        try {
            await infoDbCardApi.publishInfoDbCard(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, {
                databaseId: this.props.databaseId,
                publish: this.props.isInPublishingState === DbPublishState.Unpublished
            }).then((a: boolean) => {
                this.setState({
                    open: false
                });
                if (a) {
                    this.props.isInPublishingState !== DbPublishState.Unpublished
                        ? this.props.floatMessage.show(MessageType.Success, 'База поставлена в очередь на снятие с публикации', 2000)
                        : this.props.floatMessage.show(MessageType.Success, 'База поставлена в очередь на публикацию', 2000);
                } else {
                    this.props.isInPublishingState !== DbPublishState.Unpublished
                        ? this.props.floatMessage.show(MessageType.Error, 'Не удалось поставить базу в очередь снятие с публикации')
                        : this.props.floatMessage.show(MessageType.Error, 'Не удалось поставить базу в очередь на публикацию');
                }
            });
            this.props.onChangeField();
        } catch (e) {
            this.props.floatMessage.show(MessageType.Error, getErrorMessage(e));
        }
    }

    private renderPublishInfoDb() {
        if (this.props.isInPublishingState === DbPublishState.Unpublished && this.props.canWebPublish) {
            return (
                <OutlinedButton kind="success" onClick={ this.publishDatabase }>
                    <i className="fa fa-globe p-r-4" />&nbsp;Опубликовать базу
                </OutlinedButton>
            );
        }

        if (this.props.isInPublishingState === DbPublishState.PendingPublication) {
            return (
                <OutlinedButton kind="success" isEnabled={ false }>
                    База в процессе публикации
                </OutlinedButton>
            );
        }

        if (this.props.isInPublishingState === DbPublishState.Published) {
            return (
                <>
                    <OutlinedButton kind="success" onClick={ this.props.usedWebServices ? this.onChangeDialogMessage : this.publishDatabase }>
                        <i className="fa fa-globe p-r-4" />&nbsp;Снять с публикации
                    </OutlinedButton>
                    <CancelPublishInfoDbDialogMessage
                        isOpen={ this.state.open }
                        onNoClick={ this.onChangeDialogMessage }
                        onYesClick={ this.publishDatabase }
                    />
                </>
            );
        }

        if (this.props.isInPublishingState === DbPublishState.PendingUnpublication) {
            return (
                <OutlinedButton kind="success" isEnabled={ false }>
                    База в процессе снятия с публикации
                </OutlinedButton>
            );
        }
    }

    public render() {
        return (
            <>
                {
                    this.renderPublishInfoDb()
                }
            </>
        );
    }
}

export const PublishInfoDbButton = withFloatMessages(PublishInfoDbButtonClass);