import { hasComponentChangesFor } from 'app/common/functions';
import { DatabaseCardProcessId } from 'app/modules/databaseCard';
import { DisableDatabaseSupportThunkParams } from 'app/modules/databaseCard/store/reducers/disableDatabaseSupportReducer/params';
import { DisableDatabaseSupportThunk } from 'app/modules/databaseCard/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { ContainedButton } from 'app/views/components/controls/Button';
import { WatchReducerProcessActionState } from 'app/views/modules/_common/components/WatchReducerProcessActionState';
import cn from 'classnames';
import { IReducerProcessActionInfo } from 'core/redux/interfaces';
import React from 'react';
import { connect } from 'react-redux';
import css from '../styles.module.css';

type OwnProps = FloatMessageProps & {
    databaseId: string;
    onSuccess?: () => void;
};

type DispatchProps = {
    dispatchDisableDatabaseSupportThunk: (args?: DisableDatabaseSupportThunkParams) => void;
};

type OwnState = {
    disableDatabaseTehSupportState: {
        isInProgressState: boolean;
        isInSuccessState: boolean;
        isInErrorState: boolean;
        error?: Error;
    }
};

type AllProps = OwnProps & DispatchProps;

class DisableTehSupportDatabaseButtonClass extends React.Component<AllProps, OwnState> {
    constructor(props: AllProps) {
        super(props);
        this.onDisableDatabaseSupport = this.onDisableDatabaseSupport.bind(this);
        this.onProcessActionStateChanged = this.onProcessActionStateChanged.bind(this);
        this.state = {
            disableDatabaseTehSupportState: {
                isInErrorState: false,
                isInProgressState: false,
                isInSuccessState: false,
                error: undefined
            }
        };
    }

    public shouldComponentUpdate(nextProps: AllProps, nextState: OwnState) {
        return hasComponentChangesFor(this.props, nextProps) ||
            hasComponentChangesFor(this.state, nextState);
    }

    public componentDidUpdate(_prevProps: AllProps, prevState: OwnState) {
        if (this.state.disableDatabaseTehSupportState.isInErrorState && !prevState.disableDatabaseTehSupportState.isInErrorState) {
            this.props.floatMessage.show(MessageType.Error, this.state.disableDatabaseTehSupportState.error?.message);
        } else
            if (this.state.disableDatabaseTehSupportState.isInSuccessState && !prevState.disableDatabaseTehSupportState.isInSuccessState) {
                if (this.props.onSuccess) this.props.onSuccess();
            }
    }

    private onDisableDatabaseSupport() {
        this.props.dispatchDisableDatabaseSupportThunk({
            databaseId: this.props.databaseId,
            force: true
        });
    }

    private onProcessActionStateChanged(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId === DatabaseCardProcessId.DisableDatabaseSupport) {
            this.setState({
                disableDatabaseTehSupportState: {
                    isInProgressState: actionState.isInProgressState,
                    isInSuccessState: actionState.isInSuccessState,
                    isInErrorState: actionState.isInErrorState,
                    error
                }
            });
        }
    }

    public render() {
        return (
            <>
                <ContainedButton
                    isEnabled={ !this.state.disableDatabaseTehSupportState.isInProgressState }
                    showProgress={ this.state.disableDatabaseTehSupportState.isInProgressState }
                    kind="error"
                    className={ cn(css.btn) }
                    onClick={ this.onDisableDatabaseSupport }
                >
                    Отключить
                </ContainedButton>
                <WatchReducerProcessActionState
                    watch={ [{
                        reducerStateName: 'DatabaseCardState',
                        processIds: [DatabaseCardProcessId.DisableDatabaseSupport]
                    }] }
                    onProcessActionStateChanged={ this.onProcessActionStateChanged }
                />
            </>
        );
    }
}

const DisableTehSupportDatabaseButtonConnected = connect<NonNullable<unknown>, DispatchProps, OwnProps, AppReduxStoreState>(
    null,
    {
        dispatchDisableDatabaseSupportThunk: DisableDatabaseSupportThunk.invoke
    }
)(DisableTehSupportDatabaseButtonClass);

export const DisableTehSupportDatabaseButton = withFloatMessages(DisableTehSupportDatabaseButtonConnected);