import HelpOutlineIcon from '@mui/icons-material/HelpOutline';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import { Box } from '@mui/material';
import Button from '@mui/material/Button';
import Collapse from '@mui/material/Collapse';
import Tooltip from '@mui/material/Tooltip';
import { LocalizationProvider, TimePicker } from '@mui/x-date-pickers';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { FETCH_API } from 'app/api/useFetchApi';
import { EMessageType, useAppSelector, useFloatMessages } from 'app/hooks';
import { TextOut } from 'app/views/components/TextOut';
import ruLocale from 'date-fns/locale/ru';
import dayjs from 'dayjs';
import React, { useEffect, useState } from 'react';

type TSchedule = {
    startingTime: Date;
    endingTime: Date;
    minimumTime: number;
};

const { getBackupsSchedule, setBackupsSchedule } = FETCH_API.APPLICATIONS;

export const CopyGenerationTime = () => {
    const { show } = useFloatMessages();

    const { database } = useAppSelector(state => state.InformationBases.informationBasesList.informationBase) || {};

    const [isOpen, setIsOpen] = useState(false);
    const [schedule, setSchedule] = useState<TSchedule>({ startingTime: new Date(), endingTime: new Date(), minimumTime: 0 });

    const convertTimeStringToDate = (timeString: string) => {
        const [hours, minutes] = timeString.split(':').map(Number);
        const time = new Date();
        time.setHours(hours, minutes, 0, 0);

        return time;
    };

    useEffect(() => {
        (async () => {
            const { success, data, message } = await getBackupsSchedule({ applicationId: database?.id ?? '' });

            if (success && data) {
                const { startingTime, endingTime, minimumTime } = data;

                const [intervalHours, intervalMinutes] = minimumTime.split(':');
                const intervalInMinutes = +intervalHours * 60 + +intervalMinutes;

                setSchedule({
                    startingTime: convertTimeStringToDate(startingTime),
                    endingTime: convertTimeStringToDate(endingTime),
                    minimumTime: intervalInMinutes
                });
            } else {
                show(EMessageType.error, message);
            }
        })();
    }, [database?.id, show]);

    const handleStartTimeChange = (startingTime: Date) => {
        const { endingTime, minimumTime } = schedule;

        setSchedule(prev => ({ ...prev, startingTime }));

        if (endingTime && startingTime) {
            const minEndTime = new Date(startingTime.getTime() + minimumTime * 60000);

            if (endingTime < minEndTime) {
                setSchedule(prev => ({ ...prev, endingTime: minEndTime }));
            }
        }
    };

    const handleEndTimeChange = (endingTime: Date) => {
        const { startingTime, minimumTime } = schedule;

        setSchedule(prev => ({ ...prev, endingTime }));

        if (startingTime && endingTime) {
            const maxStartTime = new Date(endingTime.getTime() - minimumTime * 60000);

            if (startingTime > maxStartTime) {
                setSchedule(prev => ({ ...prev, startingTime: maxStartTime }));
            }
        }
    };

    const saveSchedule = async () => {
        const { success, message } = await setBackupsSchedule({
            applicationId: database?.id ?? '',
            EndingTime: dayjs(schedule.endingTime).format('HH:mm'),
            StartingTime: dayjs(schedule.startingTime).format('HH:mm')
        });

        if (success) {
            show(EMessageType.success, 'Время формирования создания бэкапов успешно изменено');
        } else {
            show(EMessageType.error, message);
        }
    };

    return (
        <LocalizationProvider dateAdapter={ AdapterDateFns } adapterLocale={ ruLocale }>
            <Button onClick={ () => setIsOpen(prev => !prev) } color="inherit">
                { isOpen ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon /> }
                Настройка времени форматирования резервных данных
            </Button>
            <Collapse in={ isOpen }>
                <Box display="flex" alignItems="center" gap="6px" padding="6px 0" margin="6px 0">
                    <TextOut>
                        Формировать копии с
                    </TextOut>
                    <LocalizationProvider dateAdapter={ AdapterDateFns } adapterLocale={ ruLocale }>
                        <TimePicker
                            value={ schedule.startingTime }
                            onChange={ value => value && handleStartTimeChange(value) }
                            label="Выберите время"
                        />
                        <TextOut>
                            по
                        </TextOut>
                        <TimePicker
                            value={ schedule.endingTime }
                            onChange={ value => value && handleEndTimeChange(value) }
                            label="Выберите время"
                        />
                        <Tooltip title={ `Минимальный интервал времени создания бэкапа ${ dayjs().startOf('day').add(schedule.minimumTime, 'minute').format('HH:mm') }` }>
                            <HelpOutlineIcon />
                        </Tooltip>
                        <Button onClick={ saveSchedule }>
                            Сохранить
                        </Button>
                    </LocalizationProvider>
                </Box>
            </Collapse>
        </LocalizationProvider>
    );
};