import { AccountDatabaseAccessState } from 'app/common/enums/AccountDatabaseAccessStateEnum';
import React from 'react';

type OwnProps = {
   extensionAccess: AccountDatabaseAccessState | null
};

export class DbStateLineAccessViews extends React.Component<OwnProps> {
    public constructor(props: OwnProps) {
        super(props);

        this.chooseStateAccess = this.chooseStateAccess.bind(this);
    }

    public chooseStateAccess(value: AccountDatabaseAccessState | null) {
        if (value === AccountDatabaseAccessState.ProcessingGrant) {
            return (
                <div style={ { color: '#6CCDBA', fontSize: '11px' } }>
                    <img src="img/database-icons/animated-clock.gif" alt="timer" style={ { width: '16px', height: '16px' } } />
               &nbsp; В процессе выдачи
                </div>
            );
        }
        if (value === AccountDatabaseAccessState.ProcessingDelete) {
            return (
                <div style={ { color: 'red', fontSize: '11px' } }>
                    <img src="img/database-icons/animated-clock.gif" alt="timer" style={ { width: '16px', height: '16px' } } />
               &nbsp; В процессе удаления
                </div>
            );
        }
        if (value === AccountDatabaseAccessState.Error) {
            return (
                <div style={ { color: 'red', fontSize: '11px' } }>Ошибка</div>
            );
        }
        return <div />;
    }

    public render() {
        return (
            <>
                {
                    this.chooseStateAccess(this.props.extensionAccess)
                }
            </>
        );
    }
}