import { Box, Pagination } from '@mui/material';
import { contextAccountId } from 'app/api';
import { TAccountProfile } from 'app/api/endpoints/accountUsers/response';
import { TDatabaseAccesses } from 'app/api/endpoints/informationBases/response';
import { FETCH_API } from 'app/api/useFetchApi';
import { REDUX_API } from 'app/api/useReduxApi';
import { configurationProfilesToRole } from 'app/common/functions';
import { getErrorMessage } from 'app/common/functions/getErrorMessage';
import { EMessageType, useAppDispatch, useAppSelector, useFloatMessages, useGetCurrency } from 'app/hooks';
import { COLORS } from 'app/utils';
import { Dialog } from 'app/views/components/controls/Dialog';
import { CheckBoxForm, DropDownListWithCheckboxes, TextBoxForm } from 'app/views/components/controls/forms';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { TextOut } from 'app/views/components/TextOut';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { DbStateLineAccess } from 'app/views/modules/_common/reusable-modules/InformationDatabaseCard/features/DbStateLineAccess';
import { BuyAccessDbDialog } from 'app/views/modules/_common/reusable-modules/InformationDatabaseCard/widgets/Header/modals/BuyAccessDbDialog';
import React, { useEffect, useState } from 'react';

import style from './style.module.css';

export type TGroupAccessChangeDialog = {
    isOpen: boolean;
    onCancelClick: () => void;
};

const pageSize = 10;

const { tryPayAccesses, getAccessPriceResponse } = FETCH_API.INFORMATION_BASES;
const { changeDatabaseAccessOnDelimiters } = FETCH_API.ACCOUNT_USERS;
const { getGivenAccountProfilesList } = REDUX_API.ACCOUNT_USERS_REDUX;

export const GroupAccessChangeDialog = ({ isOpen, onCancelClick }: TGroupAccessChangeDialog) => {
    const { show } = useFloatMessages();
    const dispatch = useAppDispatch();
    const currency = useGetCurrency();

    const [isLoading, setIsLoading] = useState(false);
    const [searchUser, setSearchUser] = useState('');
    const [selectProfiles, setSelectProfiles] = useState<Omit<TAccountProfile, 'databases'>[]>([]);
    const [selectUsers, setSelectUsers] = useState<TDatabaseAccesses[]>([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [totalSum, setTotalSum] = useState(0);
    const [buyAccessDialog, setBuyAccessDialog] = useState({
        isOpen: false,
        notEnoughMoney: 0,
        canUsePromisePayment: false
    });
    const [isFirstShowWarning, setIsFirstShowWarning] = useState(true);

    const { availability, informationBase, accessInfoDb } = useAppSelector(state => state.InformationBases.informationBasesList);
    const { database } = informationBase ?? {};
    const { accountProfilesListReducer: { data: profilesData } } = useAppSelector(state => state.AccountUsersState);

    const selectUser = (user: TDatabaseAccesses) => {
        setSelectUsers(prev => prev.find(item => item.userId === user.userId) ? prev.filter(item => item.userId !== user.userId) : [...prev, user]);
    };

    const selectAllUsers = () => {
        if (selectUsers.length === accessInfoDb.length) {
            setSelectUsers([]);
        } else {
            setSelectUsers(accessInfoDb);
        }
    };

    const closeGroupAccessChangeDialog = () => {
        onCancelClick();
        setSelectUsers([]);
        setSearchUser('');
    };

    const currentPageHandler = (_: React.ChangeEvent<unknown>, page: number) => {
        setCurrentPage(page);
    };

    const changeAccess = async (isPromisePayment?: boolean) => {
        setIsLoading(true);
        closeGroupAccessChangeDialog();

        try {
            const { success, message, data } = await tryPayAccesses({
                accountId: contextAccountId(),
                billingServiceId: availability?.myDatabaseServiceId ?? '',
                changedUsers: selectUsers.map(({ userId }) => {
                    return {
                        billingServiceTypeId: database?.myDatabasesServiceTypeId ?? '',
                        sponsorship: {
                            i: !accessInfoDb?.map(item => item.userId).includes(userId),
                            label: '',
                            me: false
                        },
                        status: true,
                        subject: userId
                    };
                }),
                usePromisePayment: isPromisePayment ?? false
            });

            if (success && data) {
                if (!data.isComplete) {
                    setBuyAccessDialog(prev => ({
                        isOpen: !prev.isOpen,
                        notEnoughMoney: data.notEnoughMoney ?? 0,
                        canUsePromisePayment: data.canUsePromisePayment ?? false,
                    }));

                    return;
                }

                const { v82Name, templateName, id } = database ?? {};

                if (profilesData) {
                    const { data: accessData } = await changeDatabaseAccessOnDelimiters(
                        {
                            permissions: selectUsers.map(({ userId }) => ({
                                'allow-backup': false,
                                profiles: selectProfiles,
                                'user-id': userId,
                                'application-id': id ?? '',
                            }))
                        },
                        selectUsers.map(({ userId }) => {
                            const internalUser = accessInfoDb.find(accessInfo => accessInfo.userId === userId);

                            return {
                                databaseName: templateName ?? '',
                                databaseId: id ?? '',
                                v82Name: v82Name ?? '',
                                userId: internalUser?.userId ?? userId,
                                userName: internalUser?.userLogin ?? '',
                                isExternal: !internalUser
                            };
                        })
                    );

                    if (accessData && accessData.results) {
                        accessData.results.forEach(statusResult => {
                            if (statusResult.status === 'failure') {
                                show(EMessageType.warning, statusResult.error ?? `Не удалось отредактировать права в базу ${ statusResult['application-id'] }`);
                            }
                        });
                    }

                    if (accessData && accessData.message) {
                        show(EMessageType.warning, accessData.message);
                    } else {
                        show(EMessageType.success, 'Изменение доступов будет выполнено в течение 1 минуты. Пожалуйста, подождите');
                    }

                    closeGroupAccessChangeDialog();
                    getGivenAccountProfilesList(dispatch, '', id);
                }
            } else {
                show(EMessageType.error, message);
            }
        } catch (e) {
            show(EMessageType.error, getErrorMessage(e));
        }

        setIsLoading(false);
    };

    const closeBuyAccessDialog = () => {
        setBuyAccessDialog({
            isOpen: false,
            canUsePromisePayment: false,
            notEnoughMoney: 0
        });
    };

    useEffect(() => {
        (async () => {
            if (selectUsers.length) {
                const { data } = await getAccessPriceResponse({
                    serviceTypesIdsList: [database?.myDatabasesServiceTypeId ?? ''],
                    accountUserDataModelsList: selectUsers.map(user => ({
                        userId: user.userId,
                        hasAccess: true,
                        state: 4,
                        hasLicense: false
                    }))
                });

                if (data) {
                    setTotalSum(data.reduce((sum, access) => {
                        sum += access.accessCost;

                        return sum;
                    }, 0));
                }
            }
        })();
    }, [database?.myDatabasesServiceTypeId, selectUsers]);

    return (
        <>
            { isLoading && <LoadingBounce /> }
            <Dialog
                title="Изменение прав доступа"
                isOpen={ isOpen }
                onCancelClick={ closeGroupAccessChangeDialog }
                buttons={ [
                    {
                        content: 'Отмена',
                        kind: 'default',
                        variant: 'contained',
                        onClick: closeGroupAccessChangeDialog
                    },
                    {
                        content: `Применить ${ totalSum ? `(${ totalSum } ${ currency }.)` : '' }`,
                        kind: 'success',
                        variant: 'contained',
                        onClick: () => changeAccess(false),
                        hiddenButton: selectUsers.length === 0
                    }
                ] }
                dialogVerticalAlign="center"
                dialogWidth="md"
            >
                <Box display="flex" flexDirection="column" gap="8px">
                    {
                        profilesData && (
                            <FormAndLabel label="Выбирите профили доступа" fullWidth={ true }>
                                <DropDownListWithCheckboxes
                                    noUpdateValueChangeHandler={ true }
                                    deepCompare={ true }
                                    hiddenSelectAll={ true }
                                    renderValueIsChip={ true }
                                    items={
                                        profilesData.flatMap(profile => {
                                            if (profile.code === database?.configurationCode) {
                                                return profile.profiles.map(pr => ({
                                                    text: pr.name,
                                                    value: pr.id,
                                                    checked: !!selectProfiles.find(item => item.id === pr.id)
                                                }));
                                            }

                                            return [];
                                        })
                                    }
                                    label=""
                                    formName="profiles"
                                    hiddenAdditionalElement={ true }
                                    onValueChange={ (_, items) => setSelectProfiles(prevProfiles => {
                                        const currentProfiles = profilesData
                                            .find(profile => profile.code === database?.configurationCode)?.profiles ?? [];
                                        const { adminRole, userRole } = configurationProfilesToRole(currentProfiles, items);

                                        if (items.length > 1 && isFirstShowWarning && adminRole.length && userRole.length) {
                                            show(EMessageType.info, 'Установка профиля доступа "Администратор" одновременно с другими профилями через личный кабинет не поддерживается');
                                            setIsFirstShowWarning(false);
                                        }

                                        if (prevProfiles && prevProfiles.some(p => p.admin)) {
                                            return (userRole.length ? userRole : adminRole).filter(pr => items.find(item => item.value === pr.id));
                                        }

                                        return (adminRole.length ? adminRole : userRole).filter(pr => items.find(item => item.value === pr.id));
                                    }) }
                                />
                            </FormAndLabel>
                        )
                    }
                    <FormAndLabel label="Поиск" fullWidth={ true }>
                        <TextBoxForm
                            formName="searchLineUser"
                            value={ searchUser }
                            onValueChange={ (_, value) => setSearchUser(value) }
                            placeholder="Поиск пользователя по логину, эл. почте или ФИО (отдельно)"
                            fullWidth={ true }
                        />
                    </FormAndLabel>
                    <CommonTableWithFilter
                        uniqueContextProviderStateId="GroupAccessChangeContextProviderStateId"
                        tableProps={ {
                            dataset: (
                                searchUser
                                    ? accessInfoDb.filter(item =>
                                        item.userLogin?.toLowerCase().includes(searchUser.toLowerCase()) ||
                                        item.userEmail?.toLowerCase().includes(searchUser.toLowerCase()) ||
                                        item.userFullName?.toLowerCase().includes(searchUser.toLowerCase())
                                    )
                                    : accessInfoDb
                            ).slice(pageSize * (currentPage - 1), pageSize * currentPage),
                            keyFieldName: 'userId',
                            fieldsView: {
                                userId: {
                                    caption: <CheckBoxForm
                                        formName="isDefault"
                                        label=""
                                        isChecked={ accessInfoDb.length === selectUsers.length }
                                        onValueChange={ selectAllUsers }
                                        className={ style.check }
                                    />,
                                    fieldContentNoWrap: false,
                                    fieldWidth: '3%',
                                    format: (value: string, item) =>
                                        <CheckBoxForm
                                            formName={ value }
                                            label=""
                                            isChecked={ !!selectUsers.find(users => users.userId === value) }
                                            onValueChange={ () => selectUser(item) }
                                            className={ style.check }
                                        />
                                },
                                userLogin: {
                                    caption: 'Логин (ФИО)',
                                    fieldContentNoWrap: false,
                                    isSortable: false,
                                    format: (value: string, item) =>
                                        <Box>
                                            <TextOut inDiv={ true }>{ value }</TextOut>
                                            <TextOut inDiv={ true }>{ item.userFullName ? `(${ item.userFullName })` : '' }</TextOut>
                                            <DbStateLineAccess extensionAccess={ item.state } />
                                        </Box>
                                },
                                userEmail: {
                                    caption: 'Эл. адрес',
                                    fieldContentNoWrap: false,
                                    isSortable: false,
                                    format: (value: string, item) =>
                                        <>
                                            <TextOut inDiv={ true } fontSize={ 13 }>
                                                { value }
                                            </TextOut>
                                            {
                                                (Object.hasOwn(item, 'accessCost') || item.isExternalAccess) && (
                                                    <TextOut inDiv={ true } style={ { color: COLORS.error, fontSize: '10px' } }>
                                                        { item.accountInfo }
                                                    </TextOut>
                                                )
                                            }
                                        </>
                                }
                            }
                        } }
                    />
                    <Box gap="5px" justifyContent="flex-end" display="flex" marginTop="10px">
                        <Pagination
                            page={ currentPage }
                            count={
                                Math.ceil((
                                    searchUser
                                        ? accessInfoDb.filter(item =>
                                            item.userLogin?.toLowerCase().includes(searchUser.toLowerCase()) ||
                                            item.userEmail?.toLowerCase().includes(searchUser.toLowerCase()) ||
                                            item.userFullName?.toLowerCase().includes(searchUser.toLowerCase())
                                        )
                                        : accessInfoDb
                                ).length / pageSize)
                            }
                            onChange={ currentPageHandler }
                        />
                    </Box>
                </Box>
            </Dialog>
            <BuyAccessDbDialog
                loading={ isLoading }
                usePromise={ true }
                isOpen={ buyAccessDialog.isOpen }
                onNoClick={ closeBuyAccessDialog }
                onYesClick={ () => changeAccess(true) }
                notEnoughMoney={ buyAccessDialog.notEnoughMoney }
                canUsePromisePayment={ buyAccessDialog.canUsePromisePayment }
            />
        </>
    );
};