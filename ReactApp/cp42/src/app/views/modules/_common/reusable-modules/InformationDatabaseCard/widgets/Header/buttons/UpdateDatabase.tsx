import { Box } from '@mui/material';
import { FETCH_API } from 'app/api/useFetchApi';
import { REDUX_API } from 'app/api/useReduxApi';
import { AccountUserGroup, DatabaseState } from 'app/common/enums';
import { EMessageType, useAppDispatch, useAppSelector, useFloatMessages, useGetCurrentUserGroups } from 'app/hooks';
import { OutlinedButton } from 'app/views/components/controls/Button';
import { Dialog } from 'app/views/components/controls/Dialog';
import { TextOut } from 'app/views/components/TextOut';
import { InfoDbCardPermissionsEnum } from 'app/web/common/enums/InfoDbCardPermissionsEnum';
import { useState } from 'react';

const { postUpdateDatabase } = FETCH_API.ACCOUNT_DATABASES;
const { useGetSupportData } = FETCH_API.INFORMATION_BASES;
const { getInformationBaseInfo } = REDUX_API.INFORMATION_BASES_REDUX;

export const UpdateDatabase = () => {
    const { show } = useFloatMessages();
    const dispatch = useAppDispatch();
    const currentUserGroups = useGetCurrentUserGroups();

    const { getUserPermissionsReducer: { permissions } } = useAppSelector(state => state.Global);
    const { informationBase } = useAppSelector(state => state.InformationBases.informationBasesList);
    const { id, v82Name, isDbOnDelimiters, state } = informationBase?.database ?? {};
    const { data: supportData } = useGetSupportData(id ?? '');

    const [openUpdateDialog, setOpenUpdateDialog] = useState(false);
    const [isDisableButton, setIsDisableButton] = useState(false);

    const open = () => {
        setOpenUpdateDialog(true);
    };

    const close = () => {
        setOpenUpdateDialog(false);
    };

    const goToAuTab = () => {
        close();
        void getInformationBaseInfo(dispatch, v82Name ?? '');
    };

    const update = async () => {
        const { success, message } = await postUpdateDatabase(id ?? '');

        show(success ? EMessageType.info : EMessageType.error, message);

        close();
        setIsDisableButton(true);
    };

    if (!(
        !isDbOnDelimiters &&
        permissions.includes(InfoDbCardPermissionsEnum.AccountDatabases_EditSupport) &&
        state !== DatabaseState.ProcessingSupport &&
        state !== DatabaseState.DeletedToTomb &&
        state !== DatabaseState.DetachedToTomb &&
        currentUserGroups.some(role => [AccountUserGroup.AccountAdmin, AccountUserGroup.Hotline, AccountUserGroup.CloudAdmin, AccountUserGroup.CloudSE].includes(role)) &&
        !supportData?.rawData.hasAutoUpdate
    )) {
        return null;
    }

    return (
        <>
            <OutlinedButton
                onClick={ open }
                kind="success"
                isEnabled={
                    !(currentUserGroups.includes(AccountUserGroup.AccountSaleManager) && currentUserGroups.length === 1) &&
                    !isDisableButton
                }
            >
                Обновить базу
            </OutlinedButton>
            <Dialog
                title="Обновление базы"
                isOpen={ openUpdateDialog }
                onCancelClick={ close }
                dialogVerticalAlign="center"
                dialogWidth="sm"
                buttons={
                    supportData?.rawData.isAuthorized
                        ? [
                            {
                                content: 'Отмена',
                                kind: 'default',
                                variant: 'outlined',
                                onClick: close
                            },
                            {
                                content: 'Подключить автообновление',
                                kind: 'success',
                                variant: 'contained',
                                onClick: goToAuTab
                            },
                            {
                                content: 'Обновить',
                                kind: 'success',
                                variant: 'contained',
                                onClick: update
                            }
                        ]
                        : [
                            {
                                content: 'Отмена',
                                kind: 'default',
                                variant: 'outlined',
                                onClick: close
                            },
                            {
                                content: 'Авторизоваться',
                                kind: 'success',
                                variant: 'contained',
                                onClick: goToAuTab
                            }
                        ]
                }
            >
                {
                    supportData?.rawData.isAuthorized
                        ? (
                            <Box display="flex" flexDirection="column" gap="5px">
                                <TextOut>
                                    Вы можете обновить базу данных вручную в любое удобное время, но возможность подключить автоматическое обновление базы данных предоставляет вам:
                                </TextOut>
                                <TextOut>
                                    <TextOut fontWeight={ 700 } inDiv={ true }>1. Избавление от необходимости отслеживать новые версии</TextOut>
                                    <TextOut>
                                        Пользователям больше не нужно следить за выходом новых версий конфигураций и запускать обновление вручную.&nbsp;
                                        Автоматическое обновление позволяет системе автоматически обновлять базу данных до последней версии без участия пользователей.
                                    </TextOut>
                                </TextOut>
                                <TextOut>
                                    <TextOut fontWeight={ 700 } inDiv={ true }>2. Экономия времени</TextOut>
                                    <TextOut>
                                        Пользователям больше не нужно тратить время на запуск процедуры обновления и ожидание ее завершения.&nbsp;
                                        Автоматическое обновление выполняется в фоновом режиме, освобождая время пользователей для выполнения других задач.
                                    </TextOut>
                                </TextOut>
                                <TextOut>
                                    <TextOut fontWeight={ 700 } inDiv={ true }>3. Без простоев в работе</TextOut>
                                    <TextOut>
                                        Поскольку обновление выполняется автоматически, нет необходимости приостанавливать работу с базой данных на время процедуры обновления.&nbsp;
                                        Это предотвращает простои в работе и обеспечивает бесперебойный доступ к данным для всех пользователей.
                                    </TextOut>
                                </TextOut>
                                <TextOut>
                                    <TextOut fontWeight={ 700 } inDiv={ true }>4. Гибкость в планировании</TextOut>
                                    <TextOut>
                                        Автоматическое обновление позволяет запускать процесс обновления в удобное для компании время, даже если это не рабочее время.&nbsp;
                                        Пользователям больше не нужно вручную запускать процесс обновления через Личный Кабинет, что делает процесс более гибким и удобным.
                                    </TextOut>
                                </TextOut>
                            </Box>
                        )
                        : <TextOut>Обновить базу возможно только после прохождения авторизации к базе.</TextOut>
                }
            </Dialog>
        </>
    );
};