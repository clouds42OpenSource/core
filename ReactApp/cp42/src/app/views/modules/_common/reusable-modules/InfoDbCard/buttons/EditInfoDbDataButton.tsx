import { contextAccountId } from 'app/api';
import { TPermissions } from 'app/api/endpoints/accountUsers/request';
import { FETCH_API } from 'app/api/useFetchApi';
import { REDUX_API } from 'app/api/useReduxApi';
import { getErrorMessage } from 'app/common/functions/getErrorMessage';
import { getGivenAccountProfilesListSlice } from 'app/modules/accountUsers';
import { ReceiveAccessThunkParams } from 'app/modules/infoDbCard/store/reducers/receiveAccessInfoDbCardReducer/params';
import { AppReduxStoreState } from 'app/redux/types';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { ContainedButton } from 'app/views/components/controls/Button';
import { Dialog } from 'app/views/components/controls/Dialog';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { BuyAccessDbDialogMessage } from 'app/views/modules/AccountManagement/InformationBases/views/Modal/BuyAccessDbDialogMessage';
import { GetAvailabilityInfoDb } from 'app/web/api/InfoDbProxy/responce-dto/GetAvailabilityInfoDb';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { DatabaseAccessesDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/AccessInfoDbDataModel';
import { EternalUserDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/EternalUserDataModel';
import { InfoDbCardAccountDatabaseInfoDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/InfoDbCardAccountDatabaseInfoDataModel';
import { RequestKind } from 'core/requestSender/enums';
import React from 'react';
import { connect } from 'react-redux';

type OwnProps = FloatMessageProps & {
    users: EternalUserDataModel[];
    onResetAddUser: (isClose?: boolean) => void;
    databaseId: string;
    valueCheck: string[];
    cost: number;
    onSuccessAccess: () => void;
    accessReceivedStatus: boolean;
    dispatchAccessCardThunk: (args: ReceiveAccessThunkParams) => void;
    showAccessOrPayDialog?: boolean;
    cancelClickShowAccessOrPayDialog?: () => void;
};

type OwnState = {
    open: boolean,
    notEnoughMoney: number,
    canUsePromisePayment: boolean,
    loading: boolean
};

type DispatchProps = {
    dispatchGetGivenAccountProfilesList: (userId?: string, databaseId?: string, needShowLoading?: boolean) => void;
    dispatchClearGivenAccountProfileList: () => void;
};

type StateProps = {
    requestDataForDelimiters: TPermissions[];
    commonDetails: InfoDbCardAccountDatabaseInfoDataModel,
    accessInfoDb: DatabaseAccessesDataModel[],
    availability: GetAvailabilityInfoDb;
};

type AllProps = OwnProps & DispatchProps & StateProps;

const { changeDatabaseAccessOnDelimiters } = FETCH_API.ACCOUNT_USERS;

export class EditInfoDbButtonClass extends React.Component<AllProps, OwnState> {
    public constructor(props: AllProps) {
        super(props);
        this.sendUserIdAdd = this.sendUserIdAdd.bind(this);
        this.sendUserIdRemove = this.sendUserIdRemove.bind(this);
        this.changeDialogMessage = this.changeDialogMessage.bind(this);
        this.AddOrRemoveAccess = this.AddOrRemoveAccess.bind(this);
        this.addIdForPay = this.addIdForPay.bind(this);

        this.state = {
            open: false,
            notEnoughMoney: 0,
            canUsePromisePayment: false,
            loading: false
        };
    }

    public componentDidMount() {
        if (this.props.cost === 0 && this.props.commonDetails.isDbOnDelimiters) {
            if (this.props.cancelClickShowAccessOrPayDialog) {
                this.props.cancelClickShowAccessOrPayDialog();
            }
            void this.AddOrRemoveAccess(false);
        }
    }

    private sendUserIdAdd(valueCheck: string[], accesstInfoDb: DatabaseAccessesDataModel[]): string[] {
        const userWithHasAccessId = Object.values(accesstInfoDb).filter(i => i.hasAccess).map(i => i.userId);

        return valueCheck.filter(i => !userWithHasAccessId.includes(i));
    }

    private sendUserIdRemove(valueCheck: string[], accesstInfoDb: DatabaseAccessesDataModel[]): string[] {
        return Object.values(accesstInfoDb).filter(i => !valueCheck.includes(i.userId) && i.hasAccess).map(value => value.userId);
    }

    private changeDialogMessage() {
        this.setState(prevState => ({
            open: !prevState.open
        }));
    }

    private addIdForPay() {
        return (!this.props.commonDetails.isFile && !this.props.commonDetails.isDbOnDelimiters)
            ? this.sendUserIdAdd(this.props.valueCheck, this.props.accessInfoDb).map(id => {
                return {
                    billingServiceTypeId: this.props.availability.AccessToServerDatabaseServiceTypeId,
                    sponsorship: {
                        i: !this.props.accessInfoDb.map(item => item.userId).includes(id),
                        label: '',
                        me: false
                    },
                    status: true,
                    subject: id
                };
            }
            ) : [];
    }

    private async AddOrRemoveAccess(isPromisePayment?: boolean) {
        try {
            this.setState({ loading: true });
            const infoDbCardApi = InterlayerApiProxy.getInfoDbCardApi();
            const ids = this.sendUserIdAdd(this.props.valueCheck, this.props.accessInfoDb);

            if (ids.length) {
                const tryPay = await infoDbCardApi.TryPayAccesses(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, {
                    accountId: contextAccountId(),
                    billingServiceId: this.props.availability.MyDatabaseServiceId,
                    changedUsers: [...ids.map(id => {
                        return {
                            billingServiceTypeId: this.props.commonDetails.myDatabasesServiceTypeId,
                            sponsorship: {
                                i: !this.props.accessInfoDb.map(item => item.userId).includes(id),
                                label: '',
                                me: false
                            },
                            status: true,
                            subject: id
                        };
                    }
                    ), ...this.addIdForPay()],
                    usePromisePayment: isPromisePayment || false
                });

                if (tryPay.success && tryPay.data) {
                    if (!tryPay.data.isComplete) {
                        this.setState(prevState => ({
                            open: !prevState.open,
                            notEnoughMoney: tryPay.data?.notEnoughMoney ?? 0,
                            canUsePromisePayment: tryPay.data?.canUsePromisePayment ?? false,
                            loading: false
                        }));

                        return;
                    }
                } else {
                    this.props.floatMessage.show(MessageType.Error, tryPay.message);
                }

                this.setState({ open: false });
                if (this.props.cancelClickShowAccessOrPayDialog) {
                    this.props.cancelClickShowAccessOrPayDialog();
                }
            }

            if (this.props.commonDetails.isDbOnDelimiters) {
                const { v82Name, templateName, id } = this.props.commonDetails;
                const removePermissions: TPermissions[] = this.sendUserIdRemove(this.props.valueCheck, this.props.accessInfoDb).map(removeId => ({
                    'allow-backup': false,
                    'user-id': removeId,
                    'application-id': id,
                    profiles: []
                }));
                const allRequestData = [...this.props.requestDataForDelimiters, ...removePermissions];

                const { data: accessData } = await changeDatabaseAccessOnDelimiters(
                    { permissions: allRequestData },
                    allRequestData.map(data => {
                        const internalUser = this.props.accessInfoDb.find(accessInfo => accessInfo.userId === data['user-id']);
                        const params = internalUser ?? this.props.users.find(user => user.userId === data['user-id']);

                        return {
                            databaseName: templateName,
                            databaseId: id,
                            v82Name,
                            userId: params?.userId ?? data['user-id'],
                            userName: params?.userLogin ?? '',
                            isExternal: !internalUser
                        };
                    })
                );

                if (accessData && accessData.results) {
                    accessData.results.forEach(statusResult => {
                        if (statusResult.status === 'failure') {
                            this.props.floatMessage.show(MessageType.Warning, statusResult.error ?? `Не удалось отредактировать права в базу ${ statusResult['application-id'] }`);
                        }
                    });
                }

                if (accessData && accessData.message) {
                    this.props.floatMessage.show(MessageType.Warning, accessData.message);
                } else {
                    this.props.floatMessage.show(MessageType.Success, 'Изменение доступов будет выполнено в течение 1 минуты. Пожалуйста, подождите');
                }

                this.setState({ loading: false });
            } else {
                const resultAdd = await infoDbCardApi.EditAccessUserInfoDbCard(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, {
                    DatabaseId: this.props.databaseId,
                    UsersId: this.sendUserIdAdd(this.props.valueCheck, this.props.accessInfoDb) || [],
                    giveAccess: true
                });
                const resultDelete = await infoDbCardApi.EditAccessUserInfoDbCard(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, {
                    DatabaseId: this.props.databaseId,
                    UsersId: this.sendUserIdRemove(this.props.valueCheck, this.props.accessInfoDb),
                    giveAccess: false
                });

                if (!resultAdd.Data.filter(i => !i.Success).length && !resultDelete.Data.filter(i => !i.Success).length) {
                    this.props.floatMessage.show(MessageType.Success,
                        'Изменение доступов будет выполнено в течение 1 минуты. Пожалуйста, подождите',
                        2000);
                }

                if (resultAdd.Data.filter(i => !i.Success).length) {
                    this.props.floatMessage.show(MessageType.Error,
                        resultAdd.Data.filter(i => !i.Success)[0].Message);
                }

                if (resultDelete.Data.filter(i => !i.Success).length) {
                    this.props.floatMessage.show(MessageType.Error,
                        resultDelete.Data.filter(i => !i.Success)[0].Message);
                }
            }

            this.props.onSuccessAccess();
            setTimeout(() => {
                if (!this.props.commonDetails.isDbOnDelimiters) {
                    this.props.onResetAddUser();
                }

                this.props.dispatchAccessCardThunk({
                    databaseId: this.props.databaseId,
                    searchString: '',
                    usersByAccessFilterType: 0,
                    showLoadingProgress: false
                });

                this.props.dispatchGetGivenAccountProfilesList('', this.props.commonDetails.id);
            }, 0);
        } catch (e) {
            this.props.floatMessage.show(MessageType.Error, getErrorMessage(e));
        }

        this.props.onResetAddUser();

        this.setState({ loading: false });
    }

    public render() {
        return (
            <>
                {
                    this.props.commonDetails.isDbOnDelimiters && (
                        <Dialog
                            isOpen={ this.props.showAccessOrPayDialog ?? false }
                            dialogVerticalAlign="center"
                            dialogWidth="sm"
                            buttons={ [
                                {
                                    onClick: () => {
                                        if (this.props.cancelClickShowAccessOrPayDialog) {
                                            this.props.cancelClickShowAccessOrPayDialog();
                                        }
                                        this.props.onResetAddUser(true);
                                        this.props.dispatchClearGivenAccountProfileList();
                                    },
                                    variant: 'contained',
                                    kind: 'default',
                                    content: 'Отменить',
                                },
                                {
                                    onClick: () => this.AddOrRemoveAccess(false),
                                    variant: 'contained',
                                    kind: 'success',
                                    content: `Купить (${ this.props.cost } руб.)`,
                                }
                            ] }
                        >
                            Для предоставления пользователю {
                                this.props.users.find(user => user.userId === this.props.requestDataForDelimiters[0]['user-id'])?.userFullName ?? ''
                            } доступа в базу &quot;{ this.props.commonDetails.v82Name }&quot; необходимо оплатить { this.props.cost } рублей
                        </Dialog>
                    )
                }
                {
                    !this.props.commonDetails.isDbOnDelimiters && (
                        <ContainedButton
                            isEnabled={ true }
                            kind="success"
                            onClick={ () => this.AddOrRemoveAccess(false) }
                            showProgress={ !this.props.accessReceivedStatus }
                        >
                            <span>{ this.props.cost ? `Купить (${ this.props.cost } руб. )` : 'Сохранить' }</span>
                        </ContainedButton>
                    )
                }
                <BuyAccessDbDialogMessage
                    loading={ this.state.loading }
                    usePromise={ true }
                    isOpen={ this.state.open }
                    onNoClick={ this.changeDialogMessage }
                    onYesClick={ () => {
                        this.setState({
                            loading: true
                        });
                        void this.AddOrRemoveAccess(true);
                    } }
                    notEnoughMoney={ this.state.notEnoughMoney }
                    canUsePromisePayment={ this.state.canUsePromisePayment }
                />
                { this.state.loading ? <LoadingBounce /> : null }

            </>
        );
    }
}

const { getGivenAccountProfilesList } = REDUX_API.ACCOUNT_USERS_REDUX;
const { clear } = getGivenAccountProfilesListSlice.actions;

const EditInfoDbDataButtonClassConnected = connect<StateProps, DispatchProps, OwnProps, AppReduxStoreState>(
    state => {
        const databaseCardState = state.InfoDbCardState;
        const infoDbListState = state.InfoDbListState;
        const { commonDetails } = databaseCardState;
        const { requestData } = state.AccountUsersState.givenAccountProfilesListReducer;

        return {
            requestDataForDelimiters: requestData,
            commonDetails,
            availability: infoDbListState.AvailabilityInfoDb,
            accessInfoDb: databaseCardState.databaseAccesses
        };
    },
    dispatch => ({
        dispatchGetGivenAccountProfilesList: (userId?: string, databaseId?: string, needShowLoading?: boolean) => getGivenAccountProfilesList(dispatch, userId, databaseId, needShowLoading),
        dispatchClearGivenAccountProfileList: () => dispatch(clear())
    })
)(EditInfoDbButtonClass);

export const EditInfoDbDataButton = withFloatMessages(EditInfoDbDataButtonClassConnected);