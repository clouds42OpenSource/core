import '@testing-library/jest-dom/extend-expect';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { CopyTextToClipBoardButton } from 'app/views/modules/_common/reusable-modules/DatabaseCard/views/_buttons/CopyTextToClipBoardButton';
import { SnackbarProvider } from 'notistack';

describe('<CopyTextToClipBoardButton />', () => {
    beforeEach(() => {
        render(
            <SnackbarProvider>
                <CopyTextToClipBoardButton text="test_text" />
            </SnackbarProvider>
        );
    });

    it('renders', () => {
        expect(screen.getByTestId(/copy-text-to-clip-board-button/i)).toBeInTheDocument();
    });

    it('triggers button', async () => {
        document.execCommand = jest.fn();
        await userEvent.click(screen.getByTestId(/copy-text-to-clip-board-button/i));
        expect(document.execCommand).toBeCalled();
    });
});