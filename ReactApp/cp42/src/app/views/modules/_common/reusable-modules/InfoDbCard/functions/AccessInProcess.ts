import { AccountDatabaseAccessState } from 'app/common/enums/AccountDatabaseAccessStateEnum';
/**
 * Узнать, находятся ли доступы временном процессе
 */
export function AccessInProcess(value: AccountDatabaseAccessState) {
    return value === AccountDatabaseAccessState.ProcessingGrant ||
        value === AccountDatabaseAccessState.ProcessingDelete;
}