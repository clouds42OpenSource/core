import '@testing-library/jest-dom/extend-expect';
import { render } from '@testing-library/react';

import { UsedWebServicesView } from 'app/views/modules/_common/reusable-modules/DatabaseCard/views/tabs/CommonTabView/partial-elements/UsedWebServicesView';
import userEvent from '@testing-library/user-event';

describe('<UsedWebServicesView />', () => {
    const onValueChangeMock: jest.Mock = jest.fn();

    const getComponent = (isInEditMode: boolean) => (
        <UsedWebServicesView
            label="test_label"
            formName="test_form_name"
            readOnlyUsedWebServices={ true }
            editUsedWebServices={ true }
            isInEditMode={ isInEditMode }
            onValueChange={ onValueChangeMock }
        />
    );

    it('renders with edit mode', async () => {
        const { container } = render(getComponent(true));
        await userEvent.click(container.querySelector('[type=checkbox]')!);
        expect(onValueChangeMock).toBeCalled();
    });

    it('renders without edit mode', async () => {
        const { container } = render(getComponent(false));
        await userEvent.click(container.querySelector('[type=checkbox]')!);
        expect(onValueChangeMock).toBeCalledTimes(0);
    });
});