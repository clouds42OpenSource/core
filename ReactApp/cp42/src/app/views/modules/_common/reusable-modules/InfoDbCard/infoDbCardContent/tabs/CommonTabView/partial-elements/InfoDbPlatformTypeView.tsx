import { PlatformType, PlatformTypeDescription } from 'app/common/enums';
import { ComboBoxForm } from 'app/views/components/controls/forms/ComboBox/ComboBoxForm';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { InfoDbCardEditDatabaseDataForm } from 'app/views/modules/AccountManagement/InformationBases/types/InfiDbCardEditDatabaseDataForm';
import { ChangePlatformDbDialogMessage } from 'app/views/modules/AccountManagement/InformationBases/views/Modal/ChangePlatformDbDialogMessage';
import { ReadOnlyKeyValueView } from 'app/views/modules/_common/components/ReadOnlyKeyValueView';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { InfoDbCardAccountDatabaseInfoDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/InfoDbCardAccountDatabaseInfoDataModel';
import { RequestKind } from 'core/requestSender/enums';
import { useState } from 'react';

type OwnProps = {
    commonDetails: InfoDbCardAccountDatabaseInfoDataModel;
    label: string;
    formName: string;
    readOnlyPlatformType: PlatformType;
    editPlatformType: PlatformType;
    platformTypes: Array<PlatformType>;
    isInEditMode: boolean;
    onChangeField: () => void;
    isOther: boolean;
};

type FormType = ReduxFormProps<InfoDbCardEditDatabaseDataForm>;

export const InfoDbPlatformTypeView = (props: OwnProps & FormType) => {
    const editPlatformType = props.editPlatformType ?? props.readOnlyPlatformType;

    const [open, setOpen] = useState(false);

    function openDialogMessage<TValue>(fieldName: string, newValue?: TValue, _prevValue?: TValue) {
        setOpen(true);

        props.reduxForm.updateReduxFormFields({
            [fieldName]: newValue
        });
    }

    function onCloseDialog() {
        setOpen(false);
    }

    async function changeTypeInfoDb() {
        const infoDbCardApi1 = InterlayerApiProxy.getInfoDbCardApi();
        await infoDbCardApi1.editPlatformInfoDbCard(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, {
            databaseId: props.commonDetails.id,
            platformType: props.reduxForm.getReduxFormFields(false).platformType,
            distributionType: props.reduxForm.getReduxFormFields(false).distributionType
        });
        props.onChangeField();
        onCloseDialog();
    }

    return props.isInEditMode && props.platformTypes.length
        ? (
            <>
                <ComboBoxForm
                    formName={ props.formName }
                    label={ props.label }
                    value={ editPlatformType }
                    items={ props.platformTypes.map(item => {
                        return {
                            value: item,
                            text: PlatformTypeDescription[item]
                        };
                    }) }
                    isReadOnly={ props.isOther }
                    onValueChange={ openDialogMessage }
                />
                <ChangePlatformDbDialogMessage isOpen={ open } onYesClick={ changeTypeInfoDb } onNoClick={ onCloseDialog } />
            </>
        )
        : (
            <ReadOnlyKeyValueView
                label={ props.label }
                text={ PlatformTypeDescription[props.readOnlyPlatformType] }
            />
        );
};

export const InfoDbPlatformType = withReduxForm(InfoDbPlatformTypeView, {
    reduxFormName: 'InfoDbCard_EditName_Db'
});