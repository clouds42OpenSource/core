import { hasComponentChangesFor } from 'app/common/functions';
import { DatabaseCardProcessId } from 'app/modules/databaseCard';
import {
    ReceiveDatabaseCardThunkParams
} from 'app/modules/databaseCard/store/reducers/receiveDatabaseCardReducer/params';
import { ReceiveDatabaseCardThunk } from 'app/modules/databaseCard/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { IReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { Dialog } from 'app/views/components/controls/Dialog';
import { DatabaseCardEditDatabaseDataForm } from 'app/views/modules/_common/reusable-modules/DatabaseCard/types';
import {
    DatabaseCardContentView
} from 'app/views/modules/_common/reusable-modules/DatabaseCard/views/DatabaseCardContentView';
import { EditDatabaseDataButton } from 'app/views/modules/_common/reusable-modules/DatabaseCard/views/_buttons';
import React from 'react';
import { connect } from 'react-redux';

type OwnProps = {
    isOpen: boolean;
    onCancelDialogButtonClick: () => void;
    accountDatabaseNumber: string;
    isReadonlyDatabaseInfo?: boolean;
};

type StateProps = {
    canEditDatabase: boolean;
    error: Error | undefined;
};

type DispatchProps = {
    dispatchReceiveDatabaseCardThunk: (args?: ReceiveDatabaseCardThunkParams) => void;
};

type AllProps = OwnProps & StateProps & DispatchProps & FloatMessageProps;

class DatabaseCardClass extends React.Component<AllProps> {
    private databaseDataTabRef = React.createRef<IReduxForm<DatabaseCardEditDatabaseDataForm>>();

    public constructor(props: AllProps) {
        super(props);
        this.getDialogButtons = this.getDialogButtons.bind(this);
        this.getDatabaseTabFormFields = this.getDatabaseTabFormFields.bind(this);
        this.resetDatabaseDataTabReduxForm = this.resetDatabaseDataTabReduxForm.bind(this);
    }

    public shouldComponentUpdate(nextProps: OwnProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    public componentDidUpdate(prevProps: AllProps) {
        if (!prevProps.isOpen && this.props.isOpen) {
            this.props.dispatchReceiveDatabaseCardThunk({
                accountDatabaseNumber: this.props.accountDatabaseNumber,
                force: true
            });
        }

        if (prevProps.isOpen && this.props.isOpen) {
            if (this.props.error) {
                this.props.floatMessage.show(MessageType.Error, 'Ошибка получения информационной базы');
                this.props.onCancelDialogButtonClick();
            }
        }
    }

    private getDialogButtons() {
        return (<EditDatabaseDataButton
            getChangedDatabaseData={ this.getDatabaseTabFormFields }
            onSuccess={ this.props.onCancelDialogButtonClick }
        />);
    }

    private getDatabaseTabFormFields(): DatabaseCardEditDatabaseDataForm {
        return this.databaseDataTabRef.current!.getReduxForm().getReduxFormFields(false);
    }

    private resetDatabaseDataTabReduxForm() {
        if (this.databaseDataTabRef.current) {
            this.databaseDataTabRef.current.getReduxForm().resetReduxForm();
        }
    }

    public render() {
        return (
            <Dialog
                title={ `Информационная база: ${ this.props.accountDatabaseNumber }` }
                isOpen={ this.props.isOpen }
                dialogWidth="md"
                maxHeight="800px"
                onCancelClick={ this.props.onCancelDialogButtonClick }
                buttons={ this.props.canEditDatabase && !this.props.isReadonlyDatabaseInfo ? this.getDialogButtons : undefined }
            >
                <DatabaseCardContentView
                    databaseDataTabRef={ this.databaseDataTabRef }
                    isReadonlyDatabaseInfo={ this.props.isReadonlyDatabaseInfo }
                />
            </Dialog>
        );
    }
}

const DatabaseCardConnected = connect<StateProps, DispatchProps, OwnProps, AppReduxStoreState>(
    state => {
        const databaseCardState = state.DatabaseCardState;
        const { tabVisibility } = databaseCardState;
        const receiveProcess = databaseCardState.reducerActions[DatabaseCardProcessId.ReceiveDatabaseCard];
        const isReceiveDatabaseCardInProgress = receiveProcess?.hasProcessActionStateChanged &&
            receiveProcess.processActionState?.isInProgressState;
        const { hasDatabaseCardReceived } = databaseCardState.hasSuccessFor;
        const { canEditDatabase } = databaseCardState;
        return {
            tabVisibility,
            isReceiveDatabaseCardInProgress,
            hasDatabaseCardReceived,
            canEditDatabase,
            error: databaseCardState.process.error
        };
    },
    {
        dispatchReceiveDatabaseCardThunk: ReceiveDatabaseCardThunk.invoke
    }
)(DatabaseCardClass);

export const DatabaseCard = withFloatMessages(DatabaseCardConnected);