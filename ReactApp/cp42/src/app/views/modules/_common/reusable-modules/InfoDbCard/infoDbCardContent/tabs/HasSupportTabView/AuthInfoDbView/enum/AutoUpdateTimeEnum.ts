export enum AutoUpdateTimeEnum {
    day = 14,
    evening = 18,
    night = 21,
}