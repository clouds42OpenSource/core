import { Box } from '@mui/material';
import { hasComponentChangesFor } from 'app/common/functions';
import { Nullable } from 'app/common/types';
import { ReceiveBackupsThunkParams } from 'app/modules/infoDbCard/store/reducers/receiveBuckupsReducer/params';
import { ReceiveBackupsInfoDbCardThunk } from 'app/modules/infoDbCard/store/thunks/ReceiveBackupsThunk';
import { AppReduxStoreState } from 'app/redux/types';
import { DateUtility } from 'app/utils';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { DoubleDateInputForm } from 'app/views/components/controls/forms/DoubleDateInputView';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { TextOut } from 'app/views/components/TextOut';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { CheckPaymentOptionDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/checkPaymentOption';
import { BackupsItem } from 'app/web/InterlayerApiProxy/MsBackupsApiProxy/getBackups';
import { RequestKind } from 'core/requestSender/enums';
import React from 'react';
import { connect } from 'react-redux';
import style from './style.module.css';

type OwnProps = {
    databaseId: string;
    isDbOnDelimiters: boolean;
    status: boolean;
    setData: (args: BackupsItem[]) => void;
};

type OwnState = {
    autoUpdatePeriodFrom?: Nullable<Date>;
    autoUpdatePeriodTo?: Nullable<Date>;
    isOpen?: boolean;
    checkPaymentOptionReceived?: boolean;
    checkPaymentOption?: CheckPaymentOptionDataModel | null;
};

type DispatchProps = {
    dispatchBackupsCardThunk: (args: ReceiveBackupsThunkParams) => void;
};

type AllProps = DispatchProps & OwnProps & FloatMessageProps;

class ArchivalInfoDbListFilterFormViewClass extends React.Component<AllProps, OwnState> {
    public constructor(props: AllProps) {
        super(props);

        this.state = {
            autoUpdatePeriodFrom: null,
            autoUpdatePeriodTo: null,
            isOpen: false,
            checkPaymentOptionReceived: false,
            checkPaymentOption: null
        };

        this.onValueChange = this.onValueChange.bind(this);
        this.onValueChangeIsDbOnDelimiters = this.onValueChangeIsDbOnDelimiters.bind(this);
        this.closeDialog = this.closeDialog.bind(this);
    }

    public shouldComponentUpdate(nextProps: OwnProps, nextState: OwnState) {
        return hasComponentChangesFor(this.props, nextProps) || hasComponentChangesFor(this.state, nextState);
    }

    private onValueChange<TForm extends string, TValue>(formName: TForm, newValue: TValue) {
        this.setState({ [formName]: newValue }, () =>
            this.props.dispatchBackupsCardThunk({
                CreationDateFrom: this.state.autoUpdatePeriodFrom ? DateUtility.dateToYearMonthDay(this.state.autoUpdatePeriodFrom) : '',
                CreationDateTo: this.state.autoUpdatePeriodTo ? DateUtility.dateToYearMonthDay(this.state.autoUpdatePeriodTo) : '',
                DatabaseId: this.props.databaseId,
                force: true,
                showLoadingProgress: true
            })
        );
    }

    private onValueChangeIsDbOnDelimiters<TForm extends string, TValue>(formName: TForm, newValue: TValue) {
        try {
            this.setState({ [formName]: newValue }, () =>
                InterlayerApiProxy.getMsBackupsApiProxy().getBackups(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, {
                    applicationId: this.props.databaseId,
                    toDate: this.state.autoUpdatePeriodTo ? DateUtility.dateToYearMonthDay(this.state.autoUpdatePeriodTo) : '',
                    fromDate: this.state.autoUpdatePeriodFrom ? DateUtility.dateToYearMonthDay(this.state.autoUpdatePeriodFrom) : ''
                }).then(response => this.props.setData(response.backups)));
        } catch (er: unknown) {
            this.props.floatMessage.show(MessageType.Error, er);
        }
    }

    private closeDialog() {
        this.setState({
            isOpen: false
        });
    }

    public render() {
        return (
            <Box display="flex" gap="10px" alignItems="flex-end" className={ style.filter }>
                <FormAndLabel label="Период поиска">
                    <DoubleDateInputForm
                        onValueChange={ this.props.isDbOnDelimiters ? this.onValueChangeIsDbOnDelimiters : this.onValueChange }
                        periodFromInputName="autoUpdatePeriodFrom"
                        periodToInputName="autoUpdatePeriodTo"
                        periodFromValue={ this.state.autoUpdatePeriodFrom ?? null }
                        periodToValue={ this.state.autoUpdatePeriodTo ?? null }
                    />
                </FormAndLabel>
                {
                    this.props.status && (
                        <TextOut>
                            {
                                this.props.isDbOnDelimiters
                                    ? 'Резервная копия создается ежедневно в ночное время, если в течение дня в базу внесли изменения, и хранится 14 дней.'
                                    : 'Резервная копия создается перед АО/ТиИ автоматически и хранится 2 последние копии.'
                            }
                            Также доступно создание копии по требованию.&nbsp;
                            <a
                                href="https://42clouds.com/ru-ru/manuals/sozdanie-vygruzka-i-vosstanovlenie-kopiy-dlya-faylovykh-baz-i-baz-na-razdelitelyakh/"
                                target="_blank"
                                className={ style.link }
                                rel="noreferrer"
                            >
                                Инструкция по созданию резервной копии
                            </a>.
                        </TextOut>
                    )
                }
            </Box>
        );
    }
}

export const ArchivalInfoDbListFilterForm = connect<object, DispatchProps, OwnProps, AppReduxStoreState>(
    () => ({}),
    {
        dispatchBackupsCardThunk: ReceiveBackupsInfoDbCardThunk.invoke,
    }
)(withFloatMessages(ArchivalInfoDbListFilterFormViewClass));