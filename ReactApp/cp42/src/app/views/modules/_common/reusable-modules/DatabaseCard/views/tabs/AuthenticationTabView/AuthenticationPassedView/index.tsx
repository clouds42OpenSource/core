import { hasComponentChangesFor } from 'app/common/functions';
import { AppReduxStoreState } from 'app/redux/types';
import { DateUtility } from 'app/utils';
import { DisableTehSupportDatabaseButton } from 'app/views/modules/_common/reusable-modules/DatabaseCard/views/_buttons/DisableTehSupportDatabaseButton';
import { DatabaseCardTehSupportInfoDataModel } from 'app/web/InterlayerApiProxy/DatabaseCardApiProxy/receiveDatabaseCard/data-models';
import cn from 'classnames';
import React from 'react';
import { connect } from 'react-redux';
import css from '../../../styles.module.css';

type OwnProps = {
    databaseId: string;
};

type StateProps = {
    tehSupportInfo: DatabaseCardTehSupportInfoDataModel;
};

type AllProps = OwnProps & StateProps;

class AuthenticationPassedViewClass extends React.Component<AllProps> {
    public shouldComponentUpdate(nextProps: AllProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    public render() {
        return (
            <div className={ cn(css['alert-wrapper']) } data-testid="authentication-passed-view">
                <div className={ cn(css['alert-msg']) }>
                    <span
                        className={ cn(css['alert-text']) }
                        dangerouslySetInnerHTML={ {
                            __html: this.props.tehSupportInfo.supportStateDescription
                        } }
                    />
                </div>
                <div className={ cn(css['btn-wrapper']) }>
                    <DisableTehSupportDatabaseButton
                        databaseId={ this.props.databaseId }
                    />
                </div>
                {
                    this.props.tehSupportInfo.lastHistoryDate
                        ? (<p>Последнее ТиИ проведено { DateUtility.dateToDayMonthYearHourMinuteSeconds(this.props.tehSupportInfo.lastHistoryDate) }</p>)
                        : (<p>ТиИ еще не проводилось</p>)
                }
            </div>
        );
    }
}

export const AuthenticationPassedView = connect<StateProps, NonNullable<unknown>, OwnProps, AppReduxStoreState>(
    state => {
        const tehSupportInfo = state.DatabaseCardState.tehSupportInfo!;
        return {
            tehSupportInfo
        };
    }
)(AuthenticationPassedViewClass);