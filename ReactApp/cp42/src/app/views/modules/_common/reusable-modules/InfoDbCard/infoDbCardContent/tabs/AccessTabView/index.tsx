import { Box, Chip, Pagination, Tooltip } from '@mui/material';
import { TPermissions } from 'app/api/endpoints/accountUsers/request';
import { TGetAccountProfileResponse, TGetGivenAccountProfilesResponse } from 'app/api/endpoints/accountUsers/response';
import { TLocale } from 'app/api/endpoints/global/response';
import { REDUX_API } from 'app/api/useReduxApi';
import { AccountDatabaseAccessState } from 'app/common/enums/AccountDatabaseAccessStateEnum';
import { configurationProfilesToRole } from 'app/common/functions';
import { getEmptyUuid } from 'app/common/helpers';
import { TimerHelper } from 'app/common/helpers/TimerHelper';
import { getGivenAccountProfilesListSlice, TChangeRequestDataPayload } from 'app/modules/accountUsers';
import { ReceiveAccessThunkParams } from 'app/modules/infoDbCard/store/reducers/receiveAccessInfoDbCardReducer/params';
import { TAppDispatch } from 'app/redux';
import { AppReduxStoreState } from 'app/redux/types';
import { COLORS } from 'app/utils';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { IReduxForm, ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { ClockLoading } from 'app/views/components/controls';
import { CheckBoxForm, DropDownListWithCheckboxes } from 'app/views/components/controls/forms';
import { DropDownListWithCheckboxItem } from 'app/views/components/controls/forms/DropDownListWithCheckboxes/types';
import CustomizedSwitches from 'app/views/components/controls/Switch/views/SwitchViews';
import { TextOut } from 'app/views/components/TextOut';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { TableViewProps } from 'app/views/modules/_common/components/CommonTableWithFilter/TableView';
import { AccessInProcess } from 'app/views/modules/_common/reusable-modules/InfoDbCard/functions/AccessInProcess';
import { AccessInfoDbListFilterForm } from 'app/views/modules/_common/reusable-modules/InfoDbCard/infoDbCardContent/tabs/AccessTabView/AccessInfoDbListFilterFormView';
import { DbStateLineAccessViews } from 'app/views/modules/_common/reusable-modules/InfoDbCard/infoDbCardContent/tabs/AccessTabView/DbStateLineAccess/DbStateLine';
import { InfoDbAccessFilterDataForm } from 'app/views/modules/AccountManagement/InformationBases/types/InfoDbAccessFilterDataForm';
import { AccountDatabaseListFilterDataFormFieldNamesType } from 'app/views/modules/DatabaseList/types';
import { GetAvailabilityInfoDb } from 'app/web/api/InfoDbProxy/responce-dto/GetAvailabilityInfoDb';
import { DatabaseAccessesDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/AccessInfoDbDataModel';
import { EternalUserDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/EternalUserDataModel';
import { InfoDbCardAccountDatabaseInfoDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/InfoDbCardAccountDatabaseInfoDataModel';
import cn from 'classnames';
import React from 'react';
import { connect } from 'react-redux';
import css from '../../../styles.module.css';

type StateProps = {
    accessInfoDb: DatabaseAccessesDataModel[],
    commonDetails: InfoDbCardAccountDatabaseInfoDataModel;
    hasAccessReceived: boolean;
    availability: GetAvailabilityInfoDb;
    profilesData: TGetAccountProfileResponse[];
    givenProfilesData: TGetGivenAccountProfilesResponse | null;
    requestData: TPermissions[];
    locale: TLocale;
};

type DispatchProps = {
    dispatchGetAccountProfileList: (locale?: TLocale) => void;
    dispatchGetGivenAccountProfilesList: (userId?: string, databaseId?: string, needShowLoading?: boolean) => void;
    dispatchChangeProfileRequestData: (data: TChangeRequestDataPayload) => void;
};

type GetProps = ReduxFormProps<InfoDbAccessFilterDataForm> & {
    addUsers: (result: EternalUserDataModel) => void;
    users: EternalUserDataModel[];
    onValueChangeCheckBox: (value: string) => void;
    valueCheck: string[];
    onValueChangeAll: () => void;
    allCheck: boolean;
    onResetAddUser: () => void;
    isOpen: boolean;
    successAccess: boolean;
    accessPermissions: boolean;
    isOnDelimiters?: boolean;
    dispatchAccessData?: (args: ReceiveAccessThunkParams) => void;
    isAdmin?: boolean;
    showAccessOrPayDialog: () => void;
};

type OwnState = {
    isFirstShowWarning: boolean;
    currentPage: number;
    tabValue: number;
    isOpenProfileSelect: boolean;
};

type AllProps = GetProps & StateProps & DispatchProps & FloatMessageProps;

const pageSize = 10;

export class AccessViewClass extends React.Component<AllProps, OwnState> {
    private intervalId: NodeJS.Timeout | number;

    private readonly emptyUuid = getEmptyUuid();

    public constructor(props: AllProps) {
        super(props);
        this.state = {
            tabValue: 0,
            currentPage: 1,
            isFirstShowWarning: true,
            isOpenProfileSelect: false
        };
        this.intervalId = setInterval(() => void 0, TimerHelper.waitTimeRefreshData);
        this.onValueChange = this.onValueChange.bind(this);
        this.onTabChange = this.onTabChange.bind(this);
        this.checkStateAccess = this.checkStateAccess.bind(this);
        this.getFilterContentView = this.getFilterContentView.bind(this);
        this.profileChangeHandler = this.profileChangeHandler.bind(this);
        this.currentPageHandler = this.currentPageHandler.bind(this);
        this.getProfile = this.getProfile.bind(this);
    }

    public componentDidMount(): void {
        this.checkStateAccess();

        if (this.props.isOnDelimiters) {
            this.props.dispatchGetAccountProfileList(this.props.locale);
            this.props.dispatchGetGivenAccountProfilesList('', this.props.commonDetails.id);
        }
    }

    public componentDidUpdate(prevProps: AllProps, _: OwnState) {
        if (
            (!prevProps.hasAccessReceived && this.props.hasAccessReceived) ||
            (
                this.props.isOnDelimiters &&
                this.props.givenProfilesData &&
                this.props.givenProfilesData.permissions.find(permission => permission.state === 'Processing')
            )
        ) {
            clearInterval(this.intervalId);
            this.checkStateAccess();
        }

        if (prevProps.isOpen && !this.props.isOpen) {
            clearInterval(this.intervalId);
        }

        if (prevProps.successAccess !== this.props.successAccess) {
            this.setState({
                tabValue: 0
            });
        }
    }

    public componentWillUnmount(): void {
        clearInterval(this.intervalId);
    }

    private onTabChange(value: number) {
        if (this.props.dispatchAccessData) {
            this.props.onResetAddUser();
            this.setState({ tabValue: value, currentPage: 1 });
            this.props.dispatchAccessData({
                databaseId: this.props.commonDetails.id,
                searchString: this.props.reduxForm.getReduxFormFields(false).searchLineUser,
                usersByAccessFilterType: value,
                force: true,
            });
        }
    }

    private onValueChange<TValue, TForm extends string = AccountDatabaseListFilterDataFormFieldNamesType>(formName: TForm, newValue: TValue) {
        this.props.reduxForm.updateReduxFormFields({
            [formName]: newValue
        });
        this.setState({ currentPage: 1 });
    }

    private getFilterContentView(ref: React.Ref<IReduxForm<object>>, onFilterChanged: (filterData: object, filterFormName: string) => void) {
        return (
            <AccessInfoDbListFilterForm
                addUsers={ this.props.addUsers }
                ref={ ref }
                isFilterFormDisabled={ false }
                onFilterChanged={ onFilterChanged }
                onTabChange={ this.onTabChange }
                onValueChange={ this.onValueChange }
                tabValue={ this.state.tabValue }
                isAdmin={ this.props.isAdmin }
                isOnDelimiters={ this.props.isOnDelimiters }
            />
        );
    }

    private getProfile(_: boolean, rowData: DatabaseAccessesDataModel) {
        const { profilesData, givenProfilesData, commonDetails, isAdmin, requestData } = this.props;

        const isInProcessing = !!givenProfilesData?.permissions.find(givenProfile => givenProfile.id === rowData.userId && givenProfile.state === 'Processing');
        const isInError = !!givenProfilesData?.permissions.find(givenProfile => givenProfile.id === rowData.userId && givenProfile.state === 'Error');
        const changeDatabaseStatusInfo = this.props.requestData.find(requestInfo => requestInfo['user-id'] === rowData.userId);
        const givenProfileInfo = givenProfilesData?.permissions.find(givenProfile => givenProfile.id === rowData.userId);
        const isSwitchChecked = !!(changeDatabaseStatusInfo?.['allow-backup'] ?? givenProfileInfo?.allowBackup);

        return (
            <Box display="flex" flexDirection="column" gap="5px" marginY="5px">
                <DropDownListWithCheckboxes
                    hiddenSelectAll={ true }
                    handleOpenSelect={ () => {
                        this.setState({ isOpenProfileSelect: true });
                        this.props.onValueChangeCheckBox(rowData.userId);
                    } }
                    handleCloseSelect={ () => {
                        this.setState({ isOpenProfileSelect: false });
                        if (requestData.find(user => user['user-id'] === rowData.userId)) {
                            this.props.showAccessOrPayDialog();
                        }
                        this.props.onResetAddUser();
                    } }
                    noUpdateValueChangeHandler={ true }
                    deepCompare={ true }
                    disabled={ isInProcessing || isAdmin }
                    selectAllText="Выбрать все пользовательские роли"
                    additionalElement={
                        <Box display="flex" justifyContent="space-between" alignItems="center" width="100%">
                            <TextOut fontWeight={ 500 }>Разрешить выгрузку базы</TextOut>
                            <CustomizedSwitches
                                onChange={ () => {
                                    if (this.props.requestData.find(data => data['user-id'] === rowData.userId)) {
                                        this.props.dispatchChangeProfileRequestData({ userId: rowData.userId, databaseId: commonDetails.id, allowBackup: !isSwitchChecked });
                                    } else {
                                        this.props.dispatchChangeProfileRequestData({ userId: rowData.userId, profiles: givenProfileInfo?.profiles, databaseId: commonDetails.id, allowBackup: !isSwitchChecked });
                                    }
                                } }
                                checked={ isSwitchChecked }
                                disabled={
                                    isInError ||
                                    !(changeDatabaseStatusInfo ? changeDatabaseStatusInfo?.profiles.find(profile => profile.admin) : givenProfileInfo?.profiles.find(profile => profile.admin)) ||
                                    isInProcessing ||
                                    isAdmin
                                }
                            />
                        </Box>
                    }
                    renderValue={ selected => (
                        <Box sx={ { display: 'flex', flexWrap: 'wrap', gap: 0.5 } }>
                            {
                                selected.map(({ value, text }) => {
                                    const stateProfile = givenProfileInfo?.profiles.find(profile => profile.id === value);

                                    return !stateProfile?.error
                                        ? <Chip key={ value } label={ text } />
                                        : (
                                            <Tooltip key={ value } placement="top" title={ stateProfile.description }>
                                                <Chip label={ text } sx={ { color: COLORS.error } } />
                                            </Tooltip>
                                        );
                                })
                            }
                        </Box>
                    ) }
                    items={
                        profilesData.flatMap(profile => {
                            if (profile.code === commonDetails.configurationCode) {
                                return profile.profiles.flatMap(pr => {
                                    const currentProfile = {
                                        text: pr.name,
                                        value: pr.id,
                                        checked: rowData.hasAccess && changeDatabaseStatusInfo
                                            ? !!changeDatabaseStatusInfo.profiles.find(profileInfo => profileInfo.id === this.emptyUuid ? profileInfo.name === pr.name : profileInfo.id === pr.id)
                                            : !!givenProfileInfo?.profiles.find(profileInfo => profileInfo.id === this.emptyUuid ? profileInfo.name === pr.name : profileInfo.id === pr.id) ||
                                            !!this.props.requestData.find(data => data['user-id'] === rowData.userId)?.profiles.find(({ id, name }) => id === this.emptyUuid ? name === pr.name : id === pr.id)
                                    };

                                    if (!pr.predefined) {
                                        if (pr.databases.includes(this.props.commonDetails.id)) {
                                            return currentProfile;
                                        }

                                        return [];
                                    }

                                    return currentProfile;
                                });
                            }

                            return [];
                        })
                    }
                    label=""
                    formName="profiles"
                    onValueChange={ (_formName, items) => this.profileChangeHandler(items, rowData.userId) }
                />
                {
                    isInProcessing && (
                        <Box display="flex" gap="5px">
                            <ClockLoading />
                            <TextOut fontSize={ 10 } style={ { color: COLORS.warning } }>В процессе редактирования</TextOut>
                        </Box>
                    )
                }
                { isInError && <TextOut fontSize={ 10 } style={ { color: COLORS.error } }>Ошибка редактирования</TextOut> }
            </Box>
        );
    }

    private checkStateAccess() {
        if (
            this.props.accessInfoDb.filter(i => i.state === AccountDatabaseAccessState.ProcessingGrant || i.state === AccountDatabaseAccessState.ProcessingDelete).length ||
            (
                this.props.givenProfilesData &&
                this.props.givenProfilesData.permissions.find(permission => permission.state === 'Processing')
            )
        ) {
            this.intervalId = setInterval(() => {
                if (this.props.isOnDelimiters && !this.state.isOpenProfileSelect) {
                    this.props.dispatchGetGivenAccountProfilesList('', this.props.commonDetails.id, false);
                }
            }, TimerHelper.waitTimeRefreshData);
        } else {
            clearInterval(this.intervalId);
        }
    }

    private profileChangeHandler(items: DropDownListWithCheckboxItem[], userId: string) {
        const { dispatchChangeProfileRequestData, profilesData, commonDetails, requestData, givenProfilesData } = this.props;

        if (profilesData) {
            const configurationProfiles = profilesData.find(({ code }) => commonDetails.configurationCode === code);

            if (configurationProfiles) {
                const { adminRole, userRole } = configurationProfilesToRole(configurationProfiles.profiles, items);

                const prevProfiles = requestData?.find(data => data['user-id'] === userId)?.profiles ?? givenProfilesData?.permissions.find(el => el.id === userId)?.profiles;

                if (items.length > 1 && this.state.isFirstShowWarning && adminRole.length && userRole.length) {
                    this.props.floatMessage.show(MessageType.Info, 'Установка профиля доступа "Администратор" одновременно с другими профилями через личный кабинет не поддерживается');
                    this.setState({ isFirstShowWarning: false });
                }

                if (prevProfiles && prevProfiles.some(profile => profile.admin)) {
                    dispatchChangeProfileRequestData({ profiles: userRole.length ? userRole : adminRole, userId, databaseId: commonDetails.id });
                } else {
                    dispatchChangeProfileRequestData({ profiles: adminRole.length ? adminRole : userRole, userId, databaseId: commonDetails.id });
                }
            }
        }
    }

    private currentPageHandler(_: React.ChangeEvent<unknown>, page: number) {
        this.setState({ currentPage: page });
    }

    public render() {
        const { currentPage } = this.state;
        const { givenProfilesData, isOnDelimiters, profilesData } = this.props;
        const reduxForm = this.props.reduxForm.getReduxFormFields(false);
        const dataset = reduxForm.searchLineUser
            ? [...this.props.users, ...this.props.accessInfoDb].filter(item =>
                item.userLogin?.toLowerCase().includes(reduxForm.searchLineUser.toLowerCase()) ||
                item.userEmail?.toLowerCase().includes(reduxForm.searchLineUser.toLowerCase()) ||
                item.userFullName?.toLowerCase().includes(reduxForm.searchLineUser.toLowerCase())
            )
            : [...this.props.users, ...this.props.accessInfoDb];

        const tableProps: TableViewProps<DatabaseAccessesDataModel | EternalUserDataModel> = {
            dataset: dataset.slice(pageSize * (currentPage - 1), pageSize * currentPage),
            keyFieldName: 'userId',
            fieldsView: {
                userId: {
                    caption: <></>,
                    format: () => <></>
                },
                userLogin: {
                    caption: 'Логин (ФИО)',
                    fieldContentNoWrap: false,
                    isSortable: false,
                    format: (value: string, item: DatabaseAccessesDataModel) =>
                        <Box>
                            <TextOut inDiv={ true }>{ value }</TextOut>
                            <TextOut inDiv={ true }>{ item.userFullName ? `(${ item.userFullName })` : '' }</TextOut>
                            <DbStateLineAccessViews extensionAccess={ item.state } />
                        </Box>
                },
                userEmail: {
                    caption: 'Эл. адрес',
                    fieldContentNoWrap: false,
                    isSortable: false,
                    format: (value: string, item: DatabaseAccessesDataModel) =>
                        <>
                            <TextOut inDiv={ true } fontSize={ 13 }>
                                { value }
                            </TextOut>
                            {
                                (Object.hasOwn(item, 'accessCost') || item.isExternalAccess) && (
                                    <TextOut inDiv={ true } style={ { color: COLORS.error, fontSize: '10px' } }>
                                        { item.accountInfo }
                                    </TextOut>
                                )
                            }
                        </>
                }
            }
        };

        if (!isOnDelimiters) {
            tableProps.fieldsView.userId = {
                caption: <CheckBoxForm
                    className={ cn(css.check) }
                    formName="isDefault"
                    isReadOnly={
                        !this.props.availability.PermissionsForWorkingWithDatabase.HasPermissionForDisplayPayments ||
                        this.props.accessPermissions ||
                        !!(givenProfilesData && givenProfilesData.permissions.find(permission => permission.state === 'Processing')) ||
                        this.props.isAdmin
                    }
                    label=""
                    isChecked={ ([...this.props.accessInfoDb, ...this.props.users].length === this.props.valueCheck.length) || this.props.allCheck }
                    onValueChange={ () => this.props.onValueChangeAll() }
                />,
                fieldContentNoWrap: false,
                fieldWidth: '3%',
                format: (value: string, item: DatabaseAccessesDataModel) =>
                    <CheckBoxForm
                        className={ cn(css.check) }
                        formName={ value }
                        isReadOnly={
                            AccessInProcess(item.state) ||
                            !this.props.availability.PermissionsForWorkingWithDatabase.HasPermissionForDisplayPayments ||
                            this.props.accessPermissions ||
                            !!(givenProfilesData && givenProfilesData.permissions.find(permission => permission.id === item.userId && permission.state === 'Processing')) ||
                            this.props.isAdmin
                        }
                        label=""
                        isChecked={ this.props.valueCheck.includes(value) }
                        onValueChange={ () => this.props.onValueChangeCheckBox(value) }
                    />
            };
        }

        if (isOnDelimiters && givenProfilesData && profilesData) {
            tableProps.fieldsView.hasAccess = {
                caption: 'Профиль доступа',
                fieldWidth: '50%',
                format: this.getProfile
            };
        }

        return (
            <>
                <CommonTableWithFilter
                    uniqueContextProviderStateId="AccessInfoDbListContextProviderStateId"
                    filterProps={ { getFilterContentView: this.getFilterContentView } }
                    tableProps={ tableProps }
                />
                <Box gap="5px" justifyContent="flex-end" display="flex" marginTop="10px">
                    <Pagination page={ currentPage } count={ Math.ceil(dataset.length / pageSize) } onChange={ this.currentPageHandler } />
                </Box>
            </>
        );
    }
}

const { getAccountProfilesList, getGivenAccountProfilesList } = REDUX_API.ACCOUNT_USERS_REDUX;
const { changeRequestData } = getGivenAccountProfilesListSlice.actions;

export const AccessViewConnect = connect<StateProps, DispatchProps, GetProps, AppReduxStoreState>(
    state => {
        const databaseCardState = state.InfoDbCardState;
        const infoDbListState = state.InfoDbListState;
        const accessInfoDb = databaseCardState.databaseAccesses;
        const { hasAccessReceived } = databaseCardState.hasSuccessFor;
        const { commonDetails } = databaseCardState;
        const availability = infoDbListState.AvailabilityInfoDb;
        const { accountProfilesListReducer: { data: profilesData }, givenAccountProfilesListReducer: { data: givenProfilesData, requestData } } = state.AccountUsersState;
        const { locale } = state.Global.getCurrentSessionSettingsReducer.settings.currentContextInfo;

        return {
            accessInfoDb,
            commonDetails,
            hasAccessReceived,
            availability,
            profilesData: profilesData ?? [],
            givenProfilesData,
            requestData,
            locale
        };
    },
    (dispatch: TAppDispatch) => {
        return {
            dispatchChangeProfileRequestData: (data: TChangeRequestDataPayload) => dispatch(changeRequestData(data)),
            dispatchGetAccountProfileList: (locale?: TLocale) => getAccountProfilesList(dispatch, locale),
            dispatchGetGivenAccountProfilesList: (userId?: string, databaseId?: string, needShowLoading?: boolean) => getGivenAccountProfilesList(dispatch, userId, databaseId, needShowLoading)
        };
    }
)(withFloatMessages(AccessViewClass));

export const AccessView = withReduxForm(AccessViewConnect, {
    reduxFormName: 'Access_Info_Db_List_Filter_Form',
    resetOnUnmount: true
});