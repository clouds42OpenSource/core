/**
 * Состояние контекста для выбора информационных баз для миграции
 */
export type SelectedDatabasesToMigrateContextState = Readonly<
    {
        /**
         * Выбранные номера информационных баз для миграции
         */
        selectedDatabaseNumbers: Array<string>;

        /**
         * ID хранилища, куда мигрировать выбранные базы
         */
        selectedStorageId: string;

        /**
         * Добавляет базу в список выбранных для миграции
         * @param databaseNumber номер базы для добавления в список выбранных
         */
        addDatabaseNumber: (databaseNumber: string) => void;
        /**
         * Удаляет базу из списка выбранных для миграции
         * @param databaseNumber номер базы для удаления из списка выбранных
         */
        removeDatabaseNumber: (databaseNumber: string) => void;

        /**
         * Устанавливает ID хранилища, куда мигрировать выбранные базы
         * @param storageId ID хранилища
         */
        setSelectedStorage: (storageId: string) => void;

        /**
         * Сбрасывает все выбранные свойства
         */
        reset: () => void;
    }
>;