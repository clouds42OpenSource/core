import { AccountUserGroup, DatabaseState, DbPublishState } from 'app/common/enums';
import { useAppSelector } from 'app/hooks';
import { OutlinedButton } from 'app/views/components/controls/Button';
import { InfoDbCardPermissionsEnum } from 'app/web/common/enums/InfoDbCardPermissionsEnum';
import { useState } from 'react';

import { ChangeTypeRequestDbDialog } from '../modals';

export const ChangeTypeRequest = () => {
    const { getUserPermissionsReducer: { permissions }, getCurrentSessionSettingsReducer: { settings: { currentContextInfo: { currentUserGroups, locale } } } } = useAppSelector(state => state.Global);
    const { informationBase } = useAppSelector(state => state.InformationBases.informationBasesList);
    const { id, v82Name, isFile, state, publishState, isDbOnDelimiters } = informationBase?.database ?? {};

    const [isOpen, setIsOpen] = useState(false);
    const [isEnabled, setIsEnabled] = useState(true);

    const openDialogMessage = () => {
        setIsOpen(true);
    };

    const closeDialogMessage = () => {
        setIsOpen(false);
    };

    if (!(
        permissions.includes(InfoDbCardPermissionsEnum.AccountDatabase_Change) &&
        state === DatabaseState.Ready &&
        publishState !== DbPublishState.PendingUnpublication &&
        publishState !== DbPublishState.PendingPublication &&
        currentUserGroups.some(role => [AccountUserGroup.AccountAdmin, AccountUserGroup.Hotline, AccountUserGroup.CloudAdmin, AccountUserGroup.CloudSE].includes(role)) &&
        (locale !== 'ru-kz' && locale !== 'ru-ua') &&
        !isDbOnDelimiters
    )) {
        return null;
    }

    return (
        <>
            <OutlinedButton kind="success" onClick={ openDialogMessage } isEnabled={ isEnabled }>
                <i className="fa fa-database" />&nbsp;Оставить заявку на перевод в { isFile ? 'серверную' : 'файловую' }
            </OutlinedButton>
            <ChangeTypeRequestDbDialog
                isOpen={ isOpen }
                isFile={ isFile }
                databaseName={ v82Name ?? '' }
                databaseId={ id ?? '' }
                onNoClick={ closeDialogMessage }
                setIsEnabled={ setIsEnabled }
            />
        </>
    );
};