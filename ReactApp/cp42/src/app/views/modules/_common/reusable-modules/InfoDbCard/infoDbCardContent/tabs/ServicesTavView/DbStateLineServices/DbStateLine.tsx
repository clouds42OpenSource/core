import { ExtensionStateServicesEnum } from 'app/common/enums/ExtensionStateServicesEnum';
import { TimerHelper } from 'app/common/helpers/TimerHelper';
import { COLORS } from 'app/utils';
import { TextOut } from 'app/views/components/TextOut';
import { ServicesInfoDbDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/ServicesInfoDbDataModel';
import React from 'react';

type OwnProps = {
   extensionState: ExtensionStateServicesEnum | null;
   getInterval: (id: string) => NodeJS.Timeout | number;
   item: ServicesInfoDbDataModel;
   isOpen: boolean;
};

export class DbStateLineServiceViews extends React.Component<OwnProps> {
    private intervalId: NodeJS.Timeout | number;

    public constructor(props: OwnProps) {
        super(props);
        this.initialInterval = this.initialInterval.bind(this);
        this.intervalId = setInterval(() => void 0, TimerHelper.waitTimeRefreshData);
    }

    public componentDidMount() {
        this.initialInterval();
    }

    public componentDidUpdate(prevProps: OwnProps) {
        if (prevProps.item.extensionState !== this.props.item.extensionState) {
            this.initialInterval();
        }

        if (!this.props.isOpen && prevProps.isOpen) {
            clearInterval(this.intervalId);
        }
    }

    public componentWillUnmount() {
        clearInterval(this.intervalId);
    }

    private initialInterval() {
        const { item: { extensionState }, getInterval } = this.props;
        clearInterval(this.intervalId);

        if (extensionState === ExtensionStateServicesEnum.ProcessingInstall || extensionState === ExtensionStateServicesEnum.ProcessingDelete) {
            this.intervalId = getInterval(this.props.item.id);
        } else {
            clearInterval(this.intervalId);
        }
    }

    public render() {
        const style: React.CSSProperties = { color: COLORS.main, display: 'flex', gap: '3px', alignItems: 'flex-start' };

        switch (this.props.extensionState) {
            case ExtensionStateServicesEnum.ProcessingInstall:
                return (
                    <TextOut inDiv={ true } fontSize={ 11 } style={ style }>
                        Расширение в процессе установки
                        <img src="img/database-icons/animated-clock.gif" width={ 13 } height={ 13 } alt="timer" />
                    </TextOut>
                );
            case ExtensionStateServicesEnum.ProcessingDelete:
                return (
                    <TextOut inDiv={ true } fontSize={ 11 } style={ { ...style, color: COLORS.error } }>
                        Расширение в процессе удаления
                        <img src="img/database-icons/animated-clock.gif" width={ 13 } height={ 13 } alt="timer" />
                    </TextOut>
                );
            case ExtensionStateServicesEnum.ErrorProcessingDelete:
                return (
                    <TextOut fontSize={ 11 } style={ { color: COLORS.error } }>Ошибка в процессе удаления</TextOut>
                );
            case ExtensionStateServicesEnum.ErrorProcessingInstall:
                return (
                    <TextOut fontSize={ 11 } style={ { color: COLORS.error } }>Ошибка в процессе установки</TextOut>
                );
            default:
                return null;
        }
    }
}