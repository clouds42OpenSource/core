import { FETCH_API } from 'app/api/useFetchApi';
import { REDUX_API } from 'app/api/useReduxApi';
import { yandexCounter, YandexMetrics } from 'app/common/_external-services/yandexMetrics';
import { AccountUserGroup, DatabaseState } from 'app/common/enums';
import { EMessageType, useAppDispatch, useAppSelector, useFloatMessages, useGetCurrentUserGroups } from 'app/hooks';
import { ContainedButton } from 'app/views/components/controls/Button';
import { InfoDbCardPermissionsEnum } from 'app/web/common/enums/InfoDbCardPermissionsEnum';
import { useState } from 'react';

import { DeleteInfoDbDialog } from '../modals';

type TProps = {
    onDialogClose: () => void;
};

const { deleteDatabases } = FETCH_API.INFORMATION_BASES;
const { getInformationBases } = REDUX_API.INFORMATION_BASES_REDUX;

export const DeleteInfoDb = ({ onDialogClose }: TProps) => {
    const { show } = useFloatMessages();
    const dispatch = useAppDispatch();
    const currentUserGroups = useGetCurrentUserGroups();

    const { getUserPermissionsReducer: { permissions } } = useAppSelector(state => state.Global);
    const { informationBase } = useAppSelector(state => state.InformationBases.informationBasesList);
    const { id, v82Name, state } = informationBase?.database ?? {};

    const [isOpen, setIsOpen] = useState(false);

    const openDialogHandler = () => {
        setIsOpen(true);
    };

    const closeDialogHandler = () => {
        setIsOpen(false);
    };

    const deleteConfirmHandler = async () => {
        const { success, message } = await deleteDatabases([id ?? '']);

        if (success) {
            setIsOpen(false);
            yandexCounter.getInstance().reachGoal(YandexMetrics.DeleteDatabaseMetric);

            void getInformationBases(dispatch);
            onDialogClose();
        } else {
            show(EMessageType.error, message);
        }
    };

    if (!(
        state !== DatabaseState.DelitingToTomb &&
        permissions.includes(InfoDbCardPermissionsEnum.AccountDatabase_DeleteToTomb) &&
        currentUserGroups.some(role => [AccountUserGroup.AccountAdmin, AccountUserGroup.Hotline, AccountUserGroup.CloudAdmin, AccountUserGroup.CloudSE].includes(role))
    )) {
        return null;
    }

    return (
        <>
            <ContainedButton kind="error" onClick={ openDialogHandler }>
                <i className="fa fa-trash" />&nbsp;Удалить
            </ContainedButton>
            <DeleteInfoDbDialog
                isOpenDialog={ isOpen }
                databaseName={ v82Name ?? '' }
                onNoClick={ closeDialogHandler }
                onYesClick={ deleteConfirmHandler }
            />
        </>
    );
};