import { Box, Link, MenuItem, OutlinedInput, Select } from '@mui/material';
import { CheckBoxForm } from 'app/views/components/controls/forms';
import dayjs from 'dayjs';
import { memo, useEffect, useState } from 'react';
import { TEditInformationBaseRequest } from 'app/api/endpoints/informationBases/request';
import { AccountDatabaseRestoreModelType, AccountUserGroup, DatabaseState, DbPublishState, PlatformType, PlatformTypeDescription } from 'app/common/enums';
import { useAppSelector } from 'app/hooks';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { InfoDbCardPermissionsEnum } from 'app/web/common/enums/InfoDbCardPermissionsEnum';

type TProps = {
    editDatabase: (body: Partial<TEditInformationBaseRequest>) => Promise<void>;
};

export const General = memo(({ editDatabase }: TProps) => {
    const {
        Global: { getUserPermissionsReducer: { permissions }, getCurrentSessionSettingsReducer: { settings: { currentContextInfo: { currentUserGroups } } } },
        InformationBases: { informationBasesList: { informationBase, availability, isOther } }
    } = useAppSelector(state => state);
    const { database, accountCardEditModeDictionaries, fieldsAccessReadModeInfo } = informationBase ?? {};

    const [name, setName] = useState('');

    useEffect(() => {
        if (database && database.v82Name) {
            setName(database.v82Name);
        }
    }, [database]);

    const changeV82Name = () => {
        if (name !== database?.v82Name) {
            void editDatabase({ v82Name: name });
        }
    };

    if (database) {
        return (
            <Box display="grid" gridTemplateColumns="1fr 1fr" gap="20px">
                <Box display="flex" flexDirection="column" gap="10px">
                    { database.configurationName && (
                        <FormAndLabel label="Конфигурация">
                            <OutlinedInput
                                fullWidth={ true }
                                disabled={ true }
                                value={ database.configurationName }
                            />
                        </FormAndLabel>
                    ) }
                    { currentUserGroups.some(role => [AccountUserGroup.Hotline, AccountUserGroup.CloudAdmin, AccountUserGroup.CloudSE].includes(role)) && (
                        <FormAndLabel label="Шаблон">
                            {
                                !database.isDbOnDelimiters &&
                                permissions.includes(InfoDbCardPermissionsEnum.AccountDatabases_EditFull) &&
                                !!accountCardEditModeDictionaries?.availableDatabaseTemplates.length &&
                                database.state !== DatabaseState.DeletedToTomb
                                    ? (
                                        <Select onChange={ ({ target: { value } }) => editDatabase({ databaseTemplateId: value }) } value={ database.dbTemplate } variant="outlined" fullWidth={ true }>
                                            { accountCardEditModeDictionaries.availableDatabaseTemplates.map(({ key, value }) => (
                                                <MenuItem key={ key } value={ key }>{ value }</MenuItem>
                                            )) }
                                        </Select>
                                    )
                                    : (
                                        <OutlinedInput
                                            fullWidth={ true }
                                            disabled={ true }
                                            value={ database.templateName }
                                        />
                                    )
                            }
                        </FormAndLabel>
                    ) }
                    <FormAndLabel label="Тип">
                        <OutlinedInput
                            fullWidth={ true }
                            disabled={ true }
                            value={ database.isFile ? 'Файловая' : database.isDbOnDelimiters ? 'Платформа 42' : 'Серверная' }
                        />
                    </FormAndLabel>
                    <FormAndLabel label="Платформа">
                        {
                            permissions.includes(InfoDbCardPermissionsEnum.AccountDatabases_EditPlatform) &&
                            database.state === DatabaseState.Ready &&
                            database.isFile &&
                            !database.isDbOnDelimiters &&
                            database.stable82Version &&
                            database.commonDataForWorkingWithDB.availablePlatformTypes.length &&
                            currentUserGroups.some(role => [AccountUserGroup.AccountAdmin, AccountUserGroup.Hotline, AccountUserGroup.CloudAdmin, AccountUserGroup.CloudSE].includes(role)) &&
                            !isOther
                                ? (
                                    <Select onChange={ ({ target: { value } }) => typeof value !== 'string' ? editDatabase({ platformType: value }) : void 0 } variant="outlined" value={ database.platformType } fullWidth={ true }>
                                        { database.commonDataForWorkingWithDB.availablePlatformTypes.map(({ key, value }) => (
                                            <MenuItem value={ key }>{ value }</MenuItem>
                                        )) }
                                    </Select>
                                )
                                : (
                                    <OutlinedInput
                                        fullWidth={ true }
                                        disabled={ true }
                                        value={ PlatformTypeDescription[database.platformType] }
                                    />
                                )
                        }
                    </FormAndLabel>
                    <FormAndLabel label="Релиз платформы">
                        {
                            permissions.includes(InfoDbCardPermissionsEnum.AccountDatabases_EditPlatform) &&
                            database.alpha83Version &&
                            database.alpha83Version !== database.stable83Version &&
                            database.isFile &&
                            database.platformType === PlatformType.V83 &&
                            database.state !== DatabaseState.DetachedToTomb &&
                            accountCardEditModeDictionaries?.availableDestributionTypes.length &&
                            !isOther &&
                            currentUserGroups.some(role => [AccountUserGroup.AccountAdmin, AccountUserGroup.Hotline, AccountUserGroup.CloudAdmin, AccountUserGroup.CloudSE].includes(role)) &&
                            database.state !== DatabaseState.DeletedToTomb
                                ? (
                                    <Select onChange={ ({ target: { value } }) => typeof value !== 'string' ? editDatabase({ distributionType: value }) : void 0 } value={ database.distributionType } variant="outlined" fullWidth={ true }>
                                        { accountCardEditModeDictionaries.availableDestributionTypes.map(({ key, value }) => (
                                            <MenuItem value={ key }>{ value }</MenuItem>
                                        )) }
                                    </Select>
                                )
                                : (
                                    <OutlinedInput
                                        fullWidth={ true }
                                        disabled={ true }
                                        value={ database.platformType === PlatformType.V83 ? database.stable83Version : database?.stable83Version }
                                    />
                                )
                        }
                    </FormAndLabel>
                    { (database.isFile || database.isDbOnDelimiters) && (
                        <FormAndLabel label="Путь к базе">
                            <OutlinedInput
                                fullWidth={ true }
                                disabled={ true }
                                value={ database.databaseConnectionPath }
                            />
                        </FormAndLabel>
                    ) }
                    {
                        availability &&
                        availability.isMainServiceAllowed &&
                        availability.permissionsForWorkingWithDatabase.hasPermissionForWeb &&
                        database.publishState === DbPublishState.Published &&
                        database.state === DatabaseState.Ready &&
                        fieldsAccessReadModeInfo?.canShowWebPublishPath && (
                            <FormAndLabel label="Веб ссылка">
                                <Link target="_blank" href={ database.webPublishPath }>{ database.webPublishPath }</Link>
                            </FormAndLabel>
                        )
                    }
                    { !!database.sizeInMB && (
                        <FormAndLabel label="Размер">
                            <OutlinedInput
                                fullWidth={ true }
                                disabled={ true }
                                value={ `${ (database.sizeInMB / 1024).toFixed(3) } ГБ на ${ dayjs(database.calculateSizeDateTime).format('DD.MM.YYYY') }` }
                            />
                        </FormAndLabel>
                    ) }
                </Box>
                <Box display="flex" flexDirection="column" gap="10px">
                    { fieldsAccessReadModeInfo?.canShowDatabaseDetails && (
                        <>
                            {
                                currentUserGroups.some(role => [AccountUserGroup.Hotline, AccountUserGroup.CloudAdmin, AccountUserGroup.CloudSE].includes(role)) && (
                                    <FormAndLabel label="Идентификатор">
                                        <OutlinedInput
                                            fullWidth={ true }
                                            disabled={ true }
                                            value={ database.id }
                                        />
                                    </FormAndLabel>
                                )
                            }
                            <FormAndLabel label="Номер">
                                <OutlinedInput
                                    fullWidth={ true }
                                    disabled={
                                        database.state === DatabaseState.DeletedToTomb ||
                                        !currentUserGroups.some(role => [AccountUserGroup.CloudAdmin].includes(role))
                                    }
                                    value={ name }
                                    onChange={ e => setName(e.target.value) }
                                    onBlur={ changeV82Name }
                                />
                            </FormAndLabel>
                        </>
                    ) }
                    { !permissions.includes(InfoDbCardPermissionsEnum.AccountDatabases_EditFull) && !!database.version && (
                        <FormAndLabel label="Номер релиза">
                            <OutlinedInput
                                fullWidth={ true }
                                disabled={ true }
                                value={ database.version }
                            />
                        </FormAndLabel>
                    ) }
                    { permissions.includes(InfoDbCardPermissionsEnum.AccountDatabases_EditFull) && (
                        <FormAndLabel label="Дата создания">
                            <OutlinedInput
                                fullWidth={ true }
                                disabled={ true }
                                value={ dayjs(database.creationDate).format('DD.MM.YYYY') }
                            />
                        </FormAndLabel>
                    ) }
                    { !database.isFile && (
                        <FormAndLabel label="Сервер 1С">
                            <OutlinedInput
                                fullWidth={ true }
                                disabled={ true }
                                value={ database.v82Server }
                            />
                        </FormAndLabel>
                    ) }
                    { !database.isFile && fieldsAccessReadModeInfo?.canShowSqlServer && (
                        <FormAndLabel label="Сервер SQL">
                            <OutlinedInput
                                fullWidth={ true }
                                disabled={ true }
                                value={ database.sqlServer }
                            />
                        </FormAndLabel>
                    ) }
                    { database.archivePath && (
                        <FormAndLabel label="Путь к архиву">
                            <OutlinedInput
                                fullWidth={ true }
                                disabled={ true }
                                value={ database.archivePath }
                            />
                        </FormAndLabel>
                    ) }
                    { fieldsAccessReadModeInfo?.canShowDatabaseDetails && database.zoneNumber && (
                        <FormAndLabel label="Номер области">
                            <OutlinedInput
                                fullWidth={ true }
                                disabled={ true }
                                value={ database.zoneNumber }
                            />
                        </FormAndLabel>
                    ) }
                    { !database.isFile && !database.isDbOnDelimiters && database.restoreModelType && (
                        <FormAndLabel label="Модель восстановления">
                            { database.canChangeRestoreModel
                                ? (
                                    <Select
                                        variant="outlined"
                                        fullWidth={ true }
                                        value={ database.restoreModelType }
                                        onChange={ e => void editDatabase({ restoreModel: e.target.value as AccountDatabaseRestoreModelType }) }
                                    >
                                        {
                                            database.commonDataForWorkingWithDB.availableAccountDatabaseRestoreModelTypes.map(({ key, value }) => (
                                                <MenuItem key={ `${ key }-${ value }` } value={ key }>{ value }</MenuItem>
                                            ))
                                        }
                                    </Select>
                                )
                                : (
                                    <OutlinedInput
                                        fullWidth={ true }
                                        disabled={ true }
                                        value={ database.commonDataForWorkingWithDB.availableAccountDatabaseRestoreModelTypes.find(type => type.key === database.restoreModelType)?.value ?? '' }
                                    />
                                )
                            }
                        </FormAndLabel>
                    ) }
                    { permissions.includes(InfoDbCardPermissionsEnum.AccountDatabases_EditStatus) &&
                        currentUserGroups.some(role => [AccountUserGroup.CloudAdmin].includes(role)) && (
                        <FormAndLabel label="Статус">
                            { accountCardEditModeDictionaries?.availableDatabaseStates.length
                                ? (
                                    <Select
                                        variant="outlined"
                                        value={ database.state }
                                        onChange={ e => void editDatabase({ databaseState: e.target.value as DatabaseState }) }
                                        fullWidth={ true }
                                    >
                                        { database.commonDataForWorkingWithDB.availableDatabaseStatuses.map(({ key, value }) => (
                                            <MenuItem key={ `${ key }-${ value }` } value={ key }>{ value }</MenuItem>
                                        )) }
                                    </Select>
                                )
                                : (
                                    <OutlinedInput
                                        fullWidth={ true }
                                        disabled={ true }
                                        value={ database.state }
                                    />
                                )
                            }
                        </FormAndLabel>
                    ) }
                    {
                        permissions.includes(InfoDbCardPermissionsEnum.AccountDatabases_EditUsedWebServices) &&
                        !database.isDbOnDelimiters &&
                        database.state !== DatabaseState.DeletedToTomb &&
                        fieldsAccessReadModeInfo?.canShowUsedWebServices &&
                        currentUserGroups.some(role => [AccountUserGroup.CloudAdmin, AccountUserGroup.CloudSE].includes(role)) && (
                            <CheckBoxForm onValueChange={ () => void editDatabase({ usedWebServices: !database.usedWebServices }) } isChecked={ database.usedWebServices } label="Опубликованы веб сервисы" formName="" />
                        )
                    }
                </Box>
            </Box>

        );
    }

    return null;
});