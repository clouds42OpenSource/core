import { REDUX_API } from 'app/api/useReduxApi';
import { hasComponentChangesFor } from 'app/common/functions';
import { AppReduxStoreState } from 'app/redux/types';
import { GotoToAccountButton } from 'app/views/modules/_common/reusable-modules/AccountCard/views/_buttons/GotoToAccountButton';
import { AccountCardButtonsVisibilityDataModel } from 'app/web/InterlayerApiProxy/AccountCardApiProxy/receiveAccountCard';
import React from 'react';
import { connect } from 'react-redux';

type StateProps = {
    buttonsVisibility: AccountCardButtonsVisibilityDataModel;
    accountId: string;
};

type DispatchProps = {
    dispatchReceiveCurrentSessionSettingsThunk: () => void;
};

type AllProps = StateProps & DispatchProps;

const { getCurrentSessionSettings } = REDUX_API.GLOBAL_REDUX;

class CommandsClass extends React.Component<AllProps> {
    public constructor(props: AllProps) {
        super(props);
    }

    public shouldComponentUpdate(nextProps: AllProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    public render() {
        const { isGotoToAccountButtonVisible } = this.props.buttonsVisibility;

        return (
            <>
                { isGotoToAccountButtonVisible &&
                    <GotoToAccountButton accountId={ this.props.accountId } refreshSessionSettings={ this.props.dispatchReceiveCurrentSessionSettingsThunk } />
                }
            </>
        );
    }
}

export const CommandsView = connect<StateProps, DispatchProps, {}, AppReduxStoreState>(
    state => {
        const accountCardState = state.AccountCardState;
        const { buttonsVisibility } = accountCardState;
        const accountId = accountCardState.accountInfo.id;
        return {
            buttonsVisibility,
            accountId
        };
    },
    dispatch => ({
        dispatchReceiveCurrentSessionSettingsThunk: () => getCurrentSessionSettings(dispatch)
    })
)(CommandsClass);