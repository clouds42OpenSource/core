import '@testing-library/jest-dom/extend-expect';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { DeleteDatabaseDialogMessage } from 'app/views/modules/_common/reusable-modules/DatabaseCard/views/DeleteDatabaseDialogMessage';

describe('<DeleteDatabaseDialogMessage />', () => {
    const onNoClickMock: jest.Mock = jest.fn();
    const onYesClickMock: jest.Mock = jest.fn();

    beforeEach(() => {
        render(<DeleteDatabaseDialogMessage
            isOpen={ true }
            databaseName="test_database_name"
            onNoClick={ onNoClickMock }
            onYesClick={ onYesClickMock }
        />);
    });

    it('renders', () => {
        expect(screen.getByRole(/dialog/i)).toBeInTheDocument();
        expect(screen.getByText(/Подтвердите удаление/i)).toBeInTheDocument();
    });

    it('has onYesClick method', async () => {
        expect(screen.getByText('Да')).toBeInTheDocument();
        await userEvent.click(screen.getAllByTestId(/button-view/i)[1]);
        expect(onYesClickMock).toBeCalled();
    });

    it('has onNoClickMock method', async () => {
        expect(screen.getByText('Нет')).toBeInTheDocument();
        await userEvent.click(screen.getAllByTestId(/button-view/i)[0]);
        expect(onNoClickMock).toBeCalled();
    });
});