import { Box, Link } from '@mui/material';
import { useAppDispatch, useAppSelector } from 'app/hooks';
import { archivalCopiesFilter } from 'app/modules/informationBases/reducers/archivalCopiesFilter';
import { DoubleDateInputForm } from 'app/views/components/controls/forms/DoubleDateInputView';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { TextOut } from 'app/views/components/TextOut';
import React from 'react';

const { changeFilter } = archivalCopiesFilter.actions;

export const ArchivalCopiesFilter = () => {
    const dispatch = useAppDispatch();
    const { archivalCopiesFilter: { from, to }, informationBasesList: { informationBase } } = useAppSelector(state => state.InformationBases);

    const isDbOnDelimiters = informationBase && informationBase.database && informationBase.database.isDbOnDelimiters;

    const onChange = (field: string, value: Date) => {
        if (field === 'from' || field === 'to') {
            dispatch(changeFilter({ field, value }));
        }
    };

    return (
        <Box display="flex" gap="16px" alignItems="flex-end">
            <FormAndLabel label="Период поиска">
                <DoubleDateInputForm
                    onValueChange={ onChange }
                    periodFromInputName="from"
                    periodToInputName="to"
                    periodFromValue={ from }
                    periodToValue={ to }
                />
            </FormAndLabel>
            <TextOut>
                {
                    isDbOnDelimiters
                        ? 'Резервная копия создается ежедневно в ночное время, если в течение дня в базу внесли изменения, и хранится 14 дней.'
                        : 'Резервная копия создается перед АО/ТиИ автоматически и хранится 2 последние копии.'
                }
                Также доступно создание копии по требованию.&nbsp;
                <Link
                    href="https://42clouds.com/ru-ru/manuals/sozdanie-vygruzka-i-vosstanovlenie-kopiy-dlya-faylovykh-baz-i-baz-na-razdelitelyakh/"
                    target="_blank"
                    rel="noreferrer"
                >
                    Инструкция по созданию резервной копии
                </Link>.
            </TextOut>
        </Box>
    );
};