import { FETCH_API } from 'app/api/useFetchApi';
import { REDUX_API } from 'app/api/useReduxApi';
import { DbPublishState } from 'app/common/enums';
import { EMessageType, useAppDispatch, useAppSelector, useFloatMessages } from 'app/hooks';
import { OutlinedButton } from 'app/views/components/controls/Button';
import { InfoDbCardPermissionsEnum } from 'app/web/common/enums/InfoDbCardPermissionsEnum';
import React from 'react';

const { getInformationBaseInfo } = REDUX_API.INFORMATION_BASES_REDUX;
const { postRestartIISPollInfoDbCard } = FETCH_API.INFORMATION_BASES;

export const RestartPoolIis = () => {
    const { show } = useFloatMessages();
    const dispatch = useAppDispatch();

    const { getUserPermissionsReducer: { permissions } } = useAppSelector(state => state.Global);
    const { informationBase } = useAppSelector(state => state.InformationBases.informationBasesList);
    const { id, v82Name, publishState, isDbOnDelimiters } = informationBase?.database ?? {};

    const restartPool = async () => {
        const { data } = await postRestartIISPollInfoDbCard({ databaseId: id ?? '' });

        if (data) {
            void getInformationBaseInfo(dispatch, v82Name ?? '');
        }

        show(data ? EMessageType.success : EMessageType.error, data ? 'Перезапуск пула IIS запущен' : 'Не удалось перезапстить пул IIS');
    };

    if (publishState === DbPublishState.RestartingPool) {
        return (
            <OutlinedButton kind="success" isEnabled={ false }>
                Перезапуск пула IIS...
            </OutlinedButton>
        );
    }

    if (
        !(permissions.includes(InfoDbCardPermissionsEnum.AccountDatabases_RestartIisAppPool) &&
        publishState === DbPublishState.Published &&
        !isDbOnDelimiters)
    ) {
        return null;
    }

    return (
        <OutlinedButton kind="success" onClick={ restartPool }>
            <i className="fa fa-recycle" />&nbsp;Перезапустить пул IIS
        </OutlinedButton>
    );
};