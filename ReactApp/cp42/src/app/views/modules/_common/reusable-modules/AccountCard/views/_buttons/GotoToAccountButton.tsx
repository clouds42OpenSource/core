import { AppRoutes } from 'app/AppRoutes';
import { hasComponentChangesFor } from 'app/common/functions';
import { OutlinedButton } from 'app/views/components/controls/Button';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import React from 'react';
import { withRouter } from 'react-router';
import { RouteComponentProps } from 'react-router-dom';

type OwnProps = RouteComponentProps & {
    accountId: string;
    refreshSessionSettings: () => void;
};

class GotoToAccountButtonClass extends React.Component<OwnProps> {
    constructor(props: OwnProps) {
        super(props);
        this.gotoToAccount = this.gotoToAccount.bind(this);
    }

    public shouldComponentUpdate(nextProps: OwnProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private gotoToAccount() {
        InterlayerApiProxy.getAccountApi().gotoToAccount({ accountId: this.props.accountId });
        this.props.refreshSessionSettings();
        window.location.href = AppRoutes.homeRoute;
    }

    public render() {
        return (
            <OutlinedButton kind="success" onClick={ this.gotoToAccount }>Перейти в режим аккаунта</OutlinedButton>
        );
    }
}

export const GotoToAccountButton = withRouter(GotoToAccountButtonClass);