import { hasComponentChangesFor } from 'app/common/functions';
import { AccountCardProcessId } from 'app/modules/accountCard';
import { ChangeAccountSegmentThunkParams } from 'app/modules/accountCard/store/reducers/changeAccountSegmentReducer/params';
import { ChangeAccountSegmentThunk } from 'app/modules/accountCard/store/thunks/ChangeAccountSegmentThunk';
import { AppReduxStoreState } from 'app/redux/types';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { ContainedButton } from 'app/views/components/controls/Button';
import { WatchReducerProcessActionState } from 'app/views/modules/_common/components/WatchReducerProcessActionState';
import { IReducerProcessActionInfo } from 'core/redux/interfaces';
import React from 'react';
import { connect } from 'react-redux';

type OwnProps = FloatMessageProps & {
    isEnabled?: boolean;
    accountIdFor: string;
    segmentIdChangeTo: string;
    onSuccess?: () => void;
    showSuccessMessage?: boolean;
    showErrorMessage?: boolean;
};

type OwnState = {
    updateSegmentState: {
        isInProgressState: boolean;
        isInSuccessState: boolean;
        isInErrorState: boolean;
        error?: Error;
    }
};

type DispatchProps = {
    dispatchChangeAccountSegmentThunk: (args: ChangeAccountSegmentThunkParams) => void;
};

type AllProps = OwnProps & DispatchProps;

export class ChangeSegmentButtonClass extends React.Component<AllProps, OwnState> {
    public constructor(props: AllProps) {
        super(props);

        this.onChangeSegmentClick = this.onChangeSegmentClick.bind(this);
        this.onProcessActionStateChanged = this.onProcessActionStateChanged.bind(this);

        this.state = {
            updateSegmentState: {
                isInErrorState: false,
                isInProgressState: false,
                isInSuccessState: false,
                error: undefined
            }
        };
    }

    public shouldComponentUpdate(nextProps: OwnProps, nextState: OwnState) {
        return hasComponentChangesFor(this.props, nextProps) ||
            hasComponentChangesFor(this.state, nextState);
    }

    public componentDidUpdate(_prevProps: AllProps, prevState: OwnState) {
        if (!prevState.updateSegmentState.isInSuccessState && this.state.updateSegmentState.isInSuccessState) {
            if (this.props.showSuccessMessage ?? true) {
                this.props.floatMessage.show(MessageType.Default, 'Добавлена задача на смену сегмента');
            }

            if (this.props.onSuccess) this.props.onSuccess();
        } else if (!prevState.updateSegmentState.isInErrorState && this.state.updateSegmentState.isInErrorState) {
            if (this.props.showErrorMessage ?? true) {
                this.props.floatMessage.show(MessageType.Error, this.state.updateSegmentState.error?.message);
            }
        }
    }

    private onProcessActionStateChanged(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId === AccountCardProcessId.ChangeAccountSegment) {
            this.setState({
                updateSegmentState: {
                    isInProgressState: actionState.isInProgressState,
                    isInSuccessState: actionState.isInSuccessState,
                    isInErrorState: actionState.isInErrorState,
                    error
                }
            });
        }
    }

    private onChangeSegmentClick() {
        this.props.dispatchChangeAccountSegmentThunk({
            accountId: this.props.accountIdFor,
            segmentId: this.props.segmentIdChangeTo,
            force: true
        });
    }

    public render() {
        return (
            <>
                <ContainedButton
                    isEnabled={ this.props.isEnabled && !this.state.updateSegmentState.isInProgressState }
                    showProgress={ this.state.updateSegmentState.isInProgressState }
                    kind="primary"
                    onClick={ this.onChangeSegmentClick }
                >
                    <span>Сменить сегмент</span>
                </ContainedButton>
                <WatchReducerProcessActionState
                    watch={ [{
                        reducerStateName: 'AccountCardState',
                        processIds: [AccountCardProcessId.ChangeAccountSegment]
                    }] }
                    onProcessActionStateChanged={ this.onProcessActionStateChanged }
                />
            </>
        );
    }
}

const ChangeSegmentButtonConnected = connect<NonNullable<unknown>, DispatchProps, OwnProps, AppReduxStoreState>(
    null,
    {
        dispatchChangeAccountSegmentThunk: ChangeAccountSegmentThunk.invoke,
    }
)(ChangeSegmentButtonClass);

export const ChangeSegmentButton = withFloatMessages(ChangeSegmentButtonConnected);