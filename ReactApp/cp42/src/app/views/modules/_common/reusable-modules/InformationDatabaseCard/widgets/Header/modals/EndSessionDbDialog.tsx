import { Dialog } from 'app/views/components/controls/Dialog';
import { TextOut } from 'app/views/components/TextOut';
import cn from 'classnames';

import css from './styles.module.css';

type TProps = {
    isOpen: boolean;
    databaseName: string;
    onNoClick: () => void;
    onYesClick: () => void;
};

export const EndSessionDbDialog = ({ isOpen, databaseName, onNoClick, onYesClick }: TProps) => {
    return (
        <Dialog
            isOpen={ isOpen }
            dialogWidth="xs"
            title="Завершение сеансов"
            isTitleSmall={ true }
            onCancelClick={ onNoClick }
            dialogVerticalAlign="center"
            buttons={ [
                {
                    kind: 'default',
                    onClick: onNoClick,
                    content: 'Нет',
                },
                {
                    kind: 'error',
                    onClick: onYesClick,
                    content: 'Да'
                }
            ] }
        >
            <div className={ cn(css['warning-icon-container'], css.pulseWarning) }>
                <span className={ cn(css['warning-icon-circle'], css.pulseWarningIns) } />
                <span className={ cn(css['warning-icon-dot'], css.pulseWarningIns) } />
            </div>
            <TextOut>
                Все активные сеансы в базе &quot;{ databaseName }&quot; будут завершены. Завершить все сеансы?
            </TextOut>
        </Dialog>
    );
};