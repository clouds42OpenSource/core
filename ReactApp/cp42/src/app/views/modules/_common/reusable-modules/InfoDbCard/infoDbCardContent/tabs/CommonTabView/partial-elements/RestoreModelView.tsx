import { AccountDatabaseRestoreModelType } from 'app/common/enums';
import { ComboBoxForm } from 'app/views/components/controls/forms/ComboBox/ComboBoxForm';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { InfoDbCardEditDatabaseDataForm } from 'app/views/modules/AccountManagement/InformationBases/types/InfiDbCardEditDatabaseDataForm';
import { ChangeRestoreTypeDialogMessage } from 'app/views/modules/AccountManagement/InformationBases/views/Modal/ChangeRestoreTypeDialogMessage';
import { ReadOnlyKeyValueView } from 'app/views/modules/_common/components/ReadOnlyKeyValueView';
import { KeyValueDataModel } from 'app/web/common/data-models';
import { useState } from 'react';

type OwnProps = {
    label: string;
    formName: string;
    readOnlyRestoreName: string;
    editRestoreId: AccountDatabaseRestoreModelType;
    restoreModels: Array<KeyValueDataModel<number, string>>;
    isInEditMode: boolean;
    onValueChange: <TValue>(fieldName: string, newValue?: TValue, prevValue?: TValue) => boolean | void;
};

type FormType = ReduxFormProps<InfoDbCardEditDatabaseDataForm>;

export const RestoreModelView = (props: OwnProps & FormType) => {
    const [open, setOpen] = useState(false);

    function openDialogMessage<TValue>(fieldName: string, newValue?: TValue) {
        setOpen(!open);
        if (Number(newValue) !== AccountDatabaseRestoreModelType.Mixed) {
            props.reduxForm.updateReduxFormFields({
                [fieldName]: newValue
            });
        }
    }

    function onCloseDialog() {
        setOpen(false);
    }

    async function changeTypeInfoDb() {
        props.onValueChange('restoreModel', props.reduxForm.getReduxFormFields(false).restoreModel);
        onCloseDialog();
    }

    return props.isInEditMode
        ? (
            <>
                <ComboBoxForm
                    formName={ props.formName }
                    label={ props.label }
                    value={ props.editRestoreId }
                    items={ props.restoreModels.map(item => {
                        return {
                            value: +item.key,
                            text: item.value
                        };
                    }) }
                    onValueChange={ openDialogMessage }
                />
                <ChangeRestoreTypeDialogMessage isOpen={ open } onYesClick={ () => changeTypeInfoDb() } onNoClick={ onCloseDialog } />
            </>
        )
        : (
            <ReadOnlyKeyValueView
                label={ props.label }
                text={ props.readOnlyRestoreName }
            />
        );
};

export const RestoreModel = withReduxForm(RestoreModelView, {
    reduxFormName: 'InfoDbCard_EditRestoreModel'
});