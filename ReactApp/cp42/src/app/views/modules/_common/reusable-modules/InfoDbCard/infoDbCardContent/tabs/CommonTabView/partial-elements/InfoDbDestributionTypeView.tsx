import { DistributionType, PlatformType } from 'app/common/enums';
import { ComboBoxForm } from 'app/views/components/controls/forms/ComboBox/ComboBoxForm';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { InfoDbCardEditDatabaseDataForm } from 'app/views/modules/AccountManagement/InformationBases/types/InfiDbCardEditDatabaseDataForm';
import { ChangeDestributionPlatformDialogMessage } from 'app/views/modules/AccountManagement/InformationBases/views/Modal/ChangeDestributionPlatformDialogMessage';
import { ReadOnlyKeyValueView } from 'app/views/modules/_common/components/ReadOnlyKeyValueView';
import { KeyValueDataModel } from 'app/web/common/data-models';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { InfoDbCardAccountDatabaseInfoDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/InfoDbCardAccountDatabaseInfoDataModel';
import { RequestKind } from 'core/requestSender/enums';
import { useState } from 'react';

type OwnProps = {
    label: string;
    formName: string;
    readOnlyDistributionType: DistributionType;
    editDistributionType: DistributionType;
    platformType: PlatformType;
    alpha83Version: string;
    stable82Version: string;
    stable83Version: string;
    distributionTypes: Array<KeyValueDataModel<DistributionType, string>>;
    isInEditMode: boolean;
    commonDetails: InfoDbCardAccountDatabaseInfoDataModel;
    onChangeField: () => void;
    isOther: boolean;
};

type FormType = ReduxFormProps<InfoDbCardEditDatabaseDataForm>;

export const InfoDbDestributionTypeView = (props: OwnProps & FormType) => {
    const editDistributionType = props.editDistributionType ?? props.readOnlyDistributionType;
    const [open, setOpen] = useState(false);

    function openDialogMessage<TValue>(fieldName: string, newValue?: TValue) {
        setOpen(!open);
        props.reduxForm.updateReduxFormFields({
            [fieldName]: newValue
        });
    }

    function onCloseDialog() {
        setOpen(false);
    }

    async function changeTypeInfoDb() {
        const infoDbCardApi1 = InterlayerApiProxy.getInfoDbCardApi();
        await infoDbCardApi1.editPlatformInfoDbCard(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, {
            databaseId: props.commonDetails.id,
            platformType: props.platformType,
            distributionType: props.reduxForm.getReduxFormFields(false).distributionType
        });
        props.onChangeField();
        onCloseDialog();
    }

    return props.isInEditMode && props.distributionTypes.length
        ? (
            <>
                <ComboBoxForm
                    formName={ props.formName }
                    label={ props.label }
                    value={ editDistributionType }
                    items={ props.distributionTypes.map(item => {
                        return {
                            value: item.key,
                            text: item.value
                        };
                    }) }
                    onValueChange={ openDialogMessage }
                    isReadOnly={ props.isOther }
                />
                <ChangeDestributionPlatformDialogMessage caption={ props.commonDetails.caption } isOpen={ open } onYesClick={ changeTypeInfoDb } onNoClick={ onCloseDialog } />
            </>
        )
        : (
            <ReadOnlyKeyValueView
                label={ props.label }
                text={
                    <div>
                        {
                            props.platformType === PlatformType.V83
                                ? props.readOnlyDistributionType === DistributionType.Alpha
                                    ? props.alpha83Version
                                    : props.stable83Version
                                : props.platformType === PlatformType.V82
                                    ? props.stable82Version
                                    : ''
                        }
                    </div>
                }
            />
        );
};

export const InfoDbDestributionType = withReduxForm(InfoDbDestributionTypeView, {
    reduxFormName: 'InfoDbCard_EditName_Db'
});