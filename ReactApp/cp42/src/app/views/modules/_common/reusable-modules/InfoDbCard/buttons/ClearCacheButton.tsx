import React from 'react';
import { OutlinedButton } from 'app/views/components/controls/Button';
import { ClearCacheDto } from 'app/web/api/InfoDbCardProxy/request-dto/ClearCacheDto';
import { Dialog } from 'app/views/components/controls/Dialog';
import { Checkbox } from '@mui/material';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { RequestKind } from 'core/requestSender/enums';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { TextOut } from 'app/views/components/TextOut';

type OwnProps = ClearCacheDto & FloatMessageProps;

type OwnState = {
    open: boolean;
    checked: boolean;
};

class ClearCacheButtonClass extends React.Component<OwnProps, OwnState> {
    constructor(props: OwnProps) {
        super(props);

        this.openDialog = this.openDialog.bind(this);
        this.closeDialog = this.closeDialog.bind(this);
        this.changeChecked = this.changeChecked.bind(this);
        this.handleConfirmDialog = this.handleConfirmDialog.bind(this);

        this.state = {
            open: false,
            checked: false
        };
    }

    private async handleConfirmDialog() {
        const infoDbCardApi = InterlayerApiProxy.getInfoDbCardApi();
        this.setState({
            open: false
        });
        this.props.floatMessage.show(MessageType.Info, 'Очистка кэша будет завершена в течение нескольких минут');
        try {
            await infoDbCardApi.clearCache(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, {
                AccountDatabaseId: this.props.AccountDatabaseId,
                ClearDatabaseLogs: this.state.checked
            }).then(data => {
                data.Success
                    ? this.props.floatMessage.show(MessageType.Success, 'Очистка кэша завершена')
                    : this.props.floatMessage.show(MessageType.Warning, data.Message);
            });
            this.setState({
                checked: false
            });
        } catch (e) {
            console.log(e);
        }
    }

    private changeChecked() {
        this.setState({
            checked: !this.state.checked
        });
    }

    private closeDialog() {
        this.setState({
            open: false
        });
    }

    private openDialog() {
        this.setState({
            open: true
        });
    }

    public render() {
        return (
            <>
                <OutlinedButton kind="success" onClick={ this.openDialog }>
                    Очистить кэш
                </OutlinedButton>
                <Dialog
                    isOpen={ this.state.open }
                    onCancelClick={ this.closeDialog }
                    title="Очистить кэш"
                    titleTextAlign="left"
                    isTitleSmall={ true }
                    dialogVerticalAlign="center"
                    dialogWidth="sm"
                    buttons={
                        [
                            {
                                content: 'Отмена',
                                kind: 'default',
                                variant: 'outlined',
                                onClick: this.closeDialog
                            },
                            {
                                content: 'Очистить',
                                kind: 'success',
                                variant: 'contained',
                                onClick: this.handleConfirmDialog
                            }
                        ]
                    }
                >
                    <TextOut>Действие приведет к очистке кэша вашей информационной базы, вы уверены?</TextOut><br />
                    <TextOut fontWeight={ 700 } style={ { color: 'red' } }>Перед очисткой кэша убедитесь, что база у пользователей закрыта или все сеансы завершатся автоматически.</TextOut>
                    <br />
                    <TextOut>
                        <Checkbox
                            checked={ this.state.checked }
                            onChange={ this.changeChecked }
                            inputProps={ { 'aria-label': 'Хотите ли вы очистить журнал регистрации.\n' } }
                        />
                        Хотите ли вы очистить журнал регистрации?
                    </TextOut>
                </Dialog>
            </>
        );
    }
}

export const ClearCacheButton = withFloatMessages(ClearCacheButtonClass);