import { Button } from '@mui/material';
import FileCopyOutlinedIcon from '@mui/icons-material/FileCopyOutlined';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import React from 'react';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';

type OwnProps = FloatMessageProps & {
    text: string;
};

class CopyTextToClipBoardButtonClass extends React.Component<OwnProps> {
    constructor(props: OwnProps) {
        super(props);
        this.copyTextToClipBoard = this.copyTextToClipBoard.bind(this);
    }

    private async copyTextToClipBoard(text: string) {
        this.props.floatMessage.show(MessageType.Default, 'Путь скопирован в буфер обмена', 700);

        if ('clipboard' in navigator) {
            return navigator.clipboard.writeText(text);
        }

        return document.execCommand('copy', true, text);
    }

    public render() {
        return (
            <Button
                onClick={ () => this.copyTextToClipBoard(this.props.text) }
                style={ { outline: 'none', padding: '0' } }
                data-testid="copy-text-to-clip-board-button"
            >
                <span title="Скопировать адрес">
                    <FileCopyOutlinedIcon fontSize="inherit" />
                </span>
            </Button>
        );
    }
}

export const CopyTextToClipBoardButton = withFloatMessages(CopyTextToClipBoardButtonClass);