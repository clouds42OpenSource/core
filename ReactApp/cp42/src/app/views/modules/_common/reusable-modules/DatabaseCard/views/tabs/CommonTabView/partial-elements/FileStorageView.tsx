import { ComboBoxForm } from 'app/views/components/controls/forms/ComboBox/ComboBoxForm';
import { ReadOnlyKeyValueView } from 'app/views/modules/_common/components/ReadOnlyKeyValueView';
import { KeyValueDataModel } from 'app/web/common/data-models';
import React from 'react';

type OwnProps = {
    label: string;
    formName: string;
    readOnlyFileStorageName: string;
    readOnlyFileStorageId: string;
    editFileStorageId: string;
    fileStorages: Array<KeyValueDataModel<string, string>>;
    isInEditMode: boolean;
    onValueChange: <TValue>(fieldName: string, newValue?: TValue, prevValue?: TValue) => boolean | void;
};
export const FileStorageView = (props: OwnProps) => {
    const editFileStorageId = props.editFileStorageId ?? props.readOnlyFileStorageId;
    return props.isInEditMode && props.fileStorages.length
        ? (
            <ComboBoxForm
                formName={ props.formName }
                label={ props.label }
                value={ editFileStorageId }
                items={ props.fileStorages.map(item => {
                    return {
                        value: item.key,
                        text: item.value
                    };
                }) }
                onValueChange={ props.onValueChange }
            />
        )
        : (
            <ReadOnlyKeyValueView
                label={ props.label }
                text={ props.readOnlyFileStorageName }
            />
        );
};