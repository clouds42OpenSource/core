import { AccountUserGroup } from 'app/common/enums';
import { hasComponentChangesFor } from 'app/common/functions';
import { ReceiveAccessThunkParams } from 'app/modules/infoDbCard/store/reducers/receiveAccessInfoDbCardReducer/params';
import { ReceiveAccessThunk } from 'app/modules/infoDbCard/store/thunks/ReceiveAccessCardThunk';
import { AppReduxStoreState } from 'app/redux/types';
import { IReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { TabsControl } from 'app/views/components/controls/TabsControl';
import { accessIsOtherTab, accessSettingsTab, archivalCopiesTab, servicesTab } from 'app/views/modules/AccountManagement/InformationBases/Permissions/getPermissionsTabs';
import { InfoDbCardEditDatabaseDataForm } from 'app/views/modules/AccountManagement/InformationBases/types/InfiDbCardEditDatabaseDataForm';
import { HeaderInfoDb } from 'app/views/modules/_common/reusable-modules/InfoDbCard/infoDbCardContent/HeaderInfoDb';
import { NameInfoDb } from 'app/views/modules/_common/reusable-modules/InfoDbCard/infoDbCardContent/NameInfoDb/NameInfoDb';
import { AccessIsOtherTabView } from 'app/views/modules/_common/reusable-modules/InfoDbCard/infoDbCardContent/tabs/AccessIsOtherTabView';
import { AccessView } from 'app/views/modules/_common/reusable-modules/InfoDbCard/infoDbCardContent/tabs/AccessTabView';
import { ArchivalCopiesView } from 'app/views/modules/_common/reusable-modules/InfoDbCard/infoDbCardContent/tabs/ArchivalСopiesTabView';
import { CommonTabView } from 'app/views/modules/_common/reusable-modules/InfoDbCard/infoDbCardContent/tabs/CommonTabView';
import { HasSupportView } from 'app/views/modules/_common/reusable-modules/InfoDbCard/infoDbCardContent/tabs/HasSupportTabView';
import { ServicesTabView } from 'app/views/modules/_common/reusable-modules/InfoDbCard/infoDbCardContent/tabs/ServicesTavView';
import { GetAvailabilityInfoDb } from 'app/web/api/InfoDbProxy/responce-dto/GetAvailabilityInfoDb';
import { InfoDbListItemDataModel } from 'app/web/InterlayerApiProxy/InfoDbApiProxy/receiveDatabaseList/data-models';
import { EternalUserDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/EternalUserDataModel';
import { HasSupportInfoDbDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/HasSupportInfoDbDataModel';
import { InfoDbCardAccountDatabaseInfoDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/InfoDbCardAccountDatabaseInfoDataModel';
import { RightsInfoDbCardDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getRightsInfoDbCard/data-models/InfoDbCardAccountDatabaseInfoDataModel';
import React from 'react';
import { connect } from 'react-redux';

type OwnProps = {
    onDataSelect?: () => void;
    onDataSelectOther?: () => void;
    databaseDataTabRef: React.Ref<IReduxForm<InfoDbCardEditDatabaseDataForm>>;
    item?: InfoDbListItemDataModel | null;
    permissions: RightsInfoDbCardDataModel;
    onChangeField: () => void;
    activeTab: number;
    addUsers: (result: EternalUserDataModel) => void;
    users: EternalUserDataModel[];
    onValueChangeCheckBox: (value: string) => void;
    valueCheck: string[];
    onValueChangeAll: () => void;
    allCheck: boolean;
    onResetAddUser: () => void;
    onCancelDialogButtonClick: () => void;
    isOpen: boolean;
    successAccess: boolean;
    accountGroup: boolean;
    isOther: boolean;
    accessPermissions: boolean;
    onChangeTab: (tab: number) => void;
    isAdmin?: boolean;
    showAccessOrPayDialog: () => void;
};

type StateProps = {
    commonDetails: InfoDbCardAccountDatabaseInfoDataModel;
    hasDatabaseCardReceived: boolean;
    hasSupportInfoDb: HasSupportInfoDbDataModel;
    availability: GetAvailabilityInfoDb;
    currentUserGroups: AccountUserGroup[];
};

type DispatchProps = {
    dispatchAccessCardThunk: (args: ReceiveAccessThunkParams) => void;
};

type AllProps = OwnProps & StateProps & DispatchProps;

class InfoDbCardContentView extends React.Component<AllProps> {
    public constructor(props: AllProps) {
        super(props);
        this.TabRenderVisible = this.TabRenderVisible.bind(this);
    }

    public shouldComponentUpdate(nextProps: AllProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private TabRenderVisible() {
        return [
            {
                title: 'Общие',
                isVisible: true,
            },
            {
                title: 'Настройки доступа',
                isVisible: this.props.isOther
            },
            {
                title: 'Настройки доступа',
                isVisible: !this.props.isOther
            },
            {
                title: 'Сервисы',
                isVisible: !this.props.isOther
            },
            {
                title: 'Архивные копии',
                isVisible: !this.props.isOther
            },
            {
                title: 'Настройки АО и ТИИ',
                isVisible: !this.props.isOther,
            },
        ].filter((i, index) => {
            if (!accessIsOtherTab(this.props.currentUserGroups, this.props.isOther) && index === 1) {
                return false;
            }

            if (!accessSettingsTab(this.props.permissions, this.props.commonDetails, this.props.accessPermissions, (this.props.item?.state ?? this.props.commonDetails.databaseState) !== 15) && index === 2) {
                return false;
            }

            if (!servicesTab(this.props.permissions, this.props.commonDetails, this.props.currentUserGroups.includes(AccountUserGroup.AccountSaleManager)) && index === 3) {
                return false;
            }

            if (!archivalCopiesTab(this.props.permissions) && index === 4) {
                return false;
            }

            return !(!(!this.props.commonDetails.isDbOnDelimiters && (this.props.item?.state ?? this.props.commonDetails.databaseState) !== 15 && (this.props.item?.state ?? this.props.commonDetails.databaseState) !== 16) && index === 5);
        });
    }

    public render() {
        return (
            <>
                <HeaderInfoDb
                    permissions={ this.props.permissions }
                    commonDetails={ this.props.commonDetails }
                    item={ this.props.item }
                    onChangeField={ this.props.onChangeField }
                    onCancelDialogButtonClick={ this.props.onCancelDialogButtonClick }
                    onDataSelect={ this.props.onDataSelect }
                    onDataSelectOther={ this.props.onDataSelectOther }
                    availability={ this.props.availability }
                    isOther={ !this.props.isOther }
                    goToAuTab={ () => this.props.onChangeTab(this.TabRenderVisible().length - 1) }
                />
                <NameInfoDb
                    onChangeField={ this.props.onChangeField }
                    isOther={ !this.props.isOther && (this.props.item?.state ?? this.props.commonDetails.databaseState) !== 15 }
                />
                { this.props.hasDatabaseCardReceived &&
                    <TabsControl
                        headers={ this.TabRenderVisible() }
                        activeTabIndex={ this.props.activeTab }
                        onActiveTabIndexChanged={ this.props.onChangeTab }
                    >
                        <CommonTabView
                            item={ this.props.item }
                            permissions={ this.props.permissions }
                            ref={ this.props.databaseDataTabRef }
                            onChangeField={ this.props.onChangeField }
                            isOther={ this.props.isOther }
                            isAdmin={ this.props.isAdmin }
                        />
                        { accessIsOtherTab(this.props.currentUserGroups, this.props.isOther) &&
                            <AccessIsOtherTabView
                                id={ this.props.commonDetails.id }
                                onCloseHandler={ this.props.onCancelDialogButtonClick }
                                refreshIsOtherDbList={ this.props.onDataSelectOther }
                            />
                        }
                        { accessSettingsTab(this.props.permissions, this.props.commonDetails, this.props.accessPermissions, (this.props.item?.state ?? this.props.commonDetails.databaseState) !== 15) &&
                            <AccessView
                                dispatchAccessData={ this.props.dispatchAccessCardThunk }
                                isOnDelimiters={ this.props.commonDetails.isDbOnDelimiters }
                                successAccess={ this.props.successAccess }
                                allCheck={ this.props.allCheck }
                                onValueChangeAll={ this.props.onValueChangeAll }
                                users={ this.props.users }
                                addUsers={ this.props.addUsers }
                                onValueChangeCheckBox={ this.props.onValueChangeCheckBox }
                                valueCheck={ this.props.valueCheck }
                                onResetAddUser={ this.props.onResetAddUser }
                                isOpen={ this.props.isOpen }
                                accessPermissions={ this.props.accessPermissions }
                                isAdmin={ this.props.isAdmin }
                                showAccessOrPayDialog={ this.props.showAccessOrPayDialog }
                            />
                        }
                        { servicesTab(this.props.permissions, this.props.commonDetails, this.props.currentUserGroups.includes(AccountUserGroup.AccountSaleManager)) &&
                            <ServicesTabView isOpen={ this.props.isOpen } />
                        }
                        { archivalCopiesTab(this.props.permissions) &&
                            <ArchivalCopiesView
                                accountGroup={ this.props.accountGroup && !this.props.commonDetails.isDbOnDelimiters }
                                state={ this.props.item?.state ?? this.props.commonDetails.databaseState }
                                status={ (this.props.item?.state ?? this.props.commonDetails.databaseState) !== 15 }
                                applicationId={ this.props.commonDetails.id }
                            />
                        }
                        {
                            !this.props.commonDetails.isDbOnDelimiters &&
                            (this.props.item?.state ?? this.props.commonDetails.databaseState) !== 15 &&
                            (this.props.item?.state ?? this.props.commonDetails.databaseState) !== 16 && (
                                <HasSupportView
                                    hasSupportInfoDb={ this.props.hasSupportInfoDb }
                                />
                            )
                        }
                    </TabsControl>
                }
            </>
        );
    }
}

export const InfoDbCardContent = connect<StateProps, DispatchProps, OwnProps, AppReduxStoreState>(
    state => {
        const infoDbListState = state.InfoDbListState;
        const databaseCardState = state.InfoDbCardState;
        const { commonDetails } = databaseCardState;
        const { hasDatabaseCardReceived } = databaseCardState.hasSuccessFor;
        const { hasDatabaseCardRightsReceived } = databaseCardState.hasSuccessFor;
        const { hasSupportInfoDb } = databaseCardState;
        const { hasHasSupportReceived } = databaseCardState.hasSuccessFor;
        const availability = infoDbListState.AvailabilityInfoDb;
        const hasSessionSettingsReceived = state.Global.getCurrentSessionSettingsReducer.hasSuccessFor.hasSettingsReceived;
        const currentUserGroups = hasSessionSettingsReceived ? state.Global.getCurrentSessionSettingsReducer.settings.currentContextInfo.currentUserGroups : [];

        return {
            hasSupportInfoDb,
            hasHasSupportReceived,
            commonDetails,
            hasDatabaseCardReceived,
            hasDatabaseCardRightsReceived,
            availability,
            currentUserGroups
        };
    },
    {
        dispatchAccessCardThunk: ReceiveAccessThunk.invoke
    }
)(InfoDbCardContentView);