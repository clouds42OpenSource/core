import { AccountUserGroup } from 'app/common/enums';
import { hasComponentChangesFor } from 'app/common/functions';
import { ReceiveAccountCardThunkParams } from 'app/modules/accountCard/store/reducers/receiveAccountCardReducer/params';
import { ReceiveAccountCardThunk } from 'app/modules/accountCard/store/thunks';
import { GetSuppliersWithPaginationThunkParams } from 'app/modules/suppliers/store/reducers/getSuppliersWithPaginationReducer/params';
import { GetSuppliersWithPaginationThunk } from 'app/modules/suppliers/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';

import { AccountCardEditAccountDataForm } from 'app/views/modules/_common/reusable-modules/AccountCard/types';
import {
    AccountCardAccountInfoDataModel,
    AccountCardCommonDataDataModel,
    AccountCardEditableFieldsDataModel,
    AccountCardLocaleInfoItemDataModel,
    AccountCardVisibleFieldsDataModel
} from 'app/web/InterlayerApiProxy/AccountCardApiProxy/receiveAccountCard';
import { SuppliersPaginationDataModel } from 'app/web/InterlayerApiProxy/SuppliersApiProxy/getSuppliersWithPagination';
import React from 'react';
import { connect } from 'react-redux';
import { View } from './View';

type OwnProps = ReduxFormProps<AccountCardEditAccountDataForm> & {
    isReadonly?: boolean;
};

type StateProps = {
    isCloudAdmin: boolean;
    isCloudEngineer: boolean;
    accountInfo: AccountCardAccountInfoDataModel;
    commonInfo: AccountCardCommonDataDataModel;
    localeArr: AccountCardLocaleInfoItemDataModel[];
    editableFields: AccountCardEditableFieldsDataModel;
    visibleFields: AccountCardVisibleFieldsDataModel;
    hasAccountCardReceived: boolean;
    suppliers: SuppliersPaginationDataModel;
};

type DispatchProps = {
    dispatchReceiveAccountCardThunk: (args?: ReceiveAccountCardThunkParams) => void;
    dispatchGetSuppliersWithPaginationThunk: (args: GetSuppliersWithPaginationThunkParams) => void;
}

type AllProps = OwnProps & StateProps & DispatchProps;

class CommonTabViewClass extends React.Component<AllProps> {
    public constructor(props: AllProps) {
        super(props);
        this.updateReduxFormField = this.updateReduxFormField.bind(this);
        this.initReduxForm = this.initReduxForm.bind(this);
    }

    public componentDidMount() {
        this.props.reduxForm.setInitializeFormDataAction(this.initReduxForm);

        if (this.props.isCloudAdmin) {
            this.props.dispatchGetSuppliersWithPaginationThunk({ page: 1, pageSize: 50 });
        }
    }

    public shouldComponentUpdate(nextProps: OwnProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private initReduxForm(): AccountCardEditAccountDataForm | undefined {
        if (!this.props.hasAccountCardReceived) {
            return;
        }

        return {
            accountId: this.props.accountInfo.id,
            accountCaption: this.props.accountInfo.accountCaption,
            description: this.props.accountInfo.description,
            inn: this.props.accountInfo.inn,
            localeId: this.props.accountInfo.localeId,
            isVip: this.props.accountInfo.isVip,
            emails: this.props.commonInfo.emails,
            deployment: this.props.accountInfo.deployment
        };
    }

    private updateReduxFormField<TValue>(fieldName: string, newValue: TValue) {
        this.props.reduxForm.updateReduxFormFields({ [fieldName]: newValue });
    }

    public render() {
        if (this.props.hasAccountCardReceived) {
            return (
                <View
                    dispatchReceiveAccountCardThunk={ this.props.dispatchReceiveAccountCardThunk }
                    suppliers={ this.props.suppliers }
                    isCloudAdmin={ this.props.isCloudAdmin }
                    isCloudEngineer={ this.props.isCloudEngineer }
                    isReadonly={ this.props.isReadonly }
                    onFieldChange={ this.updateReduxFormField }
                    editableFields={ this.props.editableFields }
                    visibleFields={ this.props.visibleFields }
                    accountInfo={ this.props.accountInfo }
                    commonInfo={ this.props.commonInfo }
                    localeArr={ this.props.localeArr }
                    accountDataForm={ this.props.reduxForm.getReduxFormFields(true) }
                />
            );
        }

        return null;
    }
}

const CommonTabViewConnected = connect<StateProps, DispatchProps, OwnProps, AppReduxStoreState>(
    state => {
        const accountCardState = state.AccountCardState;
        const commonInfo = accountCardState.commonData;
        const { accountInfo } = accountCardState;
        const localeArr = accountCardState.locales;
        const { editableFields } = accountCardState;
        const { visibleFields } = accountCardState;
        const { hasAccountCardReceived } = accountCardState.hasSuccessFor;

        const { currentUserGroups } = state.Global.getCurrentSessionSettingsReducer.settings.currentContextInfo;

        return {
            commonInfo,
            accountInfo,
            localeArr,
            editableFields,
            visibleFields,
            hasAccountCardReceived,
            isCloudAdmin: currentUserGroups.includes(AccountUserGroup.CloudAdmin),
            isCloudEngineer: currentUserGroups.includes(AccountUserGroup.CloudSE),
            suppliers: state.SuppliersState.suppliers
        };
    },
    {
        dispatchReceiveAccountCardThunk: ReceiveAccountCardThunk.invoke,
        dispatchGetSuppliersWithPaginationThunk: GetSuppliersWithPaginationThunk.invoke
    }
)(CommonTabViewClass);

export const CommonTabView = withReduxForm(CommonTabViewConnected, {
    reduxFormName: 'AccountCard_EditAccountData_Form'
});