import { getErrorMessage } from 'app/common/functions/getErrorMessage';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { OutlinedButton } from 'app/views/components/controls/Button';
import { ChangeTypeDbDialogMessage } from 'app/views/modules/AccountManagement/InformationBases/views/Modal/ChangeTypeDbDialogMessage';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { DatabaseAccessesDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/AccessInfoDbDataModel';
import { RequestKind } from 'core/requestSender/enums';
import React from 'react';

type OwnState = {
    open: boolean
};

type OwnProps = FloatMessageProps & {
    databaseName: string;
    databaseId: string;
    isFile: boolean;
    onChangeField: () => void;
    updateAccessCard: () => void;
};

type AllProps = OwnProps;

export class ChangeTypeButtonClass extends React.Component<AllProps, OwnState> {
    constructor(props: AllProps) {
        super(props);
        this.changeTypeDatabase = this.changeTypeDatabase.bind(this);
        this.openDialogMessage = this.openDialogMessage.bind(this);
        this.sendUserIdRemove = this.sendUserIdRemove.bind(this);

        this.state = {
            open: false,
        };
    }

    private openDialogMessage() {
        this.setState(prevState => ({
            open: !prevState.open
        }));
    }

    private sendUserIdRemove(valueCheck: string[], accesstInfoDb: DatabaseAccessesDataModel[]): string[] {
        return Object.values(accesstInfoDb).filter(i => !valueCheck.includes(i.userId) && i.hasAccess).map(value => value.userId);
    }

    private async changeTypeDatabase() {
        const infoDbCardApi = InterlayerApiProxy.getInfoDbCardApi();
        try {
            await infoDbCardApi.typeInfoDbCard(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, {
                databaseId: this.props.databaseId,
            });
            this.props.updateAccessCard();
        } catch (e) {
            this.props.floatMessage.show(MessageType.Error, getErrorMessage(e));
        }
        if (this.props.isFile) {
            this.setState({
                open: false
            });
            this.props.onChangeField();
        }
    }

    public render() {
        return (
            <>
                {
                    !this.props.isFile
                        ? (
                            <OutlinedButton kind="success" onClick={ this.openDialogMessage }>
                                <i className="fa fa-database" />&nbsp;Перевести в файловую
                            </OutlinedButton>
                        )
                        : (
                            <OutlinedButton kind="success" onClick={ this.openDialogMessage }>
                                <i className="fa fa-database" />&nbsp;Перевести в серверную
                            </OutlinedButton>
                        )
                }
                <ChangeTypeDbDialogMessage
                    isFile={ this.props.isFile }
                    isOpen={ this.state.open }
                    databaseName={ this.props.databaseName }
                    onNoClick={ this.openDialogMessage }
                    onYesClick={ this.changeTypeDatabase }
                    onChangeField={ this.props.onChangeField }
                />
            </>
        );
    }
}

export const ChangeTypeButton = withFloatMessages(ChangeTypeButtonClass);