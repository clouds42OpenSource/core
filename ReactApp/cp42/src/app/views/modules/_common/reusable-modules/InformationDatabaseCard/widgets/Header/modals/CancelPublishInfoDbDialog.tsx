import { Dialog } from 'app/views/components/controls/Dialog';
import cn from 'classnames';
import css from './styles.module.css';

type TProps = {
    isOpen: boolean;
    onNoClick: () => void;
    onYesClick: () => void;
};

export const CancelPublishInfoDbDialog = ({ isOpen, onYesClick, onNoClick }: TProps) => {
    return (
        <Dialog
            isOpen={ isOpen }
            dialogWidth="sm"
            isTitleSmall={ true }
            title="Подтверждение отмены публикации"
            onCancelClick={ onNoClick }
            buttons={ [
                {
                    kind: 'default',
                    onClick: onNoClick,
                    content: 'Нет'
                },
                {
                    kind: 'error',
                    onClick: onYesClick,
                    content: 'Да'
                }
            ] }
        >
            <div className={ cn(css['warning-icon-container'], css.pulseWarning) }>
                <span className={ cn(css['warning-icon-circle'], css.pulseWarningIns) } />
                <span className={ cn(css['warning-icon-dot'], css.pulseWarningIns) } />
            </div>
            <p className={ css['sweet-alert-body'] }>
                Уважаемый пользователь.<br />
                В информационной базе опубликованы веб сервисы.
                После отмены публикации они будут не доступны.
                Для повторной публикации веб сервисов обратитесь в техподдержку.
            </p>
        </Dialog>
    );
};