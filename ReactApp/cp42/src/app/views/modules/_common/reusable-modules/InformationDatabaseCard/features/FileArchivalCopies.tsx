import { TGetBackupsItem } from 'app/api/endpoints/accountDatabases/response';
import { FETCH_API } from 'app/api/useFetchApi';
import { AppRoutes } from 'app/AppRoutes';
import { AccountUserGroup } from 'app/common/enums';
import { EMessageType, useAppSelector, useFloatMessages } from 'app/hooks';
import { TextOut } from 'app/views/components/TextOut';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { CopyTextInput } from 'app/views/modules/_common/components/CopyTextInput';
import { PopperButton } from 'app/views/modules/_common/components/PopperButton';
import dayjs from 'dayjs';
import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import style from './style.module.css';

const { getBackups } = FETCH_API.ACCOUNT_DATABASES;

export const FileArchivalCopies = () => {
    const { show } = useFloatMessages();

    const { database } = useAppSelector(state => state.InformationBases.informationBasesList.informationBase) || {};
    const { from, to } = useAppSelector(state => state.InformationBases.archivalCopiesFilter);
    const { currentUserGroups } = useAppSelector(state => state.Global.getCurrentSessionSettingsReducer.settings.currentContextInfo);

    const [dataset, setDataset] = useState<TGetBackupsItem[]>([]);

    useEffect(() => {
        (async () => {
            const { success, data, message } = await getBackups({
                databaseId: database?.id ?? '',
                from: from ? dayjs(from).format('YYYY-MM-DD') : '',
                to: to ? dayjs(to).format('YYYY-MM-DD') : ''
            });

            if (success && data) {
                setDataset(data.databaseBackups);
            } else {
                show(EMessageType.error, message);
            }
        })();
    }, [database?.id, from, show, to]);

    return (
        <CommonTableWithFilter
            uniqueContextProviderStateId="fileDatabaseArchivalCopies"
            tableProps={ {
                dataset,
                keyFieldName: 'id',
                fieldsView: {
                    creationDate: {
                        caption: 'Дата и время',
                        format: creationDate => dayjs(creationDate).format('DD.MM.YYYY HH:mm')
                    },
                    backupPath: {
                        caption: 'Путь к архиву',
                        fieldWidth: '40%',
                        format: backupPath => currentUserGroups.find(role => role === AccountUserGroup.Hotline || role === AccountUserGroup.CloudAdmin || role === AccountUserGroup.CloudSE)
                            ? <CopyTextInput value={ backupPath } successMessage="Путь скопирован!" />
                            : <TextOut>Облачное хранилище</TextOut>
                    },
                    initiator: {
                        caption: 'Инициатор'
                    },
                    eventTriggerDescription: {
                        caption: 'Триггер',
                    },
                    id: {
                        caption: '',
                        format: (id, item) => (
                            <PopperButton>
                                <div>
                                    {
                                        database?.state && (
                                            <>
                                                <Link
                                                    to={ {
                                                        pathname: AppRoutes.accountManagement.createAccountDatabase.loadingDatabase,
                                                        state: { fileId: id, restoreType: '1' }
                                                    } }
                                                    className={ style.link }
                                                >
                                                    Восстановить в новую
                                                </Link>
                                                <Link
                                                    to={ {
                                                        pathname: AppRoutes.accountManagement.createAccountDatabase.loadingDatabase,
                                                        state: { fileId: id, restoreType: '0', state: database.state }
                                                    } }
                                                    className={ style.link }
                                                >
                                                    Восстановить с заменой
                                                </Link>
                                            </>
                                        )
                                    }
                                    {
                                        item.eventTrigger !== 0 && item.eventTrigger !== 4 && (
                                            <a target="_blank" rel="noreferrer" className={ style.link } href={ item.backupPath }>Скачать</a>
                                        )
                                    }
                                </div>
                            </PopperButton>
                        )
                    }
                }
            } }
        />
    );
};