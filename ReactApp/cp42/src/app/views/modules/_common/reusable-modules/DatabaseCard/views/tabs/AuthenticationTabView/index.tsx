/* eslint-disable react/prefer-stateless-function */
import { AppReduxStoreState } from 'app/redux/types';
import { AuthenticationNeedView } from 'app/views/modules/_common/reusable-modules/DatabaseCard/views/tabs/AuthenticationTabView/AuthenticationNeedView';
import { AuthenticationPassedView } from 'app/views/modules/_common/reusable-modules/DatabaseCard/views/tabs/AuthenticationTabView/AuthenticationPassedView';
import React from 'react';
import { connect } from 'react-redux';

type StateProps = {
    databaseId: string;
    isTehSupportConnects: boolean;
};

type AllProps = StateProps;

class AuthenticationTabViewClass extends React.Component<AllProps> {
    public render() {
        return this.props.isTehSupportConnects
            ? <AuthenticationPassedView databaseId={ this.props.databaseId } />
            : <AuthenticationNeedView databaseId={ this.props.databaseId } />;
    }
}

export const AuthenticationTabView = connect<StateProps, NonNullable<unknown>, NonNullable<unknown>, AppReduxStoreState>(
    state => {
        const databaseCardState = state.DatabaseCardState;
        const { tehSupportInfo } = databaseCardState;

        const isTehSupportConnects = tehSupportInfo?.isInConnectingState ?? false;
        const databaseId = databaseCardState.commonDetails.id;

        return {
            databaseId,
            isTehSupportConnects
        };
    }
)(AuthenticationTabViewClass);