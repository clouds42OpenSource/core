declare const styles: {
    readonly 'main-container': string;
    readonly 'combobox-container': string;
};
export = styles;