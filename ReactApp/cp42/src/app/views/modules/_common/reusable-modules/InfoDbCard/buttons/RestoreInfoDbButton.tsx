import { AppRoutes } from 'app/AppRoutes';
import { OutlinedButton } from 'app/views/components/controls/Button';
import React from 'react';
import { Link } from 'react-router-dom';

type OwnProps = {
   backupId: string;
};

export const RestoreInfoDbButton = ({ backupId }: OwnProps) => {
    return (
        <Link
            to={ {
                pathname: AppRoutes.accountManagement.createAccountDatabase.loadingDatabase,
                state: {
                    fileId: backupId,
                    restoreType: '1'
                }
            } }
        >
            <OutlinedButton kind="success">
                <i className="fa fa-reply p-r-4" /> &nbsp; Восстановить
            </OutlinedButton>
        </Link>
    );
};