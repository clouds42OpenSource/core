import { render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { AccountDatabasesForMigrationsView } from 'app/views/modules/_common/reusable-modules/AccountDatabasesForMigrations';
import { SnackbarProvider } from 'notistack';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';

describe('AccountDatabasesForMigrationsView', () => {
    const mockStore = configureStore();
    const initialState = {
        AccountDatabasesForMigrationState: {
            accountDatabasesMigrationInfo: {
                numberOfDatabasesToMigrate: 1
            }
        }
    };

    it('renders with canShow field equal false', () => {
        const { container } = render(
            <Provider store={ mockStore(initialState) }>
                <SnackbarProvider>
                    <AccountDatabasesForMigrationsView accountIndexNumber={ 1 } canShow={ false } />
                </SnackbarProvider>
            </Provider>
        );
        expect(container.firstChild).toBeNull();
    });
});