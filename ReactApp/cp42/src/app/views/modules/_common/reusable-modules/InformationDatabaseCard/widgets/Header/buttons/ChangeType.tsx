import { FETCH_API } from 'app/api/useFetchApi';
import { REDUX_API } from 'app/api/useReduxApi';
import { AccountUserGroup, DatabaseState, DbPublishState } from 'app/common/enums';
import { EMessageType, useAppDispatch, useAppSelector, useFloatMessages, useGetCurrentUserGroups } from 'app/hooks';
import { OutlinedButton } from 'app/views/components/controls/Button';
import { InfoDbCardPermissionsEnum } from 'app/web/common/enums/InfoDbCardPermissionsEnum';
import { useState } from 'react';

import { ChangeTypeDbDialog } from '../modals';

const { postChangeTypeDatabase } = FETCH_API.INFORMATION_BASES;
const { getAccess, getInformationBaseInfo } = REDUX_API.INFORMATION_BASES_REDUX;

export const ChangeType = () => {
    const { show } = useFloatMessages();
    const dispatch = useAppDispatch();
    const currentUserGroups = useGetCurrentUserGroups();

    const { getUserPermissionsReducer: { permissions } } = useAppSelector(state => state.Global);
    const { informationBase } = useAppSelector(state => state.InformationBases.informationBasesList);
    const { id, v82Name, isFile, state, publishState, isDbOnDelimiters } = informationBase?.database ?? {};

    const [isOpen, setIsOpen] = useState(false);

    const dialogStateHandler = () => {
        setIsOpen(prev => !prev);
    };

    const changeType = async () => {
        const { data, message } = await postChangeTypeDatabase({ databaseId: id ?? '' });

        if (data) {
            void getAccess(dispatch, { databaseId: id ?? '' });

            if (isFile) {
                setIsOpen(false);
                void getInformationBaseInfo(dispatch, v82Name ?? '');
            }
        } else {
            show(EMessageType.error, message);
        }
    };

    if (!(
        permissions.includes(InfoDbCardPermissionsEnum.AccountDatabases_EditDbType) &&
        state === DatabaseState.Ready &&
        publishState !== DbPublishState.PendingUnpublication &&
        publishState !== DbPublishState.PendingPublication &&
        currentUserGroups.some(role => [AccountUserGroup.CloudAdmin, AccountUserGroup.CloudSE].includes(role)) &&
        !isDbOnDelimiters
    )) {
        return null;
    }

    return (
        <>
            <OutlinedButton kind="success" onClick={ dialogStateHandler }>
                <i className="fa fa-database" />&nbsp;Перевести в { isFile ? 'серверную' : 'файловую' }
            </OutlinedButton>
            <ChangeTypeDbDialog
                isFile={ isFile }
                isOpen={ isOpen }
                databaseName={ v82Name ?? '' }
                onNoClick={ dialogStateHandler }
                onYesClick={ changeType }
            />
        </>
    );
};