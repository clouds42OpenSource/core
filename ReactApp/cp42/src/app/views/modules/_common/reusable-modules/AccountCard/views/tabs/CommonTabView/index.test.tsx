import { render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { getNewStore } from 'app/utils/StoreUtility';
import { CommonTabView } from 'app/views/modules/_common/reusable-modules/AccountCard/views/tabs/CommonTabView';
import configureStore, { MockStoreEnhanced } from 'redux-mock-store';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';

describe('<CommonTabView />', () => {
    const mockStore = configureStore([thunk]);
    let store: MockStoreEnhanced<unknown>;

    beforeEach(() => {
        store = mockStore(getNewStore().getState());
    });

    it('renders', () => {
        const { container } = render(<Provider store={ store }><CommonTabView /></Provider>);
        expect(container.firstChild).toBeNull();
    });
});