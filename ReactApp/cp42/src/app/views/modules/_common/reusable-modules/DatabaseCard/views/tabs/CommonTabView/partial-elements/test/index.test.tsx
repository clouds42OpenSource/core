import '@testing-library/jest-dom/extend-expect';
import { render, screen } from '@testing-library/react';

import { DatabaseState, PlatformType } from 'app/common/enums';
import {
    DatabaseCaptionView,
    DatabasePlatformTypeView,
    DatabaseStateView,
    DatabaseTemplateView,
    DatabaseV82NameView,
    FileStorageView
} from 'app/views/modules/_common/reusable-modules/DatabaseCard/views/tabs/CommonTabView/partial-elements';

describe('partial elements without UsedWebServices and DatabaseDestributionType components', () => {
    const getArrayOfPartialElements = (isInEditMode: boolean) => {
        const partialElements: JSX.Element[] = [];
        const label = 'test_label';
        const formName = 'test_form_name';
        const readOnly = 'test_readonly';
        const edit = 'test_edit';
        const readOnlyId = 'test_id';
        const onValueChangeMock: jest.Mock = jest.fn();

        partialElements.push(<DatabaseCaptionView
            label={ label }
            formName={ formName }
            readOnlyDatabaseCaption={ readOnly }
            editDatabaseCaption={ edit }
            isInEditMode={ isInEditMode }
            onValueChange={ onValueChangeMock }
        />);

        partialElements.push(<DatabaseStateView
            label={ label }
            formName={ formName }
            readOnlyState={ DatabaseState.Ready }
            editState={ DatabaseState.Ready }
            states={ [DatabaseState.Ready] }
            isInEditMode={ isInEditMode }
            onValueChange={ onValueChangeMock }
        />);

        partialElements.push(<DatabasePlatformTypeView
            label={ label }
            formName={ formName }
            readOnlyPlatformType={ PlatformType.V83 }
            editPlatformType={ PlatformType.V83 }
            platformTypes={ [PlatformType.V83] }
            isInEditMode={ isInEditMode }
            onValueChange={ onValueChangeMock }
        />);

        partialElements.push(<DatabaseTemplateView
            label={ label }
            formName={ formName }
            readOnlyDatabaseTemplateName={ readOnly }
            readOnlyDatabaseTemplateId={ readOnlyId }
            editDatabaseTemplateId={ edit }
            templates={ [{ key: 'key', value: 'value' }] }
            isInEditMode={ isInEditMode }
            onValueChange={ onValueChangeMock }
        />);

        partialElements.push(<DatabaseV82NameView
            label={ label }
            formName={ formName }
            readOnlyV82Name={ readOnly }
            editV82Name={ edit }
            isInEditMode={ isInEditMode }
            onValueChange={ onValueChangeMock }
        />);

        partialElements.push(<FileStorageView
            label={ label }
            formName={ formName }
            readOnlyFileStorageName={ readOnly }
            readOnlyFileStorageId={ readOnlyId }
            editFileStorageId={ edit }
            fileStorages={ [{ key: 'key', value: 'value' }] }
            isInEditMode={ isInEditMode }
            onValueChange={ onValueChangeMock }
        />);

        return partialElements;
    };

    it('renders with isInEditMode equal true', () => {
        getArrayOfPartialElements(true).forEach((element, index) => {
            render(element);
            expect(screen.getAllByTestId(/form-and-label/i)).toHaveLength(index + 1);
        });
    });

    it('renders with isInEditMode equal false', () => {
        getArrayOfPartialElements(false).forEach((element, index) => {
            render(element);
            expect(screen.getAllByTestId(/text-view__span/i)).toHaveLength(index + 1);
        });
    });
});