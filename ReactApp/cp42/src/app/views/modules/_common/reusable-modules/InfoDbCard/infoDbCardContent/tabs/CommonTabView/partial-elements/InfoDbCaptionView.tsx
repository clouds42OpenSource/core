import { TextBoxForm } from 'app/views/components/controls/forms';
import { ReadOnlyKeyValueView } from 'app/views/modules/_common/components/ReadOnlyKeyValueView';

type OwnProps = {
    label: string;
    formName: string;
    readOnlyDatabaseCaption: string;
    editDatabaseCaption: string;
    isInEditMode: boolean;
    onValueChange: <TValue>(fieldName: string, newValue: TValue, prevValue: TValue) => boolean | void;
};
export const InfoDbCaptionView = (props: OwnProps) => {
    const editDatabaseCaption = props.editDatabaseCaption ?? props.readOnlyDatabaseCaption;
    return props.isInEditMode
        ? (
            <TextBoxForm
                autoFocus={ false }
                formName={ props.formName }
                label={ props.label }
                value={ editDatabaseCaption }
                onValueChange={ props.onValueChange }
            />
        )
        : (
            <ReadOnlyKeyValueView
                label={ props.label }
                text={ props.readOnlyDatabaseCaption }
            />
        );
};