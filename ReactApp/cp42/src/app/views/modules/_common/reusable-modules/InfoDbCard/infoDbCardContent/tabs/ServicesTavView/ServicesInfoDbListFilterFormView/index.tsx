import { Box } from '@mui/material';
import React from 'react';
import { connect } from 'react-redux';

import { SuccessButton } from 'app/views/components/controls/Button';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { AccountDatabaseListFilterDataFormFieldNamesType } from 'app/views/modules/DatabaseList/types';
import { ReceiveServicesInfoDbCardThunk } from 'app/modules/infoDbCard/store/thunks/ReceiveServicesThunk';
import { AppReduxStoreState } from 'app/redux/types';
import { ButtonGroup } from 'app/views/components/controls/ButtonGroup';
import { TextBoxForm } from 'app/views/components/controls/forms/TextBoxForm';

type OwnProps = {
    onValueChange<TValue, TForm extends string = AccountDatabaseListFilterDataFormFieldNamesType>(formName: TForm, newValue: TValue): void;
    onTabChange(value: number): void;
    searchServices: string
    onSearch: () => void;
};

class ServicesInfoDbListFilterFormViewClass extends React.PureComponent<OwnProps> {
    private items = [
        { index: 0, text: 'Все сервисы' },
        { index: 1, text: 'Подключенные' },
        { index: 2, text: 'Отключенные' },
        { index: 3, text: 'Доступные' },
    ];

    public render() {
        return (
            <Box display="grid" gap="10px" gridTemplateColumns="5fr 4fr 1fr" alignItems="flex-end">
                <ButtonGroup
                    items={ this.items }
                    selectedButtonIndex={ 0 }
                    label="Быстрые отборы"
                    onClick={ this.props.onTabChange }
                />
                <FormAndLabel label="Поиск" fullWidth={ true }>
                    <TextBoxForm
                        fullWidth={ true }
                        formName="searchServices"
                        onValueChange={ this.props.onValueChange }
                        value={ this.props.searchServices }
                        placeholder="Название сервиса"
                    />
                </FormAndLabel>
                <SuccessButton onClick={ this.props.onSearch }>
                    Поиск
                </SuccessButton>
            </Box>
        );
    }
}

export const ServicesInfoDbListFilterForm = connect<NonNullable<unknown>, NonNullable<unknown>, OwnProps, AppReduxStoreState>(
    state => {
        const databaseCardState = state.InfoDbCardState;
        const accesstInfoDb = databaseCardState.databaseAccesses;
        const { hasHasSupportReceived } = databaseCardState.hasSuccessFor;
        const { commonDetails } = databaseCardState;

        return {
            accesstInfoDb,
            hasHasSupportReceived,
            commonDetails
        };
    },
    {
        dispatchServicesCardThunk: ReceiveServicesInfoDbCardThunk.invoke,
    }
)(ServicesInfoDbListFilterFormViewClass);