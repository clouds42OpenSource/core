import { yandexCounter, YandexMetrics } from 'app/common/_external-services/yandexMetrics';
import { hasComponentChangesFor } from 'app/common/functions';
import { ContainedButton } from 'app/views/components/controls/Button';
import { DeleteDatabaseDialogMessage } from 'app/views/modules/_common/reusable-modules/DatabaseCard/views/DeleteDatabaseDialogMessage';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { RequestKind } from 'core/requestSender/enums';
import React from 'react';

type OwnProps = {
    databaseId: string;
    databaseName: string;
    newDeleteRequest?: boolean
};

type OwnState = {
    isDeleteDatabaseDialogOpen: boolean;
};

export class DeleteDatabaseButton extends React.Component<OwnProps, OwnState> {
    constructor(props: OwnProps) {
        super(props);
        this.deleteDatabase = this.deleteDatabase.bind(this);
        this.onCancelDeleteDatabase = this.onCancelDeleteDatabase.bind(this);
        this.onDeleteDatabaseConfirmed = this.onDeleteDatabaseConfirmed.bind(this);

        this.state = {
            isDeleteDatabaseDialogOpen: false
        };
    }

    public shouldComponentUpdate(nextProps: OwnProps, nextState: OwnState) {
        return hasComponentChangesFor(this.props, nextProps) ||
            hasComponentChangesFor(this.state, nextState);
    }

    private async onDeleteDatabaseConfirmed() {
        const databaseCardApi = InterlayerApiProxy.getDatabaseApi();
        const infoDbCardApi = InterlayerApiProxy.getInfoDbApi();

        try {
            this.props.newDeleteRequest
                ? await infoDbCardApi.deleteInfoDbList(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, {
                    databasesId: [this.props.databaseId]
                })
                : await databaseCardApi.deleteDatabase(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, {
                    databaseId: this.props.databaseId
                });
            yandexCounter.getInstance().reachGoal(YandexMetrics.DeleteDatabaseMetric);
            window.location.reload();
        } catch {
        }
    }

    private onCancelDeleteDatabase() {
        this.setState({
            isDeleteDatabaseDialogOpen: false
        });
    }

    private async deleteDatabase() {
        this.setState({
            isDeleteDatabaseDialogOpen: true
        });
    }

    public render() {
        return (
            <>
                <ContainedButton kind="error" onClick={ this.deleteDatabase }>
                    <i className="fa fa-trash" />&nbsp;Удалить
                </ContainedButton>

                <DeleteDatabaseDialogMessage
                    isOpen={ this.state.isDeleteDatabaseDialogOpen }
                    databaseName={ this.props.databaseName }
                    onNoClick={ this.onCancelDeleteDatabase }
                    onYesClick={ this.onDeleteDatabaseConfirmed }
                />
            </>
        );
    }
}