import { yandexCounter, YandexMetrics } from 'app/common/_external-services/yandexMetrics';
import { hasComponentChangesFor } from 'app/common/functions';
import { DatabaseCardProcessId } from 'app/modules/databaseCard';
import { UnpublishDatabaseThunkParams } from 'app/modules/databaseCard/store/reducers/unpublishDatabaseReducer/params';
import { UnpublishDatabaseThunk } from 'app/modules/databaseCard/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { OutlinedButton } from 'app/views/components/controls/Button';
import { WatchReducerProcessActionState } from 'app/views/modules/_common/components/WatchReducerProcessActionState';
import { UnpublishDatabaseDialogMessage } from 'app/views/modules/_common/reusable-modules/DatabaseCard/views/UnpublishDatabaseDialogMessage';
import { IReducerProcessActionInfo } from 'core/redux/interfaces';
import React from 'react';
import { connect } from 'react-redux';

type OwnProps = FloatMessageProps & {
    databaseId: string;
    usedWebServices: boolean;
    isInCancelingPublishedDatabaseState: boolean;
    onSuccess?: () => void;
};

type DispatchProps = {
    dispatchUnpublishDatabaseThunk: (args?: UnpublishDatabaseThunkParams) => void;
};

type OwnState = {
    isCancelPublishDatabaseDialogOpen: boolean;
    unpublishDatabaseState: {
        actionState: IReducerProcessActionInfo;
        error?: Error;
    }
};

type AllProps = OwnProps & DispatchProps;

class CancelPublishDatabaseButtonClass extends React.Component<AllProps, OwnState> {
    constructor(props: AllProps) {
        super(props);
        this.cancelPublishedDatabase = this.cancelPublishedDatabase.bind(this);
        this.onCancelCancelPublishDatabase = this.onCancelCancelPublishDatabase.bind(this);
        this.onCancelPublishedDatabaseConfirmed = this.onCancelPublishedDatabaseConfirmed.bind(this);
        this.setConfirmDialogState = this.setConfirmDialogState.bind(this);
        this.onProcessActionStateChanged = this.onProcessActionStateChanged.bind(this);

        this.state = {
            isCancelPublishDatabaseDialogOpen: false,
            unpublishDatabaseState: {
                actionState: {
                    isInErrorState: false,
                    isInProgressState: false,
                    isInSuccessState: false
                },
                error: undefined
            }
        };
    }

    public shouldComponentUpdate(nextProps: AllProps, nextState: OwnState) {
        return hasComponentChangesFor(this.props, nextProps) ||
            hasComponentChangesFor(this.state, nextState);
    }

    public componentDidUpdate(_prevProps: AllProps, prevState: OwnState) {
        if (this.state.unpublishDatabaseState.actionState.isInErrorState &&
            !prevState.unpublishDatabaseState.actionState.isInErrorState) {
            this.props.floatMessage.show(MessageType.Error, this.state.unpublishDatabaseState.error?.message);
        } else
            if (this.state.unpublishDatabaseState.actionState.isInSuccessState &&
                !prevState.unpublishDatabaseState.actionState.isInSuccessState) {
                this.props.floatMessage.show(MessageType.Default, 'Снятие публикации информационной базы запущено.');
                this.setConfirmDialogState(false);
                if (this.props.onSuccess) this.props.onSuccess();
            }
    }

    private onProcessActionStateChanged(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId === DatabaseCardProcessId.UnpublishDatabase) {
            this.setState({
                unpublishDatabaseState: {
                    actionState,
                    error
                }
            });
        }
    }

    private onCancelCancelPublishDatabase() {
        this.setConfirmDialogState(false);
    }

    private async onCancelPublishedDatabaseConfirmed() {
        yandexCounter.getInstance().reachGoal(YandexMetrics.UnpublishDatabaseMetric);
        this.props.dispatchUnpublishDatabaseThunk({
            databaseId: this.props.databaseId,
            force: true
        });
    }

    private setConfirmDialogState(isOpen: boolean) {
        this.setState({
            isCancelPublishDatabaseDialogOpen: isOpen
        });
    }

    private async cancelPublishedDatabase() {
        if (this.props.usedWebServices) {
            this.setConfirmDialogState(true);
        } else {
            void this.onCancelPublishedDatabaseConfirmed();
        }
    }

    public render() {
        return (
            <>
                {
                    this.props.isInCancelingPublishedDatabaseState
                        ? (
                            <OutlinedButton kind="success" isEnabled={ false }>
                                База в процессе снятия публикации
                            </OutlinedButton>
                        )
                        : (
                            <OutlinedButton kind="success" onClick={ this.cancelPublishedDatabase }>
                                Отменить публикацию
                            </OutlinedButton>
                        )
                }
                <UnpublishDatabaseDialogMessage
                    isOpen={ this.state.isCancelPublishDatabaseDialogOpen }
                    onNoClick={ this.onCancelCancelPublishDatabase }
                    onYesClick={ this.onCancelPublishedDatabaseConfirmed }
                />
                <WatchReducerProcessActionState
                    watch={ [{
                        reducerStateName: 'DatabaseCardState',
                        processIds: [DatabaseCardProcessId.UnpublishDatabase]
                    }] }
                    onProcessActionStateChanged={ this.onProcessActionStateChanged }
                />
            </>
        );
    }
}

const CancelPublishDatabaseButtonConnected = connect<NonNullable<unknown>, DispatchProps, OwnProps, AppReduxStoreState>(
    null,
    {
        dispatchUnpublishDatabaseThunk: UnpublishDatabaseThunk.invoke
    }
)(CancelPublishDatabaseButtonClass);

export const CancelPublishDatabaseButton = withFloatMessages(CancelPublishDatabaseButtonConnected);