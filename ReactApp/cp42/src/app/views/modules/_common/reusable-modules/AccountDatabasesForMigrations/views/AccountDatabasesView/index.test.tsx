import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import { AccountDatabasesView } from 'app/views/modules/_common/reusable-modules/AccountDatabasesForMigrations/views/AccountDatabasesView';
import { SnackbarProvider } from 'notistack';
import { AccountDatabasesForMigrationReducerState } from 'app/modules/accountDatabasesForMigrations/store/reducers';
import { ReduxFormsReducerState } from 'app/redux/redux-forms/store/reducers/ReduxFormsReducerState';
import { getNewStore } from 'app/utils/StoreUtility';

type TInitialState = {
    AccountDatabasesForMigrationState: AccountDatabasesForMigrationReducerState;
    ReduxForms: ReduxFormsReducerState<string>;
};

describe('<AccountDatabasesView />', () => {
    const store = getNewStore();
    const initialState: TInitialState = {
        AccountDatabasesForMigrationState: store.getState().AccountDatabasesForMigrationState,
        ReduxForms: store.getState().ReduxForms
    };

    beforeAll(() => {
        render(
            <SnackbarProvider>
                <Provider store={ configureStore([thunk])(initialState) }>
                    <AccountDatabasesView accountIndexNumber={ 1 } />
                </Provider>
            </SnackbarProvider>
        );
    });

    it('renders', () => {
        expect(screen.getByTestId(/filter-view/i)).toBeInTheDocument();
    });
});