import { Box } from '@mui/material';
import { Dialog } from 'app/views/components/controls/Dialog';
import { TextOut } from 'app/views/components/TextOut';
import cn from 'classnames';
import React from 'react';
import css from './styles.module.css';

type TProps = {
    isOpenDialog: boolean;
    databaseName: string;
    onNoClick: () => void;
    onYesClick: () => void;
};

export const DeleteInfoDbDialog = ({ isOpenDialog, onYesClick, onNoClick, databaseName }: TProps) => {
    return (
        <Dialog
            isOpen={ isOpenDialog }
            dialogWidth="xs"
            isTitleSmall={ true }
            dialogVerticalAlign="center"
            title={ `Удаление ИБ: ${ databaseName }` }
            onCancelClick={ onNoClick }
            buttons={ [
                {
                    kind: 'default',
                    onClick: onNoClick,
                    content: 'Нет'
                },
                {
                    kind: 'error',
                    onClick: onYesClick,
                    content: 'Да'
                }
            ] }
        >
            <Box display="flex" flexDirection="column" gap="5px">
                <div className={ cn(css['warning-icon-container'], css.pulseWarning) }>
                    <span className={ cn(css['warning-icon-circle'], css.pulseWarningIns) } />
                    <span className={ cn(css['warning-icon-dot'], css.pulseWarningIns) } />
                </div>
                <TextOut>
                    База &quot;{ databaseName }&quot; будет перемещена в корзину и недоступна для работы.
                </TextOut>
                <TextOut>
                    Скачать резервную копию возможно в течение 30 дней.
                </TextOut>
            </Box>
        </Dialog>
    );
};