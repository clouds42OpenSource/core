import { Checkbox, Box } from '@mui/material';
import { FETCH_API } from 'app/api/useFetchApi';
import { AccountUserGroup, DatabaseState } from 'app/common/enums';
import { EMessageType, useAppSelector, useFloatMessages } from 'app/hooks';
import { COLORS } from 'app/utils';
import { OutlinedButton } from 'app/views/components/controls/Button';
import { Dialog } from 'app/views/components/controls/Dialog';
import { TextOut } from 'app/views/components/TextOut';
import { useState } from 'react';

import styles from './style.module.css';

const { postClearCash } = FETCH_API.INFORMATION_BASES;

export const ClearCache = () => {
    const { show } = useFloatMessages();

    const { getCurrentSessionSettingsReducer: { settings: { currentContextInfo: { currentUserGroups } } } } = useAppSelector(state => state.Global);
    const { informationBase } = useAppSelector(state => state.InformationBases.informationBasesList);
    const { id, isFile, state } = informationBase?.database ?? {};

    const [isOpen, setIsOpen] = useState(false);
    const [isChecked, setIsChecked] = useState(false);

    const closeDialog = () => {
        setIsOpen(false);
    };

    const openDialog = () => {
        setIsOpen(true);
    };

    const confirmButtonHandler = async () => {
        setIsOpen(false);
        show(EMessageType.info, 'Очистка кэша будет завершена в течение нескольких минут');

        try {
            const { success, message } = await postClearCash({ accountDatabaseId: id ?? '', clearDatabaseLogs: isChecked });

            if (success) {
                show(EMessageType.success, 'Очистка кэша завершена');
            } else {
                show(EMessageType.error, message);
            }

            setIsChecked(false);
        } catch (err) {
            show(EMessageType.error, err);
        }
    };

    const checkHandler = () => {
        setIsChecked(prev => !prev);
    };

    if (!(
        isFile &&
        state === DatabaseState.Ready &&
        (
            !currentUserGroups.includes(AccountUserGroup.AccountSaleManager) ||
            (currentUserGroups.includes(AccountUserGroup.AccountSaleManager) && currentUserGroups.length !== 1)
        )
    )) {
        return null;
    }

    return (
        <>
            <OutlinedButton kind="success" onClick={ openDialog }>
                Очистить кэш
            </OutlinedButton>
            <Dialog
                isOpen={ isOpen }
                onCancelClick={ closeDialog }
                title="Очистить кэш"
                isTitleSmall={ true }
                dialogVerticalAlign="center"
                dialogWidth="xs"
                buttons={
                    [
                        {
                            content: 'Отмена',
                            kind: 'default',
                            variant: 'outlined',
                            onClick: closeDialog
                        },
                        {
                            content: 'Очистить',
                            kind: 'success',
                            variant: 'contained',
                            onClick: confirmButtonHandler
                        }
                    ]
                }
            >
                <Box display="flex" flexDirection="column" gap="5px">
                    <TextOut>Действие приведет к очистке кэша вашей информационной базы, вы уверены?</TextOut>
                    <TextOut fontWeight={ 700 } style={ { color: COLORS.error } }>Перед очисткой кэша убедитесь, что база у пользователей закрыта или все сеансы завершатся автоматически.</TextOut>
                    <TextOut className={ styles.checkboxText }>
                        <Checkbox size="small" checked={ isChecked } onChange={ checkHandler } />
                        Хотите ли вы очистить журнал регистрации?
                    </TextOut>
                </Box>
            </Dialog>
        </>
    );
};