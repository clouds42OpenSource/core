export type AccountCardEditAccountDataForm = {
    /**
     *  ID аккаунта
     */
    accountId: string;

    /**
     * Название аккаунта
     */
    accountCaption: string;

    /**
     * ИНН
     */
    inn: string;

    /**
     * ID локали
     */
    localeId: string;

    /**
     * Описание
     */
    description: string;

    /**
     * Признак что аккаунт ВИП
     */
    isVip: boolean;

    /**
     * Письма для рассылок
     */
    emails: string[];
    deployment: number | null;
};