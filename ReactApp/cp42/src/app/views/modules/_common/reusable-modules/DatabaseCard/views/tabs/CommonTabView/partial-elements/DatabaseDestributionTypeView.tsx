import { DistributionType, PlatformType } from 'app/common/enums';
import { ComboBoxForm } from 'app/views/components/controls/forms/ComboBox/ComboBoxForm';
import { ReadOnlyKeyValueView } from 'app/views/modules/_common/components/ReadOnlyKeyValueView';
import { KeyValueDataModel } from 'app/web/common/data-models';
import React from 'react';

type OwnProps = {
    label: string;
    formName: string;
    readOnlyDistributionType: DistributionType;
    editDistributionType: DistributionType;
    platformType: PlatformType;
    alpha83Version: string;
    stable82Version: string;
    stable83Version: string;
    distributionTypes: Array<KeyValueDataModel<DistributionType, string>>;
    isInEditMode: boolean;
    onValueChange: <TValue>(fieldName: string, newValue?: TValue, prevValue?: TValue) => boolean | void;
};
export const DatabaseDestributionTypeView = (props: OwnProps) => {
    const editDistributionType = props.editDistributionType ?? props.readOnlyDistributionType;
    return props.isInEditMode && props.distributionTypes.length
        ? (
            <ComboBoxForm
                formName={ props.formName }
                label={ props.label }
                value={ editDistributionType }
                items={ props.distributionTypes.map(item => {
                    return {
                        value: item.key,
                        text: item.value
                    };
                }) }
                onValueChange={ props.onValueChange }
            />
        )
        : (
            <ReadOnlyKeyValueView
                label={ props.label }
                text={
                    <>
                        {
                            props.platformType === PlatformType.V83
                                ? props.readOnlyDistributionType === DistributionType.Alpha
                                    ? props.alpha83Version
                                    : props.stable83Version
                                : props.platformType === PlatformType.V82
                                    ? props.stable82Version
                                    : ''
                        }
                    </>
                }
            />
        );
};