import { emailRegex, phoneRegex, requiredMessage } from 'app/views/modules/_common/reusable-modules/BoxWithPicture/schemas';
import { transformPhoneValue } from 'app/views/modules/ToPartners/helpers';
import * as Yup from 'yup';

export const consultations1CSchema = Yup.object().shape({
    topic: Yup.string().max(200, 'Максимум 200 символов').required(requiredMessage),
    configuration: Yup.string().required(requiredMessage),
    specialist: Yup.string().required(requiredMessage),
    phone: Yup.string().transform(transformPhoneValue).matches(phoneRegex, 'Неверный формат телефона').required(requiredMessage),
    email: Yup.string().matches(emailRegex, 'Неверный формат почты').required(requiredMessage),
    text: Yup.string().max(50000, 'Максимум 50000 символов').required(requiredMessage)
});

export const consultations1CInitialValues = {
    topic: '',
    configuration: '',
    specialist: '',
    phone: '',
    email: '',
    text: '',
};