import { PlatformType, PlatformTypeDescription } from 'app/common/enums';
import { ComboBoxForm } from 'app/views/components/controls/forms/ComboBox/ComboBoxForm';
import { ReadOnlyKeyValueView } from 'app/views/modules/_common/components/ReadOnlyKeyValueView';
import React from 'react';

type OwnProps = {
    label: string;
    formName: string;
    readOnlyPlatformType: PlatformType;
    editPlatformType: PlatformType;
    platformTypes: Array<PlatformType>;
    isInEditMode: boolean;
    onValueChange: <TValue>(fieldName: string, newValue?: TValue, prevValue?: TValue) => boolean | void;
};
export const DatabasePlatformTypeView = (props: OwnProps) => {
    const editPlatformType = props.editPlatformType ?? props.readOnlyPlatformType;
    return props.isInEditMode && props.platformTypes.length
        ? (
            <ComboBoxForm
                formName={ props.formName }
                label={ props.label }
                value={ editPlatformType }
                items={ props.platformTypes.map(item => {
                    return {
                        value: item,
                        text: PlatformTypeDescription[item]
                    };
                }) }
                onValueChange={ props.onValueChange }
            />
        )
        : (
            <ReadOnlyKeyValueView
                label={ props.label }
                text={ PlatformTypeDescription[props.readOnlyPlatformType] }
            />
        );
};