import { contextAccountId } from 'app/api';
import { GetCurrentSessionsSettingsResponse } from 'app/api/endpoints/global/response';

import { AppRoutes } from 'app/AppRoutes';
import { AccountUserGroup } from 'app/common/enums';
import { ExtensionStateServicesEnum } from 'app/common/enums/ExtensionStateServicesEnum';
import { getErrorMessage } from 'app/common/functions/getErrorMessage';
import { TimerHelper } from 'app/common/helpers/TimerHelper';
import { ReceiveServicesThunkParams } from 'app/modules/infoDbCard/store/reducers/receiveServicesReducer/params';
import { ReceiveStatusServiceThunkParams } from 'app/modules/infoDbCard/store/reducers/receiveStatusServiceInfoDbCardReducer/params';
import { ReceiveServicesInfoDbCardThunk } from 'app/modules/infoDbCard/store/thunks/ReceiveServicesThunk';
import { ReceiveStatusServiceThunk } from 'app/modules/infoDbCard/store/thunks/ReceiveStatusServiceCardThunk';
import { AppReduxStoreState } from 'app/redux/types';
import { COLORS, DateUtility } from 'app/utils';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { SuccessButton } from 'app/views/components/controls/Button';
import { Dialog } from 'app/views/components/controls/Dialog';
import { DialogButtonType } from 'app/views/components/controls/Dialog/views/DialogActionsView/Index';
import CustomizedSwitches from 'app/views/components/controls/Switch/views/SwitchViews';
import { TextOut } from 'app/views/components/TextOut';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { InfoDbServicesFilterDataForm } from 'app/views/modules/AccountManagement/InformationBases/types/InfoDbServicesFilterDataForm';
import { DbStateLineServiceViews } from 'app/views/modules/_common/reusable-modules/InfoDbCard/infoDbCardContent/tabs/ServicesTavView/DbStateLineServices/DbStateLine';
import { ServicesInfoDbListFilterForm } from 'app/views/modules/_common/reusable-modules/InfoDbCard/infoDbCardContent/tabs/ServicesTavView/ServicesInfoDbListFilterFormView';
import { AccountDatabaseListFilterDataFormFieldNamesType } from 'app/views/modules/DatabaseList/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { InfoDbCardAccountDatabaseInfoDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/InfoDbCardAccountDatabaseInfoDataModel';
import { ServicesInfoDbDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/ServicesInfoDbDataModel';

import { RequestKind } from 'core/requestSender/enums';
import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { RouteComponentProps } from 'react-router-dom';

type OwnProps = FloatMessageProps & ReduxFormProps<InfoDbServicesFilterDataForm> & {
    isOpen: boolean;
};

type StateProps = {
    servicesInfoDb: ServicesInfoDbDataModel[];
    commonDetails: InfoDbCardAccountDatabaseInfoDataModel;
    currentUserGroups: Array<AccountUserGroup>;
    settings: GetCurrentSessionsSettingsResponse;
    hasSessionSettingsReceived: boolean;
};

type OwnState = {
    tabValue: number;
    isDialogOpen: boolean;
    lastServiceId: string;
    disableServiceDialog: {
        show: boolean;
        id: string | null;
        name: string | null;
    };
};

type DispatchProps = {
    dispatchServicesCardThunk: (args: ReceiveServicesThunkParams) => void;
    dispatchStatusServicesCardThunk: (args: ReceiveStatusServiceThunkParams) => void;
};

type AllProps = OwnProps & StateProps & DispatchProps & RouteComponentProps;

class ServicesTabViewClass extends React.Component<AllProps, OwnState> {
    private readonly accountId = contextAccountId();

    private dialogButtons: DialogButtonType = [
        {
            onClick: () => this.closeDialogHandler(),
            variant: 'contained',
            kind: 'default',
            content: 'Отменить'
        },
        {
            onClick: () => this.props.history.push(`${ AppRoutes.services.managingServiceDescriptions }/${ this.state.lastServiceId }`),
            variant: 'contained',
            kind: 'success',
            content: 'Перейти'
        }
    ];

    public constructor(props: AllProps) {
        super(props);
        this.state = {
            tabValue: 0,
            isDialogOpen: false,
            lastServiceId: '',
            disableServiceDialog: {
                show: false,
                id: null,
                name: null
            }
        };
        this.onConnectDemoPeriod = this.onConnectDemoPeriod.bind(this);
        this.onTabChange = this.onTabChange.bind(this);
        this.initReduxForm = this.initReduxForm.bind(this);
        this.onValueChange = this.onValueChange.bind(this);
        this.onChangeConnectService = this.onChangeConnectService.bind(this);
        this.getServiceInProccess = this.getServiceInProccess.bind(this);
        this.getInterval = this.getInterval.bind(this);
        this.closeDialogHandler = this.closeDialogHandler.bind(this);
        this.closeDisableServiceDialog = this.closeDisableServiceDialog.bind(this);
        this.onSearch = this.onSearch.bind(this);
    }

    public componentDidMount() {
        this.props.reduxForm.setInitializeFormDataAction(this.initReduxForm);
    }

    private async onConnectDemoPeriod({ id: itemId, name }: ServicesInfoDbDataModel) {
        const { ActivateServicesInfoDbCard, ChangeConnectServicesInfoDbCard } = InterlayerApiProxy.getInfoDbCardApi();
        const { commonDetails: { id, caption }, floatMessage: { show }, reduxForm: { getReduxFormFields }, dispatchServicesCardThunk } = this.props;

        if (this.state.lastServiceId) {
            this.setState({ lastServiceId: '' });
        }

        try {
            const result = await ActivateServicesInfoDbCard(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, {
                accountDatabaseId: id,
                serviceId: itemId,
                accountId: this.accountId
            });

            if (!result.errorMessage) {
                const deadline = DateUtility.dateToDayMonthYear(DateUtility.convertToDate(result.demoPeriodEndDate || ''));

                show(
                    MessageType.Success,
                    `Активирован демо период сервиса '${ name }' для базы '${ caption }' до ${ deadline }.
             Чтобы подключить сервис к ежемесячному платежу, перейдите в меню сервиса и подтвердите подписку.`,
                    2000
                );
            }

            await ChangeConnectServicesInfoDbCard(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, {
                DatabaseId: id,
                ServiceId: itemId,
                Install: true
            });

            setTimeout(() => dispatchServicesCardThunk({
                AccountDatabaseId: id,
                ServiceExtensionsForDatabaseType: this.state.tabValue,
                AccountId: this.accountId,
                ServiceName: getReduxFormFields(false).searchServices,
                force: true,
            }), 1500);
        } catch (e: unknown) {
            show(MessageType.Error, getErrorMessage(e));
        }
    }

    private async onChangeConnectService({ id: serviceId, isInstalled, isActiveService }: ServicesInfoDbDataModel) {
        const infoDbCardApi = InterlayerApiProxy.getInfoDbCardApi();
        const { commonDetails: { id }, dispatchServicesCardThunk, floatMessage: { show }, reduxForm: { getReduxFormFields } } = this.props;

        if (!isActiveService && !isInstalled) {
            this.setState({ isDialogOpen: true, lastServiceId: serviceId });

            return;
        }

        if (this.state.lastServiceId) {
            this.setState({ lastServiceId: '' });
        }

        try {
            const { Message, Success } = await infoDbCardApi.ChangeConnectServicesInfoDbCard(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, {
                DatabaseId: id,
                ServiceId: serviceId,
                Install: !isInstalled
            });

            if (Message && !Success) {
                show(MessageType.Error, getErrorMessage(Message));
            }

            dispatchServicesCardThunk({
                AccountDatabaseId: id,
                ServiceExtensionsForDatabaseType: this.state.tabValue,
                AccountId: this.accountId,
                ServiceName: getReduxFormFields(false).searchServices,
                force: true,
            });
        } catch (e: unknown) {
            show(MessageType.Error, getErrorMessage(e));
        }
    }

    private onValueChange<TValue, TForm extends string = AccountDatabaseListFilterDataFormFieldNamesType>(formName: TForm, newValue: TValue) {
        this.props.reduxForm.updateReduxFormFields({ [formName]: newValue });
    }

    private onSearch() {
        const { dispatchServicesCardThunk, commonDetails, reduxForm } = this.props;

        dispatchServicesCardThunk({
            AccountDatabaseId: commonDetails.id,
            ServiceExtensionsForDatabaseType: this.state.tabValue,
            AccountId: this.accountId,
            ServiceName: reduxForm.getReduxFormFields(false).searchServices,
            force: true,
            showLoadingProgress: false
        });
    }

    private onTabChange(value: number) {
        this.setState({ tabValue: value }, () => {
            this.props.dispatchServicesCardThunk({
                AccountDatabaseId: this.props.commonDetails.id,
                ServiceExtensionsForDatabaseType: value,
                AccountId: this.accountId,
                ServiceName: this.props.reduxForm.getReduxFormFields(false).searchServices,
                force: true,
            });
        });
    }

    private getInterval(id: string) {
        return setInterval(() => {
            this.props.dispatchStatusServicesCardThunk({
                accountDatabaseId: this.props.commonDetails.id,
                serviceId: id,
                force: true,
                showLoadingProgress: false
            });
        }, TimerHelper.waitTimeRefreshData);
    }

    private getServiceInProccess() {
        const { ProcessingInstall, ProcessingDelete } = ExtensionStateServicesEnum;

        return this.props.servicesInfoDb
            .filter(({ extensionState }) => extensionState === ProcessingInstall || extensionState === ProcessingDelete)
            .map(i => i.id);
    }

    private initReduxForm(): InfoDbServicesFilterDataForm | undefined {
        return {
            searchServices: ''
        };
    }

    private closeDialogHandler() {
        this.setState({ isDialogOpen: false });
    }

    private closeDisableServiceDialog() {
        this.setState({
            disableServiceDialog: {
                show: false,
                id: null,
                name: null
            }
        });
    }

    public render() {
        const isDifferentContext = this.props.hasSessionSettingsReceived && this.props.settings.currentContextInfo.isDifferentContextAccount;

        return (
            <>
                <Dialog
                    dialogVerticalAlign="center"
                    maxHeight="195px"
                    isOpen={ this.state.isDialogOpen }
                    dialogWidth="xs"
                    onCancelClick={ this.closeDialogHandler }
                    buttons={ this.dialogButtons }
                >
                    <TextOut textAlign="center" inDiv={ true }>
                        Сервис неактивен. Вы можете активировать его на странице сервиса.
                    </TextOut>
                </Dialog>
                <Dialog
                    title="Сервис заблокирован"
                    isOpen={ this.state.disableServiceDialog.show }
                    dialogWidth="sm"
                    dialogVerticalAlign="center"
                    onCancelClick={ this.closeDisableServiceDialog }
                    buttons={ [
                        {
                            kind: 'default',
                            content: 'Закрыть',
                            onClick: this.closeDisableServiceDialog,
                        },
                        {
                            kind: 'success',
                            content: 'Продлить',
                            onClick: () => {
                                window.open(`${ AppRoutes.services.managingServiceDescriptions }/${ this.state.disableServiceDialog.id }`, '_blank');
                            }
                        }
                    ] }
                >
                    <TextOut>
                        Ваш сервис заблокирован. Вы можете продлить или оплатить сервис на странице <a className="text-link" href={ `${ AppRoutes.services.managingServiceDescriptions }/${ this.state.disableServiceDialog.id }` } rel="noopener noreferrer" target="_blank">{ this.state.disableServiceDialog.name }</a>
                    </TextOut>
                </Dialog>
                <CommonTableWithFilter
                    uniqueContextProviderStateId="AccessInfoDbListContextProviderStateId"
                    filterProps={ {
                        getFilterContentView: () => <ServicesInfoDbListFilterForm
                            onSearch={ this.onSearch }
                            searchServices={ this.props.reduxForm.getReduxFormFields(false).searchServices }
                            onTabChange={ this.onTabChange }
                            onValueChange={ this.onValueChange }
                        />
                    } }
                    tableProps={ {
                        dataset: this.props.servicesInfoDb,
                        keyFieldName: 'id',
                        fieldsView: {
                            name: {
                                caption: 'Название сервиса',
                                fieldContentNoWrap: false,
                                fieldWidth: '30%',
                                format: (value: string, item: ServicesInfoDbDataModel) =>
                                    <>
                                        <TextOut inDiv={ true } fontWeight={ 700 } style={ item.isInstalled ? { color: COLORS.link } : {} }>
                                            { value }
                                        </TextOut>
                                        <DbStateLineServiceViews
                                            extensionState={ item.extensionState }
                                            getInterval={ this.getInterval }
                                            item={ item }
                                            isOpen={ this.props.isOpen }
                                        />
                                    </>
                            },
                            shortDescription: {
                                caption: 'Краткое описание',
                                fieldContentNoWrap: false,
                                fieldWidth: '30%',
                                isSortable: false
                            },
                            extensionLastActivityDate: {
                                caption: 'Дата Вкл/Откл',
                                fieldContentNoWrap: false,
                                isSortable: false,
                                fieldWidth: '30%',
                                format: (value: Date, { isInstalled, extensionState }: ServicesInfoDbDataModel) =>
                                    extensionState !== 6 && isInstalled && value ? DateUtility.dateToDayMonthYearHourMinute(value) : '---'
                            },
                            isActiveService: {
                                caption: (
                                    <TextOut style={ { textAlign: 'center', display: 'block', fontWeight: 600 } }>
                                        Вкл/Откл
                                    </TextOut>
                                ),
                                style: { margin: '0 auto' },
                                fieldContentNoWrap: false,
                                isSortable: false,
                                fieldWidth: '10%',
                                format: (_: boolean, item: ServicesInfoDbDataModel) => {
                                    const { isInstalled, extensionState, isActiveService, isFrozenService } = item;

                                    const onChange = () => {
                                        if (isActiveService && isFrozenService) {
                                            this.setState({
                                                disableServiceDialog: {
                                                    show: true,
                                                    id: item.id,
                                                    name: item.name
                                                }
                                            });
                                            return;
                                        }

                                        void this.onChangeConnectService(item);
                                    };

                                    return (
                                        !isDifferentContext ||
                                        (
                                            this.props.currentUserGroups.includes(AccountUserGroup.CloudAdmin) ||
                                            this.props.currentUserGroups.includes(AccountUserGroup.Hotline) ||
                                            this.props.currentUserGroups.includes(AccountUserGroup.CloudSE)
                                        )
                                    ) && (
                                        <div>
                                            {
                                                extensionState === 6 && !isInstalled && !isActiveService
                                                    ? (
                                                        <SuccessButton onClick={ () => this.onConnectDemoPeriod(item) }>
                                                            Попробовать
                                                        </SuccessButton>
                                                    )
                                                    : (
                                                        <CustomizedSwitches
                                                            checked={ isInstalled && (extensionState === 0 || extensionState === 2) }
                                                            onChange={ onChange }
                                                        />
                                                    )
                                            }
                                        </div>
                                    );
                                }
                            }
                        }
                    } }
                />
            </>
        );
    }
}

const ServicesTabViewConnected = connect<StateProps, DispatchProps, OwnProps, AppReduxStoreState>(
    state => {
        const databaseCardState = state.InfoDbCardState;
        const { servicesInfoDb } = databaseCardState;
        const { commonDetails } = databaseCardState;
        const { hasDatabaseCardReceived } = databaseCardState.hasSuccessFor;
        const { hasServicesReceived } = databaseCardState.hasSuccessFor;
        const hasSessionSettingsReceived = state.Global.getCurrentSessionSettingsReducer.hasSuccessFor.hasSettingsReceived;
        const currentUserGroups = hasSessionSettingsReceived ? state.Global.getCurrentSessionSettingsReducer.settings.currentContextInfo.currentUserGroups : [];
        const { settings } = state.Global.getCurrentSessionSettingsReducer;

        return {
            servicesInfoDb,
            commonDetails,
            hasDatabaseCardReceived,
            hasServicesReceived,
            currentUserGroups,
            hasSessionSettingsReceived,
            settings
        };
    },
    {
        dispatchServicesCardThunk: ReceiveServicesInfoDbCardThunk.invoke,
        dispatchStatusServicesCardThunk: ReceiveStatusServiceThunk.invoke,

    }
)(ServicesTabViewClass);

export const ServicesTabView = withFloatMessages(withRouter(withReduxForm(ServicesTabViewConnected, {
    reduxFormName: 'InfoDbCard_Services_Form'
})));