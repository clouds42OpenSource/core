import '@testing-library/jest-dom/extend-expect';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { DatabaseCardProcessId } from 'app/modules/databaseCard';
import { Provider } from 'react-redux';
import { SnackbarProvider } from 'notistack';

import { PublishDatabaseButton } from 'app/views/modules/_common/reusable-modules/DatabaseCard/views/_buttons/PublishDatabaseButton';

describe('<PublishDatabaseButton />', () => {
    const mockStore = configureStore([thunk]);
    const initialState = {
        DatabaseCardState: {
            processIds: [DatabaseCardProcessId.UnpublishDatabase],
            reducerActions: {
                UNPUBLISH_DATABASE: {
                    hasProcessActionStateChanged: true,
                    processActionState: {
                        isInErrorState: false,
                        isInSuccessState: true,
                        isInProgressState: false
                    },
                    process: {
                        error: undefined
                    }
                }
            },
            process: {
                showLoadingProgress: false
            },
        }
    } as const;

    const getComponent = (isInPublishingState: boolean) => (
        <Provider store={ mockStore(initialState) }>
            <SnackbarProvider>
                <PublishDatabaseButton databaseId="test" isInPublishingState={ isInPublishingState } />
            </SnackbarProvider>
        </Provider>
    );

    it('renders with isInPublishingState param false value', () => {
        render(getComponent(false));
        expect(screen.getByTestId(/button-view/i)).toBeInTheDocument();
        expect(screen.getByText(/Опубликовать базу/i)).toBeInTheDocument();
    });

    it('renders with isInPublishingState param true value', () => {
        render(getComponent(true));
        expect(screen.getByTestId(/button-view/i)).toBeInTheDocument();
        expect(screen.getByText(/База в процессе публикации/i)).toBeInTheDocument();
    });

    it('has onClick method', async () => {
        const { container } = render(getComponent(false));
        await userEvent.click(screen.getByTestId(/button-view/i));
        expect(container.querySelector('.MuiTouchRipple-child')).toBeInTheDocument();
    });
});