import { CheckBoxForm } from 'app/views/components/controls/forms';
import { TextOut } from 'app/views/components/TextOut';
import React from 'react';

type OwnProps = {
    label: string;
    formName: string;
    readOnlyUsedWebServices: boolean;
    editUsedWebServices: boolean;
    isInEditMode: boolean;
    onValueChange: (fieldName: string, newValue: boolean, prevValue: boolean) => boolean | void;
};
export const UsedWebServicesView = (props: OwnProps) => {
    const editUsedWebServices = props.editUsedWebServices ?? props.readOnlyUsedWebServices;

    return props.isInEditMode
        ? (
            <CheckBoxForm
                formName={ props.formName }
                label={ props.label }
                falseText={ <TextOut fontWeight={ 600 } fontSize={ 12 }>(нет)</TextOut> }
                trueText={ <TextOut fontWeight={ 600 } fontSize={ 12 }>(да)</TextOut> }
                isChecked={ editUsedWebServices }
                onValueChange={ props.onValueChange }
            />
        )
        : (
            <CheckBoxForm
                formName={ props.formName }
                label={ props.label }
                falseText={ <TextOut fontWeight={ 600 } fontSize={ 12 }>(нет)</TextOut> }
                trueText={ <TextOut fontWeight={ 600 } fontSize={ 12 }>(да)</TextOut> }
                isChecked={ props.readOnlyUsedWebServices }
            />
        );
};