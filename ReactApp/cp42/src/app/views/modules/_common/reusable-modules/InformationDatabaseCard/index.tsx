import SaveIcon from '@mui/icons-material/Save';
import { OutlinedInput } from '@mui/material';
import { TEditInformationBaseRequest } from 'app/api/endpoints/informationBases/request';
import { TTabVisibility } from 'app/api/endpoints/informationBases/response';
import { FETCH_API } from 'app/api/useFetchApi';
import { REDUX_API } from 'app/api/useReduxApi';
import { DatabaseState } from 'app/common/enums';
import { compareArray } from 'app/common/functions';
import { EMessageType, useAppDispatch, useAppSelector, useFloatMessages } from 'app/hooks';
import { informationBasesList } from 'app/modules/informationBases';
import { COLORS } from 'app/utils';
import { DefaultButton } from 'app/views/components/controls/Button';
import { Dialog } from 'app/views/components/controls/Dialog';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { TabsControl } from 'app/views/components/controls/TabsControl';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { AccessIsOther } from 'app/views/modules/_common/reusable-modules/InformationDatabaseCard/widgets/AccessIsOther';
import { EditInfoDbDataButton } from 'app/views/modules/_common/reusable-modules/InformationDatabaseCard/widgets/Header/buttons';
import { memo, useCallback, useEffect, useMemo, useState } from 'react';

import { Access, ArchivalCopies, General, Header, Services, Settings } from './widgets';

type TProps = {
    isOpen: boolean;
    databaseNumber: string;
    onClose: () => void;
};

const { setLoading } = informationBasesList.actions;
const { getInformationBaseInfo, getInformationBases, getAvailabilityInformationBases } = REDUX_API.INFORMATION_BASES_REDUX;
const { editInformationBase } = FETCH_API.INFORMATION_BASES;

const getHeaders = (isOther: boolean, tabVisibility?: TTabVisibility, isDelimiters?: boolean, state?: DatabaseState) => {
    const headers = [
        {
            title: 'Общие',
            isVisible: true,
        },
        {
            title: 'Настройки доступа',
            isVisible: state !== DatabaseState.DeletedToTomb && state !== DatabaseState.DetachedToTomb
        },
    ];

    if (!isOther) {
        headers.push(...[
            {
                title: 'Сервисы',
                isVisible: !!isDelimiters && state === DatabaseState.Ready
            },
            {
                title: 'Архивные копии',
                isVisible: true
            },
            {
                title: 'Настройки АО и ТИИ',
                isVisible: !!tabVisibility?.isTehSupportTabVisible && state !== DatabaseState.DeletedToTomb && state !== DatabaseState.DetachedToTomb
            },
        ]);
    }

    return headers;
};

export const InformationDatabaseCard = memo<TProps>(({ isOpen, databaseNumber, onClose }) => {
    const dispatch = useAppDispatch();
    const { show } = useFloatMessages();

    const {
        informationBase,
        error,
        loading,
        accessInfoDb,
        valueCheck,
        isOther,
        params,
        otherParams,
        availability
    } = useAppSelector(state => state.InformationBases.informationBasesList);

    const isMyDbExists = !!informationBase && !isOther;
    const isDbNotDeleted = informationBase?.database.state !== DatabaseState.DeletedToTomb && informationBase?.database.state !== DatabaseState.DetachedToTomb;
    const isDbOnDelimiters = !!informationBase?.database.isDbOnDelimiters;

    const [showAccessOrPayDialog, setShowAccessOrPayDialog] = useState(false);
    const [databaseName, setDatabaseName] = useState(informationBase?.database.caption ?? '');

    useEffect(() => {
        if (!availability && isOpen) {
            void getAvailabilityInformationBases(dispatch);
        }
    }, [dispatch, availability, isOpen]);

    useEffect(() => {
        if (databaseNumber) {
            void getInformationBaseInfo(dispatch, databaseNumber);
        }
    }, [databaseNumber, dispatch]);

    useEffect(() => {
        if (error) {
            show(EMessageType.error, error);
            onClose();
        }
    }, [error, show, onClose]);

    useEffect(() => {
        if (informationBase && informationBase.database) {
            setDatabaseName(informationBase.database.caption);
        }
    }, [informationBase]);

    const editDatabase = async (body: Partial<TEditInformationBaseRequest>) => {
        if (informationBase?.database) {
            setLoading(true);

            const { id, v82Name, caption, dbTemplate, templatePlatform, distributionType, usedWebServices, state, fileStorageId, restoreModelType } = informationBase.database;

            const { success, message } = await editInformationBase({
                databaseId: body.databaseId ?? id,
                v82Name: body.v82Name ?? v82Name,
                databaseCaption: body.databaseCaption ?? caption,
                databaseTemplateId: body.databaseTemplateId ?? dbTemplate,
                platformType: body.platformType ?? templatePlatform,
                distributionType: body.distributionType ?? distributionType,
                usedWebServices: body.usedWebServices ?? usedWebServices,
                databaseState: body.databaseState ?? state,
                fileStorageId: body.fileStorageId ?? fileStorageId,
                restoreModel: body.restoreModel ?? restoreModelType
            });

            if (success) {
                void getInformationBaseInfo(dispatch, body.v82Name ?? databaseNumber);

                void getInformationBases(dispatch, isOther ? otherParams : params, !isOther);
            } else if (message) {
                setLoading(false);
                show(EMessageType.error, message);
            }
        }
    };

    const showSaveButton = useMemo(() => {
        return compareArray(
            accessInfoDb.flatMap(item => {
                if (item.hasAccess) {
                    return item.userId;
                }

                return [];
            }),
            valueCheck
        );
    }, [accessInfoDb, valueCheck]);

    const showAccessOrPay = useCallback(() => {
        setShowAccessOrPayDialog(prev => !prev);
    }, []);

    return (
        <>
            { loading && <LoadingBounce /> }
            <Dialog
                isOpen={ isOpen }
                dialogWidth="lg"
                onCancelClick={ onClose }
                isTitleSmall={ true }
                title={ `Информационная база: ${ informationBase?.database.caption ?? '' }` }
                buttons={ () => (
                    <>
                        <DefaultButton onClick={ onClose }>Закрыть</DefaultButton>
                        { showSaveButton && (
                            <EditInfoDbDataButton
                                showAccessOrPayDialog={ showAccessOrPayDialog }
                                cancelClickShowAccessOrPayDialog={ isDbOnDelimiters ? showAccessOrPay : undefined }
                            />
                        ) }
                    </>
                ) }
            >
                <Header onClose={ onClose } />
                <FormAndLabel label="Наименование">
                    <OutlinedInput
                        value={ databaseName }
                        onChange={ ({ target: { value } }) => setDatabaseName(value) }
                        fullWidth={ true }
                        endAdornment={
                            databaseName !== informationBase?.database.caption
                                ? (
                                    <SaveIcon
                                        sx={ { cursor: 'pointer', '&:hover': { color: COLORS.main } } }
                                        onClick={ () => editDatabase({ databaseCaption: databaseName }) }
                                    />
                                )
                                : undefined
                        }
                        disabled={ isOther || informationBase?.database.state === DatabaseState.DeletedToTomb }
                    />
                </FormAndLabel>
                <TabsControl headers={ getHeaders(!!isOther, informationBase?.tabVisibility, isDbOnDelimiters, informationBase?.database.state) }>
                    <General editDatabase={ editDatabase } />
                    { isDbNotDeleted && (
                        <>
                            {
                                isMyDbExists
                                    ? <Access showAccessOrPayDialog={ showAccessOrPay } />
                                    : <AccessIsOther onCloseHandler={ onClose } />
                            }
                        </>
                    ) }
                    { isMyDbExists && isDbOnDelimiters && informationBase?.database.state === DatabaseState.Ready && <Services databaseId={ informationBase.database.id } /> }
                    { !isOther && <ArchivalCopies /> }
                    { isMyDbExists && informationBase.tabVisibility.isTehSupportTabVisible && isDbNotDeleted && <Settings databaseId={ informationBase.database.id } /> }
                </TabsControl>
            </Dialog>
        </>
    );
});