import { accountId, contextAccountId } from 'app/api';
import { AccountUserGroup } from 'app/common/enums';
import { getArrayFromEnum } from 'app/common/functions/getArrayFromEnum';
import { inputArrayInRequestQuery } from 'app/common/functions/inputArrayInRequestQuery';
import { TimerHelper } from 'app/common/helpers/TimerHelper';
import { ReceiveAccessThunkParams } from 'app/modules/infoDbCard/store/reducers/receiveAccessInfoDbCardReducer/params';
import { ReceiveBackupsThunkParams } from 'app/modules/infoDbCard/store/reducers/receiveBuckupsReducer/params';
import { ReceiveAccessCostThunkParams } from 'app/modules/infoDbCard/store/reducers/receiveCalculateAccessInfoDbCardReducer/params';
import { ReceiveInfoDbCardThunkParams } from 'app/modules/infoDbCard/store/reducers/receiveDatabaseCardReducer/params';
import { ReceiveRightsInfoDbCardThunkParams } from 'app/modules/infoDbCard/store/reducers/receiveRightsInfoDbCardReducer/params';
import { ReceiveServicesThunkParams } from 'app/modules/infoDbCard/store/reducers/receiveServicesReducer/params';
import { ReceiveInfoDbCardThunk } from 'app/modules/infoDbCard/store/thunks';
import { ReceiveAccessThunk } from 'app/modules/infoDbCard/store/thunks/ReceiveAccessCardThunk';
import { ReceiveAccessCostThunk } from 'app/modules/infoDbCard/store/thunks/ReceiveAccessCostCardThunk';
import { ReceiveCommonInfoDbCardThunk } from 'app/modules/infoDbCard/store/thunks/ReceiveCommonInfoDbCardThunk';
import { ReceiveRightsInfoDbCardThunk } from 'app/modules/infoDbCard/store/thunks/ReceiveRightsInfoDbCardThunk';
import { AppReduxStoreState } from 'app/redux/types';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { IReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { DefaultButton } from 'app/views/components/controls/Button';
import { Dialog } from 'app/views/components/controls/Dialog';
import { EditInfoDbDataButton } from 'app/views/modules/_common/reusable-modules/InfoDbCard/buttons/EditInfoDbDataButton';
import { defineInfoDbInProccessing } from 'app/views/modules/_common/reusable-modules/InfoDbCard/functions/defineInfoDbInProccessing';
import { InfoDbCardContent } from 'app/views/modules/_common/reusable-modules/InfoDbCard/infoDbCardContent/InfoDbCardContent';
import { compareArray } from 'app/views/modules/AccountManagement/InformationBases/functions/compareArray';
import { InfoDbCardEditDatabaseDataForm } from 'app/views/modules/AccountManagement/InformationBases/types/InfiDbCardEditDatabaseDataForm';
import { AccountUserDataModelsList } from 'app/web/api/InfoDbCardProxy/request-dto/InputCalculateAccesses';
import { GetAvailabilityInfoDb } from 'app/web/api/InfoDbProxy/responce-dto/GetAvailabilityInfoDb';
import { InfoDbCardPermissionsEnum } from 'app/web/common/enums/InfoDbCardPermissionsEnum';
import { InfoDbListItemDataModel } from 'app/web/InterlayerApiProxy/InfoDbApiProxy/receiveDatabaseList/data-models';
import { DatabaseAccessesDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/AccessInfoDbDataModel';
import { CalculateAccessInfoDbDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/CalculateAccessInfoDbDataModel';
import { EternalUserDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/EternalUserDataModel';
import { InfoDbCardAccountDatabaseInfoDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/InfoDbCardAccountDatabaseInfoDataModel';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';
import React, { memo } from 'react';
import { connect } from 'react-redux';

type OwnProps = {
    isOpen: boolean;
    accountGroup: boolean;
    onCancelDialogButtonClick: () => void;
    databaseId?: string;
    item?: InfoDbListItemDataModel | null,
    changeField: boolean;
    onChangeField: () => void;
    activeTab: number;
    clearTabIndex: (id: string) => void
    onDataSelect?: () => void;
    onDataSelectOther?: () => void;
    isOther: boolean;
    accessPermissions: boolean;
    onChangeTab: (tab: number) => void;
    isAdmin?: boolean;
};

type OwnState = {
    users: EternalUserDataModel[];
    valueCheck: string[];
    allCheck: boolean;
    visibleButtonSave: boolean;
    successAccess: boolean;
    showAccessOrPayDialog: boolean;
};

type StateProps = {
    commonDetails: InfoDbCardAccountDatabaseInfoDataModel;
    permissions: string[];
    hasDatabaseCardReceived: boolean;
    hasDatabaseCardRightsReceived: boolean;
    accesstInfoDb: DatabaseAccessesDataModel[];
    hasAccessReceived: boolean;
    hasAccessCostReceived: boolean;
    accessCostInfoDb: CalculateAccessInfoDbDataModel;
    availability: GetAvailabilityInfoDb;
    currentUserGroups: AccountUserGroup[];
    errorOpenDatabase: string | null;
};

type DispatchProps = {
    dispatchAccessCardThunk: (args: ReceiveAccessThunkParams) => void;
    dispatchReceiveInfoDbCardThunk: (args: IForceThunkParam & ReceiveInfoDbCardThunkParams & ReceiveServicesThunkParams & ReceiveBackupsThunkParams & { isOther?: boolean }) => void;
    dispatchReceiveCommonInfoDbCardThunk: (args: IForceThunkParam & ReceiveInfoDbCardThunkParams) => void;
    dispatchReceiveRightsInfoDbCardThunk: (args?: ReceiveRightsInfoDbCardThunkParams) => void;
    dispatchAccessCostCardThunk: (args: ReceiveAccessCostThunkParams) => void;
};
type AllProps = OwnProps & StateProps & DispatchProps & FloatMessageProps;

class InfoDbCardClass extends React.Component<AllProps, OwnState> {
    private _intervalId: NodeJS.Timeout | number;

    private accountId = contextAccountId();

    private databaseDataTabRef = React.createRef<IReduxForm<InfoDbCardEditDatabaseDataForm>>();

    public constructor(props: AllProps) {
        super(props);

        this.state = {
            users: [],
            valueCheck: [],
            allCheck: false,
            visibleButtonSave: false,
            successAccess: false,
            showAccessOrPayDialog: false
        };

        this._intervalId = setInterval(() => void 0, TimerHelper.waitTimeRefreshData);
        this.getDialogButtons = this.getDialogButtons.bind(this);
        this.addUsers = this.addUsers.bind(this);
        this.onResetAddUser = this.onResetAddUser.bind(this);
        this.onValueChangeCheckBox = this.onValueChangeCheckBox.bind(this);
        this.initialStateValueCheck = this.initialStateValueCheck.bind(this);
        this.onValueChangeAll = this.onValueChangeAll.bind(this);
        this.calculateCost = this.calculateCost.bind(this);
        this.refreshAllCheck = this.refreshAllCheck.bind(this);
        this.onSuccessAccess = this.onSuccessAccess.bind(this);
        this.showAccessOrPayDialog = this.showAccessOrPayDialog.bind(this);
    }

    public componentDidUpdate(prevProps: AllProps, prevState: OwnState) {
        if (!prevProps.hasAccessReceived && this.props.hasAccessReceived) {
            this.refreshAllCheck();
        }

        if (prevState.valueCheck.length !== this.state.valueCheck.length) {
            this.calculateCost();
        }

        if (
            defineInfoDbInProccessing(
                this.props.commonDetails.databaseState,
                this.props.commonDetails.publishState,
                this.props.commonDetails.isExistTerminatingSessionsProcessInDatabase
            ) &&
            this.props.isOpen
        ) {
            clearInterval(this._intervalId);
            this._intervalId = setInterval(() => {
                this.props.dispatchReceiveCommonInfoDbCardThunk({
                    accountId: this.accountId,
                    databaseId: this.props.item?.databaseId ?? this.props.databaseId ?? '',
                    force: true,
                    showLoadingProgress: false,
                });
            }, TimerHelper.waitTimeRefreshData);
        }

        if (
            !defineInfoDbInProccessing(
                this.props.commonDetails.databaseState,
                this.props.commonDetails.publishState,
                this.props.commonDetails.isExistTerminatingSessionsProcessInDatabase
            ) &&
            this._intervalId
        ) {
            clearInterval(this._intervalId);
        }

        if (
            (!prevProps.hasDatabaseCardReceived && this.props.hasDatabaseCardReceived) ||
            prevProps.accesstInfoDb.filter(i => i.hasAccess).length !== this.props.accesstInfoDb.filter(i => i.hasAccess).length
        ) {
            this.setState({
                valueCheck: this.initialStateValueCheck()
            });
            this.refreshAllCheck();
        }

        if (prevProps.isOpen && !this.props.isOpen) {
            clearInterval(this._intervalId);
            this.setState({
                valueCheck: [],
                allCheck: false
            });
            this.props.clearTabIndex(this.props.commonDetails.id);
            this.onResetAddUser(true);
        }

        if (this.props.errorOpenDatabase !== prevProps.errorOpenDatabase && this.props.errorOpenDatabase) {
            this.props.floatMessage.show(MessageType.Error, this.props.errorOpenDatabase);
            this.props.onCancelDialogButtonClick();
        }

        if (!prevProps.isOpen && this.props.isOpen) {
            this.props.dispatchReceiveRightsInfoDbCardThunk({
                databaseId: this.props.item?.databaseId ?? this.props.databaseId ?? '',
                objectActions: inputArrayInRequestQuery(getArrayFromEnum(InfoDbCardPermissionsEnum), 'objectActions'),
                force: false,
                showLoadingProgress: false
            });

            if (this.props.currentUserGroups.length === 1 && this.props.currentUserGroups.includes(AccountUserGroup.AccountUser)) {
                this.props.dispatchReceiveCommonInfoDbCardThunk({
                    accountId: this.accountId,
                    databaseId: this.props.item?.databaseId ?? this.props.databaseId ?? '',
                    force: true
                });
            } else {
                this.props.dispatchReceiveInfoDbCardThunk({
                    accountId: this.accountId,
                    AccountId: this.accountId,
                    databaseId: this.props.item?.databaseId ?? this.props.databaseId ?? '',
                    isDbOnDelimiters: this.props.item?.isDbOnDelimiters ?? this.props.commonDetails.isDbOnDelimiters,
                    force: true,
                    AccountDatabaseId: this.props.item?.databaseId ?? this.props.databaseId ?? '',
                    ServiceExtensionsForDatabaseType: 0,
                    ServiceName: '',
                    CreationDateFrom: '',
                    CreationDateTo: '',
                    DatabaseId: this.props.item?.databaseId ?? this.props.databaseId ?? '',
                    isOther: this.props.isOther
                });
            }
        }

        if (this.props.changeField) {
            this.props.dispatchReceiveCommonInfoDbCardThunk({
                accountId: this.accountId,
                databaseId: this.props.item?.databaseId ?? this.props.databaseId ?? '',
                force: true,
                showLoadingProgress: false,
            });
            this.props.onChangeField();
        }
    }

    public componentWillUnmount(): void {
        clearInterval(this._intervalId);
    }

    private onSuccessAccess() {
        this.setState(prevState => ({
            successAccess: prevState.successAccess
        }));
    }

    private onResetAddUser(isClose?: boolean) {
        this.setState(prev => ({
            users: isClose ? [] : prev.users,
            valueCheck: this.initialStateValueCheck(),
            allCheck: false
        }));
    }

    private onValueChangeAll() {
        const all = [...Object.values(this.props.accesstInfoDb).map(i => i.userId), ...this.state.users.map(i => i.userId!)];

        const compareWithInitial = [...this.props.accesstInfoDb.filter(i => i.hasAccess), ...this.state.users.filter(i => i.hasAccess)].length;

        this.state.allCheck
            ? this.setState(prevState => ({
                valueCheck: [],
                allCheck: !prevState.allCheck,
                visibleButtonSave: compareWithInitial !== 0
            }))
            : this.setState(prevState => ({
                valueCheck: all,
                allCheck: !prevState.allCheck,
                visibleButtonSave: compareWithInitial !== all.length
            }));
    }

    private onValueChangeCheckBox(value: string) {
        if (this.state.valueCheck.indexOf(value) === -1) {
            this.setState(prevState => ({
                allCheck: [...this.props.accesstInfoDb, ...prevState.users].length === prevState.valueCheck.length + 1,
                valueCheck: [...prevState.valueCheck, value],
                visibleButtonSave: compareArray(this.props.accesstInfoDb.filter(i => i.hasAccess).map(i => i.userId), [...prevState.valueCheck, value])
            }));
        } else {
            this.setState(prevState => ({
                allCheck: false,
                valueCheck: prevState.valueCheck.filter((i: string) => i !== value),
                visibleButtonSave: compareArray(this.props.accesstInfoDb.filter(i => i.hasAccess).map(i => i.userId), prevState.valueCheck.filter((i: string) => i !== value))
            }));
        }
    }

    private getDialogButtons() {
        return (
            <>
                <DefaultButton title="Отмена" onClick={ this.props.onCancelDialogButtonClick }>Закрыть</DefaultButton>
                { this.state.visibleButtonSave && !this.props.commonDetails.isDbOnDelimiters &&
                    <EditInfoDbDataButton
                        users={ this.state.users }
                        dispatchAccessCardThunk={ this.props.dispatchAccessCardThunk }
                        onSuccessAccess={ this.onSuccessAccess }
                        databaseId={ this.props.commonDetails.id }
                        onResetAddUser={ this.onResetAddUser }
                        valueCheck={ this.state.valueCheck }
                        accessReceivedStatus={ this.props.hasAccessCostReceived }
                        cost={ this.props.accessCostInfoDb.map(i => i.accessCost).reduce((acc, num) => acc + num, 0) }
                    />
                }
            </>
        );
    }

    private calculateCost() {
        const userWithHasAccessId = this.props.accesstInfoDb.reduce((userWithHasAccess: string[], user) => {
            if (user.hasAccess) {
                userWithHasAccess.push(user.userId);
            }

            return userWithHasAccess;
        }, []);

        const removedAccessUserList = userWithHasAccessId.reduce((userList: AccountUserDataModelsList[], userId) => {
            if (!this.state.valueCheck.includes(userId)) {
                userList.push({
                    UserId: userId,
                    HasAccess: false,
                    State: 4,
                    HasLicense: false
                });
            }

            return userList;
        }, []);

        const accountUserDataModelsList = this.state.valueCheck.reduce((userList: AccountUserDataModelsList[], userId) => {
            if (!userWithHasAccessId.includes(userId)) {
                userList.push({
                    UserId: userId,
                    HasAccess: true,
                    State: 4,
                    HasLicense: false
                });
            }

            return userList;
        }, []);

        if (
            this.props.currentUserGroups.includes(AccountUserGroup.AccountAdmin) ||
            this.props.currentUserGroups.includes(AccountUserGroup.Hotline) ||
            this.props.currentUserGroups.includes(AccountUserGroup.CloudAdmin) ||
            this.props.currentUserGroups.includes(AccountUserGroup.CloudSE)
        ) {
            this.props.dispatchAccessCostCardThunk({
                accountUserDataModelsList: removedAccessUserList.concat(accountUserDataModelsList),
                serviceTypesIdsList: !this.props.commonDetails.isFile && !this.props.commonDetails.isDbOnDelimiters
                    ? [this.props.commonDetails.myDatabasesServiceTypeId, this.props.availability.AccessToServerDatabaseServiceTypeId]
                    : [this.props.commonDetails.myDatabasesServiceTypeId],
                force: true,
                showLoadingProgress: false
            });
        }
    }

    private addUsers(result: EternalUserDataModel) {
        if (result.userId) {
            this.setState({
                users: [...this.state.users, result],
                valueCheck: this.props.commonDetails.isDbOnDelimiters ? this.state.valueCheck : [...this.state.valueCheck, result?.userId],
                visibleButtonSave: compareArray(this.props.accesstInfoDb.filter(i => i.hasAccess).map(i => i.userId), [...this.state.valueCheck, result.userId])
            });
        }
    }

    private initialStateValueCheck() {
        this.setState({ visibleButtonSave: false });

        return [...Object.values(this.props.accesstInfoDb).filter(i => i.hasAccess).map(i => i.userId)];
    }

    private refreshAllCheck() {
        this.setState({
            allCheck: this.props.accesstInfoDb.every(i => i.hasAccess)
        });
    }

    private showAccessOrPayDialog() {
        this.setState(prev => ({
            showAccessOrPayDialog: !prev.showAccessOrPayDialog
        }));
    }

    public render() {
        const isDefaultUser = this.props.currentUserGroups.length === 1 && this.props.currentUserGroups.includes(AccountUserGroup.AccountUser);

        if ((this.props.hasDatabaseCardReceived && this.props.hasDatabaseCardRightsReceived) || isDefaultUser) {
            return (
                <Dialog
                    title={ this.props.commonDetails.caption }
                    isTitleSmall={ true }
                    titleFontSize={ 20 }
                    titleTextAlign="center"
                    isOpen={ this.props.isOpen }
                    dialogWidth="lg"
                    onCancelClick={ this.props.onCancelDialogButtonClick }
                    buttons={ () => this.getDialogButtons() }
                >
                    <InfoDbCardContent
                        successAccess={ this.state.successAccess }
                        onDataSelect={ this.props.onDataSelect }
                        onDataSelectOther={ this.props.onDataSelectOther }
                        allCheck={ this.state.allCheck }
                        onValueChangeAll={ this.onValueChangeAll }
                        addUsers={ this.addUsers }
                        onValueChangeCheckBox={ this.onValueChangeCheckBox }
                        valueCheck={ this.state.valueCheck }
                        users={ this.state.users }
                        activeTab={ this.props.activeTab }
                        permissions={ this.props.permissions }
                        item={ this.props.item }
                        databaseDataTabRef={ this.databaseDataTabRef }
                        onChangeField={ this.props.onChangeField }
                        onResetAddUser={ this.onResetAddUser }
                        isOpen={ this.props.isOpen }
                        onCancelDialogButtonClick={ this.props.onCancelDialogButtonClick }
                        accountGroup={ this.props.accountGroup }
                        isOther={ this.props.isOther }
                        accessPermissions={ this.props.accessPermissions }
                        onChangeTab={ this.props.onChangeTab }
                        isAdmin={ this.props.isAdmin }
                        showAccessOrPayDialog={ this.showAccessOrPayDialog }
                    />
                    {
                        this.state.showAccessOrPayDialog && this.props.commonDetails.isDbOnDelimiters && (
                            <EditInfoDbDataButton
                                users={ this.state.users }
                                dispatchAccessCardThunk={ this.props.dispatchAccessCardThunk }
                                onSuccessAccess={ this.onSuccessAccess }
                                databaseId={ this.props.commonDetails.id }
                                onResetAddUser={ this.onResetAddUser }
                                valueCheck={ this.state.valueCheck }
                                accessReceivedStatus={ this.props.hasAccessCostReceived }
                                cost={ this.props.accessCostInfoDb.map(i => i.accessCost).reduce((acc, num) => acc + num, 0) }
                                showAccessOrPayDialog={ this.state.showAccessOrPayDialog }
                                cancelClickShowAccessOrPayDialog={ this.showAccessOrPayDialog }
                            />
                        )
                    }
                </Dialog>
            );
        }

        return null;
    }
}

export const InfoDbCardConnect = connect<StateProps, DispatchProps, OwnProps, AppReduxStoreState>(
    state => {
        const infoDbListState = state.InfoDbListState;
        const databaseCardState = state.InfoDbCardState;
        const { commonDetails } = databaseCardState;
        const permissions = databaseCardState.rightsInfoDb;
        const { hasDatabaseCardReceived } = databaseCardState.hasSuccessFor;
        const { hasDatabaseCardRightsReceived } = databaseCardState.hasSuccessFor;
        const { hasAccessReceived, hasAccessCostReceived } = databaseCardState.hasSuccessFor;
        const accesstInfoDb = databaseCardState.databaseAccesses;
        const accessCostInfoDb = databaseCardState.databaseAccessesCost;
        const availability = infoDbListState.AvailabilityInfoDb;
        const { currentUserGroups } = state.Global.getCurrentSessionSettingsReducer.settings.currentContextInfo;
        const errorOpenDatabase = databaseCardState.process.error;

        return {
            commonDetails,
            permissions,
            hasDatabaseCardReceived,
            hasDatabaseCardRightsReceived,
            accesstInfoDb,
            hasAccessReceived,
            hasAccessCostReceived,
            accessCostInfoDb,
            availability,
            currentUserGroups,
            errorOpenDatabase
        };
    },
    {
        dispatchReceiveInfoDbCardThunk: ReceiveInfoDbCardThunk.invoke,
        dispatchReceiveCommonInfoDbCardThunk: ReceiveCommonInfoDbCardThunk.invoke,
        dispatchReceiveRightsInfoDbCardThunk: ReceiveRightsInfoDbCardThunk.invoke,
        dispatchAccessCostCardThunk: ReceiveAccessCostThunk.invoke,
        dispatchAccessCardThunk: ReceiveAccessThunk.invoke
    }
)(memo(InfoDbCardClass));

export const InfoDbCard = withFloatMessages(InfoDbCardConnect);