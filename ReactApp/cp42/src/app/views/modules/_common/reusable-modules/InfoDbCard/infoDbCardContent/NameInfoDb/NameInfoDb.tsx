import { Box } from '@mui/material';
import { getErrorMessage } from 'app/common/functions/getErrorMessage';
import { EditDbListThunk } from 'app/modules/infoDbList/store/thunks/EditDbListThunk';
import { AppReduxStoreState } from 'app/redux/types';
import { COLORS, DateUtility } from 'app/utils';
import { TextOut } from 'app/views/components/TextOut';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { NameInfoDbForm } from 'app/views/modules/AccountManagement/InformationBases/types/NameInfoDbForm';
import { InfoDbCaptionView } from 'app/views/modules/_common/reusable-modules/InfoDbCard/infoDbCardContent/tabs/CommonTabView/partial-elements';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { InfoDbCardAccountDatabaseInfoDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/InfoDbCardAccountDatabaseInfoDataModel';
import { RequestKind } from 'core/requestSender/enums';
import dayjs from 'dayjs';
import React from 'react';
import { connect } from 'react-redux';

type OwnProps = FloatMessageProps & ReduxFormProps<NameInfoDbForm>;

type StateProps = {
    commonDetails: InfoDbCardAccountDatabaseInfoDataModel;
};

type GetProps = {
    isOther: boolean;
    onChangeField: () => void;
};

type OwnState = {
    errorName: string | null;
    name: string;
};

type AllProps = OwnProps & StateProps & GetProps;

class NameInfoDbClass extends React.Component<AllProps, OwnState> {
    public constructor(props: AllProps) {
        super(props);
        this.state = {
            errorName: '',
            name: this.props.reduxForm.getReduxFormFields(false).caption
        };

        this.initReduxForm = this.initReduxForm.bind(this);
        this.onValueChange = this.onValueChange.bind(this);
        this.changeCaption = this.changeCaption.bind(this);
        this.validateCaption = this.validateCaption.bind(this);
        props.reduxForm.setInitializeFormDataAction(this.initReduxForm);
    }

    private onValueChange<TValue>(fieldName: string, newValue?: TValue, _prevValue?: TValue) {
        this.props.reduxForm.updateReduxFormFields({
            [fieldName]: newValue
        });
    }

    private validateCaption(str: string) {
        return /^[а-яА-ЯІіЇїЄєҐґёЁa-zA-Z0-9\-_.():, ]+$/.test(str);
    }

    private async changeCaption() {
        const infoDbCardApi = InterlayerApiProxy.getInfoDbApi();

        try {
            if (!this.validateCaption(this.props.reduxForm.getReduxFormFields(true).caption)) {
                const errSymbol = this.props.reduxForm.getReduxFormFields(true).caption
                    .split('')
                    .filter(item => !this.validateCaption(item));

                this.setState({
                    errorName: `В названии БД указаны недопустимые символы ${ errSymbol.join(' ') }`
                });

                return;
            }

            if (
                this.props.reduxForm.getReduxFormFields(true).caption !== this.state.name
            ) {
                this.setState({ errorName: '' }, async () => {
                    infoDbCardApi.editInfoDbList(
                        RequestKind.SEND_BY_USER_SYNCHRONOUSLY,
                        {
                            databaseId: this.props.commonDetails.id,
                            caption: this.props.reduxForm.getReduxFormFields(false).caption,
                        }
                    ).then(a => {
                        if (a.Success) {
                            this.props.onChangeField();
                            this.setState({ name: this.props.reduxForm.getReduxFormFields(true).caption });
                        }
                    });
                });
            }
        } catch (e) {
            this.props.floatMessage.show(MessageType.Error, getErrorMessage(e));
        }
    }

    private initReduxForm(): NameInfoDbForm | undefined {
        return {
            caption: this.props.commonDetails.caption,
        };
    }

    public render() {
        const { commonDetails: { creationDate, isDemoDelimiters, caption }, isOther, reduxForm } = this.props;
        const daysLeft = dayjs(creationDate).add(7, 'day').diff(undefined, 'day');

        return (
            <Box onBlur={ this.changeCaption }>
                <InfoDbCaptionView
                    formName="caption"
                    label="Наименование:"
                    readOnlyDatabaseCaption={ caption }
                    editDatabaseCaption={ reduxForm.getReduxFormFields(false).caption }
                    isInEditMode={ isOther }
                    onValueChange={ this.onValueChange }
                />
                { isDemoDelimiters && (
                    <TextOut fontSize={ 11 } style={ { color: COLORS.warning } }>
                        Демо база будет удалена через { daysLeft } { DateUtility.getDayWord(daysLeft) }
                    </TextOut>
                ) }
                {
                    this.state.errorName && (
                        <TextOut style={ { marginBottom: '0px', color: COLORS.error } } inDiv={ true } fontSize={ 14 } fontWeight={ 400 }>
                            { this.state.errorName }
                        </TextOut>
                    )
                }
            </Box>
        );
    }
}

export const NameInfoDbConnected = connect<StateProps, NonNullable<unknown>, OwnProps, AppReduxStoreState>(
    state => {
        const databaseCardState = state.InfoDbCardState;
        const { commonDetails } = databaseCardState;

        return {
            commonDetails,
        };
    },
    {
        dispatchEditInfoDbThunk: EditDbListThunk.invoke,
    }
)(NameInfoDbClass);

export const NameInfoDb = withReduxForm(withFloatMessages(NameInfoDbConnected), {
    reduxFormName: 'InfoDbCard_EditName_Db',
    resetOnUnmount: true
});