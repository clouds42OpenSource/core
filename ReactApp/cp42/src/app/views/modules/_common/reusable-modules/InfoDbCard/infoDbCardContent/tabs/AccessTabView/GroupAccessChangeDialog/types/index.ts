export type TGroupAccessChangeDialog = {
    isOpen: boolean;
    onCancelClick: () => void;
};