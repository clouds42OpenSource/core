import { Box, Pagination } from '@mui/material';
import { contextAccountId } from 'app/api';
import { TAccountProfile } from 'app/api/endpoints/accountUsers/response';
import { FETCH_API } from 'app/api/useFetchApi';
import { REDUX_API } from 'app/api/useReduxApi';
import { configurationProfilesToRole } from 'app/common/functions';
import { getErrorMessage } from 'app/common/functions/getErrorMessage';
import { EMessageType, useAppDispatch, useAppSelector, useFloatMessages, useGetCurrency } from 'app/hooks';
import { COLORS } from 'app/utils';
import { Dialog } from 'app/views/components/controls/Dialog';
import { CheckBoxForm, DropDownListWithCheckboxes, TextBoxForm } from 'app/views/components/controls/forms';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { TextOut } from 'app/views/components/TextOut';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { DbStateLineAccessViews } from 'app/views/modules/_common/reusable-modules/InfoDbCard/infoDbCardContent/tabs/AccessTabView/DbStateLineAccess/DbStateLine';
import { TGroupAccessChangeDialog } from 'app/views/modules/_common/reusable-modules/InfoDbCard/infoDbCardContent/tabs/AccessTabView/GroupAccessChangeDialog/types';
import { BuyAccessDbDialogMessage } from 'app/views/modules/AccountManagement/InformationBases/views/Modal/BuyAccessDbDialogMessage';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { DatabaseAccessesDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/AccessInfoDbDataModel';
import { RequestKind } from 'core/requestSender/enums';
import React, { useEffect, useState } from 'react';

import style from './style.module.css';

const pageSize = 10;

const { TryPayAccesses, CalculateAccesses } = InterlayerApiProxy.getInfoDbCardApi();
const { changeDatabaseAccessOnDelimiters } = FETCH_API.ACCOUNT_USERS;
const { getGivenAccountProfilesList } = REDUX_API.ACCOUNT_USERS_REDUX;

export const GroupAccessChangeDialog = ({ isOpen, onCancelClick }: TGroupAccessChangeDialog) => {
    const { show } = useFloatMessages();
    const dispatch = useAppDispatch();
    const currency = useGetCurrency();

    const [isLoading, setIsLoading] = useState(false);
    const [searchUser, setSearchUser] = useState('');
    const [selectProfiles, setSelectProfiles] = useState<Omit<TAccountProfile, 'databases'>[]>([]);
    const [selectUsers, setSelectUsers] = useState<DatabaseAccessesDataModel[]>([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [totalSum, setTotalSum] = useState(0);
    const [buyAccessDialog, setBuyAccessDialog] = useState({
        isOpen: false,
        notEnoughMoney: 0,
        canUsePromisePayment: false
    });
    const [isFirstShowWarning, setIsFirstShowWarning] = useState(true);

    const { databaseAccesses, commonDetails } = useAppSelector(state => state.InfoDbCardState);
    const { AvailabilityInfoDb } = useAppSelector(state => state.InfoDbListState);
    const { accountProfilesListReducer: { data: profilesData } } = useAppSelector(state => state.AccountUsersState);

    const selectUser = (user: DatabaseAccessesDataModel) => {
        setSelectUsers(prev => prev.find(item => item.userId === user.userId) ? prev.filter(item => item.userId !== user.userId) : [...prev, user]);
    };

    const selectAllUsers = () => {
        if (selectUsers.length === databaseAccesses.length) {
            setSelectUsers([]);
        } else {
            setSelectUsers(databaseAccesses);
        }
    };

    const closeGroupAccessChangeDialog = () => {
        onCancelClick();
        setSelectUsers([]);
        setSearchUser('');
    };

    const currentPageHandler = (_: React.ChangeEvent<unknown>, page: number) => {
        setCurrentPage(page);
    };

    const changeAccess = async (isPromisePayment?: boolean) => {
        setIsLoading(true);
        closeGroupAccessChangeDialog();

        try {
            const tryPay = await TryPayAccesses(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, {
                accountId: contextAccountId(),
                billingServiceId: AvailabilityInfoDb.MyDatabaseServiceId,
                changedUsers: selectUsers.map(({ userId }) => {
                    return {
                        billingServiceTypeId: commonDetails.myDatabasesServiceTypeId,
                        sponsorship: {
                            i: !(databaseAccesses as DatabaseAccessesDataModel[]).map(item => item.userId).includes(userId),
                            label: '',
                            me: false
                        },
                        status: true,
                        subject: userId
                    };
                }),
                usePromisePayment: isPromisePayment ?? false
            });

            if (tryPay.success && tryPay.data) {
                if (!tryPay.data.isComplete) {
                    setBuyAccessDialog(prev => ({
                        isOpen: !prev.isOpen,
                        notEnoughMoney: tryPay.data?.notEnoughMoney ?? 0,
                        canUsePromisePayment: tryPay.data?.canUsePromisePayment ?? false,
                    }));

                    return;
                }

                const { v82Name, templateName, id } = commonDetails;

                if (profilesData) {
                    const { data: accessData } = await changeDatabaseAccessOnDelimiters(
                        {
                            permissions: selectUsers.map(({ userId }) => ({
                                'allow-backup': false,
                                profiles: selectProfiles,
                                'user-id': userId,
                                'application-id': id,
                            }))
                        },
                        selectUsers.map(({ userId }) => {
                            const internalUser = (databaseAccesses as DatabaseAccessesDataModel[]).find(accessInfo => accessInfo.userId === userId);

                            return {
                                databaseName: templateName,
                                databaseId: id,
                                v82Name,
                                userId: internalUser?.userId ?? userId,
                                userName: internalUser?.userLogin ?? '',
                                isExternal: !internalUser
                            };
                        })
                    );

                    if (accessData && accessData.results) {
                        accessData.results.forEach(statusResult => {
                            if (statusResult.status === 'failure') {
                                show(EMessageType.warning, statusResult.error ?? `Не удалось отредактировать права в базу ${ statusResult['application-id'] }`);
                            }
                        });
                    }

                    if (accessData && accessData.message) {
                        show(EMessageType.warning, accessData.message);
                    } else {
                        show(EMessageType.success, 'Изменение доступов будет выполнено в течение 1 минуты. Пожалуйста, подождите');
                    }

                    closeGroupAccessChangeDialog();
                    getGivenAccountProfilesList(dispatch, '', commonDetails.id);
                }
            } else {
                show(EMessageType.error, tryPay.message);
            }
        } catch (e) {
            show(EMessageType.error, getErrorMessage(e));
        }

        setIsLoading(false);
    };

    const closeBuyAccessDialog = () => {
        setBuyAccessDialog({
            isOpen: false,
            canUsePromisePayment: false,
            notEnoughMoney: 0
        });
    };

    useEffect(() => {
        (async () => {
            if (selectUsers.length) {
                const accessResponse = await CalculateAccesses(
                    RequestKind.SEND_BY_USER_ASYNCHRONOUSLY,
                    {
                        serviceTypesIdsList: [commonDetails.myDatabasesServiceTypeId],
                        accountUserDataModelsList: selectUsers.map(user => ({
                            UserId: user.userId,
                            HasAccess: true,
                            State: 4,
                            HasLicense: false
                        }))
                    }
                );

                setTotalSum(accessResponse.reduce((sum, access) => {
                    sum += access.accessCost;

                    return sum;
                }, 0));
            }
        })();
    }, [commonDetails.myDatabasesServiceTypeId, selectUsers]);

    return (
        <>
            { isLoading && <LoadingBounce /> }
            <Dialog
                title="Изменение прав доступа"
                isOpen={ isOpen }
                onCancelClick={ closeGroupAccessChangeDialog }
                buttons={ [
                    {
                        content: 'Отмена',
                        kind: 'default',
                        variant: 'contained',
                        onClick: closeGroupAccessChangeDialog
                    },
                    {
                        content: `Применить ${ totalSum ? `(${ totalSum } ${ currency }.)` : '' }`,
                        kind: 'success',
                        variant: 'contained',
                        onClick: () => changeAccess(false),
                        hiddenButton: selectUsers.length === 0
                    }
                ] }
                dialogVerticalAlign="center"
                dialogWidth="md"
            >
                <Box display="flex" flexDirection="column" gap="8px">
                    {
                        profilesData && (
                            <FormAndLabel label="Выбирите профили доступа" fullWidth={ true }>
                                <DropDownListWithCheckboxes
                                    noUpdateValueChangeHandler={ true }
                                    deepCompare={ true }
                                    hiddenSelectAll={ true }
                                    renderValueIsChip={ true }
                                    items={
                                        profilesData.flatMap(profile => {
                                            if (profile.code === commonDetails.configurationCode) {
                                                return profile.profiles.map(pr => ({
                                                    text: pr.name,
                                                    value: pr.id,
                                                    checked: !!selectProfiles.find(item => item.id === pr.id)
                                                }));
                                            }

                                            return [];
                                        })
                                    }
                                    label=""
                                    formName="profiles"
                                    hiddenAdditionalElement={ true }
                                    onValueChange={ (_, items) => setSelectProfiles(prevProfiles => {
                                        const currentProfiles = profilesData
                                            .find(profile => profile.code === commonDetails.configurationCode)?.profiles ?? [];
                                        const { adminRole, userRole } = configurationProfilesToRole(currentProfiles, items);

                                        if (items.length > 1 && isFirstShowWarning && adminRole.length && userRole.length) {
                                            show(EMessageType.info, 'Установка профиля доступа "Администратор" одновременно с другими профилями через личный кабинет не поддерживается');
                                            setIsFirstShowWarning(false);
                                        }

                                        if (prevProfiles && prevProfiles.some(p => p.admin)) {
                                            return (userRole.length ? userRole : adminRole).filter(pr => items.find(item => item.value === pr.id));
                                        }

                                        return (adminRole.length ? adminRole : userRole).filter(pr => items.find(item => item.value === pr.id));
                                    }) }
                                />
                            </FormAndLabel>
                        )
                    }
                    <FormAndLabel label="Поиск" fullWidth={ true }>
                        <TextBoxForm
                            formName="searchLineUser"
                            value={ searchUser }
                            onValueChange={ (_, value) => setSearchUser(value) }
                            placeholder="Поиск пользователя по логину, эл. почте или ФИО (отдельно)"
                            fullWidth={ true }
                        />
                    </FormAndLabel>
                    <CommonTableWithFilter
                        uniqueContextProviderStateId="GroupAccessChangeContextProviderStateId"
                        tableProps={ {
                            dataset: (
                                searchUser
                                    ? (databaseAccesses as DatabaseAccessesDataModel[]).filter(item =>
                                        item.userLogin?.toLowerCase().includes(searchUser.toLowerCase()) ||
                                        item.userEmail?.toLowerCase().includes(searchUser.toLowerCase()) ||
                                        item.userFullName?.toLowerCase().includes(searchUser.toLowerCase())
                                    )
                                    : databaseAccesses as DatabaseAccessesDataModel[]
                            ).slice(pageSize * (currentPage - 1), pageSize * currentPage),
                            keyFieldName: 'userId',
                            fieldsView: {
                                userId: {
                                    caption: <CheckBoxForm
                                        formName="isDefault"
                                        label=""
                                        isChecked={ databaseAccesses.length === selectUsers.length }
                                        onValueChange={ selectAllUsers }
                                        className={ style.check }
                                    />,
                                    fieldContentNoWrap: false,
                                    fieldWidth: '3%',
                                    format: (value: string, item) =>
                                        <CheckBoxForm
                                            formName={ value }
                                            label=""
                                            isChecked={ !!selectUsers.find(users => users.userId === value) }
                                            onValueChange={ () => selectUser(item) }
                                            className={ style.check }
                                        />
                                },
                                userLogin: {
                                    caption: 'Логин (ФИО)',
                                    fieldContentNoWrap: false,
                                    isSortable: false,
                                    format: (value: string, item: DatabaseAccessesDataModel) =>
                                        <Box>
                                            <TextOut inDiv={ true }>{ value }</TextOut>
                                            <TextOut inDiv={ true }>{ item.userFullName ? `(${ item.userFullName })` : '' }</TextOut>
                                            <DbStateLineAccessViews extensionAccess={ item.state } />
                                        </Box>
                                },
                                userEmail: {
                                    caption: 'Эл. адрес',
                                    fieldContentNoWrap: false,
                                    isSortable: false,
                                    format: (value: string, item: DatabaseAccessesDataModel) =>
                                        <>
                                            <TextOut inDiv={ true } fontSize={ 13 }>
                                                { value }
                                            </TextOut>
                                            {
                                                (Object.hasOwn(item, 'accessCost') || item.isExternalAccess) && (
                                                    <TextOut inDiv={ true } style={ { color: COLORS.error, fontSize: '10px' } }>
                                                        { item.accountInfo }
                                                    </TextOut>
                                                )
                                            }
                                        </>
                                }
                            }
                        } }
                    />
                    <Box gap="5px" justifyContent="flex-end" display="flex" marginTop="10px">
                        <Pagination
                            page={ currentPage }
                            count={
                                Math.ceil((
                                    searchUser
                                        ? (databaseAccesses as DatabaseAccessesDataModel[]).filter(item =>
                                            item.userLogin?.toLowerCase().includes(searchUser.toLowerCase()) ||
                                            item.userEmail?.toLowerCase().includes(searchUser.toLowerCase()) ||
                                            item.userFullName?.toLowerCase().includes(searchUser.toLowerCase())
                                        )
                                        : databaseAccesses as DatabaseAccessesDataModel[]
                                ).length / pageSize)
                            }
                            onChange={ currentPageHandler }
                        />
                    </Box>
                </Box>
            </Dialog>
            <BuyAccessDbDialogMessage
                loading={ isLoading }
                usePromise={ true }
                isOpen={ buyAccessDialog.isOpen }
                onNoClick={ closeBuyAccessDialog }
                onYesClick={ () => changeAccess(true) }
                notEnoughMoney={ buyAccessDialog.notEnoughMoney }
                canUsePromisePayment={ buyAccessDialog.canUsePromisePayment }
            />
        </>
    );
};