import { DatabaseState, DbPublishState } from 'app/common/enums';

/**
 * Узнать, находится ли база во временном процессе
 */
export function defineInfoDbInProccessing(DbState: number, PublishState: number, EndingSessions: boolean) {
    return DatabaseState.Attaching === DbState ||
        DatabaseState.ProcessingSupport === DbState ||
        DatabaseState.TransferArchive === DbState ||
        DatabaseState.RestoringFromTomb === DbState ||
        DatabaseState.DelitingToTomb === DbState ||
        DatabaseState.DetachingToTomb === DbState ||
        DbPublishState.PendingPublication === PublishState ||
        DbPublishState.PendingUnpublication === PublishState ||
        DbPublishState.RestartingPool === PublishState ||
        DatabaseState.NewItem === DbState ||
        EndingSessions;
}