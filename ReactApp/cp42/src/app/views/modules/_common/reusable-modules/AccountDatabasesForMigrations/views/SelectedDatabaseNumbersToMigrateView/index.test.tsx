import { render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { SelectedDatabaseNumbersToMigrateView } from 'app/views/modules/_common/reusable-modules/AccountDatabasesForMigrations/views/SelectedDatabaseNumbersToMigrateView';
import { SelectedDatabasesToMigrateContextProvider } from 'app/views/modules/_common/reusable-modules/AccountDatabasesForMigrations/context/SelectedDatabasesToMigrateContextProvider';
import { SnackbarProvider } from 'notistack';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';

describe('<SelectedDatabaseNumbersToMigrateView />', () => {
    const mockStore = configureStore();
    const initialState = {
        AccountDatabasesForMigrationState: {
            accountDatabasesMigrationInfo: {
                numberOfDatabasesToMigrate: 1
            }
        }
    };

    it('renders empty', () => {
        const { container } = render(
            <SnackbarProvider>
                <Provider store={ mockStore(initialState) }>
                    <SelectedDatabasesToMigrateContextProvider>
                        <SelectedDatabaseNumbersToMigrateView />
                    </SelectedDatabasesToMigrateContextProvider>
                </Provider>
            </SnackbarProvider>
        );
        expect(container.firstChild).toBeNull();
    });
});