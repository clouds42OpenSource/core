import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import userEvent from '@testing-library/user-event';
import { CopyAccountDatabasePathButton } from 'app/views/modules/_common/reusable-modules/AccountDatabasesForMigrations/views/_buttons/CopyAccountDatabasePathButton';
import { SnackbarProvider } from 'notistack';

describe('<CopyAccountDatabasePathButton />', () => {
    let container: Element;

    beforeEach(() => {
        container = render(
            <SnackbarProvider>
                <CopyAccountDatabasePathButton inputElementId="test_id" />
            </SnackbarProvider>
        ).container;
    });

    it('renders', () => {
        expect(screen.getByTestId(/FileCopyOutlinedIcon/i)).toBeInTheDocument();
    });

    it('renders float message error', async () => {
        await userEvent.click(container.querySelector('[type=button]')!);
        expect(screen.getByText(/Не удалось скопировать путь в буфер обмена/i)).toBeInTheDocument();
    });
});