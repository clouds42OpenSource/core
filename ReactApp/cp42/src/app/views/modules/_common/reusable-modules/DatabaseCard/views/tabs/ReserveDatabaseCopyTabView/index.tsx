import { Box, ClickAwayListener, Fade, Popper } from '@mui/material';
import { AppRoutes } from 'app/AppRoutes';
import { DatabaseCardReducerState } from 'app/modules/databaseCard/store/reducers';
import { AppReduxStoreState } from 'app/redux/types';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { ContainedButton } from 'app/views/components/controls/Button';
import { TextOut } from 'app/views/components/TextOut';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { CopyAccountDatabasePathButton } from 'app/views/modules/_common/reusable-modules/AccountDatabasesForMigrations/views/_buttons/CopyAccountDatabasePathButton';
import { ArchivalInfoDbListFilterForm } from 'app/views/modules/_common/reusable-modules/InfoDbCard/infoDbCardContent/tabs/ArchivalСopiesTabView/ArchivalInfoDbListFilterFormView';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { DatabaseCardBackupInfoDataModel } from 'app/web/InterlayerApiProxy/DatabaseCardApiProxy/receiveDatabaseCard/data-models';
import { AccountUserGroup, DatabaseState } from 'app/common/enums';
import { DateUtility, NumberUtility } from 'app/utils';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { BackupsItem } from 'app/web/InterlayerApiProxy/MsBackupsApiProxy/getBackups';
import { RequestKind } from 'core/requestSender/enums';
import React from 'react';
import cn from 'classnames';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import css from 'app/views/modules/_common/reusable-modules/InfoDbCard/styles.module.css';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';

/**
 * Свойства этого компонента
 */
type OwnProps = FloatMessageProps;

type StateProps = {
    database: DatabaseCardReducerState;
    backups: Array<DatabaseCardBackupInfoDataModel>;
    databaseState: DatabaseState;
    databaseCardHasReceived: boolean;
    currentUserGroups: AccountUserGroup[];
};

type OwnState = {
    anchorEl: HTMLElement | null;
    open: string;
    openModal: boolean;
    msBackupsList: BackupsItem[]
};

type AllProps = OwnProps & StateProps;

export class ReserveDatabaseCopyTabViewClass extends React.Component<AllProps, OwnState> {
    public constructor(props: AllProps) {
        super(props);
        this.state = {
            anchorEl: null,
            open: '',
            openModal: false,
            msBackupsList: []
        };

        this.onClickAddBase = this.onClickAddBase.bind(this);
        this.openDialogMessage = this.openDialogMessage.bind(this);
        this.onClickAway = this.onClickAway.bind(this);
        this.setDataIsDbOnDelimiters = this.setDataIsDbOnDelimiters.bind(this);
        this.getDownloadFile = this.getDownloadFile.bind(this);
        this.renderArchivalDb = this.renderArchivalDb.bind(this);
        this.renderArchivalDbOnDelimiters = this.renderArchivalDbOnDelimiters.bind(this);
        this.download = this.download.bind(this);
    }

    public componentDidUpdate(prevProps: Readonly<AllProps>) {
        if (prevProps.databaseCardHasReceived !== this.props.databaseCardHasReceived) {
            if (this.props.databaseCardHasReceived && this.props.database.commonDetails.isDbOnDelimiters) {
                const api = InterlayerApiProxy.getMsBackupsApiProxy();
                try {
                    api.getBackups(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, {
                        applicationId: this.props.database.commonDetails.id
                    }).then(response => this.setState({ msBackupsList: response.backups }));
                } catch (er) {
                    console.log(er);
                }
            }
        }
    }

    private onClickAddBase(event: React.MouseEvent<HTMLElement>, value: string) {
        this.state.anchorEl
            ? this.setState({
                anchorEl: null,
                open: ''
            })
            : this.setState({
                anchorEl: event.currentTarget,
                open: value
            });
    }

    private onClickAway() {
        this.setState({
            anchorEl: null,
            open: ''
        });
    }

    private async getDownloadFile(id: string) {
        try {
            this.setState({
                open: ''
            });
            await this.download(id);
        } catch (er) {
            console.log(er);
        }
    }

    private setDataIsDbOnDelimiters(sortData: BackupsItem[]) {
        this.setState({
            msBackupsList: sortData
        });
    }

    private async download(id: string) {
        const api = InterlayerApiProxy.getMsBackupsApiProxy();

        return await api.getDownloadFiles(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, {
            fileId: id
        }).then(response => {
            if (response.status === 204) {
                this.props.floatMessage.show(MessageType.Info, `Пожалуйста, подождите. Файл подготавливается. Примерное время ожидания ${ response.header.get('Retry-After') } секунд.`);
                setTimeout(this.download, parseInt(`${ response.header.get('Retry-After') }000`, 10), id);
            }
            if (response.status === 200) {
                window.open(response.url);
            }
        });
    }

    private openDialogMessage() {
        this.setState(prevState => ({
            openModal: !prevState.openModal
        }));
    }

    private renderArchivalDb() {
        return (
            <>
                <CommonTableWithFilter
                    uniqueContextProviderStateId="AccessInfoDbListContextProviderStateId"
                    filterProps={ {
                        getFilterContentView: () => <ArchivalInfoDbListFilterForm
                            databaseId={ this.props.database.commonDetails.id }
                            isDbOnDelimiters={ false }
                            setData={ () => { /* empty */} }
                            status={ this.props.database.commonDetails.state !== 15 }
                        />
                    } }
                    tableProps={ {
                        dataset: this.props.database.backups || [],
                        keyFieldName: 'id',
                        fieldsView: {
                            createDateTime: {
                                caption: 'Дата и время',
                                fieldContentNoWrap: false,
                                fieldWidth: '15%',
                                format: (value: Date) => {
                                    return DateUtility.dateToDayMonthYearHourMinute(value);
                                }
                            },
                            backupPath: {
                                caption: 'Путь к архиву',
                                fieldContentNoWrap: false,
                                fieldWidth: '40%',
                                format: (value, item) => {
                                    return this.props.currentUserGroups.find(role => role === 5 || role === 6 || role === 9)
                                        ? (
                                            <div style={ { display: 'flex', alignItems: 'center' } }>
                                                <input type="text" readOnly={ true } className={ cn(css['input-backup-path']) } id={ `input${ item.id }` } name="one" value={ value } />
                                                <CopyAccountDatabasePathButton inputElementId={ `input${ item.id }` } />
                                            </div>
                                        )
                                        : <TextOut>Облачное хранилище</TextOut>;
                                }
                            },
                            initiator: {
                                caption: 'Инициатор',
                                fieldContentNoWrap: false,
                                fieldWidth: '10%',
                            },
                            eventTrigger: {
                                caption: 'Триггер',
                                fieldContentNoWrap: false,
                                fieldWidth: '25%',
                            },
                            id: {
                                caption: '',
                                fieldContentNoWrap: false,
                                fieldWidth: '10%',
                                format: (value, item) => {
                                    return (
                                        <div className={ cn(css.root) }>
                                            <ContainedButton
                                                key={ value }
                                                aria-describedby={ value }
                                                kind={ this.state.open === value ? 'primary' : 'success' }
                                                className={ cn(css['archival-btn']) }
                                                onClick={ (event: React.MouseEvent<HTMLElement>) => this.onClickAddBase(event, value) }
                                            >
                                                <Box sx={ { display: 'flex', alignItems: 'center', justifyContent: 'center' } }>
                                                    <i className="fa fa-bars" />
                                                </Box>
                                            </ContainedButton>

                                            <Popper
                                                placement="bottom-end"
                                                className={ cn(css.dropdown) }
                                                id={ value }
                                                open={ this.state.open === value }
                                                anchorEl={ this.state.anchorEl }
                                                transition={ true }
                                                hidden={ this.state.openModal }
                                            >
                                                { ({ TransitionProps }) => (
                                                    <ClickAwayListener onClickAway={ this.onClickAway }>
                                                        <Fade { ...TransitionProps } timeout={ 0 }>
                                                            <Box>
                                                                <>
                                                                    {
                                                                        this.props.database.commonDetails.state !== 15 && (
                                                                            <div>
                                                                                <Link
                                                                                    className={ cn(css.linkDb) }
                                                                                    to={ {
                                                                                        pathname: AppRoutes.accountManagement.createAccountDatabase.loadingDatabase,
                                                                                        state: {
                                                                                            fileId: item.id,
                                                                                            restoreType: '1'
                                                                                        }
                                                                                    } }
                                                                                >
                                                                                    <TextOut inDiv={ true } fontSize={ 14 } fontWeight={ 400 }>
                                                                                        Восстановить в новую
                                                                                    </TextOut>
                                                                                </Link>
                                                                                <Link
                                                                                    className={ cn(css.linkDb) }
                                                                                    to={ {
                                                                                        pathname: AppRoutes.accountManagement.createAccountDatabase.loadingDatabase,
                                                                                        state: {
                                                                                            fileId: item.id,
                                                                                            restoreType: '0'
                                                                                        }
                                                                                    } }
                                                                                >
                                                                                    <TextOut inDiv={ true } fontSize={ 14 } fontWeight={ 400 }>
                                                                                        Восстановить с заменой
                                                                                    </TextOut>
                                                                                </Link>
                                                                            </div>
                                                                        )
                                                                    }
                                                                    {
                                                                        Number(item.eventTrigger) !== 4 && (
                                                                            <a target="_blank" rel="noreferrer" className={ cn(css.linkDb) } href={ `${ item.backupPath }` }>
                                                                                <TextOut inDiv={ true } fontSize={ 14 } fontWeight={ 400 }>
                                                                                    Скачать
                                                                                </TextOut>
                                                                            </a>
                                                                        )
                                                                    }
                                                                </>
                                                            </Box>
                                                        </Fade>
                                                    </ClickAwayListener>
                                                ) }
                                            </Popper>
                                        </div>
                                    );
                                }
                            },
                        }
                    } }
                />
                { this.props.database.backups.length
                    ? (
                        <TextOut style={ { marginTop: '15px' } } inDiv={ true } fontSize={ 13 } fontWeight={ 400 }>
                            Записей: { this.props.database.backups.length }
                        </TextOut>
                    )
                    : null
                }
            </>
        );
    }

    private renderArchivalDbOnDelimiters() {
        return (
            <CommonTableWithFilter
                uniqueContextProviderStateId="AccessInfoDbListOnDelimitersContextProviderStateId"
                filterProps={ {
                    getFilterContentView: () => <ArchivalInfoDbListFilterForm
                        databaseId={ this.props.database.commonDetails.id }
                        isDbOnDelimiters={ true }
                        setData={ this.setDataIsDbOnDelimiters }
                        status={ this.props.database.commonDetails.state !== 15 }
                    />
                } }
                tableProps={ {
                    dataset: this.state.msBackupsList,
                    keyFieldName: 'id',
                    fieldsView: {
                        date: {
                            caption: 'Дата и время',
                            fieldContentNoWrap: false,
                            fieldWidth: '15%',
                            format: value => {
                                return DateUtility.dateToDayMonthYearHourMinute(new Date(value));
                            }
                        },
                        appVersion: {
                            caption: 'Версия конфигурции'
                        },
                        fileSize: {
                            caption: 'Размер',
                            format: value => {
                                return NumberUtility.bytesToMegabytes(value);
                            }
                        },
                        isEternal: {
                            caption: 'Вид',
                            format: value => {
                                return value ? 'Ручная' : 'Регламентная';
                            }
                        },
                        id: {
                            caption: '',
                            fieldContentNoWrap: false,
                            fieldWidth: '10%',
                            format: (value: string, item) => {
                                return (
                                    <div className={ cn(css.root) }>
                                        <ContainedButton
                                            key={ value }
                                            aria-describedby={ value }
                                            kind={ this.state.open === value ? 'primary' : 'success' }
                                            className={ cn(css['archival-btn']) }
                                            onClick={ (event: React.MouseEvent<HTMLElement>) => this.onClickAddBase(event, value) }
                                        >
                                            <Box sx={ { display: 'flex', alignItems: 'center', justifyContent: 'center' } }>
                                                <i className="fa fa-bars" />
                                            </Box>
                                        </ContainedButton>

                                        <Popper
                                            placement="bottom-end"
                                            className={ cn(css.dropdown) }
                                            id={ value }
                                            open={ this.state.open === value }
                                            anchorEl={ this.state.anchorEl }
                                            transition={ true }
                                            hidden={ this.state.openModal }
                                        >
                                            { ({ TransitionProps }) => (
                                                <ClickAwayListener onClickAway={ this.onClickAway }>
                                                    <Fade { ...TransitionProps } timeout={ 0 }>
                                                        <Box>
                                                            {
                                                                this.props.database.commonDetails.state !== 15 && item.fileId && (
                                                                    <Link
                                                                        to={ {
                                                                            pathname: AppRoutes.accountManagement.createAccountDatabase.loadingDatabase,
                                                                            state: {
                                                                                isZip: true,
                                                                                fileId: item.fileId,
                                                                                caption: this.props.database.commonDetails.caption,
                                                                                date: item.date
                                                                            }
                                                                        } }
                                                                        className={ cn(css.linkDb) }
                                                                    >
                                                                        <TextOut inDiv={ true } fontSize={ 14 } fontWeight={ 400 }>
                                                                            Восстановить
                                                                        </TextOut>
                                                                    </Link>
                                                                )
                                                            }
                                                            <div
                                                                onClick={
                                                                    item.fileId !== ''
                                                                        ? () => this.getDownloadFile(item.fileId)
                                                                        : () => window.open(item.eternalBackupUrl, '_blank')
                                                                }
                                                                className={ cn(css.linkDb) }
                                                            >
                                                                <TextOut inDiv={ true } fontSize={ 14 } fontWeight={ 400 }>
                                                                    Скачать
                                                                </TextOut>
                                                            </div>
                                                        </Box>
                                                    </Fade>
                                                </ClickAwayListener>
                                            ) }
                                        </Popper>
                                    </div>
                                );
                            }
                        }
                    }
                } }
            />
        );
    }

    public render() {
        const { databaseCardHasReceived, database: { commonDetails: { isDbOnDelimiters } } } = this.props;

        if (databaseCardHasReceived) {
            if (!isDbOnDelimiters) {
                return this.renderArchivalDb();
            }

            return this.renderArchivalDbOnDelimiters();
        }

        return null;
    }
}

const ReserveDatabaseCopyTabViewConnected = connect<StateProps, NonNullable<unknown>, OwnProps, AppReduxStoreState>(
    state => {
        const databaseCardState = state.DatabaseCardState;
        const { backups } = databaseCardState;
        const databaseState = databaseCardState.commonDetails.state;
        const databaseCardHasReceived = databaseCardState.hasSuccessFor.hasDatabaseCardReceived;
        const { currentUserGroups } = state.Global.getCurrentSessionSettingsReducer.settings.currentContextInfo;

        return {
            database: databaseCardState,
            backups,
            databaseState,
            databaseCardHasReceived,
            currentUserGroups
        };
    }
)(ReserveDatabaseCopyTabViewClass);

export const ReserveDatabaseCopyTabView = withFloatMessages(ReserveDatabaseCopyTabViewConnected);