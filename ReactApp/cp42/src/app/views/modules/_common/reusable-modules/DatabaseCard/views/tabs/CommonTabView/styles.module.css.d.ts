declare const styles: {
    readonly 'main': string;
    readonly 'publish-url': string;
    readonly 'col-wrapper': string;
};
export = styles;