import { AppRoutes } from 'app/AppRoutes';
import { DatabaseState } from 'app/common/enums';
import { useAppSelector } from 'app/hooks';
import { OutlinedButton } from 'app/views/components/controls/Button';
import { InfoDbCardPermissionsEnum } from 'app/web/common/enums/InfoDbCardPermissionsEnum';
import React from 'react';
import { Link } from 'react-router-dom';

export const RestoreInfoDb = () => {
    const { getUserPermissionsReducer: { permissions } } = useAppSelector(state => state.Global);
    const { informationBase } = useAppSelector(state => state.InformationBases.informationBasesList);
    const { actualDatabaseBackupId, state, isDbOnDelimiters } = informationBase?.database ?? {};

    if (!(
        permissions.includes(InfoDbCardPermissionsEnum.AccountDatabase_Restore) &&
        state === DatabaseState.DetachedToTomb &&
        !isDbOnDelimiters &&
        actualDatabaseBackupId
    )) {
        return null;
    }

    return (
        <Link
            to={ {
                pathname: AppRoutes.accountManagement.createAccountDatabase.loadingDatabase,
                state: { fileId: actualDatabaseBackupId, restoreType: '1' }
            } }
        >
            <OutlinedButton kind="success">
                <i className="fa fa-reply p-r-4" /> &nbsp; Восстановить
            </OutlinedButton>
        </Link>
    );
};