import { render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { CommandsView } from 'app/views/modules/_common/reusable-modules/AccountCard/views/CommandsView/';

describe('renders', () => {
    const initialState = {
        AccountCardState: {
            accountInfo: {
                id: 'test_id'
            },
            buttonsVisibility: true,
        }
    } as const;

    it('renders', () => {
        const { container } = render(<Provider store={ configureStore()(initialState) }><CommandsView /></Provider>);
        expect(container.firstChild).toBeNull();
    });
});