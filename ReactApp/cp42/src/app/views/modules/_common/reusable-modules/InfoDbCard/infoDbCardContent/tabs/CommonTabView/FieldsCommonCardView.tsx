import { Box, Grid } from '@mui/material';
import { AccountUserGroup, DatabaseState } from 'app/common/enums';
import { hasComponentChangesFor } from 'app/common/functions';
import { restoreModelInfoDbName } from 'app/common/functions/restoreModelInfoDbName';
import { DateUtility, NumberUtility } from 'app/utils';
import CustomizedSwitches from 'app/views/components/controls/Switch/views/SwitchViews';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { TextOut } from 'app/views/components/TextOut';
import { ReadOnlyKeyValueView } from 'app/views/modules/_common/components/ReadOnlyKeyValueView';
import { InfoDbCaptionView, InfoDbDestributionType, InfoDbPlatformType, InfoDbStateView, InfoDbTemplateView, RestoreModel } from 'app/views/modules/_common/reusable-modules/InfoDbCard/infoDbCardContent/tabs/CommonTabView/partial-elements';
import { visibleWebServices } from 'app/views/modules/AccountManagement/InformationBases/Permissions/CommonPage';
import {
    InfoDbDestributionPlatformPermissions,
    InfoDbPathToBasePermissions,
    InfoDbPlatformPermissions,
    InfoDbTemplatePermissions,
    InfoDbWebLinkPermissions
} from 'app/views/modules/AccountManagement/InformationBases/Permissions/InfoDbCardCommonP';
import { InfoDbCardEditDatabaseDataForm } from 'app/views/modules/AccountManagement/InformationBases/types/InfiDbCardEditDatabaseDataForm';
import { GetAvailabilityInfoDb } from 'app/web/api/InfoDbProxy/responce-dto/GetAvailabilityInfoDb';
import { InfoDbCardPermissionsEnum } from 'app/web/common/enums/InfoDbCardPermissionsEnum';
import { InfoDbListItemDataModel } from 'app/web/InterlayerApiProxy/InfoDbApiProxy/receiveDatabaseList/data-models';
import { InfoDbCardAccountDatabaseInfoDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/InfoDbCardAccountDatabaseInfoDataModel';
import { RightsInfoDbCardDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getRightsInfoDbCard/data-models/InfoDbCardAccountDatabaseInfoDataModel';
import cn from 'classnames';
import React from 'react';
import css from './styles.module.css';

type OwnProps = {
    onChangeField: () => void;
    reduxForm: InfoDbCardEditDatabaseDataForm;
    commonDetails: InfoDbCardAccountDatabaseInfoDataModel;
    permissions: RightsInfoDbCardDataModel;
    item?: InfoDbListItemDataModel | null;
    changeWebServices: boolean;
    onChangeV82Name: () => void;
    onValueChangeWebServices: () => void;
    errorV82Name: string;
    availability: GetAvailabilityInfoDb;
    onValueChange: <TValue>(fieldName: string, newValue: TValue, prevValue: TValue) => void;
    isOther: boolean;
    currentUserGroups: AccountUserGroup[];
    isAdmin?: boolean;
};

export class FieldsCommonCardView extends React.Component<OwnProps> {
    public shouldComponentUpdate(nextProps: OwnProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private mapArraydistributionTypes() {
        const result = this.props.commonDetails.alpha83Version !== undefined
            ? [this.props.commonDetails.stable83Version, this.props.commonDetails.alpha83Version]
            : [this.props.commonDetails.stable82Version];

        return result.map((item: string, index: number) => {
            return {
                key: index,
                value: item
            };
        });
    }

    public render() {
        const platformTypes = this.props.commonDetails.commonDataForWorkingWithDB?.AvailablePlatformTypes.map(i => +i.key) ?? [];
        const { reduxForm } = this.props;

        return (
            <div className={ cn(css.main) }>
                { this.props.changeWebServices && <LoadingBounce /> }
                <Grid container={ true } spacing={ { xs: 1, sm: 2 } }>
                    <Grid className={ css['col-wrapper'] } item={ true } xs={ 12 } sm={ 6 } md={ 6 }>
                        {
                            this.props.commonDetails.configurationName && this.props.isAdmin
                                ? (
                                    <ReadOnlyKeyValueView
                                        label="Конфигурация:"
                                        text={ this.props.commonDetails.configurationName }
                                    />
                                )
                                : null
                        }
                        {
                            (this.props.currentUserGroups.includes(AccountUserGroup.Hotline) || this.props.currentUserGroups.includes(AccountUserGroup.CloudAdmin)) && (
                                <InfoDbTemplateView
                                    data-testid="InfoDbTemplate"
                                    isInEditMode={ InfoDbTemplatePermissions(this.props.commonDetails.isDbOnDelimiters, this.props.permissions) && this.props.item?.state !== 15 }
                                    formName="templateId"
                                    label="Шаблон:"
                                    templates={ this.props.commonDetails?.commonDataForWorkingWithDB?.AvailableDbTemplates }
                                    readOnlyDatabaseTemplateName={ this.props.commonDetails.templateName }
                                    readOnlyDatabaseTemplateId={ this.props.commonDetails.dbTemplate }
                                    editDatabaseTemplateId={ this.props.commonDetails.dbTemplate }
                                    onValueChange={ this.props.onValueChange }
                                />
                            )
                        }
                        <ReadOnlyKeyValueView
                            label="Тип:"
                            text={ this.props.commonDetails.isFile ? 'Файловая' : 'Серверная' }
                        />
                        <InfoDbPlatformType
                            onChangeField={ this.props.onChangeField }
                            commonDetails={ this.props.commonDetails }
                            isInEditMode={ InfoDbPlatformPermissions(this.props.commonDetails, this.props.permissions) }
                            formName="platformType"
                            label="Платформа:"
                            readOnlyPlatformType={ this.props.commonDetails.platformType }
                            editPlatformType={ reduxForm.platformType }
                            platformTypes={ platformTypes }
                            isOther={ this.props.isOther }
                        />
                        <InfoDbDestributionType
                            onChangeField={ this.props.onChangeField }
                            commonDetails={ this.props.commonDetails }
                            isInEditMode={ InfoDbDestributionPlatformPermissions(this.props.commonDetails, this.props.permissions) && this.props.item?.state !== 15 }
                            formName="distributionType"
                            label="Релиз платформы:"
                            stable82Version={ this.props.commonDetails.stable82Version }
                            stable83Version={ this.props.commonDetails.stable83Version }
                            alpha83Version={ this.props.commonDetails.alpha83Version }
                            readOnlyDistributionType={ this.props.commonDetails.distributionType }
                            editDistributionType={ +this.props.commonDetails.distributionType }
                            platformType={ this.props.commonDetails.platformType }
                            distributionTypes={ this.mapArraydistributionTypes() }
                            isOther={ this.props.isOther }
                        />
                        {
                            !this.props.permissions.includes(InfoDbCardPermissionsEnum.AccountDatabases_EditFull) && (
                                <ReadOnlyKeyValueView
                                    label="Номер релиза:"
                                    text={ this.props.commonDetails.version }
                                />
                            )
                        }
                        {
                            InfoDbWebLinkPermissions(this.props.commonDetails, this.props.permissions, this.props.availability, this.props.item?.needShowWebLink || !!this.props.commonDetails.webPublishPath) || this.props.isAdmin
                                ? (
                                    <ReadOnlyKeyValueView
                                        label="Веб ссылка"
                                        text={
                                            <a
                                                className={ cn(css['publish-url'], 'text-link') }
                                                rel="noopener noreferrer"
                                                href={ this.props.commonDetails.webPublishPath || '' }
                                                target="_blank"
                                            >
                                                { this.props.commonDetails.webPublishPath }
                                            </a>
                                        }
                                    />
                                )
                                : null
                        }
                        {
                            InfoDbPathToBasePermissions(this.props.commonDetails)
                                ? (
                                    <ReadOnlyKeyValueView
                                        label="Путь к базе"
                                        text={ this.props.commonDetails.databaseConnectionPath }
                                    />
                                )
                                : null
                        }
                        {
                            this.props.commonDetails.sizeInMb
                                ? (
                                    <ReadOnlyKeyValueView
                                        label="Размер"
                                        text={
                                            `${ NumberUtility.megabytesToClassicFormat('', this.props.commonDetails.sizeInMb) } на (${ DateUtility.getDateByFormat(new Date(this.props.commonDetails.calculateSizeDateTime), 'dd.MM.yyyy') })`
                                        }
                                    />
                                )
                                : null
                        }
                        {
                            !this.props.permissions.includes(InfoDbCardPermissionsEnum.AccountDatabases_EditFull)
                                ? (
                                    <ReadOnlyKeyValueView
                                        label="Номер"
                                        text={ this.props.commonDetails.v82Name }
                                    />
                                )
                                : null
                        }
                        {
                            this.props.isAdmin && this.props.commonDetails.lastEditedDateTime
                                ? (
                                    <ReadOnlyKeyValueView
                                        label="Изменена:"
                                        text={ DateUtility.dateToDayMonthYear(new Date(this.props.commonDetails.lastEditedDateTime)) }
                                    />
                                )
                                : null
                        }
                    </Grid>
                    {
                        this.props.permissions.includes(InfoDbCardPermissionsEnum.AccountDatabases_EditFull)
                            ? (
                                <Grid className={ css['col-wrapper'] } item={ true } xs={ 12 } sm={ 6 } md={ 6 }>
                                    {
                                        visibleWebServices(this.props.item?.isDbOnDelimiters ?? this.props.commonDetails.isDbOnDelimiters, this.props.permissions) && this.props.item?.state !== 15
                                            ? (
                                                <ReadOnlyKeyValueView
                                                    label="Опубликованы веб сервисы:"
                                                    text={
                                                        <Box className={ cn(css.publish_web) }>
                                                            <CustomizedSwitches
                                                                checked={ this.props.commonDetails.usedWebServices }
                                                                onChange={ this.props.onValueChangeWebServices }
                                                            />
                                                        </Box>
                                                    }
                                                />
                                            )
                                            : null
                                    }
                                    <Grid container={ true } spacing={ 2 }>
                                        {
                                            this.props.permissions.includes(InfoDbCardPermissionsEnum.AccountDatabases_EditPlatform)
                                                ? (
                                                    <>
                                                        <Grid item={ true } xs={ 12 } sm={ 12 } md={ 4 }>
                                                            <ReadOnlyKeyValueView
                                                                label="Дата создания:"
                                                                text={ DateUtility.dateToDayMonthYear(this.props.commonDetails.creationDate) }
                                                            />
                                                            {
                                                                this.props.isAdmin && this.props.commonDetails.backupDate && (
                                                                    <ReadOnlyKeyValueView
                                                                        label="Дата бекапа:"
                                                                        text={ DateUtility.dateToDayMonthYear(new Date(this.props.commonDetails.backupDate)) }
                                                                    />
                                                                )
                                                            }
                                                        </Grid>
                                                        <Grid item={ true } xs={ 12 } sm={ 12 } md={ 6 }>
                                                            <Box onBlur={ this.props.onChangeV82Name }>
                                                                <InfoDbCaptionView
                                                                    formName="v82Name"
                                                                    label="Номер:"
                                                                    readOnlyDatabaseCaption={ this.props.commonDetails.v82Name }
                                                                    editDatabaseCaption={ this.props.reduxForm.v82Name }
                                                                    isInEditMode={ this.props.item?.state !== 15 }
                                                                    onValueChange={ this.props.onValueChange }
                                                                />
                                                                {
                                                                    this.props.errorV82Name
                                                                        ? (
                                                                            <TextOut fontSize={ 14 } fontWeight={ 400 } className={ cn(css.errorUser) }>
                                                                                { this.props.errorV82Name }
                                                                            </TextOut>
                                                                        )
                                                                        : null
                                                                }
                                                            </Box>
                                                        </Grid>
                                                    </>
                                                )
                                                : null
                                        }
                                    </Grid>
                                    {
                                        this.props.commonDetails.version
                                            ? (
                                                <ReadOnlyKeyValueView
                                                    label="Номер релиза:"
                                                    text={ this.props.commonDetails.version }
                                                />
                                            )
                                            : null
                                    }
                                    {
                                        this.props.commonDetails.configurationName && !this.props.isAdmin
                                            ? (
                                                <ReadOnlyKeyValueView
                                                    label="Конфигурация:"
                                                    text={ this.props.commonDetails.configurationName }
                                                />
                                            )
                                            : null
                                    }
                                    {
                                        this.props.isAdmin && this.props.commonDetails.archivePath
                                            ? (
                                                <ReadOnlyKeyValueView
                                                    label="Путь к архиву:"
                                                    text={ this.props.commonDetails.archivePath }
                                                />
                                            )
                                            : null
                                    }
                                    {
                                        this.props.isAdmin && this.props.commonDetails.zoneNumber
                                            ? (
                                                <ReadOnlyKeyValueView
                                                    label="Номер области:"
                                                    text={ this.props.commonDetails.zoneNumber }
                                                />
                                            )
                                            : null
                                    }
                                    <Box display="flex" flexDirection="row" gap="150px">
                                        {
                                            !this.props.commonDetails.isFile
                                                ? (
                                                    <ReadOnlyKeyValueView
                                                        label="Сервер 1С:"
                                                        text={ this.props.commonDetails.v82Server }
                                                    />
                                                )
                                                : null
                                        }
                                        {
                                            !this.props.commonDetails.isFile
                                                ? (
                                                    <ReadOnlyKeyValueView
                                                        label="Сервер SQL:"
                                                        text={ this.props.commonDetails.sqlServer }
                                                    />
                                                )
                                                : null
                                        }
                                    </Box>
                                    {
                                        !this.props.commonDetails.isFile && !this.props.commonDetails.isDbOnDelimiters
                                            ? (
                                                <RestoreModel
                                                    label="Модель восстановления:"
                                                    formName="restoreModel"
                                                    readOnlyRestoreName={ restoreModelInfoDbName(
                                                        this.props.commonDetails.restoreModelType,
                                                        this.props.commonDetails.commonDataForWorkingWithDB.AvailableAccountDatabaseRestoreModelTypes
                                                    ) }
                                                    editRestoreId={ this.props.commonDetails.restoreModelType }
                                                    restoreModels={ this.props.commonDetails.commonDataForWorkingWithDB.AvailableAccountDatabaseRestoreModelTypes }
                                                    isInEditMode={ this.props.commonDetails.canChangeRestoreModel }
                                                    onValueChange={ this.props.onValueChange }
                                                />
                                            )
                                            : null
                                    }
                                    {
                                        this.props.permissions.includes(InfoDbCardPermissionsEnum.AccountDatabases_EditStatus)
                                            ? (
                                                <InfoDbStateView
                                                    isInEditMode={ true }
                                                    formName="databaseState"
                                                    label="Статус:"
                                                    readOnlyState={ this.props.commonDetails.databaseState }
                                                    editState={ +this.props.commonDetails.databaseState }
                                                    states={ this.props.commonDetails.commonDataForWorkingWithDB.AvailableDatabaseStatuses }
                                                    onValueChange={ this.props.onValueChange }
                                                />
                                            )
                                            : null
                                    }
                                    {
                                        this.props.isAdmin && this.props.commonDetails.id
                                            ? (
                                                <ReadOnlyKeyValueView
                                                    label="Идентификатор:"
                                                    text={ this.props.commonDetails.id }
                                                />
                                            )
                                            : null
                                    }
                                    {
                                        this.props.isAdmin && this.props.commonDetails.isStatusErrorCreated
                                            ? (
                                                <div className={ css.errorDescription }>
                                                    <ReadOnlyKeyValueView
                                                        label="Ошибка:"
                                                        text={ this.props.commonDetails.createDatabaseComment ?? 'Вы можете удалить эту базу самостоятельно или обратитесь в Хотлайн.' }
                                                    />
                                                </div>
                                            )
                                            : null
                                    }
                                </Grid>
                            )
                            : (
                                <Grid item={ true } spacing={ 2 }>
                                    {
                                        this.props.commonDetails.version && this.props.commonDetails.databaseState === DatabaseState.Ready && !(this.props.commonDetails.isFile)
                                            ? (
                                                <ReadOnlyKeyValueView
                                                    label="Номер релиза:"
                                                    text={ this.props.commonDetails.version }
                                                />
                                            )
                                            : null
                                    }
                                    {
                                        this.props.commonDetails.configurationName
                                            ? (
                                                <ReadOnlyKeyValueView
                                                    label="Конфигурация:"
                                                    text={ this.props.commonDetails.configurationName }
                                                />
                                            )
                                            : null
                                    }
                                </Grid>
                            )
                    }
                </Grid>
            </div>
        );
    }
}