import { hasComponentChangesFor } from 'app/common/functions';
import { AppReduxStoreState } from 'app/redux/types';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { ComboBoxForm } from 'app/views/components/controls/forms/ComboBox/ComboBoxForm';
import { TextBoxForm } from 'app/views/components/controls/forms/TextBoxForm';
import { AccountCardEditAccountSegmentForm } from 'app/views/modules/_common/reusable-modules/AccountCard/types';
import { ChangeSegmentButton } from 'app/views/modules/_common/reusable-modules/AccountCard/views/_buttons/ChangeSegmentButton';
import { AccountCardSegmentInfoItemDataModel } from 'app/web/InterlayerApiProxy/AccountCardApiProxy/receiveAccountCard';
import cn from 'classnames';
import React from 'react';
import { connect } from 'react-redux';
import css from '../../styles.module.css';

type OwnProps = ReduxFormProps<AccountCardEditAccountSegmentForm>;

type StateProps = {
    accountId: string;
    currentSegmentName: string;
    segmentArr: Array<AccountCardSegmentInfoItemDataModel>;
    hasAccountCardReceived: boolean;
};

type AllProps = OwnProps & StateProps;

class ChangeSegmentTabViewClass extends React.Component<AllProps> {
    public constructor(props: AllProps) {
        super(props);
        this.getSelectedSegmentId = this.getSelectedSegmentId.bind(this);
        this.onComboboxValueChange = this.onComboboxValueChange.bind(this);
        this.hasEditAccountSegmentFormChanged = this.hasEditAccountSegmentFormChanged.bind(this);
        this.getSelectedSegmentText = this.getSelectedSegmentText.bind(this);
        this.initReduxForm = this.initReduxForm.bind(this);
    }

    public componentDidMount() {
        this.props.reduxForm.setInitializeFormDataAction(this.initReduxForm);
    }

    public shouldComponentUpdate(nextProps: AllProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private onComboboxValueChange(fieldName: string, newValue?: string) {
        this.props.reduxForm.updateReduxFormFields({
            [fieldName]: newValue
        });
    }

    private getSelectedSegmentText() {
        return this.props.segmentArr.find(item => item.segmentId === this.getSelectedSegmentId())?.segmentText;
    }

    private getSelectedSegmentId() {
        const changedFields = this.props.reduxForm.getReduxFormFields(true);
        return changedFields.segmentId ??
            this.props.segmentArr.find(item => item.segmentText === this.props.currentSegmentName)?.segmentId;
    }

    private initReduxForm(): AccountCardEditAccountSegmentForm | undefined {
        if (!this.props.hasAccountCardReceived) { return; }
        return {
            segmentId: this.props.segmentArr.find(item => item.segmentText === this.props.currentSegmentName)?.segmentId ?? ''
        };
    }

    private hasEditAccountSegmentFormChanged(): boolean {
        return this.props.currentSegmentName !== this.getSelectedSegmentText();
    }

    public render() {
        if (this.props.segmentArr.length <= 0 || !this.props.hasAccountCardReceived) {
            return null;
        }
        return (
            <div className={ cn(css['main-container']) } data-testid="change-segment-tab-view">
                <TextBoxForm
                    label="Текущий сегмент:"
                    isReadOnly={ true }
                    formName="currentSegmentName"
                    value={ this.props.currentSegmentName }
                />

                <div className={ cn(css['combobox-container']) }>
                    <ComboBoxForm
                        allowAutoComplete={ true }
                        autoFocus={ true }
                        label="Новый сегмент:"
                        formName="segmentId"
                        onValueChange={ this.onComboboxValueChange }
                        value={ this.getSelectedSegmentId() }
                        items={ this.props.segmentArr.map(item => {
                            return {
                                value: item.segmentId,
                                text: item.segmentText
                            };
                        }) }
                    />
                </div>

                <ChangeSegmentButton
                    isEnabled={ this.hasEditAccountSegmentFormChanged() }
                    segmentIdChangeTo={ this.getSelectedSegmentId() }
                    accountIdFor={ this.props.accountId }
                />
            </div>
        );
    }
}

const ChangeSegmentTabViewConnected = connect<StateProps, NonNullable<unknown>, OwnProps, AppReduxStoreState>(
    state => {
        const accountCardState = state.AccountCardState;
        const accountId = accountCardState.accountInfo.id;
        const editAccountSegmentForm = state.ReduxForms.AccountCard_EditAccountSegment_Form;
        const currentSegmentName = accountCardState.commonData.segmentName;
        const segmentArr = accountCardState.segments;
        const { hasAccountCardReceived } = accountCardState.hasSuccessFor;

        return {
            accountId,
            editAccountSegmentForm,
            currentSegmentName,
            segmentArr,
            hasAccountCardReceived
        };
    }
)(ChangeSegmentTabViewClass);

export const ChangeSegmentTabView = withReduxForm(ChangeSegmentTabViewConnected, {
    reduxFormName: 'AccountCard_EditAccountSegment_Form'
});