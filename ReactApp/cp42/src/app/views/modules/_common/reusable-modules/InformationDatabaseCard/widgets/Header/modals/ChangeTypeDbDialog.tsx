import { Link } from '@mui/material';
import { Box } from '@mui/system';
import { REDUX_API } from 'app/api/useReduxApi';
import { useAppDispatch, useAppSelector } from 'app/hooks';
import { Dialog } from 'app/views/components/controls/Dialog';
import { TextOut } from 'app/views/components/TextOut';
import cn from 'classnames';
import React, { useState } from 'react';
import css from './styles.module.css';

type TProps = {
    isFile?: boolean;
    isOpen: boolean;
    databaseName: string;
    onNoClick: () => void;
    onYesClick: () => void;
};

const { getInformationBaseInfo } = REDUX_API.INFORMATION_BASES_REDUX;

export const ChangeTypeDbDialog = ({ databaseName, isOpen, onNoClick, onYesClick, isFile }: TProps) => {
    const dispatch = useAppDispatch();

    const { informationBase } = useAppSelector(state => state.InformationBases.informationBasesList);
    const { v82Name } = informationBase?.database ?? {};

    const [showWarningDialog, setShowWarningDialog] = useState(false);

    return (
        <>
            <Dialog
                title="Изменение типа базы"
                isTitleSmall={ true }
                isOpen={ isOpen }
                dialogWidth="xs"
                dialogVerticalAlign="center"
                onCancelClick={ onNoClick }
                buttons={ [
                    {
                        kind: 'default',
                        content: 'Нет',
                        onClick: onNoClick
                    },
                    {
                        kind: 'error',
                        content: 'Да',
                        onClick: () => {
                            onYesClick();
                            if (!isFile) {
                                setShowWarningDialog(true);
                            }
                        }
                    }
                ] }
            >
                <div className={ cn(css['warning-icon-container'], css.pulseWarning) }>
                    <span className={ cn(css['warning-icon-circle'], css.pulseWarningIns) } />
                    <span className={ cn(css['warning-icon-dot'], css.pulseWarningIns) } />
                </div>
                <TextOut>Вы действительно хотите изменить тип базы <TextOut fontWeight={ 700 }>{ databaseName }</TextOut> на { !isFile ? 'файловую' : 'серверную' }</TextOut>
            </Dialog>
            <Dialog
                isOpen={ showWarningDialog }
                dialogWidth="xs"
                dialogVerticalAlign="center"
                title="Внимание!"
                hiddenX={ true }
                buttons={ [
                    {
                        onClick: () => {
                            onNoClick();
                            setShowWarningDialog(false);
                            void getInformationBaseInfo(dispatch, v82Name ?? '');
                        },
                        variant: 'contained',
                        kind: 'success',
                        content: 'Продолжить',
                    }
                ] }
            >
                <Box display="flex" flexDirection="column" gap="16px" alignItems="center">
                    <div className={ cn(css['warning-icon-container'], css.pulseWarning) }>
                        <span className={ cn(css['warning-icon-circle'], css.pulseWarningIns) } />
                        <span className={ cn(css['warning-icon-dot'], css.pulseWarningIns) } />
                    </div>
                    <TextOut>
                        Если у клиента больше не осталось серверных баз, необходимо отключить доступ в разделе&nbsp;
                        <Link
                            href={ `${ window.location.origin }/my-database-services/fba5f480-fbfa-4ad9-b468-9fa7d6c43524` }
                            target="_blank"
                            rel="noreferrer"
                        >
                            &#34;Клиент-серверный режим&#34;
                        </Link>.&nbsp;
                        После перевода базы в файловый вариант, необходимо удалить базу с сервера предприятия и SQL сервера.
                    </TextOut>
                </Box>
            </Dialog>
        </>
    );
};