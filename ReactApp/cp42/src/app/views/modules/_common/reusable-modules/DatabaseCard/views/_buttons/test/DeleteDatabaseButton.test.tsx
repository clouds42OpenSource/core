import '@testing-library/jest-dom/extend-expect';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { DeleteDatabaseButton } from 'app/views/modules/_common/reusable-modules/DatabaseCard/views/_buttons/DeleteDatabaseButton';

describe('<DeleteDatabaseButton />', () => {
    beforeEach(() => {
        render(<DeleteDatabaseButton databaseId="test_database_id" databaseName="test_database_name" />);
    });

    it('renders', () => {
        expect(screen.getByTestId(/button-view/i)).toBeInTheDocument();
        expect(screen.getByText(/Удалить/i)).toBeInTheDocument();
    });

    it('has onClick method', async () => {
        await userEvent.click(screen.getByTestId(/button-view/));
        expect(screen.getByRole(/dialog/i)).toBeInTheDocument();
    });
});