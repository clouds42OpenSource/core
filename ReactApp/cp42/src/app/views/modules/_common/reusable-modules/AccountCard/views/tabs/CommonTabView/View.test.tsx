import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { View } from 'app/views/modules/_common/reusable-modules/AccountCard/views/tabs/CommonTabView/View';
import { SnackbarProvider } from 'notistack';
import configureStore, { MockStoreEnhanced } from 'redux-mock-store';
import { Provider } from 'react-redux';

type ElementProps = {
    isLocaleEditable?: boolean;
    isInnEditable?: boolean;
    isAccountCaptionEditable?: boolean;
    canShowFullInfo?: boolean;
    canShowSaleManagerInfo?: boolean;
    isVip?: boolean;
};

describe('<View />', () => {
    const onFieldChangeMock: jest.Mock = jest.fn();
    const mockStore = configureStore();
    let store: MockStoreEnhanced<unknown>;
    const initialState = {
        AccountCardState: {
            stateName: 'test_state_name',
            reducerActions: {
                NONE: {
                    hasProcessActionStateChanged: true,
                    processActionState: {
                        isInErrorState: false,
                        isInSuccessState: true,
                        isInProgressState: false
                    },
                    process: {
                        error: undefined
                    }
                }
            }
        }
    } as const;

    beforeEach(() => {
        store = mockStore(initialState);
    });

    const elementForRender = ({
        isLocaleEditable = false,
        isInnEditable = false,
        isAccountCaptionEditable = false,
        canShowFullInfo = false,
        canShowSaleManagerInfo = false,
        isVip = false
    }: ElementProps) => {
        return (
            <Provider store={ store }>
                <SnackbarProvider>
                    <View
                        accountInfo={ {
                            deployment: null,
                            id: 'test_id',
                            accountCaption: 'test_caption',
                            inn: 'test_inn',
                            localeCurrency: 'test_currency',
                            localeId: 'test_locale_id',
                            description: 'test_description',
                            isVip,
                            indexNumber: 1,
                            cloudStorageWebLink: 'test_cloud_storage_web_link'
                        } }
                        commonInfo={ {
                            serviceTotalSum: 100,
                            accountSaleManagerCaption: 'test_sale_manager_caption',
                            accountType: 'test_type',
                            balance: 150,
                            segmentName: 'test_segment_name',
                            emails: ['']
                        } }
                        dispatchReceiveAccountCardThunk={ () => void 0 }
                        localeArr={ [{ localeId: 'test_locale_id', localeText: 'test_locale_text' }] }
                        editableFields={ { isLocaleEditable, isInnEditable, isAccountCaptionEditable } }
                        visibleFields={ { canShowFullInfo, canShowSaleManagerInfo } }
                        onFieldChange={ onFieldChangeMock }
                        accountDataForm={ {
                            deployment: null,
                            accountId: 'test_account_id',
                            inn: 'test_inn',
                            accountCaption: 'test_caption',
                            localeId: 'test_locale_id',
                            description: 'test_description',
                            isVip,
                            emails: ['']
                        } }
                    />
                </SnackbarProvider>
            </Provider>
        );
    };

    it('renders', () => {
        render(elementForRender({}));
        expect(screen.getByTestId(/view__main-container/i)).toBeInTheDocument();
    });

    it('has editable inn field', () => {
        const { container } = render(elementForRender({ isInnEditable: true }));
        expect(container.querySelector('[name=inn]')).toBeInTheDocument();
    });

    it('has editable locale field', () => {
        const { container } = render(elementForRender({ isLocaleEditable: true, canShowSaleManagerInfo: true }));
        expect(container.querySelector('[name=localeId]')).toBeInTheDocument();
    });

    it('has editable account caption field', () => {
        const { container } = render(elementForRender({ isAccountCaptionEditable: true }));
        expect(container.querySelector('[name=accountCaption]')).toBeInTheDocument();
    });

    it('shows sale manager info', () => {
        render(elementForRender({ canShowSaleManagerInfo: true }));
        expect(screen.getByText(/Сейлc менеджер:/i)).toBeInTheDocument();
    });

    it('shows full info', () => {
        const { container } = render(elementForRender({ canShowFullInfo: true }));
        expect(container.querySelector('[name=description]')).toBeInTheDocument();
    });

    it('has vip status', () => {
        const { container } = render(elementForRender({ isVip: true, canShowFullInfo: true }));
        expect(container.querySelector('[type=checkbox]')!.getAttribute('value')).toEqual('true');
    });
});