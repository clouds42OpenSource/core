import { hasComponentChangesFor } from 'app/common/functions';
import { getErrorMessage } from 'app/common/functions/getErrorMessage';
import { ContainedButton } from 'app/views/components/controls/Button';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import cn from 'classnames';
import { RequestKind } from 'core/requestSender/enums';
import React from 'react';
import css from '../styles.module.css';

type OwnState = {
    isEnabled: boolean;
};
type OwnProps = FloatMessageProps & {
    username: string;
    password: string;
    databaseId: string;
    onAuthProcess: () => void;
    timeOfUpdate: number;
    completeSession: boolean;
    isConnects: boolean;
    hasAutoUpdate: boolean;
    hasSupport: boolean;
    isAuthorized: boolean;
};

type AllProps = OwnProps;

class EnableTehSupportInfoDbButtonClass extends React.Component<AllProps, OwnState> {
    constructor(props: AllProps) {
        super(props);
        this.state = {
            isEnabled: true,
        };
        this.onEnableDatabaseSupport = this.onEnableDatabaseSupport.bind(this);
    }

    public shouldComponentUpdate(nextProps: AllProps, nextState: OwnState) {
        return hasComponentChangesFor(this.props, nextProps) || this.state.isEnabled !== nextState.isEnabled;
    }

    private async onEnableDatabaseSupport() {
        const infoDbCardApi = InterlayerApiProxy.getInfoDbCardApi();
        try {
            this.setState({ isEnabled: false });
            await infoDbCardApi.AuthInfoDbCard(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, {
                databaseId: this.props.databaseId,
                login: this.props.isConnects ? this.props.username : this.props.username || '',
                password: this.props.isConnects ? this.props.password : this.props.password || '',
                timeOfUpdate: this.props.timeOfUpdate,
                completeSessioin: this.props.completeSession,
                hasAutoUpdate: this.props.hasAutoUpdate,
                hasSupport: this.props.hasSupport,
            }).then(a => {
                if (a.Success) {
                    this.props.onAuthProcess();
                    if (this.props.isConnects) {
                        this.props.floatMessage.show(MessageType.Success, 'Успешно');
                    }
                } else {
                    this.props.floatMessage.show(MessageType.Error, a.Message);
                }
            });
        } catch (e) {
            this.props.floatMessage.show(MessageType.Error, getErrorMessage(e));
        } finally {
            this.setState({ isEnabled: true });
        }
    }

    public render() {
        if (this.props.isConnects && !this.props.isAuthorized) {
            return null;
        }

        return (
            <ContainedButton
                isEnabled={ this.state.isEnabled }
                kind="primary"
                className={ cn(css.btn) }
                onClick={ this.onEnableDatabaseSupport }
            >
                { this.props.isAuthorized && this.props.isConnects ? 'Изменить' : 'Авторизоваться в базе 1С' }
            </ContainedButton>
        );
    }
}

export const EnableTehSupportInfoDbButton = withFloatMessages(EnableTehSupportInfoDbButtonClass);