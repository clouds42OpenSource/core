declare const styles: {
    readonly 'main': string;
    readonly 'publish-url': string;
    readonly 'publish_web': string;
    readonly 'errorUser': string;
    readonly 'col-wrapper': string;
    readonly 'errorDescription': string;
};
export = styles;