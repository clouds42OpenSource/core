import '@testing-library/jest-dom/extend-expect';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { SnackbarProvider } from 'notistack';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';

import { EditDatabaseDataButton } from 'app/views/modules/_common/reusable-modules/DatabaseCard/views/_buttons/EditDatabaseDataButton';
import { DatabaseCardProcessId } from 'app/modules/databaseCard';

describe('<EditDatabaseDataButton />', () => {
    const mockStore = configureStore([thunk]);
    const getChangedDatabaseDataMock: jest.Mock = jest.fn();
    const initialState = {
        DatabaseCardState: {
            processIds: [DatabaseCardProcessId.UnpublishDatabase],
            reducerActions: {
                UNPUBLISH_DATABASE: {
                    hasProcessActionStateChanged: true,
                    processActionState: {
                        isInErrorState: false,
                        isInSuccessState: true,
                        isInProgressState: false
                    },
                    process: {
                        error: undefined
                    }
                }
            },
            process: {
                showLoadingProgress: false
            },
        }
    } as const;

    beforeEach(() => {
        render(
            <Provider store={ mockStore(initialState) }>
                <SnackbarProvider>
                    <EditDatabaseDataButton getChangedDatabaseData={ getChangedDatabaseDataMock } />
                </SnackbarProvider>
            </Provider>
        );
    });

    it('renders', () => {
        expect(screen.getByTestId(/button-view/i)).toBeInTheDocument();
        expect(screen.getByText(/Сохранить/i)).toBeInTheDocument();
    });

    it('has onClick method', async () => {
        await userEvent.click(screen.getByTestId(/button-view/i));
        expect(getChangedDatabaseDataMock).toBeCalled();
    });
});