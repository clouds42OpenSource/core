import { DatabaseState } from 'app/common/enums';

/**
 * Узнать, находится ли база во временном процессе
 */
export function defineInfoDbCheckState(DbState: number) {
    return DatabaseState.NewItem === DbState ||
        DatabaseState.DelitingToTomb === DbState;
}

export function defineInfoDbOnDelimitersCheckState(DbState: number) {
    return DatabaseState.NewItem === DbState;
}