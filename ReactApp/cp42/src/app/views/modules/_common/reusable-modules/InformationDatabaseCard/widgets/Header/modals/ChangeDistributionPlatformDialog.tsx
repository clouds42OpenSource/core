import { Dialog } from 'app/views/components/controls/Dialog';
import { TextOut } from 'app/views/components/TextOut';
import React from 'react';

type TProps = {
    isOpen: boolean;
    caption: string;
    onNoClick: () => void;
    onYesClick: () => void;
};

export const ChangeDistributionPlatformDialog = ({ isOpen, onYesClick, onNoClick, caption }: TProps) => {
    return (
        <Dialog
            title="Смена релиза платформы для информационной базы"
            isTitleSmall={ true }
            isOpen={ isOpen }
            dialogWidth="sm"
            onCancelClick={ onNoClick }
            buttons={ [{
                kind: 'primary',
                content: 'Подтвердить',
                onClick: onYesClick
            },
            {
                kind: 'default',
                content: 'Отмена',
                onClick: onNoClick
            }
            ] }
        >
            <TextOut>
                Вы действительно хотите сменить релиз платформы для &quot;{ caption }&quot;?
            </TextOut>
        </Dialog>
    );
};