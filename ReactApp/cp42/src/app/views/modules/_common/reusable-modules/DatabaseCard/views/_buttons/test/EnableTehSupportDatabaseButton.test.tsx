import '@testing-library/jest-dom/extend-expect';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { SnackbarProvider } from 'notistack';
import thunk from 'redux-thunk';

import { EnableTehSupportDatabaseButton } from 'app/views/modules/_common/reusable-modules/DatabaseCard/views/_buttons/EnableTehSupportDatabaseButton';
import { DatabaseCardProcessId } from 'app/modules/databaseCard';

describe('<EnableTehSupportDatabaseButton />', () => {
    const mockStore = configureStore([thunk]);
    let container: Element;
    const initialState = {
        DatabaseCardState: {
            processIds: [DatabaseCardProcessId.UnpublishDatabase],
            reducerActions: {
                UNPUBLISH_DATABASE: {
                    hasProcessActionStateChanged: true,
                    processActionState: {
                        isInErrorState: false,
                        isInSuccessState: true,
                        isInProgressState: false
                    },
                    process: {
                        error: undefined
                    }
                }
            },
            process: {
                showLoadingProgress: false
            },
        }
    } as const;

    beforeEach(() => {
        container = render(
            <Provider store={ mockStore(initialState) }>
                <SnackbarProvider>
                    <EnableTehSupportDatabaseButton
                        username="test_username"
                        password="test_password"
                        databaseId="test_database_id"
                    />
                </SnackbarProvider>
            </Provider>
        ).container;
    });

    it('renders', () => {
        expect(screen.getByTestId(/button-view/i)).toBeInTheDocument();
        expect(screen.getByText(/Подключить/i)).toBeInTheDocument();
    });

    it('has onClick method', async () => {
        await userEvent.click(screen.getByTestId(/button-view/i));
        expect(container.querySelector('.MuiTouchRipple-child')).toBeInTheDocument();
    });
});