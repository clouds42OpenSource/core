/* eslint-disable max-len */
import { AppRoutes } from 'app/AppRoutes';

export const PAGE_TITLE = {
    [AppRoutes.settings.consultations1C]: 'Нужна помощь? Мы рядом!',
    [AppRoutes.settings.registrationNumber]: 'Нужен регистрационный номер для 1С?',
    [AppRoutes.settings.reporting]: 'Хотите подключить 1С-Отчетность?',
    [AppRoutes.settings.edo]: 'Хотите подключить 1С-ЭДО?',
    [AppRoutes.settings.reconciliationStatement]: 'Нужен акт сверки?'
};

export const DESCRIPTION_SUPPORT_SERVICES = {
    [AppRoutes.settings.consultations1C]: `В 42Clouds работает команда опытных специалистов, готовых помочь вам решить любые вопросы, связанные с 1С и другими бизнес-операциями. Если вам требуется консультация или помощь программиста для решения сложной задачи, Вы можете оставить заявку прямо на этой странице.
    Наши специалисты подберут оптимальное решение, подходящее именно для вашего бизнеса, и предложат профессиональную поддержку на каждом этапе.
    42Clouds — всегда рядом, когда вам нужна помощь!`,
    [AppRoutes.settings.registrationNumber]: `Заполните форму ниже, и мы отправим запрос на выпуск регистрационного номера для вашей версии 1С. Срок выполнения заявки — до 3 рабочих дней.
    42Clouds — мы заботимся о том, чтобы все было сделано быстро и без лишних шагов. Заполните форму, и мы позаботимся о дальнейшем процессе!`,
    [AppRoutes.settings.reporting]: `Заполните форму ниже и отправьте заявку, и мы поможем вам с подключением и настройкой сервиса 1С-Отчетность.
    42Clouds — мы сделаем подключение и настройку простыми и удобными для вас. Заполните форму, и наши эксперты позаботятся обо всем!`,
    [AppRoutes.settings.edo]: `Заполните форму ниже, и мы поможем вам подключить и настроить 1С-ЭДО. Это сделает обмен документами с контрагентами еще проще и быстрее. Мы настроим систему и окажем поддержку на каждом шаге, чтобы Вы могли без проблем использовать электронный документооборот.
    42Clouds — мы сделаем подключение и настройку 1С-ЭДО максимально удобными для вас. Заполните форму, и мы позаботимся обо всем!`,
    [AppRoutes.settings.reconciliationStatement]: `Заполните форму ниже, выберите дату, и наш менеджер отправит вам готовый акт сверки. Мы сделаем все, чтобы процесс был быстрым и удобным.
    42Clouds — легко и быстро получите акт сверки. Заполните форму, и мы позаботимся обо всем!`
};

export const FORM_TITLE = {
    [AppRoutes.settings.consultations1C]: 'Новое обращение',
    [AppRoutes.settings.registrationNumber]: 'Заявка на выпуск регистрационного номера',
    [AppRoutes.settings.reporting]: 'Заявка на подключение сервиса',
    [AppRoutes.settings.edo]: 'Заявка на подключение сервиса',
    [AppRoutes.settings.reconciliationStatement]: 'Запросить акт сверки взаиморасчетов'
};

export const SUPPORT_LOGO = {
    [AppRoutes.settings.consultations1C]: '/img/additional-pages/consultations1C.png',
    [AppRoutes.settings.registrationNumber]: '/img/additional-pages/registrationNumber.png',
    [AppRoutes.settings.reporting]: '/img/additional-pages/reporting.png',
    [AppRoutes.settings.edo]: '/img/additional-pages/edo.png',
    [AppRoutes.settings.reconciliationStatement]: '/img/additional-pages/reconciliationStatement.png'
};

export const CONFIGURATIONS = [
    'Бухгалтерия некоммерческой организации',
    'Бухгалтерия предприятия 3.0',
    'Зарплата и управление персоналом 3.1',
    'Комплексная автоматизация 2.5',
    'Розница',
    'Управление нашей фирмой 3.0',
    'Управление торговлей 11',
    'Бухгалтерия предприятия 2.0',
    'Бухгалтерия предприятия 2.0 Корп',
    'Бухгалтерия предприятия 3.0 Корп',
    'Транспортная логистика, экспедирование и управление автотранспортом КОРП',
    'Управление торговлей 10.3',
    'Розница Салон Оптики',
    '1С:CRM КОРП 2.0',
    'Кадровое агентство',
    '1С:Документооборот 8 ПРОФ',
    'Упр. торговлей и взаимоотн. с клиентами (CRM) 3.0',
];

export { ItsHelpAdornment } from './itsHelpAdornment';