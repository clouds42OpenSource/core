import { Chip } from '@mui/material';
import { hasComponentChangesFor } from 'app/common/functions';
import { SelectedDatabasesToMigrateContext } from 'app/views/modules/_common/reusable-modules/AccountDatabasesForMigrations/context/SelectedDatabasesToMigrateContext';
import React from 'react';

type SelectedDatabaseItemProps = {
    databaseNumber: string;
};
export class SelectedDatabaseItem extends React.Component<SelectedDatabaseItemProps> {
    public context!: React.ContextType<typeof SelectedDatabasesToMigrateContext>;

    public constructor(props: SelectedDatabaseItemProps) {
        super(props);
        this.onRemoveItem = this.onRemoveItem.bind(this);
    }

    public shouldComponentUpdate(nextProps: SelectedDatabaseItemProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private onRemoveItem() {
        this.context.removeDatabaseNumber(this.props.databaseNumber);
    }

    public render() {
        return (
            <Chip
                style={ { backgroundColor: '#e4e4e4' } }
                variant="outlined"
                label={ this.props.databaseNumber }
                onDelete={ this.onRemoveItem }
            />
        );
    }
}
SelectedDatabaseItem.contextType = SelectedDatabasesToMigrateContext;