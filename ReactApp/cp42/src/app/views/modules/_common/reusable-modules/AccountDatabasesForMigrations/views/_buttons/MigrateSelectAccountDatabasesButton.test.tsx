import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import userEvent from '@testing-library/user-event';
import { SnackbarProvider } from 'notistack';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';

import { MigrateSelectedAccountDatabasesButton } from 'app/views/modules/_common/reusable-modules/AccountDatabasesForMigrations/views/_buttons/MigrateSelectedAccountDatabasesButton';
import { SelectedDatabasesToMigrateContext } from 'app/views/modules/_common/reusable-modules/AccountDatabasesForMigrations/context/SelectedDatabasesToMigrateContext';
import { AccountDatabasesForMigrationReducerState } from 'app/modules/accountDatabasesForMigrations/store/reducers';
import { ReduxFormsReducerState } from 'app/redux/redux-forms/store/reducers/ReduxFormsReducerState';
import { getNewStore } from 'app/utils/StoreUtility';

type TInitialState = {
    AccountDatabasesForMigrationState: AccountDatabasesForMigrationReducerState;
    ReduxForms: ReduxFormsReducerState<string>;
};

describe('<MigrateSelectedAccountDatabasesButton />', () => {
    const store = getNewStore();
    const addDatabaseNumberMock: jest.Mock = jest.fn();
    const removeDatabaseNumberMock: jest.Mock = jest.fn();
    const setSelectedStorageMock: jest.Mock = jest.fn();
    const resetMock: jest.Mock = jest.fn();
    const initialState: TInitialState = {
        AccountDatabasesForMigrationState: store.getState().AccountDatabasesForMigrationState,
        ReduxForms: store.getState().ReduxForms
    };

    beforeEach(() => {
        render(
            <SelectedDatabasesToMigrateContext.Provider value={ {
                selectedDatabaseNumbers: ['test_number'],
                addDatabaseNumber: addDatabaseNumberMock,
                removeDatabaseNumber: removeDatabaseNumberMock,
                selectedStorageId: 'test_id',
                setSelectedStorage: setSelectedStorageMock,
                reset: resetMock
            } }
            >
                <SnackbarProvider>
                    <Provider store={ configureStore()(initialState) }>
                        <MigrateSelectedAccountDatabasesButton />
                    </Provider>
                </SnackbarProvider>
            </SelectedDatabasesToMigrateContext.Provider>
        );
    });

    it('renders', () => {
        expect(screen.getByTestId(/button-view/i)).toBeInTheDocument();
    });

    it('opens dialog', async () => {
        await userEvent.click(screen.getByTestId(/button-view/i));
        expect(screen.getByTestId(/dialog-view/i)).toBeInTheDocument();
        expect(screen.getByTestId(/dialog-content-view/i)).toBeInTheDocument();
        expect(screen.getByTestId(/dialog-actions-view/i)).toBeInTheDocument();
        expect(screen.getByText(/перенести/)).toBeInTheDocument();
        expect(screen.getByText(/отмена/i)).toBeInTheDocument();
    });
});