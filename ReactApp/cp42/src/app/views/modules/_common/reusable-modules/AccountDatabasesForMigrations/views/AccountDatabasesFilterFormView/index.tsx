import {
    AccountDatabasesForMigrationsFilterDataForm,
    AccountDatabasesForMigrationsFilterDataFormFieldNamesType
} from 'app/views/modules/_common/reusable-modules/AccountDatabasesForMigrations/types';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import {
    AccountDatabasesSummaryView
} from 'app/views/modules/_common/reusable-modules/AccountDatabasesForMigrations/views/AccountDatabasesSummaryView';
import React from 'react';
import { TextBoxForm } from 'app/views/components/controls/forms';
import { hasComponentChangesFor } from 'app/common/functions';
import { Box } from '@mui/material';

/**
 * Свойства для компонента фильтра
 */
type OwnProps = ReduxFormProps<AccountDatabasesForMigrationsFilterDataForm> & {
    accountIndexNumber: number;
    onFilterChanged: (filter: AccountDatabasesForMigrationsFilterDataForm, filterFormName: string) => void;
    isFilterFormDisabled: boolean;
};

class AccountDatabasesFilterFormViewClass extends React.Component<OwnProps> {
    public constructor(props: OwnProps) {
        super(props);
        this.initReduxForm = this.initReduxForm.bind(this);
        this.onValueChange = this.onValueChange.bind(this);
        this.onValueApplied = this.onValueApplied.bind(this);
    }

    public componentDidMount() {
        this.props.reduxForm.setInitializeFormDataAction(this.initReduxForm);
    }

    public shouldComponentUpdate(nextProps: OwnProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private onValueChange<TValue, TForm extends string = AccountDatabasesForMigrationsFilterDataFormFieldNamesType>(formName: TForm, newValue: TValue) {
        this.props.reduxForm.updateReduxFormFields({
            [formName]: newValue
        });
    }

    private onValueApplied<TValue>(formName: string, value: TValue) {
        this.props.onFilterChanged({
            ...this.props.reduxForm.getReduxFormFields(false),
            [formName]: value
        }, formName);
    }

    private initReduxForm(): AccountDatabasesForMigrationsFilterDataForm | undefined {
        return {
            accountIndexNumber: this.props.accountIndexNumber,
            searchLine: '',
            isTypeStorageFile: null
        };
    }

    public render() {
        const formFields = this.props.reduxForm.getReduxFormFields(false);

        return (
            <>
                <Box display="flex" flexDirection="row" data-testid="account-databases-filter-form-view">
                    <TextBoxForm
                        autoFocus={ false }
                        label="Поиск"
                        placeholder="Номер, размер..."
                        formName="searchLine"
                        onValueChange={ this.onValueChange }
                        onValueApplied={ this.onValueApplied }
                        value={ formFields.searchLine }
                        isReadOnly={ this.props.isFilterFormDisabled }
                    />
                </Box>
                <br />
                <AccountDatabasesSummaryView />
            </>
        );
    }
}

export const AccountDatabasesFilterFormView = withReduxForm(AccountDatabasesFilterFormViewClass, {
    reduxFormName: 'AccountDatabasesMigrations_Filter_Form',
    resetOnUnmount: true
});