import { useEffect, useRef } from 'react';
import QRCode from 'qrcode';

type TQrCodeElementProps = {
    text: string;
}

export const QrCodeElement = ({ text }: TQrCodeElementProps) => {
    const canvasRef = useRef(null);

    useEffect(() => {
        if (canvasRef.current) {
            void QRCode.toCanvas(canvasRef.current, text);
        }
    }, [text]);

    return <canvas ref={ canvasRef } />;
};