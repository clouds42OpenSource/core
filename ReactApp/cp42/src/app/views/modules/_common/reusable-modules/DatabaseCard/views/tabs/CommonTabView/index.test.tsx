import '@testing-library/jest-dom/extend-expect';
import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { CommonTabView } from 'app/views/modules/_common/reusable-modules/DatabaseCard/views/tabs/CommonTabView';
import { getNewStore } from 'app/utils/StoreUtility';
import { ReduxFormsReducerState } from 'app/redux/redux-forms/store/reducers/ReduxFormsReducerState';
import { DatabaseCardReducerState } from 'app/modules/databaseCard/store/reducers';
import { AccountDatabaseRestoreModelType, DatabaseState, DbPublishState, DistributionType, PlatformType } from 'app/common/enums';
import { SnackbarProvider } from 'notistack';

type TInitialState = {
    DatabaseCardState: DatabaseCardReducerState;
    ReduxForms: ReduxFormsReducerState<string>;
};

type TRenderedComponent = {
    hasDatabaseCardReceived: boolean;
    showOptionalComponents: boolean;
};

describe('<CommonTabView />', () => {
    const store = getNewStore();
    const initialState: TInitialState = {
        DatabaseCardState: store.getState().DatabaseCardState,
        ReduxForms: store.getState().ReduxForms
    };

    const getRenderedComponent = ({ hasDatabaseCardReceived, showOptionalComponents }: TRenderedComponent) => {
        const databaseCardState = initialState.DatabaseCardState;
        databaseCardState.hasSuccessFor.hasDatabaseCardReceived = hasDatabaseCardReceived;
        databaseCardState.commonDetails = {
            accountId: '',
            accountCaption: '',
            alpha83Version: '',
            archivePath: '',
            backupDateString: '',
            canChangeRestoreModel: false,
            canWebPublish: false,
            caption: '',
            cloudStorageWebLink: '',
            configurationName: ' ',
            createAccountDatabaseComment: '',
            creationDate: new Date(),
            databaseState: DatabaseState.ErrorCreate,
            dbNumber: 0,
            dbTemplate: '',
            distributionType: DistributionType.Stable,
            existBackUpPath: false,
            filePath: '',
            fileStorage: '',
            fileStorageName: '',
            id: '',
            isDbOnDelimiters: true,
            isDeleted: false,
            isDemoDelimiters: false,
            isExternalDb: false,
            isReady: false,
            lastActivityDate: new Date(),
            lockedState: '',
            needShowWebLink: false,
            platformType: PlatformType.V82,
            publishState: DbPublishState.Published,
            restoreModelType: AccountDatabaseRestoreModelType.Simple,
            serviceName: '',
            sizeInMB: 0,
            sqlServer: '',
            stable82Version: '',
            stable83Version: '',
            state: DatabaseState.Ready,
            templateImgUrl: '',
            templateName: '',
            templatePlatform: PlatformType.V82,
            timezoneName: '',
            usedWebServices: false,
            v82Name: '',
            v82Server: '',
            version: ' ',
            webPublishPath: '',
            zoneNumber: 5
        };

        databaseCardState.databaseCardReadModeFieldAccessInfo = {
            canShowDatabaseDetails: showOptionalComponents,
            canShowDatabaseFileStorage: showOptionalComponents,
            canShowDatabaseState: showOptionalComponents,
            canShowSqlServer: showOptionalComponents,
            canShowUsedWebServices: showOptionalComponents,
            canShowWebPublishPath: showOptionalComponents
        };

        return render(
            <Provider store={ configureStore([thunk])(initialState) }>
                <SnackbarProvider>
                    <CommonTabView />
                </SnackbarProvider>
            </Provider>
        ).container;
    };

    it('renders empty', () => {
        const container = getRenderedComponent({ hasDatabaseCardReceived: false, showOptionalComponents: false });
        expect(container.firstChild).toBeNull();
    });

    it('renders with databaseCardReadModeFieldAccessInfo', () => {
        getRenderedComponent({ hasDatabaseCardReceived: true, showOptionalComponents: true });
        expect(screen.getByTestId(/read-only-view/i)).toBeInTheDocument();
        expect(screen.getByText(/Дата создания:/i)).toBeInTheDocument();
        expect(screen.getByText(/Хранилище:/i)).toBeInTheDocument();
        expect(screen.getByText(/Статус:/i)).toBeInTheDocument();
        expect(screen.getByText(/Сервер SQL:/i)).toBeInTheDocument();
        expect(screen.getByText(/Опубликованы веб сервисы:/i)).toBeInTheDocument();
        expect(screen.getByText(/База доступна по адресу:/i)).toBeInTheDocument();
        expect(screen.getByText(/Номер релиза:/i)).toBeInTheDocument();
        expect(screen.getByText(/Номер области:/i)).toBeInTheDocument();
        expect(screen.getByText(/Ошибка:/i)).toBeInTheDocument();
    });
});