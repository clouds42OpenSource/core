import { AppRoutes } from 'app/AppRoutes';
import { Dialog } from 'app/views/components/controls/Dialog';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import cn from 'classnames';
import { useHistory } from 'react-router';

import css from './styles.module.css';

type TProps = {
    isOpen: boolean;
    onNoClick: () => void;
    onYesClick: () => void;
    notEnoughMoney: number;
    usePromise: boolean;
    canUsePromisePayment: boolean;
    loading: boolean;
};

export const BuyAccessDbDialog = ({ isOpen, onNoClick, onYesClick, loading, canUsePromisePayment, usePromise, notEnoughMoney }: TProps) => {
    const history = useHistory();

    const redirect = () => {
        history.push(`${ AppRoutes.accountManagement.replenishBalance }?customPayment.amount=${ notEnoughMoney }&customPayment.description=Оплата+за+предоставление+доступов`);
    };

    return (
        <>
            { loading && <LoadingBounce /> }
            <Dialog
                isOpen={ isOpen }
                dialogWidth="sm"
                dialogVerticalAlign="center"
                onCancelClick={ onNoClick }
                title="Внимание"
                isTitleSmall={ true }
                buttons={ [
                    {
                        onClick: onNoClick,
                        variant: 'contained',
                        kind: 'default',
                        content: 'Отмена',
                    },
                    {
                        onClick: onYesClick,
                        variant: 'contained',
                        kind: 'primary',
                        content: 'Взять обещанный платёж',
                        hiddenButton: !canUsePromisePayment
                    },
                    {
                        onClick: redirect,
                        variant: 'contained',
                        kind: 'success',
                        content: 'Оплатить сейчас',
                    }
                ] }
            >
                <div className={ cn(css['warning-icon-container'], css.pulseWarning) }>
                    <span className={ cn(css['warning-icon-circle'], css.pulseWarningIns) } />
                    <span className={ cn(css['warning-icon-dot'], css.pulseWarningIns) } />
                </div>
                {
                    usePromise
                        ? (
                            <p className={ css['sweet-alert-body'] }>
                                Уважаемый пользователь, для оплаты доступов у Вас
                                недостаточно средств в размере <b>{ notEnoughMoney.toFixed(2) }</b> руб.
                                {
                                    canUsePromisePayment
                                        ? (
                                            <p>Вы можете
                                                воспользоваться услугой <b>&quot;Обещанный платеж&quot;</b>, Вы
                                                должны будете погасить задолжность в течение 7 дней.
                                            </p>
                                        )
                                        : ''
                                }
                            </p>
                        )
                        : (
                            <p className={ css['sweet-alert-body'] }>
                                Уважаемый пользователь, для оплаты доступов у Вас
                                недостаточно средств в размере { notEnoughMoney.toFixed(2) } руб. Пожалуйста, пополните баланс
                            </p>
                        )
                }
            </Dialog>
        </>
    );
};