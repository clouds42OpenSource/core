import { hasComponentChangesFor } from 'app/common/functions';
import { AccountCardProcessId } from 'app/modules/accountCard';
import { UpdateAccountCardParams, UpdateAccountCardThunkParams } from 'app/modules/accountCard/store/reducers/updateAccountCardReducer/params';
import { UpdateAccountCardThunk } from 'app/modules/accountCard/store/thunks/UpdateAccountCardThunk';
import { AppReduxStoreState } from 'app/redux/types';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { ContainedButton } from 'app/views/components/controls/Button';
import { WatchReducerProcessActionState } from 'app/views/modules/_common/components/WatchReducerProcessActionState';
import { ValidationError } from 'app/web/request-submitter/common/RequestSender/ValidationError';
import { IReducerProcessActionInfo } from 'core/redux/interfaces';
import React from 'react';
import { connect } from 'react-redux';

type OwnProps = FloatMessageProps & {
    isEnabled?: boolean;
    onSuccess?: () => void;
    showSuccessMessage?: boolean;
    showErrorMessage?: boolean;
    getAccountDataToUpdate: () => UpdateAccountCardParams;
    needShowLocaleChangedMesssage: () => boolean;
};

type OwnState = {
    updateAccountDataState: {
        isInProgressState: boolean;
        isInSuccessState: boolean;
        isInErrorState: boolean;
        error?: Error;
    }
};

type DispatchProps = {
    dispatchUpdateAccountCardThunk: (args: UpdateAccountCardThunkParams) => void;
};

type AllProps = OwnProps & DispatchProps;

export class SaveAccountDataButtonClass extends React.Component<AllProps, OwnState> {
    public constructor(props: AllProps) {
        super(props);

        this.onUpdateAccountDataClick = this.onUpdateAccountDataClick.bind(this);
        this.onProcessActionStateChanged = this.onProcessActionStateChanged.bind(this);

        this.state = {
            updateAccountDataState: {
                isInErrorState: false,
                isInProgressState: false,
                isInSuccessState: false,
                error: undefined
            }
        };
    }

    public shouldComponentUpdate(nextProps: OwnProps, nextState: OwnState) {
        return hasComponentChangesFor(this.props, nextProps) ||
            hasComponentChangesFor(this.state, nextState);
    }

    public componentDidUpdate(_prevProps: AllProps, prevState: OwnState) {
        if (!prevState.updateAccountDataState.isInSuccessState && this.state.updateAccountDataState.isInSuccessState) {
            if (this.props.showSuccessMessage ?? true) {
                this.props.floatMessage.show(MessageType.Success, 'Информация о аккаунте сохранена!');
            }

            if (this.props.onSuccess) this.props.onSuccess();
        } else if (!prevState.updateAccountDataState.isInErrorState && this.state.updateAccountDataState.isInErrorState) {
            if (this.props.showErrorMessage ?? true) {
                this.props.floatMessage.show(MessageType.Error, this.state.updateAccountDataState.error instanceof ValidationError
                    ? 'Ошибка валидации при сохранении информации о аккаунте'
                    : this.state.updateAccountDataState.error?.message);
            }
        }
    }

    private onProcessActionStateChanged(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId === AccountCardProcessId.UpdateAccountCard) {
            this.setState({
                updateAccountDataState: {
                    isInProgressState: actionState.isInProgressState,
                    isInSuccessState: actionState.isInSuccessState,
                    isInErrorState: actionState.isInErrorState,
                    error
                }
            });
        }
    }

    private onUpdateAccountDataClick() {
        if (this.props.needShowLocaleChangedMesssage()) this.props.floatMessage.show(MessageType.Success, 'Изменение локали может занять несколько минут');

        const accountDataToUpdate = this.props.getAccountDataToUpdate();
        if (!accountDataToUpdate) { return; }
        this.props.dispatchUpdateAccountCardThunk({
            ...accountDataToUpdate,
            force: true
        });
    }

    public render() {
        return (
            <>
                <ContainedButton
                    isEnabled={ this.props.isEnabled && !this.state.updateAccountDataState.isInProgressState }
                    showProgress={ this.state.updateAccountDataState.isInProgressState }
                    onClick={ this.onUpdateAccountDataClick }
                    kind="success"
                >
                    Сохранить
                </ContainedButton>
                <WatchReducerProcessActionState
                    watch={ [{
                        reducerStateName: 'AccountCardState',
                        processIds: [AccountCardProcessId.UpdateAccountCard]
                    }] }
                    onProcessActionStateChanged={ this.onProcessActionStateChanged }
                />
            </>
        );
    }
}

const SaveAccountDataButtonConnected = connect<NonNullable<unknown>, DispatchProps, OwnProps, AppReduxStoreState>(
    null,
    {
        dispatchUpdateAccountCardThunk: UpdateAccountCardThunk.invoke,
    }
)(SaveAccountDataButtonClass);

export const SaveAccountDataButton = withFloatMessages(SaveAccountDataButtonConnected);