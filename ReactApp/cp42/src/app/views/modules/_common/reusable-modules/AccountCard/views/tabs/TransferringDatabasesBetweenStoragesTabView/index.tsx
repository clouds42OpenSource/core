import { hasComponentChangesFor } from 'app/common/functions';
import { AccountDatabasesForMigrationsView } from 'app/views/modules/_common/reusable-modules/AccountDatabasesForMigrations';
import React from 'react';

type OwnProps = {
    canLoad: boolean;
    accountIndexNumber: number;
};

export class TransferringDatabasesBetweenStoragesTabView extends React.Component<OwnProps> {
    public shouldComponentUpdate(nextProps: OwnProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    public render() {
        return (
            <AccountDatabasesForMigrationsView
                canShow={ this.props.canLoad }
                accountIndexNumber={ this.props.accountIndexNumber }
            />
        );
    }
}