import { hasComponentChangesFor } from 'app/common/functions';
import { AppReduxStoreState } from 'app/redux/types';
import { TableFilterWrapper } from 'app/views/components/_hoc/tableFilterWrapper';
import { FilterItemWrapper } from 'app/views/components/_hoc/tableFilterWrapper/filterItemWrapper';
import { ComboBoxForm } from 'app/views/components/controls/forms';
import { SelectedDatabasesToMigrateContext } from 'app/views/modules/_common/reusable-modules/AccountDatabasesForMigrations/context/SelectedDatabasesToMigrateContext';
import { MigrateSelectedAccountDatabasesButton } from 'app/views/modules/_common/reusable-modules/AccountDatabasesForMigrations/views/_buttons/MigrateSelectedAccountDatabasesButton';
import { KeyValueDataModel } from 'app/web/common/data-models';
import React from 'react';
import { connect } from 'react-redux';

type StateProps = {
    avalableFileStorages: KeyValueDataModel<string, string>[];
};

type AllProps = StateProps;
class SelectStorageToMigrateViewClass extends React.Component<AllProps> {
    public context!: React.ContextType<typeof SelectedDatabasesToMigrateContext>;

    public constructor(props: AllProps) {
        super(props);
        this.onValueChange = this.onValueChange.bind(this);
    }

    public componentDidMount() {
        if (!this.context.selectedStorageId && !this.props.avalableFileStorages.length) return;
        this.context.setSelectedStorage(this.props.avalableFileStorages[0].key);
    }

    public shouldComponentUpdate(nextProps: AllProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private onValueChange(_formName: string, newValue: string) {
        this.context.setSelectedStorage(newValue);
    }

    public render() {
        return (
            <TableFilterWrapper>
                <FilterItemWrapper width="60%">
                    <ComboBoxForm
                        formName="selectedStorage"
                        label=""
                        value={ this.context.selectedStorageId }
                        onValueChange={ this.onValueChange }
                        items={ this.props.avalableFileStorages.map(item => {
                            return {
                                text: item.value,
                                value: item.key
                            };
                        }) }
                    />
                </FilterItemWrapper>
                <MigrateSelectedAccountDatabasesButton />
            </TableFilterWrapper>
        );
    }
}

SelectStorageToMigrateViewClass.contextType = SelectedDatabasesToMigrateContext;
export const SelectStorageToMigrateView = connect<StateProps, NonNullable<unknown>, NonNullable<unknown>, AppReduxStoreState>(
    state => {
        const accountDatabasesForMigrationState = state.AccountDatabasesForMigrationState;
        const { avalableFileStorages } = accountDatabasesForMigrationState.accountDatabasesMigrationInfo;

        return {
            avalableFileStorages
        };
    })(SelectStorageToMigrateViewClass);