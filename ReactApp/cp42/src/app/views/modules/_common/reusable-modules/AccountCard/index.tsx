import { AccountCardEditAccountDataForm, AccountCardEditAccountSegmentForm } from 'app/views/modules/_common/reusable-modules/AccountCard/types';
import { AccountCardAccountInfoDataModel } from 'app/web/InterlayerApiProxy/AccountCardApiProxy/receiveAccountCard';

import { hasComponentChangesFor } from 'app/common/functions';
import { AccountCardProcessId } from 'app/modules/accountCard';
import { ReceiveAccountCardThunkParams } from 'app/modules/accountCard/store/reducers/receiveAccountCardReducer/params';
import { UpdateAccountCardParams } from 'app/modules/accountCard/store/reducers/updateAccountCardReducer/params';
import { ReceiveAccountCardThunk } from 'app/modules/accountCard/store/thunks/ReceiveAccountCardThunk';
import { AppReduxStoreState } from 'app/redux/types';
import { IReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { Dialog } from 'app/views/components/controls/Dialog';
import { WatchReducerProcessActionState } from 'app/views/modules/_common/components/WatchReducerProcessActionState';
import { AccountCardContentView } from 'app/views/modules/_common/reusable-modules/AccountCard/views/AccountCardContentView';
import { SaveAccountDataButton } from 'app/views/modules/_common/reusable-modules/AccountCard/views/_buttons/SaveAccountDataButton';
import { IReducerProcessActionInfo } from 'core/redux/interfaces';
import React from 'react';
import { connect } from 'react-redux';

type OwnProps = {
    /**
     * Состояние диалога карточки аккаунта открыта/закрыта
     */
    isOpen: boolean;
    /**
     * Событие на закрытие диалога
     */
    onCancelDialogButtonClick: () => void;
    /**
     * Номер аккаунта для открытия
     */
    accountNumber: number;
    /**
     * Событие при успешном сохранении данных аккаунта
     * @param fields поля данных аккаунта
     */
    onCommonAccountInfoChange?: (fields: AccountCardEditAccountDataForm) => void;

    /**
     * Readonly mode
     */
    isReadonlyAccountInfo?: boolean;
};

type StateProps = {
    accountCaption: string;
    accountInfo: AccountCardAccountInfoDataModel;
};

type DispatchProps = {
    dispatchReceiveAccountCardThunk: (args?: ReceiveAccountCardThunkParams) => void;
};

type OwnState = {
    isInReceiveAccountCardState: boolean;
};

type AllProps = OwnProps & StateProps & DispatchProps;

export class AccountCardClass extends React.Component<AllProps, OwnState> {
    private commonTab = React.createRef<IReduxForm<AccountCardEditAccountDataForm>>();

    private changeSegmentTab = React.createRef<IReduxForm<AccountCardEditAccountSegmentForm>>();

    public constructor(props: AllProps) {
        super(props);
        this.onProcessActionStateChanged = this.onProcessActionStateChanged.bind(this);
        this.getAccountDataToUpdate = this.getAccountDataToUpdate.bind(this);
        this.getDialogButtons = this.getDialogButtons.bind(this);
        this.onSaveChangesSuccess = this.onSaveChangesSuccess.bind(this);
        this.needShowLocaleChangedMesssage = this.needShowLocaleChangedMesssage.bind(this);

        this.state = {
            isInReceiveAccountCardState: false,
        };
    }

    public shouldComponentUpdate(nextProps: OwnProps, nextState: OwnState) {
        return hasComponentChangesFor(this.props, nextProps) ||
            hasComponentChangesFor(this.state, nextState);
    }

    public componentDidUpdate(prevProps: AllProps) {
        if (!prevProps.isOpen && this.props.isOpen) {
            this.props.dispatchReceiveAccountCardThunk({
                accountNumber: this.props.accountNumber,
                force: true
            });
        }
    }

    private onProcessActionStateChanged(processId: string, actionState: IReducerProcessActionInfo, _error?: Error) {
        if (processId === AccountCardProcessId.ReceiveAccountCard) {
            this.setState({
                isInReceiveAccountCardState: actionState.isInProgressState
            });
        }
    }

    /**
     * Событие при успешном сохранении изменений
     */
    private onSaveChangesSuccess() {
        if (this.props.onCommonAccountInfoChange) {
            this.props.onCommonAccountInfoChange(this.commonTab.current!.getReduxForm().getReduxFormFields(false));
        }
        if (this.props.onCancelDialogButtonClick) {
            this.props.onCancelDialogButtonClick();
        }
    }

    /**
     * Получить кнопки для текущего диалогового окна
     */
    private getDialogButtons() {
        return (
            <SaveAccountDataButton
                key="SaveAccountDataButton"
                getAccountDataToUpdate={ this.getAccountDataToUpdate }
                needShowLocaleChangedMesssage={ this.needShowLocaleChangedMesssage }
                onSuccess={ this.onSaveChangesSuccess }
                isEnabled={ true }
            />
        );
    }

    private getAccountDataToUpdate(): UpdateAccountCardParams {
        return { ...this.commonTab.current!.getReduxForm().getReduxFormFields(false) };
    }

    /**
     * Проверка, нужно ли показывать сообщение об изменении локали
     */
    private readonly needShowLocaleChangedMesssage = () => this.getAccountDataToUpdate().localeId !== this.props.accountInfo.localeId;

    public render() {
        return (
            <>
                <Dialog
                    title={ `Карточка компании: ${ this.props.accountCaption ?? '' }` }
                    isOpen={ this.props.isOpen }
                    dialogWidth="md"
                    onCancelClick={ this.props.onCancelDialogButtonClick }
                    buttons={ this.props.isReadonlyAccountInfo ? undefined : this.getDialogButtons }
                >
                    <AccountCardContentView
                        isReadonlyAccountInfo={ this.props.isReadonlyAccountInfo }
                        accountIndexNumber={ this.props.accountNumber }
                        changeSegmentTabRef={ this.changeSegmentTab }
                        commonTabRef={ this.commonTab }
                    />
                </Dialog>
                <WatchReducerProcessActionState
                    watch={ [{
                        reducerStateName: 'AccountCardState',
                        processIds: [AccountCardProcessId.ReceiveAccountCard]
                    }] }
                    onProcessActionStateChanged={ this.onProcessActionStateChanged }
                />
            </>
        );
    }
}

export const AccountCard = connect<StateProps, DispatchProps, OwnProps, AppReduxStoreState>(
    state => {
        const accountCardState = state.AccountCardState;
        const { tabVisibility } = accountCardState;
        const { accountCaption } = accountCardState.accountInfo;
        const { accountInfo } = accountCardState;
        const { emails } = accountCardState.commonData;
        const { hasAccountCardReceived } = accountCardState.hasSuccessFor;

        return {
            tabVisibility,
            accountCaption,
            accountInfo,
            emails,
            hasAccountCardReceived
        };
    },
    {
        dispatchReceiveAccountCardThunk: ReceiveAccountCardThunk.invoke,
    }
)(AccountCardClass);