import { EMessageType, useFloatMessages } from 'app/hooks';
import { OutlinedButton } from 'app/views/components/controls/Button';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { RequestKind } from 'core/requestSender/enums';

type TProps = {
    databaseId: string;
    onAuthProcess: () => void;
};

const { DeauthInfoDbCard } = InterlayerApiProxy.getInfoDbCardApi();

export const DisableTehSupportInfoDb = ({ databaseId, onAuthProcess }: TProps) => {
    const { show } = useFloatMessages();

    const disableDatabaseSupport = async () => {
        try {
            const result = await DeauthInfoDbCard(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, { databaseId });

            if (result) {
                onAuthProcess();
            }
        } catch (err) {
            show(EMessageType.error, err);
        }
    };

    return (
        <OutlinedButton kind="error" onClick={ disableDatabaseSupport }>
            Отключить
        </OutlinedButton>
    );
};