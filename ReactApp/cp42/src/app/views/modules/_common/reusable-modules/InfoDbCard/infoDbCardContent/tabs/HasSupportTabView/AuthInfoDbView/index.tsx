import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { Accordion, AccordionDetails, Box, Link, MenuItem, Select } from '@mui/material';
import AccordionSummary from '@mui/material/AccordionSummary';
import { AccountUserGroup, SupportState } from 'app/common/enums';
import { TimerHelper } from 'app/common/helpers/TimerHelper';
import { ReceiveHasSupportThunk } from 'app/modules/infoDbCard/store/thunks/ReceiveHasSupportThunk';
import { EditInfoDbThunkParams } from 'app/modules/infoDbList/store/reducers/editInfoDbReducer/params';
import { AppReduxStoreState } from 'app/redux/types';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import CustomizedSwitches from 'app/views/components/controls/Switch/views/SwitchViews';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { TextOut } from 'app/views/components/TextOut';
import { CommonPasswordFieldView } from 'app/views/modules/_common/components/ComponentForPasswordField';
import { EnableTehSupportDatabaseForm } from 'app/views/modules/_common/reusable-modules/DatabaseCard/types';
import { visibleImprovements } from 'app/views/modules/AccountManagement/InformationBases/Permissions/CommonPage';
import { DisableTehSupportInfoDbButton } from 'app/views/modules/_common/reusable-modules/InfoDbCard/buttons/DisableTehSupportInfoDbButton';
import { EnableTehSupportInfoDbButton } from 'app/views/modules/_common/reusable-modules/InfoDbCard/buttons/EnableTehSupportInfoDbButton';
import { AutoUpdateTimeEnum } from 'app/views/modules/_common/reusable-modules/InfoDbCard/infoDbCardContent/tabs/HasSupportTabView/AuthInfoDbView/enum/AutoUpdateTimeEnum';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { HasSupportInfoDbDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/HasSupportInfoDbDataModel';
import { InfoDbCardAccountDatabaseInfoDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/InfoDbCardAccountDatabaseInfoDataModel';
import cn from 'classnames';
import { RequestKind } from 'core/requestSender/enums';
import React from 'react';
import { connect } from 'react-redux';

import style from './style.module.css';
import css from '../../../../styles.module.css';

type OwnProps = ReduxFormProps<EnableTehSupportDatabaseForm> & {
    databaseId: string;
};

type OwnState = {
    timeOfUpdate: number;
    completeSession: boolean;
    hasModifications: boolean;
    isLoading: boolean;
    hasSupport: boolean;
    hasAutoUpdate: boolean;
};

type StateProps = {
    hasSupportInfoDb: HasSupportInfoDbDataModel;
    hasHasSupportReceived: boolean;
    completeSession: boolean;
    timeOfUpdate: number;
    commonDetails: InfoDbCardAccountDatabaseInfoDataModel;
    currentUserGroups: AccountUserGroup[];
    isOwnDatabase: boolean;
};

type DispatchProps = {
    dispatchHasSupportThunk: (args: EditInfoDbThunkParams) => void;
};

type AllProps = OwnProps & StateProps & DispatchProps & FloatMessageProps ;

const autoupdateLink = 'https://42clouds.com/ru-ru/manuals/podklyuchenie-bazy-1s-k-avtomaticheskomu-obnovleniyu-ao';
const errorFixLink = 'https://42clouds.com/ru-ru/manuals/podklyuchenie-bazy-1s-k-avtomaticheskomu-testirovaniyu-i-ispravleniyu-oshibok-tii';

class AuthInfoDbViewClass extends React.Component<AllProps, OwnState> {
    private intervalId: NodeJS.Timeout | number;

    public constructor(props: AllProps) {
        super(props);
        this.onLoginPasswordInput = this.onLoginPasswordInput.bind(this);
        this.onAuthProcess = this.onAuthProcess.bind(this);
        this.initReduxForm = this.initReduxForm.bind(this);
        this.onValueChangeImprovements = this.onValueChangeImprovements.bind(this);
        this.intervalId = setInterval(() => void 0, TimerHelper.waitTimeRefreshData);

        this.state = {
            timeOfUpdate: this.props.timeOfUpdate,
            completeSession: this.props.completeSession,
            hasModifications: this.props.hasSupportInfoDb.hasModifications,
            isLoading: false,
            hasSupport: this.props.hasSupportInfoDb.hasSupport,
            hasAutoUpdate: this.props.hasSupportInfoDb.hasAutoUpdate
        };
    }

    public componentDidMount() {
        this.props.reduxForm.setInitializeFormDataAction(this.initReduxForm);

        if (this.props.hasSupportInfoDb.supportState !== SupportState.AutorizationProcessing) {
            clearInterval(this.intervalId);
        } else {
            clearInterval(this.intervalId);
            this.intervalId = setInterval(() => {
                this.props.dispatchHasSupportThunk({
                    databaseId: this.props.databaseId,
                    force: true,
                    showLoadingProgress: false,
                });
            }, TimerHelper.waitTimeRefreshData);
        }
    }

    public componentDidUpdate(): void {
        if (this.props.hasSupportInfoDb.supportState !== SupportState.AutorizationProcessing) {
            clearInterval(this.intervalId);
        } else {
            clearInterval(this.intervalId);

            this.intervalId = setInterval(() => {
                this.props.dispatchHasSupportThunk({
                    databaseId: this.props.databaseId,
                    force: true,
                    showLoadingProgress: false
                });
            }, TimerHelper.waitTimeRefreshData);
        }
    }

    public componentWillUnmount(): void {
        clearInterval(this.intervalId);
    }

    private onAuthProcess() {
        this.props.dispatchHasSupportThunk({
            databaseId: this.props.databaseId,
            force: true,
            showLoadingProgress: true
        });
    }

    private onLoginPasswordInput(fieldName: string, newValue: string) {
        this.props.reduxForm.updateReduxFormFields({
            [fieldName]: newValue
        });
    }

    private async onValueChangeImprovements() {
        const infoDbCardApi = InterlayerApiProxy.getInfoDbApi();

        this.setState({ isLoading: true });

        const response = await infoDbCardApi.editInfoDbList(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, {
            databaseId: this.props.commonDetails.id,
            hasModifications: !this.state.hasModifications
        });

        this.setState({ isLoading: false });

        if (response.Success) {
            this.setState(({ hasModifications }) => ({ hasModifications: !hasModifications }));
            this.props.floatMessage.show(MessageType.Success, 'Успешно');
        } else {
            this.props.floatMessage.show(MessageType.Error, response.Message);
        }
    }

    private initReduxForm(): EnableTehSupportDatabaseForm | undefined {
        if (!this.props.hasHasSupportReceived) {
            return;
        }

        return { username: '', password: '' };
    }

    public render() {
        const { currentUserGroups, reduxForm, isOwnDatabase } = this.props;

        const changedFields = reduxForm.getReduxFormFields(true);
        const isProjectManger = currentUserGroups.includes(AccountUserGroup.CloudAdmin) || currentUserGroups.includes(AccountUserGroup.Hotline) || currentUserGroups.includes(AccountUserGroup.CloudSE);
        const isSalesManager = currentUserGroups.includes(AccountUserGroup.AccountSaleManager);

        return (
            <>
                { this.state.isLoading && <LoadingBounce /> }
                <div className={ css['alert-wrapper'] }>
                    <Box display="flex" flexDirection="column" gap="16px" maxWidth="500px" width="100%">
                        { !this.props.hasSupportInfoDb.isConnects && (
                            <Box display="flex" flexDirection="column" gap="16px">
                                <CommonPasswordFieldView
                                    disable={ isSalesManager && !isOwnDatabase }
                                    formName="username"
                                    label="Пользователь 1C"
                                    onValueChange={ this.onLoginPasswordInput }
                                    value={ changedFields.username }
                                />
                                <CommonPasswordFieldView
                                    disable={ isSalesManager && !isOwnDatabase }
                                    formName="password"
                                    label="Пароль"
                                    onValueChange={ this.onLoginPasswordInput }
                                    value={ changedFields.password }
                                />
                            </Box>
                        ) }
                        { this.props.hasSupportInfoDb.isConnects && isProjectManger && (
                            <Accordion className={ style.accordion }>
                                <AccordionSummary expandIcon={ <ExpandMoreIcon /> }>
                                    <TextOut fontWeight={ 700 }>Авторизационные данные</TextOut>
                                </AccordionSummary>
                                <AccordionDetails>
                                    <Box display="flex" flexDirection="column" gap="16px">
                                        <CommonPasswordFieldView
                                            formName="username"
                                            label="Пользователь 1C"
                                            autoComplete="login"
                                            value={ this.props.hasSupportInfoDb.login }
                                            isReadOnly={ true }
                                        />
                                        <CommonPasswordFieldView
                                            formName="password"
                                            label="Пароль"
                                            autoComplete="off"
                                            value={ this.props.hasSupportInfoDb.password }
                                            isReadOnly={ true }
                                        />
                                    </Box>
                                </AccordionDetails>
                            </Accordion>
                        ) }
                        { visibleImprovements(this.props.hasSupportInfoDb.hasAcDbSupport) && !this.props.hasSupportInfoDb.isAuthorized && (
                            <Box display="flex" gap="16px" justifyContent="space-between">
                                <TextOut>База содержит доработки</TextOut>
                                <CustomizedSwitches
                                    disabled={ !isProjectManger }
                                    checked={ this.state.hasModifications }
                                    onChange={ this.onValueChangeImprovements }
                                />
                            </Box>
                        ) }
                        { this.props.hasSupportInfoDb.isAuthorized && (
                            <>
                                <FormAndLabel label="Выберите время автообновления по МСК">
                                    <Select
                                        disabled={ isSalesManager && !isOwnDatabase }
                                        id="autoUpdateTime"
                                        value={ this.state.timeOfUpdate }
                                        fullWidth={ true }
                                        onChange={ e => this.setState({ timeOfUpdate: Number(e.target.value) }) }
                                    >
                                        <MenuItem value={ AutoUpdateTimeEnum.day }>14:00 - 18:00</MenuItem>
                                        <MenuItem value={ AutoUpdateTimeEnum.evening }>18:00 - 21:00</MenuItem>
                                        <MenuItem value={ AutoUpdateTimeEnum.night }>21:00 - 06:00</MenuItem>
                                    </Select>
                                </FormAndLabel>
                                <Box display="flex" flexDirection="column" gap="10px">
                                    <Box display="flex" justifyContent="space-between">
                                        <TextOut>Завершать сеансы принудительно</TextOut>
                                        <CustomizedSwitches
                                            checked={ this.state.completeSession }
                                            disabled={ isSalesManager && !isOwnDatabase }
                                            onChange={ () => this.setState(({ completeSession }) => ({ completeSession: !completeSession })) }
                                        />
                                    </Box>
                                    { visibleImprovements(this.props.hasSupportInfoDb.hasAcDbSupport) && (
                                        <Box display="flex" gap="16px" justifyContent="space-between">
                                            <TextOut>База содержит доработки</TextOut>
                                            <CustomizedSwitches
                                                disabled={ !isProjectManger }
                                                checked={ this.state.hasModifications }
                                                onChange={ this.onValueChangeImprovements }
                                            />
                                        </Box>
                                    ) }
                                    <Box display="flex" gap="16px" justifyContent="space-between">
                                        <TextOut>Автообновление</TextOut>
                                        <CustomizedSwitches
                                            checked={ this.state.hasAutoUpdate }
                                            disabled={ isSalesManager && !isOwnDatabase }
                                            onChange={ () => this.setState(({ hasAutoUpdate }) => ({ hasAutoUpdate: !hasAutoUpdate })) }
                                        />
                                    </Box>
                                    <Box display="flex" gap="16px" justifyContent="space-between">
                                        <TextOut>ТиИ</TextOut>
                                        <CustomizedSwitches
                                            checked={ this.state.hasSupport }
                                            disabled={ isSalesManager && !isOwnDatabase }
                                            onChange={ () => this.setState(({ hasSupport }) => ({ hasSupport: !hasSupport })) }
                                        />
                                    </Box>
                                </Box>
                            </>
                        ) }
                        { (!isSalesManager || (isSalesManager && isOwnDatabase)) && (
                            <Box display="flex" gap="6px" justifyContent="flex-end">
                                { this.props.hasSupportInfoDb.isConnects && this.props.hasSupportInfoDb.isAuthorized && (
                                    <DisableTehSupportInfoDbButton databaseId={ this.props.databaseId } onAuthProcess={ this.onAuthProcess } />
                                ) }
                                <EnableTehSupportInfoDbButton
                                    databaseId={ this.props.databaseId }
                                    username={ changedFields.username }
                                    password={ changedFields.password }
                                    onAuthProcess={ this.onAuthProcess }
                                    timeOfUpdate={ this.state.timeOfUpdate }
                                    completeSession={ this.state.completeSession }
                                    isConnects={ this.props.hasSupportInfoDb.isConnects }
                                    isAuthorized={ this.props.hasSupportInfoDb.isAuthorized }
                                    hasSupport={ this.state.hasSupport }
                                    hasAutoUpdate={ this.state.hasAutoUpdate }
                                />
                            </Box>
                        ) }
                    </Box>
                    <Box display="flex" flexDirection="column" gap="20px">
                        { this.props.hasSupportInfoDb.isConnects && (
                            <div className={ cn({ [css['box-auth']]: this.props.hasSupportInfoDb.isConnects }) }>
                                <TextOut inDiv={ true }>
                                    <div dangerouslySetInnerHTML={ { __html: this.props.hasSupportInfoDb.supportStateDescription } } />
                                </TextOut>
                            </div>
                        ) }
                        <TextOut>
                            Настройка автоматического обновления (АО) и процесс тестирования и исправления (ТиИ) ошибок осуществляются в нерабочее время.
                            <br />
                            <br />
                            Ознакомиться с этим процессом можно в наших статьях:
                            <br />
                            <Link href={ autoupdateLink } target="blank" rel="noreferrer" style={ { fontWeight: 'bold' } }>
                                Автоматическое обновление
                            </Link> и&nbsp;
                            <Link href={ errorFixLink } target="blank" rel="noreferrer" style={ { fontWeight: 'bold' } }>
                                Тестирование и исправление ошибок
                            </Link>.
                        </TextOut>
                    </Box>
                </div>
            </>
        );
    }
}

const AuthInfoDbViewConnected = connect<StateProps, DispatchProps, OwnProps, AppReduxStoreState>(
    state => {
        const databaseCardState = state.InfoDbCardState;
        const { hasSupportInfoDb } = databaseCardState;
        const { hasHasSupportReceived } = databaseCardState.hasSuccessFor;

        const timeOfUpdate = hasSupportInfoDb.timeOfUpdate || 21;
        const completeSession = hasSupportInfoDb ? hasSupportInfoDb.completeSession : false;
        const { commonDetails } = databaseCardState;
        const { currentUserGroups, contextAccountId, accountId } = state.Global.getCurrentSessionSettingsReducer.settings.currentContextInfo;

        return {
            hasSupportInfoDb,
            hasHasSupportReceived,
            timeOfUpdate,
            completeSession,
            commonDetails,
            currentUserGroups,
            isOwnDatabase: contextAccountId === accountId
        };
    },
    {
        dispatchHasSupportThunk: ReceiveHasSupportThunk.invoke,
    }
)(AuthInfoDbViewClass);

export const AuthInfoDbView = withReduxForm(withFloatMessages(AuthInfoDbViewConnected), {
    reduxFormName: 'InfoDbCard_EnableTehSupportDatabase_Form'
});