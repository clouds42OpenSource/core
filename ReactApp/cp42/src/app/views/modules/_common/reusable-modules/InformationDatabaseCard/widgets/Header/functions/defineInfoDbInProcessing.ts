import { DatabaseState, DbPublishState } from 'app/common/enums';

export const defineInfoDbInProcessing = (state: number, publishState: number, endingSessions: boolean) => {
    return DatabaseState.Attaching === state ||
        DatabaseState.ProcessingSupport === state ||
        DatabaseState.TransferArchive === state ||
        DatabaseState.RestoringFromTomb === state ||
        DatabaseState.DelitingToTomb === state ||
        DatabaseState.DetachingToTomb === state ||
        DbPublishState.PendingPublication === publishState ||
        DbPublishState.PendingUnpublication === publishState ||
        DbPublishState.RestartingPool === publishState ||
        DatabaseState.NewItem === state ||
        endingSessions;
};