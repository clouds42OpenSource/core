import '@testing-library/jest-dom/extend-expect';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { SnackbarProvider } from 'notistack';

import { CopyBackupPathButton } from 'app/views/modules/_common/reusable-modules/DatabaseCard/views/_buttons/CopyBackupPathButton';

describe('<CopyBackupPathButton />', () => {
    let container: Element;

    beforeEach(() => {
        container = render(
            <SnackbarProvider>
                <CopyBackupPathButton inputElementId="test_element_id" />
            </SnackbarProvider>
        ).container;
    });

    it('renders', () => {
        expect(screen.getByTestId(/FileCopyOutlinedIcon/i)).toBeInTheDocument();
    });

    it('renders float message error', async () => {
        await userEvent.click(container.querySelector('[type=button]')!);
        expect(screen.getByText(/Не удалось скопировать путь в буфер обмена/i)).toBeInTheDocument();
    });
});