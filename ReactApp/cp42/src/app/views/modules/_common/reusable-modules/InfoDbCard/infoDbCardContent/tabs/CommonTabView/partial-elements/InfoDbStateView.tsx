import { DatabaseState } from 'app/common/enums';
import { ComboBoxForm } from 'app/views/components/controls/forms/ComboBox/ComboBoxForm';
import { ReadOnlyKeyValueView } from 'app/views/modules/_common/components/ReadOnlyKeyValueView';

type OwnProps = {
    label: string;
    formName: string;
    readOnlyState: DatabaseState;
    editState: DatabaseState;
    states: Array<{ key: number, value: string }>;
    isInEditMode: boolean;
    onValueChange: <TValue>(fieldName: string, newValue?: TValue, prevValue?: TValue) => boolean | void;
};

export const InfoDbStateView = (props: OwnProps) => {
    const editState = props.editState ?? props.readOnlyState;

    return props.isInEditMode && props.states.length
        ? (
            <ComboBoxForm
                formName={ props.formName }
                label={ props.label }
                value={ editState }
                items={ props.states.map(state => {
                    return {
                        value: +state.key,
                        text: state.value
                    };
                }) }
                onValueChange={ props.onValueChange }
            />
        )
        : (
            <ReadOnlyKeyValueView
                label={ props.label }
                text={ DatabaseState[props.readOnlyState] }
            />
        );
};