import { RequestType } from 'app/api/endpoints/eternalBackup/request';
import { FETCH_API } from 'app/api/useFetchApi';
import { EMessageType, useFloatMessages } from 'app/hooks';
import { OutlinedButton } from 'app/views/components/controls/Button';
import { Dialog } from 'app/views/components/controls/Dialog';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { TextOut } from 'app/views/components/TextOut';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { RequestKind } from 'core/requestSender/enums';
import React, { useState } from 'react';

type OwnProps = {
    databaseId: string;
    isDbOnDelimiters: boolean;
};

const { ArchiveOrBackup } = InterlayerApiProxy.getInfoDbCardApi();

export const TechnicalBackupButton = ({ databaseId, isDbOnDelimiters }: OwnProps) => {
    const { show } = useFloatMessages();

    const [isLoading, setIsLoading] = useState(false);
    const [isPressed, setIsPressed] = useState(false);
    const [isOpen, setIsOpen] = useState(false);

    const tryTechnicalBackupForDelimiter = async () => {
        try {
            setIsLoading(true);
            setIsPressed(true);
            await FETCH_API.ETERNAL_BACKUPS.postBackups({
                databaseId,
                force: true,
                type: RequestType.regular,
            });
            show(EMessageType.success, 'Технический бекап будет готов в течении 30 минут');
        } catch {
            setIsLoading(false);
            setIsPressed(false);
            show(EMessageType.error, 'Ошибка создания технического бекапа');
        } finally {
            setIsLoading(false);
        }
    };

    const tryTechnicalBackup = async () => {
        try {
            setIsLoading(true);
            setIsPressed(true);
            await ArchiveOrBackup(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, {
                databaseId,
                operation: 3,
            });
            show(EMessageType.success, 'Технический бекап будет готов в течении 30 минут');
        } catch {
            setIsLoading(false);
            setIsPressed(false);
            show(EMessageType.error, 'Ошибка создания технического бекапа');
        } finally {
            setIsLoading(false);
        }
    };

    const openDialog = () => {
        setIsOpen(true);
    };

    const closeDialog = () => {
        setIsOpen(false);
    };

    const handleBackup = () => {
        if (isDbOnDelimiters) {
            void tryTechnicalBackupForDelimiter();
        } else {
            void tryTechnicalBackup();
        }
        closeDialog();
    };

    return (
        <>
            { isLoading && <LoadingBounce /> }
            <OutlinedButton isEnabled={ !isPressed } kind="success" onClick={ openDialog }>
                Технический бекап
            </OutlinedButton>
            <Dialog
                title="Создание технического бекапа"
                isOpen={ isOpen }
                dialogWidth="sm"
                dialogVerticalAlign="center"
                onCancelClick={ closeDialog }
                buttons={ [
                    {
                        content: 'Отмена',
                        onClick: closeDialog,
                        kind: 'default'
                    },
                    {
                        content: 'Создать',
                        onClick: handleBackup,
                        kind: 'success'
                    }
                ] }
            >
                <TextOut inDiv={ true }>
                    Вы хотите создать технический бекап базы?
                </TextOut>
                <TextOut inDiv={ true }>Скачать бекап или восстановить базу вы сможете на вкладке - <b>«Архивные копии»</b>.</TextOut>
                <TextOut>Время создания бекапа зависит от ее размера и <b>может занимать больше 30 минут</b>.</TextOut>
            </Dialog>
        </>
    );
};