import { hasComponentChangesFor } from 'app/common/functions';
import { getErrorMessage } from 'app/common/functions/getErrorMessage';
import { yandexCounter, YandexMetrics } from 'app/common/_external-services/yandexMetrics';
import { ContainedButton } from 'app/views/components/controls/Button';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { DeleteInfoDbDialogMessage } from 'app/views/modules/AccountManagement/InformationBases/views/Modal/DeleteInfoDbDialogMessage';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { RequestKind } from 'core/requestSender/enums';
import React from 'react';

type OwnProps = FloatMessageProps & {
    databaseId: string;
    databaseName: string;
    onCancelDialogButtonClick: () => void;
    onDataSelect?: () => void
    onDataSelectOther?: () => void
};

type OwnState = {
    isDeleteDatabaseDialogOpen: boolean;
};

export class DeleteInfoDbButtonView extends React.Component<OwnProps, OwnState> {
    constructor(props: OwnProps) {
        super(props);
        this.deleteDatabase = this.deleteDatabase.bind(this);
        this.onCancelDeleteDatabase = this.onCancelDeleteDatabase.bind(this);
        this.onDeleteDatabaseConfirmed = this.onDeleteDatabaseConfirmed.bind(this);

        this.state = {
            isDeleteDatabaseDialogOpen: false
        };
    }

    public shouldComponentUpdate(nextProps: OwnProps, nextState: OwnState) {
        return hasComponentChangesFor(this.props, nextProps) || hasComponentChangesFor(this.state, nextState);
    }

    private async onDeleteDatabaseConfirmed() {
        const infoDbCardApi = InterlayerApiProxy.getInfoDbApi();

        try {
            await infoDbCardApi.deleteInfoDbList(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, {
                databasesId: [this.props.databaseId]
            });
            this.setState({
                isDeleteDatabaseDialogOpen: false
            });
            yandexCounter.getInstance().reachGoal(YandexMetrics.DeleteDatabaseMetric);
            if (this.props.onDataSelect) {
                this.props.onDataSelect();
            }
            if (this.props.onDataSelectOther) {
                this.props.onDataSelectOther();
            }
        } catch (e) {
            this.props.floatMessage.show(MessageType.Error, getErrorMessage(e));
        }
        this.props.onCancelDialogButtonClick();
    }

    private onCancelDeleteDatabase() {
        this.setState({
            isDeleteDatabaseDialogOpen: false
        });
    }

    private async deleteDatabase() {
        this.setState({
            isDeleteDatabaseDialogOpen: true
        });
    }

    public render() {
        return (
            <>
                <ContainedButton kind="error" onClick={ this.deleteDatabase }>
                    <i className="fa fa-trash" />&nbsp;Удалить
                </ContainedButton>

                <DeleteInfoDbDialogMessage
                    isOpenDialog={ this.state.isDeleteDatabaseDialogOpen }
                    databaseName={ this.props.databaseName }
                    onNoClick={ this.onCancelDeleteDatabase }
                    onYesClick={ this.onDeleteDatabaseConfirmed }
                />
            </>
        );
    }
}

export const DeleteInfoDbButton = withFloatMessages(DeleteInfoDbButtonView);