import { hasComponentChangesFor } from 'app/common/functions';
import { DatabaseCardProcessId } from 'app/modules/databaseCard';
import { PublishDatabaseThunkParams } from 'app/modules/databaseCard/store/reducers/publishDatabaseReducer/params';
import { PublishDatabaseThunk } from 'app/modules/databaseCard/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { OutlinedButton } from 'app/views/components/controls/Button';
import { WatchReducerProcessActionState } from 'app/views/modules/_common/components/WatchReducerProcessActionState';
import { IReducerProcessActionInfo } from 'core/redux/interfaces';
import React from 'react';
import { connect } from 'react-redux';

type OwnState = {
    publishDatabaseState: {
        actionState: IReducerProcessActionInfo;
        error?: Error;
    }
};

type OwnProps = FloatMessageProps & {
    databaseId: string;
    isInPublishingState: boolean;
    onSuccess?: () => void;
};

type DispatchProps = {
    dispatchPublishDatabaseThunk: (args?: PublishDatabaseThunkParams) => void;
};

type AllProps = OwnProps & DispatchProps;

export class PublishDatabaseButtonClass extends React.Component<AllProps, OwnState> {
    constructor(props: AllProps) {
        super(props);
        this.publishDatabase = this.publishDatabase.bind(this);
        this.onProcessActionStateChanged = this.onProcessActionStateChanged.bind(this);
        this.state = {
            publishDatabaseState: {
                actionState: {
                    isInErrorState: false,
                    isInProgressState: false,
                    isInSuccessState: false
                },
                error: undefined
            }
        };
    }

    public shouldComponentUpdate(nextProps: AllProps, nextState: OwnState) {
        return hasComponentChangesFor(this.props, nextProps) ||
            hasComponentChangesFor(this.state, nextState);
    }

    public componentDidUpdate(_prevProps: AllProps, prevState: OwnState) {
        if (this.state.publishDatabaseState.actionState.isInErrorState &&
            !prevState.publishDatabaseState.actionState.isInErrorState) {
            this.props.floatMessage.show(MessageType.Error, this.state.publishDatabaseState.error?.message);
        } else
            if (this.state.publishDatabaseState.actionState.isInSuccessState &&
                !prevState.publishDatabaseState.actionState.isInSuccessState) {
                this.props.floatMessage.show(MessageType.Default, 'Публикация информационной базы запущена.');
                if (this.props.onSuccess) this.props.onSuccess();
            }
    }

    private onProcessActionStateChanged(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId === DatabaseCardProcessId.PublishDatabase) {
            this.setState({
                publishDatabaseState: {
                    actionState,
                    error
                }
            });
        }
    }

    private async publishDatabase() {
        this.props.dispatchPublishDatabaseThunk({
            databaseId: this.props.databaseId,
            force: true
        });
    }

    public render() {
        return (
            <>
                {
                    this.props.isInPublishingState
                        ? (
                            <OutlinedButton kind="success" isEnabled={ false }>
                                База в процессе публикации
                            </OutlinedButton>
                        )
                        : (
                            <OutlinedButton kind="success" onClick={ this.publishDatabase }>
                                Опубликовать базу
                            </OutlinedButton>
                        )
                }
                <WatchReducerProcessActionState
                    watch={ [{
                        reducerStateName: 'DatabaseCardState',
                        processIds: [DatabaseCardProcessId.PublishDatabase]
                    }] }
                    onProcessActionStateChanged={ this.onProcessActionStateChanged }
                />
            </>
        );
    }
}

const PublishDatabaseButtonConnected = connect<NonNullable<unknown>, DispatchProps, OwnProps, AppReduxStoreState>(
    null,
    {
        dispatchPublishDatabaseThunk: PublishDatabaseThunk.invoke
    }
)(PublishDatabaseButtonClass);

export const PublishDatabaseButton = withFloatMessages(PublishDatabaseButtonConnected);