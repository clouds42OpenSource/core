import { emailRegex, phoneRegex, requiredMessage } from 'app/views/modules/_common/reusable-modules/BoxWithPicture/schemas';
import { transformPhoneValue } from 'app/views/modules/ToPartners/helpers';
import * as Yup from 'yup';

export const registrationNumberSchema = Yup.object().shape({
    configuration: Yup.string().required(requiredMessage),
    organization: Yup.string().required(requiredMessage),
    inn: Yup.string().min(10, 'Неверный формат').max(12, 'Неверный формат').required(requiredMessage),
    kpp: Yup.string().min(9, 'Неверный формат').max(9, 'Неверный формат'),
    companyActivities: Yup.string().required(requiredMessage),
    supervisor: Yup.string().required(requiredMessage),
    postalAddress: Yup.string().required(requiredMessage),
    phone: Yup.string().transform(transformPhoneValue).matches(phoneRegex, 'Неверный формат телефона').required(requiredMessage),
    email: Yup.string().matches(emailRegex, 'Неверный формат почты').required(requiredMessage),
    itsLogin: Yup.string().required(requiredMessage),
    itsEmail: Yup.string().required(requiredMessage)
});

export const registrationNumberInitialValues = {
    configuration: '',
    organization: '',
    inn: '',
    kpp: '',
    companyActivities: '',
    supervisor: '',
    postalAddress: '',
    phone: '',
    email: '',
    itsLogin: '',
    itsEmail: '',
};