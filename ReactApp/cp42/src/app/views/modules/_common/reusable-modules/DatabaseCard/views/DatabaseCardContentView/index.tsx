import { hasComponentChangesFor } from 'app/common/functions';
import { AppReduxStoreState } from 'app/redux/types';
import { IReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { TabsControl } from 'app/views/components/controls/TabsControl';
import { DatabaseCardEditDatabaseDataForm } from 'app/views/modules/_common/reusable-modules/DatabaseCard/types';
import { CommandsView } from 'app/views/modules/_common/reusable-modules/DatabaseCard/views/CommandsView';
import {
    AuthenticationTabView
} from 'app/views/modules/_common/reusable-modules/DatabaseCard/views/tabs/AuthenticationTabView';
import { CommonTabView } from 'app/views/modules/_common/reusable-modules/DatabaseCard/views/tabs/CommonTabView';
import {
    ReserveDatabaseCopyTabView
} from 'app/views/modules/_common/reusable-modules/DatabaseCard/views/tabs/ReserveDatabaseCopyTabView';
import {
    DatabaseCardTabVisibilityDataModel
} from 'app/web/InterlayerApiProxy/DatabaseCardApiProxy/receiveDatabaseCard/data-models';
import React from 'react';
import { connect } from 'react-redux';

type OwnProps = {
    databaseDataTabRef: React.Ref<IReduxForm<DatabaseCardEditDatabaseDataForm>>;
    isReadonlyDatabaseInfo?: boolean;
};

type StateProps = {
    tabVisibility: DatabaseCardTabVisibilityDataModel;
};

type AllProps = OwnProps & StateProps;

class DatabaseCardContentViewClass extends React.Component<AllProps> {
    public shouldComponentUpdate(nextProps: OwnProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    public render() {
        return (
            <>
                <CommandsView />
                <TabsControl
                    headers={ [
                        {
                            title: 'Общие',
                            isVisible: true
                        }, {
                            title: 'Архивные копии',
                            isVisible: this.props.tabVisibility.isBackupTabVisible
                        }, {
                            title: 'Авторизация',
                            isVisible: this.props.tabVisibility.isTehSupportTabVisible
                        }
                    ] }
                >
                    <CommonTabView ref={ this.props.databaseDataTabRef } isReadonlyDatabaseInfo={ this.props.isReadonlyDatabaseInfo } />
                    { this.props.tabVisibility.isBackupTabVisible ? <ReserveDatabaseCopyTabView /> : <span /> }
                    { this.props.tabVisibility.isTehSupportTabVisible ? <AuthenticationTabView /> : <span /> }
                </TabsControl>
            </>
        );
    }
}

export const DatabaseCardContentView = connect<StateProps, NonNullable<unknown>, OwnProps, AppReduxStoreState>(
    state => {
        const databaseCardState = state.DatabaseCardState;
        const { tabVisibility } = databaseCardState;
        const { commonDetails } = databaseCardState;
        return {
            tabVisibility,
            commonDetails
        };
    }
)(DatabaseCardContentViewClass);