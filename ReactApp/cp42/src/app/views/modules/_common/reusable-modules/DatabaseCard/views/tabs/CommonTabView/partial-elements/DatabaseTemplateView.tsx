import { ComboBoxForm } from 'app/views/components/controls/forms/ComboBox/ComboBoxForm';
import { ReadOnlyKeyValueView } from 'app/views/modules/_common/components/ReadOnlyKeyValueView';
import { KeyValueDataModel } from 'app/web/common/data-models';
import React from 'react';

type OwnProps = {
    label: string;
    formName: string;
    readOnlyDatabaseTemplateName: string;
    readOnlyDatabaseTemplateId: string;
    editDatabaseTemplateId: string;
    templates: Array<KeyValueDataModel<string, string>>;
    isInEditMode: boolean;
    onValueChange: <TValue>(fieldName: string, newValue?: TValue, prevValue?: TValue) => boolean | void;
};
export const DatabaseTemplateView = (props: OwnProps) => {
    const editDatabaseTemplateId = props.editDatabaseTemplateId ?? props.readOnlyDatabaseTemplateId;
    return props.isInEditMode && props.templates.length
        ? (
            <ComboBoxForm
                formName={ props.formName }
                label={ props.label }
                value={ editDatabaseTemplateId }
                items={ props.templates.map(item => {
                    return {
                        value: item.key,
                        text: item.value
                    };
                }) }
                onValueChange={ props.onValueChange }
            />
        )
        : (
            <ReadOnlyKeyValueView
                label={ props.label }
                text={ props.readOnlyDatabaseTemplateName }
            />
        );
};