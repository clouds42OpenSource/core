import { ExtensionStateServicesEnum } from 'app/common/enums/ExtensionStateServicesEnum';
import { COLORS } from 'app/utils';
import { TextOut } from 'app/views/components/TextOut';
import { CSSProperties } from 'react';

type TProps = {
    extensionState: ExtensionStateServicesEnum;
};

const STYLE: CSSProperties = { color: COLORS.main, display: 'flex', gap: '3px', alignItems: 'flex-start' };

export const ServiceState = ({ extensionState }: TProps) => {
    switch (extensionState) {
        case ExtensionStateServicesEnum.ProcessingInstall:
            return (
                <TextOut inDiv={ true } fontSize={ 11 } style={ STYLE }>
                    Расширение в процессе установки
                    <img src="img/database-icons/animated-clock.gif" width={ 13 } height={ 13 } alt="timer" />
                </TextOut>
            );
        case ExtensionStateServicesEnum.ProcessingDelete:
            return (
                <TextOut inDiv={ true } fontSize={ 11 } style={ { ...STYLE, color: COLORS.error } }>
                    Расширение в процессе удаления
                    <img src="img/database-icons/animated-clock.gif" width={ 13 } height={ 13 } alt="timer" />
                </TextOut>
            );
        case ExtensionStateServicesEnum.ErrorProcessingDelete:
            return (
                <TextOut fontSize={ 11 } style={ { color: COLORS.error } }>Ошибка в процессе удаления</TextOut>
            );
        case ExtensionStateServicesEnum.ErrorProcessingInstall:
            return (
                <TextOut fontSize={ 11 } style={ { color: COLORS.error } }>Ошибка в процессе установки</TextOut>
            );
        default:
            return null;
    }
};