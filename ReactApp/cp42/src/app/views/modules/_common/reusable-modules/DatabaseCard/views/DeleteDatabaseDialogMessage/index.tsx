import { hasComponentChangesFor } from 'app/common/functions';
import { ContainedButton } from 'app/views/components/controls/Button';
import { DialogMessage } from 'app/views/components/controls/DialogMessage';
import cn from 'classnames';
import React from 'react';
import css from './styles.module.css';

type OwnProps = {
    isOpen: boolean;
    databaseName: string;
    onNoClick: () => void;
    onYesClick: () => void;
};
export class DeleteDatabaseDialogMessage extends React.Component<OwnProps> {
    public shouldComponentUpdate(nextProps: OwnProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    public render() {
        return (
            <DialogMessage
                isOpen={ this.props.isOpen }
                dialogWidth="xs"
                onCancelClick={ this.props.onNoClick }
            >
                <div className={ cn(css['warning-icon-container'], css.pulseWarning) }>
                    <span className={ cn(css['warning-icon-circle'], css.pulseWarningIns) } />
                    <span className={ cn(css['warning-icon-dot'], css.pulseWarningIns) } />
                </div>
                <h2 className={ cn(css['sweet-alert-title']) }>
                    Подтвердите удаление
                </h2>
                <p className={ cn(css['sweet-alert-body']) }>
                    База &quot;{ this.props.databaseName }&quot; будет удалена и недоступна для работы.
                    Также будут удалены данные резервного копирования,
                    настройки доступа к информационной базе и другие связанные объекты.
                </p>
                <div style={ { textAlign: 'center', margin: '26px 0' } }>
                    <ContainedButton
                        kind="default"
                        className={ cn(css['sweet-alert-button']) }
                        onClick={ this.props.onNoClick }
                    >
                        Нет
                    </ContainedButton>
                    <ContainedButton
                        kind="error"
                        className={ cn(css['sweet-alert-button'], css['sweet-alert-button-yes']) }
                        onClick={ this.props.onYesClick }
                    >
                        Да
                    </ContainedButton>
                </div>
            </DialogMessage>
        );
    }
}