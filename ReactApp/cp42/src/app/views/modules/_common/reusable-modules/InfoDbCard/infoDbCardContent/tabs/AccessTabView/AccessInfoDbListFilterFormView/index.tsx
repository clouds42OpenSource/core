import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { Accordion, AccordionDetails, AccordionSummary, Box } from '@mui/material';
import { COLORS } from 'app/utils';
import { GroupAccessChangeDialog } from 'app/views/modules/_common/reusable-modules/InfoDbCard/infoDbCardContent/tabs/AccessTabView/GroupAccessChangeDialog';
import React from 'react';
import { connect } from 'react-redux';
import { RequestKind } from 'core/requestSender/enums';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { AccountDatabaseListFilterDataFormFieldNamesType } from 'app/views/modules/DatabaseList/types';
import { hasComponentChangesFor } from 'app/common/functions';
import { getErrorMessage } from 'app/common/functions/getErrorMessage';
import { AppReduxStoreState } from 'app/redux/types';
import { SuccessButton } from 'app/views/components/controls/Button';
import { ButtonGroup } from 'app/views/components/controls/ButtonGroup';
import { TextBoxForm } from 'app/views/components/controls/forms/TextBoxForm';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { TextOut } from 'app/views/components/TextOut';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { InfoDbAccessFilterDataForm } from 'app/views/modules/AccountManagement/InformationBases/types/InfoDbAccessFilterDataForm';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { EternalUserDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/EternalUserDataModel';
import { InfoDbCardAccountDatabaseInfoDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/InfoDbCardAccountDatabaseInfoDataModel';

import css from '../../../../styles.module.css';
import localStyles from './style.module.css';

type OwnProps = FloatMessageProps & ReduxFormProps<InfoDbAccessFilterDataForm> & {
    onFilterChanged: (filter: InfoDbAccessFilterDataForm, filterFormName: string) => void;
    isFilterFormDisabled: boolean;
    addUsers: (result: EternalUserDataModel) => void;
    onTabChange: (value: number) => void;
    onValueChange: <TValue, TForm extends string = AccountDatabaseListFilterDataFormFieldNamesType>(formName: TForm, newValue: TValue) => void;
    tabValue: number;
    isOnDelimiters?: boolean;
    isAdmin?: boolean;
    isGroupChange?: boolean;
};

type StateProps = {
    commonDetails: InfoDbCardAccountDatabaseInfoDataModel;
};

type OwnState = {
    tabValue: number;
    error?: string;
    loading: boolean;
    openGroupAccessChange: boolean;
};

type AllProps = StateProps & OwnProps;

class AccessInfoDbListFilterFormViewClass extends React.Component<AllProps, OwnState> {
    private readonly enterKeyName = 'enter';

    private items = [
        { index: 0, text: 'Все пользователи' },
        { index: 1, text: 'С доступом' },
        { index: 2, text: 'Без доступа' },
        { index: 3, text: 'Внешние пользователи' },
    ];

    public constructor(props: AllProps) {
        super(props);

        this.state = {
            tabValue: 0,
            error: '',
            loading: false,
            openGroupAccessChange: false
        };

        this.initReduxForm = this.initReduxForm.bind(this);
        this.applyFilter = this.applyFilter.bind(this);
        this.handleEnterKey = this.handleEnterKey.bind(this);
        this.onSearchUser = this.onSearchUser.bind(this);
        this.onValueChangeUserAdd = this.onValueChangeUserAdd.bind(this);
        this.onInputKeyDown = this.onInputKeyDown.bind(this);
        this.openGroupAccessChangeDialog = this.openGroupAccessChangeDialog.bind(this);
        this.closeGroupAccessChangeDialog = this.closeGroupAccessChangeDialog.bind(this);
    }

    public componentDidMount() {
        this.props.reduxForm.setInitializeFormDataAction(this.initReduxForm);
        document.body.addEventListener('keydown', this.handleEnterKey);
    }

    public shouldComponentUpdate(nextProps: OwnProps, nextState: OwnState) {
        return hasComponentChangesFor(this.props, nextProps) || hasComponentChangesFor(this.state, nextState);
    }

    public componentWillUnmount() {
        document.body.removeEventListener('keydown', this.handleEnterKey);
    }

    private handleEnterKey(event: KeyboardEvent) {
        if (event.key !== 'Enter') {
            return;
        }

        event.preventDefault();
        this.applyFilter();
    }

    private onInputKeyDown(ev: React.KeyboardEvent<HTMLInputElement>) {
        if (ev.key.toLowerCase() === this.enterKeyName) {
            void this.onSearchUser();
        }
    }

    private onValueChangeUserAdd<TValue, TForm extends string = AccountDatabaseListFilterDataFormFieldNamesType>(formName: TForm, newValue: TValue) {
        this.props.reduxForm.updateReduxFormFields({
            [formName]: newValue
        });
    }

    private async onSearchUser() {
        this.setState({
            error: '',
            loading: true
        });

        const infoDbCardApi = InterlayerApiProxy.getInfoDbCardApi();

        try {
            const result = await infoDbCardApi.GetExternalUserInfoDbCard(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, {
                databaseId: this.props.commonDetails.id,
                accountUserEmail: this.props.reduxForm.getReduxFormFields(false).searchUser,
            });

            if (Object.hasOwn(result, 'message')) {
                this.setState({
                    error: result.message
                });
            }

            if (Object.hasOwn(result, 'userId')) {
                this.props.reduxForm.updateReduxFormFields({
                    searchUser: ''
                });
            }

            this.props.addUsers(result);
        } catch (e: unknown) {
            this.props.floatMessage.show(MessageType.Error, getErrorMessage(e));
        }

        this.setState({
            loading: false
        });
    }

    private applyFilter() {
        this.props.onFilterChanged({
            ...this.props.reduxForm.getReduxFormFields(false)
        }, '');
    }

    private initReduxForm(): InfoDbAccessFilterDataForm | undefined {
        return {
            searchLineUser: '',
            searchUser: ''
        };
    }

    private openGroupAccessChangeDialog() {
        this.setState({
            openGroupAccessChange: true
        });
    }

    private closeGroupAccessChangeDialog() {
        this.setState({
            openGroupAccessChange: false
        });
    }

    public render() {
        const formFields = this.props.reduxForm.getReduxFormFields(false);

        return (
            <>
                { this.state.loading && <LoadingBounce /> }
                <Box display="flex" flexDirection="column" gap="8px">
                    {
                        !this.props.isAdmin && !this.props.isGroupChange && (
                            <Accordion className={ localStyles.accordion }>
                                <AccordionSummary expandIcon={ <ExpandMoreIcon /> } className={ localStyles.accordionSummary }>
                                    <TextOut fontWeight={ 700 }>Предоставить доступ пользователю внешнего аккаунта</TextOut>
                                </AccordionSummary>
                                <AccordionDetails>
                                    <Box display="flex" gap="16px">
                                        <Box display="flex" flexDirection="column" gap="8px" width="100%">
                                            <TextBoxForm
                                                formName="searchUser"
                                                fullWidth={ true }
                                                onValueChange={ this.onValueChangeUserAdd }
                                                value={ formFields.searchUser }
                                                isReadOnly={ this.props.isFilterFormDisabled }
                                                placeholder="Введите эл. почту пользователя"
                                                onKeyDown={ this.onInputKeyDown }
                                            />
                                            { this.state.error &&
                                                <TextOut fontSize={ 11 } fontWeight={ 400 } style={ { color: COLORS.error } }>
                                                    <div dangerouslySetInnerHTML={ { __html: this.state.error } } />
                                                </TextOut>
                                            }
                                        </Box>
                                        <SuccessButton onClick={ this.onSearchUser } className={ localStyles.accessButton }>
                                            <i className="fa fa-plus-circle mr-1" />
                                            Предоставить доступ
                                        </SuccessButton>
                                    </Box>
                                </AccordionDetails>
                            </Accordion>
                        )
                    }
                    <FormAndLabel label="Поиск" fullWidth={ true } className={ css.searchField }>
                        <TextBoxForm
                            fullWidth={ true }
                            formName="searchLineUser"
                            onValueChange={ this.props.onValueChange }
                            value={ formFields.searchLineUser }
                            isReadOnly={ this.props.isFilterFormDisabled }
                            placeholder="Поиск пользователя по логину, эл. почте или ФИО (отдельно)"
                        />
                    </FormAndLabel>
                    {
                        !this.props.isGroupChange && (
                            <Box display="flex" gap="2px" alignItems="flex-end">
                                <ButtonGroup
                                    items={ this.items }
                                    selectedButtonIndex={ this.props.tabValue }
                                    label="Быстрые отборы"
                                    onClick={ this.props.onTabChange }
                                    fullWidth={ true }
                                />
                                {
                                    this.props.isOnDelimiters && (
                                        <SuccessButton style={ { minWidth: 'auto', marginLeft: '36px' } } onClick={ this.openGroupAccessChangeDialog }>
                                            Групповое изменение профилей
                                        </SuccessButton>
                                    )
                                }
                            </Box>
                        )
                    }
                </Box>
                <GroupAccessChangeDialog
                    isOpen={ this.state.openGroupAccessChange }
                    onCancelClick={ this.closeGroupAccessChangeDialog }
                />
            </>
        );
    }
}

export const AccessInfoDbListFilterFormConnected = connect<StateProps, NonNullable<unknown>, OwnProps, AppReduxStoreState>(
    state => {
        return {
            commonDetails: state.InfoDbCardState.commonDetails
        };
    },
    {}
)(AccessInfoDbListFilterFormViewClass);

export const AccessInfoDbListFilterFormView = withFloatMessages(AccessInfoDbListFilterFormConnected);

export const AccessInfoDbListFilterForm = withReduxForm(AccessInfoDbListFilterFormView, {
    reduxFormName: 'Access_Info_Db_List_Filter_Form',
    resetOnUnmount: true
});