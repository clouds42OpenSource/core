import FileCopyOutlinedIcon from '@mui/icons-material/FileCopyOutlined';
import { hasComponentChangesFor } from 'app/common/functions';
import { HtmlDomUtility } from 'app/utils/HtmlDomUtility';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import cn from 'classnames';
import React from 'react';
import css from '../styles.module.css';

type OwnProps = FloatMessageProps & {
    inputElementId: string;
};

class CopyBackupPathButtonClass extends React.Component<OwnProps> {
    constructor(props: OwnProps) {
        super(props);
        this.copyPath = this.copyPath.bind(this);
    }

    public shouldComponentUpdate(nextProps: OwnProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private copyPath() {
        if (HtmlDomUtility.copyTextFrom(document.getElementById(this.props.inputElementId) as HTMLInputElement)) {
            this.props.floatMessage.show(MessageType.Default, 'Путь скопирован в буфер обмена', 700);
        } else {
            this.props.floatMessage.show(MessageType.Error, 'Не удалось скопировать путь в буфер обмена', 700);
        }
    }

    public render() {
        return (
            <span title="Скопировать в буфер обмена" className={ cn(css['copy-icon']) }>
                <button onClick={ this.copyPath } className={ cn(css['copy-icon']) } type="button"><FileCopyOutlinedIcon fontSize="inherit" /></button>
            </span>

        );
    }
}

export const CopyBackupPathButton = withFloatMessages(CopyBackupPathButtonClass);