import { AccountDatabaseAccessState } from 'app/common/enums/AccountDatabaseAccessStateEnum';
import React from 'react';

type TProps = {
    extensionAccess: AccountDatabaseAccessState | null
};

export const DbStateLineAccess = ({ extensionAccess }: TProps) => {
    if (extensionAccess === AccountDatabaseAccessState.ProcessingGrant) {
        return (
            <div style={ { color: '#6CCDBA', fontSize: '11px' } }>
                <img src="img/database-icons/animated-clock.gif" alt="timer" style={ { width: '16px', height: '16px' } } />
                &nbsp; В процессе выдачи
            </div>
        );
    }

    if (extensionAccess === AccountDatabaseAccessState.ProcessingDelete) {
        return (
            <div style={ { color: 'red', fontSize: '11px' } }>
                <img src="img/database-icons/animated-clock.gif" alt="timer" style={ { width: '16px', height: '16px' } } />
                &nbsp; В процессе удаления
            </div>
        );
    }

    if (extensionAccess === AccountDatabaseAccessState.Error) {
        return (
            <div style={ { color: 'red', fontSize: '11px' } }>Ошибка</div>
        );
    }

    return null;
};