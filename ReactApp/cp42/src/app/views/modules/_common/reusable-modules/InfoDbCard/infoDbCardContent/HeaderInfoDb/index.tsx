import { Box, Tooltip } from '@mui/material';
import { TourProps, withTour } from '@reactour/tour';
import { TLocale } from 'app/api/endpoints/global/response';
import { AccountUserGroup, DatabaseState, DbPublishState } from 'app/common/enums';
import { hasComponentChangesFor } from 'app/common/functions';
import { ReceiveAccessThunkParams } from 'app/modules/infoDbCard/store/reducers/receiveAccessInfoDbCardReducer/params';
import { ReceiveAccessThunk } from 'app/modules/infoDbCard/store/thunks/ReceiveAccessCardThunk';
import { AppReduxStoreState } from 'app/redux/types';
import { COLORS } from 'app/utils';
import { HomeSvgSelector } from 'app/views/components/svgGenerator/HomeSvgSelector';
import { TextOut } from 'app/views/components/TextOut';
import { DATABASE_CARD_STEPS } from 'app/views/Layout/ProjectTour/constants';
import { ETourStages } from 'app/views/Layout/ProjectTour/enums';
import { ArchiveOrBackupButton } from 'app/views/modules/_common/reusable-modules/InfoDbCard/buttons/ArchiveOrBackupButton';
import { ChangeTypeButton } from 'app/views/modules/_common/reusable-modules/InfoDbCard/buttons/ChangeTypeButton';
import { ChangeTypeRequestButton } from 'app/views/modules/_common/reusable-modules/InfoDbCard/buttons/ChangeTypeRequestButton';
import { ClearCacheButton } from 'app/views/modules/_common/reusable-modules/InfoDbCard/buttons/ClearCacheButton';
import { DeleteInfoDbButton } from 'app/views/modules/_common/reusable-modules/InfoDbCard/buttons/DeleteInfoDbButton';
import { DtUploadButton } from 'app/views/modules/_common/reusable-modules/InfoDbCard/buttons/DtUploadButton';
import { EndingSessionsButton } from 'app/views/modules/_common/reusable-modules/InfoDbCard/buttons/EndingSessionsButton';
import { PublishInfoDbButton } from 'app/views/modules/_common/reusable-modules/InfoDbCard/buttons/PublishInfoDbButton';
import { RestartPoolIisButton } from 'app/views/modules/_common/reusable-modules/InfoDbCard/buttons/RestartPoolIisButton';
import { RestoreInfoDbButton } from 'app/views/modules/_common/reusable-modules/InfoDbCard/buttons/RestoreInfoDbButton';
import { TechnicalBackupButton } from 'app/views/modules/_common/reusable-modules/InfoDbCard/buttons/TechnicalBackupButton';
import { UpdateDatabaseButton } from 'app/views/modules/_common/reusable-modules/InfoDbCard/buttons/UpdateDatabaseButton';
import { defineInfoDbInProccessing } from 'app/views/modules/_common/reusable-modules/InfoDbCard/functions/defineInfoDbInProccessing';
import { authorizationTab } from 'app/views/modules/AccountManagement/InformationBases/Permissions/getPermissionsTabs';
import css from 'app/views/modules/AccountManagement/InformationBases/views/styles.module.css';
import { GetAvailabilityInfoDb } from 'app/web/api/InfoDbProxy/responce-dto/GetAvailabilityInfoDb';
import { InfoDbCardPermissionsEnum } from 'app/web/common/enums/InfoDbCardPermissionsEnum';
import { InfoDbListItemDataModel } from 'app/web/InterlayerApiProxy/InfoDbApiProxy/receiveDatabaseList/data-models';
import { HasSupportInfoDbDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/HasSupportInfoDbDataModel';
import { InfoDbCardAccountDatabaseInfoDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/InfoDbCardAccountDatabaseInfoDataModel';
import { RightsInfoDbCardDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getRightsInfoDbCard/data-models/InfoDbCardAccountDatabaseInfoDataModel';

import cn from 'classnames';
import React from 'react';
import { connect } from 'react-redux';
import { StaticRouterProps } from 'react-router';

type StateProps = {
    commonDetails: InfoDbCardAccountDatabaseInfoDataModel;
    currentUserGroups: AccountUserGroup[];
    hasSupportInfoDb: HasSupportInfoDbDataModel;
    locale: TLocale;
};

type OwnProps = {
    onChangeField: () => void
    item?: InfoDbListItemDataModel | null;
    commonDetails: InfoDbCardAccountDatabaseInfoDataModel;
    permissions: RightsInfoDbCardDataModel;
    onCancelDialogButtonClick: () => void;
    onDataSelect?: () => void;
    onDataSelectOther?: () => void;
    availability: GetAvailabilityInfoDb;
    isOther: boolean;
    goToAuTab: () => void;
};

type DispatchProps = {
    dispatchAccessCardThunk: (args: ReceiveAccessThunkParams) => void;
};

type AllProps = StateProps & StaticRouterProps & OwnProps & DispatchProps & Partial<TourProps>;

class HeaderInfoDbClass extends React.Component<AllProps> {
    public constructor(props: AllProps) {
        super(props);
        this.renderPublishDatabaseButton = this.renderPublishDatabaseButton.bind(this);
        this.renderDeleteDatabaseButton = this.renderDeleteDatabaseButton.bind(this);
        this.renderRestartPoolIisButton = this.renderRestartPoolIisButton.bind(this);
        this.renderChangeTypeButton = this.renderChangeTypeButton.bind(this);
        this.renderChangeTypeRequestButton = this.renderChangeTypeRequestButton.bind(this);
        this.renderEndingSessionsButton = this.renderEndingSessionsButton.bind(this);
        this.renderRestoreInfoDbButton = this.renderRestoreInfoDbButton.bind(this);
        this.updateAccessCard = this.updateAccessCard.bind(this);
        this.renderClearCacheTypeButton = this.renderClearCacheTypeButton.bind(this);
        this.openTour = this.openTour.bind(this);
        this.renderUpdateDatabaseButton = this.renderUpdateDatabaseButton.bind(this);
    }

    public shouldComponentUpdate(nextProps: AllProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private updateAccessCard() {
        this.props.dispatchAccessCardThunk({
            databaseId: this.props.item?.databaseId ?? this.props.commonDetails.id ?? '',
            searchString: '',
            usersByAccessFilterType: 0,
            force: true,
        });
    }

    private openTour() {
        if (this.props.setIsOpen && this.props.setMeta && this.props.setSteps && this.props.setCurrentStep) {
            this.props.setCurrentStep(0);
            this.props.setSteps(DATABASE_CARD_STEPS);
            this.props.setMeta(ETourStages.databaseCard);
            this.props.setIsOpen(true);
        }
    }

    private renderRestoreInfoDbButton() {
        if (
            this.props.permissions.includes(InfoDbCardPermissionsEnum.AccountDatabase_Restore) &&
            this.props.commonDetails.databaseState === DatabaseState.DetachedToTomb &&
            !this.props.commonDetails.isDbOnDelimiters
        ) {
            return (
                <RestoreInfoDbButton
                    backupId={ this.props.commonDetails.actualDatabaseBackupId }
                />
            );
        }
    }

    private renderDtUploadButton() {
        const { commonDetails: { isDbOnDelimiters, v82Name, id, databaseState }, currentUserGroups, item } = this.props;
        const userGroupRule = !((currentUserGroups.includes(AccountUserGroup.AccountUser) || currentUserGroups.includes(AccountUserGroup.AccountSaleManager)) && currentUserGroups.length === 1);

        if (isDbOnDelimiters && userGroupRule && databaseState === DatabaseState.Ready) {
            return (
                <DtUploadButton
                    databaseId={ item?.databaseId ?? id ?? '' }
                    v82name={ v82Name }
                />
            );
        }
    }

    private renderPublishDatabaseButton() {
        if (
            this.props.commonDetails.databaseState === DatabaseState.Ready &&
            this.props.permissions.includes(InfoDbCardPermissionsEnum.AccountDatabases_Publish) &&
            this.props.commonDetails.canWebPublish &&
            !this.props.commonDetails.isDbOnDelimiters &&
            this.props.availability.IsMainServiceAllowed
        ) {
            return (
                <PublishInfoDbButton
                    canWebPublish={ this.props.commonDetails.canWebPublish }
                    onChangeField={ this.props.onChangeField }
                    databaseId={ this.props.item?.databaseId ?? this.props.commonDetails.id ?? '' }
                    isInPublishingState={ this.props.commonDetails.publishState }
                    usedWebServices={ this.props.commonDetails.usedWebServices }
                />
            );
        }
    }

    private renderEndingSessionsButton() {
        if (
            this.props.commonDetails.databaseState === DatabaseState.Ready &&
            this.props.permissions.includes(InfoDbCardPermissionsEnum.AccountDatabase_TerminateSessions) &&
            this.props.availability.IsMainServiceAllowed
        ) {
            return (
                <EndingSessionsButton
                    databaseName={ this.props.commonDetails.caption }
                    onChangeField={ this.props.onChangeField }
                    databaseId={ this.props.item?.databaseId ?? this.props.commonDetails.id ?? '' }
                    isInPublishingState={ this.props.commonDetails.isExistTerminatingSessionsProcessInDatabase }
                />
            );
        }
    }

    private renderDeleteDatabaseButton() {
        if (
            this.props.commonDetails.databaseState !== DatabaseState.DelitingToTomb &&
            this.props.permissions.includes(InfoDbCardPermissionsEnum.AccountDatabase_DeleteToTomb)
        ) {
            return (
                <DeleteInfoDbButton
                    databaseId={ this.props.item?.databaseId ?? this.props.commonDetails.id ?? '' }
                    databaseName={ this.props.item?.name ?? this.props.commonDetails.v82Name ?? '' }
                    onCancelDialogButtonClick={ this.props.onCancelDialogButtonClick }
                    onDataSelect={ this.props.onDataSelect }
                    onDataSelectOther={ this.props.onDataSelectOther }
                />
            );
        }
    }

    private renderRestartPoolIisButton() {
        if (
            this.props.permissions.includes(InfoDbCardPermissionsEnum.AccountDatabases_RestartIisAppPool) &&
            this.props.commonDetails.publishState === DbPublishState.Published &&
            !this.props.commonDetails.isDbOnDelimiters
        ) {
            return (
                <RestartPoolIisButton
                    onChangeField={ this.props.onChangeField }
                    databaseId={ this.props.item?.databaseId ?? this.props.commonDetails.id ?? '' }
                    isInPublishingState={ this.props.commonDetails.publishState }
                />
            );
        }
    }

    private renderChangeTypeButton() {
        if (
            this.props.permissions.includes(InfoDbCardPermissionsEnum.AccountDatabases_EditDbType) &&
            this.props.commonDetails.databaseState === DatabaseState.Ready &&
            this.props.commonDetails.publishState !== DbPublishState.PendingUnpublication &&
            this.props.commonDetails.publishState !== DbPublishState.PendingPublication &&
            (
                this.props.currentUserGroups.includes(AccountUserGroup.CloudAdmin) ||
                this.props.currentUserGroups.includes(AccountUserGroup.Hotline)
            ) &&
            !this.props.commonDetails.isDbOnDelimiters
        ) {
            return (
                <ChangeTypeButton
                    isFile={ this.props.commonDetails.isFile }
                    databaseName={ this.props.commonDetails.caption }
                    databaseId={ this.props.item?.databaseId ?? this.props.commonDetails.id ?? '' }
                    onChangeField={ this.props.onChangeField }
                    updateAccessCard={ this.updateAccessCard }
                />
            );
        }
    }

    private renderChangeTypeRequestButton() {
        if (
            this.props.permissions.includes(InfoDbCardPermissionsEnum.AccountDatabase_Change) &&
            this.props.commonDetails.databaseState === DatabaseState.Ready &&
            this.props.commonDetails.publishState !== DbPublishState.PendingUnpublication &&
            this.props.commonDetails.publishState !== DbPublishState.PendingPublication &&
            (
                this.props.currentUserGroups.includes(AccountUserGroup.AccountAdmin) ||
                this.props.currentUserGroups.includes(AccountUserGroup.CloudAdmin) ||
                this.props.currentUserGroups.includes(AccountUserGroup.Hotline)
            ) &&
            (this.props.locale !== 'ru-kz' && this.props.locale !== 'ru-ua') &&
            !this.props.commonDetails.isDbOnDelimiters
        ) {
            return (
                <ChangeTypeRequestButton
                    isFile={ this.props.commonDetails.isFile }
                    databaseName={ this.props.commonDetails.caption }
                    databaseId={ this.props.item?.databaseId ?? this.props.commonDetails.id ?? '' }
                />
            );
        }
    }

    private renderArchiveOrBackupTypeButton() {
        if (
            this.props.commonDetails.databaseState === DatabaseState.Ready &&
            this.props.commonDetails.publishState !== DbPublishState.PendingUnpublication &&
            this.props.commonDetails.publishState !== DbPublishState.PendingPublication &&
            (
                this.props.currentUserGroups.includes(AccountUserGroup.AccountAdmin) ||
                this.props.currentUserGroups.includes(AccountUserGroup.CloudAdmin) ||
                this.props.currentUserGroups.includes(AccountUserGroup.Hotline)
            )
        ) {
            return (
                <ArchiveOrBackupButton
                    databaseOperationsItem={ this.props.commonDetails.databaseOperations }
                    databaseId={ this.props.item?.databaseId ?? this.props.commonDetails.id ?? '' }
                    isDelimiters={ this.props.item?.isDbOnDelimiters ?? this.props.commonDetails.isDbOnDelimiters ?? false }
                />
            );
        }
    }

    private renderTechnicalBackupButton() {
        const { currentUserGroups, commonDetails: { databaseState, isDbOnDelimiters, id }, item } = this.props;

        if ((currentUserGroups.includes(AccountUserGroup.CloudAdmin) || currentUserGroups.includes(AccountUserGroup.Hotline)) && databaseState === DatabaseState.Ready) {
            return (
                <TechnicalBackupButton
                    isDbOnDelimiters={ isDbOnDelimiters }
                    databaseId={ item?.databaseId ?? id ?? '' }
                />
            );
        }
    }

    private renderClearCacheTypeButton() {
        if (
            this.props.commonDetails.isFile &&
            this.props.commonDetails.databaseState === DatabaseState.Ready &&
            (
                !this.props.currentUserGroups.includes(AccountUserGroup.AccountSaleManager) ||
                (this.props.currentUserGroups.includes(AccountUserGroup.AccountSaleManager) && this.props.currentUserGroups.length !== 1)
            )
        ) {
            return <ClearCacheButton AccountDatabaseId={ this.props.item?.databaseId ?? this.props.commonDetails.id ?? '' } />;
        }
    }

    private renderUpdateDatabaseButton() {
        if (
            !this.props.commonDetails.isDbOnDelimiters &&
            !(!authorizationTab(this.props.permissions, this.props.commonDetails, (this.props.item?.state ?? this.props.commonDetails.databaseState) !== 15)) &&
            (this.props.item?.state ?? this.props.commonDetails.databaseState) !== 9 &&
            (this.props.item?.state ?? this.props.commonDetails.databaseState) !== 16 &&
            !this.props.hasSupportInfoDb.hasAutoUpdate
        ) {
            return (
                <UpdateDatabaseButton
                    databaseId={ this.props.item?.databaseId ?? this.props.commonDetails.id ?? '' }
                    onChangeTab={ this.props.goToAuTab }
                    isEnabled={ !(this.props.currentUserGroups.includes(AccountUserGroup.AccountSaleManager) && this.props.currentUserGroups.length === 1) }
                />
            );
        }
    }

    public render() {
        return (
            <div className={ cn(css.header) }>
                {
                    this.props.item
                        ? (
                            <div className={ css.icon }>
                                <HomeSvgSelector
                                    width={ 80 }
                                    height={ 80 }
                                    id={ this.props.item.icon }
                                />
                            </div>
                        )
                        : null
                }
                {
                    this.props.item?.state !== 15 &&
                        <div>
                            <Box sx={ { display: 'flex', marginBottom: '10px', alignItems: 'center' } } className={ cn(css.myinfoDb) }>
                                <TextOut inDiv={ true } fontSize={ 13 } fontWeight={ 600 }>
                                    Действия с базой
                                </TextOut>
                                {
                                    defineInfoDbInProccessing(
                                        this.props.commonDetails.databaseState,
                                        this.props.commonDetails.publishState,
                                        this.props.commonDetails.isExistTerminatingSessionsProcessInDatabase
                                    ) && (
                                        <TextOut style={ { color: COLORS.warning, marginLeft: '10px' } } inDiv={ true } fontSize={ 13 } fontWeight={ 600 }>
                                            (Обновляются данные...)
                                        </TextOut>
                                    )
                                }
                            </Box>
                            {
                                this.props.isOther && (
                                    <div className={ css.button }>
                                        { this.renderPublishDatabaseButton() }
                                        { this.renderRestartPoolIisButton() }
                                        { this.renderChangeTypeButton() }
                                        { this.renderChangeTypeRequestButton() }
                                        { this.renderEndingSessionsButton() }
                                        { this.renderArchiveOrBackupTypeButton() }
                                        { this.renderTechnicalBackupButton() }
                                        { this.renderClearCacheTypeButton() }
                                        { this.renderUpdateDatabaseButton() }
                                        { this.renderDtUploadButton() }
                                        { this.renderDeleteDatabaseButton() }
                                        { this.renderRestoreInfoDbButton() }
                                    </div>
                                )
                            }
                        </div>
                }
                {
                    !this.props.currentUserGroups.includes(AccountUserGroup.AccountUser) && (
                        <Tooltip title="Запустить инструкцию для карточки ИБ">
                            <i className={ `fa fa-question-circle-o ${ css['info-icon'] }` } aria-hidden="true" onClick={ this.openTour } />
                        </Tooltip>
                    )
                }
            </div>
        );
    }
}

export const HeaderInfoDb = connect<StateProps, DispatchProps, OwnProps, AppReduxStoreState>(state => {
    const databaseCardState = state.InfoDbCardState;
    const { commonDetails, hasSupportInfoDb } = databaseCardState;
    const permissions = databaseCardState.rightsInfoDb;
    const { currentUserGroups, locale } = state.Global.getCurrentSessionSettingsReducer.settings.currentContextInfo;

    return {
        commonDetails,
        hasSupportInfoDb,
        permissions,
        currentUserGroups,
        locale
    };
}, {
    dispatchAccessCardThunk: ReceiveAccessThunk.invoke,
})(withTour(HeaderInfoDbClass));