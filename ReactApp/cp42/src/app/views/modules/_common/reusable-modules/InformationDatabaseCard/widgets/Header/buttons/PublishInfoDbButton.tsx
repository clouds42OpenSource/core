import { FETCH_API } from 'app/api/useFetchApi';
import { REDUX_API } from 'app/api/useReduxApi';
import { AccountUserGroup, DatabaseState, DbPublishState } from 'app/common/enums';
import { EMessageType, useAppDispatch, useAppSelector, useFloatMessages, useGetCurrentUserGroups } from 'app/hooks';
import { OutlinedButton } from 'app/views/components/controls/Button';
import { InfoDbCardPermissionsEnum } from 'app/web/common/enums/InfoDbCardPermissionsEnum';
import React, { useState } from 'react';

import { CancelPublishInfoDbDialog } from '../modals';

const { getInformationBaseInfo } = REDUX_API.INFORMATION_BASES_REDUX;
const { postPublishDatabase } = FETCH_API.INFORMATION_BASES;

export const PublishInfoDbButton = () => {
    const { show } = useFloatMessages();
    const dispatch = useAppDispatch();
    const currentUserGroups = useGetCurrentUserGroups();

    const { getUserPermissionsReducer: { permissions } } = useAppSelector(state => state.Global);
    const { informationBase, availability } = useAppSelector(state => state.InformationBases.informationBasesList);
    const { id, v82Name, state, publishState, canWebPublish, isDbOnDelimiters, usedWebServices } = informationBase?.database ?? {};

    const [open, setOpen] = useState(false);

    const onChangeDialogMessage = () => {
        setOpen(prev => !prev);
    };

    const publishDatabase = async () => {
        const { data } = await postPublishDatabase({ databaseId: id ?? '', publish: publishState === DbPublishState.Unpublished });

        setOpen(false);

        if (data) {
            void getInformationBaseInfo(dispatch, v82Name ?? '');

            return publishState !== DbPublishState.Unpublished
                ? show(EMessageType.success, 'База поставлена в очередь на снятие с публикации')
                : show(EMessageType.success, 'База поставлена в очередь на публикацию');
        }

        return publishState !== DbPublishState.Unpublished
            ? show(EMessageType.error, 'Не удалось поставить базу в очередь снятие с публикации')
            : show(EMessageType.error, 'Не удалось поставить базу в очередь на публикацию');
    };

    if (!(state === DatabaseState.Ready &&
        permissions.includes(InfoDbCardPermissionsEnum.AccountDatabases_Publish) &&
        canWebPublish &&
        !isDbOnDelimiters &&
        availability?.isMainServiceAllowed &&
        currentUserGroups.some(role => [AccountUserGroup.AccountAdmin, AccountUserGroup.Hotline, AccountUserGroup.CloudAdmin, AccountUserGroup.CloudSE].includes(role))
    )) {
        return null;
    }

    if (publishState === DbPublishState.Unpublished && canWebPublish) {
        return (
            <OutlinedButton kind="success" onClick={ publishDatabase }>
                <i className="fa fa-globe p-r-4" />&nbsp;Опубликовать базу
            </OutlinedButton>
        );
    }

    if (publishState === DbPublishState.PendingPublication) {
        return (
            <OutlinedButton kind="success" isEnabled={ false }>
                База в процессе публикации
            </OutlinedButton>
        );
    }

    if (publishState === DbPublishState.Published) {
        return (
            <>
                <OutlinedButton kind="success" onClick={ usedWebServices ? onChangeDialogMessage : publishDatabase }>
                    <i className="fa fa-globe p-r-4" />&nbsp;Снять с публикации
                </OutlinedButton>
                <CancelPublishInfoDbDialog
                    isOpen={ open }
                    onNoClick={ onChangeDialogMessage }
                    onYesClick={ publishDatabase }
                />
            </>
        );
    }

    if (publishState === DbPublishState.PendingUnpublication) {
        return (
            <OutlinedButton kind="success" isEnabled={ false }>
                База в процессе снятия с публикации
            </OutlinedButton>
        );
    }

    return null;
};