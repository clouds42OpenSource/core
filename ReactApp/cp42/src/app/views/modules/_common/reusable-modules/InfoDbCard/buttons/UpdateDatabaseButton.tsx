import { Box } from '@mui/material';
import { FETCH_API } from 'app/api/useFetchApi';
import { EMessageType, useAppSelector, useFloatMessages } from 'app/hooks';
import { OutlinedButton } from 'app/views/components/controls/Button';
import { Dialog } from 'app/views/components/controls/Dialog';
import { TextOut } from 'app/views/components/TextOut';
import { useState } from 'react';

type TUpdateDatabaseButtonProps = {
    databaseId: string;
    onChangeTab: () => void;
    isEnabled?: boolean;
};

const { postUpdateDatabase } = FETCH_API.ACCOUNT_DATABASES;

export const UpdateDatabaseButton = ({ databaseId, onChangeTab, isEnabled }: TUpdateDatabaseButtonProps) => {
    const { show } = useFloatMessages();

    const [openUpdateDialog, setOpenUpdateDialog] = useState(false);
    const [isDisableButton, setIsDisableButton] = useState(false);
    const { isAuthorized } = useAppSelector(state => state.InfoDbCardState.hasSupportInfoDb);

    const open = () => {
        setOpenUpdateDialog(true);
    };

    const close = () => {
        setOpenUpdateDialog(false);
    };

    const goToAuTab = () => {
        close();
        onChangeTab();
    };

    const update = async () => {
        const { success, message } = await postUpdateDatabase(databaseId);

        show(success ? EMessageType.info : EMessageType.error, message);

        close();
        setIsDisableButton(true);
    };

    return (
        <>
            <OutlinedButton onClick={ open } kind="success" isEnabled={ isEnabled && !isDisableButton }>Обновить базу</OutlinedButton>
            <Dialog
                title="Обновление базы"
                isOpen={ openUpdateDialog }
                onCancelClick={ close }
                dialogVerticalAlign="center"
                dialogWidth="sm"
                buttons={
                    isAuthorized
                        ? [
                            {
                                content: 'Отмена',
                                kind: 'default',
                                variant: 'outlined',
                                onClick: close
                            },
                            {
                                content: 'Подключить автообновление',
                                kind: 'success',
                                variant: 'contained',
                                onClick: goToAuTab
                            },
                            {
                                content: 'Обновить',
                                kind: 'success',
                                variant: 'contained',
                                onClick: update
                            }
                        ]
                        : [
                            {
                                content: 'Отмена',
                                kind: 'default',
                                variant: 'outlined',
                                onClick: close
                            },
                            {
                                content: 'Авторизоваться',
                                kind: 'success',
                                variant: 'contained',
                                onClick: goToAuTab
                            }
                        ]
                }
            >
                {
                    isAuthorized
                        ? (
                            <Box display="flex" flexDirection="column" gap="16px">
                                <TextOut>
                                    Вы можете обновить базу данных вручную в любое удобное время, но возможность подключить автоматическое обновление базы данных предоставляет вам:
                                </TextOut>
                                <TextOut>
                                    <TextOut fontWeight={ 700 } inDiv={ true }>1. Избавление от необходимости отслеживать новые версии</TextOut>
                                    <TextOut>
                                        Пользователям больше не нужно следить за выходом новых версий конфигураций и запускать обновление вручную.&nbsp;
                                        Автоматическое обновление позволяет системе автоматически обновлять базу данных до последней версии без участия пользователей.
                                    </TextOut>
                                </TextOut>
                                <TextOut>
                                    <TextOut fontWeight={ 700 } inDiv={ true }>2. Экономия времени</TextOut>
                                    <TextOut>
                                        Пользователям больше не нужно тратить время на запуск процедуры обновления и ожидание ее завершения.&nbsp;
                                        Автоматическое обновление выполняется в фоновом режиме, освобождая время пользователей для выполнения других задач.
                                    </TextOut>
                                </TextOut>
                                <TextOut>
                                    <TextOut fontWeight={ 700 } inDiv={ true }>3. Без простоев в работе</TextOut>
                                    <TextOut>
                                        Поскольку обновление выполняется автоматически, нет необходимости приостанавливать работу с базой данных на время процедуры обновления.&nbsp;
                                        Это предотвращает простои в работе и обеспечивает бесперебойный доступ к данным для всех пользователей.
                                    </TextOut>
                                </TextOut>
                                <TextOut>
                                    <TextOut fontWeight={ 700 } inDiv={ true }>4. Гибкость в планировании</TextOut>
                                    <TextOut>
                                        Автоматическое обновление позволяет запускать процесс обновления в удобное для компании время, даже если это не рабочее время.&nbsp;
                                        Пользователям больше не нужно вручную запускать процесс обновления через Личный Кабинет, что делает процесс более гибким и удобным.
                                    </TextOut>
                                </TextOut>
                            </Box>
                        )
                        : <TextOut>Обновить базу возможно только после прохождения авторизации к базе.</TextOut>
                }
            </Dialog>
        </>
    );
};