import { AppReduxStoreState } from 'app/redux/types';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { AuthInfoDbView } from 'app/views/modules/_common/reusable-modules/InfoDbCard/infoDbCardContent/tabs/HasSupportTabView/AuthInfoDbView';
import { HasSupportInfoDbDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/HasSupportInfoDbDataModel';
import dayjs from 'dayjs';
import React from 'react';
import { connect } from 'react-redux';

type GetProps = {
    hasSupportInfoDb: HasSupportInfoDbDataModel;
};

export class HasSupportViewClass extends React.PureComponent<GetProps> {
    public render() {
        return (
            <>
                <AuthInfoDbView databaseId={ this.props.hasSupportInfoDb.databaseId } />
                <CommonTableWithFilter
                    uniqueContextProviderStateId="acDbSupportHistories"
                    isResponsiveTable={ true }
                    isBorderStyled={ true }
                    tableProps={ {
                        dataset: this.props.hasSupportInfoDb?.acDbSupportHistories?.length ? this.props.hasSupportInfoDb.acDbSupportHistories.map(item => ({ ...item, Description: item.Description.replace(/<br>/gi, '\n') })) : [],
                        keyFieldName: 'Date',
                        emptyText: '',
                        fieldsView: {
                            Date: {
                                caption: 'Дата и время',
                                format: value => dayjs(value).format('DD.MM.YYYY HH:mm'),
                                fieldWidth: '12%',
                            },
                            Operation: {
                                caption: 'Описание',
                                fieldWidth: '12%',
                            },
                            Description: {
                                caption: 'Операция'
                            }
                        }
                    } }
                />
            </>
        );
    }
}

export const HasSupportView = connect<NonNullable<unknown>, NonNullable<unknown>, GetProps, AppReduxStoreState>(
    state => {
        return {
            hasHasSupportReceived: state.InfoDbCardState.hasSuccessFor.hasHasSupportReceived

        };
    }
)(HasSupportViewClass);