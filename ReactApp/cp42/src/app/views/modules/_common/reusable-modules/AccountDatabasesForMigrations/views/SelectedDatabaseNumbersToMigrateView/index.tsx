import { Box } from '@mui/material';
import { hasComponentChangesFor } from 'app/common/functions';
import { SelectedDatabasesToMigrateContext } from 'app/views/modules/_common/reusable-modules/AccountDatabasesForMigrations/context/SelectedDatabasesToMigrateContext';
import { SelectedDatabaseItem } from 'app/views/modules/_common/reusable-modules/AccountDatabasesForMigrations/views/SelectedDatabaseNumbersToMigrateView/SelectedDatabaseItem';
import { SelectStorageToMigrateView } from 'app/views/modules/_common/reusable-modules/AccountDatabasesForMigrations/views/SelectedDatabaseNumbersToMigrateView/SelectStorageToMigrateView';
import cn from 'classnames';
import React from 'react';
import css from '../styles.module.css';

export class SelectedDatabaseNumbersToMigrateView extends React.Component<NonNullable<unknown>> {
    public context!: React.ContextType<typeof SelectedDatabasesToMigrateContext>;

    public constructor(props: NonNullable<unknown>) {
        super(props);
        this.renderSelectedDatabaseNumbersToMigrate = this.renderSelectedDatabaseNumbersToMigrate.bind(this);
    }

    public shouldComponentUpdate(nextProps: NonNullable<unknown>) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private renderSelectedDatabaseNumbersToMigrate() {
        return (
            <div className={ cn(css['selected-databases-container-wrapper']) }>
                <Box className={ cn(css.box) }>
                    <span className={ cn(css.label) }>
                        Базы для переноса
                    </span>
                    { this.context.selectedDatabaseNumbers.map(item => {
                        return <SelectedDatabaseItem key={ item } databaseNumber={ item } />;
                    }) }
                </Box>
                <SelectStorageToMigrateView />
            </div>
        );
    }

    public render() {
        return this.context.selectedDatabaseNumbers.length
            ? this.renderSelectedDatabaseNumbersToMigrate()
            : null;
    }
}

SelectedDatabaseNumbersToMigrateView.contextType = SelectedDatabasesToMigrateContext;