import { DbPublishState } from 'app/common/enums';
import { hasComponentChangesFor } from 'app/common/functions';
import { getErrorMessage } from 'app/common/functions/getErrorMessage';
import { OutlinedButton } from 'app/views/components/controls/Button';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { RequestKind } from 'core/requestSender/enums';
import React from 'react';

type OwnProps = FloatMessageProps & {
    databaseId: string;
    isInPublishingState: number;
    onChangeField: () => void
};

type AllProps = OwnProps;

export class RestartPoolIisButtonClass extends React.Component<AllProps> {
    constructor(props: AllProps) {
        super(props);
        this.restartPool = this.restartPool.bind(this);
    }

    public shouldComponentUpdate(nextProps: AllProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private async restartPool() {
        const infoDbCardApi = InterlayerApiProxy.getInfoDbCardApi();
        try {
            await infoDbCardApi.restartIISPollInfoDbCard(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, {
                databaseId: this.props.databaseId,

            }).then(a => {
                a
                    ? this.props.floatMessage.show(MessageType.Success, 'Перезапуск пула IIS запущен.', 2000)
                    : this.props.floatMessage.show(MessageType.Error, 'Не удалось перезапстить пул IIS.');
            });
            this.props.onChangeField();
        } catch (e) {
            this.props.floatMessage.show(MessageType.Error, getErrorMessage(e));
        }
    }

    public render() {
        if (this.props.isInPublishingState === DbPublishState.RestartingPool) {
            return (
                <OutlinedButton kind="success" isEnabled={ false }>
                    Перезапуск пула IIS...
                </OutlinedButton>
            );
        }

        return (
            <OutlinedButton kind="success" onClick={ this.restartPool }>
                <i className="fa fa-recycle" />&nbsp;Перезапустить пул IIS
            </OutlinedButton>
        );
    }
}

export const RestartPoolIisButton = withFloatMessages(RestartPoolIisButtonClass);