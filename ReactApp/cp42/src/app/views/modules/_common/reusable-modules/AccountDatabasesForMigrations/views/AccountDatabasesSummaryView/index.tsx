import { hasComponentChangesFor } from 'app/common/functions';
import { AppReduxStoreState } from 'app/redux/types';
import { Table } from 'app/views/components/controls/Table';
import { TextOut } from 'app/views/components/TextOut';
import { FileStorageAccountDatabasesSummaryDataModel } from 'app/web/InterlayerApiProxy/AccountApiProxy/receiveAccountDatabasesForMigration/data-models';
import cn from 'classnames';
import React from 'react';
import { connect } from 'react-redux';
import css from '../styles.module.css';

type StateProps<TDataRow = FileStorageAccountDatabasesSummaryDataModel> = {
    accountDatabasesSummary: Array<TDataRow>;
    hasDatabases: boolean;
};

type AllProps = StateProps;

class AccountDatabasesSummaryViewClass extends React.Component<AllProps> {
    public shouldComponentUpdate(nextProps: AllProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    public render() {
        if (!this.props.hasDatabases) {
            return null;
        }
        return (
            <>
                <TextOut inDiv={ true } fontSize={ 14 } fontWeight={ 600 }>
                    Инфо о базах
                </TextOut>
                <br />
                <Table
                    data-testid="account-databases-summary-view"
                    dataset={ this.props.accountDatabasesSummary }
                    keyFieldName="fileStorage"
                    fieldsView={ {
                        fileStorage: {
                            caption: 'Хранилище',
                            isSortable: false,
                            fieldWidth: 'auto',
                            format: (value, row) => {
                                return (
                                    <div className={ cn({
                                        [css['table-cell-container']]: true,
                                        [css['font-bold']]: row.isTotalInfo
                                    }) }
                                    >
                                        { value }
                                    </div>
                                );
                            }
                        },
                        totatlSizeOfDatabases: {
                            caption: 'Размер, МБ',
                            isSortable: false,
                            fieldWidth: 'auto',
                            format: (value, row) => {
                                return (
                                    <div className={ cn({
                                        [css['font-bold']]: row.isTotalInfo
                                    }) }
                                    >
                                        { value }
                                    </div>
                                );
                            }
                        },
                        databaseCount: {
                            caption: 'Количество',
                            isSortable: false,
                            fieldWidth: 'auto',
                            format: (value, row) => {
                                return (
                                    <div className={ cn({
                                        [css['font-bold']]: row.isTotalInfo
                                    }) }
                                    >
                                        { value }
                                    </div>
                                );
                            }
                        }
                    } }
                />
            </>
        );
    }
}

export const AccountDatabasesSummaryView = connect<StateProps, NonNullable<unknown>, NonNullable<unknown>, AppReduxStoreState>(
    state => {
        const accountDatabasesForMigrationState = state.AccountDatabasesForMigrationState;
        const accountDatabasesSummary = accountDatabasesForMigrationState.accountDatabasesMigrationInfo.fileStorageAccountDatabasesSummary;
        const hasDatabases = accountDatabasesForMigrationState.accountDatabasesMigrationInfo.databases.records.length > 0;

        return {
            accountDatabasesSummary,
            hasDatabases
        };
    }
)(AccountDatabasesSummaryViewClass);