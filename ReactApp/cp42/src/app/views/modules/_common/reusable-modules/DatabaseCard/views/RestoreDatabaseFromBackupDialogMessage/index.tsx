import { AccountDatabaseBackupRestoreType } from 'app/common/enums';
import { hasComponentChangesFor } from 'app/common/functions';
import { DialogMessage } from 'app/views/components/controls/DialogMessage';
import { TextOut } from 'app/views/components/TextOut';
import React from 'react';

type OwnProps = {
    isOpen: boolean;
    restoreType: AccountDatabaseBackupRestoreType;
    onNoClick: () => void;
    onYesClick: () => void;
};
export class RestoreDatabaseFromBackupDialogMessage extends React.Component<OwnProps> {
    public shouldComponentUpdate(nextProps: OwnProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    public render() {
        return (
            <DialogMessage
                title="Восстановление информационной базы"
                titleFontSize={ 14 }
                titleFontWeight={ 600 }
                titleTextAlign="left"
                isTitleSmall={ true }
                isOpen={ this.props.isOpen }
                dialogWidth="sm"
                onCancelClick={ this.props.onNoClick }
                buttons={ [{
                    kind: 'primary',
                    content: 'ПОДТВЕРДИТЬ',
                    fontSize: 12,
                    onClick: this.props.onYesClick
                },
                {
                    kind: 'default',
                    content: 'ОТМЕНА',
                    fontSize: 12,
                    onClick: this.props.onNoClick
                }] }
            >
                <TextOut fontSize={ 13 }>
                    Вы действительно хотите восстановить базу ({ this.props.restoreType === AccountDatabaseBackupRestoreType.ToCurrent ? 'Заменить текущую' : 'Создать новую' })?
                </TextOut>
            </DialogMessage>
        );
    }
}