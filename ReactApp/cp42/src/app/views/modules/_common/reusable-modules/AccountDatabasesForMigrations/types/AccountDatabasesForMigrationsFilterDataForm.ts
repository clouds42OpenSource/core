/**
 * Модель Redux формы для редактирования фильтра для таблицы информационных баз для миграции
 */
export type AccountDatabasesForMigrationsFilterDataForm = {
    /**
     * Номер аккаунта
     */
    accountIndexNumber: number;

    /**
     * Строка поиска
     */
    searchLine: string;
    /**
     * Тип хранилища, `true` - файловое, `false` - серверное, `null` - файловое и серверное
     */
    isTypeStorageFile: boolean | null;
};