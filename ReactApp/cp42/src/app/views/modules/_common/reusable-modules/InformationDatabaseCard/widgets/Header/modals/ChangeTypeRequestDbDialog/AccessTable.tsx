import { Box } from '@mui/material';
import { userId as getUserId } from 'app/api';
import { useAppSelector } from 'app/hooks';
import { CheckBoxForm } from 'app/views/components/controls/forms';
import { TextOut } from 'app/views/components/TextOut';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import css from 'app/views/modules/_common/reusable-modules/InfoDbCard/styles.module.css';
import { AccessTableProps } from 'app/views/modules/AccountManagement/InformationBases/views/Modal/ChangeTypeRequestDbDialogMessage/views/AccessTable/types';
import { AccountUserDataModelsList } from 'app/web/api/InfoDbCardProxy/request-dto/InputCalculateAccesses';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { DatabaseAccessesDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/AccessInfoDbDataModel';
import cn from 'classnames';
import { RequestKind } from 'core/requestSender/enums';
import React, { useEffect, useState } from 'react';

const { CalculateAccesses } = InterlayerApiProxy.getInfoDbCardApi();

export const AccessTable = ({ setTotalSum, setChangedUsers }: AccessTableProps) => {
    const {
        InformationBases: {
            informationBasesList: {
                rateData,
                accessInfoDb,
                informationBase,
                availability
            },
        },
    } = useAppSelector(state => state);

    const [selectedUsers, setSelectedUsers] = useState<AccountUserDataModelsList[]>([]);
    const [cost, setCost] = useState(0);

    const selectAll = () => {
        if (!selectedUsers.length && informationBase) {
            setSelectedUsers(accessInfoDb.map(item => ({
                UserId: item.userId,
                HasAccess: item.hasAccess
            })));

            setChangedUsers(accessInfoDb.flatMap(item => {
                const params = {
                    sponsorship: {
                        i: false,
                        me: false,
                        label: ''
                    },
                    status: true,
                    subject: item.userId
                };

                return [
                    {
                        billingServiceTypeId: informationBase.database.myDatabasesServiceTypeId,
                        ...params
                    },
                    {
                        billingServiceTypeId: availability?.accessToServerDatabaseServiceTypeId ?? '',
                        ...params
                    }
                ];
            }));
        } else {
            setSelectedUsers([]);
            setChangedUsers([]);
        }
    };

    const selectById = (userId: string, hasAccess: boolean) => {
        if (informationBase && !selectedUsers.find(item => item.UserId === userId)) {
            const params = {
                sponsorship: {
                    i: false,
                    me: false,
                    label: ''
                },
                status: true,
                subject: userId
            };

            setSelectedUsers([...selectedUsers, { UserId: userId, HasAccess: hasAccess }]);
            setChangedUsers(prevState => [
                ...prevState,
                {
                    billingServiceTypeId: informationBase.database.myDatabasesServiceTypeId,
                    ...params
                },
                {
                    billingServiceTypeId: availability?.accessToServerDatabaseServiceTypeId ?? '',
                    ...params
                }
            ]);
        } else {
            setSelectedUsers(selectedUsers.filter(item => item.UserId !== userId));
            setChangedUsers(prevState => prevState.filter(item => item.subject !== userId));
        }
    };

    useEffect(() => {
        (async () => {
            if (informationBase?.database.isFile && rateData) {
                const response = await CalculateAccesses(
                    RequestKind.SEND_BY_USER_ASYNCHRONOUSLY,
                    {
                        serviceTypesIdsList: [rateData?.serviceTypeId],
                        accountUserDataModelsList: [{ UserId: getUserId(), HasAccess: true }]
                    }
                );

                const ksCost = response.reduce((previousValue, currentValue) => previousValue + currentValue.accessCost, 0);
                setCost(ksCost);
                setTotalSum(ksCost);
            }
        })();
    }, [informationBase?.database.isFile, rateData, setTotalSum]);

    useEffect(() => {
        (async () => {
            if (informationBase) {
                const { database: { isFile, myDatabasesServiceTypeId } } = informationBase;

                const result = await CalculateAccesses(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY,
                    {
                        serviceTypesIdsList: isFile ? [myDatabasesServiceTypeId, availability?.accessToServerDatabaseServiceTypeId ?? ''] : [myDatabasesServiceTypeId],
                        accountUserDataModelsList: selectedUsers
                    }
                );

                setTotalSum(result.reduce((previousValue, currentValue) => previousValue + currentValue.accessCost, 0) + cost);
            }
        })();
    }, [selectedUsers, cost, informationBase, availability?.accessToServerDatabaseServiceTypeId, setTotalSum]);

    return (
        <CommonTableWithFilter
            uniqueContextProviderStateId="AccessInfoDbListContextProviderStateId"
            tableProps={ {
                dataset: accessInfoDb,
                keyFieldName: 'userId',
                fieldsView: {
                    userId: {
                        caption: <CheckBoxForm
                            className={ cn(css.check) }
                            formName="isDefault"
                            label=""
                            isChecked={ accessInfoDb.length === selectedUsers.length }
                            onValueChange={ selectAll }
                        />,
                        fieldContentNoWrap: false,
                        fieldWidth: '3%',
                        format: (value: string, item) => (
                            <CheckBoxForm
                                className={ cn(css.check) }
                                formName={ value }
                                label=""
                                isChecked={ !!selectedUsers.find(isCheck => isCheck.UserId === value) }
                                onValueChange={ () => selectById(value, item.hasAccess) }
                            />
                        )
                    },
                    userLogin: {
                        caption: 'Логин',
                        fieldContentNoWrap: false,
                    },
                    userFullName: {
                        caption: 'ФИО',
                        format: (value: string, item: DatabaseAccessesDataModel) => value || item.userLastName
                    },
                    userEmail: {
                        caption: 'Эл. адрес',
                        format: (value: string, item: DatabaseAccessesDataModel) => (
                            <>
                                <TextOut>
                                    { value }
                                </TextOut>
                                { ('accessCost' in item || item.isExternalAccess) && (
                                    (
                                        <TextOut inDiv={ true } style={ { color: 'red', fontSize: '10px' } }>
                                            { item.accountInfo }
                                        </TextOut>
                                    )
                                ) }
                            </>
                        )
                    }
                }
            } }
        />
    );
};