import { FETCH_API } from 'app/api/useFetchApi';
import { REDUX_API } from 'app/api/useReduxApi';
import { DatabaseState } from 'app/common/enums';
import { EMessageType, useAppDispatch, useAppSelector, useFloatMessages } from 'app/hooks';
import { OutlinedButton } from 'app/views/components/controls/Button';
import { InfoDbCardPermissionsEnum } from 'app/web/common/enums/InfoDbCardPermissionsEnum';
import { useState } from 'react';

import { EndSessionDbDialog } from '../modals';

const { postSessionInfoDbCard } = FETCH_API.INFORMATION_BASES;
const { getInformationBaseInfo } = REDUX_API.INFORMATION_BASES_REDUX;

export const EndingSessions = () => {
    const { show } = useFloatMessages();
    const dispatch = useAppDispatch();

    const { getUserPermissionsReducer: { permissions } } = useAppSelector(state => state.Global);

    const { informationBase, availability } = useAppSelector(state => state.InformationBases.informationBasesList);
    const { id, v82Name, isExistTerminatingSessionsProcessInDatabase, state } = informationBase?.database ?? {};

    const [isOpen, setIsOpen] = useState(false);

    const dialogStateHandler = () => {
        setIsOpen(prev => !prev);
    };

    const endSessionHandler = async () => {
        const { success } = await postSessionInfoDbCard({ databaseId: id ?? '' });

        if (success) {
            void getInformationBaseInfo(dispatch, v82Name ?? '');
            show(EMessageType.success, `Все активные сеансы в базе “${ v82Name }” будут завершены в течение нескольких минут`);
        } else {
            show(EMessageType.error, `Не удалось завершить активные сеансы в базе “${ v82Name }”`);
        }

        setIsOpen(false);
    };

    if (!(
        state === DatabaseState.Ready &&
        permissions.includes(InfoDbCardPermissionsEnum.AccountDatabase_TerminateSessions) &&
        availability?.isMainServiceAllowed
    )) {
        return null;
    }

    return (
        <>
            {
                isExistTerminatingSessionsProcessInDatabase
                    ? (
                        <OutlinedButton kind="success" isEnabled={ false }>
                            Завершение сеансов
                        </OutlinedButton>
                    )
                    : (
                        <OutlinedButton kind="success" onClick={ dialogStateHandler }>
                            Завершить сеансы
                        </OutlinedButton>
                    )
            }
            <EndSessionDbDialog
                isOpen={ isOpen }
                databaseName={ v82Name ?? '' }
                onNoClick={ dialogStateHandler }
                onYesClick={ endSessionHandler }
            />
        </>
    );
};