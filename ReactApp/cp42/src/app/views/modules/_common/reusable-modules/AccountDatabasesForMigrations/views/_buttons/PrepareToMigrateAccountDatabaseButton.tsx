import ArrowForwardOutlinedIcon from '@mui/icons-material/ArrowForwardOutlined';
import { hasComponentChangesFor } from 'app/common/functions';
import { TextButton } from 'app/views/components/controls/Button';
import { SelectedDatabasesToMigrateContext } from 'app/views/modules/_common/reusable-modules/AccountDatabasesForMigrations/context/SelectedDatabasesToMigrateContext';
import React from 'react';

type OwnProps = {
    databaseNumber: string;
};

export class PrepareToMigrateAccountDatabaseButton extends React.Component<OwnProps> {
    public context!: React.ContextType<typeof SelectedDatabasesToMigrateContext>;

    constructor(props: OwnProps) {
        super(props);
        this.prepareToMigrate = this.prepareToMigrate.bind(this);
    }

    public shouldComponentUpdate(nextProps: OwnProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private prepareToMigrate() {
        this.context.addDatabaseNumber(this.props.databaseNumber);
    }

    public render() {
        return (
            <TextButton onClick={ this.prepareToMigrate } title="Добавить в список для переноса">
                <ArrowForwardOutlinedIcon sx={ { fontSize: '15px' } } />
            </TextButton>

        );
    }
}

PrepareToMigrateAccountDatabaseButton.contextType = SelectedDatabasesToMigrateContext;