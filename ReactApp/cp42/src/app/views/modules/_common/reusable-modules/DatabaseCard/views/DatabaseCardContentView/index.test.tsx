import '@testing-library/jest-dom/extend-expect';
import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import { SnackbarProvider } from 'notistack';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import React from 'react';

import { DatabaseCardContentView } from 'app/views/modules/_common/reusable-modules/DatabaseCard/views/DatabaseCardContentView';
import { DatabaseCardReducerState } from 'app/modules/databaseCard/store/reducers';
import { ReduxFormsReducerState } from 'app/redux/redux-forms/store/reducers/ReduxFormsReducerState';
import { getNewStore } from 'app/utils/StoreUtility';
import { SupportState } from 'app/common/enums';

type TInitialState = {
    DatabaseCardState: DatabaseCardReducerState;
    ReduxForms: ReduxFormsReducerState<string>;
};

describe('<DatabaseCardContentView />', () => {
    const store = getNewStore();
    const initialState: TInitialState = {
        DatabaseCardState: store.getState().DatabaseCardState,
        ReduxForms: store.getState().ReduxForms
    };

    initialState.DatabaseCardState.tehSupportInfo = {
        isInConnectingState: true,
        supportState: SupportState.AutorizationSuccess,
        supportStateDescription: 'test_description'
    };

    const renderWithEditableParams = (isBackupTabVisible = false, isTehSupportTabVisible = false) => {
        const { tabVisibility } = initialState.DatabaseCardState;
        tabVisibility.isBackupTabVisible = isBackupTabVisible;
        tabVisibility.isTehSupportTabVisible = isTehSupportTabVisible;

        render(
            <SnackbarProvider>
                <Provider store={ configureStore([thunk])(initialState) }>
                    <DatabaseCardContentView databaseDataTabRef={ React.createRef() } />
                </Provider>
            </SnackbarProvider>
        );
    };

    it('renders', () => {
        renderWithEditableParams(false, false);
        expect(screen.getByTestId(/tab-view__default-tabs__content/i)).toBeInTheDocument();
    });
});