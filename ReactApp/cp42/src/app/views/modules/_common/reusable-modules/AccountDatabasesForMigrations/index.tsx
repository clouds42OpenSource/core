import { hasComponentChangesFor } from 'app/common/functions';
import { SelectedDatabasesToMigrateContextProvider } from 'app/views/modules/_common/reusable-modules/AccountDatabasesForMigrations/context/SelectedDatabasesToMigrateContextProvider';
import { AccountDatabasesView } from 'app/views/modules/_common/reusable-modules/AccountDatabasesForMigrations/views/AccountDatabasesView';
import { SelectedDatabaseNumbersToMigrateView } from 'app/views/modules/_common/reusable-modules/AccountDatabasesForMigrations/views/SelectedDatabaseNumbersToMigrateView';
import React from 'react';

type OwnProps = {
    accountIndexNumber: number;
    canShow: boolean;
};

export class AccountDatabasesForMigrationsView extends React.Component<OwnProps> {
    public shouldComponentUpdate(nextProps: OwnProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    public render() {
        return (
            <SelectedDatabasesToMigrateContextProvider>
                {
                    this.props.canShow
                        ? <AccountDatabasesView accountIndexNumber={ this.props.accountIndexNumber } />
                        : null
                }

                {
                    this.props.canShow
                        ? <SelectedDatabaseNumbersToMigrateView />
                        : null
                }

            </SelectedDatabasesToMigrateContextProvider>
        );
    }
}