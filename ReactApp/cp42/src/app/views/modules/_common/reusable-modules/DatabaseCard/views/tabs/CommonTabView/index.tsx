import { AppReduxStoreState } from 'app/redux/types';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { DatabaseCardEditDatabaseDataForm } from 'app/views/modules/_common/reusable-modules/DatabaseCard/types';
import { DatabaseCardAccountDatabaseInfoDataModel, DatabaseCardEditModeDictionariesDataModel, DatabaseCardReadModeFieldAccessDataModel } from 'app/web/InterlayerApiProxy/DatabaseCardApiProxy/receiveDatabaseCard/data-models';
import React from 'react';
import { connect } from 'react-redux';
import { ReadOnlyView } from './ReadOnlyView';

type OwnProps = ReduxFormProps<DatabaseCardEditDatabaseDataForm> & {
    isReadonlyDatabaseInfo?: boolean;
};

type StateProps = {
    commonDetails: DatabaseCardAccountDatabaseInfoDataModel;
    databaseCardReadModeFieldAccessInfo: DatabaseCardReadModeFieldAccessDataModel;
    accountCardEditModeDictionaries: DatabaseCardEditModeDictionariesDataModel;
    hasDatabaseCardReceived: boolean;
    canEditDatabase: boolean;
    launchPublishedDbUrl: string;
};

type AllProps = OwnProps & StateProps;

class CommonTabViewClass extends React.Component<AllProps> {
    public constructor(props: AllProps) {
        super(props);

        this.initReduxForm = this.initReduxForm.bind(this);
        this.onValueChange = this.onValueChange.bind(this);
        props.reduxForm.setInitializeFormDataAction(this.initReduxForm);
    }

    private onValueChange<TValue>(fieldName: string, newValue?: TValue, _prevValue?: TValue) {
        this.props.reduxForm.updateReduxFormFields({
            [fieldName]: newValue
        });
    }

    private initReduxForm(): DatabaseCardEditDatabaseDataForm | undefined {
        if (!this.props.hasDatabaseCardReceived) { return; }
        return {
            databaseId: this.props.commonDetails.id,
            v82Name: this.props.commonDetails.v82Name,
            databaseCaption: this.props.commonDetails.caption,
            platformType: this.props.commonDetails.platformType,
            distributionType: this.props.commonDetails.distributionType,
            databaseState: this.props.commonDetails.databaseState,
            usedWebServices: this.props.commonDetails.usedWebServices,
            fileStorageId: this.props.commonDetails.fileStorage,
            databaseTemplateId: this.props.commonDetails.dbTemplate,
            accountId: this.props.commonDetails.accountId
        };
    }

    public render() {
        return this.props.hasDatabaseCardReceived
            ? (
                <ReadOnlyView
                    reduxForm={ this.props.reduxForm.getReduxFormFields(true) }
                    launchPublishedDbUrl={ this.props.launchPublishedDbUrl }
                    canEditDatabase={ this.props.canEditDatabase && !this.props.isReadonlyDatabaseInfo }
                    commonDetails={ this.props.commonDetails }
                    databaseCardReadModeFieldAccessInfo={ this.props.databaseCardReadModeFieldAccessInfo }
                    accountCardEditModeDictionaries={ this.props.accountCardEditModeDictionaries }
                    onValueChange={ this.onValueChange }
                />
            )
            : null;
    }
}

export const CommonTabViewConnected = connect<StateProps, NonNullable<unknown>, OwnProps, AppReduxStoreState>(
    state => {
        const databaseCardState = state.DatabaseCardState;
        const { commonDetails } = databaseCardState;
        const { databaseCardReadModeFieldAccessInfo } = databaseCardState;
        const { accountCardEditModeDictionaries } = databaseCardState;
        const { hasDatabaseCardReceived } = databaseCardState.hasSuccessFor;
        const { canEditDatabase } = databaseCardState;
        const { launchPublishedDbUrl } = databaseCardState;

        return {
            commonDetails,
            databaseCardReadModeFieldAccessInfo,
            accountCardEditModeDictionaries,
            hasDatabaseCardReceived,
            canEditDatabase,
            launchPublishedDbUrl
        };
    }
)(CommonTabViewClass);

export const CommonTabView = withReduxForm(CommonTabViewConnected, {
    reduxFormName: 'DatabaseCard_EditDatabase_Form'
});