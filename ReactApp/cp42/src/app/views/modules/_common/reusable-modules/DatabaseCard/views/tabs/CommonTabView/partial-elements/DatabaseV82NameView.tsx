import { TextBoxForm } from 'app/views/components/controls/forms';
import { ReadOnlyKeyValueView } from 'app/views/modules/_common/components/ReadOnlyKeyValueView';
import React from 'react';

type OwnProps = {
    label: string;
    formName: string;
    readOnlyV82Name: string;
    editV82Name: string;
    isInEditMode: boolean;
    onValueChange: <TValue>(fieldName: string, newValue: TValue, prevValue: TValue) => boolean | void;
};
export const DatabaseV82NameView = (props: OwnProps) => {
    const editV82Name = props.editV82Name ?? props.readOnlyV82Name;
    return props.isInEditMode
        ? (
            <TextBoxForm
                formName={ props.formName }
                label={ props.label }
                value={ editV82Name }
                onValueChange={ props.onValueChange }
            />
        )
        : (
            <ReadOnlyKeyValueView
                label={ props.label }
                text={ props.readOnlyV82Name }
            />
        );
};