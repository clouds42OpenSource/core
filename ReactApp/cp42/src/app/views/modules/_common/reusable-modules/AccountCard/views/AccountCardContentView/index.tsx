import { hasComponentChangesFor } from 'app/common/functions';
import { AppReduxStoreState } from 'app/redux/types';
import { TabsControl } from 'app/views/components/controls/TabsControl';
import { IReduxForm } from 'app/views/components/_hoc/withReduxForm';
import {
    AccountCardEditAccountDataForm,
    AccountCardEditAccountSegmentForm
} from 'app/views/modules/_common/reusable-modules/AccountCard/types';
import { CommandsView } from 'app/views/modules/_common/reusable-modules/AccountCard/views/CommandsView';
import {
    ChangeSegmentTabView
} from 'app/views/modules/_common/reusable-modules/AccountCard/views/tabs/ChangeSegmentTabView';
import { CommonTabView } from 'app/views/modules/_common/reusable-modules/AccountCard/views/tabs/CommonTabView';
import {
    TransferringDatabasesBetweenStoragesTabView
} from 'app/views/modules/_common/reusable-modules/AccountCard/views/tabs/TransferringDatabasesBetweenStoragesTabView';
import { AccountCardTabVisibilityDataModel } from 'app/web/InterlayerApiProxy/AccountCardApiProxy/receiveAccountCard';
import React from 'react';
import { connect } from 'react-redux';

type OwnProps = {
    commonTabRef: React.RefObject<IReduxForm<AccountCardEditAccountDataForm>>;
    changeSegmentTabRef: React.RefObject<IReduxForm<AccountCardEditAccountSegmentForm>>;
    accountIndexNumber: number;
    isReadonlyAccountInfo?: boolean;
};

type OwnState = {
    activeTabIndex: number;
};

type StateProps = {
    tabVisibility: AccountCardTabVisibilityDataModel;
};

type AllProps = OwnProps & StateProps;

export class AccountCardContentViewClass extends React.Component<AllProps, OwnState> {
    public constructor(props: AllProps) {
        super(props);
        this.onActiveTabIndexChanged = this.onActiveTabIndexChanged.bind(this);
        this.state = {
            activeTabIndex: 0
        };
    }

    public shouldComponentUpdate(nextProps: AllProps, nextState: OwnState) {
        return hasComponentChangesFor(this.props, nextProps) ||
            hasComponentChangesFor(this.state, nextState);
    }

    private onActiveTabIndexChanged(tabIndex: number) {
        this.setState({
            activeTabIndex: tabIndex
        });
    }

    public render() {
        const { isReadonlyAccountInfo } = this.props;
        return (
            <>
                <CommandsView />
                <TabsControl
                    activeTabIndex={ this.state.activeTabIndex }
                    onActiveTabIndexChanged={ this.onActiveTabIndexChanged }
                    headers={ [
                        {
                            title: 'Информация о аккаунте',
                            isVisible: this.props.tabVisibility.isAccountDetailsTabVisible
                        },
                        {
                            title: 'Перенос баз между хранилищами',
                            isVisible: this.props.tabVisibility.isMigrateDatabaseTabVisible && !isReadonlyAccountInfo
                        },
                        {
                            title: 'Смена сегмента',
                            isVisible: this.props.tabVisibility.isChangeSegmentTabVisible && !isReadonlyAccountInfo
                        }
                    ] }
                >
                    <CommonTabView isReadonly={ isReadonlyAccountInfo } ref={ this.props.commonTabRef } />
                    <TransferringDatabasesBetweenStoragesTabView
                        canLoad={ this.state.activeTabIndex === 1 }
                        accountIndexNumber={ this.props.accountIndexNumber }
                    />
                    <ChangeSegmentTabView ref={ this.props.changeSegmentTabRef } />
                </TabsControl>
            </>
        );
    }
}

export const AccountCardContentView = connect<StateProps, NonNullable<unknown>, OwnProps, AppReduxStoreState>(
    state => {
        const accountCardState = state.AccountCardState;
        const { tabVisibility } = accountCardState;

        return {
            tabVisibility
        };
    }
)(AccountCardContentViewClass);