import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { Accordion, AccordionDetails, AccordionSummary, Box, Link, MenuItem, Select, Switch } from '@mui/material';
import OutlinedInput from '@mui/material/OutlinedInput';
import { accountId, contextAccountId } from 'app/api';
import { FETCH_API } from 'app/api/useFetchApi';
import { AccountUserGroup } from 'app/common/enums';
import { EMessageType, useFloatMessages, useGetCurrentUserGroups } from 'app/hooks';
import { OutlinedButton } from 'app/views/components/controls/Button';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { TextOut } from 'app/views/components/TextOut';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import dayjs from 'dayjs';
import React, { useMemo, useState, useEffect } from 'react';

type TProps = {
    databaseId: string;
};

type TParams = {
    completeSession: boolean;
    hasSupport: boolean;
    hasAutoUpdate: boolean;
    hasModifications: boolean;
};

const { useGetSupportData, editSupportData, signOutSupport, editInformationBaseItems } = FETCH_API.INFORMATION_BASES;

const AUTOUPDATE_LINK = 'https://42clouds.com/ru-ru/manuals/podklyuchenie-bazy-1s-k-avtomaticheskomu-obnovleniyu-ao';
const ERROR_FIX_LINK = 'https://42clouds.com/ru-ru/manuals/podklyuchenie-bazy-1s-k-avtomaticheskomu-testirovaniyu-i-ispravleniyu-oshibok-tii';

export const Settings = ({ databaseId }: TProps) => {
    const { data, refreshData } = useGetSupportData(databaseId);
    const userGroups = useGetCurrentUserGroups();
    const { show } = useFloatMessages();

    const [updateTime, setUpdateTime] = useState(data?.rawData.timeOfUpdate ?? 21);
    const [params, setParams] = useState<TParams>({
        completeSession: false,
        hasAutoUpdate: false,
        hasSupport: false,
        hasModifications: false,
    });
    const [login, setLogin] = useState('');
    const [password, setPassword] = useState('');

    useEffect(() => {
        if (data?.rawData) {
            setParams({
                completeSession: data.rawData.completeSessioin,
                hasAutoUpdate: data.rawData.hasAutoUpdate,
                hasSupport: data.rawData.hasSupport,
                hasModifications: data.rawData.hasModifications,
            });
        }
    }, [data?.rawData]);

    const isOwnDatabase = useMemo(() =>
        accountId() === contextAccountId(),
    []);
    const isProjectManager = useMemo(() =>
        userGroups.includes(AccountUserGroup.CloudAdmin) || userGroups.includes(AccountUserGroup.Hotline) || userGroups.includes(AccountUserGroup.CloudSE),
    [userGroups]);
    const isSalesManager = useMemo(() =>
        userGroups.includes(AccountUserGroup.AccountSaleManager),
    [userGroups]);

    const onDisable = async () => {
        const { message, success } = await signOutSupport(databaseId);

        if (message && !success) {
            show(EMessageType.error, message);
        } else if (refreshData) {
            refreshData();
        }
    };

    const onSubmit = async () => {
        const { message, success } = await editSupportData({
            databaseId,
            login,
            password,
            timeOfUpdate: updateTime,
            completeSessioin: params.completeSession,
            hasSupport: params.hasSupport,
            hasAutoUpdate: params.hasAutoUpdate,
        });

        if (message && !success) {
            show(EMessageType.error, message);
        } else if (refreshData) {
            refreshData();
        }
    };

    const onModification = async (checked: boolean) => {
        setParams(prev => ({ ...prev, hasModifications: checked }));
        const { message, success } = await editInformationBaseItems(databaseId, checked);

        if (message && !success) {
            show(EMessageType.error, message);
        } else if (refreshData) {
            refreshData();
        }
    };

    return (
        <Box display="flex" gap="20px" flexDirection="column">
            <Box display="grid" gap="10px" gridTemplateColumns="1fr 1fr">
                <Box display="flex" gap="10px" flexDirection="column">
                    { data?.rawData.isConnects && isProjectManager && (
                        <Accordion>
                            <AccordionSummary expandIcon={ <ExpandMoreIcon /> }>
                                <TextOut fontWeight={ 700 }>Авторизационные данные</TextOut>
                            </AccordionSummary>
                            <AccordionDetails>
                                <Box display="grid" gap="10px" gridTemplateColumns="1fr 1fr">
                                    <FormAndLabel label="Пользователь 1С">
                                        <OutlinedInput fullWidth={ true } defaultValue={ data.rawData.login } disabled={ true } />
                                    </FormAndLabel>
                                    <FormAndLabel label="Пароль">
                                        <OutlinedInput fullWidth={ true } defaultValue={ data.rawData.password } disabled={ true } />
                                    </FormAndLabel>
                                </Box>
                            </AccordionDetails>
                        </Accordion>
                    ) }
                    { !data?.rawData.isConnects && (
                        <Box display="grid" gap="10px" gridTemplateRows="1fr 1fr">
                            <FormAndLabel label="Пользователь 1С">
                                <OutlinedInput value={ login } fullWidth={ true } onChange={ ({ target: { value } }) => setLogin(value) } />
                            </FormAndLabel>
                            <FormAndLabel label="Пароль">
                                <OutlinedInput value={ password } fullWidth={ true } onChange={ ({ target: { value } }) => setPassword(value) } />
                            </FormAndLabel>
                        </Box>
                    ) }
                    { data?.rawData.isAuthorized && (
                        <>
                            <FormAndLabel label="Выберите время автообновления по МСК">
                                <Select
                                    variant="outlined"
                                    disabled={ isSalesManager && !isOwnDatabase }
                                    value={ updateTime }
                                    fullWidth={ true }
                                    onChange={ ({ target: { value } }) => setUpdateTime(Number(value)) }
                                >
                                    <MenuItem value={ 14 }>14:00 - 18:00</MenuItem>
                                    <MenuItem value={ 18 }>18:00 - 21:00</MenuItem>
                                    <MenuItem value={ 21 }>21:00 - 06:00</MenuItem>
                                </Select>
                            </FormAndLabel>
                            <Box display="flex" flexDirection="column" gap="5px">
                                <Box display="flex" justifyContent="space-between">
                                    <TextOut>База содержит доработки</TextOut>
                                    <Switch checked={ params.hasModifications } onChange={ (_, checked) => onModification(checked) } />
                                </Box>
                                <Box display="flex" justifyContent="space-between">
                                    <TextOut>Завершать сеансы принудительно</TextOut>
                                    <Switch checked={ params.completeSession } onChange={ (_, checked) => setParams(prev => ({ ...prev, completeSession: checked })) } />
                                </Box>
                                <Box display="flex" justifyContent="space-between">
                                    <TextOut>Автообновление</TextOut>
                                    <Switch checked={ params.hasAutoUpdate } onChange={ (_, checked) => setParams(prev => ({ ...prev, hasAutoUpdate: checked })) } />
                                </Box>
                                <Box display="flex" justifyContent="space-between">
                                    <TextOut>ТиИ</TextOut>
                                    <Switch checked={ params.hasSupport } onChange={ (_, checked) => setParams(prev => ({ ...prev, hasSupport: checked })) } />
                                </Box>
                            </Box>
                        </>
                    ) }
                    <Box display="flex" gap="10px" justifyContent="space-between" alignItems="center">
                        { data?.rawData.hasAcDbSupport && !data.rawData.isAuthorized && (
                            <Box display="flex" gap="10px" justifyContent="space-between">
                                <TextOut>База содержит доработки</TextOut>
                                <Switch checked={ params.hasModifications } onChange={ (_, checked) => onModification(checked) } />
                            </Box>
                        ) }
                        { (!isSalesManager || (isSalesManager && isOwnDatabase)) && (
                            <Box display="flex" gap="10px" justifyContent="flex-end">
                                { data?.rawData.isConnects && data.rawData.isAuthorized && (
                                    <OutlinedButton kind="error" onClick={ onDisable }>
                                        Отключить
                                    </OutlinedButton>
                                ) }
                                { !(data?.rawData.isConnects && !data.rawData.isAuthorized) && (
                                    <OutlinedButton kind="success" onClick={ onSubmit }>
                                        { data?.rawData.isConnects && data.rawData.isAuthorized ? 'Изменить' : 'Авторизоваться' }
                                    </OutlinedButton>
                                ) }
                            </Box>
                        ) }
                    </Box>
                </Box>
                <Box display="flex" gap="10px" flexDirection="column">
                    { data?.rawData.isConnects && (
                        <Box sx={ { fontSize: '13px', fontWeight: 'bold' } } dangerouslySetInnerHTML={ { __html: data.rawData.supportStateDescription } } />
                    ) }
                    <TextOut>
                        Настройка автоматического обновления (АО) и процесс тестирования и исправления (ТиИ) ошибок осуществляются в нерабочее время.
                        <br />
                        <br />
                        Ознакомиться с этим процессом можно в наших статьях:
                        <br />
                        <Link href={ AUTOUPDATE_LINK } target="blank" rel="noreferrer" style={ { fontWeight: 'bold' } }>
                            Автоматическое обновление
                        </Link> и&nbsp;
                        <Link href={ ERROR_FIX_LINK } target="blank" rel="noreferrer" style={ { fontWeight: 'bold' } }>
                            Тестирование и исправление ошибок
                        </Link>.
                    </TextOut>
                </Box>
            </Box>
            <CommonTableWithFilter
                uniqueContextProviderStateId="acDbSupportHistories"
                tableProps={ {
                    dataset: data?.rawData.acDbSupportHistories ?? [],
                    keyFieldName: 'date',
                    fieldsView: {
                        date: {
                            caption: 'Дата и время',
                            format: (value: string) => dayjs(value).format('DD.MM.YYYY HH:mm'),
                        },
                        operation: {
                            caption: 'Операция',
                        },
                        description: {
                            caption: 'Описание',
                            format: (value: string) => <div dangerouslySetInnerHTML={ { __html: value } } />
                        }
                    }
                } }
            />
        </Box>
    );
};