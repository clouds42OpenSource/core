import { AppReduxStoreState } from 'app/redux/types';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { TextBoxForm } from 'app/views/components/controls/forms/TextBoxForm';
import { EnableTehSupportDatabaseForm } from 'app/views/modules/_common/reusable-modules/DatabaseCard/types';
import { EnableTehSupportDatabaseButton } from 'app/views/modules/_common/reusable-modules/DatabaseCard/views/_buttons/EnableTehSupportDatabaseButton';
import { DatabaseCardTehSupportInfoDataModel } from 'app/web/InterlayerApiProxy/DatabaseCardApiProxy/receiveDatabaseCard/data-models';
import cn from 'classnames';
import React from 'react';
import { connect } from 'react-redux';
import css from '../../../styles.module.css';

type OwnProps = ReduxFormProps<EnableTehSupportDatabaseForm> & {
    databaseId: string;
};

type StateProps = {
    tehSupportInfo: DatabaseCardTehSupportInfoDataModel;
    hasDatabaseCardReceived: boolean;
};

type AllProps = OwnProps & StateProps;

class AuthenticationNeedViewClass extends React.Component<AllProps> {
    public constructor(props: AllProps) {
        super(props);
        this.onLoginPasswordInput = this.onLoginPasswordInput.bind(this);
        this.initReduxForm = this.initReduxForm.bind(this);
    }

    public componentDidMount() {
        this.props.reduxForm.setInitializeFormDataAction(this.initReduxForm);
    }

    private onLoginPasswordInput(fieldName: string, newValue: string) {
        this.props.reduxForm.updateReduxFormFields({
            [fieldName]: newValue
        });
    }

    private initReduxForm(): EnableTehSupportDatabaseForm | undefined {
        if (!this.props.hasDatabaseCardReceived) { return; }
        return {
            username: '',
            password: ''
        };
    }

    public render() {
        const changedFields = this.props.reduxForm.getReduxFormFields(true);
        return (
            <div className={ cn(css['alert-wrapper']) } style={ { display: 'flex', flexDirection: 'column', gap: '16px' } } data-testid="authentication-need-view">
                <TextBoxForm
                    autoFocus={ true }
                    type="text"
                    label="Пользователь 1С"
                    formName="username"
                    value={ changedFields.username }
                    onValueChange={ this.onLoginPasswordInput }
                />

                <TextBoxForm
                    type="password"
                    label="Пароль"
                    formName="password"
                    value={ changedFields.password }
                    onValueChange={ this.onLoginPasswordInput }
                />

                <div className={ cn(css['btn-wrapper']) }>
                    <EnableTehSupportDatabaseButton
                        databaseId={ this.props.databaseId }
                        username={ changedFields.username }
                        password={ changedFields.password }
                    />
                </div>
                <div>
                    { this.props.tehSupportInfo.supportStateDescription }
                </div>
            </div>
        );
    }
}

const AuthenticationNeedViewConnected = connect<StateProps, NonNullable<unknown>, OwnProps, AppReduxStoreState>(
    state => {
        const tehSupportInfo = state.DatabaseCardState.tehSupportInfo!;
        const { hasDatabaseCardReceived } = state.DatabaseCardState.hasSuccessFor;
        return {
            hasDatabaseCardReceived,
            tehSupportInfo
        };
    }
)(AuthenticationNeedViewClass);

export const AuthenticationNeedView = withReduxForm(AuthenticationNeedViewConnected, {
    reduxFormName: 'DatabaseCard_EnableTehSupportDatabase_Form'
});