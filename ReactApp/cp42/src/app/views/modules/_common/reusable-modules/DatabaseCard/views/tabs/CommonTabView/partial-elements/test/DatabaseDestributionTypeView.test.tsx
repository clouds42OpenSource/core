import '@testing-library/jest-dom/extend-expect';
import { render, screen } from '@testing-library/react';

import { DatabaseDestributionTypeView } from 'app/views/modules/_common/reusable-modules/DatabaseCard/views/tabs/CommonTabView/partial-elements/DatabaseDestributionTypeView';
import { DistributionType, PlatformType } from 'app/common/enums';

describe('<DatabaseDestributionTypeView />', () => {
    const alpha83Version = 'test_alpha_83';
    const stable82Version = 'test_stable_82';
    const stable83Version = 'test_stable_83';

    const getComponent = (distributionType: DistributionType, platformType: PlatformType, isInEditMode: boolean) => (
        <DatabaseDestributionTypeView
            label="test_label"
            formName="test_form_name"
            readOnlyDistributionType={ distributionType }
            editDistributionType={ distributionType }
            platformType={ platformType }
            alpha83Version={ alpha83Version }
            stable82Version={ stable82Version }
            stable83Version={ stable83Version }
            distributionTypes={ [{ key: DistributionType.Stable, value: 'test_value' }] }
            isInEditMode={ isInEditMode }
            onValueChange={ jest.fn() }
        />
    );

    it('renders with isInEditMode equal true', () => {
        render(getComponent(DistributionType.Alpha, PlatformType.V83, true));
        expect(screen.getByTestId(/form-and-label/i)).toBeInTheDocument();
    });

    it('renders with isInEditMode equal false and stable V83', () => {
        render(getComponent(DistributionType.Stable, PlatformType.V83, false));
        expect(screen.getByText(stable83Version)).toBeInTheDocument();
    });

    it('renders with isInEditMode equal false and alpha V83', () => {
        render(getComponent(DistributionType.Alpha, PlatformType.V83, false));
        expect(screen.getByText(alpha83Version)).toBeInTheDocument();
    });

    it('renders with isInEditMode equal false and stable V82', () => {
        render(getComponent(DistributionType.Stable, PlatformType.V82, false));
        expect(screen.getByText(stable82Version)).toBeInTheDocument();
    });

    it('renders with isInEditMode equal false and undefined platform', () => {
        render(getComponent(DistributionType.Alpha, PlatformType.Undefined, false));
        expect(screen.getByTestId(/text-view__div/i)).toHaveTextContent('');
    });
});