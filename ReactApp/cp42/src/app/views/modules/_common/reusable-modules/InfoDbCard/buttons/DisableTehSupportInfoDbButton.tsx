import { hasComponentChangesFor } from 'app/common/functions';
import { getErrorMessage } from 'app/common/functions/getErrorMessage';
import { ContainedButton } from 'app/views/components/controls/Button';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import cn from 'classnames';
import { RequestKind } from 'core/requestSender/enums';
import React from 'react';
import css from '../styles.module.css';

type OwnProps = FloatMessageProps & {
    databaseId: string;
    onAuthProcess: () => void;
};

type AllProps = OwnProps;

class DisableTehSupportInfoDbButtonClass extends React.Component<AllProps> {
    constructor(props: AllProps) {
        super(props);
        this.onDisableDatabaseSupport = this.onDisableDatabaseSupport.bind(this);
    }

    public shouldComponentUpdate(nextProps: AllProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private async onDisableDatabaseSupport() {
        const infoDbCardApi = InterlayerApiProxy.getInfoDbCardApi();

        try {
            const result = await infoDbCardApi.DeauthInfoDbCard(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, { databaseId: this.props.databaseId, });

            if (result) {
                this.props.onAuthProcess();
            }
        } catch (err: unknown) {
            this.props.floatMessage.show(MessageType.Error, getErrorMessage(err));
        }
    }

    public render() {
        return (
            <ContainedButton
                isEnabled={ true }
                kind="error"
                className={ cn(css.btn) }
                onClick={ this.onDisableDatabaseSupport }
            >
                Отключить
            </ContainedButton>
        );
    }
}

export const DisableTehSupportInfoDbButton = withFloatMessages(DisableTehSupportInfoDbButtonClass);