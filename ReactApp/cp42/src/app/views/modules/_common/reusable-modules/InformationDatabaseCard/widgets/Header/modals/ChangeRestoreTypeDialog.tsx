import { Dialog } from 'app/views/components/controls/Dialog';
import { TextOut } from 'app/views/components/TextOut';

type TProps = {
    isOpen: boolean;
    onNoClick: () => void;
    onYesClick(): void;
};

export const ChangeRestoreTypeDialog = ({ isOpen, onNoClick, onYesClick }: TProps) => {
    return (
        <Dialog
            title="Смена модели восстановления для информационной базы"
            isTitleSmall={ true }
            isOpen={ isOpen }
            dialogWidth="sm"
            onCancelClick={ onNoClick }
            buttons={ [{
                kind: 'primary',
                content: 'Подтвердить',
                onClick: onYesClick
            },
            {
                kind: 'default',
                content: 'Отмена',
                onClick: onNoClick
            }
            ] }
        >
            <TextOut>
                Вы действительно хотите сменить модель восстановления для текущей информационной базы?
            </TextOut>
        </Dialog>
    );
};