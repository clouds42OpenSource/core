import { CircularProgress, Box, Tooltip } from '@mui/material';
import { TourProps, withTour } from '@reactour/tour';
import { REDUX_API } from 'app/api/useReduxApi';
import { AccountUserGroup, DatabaseState, DbPublishState } from 'app/common/enums';
import { TimerHelper } from 'app/common/helpers/TimerHelper';
import { useAppDispatch, useAppSelector, useGetCurrentUserGroups } from 'app/hooks';
import { COLORS } from 'app/utils';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { HomeSvgSelector } from 'app/views/components/svgGenerator/HomeSvgSelector';
import { TextOut } from 'app/views/components/TextOut';
import { DATABASE_CARD_STEPS } from 'app/views/Layout/ProjectTour/constants';
import { ETourStages } from 'app/views/Layout/ProjectTour/enums';
import { defineInfoDbInProcessing } from 'app/views/modules/_common/reusable-modules/InformationDatabaseCard/widgets/Header/functions';
import css from 'app/views/modules/AccountManagement/InformationBases/views/styles.module.css';
import React, { useEffect, useRef } from 'react';

import styles from './style.module.css';

import {
    ArchiveOrBackup,
    ChangeTypeRequest,
    ChangeType,
    TechnicalBackup,
    UpdateDatabase,
    RestoreInfoDb,
    RestartPoolIis,
    DtUpload,
    ClearCache,
    DeleteInfoDb,
    EndingSessions,
    PublishInfoDbButton
} from './buttons';

type TProps = {
    onClose: () => void;
};

const { getInformationBaseInfo } = REDUX_API.INFORMATION_BASES_REDUX;

export const Header = withTour(({ onClose, setIsOpen, setMeta, setSteps, setCurrentStep }: TProps & Partial<TourProps>) => {
    const intervalDatabasesRef = useRef<NodeJS.Timeout | number>(0);

    const currentUserGroups = useGetCurrentUserGroups();
    const dispatch = useAppDispatch();

    const { informationBase, isOther } = useAppSelector(state => state.InformationBases.informationBasesList);
    const database = informationBase?.database;

    const openTour = () => {
        if (setIsOpen && setMeta && setSteps && setCurrentStep) {
            setCurrentStep(0);
            setSteps(DATABASE_CARD_STEPS);
            setMeta(ETourStages.databaseCard);
            setIsOpen(true);
        }
    };

    const infoDbInProcessing = defineInfoDbInProcessing(
        database?.state ?? DatabaseState.Undefined,
        database?.publishState ?? DbPublishState.Published,
        !!database?.isExistTerminatingSessionsProcessInDatabase
    );

    useEffect(() => {
        if (infoDbInProcessing) {
            intervalDatabasesRef.current = setInterval(() => {
                void getInformationBaseInfo(dispatch, database?.v82Name ?? '', false);
            }, TimerHelper.waitTimeRefreshDatabaseState);
        }

        return () => clearInterval(intervalDatabasesRef.current);
    }, [database?.v82Name, dispatch, infoDbInProcessing]);

    if (database && database.state !== DatabaseState.DeletedToTomb) {
        return (
            <Box display="flex" gap="16px" alignItems="center" position="relative">
                <HomeSvgSelector width={ 80 } height={ 80 } id={ database.templateImgUrl } />
                <Box display="flex" gap="10px" flexDirection="column">
                    { infoDbInProcessing && (
                        <TextOut inDiv={ true } fontWeight={ 600 } style={ { color: COLORS.warning, display: 'flex', gap: '5px', alignItems: 'center' } }>
                            <CircularProgress size="13px" color="warning" sx={ { mb: '1px' } } />
                            Обновляются данные
                        </TextOut>
                    ) }
                    <FormAndLabel className={ styles.container } label="Действия с базой">
                        { !isOther && (
                            <Box display="flex" gap="5px" flexWrap="wrap">
                                <PublishInfoDbButton />
                                <RestartPoolIis />
                                <ChangeType />
                                <ChangeTypeRequest />
                                <EndingSessions />
                                <ArchiveOrBackup />
                                <TechnicalBackup />
                                <ClearCache />
                                <UpdateDatabase />
                                <DtUpload />
                                <DeleteInfoDb onDialogClose={ onClose } />
                                <RestoreInfoDb />
                            </Box>
                        ) }
                    </FormAndLabel>
                </Box>
                { !currentUserGroups.includes(AccountUserGroup.AccountUser) && (
                    <Tooltip title="Запустить инструкцию для карточки ИБ">
                        <i className={ `fa fa-question-circle-o ${ css['info-icon'] }` } aria-hidden="true" onClick={ openTour } />
                    </Tooltip>
                ) }
            </Box>
        );
    }

    return null;
});