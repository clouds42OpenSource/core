import '@testing-library/jest-dom/extend-expect';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { RestoreDatabaseFromBackupDialogMessage } from 'app/views/modules/_common/reusable-modules/DatabaseCard/views/RestoreDatabaseFromBackupDialogMessage';
import { AccountDatabaseBackupRestoreType } from 'app/common/enums';

describe('<RestoreDatabaseFromBackupDialogMessage />', () => {
    const onNoClickMock: jest.Mock = jest.fn();
    const onYesClickMock: jest.Mock = jest.fn();

    beforeEach(() => {
        render(<RestoreDatabaseFromBackupDialogMessage
            isOpen={ true }
            restoreType={ AccountDatabaseBackupRestoreType.ToCurrent }
            onNoClick={ onNoClickMock }
            onYesClick={ onYesClickMock }
        />);
    });

    it('renders', () => {
        expect(screen.getByRole(/dialog/i)).toBeInTheDocument();
        expect(screen.getByText(/Восстановление информационной базы/i)).toBeInTheDocument();
    });

    it('has onYesClick method', async () => {
        expect(screen.getByText('ПОДТВЕРДИТЬ')).toBeInTheDocument();
        await userEvent.click(screen.getAllByTestId(/button-view/i)[1]);
        expect(onYesClickMock).toBeCalled();
    });

    it('has onNoClickMock method', async () => {
        expect(screen.getByText('ОТМЕНА')).toBeInTheDocument();
        await userEvent.click(screen.getAllByTestId(/button-view/i)[0]);
        expect(onNoClickMock).toBeCalled();
    });
});