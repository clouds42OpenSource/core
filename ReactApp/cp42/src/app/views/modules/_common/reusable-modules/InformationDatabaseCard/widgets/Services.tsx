import { Box, Chip, Tooltip } from '@mui/material';
import OutlinedInput from '@mui/material/OutlinedInput';
import { ExtensionStateServicesEnum } from 'app/common/enums/ExtensionStateServicesEnum';
import { TextOut } from 'app/views/components/TextOut';
import { accountId, contextAccountId } from 'app/api';
import { TConnectToDatabase } from 'app/api/endpoints/services/request';
import { FETCH_API } from 'app/api/useFetchApi';
import { AppRoutes } from 'app/AppRoutes';
import { AccountUserGroup } from 'app/common/enums';
import { EMessageType, useDebounce, useFloatMessages, useGetCurrentUserGroups } from 'app/hooks';
import { ButtonGroup } from 'app/views/components/controls/ButtonGroup';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import dayjs from 'dayjs';
import { useEffect, useState } from 'react';
import { ServiceState } from '../features';

const { useGetServicesList, connectToDatabase, demoConnectToDatabase } = FETCH_API.SERVICES;

type TProps = {
    databaseId: string;
};

const BUTTON_ITEMS = [
    { index: 0, text: 'Все сервисы' },
    { index: 1, text: 'Подключенные' },
    { index: 2, text: 'Отключенные' },
    { index: 3, text: 'Доступные' },
];

export const Services = ({ databaseId }: TProps) => {
    const [type, setType] = useState(0);
    const [serviceName, setServiceName] = useState('');
    const debounced = useDebounce(serviceName, 500);

    const userGroups = useGetCurrentUserGroups();
    const { show } = useFloatMessages();

    const { data, isLoading, refreshData } = useGetServicesList({
        accountId: contextAccountId(),
        serviceName: debounced,
        accountDatabaseId: databaseId,
        serviceExtensionsForDatabaseType: type
    });

    const onConnect = async (request: TConnectToDatabase, isDemo: boolean) => {
        if (isDemo) {
            const { success } = await demoConnectToDatabase(request);

            if (success) {
                show(EMessageType.success, 'Активирован демо период сервиса');
            }
        }

        const { success, message } = await connectToDatabase(request);

        if (!success && message) {
            show(EMessageType.error, message);
        } else if (refreshData) {
            refreshData();
        }
    };

    useEffect(() => {
        if (refreshData && data?.rawData.find(item => item.extensionState && [ExtensionStateServicesEnum.ProcessingInstall, ExtensionStateServicesEnum.ProcessingDelete].includes(item.extensionState))) {
            setTimeout(refreshData, 10000);
        }
    }, [data?.rawData, refreshData]);

    return (
        <>
            { isLoading && <LoadingBounce /> }
            <Box display="flex" flexDirection="column" gap="20px">
                <Box display="grid" gap="10px" alignItems="flex-end" gridTemplateColumns="1fr 1fr">
                    <ButtonGroup
                        items={ BUTTON_ITEMS }
                        selectedButtonIndex={ type }
                        label="Быстрые отборы"
                        onClick={ index => setType(index) }
                    />
                    <Box flex="1">
                        <FormAndLabel label="Поиск">
                            <OutlinedInput
                                fullWidth={ true }
                                value={ serviceName }
                                onChange={ ({ target: { value } }) => setServiceName(value) }
                            />
                        </FormAndLabel>
                    </Box>
                </Box>
                <CommonTableWithFilter
                    uniqueContextProviderStateId="servicesList"
                    tableProps={ {
                        emptyText: 'Не найдено доступных сервисов',
                        keyFieldName: 'id',
                        dataset: data?.rawData ?? [],
                        fieldsView: {
                            name: {
                                caption: 'Название',
                                format: (value: string, { extensionState }) => (
                                    <>
                                        <TextOut>{ value }</TextOut>
                                        <ServiceState extensionState={ extensionState ?? 0 } />
                                    </>
                                )
                            },
                            shortDescription: {
                                caption: 'Описание',
                            },
                            extensionLastActivityDate: {
                                caption: 'Дата Вкл/Откл',
                                format: (value: string, { isInstalled, extensionState }) =>
                                    extensionState !== 6 && isInstalled && value ? dayjs(value).format('DD.MM.YYYY HH:mm') : '---'
                            },
                            isActiveService: {
                                caption: 'Состояние',
                                format: (_: boolean, { isInstalled, extensionState, isActiveService, isFrozenService, id }) => {
                                    if (accountId() !== contextAccountId() && !userGroups.some(group => [AccountUserGroup.CloudAdmin, AccountUserGroup.Hotline, AccountUserGroup.CloudSE].includes(group))) {
                                        return null;
                                    }

                                    const checked = isInstalled && (extensionState === ExtensionStateServicesEnum.DoneInstall || extensionState === ExtensionStateServicesEnum.ProcessingInstall);

                                    const isDemo = extensionState === ExtensionStateServicesEnum.NotInstalled && !isInstalled && !isActiveService;

                                    if (isDemo) {
                                        return (
                                            <Tooltip placement="top" title="Активировать демо период для сервиса">
                                                <Chip
                                                    sx={ { width: '100%' } }
                                                    label="Попробовать"
                                                    color="info"
                                                    onClick={ () => onConnect({ install: !checked, serviceId: id, databaseId }, true) }
                                                />
                                            </Tooltip>
                                        );
                                    }

                                    const isFrozen = isActiveService && isFrozenService;

                                    if (isFrozen) {
                                        return (
                                            <Tooltip placement="top" title="Нажмите, чтобы продлить или оплатить сервис">
                                                <Chip
                                                    sx={ { width: '100%' } }
                                                    label="Заблокирован"
                                                    color="warning"
                                                    onClick={ () => window.open(`${ AppRoutes.services.managingServiceDescriptions }/${ id }`, '_blank') }
                                                />
                                            </Tooltip>
                                        );
                                    }

                                    const isInactive = !isActiveService && !isInstalled;

                                    if (isInactive) {
                                        return (
                                            <Tooltip placement="top" title="Сервис неактивен. Вы можете активировать его на странице сервиса">
                                                <Chip
                                                    sx={ { width: '100%' } }
                                                    label="Неактивен"
                                                    color="default"
                                                    onClick={ () => window.open(`${ AppRoutes.services.managingServiceDescriptions }/${ id }`, '_blank') }
                                                />
                                            </Tooltip>
                                        );
                                    }

                                    return (
                                        <Tooltip placement="top" title={ checked ? 'Отключить сервис от базы' : 'Подключить сервис к базе' }>
                                            <Chip
                                                sx={ { width: '100%' } }
                                                color={ checked ? 'primary' : 'error' }
                                                label={ !checked ? 'Отключён' : 'Подключён' }
                                                onClick={ () => onConnect({ install: !checked, serviceId: id, databaseId }, false) }
                                            />
                                        </Tooltip>
                                    );
                                }
                            },
                        },
                    } }
                />
            </Box>
        </>
    );
};