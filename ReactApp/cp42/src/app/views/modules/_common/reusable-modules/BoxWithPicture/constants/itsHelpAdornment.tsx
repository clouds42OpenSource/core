import HelpOutlineOutlinedIcon from '@mui/icons-material/HelpOutlineOutlined';
import { Box, InputAdornment, Tooltip } from '@mui/material';
import { COLORS } from 'app/utils';
import { TextOut } from 'app/views/components/TextOut';
import React from 'react';

export const ItsHelpAdornment = () => {
    return (
        <InputAdornment position="end">
            <Tooltip
                placement="top"
                title={
                    <Box
                        display="flex"
                        alignItems="center"
                        flexDirection="column"
                        gap="8px"
                    >
                        <img
                            src="img/additional-pages/its-help.jpg"
                            alt="Портала 1С:ИТС"
                            style={ { maxWidth: '200px' } }
                            loading="lazy"
                        />
                        <TextOut style={ { color: COLORS.white } }>
                            Если вы не зарегистрированны, то необходимо зарегистрироваться по&nbsp;
                            <a
                                href="https://login.1c.ru/registration?redirect=https%3A%2F%2Fportal.1c.ru%2Fpublic%2Fsecurity_check"
                                target="_blank"
                                rel="noreferrer"
                                style={ { textDecoration: 'underline' } }
                            >
                                ссылке
                            </a> и указать данные от Портала 1C:ИТС.
                        </TextOut>
                    </Box>
                }
            >
                <HelpOutlineOutlinedIcon />
            </Tooltip>
        </InputAdornment>
    );
};