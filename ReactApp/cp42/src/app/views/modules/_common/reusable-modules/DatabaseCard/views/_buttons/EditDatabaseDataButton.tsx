import { hasComponentChangesFor } from 'app/common/functions';
import { DatabaseCardProcessId } from 'app/modules/databaseCard';
import { EditDatabaseDataThunkParams } from 'app/modules/databaseCard/store/reducers/editDatabaseDataReducer/params';
import { EditDatabaseDataThunk } from 'app/modules/databaseCard/store/thunks/EditDatabaseDataThunk';
import { AppReduxStoreState } from 'app/redux/types';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { ContainedButton } from 'app/views/components/controls/Button';
import { WatchReducerProcessActionState } from 'app/views/modules/_common/components/WatchReducerProcessActionState';
import { DatabaseCardEditDatabaseDataForm } from 'app/views/modules/_common/reusable-modules/DatabaseCard/types';
import { IReducerProcessActionInfo } from 'core/redux/interfaces';
import React from 'react';
import { connect } from 'react-redux';

type OwnProps = FloatMessageProps & {
    getChangedDatabaseData: () => DatabaseCardEditDatabaseDataForm;
    showSuccessMessage?: boolean;
    showErrorMessage?: boolean;
    onSuccess?: () => void;
};

type OwnState = {
    changedDatabaseDataState: {
        isInProgressState: boolean;
        isInSuccessState: boolean;
        isInErrorState: boolean;
        error?: Error;
    }
};

type DispatchProps = {
    dispatchEditDatabaseDataThunk: (args: EditDatabaseDataThunkParams) => void;
};

type AllProps = OwnProps & DispatchProps;

export class EditDatabaseDataButtonClass extends React.Component<AllProps, OwnState> {
    public constructor(props: AllProps) {
        super(props);

        this.onEditDatabaseDataClick = this.onEditDatabaseDataClick.bind(this);
        this.onProcessActionStateChanged = this.onProcessActionStateChanged.bind(this);

        this.state = {
            changedDatabaseDataState: {
                isInErrorState: false,
                isInProgressState: false,
                isInSuccessState: false,
                error: undefined
            }
        };
    }

    public shouldComponentUpdate(nextProps: OwnProps, nextState: OwnState) {
        return hasComponentChangesFor(this.props, nextProps) ||
            hasComponentChangesFor(this.state, nextState);
    }

    public componentDidUpdate(_prevProps: AllProps, prevState: OwnState) {
        if (!prevState.changedDatabaseDataState.isInSuccessState && this.state.changedDatabaseDataState.isInSuccessState) {
            if (this.props.showSuccessMessage ?? true) {
                this.props.floatMessage.show(MessageType.Success, 'Общие данные о информационной базе сохранены.');
            }

            if (this.props.onSuccess) this.props.onSuccess();
        } else if (!prevState.changedDatabaseDataState.isInErrorState && this.state.changedDatabaseDataState.isInErrorState) {
            if (this.props.showErrorMessage ?? true) {
                this.props.floatMessage.show(MessageType.Error, this.state.changedDatabaseDataState.error?.message);
            }
        }
    }

    private onProcessActionStateChanged(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId === DatabaseCardProcessId.EditDatabaseData) {
            this.setState({
                changedDatabaseDataState: {
                    isInProgressState: actionState.isInProgressState,
                    isInSuccessState: actionState.isInSuccessState,
                    isInErrorState: actionState.isInErrorState,
                    error
                }
            });
        }
    }

    private onEditDatabaseDataClick() {
        const changedDatabaseData = this.props.getChangedDatabaseData();
        this.props.dispatchEditDatabaseDataThunk({
            ...changedDatabaseData,
            force: true
        });
    }

    public render() {
        return (
            <>
                <ContainedButton
                    isEnabled={ !this.state.changedDatabaseDataState.isInProgressState }
                    showProgress={ this.state.changedDatabaseDataState.isInProgressState }
                    kind="success"
                    onClick={ this.onEditDatabaseDataClick }
                >
                    <span>Сохранить</span>
                </ContainedButton>
                <WatchReducerProcessActionState
                    watch={ [{
                        reducerStateName: 'DatabaseCardState',
                        processIds: [DatabaseCardProcessId.EditDatabaseData]
                    }] }
                    onProcessActionStateChanged={ this.onProcessActionStateChanged }
                />
            </>
        );
    }
}

const EditDatabaseDataButtonClassConnected = connect<NonNullable<unknown>, DispatchProps, OwnProps, AppReduxStoreState>(
    null,
    {
        dispatchEditDatabaseDataThunk: EditDatabaseDataThunk.invoke,
    }
)(EditDatabaseDataButtonClass);

export const EditDatabaseDataButton = withFloatMessages(EditDatabaseDataButtonClassConnected);