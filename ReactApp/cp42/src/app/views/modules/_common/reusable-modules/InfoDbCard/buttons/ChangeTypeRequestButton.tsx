import { OutlinedButton } from 'app/views/components/controls/Button';
import { ChangeTypeDbDialogMessage } from 'app/views/modules/AccountManagement/InformationBases/views/Modal/ChangeTypeRequestDbDialogMessage';
import React, { useState } from 'react';

type ChangeTypeRequestButtonProps = {
    databaseName: string;
    databaseId: string;
    isFile: boolean;
}

export const ChangeTypeRequestButton = ({ databaseName, databaseId, isFile }: ChangeTypeRequestButtonProps) => {
    const [isOpen, setIsOpen] = useState(false);
    const [isEnabled, setIsEnabled] = useState(true);

    const openDialogMessage = () => setIsOpen(true);
    const closeDialogMessage = () => setIsOpen(false);

    return (
        <>
            <OutlinedButton kind="success" onClick={ openDialogMessage } isEnabled={ isEnabled }>
                <i className="fa fa-database" />&nbsp;Оставить заявку на перевод в { isFile ? 'серверную' : 'файловую' }
            </OutlinedButton>
            <ChangeTypeDbDialogMessage
                isOpen={ isOpen }
                isFile={ isFile }
                databaseName={ databaseName }
                databaseId={ databaseId }
                onNoClick={ closeDialogMessage }
                setIsEnabled={ setIsEnabled }
            />
        </>
    );
};