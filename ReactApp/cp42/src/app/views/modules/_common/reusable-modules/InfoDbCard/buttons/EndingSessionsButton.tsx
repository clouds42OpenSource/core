import { hasComponentChangesFor } from 'app/common/functions';
import { getErrorMessage } from 'app/common/functions/getErrorMessage';
import { OutlinedButton } from 'app/views/components/controls/Button';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import {
    EndSessionDbDialogMessage
} from 'app/views/modules/AccountManagement/InformationBases/views/Modal/EndSessionDbDialogMessage';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { RequestKind } from 'core/requestSender/enums';
import React from 'react';

type OwnState = {
    open: boolean
};

type OwnProps = FloatMessageProps & {
    databaseId: string;
    databaseName: string;
    isInPublishingState: boolean;
    onChangeField: () => void
};

type AllProps = OwnProps;

export class EndingSessionsButtonClass extends React.Component<AllProps, OwnState> {
    constructor(props: AllProps) {
        super(props);
        this.endingSession = this.endingSession.bind(this);
        this.openDialogMessage = this.openDialogMessage.bind(this);
        this.state = {
            open: false,
        };
    }

    public shouldComponentUpdate(nextProps: AllProps, nextState: OwnState) {
        return hasComponentChangesFor(this.props, nextProps) || hasComponentChangesFor(this.state, nextState);
    }

    private openDialogMessage() {
        this.setState(prevState => ({
            open: !prevState.open
        }));
    }

    private async endingSession() {
        const infoDbCardApi = InterlayerApiProxy.getInfoDbCardApi();
        try {
            await infoDbCardApi.sessionInfoDbCard(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, {
                databaseId: this.props.databaseId,
            }).then(a => {
                a
                    ? this.props.floatMessage.show(MessageType.Success, `Все активные сеансы в базе “${ this.props.databaseName }” будут завершены в течение нескольких минут.`, 2000)
                    : this.props.floatMessage.show(MessageType.Error, `Не удалось завершить активные сеансы в базе “${ this.props.databaseName }”.`);
                this.setState({
                    open: false
                });
            });
            this.props.onChangeField();
        } catch (e) {
            this.props.floatMessage.show(MessageType.Error, getErrorMessage(e));
        }
    }

    public render() {
        return (
            <>
                {
                    this.props.isInPublishingState
                        ? (
                            <OutlinedButton kind="success" isEnabled={ false }>
                                Завершение сеансов
                            </OutlinedButton>
                        )
                        : (
                            <OutlinedButton kind="success" onClick={ this.openDialogMessage }>
                                Завершить сеансы
                            </OutlinedButton>
                        )
                }
                <EndSessionDbDialogMessage
                    isOpen={ this.state.open }
                    databaseName={ this.props.databaseName }
                    onNoClick={ this.openDialogMessage }
                    onYesClick={ this.endingSession }
                />
            </>
        );
    }
}

export const EndingSessionsButton = withFloatMessages(EndingSessionsButtonClass);