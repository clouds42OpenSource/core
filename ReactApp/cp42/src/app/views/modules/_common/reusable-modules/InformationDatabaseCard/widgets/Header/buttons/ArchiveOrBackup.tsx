import { AccountUserGroup, DatabaseState, DbPublishState } from 'app/common/enums';
import { useState } from 'react';

import { EMessageType, useAppSelector, useFloatMessages } from 'app/hooks';
import { OutlinedButton } from 'app/views/components/controls/Button';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { CheckPaymentOptionDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/checkPaymentOption';
import { RequestKind } from 'core/requestSender/enums';

import { ArchiveOrBackupDialog } from '../modals';

const { CheckPaymentOption } = InterlayerApiProxy.getInfoDbCardApi();

export const ArchiveOrBackup = () => {
    const { show } = useFloatMessages();

    const { getCurrentSessionSettingsReducer: { settings: { currentContextInfo: { currentUserGroups } } } } = useAppSelector(state => state.Global);
    const { informationBase } = useAppSelector(state => state.InformationBases.informationBasesList);
    const { id: databaseId, state, publishState, isDbOnDelimiters, databaseOperations } = informationBase?.database ?? {};

    const [checkPaymentOption, setCheckPaymentOption] = useState<CheckPaymentOptionDataModel | null>(null);
    const [isOpen, setIsOpen] = useState(false);
    const [isEnable, setIsEnable] = useState(true);
    const [cost, setCost] = useState(databaseOperations?.[1].cost ?? 0);
    const [currency, setCurrency] = useState(databaseOperations?.[1].currency ?? '');
    const [id, setId] = useState(databaseOperations?.[1].id ?? '');

    const openDialog = async () => {
        try {
            const response = await CheckPaymentOption(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, { cost, currency });
            setCheckPaymentOption(response);
            setIsOpen(true);
        } catch {
            show(EMessageType.error, 'При получении данных о стоимости операции произошла ошибка.');
        }
    };

    const closeDialog = () => {
        setIsOpen(false);
        setCheckPaymentOption(null);
    };

    const enableButton = () => {
        setIsEnable(false);
    };

    const switchType = (index: number) => {
        if (databaseOperations) {
            setCost(databaseOperations[index].cost);
            setCurrency(databaseOperations[index].currency);
            setId(databaseOperations[index].id);
        }
    };

    if (!(
        state === DatabaseState.Ready &&
        publishState !== DbPublishState.PendingUnpublication &&
        publishState !== DbPublishState.PendingPublication &&
        currentUserGroups.some(role => [AccountUserGroup.AccountAdmin, AccountUserGroup.Hotline, AccountUserGroup.CloudAdmin, AccountUserGroup.CloudSE].includes(role))
    )) {
        return null;
    }

    return (
        <>
            <OutlinedButton kind="success" onClick={ openDialog } isEnabled={ isEnable }>
                Архивация
            </OutlinedButton>
            <ArchiveOrBackupDialog
                isOpen={ isOpen }
                completeStatus={ !!checkPaymentOption?.complete }
                canUsePromisePayment={ !!checkPaymentOption?.canUsePromisePayment }
                canIncreasePromisePayment={ !!checkPaymentOption?.canIncreasePromisePayment }
                cost={ cost }
                currency={ currency }
                notEnoughMoney={ checkPaymentOption?.notEnoughMoney ?? 0 }
                databaseId={ databaseId ?? '' }
                serviceId={ id }
                closeDialog={ closeDialog }
                switchType={ switchType }
                enableButton={ enableButton }
                isDelimiters={ !!isDbOnDelimiters }
            />
        </>
    );
};