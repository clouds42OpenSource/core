import { AppReduxStoreState } from 'app/redux/types';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { SelectedDatabasesToMigrateContext } from 'app/views/modules/_common/reusable-modules/AccountDatabasesForMigrations/context/SelectedDatabasesToMigrateContext';
import React from 'react';
import { connect } from 'react-redux';

type OwnState = {
    selectedDatabaseNumbers: Array<string>;
    selectedStorageId: string;
};

type StateProps = {
    numberOfDatabasesToMigrate: number;
};

type OwnProps = FloatMessageProps;
type AllProps = OwnProps & StateProps;

class SelectedDatabasesToMigrateContextProviderClass extends React.Component<AllProps, OwnState> {
    public constructor(props: AllProps) {
        super(props);

        this.addDatabaseNumber = this.addDatabaseNumber.bind(this);
        this.removeDatabaseNumber = this.removeDatabaseNumber.bind(this);
        this.setSelectedStorage = this.setSelectedStorage.bind(this);
        this.reset = this.reset.bind(this);
        this.state = {
            selectedDatabaseNumbers: [],
            selectedStorageId: ''
        };
    }

    private setSelectedStorage(storageId: string) {
        this.setState({
            selectedStorageId: storageId
        });
    }

    private removeDatabaseNumber(databaseNumber: string) {
        const currentSelectedDatabaseNumbers = this.state.selectedDatabaseNumbers;
        const foundIndex = currentSelectedDatabaseNumbers.indexOf(databaseNumber);
        if (foundIndex >= 0) {
            currentSelectedDatabaseNumbers.splice(foundIndex, 1);
        }
        this.setState({
            selectedDatabaseNumbers: currentSelectedDatabaseNumbers
        });
    }

    private addDatabaseNumber(databaseNumber: string) {
        const currentSelectedDatabaseNumbers = this.state.selectedDatabaseNumbers;
        if (currentSelectedDatabaseNumbers.length === this.props.numberOfDatabasesToMigrate) {
            this.props.floatMessage.show(MessageType.Warning, `За 1 раз можно перенести не более ${ this.props.numberOfDatabasesToMigrate } баз.`);
            return;
        }

        const foundIndex = currentSelectedDatabaseNumbers.indexOf(databaseNumber);
        if (foundIndex >= 0) {
            currentSelectedDatabaseNumbers.splice(foundIndex, 1);
        }
        currentSelectedDatabaseNumbers.push(databaseNumber);
        this.setState({
            selectedDatabaseNumbers: currentSelectedDatabaseNumbers
        });
    }

    private reset() {
        this.setState({
            selectedDatabaseNumbers: [],
            selectedStorageId: ''
        });
    }

    public render() {
        return (
            <SelectedDatabasesToMigrateContext.Provider
                value={ {
                    selectedDatabaseNumbers: this.state.selectedDatabaseNumbers,
                    addDatabaseNumber: this.addDatabaseNumber,
                    removeDatabaseNumber: this.removeDatabaseNumber,
                    selectedStorageId: this.state.selectedStorageId,
                    setSelectedStorage: this.setSelectedStorage,
                    reset: this.reset
                } }
            >
                { this.props.children }
            </SelectedDatabasesToMigrateContext.Provider>
        );
    }
}

const SelectedDatabasesToMigrateContextProviderConnected = connect<StateProps, NonNullable<unknown>, OwnProps, AppReduxStoreState>(
    state => {
        const accountDatabasesForMigrationState = state.AccountDatabasesForMigrationState;
        const { numberOfDatabasesToMigrate } = accountDatabasesForMigrationState.accountDatabasesMigrationInfo;

        return {
            numberOfDatabasesToMigrate
        };
    }
)(SelectedDatabasesToMigrateContextProviderClass);

export const SelectedDatabasesToMigrateContextProvider = withFloatMessages(SelectedDatabasesToMigrateContextProviderConnected);