import '@testing-library/jest-dom/extend-expect';
import { render, screen } from '@testing-library/react';
import { SnackbarProvider } from 'notistack';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';

import { DatabaseCardReducerState } from 'app/modules/databaseCard/store/reducers';
import { getNewStore } from 'app/utils/StoreUtility';
import { CommandsView } from 'app/views/modules/_common/reusable-modules/DatabaseCard/views/CommandsView';
import { DatabaseCardCommandVisibilityDataModel } from 'app/web/InterlayerApiProxy/DatabaseCardApiProxy/receiveDatabaseCard';

type TInitialState = {
    DatabaseCardState: DatabaseCardReducerState;
};

describe('<CommandsView />', () => {
    const initialState: TInitialState = {
        DatabaseCardState: getNewStore().getState().DatabaseCardState
    };

    const componentWithEditableParams = (isVisible: boolean) => {
        const visibilityObject = initialState.DatabaseCardState.commandVisibility;

        for (const key of Object.keys(visibilityObject)) {
            visibilityObject[key as keyof DatabaseCardCommandVisibilityDataModel] = isVisible;
        }

        return (
            <Provider store={ configureStore()(initialState) }>
                <SnackbarProvider>
                    <CommandsView />
                </SnackbarProvider>
            </Provider>
        );
    };

    it('does not renders', () => {
        const { container } = render(componentWithEditableParams(false));
        expect(container.firstChild).toBeEmptyDOMElement();
    });

    it('renders', () => {
        render(componentWithEditableParams(true));
        expect(screen.getAllByTestId(/button-view/i).length).toEqual(3);
    });
});