import '@testing-library/jest-dom/extend-expect';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { SnackbarProvider } from 'notistack';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';

import { DisableTehSupportDatabaseButton } from 'app/views/modules/_common/reusable-modules/DatabaseCard/views/_buttons/DisableTehSupportDatabaseButton';
import { DatabaseCardProcessId } from 'app/modules/databaseCard';

describe('<DisableTehSupportDatabaseButton />', () => {
    const mockStore = configureStore([thunk]);
    let container: Element;
    const initialState = {
        DatabaseCardState: {
            processIds: [DatabaseCardProcessId.UnpublishDatabase],
            reducerActions: {
                UNPUBLISH_DATABASE: {
                    hasProcessActionStateChanged: true,
                    processActionState: {
                        isInErrorState: false,
                        isInSuccessState: true,
                        isInProgressState: false
                    },
                    process: {
                        error: undefined
                    }
                }
            },
            process: {
                showLoadingProgress: false
            },
        }
    } as const;

    beforeEach(() => {
        container = render(
            <Provider store={ mockStore(initialState) }>
                <SnackbarProvider>
                    <DisableTehSupportDatabaseButton databaseId="test_database_id" />
                </SnackbarProvider>
            </Provider>
        ).container;
    });

    it('renders', () => {
        expect(screen.getByTestId(/button-view/i)).toBeInTheDocument();
        expect(screen.getByText(/Отключить/i)).toBeInTheDocument();
    });

    it('has onClick method', async () => {
        await userEvent.click(screen.getByTestId(/button-view/i));
        expect(container.querySelector('.MuiTouchRipple-child')).toBeInTheDocument();
    });
});