import { useAppDispatch, useAppSelector } from 'app/hooks';
import { archivalCopiesFilter } from 'app/modules/informationBases/reducers/archivalCopiesFilter';
import { FileArchivalCopies } from 'app/views/modules/_common/reusable-modules/InformationDatabaseCard/features';
import { ArchivalCopiesFilter } from 'app/views/modules/_common/reusable-modules/InformationDatabaseCard/features/ArchivalCopiesFilter';
import { CopyGenerationTime } from 'app/views/modules/_common/reusable-modules/InformationDatabaseCard/features/CopyGenerationTime';
import { DelimitersArchivalCopies } from 'app/views/modules/_common/reusable-modules/InformationDatabaseCard/features/DelimitersArchivalCopies';
import { useEffect } from 'react';

const { resetFilter } = archivalCopiesFilter.actions;

export const ArchivalCopies = () => {
    const dispatch = useAppDispatch();
    const { database } = useAppSelector(state => state.InformationBases.informationBasesList.informationBase) || {};

    useEffect(() => {
        return () => {
            dispatch(resetFilter());
        };
    }, [dispatch]);

    const isDbOnDelimiters = database && database.isDbOnDelimiters;

    return (
        <>
            { isDbOnDelimiters && <CopyGenerationTime /> }
            <ArchivalCopiesFilter />
            { isDbOnDelimiters ? <DelimitersArchivalCopies /> : <FileArchivalCopies /> }
        </>
    );
};