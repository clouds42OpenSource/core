import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { OutlinedButton } from 'app/views/components/controls/Button';
import { ArchiveOrBackupDialogMessage } from 'app/views/modules/AccountManagement/InformationBases/views/Modal/ArchiveOrBackupDialogMessage';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { CheckPaymentOptionDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/checkPaymentOption';
import { DatabaseOperationsItem } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/InfoDbCardAccountDatabaseInfoDataModel';
import { RequestKind } from 'core/requestSender/enums';
import React from 'react';

type StateProps = {
    databaseId: string;
    databaseOperationsItem: DatabaseOperationsItem[];
    isDelimiters: boolean;
} & FloatMessageProps;

type OwnState = {
    isOpen: boolean;
    checkPaymentOptionReceived: boolean;
    /**
     * Информация о статусе оплаты
     */
    checkPaymentOption: CheckPaymentOptionDataModel | null;
    cost: number;
    currency: string;
    serviceId: string;
    isEnable: boolean;
};

class ArchiveOrBackupButtonClass extends React.Component<StateProps, OwnState> {
    constructor(props: StateProps) {
        super(props);

        this.closeDialog = this.closeDialog.bind(this);
        this.openDialog = this.openDialog.bind(this);
        this.switchType = this.switchType.bind(this);
        this.enableButton = this.enableButton.bind(this);

        this.state = {
            isOpen: false,
            checkPaymentOptionReceived: false,
            checkPaymentOption: null,
            cost: this.props.databaseOperationsItem[1].cost,
            currency: this.props.databaseOperationsItem[1].currency,
            serviceId: this.props.databaseOperationsItem[1].id,
            isEnable: true
        };
    }

    private openDialog() {
        const api = InterlayerApiProxy.getInfoDbCardApi();
        try {
            api.CheckPaymentOption(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, {
                cost: this.state.cost,
                currency: this.state.currency
            }).then(response => this.setState({ checkPaymentOption: response, checkPaymentOptionReceived: true }));
        } catch (er) {
            this.props.floatMessage.show(MessageType.Error, 'При получении данных о стоимости операции произошла ошибка.');
        }
        this.setState({
            isOpen: true
        });
    }

    private closeDialog() {
        this.setState({
            isOpen: false
        });
    }

    private switchType(type: number) {
        this.setState({
            cost: this.props.databaseOperationsItem[type].cost,
            currency: this.props.databaseOperationsItem[type].currency,
            serviceId: this.props.databaseOperationsItem[type].id,
        });
    }

    private enableButton() {
        this.setState({
            isEnable: false
        });
    }

    public render() {
        return (
            <>
                <OutlinedButton kind="success" onClick={ this.openDialog } isEnabled={ this.state.isEnable }>
                    Архивация
                </OutlinedButton>
                {
                    this.state.checkPaymentOptionReceived && <ArchiveOrBackupDialogMessage
                        isOpen={ this.state.isOpen }
                        completeStatus={ this.state.checkPaymentOption!.complete }
                        canUsePromisePayment={ this.state.checkPaymentOption!.canUsePromisePayment }
                        canIncreasePromisePayment={ this.state.checkPaymentOption!.canIncreasePromisePayment }
                        cost={ this.state.cost }
                        currency={ this.state.currency }
                        notEnoughMoney={ this.state.checkPaymentOption!.notEnoughMoney }
                        databaseId={ this.props.databaseId }
                        serviceId={ this.state.serviceId }
                        closeDialog={ this.closeDialog }
                        switchType={ this.switchType }
                        enableButton={ this.enableButton }
                        isDelimiters={ this.props.isDelimiters }
                    />
                }
            </>
        );
    }
}

export const ArchiveOrBackupButton = withFloatMessages(ArchiveOrBackupButtonClass);