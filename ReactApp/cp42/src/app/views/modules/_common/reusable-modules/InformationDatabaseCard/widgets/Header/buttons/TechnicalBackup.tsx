import { Box } from '@mui/material';
import { RequestType } from 'app/api/endpoints/eternalBackup/request';
import { FETCH_API } from 'app/api/useFetchApi';
import { AccountUserGroup, DatabaseState } from 'app/common/enums';
import { EMessageType, useAppSelector, useFloatMessages } from 'app/hooks';
import { OutlinedButton } from 'app/views/components/controls/Button';
import { Dialog } from 'app/views/components/controls/Dialog';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { TextOut } from 'app/views/components/TextOut';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { RequestKind } from 'core/requestSender/enums';
import React, { useState } from 'react';

const { ArchiveOrBackup } = InterlayerApiProxy.getInfoDbCardApi();
const { postBackups } = FETCH_API.ETERNAL_BACKUPS;

export const TechnicalBackup = () => {
    const { show } = useFloatMessages();

    const { getCurrentSessionSettingsReducer: { settings: { currentContextInfo: { currentUserGroups } } } } = useAppSelector(state => state.Global);
    const { informationBase } = useAppSelector(state => state.InformationBases.informationBasesList);
    const { id, state, isDbOnDelimiters } = informationBase?.database ?? {};

    const [isLoading, setIsLoading] = useState(false);
    const [isPressed, setIsPressed] = useState(false);
    const [isOpen, setIsOpen] = useState(false);

    const tryTechnicalBackupForDelimiter = async () => {
        try {
            setIsLoading(true);
            setIsPressed(true);
            await postBackups({ databaseId: id ?? '', force: true, type: RequestType.regular });
            show(EMessageType.success, 'Технический бекап будет готов в течении 30 минут');
        } catch {
            setIsPressed(false);
            show(EMessageType.error, 'Ошибка создания технического бекапа');
        } finally {
            setIsLoading(false);
        }
    };

    const tryTechnicalBackup = async () => {
        try {
            setIsLoading(true);
            setIsPressed(true);
            await ArchiveOrBackup(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, { databaseId: id ?? '', operation: 3 });
            show(EMessageType.success, 'Технический бекап будет готов в течении 30 минут');
        } catch {
            setIsPressed(false);
            show(EMessageType.error, 'Ошибка создания технического бекапа');
        } finally {
            setIsLoading(false);
        }
    };

    const closeDialog = () => {
        setIsOpen(false);
    };

    const openDialog = () => {
        setIsOpen(true);
    };

    const handleBackup = () => {
        if (isDbOnDelimiters) {
            void tryTechnicalBackupForDelimiter();
        } else {
            void tryTechnicalBackup();
        }

        closeDialog();
    };

    if (!(
        currentUserGroups.some(role => [AccountUserGroup.Hotline, AccountUserGroup.CloudAdmin, AccountUserGroup.CloudSE].includes(role)) &&
        state === DatabaseState.Ready
    )) {
        return null;
    }

    return (
        <>
            { isLoading && <LoadingBounce /> }
            <OutlinedButton isEnabled={ !isPressed } kind="success" onClick={ openDialog }>
                Технический бекап
            </OutlinedButton>
            <Dialog
                title="Создание технического бекапа"
                isOpen={ isOpen }
                dialogWidth="xs"
                dialogVerticalAlign="center"
                onCancelClick={ closeDialog }
                buttons={ [
                    {
                        content: 'Отмена',
                        onClick: closeDialog,
                        kind: 'default',
                        variant: 'outlined'
                    },
                    {
                        content: 'Создать',
                        onClick: handleBackup,
                        kind: 'success'
                    }
                ] }
            >
                <Box display="flex" flexDirection="column" gap="5px">
                    <TextOut>
                        Вы хотите создать технический бекап базы?
                    </TextOut>
                    <TextOut>Скачать бекап или восстановить базу вы сможете на вкладке - <b>«Архивные копии»</b>.</TextOut>
                    <TextOut>Время создания бекапа зависит от ее размера и <b>может занимать больше 30 минут</b>.</TextOut>
                </Box>
            </Dialog>
        </>
    );
};