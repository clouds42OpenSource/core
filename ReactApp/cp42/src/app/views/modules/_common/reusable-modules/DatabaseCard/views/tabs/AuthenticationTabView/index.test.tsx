import '@testing-library/jest-dom/extend-expect';
import { render, screen } from '@testing-library/react';
import { SnackbarProvider } from 'notistack';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';

import { AuthenticationTabView } from 'app/views/modules/_common/reusable-modules/DatabaseCard/views/tabs/AuthenticationTabView';
import { ReduxFormsReducerState } from 'app/redux/redux-forms/store/reducers/ReduxFormsReducerState';
import { getNewStore } from 'app/utils/StoreUtility';
import { SupportState } from 'app/common/enums';
import { DatabaseCardReducerState } from 'app/modules/databaseCard/store/reducers';

type TInitialState = {
    DatabaseCardState: DatabaseCardReducerState;
    ReduxForms: ReduxFormsReducerState<string>;
};

describe('<AuthenticationTabView />', () => {
    const store = getNewStore();
    const initialState: TInitialState = {
        DatabaseCardState: store.getState().DatabaseCardState,
        ReduxForms: store.getState().ReduxForms
    };

    const getRenderedComponent = (isInConnectingState: boolean) => {
        initialState.DatabaseCardState.tehSupportInfo = {
            isInConnectingState,
            lastHistoryDate: undefined,
            supportState: SupportState.AutorizationSuccess,
            supportStateDescription: 'test_description'
        };
        render(
            <Provider store={ configureStore([thunk])(initialState) }>
                <SnackbarProvider>
                    <AuthenticationTabView />
                </SnackbarProvider>
            </Provider>
        );
    };

    it('renders as need view', () => {
        getRenderedComponent(false);
        expect(screen.getByTestId(/authentication-need-view/i)).toBeInTheDocument();
    });

    it('renders as passed view', () => {
        getRenderedComponent(true);
        expect(screen.getByTestId(/authentication-passed-view/i)).toBeInTheDocument();
    });
});