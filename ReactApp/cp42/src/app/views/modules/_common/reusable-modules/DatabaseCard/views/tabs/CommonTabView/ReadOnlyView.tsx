import { DateUtility, NumberUtility } from 'app/utils';
import {
    DatabaseCaptionView,
    DatabaseDestributionTypeView,
    DatabasePlatformTypeView,
    DatabaseStateView,
    DatabaseTemplateView,
    FileStorageView,
    UsedWebServicesView
} from 'app/views/modules/_common/reusable-modules/DatabaseCard/views/tabs/CommonTabView/partial-elements';
import {
    DatabaseCardAccountDatabaseInfoDataModel,
    DatabaseCardEditModeDictionariesDataModel,
    DatabaseCardReadModeFieldAccessDataModel
} from 'app/web/InterlayerApiProxy/DatabaseCardApiProxy/receiveDatabaseCard/data-models';

import { Box, Grid } from '@mui/material';
import { DatabaseState } from 'app/common/enums/DatabaseState';
import { hasComponentChangesFor } from 'app/common/functions';
import { ReadOnlyKeyValueView } from 'app/views/modules/_common/components/ReadOnlyKeyValueView';
import { DatabaseCardEditDatabaseDataForm } from 'app/views/modules/_common/reusable-modules/DatabaseCard/types';
import {
    CopyTextToClipBoardButton
} from 'app/views/modules/_common/reusable-modules/DatabaseCard/views/_buttons/CopyTextToClipBoardButton';
import {
    DatabaseV82NameView
} from 'app/views/modules/_common/reusable-modules/DatabaseCard/views/tabs/CommonTabView/partial-elements/DatabaseV82NameView';
import cn from 'classnames';
import React from 'react';
import css from './styles.module.css';

type OwnProps = {
    reduxForm: DatabaseCardEditDatabaseDataForm;
    commonDetails: DatabaseCardAccountDatabaseInfoDataModel;
    launchPublishedDbUrl: string;
    databaseCardReadModeFieldAccessInfo: DatabaseCardReadModeFieldAccessDataModel;
    accountCardEditModeDictionaries: DatabaseCardEditModeDictionariesDataModel;
    onValueChange: <TValue>(fieldName: string, newValue: TValue, prevValue: TValue) => boolean | void;
    canEditDatabase: boolean;
};
export class ReadOnlyView extends React.Component<OwnProps> {
    public shouldComponentUpdate(nextProps: OwnProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    public render() {
        if (!this.props.commonDetails || !this.props.databaseCardReadModeFieldAccessInfo) {
            return null;
        }

        if (Object.keys(this.props.commonDetails).length === 0 || Object.keys(this.props.databaseCardReadModeFieldAccessInfo).length === 0) {
            return null;
        }
        const { reduxForm } = this.props;

        return (
            <div className={ cn(css.main) } data-testid="read-only-view">
                <Grid container={ true } spacing={ 2 }>
                    <Grid className={ css['col-wrapper'] } item={ true } xs={ 12 } sm={ 6 }>
                        <DatabaseCaptionView
                            formName="databaseCaption"
                            label="Наименование:"
                            readOnlyDatabaseCaption={ this.props.commonDetails.caption }
                            editDatabaseCaption={ reduxForm.databaseCaption }
                            isInEditMode={ this.props.canEditDatabase }
                            onValueChange={ this.props.onValueChange }
                        />
                        <DatabaseTemplateView
                            isInEditMode={ this.props.canEditDatabase }
                            formName="databaseTemplateId"
                            label="Шаблон:"
                            templates={ this.props.accountCardEditModeDictionaries.availableDatabaseTemplates }
                            readOnlyDatabaseTemplateName={ this.props.commonDetails.templateName }
                            readOnlyDatabaseTemplateId={ this.props.commonDetails.dbTemplate }
                            editDatabaseTemplateId={ reduxForm.databaseTemplateId }
                            onValueChange={ this.props.onValueChange }
                        />
                        <Box display="flex" flexDirection="row" gap="100px">
                            <ReadOnlyKeyValueView
                                label="Тип:"
                                text={ this.props.commonDetails.isFile ? 'Файловая' : 'Серверная' }
                            />
                            <ReadOnlyKeyValueView
                                label="Размер:"
                                text={
                                    <>
                                        { NumberUtility.megabytesToClassicFormat('-', this.props.commonDetails.sizeInMB) }
                                        {
                                            this.props.commonDetails.calculateSizeDateTime && this.props.commonDetails.sizeInMB > 0
                                                ? ` на (${ DateUtility.dateToDayMonthYear(this.props.commonDetails.calculateSizeDateTime) })`
                                                : ''
                                        }
                                    </>
                                }
                            />
                        </Box>
                        <DatabasePlatformTypeView
                            isInEditMode={ this.props.canEditDatabase }
                            formName="platformType"
                            label="Платформа:"
                            readOnlyPlatformType={ this.props.commonDetails.platformType }
                            editPlatformType={ reduxForm.platformType }
                            platformTypes={ this.props.accountCardEditModeDictionaries.availablePlatformTypes }
                            onValueChange={ this.props.onValueChange }
                        />
                        <DatabaseDestributionTypeView
                            isInEditMode={ this.props.canEditDatabase }
                            formName="distributionType"
                            label="Релиз платформы:"
                            stable82Version={ this.props.commonDetails.stable82Version }
                            stable83Version={ this.props.commonDetails.stable83Version }
                            alpha83Version={ this.props.commonDetails.alpha83Version }
                            readOnlyDistributionType={ this.props.commonDetails.distributionType }
                            editDistributionType={ reduxForm.distributionType }
                            platformType={ this.props.commonDetails.platformType }
                            distributionTypes={ this.props.accountCardEditModeDictionaries.availableDestributionTypes }
                            onValueChange={ this.props.onValueChange }
                        />
                        {
                            this.props.databaseCardReadModeFieldAccessInfo.canShowWebPublishPath
                                ? (
                                    <ReadOnlyKeyValueView
                                        label="База доступна по адресу:"
                                        text={
                                            <div style={ { display: 'flex' } }>
                                                <a
                                                    className={ cn(css['publish-url'], 'text-link') }
                                                    rel="noopener noreferrer"
                                                    href={ this.props.launchPublishedDbUrl }
                                                    target="_blank"
                                                >
                                                    { this.props.commonDetails.webPublishPath }
                                                </a>
                                                <CopyTextToClipBoardButton text={ this.props.launchPublishedDbUrl } />
                                            </div>
                                        }
                                    />
                                )
                                : null
                        }
                        <ReadOnlyKeyValueView
                            label="Изменена:"
                            text={ DateUtility.dateToDayMonthYear(this.props.commonDetails.lastActivityDate) }
                        />
                        {
                            this.props.databaseCardReadModeFieldAccessInfo.canShowUsedWebServices
                                ? (
                                    <UsedWebServicesView
                                        formName="usedWebServices"
                                        label="Опубликованы веб сервисы:"
                                        readOnlyUsedWebServices={ this.props.commonDetails.usedWebServices }
                                        editUsedWebServices={ reduxForm.usedWebServices }
                                        isInEditMode={ this.props.canEditDatabase }
                                        onValueChange={ this.props.onValueChange }
                                    />
                                )
                                : null
                        }
                    </Grid>
                    <Grid className={ css['col-wrapper'] } item={ true } xs={ 12 } sm={ 6 }>
                        {
                            this.props.databaseCardReadModeFieldAccessInfo.canShowDatabaseDetails
                                ? (
                                    <>
                                        <Box display="flex" flexDirection="row" gap="100px">
                                            <ReadOnlyKeyValueView
                                                label="Дата создания:"
                                                text={ DateUtility.dateToDayMonthYearHourMinuteSeconds(this.props.commonDetails.creationDate) }
                                            />
                                            <ReadOnlyKeyValueView
                                                label="Дата бекапа:"
                                                text={ this.props.commonDetails.backupDate ? DateUtility.dateToDayMonthYearHourMinuteSeconds(this.props.commonDetails.backupDate) : '' }
                                            />
                                        </Box>
                                        <DatabaseV82NameView
                                            formName="v82Name"
                                            label="Номер:"
                                            readOnlyV82Name={ this.props.commonDetails.v82Name }
                                            editV82Name={ reduxForm.v82Name }
                                            isInEditMode={ this.props.canEditDatabase }
                                            onValueChange={ this.props.onValueChange }
                                        />
                                        <ReadOnlyKeyValueView
                                            label="Ссылка на склеп:"
                                            text={ this.props.commonDetails.cloudStorageWebLink }
                                        />
                                        <Box display="flex" flexDirection="row" gap="150px">
                                            {
                                                !this.props.commonDetails.isFile
                                                    ? (
                                                        <ReadOnlyKeyValueView
                                                            label="Сервер 1С:"
                                                            text={ this.props.commonDetails.v82Server }
                                                        />
                                                    )
                                                    : null
                                            }
                                            {
                                                !this.props.commonDetails.isFile &&
                                                this.props.databaseCardReadModeFieldAccessInfo.canShowSqlServer
                                                    ? (
                                                        <ReadOnlyKeyValueView
                                                            label="Сервер SQL:"
                                                            text={ this.props.commonDetails.sqlServer }
                                                        />
                                                    )
                                                    : null
                                            }
                                        </Box>
                                        <Box display="flex" flexDirection="row" gap="6px">
                                            <>
                                                {this.props.commonDetails.version && <ReadOnlyKeyValueView
                                                    label="Номер релиза:"
                                                    text={ this.props.commonDetails.version }
                                                />}

                                                {this.props.commonDetails.configurationName && <ReadOnlyKeyValueView
                                                    label="Конфигурация:"
                                                    text={ this.props.commonDetails.configurationName }
                                                />}
                                            </>
                                        </Box>
                                        <ReadOnlyKeyValueView
                                            label="Путь к архиву:"
                                            text={ this.props.commonDetails.archivePath }
                                        />
                                        {
                                            this.props.commonDetails.isDbOnDelimiters && this.props.commonDetails.zoneNumber
                                                ? (
                                                    <ReadOnlyKeyValueView
                                                        label="Номер области:"
                                                        text={ this.props.commonDetails.zoneNumber }
                                                    />
                                                )
                                                : null
                                        }
                                        {
                                            this.props.commonDetails.isFile
                                                ? (
                                                    <ReadOnlyKeyValueView
                                                        label="Путь к базе:"
                                                        text={ this.props.commonDetails.filePath }
                                                    />
                                                )
                                                : null
                                        }
                                    </>
                                )
                                : null
                        }
                        {
                            this.props.databaseCardReadModeFieldAccessInfo.canShowDatabaseState
                                ? (
                                    <DatabaseStateView
                                        isInEditMode={ this.props.canEditDatabase }
                                        formName="databaseState"
                                        label="Статус:"
                                        readOnlyState={ this.props.commonDetails.databaseState }
                                        editState={ reduxForm.databaseState }
                                        states={ this.props.accountCardEditModeDictionaries.availableDatabaseStates }
                                        onValueChange={ this.props.onValueChange }
                                    />
                                )
                                : null
                        }
                        {
                            this.props.databaseCardReadModeFieldAccessInfo.canShowDatabaseFileStorage
                                ? (
                                    <FileStorageView
                                        isInEditMode={ this.props.canEditDatabase }
                                        formName="FileStorageId"
                                        label="Хранилище:"
                                        readOnlyFileStorageName={ this.props.commonDetails.fileStorageName }
                                        readOnlyFileStorageId={ this.props.commonDetails.fileStorage }
                                        editFileStorageId={ reduxForm.fileStorageId }
                                        fileStorages={ this.props.accountCardEditModeDictionaries.availableFileStorages }
                                        onValueChange={ this.props.onValueChange }
                                    />
                                )
                                : null
                        }
                        {
                            this.props.commonDetails.databaseState === DatabaseState.ErrorNotEnoughSpace ||
                            this.props.commonDetails.databaseState === DatabaseState.ErrorCreate ||
                            this.props.commonDetails.databaseState === DatabaseState.ErrorDtFormat
                                ? (
                                    <ReadOnlyKeyValueView
                                        label="Ошибка:"
                                        text="При создании базы возникла ошибка. Вы можете удалить эту базу самостоятельно или обратитесь в Хотлайн."
                                    />
                                )
                                : null
                        }
                        <ReadOnlyKeyValueView
                            label="Идентификатор:"
                            text={ this.props.commonDetails.id }
                        />
                    </Grid>
                </Grid>
            </div>
        );
    }
}