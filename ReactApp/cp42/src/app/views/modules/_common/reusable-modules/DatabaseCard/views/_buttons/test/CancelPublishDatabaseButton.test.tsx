import '@testing-library/jest-dom/extend-expect';
import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import { SnackbarProvider } from 'notistack';
import configureStore from 'redux-mock-store';

import { CancelPublishDatabaseButton } from 'app/views/modules/_common/reusable-modules/DatabaseCard/views/_buttons/CancelPublishDatabaseButton';
import { getNewStore } from 'app/utils/StoreUtility';
import { DatabaseCardReducerState } from 'app/modules/databaseCard/store/reducers';

type TInitialState = {
    DatabaseCardState: DatabaseCardReducerState;
};

describe('<CancelPublishDatabaseButton />', () => {
    const initialState: TInitialState = {
        DatabaseCardState: getNewStore().getState().DatabaseCardState
    };

    const getRenderedComponent = (usedWebServices: boolean, isInCancelingPublishedDatabaseState: boolean) => {
        render(
            <SnackbarProvider>
                <Provider store={ configureStore()(initialState) }>
                    <CancelPublishDatabaseButton
                        databaseId="test_database_id"
                        usedWebServices={ usedWebServices }
                        isInCancelingPublishedDatabaseState={ isInCancelingPublishedDatabaseState }
                    />
                </Provider>
            </SnackbarProvider>
        );
    };

    it('renders with isInCancelingPublishedDatabaseState equal true', () => {
        getRenderedComponent(false, true);
        expect(screen.getByText(/База в процессе снятия публикации/i)).toBeInTheDocument();
    });

    it('renders with isInCancelingPublishedDatabaseState equal false', () => {
        getRenderedComponent(false, false);
        expect(screen.getByText(/Отменить публикацию/i)).toBeInTheDocument();
    });
});