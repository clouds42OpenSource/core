/* eslint-disable react/no-unstable-nested-components */
import HelpOutlineIcon from '@mui/icons-material/HelpOutline';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import { Box, Button, ClickAwayListener, Collapse, Fade, Popper } from '@mui/material';
import Tooltip from '@mui/material/Tooltip';
import { LocalizationProvider, TimePicker } from '@mui/x-date-pickers';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { FETCH_API } from 'app/api/useFetchApi';
import { AppRoutes } from 'app/AppRoutes';
import { AccountUserGroup, DatabaseState } from 'app/common/enums';
import { DatabaseCardReducerState } from 'app/modules/databaseCard/store/reducers';
import { AppReduxStoreState } from 'app/redux/types';
import { COLORS, DateUtility, NumberUtility } from 'app/utils';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { ContainedButton } from 'app/views/components/controls/Button';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { TextOut } from 'app/views/components/TextOut';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { ArchivalInfoDbListFilterForm } from 'app/views/modules/_common/reusable-modules/InfoDbCard/infoDbCardContent/tabs/ArchivalСopiesTabView/ArchivalInfoDbListFilterFormView';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { BuckupInfoDbDataModel, BuckupItemInfoDbDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/BuckupInfoDbDataModel';
import { InfoDbCardAccountDatabaseInfoDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/InfoDbCardAccountDatabaseInfoDataModel';
import { BackupsItem } from 'app/web/InterlayerApiProxy/MsBackupsApiProxy/getBackups';
import cn from 'classnames';
import { RequestKind } from 'core/requestSender/enums';
import ruLocale from 'date-fns/locale/ru';
import dayjs from 'dayjs';
import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import css from '../../../styles.module.css';

type StateProps = {
    database: DatabaseCardReducerState;
    backupsInfoDb: BuckupInfoDbDataModel;
    hasHasSupportReceived: boolean;
    commonDetails: InfoDbCardAccountDatabaseInfoDataModel;
    currentUserGroups: AccountUserGroup[];
};

type OwnProps = {
    accountGroup: boolean;
    status: boolean;
    state: DatabaseState;
    applicationId: string;
};

type OwnState = {
    anchorEl: HTMLElement | null;
    open: string;
    openModal: boolean;
    msBackupsList: BackupsItem[];
    isOpened: boolean;
    StartingTime: Date;
    EndingTime: Date;
    minInterval: number;
    isLoading: boolean;
};

type AllProps = StateProps & OwnProps & FloatMessageProps;

const { getBackupUrl } = FETCH_API.ACCOUNT_DATABASES;

export class ArchivalCopiesViewClass extends React.Component<AllProps, OwnState> {
    public constructor(props: AllProps) {
        super(props);
        this.state = {
            anchorEl: null,
            open: '',
            openModal: false,
            msBackupsList: [],
            isOpened: false,
            StartingTime: new Date(),
            EndingTime: new Date(),
            minInterval: 0,
            isLoading: false
        };
        this.openDialogMessage = this.openDialogMessage.bind(this);
        this.onClickAway = this.onClickAway.bind(this);
        this.onClickAddBase = this.onClickAddBase.bind(this);
        this.setDataIsDbOnDelimiters = this.setDataIsDbOnDelimiters.bind(this);
        this.getDownloadFile = this.getDownloadFile.bind(this);
        this.renderArchivalDb = this.renderArchivalDb.bind(this);
        this.renderArchivalDbOnDelimiters = this.renderArchivalDbOnDelimiters.bind(this);
        this.convertTimeStringToDate = this.convertTimeStringToDate.bind(this);
        this.download = this.download.bind(this);
        this.handleSetBackupsSchedule = this.handleSetBackupsSchedule.bind(this);
        this.handleStartTimeChange = this.handleStartTimeChange.bind(this);
        this.handleEndTimeChange = this.handleEndTimeChange.bind(this);
    }

    public componentDidMount() {
        const { commonDetails, floatMessage, applicationId } = this.props;

        if (commonDetails.isDbOnDelimiters) {
            (async () => {
                const { data } = await FETCH_API.APPLICATIONS.getBackupsSchedule({ applicationId });

                if (data) {
                    // const [intervalHours, intervalMinutes] = data.MinimumTime.split(':').map(Number);
                    // const intervalInMinutes = intervalHours * 60 + intervalMinutes;
                    // this.setState({ StartingTime: this.convertTimeStringToDate(data.StartingTime), EndingTime: this.convertTimeStringToDate(data.EndingTime), minInterval: intervalInMinutes });
                }

                try {
                    const { backups } = await InterlayerApiProxy.getMsBackupsApiProxy().getBackups(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, { applicationId: this.props.commonDetails.id });
                    this.setState({ msBackupsList: backups });
                } catch (err: unknown) {
                    floatMessage.show(MessageType.Error, err);
                }
            })();
        }
    }

    private handleStartTimeChange(newStartTime: Date) {
        const { EndingTime } = this.state;
        this.setState({ StartingTime: newStartTime });

        if (EndingTime && newStartTime) {
            const minEndTime = new Date(newStartTime.getTime() + this.state.minInterval * 60000);

            if (EndingTime < minEndTime) {
                this.setState({ EndingTime: minEndTime });
            }
        }
    }

    private async handleSetBackupsSchedule() {
        const { show } = this.props.floatMessage;

        this.setState({ isLoading: true });
        const response = await FETCH_API.APPLICATIONS.setBackupsSchedule({
            applicationId: this.props.applicationId,
            StartingTime: dayjs(this.state.StartingTime).format('HH:mm'),
            EndingTime: dayjs(this.state.EndingTime).format('HH:mm')
        });
        this.setState({ isLoading: false });

        if (response.success) {
            show(MessageType.Success, 'Время формирования создания бэкапов успешно изменено');
        } else {
            show(MessageType.Error, response.message);
        }
    }

    private handleEndTimeChange(newEndTime: Date) {
        this.setState({ EndingTime: newEndTime });

        if (this.state.StartingTime && newEndTime) {
            const maxStartTime = new Date(newEndTime.getTime() - this.state.minInterval * 60000);
            if (this.state.StartingTime > maxStartTime) {
                this.setState({ StartingTime: maxStartTime });
            }
        }
    }

    private onClickAway() {
        this.setState({ anchorEl: null, open: '' });
    }

    private onClickAddBase(event: React.MouseEvent<HTMLElement>, value: string) {
        const { anchorEl } = this.state;

        this.setState(anchorEl ? { anchorEl: null, open: '' } : { anchorEl: event.currentTarget, open: value });
    }

    private setDataIsDbOnDelimiters(sortData: BackupsItem[]) {
        this.setState({ msBackupsList: sortData });
    }

    private async getDownloadFile(id: string, isDbOnDelimiters = false) {
        const { show } = this.props.floatMessage;

        if (isDbOnDelimiters) {
            try {
                this.setState({ open: '' });
                await this.download(id);
            } catch (err) {
                show(MessageType.Error, err);
            }
        } else {
            const { data, message } = await getBackupUrl(id);

            if (data) {
                window.open(data, '_blank');
            } else {
                show(MessageType.Error, message ?? 'Не удалось получить файл архивной копии');
            }
        }
    }

    private convertTimeStringToDate(timeString: string) {
        const [hours, minutes] = timeString.split(':').map(Number);
        const time = new Date();
        time.setHours(hours, minutes, 0, 0);

        return time;
    }

    private async download(id: string) {
        const { status, header, url } = await InterlayerApiProxy.getMsBackupsApiProxy().getDownloadFiles(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, { fileId: id });

        if (status === 204) {
            const retry = header.get('Retry-After') ?? '0';

            this.props.floatMessage.show(MessageType.Info, `Пожалуйста, подождите. Файл подготавливается. Примерное время ожидания ${ retry } секунд.`);
            setTimeout(this.download, parseInt(`${ retry }000`, 10), id);
        }

        if (status === 200) {
            window.open(url);
        }
    }

    private openDialogMessage() {
        this.setState(({ openModal }) => ({ openModal: !openModal }));
    }

    private renderArchivalDb() {
        return (
            <>
                <CommonTableWithFilter
                    uniqueContextProviderStateId="AccessInfoDbListContextProviderStateId"
                    filterProps={ {
                        getFilterContentView: () => <ArchivalInfoDbListFilterForm
                            databaseId={ this.props.commonDetails.id }
                            isDbOnDelimiters={ false }
                            setData={ () => void 0 }
                            status={ this.props.status }
                        />
                    } }
                    tableProps={ {
                        dataset: this.props.backupsInfoDb.databaseBackups ?? [],
                        keyFieldName: 'id',
                        fieldsView: {
                            creationDate: {
                                caption: 'Дата и время',
                                format: (value: Date) =>
                                    DateUtility.dateToDayMonthYearHourMinute(value)
                            },
                            initiator: {
                                caption: 'Инициатор',
                            },
                            eventTriggerDescription: {
                                caption: 'Триггер',
                            },
                            id: {
                                caption: '',
                                format: (value: string, item: BuckupItemInfoDbDataModel) =>
                                    <div className={ cn(css.root) }>
                                        <ContainedButton
                                            key={ value }
                                            aria-describedby={ value }
                                            kind={ this.state.open === value ? 'primary' : 'success' }
                                            className={ cn(css['archival-btn']) }
                                            onClick={ (event: React.MouseEvent<HTMLElement>) => this.onClickAddBase(event, value) }
                                        >
                                            <Box sx={ { display: 'flex', alignItems: 'center', justifyContent: 'center' } }>
                                                <i className="fa fa-bars" />
                                            </Box>
                                        </ContainedButton>
                                        <Popper
                                            placement="bottom-end"
                                            className={ cn(css.dropdown) }
                                            id={ value }
                                            open={ this.state.open === value }
                                            anchorEl={ this.state.anchorEl }
                                            transition={ true }
                                            hidden={ this.state.openModal }
                                        >
                                            { ({ TransitionProps }) => (
                                                <ClickAwayListener onClickAway={ this.onClickAway }>
                                                    <Fade { ...TransitionProps } timeout={ 300 }>
                                                        <Box>
                                                            { this.props.status && (
                                                                <div>
                                                                    <Link
                                                                        className={ cn(css.linkDb) }
                                                                        to={ {
                                                                            pathname: AppRoutes.accountManagement.createAccountDatabase.loadingDatabase,
                                                                            state: { fileId: item.id, restoreType: '1' }
                                                                        } }
                                                                    >
                                                                        <TextOut inDiv={ true } fontSize={ 14 } fontWeight={ 400 }>
                                                                            Восстановить в новую
                                                                        </TextOut>
                                                                    </Link>
                                                                    <Link
                                                                        className={ cn(css.linkDb) }
                                                                        to={ {
                                                                            pathname: AppRoutes.accountManagement.createAccountDatabase.loadingDatabase,
                                                                            state: { fileId: item.id, restoreType: '0', state: this.props.state }
                                                                        } }
                                                                    >
                                                                        <TextOut inDiv={ true } fontSize={ 14 } fontWeight={ 400 }>
                                                                            Восстановить с заменой
                                                                        </TextOut>
                                                                    </Link>
                                                                </div>
                                                            ) }
                                                            { item.eventTrigger !== 4 && item.sourceType !== 0 && (
                                                                <TextOut className={ css.linkDb } inDiv={ true } fontSize={ 14 } fontWeight={ 400 } onClick={ () => this.getDownloadFile(item.id) }>
                                                                    Скачать
                                                                </TextOut>
                                                            )
                                                            }
                                                        </Box>
                                                    </Fade>
                                                </ClickAwayListener>
                                            ) }
                                        </Popper>
                                    </div>
                            },
                        }
                    } }
                />
                { !!this.props.backupsInfoDb.dbBackupsCount && (
                    <TextOut style={ { marginTop: '15px' } } inDiv={ true } fontSize={ 13 } fontWeight={ 400 }>
                        Записей: { this.props.backupsInfoDb.dbBackupsCount }
                    </TextOut>
                ) }
            </>
        );
    }

    private renderArchivalDbOnDelimiters() {
        return (
            <>
                { this.state.isLoading && <LoadingBounce /> }
                <Box>
                    <Button onClick={ () => this.setState(({ isOpened }) => ({ isOpened: !isOpened })) } style={ { color: COLORS.font } }>
                        { this.state.isOpened ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon /> }
                        <TextOut>
                            Настройка времени формирования резервных копий
                        </TextOut>
                    </Button>
                    <Collapse in={ this.state.isOpened }>
                        <Box display="flex" alignItems="center" gap="6px" padding="6px 0" margin="6px 0">
                            <TextOut>
                                Формировать копии с
                            </TextOut>
                            <LocalizationProvider dateAdapter={ AdapterDateFns } adapterLocale={ ruLocale }>
                                <TimePicker
                                    value={ this.state.StartingTime }
                                    onChange={ value => value && this.handleStartTimeChange(value) }
                                    label="Выберите время"
                                />
                                <TextOut>
                                    по
                                </TextOut>
                                <TimePicker
                                    value={ this.state.EndingTime }
                                    onChange={ value => value && this.handleEndTimeChange(value) }
                                    label="Выберите время"
                                />
                                <Tooltip title={ `Минимальный интервал времени создания бэкапа ${ dayjs().startOf('day').add(this.state.minInterval, 'minute').format('HH:mm') }` }>
                                    <HelpOutlineIcon />
                                </Tooltip>
                                <Button onClick={ this.handleSetBackupsSchedule }>
                                    Сохранить
                                </Button>
                            </LocalizationProvider>
                        </Box>
                    </Collapse>
                </Box>
                <CommonTableWithFilter
                    uniqueContextProviderStateId="AccessInfoDbListOnDelimitersContextProviderStateId"
                    filterProps={ {
                        getFilterContentView: () => <ArchivalInfoDbListFilterForm
                            databaseId={ this.props.commonDetails.id }
                            isDbOnDelimiters={ true }
                            setData={ this.setDataIsDbOnDelimiters }
                            status={ this.props.status }
                        />
                    } }
                    tableProps={ {
                        dataset: this.state.msBackupsList,
                        keyFieldName: 'id',
                        fieldsView: {
                            date: {
                                caption: 'Дата и время',
                                fieldContentNoWrap: false,
                                fieldWidth: '15%',
                                format: value =>
                                    DateUtility.dateToDayMonthYearHourMinute(new Date(value))
                            },
                            appVersion: {
                                caption: 'Версия конфигурции'
                            },
                            fileSize: {
                                caption: 'Размер',
                                format: value =>
                                    NumberUtility.bytesToMegabytes(value)
                            },
                            isOndemand: {
                                caption: 'Вид',
                                format: (value, row) => {
                                    return row.type === 'dt' ? 'Выгрузка dt' : (value ? 'Ручная' : 'Регламентная');
                                }
                            },
                            id: {
                                caption: '',
                                fieldContentNoWrap: false,
                                fieldWidth: '10%',
                                format: (value: string, item) =>
                                    <div className={ cn(css.root) }>
                                        <ContainedButton
                                            key={ value }
                                            aria-describedby={ value }
                                            kind={ this.state.open === value ? 'primary' : 'success' }
                                            className={ cn(css['archival-btn']) }
                                            onClick={ (event: React.MouseEvent<HTMLElement>) => this.onClickAddBase(event, value) }
                                        >
                                            <Box sx={ { display: 'flex', alignItems: 'center', justifyContent: 'center' } }>
                                                <i className="fa fa-bars" />
                                            </Box>
                                        </ContainedButton>
                                        <Popper
                                            placement="bottom-end"
                                            className={ cn(css.dropdown) }
                                            id={ value }
                                            open={ this.state.open === value }
                                            anchorEl={ this.state.anchorEl }
                                            transition={ true }
                                            hidden={ this.state.openModal }
                                        >
                                            { ({ TransitionProps }) => (
                                                <ClickAwayListener onClickAway={ this.onClickAway }>
                                                    <Fade { ...TransitionProps } timeout={ 0 }>
                                                        <Box>
                                                            { this.props.status && item.fileId && item.type !== 'dt' && (
                                                                <Link
                                                                    className={ cn(css.linkDb) }
                                                                    to={ {
                                                                        pathname: AppRoutes.accountManagement.createAccountDatabase.loadingDatabase,
                                                                        state: {
                                                                            isZip: true,
                                                                            fileId: item.fileId,
                                                                            caption: this.props.database.commonDetails.caption,
                                                                            date: item.date
                                                                        }
                                                                    } }
                                                                >
                                                                    <TextOut inDiv={ true } fontSize={ 14 } fontWeight={ 400 }>
                                                                        Восстановить
                                                                    </TextOut>
                                                                </Link>
                                                            ) }
                                                            <TextOut
                                                                className={ css.linkDb }
                                                                inDiv={ true }
                                                                fontSize={ 14 }
                                                                fontWeight={ 400 }
                                                                onClick={
                                                                    item.fileId !== ''
                                                                        ? () => this.getDownloadFile(item.fileId, true)
                                                                        : () => window.open(item.eternalBackupUrl, '_blank')
                                                                }
                                                            >
                                                                Скачать
                                                            </TextOut>
                                                        </Box>
                                                    </Fade>
                                                </ClickAwayListener>
                                            ) }
                                        </Popper>
                                    </div>
                            }
                        }
                    } }
                />
            </>
        );
    }

    public render() {
        if (!this.props.commonDetails.isDbOnDelimiters) {
            return this.renderArchivalDb();
        }

        return this.renderArchivalDbOnDelimiters();
    }
}

const ArchivalCopiesConnect = connect<StateProps, NonNullable<unknown>, NonNullable<unknown>, AppReduxStoreState>(
    state => {
        const databaseCardState = state.InfoDbCardState;
        const { backupsInfoDb } = databaseCardState;
        const { hasHasSupportReceived } = databaseCardState.hasSuccessFor;
        const { commonDetails } = databaseCardState;
        const { currentUserGroups } = state.Global.getCurrentSessionSettingsReducer.settings.currentContextInfo;

        return {
            database: databaseCardState,
            backupsInfoDb,
            hasHasSupportReceived,
            commonDetails,
            currentUserGroups
        };
    }
)(ArchivalCopiesViewClass);

export const ArchivalCopiesView = withFloatMessages(ArchivalCopiesConnect);