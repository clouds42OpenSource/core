import { DatabaseState, SortingKind } from 'app/common/enums';
import { hasComponentChangesFor } from 'app/common/functions';
import { ProcessIdStateHelper } from 'app/common/helpers/ProcessIdStateHelper';
import { AccountDatabasesForMigrationProcessId } from 'app/modules/accountDatabasesForMigrations/AccountDatabasesForMigrationProcessId';
import { ReceiveAccountDatabasesForMigrationThunkParams } from 'app/modules/accountDatabasesForMigrations/store/reducers/receiveAccountDatabasesForMigrationReducer/params';
import { AccountDatabasesForMigrationThunk } from 'app/modules/accountDatabasesForMigrations/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { CommonTableWithFilter, SelectDataInitiator } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { TablePagination } from 'app/views/modules/_common/components/CommonTableWithFilter/TableView';
import { AccountDatabasesForMigrationsFilterDataForm } from 'app/views/modules/_common/reusable-modules/AccountDatabasesForMigrations/types';
import { CopyAccountDatabasePathButton } from 'app/views/modules/_common/reusable-modules/AccountDatabasesForMigrations/views/_buttons/CopyAccountDatabasePathButton';
import { PrepareToMigrateAccountDatabaseButton } from 'app/views/modules/_common/reusable-modules/AccountDatabasesForMigrations/views/_buttons/PrepareToMigrateAccountDatabaseButton';
import { AccountDatabasesFilterFormView } from 'app/views/modules/_common/reusable-modules/AccountDatabasesForMigrations/views/AccountDatabasesFilterFormView';
import { SelectDataCommonDataModel, SortingDataModel } from 'app/web/common/data-models';
import { AccountDatabaseMigrationItemDataModel } from 'app/web/InterlayerApiProxy/AccountApiProxy/receiveAccountDatabasesForMigration/data-models';
import cn from 'classnames';
import React from 'react';
import { connect } from 'react-redux';
import css from '../styles.module.css';

type OwnProps = {
    accountIndexNumber: number;
};

type FakeDbItemProps = {
    migrateAction: any;
};

type StateProps<TDataRow = AccountDatabaseMigrationItemDataModel> = {
    dataset: (TDataRow & FakeDbItemProps)[];
    isInLoadingDataProcess: boolean;
    metadata: TablePagination;
    accountId: string;
};

type DispatchProps = {
    dispatchAccountDatabasesForMigrationThunk: (args: ReceiveAccountDatabasesForMigrationThunkParams) => void;
};

type AllProps = OwnProps & StateProps & DispatchProps;

class AccountDatabasesViewClass extends React.Component<AllProps> {
    public constructor(props: AllProps) {
        super(props);
        this.onDataSelect = this.onDataSelect.bind(this);
    }

    public shouldComponentUpdate(nextProps: AllProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private getSortQuery(args: SortingDataModel) {
        return `${ args.fieldName }.${ args.sortKind ? 'desc' : 'asc' }`;
    }

    private onDataSelect(args: SelectDataCommonDataModel<AccountDatabasesForMigrationsFilterDataForm>,
        _filterFormName: string | null, initiator: SelectDataInitiator) {
        this.props.dispatchAccountDatabasesForMigrationThunk({
            pageNumber: args.pageNumber,
            filter: {
                accountIndexNumber: args.filter?.accountIndexNumber ?? 0,
                accountId: this.props.accountId,
                searchLine: args.filter?.searchLine
            },
            orderBy: args.sortingData ? this.getSortQuery(args.sortingData) : 'caption.desc',
            force: initiator !== 'initial'
        });
    }

    public render() {
        return (
            <CommonTableWithFilter
                uniqueContextProviderStateId="Configurations1CContextProviderStateId"
                filterProps={ {
                    getFilterContentView: (ref, onFilterChanged) => <AccountDatabasesFilterFormView
                        isFilterFormDisabled={ this.props.isInLoadingDataProcess }
                        ref={ ref }
                        onFilterChanged={ onFilterChanged }
                        accountIndexNumber={ this.props.accountIndexNumber }
                    />
                } }
                tableProps={ {
                    dataset: this.props.dataset,
                    keyFieldName: 'accountDatabaseId',
                    sorting: {
                        sortFieldName: 'v82Name',
                        sortKind: SortingKind.Asc
                    },
                    fieldsView: {
                        v82Name: {
                            caption: 'Номер',
                            fieldContentNoWrap: false,
                            isSortable: true,
                            fieldWidth: 'auto'
                        },
                        path: {
                            caption: 'Путь',
                            fieldContentWrapEverySymbol: true,
                            fieldContentNoWrap: false,
                            isSortable: false,
                            fieldWidth: 'auto',
                            format: (value: string, data) => {
                                return (
                                    <>
                                        <div className={ cn(css['copy-accountdatabase-path-button-container']) }>
                                            <CopyAccountDatabasePathButton inputElementId={ `input${ data.accountDatabaseId }` } />
                                        </div>
                                        <div className={ cn(css['accountdatabase-path-container']) }>
                                            <input className={ cn(css.accountDatabasePathContainer) } type="text" id={ `input${ data.accountDatabaseId }` } readOnly={ true } defaultValue={ value } />
                                        </div>
                                    </>
                                );
                            }
                        },
                        isFile: {
                            caption: 'Тип',
                            fieldContentNoWrap: true,
                            isSortable: false,
                            fieldWidth: 'auto',
                            format: (value: boolean) => {
                                return value ? 'Ф' : 'С';
                            }
                        },
                        size: {
                            caption: 'Размер, МБ',
                            fieldContentNoWrap: false,
                            isSortable: false,
                            fieldWidth: 'auto'
                        },
                        isPublishDatabase: {
                            caption: 'Web базы',
                            fieldContentNoWrap: true,
                            isSortable: false,
                            fieldWidth: 'auto',
                            format: (value: boolean) => {
                                return value ? '+' : '';
                            }
                        },
                        isPublishServices: {
                            caption: 'Web сервис',
                            fieldContentNoWrap: true,
                            isSortable: false,
                            fieldWidth: 'auto',
                            format: (value: boolean) => {
                                return value ? '+' : '';
                            }
                        },
                        migrateAction: {
                            caption: '',
                            format: (_value, row) => {
                                return row.status === DatabaseState.Ready && row.isFile
                                    ? (
                                        <div className={ cn(css['prepare-for-migration-container']) }>
                                            <PrepareToMigrateAccountDatabaseButton databaseNumber={ row.v82Name } />
                                        </div>
                                    )
                                    : null;
                            }
                        }
                    },
                    pagination: this.props.metadata
                } }
                onDataSelect={ this.onDataSelect }
            />
        );
    }
}

export const AccountDatabasesView = connect<StateProps, DispatchProps, OwnProps, AppReduxStoreState>(
    state => {
        const accountDatabasesForMigrationState = state.AccountDatabasesForMigrationState;
        const databasesInfo = accountDatabasesForMigrationState.accountDatabasesMigrationInfo.databases;
        const accountCardState = state.AccountCardState;
        const accountInfo = accountCardState?.accountInfo;
        const dataset = databasesInfo.records.map(item => {
            return {
                ...item,
                migrateAction: true,
            };
        });

        const currentPage = databasesInfo.metadata.pageNumber;
        const totalPages = databasesInfo.metadata.pageCount;
        const { pageSize } = databasesInfo.metadata;
        const recordsCount = databasesInfo.metadata.totalItemCount;

        const isInLoadingDataProcess = ProcessIdStateHelper.isInProgress(accountDatabasesForMigrationState.reducerActions, AccountDatabasesForMigrationProcessId.ReceiveAccountDatabasesForMigration);
        return {
            dataset,
            isInLoadingDataProcess,
            metadata: {
                currentPage,
                totalPages,
                pageSize,
                recordsCount
            },
            accountId: accountInfo?.id

        };
    },
    {
        dispatchAccountDatabasesForMigrationThunk: AccountDatabasesForMigrationThunk.invoke
    }
)(AccountDatabasesViewClass);