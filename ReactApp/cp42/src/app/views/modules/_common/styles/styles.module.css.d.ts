declare const styles: {
    readonly 'search-button': string;
    readonly 'search-button-margin': string;
    readonly 'button-text': string;
    readonly 'date-range': string;
    readonly 'icon-link': string;
};
export = styles;