import { render } from '@testing-library/react';
import '@testing-library/jest-dom';

import { VipAccountValue } from 'app/views/modules/_common/shared/PartnersPages/vipAccountValue/index';

describe('<VipAccountValue />', () => {
    it('render component with VIP status', () => {
        const { getByText } = render(<VipAccountValue value="value" isCompanyName={ true } />);
        expect(getByText(/VIP/i)).toBeInTheDocument();
        expect(getByText(/value/i)).toBeInTheDocument();
    });

    it('render component without VIP status', () => {
        const { getByText } = render(<VipAccountValue value="value" />);
        expect(getByText(/value/i)).toBeInTheDocument();
        expect(() => getByText(/VIP/i)).toThrow();
    });
});