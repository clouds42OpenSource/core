import React from 'react';
import { Link } from '@mui/material';
import dayjs from 'dayjs';

import { TAgentPayment } from 'app/api/endpoints/partners/response';
import { EAgentPaymentSourceType, EPaymentType, ETransactionType } from 'app/api/endpoints/partners/enum';
import { TextOut } from 'app/views/components/TextOut';
import { setCashFormat } from 'app/common/helpers/setCashFormat';

import styles from './style.module.css';

type TVipAccountValue = {
    value: string | number;
    isCompanyName?: boolean;
    linkHandler?: (id: number) => void;
    linkId?: number;
    noNeedVipText?: boolean;
};

export const formatVipAccount = (value: string | number, { agentPaymentSourceType, paymentType }: TAgentPayment) => {
    const isStyled = (agentPaymentSourceType === EAgentPaymentSourceType.agentCashOutRequest && paymentType === EPaymentType.outflow) ||
        agentPaymentSourceType === EAgentPaymentSourceType.agentTransferBalanceRequest;

    return (
        <>
            { typeof value === 'string' && value === '0,00'
                ? <div />
                : isStyled ? <VipAccountValue value={ value } /> : value
            }
        </>
    );
};

export const formatTransactionDate = (value: string, agentPayment: TAgentPayment, withTime = true) => {
    return formatVipAccount(withTime ? dayjs(value).format('DD.MM.YYYY HH:mm:ss') : dayjs(value).format('DD.MM.YYYY'), agentPayment);
};

export const formatAccountCaption =
    (
        value: string,
        { agentPaymentSourceType, paymentType, isVipAccount }: TAgentPayment,
        linkHandler?: (id: number) => void,
        linkId?: number,
        noNeedVipText?: boolean
    ) => {
        const isStyled = (agentPaymentSourceType === EAgentPaymentSourceType.agentCashOutRequest && paymentType === EPaymentType.outflow) ||
        agentPaymentSourceType === EAgentPaymentSourceType.agentTransferBalanceRequest;

        if (isStyled) {
            return <VipAccountValue value={ value } isCompanyName={ isVipAccount } linkHandler={ linkHandler } linkId={ linkId } noNeedVipText={ noNeedVipText } />;
        }

        if (linkId && linkHandler) {
            return <Link onClick={ () => linkHandler(linkId) }>{ value }</Link>;
        }

        return <TextOut>{ value } { isVipAccount && <span className={ styles.vipTextThin }>VIP</span> }</TextOut>;
    };

export const formatAgentPaymentSourceType = (value: EAgentPaymentSourceType, agentPayment: TAgentPayment) => {
    if (value === EAgentPaymentSourceType.agentCashOutRequest || value === EAgentPaymentSourceType.agentTransferBalanceRequest) {
        return formatVipAccount('', agentPayment);
    }

    if (agentPayment.transactionType === ETransactionType.money) {
        return formatVipAccount('Основной', agentPayment);
    }

    return formatVipAccount('Бонусный', agentPayment);
};

export const formatTransactionSum = (value: number, agentPayment: TAgentPayment) => {
    const formattedValue = setCashFormat(value);

    if (
        (agentPayment.agentPaymentSourceType === EAgentPaymentSourceType.agentCashOutRequest &&
            agentPayment.paymentType === EPaymentType.outflow) ||
        agentPayment.agentPaymentSourceType === EAgentPaymentSourceType.agentTransferBalanceRequest
    ) {
        return formatVipAccount(`+${ formattedValue }`, agentPayment);
    }

    if (agentPayment.paymentType === EPaymentType.outflow) {
        return formatVipAccount(`-${ formattedValue }`, agentPayment);
    }

    return formatVipAccount(formattedValue, agentPayment);
};

export const formatClientPaymentSum = (value: number, agentPayment: TAgentPayment) => {
    const formattedValue = setCashFormat(value);

    return formatVipAccount(formattedValue, agentPayment);
};

export const VipAccountValue = ({ value, isCompanyName, linkHandler, linkId, noNeedVipText }: TVipAccountValue) => {
    return (
        <div className={ styles.container }>
            { linkHandler && linkId
                ? <Link onClick={ () => linkHandler(linkId) } className={ styles.link }>{ value }</Link>
                : <span className={ styles.vip }>{ value }</span>
            }
            { isCompanyName && !noNeedVipText && <span className={ styles.vipText }>VIP</span> }
        </div>
    );
};