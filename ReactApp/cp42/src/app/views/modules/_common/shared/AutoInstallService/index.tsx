import { FormControlLabel } from '@mui/material';
import Checkbox from '@mui/material/Checkbox';
import { FETCH_API } from 'app/api/useFetchApi';
import { EMessageType, useFloatMessages } from 'app/hooks';
import { TextOut } from 'app/views/components/TextOut';
import { AutoInstallServiceProps } from 'app/views/modules/_common/shared/AutoInstallService/types';
import React, { useEffect, useState } from 'react';

const { getAutoInstall, postAutoInstall } = FETCH_API.MANAGING_SERVICE_SUBSCRIPTIONS;

export const AutoInstallService = ({ id }: AutoInstallServiceProps) => {
    const { show } = useFloatMessages();
    const [isAutoInstall, setIsAutoInstall] = useState(false);

    useEffect(() => {
        getAutoInstall(id)
            .then(response => {
                if (response.success && response.data) {
                    setIsAutoInstall(response.data.autoinstall);
                } else {
                    show(EMessageType.error, response.message);
                }
            });
    }, [id, show]);

    const changeAutoInstallStatus = () => {
        postAutoInstall(id, !isAutoInstall)
            .then(response => {
                if (response.success) {
                    setIsAutoInstall(prev => !prev);
                } else {
                    show(EMessageType.error, response.message);
                }
            });
    };

    return (
        <FormControlLabel
            control={
                <Checkbox
                    checked={ isAutoInstall }
                    onChange={ changeAutoInstallStatus }
                    size="small"
                />
            }
            label={
                <TextOut>Автоматически устанавливать сервис во все новые базы моего аккаунта</TextOut>
            }
        />
    );
};