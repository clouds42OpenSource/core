import { Link } from '@mui/material';
import CircularProgress from '@mui/material/CircularProgress';
import { EPartnersTourId, ERefreshId, ETourId } from 'app/views/Layout/ProjectTour/enums';
import { ReactNode } from 'react';

import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { TextOut } from 'app/views/components/TextOut';

import styles from './style.module.css';

type TInfoCard = FloatMessageProps & {
    image?: string;
    title: ReactNode;
    description: string;
    isLink?: boolean;
    isHidden?: boolean;
    isLoading?: boolean;
    tourId?: EPartnersTourId | ETourId;
    refreshId?: ERefreshId;
};

export const InfoCard = withFloatMessages(({ image, title, tourId, refreshId, description, isLink, isHidden = false, floatMessage: { show }, isLoading = false }: TInfoCard) => {
    const handleCopy = () => {
        if ('clipboard' in navigator) {
            navigator.clipboard.writeText(description).then(_ => {
                show(MessageType.Info, 'Скопировано в буфер обмена', 1500);
            });
        }
    };

    return (
        <div className={ styles.container } data-testid="info-card-container" data-tourid={ tourId } data-refreshid={ refreshId }>
            { image && typeof title === 'string' &&
                <img src={ image } height={ 50 } width={ 50 } alt={ title } />
            }
            <div className={ styles.text }>
                { typeof title === 'string'
                    ? <TextOut fontWeight={ 600 } fontSize={ 12 }>{ title }</TextOut>
                    : title
                }
                { isLoading &&
                    <CircularProgress color="primary" size={ 24 } style={ { paddingBlock: '5px' } } />
                }
                { !isHidden && !isLoading &&
                    <div>
                        { isLink
                            ? (
                                <div className={ styles.link }>
                                    <Link href={ description } data-testid="info-card-href">{ description }</Link>
                                    <i
                                        className={ `${ styles.icon } fa fa-clipboard` }
                                        aria-hidden="true"
                                        onClick={ handleCopy }
                                    />
                                </div>
                            )
                            : <span data-testid="info-card-text">{ description }</span>
                        }
                    </div>
                }
            </div>
        </div>
    );
});