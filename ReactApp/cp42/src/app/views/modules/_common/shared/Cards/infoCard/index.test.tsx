import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';

import { InfoCard } from 'app/views/modules/_common/shared/Cards/infoCard/index';
import { SnackbarProvider } from 'notistack';

describe('<InfoCard  />', () => {
    it('renders with link', () => {
        const { getByText } = render(
            <SnackbarProvider>
                <InfoCard image="" title="title" description="description" isLink={ true } />
            </SnackbarProvider>
        );
        expect(screen.getByTestId('info-card-container')).toBeInTheDocument();
        expect(screen.getByTestId('info-card-href')).toBeInTheDocument();
        expect(getByText(/title/i)).toBeInTheDocument();
    });

    it('renders with link', () => {
        const { getByText } = render(
            <SnackbarProvider>
                <InfoCard image="" title="title" description="description" />
            </SnackbarProvider>
        );
        expect(screen.getByTestId('info-card-container')).toBeInTheDocument();
        expect(screen.getByTestId('info-card-text')).toBeInTheDocument();
        expect(getByText(/description/i)).toBeInTheDocument();
    });
});