// import { useTour } from '@reactour/tour';
import { apiHost } from 'app/api';
import React, { useEffect, useState } from 'react';
import dayjs from 'dayjs';
import { Link } from '@mui/material';

import { DialogView } from 'app/views/components/controls/Dialog/views/DialogView/Index';
import { ContainedButton, SuccessButton } from 'app/views/components/controls/Button';
import { useAppDispatch, useAppSelector } from 'app/hooks';
import { TextOut } from 'app/views/components/TextOut';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { FETCH_API } from 'app/api/useFetchApi';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { REDUX_API } from 'app/api/useReduxApi';
import { getHref } from 'app/views/modules/ToPartners/helpers';

import styles from './style.module.css';

type TAgencyAgreementDialog = FloatMessageProps & {
    isOpen: boolean;
    setIsOpen: React.Dispatch<React.SetStateAction<boolean>>;
    needGetSummaryData?: boolean;
};

export const getAgencyAgreementUrl = (id: string) => apiHost(`agency-agreements/${ id }/print`);

const { getSummaryData, getAgencyAgreementStatus } = REDUX_API.PARTNERS_REDUX;

export const AgencyAgreementDialog = withFloatMessages(({ isOpen, setIsOpen, floatMessage: { show }, needGetSummaryData }: TAgencyAgreementDialog) => {
    const dispatch = useAppDispatch();

    const [isLoading, setIsLoading] = useState(false);

    const { data } = useAppSelector(state => state.PartnersState.summaryDataReducer);

    const cancelClickHandler = () => {
        setIsOpen(false);
    };

    useEffect(() => {
        if (needGetSummaryData) {
            void getSummaryData(dispatch);
        }
    }, [dispatch, needGetSummaryData]);

    const submitClickHandler = async () => {
        setIsLoading(true);
        const { error } = await FETCH_API.PARTNERS.acceptAgencyAgreement();
        setIsLoading(false);

        if (error && error !== 'Успешно!') {
            return show(MessageType.Error, error);
        }

        getAgencyAgreementStatus(dispatch);
        show(MessageType.Success, 'Агентское соглашение успешно принято');
    };

    const renderButtons = () => {
        return (
            <div className={ styles.buttons }>
                <ContainedButton onClick={ cancelClickHandler }>Позже</ContainedButton>
                <SuccessButton onClick={ submitClickHandler }>Согласиться</SuccessButton>
            </div>
        );
    };

    return (
        <>
            { isLoading && <LoadingBounce /> }
            <DialogView
                isTitleSmall={ true }
                title="Агентское соглашение"
                isOpen={ isOpen }
                dialogWidth="xs"
                onCancelClick={ cancelClickHandler }
                buttons={ renderButtons }
                className={ styles.dialog }
            >
                <div className={ `${ styles.dialogBody } ${ styles.extendedDialogBody }` }>
                    <img src={ getHref('warning') } alt="warning" height={ 100 } width={ 100 } />
                    { data &&
                        <TextOut className={ styles.text }>
                            Для продолжения использования партнерского раздела и всех его функций вам необходимо согласиться с условиями
                            <Link
                                target="_blank"
                                href={ getAgencyAgreementUrl(data.rawData.actualAgencyAgreement.id) }
                            >
                                { ` ${ data.rawData.actualAgencyAgreement.name } от ${ dayjs(data.rawData.actualAgencyAgreement.effectiveDate).format('DD.MM.YYYY') }` }
                            </Link>
                            { data.rawData.nextEffectiveAgencyAgreement &&
                                <>
                                    , которые действуют до { `${ dayjs(data.rawData.actualAgencyAgreement.effectiveDate).format('DD.MM.YYYY') } ` } и с условиями
                                    <Link
                                        target="_blank"
                                        href={ getAgencyAgreementUrl(data.rawData.nextEffectiveAgencyAgreement.id) }
                                    >
                                        { ` ${ data.rawData.nextEffectiveAgencyAgreement.name } от ${ dayjs(data.rawData.nextEffectiveAgencyAgreement.effectiveDate).format('DD.MM.YYYY') }` }
                                    </Link>
                                    , которые вступят в силу с
                                    { ` ${ dayjs(data.rawData.nextEffectiveAgencyAgreement.effectiveDate).format('DD.MM.YYYY') }` }
                                </>
                            }
                        </TextOut>
                    }
                </div>
            </DialogView>
        </>
    );
});