import React from 'react';
import { Box, LinearProgress, Link, Table, TableBody, TableRow, TableCell } from '@mui/material';
import { ConfigFileUploader } from 'app/views/modules/_common/components/ConfigFileUploader';
import { EMessageType, useFloatMessages } from 'app/hooks';
import { TextOut } from 'app/views/components/TextOut';
import styles from '../../styles.module.css';

type Props = {
    allowedFileTypes?: string[];
    onUpload?: (file: File) => void | Promise<void>;
    disabled?: boolean;
    uploadText?: string;
    status?: number;
    login: string | null;
    pass: string | null;
    link: string | null;
    uploadProgress?: number;
    uploadedFileName?: string;
}

const maxFileSize = 10737418240;

export const FileUploader = ({
    allowedFileTypes = ['cf', 'cfe', 'epf'],
    onUpload,
    disabled = false,
    uploadText = 'Загрузите файл',
    status,
    login,
    pass,
    link,
    uploadProgress = 0,
    uploadedFileName,
}: Props) => {
    const { show } = useFloatMessages();

    const getStatusDescription = (statusId?: number) => {
        switch (statusId) {
            case 0:
                return 'Не запущено';
            case 1:
                return 'Сканирование...';
            case 2:
                return 'Завершено';
            default:
                return 'Не запущено';
        }
    };

    const handleFileSelect = (file: File) => {
        const extension = file.name.split('.').pop()?.toLowerCase();
        if (!extension || !allowedFileTypes.includes(extension)) {
            show(EMessageType.error, `Разрешены только файлы типов: ${ allowedFileTypes.join(', ') }`);
            return;
        }

        if (file.size > maxFileSize) {
            show(EMessageType.error, 'Файл слишком большой, максимальный размер: 10GB');
            return;
        }

        const nameWithoutExtension = file.name.replace(/\.[^/.]+$/, '');
        if (/[^a-zA-Z0-9_-]/.test(nameWithoutExtension)) {
            show(EMessageType.error, 'Имя файла должно содержать только латиницу, цифры, символы "-" или "_"');
            return;
        }

        if (onUpload) {
            onUpload(file);
        }
    };

    return (
        <Box>
            <Box>
                <Box className={ styles.customFileUploader }>
                    <Box>
                        <Table className={ styles.customTable }>
                            <TableBody>
                                <TableRow>
                                    <TableCell className={ `${ styles.customCell } ${ styles.customCellWide }` }>
                                        <TextOut fontWeight={ 500 }>
                                            Загрузка файла:
                                        </TextOut>
                                    </TableCell>
                                    <TableCell className={ styles.customCell }>
                                        <ConfigFileUploader
                                            callBack={ handleFileSelect }
                                            fileTypesArr={ allowedFileTypes }
                                            required={ true }
                                            text={ uploadText }
                                            className={ styles.fileUploaderButton }
                                            limit={ 1 }
                                            disabled={ !!uploadProgress || disabled || status === 1 }
                                        />
                                        { status === 1 && (
                                            <TextOut>
                                                Загружено, дождитесь окончания проверки
                                            </TextOut>
                                        ) }
                                        { uploadProgress > 0 && <LinearProgress variant="determinate" value={ uploadProgress } /> }
                                    </TableCell>
                                </TableRow>
                            </TableBody>
                        </Table>
                        { /* <ConfigFileUploader
                            callBack={ handleFileSelect }
                            fileTypesArr={ allowedFileTypes }
                            required={ true }
                            text={ uploadText }
                            limit={ 1 }
                            disabled={ !!uploadProgress || disabled }
                        /> */ }
                    </Box>
                </Box>
                <Box mt={ 2 }>
                    <Table className={ styles.customTable }>
                        <TableBody>
                            <TableRow>
                                <TableCell className={ `${ styles.customCell } ${ styles.customCellWide }` }>
                                    <TextOut fontWeight={ 500 }>Статус сканирования:</TextOut>
                                </TableCell>
                                <TableCell className={ styles.customCell }>
                                    <TextOut>
                                        { getStatusDescription(status) }
                                    </TextOut>
                                </TableCell>
                            </TableRow>
                        </TableBody>
                    </Table>
                </Box>

                { uploadedFileName && (
                    <Box mt={ 2 }>
                        <TextOut>Загруженный файл: { uploadedFileName }</TextOut>
                    </Box>
                ) }

                { link && (
                    <Box mt={ 2 }>
                        <TextOut fontWeight={ 500 }>Доступ к сканеру:</TextOut>
                        <Table className={ styles.customTable }>
                            <TableBody>
                                <TableRow>
                                    <TableCell className={ `${ styles.customCell } ${ styles.customCellWide }` }>
                                        <TextOut fontWeight={ 500 }>
                                            Ссылка
                                        </TextOut>
                                    </TableCell>
                                    <TableCell className={ styles.customCell }>
                                        <Link href={ link } target="_blank" rel="noreferrer">
                                            Ссылка на отчет
                                        </Link>
                                    </TableCell>
                                </TableRow>
                                { login && (
                                    <TableRow>
                                        <TableCell className={ `${ styles.customCell } ${ styles.customCellWide }` }>
                                            <TextOut fontWeight={ 500 }>
                                                Логин
                                            </TextOut>
                                        </TableCell>
                                        <TableCell className={ styles.customCell }>
                                            <TextOut>
                                                { login }
                                            </TextOut>
                                        </TableCell>
                                    </TableRow>
                                ) }
                                { pass && (
                                    <TableRow>
                                        <TableCell className={ `${ styles.customCell } ${ styles.customCellWide }` }>
                                            <TextOut fontWeight={ 500 }>
                                                Пароль
                                            </TextOut>
                                        </TableCell>
                                        <TableCell className={ styles.customCell }>
                                            <TextOut>
                                                { pass }
                                            </TextOut>
                                        </TableCell>
                                    </TableRow>
                                ) }
                            </TableBody>
                        </Table>
                    </Box>

                ) }
            </Box>
        </Box>
    );
};