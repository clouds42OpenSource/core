import { Box } from '@mui/material';
import { TextOut } from 'app/views/components/TextOut';
import PriorityHighIcon from '@mui/icons-material/PriorityHigh';

export const FileRequirements = () => {
    return (
        <Box border="1px dotted black" p="4px" margin="8px 0" maxWidth="400px">
            <TextOut inDiv={ true } style={ { display: 'flex' } } fontWeight={ 500 }>
                <PriorityHighIcon fontSize="small" color="error" /> Требования к загружаемому файлу:
            </TextOut>
            <ul style={ { fontSize: '13px' } }>
                <li>Формат: .cf, .cfe, .epf</li>
                <li>Размер файла не должен превышать 10 ГБ</li>
                <li>Название файла должно быть на латинице, без скобок и спецсимволов (пример: filename.cf)</li>
            </ul>
        </Box>
    );
};