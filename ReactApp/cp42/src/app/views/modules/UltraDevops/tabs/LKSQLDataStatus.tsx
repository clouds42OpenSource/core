import { Chip } from '@mui/material';
import { postLKSQLResponseDto } from 'app/api/endpoints/lksql/response/postLKSQLResponseDto';

export const LKSQLDataStatus = ({ data }: { data: postLKSQLResponseDto }) => {
    const {
        lkcfstatus,
        lkgitstatus,
        lkrepostatus,
        lksquserstatus,
        lksqprojectstatus,
        lksqscanstatus,
    } = data;

    const statusFields = [
        lkcfstatus,
        lkgitstatus,
        lkrepostatus,
        lksquserstatus,
        lksqprojectstatus,
        lksqscanstatus
    ];

    if (statusFields.some(status => status === 'failed')) {
        return <Chip label="Ошибка" color="error" />;
    }

    if (statusFields.every(status => status === 'done')) {
        return <Chip label="Успешно" color="success" />;
    }

    return <Chip label="В процессе" color="info" />;
};