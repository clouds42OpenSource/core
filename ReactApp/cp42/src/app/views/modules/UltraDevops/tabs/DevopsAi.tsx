import React from 'react';
import { TextOut } from 'app/views/components/TextOut';
import { COLORS } from 'app/utils';

export const DevopsAi = () => {
    return (
        <TextOut inDiv={ true }>
            <TextOut>
                Получите доступ к альфа-версии бота-ассистента <b>ES Ассистент</b> для консультативной поддержки в разработке 1С. <br /><br />
                Бот работает через Telegram и имеет ограниченные лимиты использования, предназначенные для тестирования. Авторизация осуществляется посредством идентификации Telegram-логина. <br /><br />
                Доступ предоставляется для клиентов, использующих контур (сервисы) разработки <b>EFSOL-42Clouds</b>. <br /><br />
                Получить тестовый доступ можно по{ ' ' }
                <a style={ { color: COLORS.link } } href="https://t.me/es_ai_assistant_bot" target="_blank" rel="noopener noreferrer">
                    ссылке
                </a>.
            </TextOut>
        </TextOut>
    );
};