/* eslint-disable react/no-unescaped-entities */
import { Box } from '@mui/material';
import { ProfileItemResponseDto } from 'app/api/endpoints/profile/response';
import { FETCH_API } from 'app/api/useFetchApi';
import { EMessageType, useFloatMessages } from 'app/hooks';
import { SuccessButton } from 'app/views/components/controls/Button';
import { Dialog } from 'app/views/components/controls/Dialog';
import { TextOut } from 'app/views/components/TextOut';
import { MyCompanyItemDataModel } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/receiveMyCompany';
import { memo, useState } from 'react';

type Props = {
    myCompany: MyCompanyItemDataModel;
    userData: ProfileItemResponseDto;
};

const PremiumModalView = ({ myCompany, userData }: Props) => {
    const [isModalOpened, setIsModalOpened] = useState(false);
    const { show } = useFloatMessages();

    const sendEmail = async () => {
        const response = await FETCH_API.EMAILS.sendEmail({
            body: `Дата подачи заявки: ${ new Date().toLocaleString() }\n\nID компании: ${ myCompany.indexNumber }\nЛогин пользователя: ${ userData.login }`,
            subject: `Заявка на подключение "Премиум-режима" от компании ${ myCompany.indexNumber }`,
            header: `Заявка на подключение "Премиум-режима" от компании ${ myCompany.indexNumber }`,
            categories: 'premium-mode',
            reference: 'devops@efsol.ru',
        });

        if (response.success) {
            show(EMessageType.success, 'Заявка успешно отправлена');
            setIsModalOpened(false);
        } else {
            show(EMessageType.error, 'Ошибка при отправке заявки');
        }
    };

    return (
        <>
            <Box display="flex" justifyContent="center" alignItems="center">
                <SuccessButton fullWidth={ true } style={ { marginTop: '16px', maxWidth: '200px' } } onClick={ () => setIsModalOpened(true) }>
                    Подключить
                </SuccessButton>
            </Box>
            <Dialog
                isOpen={ isModalOpened }
                dialogWidth="sm"
                dialogVerticalAlign="top"
                onCancelClick={ () => setIsModalOpened(false) }
                title='Заявка на подключение "Премиум-режима"'
                titleFontSize={ 20 }
                titleTextAlign="center"
                isTitleSmall={ true }
            >
                <Box textAlign="center">
                    <TextOut inDiv={ true }>
                        Подтвердите отправку заявки на подключение статического анализа кода 1С "Премиум-режим".

                    </TextOut>
                    <SuccessButton style={ { marginTop: '12px' } } onClick={ sendEmail }>
                        Отправить заявку
                    </SuccessButton>
                </Box>
            </Dialog>
        </>
    );
};

export const PremiumRequestModal = memo(PremiumModalView);