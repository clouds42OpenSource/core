import { Box, LinearProgress } from '@mui/material';
import { ProfileItemResponseDto } from 'app/api/endpoints/profile/response';
import { TextOut } from 'app/views/components/TextOut';
import dayjs from 'dayjs';
import { useCallback, useEffect, useState } from 'react';
import { EMessageType, useFloatMessages } from 'app/hooks';
import { FETCH_API } from 'app/api/useFetchApi';
import { postLKSQLResponseDto } from 'app/api/endpoints/lksql/response/postLKSQLResponseDto';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { COLORS } from 'app/utils';
import { bucketName, findRelevantRecords, minioClient } from 'app/views/modules/UltraDevops/tabs/utils/minioHelpers';
import { LKSQLDataStatus } from 'app/views/modules/UltraDevops/tabs/LKSQLDataStatus';
import { ConfigFileUploader } from 'app/views/modules/_common/components/ConfigFileUploader';
import { Upload } from '@aws-sdk/lib-storage';
import 'dayjs/locale/ru';
import { MyCompanyItemDataModel } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/receiveMyCompany';
import { FileRequirements } from './components/FileRequirements';
import styles from '../styles.module.css';
import { PremiumRequestModal } from './components/PremiumRequestModal';
import { SupportDescription } from './components/SupportDescription';
import { ReferenceComponent } from '../../Ultra/tabs/UltraPerformance/tabs/components/ReferenceComponent';
import { SuggestImprovementsModal } from '../../Ultra/components/SuggestImprovementModal';

dayjs.locale('ru');

type Props = {
    userData: ProfileItemResponseDto;
    LKSQLDataList: postLKSQLResponseDto[];
    myCompany: MyCompanyItemDataModel;
};

const maxFileSize = 10737418240;

const { getLKSQLData, postLKSQLData } = FETCH_API.LKSQL;

export const BaseStaticAnalysis = ({ userData, LKSQLDataList, myCompany }: Props) => {
    const [isLoading, setIsLoading] = useState(false);
    const [uploadProgress, setUploadProgress] = useState(0);
    const [LKSQLData, setLKSQLData] = useState<postLKSQLResponseDto | null>(null);
    const [isUploadAllowed, setIsUploadAllowed] = useState(true);

    const { show } = useFloatMessages();

    const handleGetLKSQLData = useCallback(async (id: string) => {
        const response = await getLKSQLData(id);

        if (response.data) {
            setLKSQLData(response.data[0]);
        }
    }, [getLKSQLData]);

    useEffect(() => {
        const relevantRecord = findRelevantRecords(LKSQLDataList);
        setLKSQLData(relevantRecord);
    }, [LKSQLDataList]);

    useEffect(() => {
        if (LKSQLData) {
            setIsUploadAllowed(
                !!LKSQLData.lksqurl && !!LKSQLData.lksquser && !!LKSQLData.lksqpassword
            );
        }
    }, [LKSQLData]);

    useEffect(() => {
        if (LKSQLData) {
            if (!LKSQLData.lksqurl || !LKSQLData.lksquser || !LKSQLData.lksqpassword) {
                const intervalId = setInterval(() => {
                    void handleGetLKSQLData(LKSQLData.lkid);
                }, 15000);

                return () => clearInterval(intervalId);
            }
        }
    }, [LKSQLData, handleGetLKSQLData]);

    const handleUploadFile = async (fileName: string) => {
        const dataObject = {
            lkuserlogin: userData.login,
            lkuseremail: userData.email,
            lks3status: 'done',
            lkdate: dayjs().format('YYYY-MM-DDTHH:mm:ss'),
            lks3url: fileName
        };
        setIsLoading(true);
        const response = await postLKSQLData(dataObject);
        setIsLoading(false);

        if (response.success && response.data) {
            setLKSQLData(response.data[0]);
        } else {
            show(EMessageType.error, response.message);
        }
    };

    const uploadFileToCloudRU = async (file: File) => {
        try {
            setIsLoading(true);
            setUploadProgress(0);
            const upload = new Upload({
                client: minioClient,
                params: {
                    Bucket: bucketName,
                    Key: file.name,
                    Body: file,
                },
                queueSize: 4,
                partSize: 5 * 1024 * 1024,
                leavePartsOnError: false,
            });

            upload.on('httpUploadProgress', progress => {
                setIsLoading(false);
                if (progress.loaded && progress.total) {
                    const percentage = Math.round((progress.loaded / progress.total) * 100);
                    setUploadProgress(percentage);
                } else {
                    setUploadProgress(0);
                }
            });

            await upload.done();
            setIsUploadAllowed(false);
            setUploadProgress(0);
            show(EMessageType.success, 'Файл успешно загружен на s3');
            void handleUploadFile(file.name);
        } catch (err) {
            setIsUploadAllowed(true);
            setUploadProgress(0);
            show(EMessageType.error, 'Ошибка при загрузке файла');
        } finally {
            setIsLoading(false);
        }
    };

    const handleFileUpload = (file: File) => {
        if (!isUploadAllowed) {
            show(EMessageType.error, 'Сканирование последнего файла еще не завершено');
            return;
        }

        if (file.size <= maxFileSize) {
            const nameWithoutExtension = file.name.replace(/\.[^/.]+$/, '');

            if (/[^a-zA-Z0-9]/.test(nameWithoutExtension)) {
                show(EMessageType.error, 'Имя файла должно содержать только латинские буквы и цифры');
            } else {
                void uploadFileToCloudRU(file);
            }
        } else {
            show(EMessageType.error, 'Файл слишком большой, максимальный размер: 10GB');
        }
    };

    return (
        <>
            { isLoading && <LoadingBounce /> }
            <Box>
                <ReferenceComponent
                    extraButtons={
                        <SuggestImprovementsModal
                            functionalityName="Статический анализ кода 1С"
                            companyId={ myCompany.indexNumber }
                        />
                    }
                >
                    <Box>
                        <TextOut>
                            Сервис статического анализа кода 1С используется для проверки кода 1С на качество и позволяет:
                        </TextOut>
                        <TextOut inDiv={ true }>
                            <ul>
                                <li>
                                    проверять код 1С на ошибки, дефекты и уязвимости;
                                </li>
                                <li>
                                    получить отчет качества кода 1С.
                                </li>
                            </ul>
                        </TextOut>
                        <FileRequirements />
                    </Box>
                </ReferenceComponent>
                <Box className={ styles.tableBlock } justifyItems="center" pb={ 2 } mb={ 2 }>
                    <TextOut fontWeight={ 500 }>
                        Режим работы функционала
                    </TextOut>
                    <TextOut fontWeight={ 500 }>
                        Статус
                    </TextOut>
                    <TextOut fontWeight={ 500 }>
                        Запуск анализа кода
                    </TextOut>
                    <TextOut fontWeight={ 500 }>
                        Результат
                    </TextOut>
                </Box>
                <Box>
                    <Box className={ styles.tableBlock }>
                        <TextOut inDiv={ true }>
                            <TextOut fontWeight={ 500 } inDiv={ true } textAlign="center">
                                Базовый режим
                            </TextOut>
                            <ul>
                                <li>Проверка кода 1С на ошибки, дефекты и уязвимости</li>
                                <li>Поддержка входного формата: .cf, .cfe, .epf</li>
                                <li>Однократные проверки сканером без исторических данных</li>
                                <li>Хранение отчета 7 дней</li>
                            </ul>
                        </TextOut>
                        <Box className={ styles.chip }>
                            <TextOut inDiv={ true } textAlign="center">
                                Активирован
                            </TextOut>
                        </Box>
                        <Box display="grid" alignItems="center" justifyItems="center">
                            <ConfigFileUploader
                                callBack={ handleFileUpload }
                                fileTypesArr={ ['cf', 'cfe', 'epf'] }
                                required={ true }
                                text="Загрузите файл"
                                limit={ 1 }
                                className={ styles.fileUploader }
                                disabled={ !!uploadProgress }
                            />
                            { uploadProgress ? <LinearProgress variant="determinate" value={ uploadProgress } /> : '' }
                        </Box>
                        { LKSQLData && (
                            <Box>
                                <Box display="grid" gap="4px" gridTemplateColumns="1fr 1fr" maxWidth="400px">
                                    <TextOut>
                                        Дата загрузки:
                                    </TextOut>
                                    <TextOut>
                                        { LKSQLData.lkdate ? dayjs(LKSQLData.lkdate).format('DD.MM.YYYY HH:mm') : '-' }
                                    </TextOut>
                                    <TextOut>
                                        Cтатус загрузки файла
                                    </TextOut>
                                    <TextOut>
                                        { LKSQLData.lks3status ? 'Загружено' : 'Нет данных' }
                                    </TextOut>
                                    <TextOut style={ { whiteSpace: 'nowrap' } }>
                                        Статус сканирования кода
                                    </TextOut>
                                    {
                                        LKSQLData.lksqurl && LKSQLData.lksquser && LKSQLData.lksqpassword ? (
                                            <Box display="flex" flexDirection="column">
                                                <TextOut>
                                                    <a href={ LKSQLData.lksqurl } style={ { color: COLORS.link } } target="_blank" rel="noreferrer">Ссылка на результат</a>
                                                </TextOut>
                                                <TextOut>
                                                    Логин: { LKSQLData.lksquser }
                                                </TextOut>
                                                <TextOut>
                                                    Пароль: { LKSQLData.lksqpassword }
                                                </TextOut>
                                            </Box>
                                        ) : (
                                            <TextOut>
                                                <LKSQLDataStatus data={ LKSQLData } />
                                            </TextOut>
                                        )
                                    }
                                </Box>
                            </Box>
                        )
                        }
                    </Box>
                    <Box className={ styles.tableBlock }>
                        <TextOut inDiv={ true }>
                            <TextOut fontWeight={ 500 } inDiv={ true } textAlign="center">
                                Премиум режим
                            </TextOut>
                            <ul>
                                <li>Проверка кода 1С на ошибки, дефекты и уязвимости</li>
                                <li>Разделение кода по проектам</li>
                                <li>История проверок и сравнение результатов</li>
                                <li>Поддержка входного формата: .cf, .cfe, .epf</li>
                                <li>Хранение отчета бессрочно</li>
                            </ul>
                        </TextOut>
                        <PremiumRequestModal myCompany={ myCompany } userData={ userData } />
                    </Box>
                </Box>
            </Box>
            <SupportDescription />
        </>
    );
};