import { Box } from '@mui/material';
import { TextOut } from 'app/views/components/TextOut';
import styles from '../styles.module.css';
import { ReviewModal, ReviewModalProps } from '../../Ultra/components/ReviewModal';

const baseComposition = [
    'dev-сервер 1С',
    'dev-сервер СУБД',
    'подключение по RDP/VPN',
    'индивидуальные облачные рабочие места разработчиков с изолированными ресурсами (опционально)',
    'веб-публикация (опционально)',
];

const advancedComposition = [
    ...baseComposition,
    'Gitlab-сервер (персональный)',
    'SonarQube-сервер (персональный)',
    'Gitlab Runner (персональный)',
];

const defaultModalData = {
    placeholder: 'Описание',
    buttonText: 'Заказать',
    description: 'Опишите вашу задачу подробнее. Укажите особенности вашей разработки 1С, конфигурацию, версию платформы и другое.',
};

type DevelopmentOptionProps = {
    title: string;
    description: string;
    composition: string[];
    modalData: ReviewModalProps;
    imgSrc: string;
};

const DevelopmentOption = ({ title, description, composition, modalData, imgSrc }: DevelopmentOptionProps) => (
    <TextOut className={ styles.descriptionWrapper } inDiv={ true }>
        <Box>
            <img src={ imgSrc } alt="Логотип сервиса" style={ { maxWidth: '30vw' } } loading="lazy" />
            <b>{ title }</b>
            <span>{ description }</span>
            <span>Состав:</span>
            <ul>
                { composition.map(item => (
                    <li key={ item }>{ item }</li>
                )) }
            </ul>
        </Box>
        <Box display="flex" justifyContent="center">
            <ReviewModal { ...modalData } />
        </Box>
    </TextOut>
);

export const DevBoundary = () => {
    return (
        <Box display="grid" gridTemplateColumns="1fr 1fr">
            <DevelopmentOption
                title="Контур разработки. Базовый вариант:"
                description={ `Этот вариант инфраструктуры разработки представляет собой классическую схему, когда команда разработчиков 1С работает через конфигуратор или хранилище конфигураций 1С.
                По-умолчанию, рабочие места разработчиков разделяются внутри терминального сервера, но возможна инсталляция индивидуальных рабочих мест для более полноценной замены локальных машин.` }
                composition={ baseComposition }
                imgSrc={ `${ window.location.origin }/img/ultra/devOutlineBase.png` }
                modalData={ {
                    ...defaultModalData,
                    title: 'Заказать (Базовый вариант)',
                    subject: 'Базовый вариант заказа',
                    header: 'Заказ базового варианта',
                } }
            />
            <DevelopmentOption
                title="Контур разработки. Расширенный вариант:"
                description={ `Этот вариант инфраструктуры разработки представляет собой более продвинутую схему, когда команда разработчиков 1С работает хранилище конфигураций 1С + git.
                Опционально возможна работа EDT+git. Версионирование ведется в gitlab, также настраивается ветвление разработки, каждый разработчик работает в своей ветке и сливается с общей только после всех проверок и код-ревью. 
                В упрощенном pipeline для разработки 1С предусмотрена проверка SonarScanner для статического анализа кода 1С с отчетом и историей.
                По-умолчанию, рабочие места разработчиков разделяются внутри терминального сервера, но возможна инсталляция индивидуальных рабочих мест для более полноценной замены локальных машин.` }
                composition={ advancedComposition }
                imgSrc={ `${ window.location.origin }/img/ultra/devOutlineFull.png` }
                modalData={ {
                    ...defaultModalData,
                    title: 'Заказать (Расширенный вариант)',
                    subject: 'Расширенный вариант заказа',
                    header: 'Заказ расширенного варианта',
                } }
            />
        </Box>
    );
};