/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useRef, useState } from 'react';
import { Box, Table, TableBody, TableCell, TableRow } from '@mui/material';
import { EMessageType, useFloatMessages } from 'app/hooks';
import { Upload } from '@aws-sdk/lib-storage';
import { minioClient, bucketName } from 'app/views/modules/UltraDevops/tabs/utils/minioHelpers';
import { ProfileItemResponseDto } from 'app/api/endpoints/profile/response';
import { FETCH_API } from 'app/api/useFetchApi';
import { MyCompanyItemDataModel } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/receiveMyCompany';
import { TextOut } from 'app/views/components/TextOut';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { getProjectsResponseDto, getUserCredsResponseDto } from 'app/api/endpoints/staticAnalysis/response';
import { templateErrorText } from 'app/api';
import { CreateProjectModal } from './components/CreateProjectModal';
import { ProjectSelection } from './components/ProjectSelection';
import { FileUploader } from './components/FileUploader';
import styles from '../styles.module.css';
import { FileRequirements } from './components/FileRequirements';
import { SupportDescription } from './components/SupportDescription';
import { ReferenceComponent } from '../../Ultra/tabs/UltraPerformance/tabs/components/ReferenceComponent';
import { SuggestImprovementsModal } from '../../Ultra/components/SuggestImprovementModal';

type Props = {
    userData: ProfileItemResponseDto;
    myCompany: MyCompanyItemDataModel;
};

const maxFileSize = 10737418240;

const { getUserCredsByCompanyId, getResultByProjectName, getProjectsByCompanyId, createOldUserProject, createNewUserNewProject } = FETCH_API.STATICANALYSIS;

export const PremiumStaticAnalysis = ({ userData, myCompany }: Props) => {
    const { show } = useFloatMessages();

    const companyid = myCompany.indexNumber as unknown as string;
    const usernameportal = userData.login;
    const description = userData.email;
    const [projectName, setProjectName] = useState('');
    const [projects, setProjects] = useState<getProjectsResponseDto[]>([]);
    const [isLoading, setIsLoading] = useState(false);
    const [uploadProgress, setUploadProgress] = useState(0);
    const [scanStatus, setScanStatus] = useState(0);
    const [oldProjectId, setOldProjectId] = useState('');
    const [login, setLogin] = useState<string | null>(null);
    const [pass, setPass] = useState<string | null>(null);
    const [link, setLink] = useState<string | null>(null);
    const [userCreds, setUserCreds] = useState<getUserCredsResponseDto | null>(null);
    const [isCreating, setIsCreating] = useState(false);
    const [oldFileName, setOldFileName] = useState('');
    const intervalRef = useRef<NodeJS.Timeout | null>(null);

    useEffect(() => {
        (async () => {
            const response = await getUserCredsByCompanyId(companyid);
            if (response.success && response.data && response.data.length > 0) {
                setUserCreds(response.data[0]);
            }
        })();
    }, [companyid]);

    const clearInfo = () => {
        setLogin('');
        setPass('');
        setOldFileName('');
        setLink('');
        setScanStatus(0);
    };

    const handleSelectProject = async (name: string, id: string, oldId: string) => {
        if (id) {
            setIsCreating(false);
            setIsLoading(true);
            const response = await getResultByProjectName(id);
            setIsLoading(false);
            if (response.success && response.data && response.data.length) {
                setLogin(response.data[0].usernameportal);
                setPass(response.data[0].pass);
                setLink(response.data[0].scanurl);
                setOldFileName(response.data[0].url);
                setOldProjectId(oldId);
                setProjectName(name);
            }
        } else {
            clearInfo();
            setProjectName('');
        }
    };

    const getProjects = (async (id?: string) => {
        const response = await getProjectsByCompanyId(companyid);
        if (response.success && response.data) {
            const projectNames = response.data
                .filter(({ addinfo }) => !!addinfo);
            setProjects(projectNames);
            if (id) {
                const createdProject = projectNames.find(project => project.id === id);
                if (createdProject) {
                    handleSelectProject(createdProject.addinfo, createdProject.id, createdProject.project);
                }
            }
        }
    });

    const startChecking = (id: string) => {
        if (intervalRef.current) {
            clearInterval(intervalRef.current);
            intervalRef.current = null;
        }

        const interval = setInterval(async () => {
            const response = await getResultByProjectName(id);
            const { data } = response;

            if (data && data[0].status === 'done') {
                setLogin(data[0].usernameportal);
                setPass(data[0].pass);

                if (data[0].scanurl) {
                    setLink(data[0].scanurl);
                }
                setOldFileName(data[0].url);
                setOldProjectId(data[0].project);
                setScanStatus(2);
                getProjects();
                clearInterval(interval);
            }

            if (!response.success) {
                show(EMessageType.error, response.message ?? templateErrorText);
                clearInterval(interval);
            }
        }, 5000);

        intervalRef.current = interval;
    };

    useEffect(() => {
        if (intervalRef.current) {
            clearInterval(intervalRef.current);
            intervalRef.current = null;
            clearInfo();
        }
    }, [projectName]);

    useEffect(() => {
        if (userCreds) {
            getProjects();
        }
    }, [companyid, userCreds]);

    const handleSetNewProjectName = (newProjectName: string) => {
        setIsCreating(true);
        setProjectName(newProjectName);
        clearInfo();
    };

    const handleUploadFileInfo = async (newProjectName: string, newFileName: string, isNewProject?: boolean) => {
        setIsLoading(true);

        const payload = {
            companyid,
            url: newFileName,
            description,
            usernameportal,
            ischanged: '1',
            addinfo: newProjectName,
        };

        let response;

        if (isNewProject && userCreds) {
            response = await createOldUserProject({
                ...payload,
                addinfo: newProjectName,
                login: userCreds.login,
                pass: userCreds.pass,
                status: 'OUNP',
            });
        } else if (userCreds) {
            response = await createOldUserProject({
                ...payload,
                login: userCreds.login,
                pass: userCreds.pass,
                status: 'OUOP',
                project: oldProjectId
            });
        } else {
            response = await createNewUserNewProject({
                ...payload,
                status: 'NUNP',
            });
        }

        if (isNewProject) {
            setProjectName(newProjectName ?? '');
        }

        setIsLoading(false);
        if (response.success && response.data) {
            getProjects(response.data[0].id);
            startChecking(response.data[0].id);
            setIsCreating(false);
        }
    };

    const handleFileUpload = async (file: File) => {
        if (file.size > maxFileSize) {
            show(EMessageType.error, 'Файл слишком большой, максимальный размер: 10GB');
            return;
        }

        const nameWithoutExtension = file.name.replace(/\.[^/.]+$/, '');
        if (/[^a-zA-Z0-9_-]/.test(nameWithoutExtension)) {
            show(EMessageType.error, 'Имя файла должно быть на латинице, без спецсимволов (кроме "-" и "_")');
            return;
        }

        if (oldFileName && file.name !== oldFileName) {
            show(EMessageType.error, `В выбранном проекте проверяется код: “${ oldFileName }”, загрузите корректный файл для проверки.`);
            return;
        }
        try {
            setUploadProgress(0);

            const upload = new Upload({
                client: minioClient,
                params: {
                    Bucket: bucketName,
                    Key: file.name,
                    Body: file,
                },
                queueSize: 4,
                partSize: 5 * 1024 * 1024,
                leavePartsOnError: false,
            });

            upload.on('httpUploadProgress', progress => {
                if (progress.loaded && progress.total) {
                    const percentage = Math.round((progress.loaded / progress.total) * 100);
                    setUploadProgress(percentage);
                } else {
                    setUploadProgress(0);
                }
            });

            await upload.done();

            handleUploadFileInfo(projectName, file.name, isCreating);

            show(EMessageType.success, 'Файл успешно загружен на s3');

            setScanStatus(1);
        } catch (err) {
            show(EMessageType.error, 'Ошибка при загрузке файла');
        } finally {
            setUploadProgress(0);
        }
    };

    return (
        <Box>
            <ReferenceComponent
                extraButtons={
                    <SuggestImprovementsModal
                        functionalityName="Статический анализ кода 1С"
                        companyId={ myCompany.indexNumber }
                    />
                }
            >
                <Box>
                    <TextOut>
                        Сервис статического анализа кода 1С используется для проверки кода 1С на качество и позволяет:
                    </TextOut>
                    <TextOut inDiv={ true }>
                        <ul>
                            <li>
                                проверять код 1С на ошибки, дефекты и уязвимости;
                            </li>
                            <li>
                                получить отчет качества кода 1С.
                            </li>
                        </ul>
                    </TextOut>
                    <FileRequirements />
                </Box>
            </ReferenceComponent>
            <Box className={ styles.staticAnalysis }>
                <Box>
                    <TextOut inDiv={ true }>
                        <TextOut fontWeight={ 500 } inDiv={ true }>
                            Премиум режим
                        </TextOut>
                        <ul>
                            <li>Проверка кода 1С на ошибки, дефекты и уязвимости</li>
                            <li>Разделение кода по проектам</li>
                            <li>История проверок и сравнение результатов</li>
                            <li>Поддержка входного формата: .cf, .cfe, .epf</li>
                            <li>Хранение отчета бессрочно</li>
                        </ul>
                    </TextOut>
                    <SupportDescription />
                </Box>
                <Box>
                    { isLoading && <LoadingBounce /> }
                    <TextOut fontWeight={ 500 }>Управление проектом</TextOut>
                    <Table className={ styles.customTable }>
                        <TableBody>
                            <TableRow>
                                <TableCell className={ styles.customCell } sx={ { display: 'flex', gap: '6px' } }>
                                    <CreateProjectModal projects={ projects } onSubmit={ handleSetNewProjectName } />
                                    { userCreds && <ProjectSelection projectName={ projectName } isCreating={ isCreating } projects={ projects } onSelect={ handleSelectProject } /> }
                                </TableCell>
                                { oldFileName && (
                                    <TableCell>
                                        <TextOut inDiv={ true }>
                                            Имя файла: <b>{ oldFileName }</b>
                                        </TextOut>
                                    </TableCell>) }
                            </TableRow>
                        </TableBody>
                    </Table>
                    { projectName && (
                        <Box mt={ 4 }>
                            <FileUploader
                                onUpload={ handleFileUpload }
                                status={ scanStatus }
                                login={ login }
                                pass={ pass }
                                link={ link }
                                uploadProgress={ uploadProgress }
                            />
                        </Box>
                    ) }
                </Box>
            </Box>
        </Box>
    );
};