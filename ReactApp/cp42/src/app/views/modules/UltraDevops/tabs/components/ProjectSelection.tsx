import { useEffect, useState } from 'react';
import { Select, MenuItem, SelectChangeEvent } from '@mui/material';
import { getProjectsResponseDto } from 'app/api/endpoints/staticAnalysis/response';

type Props = {
    isCreating: boolean;
    projects: getProjectsResponseDto[];
    onSelect: (projectName: string, id: string, oldId: string) => void;
    projectName: string;
}

export const ProjectSelection = ({ isCreating, projects, onSelect, projectName }: Props) => {
    const [selectedProject, setSelectedProject] = useState('');

    const handleSelect = (event: SelectChangeEvent<string>) => {
        const { value } = event.target;
        const [name, projectId, oldId] = value.split('|');

        setSelectedProject(value);
        onSelect(name, projectId, oldId);
    };

    useEffect(() => {
        if (isCreating) {
            setSelectedProject('');
        } else {
            const createdProject = projects.find(({ addinfo }) => addinfo === projectName);
            if (createdProject) {
                const { addinfo, id } = createdProject;
                const combinedValue = `${ addinfo }|${ id }|${ createdProject.project }`;
                setSelectedProject(combinedValue);
            }
        }
    }, [isCreating, projectName, projects]);

    return (
        <Select
            value={ selectedProject }
            placeholder="Выберите проект"
            displayEmpty={ true }
            onChange={ handleSelect }
        >
            <MenuItem value="">
                Выберите проект
            </MenuItem>
            { projects.map(project => {
                const { addinfo, id } = project;
                const combinedValue = `${ addinfo }|${ id }|${ project.project }`;
                return (
                    <MenuItem key={ id } value={ combinedValue }>
                        { addinfo }
                    </MenuItem>
                );
            }) }
        </Select>
    );
};