import { S3Client } from '@aws-sdk/client-s3';
import { postLKSQLResponseDto } from 'app/api/endpoints/lksql/response/postLKSQLResponseDto';
import dayjs from 'dayjs';

export const bucketName = import.meta.env.REACT_MINIO_BUCKET ?? '';
export const minioClient = new S3Client({
    endpoint: 'https://minio.esit.info',
    credentials: {
        accessKeyId: import.meta.env.REACT_MINIO_ACCESS_KEY,
        secretAccessKey: import.meta.env.REACT_MINIO_SECRET_KEY
    },
    forcePathStyle: true,
    region: 'ru-central1'
});

export const findRelevantRecords = (data: postLKSQLResponseDto[]): postLKSQLResponseDto | null => {
    if (data.length === 0) return null;

    const latestUserRecord = data.reduce((latest, current) => {
        return dayjs(latest.lkdate).isBefore(dayjs(current.lkdate)) ? current : latest;
    }, data[0]);

    const oneWeekAgo = dayjs().subtract(7, 'days');

    if (dayjs(latestUserRecord.lkdate).isBefore(oneWeekAgo)) {
        return null;
    }

    return latestUserRecord;
};