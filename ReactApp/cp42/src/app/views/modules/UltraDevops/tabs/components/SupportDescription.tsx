import { Link } from '@mui/material';
import { TextOut } from 'app/views/components/TextOut';
import styles from '../../styles.module.css';

export const SupportDescription = () => {
    return (
        <TextOut inDiv={ true } className={ styles.supportDescription }>
            <span style={ { fontWeight: 'bold' } }> Возникли вопросы или требуется индивидуальное решение для автоматизации разработки 1С? <br /> </span>
            Пишите в <Link target="_blank" href="https://t.me/efsoldevopsbot" rel="noreferrer">telegram бот</Link> команды EFSOL DevOps.
        </TextOut>
    );
};