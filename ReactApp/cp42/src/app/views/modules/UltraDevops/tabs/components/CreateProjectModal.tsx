import React, { useState } from 'react';
import { TextField, Box } from '@mui/material';
import { Dialog } from 'app/views/components/controls/Dialog';
import { SuccessButton } from 'app/views/components/controls/Button';
import { TextOut } from 'app/views/components/TextOut';
import { getProjectsResponseDto } from 'app/api/endpoints/staticAnalysis/response';

type Props = {
    onSubmit: (projectName: string) => void;
    projects: getProjectsResponseDto[];
}

export const CreateProjectModal = ({ onSubmit, projects }: Props) => {
    const [projectName, setProjectName] = useState('');
    const [error, setError] = useState('');
    const [isModalOpened, setIsModalOpened] = useState(false);

    const handleOpenModal = () => {
        setIsModalOpened(true);
    };

    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setProjectName(e.target.value);
        setError('');
    };

    const handleSubmit = async () => {
        const latinRegex = /^[a-zA-Z0-9_-]+$/;
        if (!latinRegex.test(projectName)) {
            setError('Используйте латинские символы для написания имени проекта');
            return;
        }

        const isProjectExists = projects.some(({ addinfo }) => {
            return addinfo?.toLowerCase() === projectName.toLowerCase();
        });

        if (isProjectExists) {
            setError('Проект с таким именем уже существует');
            return;
        }
        onSubmit(projectName);
        setIsModalOpened(false);
    };

    return (
        <>
            <SuccessButton onClick={ handleOpenModal }>
                Создать новый проект
            </SuccessButton>
            <Dialog
                isOpen={ isModalOpened }
                dialogWidth="xs"
                onCancelClick={ () => setIsModalOpened(false) }
                title="Введите имя проекта"
                titleFontSize={ 20 }
                titleTextAlign="left"
                maxHeight="300px"
                isTitleSmall={ true }
            >
                <Box display="flex" flexDirection="column" justifyContent="space-between" height="100%">
                    <Box textAlign="center">
                        <TextField
                            fullWidth={ true }
                            placeholder="Введите имя проекта"
                            value={ projectName }
                            onChange={ handleChange }
                            error={ !!error }
                            helperText={ error }
                        />
                        <TextOut>
                            * Укажите имя на латинице
                        </TextOut>
                    </Box>
                    <Box textAlign="center">
                        <SuccessButton onClick={ handleSubmit }>Создать проект</SuccessButton>
                    </Box>
                </Box>
            </Dialog>
        </>
    );
};