import { templateErrorText } from 'app/api';
import { postLKSQLResponseDto } from 'app/api/endpoints/lksql/response/postLKSQLResponseDto';
import { FETCH_API } from 'app/api/useFetchApi';
import { EMessageType, useAppSelector, useFloatMessages } from 'app/hooks';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import { TabsControl } from 'app/views/components/controls/TabsControl';
import { ReceiveMyCompanyThunk } from 'app/modules/myCompany/store/thunks';
import { ReceiveMyCompanyThunkParams } from 'app/modules/myCompany/store/reducers/receiveMyCompanyData/params';
import { connect } from 'react-redux';
import { AppReduxStoreState } from 'app/redux/types';
import { DevopsAi } from './tabs/DevopsAi';
import { DevBoundary } from './tabs/DevBoundary';
import { PremiumStaticAnalysis } from './tabs/PremiumStaticAnalysis';
import { BaseStaticAnalysis } from './tabs/BaseStaticAnalysis';

type StateProps = {
    isCompanyLoading: boolean;
};

type DispatchProps = {
    dispatchReceiveMyCompanyThunk: (args: ReceiveMyCompanyThunkParams) => void;
};

const UltraDevopsForOneCView = ({ isCompanyLoading, dispatchReceiveMyCompanyThunk }: StateProps & DispatchProps) => {
    const { data, error, isLoading: isProfileLoading } = FETCH_API.PROFILE.useGetProfile();
    const [isLoading, setIsLoading] = useState(false);
    const [LKSQLDataList, setLKSQLDataList] = useState<postLKSQLResponseDto[]>([]);
    const [isStatycAnalysisConnected, setIsStatycAnalysisConnected] = useState(false);
    const myCompany = useAppSelector(state => state.MyCompanyState.myCompany);
    const { show } = useFloatMessages();

    useEffect(() => {
        dispatchReceiveMyCompanyThunk({ force: true });
    }, [dispatchReceiveMyCompanyThunk]);

    useEffect(() => {
        if (data) {
            (async () => {
                setIsLoading(true);
                const response = await FETCH_API.LKSQL.getLKSQLDataList(data?.rawData.login);
                setIsLoading(false);

                if (response.success && response.data) {
                    setLKSQLDataList(response.data);
                } else {
                    show(EMessageType.error, response.message ?? templateErrorText);
                }
            })();
        }
    }, [show, data]);

    useEffect(() => {
        (async () => {
            if (myCompany.indexNumber) {
                const response = await FETCH_API.STATICANALYSIS.getIsStaticAnalysisConnected(myCompany.indexNumber);

                if (response.success && response.data && response.data.length) {
                    setIsStatycAnalysisConnected(!!response.data[0].isconnected);
                }
            }
        })();
    }, [myCompany]);

    useEffect(() => {
        if (error) {
            show(EMessageType.error, error);
        }
    }, [error, show]);

    if (isProfileLoading || !isCompanyLoading) {
        return <LoadingBounce />;
    }

    return (
        <div>
            <Helmet>
                <title>
                    Инструменты для разработчиков 1С
                </title>
            </Helmet>
            { isLoading && <LoadingBounce /> }
            <TabsControl
                headers={ [
                    {
                        title: 'Статический анализ кода 1С',
                        isVisible: !!data
                    },
                    {
                        title: 'Контур разработки 1С',
                        isVisible: true,
                    },
                    {
                        title: 'ИИ-помощник',
                        isVisible: true,
                    },
                ] }
            >
                { data && (isStatycAnalysisConnected ? <PremiumStaticAnalysis myCompany={ myCompany } userData={ data.rawData } /> : <BaseStaticAnalysis myCompany={ myCompany } LKSQLDataList={ LKSQLDataList } userData={ data.rawData } />) }
                <DevBoundary />
                <DevopsAi />
            </TabsControl>
        </div>
    );
};

const UltraDevopsForOneCConnected = connect<StateProps, DispatchProps, object, AppReduxStoreState>(
    state => {
        const { settings, hasSuccessFor } = state.Global.getCurrentSessionSettingsReducer;
        const { hasSettingsReceived } = hasSuccessFor;
        const currentUserGroups = hasSettingsReceived ? settings.currentContextInfo.currentUserGroups : [];

        return {
            currentUserGroups,
            isCompanyLoading: state.MyCompanyState.hasSuccessFor.hasMyCompanyReceived,
        };
    },
    {
        dispatchReceiveMyCompanyThunk: ReceiveMyCompanyThunk.invoke
    }
)(UltraDevopsForOneCView);

export const UltraDevopsForOneC = UltraDevopsForOneCConnected;