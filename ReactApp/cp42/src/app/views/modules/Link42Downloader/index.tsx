import { Box } from '@mui/material';
import GetAppIcon from '@mui/icons-material/GetApp';

import { EBitDepth, ESystemType } from 'app/api/endpoints/link42/enum';
import { FETCH_API } from 'app/api/useFetchApi';
import { EMessageType, useFloatMessages } from 'app/hooks';
import { useQuery } from 'app/hooks/useQuery';
import { SuccessButton } from 'app/views/components/controls/Button';
import { TextOut } from 'app/views/components/TextOut';
import cn from 'classnames';
import { useEffect, useMemo } from 'react';
import { Helmet } from 'react-helmet';
import UAParser from 'ua-parser-js';

import styles from './style.module.css';

const { useGetInstallLimits } = FETCH_API.LINK42;
const { os, cpu } = new UAParser(window.navigator.userAgent).getResult();

export const Link42Downloader = () => {
    const { data, error } = useGetInstallLimits();
    const { show } = useFloatMessages();
    const query = useQuery();

    const windowsVersions = useMemo(() => data?.rawData.bitDepths.filter(bitDepth => bitDepth.systemType === ESystemType.windows) ?? [], [data?.rawData]);

    const macOsVersions = useMemo(() => data?.rawData.bitDepths.filter(bitDepth => bitDepth.systemType === ESystemType.macOS) ?? [], [data?.rawData]);

    const isArchive = query.has('archive');

    useEffect(() => {
        if (error) {
            show(EMessageType.error, error);
        }
    }, [show, error]);

    const downloadLinkHandler = (link?: string) => () => {
        if (link) {
            window.open(link, '_blank ');
        } else {
            const isNotX64 = cpu.architecture && !cpu.architecture.includes('64');

            switch (os.name) {
                case 'Windows':
                    if (os.version !== '10' && os.version !== '11') {
                        window.open(`${ import.meta.env.REACT_APP_CP_HOST }/downloads/link42_old/Link42Setup_${ isNotX64 ? 'x86' : 'x64' }.exe`);
                    } else {
                        window.open(windowsVersions.find(version => isNotX64 ? version.bitDepth === EBitDepth.x86 : version.bitDepth === EBitDepth.x64)?.downloadLink, '_blank');
                    }

                    break;
                case 'Mac OS':
                    window.open(macOsVersions.find(version => isNotX64 ? version.bitDepth === EBitDepth.arm : version.bitDepth === EBitDepth.x64)?.downloadLink, '_blank');

                    break;
                default:
                    show(EMessageType.warning, 'Ваша операционная система не подходит для приложения');
            }
        }
    };

    return (
        <Box className={ styles.main } sx={ { backgroundImage: `url(${ import.meta.env.REACT_APP_CP_HOST }/img/signin/background.png)` } }>
            <Helmet>
                <title>Линк42</title>
                <link rel="shortcut icon" href="/img/link42.ico" />
            </Helmet>
            <Box paddingInline="10px" display="flex" flexDirection="column" maxWidth="400px" gap="15vh" marginInline="auto" marginTop="10vh">
                <Box textAlign="center" alignItems="center" width="100%" marginTop="20px" display="flex" flexDirection="column" gap="10px">
                    <img
                        height="50px"
                        src={ `${ import.meta.env.REACT_APP_API_HOST }/img/registration/logo.png` }
                        alt="42clouds"
                        style={ isArchive ? {} : { marginBottom: '50px' } }
                    />
                    { !isArchive && (
                        <>
                            { (os.name === 'Windows' || os.name === 'Mac OS')
                                ? (
                                    <>
                                        <TextOut fontSize={ 20 } fontWeight={ 700 }>
                                            Подходящая для Вас версия
                                        </TextOut>
                                        <SuccessButton fullWidth={ true } kind="success" className={ styles.button } onClick={ downloadLinkHandler() }>
                                            <img height={ 20 } width={ 20 } src="/img/link42.png" alt="link42" /> Скачать Линк42 для { os.name }
                                        </SuccessButton>
                                    </>
                                )
                                : (
                                    <TextOut textAlign="center" fontSize={ 20 } fontWeight={ 700 }>
                                        Ваша операционная система не подходит для установки Линк42
                                    </TextOut>
                                )
                            }
                            { os.name === 'Windows' && os.version !== '10' && os.version !== '11' && (
                                <TextOut textAlign="center" inDiv={ true } fontSize={ 11 }>Ваша операционная система не поддерживается, рекомендуем обновить её до Windows 10 и выше или скачать устаревшую версию приложения.</TextOut>
                            ) }
                        </>
                    ) }
                </Box>
                <Box width="100%" display="flex" justifyContent="space-between" textAlign="center" gap="10px">
                    <Box width="100%" display="flex" flexDirection="column" gap="10px">
                        <i className={ cn('fa fa-windows', styles.icon) } aria-hidden="true" />
                        { windowsVersions.map(bitDepth => (
                            <SuccessButton fullWidth={ true } kind="success" key={ bitDepth.downloadLink } onClick={ downloadLinkHandler(bitDepth.downloadLink) }>
                                <GetAppIcon /> Windows { bitDepth.bitDepth === EBitDepth.x86 ? 'х86' : 'x64' }
                            </SuccessButton>
                        )) }
                    </Box>
                    <Box width="100%" display="flex" flexDirection="column" gap="10px">
                        <i className={ cn('fa fa-apple', styles.icon) } aria-hidden="true" />
                        { macOsVersions.map(bitDepth => (
                            <SuccessButton fullWidth={ true } kind="success" key={ bitDepth.downloadLink } onClick={ downloadLinkHandler(bitDepth.downloadLink) }>
                                <GetAppIcon /> MacOS { bitDepth.bitDepth === EBitDepth.x64 ? 'Intel' : 'ARM' }
                            </SuccessButton>
                        )) }
                    </Box>
                    { isArchive && (
                        <Box width="100%" display="flex" flexDirection="column" gap="10px">
                            <Box display="flex" gap="10px" justifyContent="center" alignItems="flex-end">
                                <i className={ cn('fa fa-windows', styles.icon) } aria-hidden="true" />
                                <TextOut fontSize={ 16 } fontWeight={ 600 }>Архивная</TextOut>
                            </Box>
                            <SuccessButton
                                fullWidth={ true }
                                kind="success"
                                onClick={ downloadLinkHandler(`${ import.meta.env.REACT_APP_CP_HOST }/downloads/link42_old/Link42Setup_x86.exe`) }
                            >
                                <GetAppIcon /> Windows x86
                            </SuccessButton>
                            <SuccessButton
                                fullWidth={ true }
                                kind="success"
                                onClick={ downloadLinkHandler(`${ import.meta.env.REACT_APP_CP_HOST }/downloads/link42_old/Link42Setup_x64.exe`) }
                            >
                                <GetAppIcon /> Windows x64
                            </SuccessButton>
                        </Box>
                    ) }
                </Box>
            </Box>
        </Box>
    );
};