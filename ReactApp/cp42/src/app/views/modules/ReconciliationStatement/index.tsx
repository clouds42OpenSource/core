import { FormControl, FormHelperText, MenuItem, Select, TextField } from '@mui/material';
import { DatePicker, LocalizationProvider } from '@mui/x-date-pickers';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { ELogEvent } from 'app/api/endpoints/global/enum';
import { FETCH_API } from 'app/api/useFetchApi';
import { EMessageType, useAppSelector, useDebounce, useFloatMessages } from 'app/hooks';
import { withHeader } from 'app/views/components/_hoc/withHeader';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { BoxWithPicture } from 'app/views/modules/_common/reusable-modules/BoxWithPicture';
import { reconciliationStatementInitialValues, reconciliationStatementSchema } from 'app/views/modules/_common/reusable-modules/BoxWithPicture/schemas';
import { allowedCountries } from 'app/views/modules/ToPartners/config/constants';
import ruLocale from 'date-fns/locale/ru';
import dayjs from 'dayjs';
import { useFormik } from 'formik';
import { MuiTelInput } from 'mui-tel-input';
import { useEffect, useState } from 'react';

const sendList = ['Электронная почта', 'ЭДО'];

const { getLegalEntityDetails } = FETCH_API.PARTNERS;
const { sendEmailBySupportPage } = FETCH_API.EMAILS;
const { sendLogEvent } = FETCH_API.GLOBAL;

const ReconciliationStatement = () => {
    const { show } = useFloatMessages();

    const { contextAccountIndex } = useAppSelector(state => state.Global.getCurrentSessionSettingsReducer.settings.currentContextInfo);
    const { data } = useAppSelector(state => state.MainPageState.mainPageReducer);
    const { email, login } = data ?? {};

    const [isLoading, setIsLoading] = useState(false);

    const { values, handleChange, handleBlur, touched, errors, setFieldValue, handleSubmit, resetForm, setFieldTouched } = useFormik({
        validationSchema: reconciliationStatementSchema,
        initialValues: reconciliationStatementInitialValues,
        onSubmit: async value => {
            setIsLoading(true);

            const { success, message } = await sendEmailBySupportPage(
                `
                    <p>Логин: ${ login }</p>
                    <p>ИНН: ${ value.inn }</p>
                    <p>КПП: ${ value.kpp }</p>
                    <p>Дата начала: ${ dayjs(value.from).format('DD.MM.YYYY') }</p>
                    <p>Дата окончания: ${ dayjs(value.to).format('DD.MM.YYYY') }</p>
                    <p>Как отправить акт сверки взаиморасчетов: ${ value.send }</p>
                    <p>Почта: ${ value.email }</p>
                    <p>Телефон: ${ value.phone }</p>
                `,
                `Запрос на получение акта сверки взаиморасчетов  от ${ contextAccountIndex } ${ dayjs().format('DD.MM.YYYY HH:mm:ss') }`,
                email
            );

            if (success) {
                show(EMessageType.success, 'Спасибо за Вашу заявку! Мы обработаем ее в ближайшее время.');

                resetForm();
                await setFieldValue('from', '');
                await setFieldValue('to', '');

                await sendLogEvent(ELogEvent.requestSent, 'Отправлена заявка на получение Акта сверки');
            } else {
                show(EMessageType.error, message);
            }

            setIsLoading(false);
        }
    });

    const debounceInn = useDebounce(values.inn, 500);

    useEffect(() => {
        (async () => {
            if (!errors.inn && debounceInn) {
                const { data: legalEntityDetails, error } = await getLegalEntityDetails({ inn: debounceInn });

                if (!error && legalEntityDetails && legalEntityDetails.rawData && legalEntityDetails.rawData.success) {
                    await setFieldValue('kpp', legalEntityDetails.rawData.kpp);
                }
            }
        })();
    }, [debounceInn, errors.inn, setFieldValue]);

    return (
        <LocalizationProvider dateAdapter={ AdapterDateFns } adapterLocale={ ruLocale }>
            <BoxWithPicture handleSubmit={ handleSubmit } isLoading={ isLoading }>
                <FormAndLabel label="ИНН" fullWidth={ true }>
                    <TextField
                        name="inn"
                        value={ values.inn }
                        onChange={ handleChange }
                        onBlur={ handleBlur }
                        error={ touched.inn && !!errors.inn }
                        helperText={ touched.inn && errors.inn }
                        fullWidth={ true }
                    />
                </FormAndLabel>
                <FormAndLabel label="КПП" fullWidth={ true }>
                    <TextField
                        name="kpp"
                        value={ values.kpp }
                        onChange={ handleChange }
                        onBlur={ handleBlur }
                        error={ touched.kpp && !!errors.kpp }
                        helperText={ touched.kpp && errors.kpp }
                        fullWidth={ true }
                    />
                </FormAndLabel>
                <FormAndLabel label="Дата начала" fullWidth={ true }>
                    <DatePicker
                        value={ values.from }
                        onChange={ value => setFieldValue('from', value) }
                        slotProps={ {
                            textField: {
                                name: 'from',
                                error: !!(touched.from && !!errors.from),
                                helperText: touched.from && errors.from,
                                fullWidth: true,
                                onBlur: handleBlur
                            }
                        } }
                        maxDate={ new Date() }
                    />
                </FormAndLabel>
                <FormAndLabel label="Дата окончания" fullWidth={ true }>
                    <DatePicker
                        value={ values.to }
                        onChange={ value => setFieldValue('to', value) }
                        slotProps={ {
                            textField: {
                                name: 'to',
                                error: !!(touched.to && !!errors.to),
                                helperText: touched.to && errors.to,
                                fullWidth: true,
                                onBlur: handleBlur
                            }
                        } }
                        maxDate={ new Date() }
                    />
                </FormAndLabel>
                <FormAndLabel label="Как отправить акт сверки взаиморасчетов" fullWidth={ true }>
                    <FormControl fullWidth={ true }>
                        <Select
                            name="send"
                            value={ values.send }
                            onChange={ handleChange }
                            onBlur={ handleBlur }
                            error={ touched.send && !!errors.send }
                            variant="outlined"
                            fullWidth={ true }
                        >
                            { sendList.map(item => <MenuItem key={ item } value={ item }>{ item }</MenuItem>) }
                        </Select>
                        <FormHelperText error={ touched.send && !!errors.send }>{ touched.send && errors.send }</FormHelperText>
                    </FormControl>
                </FormAndLabel>
                <FormAndLabel label="Укажите адрес электронной почты" fullWidth={ true }>
                    <TextField
                        name="email"
                        value={ values.email }
                        onChange={ handleChange }
                        onBlur={ handleBlur }
                        error={ touched.email && !!errors.email }
                        helperText={ touched.email && errors.email }
                        fullWidth={ true }
                    />
                </FormAndLabel>
                <FormAndLabel label="Номер телефона" fullWidth={ true }>
                    <MuiTelInput
                        fullWidth={ true }
                        name="phone"
                        autoComplete="off"
                        onChange={ e => setFieldValue('phone', e) }
                        onBlur={ () => setFieldTouched('phone', true, true) }
                        value={ values.phone }
                        preferredCountries={ allowedCountries }
                        defaultCountry="RU"
                        langOfCountryName="RU"
                        error={ touched.phone && !!errors.phone }
                        helperText={ touched.phone && errors.phone }
                    />
                </FormAndLabel>
            </BoxWithPicture>
        </LocalizationProvider>
    );
};

export const ReconciliationStatementView = withHeader({
    title: 'Акт сверки',
    page: ReconciliationStatement
});