/**
 * Тип параметра для свойства компонента роутера
 */
export type SupplierIdRouteParams = {
    /**
     * ID поставщика
     */
    id?: string
};