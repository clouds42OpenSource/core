/**
 * Модель Redux формы для создания, редактирования поставщика
 */
export type CreateEditSupplierDataForm = {
    /**
     * Название поставщика
     */
    name: string;

    /**
     * Код поставщика
     */
    code: string;

    /**
     * ID локали
     */
    localeId: string;

    /**
     * Стандартный поставщик на локаль
     */
    isDefault: boolean;

    /**
     * ID печатной формы счета на оплату
     */
    printedHtmlFormInvoiceId: string;

    /**
     * ID печатной формы фискального чека
     */
    printedHtmlFormInvoiceReceiptId: string;

    /**
     * Файл договора оферты
     */
    cloudFile?: File;

    /**
     * Названия файла для инициализации
     */
    contentName?: string;
};