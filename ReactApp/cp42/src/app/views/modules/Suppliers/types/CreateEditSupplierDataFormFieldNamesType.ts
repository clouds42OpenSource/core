import { CreateEditSupplierDataForm } from 'app/views/modules/Suppliers/types';

export type CreateEditSupplierDataFormFieldNamesType = keyof CreateEditSupplierDataForm;