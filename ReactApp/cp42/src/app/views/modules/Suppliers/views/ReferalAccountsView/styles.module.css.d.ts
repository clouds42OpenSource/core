declare const styles: {
  readonly 'button-add': string;
  readonly 'button-delete': string;
  readonly 'select-list': string;
  readonly 'select-list-popper': string;
  readonly 'menu-box': string;
};
export = styles;