import { AppRoutes } from 'app/AppRoutes';
import { SuppliersProcessId } from 'app/modules/suppliers';
import { ReferralAccountThunkParams, SupplierThunkParams } from 'app/modules/suppliers/store/reducers/common/params';
import { CreateSupplierReferralAccountThunk } from 'app/modules/suppliers/store/thunks/CreateSupplierReferralAccountThunk';
import { DeleteSupplierReferralAccountThunk } from 'app/modules/suppliers/store/thunks/DeleteSupplierReferralAccountThunk';
import { GetReferralAccountsBySupplierIdThunk } from 'app/modules/suppliers/store/thunks/GetReferralAccountsBySupplierIdThunk';
import { AppReduxStoreState } from 'app/redux/types';
import { ContainedButton, DefaultButton } from 'app/views/components/controls/Button';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { withHeader } from 'app/views/components/_hoc/withHeader';
import { GuidRegex } from 'app/views/modules/Suppliers/common/regex';
import { SupplierIdRouteParams } from 'app/views/modules/Suppliers/types';
import { SearchReferalAccountView } from 'app/views/modules/Suppliers/views/ReferalAccountsView/partial-elements/SearchReferalAccountView';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { WatchReducerProcessActionState } from 'app/views/modules/_common/components/WatchReducerProcessActionState';
import { SupplierReferralAccountsDataModel } from 'app/web/InterlayerApiProxy/SuppliersApiProxy/getReferralAccountsBySupplierId';
import { IReducerProcessActionInfo } from 'core/redux/interfaces';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps, withRouter } from 'react-router-dom';
import { Box } from '@mui/material';
import ErrorOutlineIcon from '@mui/icons-material/ErrorOutline';
import { Dialog } from 'app/views/components/controls/Dialog';
import css from './styles.module.css';

type OwnState = {
    /**
     * ID поставщика
     */
    currentSupplierId: string,

    /**
     * ID реферального аккаунта для удаления
     */
    deleteReferralAccountId?: string,

    /**
     * ID реферального аккаунта для добавления
     */
    addReferralAccountId?: string
    /**
     * Состояние модального окна на удаление реферального аккаунта
     */
    deleteIsOpen: boolean;
};

type StateProps = {
    /**
     * Модель словаря для реферальных аккаунтов поставщика
     * Key - ID реферального аккаунта
     * Value - название реферального аккаунта
     */
    supplierReferralAccounts: SupplierReferralAccountsDataModel
};

type DispatchProps = {
    dispatchGetReferralAccountsBySupplierIdThunk: (args: SupplierThunkParams) => void;
    dispatchDeleteSupplierReferralAccountThunk: (args: ReferralAccountThunkParams) => void;
    dispatchCreateSupplierReferralAccountThunk: (args: ReferralAccountThunkParams) => void;
};

type AllProps = StateProps & DispatchProps & RouteComponentProps<SupplierIdRouteParams> & FloatMessageProps;

class ReferalAccountsClass extends Component<AllProps, OwnState> {
    public constructor(props: AllProps) {
        super(props);

        this.state = this.getInitState(props);

        this.onClickDeleteSupplierReferralAccount = this.onClickDeleteSupplierReferralAccount.bind(this);
        this.onAddSupplierReferralAccount = this.onAddSupplierReferralAccount.bind(this);
        this.successOrFaildStateNotification = this.successOrFaildStateNotification.bind(this);
        this.onProcessActionStateChanged = this.onProcessActionStateChanged.bind(this);
        this.getFormatDeleteButton = this.getFormatDeleteButton.bind(this);
        this.onChangeCombobox = this.onChangeCombobox.bind(this);
        this.openModalForDeleteReferalAccount = this.openModalForDeleteReferalAccount.bind(this);
        this.onProcessCreateSupplierReferralAccount = this.onProcessCreateSupplierReferralAccount.bind(this);
        this.onProcessDeleteSupplierReferralAccount = this.onProcessDeleteSupplierReferralAccount.bind(this);
        this.closeModalForDeleteReferalAccount = this.closeModalForDeleteReferalAccount.bind(this);
    }

    public componentDidMount() {
        this.props.dispatchGetReferralAccountsBySupplierIdThunk({
            supplierId: this.state.currentSupplierId,
            force: true
        });
    }

    private onAddSupplierReferralAccount() {
        const { addReferralAccountId, currentSupplierId } = this.state;

        if (addReferralAccountId && currentSupplierId) {
            this.props.dispatchCreateSupplierReferralAccountThunk({
                accountId: addReferralAccountId,
                supplierId: currentSupplierId,
                force: true
            });
        } else this.props.floatMessage.show(MessageType.Error, 'Выберите реферальный аккаунт.', 3000);
    }

    private onClickDeleteSupplierReferralAccount() {
        const { currentSupplierId, deleteReferralAccountId } = this.state;

        if (currentSupplierId && deleteReferralAccountId) {
            this.props.dispatchDeleteSupplierReferralAccountThunk({
                accountId: deleteReferralAccountId,
                supplierId: currentSupplierId,
                force: true
            });
            this.setState({
                deleteIsOpen: false
            });
        }
    }

    private onChangeCombobox(referalAccountId: string) {
        this.setState({
            addReferralAccountId: referalAccountId
        });
    }

    private async onProcessActionStateChanged(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        this.onProcessDeleteSupplierReferralAccount(processId, actionState, error);

        this.onProcessCreateSupplierReferralAccount(processId, actionState, error);
    }

    private onProcessDeleteSupplierReferralAccount(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId === SuppliersProcessId.DeleteSupplierReferralAccount) {
            if (actionState.isInSuccessState) {
                this.successOrFaildStateNotification('Реферальный аккаунт успешно удален', true);
            }
            if (actionState.isInErrorState) {
                this.successOrFaildStateNotification(`Ошибка при удалении реферального аккаунта: ${ error?.message }`, false);
            }
        }
    }

    private onProcessCreateSupplierReferralAccount(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId === SuppliersProcessId.CreateSupplierReferralAccount) {
            if (actionState.isInSuccessState) {
                this.successOrFaildStateNotification('Реферальный аккаунт успешно добавлен', true);
            }
            if (actionState.isInErrorState) {
                this.successOrFaildStateNotification(`Ошибка добавлении реферального аккаунта: ${ error?.message }`, false);
            }
        }
    }

    private getFormatDeleteButton(referalAccountId: string) {
        return (
            <ContainedButton
                className={ css['button-delete'] }
                kind="error"
                onClick={ () => this.openModalForDeleteReferalAccount(referalAccountId) }
            >
                Удалить
            </ContainedButton>
        );
    }

    private getInitState(props: AllProps) {
        const stateObj = {
            currentSupplierId: '',
            deleteIsOpen: false
        } as OwnState;

        const currentSupplierId = props.match.params.id;

        if (currentSupplierId && GuidRegex.test(currentSupplierId)) {
            stateObj.currentSupplierId = currentSupplierId;
        } else if (currentSupplierId && !GuidRegex.test(currentSupplierId)) {
            this.props.history.push(AppRoutes.homeRoute);
        }

        return stateObj;
    }

    private openModalForDeleteReferalAccount(referalAccountId: string) {
        this.setState({
            deleteReferralAccountId: referalAccountId,
            deleteIsOpen: true
        });
    }

    private closeModalForDeleteReferalAccount() {
        this.setState({
            deleteIsOpen: false
        });
    }

    private successOrFaildStateNotification(message: string, isSuccess: boolean) {
        if (isSuccess) {
            this.props.floatMessage.show(MessageType.Success, message, 3000);
            this.props.dispatchGetReferralAccountsBySupplierIdThunk({
                supplierId: this.state.currentSupplierId,
                force: true
            });
        } else {
            this.props.floatMessage.show(MessageType.Error, message, 5000);
        }
    }

    public render() {
        return (
            <Box display="flex" flexDirection="column" gap="10px">
                <Box display="flex" flexDirection="row" gap="10px" className={ css['menu-box'] }>
                    <SearchReferalAccountView
                        onValueChange={ this.onChangeCombobox }
                        className={ css['select-list'] }
                        popperClassName={ css['select-list-popper'] }
                    />
                    <ContainedButton
                        className={ css['button-add'] }
                        kind="success"
                        onClick={ this.onAddSupplierReferralAccount }
                    >
                        Добавить
                    </ContainedButton>
                </Box>
                <CommonTableWithFilter
                    uniqueContextProviderStateId="SupplierReferalAccountsTableContextProviderStateId"
                    tableProps={ {
                        dataset: this.props.supplierReferralAccounts,
                        keyFieldName: 'key',
                        fieldsView: {
                            value: {
                                caption: 'Аккаунт',
                                fieldContentNoWrap: false,
                                fieldWidth: 'auto'
                            },
                            key: {
                                caption: 'Дополнительно',
                                fieldContentNoWrap: false,
                                fieldWidth: '20px',
                                format: this.getFormatDeleteButton
                            }
                        }
                    } }
                />
                <div className="text-left">
                    <Link
                        to={ AppRoutes.administration.suppliersRoute }
                        style={ { textDecoration: 'none', color: 'inherit' } }
                    >
                        <DefaultButton>
                            Отмена
                        </DefaultButton>
                    </Link>
                </div>
                <WatchReducerProcessActionState
                    watch={ [{
                        reducerStateName: 'SuppliersState',
                        processIds: [
                            SuppliersProcessId.DeleteSupplierReferralAccount,
                            SuppliersProcessId.CreateSupplierReferralAccount
                        ]
                    }] }
                    onProcessActionStateChanged={ this.onProcessActionStateChanged }
                />
                <Dialog
                    isOpen={ this.state.deleteIsOpen }
                    dialogWidth="xs"
                    dialogVerticalAlign="center"
                    onCancelClick={ this.closeModalForDeleteReferalAccount }
                    buttons={
                        [
                            {
                                content: 'Нет',
                                kind: 'primary',
                                variant: 'text',
                                onClick: this.closeModalForDeleteReferalAccount
                            },
                            {
                                content: 'Да',
                                kind: 'error',
                                variant: 'contained',
                                onClick: this.onClickDeleteSupplierReferralAccount
                            }
                        ]
                    }
                >
                    <Box display="flex" flexDirection="column" alignItems="center" justifyContent="center">
                        <ErrorOutlineIcon color="warning" sx={ { width: '5em', height: '5em' } } />
                        Вы действительно хотите удалить реферального аккаунта?
                    </Box>
                </Dialog>
            </Box>
        );
    }
}

export const ReferalAccountsConnected = connect<StateProps, DispatchProps, {}, AppReduxStoreState>(
    state => {
        const suppliersState = state.SuppliersState;
        const { supplierReferralAccounts } = suppliersState;

        return {
            supplierReferralAccounts
        };
    },
    {
        dispatchGetReferralAccountsBySupplierIdThunk: GetReferralAccountsBySupplierIdThunk.invoke,
        dispatchDeleteSupplierReferralAccountThunk: DeleteSupplierReferralAccountThunk.invoke,
        dispatchCreateSupplierReferralAccountThunk: CreateSupplierReferralAccountThunk.invoke
    }
)(ReferalAccountsClass);

export const ReferalAccountsView = withHeader({
    title: 'Реферальные аккаунты',
    page: withRouter(withFloatMessages(ReferalAccountsConnected))
});