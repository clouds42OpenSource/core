import { AppRoutes } from 'app/AppRoutes';
import { CheckBoxForm, ComboBoxForm, TextBoxForm } from 'app/views/components/controls/forms';
import { CreateEditSupplierDataForm, CreateEditSupplierDataFormFieldNamesType, SupplierFieldLengthEnumType, SupplierIdRouteParams } from 'app/views/modules/Suppliers/types';
import { DefaultButton, SuccessButton } from 'app/views/components/controls/Button';
import React, { Component } from 'react';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { SupplierOperationThunkParams, SupplierThunkParams } from 'app/modules/suppliers/store/reducers/common/params';
import { AppReduxStoreState } from 'app/redux/types';
import { ComboboxItemModel } from 'app/views/components/controls/forms/ComboBox/models';
import { CreateSupplierThunk } from 'app/modules/suppliers/store/thunks/CreateSupplierThunk';
import { EditSupplierThunk } from 'app/modules/suppliers/store/thunks/EditSupplierThunk';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { GetInitCreateEditPageItemsThunk } from 'app/modules/suppliers/store/thunks/GetInitCreateEditPageItemsThunk';
import { GetSupplierByIdThunk } from 'app/modules/suppliers/store/thunks/GetSupplierByIdThunk';
import { GuidRegex } from 'app/views/modules/Suppliers/common/regex';
import { IErrorsType } from 'app/common/interfaces';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';
import { IReducerProcessActionInfo } from 'core/redux/interfaces';
import { InputFileUploaderForm } from 'app/views/components/controls/forms/InputFileUploaderForm';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { PageEnumType } from 'app/common/enums';
import { PageHeaderView } from 'app/views/components/_hoc/withHeader/PageHeaderView';
import { Link, RouteComponentProps } from 'react-router-dom';
import { ShowErrorMessage } from 'app/views/modules/_common/components/ShowErrorMessage';
import { SupplierByIdDataModel } from 'app/web/InterlayerApiProxy/SuppliersApiProxy/getSupplierById';
import { SuppliersProcessId } from 'app/modules/suppliers';
import { TextWithTooltip } from 'app/views/components/controls/forms/Tooltips';
import { TooltipPlacementTypes } from 'app/views/components/controls/forms/Tooltips/types';
import { WatchReducerProcessActionState } from 'app/views/modules/_common/components/WatchReducerProcessActionState';
import { checkValidOfStringField } from 'app/common/functions';
import { connect } from 'react-redux';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { Box } from '@mui/material';

type OwnProps = ReduxFormProps<CreateEditSupplierDataForm>;

type OwnState = {
    /**
     * Обьект Ошибок для валидации
     */
    errors: IErrorsType;

    /**
     * Тип страницы для динамического компонента редактирования/создания
     */
    pageType: PageEnumType;

    /**
     * ID поставщика
     */
    currentSupplierId?: string;
};

type StateProps = {
    /**
     * Все локали
     */
    locales: Array<ComboboxItemModel<string>>;

    /**
     * Печатная форма
     */
    printedHtmlForms: Array<ComboboxItemModel<string>>;

    /**
     * Модель поставщика
     */
    supplier: SupplierByIdDataModel
};

type DispatchProps = {
    dispatchGetInitCreateEditPageItemsThunk: (args: IForceThunkParam) => void;
    dispatchCreateSupplierThunk: (args: SupplierOperationThunkParams) => void;
    dispatchEditSupplierThunk: (args: SupplierOperationThunkParams) => void;
    dispatchGetSupplierByIdThunk: (args: SupplierThunkParams) => void;
};

type AllProps = StateProps & OwnProps & DispatchProps & FloatMessageProps & RouteComponentProps<SupplierIdRouteParams>;

class CreateEditSupplierClass extends Component<AllProps, OwnState> {
    public constructor(props: AllProps) {
        super(props);

        this.state = this.getInitState(props);

        this.onValueChange = this.onValueChange.bind(this);
        this.onFileSelectError = this.onFileSelectError.bind(this);
        this.onFileSelectSuccess = this.onFileSelectSuccess.bind(this);
        this.submitButton = this.submitButton.bind(this);
        this.isValidSupplierForm = this.isValidSupplierForm.bind(this);
        this.onProcessActionStateChanged = this.onProcessActionStateChanged.bind(this);
        this.onProcessEditSupplier = this.onProcessEditSupplier.bind(this);
        this.onProcessCreateSupplier = this.onProcessCreateSupplier.bind(this);
        this.onProcessGetSupplierById = this.onProcessGetSupplierById.bind(this);
        this.onProcessGetInitCreateEditPageItems = this.onProcessGetInitCreateEditPageItems.bind(this);
        this.props.reduxForm.setInitializeFormDataAction(this.defaultInitDateReduxForm);
    }

    public componentDidMount() {
        this.props.dispatchGetInitCreateEditPageItemsThunk({
            force: true
        });
    }

    private async onProcessActionStateChanged(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        this.onProcessGetInitCreateEditPageItems(processId, actionState, error);

        this.onProcessGetSupplierById(processId, actionState, error);

        this.onProcessCreateSupplier(processId, actionState, error);

        this.onProcessEditSupplier(processId, actionState, error);
    }

    private onProcessGetInitCreateEditPageItems(processId: string, actionState: IReducerProcessActionInfo, _?: Error) {
        if (processId === SuppliersProcessId.GetInitCreateEditPageItems) {
            if (actionState.isInSuccessState) {
                if (this.state.pageType === PageEnumType.Edit) {
                    this.props.dispatchGetSupplierByIdThunk({
                        supplierId: this.state.currentSupplierId!,
                        force: true
                    });
                }
                if (this.state.pageType === PageEnumType.Create) {
                    this.props.reduxForm.updateReduxFormFields({
                        localeId: this.props.locales[0].value
                    });
                }
            }
        }
    }

    private onProcessGetSupplierById(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId === SuppliersProcessId.GetSupplierById) {
            if (actionState.isInSuccessState) {
                const { supplier } = this.props;

                this.props.reduxForm.updateReduxFormFields({
                    contentName: supplier.agreement?.fileName,
                    code: supplier.code,
                    isDefault: supplier.isDefault,
                    localeId: supplier.localeId,
                    name: supplier.name,
                    printedHtmlFormInvoiceId: supplier.printedHtmlFormInvoiceId,
                    printedHtmlFormInvoiceReceiptId: supplier.printedHtmlFormInvoiceReceiptId
                });
            }
            if (actionState.isInErrorState) {
                this.successOrFaildStateNotification(`Ошибка при инициализации поставщика: ${ error?.message }`, false);
            }
        }
    }

    private onProcessCreateSupplier(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId === SuppliersProcessId.CreateSupplier) {
            if (actionState.isInSuccessState) {
                this.successOrFaildStateNotification('Запись успешно создана', true);
            }
            if (actionState.isInErrorState) {
                this.successOrFaildStateNotification(`Ошибка при создании записи: ${ error?.message }`, false);
            }
        }
    }

    private onProcessEditSupplier(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId === SuppliersProcessId.EditSupplier) {
            if (actionState.isInSuccessState) {
                this.successOrFaildStateNotification('Запись успешно сохранена', true);
            }
            if (actionState.isInErrorState) {
                this.successOrFaildStateNotification(`Ошибка сохранения записи: ${ error?.message }`, false);
            }
        }
    }

    private onFileSelectSuccess(acceptedFile: File) {
        const { errors } = this.state;
        errors.agreementCloudFile = '';
        this.setState({ errors });

        this.props.reduxForm.updateReduxFormFields({
            cloudFile: acceptedFile
        });
    }

    private onFileSelectError(errorMessage: string) {
        const { errors } = this.state;
        errors.agreementCloudFile = errorMessage;
        this.setState({ errors });
    }

    private onValueChange<TValue, TForm extends string = CreateEditSupplierDataFormFieldNamesType>(formName: TForm, newValue: TValue) {
        this.props.reduxForm.updateReduxFormFields({
            [formName]: newValue
        });

        const { errors } = this.state;

        if (typeof newValue === 'string') {
            switch (formName) {
                case 'name':
                    errors[formName] = checkValidOfStringField(newValue, 'Название поставщика - обязательное поле', SupplierFieldLengthEnumType.Name);
                    break;
                case 'code':
                    errors[formName] = checkValidOfStringField(newValue, 'Код поставщика - обязательное поле', SupplierFieldLengthEnumType.Code);
                    break;
                default:
                    break;
            }

            this.setState({ errors });
        }
    }

    private getInitState(props: AllProps) {
        const stateObj = {
            errors: {},
            pageType: PageEnumType.Create
        } as OwnState;

        const currentSupplierId = props.match.params.id;

        if (currentSupplierId && GuidRegex.test(currentSupplierId)) {
            stateObj.pageType = PageEnumType.Edit;
            stateObj.currentSupplierId = currentSupplierId;
        } else if (currentSupplierId && !GuidRegex.test(currentSupplierId)) {
            this.props.history.push(AppRoutes.homeRoute);
        }

        return stateObj;
    }

    private getLabelSupplierCode() {
        return (
            <>
                Код поставщика
                <TextWithTooltip
                    text={ <i className="fa fa-question-circle ml-1" /> }
                    tooltipPlacement={ TooltipPlacementTypes.top }
                    tooltipText="Можно получить в КОРПe &gt; Справочник Организаций"
                />
            </>
        );
    }

    private defaultInitDateReduxForm(): CreateEditSupplierDataForm | undefined {
        return {
            code: '',
            isDefault: false,
            localeId: '',
            name: '',
            contentName: '',
            printedHtmlFormInvoiceId: '',
            printedHtmlFormInvoiceReceiptId: ''
        };
    }

    private isValidSupplierForm(formFields: CreateEditSupplierDataForm) {
        const errors: IErrorsType = {};
        let result = true;

        if (!formFields.name || formFields.name === '') {
            errors.name = 'Название поставщика - обязательное поле';
            result = false;
        } else if (formFields.name && formFields.name.length > SupplierFieldLengthEnumType.Name) {
            errors.name = `Пожалуйста, введите не больше чем ${ SupplierFieldLengthEnumType.Name } символов`;
            result = false;
        }
        if (!formFields.code || formFields.code === '') {
            errors.code = 'Код поставщика - обязательное поле';
            result = false;
        } else if (formFields.code && formFields.code.length > SupplierFieldLengthEnumType.Code) {
            errors.code = `Пожалуйста, введите не больше чем ${ SupplierFieldLengthEnumType.Code } символов`;
            result = false;
        }
        if (formFields.isDefault) {
            errors.agreementCloudFile = 'Договор оферты обязателен, если поставщик стандартный на локаль.';

            if (this.state.pageType === PageEnumType.Edit && !formFields.contentName) result = false;

            if (this.state.pageType === PageEnumType.Create && !formFields.cloudFile) result = false;
        }
        if (!result) {
            this.props.floatMessage.show(MessageType.Warning, 'Форма к отправке не готова. Пожалуйста, заполните все поля.', 3000);
            this.setState({ errors });
        }

        return result;
    }

    private successOrFaildStateNotification(message: string, isSuccess: boolean) {
        if (isSuccess) {
            this.props.floatMessage.show(MessageType.Success, message, 3000);
            this.props.history.push(AppRoutes.administration.suppliersRoute);
        } else {
            this.props.floatMessage.show(MessageType.Error, message, 5000);
        }
    }

    private submitButton() {
        const formFields = this.props.reduxForm.getReduxFormFields(false);

        if (!this.isValidSupplierForm(formFields)) return;

        if (this.state.pageType === PageEnumType.Create) {
            this.props.dispatchCreateSupplierThunk({
                id: '',
                localeName: '',
                code: formFields.code,
                isDefault: formFields.isDefault,
                localeId: formFields.localeId,
                name: formFields.name,
                cloudFile: formFields.cloudFile,
                printedHtmlFormInvoiceId: formFields.printedHtmlFormInvoiceId,
                printedHtmlFormInvoiceReceiptId: formFields.printedHtmlFormInvoiceReceiptId,
                force: true
            });
        } else if (this.state.pageType === PageEnumType.Edit) {
            this.props.dispatchEditSupplierThunk({
                id: this.state.currentSupplierId!,
                localeName: '',
                agreementId: this.props.supplier.agreementId,
                code: formFields.code,
                isDefault: formFields.isDefault,
                localeId: formFields.localeId,
                name: formFields.name,
                cloudFile: formFields.cloudFile,
                printedHtmlFormInvoiceId: formFields.printedHtmlFormInvoiceId,
                printedHtmlFormInvoiceReceiptId: formFields.printedHtmlFormInvoiceReceiptId,
                force: true
            });
        }
    }

    public render() {
        const { errors, pageType } = this.state;
        const formFields = this.props.reduxForm.getReduxFormFields(false);
        const textTitle = pageType === PageEnumType.Create ? 'Добавление' : 'Редактирование';

        return (
            <>
                <PageHeaderView title={ `${ textTitle } поставщика` } />
                <Box display="flex" flexDirection="column" gap="25px">
                    <div>
                        <TextBoxForm
                            label="Название поставщика"
                            formName="name"
                            onValueChange={ this.onValueChange }
                            autoFocus={ false }
                            value={ formFields.name }
                            placeholder="Введите название поставщика"
                        />
                        <ShowErrorMessage errors={ errors } formName="name" />
                    </div>
                    <div>
                        <TextBoxForm
                            formName="code"
                            label={ <span>{ this.getLabelSupplierCode() }</span> }
                            onValueChange={ this.onValueChange }
                            autoFocus={ false }
                            value={ formFields.code }
                            placeholder="Введите код поставщика"
                        />
                        <ShowErrorMessage errors={ errors } formName="code" />
                    </div>
                    <div>
                        <ComboBoxForm
                            formName="localeId"
                            label="Локаль"
                            value={ formFields.localeId }
                            items={ this.props.locales }
                            onValueChange={ this.onValueChange }
                        />
                    </div>
                    <div>
                        <CheckBoxForm
                            formName="isDefault"
                            label="Стандартный поставщик на локаль"
                            isChecked={ formFields.isDefault }
                            onValueChange={ this.onValueChange }
                        />
                    </div>
                    <div>
                        <InputFileUploaderForm
                            label="Договор оферты"
                            accept=".pdf"
                            defaultContentName={ formFields.contentName }
                            onFileSelectError={ this.onFileSelectError }
                            onFileSelectSuccess={ this.onFileSelectSuccess }
                        />
                        <ShowErrorMessage errors={ errors } formName="agreementCloudFile" />
                    </div>
                    <div>
                        <ComboBoxForm
                            formName="printedHtmlFormInvoiceId"
                            label="Печатная форма счета на оплату"
                            value={ formFields.printedHtmlFormInvoiceId }
                            items={ this.props.printedHtmlForms }
                            onValueChange={ this.onValueChange }
                        />
                    </div>
                    <div>
                        <ComboBoxForm
                            formName="printedHtmlFormInvoiceReceiptId"
                            label="Печатная форма фискального чека"
                            value={ formFields.printedHtmlFormInvoiceReceiptId }
                            items={ this.props.printedHtmlForms }
                            onValueChange={ this.onValueChange }
                        />
                    </div>
                </Box>
                <Box display="flex" flexDirection="row" justifyContent="flex-end" marginTop="16px" gap="16px">
                    <Link
                        to={ AppRoutes.administration.suppliersRoute }
                        style={ { textDecoration: 'none', color: 'inherit' } }
                    >
                        <DefaultButton>
                            Отмена
                        </DefaultButton>
                    </Link>
                    <SuccessButton
                        onClick={ this.submitButton }
                    >
                        {
                            pageType === PageEnumType.Create
                                ? 'Создать'
                                : 'Сохранить'
                        }
                    </SuccessButton>
                </Box>
                <WatchReducerProcessActionState
                    watch={ [{
                        reducerStateName: 'SuppliersState',
                        processIds: [
                            SuppliersProcessId.GetInitCreateEditPageItems,
                            SuppliersProcessId.CreateSupplier,
                            SuppliersProcessId.EditSupplier,
                            SuppliersProcessId.GetSupplierById,
                        ]
                    }] }
                    onProcessActionStateChanged={ this.onProcessActionStateChanged }
                />
            </>
        );
    }
}

export const CreateEditSupplierConnected = connect<StateProps, DispatchProps, OwnProps, AppReduxStoreState>(
    state => {
        const suppliersState = state.SuppliersState;
        const { locales } = suppliersState.initItemsCreateEditPage;
        const { printedHtmlForms } = suppliersState.initItemsCreateEditPage;
        const { supplier } = suppliersState;

        return {
            locales,
            printedHtmlForms,
            supplier
        };
    },
    {
        dispatchGetInitCreateEditPageItemsThunk: GetInitCreateEditPageItemsThunk.invoke,
        dispatchCreateSupplierThunk: CreateSupplierThunk.invoke,
        dispatchEditSupplierThunk: EditSupplierThunk.invoke,
        dispatchGetSupplierByIdThunk: GetSupplierByIdThunk.invoke
    }
)(CreateEditSupplierClass);

export const CreateEditSupplierView = withReduxForm(withFloatMessages(CreateEditSupplierConnected),
    {
        reduxFormName: 'Supplier_Create_Edit_Form',
        resetOnUnmount: true
    });