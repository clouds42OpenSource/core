import { hasComponentChangesFor } from 'app/common/functions';
import { DynAutocompleteComboBoxForm } from 'app/views/components/controls/forms/ComboBox/DynAutocompleteComboBoxForm';
import { ComboboxItemModel } from 'app/views/components/controls/forms/ComboBox/models';
import { KeyValueDataModel } from 'app/web/common/data-models';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { SupplierReferralAccountsDataModel } from 'app/web/InterlayerApiProxy/SuppliersApiProxy/getReferralAccountsBySupplierId';
import { RequestKind } from 'core/requestSender/enums';
import React, { Component } from 'react';

type SearchReferalProps = {
    /**
     * Заголовок
     */
    label?: string;

    /**
     * Авто фокус
     */
    autoFocus?: boolean;

    /**
     * Название класса
     */
    className?: string;

    /**
     * Название класса всплывающей менюшки
     */
    popperClassName?: string;

    /**
     * Call back Функция на изменения Combobox
     */
    onValueChange: (referalAccountId: string) => void;
};

/**
 * Компонент для поиска реферальных аккаунтов
 */
export class SearchReferalAccountView extends Component<SearchReferalProps> {
    public constructor(props: SearchReferalProps) {
        super(props);

        this.searchReferalAccounts = this.searchReferalAccounts.bind(this);
        this.onValueChange = this.onValueChange.bind(this);
        this.foundReferalAccountsToComboboxItemModel = this.foundReferalAccountsToComboboxItemModel.bind(this);
    }

    public shouldComponentUpdate(nextProps: SearchReferalProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    /**
     * Функция на изменение Combobox'а
     * @param _formName Название формы
     * @param _originalItems Все элементы
     * @param newValue Новое значение
     */
    private onValueChange(_formName: string, _originalItems: SupplierReferralAccountsDataModel, newValue: string) {
        this.props.onValueChange(newValue);
    }

    /**
     * Функция для поиска реферальных аккаунтов
     * @param searchValue Значение поиска
     * @param setItems Функция на добавление найденны элементов в state Combobox'а
     */
    private searchReferalAccounts(searchValue: string, setItems: (items: SupplierReferralAccountsDataModel) => void) {
        const api = InterlayerApiProxy.getSuppliersApiProxy();
        api.getReferralAccountsBySearchValue(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, searchValue).then(foundReferalAccounts => { setItems(foundReferalAccounts); });
    }

    /**
     * Маппинг найденного элемента в тип объекта списка элементов для ComboBox
     * @param item Тип ключ-значение
     * @returns Тип объекта списка элементов для ComboBox
     */
    private foundReferalAccountsToComboboxItemModel(item: KeyValueDataModel<string, string>): ComboboxItemModel<string> {
        return {
            value: item.key,
            text: item.value
        };
    }

    public render() {
        const { autoFocus, label } = this.props;
        return (
            <DynAutocompleteComboBoxForm
                autoFocus={ autoFocus }
                className={ this.props.className }
                popperClassName={ this.props.popperClassName }
                label={ label }
                value=""
                formName=""
                placeholder="Выберите аккаунт"
                onValueChange={ this.onValueChange }
                onGetItems={ this.searchReferalAccounts }
                originalItemToComboboxItemModel={ this.foundReferalAccountsToComboboxItemModel }
            />
        );
    }
}