declare const styles: {
    readonly 'offer-link': string;
    readonly 'text-row-table': string;
    readonly 'offer-wrapper': string;
    readonly 'button-container': string;
    readonly 'name-container': string;
};
export = styles;