import ErrorOutlineIcon from '@mui/icons-material/ErrorOutline';
import { Box, Link as MuiLink } from '@mui/material';
import { AppRoutes } from 'app/AppRoutes';
import { SuppliersProcessId } from 'app/modules/suppliers';
import { SupplierThunkParams } from 'app/modules/suppliers/store/reducers/common/params';
import { GetSuppliersWithPaginationThunkParams } from 'app/modules/suppliers/store/reducers/getSuppliersWithPaginationReducer/params';
import { GetSuppliersWithPaginationThunk } from 'app/modules/suppliers/store/thunks';
import { DeleteSupplierThunk } from 'app/modules/suppliers/store/thunks/DeleteSupplierThunk';
import { AppReduxStoreState } from 'app/redux/types';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { withHeader } from 'app/views/components/_hoc/withHeader';
import { OutlinedButton, SuccessButton, TextLinkButton } from 'app/views/components/controls/Button';
import { Dialog } from 'app/views/components/controls/Dialog';
import { TextWithTooltip } from 'app/views/components/controls/forms/Tooltips';
import { TooltipPlacementTypes } from 'app/views/components/controls/forms/Tooltips/types';
import { TextOut } from 'app/views/components/TextOut';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { WatchReducerProcessActionState } from 'app/views/modules/_common/components/WatchReducerProcessActionState';
import { SelectDataCommonDataModel } from 'app/web/common/data-models';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { SupplierDataModel, SuppliersPaginationDataModel } from 'app/web/InterlayerApiProxy/SuppliersApiProxy/getSuppliersWithPagination';
import { IReducerProcessActionInfo } from 'core/redux/interfaces';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Link, RouteComponentProps } from 'react-router-dom';
import css from './styles.module.css';

type OwnState = {
    currentSupplierId?: string;
    deleteIsOpen: boolean;
};

type StateProps = {
    suppliers: SuppliersPaginationDataModel
};

type DispatchProps = {
    dispatchGetSuppliersWithPaginationThunk: (args: GetSuppliersWithPaginationThunkParams) => void;
    dispatchDeleteSupplierThunk: (args: SupplierThunkParams) => void;
};

type AllProps = StateProps & DispatchProps & FloatMessageProps & RouteComponentProps;

class SupplierClass extends Component<AllProps, OwnState> {
    public constructor(props: AllProps) {
        super(props);

        this.state = {
            deleteIsOpen: false
        };

        this.onDataSelect = this.onDataSelect.bind(this);
        this.getFormatSupplierName = this.getFormatSupplierName.bind(this);
        this.getFormatActionButtons = this.getFormatActionButtons.bind(this);
        this.onClickDeleteSupplier = this.onClickDeleteSupplier.bind(this);
        this.getFormatAdditionalButton = this.getFormatAdditionalButton.bind(this);
        this.onClickRedirectToReferalAccounts = this.onClickRedirectToReferalAccounts.bind(this);
        this.onProcessActionStateChanged = this.onProcessActionStateChanged.bind(this);
        this.openModalForDeleteSupplier = this.openModalForDeleteSupplier.bind(this);
        this.closeModalForDeleteSupplier = this.closeModalForDeleteSupplier.bind(this);
        this.openOfferDocumentFile = this.openOfferDocumentFile.bind(this);
    }

    public componentDidMount() {
        this.props.dispatchGetSuppliersWithPaginationThunk({
            page: 1,
            force: true
        });
    }

    private async onProcessActionStateChanged(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId === SuppliersProcessId.DeleteSupplier) {
            if (actionState.isInSuccessState) {
                this.props.floatMessage.show(MessageType.Success, 'Запись успешно удалена', 3000);
                this.props.dispatchGetSuppliersWithPaginationThunk({
                    page: 1,
                    force: true
                });
            }
            if (actionState.isInErrorState) {
                this.props.floatMessage.show(MessageType.Error, `Ошибка удаления записи: ${ error?.message }`, 5000);
            }
        }
    }

    private onClickDeleteSupplier() {
        const { currentSupplierId } = this.state;

        if (currentSupplierId) {
            this.props.dispatchDeleteSupplierThunk({
                supplierId: currentSupplierId,
                force: true
            });
            this.setState({
                deleteIsOpen: false
            });
        }
    }

    private onClickRedirectToReferalAccounts(id: string) {
        this.props.history.push(`${ AppRoutes.administration.suppliers.supplierReferalAccountsPath }/${ id }`);
    }

    private onDataSelect(args: SelectDataCommonDataModel<undefined>) {
        this.props.dispatchGetSuppliersWithPaginationThunk({
            page: args.pageNumber,
            force: true
        });
    }

    private getFormatAdditionalButton(_: boolean, data: SupplierDataModel) {
        return (
            <Link
                to={ `${ AppRoutes.administration.suppliers.supplierReferalAccountsPath }/${ data.id }` }
                style={ { textDecoration: 'none' } }
            >
                <OutlinedButton
                    kind="success"
                    className="font-weight-normal"
                >
                    Реферальные аккаунты
                </OutlinedButton>
            </Link>
        );
    }

    private getFormatSupplierName(value: string, data: SupplierDataModel) {
        return (
            <div className={ css['name-container'] }>
                <Link to={ `${ AppRoutes.administration.suppliers.editCreateSupplierPath }/${ data.id }` }>
                    <TextLinkButton>
                        <TextOut fontSize={ 13 } fontWeight={ 600 } className="text-link">
                            <span className={ css['text-row-table'] }>{ value }</span>
                            { data.isDefault
                                ? (
                                    <span className="text text-info ml-2">
                                        <TextWithTooltip
                                            text={ <i className="fa fa-check-circle-o" /> }
                                            tooltipPlacement={ TooltipPlacementTypes.top }
                                            tooltipText="Является стандартным поставщиком для своей локали"
                                        />
                                    </span>
                                )
                                : null
                            }
                        </TextOut>
                    </TextLinkButton>
                </Link>
                <MuiLink onClick={ () => this.openOfferDocumentFile(data.id) }>Договор оферты</MuiLink>
            </div>
        );
    }

    private getFormatActionButtons(supplierId: string) {
        return (
            <div className={ css['button-container'] }>
                <Link to={ `${ AppRoutes.administration.suppliers.editCreateSupplierPath }/${ supplierId }` } style={ { textDecoration: 'none' } }>
                    <OutlinedButton kind="success" className="font-weight-normal">Редактировать</OutlinedButton>
                </Link>
                <OutlinedButton
                    kind="error"
                    className="font-weight-normal"
                    onClick={ () => {
                        this.openModalForDeleteSupplier(supplierId);
                    } }
                >
                    <i className="fa fa-times-circle mr-1" />&nbsp;Удалить
                </OutlinedButton>
            </div>
        );
    }

    private async openOfferDocumentFile(id: string) {
        const { responseBody } = await InterlayerApiProxy.getSuppliersApiProxy().getOfferDocumentFile(id);
        window.open(URL.createObjectURL(responseBody), '_blank');
    }

    private openModalForDeleteSupplier(supplierId: string) {
        this.setState({
            currentSupplierId: supplierId,
            deleteIsOpen: true
        });
    }

    private closeModalForDeleteSupplier() {
        this.setState({
            deleteIsOpen: false
        });
    }

    public render() {
        const { metadata, records } = this.props.suppliers;

        return (
            <>
                <Link to={ AppRoutes.administration.suppliers.editCreateSupplierPath } style={ { textDecoration: 'none' } }>
                    <SuccessButton kind="success" style={ { marginBottom: '10px' } }>
                        <i className="fa fa-plus-circle mr-1" />&nbsp;Добавить поставщика
                    </SuccessButton>
                </Link>
                <CommonTableWithFilter
                    isBorderStyled={ true }
                    isResponsiveTable={ true }
                    uniqueContextProviderStateId="SuppliersTableContextProviderStateId"
                    tableProps={ {
                        dataset: records,
                        keyFieldName: 'id',
                        fieldsView: {
                            name: {
                                caption: 'Название поставщика',
                                fieldWidth: '35%',
                                fieldContentNoWrap: false,
                                format: this.getFormatSupplierName,
                            },
                            code: {
                                caption: 'Код',
                                fieldWidth: '200px',
                                fieldContentNoWrap: false
                            },
                            localeName: {
                                caption: 'Локаль',
                                fieldWidth: '50px',
                                fieldContentNoWrap: false
                            },
                            id: {
                                caption: 'Действия',
                                fieldWidth: '245px',
                                fieldContentNoWrap: false,
                                format: this.getFormatActionButtons
                            },
                            isDefault: {
                                caption: 'Дополнительно',
                                fieldWidth: '200px',
                                fieldContentNoWrap: false,
                                format: this.getFormatAdditionalButton
                            }
                        },
                        pagination: {
                            currentPage: metadata.pageNumber,
                            pageSize: metadata.pageSize,
                            recordsCount: metadata.totalItemCount,
                            totalPages: metadata.pageCount
                        },
                    } }
                    onDataSelect={ this.onDataSelect }
                />
                <WatchReducerProcessActionState
                    watch={ [{
                        reducerStateName: 'SuppliersState',
                        processIds: [
                            SuppliersProcessId.DeleteSupplier
                        ]
                    }] }
                    onProcessActionStateChanged={ this.onProcessActionStateChanged }
                />
                <Dialog
                    isOpen={ this.state.deleteIsOpen }
                    dialogWidth="xs"
                    dialogVerticalAlign="center"
                    onCancelClick={ this.closeModalForDeleteSupplier }
                    buttons={
                        [
                            {
                                content: 'Нет',
                                kind: 'primary',
                                variant: 'text',
                                onClick: this.closeModalForDeleteSupplier
                            },
                            {
                                content: 'Да',
                                kind: 'error',
                                variant: 'contained',
                                onClick: this.onClickDeleteSupplier
                            }
                        ]
                    }
                >
                    <Box display="flex" flexDirection="column" alignItems="center" justifyContent="center">
                        <ErrorOutlineIcon color="warning" sx={ { width: '5em', height: '5em' } } />
                        Вы действительно хотите удалить поставщика?
                    </Box>
                </Dialog>
            </>
        );
    }
}

const SupplierConnected = connect<StateProps, DispatchProps, {}, AppReduxStoreState>(
    state => {
        const { suppliers } = state.SuppliersState;

        return {
            suppliers
        };
    },
    {
        dispatchGetSuppliersWithPaginationThunk: GetSuppliersWithPaginationThunk.invoke,
        dispatchDeleteSupplierThunk: DeleteSupplierThunk.invoke
    }
)(withRouter(SupplierClass));

export const SupplierView = withHeader({
    title: 'Управление поставщиками',
    page: withFloatMessages(SupplierConnected)
});