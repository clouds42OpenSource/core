import { Box } from '@mui/material';
import { apiHost, templateErrorText } from 'app/api';
import { getFastaResourcesResponseDto } from 'app/api/endpoints/fasta/response';
import { FETCH_API } from 'app/api/useFetchApi';
import { AccountUserGroup } from 'app/common/enums';
import { AppReduxStoreState } from 'app/redux/types';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { withHeader } from 'app/views/components/_hoc/withHeader';
import { TabsControl } from 'app/views/components/controls/TabsControl';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { TextOut } from 'app/views/components/TextOut';
import { FastaControl } from 'app/views/modules/Fasta/tabs/FastaControl';
import { FastaInfo } from 'app/views/modules/Fasta/tabs/FastaInfo';
import { useEffect, useState } from 'react';
import { connect } from 'react-redux';

type StateProps = {
    currentUserGroups: AccountUserGroup[];
};

type DispatchProps = {
// todo add localizations
};

export const FastaView = ({ floatMessage: { show }, currentUserGroups }: StateProps & DispatchProps & FloatMessageProps) => {
    const [activeTabIndex, setActiveTabIndex] = useState(0);
    const [isLoading, setIsLoading] = useState(false);
    const [fastaResourcesData, setFastaResourcesData] = useState<getFastaResourcesResponseDto | null>(null);
    const { data: fasta, isLoading: isFastaLoading } = FETCH_API.FASTA.useGetFasta();

    const fastaData = fasta?.rawData;

    const handleGetFastaResources = async () => {
        setIsLoading(true);
        const response = await FETCH_API.FASTA.getFastaResources();
        setIsLoading(false);

        if (response.success) {
            setFastaResourcesData(response.data);
        } else {
            show(MessageType.Error, response.message ?? templateErrorText);
        }
    };

    useEffect(() => {
        setIsLoading(isFastaLoading);
        if (isFastaLoading) {
            setIsLoading(true);
        } else if (!fastaResourcesData && (currentUserGroups.includes(AccountUserGroup.CloudAdmin) || currentUserGroups.includes(AccountUserGroup.AccountSaleManager))) {
            void handleGetFastaResources();
        } else {
            setIsLoading(false);
        }
    }, [isFastaLoading]);

    if (isLoading && !fastaData) {
        return <LoadingBounce />;
    }

    return (
        <div>
            { isLoading && <LoadingBounce /> }
            { !activeTabIndex && (
                <Box display="grid" gridTemplateColumns="200px 1fr" alignItems="start">
                    <img width="92px" src={ apiHost('img/services/esdl.png') } alt="esdl" />
                    <TextOut inDiv={ true } fontSize={ 13 } fontWeight={ 400 }>
                        { fastaData?.serviceNameRepresentation.summary }
                    </TextOut>
                </Box>
            )
            }
            <TabsControl
                activeTabIndex={ activeTabIndex }
                headers={
                    [{
                        title: 'Информация о сервисе',
                        isVisible: true
                    }, {
                        title: 'Управление',
                        isVisible: currentUserGroups.includes(AccountUserGroup.CloudAdmin) || currentUserGroups.includes(AccountUserGroup.Hotline) || currentUserGroups.includes(AccountUserGroup.AccountSaleManager)
                    }] }
            >
                { fastaData && <FastaInfo setIsLoading={ setIsLoading } show={ show } fastaData={ fastaData } /> }
                { fastaData && fastaResourcesData && <FastaControl currentUserGroups={ currentUserGroups } setIsLoading={ setIsLoading } show={ show } fastaResourcesData={ fastaResourcesData } /> }
            </TabsControl>
        </div>
    );
};

const FastaViewConnected = connect<StateProps, DispatchProps, object, AppReduxStoreState>(
    state => {
        const { currentUserGroups } = state.Global.getCurrentSessionSettingsReducer.settings.currentContextInfo;
        return {
            currentUserGroups,
        };
    },
    {
        // todo add localizations
    }
)(FastaView);

export const FastaViewComponent = withHeader({
    title: 'Сервис "Fasta"',
    page: withFloatMessages(FastaViewConnected)
});