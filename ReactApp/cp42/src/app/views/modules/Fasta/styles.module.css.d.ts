declare const styles: {
    readonly 'block-wrapper': string;
    readonly 'table-wrapper': string;
    readonly 'form-wrapper': string;
};
export = styles;