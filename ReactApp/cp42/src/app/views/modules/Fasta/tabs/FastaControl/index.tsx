import { Box, Button, TextField, Typography } from '@mui/material';
import { DateTimePicker, LocalizationProvider } from '@mui/x-date-pickers';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { getFastaDemoResponseDto, getFastaResourcesResponseDto } from 'app/api/endpoints/fasta/response';
import { FETCH_API } from 'app/api/useFetchApi';
import { AppRoutes } from 'app/AppRoutes';
import { AccountUserGroup } from 'app/common/enums';
import { DateUtility } from 'app/utils';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { Dialog } from 'app/views/components/controls/Dialog';
import css from 'app/views/modules/Fasta/styles.module.css';
import ruLocale from 'date-fns/locale/ru';
import dayjs from 'dayjs';
import { ReactNode, useEffect, useState } from 'react';
import { useHistory } from 'react-router';

type Props = {
    fastaResourcesData: getFastaResourcesResponseDto;
    show: (type: MessageType, content: ReactNode, autoHideDuration?: number) => void;
    setIsLoading: (value: boolean) => void;
    currentUserGroups: AccountUserGroup[];
};

export const FastaControl = ({ fastaResourcesData, show, setIsLoading, currentUserGroups }: Props) => {
    const [isDemoProlong, setIsDemoProlong] = useState(false);
    const [date, setDate] = useState(fastaResourcesData.expiredate);
    const [comment, setComment] = useState('');
    const [pagesAmount, setPagesAmount] = useState(fastaResourcesData.pagesLeft);
    const [demoData, setDemoData] = useState<getFastaDemoResponseDto | null>(null);
    const [demoExpireDate, setDemoExpireDate] = useState('');
    const history = useHistory();

    const isAllowChange = currentUserGroups.includes(AccountUserGroup.CloudAdmin);

    const handleDateTimePickerChange = (dateTime: Date | null) => {
        if (dateTime) {
            setDate(DateUtility.dateToIsoDateString(dateTime));
        }
    };

    const handleEditResource = async () => {
        if (!comment) {
            show(MessageType.Warning, 'Заполните поле комментарий');
            return;
        }

        const resourceData = {
            accountId: fastaResourcesData.accountId,
            pagesResourceId: fastaResourcesData.pagesResourceId,
            expireDate: dayjs(date).format('YYYY-MM-DDTHH:mm:ss'),
            pagesAmount,
            comment
        };
        setIsLoading(true);
        const response = await FETCH_API.FASTA.editFastaResources(resourceData);
        setIsLoading(false);

        if (response.success) {
            show(MessageType.Success, 'Успешно');
            history.push(AppRoutes.services.fasta);
        } else {
            show(MessageType.Error, response.message);
        }
    };

    const handleCloseDemoModal = () => {
        setIsDemoProlong(false);
    };

    const handleGetDemoData = async () => {
        setIsLoading(true);
        const response = await FETCH_API.FASTA.getFastaDemoPeriod();
        setIsLoading(false);

        if (response.success) {
            setDemoData(response.data);
            setIsDemoProlong(true);
        } else {
            show(MessageType.Error, response.message);
        }
    };

    const handleProlongDemo = async () => {
        setIsLoading(true);
        const response = await FETCH_API.FASTA.editFastaDemoPeriod(demoExpireDate);
        setIsLoading(false);

        if (response.success) {
            show(MessageType.Success, 'Успешно');
            history.push(AppRoutes.services.fasta);
        } else {
            show(MessageType.Error, response.message);
        }
    };

    const handleDemoExpiredDateChange = (dateTime: Date | null) => {
        if (dateTime) {
            setDemoExpireDate(DateUtility.dateToIsoDateString(dateTime));
        }
    };

    useEffect(() => {
        if (demoData?.demoExpiredDate) {
            setDemoExpireDate(demoData.demoExpiredDate);
        } else {
            setDemoExpireDate('');
        }
    }, [demoData]);

    return (
        <div>
            <Box className={ `${ css['block-wrapper'] } ${ css['form-wrapper'] }` }>
                <Box>
                    <Typography variant="caption">
                        Кол-во страниц:
                    </Typography>
                    <TextField
                        value={ pagesAmount }
                        disabled={ !isAllowChange }
                        onChange={ e => setPagesAmount(Number(e.target.value)) }
                        type="number"
                        inputProps={ {
                            style: {
                                fontSize: '14px',
                                padding: '6px',
                                color: '#676a6c'
                            },
                        } }
                    />
                </Box>
                <Box>
                    <Typography variant="caption">
                        Активно до:
                    </Typography>
                    <LocalizationProvider dateAdapter={ AdapterDateFns } adapterLocale={ ruLocale }>
                        <DateTimePicker
                            sx={ {
                                '& input': {
                                    padding: '6px',
                                    color: '#676a6c'
                                }
                            } }
                            disabled={ !isAllowChange }
                            onChange={ handleDateTimePickerChange }
                            value={ new Date(date) }
                        />
                    </LocalizationProvider>
                </Box>
                <Box>
                    <Typography variant="caption">
                        Комментарий
                    </Typography>
                    <TextField
                        value={ comment }
                        onChange={ e => setComment(e.target.value) }
                        disabled={ !isAllowChange }
                        inputProps={ {
                            style: {
                                fontSize: '14px',
                                padding: '6px',
                                color: '#676a6c'
                            },
                        } }
                    />
                </Box>
                {
                    isAllowChange && (
                        <Button
                            variant="contained"
                            onClick={ handleEditResource }
                            sx={ {
                                textTransform: 'initial',
                                color: 'white',
                                background: '#1ab394',
                                '&:hover': {
                                    background: '#1ab394'
                                }
                            } }
                        >
                            Сохранить изменения
                        </Button>
                    )
                }
                {
                    fastaResourcesData.isDemoPeriod
                        ? (
                            <Button
                                onClick={ handleGetDemoData }
                                variant="contained"
                                sx={ {
                                    textTransform: 'initial',
                                    color: 'white',
                                    background: '#23c6c8',
                                    marginLeft: '6px',
                                    '&:hover': {
                                        background: '#23c6c8',
                                    },
                                } }
                            >
                                Продлить демо период
                            </Button>
                        )
                        : ''
                }
            </Box>
            <Dialog
                isOpen={ isDemoProlong }
                dialogWidth="xs"
                dialogVerticalAlign="center"
                titleFontSize={ 12 }
                isTitleSmall={ true }
                title="Продление демо периода"
                onCancelClick={ handleCloseDemoModal }
                buttons={
                    [
                        {
                            content: 'Закрыть',
                            kind: 'primary',
                            variant: 'contained',
                            onClick: handleCloseDemoModal
                        },
                        {
                            content: 'Продлить',
                            kind: 'primary',
                            variant: 'text',
                            onClick: handleProlongDemo
                        }
                    ]
                }
            >
                {
                    demoData && (
                        <Box display="flex" flexDirection="column" alignItems="center" justifyContent="center" gap="6px">
                            <Typography variant="body2">
                                Выберите дату окончания демо периода
                            </Typography>
                            <LocalizationProvider dateAdapter={ AdapterDateFns } adapterLocale={ ruLocale }>
                                <DateTimePicker
                                    sx={ {
                                        '& input': {
                                            padding: '6px'
                                        }
                                    } }
                                    minDate={ isAllowChange ? undefined : new Date() }
                                    maxDate={ isAllowChange ? undefined : new Date(demoData.maxDemoExpiredDate) }
                                    value={ new Date(demoExpireDate) }
                                    onChange={ handleDemoExpiredDateChange }
                                />
                            </LocalizationProvider>
                        </Box>
                    )
                }
            </Dialog>
        </div>
    );
};