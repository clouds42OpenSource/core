import { Add as AddIcon, Remove as RemoveIcon } from '@mui/icons-material';
import { Box, IconButton, Typography } from '@mui/material';
import { useState } from 'react';

type FastaItemSelectorProps = {
    items: number[];
    setItem: (value: number) => void;
    item: number;
    caption?: string;
};

export const FastaItemSelector = ({ items, setItem, item, caption = '' }: FastaItemSelectorProps) => {
    const [index, setIndex] = useState(1);

    const handleIncrement = () => {
        setItem(items[index + 1]);
        setIndex(index + 1);
    };

    const handleDecrement = () => {
        setItem(items[index - 1]);
        setIndex(index - 1);
    };

    return (
        <Box justifyContent="center" display="flex" alignItems="center">
            <IconButton sx={ { color: '#337ab7' } } size="small" disabled={ index === 0 } onClick={ handleDecrement }><RemoveIcon fontSize="inherit" /></IconButton>
            <Typography variant="caption" minWidth="35px" textAlign="center">{ item } { caption && <b>{ caption }</b> }</Typography>
            <IconButton sx={ { color: '#337ab7' } } size="small" disabled={ items.length - 1 === index } onClick={ handleIncrement }><AddIcon fontSize="inherit" /></IconButton>
        </Box>
    );
};