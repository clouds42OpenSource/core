import { Check as CheckIcon, DoDisturb as DoDisturbIcon, ErrorOutline as ErrorOutlineIcon } from '@mui/icons-material';
import { Box, Button, Typography } from '@mui/material';
import { getCostResponseDto, getFastaResponseDto } from 'app/api/endpoints/fasta/response';
import { FETCH_API } from 'app/api/useFetchApi';
import { AppRoutes } from 'app/AppRoutes';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { Dialog } from 'app/views/components/controls/Dialog';
import css from 'app/views/modules/Fasta/styles.module.css';
import dayjs from 'dayjs';
import 'dayjs/locale/ru';
import { ReactNode, useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import { FastaItemSelector } from './components';

dayjs.locale('ru');

type Props = {
    fastaData: getFastaResponseDto;
    show: (type: MessageType, content: ReactNode, autoHideDuration?: number) => void;
    setIsLoading: (value: boolean) => void;
};

const possibleYears = [0, 1, 2, 3];

export const FastaInfo = ({ fastaData, show, setIsLoading }: Props) => {
    const [pagesAmount, setPagesAmount] = useState(fastaData.defaultPagesTariff);
    const [yearsAmount, setYearsAmount] = useState(fastaData.licensePeriod);
    const [costData, setCostData] = useState<null | getCostResponseDto>(null);
    const [isBuy, setIsBuy] = useState(false);
    const [isModalOpened, setIsModalOpened] = useState(false);
    const history = useHistory();

    const handleCalculateCost = async () => {
        setIsLoading(true);
        const { data } = await FETCH_API.FASTA.getCalculatedCost({ pagesAmount, yearsAmount });
        setIsLoading(false);
        setCostData(data);
    };

    useEffect(() => {
        void handleCalculateCost();
    }, [pagesAmount, yearsAmount]);

    useEffect(() => {
        setPagesAmount(fastaData.defaultPagesTariff);
        setYearsAmount(fastaData.licensePeriod);
    }, [fastaData]);

    const handleTryBuyFasta = async (isShowDialog: boolean) => {
        if (!pagesAmount && !yearsAmount) {
            show(MessageType.Error, 'Выберите количество страниц или лицензий для покупки');
            return;
        }

        if (isShowDialog) {
            setIsModalOpened(true);
        } else {
            setIsLoading(true);
            const response = await FETCH_API.FASTA.tryBuyFasta({ pagesAmount, yearsAmount });
            setIsLoading(false);

            if (response.success) {
                show(MessageType.Success, 'Успешно');
                setIsModalOpened(false);
            } else {
                show(MessageType.Warning, response.message);
                history.push(AppRoutes.accountManagement.balanceManagement);
            }
        }
    };

    const handleGetFastaProgram = async () => {
        setIsLoading(true);
        const response = await FETCH_API.FASTA.getFastaProgram();
        setIsLoading(false);
        const url = window.URL.createObjectURL(new Blob([response.data!]));
        const link = document.createElement('a');
        link.href = url;
        link.download = 'DocLoader.zip';
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    };

    const handleCloseModal = () => {
        setIsModalOpened(false);
    };

    return (
        <div>
            <Box className={ css['block-wrapper'] } sx={ { display: 'grid', gridTemplateColumns: { xs: '1fr', md: '300px 1fr' } } }>
                <Typography variant="body2">
                    Количество страниц для распознавания:
                </Typography>
                <Typography variant="body2">
                    <b> Тариф { fastaData.allowIncreaseDocLoaderByStandardRent1C ? 'Аренда 1С Стандарт' : fastaData.currentPagesTariff }</b> (осталось { fastaData.pagesRemain } страниц)
                </Typography>
                <Typography variant="body2">
                    Использование сервиса возможно до:
                </Typography>
                <Typography variant="body2">
                    { dayjs(fastaData.expireDate).format('D MMMM YYYY [г.]') }
                </Typography>
            </Box>
            <Box className={ css['table-wrapper'] }>
                <Box>
                    <Typography variant="body2">
                        Стоимость лицензии
                    </Typography>
                    <Box>
                        <FastaItemSelector items={ possibleYears } caption="год" setItem={ setYearsAmount } item={ yearsAmount } />
                    </Box>
                    <Typography variant="body2">
                        { costData?.fastaCost } { fastaData.locale.currency }
                    </Typography>
                </Box>
                <Box>
                    <Typography variant="body2">
                        Количество страниц для распознавания
                    </Typography>
                    <Box>
                        <FastaItemSelector items={ [0, ...fastaData.pagesTariffList] } setItem={ setPagesAmount } item={ pagesAmount } />
                    </Box>
                    <Typography variant="body2">
                        { costData?.recognitionCost } { fastaData.locale.currency }
                    </Typography>
                </Box>
                <Box>
                    <Typography variant="body2">
                        <b>Итого</b>
                    </Typography>
                    <Typography variant="body2" />
                    <Typography variant="body2">
                        <b>{ costData?.totalCost } { fastaData.locale.currency }</b>
                    </Typography>
                </Box>
            </Box>
            <Box display="flex" justifyContent="space-between">
                <Button
                    variant="contained"
                    onClick={ handleGetFastaProgram }
                    sx={ {
                        background: '#23c6c8',
                        color: '#FFFFFF',
                        textTransform: 'none',
                        '&:hover': {
                            background: '#23c6c8'
                        }
                    } }
                >
                    Скачать программу "Fasta"
                </Button>
                { !isBuy
                    ? (
                        <Button
                            variant="contained"
                            startIcon={ <CheckIcon /> }
                            onClick={ () => setIsBuy(true) }
                            sx={ {
                                textTransform: 'initial',
                                color: 'white',
                                background: '#1a7bb9',
                                '&:hover': {
                                    background: '#1a7bb9'
                                }
                            } }
                        >
                            Применить
                        </Button>
                    )
                    : (
                        <Box>
                            <Button
                                variant="contained"
                                startIcon={ <CheckIcon /> }
                                onClick={ () => handleTryBuyFasta(fastaData.allowIncreaseDocLoaderByStandardRent1C) }
                                sx={ {
                                    textTransform: 'initial',
                                    color: 'white',
                                    background: '#1a7bb9',
                                    '&:hover': {
                                        background: '#1a7bb9'
                                    }
                                } }
                            >
                                Купить { `(${ costData?.totalCost } ${ fastaData.locale.currency })` }
                            </Button>
                            <Button
                                variant="outlined"
                                startIcon={ <DoDisturbIcon /> }
                                onClick={ () => setIsBuy(false) }
                                sx={ {
                                    textTransform: 'initial',
                                    color: '#333',
                                    background: '#e6e6e6',
                                    border: '1px solid #e6e6e6',
                                    '&:hover': {
                                        background: '#e6e6e6'
                                    }
                                } }
                            >
                                Отменить
                            </Button>
                        </Box>
                    )
                }
            </Box>
            <Dialog
                isOpen={ isModalOpened }
                dialogWidth="sm"
                dialogVerticalAlign="center"
                onCancelClick={ handleCloseModal }
                buttons={
                    [
                        {
                            content: 'Отмена',
                            kind: 'default',
                            variant: 'contained',
                            onClick: handleCloseModal
                        },
                        {
                            content: 'Продолжить',
                            kind: 'primary',
                            variant: 'text',
                            onClick: () => handleTryBuyFasta(false)
                        }
                    ]
                }
            >
                <Box display="flex" flexDirection="column" alignItems="center" justifyContent="center">
                    <ErrorOutlineIcon sx={ { fontSize: '88px', color: '#f8bb86' } } />
                    <Typography>
                        Для вашего аккаунта активирована услуга <b>“Аренда 1С Стандарт”</b>, в рамках которой сервис <b>Fasta</b> предоставляется бесплатно. Вы действительно хотите совершить покупку?
                    </Typography>
                </Box>
            </Dialog>
        </div>

    );
};