import { Dispatch, SetStateAction } from 'react';

export type Props = {
    id: string;
    setCount: Dispatch<SetStateAction<number>>;
    canInstallExtension: boolean;
    name: string;
    isNeedToDelete: boolean;
};