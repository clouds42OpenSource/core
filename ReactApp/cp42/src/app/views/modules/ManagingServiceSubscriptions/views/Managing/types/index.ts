import { GetBillingService } from 'app/api/endpoints/managingServiceSubscriptions/response';

export type Props = {
    billingServiceId: string;
    serviceExpireDate: Date;
    serviceIsLocked: boolean;
    serviceDependsOnRent: boolean;
    billingData: GetBillingService[];
};