import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { memo } from 'react';
import { TextOut } from 'app/views/components/TextOut';
import { Props } from './types';

export const ServicesBillingTableView = memo(({ billingTableDataset, currency }: Props) => {
    const isSponsorTable = billingTableDataset.filter(item => item.sponsoredLicenses || item.sponsoringLicenses).length === 0;
    const sponsoredLicenses = billingTableDataset.filter(item => item.sponsoredLicenses).length;
    const sponsoringLicenses = billingTableDataset.filter(item => item.sponsoringLicenses).length;

    return billingTableDataset && billingTableDataset.length
        ? (
            <CommonTableWithFilter
                uniqueContextProviderStateId="ServicesBillingTable"
                tableProps={ {
                    dataset: billingTableDataset,
                    keyFieldName: 'name',
                    fieldsView: isSponsorTable
                        ? {
                            name: {
                                caption: 'Услуга',
                                style: { lineHeight: '35px' },
                                format: value => {
                                    return value === 'Итого в рамках текущего периода'
                                        ? <TextOut fontWeight={ 700 }>{ value }</TextOut>
                                        : <TextOut>{ value }</TextOut>;
                                }
                            },
                            usedLicenses: {
                                caption: 'Пользователи',
                                format: value => {
                                    return <TextOut textAlign="center" style={ { marginLeft: '40px' } }>{ value }</TextOut>;
                                }
                            },
                            amount: {
                                caption: `Сумма, ${ currency }/мес`,
                                format: value => {
                                    return <TextOut textAlign="center" style={ { marginLeft: '40px' } }>{ value }</TextOut>;
                                }
                            }
                        }
                        : sponsoringLicenses > 0 && sponsoredLicenses > 0
                            ? {
                                name: {
                                    caption: 'Услуга',
                                    style: { lineHeight: '35px' },
                                    format: value => {
                                        return value === 'Итого в рамках текущего периода'
                                            ? <TextOut fontWeight={ 700 }>{ value }</TextOut>
                                            : <TextOut>{ value }</TextOut>;
                                    }
                                },
                                usedLicenses: {
                                    caption: 'Пользователи',
                                    format: value => {
                                        return <TextOut textAlign="center" style={ { marginLeft: '40px' } }>{ value }</TextOut>;
                                    }
                                },
                                sponsoredLicenses: {
                                    caption: 'Спонсируют'
                                },
                                sponsoringLicenses: {
                                    caption: 'Спонсирую'
                                },
                                amount: {
                                    caption: `Сумма, ${ currency }/мес`,
                                    format: value => {
                                        return <TextOut textAlign="center" style={ { marginLeft: '40px' } }>{ value }</TextOut>;
                                    }
                                }
                            }
                            : sponsoringLicenses > 0
                                ? {
                                    name: {
                                        caption: 'Услуга',
                                        style: { lineHeight: '35px' },
                                        format: value => {
                                            return value === 'Итого в рамках текущего периода'
                                                ? <TextOut fontWeight={ 700 }>{ value }</TextOut>
                                                : <TextOut>{ value }</TextOut>;
                                        }
                                    },
                                    usedLicenses: {
                                        caption: 'Пользователи',
                                        format: value => {
                                            return <TextOut textAlign="center" style={ { marginLeft: '40px' } }>{ value }</TextOut>;
                                        }
                                    },
                                    sponsoringLicenses: {
                                        caption: 'Спонсирую'
                                    },
                                    amount: {
                                        caption: `Сумма, ${ currency }/мес`,
                                        format: value => {
                                            return <TextOut textAlign="center" style={ { marginLeft: '40px' } }>{ value }</TextOut>;
                                        }
                                    }
                                }
                                : {
                                    name: {
                                        caption: 'Услуга',
                                        style: { lineHeight: '35px' },
                                        format: value => {
                                            return value === 'Итого в рамках текущего периода'
                                                ? <TextOut fontWeight={ 700 }>{ value }</TextOut>
                                                : <TextOut>{ value }</TextOut>;
                                        }
                                    },
                                    usedLicenses: {
                                        caption: 'Пользователи',
                                        format: value => {
                                            return <TextOut textAlign="center" style={ { marginLeft: '40px' } }>{ value }</TextOut>;
                                        }
                                    },
                                    sponsoredLicenses: {
                                        caption: 'Спонсируют'
                                    },
                                    amount: {
                                        caption: `Сумма, ${ currency }/мес`,
                                        format: value => {
                                            return <TextOut textAlign="center" style={ { marginLeft: '40px' } }>{ value }</TextOut>;
                                        }
                                    }
                                }
                } }
            />
        )
        : <></>;
});