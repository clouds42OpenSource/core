import { Box } from '@mui/system';

import { ContainedButton, SuccessButton } from 'app/views/components/controls/Button';
import { Props } from './types';

import css from './style.module.css';

export const AcceptOrReset = ({ cost, currency, acceptClick, resetClick }: Props) => {
    return (
        <Box className={ css.container }>
            <ContainedButton kind="warning" onClick={ resetClick }>Отмена</ContainedButton>
            <SuccessButton onClick={ acceptClick }>{ cost === 0 ? 'Применить' : `Купить (${ cost } ${ currency })` }</SuccessButton>
        </Box>
    );
};