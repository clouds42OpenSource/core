import { Box } from '@mui/system';
import { GetBillingService, GetServices } from 'app/api/endpoints/managingServiceSubscriptions/response';
import { FETCH_API } from 'app/api/useFetchApi';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { PageHeaderView } from 'app/views/components/_hoc/withHeader/PageHeaderView';
import { AutoInstallService } from 'app/views/modules/_common/shared/AutoInstallService';
import { AlpacaDescription } from 'app/views/modules/AlpacaMeet/views/alpacaDescription';
import { AlpacaTariffs } from 'app/views/modules/AlpacaMeet/views/alpacaTariffs';
import { PeriodInfo } from 'app/views/modules/ManagingServiceSubscriptions/views/PeriodInfo';
import { ServiceHeader } from 'app/views/modules/ManagingServiceSubscriptions/views/ServiceHeader';
import { ServiceOptions } from 'app/views/modules/ManagingServiceSubscriptions/views/ServiceOptions';
import { ServicesBillingTableView } from 'app/views/modules/ManagingServiceSubscriptions/views/ServicesBillingTable';
import { totalValue } from 'app/views/modules/ManagingServiceSubscriptions/views/ServicesBillingTable/functions';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { TServiceInformation } from 'app/web/api/ServicesProxy/response-dto';
import { RequestKind } from 'core/requestSender/enums';
import React, { memo, useCallback, useEffect, useState } from 'react';
import { Props } from './types';

const { getServices: getServicesResponse, getBillingService } = FETCH_API.MANAGING_SERVICE_SUBSCRIPTIONS;
const { receiveServiceInfo } = InterlayerApiProxy.getServicesApiProxy();

const ActiveServicesView = ({ id, setIsLoading, isActive, isAlpaca, floatMessage: { show } }: Props & FloatMessageProps) => {
    const [serviceData, setServiceData] = useState<GetServices | null>(null);
    const [icon, setIcon] = useState<TServiceInformation>();
    const [visibleLicenses, setVisibleLicenses] = useState(false);
    const [visibleUsers, setVisibleUsers] = useState(false);
    const [billingTableDataset, setBillingTableDataset] = useState<GetBillingService[]>([]);
    const [currency, setCurrency] = useState('');
    const [sum, setSum] = useState(0);

    /**
     * Get information about the service
     */
    const getServices = useCallback(() => {
        if (isActive) {
            getServicesResponse({ id })
                .then(response => {
                    if (response?.success) {
                        setServiceData(response.data);
                    } else {
                        show(MessageType.Error, response?.message);
                    }

                    setIsLoading(false);
                });
        }

        receiveServiceInfo(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, id)
            .then(response => setIcon(response?.Result));
    }, [id, isActive, setIsLoading, show]);

    useEffect(() => {
        getServices();
    }, [getServices]);

    /**
     * Get billing info and forming the result line
     */
    const getBilling = useCallback(() => {
        getBillingService({ id })
            .then(response => {
                if (response.success) {
                    const totalUsers = response.data?.reduce((prev, cur) => prev + cur.usedLicenses, 0);
                    const totalSum = response.data?.reduce((prev, cur) => prev + cur.amount, 0);
                    const totalSponsered = response.data?.reduce((prev, cur) => prev + cur.sponsoredLicenses, 0);
                    const totalSponsering = response.data?.reduce((prev, cur) => prev + cur.sponsoringLicenses, 0);
                    const total = totalValue(totalSum ?? 0, totalUsers ?? 0, totalSponsering ?? 0, totalSponsered ?? 0);

                    setSum(totalSum ?? 0);
                    setBillingTableDataset([...response.data ?? [], total]);
                    setCurrency(response.data?.length ? response.data[0].currency : '');
                } else {
                    show(MessageType.Error, response.message);
                }
            });
    }, [id, show]);

    useEffect(() => {
        getBilling();
    }, [getBilling]);

    /**
     * Set count services
     */
    const countServices = useCallback(() => {
        if (billingTableDataset) {
            const currentBillingTable = billingTableDataset.filter(item => item.id !== '');

            const countUsersServices = currentBillingTable.filter(item => item.billingType === 0).length;
            const countAccountServices = currentBillingTable.filter(item => item.billingType === 1).length;

            setVisibleUsers(!!countUsersServices);
            setVisibleLicenses(!!countAccountServices);
        }
    }, [billingTableDataset, setVisibleLicenses, setVisibleUsers]);

    useEffect(() => {
        countServices();
    }, [countServices]);

    return (
        <>
            { !isAlpaca && <PageHeaderView title={ `Сервис "${ serviceData?.name ?? icon?.ServiceName ?? '' }"` } /> }
            { serviceData && icon && isActive && (
                <>
                    <ServiceHeader
                        stringId={ icon.StringID }
                        iconSrc={ icon.ServiceIcon }
                        title={ serviceData.name }
                        instruction={ icon.InstructionURL }
                        id={ id }
                        isActive={ isActive }
                        date={ serviceData.serviceExpireDate }
                        description={ icon.ServiceShortDescription }
                        isAlpaca={ isAlpaca }
                    />
                    {
                        !isAlpaca && (
                            <>
                                <ServicesBillingTableView
                                    billingTableDataset={ billingTableDataset }
                                    currency={ currency }
                                />
                                <PeriodInfo
                                    id={ id }
                                    serviceStatus={ serviceData.serviceStatus }
                                    amountData={ serviceData.amountData }
                                    servicesName={ serviceData.name }
                                    date={ serviceData.serviceExpireDate }
                                    dateRent={ serviceData.rent1CExpireDate }
                                    totalSum={ sum }
                                    isDemoPeriod={ serviceData.isDemoPeriod }
                                    serviceDependsOnRent={ serviceData.serviceDependsOnRent }
                                    currency={ currency }
                                    refreshServiceInfo={ getServices }
                                    show={ show }
                                />
                                <AutoInstallService id={ id } />
                                <ServiceOptions
                                    id={ id }
                                    visibleLicenses={ visibleLicenses }
                                    visibleUsers={ visibleUsers }
                                    serviceData={ serviceData }
                                    canInstallExtension={ serviceData.canInstallExtension }
                                    billingTable={ billingTableDataset.filter(item => item.id !== '') }
                                />
                            </>
                        )
                    }
                    {
                        isAlpaca && (
                            <Box display="flex" flexDirection="column" gap="16px">
                                <AlpacaDescription />
                                <AlpacaTariffs />
                            </Box>
                        )
                    }
                </>
            ) }
            { icon && !isActive && (
                <ServiceHeader
                    stringId={ icon.StringID }
                    iconSrc={ icon.ServiceIcon }
                    title={ serviceData?.name ?? icon.ServiceName }
                    instruction={ icon.InstructionURL }
                    id={ id }
                    isActive={ isActive }
                    date={ serviceData?.serviceExpireDate ?? new Date() }
                    description={ icon.ServiceShortDescription }
                />
            ) }
        </>
    );
};

export const ActiveServices = memo(withFloatMessages(ActiveServicesView));