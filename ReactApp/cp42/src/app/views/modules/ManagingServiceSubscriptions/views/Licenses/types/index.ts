import { GetBillingService } from 'app/api/endpoints/managingServiceSubscriptions/response';

export type Props = {
    id: string;
    billingTable: GetBillingService[];
};