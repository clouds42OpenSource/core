import { Paper } from '@mui/material';
import { Link } from 'react-router-dom';

import { COLORS, DateUtility } from 'app/utils';
import { TextOut } from 'app/views/components/TextOut';
import { AppRoutes } from 'app/AppRoutes';
import { Props } from './types';

const paperStyle = {
    padding: '9.5px',
    fontSize: '14px',
    lineHeight: '1',
    boxShadow: 'none',
    border: '1px solid',
    flex: 1,
} as const;

export const DemoDate = ({ date, dateRent, isDemoPeriod, servicesName, serviceDependsOnRent, serviceStatus }: Props) => {
    const serviceExpireDate = new Date(date);
    const rentExpireDate = new Date(dateRent);

    if (serviceDependsOnRent && (rentExpireDate < new Date())) {
        return (
            <Paper
                sx={ {
                    ...paperStyle,
                    color: '#a94442',
                    backgroundColor: '#f2dede',
                    borderColor: '#ebccd1',
                } }
            >
                <TextOut fontWeight={ 700 } style={ { color: '#a94442' } }>Внимание! </TextOut>
                Работа с сервисом &quot;{ servicesName }&quot; возможна при активном сервисе &quot;Аренда 1С&quot;. Пожалуйста, <Link style={ { color: COLORS.link } } to={ AppRoutes.accountManagement.balanceManagement }>оплатите сервис</Link>.
            </Paper>
        );
    }

    if (serviceStatus.serviceIsLocked) {
        return (
            <Paper
                sx={ {
                    ...paperStyle,
                    color: '#a94442',
                    backgroundColor: '#f2dede',
                    borderColor: '#ebccd1',
                } }
            >
                <TextOut fontWeight={ 700 } style={ { color: '#a94442' } }>Внимание! </TextOut>
                <TextOut style={ { color: '#a94442' } }>{ serviceStatus.serviceLockReason }</TextOut>
            </Paper>
        );
    }

    if (isDemoPeriod) {
        return (
            <Paper
                sx={ {
                    ...paperStyle,
                    color: '#31708f',
                    backgroundColor: '#d9edf7',
                    borderColor: '#bce8f1',
                } }
            >
                Активирован демо период до { DateUtility.getDateWithDayMonthNameYear(serviceExpireDate) }
            </Paper>
        );
    }

    if (serviceStatus.promisedPaymentIsActive) {
        return (
            <Paper
                sx={ {
                    ...paperStyle,
                    color: '#31708f',
                    backgroundColor: '#d9edf7',
                    borderColor: '#bce8f1',
                } }
            >
                Внимание! У вас активирован обещанный платеж на сумму { serviceStatus.promisedPaymentSum }
                Пожалуйста, погасите задолженность до { DateUtility.getDateWithDayMonthNameYear(new Date(serviceStatus.promisedPaymentExpireDate)) } во избежание блокировки сервиса.
            </Paper>
        );
    }

    return (
        <Paper
            sx={ {
                ...paperStyle,
                color: '#31708f',
                backgroundColor: '#d9edf7',
                borderColor: '#bce8f1',
            } }
        >
            Использование сервиса возможно до { DateUtility.getDateWithDayMonthNameYear(serviceDependsOnRent ? rentExpireDate : serviceExpireDate) }
        </Paper>
    );
};