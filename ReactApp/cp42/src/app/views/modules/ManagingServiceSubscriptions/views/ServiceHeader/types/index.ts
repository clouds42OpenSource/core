export type Props = {
    iconSrc: string;
    title: string;
    instruction: string | null;
    id: string;
    isActive: boolean;
    date: Date;
    description?: string;
    stringId?: string;
    isAlpaca?: boolean;
};