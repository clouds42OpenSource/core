import { Box } from '@mui/system';
import { AppRoutes } from 'app/AppRoutes';
import { contextAccountId } from 'app/api';
import { PutServicesActivationDemo } from 'app/api/endpoints/managingServiceSubscriptions/response';
import { FETCH_API } from 'app/api/useFetchApi';
import { DateUtility } from 'app/utils';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { TextOut } from 'app/views/components/TextOut';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { BaseButton } from 'app/views/components/controls/Button/BaseButton';
import { Dialog } from 'app/views/components/controls/Dialog';
import { DemoDate } from 'app/views/modules/ManagingServiceSubscriptions/views/DemoDate';
import { getHref } from 'app/views/modules/ToPartners/helpers';
import { memo, useCallback, useState } from 'react';
import { useHistory } from 'react-router';
import { Props } from './types';

const currentDate = new Date();
const api = FETCH_API.MANAGING_SERVICE_SUBSCRIPTIONS;

export const PeriodInfo = memo(({ id, date, dateRent, isDemoPeriod, totalSum, servicesName, serviceDependsOnRent, currency, show, refreshServiceInfo, serviceStatus, amountData }: Props) => {
    const history = useHistory();

    const [isOpen, setIsOpen] = useState(false);
    const [applyStatus, setApplyStatus] = useState<PutServicesActivationDemo | null>(null);
    const [isLoading, setIsLoading] = useState(false);

    const isNeedMoney = !!(applyStatus && applyStatus.needMoney && applyStatus.needMoney > 0);

    const renderButton = useCallback(() => {
        return <BaseButton kind="primary" onClick={ () => setIsOpen(true) }>{ totalSum > 0 ? 'Оплатить подписку' : 'Подтвердить подписку' }</BaseButton>;
    }, [totalSum]);

    const renderButtonOnRent = useCallback(() => {
        if (serviceDependsOnRent) {
            if (currentDate <= new Date(dateRent)) {
                return renderButton();
            }
        } else if (currentDate >= new Date(date)) {
            return renderButton();
        }
    }, [date, dateRent, renderButton, serviceDependsOnRent]);

    const applyDemo = (usePromisePayment = false) => {
        setIsLoading(true);
        api.putServicesActivationDemo({
            accountId: contextAccountId(),
            serviceId: id,
            usePromisePayment
        }).then(response => {
            if (response?.success && response.data) {
                if (response.data.complete) {
                    setIsOpen(false);
                    refreshServiceInfo();
                } else {
                    setApplyStatus(response.data);
                }
            } else {
                show(MessageType.Error, response?.message);
            }

            setIsLoading(false);
        });
    };

    return (
        <>
            { isLoading && <LoadingBounce /> }
            <Box display="flex" flexDirection="row" justifyContent="space-between" alignItems="center" gap="16px" marginY="16px">
                <DemoDate
                    serviceStatus={ serviceStatus }
                    date={ date }
                    dateRent={ dateRent }
                    isDemoPeriod={ isDemoPeriod }
                    servicesName={ servicesName }
                    serviceDependsOnRent={ serviceDependsOnRent }
                />
                { isDemoPeriod && renderButtonOnRent() }
            </Box>
            <Dialog
                title={
                    isDemoPeriod && applyStatus && !isNeedMoney
                        ? `Оплата сервиса "${ servicesName }"`
                        : isNeedMoney
                            ? 'Внимание'
                            : `Подтверждение подписки сервиса "${ servicesName }"`
                }
                isTitleSmall={ true }
                isOpen={ isOpen }
                dialogWidth={ applyStatus ? 'sm' : 'xs' }
                dialogVerticalAlign="center"
                buttons={ applyStatus === null
                    ? [
                        {
                            content: 'Отмена',
                            kind: 'default',
                            onClick: () => setIsOpen(false)
                        },
                        {
                            content: amountData.servicePartialAmount && amountData.servicePartialAmount > 0 ? `Оплатить (${ amountData.servicePartialAmount } ${ currency })` : 'Подтвердить подписку',
                            kind: 'primary',
                            onClick: () => applyDemo()
                        }
                    ]
                    : applyStatus.canUsePromisePayment
                        ? [
                            {
                                content: 'Отмена',
                                kind: 'default',
                                onClick: () => setIsOpen(false)
                            },
                            {
                                content: `Взять обещанный платеж (${ applyStatus.needMoney } ${ currency })`,
                                kind: 'primary',
                                onClick: () => applyDemo(true),
                            },
                            {
                                content: `Пополнить счет (${ applyStatus.needMoney } ${ currency })`,
                                kind: 'primary',
                                onClick: () => history.push(`${ AppRoutes.accountManagement.replenishBalance }?customPayment.amount=${ applyStatus.needMoney }&customPayment.description=Применение+подписки+демо+сервиса`),
                            }
                        ]
                        : [
                            {
                                content: 'Отмена',
                                kind: 'default',
                                onClick: () => setIsOpen(false)
                            },
                            {
                                content: `Пополнить счет (${ applyStatus.needMoney } ${ currency })`,
                                kind: 'primary',
                                onClick: () => history.push(`${ AppRoutes.accountManagement.replenishBalance }?customPayment.amount=${ applyStatus.needMoney }&customPayment.description=Применение+подписки+демо+сервиса`),
                            }
                        ]
                }
                onCancelClick={ () => setIsOpen(false) }
            >
                <>
                    { isNeedMoney
                        ? (
                            <Box display="flex" flexDirection="column" gap="4px" alignItems="center">
                                <img src={ getHref('warning') } alt="warning" height={ 100 } width={ 100 } />
                                <TextOut>
                                    Уважаемый пользователь, для активации сервиса у Вас недостаточно средств в размере <TextOut fontWeight={ 700 }>{ applyStatus.needMoney } { currency }. </TextOut>
                                    Вы можете воспользоваться услугой <TextOut fontWeight={ 700 }>&quot;Обещанный платеж&quot;</TextOut>, Вы должны будете погасить задолжность в течение 7 дней.
                                </TextOut>
                            </Box>
                        )
                        : (
                            <Box display="flex" flexDirection="column" gap="4px">
                                { isDemoPeriod &&
                                <TextOut>Этим действием вы подтверждаете завершение демо периода.</TextOut>
                                }
                                { amountData.servicePartialAmount && amountData.servicePartialAmount > 0 &&
                                <TextOut>Сумма <TextOut fontWeight={ 700 }>{ amountData.servicePartialAmount } { currency }.</TextOut> будет списана с баланса сейчас.</TextOut>
                                }
                                <TextOut>
                                    Сервис <TextOut fontWeight={ 700 }>&quot;{ servicesName }&quot;</TextOut> будет продлен до {serviceDependsOnRent ? DateUtility.getDateWithMonthName(new Date(dateRent)) : DateUtility.getDateWithMonthName(DateUtility.getSomeMonthAgoDate(-1)) }
                                    {serviceDependsOnRent ? <span>, согласно даты списания за сервис <TextOut fontWeight={ 700 }>Аренда 1С</TextOut></span> : ''}.
                                </TextOut>
                                { applyStatus !== null &&
                                <TextOut>Сумма { applyStatus.needMoney } { currency } будет списана с баланса сейчас.</TextOut>
                                }
                                <TextOut>Ежемесячный платеж за сервис <TextOut fontWeight={ 700 }>"{ servicesName }"</TextOut> составляет <TextOut fontWeight={ 700 }>{ totalSum } { currency }</TextOut>.</TextOut>
                            </Box>
                        )
                    }
                </>
            </Dialog>
        </>
    );
});