import { Box, CircularProgress, ClickAwayListener, Fade, Link, Popper, PopperPlacementType, Typography } from '@mui/material';
import { GetServicesDatabaseResponse } from 'app/api/endpoints/managingServiceSubscriptions/response';
import { FETCH_API } from 'app/api/useFetchApi';
import { TextOut } from 'app/views/components/TextOut';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { SuccessButton } from 'app/views/components/controls/Button';
import React, { memo, useCallback, useEffect, useState } from 'react';
import { Redirect } from 'react-router-dom';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import style from './style.module.css';
import { Props } from './types';

const { getServicesActivation, getServicesDatabase } = FETCH_API.MANAGING_SERVICE_SUBSCRIPTIONS;

const currentData = new Date();

const { REACT_WP_API_URL, REACT_WP_PROMO_URL } = import.meta.env;

export const ServiceHeader = withFloatMessages(memo(({ stringId, description, iconSrc, title, instruction, id, isActive, date, isAlpaca, floatMessage: { show } }: Props & FloatMessageProps) => {
    const [anchorEl, setAnchorEl] = useState<HTMLButtonElement | null>(null);
    const [open, setOpen] = useState(false);
    const [placement, setPlacement] = useState<PopperPlacementType>();
    const [databasesToRun, setDatabasesToRun] = useState<GetServicesDatabaseResponse | null>(null);
    const [isLoading, setIsLoading] = useState(false);
    const [reload, setReload] = useState(false);

    const getDatabaseToRun = useCallback(() => {
        setIsLoading(true);

        getServicesDatabase({ id }).then(response => {
            if (response?.success) {
                setDatabasesToRun(response);
            }

            setIsLoading(false);
        });
    }, [id]);

    useEffect(() => {
        if (isActive) {
            getDatabaseToRun();
        }
    }, [getDatabaseToRun, isActive]);

    const handleClick = (newPlacement: PopperPlacementType) => (event: React.MouseEvent<HTMLButtonElement>) => {
        if (!open) {
            getDatabaseToRun();
        }

        setAnchorEl(event.currentTarget);
        setOpen(prev => placement !== newPlacement || !prev);
        setPlacement(newPlacement);
    };

    const tryService = () => {
        setIsLoading(true);
        getServicesActivation({ id }).then(response => {
            if (response && response.success) {
                setReload(true);
            } else {
                show(MessageType.Error, response?.message ?? 'Не удалось активировать пробный период');
            }

            setIsLoading(false);
        });
    };

    if (reload) {
        return <Redirect to={ window.location.pathname } />;
    }

    return (
        <>
            { isLoading && <LoadingBounce /> }
            { isActive
                ? (
                    <Box display="flex" flexDirection="row" gap="32px" mb="32px">
                        <img src={ iconSrc } alt="icon service" loading="lazy" className={ style.img } />
                        <Box display="flex" flexDirection="column" gap="16px">
                            <TextOut fontSize={ 14 }>{ description }</TextOut>
                            { instruction && !isAlpaca && <Link href={ instruction } target="_blank" rel="noreferrer" className={ style.link }>Инструкция</Link> }
                            { stringId && (
                                <Link
                                    rel="noreferrer"
                                    target="_blank"
                                    href={
                                        isAlpaca
                                            ? `${ REACT_WP_PROMO_URL }/solutions/alpacameet-telegram-meeting-recording-transcription/#кейсИИ`
                                            : `https://${ REACT_WP_API_URL && REACT_WP_API_URL.includes('beta') ? 'wpbeta.' : '' }42clouds.com/ru-ru/market/service/${ stringId }`
                                    }
                                    className={ style.link }
                                >
                                    { isAlpaca ? 'Ссылка на сервис' : 'Маркет 42' }
                                </Link>
                            ) }
                            { databasesToRun && databasesToRun.data && databasesToRun.data.accountDatabasesOnDelimitersToRun.length > 0 &&
                            <Box display="flex" flexDirection="row" alignItems="center" gap="16px">
                                <ClickAwayListener onClickAway={ () => setOpen(false) }>
                                    <div>
                                        <SuccessButton
                                            onClick={ handleClick('bottom-start') }
                                            isEnabled={ currentData <= new Date(date) }
                                            className={ style.start }
                                        >
                                            Запустить { title }
                                        </SuccessButton>
                                        <Popper open={ open } anchorEl={ anchorEl } placement={ placement } transition={ true } className={ style.dropdown }>
                                            { ({ TransitionProps }) => (
                                                <Fade { ...TransitionProps }>
                                                    <Box>
                                                        { databasesToRun && !isLoading && databasesToRun.data?.accountDatabasesOnDelimitersToRun.map(item =>
                                                            <Typography sx={ { p: '8px' } } key={ item.id }>
                                                                <Link href={ item.webPublishPath } target="_blank" rel="noreferrer" style={ { width: '100%' } }>{ item.caption }</Link>
                                                            </Typography>
                                                        ) }
                                                    </Box>
                                                </Fade>
                                            ) }
                                        </Popper>
                                    </div>
                                </ClickAwayListener>
                                { isLoading && <CircularProgress size={ 25 } /> }
                            </Box>
                            }
                        </Box>
                    </Box>
                )
                : (
                    <Box display="flex" flexDirection="row" gap="32px" mb="32px">
                        <img src={ iconSrc } alt="icon service" loading="lazy" className={ style.img } />
                        <Box display="flex" flexDirection="column" gap="16px">
                            <TextOut fontSize={ 14 }>{ description }</TextOut>
                            <SuccessButton onClick={ tryService } style={ { width: 'fit-content' } }>Попробовать бесплатно</SuccessButton>
                        </Box>
                    </Box>
                )
            }
        </>
    );
}));