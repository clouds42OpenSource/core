import { ServiceStatus } from 'app/api/endpoints/managingServiceSubscriptions/response';

export type Props = {
    date: Date;
    dateRent: Date;
    isDemoPeriod: boolean;
    servicesName: string;
    serviceDependsOnRent: boolean;
    serviceStatus: ServiceStatus;
};