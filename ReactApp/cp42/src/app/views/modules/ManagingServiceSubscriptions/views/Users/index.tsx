import { Box, TextField } from '@mui/material';
import { AppRoutes } from 'app/AppRoutes';
import { Services } from 'app/api/endpoints/managingServiceSubscriptions/request';
import { GetBillingService, GetServicesUsers, PutServicesApply, PutServicesPrecalculatedCost, Sponsorship } from 'app/api/endpoints/managingServiceSubscriptions/response';
import { FETCH_API } from 'app/api/useFetchApi';
import { SortingKind } from 'app/common/enums';
import { localStorageHelper } from 'app/common/helpers/LocalStorageHelper';
import { COLORS } from 'app/utils';
import { TextOut } from 'app/views/components/TextOut';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { BaseButton } from 'app/views/components/controls/Button/BaseButton';
import { Dialog } from 'app/views/components/controls/Dialog';
import CustomizedSwitches from 'app/views/components/controls/Switch/views/SwitchViews';
import { TableCellResponseView } from 'app/views/components/controls/Table/views/TableResponsiveView/partial-views/TableCellResponseView';
import { AcceptOrReset } from 'app/views/modules/ManagingServiceSubscriptions/views/AcceptOrReset';
import { CommonTableWithFilter, SelectDataInitiator } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { SelectDataCommonDataModel, SelectDataMetadataResponseDto, SortingDataModel } from 'app/web/common';
import React, { ChangeEvent, useCallback, useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import ClearIcon from '@mui/icons-material/Clear';

import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { PayDialog } from 'app/views/modules/ManagingServiceSubscriptions/modules/PayDialog';
import { Redirect } from 'react-router-dom';
import { SuccessButton } from 'app/views/components/controls/Button';
import styles from './style.module.css';
import { Props, SortingSwitch } from './types';

const api = FETCH_API.MANAGING_SERVICE_SUBSCRIPTIONS;
const ctxGuid = localStorageHelper.getContextAccountId() ?? '';

export const Users = withFloatMessages(({ id, billingTable, isDemo, floatMessage: { show } }: Props & FloatMessageProps) => {
    const [users, setUsers] = useState<SelectDataMetadataResponseDto<GetServicesUsers> | null>(null);
    const [defaultUsers, setDefaultUsers] = useState<SelectDataMetadataResponseDto<GetServicesUsers> | null>(null);
    const [usersBilling, setUsersBilling] = useState<GetBillingService[]>(billingTable);
    const [currentServices, setCurrentServices] = useState<Services>();
    const [precalculatedCostInfo, setPrecalculatedCostInfo] = useState<PutServicesPrecalculatedCost[]>([]);
    const [errorMessage, setErrorMessage] = useState('');
    const [sponsorship, setSponsorship] = useState('');
    const [sortingSwitch, setSortingSwitch] = useState<SortingSwitch[]>([]);
    const [searchUser, setSearchUser] = useState('');

    const [showAcceptOrReset, setShowAcceptOrReset] = useState(false);
    const [showResetDialog, setShowResetDialog] = useState(false);
    const [showAcceptDialog, setShowAcceptDialog] = useState(false);
    const [showErrorDialog, setShowErrorDialog] = useState(false);
    const [showSponsor, setShowSponsor] = useState(false);

    const [isOpenPayDialog, setIsOpenPayDialog] = useState(false);
    const [currentServicesApply, setCurrentServicesApply] = useState<PutServicesApply | null>(null);
    const [isLoading, setIsLoading] = useState(false);
    const [isNeedRefresh, setIsNeedRefresh] = useState(false);

    const history = useHistory();

    const setDefaultServicesUsers = useCallback(() => {
        api.getServicesUsers(
            { id },
            {
                accountId: localStorageHelper.getContextAccountId() ?? '',
                orderBy: 'accountUserName.asc'
            }
        ).then(response => {
            setUsers(response.data);
            setDefaultUsers(response.data);
        });
    }, [id]);

    useEffect(() => {
        setDefaultServicesUsers();
    }, [setDefaultServicesUsers]);

    const getSortQuery = (args: SortingDataModel) => {
        setSortingSwitch(sortingSwitch.map(item => {
            return {
                id: item.id,
                sortKind: undefined
            };
        }));
        switch (args.fieldName) {
            case 'accountUserName':
                return `accountUserName.${ args.sortKind ? 'desc' : 'asc' }`;
            default:
                return '';
        }
    };

    const onDataSelect = (args: SelectDataCommonDataModel<GetServicesUsers>, _: string | null, initiator: SelectDataInitiator) => {
        if (initiator !== 'initial') {
            api.getServicesUsers(
                { id },
                {
                    accountId: localStorageHelper.getContextAccountId() ?? '',
                    orderBy: args.sortingData ? getSortQuery(args.sortingData) : '',
                    'filter.accountUserName': searchUser,
                    pageNumber: args.pageNumber
                }
            ).then(response => setUsers(response.data));
        }
    };

    const onSearchUser = () => {
        api.getServicesUsers(
            { id },
            {
                accountId: localStorageHelper.getContextAccountId() ?? '',
                orderBy: 'accountUserName.asc',
                'filter.accountUserName': searchUser,
            }
        ).then(response => setUsers(response.data));
    };

    const getPrecalculateInfo = (billingServiceTypeId: string, status: boolean, newUserServices: Services[], subject: string, sponsorshipInfo?: Sponsorship) => {
        const currentUserServices: Services = {
            billingServiceTypeId,
            status,
            subject,
            sponsorship: sponsorshipInfo
        };

        setCurrentServices(currentUserServices);

        api.putServicesPrecalculatedCost({
            accountId: ctxGuid,
            changedLicense: currentUserServices,
            billingServiceId: id,
            changedUsers: newUserServices,
        }).then(response => {
            if (response.success) {
                setPrecalculatedCostInfo(response.data ?? []);
            } else {
                show(MessageType.Error, response.message);
            }
        });
    };

    const switchStatus = (id: string, serviceId: string, statusValue?: boolean) => {
        if (!showAcceptOrReset) {
            setShowAcceptOrReset(true);
        }

        if (users) {
            const newRecords = users.records.map(item => {
                if (item.accountUserId === id) {
                    const status = item.services.includes(serviceId);
                    const sponsorshipItem = item.sponsorship;
                    const changeItem: Services = {
                        status: statusValue ? !statusValue : !status,
                        billingServiceTypeId: serviceId,
                        subject: item.accountUserId,
                        sponsorship: sponsorshipItem
                    };

                    if (status) {
                        if (precalculatedCostInfo.find(service => service.billingServiceTypeId === changeItem.billingServiceTypeId)) {
                            const newServices: Services[] = precalculatedCostInfo.flatMap(serviceItem => {
                                if (serviceItem.billingServiceTypeId === changeItem.billingServiceTypeId && serviceItem.subject === changeItem.subject) {
                                    return [];
                                }

                                return serviceItem;
                            });

                            newServices.push(changeItem);

                            getPrecalculateInfo(changeItem.billingServiceTypeId, true, newServices, item.accountUserId, sponsorshipItem);
                        } else {
                            const newServices = [...precalculatedCostInfo, changeItem];
                            getPrecalculateInfo(changeItem.billingServiceTypeId, changeItem.status, newServices, item.accountUserId, sponsorshipItem);
                        }

                        return {
                            accountUserId: item.accountUserId,
                            sponsorship: item.sponsorship,
                            services: item.services.filter(servicesItem => servicesItem !== serviceId),
                            accountUserName: item.accountUserName,
                            sponsoredServiceTypesByMyAccount: item.sponsoredServiceTypesByMyAccount
                        };
                    }
                    if (precalculatedCostInfo.length === 0) {
                        const newServices = [...precalculatedCostInfo, changeItem];
                        getPrecalculateInfo(changeItem.billingServiceTypeId, false, newServices, item.accountUserId, sponsorshipItem);
                    } else {
                        const newServices: Services[] = precalculatedCostInfo.flatMap(newItem => {
                            if (newItem.billingServiceTypeId === changeItem.billingServiceTypeId && changeItem.subject === newItem.subject) {
                                return [];
                            }

                            return newItem;
                        });
                        newServices.push(changeItem);
                        getPrecalculateInfo(changeItem.billingServiceTypeId, changeItem.status, newServices, item.accountUserId, sponsorshipItem);
                    }
                }

                return item;
            });

            setUsers({ metadata: users.metadata, records: newRecords });
        }
    };

    const setDefaultValue = () => {
        setDefaultServicesUsers();
        setShowResetDialog(false);
        setShowAcceptOrReset(false);
        setPrecalculatedCostInfo([]);
        setUsersBilling(billingTable);
    };

    const apply = (isPromise?: boolean) => {
        if (currentServices) {
            setIsLoading(true);

            api.putServicesApply({
                accountId: localStorageHelper.getContextAccountId() ?? '',
                billingServiceId: id,
                changedLicenses: [],
                changedUsers: [...precalculatedCostInfo],
                usePromisePayment: isPromise ?? false
            }).then(response => {
                if (!response.success) {
                    show(MessageType.Error, response.message);
                } else if (response.data?.isComplete) {
                    setShowAcceptDialog(false);
                    setIsNeedRefresh(true);
                } else if (response.data?.notEnoughMoney && response.data.notEnoughMoney > 0) {
                    setIsOpenPayDialog(true);
                    setCurrentServicesApply(response.data);
                }

                setIsLoading(false);
            });
        }
    };

    useEffect(() => {
        if (precalculatedCostInfo && precalculatedCostInfo.length > 0) {
            precalculatedCostInfo.map(item => {
                if (item.errors !== null && item.errors.length > 0 && item.status) {
                    setErrorMessage(item.errors[0].description);
                    setShowErrorDialog(true);
                }

                return item;
            });
        }
    }, [precalculatedCostInfo]);

    const checkSponsorship = () => {
        api.getSponsorship(id, { email: sponsorship })
            .then(response => {
                if (response.success) {
                    if (response.data !== null) {
                        setUsers({
                            metadata: users!.metadata,
                            records: [response.data, ...users!.records]
                        });
                    }

                    setShowSponsor(false);
                } else {
                    show(MessageType.Error, response.message);
                }
            });
    };

    const sorting = (id: string) => {
        const dropSwitch = document.getElementsByClassName('fa-sort-asc');

        if (dropSwitch.length > 0 && dropSwitch[0].classList.contains('fa-sort-asc')) {
            dropSwitch[0].classList.add('fa-sort');
            dropSwitch[0].classList.remove('fa-sort-asc');
        }

        if (sortingSwitch.length > 0) {
            const newSort = [...sortingSwitch, { id, sortKind: SortingKind.Desc }];

            const sort = newSort.map(item => {
                if (item.id === id) {
                    return {
                        id: item.id,
                        sortKind: item.sortKind === SortingKind.Asc ? SortingKind.Desc : SortingKind.Asc
                    };
                }

                return {
                    id: item.id,
                    sortKind: undefined
                };
            });

            if (users && users.metadata && users.records) {
                setSortingSwitch(sort);
                setUsers({
                    metadata: users.metadata,
                    records: sortingSwitch.find(item => item.id === id)?.sortKind === SortingKind.Asc
                        ? users.records.sort((a, b) => a.services.includes(id) > b.services.includes(id) ? 1 : -1)
                        : users.records.sort((a, b) => a.services.includes(id) < b.services.includes(id) ? 1 : -1)
                });
            }
        } else {
            setSortingSwitch([{ id, sortKind: SortingKind.Asc }]);
            if (users && users.metadata && users.records) {
                setUsers({
                    metadata: users.metadata,
                    records: users.records.sort((a, b) => a.services.includes(id) < b.services.includes(id) ? 1 : -1)
                });
            }
        }
    };

    const removeSponsorshipUser = (idSponsorshipUser: string) => {
        setUsers({
            metadata: users!.metadata,
            records: users!.records.filter(item => item.accountUserId !== idSponsorshipUser)
        });
    };

    const onCancelClickWarningDialogHandler = () => {
        setShowErrorDialog(false);
        switchStatus(currentServices!.subject, currentServices!.billingServiceTypeId, true);
    };

    if (isNeedRefresh) {
        return <Redirect to={ window.location.pathname } />;
    }

    return users
        ? <>
            <PayDialog applyStatus={ currentServicesApply } isOpen={ isOpenPayDialog } setIsOpen={ setIsOpenPayDialog } />
            { isLoading && <LoadingBounce /> }
            <Box display="flex" gap="16px">
                <TextField
                    id="searchUser"
                    placeholder="Поиск"
                    value={ searchUser }
                    className={ styles.search }
                    onChange={ (event: ChangeEvent<HTMLInputElement>) => setSearchUser(event.target.value) }
                />
                <SuccessButton onClick={ onSearchUser }>Найти</SuccessButton>
            </Box>
            <CommonTableWithFilter
                uniqueContextProviderStateId="managingServiceContextProviderStateId"
                tableProps={ {
                    dataset: users.records,
                    keyFieldName: 'accountUserId',
                    sorting: {
                        sortFieldName: 'accountUserName',
                        sortKind: SortingKind.Asc
                    },
                    fieldsView: {
                        accountUserName: {
                            caption: 'Пользователи',
                            isSortable: true,
                            style: {
                                lineHeight: '35px',
                            },
                            format: (value, data) => {
                                if (data.sponsorship.i) {
                                    return (<TextOut className={ styles.text }>
                                        { value }
                                        <TextOut fontSize={ 10 } style={ { color: COLORS.error } } className={ styles.text }>
                                            Спонсирую ({ data.sponsorship.label })
                                            { defaultUsers?.records.filter(item => item.accountUserId === data.accountUserId).length === 0 &&
                                                <ClearIcon style={ { width: '12px', height: '12px', cursor: 'pointer' } } onClick={ () => removeSponsorshipUser(data.accountUserId) } />
                                            }
                                        </TextOut>
                                            </TextOut>);
                                }

                                if (data.sponsorship.me) {
                                    return (<TextOut className={ styles.text }>
                                        { value }
                                        <TextOut fontSize={ 10 } style={ { color: COLORS.main } } className={ styles.text }>
                                            Спонсируется ({ data.sponsorship.label })
                                        </TextOut>
                                            </TextOut>);
                                }

                                return <TextOut className={ styles.text }>{ value }</TextOut>;
                            }
                        },
                        accountUserId: {
                            caption: usersBilling.map(item => {
                                return (<TableCellResponseView
                                    key={ item.id }
                                    isHeader={ true }
                                    isSortable={ true }
                                    sortKind={
                                        sortingSwitch.length > 0
                                            ? sortingSwitch.find(sortingItem => sortingItem.id === item.id)?.sortKind
                                            : undefined
                                    }
                                    style={ { cursor: 'pointer' } }
                                    onHeaderSortClick={ () => sorting(item.id) }
                                >
                                    { item.name }
                                        </TableCellResponseView>);
                            }),
                            style: {
                                display: 'grid',
                                gridTemplateColumns: `repeat(${ usersBilling.length }, minmax(0, 1fr))`,
                                gap: '8px',
                                alignItems: 'center',
                                height: '35px',
                            },
                            format: (value, data) => {
                                return usersBilling.map(item => {
                                    const currentSwitchData = (precalculatedCostInfo ?? []).find(costInfo => costInfo.subject === value && costInfo.billingServiceTypeId === item.id);

                                    return (<Box display="flex" alignItems="center" gap="8px" flex="1 1 auto" key={ item.id }>
                                        <CustomizedSwitches
                                            key={ item.id }
                                            onChange={ () => switchStatus(value, item.id, currentSwitchData?.status) }
                                            checked={ currentSwitchData?.status ?? data.services.includes(item.id) }
                                        />
                                        { currentSwitchData && currentSwitchData.cost > 0 &&
                                            <TextOut>{ currentSwitchData.cost } { usersBilling[0].currency ?? '' }</TextOut>
                                        }
                                            </Box>);
                                });
                            }
                        }
                    },
                    pagination: {
                        currentPage: users.metadata.pageNumber,
                        pageSize: users.metadata.pageSize,
                        recordsCount: users.metadata.totalItemCount,
                        totalPages: users.metadata.pageCount
                    }
                } }
                onDataSelect={ onDataSelect }
            />
            { !isDemo && <BaseButton kind="primary" onClick={ () => setShowSponsor(true) }>Спонсировать</BaseButton> }
            { showAcceptOrReset &&
                <AcceptOrReset
                    cost={ precalculatedCostInfo?.reduce((prev, curr) => prev + curr.cost, 0) ?? 0 }
                    currency={ usersBilling[0].currency ?? '' }
                    acceptClick={ () => setShowAcceptDialog(true) }
                    resetClick={ () => setShowResetDialog(true) }
                />
            }
            <Dialog
                title="Подтвердите отмену"
                isTitleSmall={ true }
                isOpen={ showResetDialog }
                dialogWidth="xs"
                dialogVerticalAlign="center"
                onCancelClick={ () => setShowResetDialog(false) }
                buttons={
                    [
                        {
                            content: 'Да',
                            kind: 'default',
                            onClick: setDefaultValue,
                            variant: 'text'
                        },
                        {
                            content: 'Нет',
                            kind: 'warning',
                            onClick: () => setShowResetDialog(false)
                        }
                    ]
                }
            >
                <TextOut>Вы уверены что хотите сбросить все изменения?</TextOut>
            </Dialog>
            <Dialog
                title="Подтвердите операцию"
                isTitleSmall={ true }
                isOpen={ showAcceptDialog }
                dialogWidth="xs"
                dialogVerticalAlign="center"
                onCancelClick={ () => setShowAcceptDialog(false) }
                buttons={
                    [
                        {
                            content: 'Отмена',
                            kind: 'default',
                            onClick: () => setShowAcceptDialog(false),
                            variant: 'text'
                        },
                        {
                            content: 'Продолжить',
                            kind: 'success',
                            onClick: () => apply()
                        }
                    ]
                }
            >
                { precalculatedCostInfo.length > 0 && precalculatedCostInfo.filter(info => info.status).length === 0 &&
                    <TextOut>Внимание! При отключении всех пользователей сервис будет удален из ВСЕХ информационных баз! </TextOut>
                }
                { (precalculatedCostInfo?.reduce((prev, curr) => prev + curr.cost, 0) ?? 0) > 0
                    ? <TextOut>Нажмите «Продолжить», чтобы совершить покупку на выбранные Вами услуги.</TextOut>
                    : <TextOut>Нажмите «Продолжить», что бы применить изменения.</TextOut>
                }
            </Dialog>
            <Dialog
                title="Внимание"
                isTitleSmall={ true }
                isOpen={ showErrorDialog }
                dialogWidth="xs"
                dialogVerticalAlign="center"
                onCancelClick={ onCancelClickWarningDialogHandler }
                buttons={
                    [
                        {
                            content: 'Закрыть',
                            kind: 'default',
                            onClick: onCancelClickWarningDialogHandler,
                            variant: 'text'
                        },
                        {
                            content: 'Подключить Аренду 1С',
                            kind: 'primary',
                            onClick: () => history.push(AppRoutes.services.rentReference)
                        }
                    ]
                }
            >
                <TextOut>
                    <div dangerouslySetInnerHTML={ { __html: errorMessage } } />
                </TextOut>
            </Dialog>
            <Dialog
                title="Спонсирование пользователя"
                isTitleSmall={ true }
                isOpen={ showSponsor }
                dialogWidth="xs"
                dialogVerticalAlign="center"
                onCancelClick={ () => setShowSponsor(false) }
                buttons={
                    [
                        {
                            content: 'Отмена',
                            kind: 'default',
                            onClick: () => setShowSponsor(false),
                            variant: 'text'
                        },
                        {
                            content: 'Спонсировать',
                            kind: 'primary',
                            onClick: () => checkSponsorship()
                        }
                    ]
                }
            >
                <Box display="flex" flexDirection="column" gap="16px" alignItems="center">
                    <TextOut>Вы можете проспонсировать подключение к сервису для пользователя внешнего аккаунта</TextOut>
                    <TextField
                        InputLabelProps={ { shrink: true } }
                        label="Введите e-mail пользователя"
                        size="small"
                        value={ sponsorship }
                        fullWidth={ true }
                        onChange={ (event: ChangeEvent<HTMLInputElement>) => setSponsorship(event.target.value) }
                    />
                </Box>
            </Dialog>
        </>
        : <></>;
});