import { Box } from '@mui/material';
import { AppRoutes } from 'app/AppRoutes';
import { PostExtensionsRequest } from 'app/api/endpoints/managingServiceSubscriptions/request';
import { GetExtensions, GetServicesCompatibleTemplatesResponse } from 'app/api/endpoints/managingServiceSubscriptions/response';
import { FETCH_API } from 'app/api/useFetchApi';
import { localStorageHelper } from 'app/common/helpers/LocalStorageHelper';
import { TimerHelper } from 'app/common/helpers/TimerHelper';
import { COLORS, DateUtility } from 'app/utils';
import { TextOut } from 'app/views/components/TextOut';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import CustomizedSwitches from 'app/views/components/controls/Switch/views/SwitchViews';
import { HomeSvgSelector } from 'app/views/components/svgGenerator/HomeSvgSelector';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { SelectDataCommonDataModel, SelectDataMetadataResponseDto } from 'app/web/common';
import { memo, useCallback, useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { ExtensionState } from 'app/api/endpoints/managingServiceSubscriptions/enum';
import style from './style.module.css';
import { Props } from './types';

const api = FETCH_API.MANAGING_SERVICE_SUBSCRIPTIONS;

const InstallingView = ({ id, setCount, canInstallExtension, name, floatMessage: { show }, isNeedToDelete }: Props & FloatMessageProps) => {
    const [extension, setExtension] = useState<SelectDataMetadataResponseDto<GetExtensions> | null>(null);
    const [templates, setTemplates] = useState<GetServicesCompatibleTemplatesResponse | null>(null);
    /**
     * State change status
     */
    const [status, setStatus] = useState<string[]>([]);

    /**
     * Get info extensions
     */
    const getExtension = useCallback((args?: SelectDataCommonDataModel<NonNullable<unknown>>) => {
        api.getExtensions(
            {
                id,
                accountId: localStorageHelper.getContextAccountId()!,
                pageSize: args?.pageSize,
                pageNumber: args?.pageNumber
            }
        ).then(response => {
            if (response.success) {
                if (response.data && response.data.records.length > 0) {
                    setExtension(response.data);

                    response.data.records.forEach(record => {
                        if ((record.extensionState === ExtensionState.ProcessingInstall || record.extensionState === ExtensionState.ProcessingDelete) && !status.includes(record.id)) {
                            setStatus([...status, record.id]);
                            checkStatus(id, record.id);
                        }
                    });
                } else {
                    setCount(0);
                    if (canInstallExtension) {
                        api.getServicesCompatibleTemplates({
                            id
                        }).then(templatesResponse => {
                            if (templatesResponse?.success) {
                                setTemplates(templatesResponse);
                            } else {
                                show(MessageType.Error, templatesResponse?.message);
                            }
                        });
                    }
                }
            } else {
                show(MessageType.Error, response.message);
            }
        });
    }, [canInstallExtension, id, setCount, show]);

    useEffect(() => {
        getExtension();
    }, [getExtension]);

    /**
     * Check status extension
     */
    const checkStatus = useCallback((idService: string, accountDatabaseId: string) => {
        setTimeout(() => {
            api.getExtensionsState({
                id: idService,
                accountDatabaseId
            }).then(response => {
                if (response.success && response.data) {
                    if (response.data.extensionDatabaseStatus === ExtensionState.ProcessingInstall || response.data.extensionDatabaseStatus === ExtensionState.ProcessingDelete) {
                        checkStatus(idService, accountDatabaseId);
                    } else {
                        getExtension();
                        setStatus(status.filter(item => item !== idService));
                    }
                } else {
                    show(MessageType.Error, response.message);
                }
            });
        }, TimerHelper.waitTimeRefreshDatabaseState);
    }, [getExtension, show, status]);

    /**
     * Enable or disable extension
     */
    const extensionChange = useCallback((isInstalled: boolean, requestArgs: PostExtensionsRequest) => {
        if (isInstalled) {
            void api.deleteExtensions({
                id: requestArgs.serviceId,
                accountDatabaseId: requestArgs.databaseIds.join('')
            });
        } else {
            void api.postExtensions(requestArgs);
        }

        if (!isNeedToDelete) {
            checkStatus(requestArgs.serviceId, requestArgs.databaseIds.join(''));
        }
    }, [checkStatus, isNeedToDelete]);

    useEffect(() => {
        if (isNeedToDelete && extension) {
            extension.records.forEach(ext => {
                if (ext.isInstalled) {
                    extensionChange(true, { serviceId: id, databaseIds: [ext.id] });
                }
            });
        }
    }, [extension, extensionChange, id, isNeedToDelete]);

    return (
        <div>
            {
                extension && extension.records.length > 0 && !isNeedToDelete
                    ? (
                        <CommonTableWithFilter
                            uniqueContextProviderStateId="installingProviderStateId"
                            className={ style.table }
                            tableProps={ {
                                dataset: extension.records,
                                keyFieldName: 'id',
                                isResponsiveTable: true,
                                fieldsView: {
                                    caption: {
                                        caption: 'Название',
                                        style: {
                                            lineHeight: '35px'
                                        },
                                        format: (value, item) => {
                                            return (
                                                <Box display="flex" gap="8px" alignItems="center">
                                                    <HomeSvgSelector width={ 36 } height={ 36 } id={ item.databaseImageCssClass } />
                                                    <Box display="flex" flexDirection="column" gap="8px">
                                                        <TextOut fontSize={ 14 } fontWeight={ 700 } style={ { wordBreak: 'normal' } }>{ value }</TextOut>
                                                        {
                                                            item.extensionState === ExtensionState.ProcessingInstall || item.extensionState === ExtensionState.ProcessingDelete
                                                                ? (
                                                                    <Box display="flex" flexDirection="row" gap="6px">
                                                                        <img src="/img/database-icons/animated-clock.gif" alt="timer" style={ { width: '16px', height: '16px' } } />
                                                                        <TextOut style={ { color: item.extensionState === ExtensionState.ProcessingDelete ? COLORS.error : COLORS.main } }>{
                                                                            item.extensionState === ExtensionState.ProcessingDelete
                                                                                ? 'В процессе отключения'
                                                                                : 'В процессе установки'
                                                                        }
                                                                        </TextOut>
                                                                    </Box>
                                                                )
                                                                : <div />
                                                        }
                                                    </Box>
                                                </Box>
                                            );
                                        }
                                    },
                                    templateName: {
                                        caption: 'Шаблон'
                                    },
                                    extensionLastActivityDate: {
                                        caption: 'Активность',
                                        format: value => {
                                            return <TextOut>{ DateUtility.dateToDayMonthYearHourMinute(new Date(value)) }</TextOut>;
                                        }
                                    },
                                    id: {
                                        caption: name,
                                        format: (_, item) =>
                                            <div className={ style.switchContainer }>
                                                <CustomizedSwitches
                                                    checked={ item.isInstalled }
                                                    disabled={ item.extensionState === ExtensionState.ProcessingInstall || item.extensionState === ExtensionState.ProcessingDelete }
                                                    onChange={ () => {
                                                        extensionChange(item.isInstalled, { databaseIds: [item.id], serviceId: id });
                                                        item.isInstalled = !item.isInstalled;
                                                        item.extensionState = !item.isInstalled ? ExtensionState.ProcessingDelete : ExtensionState.ProcessingInstall;
                                                        setStatus([...status, item.id]);
                                                    } }
                                                />
                                            </div>
                                    }
                                },
                                pagination: {
                                    currentPage: extension?.metadata.pageNumber ?? 1,
                                    pageSize: extension?.metadata.pageSize ?? 1,
                                    recordsCount: extension?.metadata.totalItemCount ?? 0,
                                    totalPages: extension?.metadata.pageCount ?? 1
                                }
                            } }
                            onDataSelect={ getExtension }
                        />
                    )
                    : (
                        <Box display="flex" flexDirection="column">
                            <TextOut fontSize={ 14 }>
                                Для установки сервиса перейдите в меню &quot;<Link style={ { color: COLORS.link } } to={ AppRoutes.accountManagement.informationBases }>Информационные базы</Link>&quot; и создайте базу с одной из совместимых конфигураций:
                            </TextOut>
                            {
                                templates && templates.data?.map((item, index) => {
                                    return <TextOut fontSize={ 14 } key={ index }>-{ item }</TextOut>;
                                })
                            }
                        </Box>
                    )
            }
        </div>
    );
};

export const Installing = memo(withFloatMessages(InstallingView));