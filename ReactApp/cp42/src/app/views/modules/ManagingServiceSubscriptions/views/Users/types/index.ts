import { GetBillingService } from 'app/api/endpoints/managingServiceSubscriptions/response';
import { SortingKind } from 'app/common/enums';

export type Props = {
    id: string;
    billingTable: GetBillingService[];
    isDemo: boolean;
};

export type SortingSwitch = {
    id: string;
    sortKind?: SortingKind;
};