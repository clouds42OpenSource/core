export type Props = {
    cost: number;
    currency: string;
    acceptClick: () => void;
    resetClick: () => void;
};