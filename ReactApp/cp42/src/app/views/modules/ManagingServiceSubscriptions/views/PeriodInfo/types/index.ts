import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { ReactNode } from 'react';
import { AmountData, ServiceStatus } from 'app/api/endpoints/managingServiceSubscriptions/response';

export type Props = {
    id: string;
    date: Date;
    dateRent: Date;
    isDemoPeriod: boolean;
    totalSum: number;
    servicesName: string;
    serviceDependsOnRent: boolean;
    currency: string;
    refreshServiceInfo: () => void;
    serviceStatus: ServiceStatus;
    amountData: AmountData;
    show: (type: MessageType, content: ReactNode, autoHideDuration?: number) => void;
};