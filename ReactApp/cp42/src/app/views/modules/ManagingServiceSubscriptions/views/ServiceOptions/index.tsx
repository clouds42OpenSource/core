import { TabsControl } from 'app/views/components/controls/TabsControl';
import { Installing } from 'app/views/modules/ManagingServiceSubscriptions/views/Installing';
import { Licenses } from 'app/views/modules/ManagingServiceSubscriptions/views/Licenses';
import { Managing } from 'app/views/modules/ManagingServiceSubscriptions/views/Managing';
import { Users } from 'app/views/modules/ManagingServiceSubscriptions/views/Users';
import { useState } from 'react';
import { useAppSelector } from 'app/hooks';
import { AccountUserGroup } from 'app/common/enums';
import { Props } from './types';

const currentDate = new Date();

export const ServiceOptions = ({ id, visibleLicenses, canInstallExtension, visibleUsers, serviceData, billingTable }: Props) => {
    const { settings: { currentContextInfo: { currentUserGroups } } } = useAppSelector(state => state.Global.getCurrentSessionSettingsReducer);
    const [countInstalling, setCountInstalling] = useState(1);
    const isNeedToCheckInstallingStatus = !(billingTable.filter(tableElem => tableElem.usedLicenses !== 0).length > 0);
    const isVisibleInstalling = !isNeedToCheckInstallingStatus && ((currentDate <= new Date(serviceData.serviceExpireDate)) && (countInstalling > 0 || (countInstalling === 0 && canInstallExtension)));

    const renderHeaders = [
        {
            title: 'Лицензии на аккаунт',
            isVisible: visibleLicenses,
        },
        {
            title: 'Пользователи',
            isVisible: visibleUsers,
        },
        {
            title: 'Установка сервиса в базы',
            isVisible: isVisibleInstalling
        },
        {
            title: 'Управление',
            isVisible: currentUserGroups.includes(AccountUserGroup.CloudAdmin),
        },
    ].filter(item => item.isVisible);

    return (
        <TabsControl headers={ renderHeaders }>
            { visibleLicenses &&
            <Licenses
                id={ id }
                billingTable={ billingTable.filter(item => item.billingType === 1) }
            />
            }
            { visibleUsers &&
            <Users
                id={ id }
                billingTable={ billingTable.filter(item => item.billingType === 0) }
                isDemo={ serviceData.isDemoPeriod }
            />
            }
            { isVisibleInstalling &&
            <Installing
                isNeedToDelete={ isNeedToCheckInstallingStatus }
                name={ serviceData.name }
                id={ id }
                setCount={ setCountInstalling }
                canInstallExtension={ canInstallExtension }
            />
            }
            { currentUserGroups.includes(AccountUserGroup.CloudAdmin) &&
            <Managing
                billingServiceId={ id }
                serviceExpireDate={ serviceData.serviceExpireDate }
                serviceIsLocked={ serviceData.serviceStatus.serviceIsLocked }
                serviceDependsOnRent={ serviceData.serviceDependsOnRent }
                billingData={ billingTable }
            />
            }
            { isNeedToCheckInstallingStatus &&
            <Installing
                isNeedToDelete={ isNeedToCheckInstallingStatus }
                name={ serviceData.name }
                id={ id }
                setCount={ setCountInstalling }
                canInstallExtension={ canInstallExtension }
            />
            }
        </TabsControl>
    );
};