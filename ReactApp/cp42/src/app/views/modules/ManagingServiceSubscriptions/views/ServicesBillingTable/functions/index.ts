import { GetBillingService } from 'app/api/endpoints/managingServiceSubscriptions/response';
import { BillingType } from 'app/api/endpoints/managingServiceSubscriptions/enum';

export const totalValue = (cost: number, usedLicenses: number, sponsoringLicenses: number, sponsoredLicenses: number): GetBillingService => {
    return {
        id: '',
        parentServiceTypes: [],
        childServiceTypes: [],
        name: 'Итого в рамках текущего периода',
        amount: cost,
        currency: '',
        cost,
        costPerLicense: 0,
        usedLicenses,
        sponsoringLicenses,
        sponsoredLicenses,
        billingType: BillingType.ForAccount,
        serviceId: '',
        serviceName: '',
        isActive: false
    };
};