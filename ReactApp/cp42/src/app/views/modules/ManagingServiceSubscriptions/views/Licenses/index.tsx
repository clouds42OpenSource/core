import { Box } from '@mui/material';
import { Services } from 'app/api/endpoints/managingServiceSubscriptions/request';
import { GetBillingService, PutServicesPrecalculatedCost } from 'app/api/endpoints/managingServiceSubscriptions/response';
import { FETCH_API } from 'app/api/useFetchApi';
import { localStorageHelper } from 'app/common/helpers/LocalStorageHelper';
import { COLORS } from 'app/utils';
import { TextOut } from 'app/views/components/TextOut';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { Dialog } from 'app/views/components/controls/Dialog';
import CustomizedSwitches from 'app/views/components/controls/Switch/views/SwitchViews';
import { AcceptOrReset } from 'app/views/modules/ManagingServiceSubscriptions/views/AcceptOrReset';
import React, { useCallback, useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import { AppRoutes } from 'app/AppRoutes';
import { Redirect } from 'react-router-dom';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { Props } from './types';

const api = FETCH_API.MANAGING_SERVICE_SUBSCRIPTIONS;
const ctxGuid = localStorageHelper.getContextAccountId() ?? '';

export const Licenses = withFloatMessages(({ id, billingTable, floatMessage: { show } }: Props & FloatMessageProps) => {
    const [licensesTable, setLicensesTable] = useState<GetBillingService[]>([]);
    const [servicesList, setServicesList] = useState<Services[]>([]);
    const [currentId, setCurrentId] = useState('');
    const [newServicesList, setNewServicesList] = useState<Services[]>([]);
    const [currentServices, setCurrentServices] = useState<Services>();
    const [precalculatedCostInfo, setPrecalculatedCostInfo] = useState<PutServicesPrecalculatedCost[] | null>(null);
    const [errorMessage, setErrorMessage] = useState('');

    const [showAcceptOrReset, setShowAcceptOrReset] = useState(false);
    const [showResetDialog, setShowResetDialog] = useState(false);
    const [showAcceptDialog, setShowAcceptDialog] = useState(false);
    const [showErrorDialog, setShowErrorDialog] = useState(false);

    const [isNeedRefresh, setIsNeedRefresh] = useState(false);
    const [isLoading, setIsLoading] = useState(false);

    const history = useHistory();

    const setDefaultLicensesTable = useCallback(() => {
        setLicensesTable(billingTable);
    }, [billingTable]);

    const setDefaultServicesList = useCallback(() => {
        setServicesList(billingTable.map(item => {
            return {
                billingServiceTypeId: item.id,
                status: item.isActive,
                subject: ctxGuid
            };
        }));
    }, [billingTable]);

    const setDefaultValue = () => {
        setDefaultLicensesTable();
        setDefaultServicesList();
        setNewServicesList([]);
        setShowAcceptOrReset(false);
        setShowResetDialog(false);
    };

    useEffect(() => {
        setDefaultLicensesTable();
        setDefaultServicesList();
    }, [setDefaultLicensesTable, setDefaultServicesList]);

    const getPrecalculateInfo = (billingServiceTypeId: string, status: boolean, newUserServices: Services[]) => {
        const currentUserServices = {
            billingServiceTypeId,
            status,
            subject: ctxGuid
        };

        setCurrentServices(currentUserServices);

        api.putServicesPrecalculatedCost({
            accountId: ctxGuid,
            changedLicense: currentUserServices,
            billingServiceId: id,
            changedUsers: newUserServices
        }).then(response => setPrecalculatedCostInfo(response.data));
    };

    const changeServiceStatus = (id: string) => {
        if (!showAcceptOrReset) {
            setShowAcceptOrReset(true);
        }

        const changeServicesList = servicesList.map(item => {
            if (item.billingServiceTypeId === id) {
                const changeItem = {
                    status: !item.status,
                    billingServiceTypeId: item.billingServiceTypeId,
                    subject: item.subject
                };

                if (newServicesList.filter(switchItem => switchItem.billingServiceTypeId === changeItem.billingServiceTypeId).length === 0) {
                    const newServices = [...newServicesList, changeItem];

                    getPrecalculateInfo(changeItem.billingServiceTypeId, changeItem.status, newServices);
                    setNewServicesList(newServices);
                } else {
                    const newServices = newServicesList.map(newItem => {
                        if (newItem.billingServiceTypeId === item.billingServiceTypeId) {
                            return {
                                status: !newItem.status,
                                billingServiceTypeId: newItem.billingServiceTypeId,
                                subject: newItem.subject,
                            };
                        }

                        return newItem;
                    });

                    getPrecalculateInfo(changeItem.billingServiceTypeId, changeItem.status, newServices);
                    setNewServicesList(newServices);
                }

                return changeItem;
            }

            return item;
        });

        setServicesList(changeServicesList);
    };

    const apply = () => {
        if (currentServices) {
            setIsLoading(true);

            api.putServicesApply({
                accountId: localStorageHelper.getContextAccountId() ?? '',
                billingServiceId: id,
                changedLicenses: newServicesList,
                changedUsers: [],
                usePromisePayment: false
            }).then(response => {
                setIsLoading(false);

                if (!response.success) {
                    show(MessageType.Error, response.message);
                } else {
                    setIsNeedRefresh(true);
                }
            });
        }
    };

    useEffect(() => {
        if (precalculatedCostInfo && precalculatedCostInfo.length > 0) {
            precalculatedCostInfo.map(item => {
                const { errors } = item;

                if (errors !== null && errors.length > 0) {
                    setErrorMessage(errors[0].description);
                    setShowErrorDialog(true);
                }

                return item;
            });
        }
    }, [precalculatedCostInfo]);

    if (isNeedRefresh) {
        return <Redirect to={ window.location.pathname } />;
    }

    const onCancelClickWarningDialogHandler = () => {
        setShowErrorDialog(false);
        changeServiceStatus(currentId);
    };

    return (
        <>
            { isLoading && <LoadingBounce /> }
            <TextOut fontSize={ 14 }>Услуга</TextOut>
            <Box display="flex" flexDirection="column" gap="16px" margin="16px 0">
                { licensesTable.map(item =>
                    <Box key={ item.id } display="flex" alignItems="center" borderBottom={ `1px solid ${ COLORS.border }` } padding="8px">
                        <TextOut style={ { width: '60%' } }>{ item.name }</TextOut>
                        <CustomizedSwitches
                            checked={ servicesList.find(servicesListItem => servicesListItem.billingServiceTypeId === item.id)?.status ?? false }
                            onChange={ () => {
                                setCurrentId(item.id);
                                changeServiceStatus(item.id);
                            } }
                        />
                    </Box>
                ) }
            </Box>
            { showAcceptOrReset &&
                <AcceptOrReset
                    cost={ precalculatedCostInfo?.reduce((prev, curr) => prev + curr.cost, 0) ?? 0 }
                    currency={ billingTable[0].currency ?? '' }
                    acceptClick={ () => setShowAcceptDialog(true) }
                    resetClick={ () => setShowResetDialog(true) }
                />
            }
            <Dialog
                title="Подтвердите отмену"
                isTitleSmall={ true }
                isOpen={ showResetDialog }
                dialogWidth="xs"
                dialogVerticalAlign="center"
                onCancelClick={ () => setShowResetDialog(false) }
                buttons={
                    [
                        {
                            content: 'Да',
                            kind: 'default',
                            onClick: setDefaultValue,
                            variant: 'text'
                        },
                        {
                            content: 'Нет',
                            kind: 'warning',
                            onClick: () => setShowResetDialog(false)
                        }
                    ]
                }
            >
                <TextOut>Вы уверены что хотите сбросить все изменения?</TextOut>
            </Dialog>
            <Dialog
                title="Подтвердите операцию"
                isTitleSmall={ true }
                isOpen={ showAcceptDialog }
                dialogWidth="xs"
                dialogVerticalAlign="center"
                onCancelClick={ () => setShowAcceptDialog(false) }
                buttons={
                    [
                        {
                            content: 'Отмена',
                            kind: 'default',
                            onClick: () => setShowAcceptDialog(false),
                            variant: 'text'
                        },
                        {
                            content: 'Продолжить',
                            kind: 'success',
                            onClick: () => apply(),
                        }
                    ]
                }
            >
                <>
                    { newServicesList.length > 0 && !newServicesList[0].status &&
                    <TextOut>Внимание! При отключении всех пользователей сервис будет удаление из ВСЕХ информационных баз! </TextOut>
                    }
                    <TextOut>Нажмите &quot;Продолжить&quot;, чтобы применить изменения.</TextOut>
                </>
            </Dialog>
            <Dialog
                title="Внимание"
                isTitleSmall={ true }
                isOpen={ showErrorDialog }
                dialogWidth="xs"
                dialogVerticalAlign="center"
                onCancelClick={ onCancelClickWarningDialogHandler }
                buttons={
                    [
                        {
                            content: 'Закрыть',
                            kind: 'default',
                            onClick: onCancelClickWarningDialogHandler,
                            variant: 'text'
                        },
                        {
                            content: 'Подключить Аренду 1С',
                            kind: 'primary',
                            onClick: () => history.push(AppRoutes.services.rentReference)
                        }
                    ]
                }
            >
                <TextOut>
                    <div dangerouslySetInnerHTML={ { __html: errorMessage } } />
                </TextOut>
            </Dialog>
        </>
    );
});