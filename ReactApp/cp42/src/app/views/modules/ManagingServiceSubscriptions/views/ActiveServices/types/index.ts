import { Dispatch, SetStateAction } from 'react';

export type Props = {
    id: string;
    setIsLoading: Dispatch<SetStateAction<boolean>>;
    isActive: boolean;
    isAlpaca?: boolean;
};