import { Box, Checkbox, TextField } from '@mui/material';
import { NewCostsRequest } from 'app/api/endpoints/managingServiceSubscriptions/request';
import { FETCH_API } from 'app/api/useFetchApi';

import { AppRoutes } from 'app/AppRoutes';
import { localStorageHelper } from 'app/common/helpers/LocalStorageHelper';
import { COLORS } from 'app/utils';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { SuccessButton } from 'app/views/components/controls/Button';
import { MuiDateInputForm } from 'app/views/components/controls/forms/MuiDateInputForm';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { TextOut } from 'app/views/components/TextOut';
import dayjs from 'dayjs';
import React, { useEffect, useState } from 'react';
import { Link, Redirect } from 'react-router-dom';

import css from './style.module.css';
import { Props } from './types';

const { putServicesManagement, postProlongDemo } = FETCH_API.MANAGING_SERVICE_SUBSCRIPTIONS;

const currentDate = new Date();

export const Managing = withFloatMessages(({ billingServiceId, serviceIsLocked, serviceExpireDate, billingData, serviceDependsOnRent, floatMessage: { show } }: Props & FloatMessageProps) => {
    const [servicesCost, setServicesCost] = useState<NewCostsRequest[]>([]);
    const [expireDate, setExpireDate] = useState(new Date(serviceExpireDate));
    const [countDay, setCountDay] = useState(1);
    const [isLoading, setIsLoading] = useState(false);
    const [isNeedRefresh, setIsNeedRefresh] = useState(false);

    const isExpireDateActive = expireDate >= currentDate;

    const dateHandleChange = (_: string, e: Date | null) => {
        setExpireDate(e ?? currentDate);
    };

    const costHandleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const newValue = servicesCost.map(item => {
            if (item.key === event.target.id) {
                return {
                    key: item.key,
                    value: event.target.value !== '' ? parseInt(event.target.value, 10) : 0
                };
            }

            return item;
        });

        setServicesCost(newValue);
    };

    const countDayHandleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const dayCount = Number(event.target.value);
        setCountDay(Number.isNaN(dayCount) ? 0 : dayCount);
    };

    useEffect(() => {
        const servicesCostData: NewCostsRequest[] = billingData.map(item => (
            { key: item.id, value: item.cost }
        ));

        setServicesCost(servicesCostData);
    }, [billingData]);

    const handleSaveChange = async () => {
        setIsLoading(true);
        const servicesCostObject: { [key: string]: number } = {};

        for (const cost of servicesCost) {
            servicesCostObject[cost.key] = cost.value;
        }

        const response = await putServicesManagement({
            accountId: localStorageHelper.getContextAccountId() ?? '',
            billingServiceId,
            newCosts: servicesCostObject,
            newExpireDate: dayjs(expireDate).format('YYYY-MM-DDTHH:mm:ss')
        });

        if (response && !response.success) {
            show(MessageType.Error, response.message);
        } else {
            show(MessageType.Success, 'Изменения успешно сохранены');
            setIsNeedRefresh(true);
        }

        setIsLoading(false);
    };

    const handleProlongDemo = async () => {
        setIsLoading(true);

        postProlongDemo({
            accountId: localStorageHelper.getContextAccountId() ?? '',
            serviceId: billingServiceId,
            prolongDayCount: countDay
        }).then(response => {
            if (response.success) {
                show(MessageType.Success, 'Изменения успешно сохранены');
                setIsNeedRefresh(true);
                setIsLoading(false);
            } else {
                setIsLoading(false);
                show(MessageType.Error, response.message);
            }
        });
    };

    if (isNeedRefresh) {
        return <Redirect to={ window.location.pathname } />;
    }

    return (
        <>
            { isLoading && <LoadingBounce /> }
            <Box display="flex" flexDirection="column" justifyContent="space-between" gap="16px" marginBottom="16px">
                <Box display="flex" flexDirection="column" gap="16px">
                    { servicesCost.map((item, index) =>
                        <Box display="flex" alignItems="center" key={ item.key }>
                            <TextOut className={ css.inputWidth }>{ billingData[index].name }, цена:</TextOut>
                            <TextField id={ item.key } value={ item.value } className={ css.inputWidth } onChange={ costHandleChange } />
                        </Box>
                    ) }
                </Box>
                <Box display="flex" alignItems="center">
                    <TextOut className={ css.inputWidth }>Оплачено до:</TextOut>
                    <div className={ css.inputWidth }>
                        <MuiDateInputForm
                            className={ css.datePecker }
                            label="Дата"
                            formName="Date"
                            value={ expireDate }
                            onValueChange={ dateHandleChange }
                            includeTime={ true }
                            isReadOnly={ isExpireDateActive && serviceDependsOnRent }
                        />
                        { isExpireDateActive && serviceDependsOnRent &&
                            <TextOut fontSize={ 13 } style={ { color: COLORS.error } }>
                                Для изменения пролонгации перейдите в сервис <Link style={ { color: COLORS.link } } to={ AppRoutes.services.rentReference }>Аренда 1С</Link>.
                            </TextOut>
                        }
                    </div>
                </Box>
                <Box display="flex" alignItems="center">
                    <TextOut className={ css.inputWidth }>Блокировано:</TextOut>
                    <Checkbox checked={ serviceIsLocked } disabled={ true } className={ css.checkBox } />
                </Box>
                <SuccessButton onClick={ handleSaveChange } className={ css.buttonRight }>
                    Сохранить изменения
                </SuccessButton>
            </Box>
            <Box display="flex" flexDirection="column" justifyContent="space-between" gap="16px">
                <Box display="flex" alignItems="center">
                    <TextOut className={ css.inputWidth }>Продлить демо период, дни</TextOut>
                    <TextField value={ countDay } inputProps={ { inputMode: 'numeric', pattern: '[0-9]*' } } className={ css.inputWidth } onChange={ countDayHandleChange } />
                </Box>
                <SuccessButton onClick={ handleProlongDemo } className={ css.buttonRight }>
                    Продлить демо
                </SuccessButton>
            </Box>
        </>
    );
});