import { GetBillingService } from 'app/api/endpoints/managingServiceSubscriptions/response';

export type Props = {
    billingTableDataset: GetBillingService[];
    currency: string;
};