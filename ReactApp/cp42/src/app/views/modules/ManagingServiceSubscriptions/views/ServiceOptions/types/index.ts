import { GetBillingService, GetServices } from 'app/api/endpoints/managingServiceSubscriptions/response';

export type Props = {
    id: string;
    visibleLicenses: boolean;
    visibleUsers: boolean;
    canInstallExtension: boolean;
    serviceData: GetServices;
    billingTable: GetBillingService[];
};