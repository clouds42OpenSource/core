import { useParams } from 'react-router';
import { LocationProps } from 'app/views/modules/ManagingServiceSubscriptions/types';
import React, { useCallback, useEffect, useState } from 'react';
import { FETCH_API } from 'app/api/useFetchApi';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { ActiveServices } from 'app/views/modules/ManagingServiceSubscriptions/views/ActiveServices';

const api = FETCH_API.MANAGING_SERVICE_SUBSCRIPTIONS;

const ManagingServiceSubscriptions = ({ floatMessage: { show } }: FloatMessageProps) => {
    const { id } = useParams<LocationProps>();
    const [isLoading, setIsLoading] = useState(false);
    const [isActive, setIsActive] = useState<boolean>();

    const getActivationStatus = useCallback(() => {
        api.getActivationStatus({ id })
            .then(response => {
                setIsLoading(true);

                if (response.success) {
                    setIsActive(response.data ?? false);
                } else {
                    show(MessageType.Error, response.message);
                }

                setIsLoading(false);
            });
    }, [id, show]);

    /**
     * Check activation services
     */
    useEffect(() => {
        getActivationStatus();
    }, [getActivationStatus]);

    return (
        <>
            { isLoading && <LoadingBounce /> }
            <ActiveServices
                id={ id }
                setIsLoading={ setIsLoading }
                isActive={ !!isActive }
            />
        </>
    );
};

export const ManagingServiceSubscriptionsView = withFloatMessages(ManagingServiceSubscriptions);