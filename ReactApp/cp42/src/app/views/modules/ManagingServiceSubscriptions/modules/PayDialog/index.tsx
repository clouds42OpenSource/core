import React, { useState } from 'react';
import { useHistory } from 'react-router';
import { PutServicesApply } from 'app/api/endpoints/managingServiceSubscriptions/response';

import { Dialog } from 'app/views/components/controls/Dialog';
import { AppRoutes } from 'app/AppRoutes';
import { Box } from '@mui/system';
import { getHref } from 'app/views/modules/ToPartners/helpers';
import { TextOut } from 'app/views/components/TextOut';
import { contextAccountId } from 'app/api';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { FETCH_API } from 'app/api/useFetchApi';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { Redirect } from 'react-router-dom';
import { LoadingBounce } from 'app/views/components/LoadingBounce';

type TPayDialog = FloatMessageProps & {
    applyStatus: PutServicesApply | null;
    isOpen: boolean;
    setIsOpen: React.Dispatch<React.SetStateAction<boolean>>;
    serviceId?: string;
};

const { putServicesActivationDemo } = FETCH_API.MANAGING_SERVICE_SUBSCRIPTIONS;

export const PayDialog = withFloatMessages(({ isOpen, setIsOpen, applyStatus, floatMessage: { show }, serviceId }: TPayDialog) => {
    const history = useHistory();

    const [isLoading, setIsLoading] = useState(false);
    const [isNeedRefresh, setIsNeedRefresh] = useState(false);

    const applyDemo = (usePromisePayment = false) => {
        setIsLoading(true);
        putServicesActivationDemo({
            accountId: contextAccountId(),
            serviceId: serviceId ?? '',
            usePromisePayment
        }).then(response => {
            if (response?.success && response.data) {
                setIsNeedRefresh(true);
            } else {
                show(MessageType.Error, response?.message);
            }

            setIsLoading(false);
        });
    };

    const closeDialogHandler = () => {
        setIsOpen(false);
    };

    if (isNeedRefresh) {
        return <Redirect to={ window.location.pathname } />;
    }

    return (
        <>
            { isLoading && <LoadingBounce /> }
            { applyStatus &&
                <Dialog
                    title="Внимание"
                    isTitleSmall={ true }
                    isOpen={ isOpen }
                    dialogWidth="sm"
                    dialogVerticalAlign="center"
                    buttons={ applyStatus.canUsePromisePayment
                        ? [
                            {
                                content: 'Отмена',
                                kind: 'default',
                                onClick: closeDialogHandler
                            },
                            {
                                content: `Взять обещанный платеж (${ applyStatus.notEnoughMoney } ${ applyStatus.currency })`,
                                kind: 'primary',
                                onClick: () => applyDemo(true),
                            },
                            {
                                content: `Пополнить счет (${ applyStatus.notEnoughMoney } ${ applyStatus.currency })`,
                                kind: 'primary',
                                onClick: () => history.push(`${ AppRoutes.accountManagement.replenishBalance }?customPayment.amount=${ applyStatus.notEnoughMoney }&customPayment.description=Применение+подписки+демо+сервиса`),
                            }
                        ]
                        : [
                            {
                                content: 'Отмена',
                                kind: 'default',
                                onClick: closeDialogHandler
                            },
                            {
                                content: `Пополнить счет (${ applyStatus.notEnoughMoney } ${ applyStatus.currency })`,
                                kind: 'primary',
                                onClick: () => history.push(`${ AppRoutes.accountManagement.replenishBalance }?customPayment.amount=${ applyStatus.notEnoughMoney }&customPayment.description=Применение+подписки+демо+сервиса`),
                            }
                        ]
                    }
                    onCancelClick={ closeDialogHandler }
                >
                    <Box display="flex" flexDirection="column" gap="4px" alignItems="center">
                        <img src={ getHref('warning') } alt="warning" height={ 100 } width={ 100 } />
                        { applyStatus.canUsePromisePayment
                            ? (
                                <TextOut>
                                    Уважаемый пользователь, для активации сервиса у Вас недостаточно средств в размере <TextOut fontWeight={ 700 }>{ applyStatus.notEnoughMoney } { applyStatus.currency }. </TextOut>
                                    Вы можете воспользоваться услугой <TextOut fontWeight={ 700 }>&quot;Обещанный платеж&quot;</TextOut>, Вы должны будете погасить задолжность в течение 7 дней.
                                </TextOut>
                            )
                            : (
                                <TextOut>
                                    Уважаемый пользователь, для активации сервиса у Вас недостаточно средств в размере <TextOut fontWeight={ 700 }>{ applyStatus.notEnoughMoney } { applyStatus.currency }</TextOut>.
                                </TextOut>
                            )
                        }
                    </Box>
                </Dialog>
            }
        </>
    );
});