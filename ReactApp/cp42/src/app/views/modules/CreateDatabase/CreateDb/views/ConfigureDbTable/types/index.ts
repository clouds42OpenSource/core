import { AvailableTemplatesItem } from 'app/api/endpoints/createDb/response';
import { CreateDatabasesModelDcItem } from 'app/api/endpoints/createDb/request/postFromTemplateRequest';
import { Dispatch, SetStateAction } from 'react';

export type Props = {
    dataset: AvailableTemplatesItem[];
    selectedWebDatabase: CreateDatabasesModelDcItem[];
    setSelectedWebDatabase: Dispatch<SetStateAction<CreateDatabasesModelDcItem[]>>;
    selectedFileDatabase: CreateDatabasesModelDcItem[];
    setSelectedFileDatabase: Dispatch<SetStateAction<CreateDatabasesModelDcItem[]>>;
    isDbOnDelimiters: boolean;
    selectUsers: string[];
    serviceTypesIdsList: (string | null)[];
    setServiceTypesIdsList: Dispatch<SetStateAction<(string | null)[]>>;
};