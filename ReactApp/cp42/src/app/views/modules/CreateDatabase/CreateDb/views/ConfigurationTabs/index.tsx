import { AccountUserGroup } from 'app/common/enums';
import { TabsControl } from 'app/views/components/controls/TabsControl';
import { Configuration } from 'app/views/modules/CreateDatabase/CreateDb/views/ConfigurationTabs/tabs/Configuration';
import { Props } from './types';

const renderHeader = (isConfigurationsVisible: boolean) => {
    return [
        {
            title: 'Конфигурация 1С',
            isVisible: true,
        },
        {
            title: 'Конфигурация из шаблона',
            isVisible: isConfigurationsVisible,
        }
    ];
};

export const ConfigurationTabs = ({
    isVip,
    isDbOnDelimiters,
    webDataset,
    fileDataset,
    onDataSelect,
    setSelectedFileDatabase,
    setSelectedWebDatabase,
    selectedWebDatabase,
    selectedFileDatabase,
    selectUsers,
    setServiceTypesIdsList,
    serviceTypesIdsList,
    currentUserGroups
}: Props) => {
    const renderTabs = () => {
        return webDataset.length
            ? (
                <TabsControl
                    headers={ renderHeader((currentUserGroups.includes(AccountUserGroup.CloudAdmin) ||
                        currentUserGroups.includes(AccountUserGroup.CloudSE) ||
                        currentUserGroups.includes(AccountUserGroup.Hotline))
                    ) }
                >
                    <Configuration
                        dataset={ webDataset }
                        datasetSelect={ onDataSelect }
                        selectedFileDatabase={ selectedFileDatabase }
                        selectedWebDatabase={ selectedWebDatabase }
                        setSelectedFileDatabase={ setSelectedFileDatabase }
                        setSelectedWebDatabase={ setSelectedWebDatabase }
                        isDbOnDelimiters={ true }
                        selectUsers={ selectUsers }
                        setServiceTypesIdsList={ setServiceTypesIdsList }
                        serviceTypesIdsList={ serviceTypesIdsList }
                        isVip={ isVip }
                    />
                    <Configuration
                        dataset={ fileDataset }
                        datasetSelect={ onDataSelect }
                        selectedFileDatabase={ selectedFileDatabase }
                        selectedWebDatabase={ selectedWebDatabase }
                        setSelectedFileDatabase={ setSelectedFileDatabase }
                        setSelectedWebDatabase={ setSelectedWebDatabase }
                        isDbOnDelimiters={ false }
                        selectUsers={ selectUsers }
                        setServiceTypesIdsList={ setServiceTypesIdsList }
                        serviceTypesIdsList={ serviceTypesIdsList }
                        isVip={ isVip }
                    />
                </TabsControl>
            ) : (
                <Configuration
                    dataset={ fileDataset }
                    datasetSelect={ onDataSelect }
                    selectedFileDatabase={ selectedFileDatabase }
                    selectedWebDatabase={ selectedWebDatabase }
                    setSelectedFileDatabase={ setSelectedFileDatabase }
                    setSelectedWebDatabase={ setSelectedWebDatabase }
                    isDbOnDelimiters={ false }
                    selectUsers={ selectUsers }
                    setServiceTypesIdsList={ setServiceTypesIdsList }
                    serviceTypesIdsList={ serviceTypesIdsList }
                    isVip={ isVip }
                />);
    };

    const renderIsDelimiters = () => {
        return isDbOnDelimiters
            ? (
                <Configuration
                    dataset={ webDataset }
                    datasetSelect={ onDataSelect }
                    selectedFileDatabase={ selectedFileDatabase }
                    selectedWebDatabase={ selectedWebDatabase }
                    setSelectedFileDatabase={ setSelectedFileDatabase }
                    setSelectedWebDatabase={ setSelectedWebDatabase }
                    isDbOnDelimiters={ true }
                    selectUsers={ selectUsers }
                    setServiceTypesIdsList={ setServiceTypesIdsList }
                    serviceTypesIdsList={ serviceTypesIdsList }
                    isVip={ isVip }
                />
            ) : (
                <Configuration
                    dataset={ fileDataset }
                    datasetSelect={ onDataSelect }
                    selectedFileDatabase={ selectedFileDatabase }
                    selectedWebDatabase={ selectedWebDatabase }
                    setSelectedFileDatabase={ setSelectedFileDatabase }
                    setSelectedWebDatabase={ setSelectedWebDatabase }
                    isDbOnDelimiters={ false }
                    selectUsers={ selectUsers }
                    setServiceTypesIdsList={ setServiceTypesIdsList }
                    serviceTypesIdsList={ serviceTypesIdsList }
                    isVip={ isVip }
                />);
    };

    const isVipRender = () => {
        return isVip
            ? renderIsDelimiters()
            : renderTabs();
    };

    return isVipRender();
};