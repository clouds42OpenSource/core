import { CreateDatabasesModelDcItem } from 'app/api/endpoints/createDb/request/postFromTemplateRequest';
import { AvailableTemplatesItem } from 'app/api/endpoints/createDb/response';
import { AccountUserGroup } from 'app/common/enums';
import { Dispatch, SetStateAction } from 'react';

export type Props = {
    isVip: boolean;
    isDbOnDelimiters: boolean;
    webDataset: AvailableTemplatesItem[];
    fileDataset: AvailableTemplatesItem[];
    selectedWebDatabase: CreateDatabasesModelDcItem[];
    setSelectedWebDatabase: Dispatch<SetStateAction<CreateDatabasesModelDcItem[]>>;
    selectedFileDatabase: CreateDatabasesModelDcItem[];
    setSelectedFileDatabase: Dispatch<SetStateAction<CreateDatabasesModelDcItem[]>>;
    selectUsers: string[];
    onDataSelect: (isDelimiters?: boolean) => void;
    serviceTypesIdsList: (string | null)[];
    setServiceTypesIdsList: Dispatch<SetStateAction<(string | null)[]>>;
    currentUserGroups: AccountUserGroup[];
};

export type TabsProps = {
    dataset: AvailableTemplatesItem[];
    datasetSelect: (isDelimiters?: boolean) => void;
    selectedWebDatabase: CreateDatabasesModelDcItem[];
    setSelectedWebDatabase: Dispatch<SetStateAction<CreateDatabasesModelDcItem[]>>;
    selectedFileDatabase: CreateDatabasesModelDcItem[];
    setSelectedFileDatabase: Dispatch<SetStateAction<CreateDatabasesModelDcItem[]>>;
    isDbOnDelimiters: boolean;
    selectUsers: string[];
    serviceTypesIdsList: (string | null)[];
    setServiceTypesIdsList: Dispatch<SetStateAction<(string | null)[]>>;
    isVip: boolean;
};