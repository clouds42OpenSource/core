import { BaseButton } from 'app/views/components/controls/Button/BaseButton';
import { ERefreshId } from 'app/views/Layout/ProjectTour/enums';
import { TabsProps } from 'app/views/modules/CreateDatabase/CreateDb/views/ConfigurationTabs/types';
import { ConfigureDbTable } from 'app/views/modules/CreateDatabase/CreateDb/views/ConfigureDbTable';
import { useState } from 'react';

export const Configuration = ({
    dataset,
    datasetSelect,
    setSelectedFileDatabase,
    setSelectedWebDatabase,
    selectedWebDatabase,
    selectedFileDatabase,
    isDbOnDelimiters,
    selectUsers,
    serviceTypesIdsList,
    setServiceTypesIdsList,
    isVip
}: TabsProps) => {
    const [visible, setVisible] = useState(true);

    const getTemplatesAndHiddenButton = () => {
        datasetSelect(false);
        setVisible(false);
    };

    return (
        <div data-refreshid={ ERefreshId.createDatabaseTable }>
            <ConfigureDbTable
                dataset={ dataset }
                setSelectedWebDatabase={ setSelectedWebDatabase }
                setSelectedFileDatabase={ setSelectedFileDatabase }
                selectedWebDatabase={ selectedWebDatabase }
                selectedFileDatabase={ selectedFileDatabase }
                isDbOnDelimiters={ isDbOnDelimiters }
                selectUsers={ selectUsers }
                serviceTypesIdsList={ serviceTypesIdsList }
                setServiceTypesIdsList={ setServiceTypesIdsList }
            />
            { isDbOnDelimiters && visible && !isVip &&
                <BaseButton
                    variant="outlined"
                    fullWidth={ true }
                    style={ { marginTop: '16px' } }
                    onClick={ getTemplatesAndHiddenButton }
                >
                    Другие конфигурации 1С
                </BaseButton>
            }
        </div>
    );
};