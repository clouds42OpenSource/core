import Box from '@mui/material/Box';
import { AppRoutes } from 'app/AppRoutes';
import { ContainedButton, SuccessButton } from 'app/views/components/controls/Button';
import { ETourId } from 'app/views/Layout/ProjectTour/enums';
import React from 'react';
import { Link } from 'react-router-dom';
import { Props } from './types';

export const CancelOrCreateDb = ({ onClick, possibilityToCreate, textButton }: Props) => (
    <Box position="fixed" bottom="16px" right="32px" zIndex={ 1 }>
        <Link to={ AppRoutes.accountManagement.informationBases } style={ { textDecoration: 'none', color: 'inherit', marginRight: '16px' } }>
            <ContainedButton>
                Отменить
            </ContainedButton>
        </Link>
        <SuccessButton tourId={ ETourId.createDatabasePageButton } onClick={ onClick } isEnabled={ !possibilityToCreate }>{ textButton }</SuccessButton>
    </Box>
);