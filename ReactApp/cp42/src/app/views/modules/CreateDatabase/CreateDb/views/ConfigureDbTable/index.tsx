import { Box, Checkbox } from '@mui/material';
import { useTour } from '@reactour/tour';
import { Dialog } from 'app/views/components/controls/Dialog';
import { TextOut } from 'app/views/components/TextOut';
import { ETourId } from 'app/views/Layout/ProjectTour/enums';
import { useCallback, useState } from 'react';
import { contextAccountId } from 'app/api';
import { CreateDatabasesModelDcItem } from 'app/api/endpoints/createDb/request/postFromTemplateRequest';
import { AvailableTemplatesItem } from 'app/api/endpoints/createDb/response';
import { HomeSvgSelector } from 'app/views/components/svgGenerator/HomeSvgSelector';
import { validate } from 'app/views/modules/CreateDatabase/const';
import { InputNameDt } from 'app/views/modules/CreateDatabase/views/InputNameDt';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { Props } from './types';

export const ConfigureDbTable =
    ({
        dataset,
        selectedWebDatabase,
        setSelectedFileDatabase,
        selectedFileDatabase,
        setSelectedWebDatabase,
        isDbOnDelimiters,
        selectUsers,
        setServiceTypesIdsList,
        serviceTypesIdsList
    }: Props) => {
        const { setCurrentStep } = useTour();

        const [firstShow, setFirstShow] = useState({
            autoUpdate: true,
            support: true
        });
        const [showWarning, setShowWarning] = useState({
            autoUpdate: false,
            support: false
        });
        const [selectedItem, setSelectedItem] = useState<AvailableTemplatesItem | null>(null);

        const addDatabase = useCallback(async (item: AvailableTemplatesItem, isChecked: boolean) => {
            if (isDbOnDelimiters) {
                setCurrentStep(prev => prev + 1);

                if (isChecked) {
                    setSelectedWebDatabase(selectedWebDatabase.filter(select => select.templateId !== item.id));

                    if (item.myDatabasesServiceTypeId !== null) {
                        setServiceTypesIdsList(serviceTypesIdsList.filter(idItem => idItem !== item.myDatabasesServiceTypeId));
                    }
                } else {
                    setSelectedWebDatabase([
                        ...selectedWebDatabase,
                        {
                            isChecked: true,
                            templateId: item.id,
                            dataBaseName: item.captionOnDelimiters
                                .split('')
                                .map(itemOnDelimiters => {
                                    if (validate.nameDatabase.test(itemOnDelimiters)) {
                                        return itemOnDelimiters;
                                    }

                                    return '';
                                })
                                .join('') ??
                                item.captionOnDelimiters,
                            dbTemplateDelimiters: item.isDbTemplateDelimiters,
                            accountId: contextAccountId(),
                            isFile: !item.isDbTemplateDelimiters,
                            publish: item.canWebPublish,
                            usersToGrantAccess: selectUsers,
                            demoData: false,
                            hasSupport: !item.isDbTemplateDelimiters,
                            hasAutoupdate: !item.isDbTemplateDelimiters
                        }
                    ]);

                    if (item.myDatabasesServiceTypeId !== null) {
                        setServiceTypesIdsList([...serviceTypesIdsList, item.myDatabasesServiceTypeId]);
                    }
                }
            } else if (isChecked) {
                setSelectedFileDatabase(selectedFileDatabase.filter(select => select.templateId !== item.id));

                if (item.myDatabasesServiceTypeId !== null) {
                    setServiceTypesIdsList(serviceTypesIdsList.filter(idItem => idItem !== item.myDatabasesServiceTypeId));
                }
            } else {
                setSelectedFileDatabase([
                    ...selectedFileDatabase,
                    {
                        isChecked: true,
                        templateId: item.id,
                        dataBaseName: item.caption
                            .split('')
                            .map(itemOnDelimiters => {
                                if (validate.nameDatabase.test(itemOnDelimiters)) {
                                    return itemOnDelimiters;
                                }

                                return '';
                            })
                            .join(''),
                        dbTemplateDelimiters: false,
                        accountId: contextAccountId(),
                        isFile: true,
                        publish: item.canWebPublish,
                        usersToGrantAccess: selectUsers,
                        demoData: false,
                        hasAutoupdate: true,
                        hasSupport: true
                    }
                ]);

                if (item.myDatabasesServiceTypeId !== null) {
                    setServiceTypesIdsList([...serviceTypesIdsList, item.myDatabasesServiceTypeId]);
                }
            }
        }, [isDbOnDelimiters, selectUsers, selectedFileDatabase, selectedWebDatabase, serviceTypesIdsList, setSelectedFileDatabase, setSelectedWebDatabase, setServiceTypesIdsList]);

        const selectCurrentNameItem = (selectItem: CreateDatabasesModelDcItem[], item: AvailableTemplatesItem, name: string) => {
            return selectItem.map(select => {
                if (select.templateId === item.id) {
                    return {
                        ...select,
                        dataBaseName: name
                    };
                }

                return select;
            });
        };

        const nameHandler = useCallback(async (item: AvailableTemplatesItem, name: string) => {
            if (isDbOnDelimiters) {
                return setSelectedWebDatabase(selectCurrentNameItem(selectedWebDatabase, item, name));
            }

            return setSelectedFileDatabase(selectCurrentNameItem(selectedFileDatabase, item, name));
        }, [isDbOnDelimiters, selectedFileDatabase, selectedWebDatabase, setSelectedFileDatabase, setSelectedWebDatabase]);

        const selectCurrentDemoItem = (selectItem: CreateDatabasesModelDcItem[], item: AvailableTemplatesItem) => {
            return selectItem.map(select => {
                if (select.templateId === item.id) {
                    return {
                        ...select,
                        dataBaseName: select.demoData ? select.dataBaseName.replace(' (Демо)', '') : `${ select.dataBaseName } (Демо)`,
                        demoData: !select.demoData
                    };
                }

                return select;
            });
        };

        const demoHandler = useCallback((item: AvailableTemplatesItem) => {
            if (isDbOnDelimiters) {
                setCurrentStep(prev => prev + 1);
                return setSelectedWebDatabase(selectCurrentDemoItem(selectedWebDatabase, item));
            }

            return setSelectedFileDatabase(selectCurrentDemoItem(selectedFileDatabase, item));
        }, [isDbOnDelimiters, selectedFileDatabase, selectedWebDatabase, setCurrentStep, setSelectedFileDatabase, setSelectedWebDatabase]);

        const selectCurrentWebItem = (selectItem: CreateDatabasesModelDcItem[], item: AvailableTemplatesItem) => {
            return selectItem.map(select => {
                if (select.templateId === item.id) {
                    return {
                        ...select,
                        publish: !select.publish
                    };
                }

                return select;
            });
        };

        const webHandler = useCallback((item: AvailableTemplatesItem) => {
            if (isDbOnDelimiters) {
                return setSelectedWebDatabase(selectCurrentWebItem(selectedWebDatabase, item));
            }

            return setSelectedFileDatabase(selectCurrentWebItem(selectedFileDatabase, item));
        }, [isDbOnDelimiters, selectedFileDatabase, selectedWebDatabase, setSelectedFileDatabase, setSelectedWebDatabase]);

        const selectAutoupdateItem = (selectItem: CreateDatabasesModelDcItem[], item: AvailableTemplatesItem) => {
            return selectItem.map(select => {
                if (select.templateId === item.id) {
                    return {
                        ...select,
                        hasAutoupdate: !select.hasAutoupdate
                    };
                }

                return select;
            });
        };

        const autoupdateHandler = useCallback((item: AvailableTemplatesItem) => {
            if (isDbOnDelimiters) {
                return setSelectedWebDatabase(selectAutoupdateItem(selectedWebDatabase, item));
            }

            return setSelectedFileDatabase(selectAutoupdateItem(selectedFileDatabase, item));
        }, [isDbOnDelimiters, selectedFileDatabase, selectedWebDatabase, setSelectedFileDatabase, setSelectedWebDatabase]);

        const autoupdateChange = (item: AvailableTemplatesItem, checked: boolean) => {
            setSelectedItem(item);

            if (firstShow.autoUpdate && checked) {
                setFirstShow(prev => ({ ...prev, autoUpdate: false }));
                setShowWarning(prev => ({ ...prev, autoUpdate: true }));
            } else {
                autoupdateHandler(item);
            }
        };

        const selectSupportItem = (selectItem: CreateDatabasesModelDcItem[], item: AvailableTemplatesItem) => {
            return selectItem.map(select => {
                if (select.templateId === item.id) {
                    return {
                        ...select,
                        hasSupport: !select.hasSupport
                    };
                }

                return select;
            });
        };

        const supportHandler = useCallback((item: AvailableTemplatesItem) => {
            if (isDbOnDelimiters) {
                return setSelectedWebDatabase(selectSupportItem(selectedWebDatabase, item));
            }

            return setSelectedFileDatabase(selectSupportItem(selectedFileDatabase, item));
        }, [isDbOnDelimiters, selectedFileDatabase, selectedWebDatabase, setSelectedFileDatabase, setSelectedWebDatabase]);

        const supportChange = (item: AvailableTemplatesItem, checked: boolean) => {
            setSelectedItem(item);

            if (firstShow.support && checked) {
                setFirstShow(prev => ({ ...prev, support: false }));
                setShowWarning(prev => ({ ...prev, support: true }));
            } else {
                supportHandler(item);
            }
        };

        const closeWarningModal = () => {
            setShowWarning({ autoUpdate: false, support: false });
            setSelectedItem(null);
        };

        return (
            <>
                {
                    isDbOnDelimiters
                        ? (
                            <CommonTableWithFilter
                                tourId={ ETourId.createDatabasePageTemplate }
                                uniqueContextProviderStateId="configure1cTemplate"
                                tableProps={ {
                                    dataset: dataset ?? [],
                                    keyFieldName: 'id',
                                    isResponsiveTable: true,
                                    fieldsView: {
                                        id: {
                                            caption: '',
                                            fieldWidth: '5%',
                                            format: (_, item) =>
                                                <Checkbox
                                                    onChange={ () => addDatabase(item, !!selectedWebDatabase.filter(selectItem => selectItem.templateId === item.id).length) }
                                                    checked={ !!selectedWebDatabase.filter(selectItem => selectItem.templateId === item.id).length }
                                                />
                                        },
                                        caption: {
                                            caption: 'Шаблон',
                                            isSortable: false,
                                            fieldWidth: '25%',
                                            format: (value, item) =>
                                                <Box display="flex" gap="8px" alignItems="center">
                                                    <HomeSvgSelector width={ 36 } height={ 36 } id={ item.templateImgUrl } />
                                                    <TextOut fontSize={ 14 } fontWeight={ 700 } style={ { wordBreak: 'normal' } }>
                                                        {
                                                            isDbOnDelimiters
                                                                ? item.isDbTemplateDelimiters ? `${ item.captionOnDelimiters } (${ item.versionTemplate })` : value
                                                                : value
                                                        }
                                                    </TextOut>
                                                </Box>
                                        },
                                        versionTemplate: {
                                            caption: 'Название',
                                            fieldWidth: '35',
                                            format: (_, item) => {
                                                const currentDatabase = selectedWebDatabase.find(database => database.templateId === item.id);

                                                if (currentDatabase) {
                                                    return (
                                                        <InputNameDt
                                                            name={ currentDatabase.dataBaseName }
                                                            setName={ name => nameHandler(item, name) }
                                                        />
                                                    );
                                                }

                                                return null;
                                            }
                                        },
                                        isDemoDataAvailable: {
                                            caption: 'Демо-данные',
                                            fieldWidth: '5%',
                                            format: (value, item) => {
                                                if (item.isDemoDataAvailable && selectedWebDatabase.filter(selectItem => selectItem.templateId === item.id).length) {
                                                    return (
                                                        <Checkbox
                                                            data-tourid={ ETourId.createDatabasePageDemoCheckbox }
                                                            checked={ selectedWebDatabase.filter(selectItem => selectItem.templateId === item.id)[0].demoData }
                                                            onChange={ () => demoHandler(item) }
                                                            sx={ { marginLeft: '25px' } }
                                                        />
                                                    );
                                                }

                                                return null;
                                            }
                                        },
                                        canWebPublish: {
                                            caption: 'Web доступ',
                                            fieldWidth: '5%',
                                            format: (_, item) => {
                                                if (selectedWebDatabase.filter(selectItem => selectItem.templateId === item.id).length && item.canWebPublish) {
                                                    return (
                                                        <Checkbox
                                                            checked={ selectedWebDatabase.filter(selectItem => selectItem.templateId === item.id)[0].publish }
                                                            disabled={ item.isDbTemplateDelimiters }
                                                            onChange={ () => webHandler(item) }
                                                            sx={ {
                                                                marginLeft: '20px'
                                                            } }
                                                        />
                                                    );
                                                }

                                                return null;
                                            }
                                        },
                                        myDatabasesServiceTypeId: {
                                            caption: 'АО',
                                            fieldWidth: '5%',
                                            format: (_, item) => {
                                                if (selectedWebDatabase.filter(selectItem => selectItem.templateId === item.id).length) {
                                                    const checked = selectedWebDatabase.filter(selectItem => selectItem.templateId === item.id)[0].hasAutoupdate;

                                                    return (
                                                        <Checkbox
                                                            disabled={ item.isDbTemplateDelimiters }
                                                            checked={ selectedWebDatabase.filter(selectItem => selectItem.templateId === item.id)[0].hasAutoupdate }
                                                            onChange={ () => autoupdateChange(item, checked) }
                                                        />
                                                    );
                                                }

                                                return null;
                                            }
                                        },
                                        templateImgUrl: {
                                            caption: 'ТиИ',
                                            fieldWidth: '5%',
                                            format: (_, item) => {
                                                if (selectedWebDatabase.filter(selectItem => selectItem.templateId === item.id).length) {
                                                    const checked = selectedWebDatabase.filter(selectItem => selectItem.templateId === item.id)[0].hasSupport;
                                                    return (
                                                        <Checkbox
                                                            disabled={ item.isDbTemplateDelimiters }
                                                            checked={ checked }
                                                            onChange={ () => supportChange(item, checked) }
                                                        />
                                                    );
                                                }

                                                return null;
                                            }
                                        }
                                    }
                                } }
                            />
                        )
                        : (
                            <CommonTableWithFilter
                                uniqueContextProviderStateId="configure1cTemplate"
                                tableProps={ {
                                    dataset: dataset ?? [],
                                    keyFieldName: 'id',
                                    fieldsView: {
                                        id: {
                                            caption: '',
                                            fieldWidth: '5%',
                                            format: (_, item) =>
                                                <Checkbox
                                                    onChange={ () => addDatabase(item, !!selectedFileDatabase.filter(selectItem => selectItem.templateId === item.id).length) }
                                                    checked={ !!selectedFileDatabase.filter(selectItem => selectItem.templateId === item.id).length }
                                                />
                                        },
                                        caption: {
                                            caption: 'Шаблон',
                                            isSortable: false,
                                            fieldWidth: '25%',
                                            format: (value, item) =>
                                                <Box display="flex" gap="8px" alignItems="center">
                                                    <HomeSvgSelector width={ 36 } height={ 36 } id={ item.templateImgUrl } />
                                                    <TextOut fontSize={ 14 } fontWeight={ 700 }>{ value }</TextOut>
                                                </Box>
                                        },
                                        versionTemplate: {
                                            caption: 'Название',
                                            fieldWidth: '35',
                                            format: (_, item) => {
                                                const currentDatabase = selectedFileDatabase.find(database => database.templateId === item.id);

                                                if (currentDatabase) {
                                                    return (
                                                        <InputNameDt
                                                            name={ currentDatabase.dataBaseName }
                                                            setName={ name => nameHandler(item, name) }
                                                        />
                                                    );
                                                }

                                                return null;
                                            }
                                        },
                                        isDemoDataAvailable: {
                                            caption: 'Демо-данные',
                                            fieldWidth: '5%',
                                            format: (_, item) => {
                                                if (selectedFileDatabase.filter(selectItem => selectItem.templateId === item.id).length) {
                                                    return (
                                                        <Checkbox
                                                            checked={ selectedFileDatabase.filter(selectItem => selectItem.templateId === item.id)[0].demoData }
                                                            onChange={ () => demoHandler(item) }
                                                            sx={ {
                                                                marginLeft: '25px'
                                                            } }
                                                        />
                                                    );
                                                }

                                                return null;
                                            }
                                        },
                                        canWebPublish: {
                                            caption: 'Web доступ',
                                            fieldWidth: '5%',
                                            format: (_, item) => {
                                                if (selectedFileDatabase.filter(selectItem => selectItem.templateId === item.id).length) {
                                                    return (
                                                        <Checkbox
                                                            checked={ selectedFileDatabase.filter(selectItem => selectItem.templateId === item.id)[0].publish }
                                                            onChange={ () => webHandler(item) }
                                                            sx={ {
                                                                marginLeft: '20px'
                                                            } }
                                                        />
                                                    );
                                                }

                                                return null;
                                            }
                                        },
                                        myDatabasesServiceTypeId: {
                                            caption: 'АО',
                                            fieldWidth: '5%',
                                            format: (_, item) => {
                                                if (selectedFileDatabase.filter(selectItem => selectItem.templateId === item.id).length) {
                                                    const checked = selectedFileDatabase.filter(selectItem => selectItem.templateId === item.id)[0].hasAutoupdate;

                                                    return (
                                                        <Checkbox
                                                            checked={ checked }
                                                            onChange={ () => autoupdateChange(item, checked) }
                                                        />
                                                    );
                                                }

                                                return null;
                                            }
                                        },
                                        templateImgUrl: {
                                            caption: 'ТиИ',
                                            fieldWidth: '5%',
                                            format: (_, item) => {
                                                if (selectedFileDatabase.filter(selectItem => selectItem.templateId === item.id).length) {
                                                    const checked = selectedFileDatabase.filter(selectItem => selectItem.templateId === item.id)[0].hasSupport;
                                                    return (
                                                        <Checkbox
                                                            checked={ checked }
                                                            onChange={ () => supportChange(item, checked) }
                                                        />
                                                    );
                                                }

                                                return null;
                                            }
                                        }
                                    }
                                } }
                            />
                        )
                }
                <Dialog
                    title="Внимание!"
                    isOpen={ showWarning.autoUpdate }
                    dialogVerticalAlign="center"
                    dialogWidth="sm"
                    onCancelClick={ closeWarningModal }
                    buttons={ [
                        {
                            content: 'Отмена',
                            kind: 'default',
                            onClick: closeWarningModal
                        },
                        {
                            content: 'Отключить',
                            kind: 'error',
                            onClick: () => {
                                if (selectedItem) {
                                    autoupdateHandler(selectedItem);
                                }
                                closeWarningModal();
                            }
                        }
                    ] }
                >
                    <TextOut>
                        Уважаемый пользователь. Рекомендуем оставить функцию &quot;Автоматического обновления&quot; включенной.&nbsp;
                        Она не только помогает поддерживать базу в актуальном состоянии, но обеспечивает более надежную и эффективную работу базы 1С.
                    </TextOut>
                </Dialog>
                <Dialog
                    title="Внимание!"
                    isOpen={ showWarning.support }
                    dialogVerticalAlign="center"
                    dialogWidth="sm"
                    onCancelClick={ closeWarningModal }
                    buttons={ [
                        {
                            content: 'Отмена',
                            kind: 'default',
                            onClick: closeWarningModal
                        },
                        {
                            content: 'Отключить',
                            kind: 'error',
                            onClick: () => {
                                if (selectedItem) {
                                    supportHandler(selectedItem);
                                }
                                closeWarningModal();
                            }
                        }
                    ] }
                >
                    <TextOut>
                        Уважаемый пользователь. Рекомендуем оставить функцию &quot;Тестирования и исправления ошибок&quot; включенной.&nbsp;
                        Она не только помогает устранять ошибки в работе информационной базы, но и способствует повышению скорости её работы.
                    </TextOut>
                </Dialog>
            </>
        );
    };