import { Box } from '@mui/material';
import { AppRoutes } from 'app/AppRoutes';
import { useTour } from '@reactour/tour';
import { useDebounceCallback } from 'app/hooks';
import { TextOut } from 'app/views/components/TextOut';
import { UserTableDistribution } from 'app/views/modules/CreateDatabase/LoadingDatabase/view/UserTableDistributionRestore';
import { ConfigurationTabs } from 'app/views/modules/CreateDatabase/CreateDb/views/ConfigurationTabs';
import React, { useCallback, useEffect, useState } from 'react';
import { CancelOrCreateDb } from 'app/views/modules/CreateDatabase/CreateDb/views/CancelOrCreateDb';
import { FETCH_API } from 'app/api/useFetchApi';
import { CreateDatabasesModelDcItem } from 'app/api/endpoints/createDb/request/postFromTemplateRequest';
import { AvailableTemplatesItem, AvailableUsersForGrantAccessItem } from 'app/api/endpoints/createDb/response';
import { AccountUserGroup } from 'app/common/enums';
import { AppReduxStoreState } from 'app/redux/types';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { withHeader } from 'app/views/components/_hoc/withHeader';
import { Dialog } from 'app/views/components/controls/Dialog';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { CalculateAccessInfoDbDataModelItem } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/CalculateAccessInfoDbDataModel';
import { CostOfCreatingDatabasesModel } from 'app/web/InterlayerApiProxy/MsBackupsApiProxy/costOfCreatingDatabases';
import { RequestKind } from 'core/requestSender/enums';
import { connect } from 'react-redux';
import { useHistory, useLocation } from 'react-router';
import { LocationProps } from './types';

const api = FETCH_API.CRETE_DB;
const { postActivateRentIfNeed } = FETCH_API.CRETE_DB;
const costOrCreateApi = InterlayerApiProxy.getMsBackupsApiProxy();
const accessPriceApi = InterlayerApiProxy.getInfoDbCardApi();

type StateProps = {
    currentUserGroups: AccountUserGroup[];
};

const CreateDb = ({ currentUserGroups, floatMessage: { show } }: FloatMessageProps & StateProps) => {
    const { isVip, isDbOnDelimiters } = useLocation<LocationProps>().state || { isVip: false, isDbOnDelimiters: true };
    const history = useHistory();
    const { setCurrentStep, isOpen: isOpenTour } = useTour();

    const [isLoading, setIsLoading] = useState(false);
    const [allUsers, setAllUsers] = useState<AvailableUsersForGrantAccessItem[]>([]);
    const [usersForAdd, setUsersForAdd] = useState<string[]>([]);
    const [email, setEmail] = useState<string[]>([]);
    const [selectedDatabases, setSelectedDatabases] = useState<CreateDatabasesModelDcItem[]>([]);
    const [selectedWebDatabases, setSelectedWebDatabases] = useState<CreateDatabasesModelDcItem[]>([]);
    const [selectedFileDatabases, setSelectedFileDatabases] = useState<CreateDatabasesModelDcItem[]>([]);
    const [isOpen, setIsOpen] = useState(false);
    const [webDataset, setWebDataset] = useState<AvailableTemplatesItem[]>([]);
    const [fileDataset, setFileDataset] = useState<AvailableTemplatesItem[]>([]);
    const [costOrCreating, setCostOrCreating] = useState<CostOfCreatingDatabasesModel>();
    const [serviceTypesIdsList, setServiceTypesIdsList] = useState<(string | null)[]>([]);
    const [accessCost, setAccessCost] = useState<CalculateAccessInfoDbDataModelItem[]>([]);
    const [totalSum, setTotalSum] = useState(0);

    useEffect(() => {
        void postActivateRentIfNeed();
    }, []);

    const handleUserForAdd = useCallback((users: string[]) => {
        setUsersForAdd(users);
        setSelectedWebDatabases(selectedWebDatabases.map(item => {
            return {
                ...item,
                usersToGrantAccess: users
            };
        }));
        setSelectedFileDatabases(selectedFileDatabases.map(item => {
            return {
                ...item,
                usersToGrantAccess: users
            };
        }));
    }, [selectedWebDatabases, selectedFileDatabases]);

    const handleEmail = useCallback((emailList: string[]) => setEmail(emailList), []);

    const getTemplates = useCallback((isDelimiters?: boolean) => {
        setIsLoading(true);
        api.getTemplates({
            onlyTemplatesOnDelimiters: isDelimiters ?? false
        }).then(({ success, data, message }) => {
            if (success) {
                if (data?.availableTemplates.length === 0) {
                    api.getTemplates({ onlyTemplatesOnDelimiters: false })
                        .then(response => {
                            setFileDataset(response.data?.availableTemplates.filter(item => !item.isDbTemplateDelimiters) ?? []);
                        });
                } else {
                    setWebDataset(data?.availableTemplates ?? []);
                    setFileDataset(data?.availableTemplates ?? []);
                }
                setIsLoading(false);
                setAllUsers(data?.availableUsersForGrantAccess ?? []);
                if (data?.availableUsersForGrantAccess) {
                    setUsersForAdd(data?.availableUsersForGrantAccess.flatMap(item => {
                        if (item.hasAccess) {
                            return item.userId;
                        }

                        return [];
                    }));
                }
            } else {
                show(MessageType.Error, message);
            }
        });
    }, [show]);

    const getTemplatesMore = useCallback((isDelimiters?: boolean) => {
        setIsLoading(true);
        api.getTemplates({
            onlyTemplatesOnDelimiters: isDelimiters ?? false
        }).then(({ success, data, message }) => {
            setIsLoading(false);
            if (success) {
                setWebDataset(data?.availableTemplates ?? []);
            } else {
                show(MessageType.Error, message);
            }
        });
    }, [show]);

    const createDb = async (isPromise?: boolean) => {
        setIsLoading(true);
        setIsOpen(false);

        const { success, data, message } = await api.postFromTemplate({
            needUsePromisePayment: isPromise ?? false,
            createDatabasesModelDc: selectedDatabases
        });

        if (!success) {
            show(MessageType.Error, `При создании ${ selectedDatabases.length > 1 ? 'баз' : 'базы' } произошла ошибка. ${ message }`);
        }
        if (data?.notEnoughMoney !== 0) {
            show(MessageType.Error, `На создание ${ selectedDatabases.length > 1 ? 'баз' : 'базы' } не хватает ${ data?.notEnoughMoney } ${ costOrCreating?.data.currency }.`);
        }
    };

    const openDialog = () => setIsOpen(true);

    const closeDialog = () => setIsOpen(false);

    const createNow = () => {
        if (isOpenTour) {
            setCurrentStep(prev => {
                history.push(AppRoutes.accountManagement.informationBases);
                return prev + 1;
            });
        } else {
            openDialog();
        }
    };

    useEffect(() => {
        setSelectedDatabases([...selectedFileDatabases, ...selectedWebDatabases]);
    }, [selectedWebDatabases, selectedFileDatabases]);

    useEffect(() => {
        if (isVip && !isDbOnDelimiters) {
            return getTemplates(false);
        }

        if (isVip && isDbOnDelimiters) {
            return getTemplates(true);
        }

        return getTemplates(true);
    }, [getTemplates, isDbOnDelimiters, isVip]);

    const handleTurnRent = useCallback(async () => {
        setIsLoading(true);
        const response = await FETCH_API.RENT.turnOnRent();
        if (response.success) {
            history.push(AppRoutes.accountManagement.createAccountDatabase.createDb);
        } else {
            setIsLoading(false);
            show(MessageType.Error, response.message);
        }
    }, [history, show]);

    const getCostOrCreating = useCallback(async () => {
        const response = await costOrCreateApi.costOrCreatingDatabases(
            RequestKind.SEND_BY_USER_ASYNCHRONOUSLY,
            selectedDatabases.length.toString()
        );

        if (response.success) {
            setCostOrCreating(response);
        } else if (response.message === 'Не удалось получить данные для калькуляции услуги \'Информационные базы\'') {
            await handleTurnRent();
        } else {
            show(MessageType.Error, response.message);
        }
    }, [selectedDatabases, handleTurnRent, show]);

    const debouncedGetCostOrCreating = useDebounceCallback(getCostOrCreating);

    useEffect(() => {
        debouncedGetCostOrCreating();
    }, [debouncedGetCostOrCreating]);

    useEffect(() => {
        try {
            accessPriceApi.CalculateAccesses(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, {
                accountUserDataModelsList: usersForAdd.map(item => {
                    return {
                        UserId: item,
                        HasAccess: allUsers.filter(userItem => userItem.userId === item)[0].hasAccess,
                        HasLicense: true
                    };
                }),
                serviceTypesIdsList
            }).then(response => setAccessCost(response));
        } catch (er: unknown) {
            show(MessageType.Error, 'При расчете стоимости добавления пользователя произошла ошибка');
        }
    }, [allUsers, serviceTypesIdsList, show, usersForAdd]);

    useEffect(() => {
        if (costOrCreating) {
            const accessSum = accessCost.reduce((previousValue, currentValue) => previousValue + currentValue.accessCost, 0);

            setTotalSum(costOrCreating.data.costOftariff + accessSum);
        }
    }, [accessCost, costOrCreating]);

    return (
        <>
            {
                isLoading && <LoadingBounce />
            }
            <CancelOrCreateDb
                onClick={ costOrCreating && (totalSum <= costOrCreating.data.balance) && costOrCreating.data.enoughMoney === 0
                    ? createNow
                    : openDialog
                }
                possibilityToCreate={ selectedDatabases.length === 0 }
                textButton={ `Создать${ costOrCreating && totalSum > 0
                    ? ` (${ totalSum } ${ costOrCreating.data.currency })`
                    : ''
                }` }
            />
            <Box display="flex" flexDirection="column">
                <TextOut fontSize={ 14 }>Выберите способ создания информационной базы</TextOut>
                <ConfigurationTabs
                    currentUserGroups={ currentUserGroups }
                    isVip={ !!isVip }
                    isDbOnDelimiters={ isDbOnDelimiters }
                    webDataset={ webDataset }
                    fileDataset={ fileDataset }
                    onDataSelect={ getTemplatesMore }
                    selectedFileDatabase={ selectedFileDatabases }
                    selectedWebDatabase={ selectedWebDatabases }
                    setSelectedFileDatabase={ setSelectedFileDatabases }
                    setSelectedWebDatabase={ setSelectedWebDatabases }
                    selectUsers={ usersForAdd }
                    serviceTypesIdsList={ serviceTypesIdsList }
                    setServiceTypesIdsList={ setServiceTypesIdsList }
                />
                <br />
                <TextOut fontSize={ 14 }>Настройте доступы</TextOut>
                <UserTableDistribution
                    select={ handleUserForAdd }
                    adminEmail={ handleEmail }
                    initSelect={ usersForAdd }
                />
            </Box>
            {
                costOrCreating && (
                    <Dialog
                        isOpen={ isOpen }
                        dialogWidth="sm"
                        dialogVerticalAlign="center"
                        title={ `Создание ${ selectedDatabases.length > 1 ? 'баз' : 'базы' }` }
                        onCancelClick={ closeDialog }
                        buttons={
                            totalSum <= costOrCreating.data.balance && costOrCreating.data.enoughMoney === 0
                                ? [
                                    {
                                        variant: 'contained',
                                        kind: 'success',
                                        content: 'Создать',
                                        onClick: async () => {
                                            await createDb();

                                            history.push(AppRoutes.accountManagement.informationBases);
                                        }
                                    }
                                ]
                                : costOrCreating.data.canGetPromisePayment
                                    ? [
                                        {
                                            variant: 'text',
                                            content: 'Отмена',
                                            kind: 'default',
                                            onClick: closeDialog
                                        },
                                        {
                                            variant: 'contained',
                                            kind: 'primary',
                                            content:
                                                `Взять обещанный платеж (${
                                                    costOrCreating.data.enoughMoney + accessCost.reduce((previousValue, currentValue) => previousValue + currentValue.accessCost, 0)
                                                } ${ costOrCreating.data.currency })`,
                                            onClick: async () => {
                                                await createDb(true);

                                                history.push(AppRoutes.accountManagement.informationBases);
                                            }
                                        },
                                        {
                                            variant: 'contained',
                                            kind: 'primary',
                                            content: `Пополнить счет на (${
                                                costOrCreating.data.enoughMoney + accessCost.reduce((previousValue, currentValue) => previousValue + currentValue.accessCost, 0)
                                            } ${ costOrCreating.data.currency })`,
                                            onClick: () => history.push(
                                                `${ AppRoutes.accountManagement.replenishBalance }?customPayment.amount=${
                                                    totalSum === costOrCreating.data.enoughMoney ? totalSum : totalSum - costOrCreating.data.enoughMoney
                                                }&customPayment.description=Оплата+за+создание+ИБ`
                                            )
                                        }
                                    ]
                                    : [
                                        {
                                            variant: 'text',
                                            content: 'Отмена',
                                            kind: 'default',
                                            onClick: closeDialog
                                        },
                                        {
                                            variant: 'contained',
                                            kind: 'primary',
                                            content: `Пополнить счет на (${
                                                costOrCreating.data.enoughMoney + accessCost.reduce((previousValue, currentValue) => previousValue + currentValue.accessCost, 0)
                                            } ${ costOrCreating.data.currency })`,
                                            onClick: () => history.push(
                                                `${ AppRoutes.accountManagement.replenishBalance }?customPayment.amount=${
                                                    costOrCreating.data.enoughMoney + accessCost.reduce((previousValue, currentValue) => previousValue + currentValue.accessCost, 0)
                                                }&customPayment.description=Оплата+за+создание+ИБ`
                                            )
                                        }
                                    ]
                        }
                    >
                        Производится создание { selectedDatabases.length > 1 ? 'баз' : 'базы' }.<br />
                        После завершения процесса Вам на эл. почту { email.map((item, index) => {
                            if (index !== email.length - 1) {
                                return <TextOut key={ item } fontSize={ 14 } style={ { color: '#337ab7' } }>{ item }, </TextOut>;
                            }
                            return <TextOut key={ item } fontSize={ 14 } style={ { color: '#337ab7' } }>{ item } </TextOut>;
                        }) } будет отправлено сообщение.
                    </Dialog>
                )
            }
        </>
    );
};

const CreateDbViewConnected = connect<StateProps, object, object, AppReduxStoreState>(
    state => {
        const hasSessionSettingsReceived = state.Global.getCurrentSessionSettingsReducer.hasSuccessFor.hasSettingsReceived;
        const currentUserGroups = hasSessionSettingsReceived ? state.Global.getCurrentSessionSettingsReducer.settings.currentContextInfo.currentUserGroups : [];
        return {
            currentUserGroups,
        };
    },
    {
    }
)(CreateDb);

export const CreateDbView = withHeader({
    title: 'Создание информационной базы',
    page: withFloatMessages(CreateDbViewConnected)
});