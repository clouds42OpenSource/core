export type LocationProps = {
    isVip?: boolean | null;
    isDbOnDelimiters: boolean;
};