export type Props = {
    possibilityToCreate: boolean;
    textButton: string;
    onClick: () => void;
};