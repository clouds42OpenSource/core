import React from 'react';

export type StepsType = {
    /**
     * Заголовок шага
     */
    label: string;
    /**
     * Контент шага
     */
    description: string | React.ReactComponentElement<any> | HTMLImageElement;
};

export type Props = {
    /**
     * Массив шагов
     */
    steps: StepsType[];
    /**
     * Шаг, на котором кнопка "Продолжить" обрабатывается особым образом
     */
    rules: number[];
    /**
     * Действие для кнопки "Завершить"
     */
    finalizeStep: () => void;
    /**
     * Текст кнопки на последнем шагу
     */
    finalizeStepContent: string;
};