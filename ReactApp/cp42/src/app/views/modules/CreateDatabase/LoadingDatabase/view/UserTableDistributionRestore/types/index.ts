export type Props = {
    /**
     * Выбранные id
     */
    select: (users: string[]) => void;
    /**
     * Список почт админов
     */
    adminEmail: (email: string[]) => void;
    /**
     * Предзаполнение таблицы
     */
    initSelect: string[];
};