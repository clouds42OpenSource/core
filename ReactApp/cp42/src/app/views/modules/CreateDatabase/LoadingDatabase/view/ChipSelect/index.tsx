import Box from '@mui/material/Box';
import Checkbox from '@mui/material/Checkbox';
import Chip from '@mui/material/Chip';
import FormControl from '@mui/material/FormControl';
import ListItemText from '@mui/material/ListItemText';
import MenuItem from '@mui/material/MenuItem';
import OutlinedInput from '@mui/material/OutlinedInput';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import { TAccountProfile } from 'app/api/endpoints/accountUsers/response';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { ProfilesModel } from 'app/web/InterlayerApiProxy/MsBackupsApiProxy/getFileInfoById';
import React from 'react';
import cn from 'classnames';
import { SxProps } from '@mui/system';
import { Theme } from '@mui/material';
import css from './style.module.css';

type StateProps = {
    key?: string;
    value: string[];
    multiple: boolean;
    defaultValue: string[];
    isChip: boolean;
    rowKey: string;
    callback: (rowKey: string, value: string[]) => void;
    getDecorationText: (value: string) => string;
    sx?: SxProps<Theme>;
    allProfiles?: ProfilesModel[];
    isFirstShowWarning?: boolean;
    setIsFirstShowWarning?: () => void;
    isProfileSelect?: boolean;
};

type OwnState = {
    value: string[];
    isOpened: boolean;
};

type AllProps = StateProps & FloatMessageProps;

type TRole = Omit<TAccountProfile, 'databases'>;

class ChipSelectClass extends React.Component<AllProps, OwnState> {
    public constructor(props: AllProps) {
        super(props);

        this.handleChange = this.handleChange.bind(this);
        this.handleOpen = this.handleOpen.bind(this);
        this.handleClose = this.handleClose.bind(this);

        this.state = {
            value: this.props.defaultValue,
            isOpened: false,
        };
    }

    public shouldComponentUpdate(_nextProps: Readonly<StateProps>, nextState: Readonly<OwnState>) {
        if (!nextState.isOpened && JSON.stringify(this.props.defaultValue) !== JSON.stringify(nextState.value)) {
            this.props.callback(this.props.rowKey, nextState.value);

            return true;
        }

        return true;
    }

    private handleChange(event: SelectChangeEvent<string[]>) {
        const { target: { value } } = event;

        if (this.props.isProfileSelect) {
            const normalizeValue = typeof value === 'string' ? value.split(',') : value;

            if (!this.props.isChip) {
                this.setState(prevState => ({
                    value: prevState.value.filter(item => item !== value)
                }));
            }

            const { adminRole, userRole } = (this.props.allProfiles ?? []).reduce<{userRole: TRole[], adminRole: TRole[]}>((profiles, profile) => {
                if (normalizeValue.find(item => item === profile.id)) {
                    if (profile.admin) {
                        profiles.adminRole.push(profile);
                    } else {
                        profiles.userRole.push(profile);
                    }
                }

                return profiles;
            }, { adminRole: [], userRole: [] });

            if (value.length > 1 && this.props.isFirstShowWarning && adminRole.length && userRole.length) {
                this.props.floatMessage.show(MessageType.Info, 'Установка профиля доступа "Администратор" одновременно с другими профилями через личный кабинет не поддерживается');

                if (this.props.setIsFirstShowWarning) {
                    this.props.setIsFirstShowWarning();
                }
            }

            if (this.state.value.some(profile => this.props.allProfiles?.find(pr => pr.id === profile)?.admin)) {
                this.setState({
                    value: (userRole.length ? userRole : adminRole).map(profileItem => profileItem.id)
                });
            } else {
                this.setState({
                    value: (adminRole.length ? adminRole : userRole).map(profileItem => profileItem.id)
                });
            }
        } else {
            if (!this.props.isChip) {
                this.setState(prevState => ({
                    value: prevState.value.filter(item => item !== value)
                }));
            }

            this.setState({
                value: typeof value === 'string' ? value.split(',') : value
            });
        }
    }

    private handleClose() {
        this.setState({
            isOpened: false
        });
    }

    private handleOpen() {
        this.setState({
            isOpened: true
        });
    }

    public render() {
        return (
            <FormControl sx={ { m: 1 } } size="small" className={ cn(css.chipSelect) }>
                { this.props.isChip
                    ? (
                        <Select
                            key={ this.props.key }
                            displayEmpty={ true }
                            multiple={ this.props.multiple }
                            value={ this.state.value }
                            onChange={ this.handleChange }
                            onClose={ this.handleClose }
                            onOpen={ this.handleOpen }
                            input={ <OutlinedInput label={ false } /> }
                            renderValue={ selected => (
                                <Box sx={ { display: 'flex', flexWrap: 'wrap', gap: 0.5 } }>
                                    { selected.map(value => (
                                        <Chip key={ value } label={ this.props.getDecorationText(value) } size="small" />
                                    )) }
                                </Box>
                            ) }
                            sx={ { minWidth: '250px', maxWidth: '650px', ...this.props.sx } }
                        >
                            { this.props.value.map(item => (
                                <MenuItem key={ item } value={ item }>
                                    <Checkbox checked={ this.state.value.indexOf(item) > -1 } />
                                    <ListItemText primary={ this.props.getDecorationText(item) } />
                                </MenuItem>
                            )) }
                        </Select>
                    )
                    : (
                        <Select
                            displayEmpty={ true }
                            multiple={ this.props.multiple }
                            value={ this.state.value }
                            defaultValue={ this.props.defaultValue }
                            onChange={ this.handleChange }
                            onClose={ this.handleClose }
                            onOpen={ this.handleOpen }
                            input={ <OutlinedInput label={ false } /> }
                            sx={ { m: 1, minWidth: '250px', maxWidth: '650px' } }
                        >
                            <MenuItem value="">
                                <span style={ { opacity: '0.75' } }>
                                    { this.state.value.join()
                                        ? 'Не указан'
                                        : this.props.getDecorationText('')
                                    }
                                </span>
                            </MenuItem>
                            { this.props.value.map(item => (
                                <MenuItem key={ item } value={ item }>
                                    { this.props.getDecorationText(item) }
                                </MenuItem>
                            )) }
                        </Select>
                    )
                }
            </FormControl>
        );
    }
}

export const ChipSelect = withFloatMessages(ChipSelectClass);