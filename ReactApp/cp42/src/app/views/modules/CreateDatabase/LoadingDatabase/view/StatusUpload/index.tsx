import CircularProgress from '@mui/material/CircularProgress';
import { Box } from '@mui/system';
import { COLORS } from 'app/utils';
import { TextOut } from 'app/views/components/TextOut';
import style from './style.module.css';
import { Props } from './types';

export const StatusUpload = ({ status }: Props) => {
    switch (status) {
        case 'Файл в процессе загрузки':
            return (
                <Box display="flex" gap="5px" alignItems="flex-start">
                    <CircularProgress sx={ { color: COLORS.main } } size="15px" />
                    <TextOut>
                        Идет загрузка архивной копии из хранилища. Пожалуйста, подождите.
                        <TextOut fontWeight={ 700 }> При перезагрузке страницы процесс начнется заново!</TextOut>
                    </TextOut>
                </Box>
            );
        case 'Файл загружен успешно':
            return (
                <TextOut className={ style.success }>
                    <i className="fa fa-check" /> Загрузка архивной копии из хранилища успешно завершена.
                </TextOut>
            );
        default:
            return (
                <TextOut className={ style.error }>
                    <i className="fa fa-exclamation-triangle" /> При загрузке файла архивной копии произошла ошибка ({ status }).
                </TextOut>
            );
    }
};