import React from 'react';
import Box from '@mui/material/Box';
import { TextOut } from 'app/views/components/TextOut';
import { Link } from 'react-router-dom';
import { AppRoutes } from 'app/AppRoutes';
import { BaseButton } from 'app/views/components/controls/Button/BaseButton';
import { StateProps } from 'app/views/modules/CreateDatabase/LoadingDatabase/view/EnoughMoneyButton/types';

export const EnoughMoneyButton = ({ enoughMoney, loading, currency, costOrCreating, setIsPromise }: StateProps) => {
    if (loading && enoughMoney > 0) {
        return (
            <Box display="flex" flexDirection="column" gap="16px">
                <TextOut>Сумма { enoughMoney } { currency } будет списана с баланса за создание ИБ с архива</TextOut>
                <Box display="flex" flexDirection="row">
                    <Link to={ AppRoutes.accountManagement.informationBases } style={ { textDecoration: 'none' } }>
                        <BaseButton kind="default">Отмена</BaseButton>
                    </Link>
                    <Link
                        to={ {
                            pathname: AppRoutes.accountManagement.replenishBalance,
                            search: `?customPayment.amount=${ enoughMoney }&customPayment.description=Оплата+за+создание+ИБ+из+архива`
                        } }
                        style={ { textDecoration: 'none' } }
                    >
                        <BaseButton kind="primary">Пополнить счет на ({ enoughMoney } { currency })</BaseButton>
                    </Link>
                    { costOrCreating?.data.canGetPromisePayment && <BaseButton kind="primary" onClick={ () => setIsPromise(true) }>Обещанный платеж</BaseButton> }
                </Box>
            </Box>
        );
    }

    return null;
};