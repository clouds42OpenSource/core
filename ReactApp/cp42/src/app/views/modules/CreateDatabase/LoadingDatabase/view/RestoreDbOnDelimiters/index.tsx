import LinearProgress from '@mui/material/LinearProgress';
import { AppRoutes } from 'app/AppRoutes';
import { DateUtility } from 'app/utils/DateUtility';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { Dialog } from 'app/views/components/controls/Dialog';
import { TextOut } from 'app/views/components/TextOut';
import { ArchiveVerification } from 'app/views/modules/CreateDatabase/LoadingDatabase/view/ArchiveVerification';
import css from 'app/views/modules/CreateDatabase/LoadingDatabase/view/CreateDb/style.module.css';
import { VerticalLinearStepper } from 'app/views/modules/CreateDatabase/LoadingDatabase/view/Stepper';
import { UserMappingTable } from 'app/views/modules/CreateDatabase/LoadingDatabase/view/UserMappingTable';
import { InputNameDt } from 'app/views/modules/CreateDatabase/views/InputNameDt';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { FileInfoByIdModel } from 'app/web/InterlayerApiProxy/MsBackupsApiProxy/getFileInfoById';
import { UploadFileAccessPriceModel } from 'app/web/InterlayerApiProxy/MsBackupsApiProxy/uploadFileAccessPrice';
import { UserItemInputParams } from 'app/web/InterlayerApiProxy/MsBackupsApiProxy/zipDatabase';
import { RequestKind } from 'core/requestSender/enums';
import React from 'react';
import { Link } from 'react-router-dom';

type StateProps = FloatMessageProps & {
    fileId: string;
    name: string;
    date: string;
    isPromise: boolean;
    sum: number;
    currency: string;
    balance: number;
};

type OwnState = {
    rules: number[];
    fileInfoById: FileInfoByIdModel | null;
    isFileChecked: boolean;
    zip: UserItemInputParams[] | null;
    name: string;
    isCreateDatabase: boolean;
    allEmail: string[];
    conformation: boolean;
    notDistributed: string[];
    configuration: string;
    uploadFileAccessPrice: UploadFileAccessPriceModel | null;
    totalSum: number;
    extentions: string[];
    error: string;
};

const { getFileInfoById, uploadFileAccessPrice, zipDatabase } = InterlayerApiProxy.getMsBackupsApiProxy();

class RestoreDbOnDelimitersView extends React.Component<StateProps, OwnState> {
    public constructor(props: StateProps) {
        super(props);

        this.postApplication = this.postApplication.bind(this);
        this.getZip = this.getZip.bind(this);
        this.openCreateDbDialog = this.openCreateDbDialog.bind(this);
        this.closeCreateDbDialog = this.closeCreateDbDialog.bind(this);
        this.addExtentions = this.addExtentions.bind(this);
        this.getInfoById = this.getInfoById.bind(this);

        this.state = {
            /**
             * 2 - проверка архива на валидность
             */
            rules: [2],
            fileInfoById: null,
            isFileChecked: false,
            zip: null,
            name: `${ this.props.name } (копия от ${ DateUtility.getDateWithMonthName(new Date(this.props.date)) })`,
            isCreateDatabase: false,
            allEmail: [],
            conformation: false,
            notDistributed: [],
            configuration: '',
            uploadFileAccessPrice: null,
            totalSum: 0,
            extentions: [],
            error: ''
        };
    }

    public async componentDidMount() {
        void this.getInfoById();
    }

    private async getInfoById() {
        const fileInfoById = await getFileInfoById(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, { fileId: this.props.fileId, type: 'data_dump' });

        if (fileInfoById.status && !fileInfoById.status.ready) {
            setTimeout(() => this.getInfoById(), (fileInfoById.status?.timeout ?? 1) * 1000);
            return;
        }

        this.setState({
            fileInfoById,
            isFileChecked: true,
            configuration: fileInfoById.configuration.id
        });

        if (
            this.state.fileInfoById?.configuration.configurationSupported &&
            this.state.fileInfoById?.configuration.versionSupported
        ) {
            const accessPrice = await uploadFileAccessPrice(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, { configurationId: this.state.configuration, configurationName: this.state.fileInfoById.configuration.name });
            this.setState({
                uploadFileAccessPrice: accessPrice
            });
            this.setState({
                rules: []
            });
        }
    }

    private getZip(value: UserItemInputParams[], allEmail: string[], notDistributed: string[], totalSum: number) {
        this.setState({
            zip: value,
            allEmail,
            notDistributed,
            totalSum
        });
    }

    private addExtentions(value: string, checked: boolean) {
        if (checked) {
            this.setState(prevState => ({
                extentions: [...prevState.extentions, value]
            }));
        } else {
            this.setState(prevState => ({
                extentions: prevState.extentions.filter(extention => extention !== value)
            }));
        }
    }

    private async postApplication(args: UserItemInputParams[]) {
        try {
            await zipDatabase(
                RequestKind.SEND_BY_USER_ASYNCHRONOUSLY,
                {
                    fileId: this.props.fileId,
                    databasesServiceId: this.state.uploadFileAccessPrice!.data.serviceTypesId,
                    name: this.state.name,
                    users: args,
                    configuration: this.state.configuration,
                    needUsePromisePayment: this.props.isPromise,
                    extentions: this.state.extentions
                }
            );
        } catch (er: unknown) {
            this.props.floatMessage.show(MessageType.Error, er);
        }
    }

    private openCreateDbDialog() {
        if (!this.state.notDistributed.length && this.state.zip) {
            this.postApplication(this.state.zip);
        }
        this.setState({
            isCreateDatabase: true
        });
    }

    private closeCreateDbDialog() {
        this.setState({
            isCreateDatabase: false
        });
    }

    public render() {
        return (
            <>
                <VerticalLinearStepper
                    linkToBack={ AppRoutes.accountManagement.informationBases }
                    rules={ this.state.rules }
                    totalSum={ 0 }
                    isDisabled={ !!this.state.error }
                    steps={
                        [
                            {
                                label: 'Введите название базы',
                                description: <InputNameDt
                                    setError={ error => this.setState({ error }) }
                                    noNeedToCheckUsedName={ true }
                                    name={ this.state.name }
                                    setName={ name => this.setState({ name }) }
                                />
                            },
                            {
                                label: 'Подождите, мы проверим резервную копию.',
                                description: this.state.isFileChecked
                                    ? (
                                        <ArchiveVerification
                                            configuration={ this.state.fileInfoById?.configuration.name ?? '' }
                                            configurationSupported={ this.state.fileInfoById?.configuration.configurationSupported ?? false }
                                            version={ this.state.fileInfoById?.configuration.version ?? '' }
                                            versionSupported={ this.state.fileInfoById?.configuration.versionSupported ?? false }
                                            extensions={ this.state.fileInfoById?.extensions ?? [] }
                                            error={ this.state.fileInfoById?.errors ?? [] }
                                            addExtentions={ this.addExtentions }
                                            selectedExtentions={ this.state.extentions }
                                            messages={ [] }
                                            changeLoadingTo={ () => { /* empty */ } }
                                            loadingTo="zip"
                                            isDt={ false }
                                        />
                                    )
                                    : <LinearProgress sx={ { width: '60%' } } />,
                            },
                            {
                                label: 'Сопоставление пользователей.',
                                description: <UserMappingTable
                                    allUsers={ this.state.fileInfoById?.users ?? [] }
                                    allProfiles={ this.state.fileInfoById?.profiles ?? [] }
                                    zip={ this.getZip }
                                    accessPrice={ this.state.uploadFileAccessPrice }
                                    currency={ this.props.currency }
                                />
                            },
                        ]
                    }
                    finalizeStep={ this.openCreateDbDialog }
                    enoughMoney={ (this.props.sum + this.state.totalSum) <= this.props.balance }
                    notEnoughMoney={ Math.abs(this.props.balance - this.state.totalSum - this.props.sum) }
                    currency={ this.props.currency }
                />
                <Dialog
                    isOpen={ this.state.isCreateDatabase }
                    dialogWidth="sm"
                    dialogVerticalAlign="center"
                    onCancelClick={ this.closeCreateDbDialog }
                    title="Создание базы 1С"
                    buttons={
                        (!this.state.conformation && this.state.notDistributed.length) || this.state.totalSum > 0
                            ? [
                                {
                                    content: 'Отмена',
                                    variant: 'contained',
                                    kind: 'default',
                                    onClick: this.closeCreateDbDialog
                                },
                                {
                                    content: this.props.sum !== 0 || this.state.totalSum > 0
                                        ? `Создать базу (${ this.props.sum + this.state.totalSum } ${ this.props.currency })`
                                        : 'Создать базу',
                                    variant: 'contained',
                                    kind: 'success',
                                    onClick: () => {
                                        if (this.state.zip) {
                                            this.setState({
                                                conformation: true
                                            });
                                            return this.postApplication(this.state.zip);
                                        }
                                    }
                                }
                            ]
                            : [
                                {
                                    content: (
                                        <Link
                                            to={ AppRoutes.accountManagement.informationBases }
                                            className={ css.createDbButton }
                                        >
                                            Завершить
                                        </Link>
                                    ),
                                    variant: 'contained',
                                    kind: 'success',
                                    onClick: () => null
                                }
                            ]
                    }
                >
                    {
                        !this.state.conformation && this.state.notDistributed.length
                            ? (
                                <>
                                    <TextOut fontWeight={ 700 }>Внимание!</TextOut><br />
                                    <TextOut>Для пользователей 1С ({ this.state.notDistributed.join(', ') }) не задано соответствие пользователей аккаунта.</TextOut><br />
                                    <TextOut>Созданная база будет доступна только тем пользователям аккаунта, для которых выставлено соответствие.</TextOut>
                                </>
                            )
                            : (
                                <>
                                    <TextOut>Проводится создание базы 1С.</TextOut><br />
                                    <TextOut>После завершения процесса на эл. почту { this.state.allEmail.join(', ') } будет отправлено уведомление.</TextOut>
                                </>
                            )
                    }
                </Dialog>
            </>
        );
    }
}

export const RestoreDbOnDelimiters = withFloatMessages(RestoreDbOnDelimitersView);