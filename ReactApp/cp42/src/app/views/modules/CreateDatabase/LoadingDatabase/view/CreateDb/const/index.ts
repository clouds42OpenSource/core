import { CreateDbState } from 'app/views/modules/CreateDatabase/LoadingDatabase/view/CreateDb/types';

export const initialStateCreateDbState: CreateDbState = {
    selectedFile: null,
    upload: false,
    uploadState: false,
    /**
     * 1 - загрузка файла
     * 2 - проверка архива на валидность
     * 3 - Этап создания в дт есть
     */
    rules: [1, 2],
    progress: 0,
    baseId: '',
    eTag: [],
    fileInfoById: null,
    isFileChecked: false,
    errorUploadFile: '',
    zip: null,
    name: 'Своя база',
    isCreateDatabase: false,
    allEmail: [],
    conformation: false,
    notDistributed: [],
    configuration: '',
    uploadFileAccessPrice: null,
    totalSum: 0,
    visibleLoader: false,
    currentLogin: '',
    userIsAdmin: [],
    password: '',
    passwordIsValid: false,
    showPassword: false,
    isDragUnknown: false,
    extentions: [],
    shouldContinue: true,
    loadingTo: 'zip',
    email: [],
    usersForAdd: [],
    additionalOptions: {
        hasSupport: true,
        hasAutoupdate: true
    },
    showWarning: {
        autoUpdate: false,
        support: false
    },
    firstShow: {
        autoUpdate: true,
        support: true
    }
};

export const stylePassword = {
    width: '300px',
    '& input': {
        textSecurity: 'disc',
        MozTextSecurity: 'disc',
        WebkitTextSecurity: 'disc'
    },
};