import Box from '@mui/material/Box';
import { FETCH_API } from 'app/api/useFetchApi';
import { AppRoutes } from 'app/AppRoutes';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { withHeader } from 'app/views/components/_hoc/withHeader';
import { BaseButton } from 'app/views/components/controls/Button/BaseButton';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { TextOut } from 'app/views/components/TextOut';
import { CreateDb } from 'app/views/modules/CreateDatabase/LoadingDatabase/view/CreateDb';
import { RestoreDbFile } from 'app/views/modules/CreateDatabase/LoadingDatabase/view/RestoreDbFile';
import { RestoreDbOnDelimiters } from 'app/views/modules/CreateDatabase/LoadingDatabase/view/RestoreDbOnDelimiters';
import { TLocationProps } from 'app/views/modules/CreateDatabase/types';
import { GoToInfoBasesPage } from 'app/views/modules/CreateDatabase/views/GoToInfoBasesPage';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { CostOfCreatingDatabasesModel } from 'app/web/InterlayerApiProxy/MsBackupsApiProxy/costOfCreatingDatabases';
import { RequestKind } from 'core/requestSender/enums';
import React from 'react';
import { Link, RouteComponentProps, withRouter } from 'react-router-dom';

type StateProps = {
    costOrCreating: CostOfCreatingDatabasesModel | null;
    isPromise: boolean;
    loading: boolean;
    enoughMoney: number;
    currency: string;
    balance: number;
};

type RoutProps = FloatMessageProps & RouteComponentProps<NonNullable<unknown>, NonNullable<unknown>, TLocationProps>;

const { postActivateRentIfNeed } = FETCH_API.CRETE_DB;
const { costOrCreatingDatabases } = InterlayerApiProxy.getMsBackupsApiProxy();

class CreateCustomDatabaseClass extends React.Component<RoutProps, StateProps> {
    private readonly locationProps = this.props.location.state;

    public constructor(props: RoutProps) {
        super(props);

        this.promisePayment = this.promisePayment.bind(this);
        this.renderCreateOrRestoreComponent = this.renderCreateOrRestoreComponent.bind(this);

        this.state = {
            costOrCreating: null,
            isPromise: false,
            loading: false,
            enoughMoney: 0,
            currency: '',
            balance: 0
        };
    }

    public componentDidMount() {
        const { history, floatMessage: { show } } = this.props;

        try {
            void postActivateRentIfNeed();
            costOrCreatingDatabases(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY).then(response => {
                // if (response.data.currency !== 'руб') {
                //     history.push(AppRoutes.accountManagement.createAccountDatabase.createCustomDt);
                // }

                if (!response.success) {
                    show(MessageType.Error, response.message ?? 'Не удалось рассчитать стоимость создания ИБ');
                    history.push(AppRoutes.services.rentReference);
                }

                this.setState({
                    costOrCreating: response,
                    enoughMoney: response.data.enoughMoney,
                    currency: response.data.currency,
                    balance: response.data.balance,
                    loading: true
                });
            });
        } catch (err: unknown) {
            this.props.floatMessage.show(MessageType.Error, err);
        }
    }

    private promisePayment() {
        this.setState({
            isPromise: true
        });
    }

    private renderCreateOrRestoreComponent() {
        if (this.locationProps) {
            const { isZip, restoreType, date, state, fileId, caption } = this.locationProps;

            if (isZip) {
                return (
                    <RestoreDbOnDelimiters
                        fileId={ fileId ?? '' }
                        name={ caption ?? '' }
                        date={ date ?? '' }
                        isPromise={ this.state.isPromise }
                        sum={ this.state.costOrCreating?.data.costOftariff ?? 0 }
                        currency={ this.state.costOrCreating?.data.currency ?? '' }
                        balance={ this.state.balance }
                    />
                );
            }

            return (
                <RestoreDbFile
                    state={ state }
                    backupId={ fileId ?? '' }
                    restoreType={ restoreType ?? '1' }
                />
            );
        }

        return (
            <CreateDb
                isPromise={ this.state.isPromise }
                sum={ this.state.costOrCreating?.data.costOftariff ?? 0 }
                currency={ this.state.costOrCreating?.data.currency ?? '' }
                balance={ this.state.balance }
            />
        );
    }

    public render() {
        return (
            <>
                { !this.state.loading && <LoadingBounce /> }
                <GoToInfoBasesPage />
                { this.state.costOrCreating?.data.complete || this.state.enoughMoney === 0 || this.state.isPromise
                    ? this.renderCreateOrRestoreComponent()
                    : this.state.loading && this.state.enoughMoney > 0 && (
                        <Box display="flex" flexDirection="column">
                            <TextOut>Сумма { this.state.enoughMoney } { this.state.currency } будет списана с баланса за создание ИБ с архива</TextOut>
                            <Box display="flex" flexDirection="row" gap="16px">
                                <Link to={ AppRoutes.accountManagement.informationBases } style={ { textDecoration: 'none' } }>
                                    <BaseButton kind="default">Отмена</BaseButton>
                                </Link>
                                <Link
                                    to={ {
                                        pathname: AppRoutes.accountManagement.replenishBalance,
                                        search: `?customPayment.amount=${ this.state.enoughMoney }&customPayment.description=Оплата+за+создание+ИБ+из+архива`
                                    } }
                                    style={ { textDecoration: 'none' } }
                                >
                                    <BaseButton kind="primary">Пополнить счет на ({ this.state.enoughMoney } { this.state.currency })</BaseButton>
                                </Link>
                                { this.state.costOrCreating?.data.canGetPromisePayment && (
                                    <BaseButton kind="primary" onClick={ this.promisePayment }>Обещанный платеж</BaseButton>
                                ) }
                            </Box>
                        </Box>
                    ) }
            </>
        );
    }
}

export const CreateCustomDatabaseView = withHeader({
    title: 'Создание информационной базы',
    page: withRouter(withFloatMessages(CreateCustomDatabaseClass))
});