import { TextOut } from 'app/views/components/TextOut';
import React from 'react';
import { Box, LinearProgress } from '@mui/material';

type TProps = {
    progress: number;
};

export const ProgressBar = ({ progress }: TProps) => {
    return (
        <Box sx={ { width: '60%' } }>
            <Box sx={ { display: 'flex', alignItems: 'center' } }>
                <Box sx={ { width: '100%', mr: 1 } }>
                    <LinearProgress variant="determinate" sx={ { height: 7 } } value={ progress } />
                </Box>
                <Box sx={ { minWidth: 35 } }>
                    <TextOut>{ `${ Math.round(progress) }%` }</TextOut>
                </Box>
            </Box>
        </Box>
    );
};