import { Box, Step, StepContent, StepLabel, Stepper } from '@mui/material';
import { AppRoutes } from 'app/AppRoutes';
import { SuccessButton, TextButton } from 'app/views/components/controls/Button';
import { Props } from 'app/views/modules/CreateDatabase/LoadingDatabase/view/StepperRestore/types';
import React, { memo, useCallback } from 'react';
import { Link } from 'react-router-dom';

export const VerticalLinearStepper = memo(({ rules, steps, finalizeStep, finalizeStepContent }: Props) => {
    /**
     * Активный шаг
     */
    const [activeStep, setActiveStep] = React.useState(0);

    /**
     * Обработчик шага вперед
     */
    const handleNext = useCallback(() => setActiveStep(prevActiveStep => prevActiveStep + 1), []);

    /**
     * Обработчик шага назад
     */
    const handleBack = useCallback(() => setActiveStep(prevActiveStep => prevActiveStep - 1), []);

    /**
     * Обработка кнопок вперед из props.rules
     */
    const validateButton = useCallback((index: number) => {
        const isLastIndex = index === steps.length - 1;
        if (!rules.includes(index)) {
            return (
                <SuccessButton
                    onClick={ isLastIndex ? finalizeStep : handleNext }
                >
                    { isLastIndex ? finalizeStepContent : 'Продолжить' }
                </SuccessButton>
            );
        }

        return null;
    }, [finalizeStep, handleNext, rules, steps.length]);

    return (
        <Box>
            <Stepper activeStep={ activeStep } orientation="vertical">
                { steps.map((step, index) => (
                    <Step key={ step.label }>
                        <StepLabel>
                            { step.label }
                        </StepLabel>
                        <StepContent TransitionProps={ { unmountOnExit: false } }>
                            <Box sx={ { mb: '20px' } }>{ step.description }</Box>
                            <Box sx={ { mb: 2 } }>
                                <div>
                                    { validateButton(index) }
                                    {
                                        index === 0
                                            ? (
                                                <Link to={ AppRoutes.accountManagement.informationBases }>
                                                    <TextButton>
                                                        Вернуться
                                                    </TextButton>
                                                </Link>
                                            )
                                            : (
                                                <TextButton onClick={ handleBack } style={ { textDecoration: 'none' } }>
                                                    Вернуться
                                                </TextButton>
                                            )
                                    }
                                </div>
                            </Box>
                        </StepContent>
                    </Step>
                )) }
            </Stepper>
        </Box>
    );
});