import { FileInfoByIdModel, UserModel } from 'app/web/InterlayerApiProxy/MsBackupsApiProxy/getFileInfoById';
import { UploadFileAccessPriceModel } from 'app/web/InterlayerApiProxy/MsBackupsApiProxy/uploadFileAccessPrice';
import { UserItemInputParams } from 'app/web/InterlayerApiProxy/MsBackupsApiProxy/zipDatabase';

export type CreateDbProps = {
    isPromise: boolean;
    sum: number;
    currency: string;
    balance: number;
};

export type CreateDbState = {
    selectedFile: File | null;
    /**
     * Состояние загруженного файла
     * true - загружен, false - загружается/не выбран
     */
    upload: boolean;
    /**
     * Состояние отправки файла
     */
    uploadState: boolean;
    rules: number[];
    progress: number;
    baseId: string;
    eTag: string[];
    fileInfoById: FileInfoByIdModel | null;
    isFileChecked: boolean;
    errorUploadFile: string;
    zip: UserItemInputParams[] | null;
    name: string;
    isCreateDatabase: boolean;
    allEmail: string[];
    conformation: boolean;
    notDistributed: string[];
    configuration: string;
    uploadFileAccessPrice: UploadFileAccessPriceModel | null;
    totalSum: number;
    visibleLoader: boolean;
    userIsAdmin: UserModel[]
    currentLogin: string;
    password: string;
    passwordIsValid: boolean;
    showPassword: boolean;
    isDragUnknown: boolean;
    extentions: string[];
    shouldContinue: boolean;
    loadingTo: 'zip' | 'dt';
    usersForAdd: string[];
    email: string[];
    additionalOptions: {
        hasAutoupdate: boolean;
        hasSupport: boolean;
    };
    showWarning: {
        autoUpdate: boolean;
        support: boolean;
    };
    firstShow: {
        autoUpdate: boolean;
        support: boolean;
    };
    goToNext?: boolean;
};