import Box from '@mui/material/Box';
import Step from '@mui/material/Step';
import StepContent from '@mui/material/StepContent';
import StepLabel from '@mui/material/StepLabel';
import Stepper from '@mui/material/Stepper';
import { AppRoutes } from 'app/AppRoutes';
import { hasComponentChangesFor } from 'app/common/functions';
import { SuccessButton, TextButton, ContainedButton } from 'app/views/components/controls/Button';
import React from 'react';
import { Link } from 'react-router-dom';
import { stepsForStepper } from './types';

type StateProps = {
    steps: stepsForStepper[];
    linkToBack: string;
    rules: number[];
    finalizeStep: () => void;
    totalSum: number;
    enoughMoney: boolean;
    notEnoughMoney: number;
    currency: string;
    isDisabled?: boolean;
    goToNext?: boolean;
};

type OwnState = {
    activeStep: number;
};

export class VerticalLinearStepper extends React.Component<StateProps, OwnState> {
    public constructor(props: StateProps) {
        super(props);
        this.handleNext = this.handleNext.bind(this);
        this.validateButton = this.validateButton.bind(this);
        this.handleBack = this.handleBack.bind(this);
        this.state = {
            activeStep: 0
        };
    }

    public shouldComponentUpdate(nextProps: StateProps, nextState: OwnState) {
        return hasComponentChangesFor(this.props, nextProps) || hasComponentChangesFor(this.state, nextState);
    }

    public componentDidUpdate(prevProps: Readonly<StateProps>): void {
        if (prevProps.goToNext !== this.props.goToNext && !!this.props.goToNext) {
            this.handleNext();
        }
    }

    private handleNext() {
        this.setState(prevState => ({
            activeStep: prevState.activeStep + 1
        }));
    }

    private handleBack() {
        this.setState(prevState => ({
            activeStep: prevState.activeStep - 1
        }));
    }

    private validateButton(index: number, hideNextButton = false) {
        if (this.props.isDisabled) {
            return (
                <SuccessButton isEnabled={ false }>
                    { index === this.props.steps.length - 1 ? 'Закончить' : 'Продолжить' }
                </SuccessButton>
            );
        }

        if (!this.props.rules.includes(index + 1) && index === 0 && !hideNextButton) {
            return (
                <SuccessButton onClick={ index === this.props.steps.length - 1 ? this.props.finalizeStep : this.handleNext }>
                    { index === this.props.steps.length - 1 ? 'Закончить' : 'Продолжить' }
                </SuccessButton>
            );
        }

        if (!this.props.rules.includes(index + 1) && index === 1 && !hideNextButton) {
            return (
                <SuccessButton onClick={ index === this.props.steps.length - 1 ? this.props.finalizeStep : this.handleNext }>
                    { index === this.props.steps.length - 1 ? 'Закончить' : 'Продолжить' }
                </SuccessButton>
            );
        }

        if (!this.props.rules.includes(index + 1) && index === 2 && !hideNextButton) {
            return (
                <SuccessButton onClick={ index === this.props.steps.length - 1 ? this.props.finalizeStep : this.handleNext }>
                    { index === this.props.steps.length - 1 ? `Закончить${ this.props.totalSum !== 0 ? ` (${ this.props.totalSum } ${ this.props.currency })` : '' }` : 'Продолжить' }
                </SuccessButton>
            );
        }

        return null;
    }

    public render() {
        return (
            <Box>
                <Stepper activeStep={ this.state.activeStep } orientation="vertical">
                    { this.props.steps.map((step, index) => (
                        <Step key={ step.label }>
                            <StepLabel>
                                { step.label }
                            </StepLabel>
                            <StepContent>
                                <Box sx={ { mb: '20px' } }>{ step.description }</Box>
                                <Box sx={ { mb: 2 } } display="flex" gap="8px">
                                    { index !== this.props.steps.length && this.props.enoughMoney
                                        ? this.validateButton(index, step.hideNextButton)
                                        : (
                                            <Link
                                                to={ {
                                                    pathname: AppRoutes.accountManagement.replenishBalance,
                                                    search: `?customPayment.amount=${ this.props.notEnoughMoney }&customPayment.description=Оплата+за+создание+ИБ+из+архива`
                                                } }
                                                style={ { textDecoration: 'none' } }
                                            >
                                                <SuccessButton>Пополнить счет на { this.props.notEnoughMoney } { this.props.currency }</SuccessButton>
                                            </Link>
                                        )
                                    }
                                    { index === 0
                                        ? (
                                            <Link to={ this.props.linkToBack } style={ { textDecoration: 'none' } }>
                                                <ContainedButton kind="default">
                                                    Вернуться
                                                </ContainedButton>
                                            </Link>
                                        )
                                        : (
                                            <ContainedButton kind="default" onClick={ this.handleBack } style={ { textDecoration: 'none' } }>
                                                Вернуться
                                            </ContainedButton>
                                        )
                                    }
                                </Box>
                            </StepContent>
                        </Step>
                    )) }
                </Stepper>
            </Box>
        );
    }
}