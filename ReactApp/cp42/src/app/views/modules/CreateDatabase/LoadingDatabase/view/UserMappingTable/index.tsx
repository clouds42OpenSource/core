import { Box, Skeleton, TextField } from '@mui/material';
import { getEmptyUuid } from 'app/common/helpers';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { TextOut } from 'app/views/components/TextOut';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { ChipSelect } from 'app/views/modules/CreateDatabase/LoadingDatabase/view/ChipSelect';
import { MsBackupsApiProxy } from 'app/web/InterlayerApiProxy/MsBackupsApiProxy';
import { AccountUserListItem } from 'app/web/InterlayerApiProxy/MsBackupsApiProxy/getAccountUsersById';
import { ProfilesModel, UserModel } from 'app/web/InterlayerApiProxy/MsBackupsApiProxy/getFileInfoById';
import { UploadFileAccessPriceModel } from 'app/web/InterlayerApiProxy/MsBackupsApiProxy/uploadFileAccessPrice';
import { editTableUser, UserItemInputParams } from 'app/web/InterlayerApiProxy/MsBackupsApiProxy/zipDatabase';
import { RequestKind } from 'core/requestSender/enums';
import React, { memo } from 'react';

type StateProps = {
    allUsers: UserModel[];
    allProfiles: ProfilesModel[];
    zip: (value: UserItemInputParams[], allEmail: string[], notDistributed: string[], totalSum: number) => void;
    accessPrice: UploadFileAccessPriceModel | null;
    currency: string;
};

type OwnState = {
    users: AccountUserListItem[];
    editedUsersTable: editTableUser[];
    totalSum: number;
    search: string;
    isLoading: boolean;
    isFirstShowWarning: boolean;
};

type AllProps = StateProps & FloatMessageProps;

class UserMappingTableClass extends React.Component<AllProps, OwnState> {
    private _commonTableWithFilterViewRef = React.createRef<CommonTableWithFilter<any, any>>();

    public constructor(props: AllProps) {
        super(props);

        this.updateProfilesInDataToSend = this.updateProfilesInDataToSend.bind(this);
        this.updateUserInDataToSend = this.updateUserInDataToSend.bind(this);
        this.getSendingUsersData = this.getSendingUsersData.bind(this);
        this.getZip = this.getZip.bind(this);

        this.state = {
            users: [],
            editedUsersTable: [],
            totalSum: 0,
            search: '',
            isLoading: true,
            isFirstShowWarning: true
        };
    }

    public async componentDidMount() {
        await MsBackupsApiProxy.getAccountUsersById(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY)
            .then(response => {
                this.setState({
                    users: response.accountUserList
                        .filter(item => item.activated)
                        .sort((a, b) => {
                            const aMsUsers = this.props.allUsers.filter(msUser => msUser.accountUserId === a.id);
                            const bMsUsers = this.props.allUsers.filter(msUser => msUser.accountUserId === b.id);

                            if (aMsUsers.length === bMsUsers.length) {
                                return this.getCloudUserName(a).localeCompare(this.getCloudUserName(b));
                            }

                            return bMsUsers.length - aMsUsers.length;
                        }),
                    editedUsersTable: response.accountUserList.reduce((usersTable: editTableUser[], item) => {
                        if (item.activated) {
                            usersTable.push({
                                accountUserId: item.id,
                                name: this.getCloudUserName(item),
                                dataDumpUserId: this.props.allUsers
                                    .reduce((userId, dumpUserId) => {
                                        if (dumpUserId.accountUserId === item.id) {
                                            if (userId.length > 0) {
                                                userId += ',';
                                            }

                                            userId += dumpUserId.dataDumpUserId;
                                        }

                                        return userId;
                                    }, ''),
                                profilesIds: this.props.allUsers
                                    .filter(msUser => msUser.accountUserId === item.id)
                                    .flatMap(accountIdItem => accountIdItem.profiles.map(profile => profile.id))
                            });
                        }

                        return usersTable;
                    }, []),
                    isLoading: false
                });
            });
        this.getZip();
    }

    private getSendingUsersData(editedUsersTable: editTableUser[]): UserItemInputParams[] {
        return editedUsersTable.filter(item => item.profilesIds.length).map(
            item => {
                return {
                    dataDumpUserId: item.dataDumpUserId || getEmptyUuid(),
                    accountUserId: item.accountUserId,
                    catalogUserId: this.props.allUsers
                        .flatMap(msUser => msUser.dataDumpUserId === item.dataDumpUserId ? msUser.catalogUserId : [])
                        .join() || getEmptyUuid(),
                    profiles: this.props.allProfiles.filter(profile => item.profilesIds.includes(profile.id))
                };
            }
        );
    }

    private getCloudUserName(data: AccountUserListItem) {
        const namePartsArray: string[] = [];

        if (data.firstName !== '') {
            namePartsArray.push(data.firstName);
        }

        if (data.middleName !== '') {
            namePartsArray.push(data.middleName);
        }

        if (data.lastName !== '') {
            namePartsArray.push(data.lastName);
        }

        if (namePartsArray.length === 0) {
            namePartsArray.push(data.login);
        } else {
            namePartsArray.push(` (${ data.login })`);
        }

        const cloudUser = namePartsArray.join(' ');

        return (
            cloudUser
        );
    }

    private getZip() {
        const userData = this.getSendingUsersData(this.state.editedUsersTable);
        if (userData.length) {
            const totalSum = this.props.accessPrice!.data.accountUserLicence
                .reduce((sum, item) => {
                    if (item.accountUserId.includes(item.accountUserId)) {
                        sum += item.accessCost;
                    }

                    return sum;
                }, 0);
            this.setState({
                totalSum
            });
        }
        this.props.zip(
            userData,
            this.state.users.flatMap(item => {
                if (item.isManager) {
                    return item.email;
                }
                return [];
            }),
            this.props.allUsers.filter(item => !this.state.editedUsersTable
                .flatMap(editedUsersTableItem => {
                    if (editedUsersTableItem.dataDumpUserId) {
                        return editedUsersTableItem.dataDumpUserId;
                    }
                    return [];
                })
                .includes(item.dataDumpUserId)
            )
                .map(item => item.name),
            this.props.accessPrice!.data.accountUserLicence
                .reduce((sum, item) => {
                    if (userData.find(userDataItem => userDataItem.accountUserId.includes(item.accountUserId))) {
                        sum += item.accessCost;
                    }

                    return sum;
                }, 0)
        );
    }

    private updateProfilesInDataToSend(rowKey: string, value: string[]) {
        const updatedEditedUsersTable = this.state.editedUsersTable.map(item => {
            if (item.accountUserId === rowKey) {
                item.profilesIds = value;
            }

            return item;
        });

        this.setState({
            editedUsersTable: updatedEditedUsersTable
        });

        this.getZip();
    }

    private updateUserInDataToSend(rowKey: string, value: string[]) {
        const updatedEditedUsersTable = this.state.editedUsersTable.map(item => {
            if (item.accountUserId === rowKey) {
                const joinedValue = value.join();

                item.dataDumpUserId = joinedValue;

                if (joinedValue) {
                    item.profilesIds = this.props.allUsers
                        .filter(msUser => msUser.dataDumpUserId === joinedValue)
                        .flatMap(dumpUserItem => dumpUserItem.profiles.map(allItem => allItem.id));
                } else {
                    item.profilesIds = [];
                }
            }

            return item;
        });

        this.setState({
            editedUsersTable: updatedEditedUsersTable
        });

        this.getZip();
    }

    public render() {
        return (
            !this.state.isLoading
                ? (
                    <>
                        <TextField
                            variant="outlined"
                            label="Поиск пользователя"
                            size="small"
                            value={ this.state.search }
                            sx={ { margin: '16px 0', width: '40%', height: '40px' } }
                            onChange={ e => this.setState({ search: e.target.value }) }
                        />
                        <CommonTableWithFilter
                            ref={ this._commonTableWithFilterViewRef }
                            uniqueContextProviderStateId="UserMappingContextProviderStateId"
                            tableProps={ {
                                emptyText: `По запросу "${ this.state.search }" не удалось найти совпадений. Попробуйте другой вариант поисковой фразы.`,
                                dataset: this.state.users.filter(item => {
                                    if (
                                        item.login.toLowerCase().includes(this.state.search.toLowerCase()) ||
                                        `${ item.middleName.toLowerCase() } ${ item.firstName.toLowerCase() } ${ item.lastName.toLowerCase() }`.includes(this.state.search.toLowerCase()) ||
                                        item.email.toLowerCase().includes(this.state.search.toLowerCase())
                                    ) {
                                        return item;
                                    }

                                    return null;
                                }),
                                keyFieldName: 'id',
                                fieldsView: {
                                    id: {
                                        caption: 'Пользователь 42Clouds',
                                        fieldWidth: '20%',
                                        format: (_value, data) => (
                                            <TextOut style={ { wordWrap: 'break-word' } }> { this.getCloudUserName(data) } </TextOut>
                                        )
                                    },
                                    login: {
                                        caption: 'Пользователь 1С',
                                        style: { display: this.props.allUsers.length ? 'table-cell' : 'none' },
                                        fieldWidth: '20%',
                                        format: (_value, data) => {
                                            const currentUser = this.state.editedUsersTable.find(item => item.accountUserId === data.id);

                                            return (
                                                <ChipSelect
                                                    key={ `${ data.id }-${ currentUser?.dataDumpUserId ?? '' }-${ (currentUser?.profilesIds ?? []).length }` }
                                                    rowKey={ data.id }
                                                    defaultValue={
                                                        this.state.editedUsersTable
                                                            .flatMap(item => item.accountUserId === data.id ? item.dataDumpUserId : [])
                                                    }
                                                    value={ this.props.allUsers
                                                        .map(item => item.dataDumpUserId)
                                                        .filter(item => !this.state.editedUsersTable
                                                            .filter(accountIdItem => accountIdItem.accountUserId !== data.id && accountIdItem.dataDumpUserId)
                                                            .map(editItem => editItem.dataDumpUserId)
                                                            .includes(item)
                                                        ) }
                                                    multiple={ false }
                                                    isChip={ false }
                                                    callback={ this.updateUserInDataToSend }
                                                    getDecorationText={ key => {
                                                        return key
                                                            ? this.props.allUsers
                                                                .reduce((acc: string[], msUser) => {
                                                                    if (msUser.dataDumpUserId === key) {
                                                                        acc.push(msUser.name);
                                                                    }

                                                                    return acc;
                                                                }, []).join() || key
                                                            : this.state.editedUsersTable
                                                                .filter(item => item.accountUserId === data.id)
                                                                .flatMap(item => item.profilesIds)
                                                                .length
                                                                ? 'Новый пользователь'
                                                                : 'Не указан';
                                                    } }
                                                />
                                            );
                                        }
                                    },
                                    profiles: {
                                        caption: 'Профили групп доступа',
                                        fieldWidth: '60%',
                                        format: (_value, data) => {
                                            const selectProfiles = this.state.editedUsersTable.find(item => item.accountUserId === data.id)?.profilesIds;

                                            return (
                                                <ChipSelect
                                                    key={ `${ data.id }-${ (selectProfiles ?? []).join('') }` }
                                                    rowKey={ data.id }
                                                    defaultValue={
                                                        this.state.editedUsersTable.find(item => item.accountUserId === data.id)?.profilesIds ?? []
                                                    }
                                                    value={ this.props.allProfiles.map(item => item.id) }
                                                    multiple={ true }
                                                    isChip={ true }
                                                    callback={ this.updateProfilesInDataToSend }
                                                    sx={ { width: '600px' } }
                                                    getDecorationText={ key => this.props.allProfiles
                                                        .reduce((acc: string[], profile) => {
                                                            if (profile.id === key) {
                                                                acc.push(profile.name);
                                                            }

                                                            return acc;
                                                        }, []).join() || key
                                                    }
                                                    allProfiles={ this.props.allProfiles }
                                                    isFirstShowWarning={ this.state.isFirstShowWarning }
                                                    setIsFirstShowWarning={ () => this.setState({ isFirstShowWarning: false }) }
                                                    isProfileSelect={ true }
                                                />
                                            );
                                        }
                                    },
                                    sum: {
                                        caption: this.state.totalSum ? `Сумма, ${ this.props.currency }` : '',
                                        format: (_value, data) => {
                                            const sumCurrencyUser = (this.props.accessPrice?.data.accountUserLicence || [])
                                                .reduce((acc, item) => {
                                                    if (item.accountUserId === data.id) {
                                                        acc += item.accessCost || 0;
                                                    }
                                                    return acc;
                                                }, 0);
                                            return this.state.totalSum ? <TextOut>{ sumCurrencyUser }</TextOut> : <></>;
                                        }
                                    }
                                }
                            } }
                            isResponsiveTable={ true }
                        />
                    </>
                )
                : (
                    <Box sx={ { height: 'max-content' } }>
                        { [...Array(4)].map((_, index) => (
                            <Box key={ index } display="flex" flexDirection="row" justifyContent="space-between">
                                <Skeleton variant="rectangular" animation="pulse" sx={ { my: 1, mx: 1, width: '20%', height: '40px' } } />
                                <Skeleton variant="rectangular" animation="pulse" sx={ { my: 1, mx: 1, width: '20%', height: '40px' } } />
                                <Skeleton variant="rectangular" animation="pulse" sx={ { my: 1, mx: 1, width: '60%', height: '40px' } } />
                            </Box>
                        )) }
                    </Box>
                )
        );
    }
}

export const UserMappingTable = memo(withFloatMessages(UserMappingTableClass));