import { DatabaseState } from 'app/common/enums';

export type Props = {
    backupId: string;
    restoreType: string;
    state?: DatabaseState;
};