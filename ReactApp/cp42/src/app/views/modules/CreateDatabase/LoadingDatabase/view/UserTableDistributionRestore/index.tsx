import { Box, Checkbox, Skeleton, TextField } from '@mui/material';
import { TextOut } from 'app/views/components/TextOut';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { Props } from 'app/views/modules/CreateDatabase/LoadingDatabase/view/UserTableDistributionRestore/types';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { MsBackupsApiProxy } from 'app/web/InterlayerApiProxy/MsBackupsApiProxy';
import { AccountUserListItem } from 'app/web/InterlayerApiProxy/MsBackupsApiProxy/getAccountUsersById';
import { RequestKind } from 'core/requestSender/enums';
import React, { useEffect, useState } from 'react';

const UserTableDistributionFc = ({ select, adminEmail, initSelect, floatMessage: { show } }: Props & FloatMessageProps) => {
    /**
     * Список пользователей с активной 1С
     */
    const [userList, setUserList] = useState<AccountUserListItem[]>([]);
    /**
     * Выбранные пользователи
     */
    const [selectUsers, setSelectUsers] = useState<string[]>([]);
    /**
     * Статус checkbox
     */
    const [check, setCheck] = useState<boolean[]>([]);
    /**
     * Поиск пользователя
     */
    const [search, setSearch] = useState('');

    /**
     * Получить список пользователей с активной арендой 1с
     */
    useEffect(() => {
        try {
            MsBackupsApiProxy.getAccountUsersById(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY)
                .then(response => {
                    setUserList(response.accountUserList.filter(item => item.activated));
                    adminEmail(response.accountUserList.flatMap(item => {
                        if (item.isManager) {
                            return item.email;
                        }
                        return [];
                    }));
                });
        } catch (er: unknown) {
            show(MessageType.Error, 'При получении списка всех пользователей произошла ошибка. Пожалуйста, повторите позже.');
        }
    }, []);

    /**
     * Значение чекбоксов по умолчанию
     */
    useEffect(() => {
        setCheck(new Array(userList.length).fill(false));
    }, [userList]);

    useEffect(() => {
        if (initSelect.length) {
            const initSelectUsers = userList.flatMap(userItem => {
                if (initSelect.includes(userItem.id)) {
                    return userItem.id;
                }

                return [];
            });

            setCheck(userList.map(userItem => initSelect.includes(userItem.id)));
            setSelectUsers(initSelectUsers);
        } else {
            select(selectUsers);
            setSelectUsers(selectUsers);
        }
    }, [userList, initSelect]);

    /**
     * Изменение конкретного чекбокса
     */
    const handleChange = (currentId: string, indexCheck: number) => {
        setCheck(check.map((item, index) => {
            if (index === indexCheck) {
                if (!item) {
                    select([...selectUsers, currentId]);
                    setSelectUsers([...selectUsers, currentId]);
                } else {
                    select(selectUsers.filter(selectUsersItem => selectUsersItem !== currentId));
                    setSelectUsers(selectUsers.filter(selectUsersItem => selectUsersItem !== currentId));
                }
                return !item;
            }

            return item;
        }));
    };

    /**
     * Изменение всех чекбоксов
     */
    const handleChangeAll = () => {
        if (check.filter(item => item).length === userList.length) {
            setCheck(check.map(() => false));
            select([]);
            setSelectUsers([]);
        } else {
            setCheck(check.map(() => true));
            select(userList.map(item => item.id));
            setSelectUsers(userList.map(item => item.id));
        }
    };

    return userList.length
        ? (
            <>
                <TextField
                    variant="outlined"
                    label="Поиск пользователя"
                    size="small"
                    value={ search }
                    sx={ { margin: '16px 0', width: '40%', height: '40px' } }
                    onChange={ e => setSearch(e.target.value) }
                />
                <CommonTableWithFilter
                    uniqueContextProviderStateId="UserTableDistributionContextProviderStateId"
                    tableProps={ {
                        emptyText: `По запросу "${ search }" не удалось найти совпадений. Попробуйте другой вариант поисковой фразы.`,
                        dataset: userList.filter(item => {
                            if (
                                item.login.includes(search) ||
                            `${ item.middleName } ${ item.firstName } ${ item.lastName }`.includes(search) ||
                            item.email.includes(search)
                            ) {
                                return item;
                            }

                            return null;
                        }),
                        keyFieldName: 'id',
                        fieldsView: {
                            id: {
                                caption: <Checkbox
                                    checked={ check.filter(item => item).length === userList.length }
                                    onChange={ handleChangeAll }
                                />,
                                format: (value, data) => {
                                    const index = userList.findIndex(item => item === data);
                                    return (<Checkbox
                                        checked={ check[index] ?? false }
                                        onChange={ () => handleChange(value, index) }
                                    />);
                                }
                            },
                            login: {
                                caption: 'Логин',
                                format: value => (
                                    <TextOut fontWeight={ 700 }>{ value }</TextOut>
                                )
                            },
                            firstName: {
                                caption: 'ФИО',
                                format: (value, data) => (
                                    <TextOut>{ data.middleName } { data.firstName } { data.lastName }</TextOut>
                                )
                            },
                            email: {
                                caption: 'Эл. почта'
                            }
                        }
                    } }
                    onDataSelect={ () => { } }
                />
            </>
        )
        : (
            <Box sx={ { height: 'max-content' } }>
                { new Array(4).map((_, index) => (
                    <Box key={ index } display="flex" flexDirection="row" justifyContent="space-between">
                        <Skeleton variant="rectangular" animation="pulse" sx={ { my: 1, mx: 1, width: '5%', height: '40px' } } />
                        <Skeleton variant="rectangular" animation="pulse" sx={ { my: 1, mx: 1, width: '25%', height: '40px' } } />
                        <Skeleton variant="rectangular" animation="pulse" sx={ { my: 1, mx: 1, width: '30%', height: '40px' } } />
                        <Skeleton variant="rectangular" animation="pulse" sx={ { my: 1, mx: 1, width: '30%', height: '40px' } } />
                    </Box>
                )) }
            </Box>
        );
};

export const UserTableDistribution = withFloatMessages(UserTableDistributionFc);