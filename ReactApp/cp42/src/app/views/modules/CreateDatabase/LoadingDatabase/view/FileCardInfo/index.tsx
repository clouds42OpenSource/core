import CloseIcon from '@mui/icons-material/Close';
import Paper from '@mui/material/Paper';
import Stack from '@mui/material/Stack';
import { hasComponentChangesFor } from 'app/common/functions';
import { NumberUtility } from 'app/utils/NumberUtility';
import { TextOut } from 'app/views/components/TextOut';
import React from 'react';

type StateProps = {
    name: string;
    size: number;
    isDisable: boolean;
    deleteFile: () => void;
};

export class FileCardInfo extends React.Component<StateProps, NonNullable<unknown>> {
    public shouldComponentUpdate(nextProps: StateProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    public render() {
        return (
            <Paper elevation={ 2 } sx={ { width: '60%', marginTop: '16px' } }>
                <Stack
                    direction="row"
                    justifyContent="space-between"
                    alignItems="center"
                    spacing="2"
                    padding="5px 10px"
                    sx={ { background: '#E7EAEC' } }
                >
                    <Stack
                        direction="row"
                        justifyContent="space-between"
                        alignItems="center"
                        spacing="2"
                        width="80%"
                    >
                        <TextOut>{ this.props.name }</TextOut>
                        <TextOut>{ NumberUtility.bytesToMegabytes(this.props.size) }</TextOut>
                    </Stack>
                    <CloseIcon onClick={ this.props.isDisable ? undefined : this.props.deleteFile } sx={ this.props.isDisable ? { cursor: 'default' } : { cursor: 'pointer' } } />
                </Stack>
            </Paper>
        );
    }
}