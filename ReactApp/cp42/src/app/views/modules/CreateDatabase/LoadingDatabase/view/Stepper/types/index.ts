import React from 'react';

export type stepsForStepper = {
    label: string;
    description: string | React.ReactComponentElement<any> | HTMLImageElement;
    hideNextButton?: boolean;
};