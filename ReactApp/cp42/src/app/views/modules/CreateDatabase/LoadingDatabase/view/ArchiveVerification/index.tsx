import CheckIcon from '@mui/icons-material/Check';
import ErrorOutlineIcon from '@mui/icons-material/ErrorOutline';
import { Box, Checkbox } from '@mui/material';
import { ReceiveMyCompanyThunkParams } from 'app/modules/myCompany/store/reducers/receiveMyCompanyData/params';
import { ReceiveMyCompanyThunk } from 'app/modules/myCompany/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { COLORS } from 'app/utils';
import { ContainedButton } from 'app/views/components/controls/Button';
import { Dialog } from 'app/views/components/controls/Dialog';
import { TextOut } from 'app/views/components/TextOut';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { AdvantagesTable } from 'app/views/modules/CreateDatabase/LoadingDatabase/view/AdvantagesTable';
import { ExtensionsModel } from 'app/web/InterlayerApiProxy/MsBackupsApiProxy/getFileInfoById';
import { MyCompanyDataModel } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/receiveMyCompany';
import React from 'react';
import cn from 'classnames';
import { connect } from 'react-redux';
import css from './style.module.css';

type TStateProps = {
    company: MyCompanyDataModel;
};

type TDispatchProps = {
    dispatchReceiveMyCompanyThunk: (args: ReceiveMyCompanyThunkParams) => void;
};

type TOwnProps = {
    configuration: string;
    configurationSupported: boolean;
    version: string;
    versionSupported: boolean;
    extensions: ExtensionsModel[];
    error: string[];
    addExtentions: (value: string, checked: boolean) => void;
    selectedExtentions: string[];
    messages: string[];
    isDt: boolean;
    loadingTo: 'zip' | 'dt';
    changeLoadingTo: (to: 'zip' | 'dt') => void;
};

type TState = {
    openAdvantagesDialog: boolean;
};

type TProps = TStateProps & TDispatchProps & TOwnProps;

class ArchiveVerificationClass extends React.Component<TProps, TState> {
    constructor(props: TProps) {
        super(props);

        this.state = {
            openAdvantagesDialog: false
        };

        this.renderElement = this.renderElement.bind(this);
    }

    public componentDidMount() {
        this.props.dispatchReceiveMyCompanyThunk({});
    }

    private openAdvantagesDialog() {
        this.setState({ openAdvantagesDialog: true });
    }

    private closeAdvantagesDialog() {
        this.setState({ openAdvantagesDialog: false });
    }

    private renderElement(isNotError: boolean, value: string, isDt: boolean, key?: string) {
        if (isDt) {
            return (
                <TextOut key={ key }>{ value }</TextOut>
            );
        }

        return (
            <TextOut
                key={ key }
                className={
                    cn(
                        css.check,
                        {
                            [css.accessCheck]: isNotError,
                            [css.errorCheck]: !isNotError
                        }
                    )
                }
            >
                { isNotError ? <CheckIcon /> : <ErrorOutlineIcon /> }
                { value }
            </TextOut>
        );
    }

    public render() {
        return (
            <Box display="flex" flexDirection="column" gap="16px" mt="16px">
                {
                    this.props.isDt && !this.props.error.length && (
                        <TextOut inDiv={ true }>
                            Загрузить в:&nbsp;
                            <ContainedButton
                                kind={ this.props.loadingTo === 'zip' ? 'success' : 'default' }
                                onClick={ () => this.props.changeLoadingTo('zip') }
                                className={ css.loadToButton }
                            >
                                Разделители (рекомендуется)
                            </ContainedButton>
                            <ContainedButton
                                kind={ this.props.loadingTo === 'dt' ? 'success' : 'default' }
                                onClick={ () => this.openAdvantagesDialog() }
                            >
                                Файловая база
                            </ContainedButton>
                        </TextOut>
                    )
                }
                <Box display="flex" flexDirection="column" marginBottom="16px">
                    <TextOut fontWeight={ 700 }>Конфигурация:</TextOut>
                    { this.renderElement(this.props.configurationSupported, this.props.configuration, this.props.isDt) }
                </Box>
                <Box display="flex" flexDirection="column" marginBottom="16px">
                    <TextOut fontWeight={ 700 }>Версия конфигурации:</TextOut>
                    { this.renderElement(this.props.versionSupported, this.props.version, this.props.isDt) }
                </Box>
                {
                    this.props.extensions.length && this.props.loadingTo === 'zip' ? (
                        <>
                            <CommonTableWithFilter
                                uniqueContextProviderStateId="archiveVeritication"
                                className={ css['custom-table'] }
                                tableProps={ {
                                    dataset: this.props.extensions,
                                    keyFieldName: 'name',
                                    isResponsiveTable: true,
                                    fieldsView: {
                                        name: {
                                            caption: 'Расширение',
                                            fieldWidth: '50%',
                                            format: (value, row) => (
                                                <Box display="flex" alignItems="center" gap="3px" color={ row.auditFailed ? COLORS.error : COLORS.main }>
                                                    { row.serviceStringId ? (
                                                        <a target="_blank" href={ `${ import.meta.env.PUBLIC_URL || window.location.origin }/ru-ru/market/service/${ row.serviceStringId }` } rel="noreferrer">
                                                            <TextOut inheritColor={ true } style={ { textDecoration: 'underline' } }>{ value }</TextOut>
                                                            <img alt="cloud42" height="20px" src={ `${ import.meta.env.PUBLIC_URL || window.location.origin }/img/create-custom-database/cloud.png` } />
                                                        </a>
                                                    ) : (
                                                        <TextOut inheritColor={ true }>{ value }</TextOut>
                                                    ) }
                                                </Box>
                                            )
                                        },
                                        information: {
                                            caption: 'Информация',
                                            fieldWidth: 'auto',
                                            format: (value, row) => <TextOut style={ { color: row.auditFailed ? COLORS.error : COLORS.main } }>{ value }</TextOut>
                                        },
                                        id: {
                                            caption: 'Подключить',
                                            fieldWidth: 'auto',
                                            format: (value, row) => (row.serviceStringId && row.serviceIsPaid) || (!row.serviceStringId && !row.auditFailed) ? (
                                                <Checkbox
                                                    checked={ this.props.selectedExtentions.includes(value) }
                                                    onChange={ event => this.props.addExtentions(value, event.target.checked) }
                                                />
                                            ) : ''
                                        },
                                    }
                                } }
                            />
                            <Box marginTop="6px">
                                <TextOut>
                                    Если не удастся подключить расширение, будет установлена каркасная версия.
                                </TextOut>
                            </Box>
                        </>
                    ) : null
                }
                {
                    this.props.error.length
                        ? (
                            <Box display="flex" flexDirection="column" marginBottom="16px">
                                <TextOut style={ { marginBottom: '8px' } } fontWeight={ 700 }>
                                    {
                                        (this.props.isDt || this.props.loadingTo === 'dt')
                                            ? `База будет загружена в ${ this.props.company.createClusterDatabase ? 'серверную' : 'файловую' } по причине:`
                                            : 'Ошибки:'
                                    }
                                </TextOut>
                                <ul>
                                    {
                                        this.props.error.map(item => <li style={ { whiteSpace: 'pre-wrap' } } key={ item }>{ item }</li>)
                                    }
                                </ul>
                            </Box>
                        )
                        : null
                }
                {
                    this.props.messages.length && (this.props.loadingTo === 'zip' || !this.props.isDt)
                        ? (
                            <Box display="flex" flexDirection="column">
                                <TextOut style={ { marginBottom: '8px' } } fontWeight={ 700 }>Предупреждения об изменениях:</TextOut>
                                <ul>
                                    {
                                        this.props.messages.map(item => {
                                            const messageLine = item.replace(
                                                /\b(https?\:\/\/\S+)/mg,
                                                '<a class="text-link" target="_blank" rel="noreferrer" href="$1">$1</a>'
                                            );
                                            const createMessageLine = () => {
                                                return { __html: messageLine };
                                            };

                                            return <li style={ { whiteSpace: 'pre-wrap' } } key={ item } dangerouslySetInnerHTML={ createMessageLine() } />;
                                        })
                                    }
                                </ul>
                            </Box>
                        )
                        : null
                }
                <Dialog
                    isOpen={ this.state.openAdvantagesDialog }
                    dialogWidth="sm"
                    dialogVerticalAlign="center"
                    onCancelClick={ () => this.closeAdvantagesDialog() }
                    buttons={ [
                        {
                            content: 'Отмена',
                            kind: 'default',
                            onClick: () => this.closeAdvantagesDialog()
                        },
                        {
                            content: 'Переключить',
                            kind: 'success',
                            onClick: () => {
                                this.props.changeLoadingTo('dt');
                                this.closeAdvantagesDialog();
                            }
                        }
                    ] }
                >
                    <AdvantagesTable />
                </Dialog>
            </Box>
        );
    }
}

export const ArchiveVerification = connect<TStateProps, TDispatchProps, NonNullable<unknown>, AppReduxStoreState>(
    ({ MyCompanyState: { myCompany } }) => ({
        company: myCompany,
    }),
    {
        dispatchReceiveMyCompanyThunk: ReceiveMyCompanyThunk.invoke
    }
)(ArchiveVerificationClass);