/* eslint-disable react/jsx-no-useless-fragment */
import { Visibility, VisibilityOff } from '@mui/icons-material';
import CloudUploadIcon from '@mui/icons-material/CloudUpload';
import { Button, FormControl, IconButton, InputAdornment, InputLabel, MenuItem, Select, SelectChangeEvent, TextField, Box, Link as MuiLink, FormControlLabel, Checkbox } from '@mui/material';
import LinearProgress from '@mui/material/LinearProgress';
import { retryFetch } from 'app/api/utility/retryFetch';
import { AppRoutes } from 'app/AppRoutes';
import { AppConsts } from 'app/common/constants';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { Dialog } from 'app/views/components/controls/Dialog';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { TextOut } from 'app/views/components/TextOut';
import { ArchiveVerification } from 'app/views/modules/CreateDatabase/LoadingDatabase/view/ArchiveVerification';
import { initialStateCreateDbState, stylePassword } from 'app/views/modules/CreateDatabase/LoadingDatabase/view/CreateDb/const';
import { CreateDbProps, CreateDbState } from 'app/views/modules/CreateDatabase/LoadingDatabase/view/CreateDb/types';
import { FileCardInfo } from 'app/views/modules/CreateDatabase/LoadingDatabase/view/FileCardInfo';
import { ProgressBar } from 'app/views/modules/CreateDatabase/LoadingDatabase/view/ProgressBar';
import { VerticalLinearStepper } from 'app/views/modules/CreateDatabase/LoadingDatabase/view/Stepper';
import { UserMappingTable } from 'app/views/modules/CreateDatabase/LoadingDatabase/view/UserMappingTable';
import { UserTableDistribution } from 'app/views/modules/CreateDatabase/LoadingDatabase/view/UserTableDistribution';
import { InputNameDt } from 'app/views/modules/CreateDatabase/views/InputNameDt';
import { ETypeFile } from 'app/web/api/CreateCustomDatabaseDt/request-dto';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { UserItemInputParams } from 'app/web/InterlayerApiProxy/MsBackupsApiProxy/zipDatabase';
import { RequestKind } from 'core/requestSender/enums';
import Sha1 from 'crypto-js/sha1';
import Sha256 from 'crypto-js/sha256';
import Sha512 from 'crypto-js/sha512';
import pbkdf2 from 'pbkdf2';
import React, { ChangeEvent } from 'react';
import Dropzone, { FileRejection } from 'react-dropzone';
import { RouteComponentProps, withRouter } from 'react-router-dom';

type AllProps = CreateDbProps & FloatMessageProps & RouteComponentProps;

const { createFromDt } = InterlayerApiProxy.getCreateCustomDatabaseDtApiProxy();
const {
    uploadFileAccessPrice,
    deleteFiles,
    getFileInfoById,
    zipDatabase,
    postFiles,
    getChunkUrls,
    endDownloadFile,
    dtToZipDatabase
} = InterlayerApiProxy.getMsBackupsApiProxy();

class CreateDbClass extends React.Component<AllProps, CreateDbState> {
    private readonly chunkSize = AppConsts.maxSizeChunkForBase * 1000000;

    private readonly dtFileHeader = '1CIBDmpF3';

    private abortController = new AbortController();

    public constructor(props: AllProps) {
        super(props);

        this.uploadFiles = this.uploadFiles.bind(this);
        this.deleteFile = this.deleteFile.bind(this);
        this.handleDrop = this.handleDrop.bind(this);
        this.handleDropError = this.handleDropError.bind(this);
        this.postApplication = this.postApplication.bind(this);
        this.getZip = this.getZip.bind(this);
        this.closeCreateDbDialog = this.closeCreateDbDialog.bind(this);
        this.openCreateDbDialog = this.openCreateDbDialog.bind(this);
        this.endCreating = this.endCreating.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleChangePassword = this.handleChangePassword.bind(this);
        this.handleClickShowPassword = this.handleClickShowPassword.bind(this);
        this.handleMouseDownPassword = this.handleMouseDownPassword.bind(this);
        this.addExtentions = this.addExtentions.bind(this);
        this.changeLoadingTo = this.changeLoadingTo.bind(this);
        this.handleUserForAdd = this.handleUserForAdd.bind(this);
        this.handleEmail = this.handleEmail.bind(this);
        this.getFileInfoById = this.getFileInfoById.bind(this);
        this.changeAdditionalOptions = this.changeAdditionalOptions.bind(this);
        this.closeWarningModal = this.closeWarningModal.bind(this);

        this.state = initialStateCreateDbState;
    }

    private handleUserForAdd(users: string[]) {
        const totalSum = this.state.uploadFileAccessPrice?.data.accountUserLicence.reduce((total, access) => {
            if (users.includes(access.accountUserId)) {
                total += access.accessCost;
            }

            return total;
        }, 0) ?? 0;

        this.setState({
            usersForAdd: users,
            totalSum
        });
    }

    private handleEmail(arrEmail: string[]) {
        this.setState({
            email: arrEmail
        });
    }

    private handleDrop(acceptedFiles: File[]) {
        if (!acceptedFiles.length) {
            return;
        }

        const file = acceptedFiles[0];

        if (file.name.includes('.dt')) {
            const validatedChunk = file.slice(0, 10);
            const reader = new FileReader();

            reader.readAsText(validatedChunk);
            reader.onload = () => {
                const result = reader.result?.slice(0, 9);
                const version = result?.slice(-1);

                if (result === this.dtFileHeader && version === '3') {
                    this.setState({
                        selectedFile: file
                    });
                    void this.uploadFiles();
                } else {
                    this.props.floatMessage.show(MessageType.Error, this.getErrorVersionDtMessage(version?.toString()));
                }
            };
        } else {
            this.setState({
                selectedFile: file
            });
            void this.uploadFiles();
        }
    }

    private handleDropError(fileRejections: FileRejection[]) {
        this.setState({
            errorUploadFile: [
                ...new Set(fileRejections
                    .flatMap(value => value.errors.map(error => error.code))
                )]
                .map(code => this.getDropErrorMessage(code))
                .join(', ')
        });
        this.props.floatMessage.show(MessageType.Error, this.state.errorUploadFile);
    }

    private handleChange(event: SelectChangeEvent) {
        this.setState(prevState => ({
            currentLogin: event.target.value,
            password: '',
            passwordIsValid: false,
            rules: prevState.userIsAdmin.find(item => item.login === event.target.value)?.hasPassword ? [3] : []
        }));
    }

    private handleChangePassword({ target: { value } }: ChangeEvent<HTMLInputElement>) {
        this.setState({ password: value });
        const currentUser = this.state.userIsAdmin.find(item => item.login === this.state.currentLogin);

        const { fileInfoById } = this.state;
        const strongPassword = fileInfoById?.strongPassword;
        const alg = currentUser?.alg?.toUpperCase() ?? 'SHA1';
        const salt = currentUser?.salt ?? '';

        let processedPassword = value;
        if (!strongPassword) {
            processedPassword = processedPassword.toUpperCase();
        }

        let computedHash: string;
        switch (alg) {
            case 'SHA1':
                computedHash = Sha1(processedPassword).toString();
                break;
            case 'SHA256':
                computedHash = Sha256(processedPassword + salt).toString();
                break;
            case 'SHA512':
                computedHash = Sha512(processedPassword + salt).toString();
                break;
            case 'PBKDF2SHA256':
                computedHash = pbkdf2.pbkdf2Sync(processedPassword, salt, 16000, 64, 'sha256').toString('hex');
                break;
            default:
                computedHash = '';
                break;
        }

        const targetHash = strongPassword
            ? currentUser?.hash1
            : currentUser?.hash2;

        const isValid = targetHash === computedHash;

        this.setState({
            passwordIsValid: isValid,
            rules: isValid ? [] : [3]
        });
    }

    private handleMouseDownPassword(event: React.MouseEvent<HTMLButtonElement>) {
        event.preventDefault();
    }

    private handleClickShowPassword() {
        this.setState(prev => ({
            showPassword: !prev.showPassword
        }));
    }

    private getDropErrorMessage(code: string) {
        switch (code) {
            case 'file-too-large':
                return 'Файл слишком большой';
            case 'file-too-small':
                return 'Файл слишком маленький';
            case 'too-many-files':
                return 'Выбрано слишком много файлов';
            case 'file-invalid-type':
                return 'Выберите архив с расширением .zip или .dt';
            default:
                return 'Ошибка добавления файла';
        }
    }

    private getErrorVersionDtMessage(version?: string) {
        return `Файл DT создан в 1С версии 8.${ version }. Платформа42 поддерживает загрузку только DT файлов, выгруженных из 1С версии 8.3.
                Вы можете выполнить загрузку в файловую базу или обновить 1С до актуальной версии и повторить операцию.`;
    }

    private async getFileInfoById() {
        const responseById = await getFileInfoById(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, { fileId: this.state.baseId, type: (this.state.selectedFile?.name ?? '').includes('.dt') ? 'dt' : 'data_dump' });

        if (responseById.status && !responseById.status.ready) {
            setTimeout(() => this.getFileInfoById(), (responseById.status?.timeout ?? 1) * 1000);
            return;
        }

        if ('message' in responseById) {
            this.props.floatMessage.show(MessageType.Error, responseById.message);
        }

        const userIsAdmin = responseById.users.filter(item => item.isAdmin && item.eauth);

        if (responseById.target === 'file') {
            this.setState({ loadingTo: 'dt' });
        }

        if (this.state.selectedFile?.name.includes('.dt') && responseById.errors.length) {
            this.setState({ loadingTo: 'dt' });
        }

        if (this.props.currency === 'грн') {
            this.setState({ loadingTo: 'dt' });
        }

        if (responseById.configuration.name) {
            this.setState({ name: responseById.configuration.name });
        }

        this.setState({
            fileInfoById: responseById,
            isFileChecked: true,
            configuration: responseById.configuration.id
        });

        if (this.state.selectedFile?.name.includes('.dt') && userIsAdmin.length > 0) {
            const isHasPassword = userIsAdmin.filter(item => !item.hasPassword);

            this.setState(prevState => ({
                userIsAdmin,
                currentLogin: (isHasPassword.length > 0 ? isHasPassword[0].login : userIsAdmin[0].login) ?? '',
                passwordIsValid: !(isHasPassword.length > 0 ? isHasPassword[0].hasPassword : userIsAdmin[0].hasPassword),
                rules: (isHasPassword.length > 0 || prevState.loadingTo === 'dt' ? prevState.rules.filter(item => item !== 3) : [3]),
            }));
        }

        if (!this.state.fileInfoById?.errors.length || this.state.fileInfoById?.type === 'dt' || this.state.fileInfoById?.target === 'file') {
            this.setState(prevState => ({ rules: prevState.rules.filter(item => item !== 2) }));

            uploadFileAccessPrice(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, { configurationId: this.state.configuration, configurationName: this.state.fileInfoById?.configuration.name ?? '' })
                .then(responseAccessPrice => {
                    if (responseAccessPrice.message) {
                        this.props.floatMessage.show(MessageType.Error, responseAccessPrice.message);
                    }

                    this.setState({ uploadFileAccessPrice: responseAccessPrice });
                });
        }
    }

    private getZip(value: UserItemInputParams[], allEmail: string[], notDistributed: string[], totalSum: number) {
        this.setState({
            zip: value,
            allEmail,
            notDistributed,
            totalSum
        });
    }

    private changeAdditionalOptions = (field: string, checked: boolean) => {
        this.setState(prevState => ({
            additionalOptions: {
                ...prevState.additionalOptions,
                [field]: checked
            }
        }));
    };

    private changeLoadingTo(to: 'zip' | 'dt') {
        if (to === 'dt') {
            this.setState(prevState => ({
                rules: prevState.rules.filter(rule => rule !== 3)
            }));
        }

        if (to === 'zip') {
            const isHasPassword = this.state.userIsAdmin.filter(item => !item.hasPassword);

            this.setState(prevState => ({
                currentLogin: (isHasPassword.length ? isHasPassword[0] : prevState.userIsAdmin[0]).login ?? '',
                passwordIsValid: !(isHasPassword.length ? isHasPassword[0] : prevState.userIsAdmin[0]).hasPassword,
                rules: (isHasPassword.length > 0 ? prevState.rules.filter(item => item !== 3) : [3]),
            }));
        }

        this.setState({
            loadingTo: to
        });
    }

    private closeCreateDbDialog() {
        this.setState({
            isCreateDatabase: false
        });

        if (this.state.conformation) {
            this.props.history.push(AppRoutes.accountManagement.informationBases);
        }
    }

    private addExtentions(value: string, checked: boolean) {
        if (checked) {
            this.setState(prevState => ({ extentions: [...prevState.extentions, value] }));
        } else {
            this.setState(prevState => ({ extentions: prevState.extentions.filter(extension => extension !== value) }));
        }
    }

    private endCreating() {
        this.setState({
            visibleLoader: true
        });
        setTimeout(() => {
            this.props.history.push(AppRoutes.accountManagement.informationBases);
        }, 2000);
    }

    private openCreateDbDialog() {
        this.setState({
            isCreateDatabase: true
        });
    }

    private async uploadFiles() {
        if (this.state.selectedFile) {
            this.abortController = new AbortController();
            this.setState({ upload: true, uploadState: true, goToNext: false });

            try {
                const { size, name } = this.state.selectedFile;

                const response = await postFiles(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, { size, name, type: name.includes('.dt') ? 'dt' : 'data_dump' });

                if ('message' in response) {
                    this.props.floatMessage.show(MessageType.Error, response.message);
                }

                let alreadySent = 0;

                let chunkUrls = response.parts;
                const partToLength = response.parts.length;

                this.setState({ shouldContinue: true });

                for (let start = 0, index = 0; start < this.state.selectedFile.size && this.state.shouldContinue; start += this.chunkSize, index++) {
                    const chunk = this.state.selectedFile.slice(start, start + this.chunkSize);
                    const fd = new FormData();
                    fd.set('data', chunk);
                    let isSuccess = false;

                    await retryFetch(() => fetch(
                        chunkUrls[index - alreadySent].url,
                        {
                            method: 'PUT',
                            headers: chunkUrls[index - alreadySent].headers,
                            body: fd.get('data'),
                            signal: this.abortController.signal
                        }
                    ), 3, 10000, this.abortController.signal.aborted)
                        .then(responseCloud => {
                            const eTag = responseCloud.headers.get('etag');

                            if (eTag) {
                                this.setState(prevState => ({ eTag: [...prevState.eTag, eTag.toString().replace(/[^a-zA-Z0-9]+/g, '')] }));
                            }

                            isSuccess = true;
                        })
                        .catch(async () => {
                            if (!this.abortController.signal.aborted) {
                                const chunkUrlsResponse = await getChunkUrls(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, {
                                    fileId: response.id,
                                    'parts-from': index + 1,
                                    'parts-to': partToLength
                                });

                                if (chunkUrlsResponse.parts.length) {
                                    chunkUrls = chunkUrlsResponse.parts;
                                    alreadySent = index;
                                    isSuccess = false;
                                    --index;
                                    start -= this.chunkSize;
                                } else {
                                    this.props.floatMessage.show(MessageType.Error, 'message' in chunkUrlsResponse ? chunkUrlsResponse.message : 'Возникла ошибка при загрузке файла. Попробуйте еще раз');
                                    this.setState({ shouldContinue: false });
                                }
                            }
                        });

                    if (isSuccess) {
                        this.setState(prevState => ({ progress: Math.min(prevState.progress + ((this.chunkSize / (prevState.selectedFile?.size ?? 1)) * 100), 100) }));
                        if (this.state.selectedFile) {
                            this.setState(prevState => ({
                                errorUploadFile: '',
                                baseId: response.id,
                                rules: prevState.rules.filter(item => item !== 1),
                            }));
                        }
                    }
                }

                const endDownloadFileResult = await endDownloadFile(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, { fileId: this.state.baseId, parts: this.state.eTag });

                if ('message' in endDownloadFileResult) {
                    this.props.floatMessage.show(MessageType.Error, endDownloadFileResult.message);
                }

                this.setState({ uploadState: true, goToNext: true });

                void this.getFileInfoById();
            } catch { /* empty */ }
        }
    }

    private async deleteFile() {
        this.abortController.abort();
        this.setState({ isFileChecked: false });

        if (this.state.uploadState) {
            try {
                const result = await deleteFiles(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, { fileId: this.state.baseId });

                if ('message' in result) {
                    this.props.floatMessage.show(MessageType.Error, result.message);
                }
            } catch (err: unknown) {
                this.props.floatMessage.show(MessageType.Error, err);
            }
        }

        this.setState({ ...initialStateCreateDbState });
    }

    private async postApplication() {
        try {
            if (this.state.selectedFile && this.state.selectedFile.name.includes('.dt')) {
                if (this.state.loadingTo === 'zip') {
                    const { message } = await dtToZipDatabase(
                        RequestKind.SEND_BY_USER_ASYNCHRONOUSLY,
                        {
                            fileId: this.state.baseId,
                            databasesServiceId: this.state.uploadFileAccessPrice?.data.serviceTypesId ?? '',
                            name: this.state.name,
                            users: this.state.zip ?? [],
                            configuration: this.state.configuration,
                            needUsePromisePayment: this.props.isPromise,
                            password: this.state.password,
                            login: this.state.currentLogin
                        }
                    );

                    if (message) {
                        this.props.floatMessage.show(MessageType.Error, message);
                    }
                } else {
                    const { message } = await createFromDt(
                        RequestKind.SEND_BY_USER_ASYNCHRONOUSLY,
                        {
                            login: this.state.currentLogin,
                            currentVersion: this.state.fileInfoById?.configuration.version ?? null,
                            password: this.state.password,
                            configurationName: this.state.fileInfoById?.configuration.name ?? '',
                            dbCaption: this.state.name,
                            needUsePromisePayment: this.props.isPromise,
                            uploadedFileId: this.state.baseId,
                            usersIdForAddAccess: this.state.usersForAdd,
                            typeFile: ETypeFile.DtFile,
                            hasSupport: this.state.additionalOptions.hasSupport,
                            hasAutoupdate: this.state.additionalOptions.hasAutoupdate
                        }
                    );

                    if (message) {
                        this.props.floatMessage.show(MessageType.Error, message);
                    }
                }
            } else if (this.state.loadingTo === 'dt') {
                const { message } = await createFromDt(
                    RequestKind.SEND_BY_USER_ASYNCHRONOUSLY,
                    {
                        configurationId: this.state.fileInfoById?.configuration.id,
                        login: this.state.currentLogin,
                        currentVersion: this.state.fileInfoById?.configuration.version ?? null,
                        password: this.state.password,
                        configurationName: this.state.fileInfoById?.configuration.name ?? '',
                        dbCaption: this.state.name,
                        needUsePromisePayment: this.props.isPromise,
                        uploadedFileId: this.state.baseId,
                        usersIdForAddAccess: this.state.usersForAdd,
                        typeFile: ETypeFile.ZipFile,
                        hasSupport: this.state.additionalOptions.hasSupport,
                        hasAutoupdate: this.state.additionalOptions.hasAutoupdate
                    }
                );

                if (message) {
                    this.props.floatMessage.show(MessageType.Error, message);
                }
            } else {
                const { message } = await zipDatabase(
                    RequestKind.SEND_BY_USER_ASYNCHRONOUSLY,
                    {
                        fileId: this.state.baseId,
                        databasesServiceId: this.state.uploadFileAccessPrice?.data.serviceTypesId ?? '',
                        name: this.state.name,
                        users: this.state.zip ?? [],
                        configuration: this.state.configuration,
                        needUsePromisePayment: this.props.isPromise,
                        extentions: this.state.extentions,
                    }
                );

                if (message) {
                    this.props.floatMessage.show(MessageType.Error, message);
                }
            }
        } catch (err: unknown) {
            this.props.floatMessage.show(MessageType.Error, err);
        }
    }

    private closeWarningModal() {
        this.setState({ showWarning: { autoUpdate: false, support: false } });
    }

    public render() {
        return (
            <>
                { this.state.visibleLoader && <LoadingBounce /> }
                <VerticalLinearStepper
                    totalSum={ this.state.totalSum }
                    linkToBack={ AppRoutes.accountManagement.informationBases }
                    rules={ this.state.rules }
                    goToNext={ this.state.goToNext }
                    steps={
                        [
                            {
                                hideNextButton: !this.state.goToNext,
                                label: 'Выберите сохраненный файл с данными вашей информационной базы.',
                                description: (
                                    <>
                                        {
                                            !this.state.upload
                                                ? (
                                                    <Box display="flex" gap="16px" alignItems="center">
                                                        <Dropzone
                                                            onDrop={ this.handleDrop }
                                                            onDropRejected={ this.handleDropError }
                                                            maxFiles={ 1 }
                                                            multiple={ false }
                                                            noClick={ true }
                                                            accept={ {
                                                                'application/zip': ['.zip'],
                                                                'application/x-zip-compressed': ['.zip'],
                                                                'application/octet-stream': ['.dt']
                                                            } }
                                                            onDragEnter={ e => {
                                                                if (e.dataTransfer.items[0].kind === 'file' && e.dataTransfer.items[0].type === '') {
                                                                    this.setState({
                                                                        isDragUnknown: true
                                                                    });
                                                                    return;
                                                                }
                                                                this.setState({
                                                                    isDragUnknown: false
                                                                });
                                                            } }
                                                            onDragLeave={ () => this.setState({ isDragUnknown: false }) }
                                                        >
                                                            { ({ getRootProps, getInputProps, isDragActive, isDragReject }) => (
                                                                <section>
                                                                    <div { ...getRootProps() }>
                                                                        <Button
                                                                            variant="contained"
                                                                            color={ isDragActive ? (isDragReject && !this.state.isDragUnknown ? 'error' : 'success') : 'primary' }
                                                                            component="label"
                                                                            endIcon={ <CloudUploadIcon /> }
                                                                            style={ { margin: '16px 0' } }
                                                                        >
                                                                            Выберите или перетащите сюда файл { this.props.sum !== 0 ? `(${ this.props.sum } ${ this.props.currency })` : '' }
                                                                            <input
                                                                                { ...getInputProps() }
                                                                            />
                                                                        </Button>
                                                                    </div>
                                                                </section>
                                                            ) }
                                                        </Dropzone>
                                                        <Box display="flex" flexDirection="column">
                                                            <TextOut fontWeight={ 600 } fontSize={ 13 }>Вы можете загрузить в сервис базу в следующих форматах:</TextOut>
                                                            <MuiLink
                                                                href="https://42clouds.com/ru-ru/manuals/kak-vygruzit-dannye-iz-1s-bp3-0-v-formate-fayla-data-dump-zip/"
                                                                target="_blank"
                                                                rel="noopener noreferrer"
                                                            >
                                                                .zip (data_dump)
                                                            </MuiLink>
                                                            <MuiLink
                                                                href="https://42clouds.com/ru-ru/manuals/kak-vygruzit-s-bazy-1s-fayl-dt/"
                                                                target="_blank"
                                                                rel="noopener noreferrer"
                                                            >
                                                                .dt (выгрузка информационной базы из конфигуратора)
                                                            </MuiLink>
                                                        </Box>
                                                    </Box>
                                                )
                                                : this.state.upload && this.state.selectedFile && (
                                                    <>
                                                        <FileCardInfo
                                                            name={ this.state.selectedFile.name ?? '' }
                                                            size={ this.state.selectedFile.size }
                                                            deleteFile={ this.deleteFile }
                                                            isDisable={ this.state.baseId === '' }
                                                        />
                                                        <ProgressBar progress={ this.state.progress } />
                                                    </>
                                                )
                                        }
                                    </>
                                ),
                            },
                            {
                                label: 'Результат проверки файла.',
                                description: this.state.isFileChecked
                                    ? (
                                        <>
                                            <InputNameDt
                                                setName={ name => this.setState({ name }) }
                                                name={ this.state.fileInfoById?.configuration.name }
                                                isFullWidth={ false }
                                            />
                                            <ArchiveVerification
                                                configuration={ this.state.fileInfoById?.configuration.name ?? '' }
                                                configurationSupported={ this.state.fileInfoById?.configuration.configurationSupported ?? false }
                                                version={ this.state.fileInfoById?.configuration.version ?? '' }
                                                versionSupported={ this.state.fileInfoById?.configuration.versionSupported ?? false }
                                                extensions={ this.state.fileInfoById?.extensions ?? [] }
                                                error={ this.state.fileInfoById?.errors ?? [] }
                                                addExtentions={ this.addExtentions }
                                                selectedExtentions={ this.state.extentions }
                                                isDt={ this.state.selectedFile?.name.includes('.dt') ?? false }
                                                messages={ this.state.fileInfoById?.messages ?? [] }
                                                loadingTo={ this.state.loadingTo }
                                                changeLoadingTo={ this.changeLoadingTo }
                                            />
                                        </>
                                    )
                                    : <LinearProgress sx={ { width: '60%' } } />,
                            },
                            {
                                label: this.state.loadingTo === 'zip' ? 'Сопоставьте пользователей и укажите профили доступа.' : 'Настройте доступы пользователей',
                                description: (
                                    <>
                                        {
                                            this.state.selectedFile?.name.includes('.dt') && this.state.loadingTo === 'zip'
                                                ? (
                                                    <Box display="flex" flexDirection="column" gap="16px">
                                                        {
                                                            this.state.userIsAdmin.length
                                                                ? (
                                                                    <TextOut fontSize={ 14 }>Учетная запись администратора исходной базы:</TextOut>
                                                                )
                                                                : null
                                                        }
                                                        <Box display="flex" gap="16px" marginBottom="16px">
                                                            {
                                                                this.state.userIsAdmin.length
                                                                    ? (
                                                                        <FormControl>
                                                                            <InputLabel id="login">Логин</InputLabel>
                                                                            <Select
                                                                                variant="outlined"
                                                                                labelId="login"
                                                                                id="login"
                                                                                value={ this.state.currentLogin }
                                                                                label="Логин"
                                                                                onChange={ this.handleChange }
                                                                                sx={ { width: '300px' } }
                                                                            >
                                                                                {
                                                                                    this.state.userIsAdmin.map(item => {
                                                                                        return <MenuItem key={ item.accountUserId } value={ item.login }>{ item.name }</MenuItem>;
                                                                                    })
                                                                                }
                                                                            </Select>
                                                                        </FormControl>
                                                                    )
                                                                    : null
                                                            }
                                                            {
                                                                this.state.userIsAdmin.find(item => item.login === this.state.currentLogin)?.hasPassword &&
                                                                <FormControl variant="outlined">
                                                                    <TextField
                                                                        id="currentPassword"
                                                                        value={ this.state.password }
                                                                        onChange={ this.handleChangePassword }
                                                                        autoComplete="off"
                                                                        InputProps={ {
                                                                            endAdornment: (
                                                                                <InputAdornment position="end">
                                                                                    <IconButton
                                                                                        aria-label="toggle password visibility"
                                                                                        onClick={ this.handleClickShowPassword }
                                                                                        onMouseDown={ this.handleMouseDownPassword }
                                                                                        edge="end"
                                                                                    >
                                                                                        { this.state.showPassword ? <VisibilityOff /> : <Visibility /> }
                                                                                    </IconButton>
                                                                                </InputAdornment>
                                                                            ),
                                                                        } }
                                                                        InputLabelProps={ {
                                                                            shrink: true
                                                                        } }
                                                                        sx={ !this.state.showPassword ? stylePassword : { width: '300px' } }
                                                                        label="Текущий пароль"
                                                                        error={ !this.state.passwordIsValid }
                                                                        size="small"
                                                                    />
                                                                </FormControl>
                                                            }
                                                        </Box>
                                                    </Box>
                                                )
                                                : <div />
                                        }
                                        {
                                            this.state.loadingTo === 'dt' && (
                                                <Box display="flex" gap="8px">
                                                    <FormControlLabel
                                                        control={
                                                            <Checkbox
                                                                checked={ this.state.additionalOptions.hasAutoupdate }
                                                                onChange={ e => {
                                                                    if (this.state.firstShow.autoUpdate) {
                                                                        this.setState(prev => ({ showWarning: { ...prev.showWarning, autoUpdate: true }, firstShow: { ...prev.firstShow, autoUpdate: false } }));
                                                                    } else {
                                                                        this.changeAdditionalOptions('hasAutoupdate', e.target.checked);
                                                                    }
                                                                } }
                                                            />
                                                        }
                                                        label="Подключить АО"
                                                    />
                                                    <FormControlLabel
                                                        control={
                                                            <Checkbox
                                                                checked={ this.state.additionalOptions.hasSupport }
                                                                onChange={ e => {
                                                                    if (this.state.firstShow.support) {
                                                                        this.setState(prev => ({ showWarning: { ...prev.showWarning, support: true }, firstShow: { ...prev.firstShow, support: false } }));
                                                                    } else {
                                                                        this.changeAdditionalOptions('hasSupport', e.target.checked);
                                                                    }
                                                                } }
                                                            />
                                                        }
                                                        label="Подключить ТиИ"
                                                    />
                                                </Box>
                                            )
                                        }
                                        {
                                            this.state.loadingTo === 'dt'
                                                ? (
                                                    <UserTableDistribution
                                                        select={ this.handleUserForAdd }
                                                        adminEmail={ this.handleEmail }
                                                        initSelect={ [] }
                                                    />
                                                )
                                                : (
                                                    <UserMappingTable
                                                        allUsers={ this.state.fileInfoById?.users ?? [] }
                                                        allProfiles={ this.state.fileInfoById?.profiles ?? [] }
                                                        zip={ this.getZip }
                                                        accessPrice={ this.state.uploadFileAccessPrice }
                                                        currency={ this.props.currency }
                                                    />
                                                )
                                        }
                                    </>
                                )
                            },
                        ]
                    }
                    finalizeStep={ this.openCreateDbDialog }
                    enoughMoney={ (this.props.sum + this.state.totalSum) <= this.props.balance }
                    notEnoughMoney={ Math.abs(this.props.balance - this.state.totalSum - this.props.sum) }
                    currency={ this.props.currency }
                />
                <Dialog
                    isOpen={ this.state.isCreateDatabase }
                    dialogWidth="sm"
                    dialogVerticalAlign="center"
                    onCancelClick={ this.closeCreateDbDialog }
                    title="Создание базы 1С"
                    buttons={
                        !this.state.conformation
                            ? [
                                {
                                    content: 'Отмена',
                                    variant: 'contained',
                                    kind: 'default',
                                    onClick: this.closeCreateDbDialog
                                },
                                {
                                    content: this.props.sum !== 0 || this.state.totalSum > 0
                                        ? `Создать базу (${ this.props.sum + this.state.totalSum } ${ this.props.currency })`
                                        : 'Создать базу',
                                    variant: 'contained',
                                    kind: 'success',
                                    onClick: () => {
                                        this.setState({
                                            conformation: true
                                        });
                                        return this.postApplication();
                                    }
                                }
                            ]
                            : [
                                {
                                    content: 'Завершить',
                                    variant: 'contained',
                                    kind: 'success',
                                    onClick: this.endCreating
                                }
                            ]
                    }
                >
                    {
                        !this.state.conformation
                            ? (
                                <>
                                    <TextOut fontWeight={ 700 }>Внимание!</TextOut><br />
                                    {
                                        !!this.state.notDistributed.length && (
                                            <><TextOut>Для пользователей 1С ({ this.state.notDistributed.join(', ') }) не задано соответствие пользователей аккаунта.</TextOut><br /></>
                                        )
                                    }
                                    <TextOut>Созданная база будет доступна только тем пользователям, для которых выставлено соответствие.</TextOut>
                                </>
                            )
                            : (
                                <>
                                    <TextOut>Проводится создание базы 1С.</TextOut><br />
                                    <TextOut>После завершения процесса на эл. почту { this.state.loadingTo === 'zip' ? this.state.allEmail.join(', ') : this.state.email.join(', ') } будет отправлено уведомление.</TextOut>
                                </>
                            )
                    }
                </Dialog>
                <Dialog
                    title="Внимание!"
                    isOpen={ this.state.showWarning.autoUpdate }
                    dialogVerticalAlign="center"
                    dialogWidth="sm"
                    onCancelClick={ this.closeWarningModal }
                    buttons={ [
                        {
                            content: 'Отмена',
                            kind: 'default',
                            onClick: this.closeWarningModal
                        },
                        {
                            content: 'Отключить',
                            kind: 'error',
                            onClick: () => {
                                this.changeAdditionalOptions('hasAutoupdate', !this.state.additionalOptions.hasAutoupdate);
                                this.closeWarningModal();
                            }
                        }
                    ] }
                >
                    <TextOut>
                        Уважаемый пользователь. Рекомендуем оставить функцию &quot;Автоматического обновления&quot; включенной.&nbsp;
                        Она не только помогает поддерживать базу в актуальном состоянии, но обеспечивает более надежную и эффективную работу базы 1С.
                    </TextOut>
                </Dialog>
                <Dialog
                    title="Внимание!"
                    isOpen={ this.state.showWarning.support }
                    dialogVerticalAlign="center"
                    dialogWidth="sm"
                    onCancelClick={ this.closeWarningModal }
                    buttons={ [
                        {
                            content: 'Отмена',
                            kind: 'default',
                            onClick: this.closeWarningModal
                        },
                        {
                            content: 'Отключить',
                            kind: 'error',
                            onClick: () => {
                                this.changeAdditionalOptions('hasSupport', !this.state.additionalOptions.hasSupport);
                                this.closeWarningModal();
                            }
                        }
                    ] }
                >
                    <TextOut>
                        Уважаемый пользователь. Рекомендуем оставить функцию &quot;Тестирования и исправления ошибок&quot; включенной.&nbsp;
                        Она не только помогает устранять ошибки в работе информационной базы, но и способствует повышению скорости её работы.
                    </TextOut>
                </Dialog>
            </>
        );
    }
}

export const CreateDb = withRouter(withFloatMessages(CreateDbClass));