import { FETCH_API } from 'app/api/useFetchApi';
import { AppRoutes } from 'app/AppRoutes';
import { DatabaseState } from 'app/common/enums';
import { EMessageType, useFloatMessages } from 'app/hooks';
import { COLORS } from 'app/utils';
import { PageHeaderView } from 'app/views/components/_hoc/withHeader/PageHeaderView';
import { Dialog } from 'app/views/components/controls/Dialog';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { TextOut } from 'app/views/components/TextOut';
import { EnoughMoneyButton } from 'app/views/modules/CreateDatabase/LoadingDatabase/view/EnoughMoneyButton';
import { StatusUpload } from 'app/views/modules/CreateDatabase/LoadingDatabase/view/StatusUpload';
import { VerticalLinearStepper } from 'app/views/modules/CreateDatabase/LoadingDatabase/view/StepperRestore';
import { StepsType } from 'app/views/modules/CreateDatabase/LoadingDatabase/view/StepperRestore/types';
import { UserTableDistribution } from 'app/views/modules/CreateDatabase/LoadingDatabase/view/UserTableDistributionRestore';
import { InputNameDt } from 'app/views/modules/CreateDatabase/views/InputNameDt';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { CostOfCreatingDatabasesModel } from 'app/web/InterlayerApiProxy/MsBackupsApiProxy/costOfCreatingDatabases';
import { RequestKind } from 'core/requestSender/enums';
import React, { useCallback, useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import { Props } from './types';

const { costOrCreatingDatabases } = InterlayerApiProxy.getMsBackupsApiProxy();
const { postActivateRentIfNeed } = FETCH_API.CRETE_DB;

export const RestoreDbFile = ({ backupId, restoreType, state }: Props) => {
    const { show } = useFloatMessages();
    const history = useHistory();

    const [nameDatabase, setNameDatabase] = useState('');
    const [costOrCreating, setCostOrCreating] = useState<CostOfCreatingDatabasesModel>();
    const [loading, setLoading] = useState(false);
    const [enoughMoney, setEnoughMoney] = useState(0);
    const [currency, setCurrency] = useState('');
    const [isPromise, setIsPromise] = useState(false);
    const [usersForAdd, setUsersForAdd] = useState<string[]>([]);
    const [adminEmail, setAdminEmail] = useState<string[]>([]);
    const [uploadFileId, setUploadFileId] = useState('');
    const [uploadedFileStatus, setUploadedFileStatus] = useState('Файл в процессе загрузки');
    const [rules, setRules] = useState([0]);
    const [createDbModal, setCreateDbModal] = useState(false);

    const handlePromisePayment = useCallback((promise: boolean) => setIsPromise(promise), []);
    const handleNameDatabase = useCallback(name => setNameDatabase(name), []);
    const handleUserForAdd = useCallback((users: string[]) => setUsersForAdd(users), []);

    const statusCheck = useCallback(() => {
        if (uploadedFileStatus === 'Файл загружен успешно') {
            return setRules([]);
        }

        if (uploadedFileStatus === 'Ошибка загрузки') {
            return show(EMessageType.error, 'При загрузке архива произошла ошибка');
        }

        if (uploadedFileStatus === 'Загруженный файл удален') {
            return show(EMessageType.error, 'При загрузке архива произошла ошибка (Загруженный файл удален)');
        }

        if (uploadFileId !== '' && uploadedFileStatus !== 'Файл загружен успешно') {
            setTimeout(() => {
                FETCH_API.RESTORE_DATABASE_BACKUP.getUploadStatus({
                    uploadedFileId: uploadFileId
                }).then(response => {
                    setUploadedFileStatus(response.data?.statusComment ?? '');

                    if (response.message) {
                        show(EMessageType.error, response.message);
                    }

                    if (response.data?.statusComment === 'Файл в процессе загрузки') {
                        statusCheck();
                    }
                });
            }, 10000);
        }
    }, [show, uploadFileId, uploadedFileStatus]);

    const restoreDb = async () => {
        setCreateDbModal(true);
        const { message } = await FETCH_API.RESTORE_DATABASE_BACKUP.postRestore({
            accountDatabaseBackupId: backupId,
            accountDatabaseName: nameDatabase,
            myDatabasesServiceTypeId: '',
            needUsePromisePayment: isPromise,
            restoreType: parseInt(restoreType, 10),
            uploadedFileId: uploadFileId,
            usersIdForAddAccess: usersForAdd
        });

        if (message) {
            show(EMessageType.error, message);
        }
    };

    useEffect(() => {
        window.scroll(0, 0);
    }, []);

    /**
     * Get information about the possibility of creating a database
     */
    useEffect(() => {
        try {
            void postActivateRentIfNeed();
            costOrCreatingDatabases(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY)
                .then(response => {
                    if (response.message) {
                        show(EMessageType.error, response.message);
                    }

                    setCostOrCreating(response);
                    setEnoughMoney(response.data.enoughMoney);
                    setCurrency(response.data.currency);
                    setLoading(true);
                });
        } catch (er: unknown) {
            show(EMessageType.error, 'При проверки статуса создания базы произошла ошибка. Пожалуйста, повторите позже.');
        }
    }, [show]);

    /**
     * Get backup info
     */
    useEffect(() => {
        FETCH_API.RESTORE_DATABASE_BACKUP.getDatabaseBackup({ id: backupId })
            .then(response => {
                const res = response.data;

                if (response.message) {
                    show(EMessageType.error, response.message);
                }

                if (res) {
                    setNameDatabase(res.accountDatabaseName);
                    setAdminEmail(res.emailAddresses);
                    setUploadFileId(res.uploadedFileId);
                }
            });
    }, [backupId, show]);

    /**
     * Start the process of downloading the backup
     */
    useEffect(() => {
        if (uploadFileId !== '') {
            (async () => {
                const result = await FETCH_API.RESTORE_DATABASE_BACKUP.putStartFetching({
                    backupId,
                    uploadedFileId: uploadFileId
                });

                if (result.message) {
                    show(EMessageType.error, result.message);
                }
            })();
        }
    }, [backupId, show, uploadFileId]);

    useEffect(() => {
        statusCheck();
    }, [statusCheck]);

    const redirectToDbPage = () => {
        setCreateDbModal(false);
        setLoading(false);
        setTimeout(() => {
            history.push(AppRoutes.accountManagement.informationBases);
        }, 5000);
    };

    const getSteps = () => {
        const steps: StepsType[] = [
            {
                label: 'Загрузка файла архивной копии для восстановления',
                description: <StatusUpload status={ uploadedFileStatus } />
            },
        ];

        if (restoreType !== '0') {
            steps.push({
                label: 'Введите название базы, которая будет создана из архивной копии',
                description: <InputNameDt
                    name={ nameDatabase }
                    setName={ handleNameDatabase }
                    isFullWidth={ false }
                />
            });
        }

        if (restoreType !== '0' || (restoreType === '0' && state !== DatabaseState.Ready)) {
            steps.push({
                label: 'Настройте доступы',
                description: <UserTableDistribution
                    select={ handleUserForAdd }
                    adminEmail={ () => void 0 }
                    initSelect={ [] }
                />
            });
        }

        return steps;
    };

    return (
        <>
            <PageHeaderView title="Восстановление информационной базы" />
            { !loading && <LoadingBounce /> }
            { costOrCreating
                ? (
                    <VerticalLinearStepper
                        steps={ getSteps() }
                        rules={ rules }
                        finalizeStep={ restoreDb }
                        finalizeStepContent="Создать базу"
                    />
                )
                : (
                    <EnoughMoneyButton
                        enoughMoney={ enoughMoney }
                        costOrCreating={ costOrCreating }
                        loading={ loading }
                        currency={ currency }
                        setIsPromise={ handlePromisePayment }
                    />
                )
            }
            <Dialog
                isOpen={ createDbModal }
                dialogWidth="sm"
                dialogVerticalAlign="center"
                onCancelClick={ redirectToDbPage }
                buttons={ [
                    {
                        kind: 'success',
                        content: 'Создать',
                        onClick: redirectToDbPage
                    }
                ] }
            >
                Производится создание базы 1с.
                После завершения процесса Вам на эл. почту {
                    adminEmail.map((item, index) => <TextOut key={ item } fontSize={ 14 } style={ { color: COLORS.link } }>{ item }{ index !== adminEmail.length - 1 ? ',' : '' } </TextOut>)
                } будет отправлено уведомление.
            </Dialog>
        </>
    );
};