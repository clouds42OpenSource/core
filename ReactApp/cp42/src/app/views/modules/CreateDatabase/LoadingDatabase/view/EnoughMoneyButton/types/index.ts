import { CostOfCreatingDatabasesModel } from 'app/web/InterlayerApiProxy/MsBackupsApiProxy/costOfCreatingDatabases';

export type StateProps = {
    /**
     * Состояние загрузки
     */
    loading: boolean;
    /**
     * Не хватает денег
     */
    enoughMoney: number;
    /**
     * Валюта
     */
    currency: string;
    /**
     * Данные о возможности создать новую ИБ
     */
    costOrCreating?: CostOfCreatingDatabasesModel;
    /**
     * Выбор обещанного платежа
     */
    setIsPromise: (isPromise: boolean) => void;
};