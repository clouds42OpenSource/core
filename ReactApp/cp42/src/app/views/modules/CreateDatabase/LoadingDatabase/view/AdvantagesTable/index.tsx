import { Box, TableHead } from '@mui/material';
import ErrorOutlineIcon from '@mui/icons-material/ErrorOutline';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableRow from '@mui/material/TableRow';
import { COLORS } from 'app/utils';
import { TextOut } from 'app/views/components/TextOut';
import CheckIcon from '@mui/icons-material/Check';

const advantages = [
    {
        description: 'Не требует оплаты за "Мой диск"',
        pl42: true,
        file: false,
    },
    {
        description: 'Автоматическое резервное копирование',
        pl42: true,
        file: false,
    },
    {
        description: 'Подключение сервисов из Маркет42',
        pl42: true,
        file: false,
    },
    {
        description: 'Управление профилями пользователей из ЛК',
        pl42: true,
        file: false,
    },
    {
        description: 'Доступ к конфигуратору',
        pl42: false,
        file: true,
    }
];

export const AdvantagesTable = () => {
    return (
        <Box display="flex" flexDirection="column" gap="16px" alignItems="center">
            <ErrorOutlineIcon style={ { color: COLORS.warning, width: '100px', height: '100px' } } />
            <TextOut fontWeight={ 700 }>Файловая база предоставляет не все возможности сервиса</TextOut>
            <TableContainer>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell size="small" />
                            <TableCell size="small" align="center">Разделители</TableCell>
                            <TableCell size="small" align="center">Файловая база</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {
                            advantages.map(item => (
                                <TableRow key={ item.description }>
                                    <TableCell size="small">{ item.description }</TableCell>
                                    <TableCell size="small" align="center">{ item.pl42 ? <CheckIcon style={ { color: COLORS.main } } /> : null }</TableCell>
                                    <TableCell size="small" align="center">{ item.file ? <CheckIcon style={ { color: COLORS.main } } /> : null }</TableCell>
                                </TableRow>
                            ))
                        }
                    </TableBody>
                </Table>
            </TableContainer>
        </Box>
    );
};