import { DatabaseState } from 'app/common/enums';

export type TLocationProps = {
    isZip?: boolean;
    fileId?: string;
    caption?: string;
    date?: string;
    restoreType?: string;
    state?: DatabaseState;
};

export type TLocationPropsOld = {
    isZip?: boolean;
    isDt?: boolean;
};