import { Link } from 'react-router-dom';
import { AppRoutes } from 'app/AppRoutes';
import { DefaultButton } from 'app/views/components/controls/Button';
import Box from '@mui/material/Box';
import css from './style.module.css';

export const GoToInfoBasesPage = () => {
    return (
        <Box className={ css.container }>
            <Link to={ AppRoutes.accountManagement.informationBases }>
                <DefaultButton>
                    Отменить
                </DefaultButton>
            </Link>
        </Box>
    );
};