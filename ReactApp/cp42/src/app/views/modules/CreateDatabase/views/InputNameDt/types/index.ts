export type Props = {
    name?: string;
    setName: (name: string) => void;
    isFullWidth?: boolean;
    noNeedToCheckUsedName?: boolean;
    setError?: (error: string) => void;
};