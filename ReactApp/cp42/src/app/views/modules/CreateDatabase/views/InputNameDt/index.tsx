import { TextField } from '@mui/material';
import { FETCH_API } from 'app/api/useFetchApi';
import { useDebounce } from 'app/hooks';
import { validate } from 'app/views/modules/CreateDatabase/const';
import React, { memo, useCallback, useEffect, useState } from 'react';
import { Props } from './types';

export const InputNameDt = memo(({ name, setName, noNeedToCheckUsedName, setError, isFullWidth = true }: Props) => {
    /**
     * Имя базы
     */
    const [nameDatabase, setNameDatabase] = useState(name ?? 'Своя база');

    const debouncedNameDatabase = useDebounce(nameDatabase);

    /**
     * Недопустимые символы в название базы
     */
    const [errorSymbol, setErrorSymbol] = useState('');

    /**
     * Инициализация состояния
     */
    const [isInitial, setIsInitial] = useState(true);

    /**
     * Используется ли имя
     */
    const [isNameUsed, setIsNameUsed] = useState(false);

    useEffect(() => {
        if (isInitial) {
            setNameDatabase(nameDatabase
                .split('')
                .map(item => {
                    if (validate.nameDatabase.test(item)) {
                        return item;
                    }

                    return '';
                })
                .join(''));
        }
        setIsInitial(false);
    }, [isInitial, nameDatabase]);

    useEffect(() => {
        if (debouncedNameDatabase !== null && !noNeedToCheckUsedName) {
            (async () => {
                const response = await FETCH_API.ACCOUNT_DATABASES.checkDatabaseName({
                    name: debouncedNameDatabase as string
                });

                if (response.success) {
                    setIsNameUsed(!!response.data);
                }
            })();
        }
    }, [debouncedNameDatabase, noNeedToCheckUsedName]);

    useEffect(() => {
        setNameDatabase(name ?? '');
    }, [name]);

    const onChange = useCallback((event: React.ChangeEvent<HTMLInputElement>) => {
        setNameDatabase(event.target.value);

        if (!validate.nameDatabase.test(event.target.value)) {
            const error = event.target.value
                .split('')
                .filter(item => !validate.nameDatabase.test(item))
                .join('');

            setErrorSymbol(error);

            if (setError) {
                setError(error);
            }
        } else {
            setName(event.target.value);
            setErrorSymbol('');

            if (setError) {
                setError('');
            }
        }
    }, [setError, setName]);

    if (errorSymbol.length || isNameUsed || nameDatabase.length > 100) {
        return (
            <TextField
                error={ !!errorSymbol.length || isNameUsed || nameDatabase.length > 100 }
                helperText={ isNameUsed ? 'Внимание! База с таким именем уже существует ' : nameDatabase.length > 100
                    ? 'Название базы не должно превышать 100 символов' : `В названии БД указаны недопустимые символы ${ errorSymbol.split('').join(', ') }` }
                variant="outlined"
                size="small"
                required={ true }
                fullWidth={ isFullWidth }
                value={ nameDatabase }
                sx={ { paddingTop: '8px',
                    width: isFullWidth ? '100%' : '60%',
                    '& .MuiFormHelperText-root': {
                        color: isNameUsed ? '#ff9800 !important' : '#ed5565'
                    },
                    '& .MuiOutlinedInput-notchedOutline': {
                        borderColor: isNameUsed ? '#ff9800 !important' : 'inherit'
                    }
                } }
                onChange={ onChange }
                FormHelperTextProps={ {
                    sx: { fontSize: '11px', margin: 0 }
                } }
            />
        );
    }

    return (
        <TextField
            variant="outlined"
            size="small"
            fullWidth={ isFullWidth ?? true }
            required={ true }
            value={ nameDatabase }
            onChange={ onChange }
            sx={ isFullWidth ? { width: '100%' } : { width: '60%' } }
            FormHelperTextProps={ {
                sx: { fontSize: '11px', margin: 0 }
            } }
        />
    );
});