import AddIcon from '@mui/icons-material/Add';
import { Autocomplete, Box, Chip, FormControl, FormHelperText, MenuItem, Select, TextField } from '@mui/material';
import { ELogEvent } from 'app/api/endpoints/global/enum';
import { FETCH_API } from 'app/api/useFetchApi';
import { EMessageType, useAppSelector, useFloatMessages } from 'app/hooks';
import { fileToBase64 } from 'app/utils';
import { withHeader } from 'app/views/components/_hoc/withHeader';
import { DefaultButton } from 'app/views/components/controls/Button';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { BoxWithPicture } from 'app/views/modules/_common/reusable-modules/BoxWithPicture';
import { CONFIGURATIONS } from 'app/views/modules/_common/reusable-modules/BoxWithPicture/constants';
import { consultations1CInitialValues, consultations1CSchema } from 'app/views/modules/_common/reusable-modules/BoxWithPicture/schemas';
import { allowedCountries } from 'app/views/modules/ToPartners/config/constants';
import dayjs from 'dayjs';
import { useFormik } from 'formik';
import { MuiTelInput } from 'mui-tel-input';
import React, { ChangeEvent, useRef, useState } from 'react';

const specialistList = ['Программист 1С', 'Консультант 1С'];

const { sendEmailBySupportPage } = FETCH_API.EMAILS;
const { sendLogEvent } = FETCH_API.GLOBAL;

const Consultations1C = () => {
    const { show } = useFloatMessages();
    const fileInputRef = useRef<HTMLInputElement>(null);

    const { contextAccountIndex } = useAppSelector(state => state.Global.getCurrentSessionSettingsReducer.settings.currentContextInfo);
    const { data } = useAppSelector(state => state.MainPageState.mainPageReducer);
    const { login } = data ?? {};

    const [selectedFiles, setSelectedFiles] = useState<File[]>([]);
    const [isLoading, setIsLoading] = useState(false);

    const handleAddFiles = (e: ChangeEvent<HTMLInputElement>) => {
        const { files } = e.target;

        if (files) {
            const newFiles = [...selectedFiles, ...Array.from(files)];

            if (newFiles.length > 5) {
                show(EMessageType.error, 'Максимум можно выбрать 5 файлов');
            } else {
                setSelectedFiles(newFiles);
            }
        }
    };

    const addFile = () => {
        fileInputRef.current?.click();
    };

    const deleteFile = (index: number) => {
        setSelectedFiles(prevState => prevState.filter((_, i) => i !== index));
    };

    const { values, handleChange, handleBlur, touched, errors, setFieldValue, handleSubmit, setFieldTouched, resetForm } = useFormik({
        validationSchema: consultations1CSchema,
        initialValues: consultations1CInitialValues,
        onSubmit: async value => {
            setIsLoading(true);

            const { success, message } = await sendEmailBySupportPage(
                `
                    <p>Логин: ${ login }</p>
                    <p>Тема обращения: ${ value.topic }</p>
                    <p>Конфигурация 1С: ${ value.configuration }</p>
                    <p>Специалист: ${ value.specialist }</p>
                    <p>Телефон: ${ value.phone }</p>
                    <p>Почта: ${ value.email }</p>
                    <p>Текст обращения:</p>
                    <p>${ value.text }</p>
                `,
                `Консультация 1С с ЛК от ${ contextAccountIndex } ${ dayjs().format('DD.MM.YYYY HH:mm:ss') }`,
                value.email,
                await Promise.all(selectedFiles.map(async file => {
                    return {
                        fileName: file.name,
                        bytes: ((await fileToBase64(file)) as string).split('base64,')[1],
                    };
                }))
            );

            if (success) {
                show(EMessageType.success, 'Спасибо за Вашу заявку! Мы обработаем ее в ближайшее время.');

                resetForm();
                setSelectedFiles([]);
                await setFieldValue('configuration', '');

                await sendLogEvent(ELogEvent.requestSent, 'Отправлена заявка на получение консультации по 1С');
            } else {
                show(EMessageType.error, message);
            }

            setIsLoading(false);
        }
    });

    return (
        <BoxWithPicture handleSubmit={ handleSubmit } isLoading={ isLoading }>
            <FormAndLabel label="Тема обращения" fullWidth={ true }>
                <TextField
                    name="topic"
                    value={ values.topic }
                    onChange={ handleChange }
                    onBlur={ handleBlur }
                    error={ touched.topic && !!errors.topic }
                    helperText={ touched.topic && errors.topic }
                    fullWidth={ true }
                />
            </FormAndLabel>
            <Box display="grid" gridTemplateColumns="1fr 1fr" columnGap={ 2 }>
                <FormAndLabel label="Ваша конфигурация 1С" fullWidth={ true }>
                    <Autocomplete
                        noOptionsText="Конфигурация не найдена"
                        fullWidth={ true }
                        renderInput={ params => <TextField
                            { ...params }
                            name="configuration"
                            error={ touched.configuration && !!errors.configuration }
                            helperText={ touched.configuration && errors.configuration }
                        /> }
                        value={ values.configuration }
                        onBlur={ handleBlur }
                        options={ CONFIGURATIONS }
                        onChange={ (_, value) => setFieldValue('configuration', value) }
                        size="small"
                    />
                </FormAndLabel>
                <FormAndLabel label="Выберите специалиста" fullWidth={ true }>
                    <FormControl fullWidth={ true }>
                        <Select
                            name="specialist"
                            value={ values.specialist }
                            onChange={ handleChange }
                            onBlur={ handleBlur }
                            error={ touched.specialist && !!errors.specialist }
                            variant="outlined"
                            fullWidth={ true }
                        >
                            { specialistList.map(item => <MenuItem key={ item } value={ item }>{ item }</MenuItem>) }
                        </Select>
                        <FormHelperText error={ touched.specialist && !!errors.specialist }>{ touched.specialist && errors.specialist }</FormHelperText>
                    </FormControl>
                </FormAndLabel>
                <FormAndLabel label="Укажите номер телефона" fullWidth={ true }>
                    <MuiTelInput
                        fullWidth={ true }
                        name="phone"
                        autoComplete="off"
                        onChange={ e => setFieldValue('phone', e) }
                        onBlur={ () => setFieldTouched('phone', true, true) }
                        value={ values.phone }
                        preferredCountries={ allowedCountries }
                        defaultCountry="RU"
                        langOfCountryName="RU"
                        error={ touched.phone && !!errors.phone }
                        helperText={ touched.phone && errors.phone }
                    />
                </FormAndLabel>
                <FormAndLabel label="Укажите адрес электронной почты" fullWidth={ true }>
                    <TextField
                        name="email"
                        value={ values.email }
                        onChange={ handleChange }
                        onBlur={ handleBlur }
                        error={ touched.email && !!errors.email }
                        helperText={ touched.email && errors.email }
                        fullWidth={ true }
                    />
                </FormAndLabel>
            </Box>
            <FormAndLabel label="Текст обращения">
                <TextField
                    name="text"
                    value={ values.text }
                    onChange={ handleChange }
                    onBlur={ handleBlur }
                    error={ touched.text && !!errors.text }
                    helperText={ touched.text && errors.text }
                    multiline={ true }
                    rows={ 4 }
                    fullWidth={ true }
                />
            </FormAndLabel>
            <Box mt={ 1 } display="flex" flexDirection="column">
                <input
                    ref={ fileInputRef }
                    hidden={ true }
                    type="file"
                    multiple={ true }
                    accept="image/png, image/jpeg, image/svg+xml, application/pdf, text/plain, text/xml, application/msword"
                    max={ 5 }
                    onChange={ handleAddFiles }
                />
                {
                    selectedFiles.length < 5 && (
                        <DefaultButton
                            onClick={ addFile }
                            style={ { maxWidth: '160px' } }
                        >
                            Прикрепить файл <AddIcon color="success" style={ { fontSize: '14px' } } />
                        </DefaultButton>
                    )
                }
                <Box display="flex" gap="8px" flexWrap="wrap" mt="8px">
                    { selectedFiles.map((file, index) => (
                        <Chip
                            key={ file.name }
                            label={ file.name }
                            onDelete={ () => deleteFile(index) }
                            style={ { fontSize: '14px', padding: '12px 0' } }
                        />
                    )) }
                </Box>
            </Box>
        </BoxWithPicture>
    );
};

export const Consultations1CView = withHeader({
    title: 'Консультант 1С',
    page: Consultations1C
});