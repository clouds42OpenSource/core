import { Autocomplete, Box, FormControl, FormHelperText, MenuItem, Select, TextField } from '@mui/material';
import { ELogEvent } from 'app/api/endpoints/global/enum';
import { FETCH_API } from 'app/api/useFetchApi';
import { RUSSIAN_REGIONS } from 'app/common/constants/regionsOfTheRussian';
import { EMessageType, useAppSelector, useDebounce, useFloatMessages } from 'app/hooks';
import { withHeader } from 'app/views/components/_hoc/withHeader';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { BoxWithPicture } from 'app/views/modules/_common/reusable-modules/BoxWithPicture';
import { reportingInitialValues, reportingSchema } from 'app/views/modules/_common/reusable-modules/BoxWithPicture/schemas';
import { allowedCountries } from 'app/views/modules/ToPartners/config/constants';
import dayjs from 'dayjs';
import { useFormik } from 'formik';
import { MuiTelInput } from 'mui-tel-input';
import { useEffect, useState } from 'react';

const booleanResponse = ['Да', 'Нет'];

const { getLegalEntityDetails } = FETCH_API.PARTNERS;
const { sendEmailBySupportPage } = FETCH_API.EMAILS;
const { sendLogEvent } = FETCH_API.GLOBAL;

const Reporting = () => {
    const { show } = useFloatMessages();

    const { contextAccountIndex } = useAppSelector(state => state.Global.getCurrentSessionSettingsReducer.settings.currentContextInfo);
    const { data } = useAppSelector(state => state.MainPageState.mainPageReducer);
    const { email, login } = data ?? {};

    const [isLoading, setIsLoading] = useState(false);

    const { values, handleChange, handleBlur, touched, errors, setFieldValue, handleSubmit, resetForm, setFieldTouched } = useFormik({
        validationSchema: reportingSchema,
        initialValues: reportingInitialValues,
        onSubmit: async value => {
            setIsLoading(true);

            const { success, message } = await sendEmailBySupportPage(
                `
                    <p>Логин: ${ login }</p>
                    <p>ИНН: ${ value.inn }</p>
                    <p>КПП: ${ value.kpp }</p>
                    <p>Регион регистрации организации: ${ value.region }</p>
                    <p>Наличие ЭЦП: ${ value.eds }</p>
                    <p>Хотели бы перенести сервис от другого партнера: ${ value.postpone }</p>
                    <p>Телефон: ${ value.phone }</p>
                    <p>Почта: ${ value.email }</p>
                `,
                `Заявка на подключение сервиса 1С: Отчетность от ${ contextAccountIndex } ${ dayjs().format('DD.MM.YYYY HH:mm:ss') }`,
                email
            );

            if (success) {
                show(EMessageType.success, 'Спасибо за Вашу заявку! Мы обработаем ее в ближайшее время.');

                resetForm();

                await sendLogEvent(ELogEvent.requestSent, 'Отправлена заявка на подключение 1С Отчетность');
            } else {
                show(EMessageType.error, message);
            }

            setIsLoading(false);
        }
    });

    const debounceInn = useDebounce(values.inn, 500);

    useEffect(() => {
        (async () => {
            if (!errors.inn && debounceInn) {
                const { data: legalEntityDetails, error } = await getLegalEntityDetails({ inn: debounceInn });

                if (!error && legalEntityDetails && legalEntityDetails.rawData && legalEntityDetails.rawData.success) {
                    await setFieldValue('kpp', legalEntityDetails.rawData.kpp);
                }
            }
        })();
    }, [debounceInn, errors.inn, setFieldValue]);

    return (
        <BoxWithPicture handleSubmit={ handleSubmit } isLoading={ isLoading }>
            <Box display="grid" gridTemplateColumns="1fr 1fr" columnGap={ 2 }>
                <FormAndLabel label="ИНН" fullWidth={ true }>
                    <TextField
                        name="inn"
                        value={ values.inn }
                        onChange={ handleChange }
                        onBlur={ handleBlur }
                        error={ touched.inn && !!errors.inn }
                        helperText={ touched.inn && errors.inn }
                        fullWidth={ true }
                    />
                </FormAndLabel>
                <FormAndLabel label="КПП" fullWidth={ true }>
                    <TextField
                        name="kpp"
                        value={ values.kpp }
                        onChange={ handleChange }
                        onBlur={ handleBlur }
                        error={ touched.kpp && !!errors.kpp }
                        helperText={ touched.kpp && errors.kpp }
                        fullWidth={ true }
                    />
                </FormAndLabel>
                <FormAndLabel label="Наличие электронной цифровой подписи (ЭЦП)" fullWidth={ true }>
                    <FormControl fullWidth={ true }>
                        <Select
                            name="eds"
                            value={ values.eds }
                            onChange={ handleChange }
                            onBlur={ handleBlur }
                            error={ touched.eds && !!errors.eds }
                            variant="outlined"
                            fullWidth={ true }
                        >
                            { booleanResponse.map(item => <MenuItem key={ `eds-${ item }` } value={ item }>{ item }</MenuItem>) }
                        </Select>
                        <FormHelperText error={ touched.eds && !!errors.eds }>{ touched.eds && errors.eds }</FormHelperText>
                    </FormControl>
                </FormAndLabel>
                <FormAndLabel label="Регион" fullWidth={ true }>
                    <FormControl fullWidth={ true }>
                        <Autocomplete
                            noOptionsText="Регион не найден"
                            fullWidth={ true }
                            renderInput={ params => <TextField
                                { ...params }
                                name="region"
                                error={ touched.region && !!errors.region }
                                helperText={ touched.region && errors.region }
                            /> }
                            value={ values.region }
                            onBlur={ handleBlur }
                            options={ RUSSIAN_REGIONS }
                            onChange={ (_, value) => setFieldValue('region', value) }
                            size="small"
                        />
                    </FormControl>
                </FormAndLabel>
                <FormAndLabel label="Хотели бы перенести сервис от другого партнёра?" fullWidth={ true }>
                    <FormControl fullWidth={ true }>
                        <Select
                            name="postpone"
                            value={ values.postpone }
                            onChange={ handleChange }
                            onBlur={ handleBlur }
                            error={ touched.postpone && !!errors.postpone }
                            variant="outlined"
                            fullWidth={ true }
                        >
                            { booleanResponse.map(item => <MenuItem key={ `postpone-${ item }` } value={ item }>{ item }</MenuItem>) }
                        </Select>
                        <FormHelperText error={ touched.postpone && !!errors.postpone }>{ touched.postpone && errors.postpone }</FormHelperText>
                    </FormControl>
                </FormAndLabel>
                <FormAndLabel label="Номер телефона" fullWidth={ true }>
                    <MuiTelInput
                        fullWidth={ true }
                        name="phone"
                        autoComplete="off"
                        onChange={ e => setFieldValue('phone', e) }
                        onBlur={ () => setFieldTouched('phone', true, true) }
                        value={ values.phone }
                        preferredCountries={ allowedCountries }
                        defaultCountry="RU"
                        langOfCountryName="RU"
                        error={ touched.phone && !!errors.phone }
                        helperText={ touched.phone && errors.phone }
                    />
                </FormAndLabel>
                <FormAndLabel label="Электронная почта" fullWidth={ true }>
                    <TextField
                        name="email"
                        value={ values.email }
                        onChange={ handleChange }
                        onBlur={ handleBlur }
                        error={ touched.email && !!errors.email }
                        helperText={ touched.email && errors.email }
                        fullWidth={ true }
                    />
                </FormAndLabel>
            </Box>
        </BoxWithPicture>
    );
};

export const ReportingView = withHeader({
    title: '1С отчётность',
    page: Reporting
});