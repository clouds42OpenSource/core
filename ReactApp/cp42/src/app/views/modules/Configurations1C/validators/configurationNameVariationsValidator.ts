import { Validator } from 'app/common/types';

export function createConfigurationNameVariationsValidator(): Validator<string, string[]> {
    const validator: Validator<string, string[]> = {
        onChange: (configurationNameVariationTag, prevValue, configurationNameVariations?) => {
            if (!configurationNameVariationTag || configurationNameVariationTag === prevValue) {
                return;
            }
            if (configurationNameVariations && configurationNameVariations.findIndex(item => item.toLowerCase() === configurationNameVariationTag.toLowerCase()) >= 0) {
                return `Вариация "${ configurationNameVariationTag }" уже добавлена.`;
            }
            return void 0;
        },

        onBlur: (configurationNameVariationTag, prevValue, configurationNameVariations?) => {
            const error = validator.onChange(configurationNameVariationTag, prevValue, configurationNameVariations);
            if (error) { return error; }

            if (!configurationNameVariationTag || configurationNameVariationTag === prevValue) {
                return;
            }

            if (configurationNameVariations && configurationNameVariations.findIndex(item => item.toLowerCase() === configurationNameVariationTag.toLowerCase()) >= 0) {
                return `Вариация "${ configurationNameVariationTag }" уже добавлена.`;
            }
        }
    };

    return validator;
}