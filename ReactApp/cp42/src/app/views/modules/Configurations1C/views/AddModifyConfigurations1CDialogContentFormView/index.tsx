import { Box } from '@mui/material';
import { hasComponentChangesFor } from 'app/common/functions';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { Table } from 'app/views/components/controls/Table';
import { CheckBoxForm, ComboBoxForm, TagInputForm, TextBoxForm } from 'app/views/components/controls/forms';
import { createConfigurationNameVariationsValidator } from 'app/views/modules/Configurations1C/validators';
import {
    Configurations1CItemDataModel
} from 'app/web/InterlayerApiProxy/Configurations1CApiProxy/receiveConfigurations1C';
import { ItsDataAuthorizationDataModel } from 'app/web/InterlayerApiProxy/ItsAuthorizationDataApiProxy/data-models';
import React from 'react';

type OwnProps = ReduxFormProps<Configurations1CItemDataModel> & {
    /**
     * Флаг добавления в другом режиме
     */
    isAddNewMode: boolean;

    /**
     * Модель свойств элемента конфигурации 1С
     */
    itemToEdit: Configurations1CItemDataModel;

    /**
     * Массив данных авторизации в ИТС
     */
    itsDataAutorizations: ItsDataAuthorizationDataModel[];
};

class AddModifyConfigurations1CDialogContentFormViewClass extends React.Component<OwnProps> {
    public constructor(props: OwnProps) {
        super(props);
        this.onValueChange = this.onValueChange.bind(this);
        this.initReduxForm = this.initReduxForm.bind(this);

        this.props.reduxForm.setInitializeFormDataAction(this.initReduxForm);
    }

    public shouldComponentUpdate(nextProps: OwnProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private onValueChange<TValue>(formName: string, newValue: TValue) {
        this.props.reduxForm.updateReduxFormFields({
            [formName]: newValue
        });
    }

    private initReduxForm(): Configurations1CItemDataModel | undefined {
        if (!this.props.itemToEdit) {
            return;
        }
        return {
            configurationName: this.props.itemToEdit.configurationName,
            configurationCatalog: this.props.itemToEdit.configurationCatalog,
            platformCatalog: this.props.itemToEdit.platformCatalog,
            updateCatalogUrl: this.props.itemToEdit.updateCatalogUrl,
            redactionCatalogs: this.props.itemToEdit.redactionCatalogs,
            useComConnectionForApplyUpdates: this.props.itemToEdit.useComConnectionForApplyUpdates,
            shortCode: this.props.itemToEdit.shortCode,
            needCheckUpdates: this.props.itemToEdit.needCheckUpdates,
            configurationCost: this.props.itemToEdit.configurationCost,
            itsAuthorizationDataId: this.props.itemToEdit.itsAuthorizationDataId,
            configurationVariations: this.props.itemToEdit.configurationVariations,
            reductions: this.props.itemToEdit.reductions
        };
    }

    public render() {
        if (!this.props.itemToEdit) {
            return null;
        }

        const reduxForm = this.props.reduxForm.getReduxFormFields(false);

        return (
            <Box display="flex" flexDirection="column" gap="16px">
                <TextBoxForm
                    formName="configurationName"
                    label="Название конфигурации"
                    value={ reduxForm.configurationName }
                    autoFocus={ this.props.isAddNewMode }
                    onValueChange={ this.props.isAddNewMode ? this.onValueChange : void 0 }
                    isReadOnly={ !this.props.isAddNewMode }
                />
                <TextBoxForm
                    formName="configurationCost"
                    label="Стоимость конфигурации"
                    value={ reduxForm.configurationCost.toString() }
                    onValueChange={ (name, value) => {
                        if (Number(value)) {
                            this.onValueChange(name, value);
                        }
                    } }
                />
                <TextBoxForm
                    autoFocus={ !this.props.isAddNewMode }
                    formName="configurationCatalog"
                    label="Каталог конфигурации"
                    value={ reduxForm.configurationCatalog }
                    onValueChange={ this.onValueChange }
                />
                <TextBoxForm
                    formName="redactionCatalogs"
                    label="Редакции (вводится через ;)"
                    value={ reduxForm.redactionCatalogs }
                    onValueChange={ this.onValueChange }
                />
                <TextBoxForm
                    formName="platformCatalog"
                    label="Платформа"
                    maxLength={ 3 }
                    value={ reduxForm.platformCatalog }
                    onValueChange={ this.onValueChange }
                />
                <TextBoxForm
                    formName="updateCatalogUrl"
                    label="Адрес каталога обновлений"
                    value={ reduxForm.updateCatalogUrl }
                    onValueChange={ this.onValueChange }
                />
                <TextBoxForm
                    formName="shortCode"
                    label="Код конфигурации"
                    maxLength={ 5 }
                    value={ reduxForm.shortCode }
                    onValueChange={ this.onValueChange }
                />
                <CheckBoxForm
                    formName="useComConnectionForApplyUpdates"
                    label="Использовать COM соединение для принятия обновлений"
                    isChecked={ reduxForm.useComConnectionForApplyUpdates }
                    falseText="(нет)"
                    trueText="(да)"
                    onValueChange={ this.onValueChange }
                />
                <CheckBoxForm
                    formName="needCheckUpdates"
                    label="Регулярно проверять наличие новых релизов"
                    isChecked={ reduxForm.needCheckUpdates }
                    falseText="(нет)"
                    trueText="(да)"
                    onValueChange={ this.onValueChange }
                />
                <TagInputForm
                    onTagsChanged={ this.onValueChange }
                    placeholder="Введите вариацию имени конфигурации 1С"
                    tags={ reduxForm.configurationVariations }
                    formName="configurationVariations"
                    label="Вариации имени конфигурации 1C"
                    validator={ createConfigurationNameVariationsValidator() }
                />
                <ComboBoxForm
                    label="Учетные записи:"
                    formName="itsAuthorizationDataId"
                    items={ this.props.itsDataAutorizations
                        ? this.props.itsDataAutorizations.map(item => ({ text: item.login, value: item.id }))
                        : [] }
                    value={ reduxForm.itsAuthorizationDataId }
                    onValueChange={ this.onValueChange }
                />
                {
                    this.props.isAddNewMode
                        ? null
                        : (
                            <>
                                <br />
                                <Table
                                    dataset={ this.props.itemToEdit.reductions }
                                    keyFieldName="redactionCatalog"
                                    fieldsView={ {
                                        redactionCatalog: {
                                            caption: 'Редакция'
                                        },
                                        urlOfMapping: {
                                            caption: 'Ссылка'
                                        }
                                    } }
                                />
                            </>
                        )
                }
            </Box>
        );
    }
}

export const AddModifyConfigurations1CDialogContentFormView = withReduxForm(AddModifyConfigurations1CDialogContentFormViewClass, {
    reduxFormName: 'AddModifyConfigurations1C_ReduxForm',
    resetOnUnmount: true
});