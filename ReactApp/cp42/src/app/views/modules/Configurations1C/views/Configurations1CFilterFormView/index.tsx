import {
    Configurations1CDataFormFieldNamesType,
    Configurations1CFilterDataForm
} from 'app/views/modules/Configurations1C/types';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import React from 'react';
import { TableFilterWrapper } from 'app/views/components/_hoc/tableFilterWrapper';
import { TextBoxForm } from 'app/views/components/controls/forms/TextBoxForm';
import { hasComponentChangesFor } from 'app/common/functions';

/**
 * Свойства для компонента фильтра
 */
type OwnProps = ReduxFormProps<Configurations1CFilterDataForm> & {
    onFilterChanged: (filter: Configurations1CFilterDataForm, filterFormName: string) => void;
    isFilterFormDisabled: boolean;
};

class Configurations1CFilterFormViewClass extends React.Component<OwnProps> {
    public constructor(props: OwnProps) {
        super(props);
        this.initReduxForm = this.initReduxForm.bind(this);
        this.onValueChange = this.onValueChange.bind(this);
        this.onValueApplied = this.onValueApplied.bind(this);
    }

    public componentDidMount() {
        this.props.reduxForm.setInitializeFormDataAction(this.initReduxForm);
    }

    public shouldComponentUpdate(nextProps: OwnProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private onValueChange<TValue, TForm extends string = Configurations1CDataFormFieldNamesType>(formName: TForm, newValue: TValue) {
        this.props.reduxForm.updateReduxFormFields({
            [formName]: newValue
        });
        if ((formName === 'configurationName' ||
            formName === 'configurationCatalog') && newValue && (newValue as any as string).length < 3) {
            return false;
        }
    }

    private onValueApplied<TValue>(formName: string, value: TValue) {
        this.props.onFilterChanged({
            ...this.props.reduxForm.getReduxFormFields(false),
            [formName]: value
        }, formName);
    }

    private initReduxForm(): Configurations1CFilterDataForm | undefined {
        return {
            configurationName: '',
            configurationCatalog: ''
        };
    }

    public render() {
        const formFields = this.props.reduxForm.getReduxFormFields(false);
        return (
            <TableFilterWrapper>
                <div style={ { width: '350px' } }>
                    <TextBoxForm
                        autoFocus={ true }
                        label="Название конфигурации"
                        formName="configurationName"
                        onValueChange={ this.onValueChange }
                        onValueApplied={ this.onValueApplied }
                        value={ formFields.configurationName }
                        isReadOnly={ this.props.isFilterFormDisabled }
                    />
                </div>
                <div style={ { width: '350px' } }>
                    <TextBoxForm
                        label="Каталог конфигурации"
                        formName="configurationCatalog"
                        onValueChange={ this.onValueChange }
                        onValueApplied={ this.onValueApplied }
                        value={ formFields.configurationCatalog }
                        isReadOnly={ this.props.isFilterFormDisabled }
                    />
                </div>
            </TableFilterWrapper>
        );
    }
}

export const Configurations1CFilterFormView = withReduxForm(Configurations1CFilterFormViewClass, {
    reduxFormName: 'Configurations1C_Filter_Form',
    resetOnUnmount: true
});