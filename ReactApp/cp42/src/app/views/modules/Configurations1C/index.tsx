import { SortingKind } from 'app/common/enums';
import { checkOnValidField, hasComponentChangesFor } from 'app/common/functions';
import { ProcessIdStateHelper } from 'app/common/helpers/ProcessIdStateHelper';
import { IErrorsType } from 'app/common/interfaces';
import { Configurations1CProcessId } from 'app/modules/configurations1C/Configurations1CProcessId';
import {
    AddConfiguration1CThunkParams
} from 'app/modules/configurations1C/store/reducers/addConfiguration1CReducer/params';
import {
    DeleteConfiguration1CThunkParams
} from 'app/modules/configurations1C/store/reducers/deleteConfiguration1CReducer/params';
import {
    ReceiveConfigurations1CThunkParams
} from 'app/modules/configurations1C/store/reducers/receiveConfigurations1CReducer/params';
import {
    UpdateConfiguration1CThunkParams
} from 'app/modules/configurations1C/store/reducers/updateConfiguration1CReducer/params';
import {
    AddConfiguration1CThunk, DeleteConfiguration1CThunk,
    ReceiveConfigurations1CThunk,
    UpdateConfiguration1CThunk
} from 'app/modules/configurations1C/store/thunks';
import {
    GetItsAuthorizationDataBySearchValueThunkParams
} from 'app/modules/itsAuthorizationData/store/reducers/getItsAuthorizationDataBySearchValueReducer/params';
import { GetItsAuthorizationDataBySearchValueThunk } from 'app/modules/itsAuthorizationData/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { TextOut } from 'app/views/components/TextOut';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { withHeader } from 'app/views/components/_hoc/withHeader';
import { Configurations1CFilterDataForm } from 'app/views/modules/Configurations1C/types';
import {
    AddModifyConfigurations1CDialogContentFormView
} from 'app/views/modules/Configurations1C/views/AddModifyConfigurations1CDialogContentFormView';
import {
    Configurations1CFilterFormView
} from 'app/views/modules/Configurations1C/views/Configurations1CFilterFormView';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { TablePagination } from 'app/views/modules/_common/components/CommonTableWithFilter/TableView';
import {
    Configurations1CItemDataModel
} from 'app/web/InterlayerApiProxy/Configurations1CApiProxy/receiveConfigurations1C';
import { ItsDataAuthorizationDataModel } from 'app/web/InterlayerApiProxy/ItsAuthorizationDataApiProxy/data-models';
import { SelectDataCommonDataModel, SortingDataModel } from 'app/web/common/data-models';
import React from 'react';
import { connect } from 'react-redux';

type StateProps<TDataRow = Configurations1CItemDataModel> = {
    /**
     * Массив конфигурации 1С
     */
    dataset: TDataRow[];

    /**
     * Флаг загрузки данных конфигурации 1С
     */
    isInLoadingDataProcess: boolean;

    /**
     * Модель пагинации
     */
    pagination: TablePagination;

    /**
     * Массив данных авторизации в ИТС
     */
    itsDataAutorizations: ItsDataAuthorizationDataModel[];

    /**
     * Если true, то конфигурации 1С получены
     */
    hasConfigurations1CReceived: boolean;

    /**
     * Если true, то массив данных авторизации в ИТС по значению поиска получены
     */
    hasItsDataAutorizationsReceived: boolean;
};

type DispatchProps = {
    dispatchReceiveConfigurations1CThunk: (args: ReceiveConfigurations1CThunkParams) => void;
    dispatchUpdateConfiguration1CThunk: (args: UpdateConfiguration1CThunkParams) => void;
    dispatchAddConfiguration1CThunk: (args: AddConfiguration1CThunkParams) => void;
    dispatchDeleteConfiguration1CThunk: (args: DeleteConfiguration1CThunkParams) => void;
    dispatchGetItsAuthorizationDataBySearchValueThunk: (args: GetItsAuthorizationDataBySearchValueThunkParams) => void;
};

type AllProps = StateProps & DispatchProps & FloatMessageProps;

class Configurations1CClass extends React.Component<AllProps> {
    public constructor(props: AllProps) {
        super(props);
        this.onDataSelect = this.onDataSelect.bind(this);
        this.getSortQuery = this.getSortQuery.bind(this);
        this.isFormValid = this.isFormValid.bind(this);
    }

    public shouldComponentUpdate(nextProps: AllProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    public componentDidUpdate() {
        const { hasConfigurations1CReceived, hasItsDataAutorizationsReceived, itsDataAutorizations } = this.props;

        if (hasConfigurations1CReceived && !hasItsDataAutorizationsReceived && itsDataAutorizations.length === 0) {
            this.props.dispatchGetItsAuthorizationDataBySearchValueThunk({
                force: true,
                showLoadingProgress: false
            });
        }
    }

    private onDataSelect(args: SelectDataCommonDataModel<Configurations1CFilterDataForm>) {
        this.props.dispatchReceiveConfigurations1CThunk({
            pageNumber: args.pageNumber,
            filter: args.filter,
            orderBy: args.sortingData ? this.getSortQuery(args.sortingData) : 'name.desc',
            force: true
        });
    }

    private getSortQuery(args: SortingDataModel) {
        switch (args.fieldName) {
            case 'configurationName':
                return `name.${ args.sortKind ? 'desc' : 'asc' }`;
            case 'configurationCatalog':
                return `configurationCatalog.${ args.sortKind ? 'desc' : 'asc' }`;
            default:
                return 'name.desc';
        }
    }

    public isFormValid(form: Configurations1CItemDataModel) {
        const errors: IErrorsType = {};
        const arrayResults: boolean[] = [];

        arrayResults.push(checkOnValidField(errors, 'configurationName', form.configurationName, 'Поле обязательно'));
        arrayResults.push(checkOnValidField(errors, 'configurationCatalog', form.configurationCatalog, 'Поле обязательно'));
        arrayResults.push(checkOnValidField(errors, 'redactionCatalogs', form.redactionCatalogs, 'Поле обязательно'));
        arrayResults.push(checkOnValidField(errors, 'platformCatalog', form.platformCatalog, 'Поле обязательно'));
        arrayResults.push(checkOnValidField(errors, 'updateCatalogUrl', form.updateCatalogUrl, 'Поле обязательно'));
        arrayResults.push(checkOnValidField(errors, 'configurationCost', String(form.configurationCost), 'Поле обязательно'));

        if (arrayResults.some(item => !item)) {
            this.props.floatMessage.show(MessageType.Warning, 'Форма к отправке не готова. Пожалуйста, заполните все поля.');
            return false;
        }

        return true;
    }

    public render() {
        return (
            <>
                <div>
                    <TextOut fontSize={ 13 } fontWeight={ 400 }>
                        На этой странице вы можете управлять конфигурациями 1С.
                    </TextOut>
                </div>
                <div style={ {
                    marginTop: '10px'
                } }
                >
                    <TextOut fontSize={ 12 } fontWeight={ 600 }>
                        Управление конфигурациями
                    </TextOut>
                </div>
                <br />
                <CommonTableWithFilter
                    uniqueContextProviderStateId="Configurations1CContextProviderStateId"
                    tableEditProps={ {
                        processProps: {
                            watch: {
                                reducerStateName: 'Configurations1CState',
                                processIds: {
                                    addNewItemProcessId: Configurations1CProcessId.AddConfiguration1C,
                                    modifyItemProcessId: Configurations1CProcessId.UpdateConfiguration1C,
                                    deleteItemProcessId: Configurations1CProcessId.DeleteConfiguration1C
                                }
                            },
                            processMessages: {
                                getOnSuccessAddNewItemMessage: () => 'Добавление новой конфигураций 1C успешно завершено.',
                                getOnSuccessModifyItemMessage: item => `Редактирование каталога конфигураций "${ item.configurationName }" успешно завершено.`,
                                getOnSuccessDeleteItemMessage: item => `Удаление конфигураций "${ item.configurationName }" успешно завершено.`
                            }
                        },

                        dialogProps: {
                            deleteDialog: {
                                getDialogTitle: item => `Удаление конфигурации "${ item.configurationName }"`,
                                getDialogContentView: itemToDelete => `Вы действительно хотите удалить конфигурацию "${ itemToDelete.configurationName }"?`,
                                confirmDeleteItemButtonText: 'Удалить',
                                onDeleteItemConfirmed: itemToDelete => {
                                    this.props.dispatchDeleteConfiguration1CThunk({
                                        configurationName: itemToDelete.configurationName,
                                        force: true
                                    });
                                }
                            },
                            addDialog: {
                                dialogWidth: 'md',
                                getDialogTitle: () => 'Добавление новой конфигурации',
                                getDialogContentFormView: ref =>
                                    <AddModifyConfigurations1CDialogContentFormView
                                        ref={ ref }
                                        isAddNewMode={ true }
                                        itemToEdit={
                                            {
                                                configurationName: '',
                                                configurationCatalog: '',
                                                platformCatalog: '',
                                                updateCatalogUrl: '',
                                                redactionCatalogs: '',
                                                useComConnectionForApplyUpdates: false,
                                                shortCode: '',
                                                configurationCost: 0,
                                                needCheckUpdates: false,
                                                itsAuthorizationDataId: this.props.itsDataAutorizations[0]?.id ?? '',
                                                configurationVariations: [],
                                                reductions: []
                                            }
                                        }
                                        itsDataAutorizations={ this.props.itsDataAutorizations }
                                    />,
                                confirmAddItemButtonText: 'Добавить',
                                showAddDialogButtonText: 'Добавить конфигурацию',
                                onAddNewItemConfirmed: itemToAdd => {
                                    if (this.isFormValid(itemToAdd)) {
                                        this.props.dispatchAddConfiguration1CThunk({
                                            configurationName: itemToAdd.configurationName,
                                            configurationCatalog: itemToAdd.configurationCatalog,
                                            configurationVariations: itemToAdd.configurationVariations,
                                            needCheckUpdates: itemToAdd.needCheckUpdates,
                                            configurationCost: itemToAdd.configurationCost,
                                            platformCatalog: itemToAdd.platformCatalog,
                                            redactionCatalogs: itemToAdd.redactionCatalogs,
                                            updateCatalogUrl: itemToAdd.updateCatalogUrl,
                                            useComConnectionForApplyUpdates: itemToAdd.useComConnectionForApplyUpdates,
                                            shortCode: itemToAdd.shortCode,
                                            itsAuthorizationDataId: itemToAdd.itsAuthorizationDataId,
                                            force: true
                                        });
                                    }
                                }
                            },
                            modifyDialog: {
                                getDialogTitle: () => 'Редактирование конфигурации',
                                dialogWidth: 'md',
                                confirmModifyItemButtonText: 'Сохранить',
                                getDialogContentFormView: (ref, item) => <AddModifyConfigurations1CDialogContentFormView
                                    ref={ ref }
                                    isAddNewMode={ false }
                                    itemToEdit={ item }
                                    itsDataAutorizations={ this.props.itsDataAutorizations }
                                />,
                                onModifyItemConfirmed: itemToModify => {
                                    this.props.dispatchUpdateConfiguration1CThunk({
                                        ...itemToModify,
                                        force: true
                                    });
                                }
                            }
                        }
                    } }
                    filterProps={ {
                        getFilterContentView: (ref, onFilterChanged) => <Configurations1CFilterFormView ref={ ref } isFilterFormDisabled={ this.props.isInLoadingDataProcess } onFilterChanged={ onFilterChanged } />
                    } }
                    tableProps={ {
                        dataset: this.props.dataset,
                        keyFieldName: 'configurationName',
                        sorting: {
                            sortFieldName: 'configurationName',
                            sortKind: SortingKind.Asc
                        },
                        fieldsView: {
                            configurationName: {
                                caption: 'Название конфигурации',
                                fieldContentNoWrap: false,
                                isSortable: true,
                                fieldWidth: 'auto'
                            },
                            configurationCatalog: {
                                caption: 'Каталог конфигурации',
                                fieldContentNoWrap: false,
                                isSortable: true,
                                fieldWidth: 'auto'
                            },
                            redactionCatalogs: {
                                caption: 'Каталог редакции',
                                fieldContentNoWrap: false,
                                isSortable: false,
                                fieldWidth: 'auto'
                            },
                            platformCatalog: {
                                caption: 'Каталог платформы',
                                fieldContentNoWrap: false,
                                isSortable: false,
                                fieldWidth: 'auto'
                            },
                            updateCatalogUrl: {
                                caption: 'Адрес каталога обновлений',
                                fieldContentNoWrap: false,
                                isSortable: false,
                                fieldWidth: 'auto'
                            }
                        },
                        pagination: this.props.pagination
                    } }
                    onDataSelect={ this.onDataSelect }
                />
            </>
        );
    }
}

const Configurations1CConnected = connect<StateProps, DispatchProps, {}, AppReduxStoreState>(
    state => {
        const configurations1CState = state.Configurations1CState;
        const { configurations1C } = configurations1CState;
        const dataset = configurations1C.records;
        const { hasConfigurations1CReceived } = configurations1CState.hasSuccessFor;

        const itsAuthorizationDataState = state.ItsAuthorizationDataState;
        const itsDataAutorizations = itsAuthorizationDataState.itsDataAutorizationsBySearchValue;
        const hasItsDataAutorizationsReceived = itsAuthorizationDataState.hasSuccessFor.hasItsDataAutorizationsBySearchValueReceived;

        const currentPage = configurations1C.metadata.pageNumber;
        const totalPages = configurations1C.metadata.pageCount;
        const { pageSize } = configurations1C.metadata;
        const recordsCount = configurations1C.metadata.totalItemCount;

        const isInLoadingDataProcess = ProcessIdStateHelper.isInProgress(configurations1CState.reducerActions, Configurations1CProcessId.ReceiveConfigurations1C);

        return {
            dataset,
            isInLoadingDataProcess,
            pagination: {
                currentPage,
                totalPages,
                pageSize,
                recordsCount
            },
            itsDataAutorizations,
            hasConfigurations1CReceived,
            hasItsDataAutorizationsReceived
        };
    },
    {
        dispatchReceiveConfigurations1CThunk: ReceiveConfigurations1CThunk.invoke,
        dispatchUpdateConfiguration1CThunk: UpdateConfiguration1CThunk.invoke,
        dispatchAddConfiguration1CThunk: AddConfiguration1CThunk.invoke,
        dispatchDeleteConfiguration1CThunk: DeleteConfiguration1CThunk.invoke,
        dispatchGetItsAuthorizationDataBySearchValueThunk: GetItsAuthorizationDataBySearchValueThunk.invoke
    }
)(Configurations1CClass);

export const Configurations1CView = withHeader({
    title: 'Страница каталога конфигураций 1С',
    page: withFloatMessages(Configurations1CConnected)
});