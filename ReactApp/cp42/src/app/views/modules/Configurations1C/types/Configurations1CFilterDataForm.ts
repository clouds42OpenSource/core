/**
 * Модель Redux формы для редактирования фильтра для таблицы конфигураций 1С
 */
export type Configurations1CFilterDataForm = {
    /**
     * Название конфигурации
     */
    configurationName: string;

    /**
     * Каталог конфигурации на ИТС
     */
    configurationCatalog: string;
};