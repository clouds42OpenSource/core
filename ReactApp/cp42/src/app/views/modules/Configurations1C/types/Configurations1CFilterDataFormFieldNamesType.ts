import { Configurations1CFilterDataForm } from 'app/views/modules/Configurations1C/types/Configurations1CFilterDataForm';

export type Configurations1CDataFormFieldNamesType = keyof Configurations1CFilterDataForm;