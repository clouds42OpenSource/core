import { Instruktions } from '../types';

const connectTo = (to: string) => `Подключение к 1С для пользователей ${ to }`;

const currentUrl = (path: string) => `https://42clouds.com/ru-ru/manuals/${ path }`;

export const getInstruktions = (): Instruktions[] => [
    { text: `${ connectTo('GNU/Linux') }`, href: `${ currentUrl('rent-1c-linux.html') }` },
    { text: `${ connectTo('iPhone и iPad') }`, href: `${ currentUrl('podklyuchenie-k-1s-dlya-polzovateley-iphone-i-ipad.html') }` },
    { text: `${ connectTo('OC Android и Windows Phone') }`, href: `${ currentUrl('rent-1c-android.html') }` },
    { text: `${ connectTo('Mac OS') }`, href: `${ currentUrl('rent-1c-mac-os.html') }` }
];