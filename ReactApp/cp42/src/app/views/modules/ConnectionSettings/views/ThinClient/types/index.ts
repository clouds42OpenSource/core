export type PlatformThinClientVersion = {
    version: string;
    macOsDownloadLink: string;
    windowsDownloadLink: string;
};

export type Props = {
    platformThin: PlatformThinClientVersion[];
};