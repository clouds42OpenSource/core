import { Props } from 'app/views/modules/ConnectionSettings/views/LinkTo/types';

import style from './style.module.css';

export const LinkTo = ({ href, text = 'Скачать' }: Props) => {
    return <a href={ href } target="_blank" rel="noreferrer" className={ style.link }>{ text }</a>;
};