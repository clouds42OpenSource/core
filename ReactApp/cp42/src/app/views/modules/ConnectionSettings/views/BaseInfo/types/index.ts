export type Props = {
    login: string;
    terminalFarmName: string;
    terminalName: string;
};