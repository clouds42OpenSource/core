import { TextOut } from 'app/views/components/TextOut';
import { ETourId } from 'app/views/Layout/ProjectTour/enums';
import { LinkTo } from 'app/views/modules/ConnectionSettings/views/LinkTo';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { Props } from './types';

export const ThinClient = ({ platformThin }: Props) => {
    return (
        <div data-tourid={ ETourId.connectionSettingsPageVersions }>
            <TextOut fontSize={ 14 } fontWeight={ 600 }>Тонкий клиент</TextOut>
            <CommonTableWithFilter
                uniqueContextProviderStateId="thinClientContextProviderStateId"
                isResponsiveTable={ true }
                tableProps={ {
                    dataset: platformThin,
                    keyFieldName: 'version',
                    fieldsView: {
                        version: {
                            caption: 'Версия',
                            style: { lineHeight: '35px', paddingLeft: '16px' },
                            fieldWidth: '33%',
                            fieldContentNoWrap: true
                        },
                        windowsDownloadLink: {
                            caption: 'Windows',
                            fieldWidth: '33%',
                            format: (value: string) => <LinkTo href={ value } />
                        },
                        macOsDownloadLink: {
                            caption: 'MacOS',
                            fieldWidth: '33%',
                            format: (value: string) => <LinkTo href={ value } />
                        }
                    }
                } }
            />
        </div>
    );
};