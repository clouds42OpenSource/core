import { TextOut } from 'app/views/components/TextOut';
import { ERefreshId, ETourId } from 'app/views/Layout/ProjectTour/enums';
import { getInstruktions } from 'app/views/modules/ConnectionSettings/views/Instruction/funtions';
import { Instruktions } from 'app/views/modules/ConnectionSettings/views/Instruction/types';
import { LinkTo } from 'app/views/modules/ConnectionSettings/views/LinkTo';
import style from './style.module.css';

const dataset: Instruktions[] = getInstruktions();

export const Instruction = () => (
    <div data-tourid={ ETourId.connectionSettingsPageInstructions } data-refreshid={ ERefreshId.connectionSettingsInfo }>
        <TextOut fontSize={ 14 } fontWeight={ 600 }>Инструкция</TextOut>
        {
            dataset.map(item =>
                <div key={ item.text } className={ style.line }>
                    <LinkTo href={ item.href } text={ item.text } />
                </div>
            )
        }
    </div>
);