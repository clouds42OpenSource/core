import { ETourId } from 'app/views/Layout/ProjectTour/enums';
import { CommonTableBasic, DatasetType } from 'app/views/modules/_common/components/CommonTableBasic';
import { useCallback, useEffect, useState } from 'react';
import { Props } from './types/index';

export const BaseInfo = ({ login, terminalFarmName, terminalName }: Props) => {
    /**
     * Dataset
     */
    const [dataset, setDataset] = useState<DatasetType[]>([]);

    /**
     * Mapping data to table model
     */
    const createData = useCallback((caption: string, value: string, visible = true): DatasetType => {
        return { caption, value, visible };
    }, []);

    useEffect(() => {
        setDataset([
            createData('Логин', `ad\\${ login }`),
            createData('Адрес подключения', terminalFarmName),
            createData('Адрес шлюза терминалов', terminalName)
        ]);
    }, [createData, login, terminalFarmName, terminalName]);

    return <CommonTableBasic dataset={ dataset } tourId={ ETourId.connectionSettingsPageRdp } />;
};