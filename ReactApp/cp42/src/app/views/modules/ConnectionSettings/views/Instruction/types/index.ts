export type Instruktions = {
    text: string;
    href: string;
};