export type Props = {
    href: string;
    text?: string;
};