import { Box } from '@mui/material';
import { GetConnectionSettings } from 'app/api/endpoints/connectionSettings/response';
import { FETCH_API } from 'app/api/useFetchApi';
import { EMessageType, useFloatMessages } from 'app/hooks';
import { withHeader } from 'app/views/components/_hoc/withHeader';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { ERefreshId } from 'app/views/Layout/ProjectTour/enums';
import { useEffect, useState } from 'react';
import { BaseInfo, Instruction, ThinClient } from './views';

const ConnectionSettings = () => {
    const { show } = useFloatMessages();

    const [data, setData] = useState<GetConnectionSettings>();
    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        window.scroll(0, 0);

        (async () => {
            setIsLoading(true);
            const { data: responseData, message, success } = await FETCH_API.CONNECTION_SETTINGS.getConnectionSettings();

            if (responseData && success) {
                setData(responseData);
            } else if (message) {
                show(EMessageType.error, message);
            }

            setIsLoading(false);
        })();
    }, [show]);

    return (
        <>
            { isLoading && <LoadingBounce /> }
            <Box display="flex" flexDirection="column" gap="16px" data-refreshid={ ERefreshId.connectionSettingsInfo }>
                <BaseInfo
                    login={ data?.login ?? '' }
                    terminalFarmName={ data?.servicesTerminalFarmName ?? '' }
                    terminalName={ data?.gatewayTerminalsName ?? '' }
                />
                <Instruction />
                <ThinClient platformThin={ data?.platformThinClientVersions ?? [] } />
            </Box>
        </>
    );
};

export const ConnectionSettingsView = withHeader({
    page: ConnectionSettings,
    title: 'Настройки подключения'
});