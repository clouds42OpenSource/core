import { hasComponentChangesFor } from 'app/common/functions';
import { TextBoxForm } from 'app/views/components/controls/forms/TextBoxForm';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import {
    AgencyAgreementFilterDataForm,
    AgencyAgreementFilterDataFormFieldNamesType
} from 'app/views/modules/AgencyAgreement/types';
import React from 'react';

/**
 * Свойства для компонента фильтра
 */
type OwnProps = ReduxFormProps<AgencyAgreementFilterDataForm> & {
    onFilterChanged: (filter: AgencyAgreementFilterDataForm, filterFormName: string) => void;
    isFilterFormDisabled: boolean;
};

class AgencyAgreementFilterFormViewClass extends React.Component<OwnProps> {
    public constructor(props: OwnProps) {
        super(props);
        this.initReduxForm = this.initReduxForm.bind(this);
        this.onValueChange = this.onValueChange.bind(this);
        this.onValueApplied = this.onValueApplied.bind(this);
    }

    public componentDidMount() {
        this.props.reduxForm.setInitializeFormDataAction(this.initReduxForm);
    }

    public shouldComponentUpdate(nextProps: OwnProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private onValueChange<TValue, TForm extends string = AgencyAgreementFilterDataFormFieldNamesType>(formName: TForm, newValue: TValue) {
        this.props.reduxForm.updateReduxFormFields({
            [formName]: newValue
        });
        if (formName === 'agencyAgreementName' && newValue && (newValue as any as string).length < 3) {
            return false;
        }
    }

    private onValueApplied<TValue>(formName: string, value: TValue) {
        this.props.onFilterChanged({
            ...this.props.reduxForm.getReduxFormFields(false),
            [formName]: value
        }, formName);
    }

    private initReduxForm(): AgencyAgreementFilterDataForm | undefined {
        return {
            agencyAgreementName: ''
        };
    }

    public render() {
        const formFields = this.props.reduxForm.getReduxFormFields(false);

        return (
            <TextBoxForm
                autoFocus={ true }
                label="Агентское соглашение"
                formName="agencyAgreementName"
                onValueChange={ this.onValueChange }
                onValueApplied={ this.onValueApplied }
                value={ formFields.agencyAgreementName }
                isReadOnly={ this.props.isFilterFormDisabled }
            />
        );
    }
}

export const AgencyAgreementFilterFormView = withReduxForm(AgencyAgreementFilterFormViewClass, {
    reduxFormName: 'AgencyAgreement_Filter_Form',
    resetOnUnmount: true
});