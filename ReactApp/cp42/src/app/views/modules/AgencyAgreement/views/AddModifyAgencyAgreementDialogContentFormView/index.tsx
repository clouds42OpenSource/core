import { Box } from '@mui/material';
import { hasComponentChangesFor } from 'app/common/functions';
import { TextOut } from 'app/views/components/TextOut';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { TextButton } from 'app/views/components/controls/Button';
import { ComboBoxForm, TextBoxForm } from 'app/views/components/controls/forms';
import { MuiDateInputForm } from 'app/views/components/controls/forms/MuiDateInputForm';
import { PercentInputForm } from 'app/views/components/controls/forms/mask-input/PercentInputForm';
import { ReadOnlyKeyValueView } from 'app/views/modules/_common/components/ReadOnlyKeyValueView';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import {
    AgencyAgreementItemDataModel
} from 'app/web/InterlayerApiProxy/AgencyAgreementApiProxy/receiveAgencyAgreements/data-models/AgencyAgreementItemDataModel';
import { KeyValueDataModel } from 'app/web/common/data-models';
import { RequestKind } from 'core/requestSender/enums';
import React from 'react';

type OwnProps = ReduxFormProps<AgencyAgreementItemDataModel> & {
    isAddNewMode: boolean;
    itemToEdit: AgencyAgreementItemDataModel;
};

type OwnState = {
    printedHtmlFormsForAgencyAgreement: KeyValueDataModel<string, string>[];
};

class AddModifyAgencyAgreementDialogContentFormViewClass extends React.Component<OwnProps, OwnState> {
    public constructor(props: OwnProps) {
        super(props);
        this.onValueChange = this.onValueChange.bind(this);
        this.initReduxForm = this.initReduxForm.bind(this);

        this.props.reduxForm.setInitializeFormDataAction(this.initReduxForm);
        this.showAgencyAgreement = this.showAgencyAgreement.bind(this);
        this.getAgencyAgreementText = this.getAgencyAgreementText.bind(this);
        this.state = {
            printedHtmlFormsForAgencyAgreement: []
        };
    }

    public async componentDidMount() {
        const api = InterlayerApiProxy.getPrintedHtmlFormApiProxy();
        try {
            const result = await api.receivePrintedHtmlFormsForAgencyAgreement(RequestKind.SEND_BY_ROBOT);
            this.setState({
                printedHtmlFormsForAgencyAgreement: result
            });
        } catch (ex) {
            console.log(ex);
        }
    }

    public shouldComponentUpdate(nextProps: OwnProps, nextState: OwnState) {
        return hasComponentChangesFor(this.props, nextProps) ||
            hasComponentChangesFor(this.state, nextState);
    }

    private onValueChange<TValue>(formName: string, newValue: TValue) {
        this.props.reduxForm.updateReduxFormFields({
            [formName]: newValue
        });
    }

    private getAgencyAgreementText() {
        const foundItem = this.state.printedHtmlFormsForAgencyAgreement.filter(item => item.key === this.props.itemToEdit.printedHtmlFormId);
        return foundItem.length ? foundItem[0].value : '!!!Неизвестное агентское соглашение';
    }

    private showAgencyAgreement() {
        const api = InterlayerApiProxy.getAgencyAgreementApiProxy();
        api.openAgencyAgreement({
            agencyAgreementId: this.props.itemToEdit.agencyAgreementId
        });
    }

    private initReduxForm(): AgencyAgreementItemDataModel | undefined {
        if (!this.props.itemToEdit || !this.state.printedHtmlFormsForAgencyAgreement.length) {
            return;
        }
        return {
            agencyAgreementId: this.props.itemToEdit.agencyAgreementId,
            agencyAgreementName: this.props.itemToEdit.agencyAgreementName,
            printedHtmlFormId: this.props.itemToEdit.printedHtmlFormId ? this.props.itemToEdit.printedHtmlFormId : this.state.printedHtmlFormsForAgencyAgreement[0].key,
            myDiskRewardPercent: this.props.itemToEdit.myDiskRewardPercent,
            rent1CRewardPercent: this.props.itemToEdit.rent1CRewardPercent,
            serviceOwnerRewardPercent: this.props.itemToEdit.serviceOwnerRewardPercent,
            rent1CRewardPercentForVipAccounts: this.props.itemToEdit.rent1CRewardPercentForVipAccounts,
            myDiskRewardPercentForVipAccounts: this.props.itemToEdit.myDiskRewardPercentForVipAccounts,
            serviceOwnerRewardPercentForVipAccounts: this.props.itemToEdit.serviceOwnerRewardPercentForVipAccounts,
            creationDate: this.props.itemToEdit.creationDate,
            effectiveDate: this.props.itemToEdit.effectiveDate
        };
    }

    public render() {
        if (!this.props.itemToEdit || !this.state.printedHtmlFormsForAgencyAgreement.length) {
            return null;
        }

        const reduxForm = this.props.reduxForm.getReduxFormFields(false);
        const agencyAgreementText = this.props.isAddNewMode ? '' : this.getAgencyAgreementText();
        return (
            <Box display="flex" flexDirection="column" gap="16px">
                <TextBoxForm
                    formName="agencyAgreementName"
                    label="Название агентского соглашения"
                    value={ reduxForm.agencyAgreementName }
                    autoFocus={ this.props.isAddNewMode }
                    onValueChange={ this.props.isAddNewMode ? this.onValueChange : void 0 }
                    isReadOnly={ !this.props.isAddNewMode }
                />
                {
                    this.props.isAddNewMode
                        ? (
                            <ComboBoxForm
                                formName="printedHtmlFormId"
                                label="Выберите шаблон агентского соглашения"
                                value={ reduxForm.printedHtmlFormId }
                                onValueChange={ this.props.isAddNewMode ? this.onValueChange : void 0 }
                                items={ this.state.printedHtmlFormsForAgencyAgreement.map(item => {
                                    return {
                                        value: item.key,
                                        text: item.value
                                    };
                                }) }
                            />
                        )
                        : (
                            <ReadOnlyKeyValueView
                                label="Шаблон агентского соглашения"
                                text={
                                    <TextButton
                                        className="text-link"
                                        onClick={ this.showAgencyAgreement }
                                    >
                                        { agencyAgreementText }
                                    </TextButton>
                                }
                            />
                        )
                }
                <MuiDateInputForm
                    formName="effectiveDate"
                    label="Дата вступления в силу"
                    value={ reduxForm.effectiveDate }
                    onValueChange={ this.props.isAddNewMode ? this.onValueChange : void 0 }
                    isReadOnly={ !this.props.isAddNewMode }
                />
                <TextOut fontSize={ 14 } fontWeight={ 600 }>
                    Вознаграждения агентов в разрезе сервисов
                </TextOut>
                <PercentInputForm
                    formName="rent1CRewardPercent"
                    label="Аренда 1С"
                    value={ reduxForm.rent1CRewardPercent }
                    onValueChange={ this.onValueChange }
                    isReadOnly={ !this.props.isAddNewMode }
                />
                <PercentInputForm
                    formName="myDiskRewardPercent"
                    label="Мой диск"
                    value={ reduxForm.myDiskRewardPercent }
                    onValueChange={ this.onValueChange }
                    isReadOnly={ !this.props.isAddNewMode }
                />
                <PercentInputForm
                    formName="rent1CRewardPercentForVipAccounts"
                    label="Аренда 1С (VIP)"
                    value={ reduxForm.rent1CRewardPercentForVipAccounts }
                    onValueChange={ this.onValueChange }
                    isReadOnly={ !this.props.isAddNewMode }
                />
                <PercentInputForm
                    formName="myDiskRewardPercentForVipAccounts"
                    label="Мой диск (VIP)"
                    value={ reduxForm.myDiskRewardPercentForVipAccounts }
                    onValueChange={ this.onValueChange }
                    isReadOnly={ !this.props.isAddNewMode }
                />
                <TextOut fontSize={ 14 } fontWeight={ 600 }>
                    Вознаграждения владельцев сервисов
                </TextOut>
                <PercentInputForm
                    formName="serviceOwnerRewardPercent"
                    label="Собственные сервисы разработчика"
                    value={ reduxForm.serviceOwnerRewardPercent }
                    onValueChange={ this.onValueChange }
                    isReadOnly={ !this.props.isAddNewMode }
                />
                <PercentInputForm
                    formName="serviceOwnerRewardPercentForVipAccounts"
                    label="Собственные сервисы разработчика (VIP)"
                    value={ reduxForm.serviceOwnerRewardPercentForVipAccounts }
                    onValueChange={ this.onValueChange }
                    isReadOnly={ !this.props.isAddNewMode }
                />
            </Box>
        );
    }
}

export const AddModifyAgencyAgreementDialogContentFormView = withReduxForm(AddModifyAgencyAgreementDialogContentFormViewClass, {
    reduxFormName: 'AddModifyAgencyAgreement_ReduxForm',
    resetOnUnmount: true
});