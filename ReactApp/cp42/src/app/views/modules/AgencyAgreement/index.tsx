import { ImportContacts } from '@mui/icons-material';
import { SortingKind } from 'app/common/enums';
import { hasComponentChangesFor } from 'app/common/functions';
import { ProcessIdStateHelper } from 'app/common/helpers/ProcessIdStateHelper';
import { AgencyAgreementProcessId } from 'app/modules/AgencyAgreement/AgencyAgreementProcessId';
import { AddAgencyAgreementThunkParams } from 'app/modules/AgencyAgreement/store/reducers/addAgencyAgreementReducer/params';
import { ReceiveAgencyAgreementsParamsThunkParams } from 'app/modules/AgencyAgreement/store/reducers/receiveAgencyAgreementsReducer/params';
import { AddAgencyAgreementThunk, ReceiveAgencyAgreementsThunk } from 'app/modules/AgencyAgreement/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { DateUtility } from 'app/utils';
import { withHeader } from 'app/views/components/_hoc/withHeader';
import { AgencyAgreementFilterDataForm } from 'app/views/modules/AgencyAgreement/types';
import { AddModifyAgencyAgreementDialogContentFormView } from 'app/views/modules/AgencyAgreement/views/AddModifyAgencyAgreementDialogContentFormView';
import { AgencyAgreementFilterFormView } from 'app/views/modules/AgencyAgreement/views/AgencyAgreementFilterFormView';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { TablePagination } from 'app/views/modules/_common/components/CommonTableWithFilter/TableView';
import { AgencyAgreementItemDataModel } from 'app/web/InterlayerApiProxy/AgencyAgreementApiProxy/receiveAgencyAgreements/data-models/AgencyAgreementItemDataModel';
import { SelectDataCommonDataModel } from 'app/web/common/data-models';
import { SortingDataModel } from 'app/web/common/data-models/SortingDataModel';
import React from 'react';
import { connect } from 'react-redux';

type StateProps<TDataRow = AgencyAgreementItemDataModel> = {
    dataset: TDataRow[];
    isInLoadingDataProcess: boolean;
    metadata: TablePagination;
};

type DispatchProps = {
    dispatchReceiveAgencyAgreementsThunk: (args: ReceiveAgencyAgreementsParamsThunkParams) => void;
    dispatchAddAgencyAgreementThunk: (args: AddAgencyAgreementThunkParams) => void;
};

type AllProps = StateProps & DispatchProps;

class AgencyAgreementClass extends React.Component<AllProps> {
    public constructor(props: AllProps) {
        super(props);
        this.onDataSelect = this.onDataSelect.bind(this);
    }

    public shouldComponentUpdate(nextProps: AllProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private onDataSelect(args: SelectDataCommonDataModel<AgencyAgreementFilterDataForm>) {
        this.props.dispatchReceiveAgencyAgreementsThunk({
            pageNumber: args.pageNumber,
            filter: args.filter,
            orderBy: args.sortingData ? this.getSortQuery(args.sortingData) : 'name.asc',
            force: true
        });
    }

    private getSortQuery(args: SortingDataModel) {
        switch (args.fieldName) {
            case 'agencyAgreementName':
                return `name.${ args.sortKind ? 'desc' : 'asc' }`;
            default:
                break;
        }
    }

    public render() {
        return (
            <CommonTableWithFilter
                uniqueContextProviderStateId="AgencyAgreementContextProviderStateId"
                tableEditProps={ {
                    processProps: {
                        watch: {
                            reducerStateName: 'AgencyAgreementState',
                            processIds: {
                                addNewItemProcessId: AgencyAgreementProcessId.AddAgencyAgreement
                            }
                        },
                        processMessages: {
                            getOnSuccessAddNewItemMessage: () => 'Добавление нового агентского соглашения успешно завершено.'
                        }
                    },
                    dialogProps: {
                        addDialog: {
                            dialogWidth: 'sm',
                            getDialogTitle: () => 'Добавление новой редакции Агентского соглашения',
                            getDialogContentFormView: ref =>
                                <AddModifyAgencyAgreementDialogContentFormView
                                    ref={ ref }
                                    isAddNewMode={ true }
                                    itemToEdit={
                                        {
                                            agencyAgreementId: '',
                                            agencyAgreementName: '',
                                            printedHtmlFormId: '',
                                            myDiskRewardPercent: 0,
                                            rent1CRewardPercent: 0,
                                            serviceOwnerRewardPercent: 0,
                                            rent1CRewardPercentForVipAccounts: 0,
                                            myDiskRewardPercentForVipAccounts: 0,
                                            serviceOwnerRewardPercentForVipAccounts: 0,
                                            effectiveDate: new Date(),
                                            creationDate: new Date()
                                        }
                                    }
                                />,
                            confirmAddItemButtonText: 'Добавить',
                            showAddDialogButtonText: 'Добавить новую редакцию',
                            onAddNewItemConfirmed: itemToAdd => {
                                this.props.dispatchAddAgencyAgreementThunk({
                                    agencyAgreementName: itemToAdd.agencyAgreementName,
                                    effectiveDate: new Date(itemToAdd.effectiveDate),
                                    printedHtmlFormId: itemToAdd.printedHtmlFormId,
                                    myDiskRewardPercent: itemToAdd.myDiskRewardPercent,
                                    serviceOwnerRewardPercent: itemToAdd.serviceOwnerRewardPercent,
                                    rent1CRewardPercent: itemToAdd.rent1CRewardPercent,
                                    rent1CRewardPercentForVipAccounts: itemToAdd.rent1CRewardPercentForVipAccounts,
                                    myDiskRewardPercentForVipAccounts: itemToAdd.myDiskRewardPercentForVipAccounts,
                                    serviceOwnerRewardPercentForVipAccounts: itemToAdd.serviceOwnerRewardPercentForVipAccounts,
                                    force: true
                                });
                            }
                        },
                        modifyDialog: {
                            getDialogTitle: () => 'Просмотр агентского соглашения',
                            dialogWidth: 'sm',
                            confirmModifyItemButtonText: 'Сохранить',
                            cancelButtonText: 'Закрыть',
                            getDialogContentFormView: (ref, item) => <AddModifyAgencyAgreementDialogContentFormView
                                ref={ ref }
                                isAddNewMode={ false }
                                itemToEdit={ item }
                            />
                        }
                    }
                } }
                filterProps={ {
                    getFilterContentView: (ref, onFilterChanged) => <AgencyAgreementFilterFormView ref={ ref } isFilterFormDisabled={ this.props.isInLoadingDataProcess } onFilterChanged={ onFilterChanged } />
                } }
                tableProps={ {
                    dataset: this.props.dataset,
                    keyFieldName: 'agencyAgreementId',
                    sorting: {
                        sortFieldName: 'agencyAgreementName',
                        sortKind: SortingKind.Asc
                    },
                    fieldsView: {
                        agencyAgreementName: {
                            caption: 'Соглашение',
                            fieldContentNoWrap: false,
                            isSortable: true,
                            fieldWidth: 'auto'
                        },
                        effectiveDate: {
                            caption: 'Дата вступления в силу',
                            fieldContentNoWrap: true,
                            isSortable: false,
                            format: value => {
                                return DateUtility.dateToDayMonthYear(value);
                            }
                        }
                    },
                    modifyButtonContent: <ImportContacts />,
                    modifyButtonTitle: 'Просмотр',
                    pagination: this.props.metadata
                } }
                onDataSelect={ this.onDataSelect }
            />
        );
    }
}

const AgencyAgreementConnected = connect<StateProps, DispatchProps, {}, AppReduxStoreState>(
    state => {
        const agencyAgreementState = state.AgencyAgreementState;
        const { agencyAgreements } = agencyAgreementState;
        const dataset = agencyAgreements.records;
        const currentPage = agencyAgreements.metadata.pageNumber;
        const totalPages = agencyAgreements.metadata.pageCount;
        const { pageSize } = agencyAgreements.metadata;
        const recordsCount = agencyAgreements.metadata.totalItemCount;

        const isInLoadingDataProcess = ProcessIdStateHelper.isInProgress(agencyAgreementState.reducerActions, AgencyAgreementProcessId.ReceiveAgencyAgreements);

        return {
            dataset,
            isInLoadingDataProcess,
            metadata: {
                currentPage,
                totalPages,
                pageSize,
                recordsCount
            }
        };
    },
    {
        dispatchReceiveAgencyAgreementsThunk: ReceiveAgencyAgreementsThunk.invoke,
        dispatchAddAgencyAgreementThunk: AddAgencyAgreementThunk.invoke
    }
)(AgencyAgreementClass);

export const AgencyAgreementView = withHeader({
    title: 'Справочник "Агентское соглашение"',
    page: AgencyAgreementConnected
});