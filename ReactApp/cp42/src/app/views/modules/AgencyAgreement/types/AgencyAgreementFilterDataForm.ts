/**
 * Модель Redux формы для редактирования фильтра для таблицы агентских соглашений
 */
export type AgencyAgreementFilterDataForm = {
    /**
     * Название агентского соглашения
     */
    agencyAgreementName: string;
};