import { AgencyAgreementFilterDataForm } from 'app/views/modules/AgencyAgreement/types';

export type AgencyAgreementFilterDataFormFieldNamesType = keyof AgencyAgreementFilterDataForm;