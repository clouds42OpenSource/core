import { SortingKind, StateMachineComponentStatus, StateMachineComponentStatusExtensions } from 'app/common/enums';
import { hasComponentChangesFor } from 'app/common/functions';
import { ProcessIdStateHelper } from 'app/common/helpers/ProcessIdStateHelper';
import { ProcessFlowProcessId } from 'app/modules/ProcessFlow';
import { ReceiveProcessFlowThunkParams } from 'app/modules/ProcessFlow/store/reducers/receiveProcessFlowReducer/params';
import { ReceiveProcessFlowThunk } from 'app/modules/ProcessFlow/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { DateUtility } from 'app/utils';
import { TextLinkButton } from 'app/views/components/controls/Button';
import { StatusLabel } from 'app/views/components/controls/Labels';
import { TextOut } from 'app/views/components/TextOut';
import { withHeader } from 'app/views/components/_hoc/withHeader';
import { ProcessFlowFilterDataForm } from 'app/views/modules/ProcessFlow/types';
import { ProcessFlowFilterFormView } from 'app/views/modules/ProcessFlow/views/ProcessFlowFilterFormView';
import { RetryProcessFlowButton } from 'app/views/modules/ProcessFlow/views/_buttons';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { TablePagination } from 'app/views/modules/_common/components/CommonTableWithFilter/TableView';
import { SelectDataCommonDataModel } from 'app/web/common/data-models';
import { ProcessFlowItemDataModel } from 'app/web/InterlayerApiProxy/ProcessFlowApiProxy/receiveProcessFlowList';
import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { RouteComponentProps } from 'react-router-dom';

type StateProps<TDataRow = ProcessFlowItemDataModel> = {
    dataset: TDataRow[];
    isInLoadingDataProcess: boolean;
    metadata: TablePagination;
};

type DispatchProps = {
    dispatchReceiveProcessFlowThunkThunk: (args: ReceiveProcessFlowThunkParams) => void;
};

type AllProps = StateProps & DispatchProps & RouteComponentProps;

class ProcessFlowViewClass extends React.Component<AllProps> {
    public constructor(props: AllProps) {
        super(props);
        this.onDataSelect = this.onDataSelect.bind(this);
        this.getProcessFlowNameFormat = this.getProcessFlowNameFormat.bind(this);
        this.openProcessFlowDetailsPage = this.openProcessFlowDetailsPage.bind(this);
    }

    public shouldComponentUpdate(nextProps: AllProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private onDataSelect(args: SelectDataCommonDataModel<ProcessFlowFilterDataForm>) {
        this.props.dispatchReceiveProcessFlowThunkThunk({
            pageNumber: args.pageNumber,
            filter: args.filter,
            sortingData: args.sortingData,
            force: true
        });
    }

    private getProcessFlowNameFormat(value: string, data: ProcessFlowItemDataModel) {
        return (
            <TextLinkButton
                canSelectButtonContent={ true }
                wrapLongText={ true }
                onClick={ () => { this.openProcessFlowDetailsPage(data.processFlowId); } }
            >
                <TextOut fontSize={ 13 } className="text-link">
                    { value }
                </TextOut>
            </TextLinkButton>
        );
    }

    private openProcessFlowDetailsPage(id: string) {
        this.props.history.push(`process-flow/details/${ id }`);
    }

    public render() {
        return (
            <CommonTableWithFilter
                isResponsiveTable={ true }
                isBorderStyled={ true }
                uniqueContextProviderStateId="ProcessFlowContextProviderStateId"
                filterProps={ {
                    getFilterContentView: (ref, onFilterChanged) => <ProcessFlowFilterFormView ref={ ref } isFilterFormDisabled={ this.props.isInLoadingDataProcess } onFilterChanged={ onFilterChanged } />
                } }
                tableProps={ {
                    dataset: this.props.dataset,
                    keyFieldName: 'processFlowId',
                    sorting: {
                        sortFieldName: 'creationDateTime',
                        sortKind: SortingKind.Desc
                    },
                    fieldsView: {
                        creationDateTime: {
                            caption: 'Дата создания',
                            format: (value: Date) => {
                                return DateUtility.dateToDayMonthYearHourMinute(value);
                            }
                        },
                        processFlowName: {
                            caption: 'Название процесса',
                            format: this.getProcessFlowNameFormat
                        },
                        status: {
                            caption: 'Статус',
                            format: (value: StateMachineComponentStatus) =>
                                <StatusLabel
                                    type={ StateMachineComponentStatusExtensions.getLabel(value) }
                                    text={ StateMachineComponentStatusExtensions.getDescription(value) }
                                />
                        },
                        stateDescription: {
                            caption: 'Состояние',
                        },
                        processedObjectName: {
                            caption: 'Имя обрабатываемого процессом объекта',
                        },
                        comment: {
                            caption: 'Комментарий',
                            format: (value: string, row) => {
                                return (
                                    <>
                                        <TextOut>{ value }</TextOut>
                                        { row.status === StateMachineComponentStatus.Error &&
                                            <>
                                                <br />
                                                <RetryProcessFlowButton processFlowId={ row.processFlowId } />
                                            </>
                                        }
                                    </>
                                );
                            }
                        }

                    },
                    pagination: this.props.metadata
                } }
                onDataSelect={ this.onDataSelect }
            />
        );
    }
}

const ProcessFlowViewConnected = connect<StateProps, DispatchProps, object, AppReduxStoreState>(
    state => {
        const processFlowState = state.ProcessFlowState;
        const dataset = processFlowState.processFlow.records;

        const currentPage = processFlowState.processFlow.metadata.pageNumber;
        const totalPages = processFlowState.processFlow.metadata.pageCount;
        const { pageSize } = processFlowState.processFlow.metadata;
        const recordsCount = processFlowState.processFlow.metadata.totalItemCount;

        const isInLoadingDataProcess = ProcessIdStateHelper.isInProgress(processFlowState.reducerActions, ProcessFlowProcessId.ReceiveProcessFlow);

        return {
            dataset,
            isInLoadingDataProcess,
            metadata: {
                currentPage,
                totalPages,
                pageSize,
                recordsCount
            }
        };
    },
    {
        dispatchReceiveProcessFlowThunkThunk: ReceiveProcessFlowThunk.invoke
    }
)(withRouter(ProcessFlowViewClass));

export const ProcessFlowView = withHeader({
    title: 'Рабочие процессы',
    page: ProcessFlowViewConnected
});