import { ProcessFlowFilterDataForm } from 'app/views/modules/ProcessFlow/types';

export type ProcessFlowFilterDataFormFieldNamesType = keyof ProcessFlowFilterDataForm;