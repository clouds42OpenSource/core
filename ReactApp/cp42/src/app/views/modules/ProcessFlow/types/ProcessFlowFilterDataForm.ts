import { StateMachineComponentStatus } from 'app/common/enums';

/**
 * Модель Redux формы для редактирования фильтра для таблицы рабочих процесов
 */
export type ProcessFlowFilterDataForm = {
    /**
     * Строка фильтра
     */
    searchString: string;

    /**
     * Статус рабочего процеса
     */
    status: StateMachineComponentStatus;
};