import { hasComponentChangesFor } from 'app/common/functions';
import { ProcessFlowProcessId } from 'app/modules/ProcessFlow';
import { RetryProcessFlowThunkParams } from 'app/modules/ProcessFlow/store/reducers/retryProcessFlowReducer/params';
import { RetryProcessFlowThunk } from 'app/modules/ProcessFlow/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { OutlinedButton } from 'app/views/components/controls/Button';
import { WatchReducerProcessActionState } from 'app/views/modules/_common/components/WatchReducerProcessActionState';
import cn from 'classnames';
import { IReducerProcessActionInfo } from 'core/redux/interfaces';
import React from 'react';
import { connect } from 'react-redux';
import css from '../styles.module.css';

type OwnProps = FloatMessageProps & {
    processFlowId: string;
};

type OwnState = {
    retryProcessFlowState: {
        isInProgressState: boolean;
        isInSuccessState: boolean;
        isInErrorState: boolean;
        error?: Error;
    }
};

type DispatchProps = {
    dispatchRetryprocessFlowThunk: (args: RetryProcessFlowThunkParams) => void;
};

type StateProps = {
    propcessFlowIdInRetry: string | null;
};

type AllProps = OwnProps & DispatchProps & StateProps;

class RetryProcessFlowButtonClass extends React.Component<AllProps, OwnState> {
    public constructor(props: AllProps) {
        super(props);

        this.onRetryProcessFlowClick = this.onRetryProcessFlowClick.bind(this);
        this.onProcessActionStateChanged = this.onProcessActionStateChanged.bind(this);

        this.state = {
            retryProcessFlowState: {
                isInErrorState: false,
                isInProgressState: false,
                isInSuccessState: false,
                error: undefined
            }
        };
    }

    public shouldComponentUpdate(nextProps: OwnProps, nextState: OwnState) {
        return hasComponentChangesFor(this.props, nextProps) ||
            hasComponentChangesFor(this.state, nextState);
    }

    public componentDidUpdate(_prevProps: AllProps, prevState: OwnState) {
        const isThisProccessFlowId = this.props.processFlowId === this.props.propcessFlowIdInRetry;

        if (!isThisProccessFlowId) {
            return;
        }

        if (!prevState.retryProcessFlowState.isInSuccessState && this.state.retryProcessFlowState.isInSuccessState) {
            this.props.floatMessage.show(MessageType.Success, 'Перезапуск рабочего процеса выполнен.');
        } else if (!prevState.retryProcessFlowState.isInErrorState && this.state.retryProcessFlowState.isInErrorState) {
            this.props.floatMessage.show(MessageType.Error, this.state.retryProcessFlowState.error?.message);
        }
    }

    private onRetryProcessFlowClick() {
        this.props.dispatchRetryprocessFlowThunk({
            processFlowId: this.props.processFlowId,
            force: true
        });
    }

    private onProcessActionStateChanged(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId === ProcessFlowProcessId.RetryProcessFlow) {
            this.setState({
                retryProcessFlowState: {
                    isInProgressState: actionState.isInProgressState,
                    isInSuccessState: actionState.isInSuccessState,
                    isInErrorState: actionState.isInErrorState,
                    error
                }
            });
        }
    }

    public render() {
        const isThisProccessFlowId = this.props.processFlowId === this.props.propcessFlowIdInRetry;
        return (
            <>
                <OutlinedButton
                    className={ cn(css['retry-process-button']) }
                    isEnabled={ !isThisProccessFlowId || !this.state.retryProcessFlowState.isInProgressState }
                    showProgress={ isThisProccessFlowId && this.state.retryProcessFlowState.isInProgressState }
                    kind="primary"
                    title="Перезапустить рабочий процесс"
                    onClick={ this.onRetryProcessFlowClick }
                >
                    <i className="fa fa-recycle" />
                </OutlinedButton>
                <WatchReducerProcessActionState
                    watch={ [{
                        reducerStateName: 'ProcessFlowState',
                        processIds: [ProcessFlowProcessId.RetryProcessFlow]
                    }] }
                    onProcessActionStateChanged={ this.onProcessActionStateChanged }
                />
            </>
        );
    }
}

const RetryProcessFlowButtonClassConnected = connect<StateProps, DispatchProps, OwnProps, AppReduxStoreState>(
    state => {
        const processFlowState = state.ProcessFlowState;
        const { propcessFlowIdInRetry } = processFlowState;

        return {
            propcessFlowIdInRetry
        };
    },
    {
        dispatchRetryprocessFlowThunk: RetryProcessFlowThunk.invoke,
    }
)(RetryProcessFlowButtonClass);

export const RetryProcessFlowButton = withFloatMessages(RetryProcessFlowButtonClassConnected);