import {
    ProcessFlowFilterDataForm,
    ProcessFlowFilterDataFormFieldNamesType
} from 'app/views/modules/ProcessFlow/types';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { StateMachineComponentStatus, StateMachineComponentStatusExtensions } from 'app/common/enums';
import { getComboBoxFormOptionsFromKeyValueDataModel, hasComponentChangesFor } from 'app/common/functions';
import { ComboBoxForm } from 'app/views/components/controls/forms';
import { FilterItemWrapper } from 'app/views/components/_hoc/tableFilterWrapper/filterItemWrapper';
import React from 'react';
import { TableFilterWrapper } from 'app/views/components/_hoc/tableFilterWrapper';
import { TextBoxForm } from 'app/views/components/controls/forms/TextBoxForm';

/**
 * Свойства для компонента фильтра
 */
type OwnProps = ReduxFormProps<ProcessFlowFilterDataForm> & {
    onFilterChanged: (filter: ProcessFlowFilterDataForm, filterFormName: string) => void;
    isFilterFormDisabled: boolean;
};

class ProcessFlowFilterFormViewClass extends React.Component<OwnProps> {
    public constructor(props: OwnProps) {
        super(props);
        this.initReduxForm = this.initReduxForm.bind(this);
        this.onValueChange = this.onValueChange.bind(this);
        this.onValueApplied = this.onValueApplied.bind(this);
    }

    public componentDidMount() {
        this.props.reduxForm.setInitializeFormDataAction(this.initReduxForm);
    }

    public shouldComponentUpdate(nextProps: OwnProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private onValueChange<TValue, TForm extends string = ProcessFlowFilterDataFormFieldNamesType>(formName: TForm, newValue: TValue) {
        this.props.reduxForm.updateReduxFormFields({
            [formName]: newValue
        });

        if (formName === 'searchLine' && newValue && (newValue as any as string).length < 3) {
            return false;
        }
    }

    private onValueApplied<TValue>(formName: string, value: TValue) {
        this.props.onFilterChanged({
            ...this.props.reduxForm.getReduxFormFields(false),
            [formName]: value
        }, formName);
    }

    private initReduxForm(): ProcessFlowFilterDataForm | undefined {
        return {
            searchString: '',
            status: StateMachineComponentStatus.Any
        };
    }

    public render() {
        const formFields = this.props.reduxForm.getReduxFormFields(false);

        return (
            <TableFilterWrapper>
                <FilterItemWrapper width="370px">
                    <TextBoxForm
                        label="Обрабатываемый объект"
                        formName="searchString"
                        onValueChange={ this.onValueChange }
                        onValueApplied={ this.onValueApplied }
                        value={ formFields.searchString }
                        isReadOnly={ this.props.isFilterFormDisabled }
                        placeholder="Введите имя обрабатываемого процессом объекта"
                    />
                </FilterItemWrapper>
                <FilterItemWrapper width="370px">
                    <ComboBoxForm
                        label="Состояние процесса"
                        formName="status"
                        onValueChange={ this.onValueChange }
                        onValueApplied={ this.onValueApplied }
                        value={ formFields.status }
                        isReadOnly={ this.props.isFilterFormDisabled }
                        items={ getComboBoxFormOptionsFromKeyValueDataModel(StateMachineComponentStatusExtensions.getAllDescriptions()) }
                    />
                </FilterItemWrapper>
            </TableFilterWrapper>
        );
    }
}

export const ProcessFlowFilterFormView = withReduxForm(ProcessFlowFilterFormViewClass, {
    reduxFormName: 'ProcessFlow_Filter_Form',
    resetOnUnmount: true
});