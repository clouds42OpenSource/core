import { Box } from '@mui/material';
import { AppRoutes } from 'app/AppRoutes';
import { StateMachineComponentStatus, StateMachineComponentStatusExtensions } from 'app/common/enums';
import { ProcessFlowProcessId } from 'app/modules/ProcessFlow';
import { ReceiveProcessFlowDetailsThunkParams } from 'app/modules/ProcessFlow/store/reducers/receiveProcessFlowDetailsReducer/params';
import { RetryProcessFlowThunkParams } from 'app/modules/ProcessFlow/store/reducers/retryProcessFlowReducer/params';
import { ReceiveProcessFlowDetailsThunk, RetryProcessFlowThunk } from 'app/modules/ProcessFlow/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { DateUtility } from 'app/utils';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { PageHeaderView } from 'app/views/components/_hoc/withHeader/PageHeaderView';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { ContainedButton, SuccessButton } from 'app/views/components/controls/Button';
import { StatusLabel } from 'app/views/components/controls/Labels';
import { ReadOnlyKeyValueView } from 'app/views/modules/_common/components/ReadOnlyKeyValueView';
import { WatchReducerProcessActionState } from 'app/views/modules/_common/components/WatchReducerProcessActionState';
import { ActionFlowDataModel, ProcessFlowDetailsDataModel } from 'app/web/InterlayerApiProxy/ProcessFlowApiProxy/receiveProcessFlowDetails/data-models';
import { IReducerProcessActionInfo } from 'core/redux/interfaces';
import { Component } from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router-dom';
import css from './styles.module.css';

type OwnProps = ReduxFormProps<ProcessFlowDetailsDataModel>;

type OwnState = {
    /**
     * ID рабочего процесса
     */
    processFlowId: string;
};

type StateProps = {
    processFlowDetails: ProcessFlowDetailsDataModel,
};

type DispatchProps = {
    dispatchGetProcessFlowDetailsByIdThunk: (args: ReceiveProcessFlowDetailsThunkParams) => void;
    dispatchRetryProcessFlowThunk: (args: RetryProcessFlowThunkParams) => void;
};

type AllProps = OwnProps & StateProps & DispatchProps & FloatMessageProps & RouteComponentProps<{ id?: string }>;

class ProcessFlowDetailsViewClass extends Component<AllProps, OwnState> {
    public constructor(props: AllProps) {
        super(props);

        this.state = this.getInitState(props);

        this.defaultInitDateReduxForm = this.defaultInitDateReduxForm.bind(this);
        this.onRetryProcessFlowClick = this.onRetryProcessFlowClick.bind(this);
        this.onProcessActionStateChanged = this.onProcessActionStateChanged.bind(this);
        this.successOrFailedStateNotification = this.successOrFailedStateNotification.bind(this);
        this.redirectToProcessFlowsPage = this.redirectToProcessFlowsPage.bind(this);
        this.getActionFlowDetailsContainer = this.getActionFlowDetailsContainer.bind(this);
        this.getStateMachineComponentStatusLabel = this.getStateMachineComponentStatusLabel.bind(this);
        this.props.reduxForm.setInitializeFormDataAction(this.defaultInitDateReduxForm);
    }

    public componentDidMount() {
        this.props.dispatchGetProcessFlowDetailsByIdThunk({
            processFlowId: this.state.processFlowId,
            force: true
        });
    }

    private async onProcessActionStateChanged(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId === ProcessFlowProcessId.ReceiveProcessFlowDetails) {
            if (actionState.isInErrorState) {
                this.successOrFailedStateNotification('Ошибка при получении деталей рабочего процесса', false);
            }
            if (actionState.isInSuccessState) {
                const { processFlowDetails } = this.props;

                this.props.reduxForm.updateReduxFormFields({
                    comment: processFlowDetails.comment,
                    status: processFlowDetails.status,
                    stateDescription: processFlowDetails.stateDescription,
                    creationDateTime: processFlowDetails.creationDateTime,
                    processedObjectName: processFlowDetails.processedObjectName,
                    processFlowName: processFlowDetails.processFlowName,
                    processFlowId: processFlowDetails.processFlowId,
                    actionFlows: processFlowDetails.actionFlows
                });
            }
        }
        if (processId === ProcessFlowProcessId.RetryProcessFlow) {
            if (actionState.isInSuccessState) {
                this.successOrFailedStateNotification('Рабочий процесс успешно перезапущен', true);
                this.props.dispatchGetProcessFlowDetailsByIdThunk({
                    processFlowId: this.state.processFlowId,
                    force: true
                });
            }
            if (actionState.isInErrorState) {
                this.successOrFailedStateNotification(`Ошибка перезапуска рабочего процесса. ${ error?.message }`, false);
            }
        }
    }

    private onRetryProcessFlowClick() {
        this.props.dispatchRetryProcessFlowThunk({
            processFlowId: this.state.processFlowId,
            force: true
        });
    }

    private getStateMachineComponentStatusLabel(status: StateMachineComponentStatus) {
        return (<StatusLabel
            type={ StateMachineComponentStatusExtensions.getLabel(status) }
            text={ StateMachineComponentStatusExtensions.getDescription(status) }
        />);
    }

    private getActionFlowDetailsContainer(actionFlowData: ActionFlowDataModel, index: number) {
        return (
            <div className={ css['sl-item'] }>
                <div className={ css['sl-left'] }>
                    <span className={ css['sl-number-container'] }>
                        <span>{ index + 1 }</span>
                    </span>
                </div>
                <div className={ css['sl-right'] }>
                    <div>
                        <p><b>{ actionFlowData.actionDescription }</b></p>
                        <span>{ actionFlowData.actionName }</span>
                        <div>
                            <span>Количество попыток: { actionFlowData.countOfAttemps } </span>
                            { this.getStateMachineComponentStatusLabel(actionFlowData.status) }
                            { actionFlowData.status === StateMachineComponentStatus.Error
                                ? <span>{ actionFlowData.errorMessage }</span>
                                : ''
                            }
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    private getInitState(props: AllProps) {
        const stateObj = {} as OwnState;
        const currentProcessFlowId = props.match.params.id;

        if (currentProcessFlowId) {
            stateObj.processFlowId = currentProcessFlowId;
        } else {
            this.props.history.push(AppRoutes.homeRoute);
        }

        return stateObj;
    }

    private successOrFailedStateNotification(message: string, isSuccess: boolean) {
        if (isSuccess) {
            this.props.floatMessage.show(MessageType.Success, message, 3000);
        } else {
            this.props.floatMessage.show(MessageType.Error, message, 3000);
        }
    }

    private redirectToProcessFlowsPage() {
        this.props.history.push(AppRoutes.administration.processFlowRoute);
    }

    private defaultInitDateReduxForm(): ProcessFlowDetailsDataModel | undefined {
        return {
            processFlowId: '',
            comment: '',
            creationDateTime: new Date(),
            processedObjectName: '',
            processFlowName: '',
            status: StateMachineComponentStatus.Any,
            stateDescription: '',
            actionFlows: []
        };
    }

    public render() {
        const formFields = this.props.reduxForm.getReduxFormFields(false);

        return (
            <>
                <PageHeaderView title="Детали рабочего процесса" />
                <Box display="flex" flexDirection="column">
                    <div>
                        <p><b>Информация</b></p>
                    </div>
                    <ReadOnlyKeyValueView
                        label="Название процесса:"
                        text={ formFields.processFlowName }
                    />
                    <ReadOnlyKeyValueView
                        label="Имя обрабатываемого процессом объекта:"
                        text={ formFields.processedObjectName }
                    />
                    <ReadOnlyKeyValueView
                        label="Дата создания процесса:"
                        text={ DateUtility.dateToDayMonthYearHourMinute(formFields.creationDateTime) }
                    />
                    <ReadOnlyKeyValueView
                        label="Состояние:"
                        text={ formFields.stateDescription }
                    />
                    <ReadOnlyKeyValueView
                        label="Статус:"
                        text={ this.getStateMachineComponentStatusLabel(formFields.status) }
                    />
                    <ReadOnlyKeyValueView
                        label="Коментарий:"
                        text={ formFields.comment }
                    />
                </Box>
                <Box display="flex" flexDirection="column">
                    <div>
                        <p><b>План выполнения</b></p>
                    </div>
                    <div className={ css.profiletimeline }>
                        { formFields.actionFlows.map((element, index) => {
                            return this.getActionFlowDetailsContainer(element, index);
                        }) }
                    </div>
                </Box>
                <Box display="flex" flexDirection="row" justifyContent="flex-end" gap={ 2 }>
                    <ContainedButton onClick={ this.redirectToProcessFlowsPage }>
                        Назад
                    </ContainedButton>
                    { formFields.status === StateMachineComponentStatus.Error &&
                        <SuccessButton
                            onClick={ this.onRetryProcessFlowClick }
                        >
                            <i className="fa fa-recycle mr-1" />
                            Перезапустить
                        </SuccessButton>
                    }
                </Box>
                <WatchReducerProcessActionState
                    watch={ [{
                        reducerStateName: 'ProcessFlowState',
                        processIds: [
                            ProcessFlowProcessId.ReceiveProcessFlowDetails,
                            ProcessFlowProcessId.RetryProcessFlow
                        ]
                    }] }
                    onProcessActionStateChanged={ this.onProcessActionStateChanged }
                />
            </>
        );
    }
}

export const ProcessFlowDetailsViewConnected = connect<StateProps, DispatchProps, OwnProps, AppReduxStoreState>(
    state => {
        const processFlowState = state.ProcessFlowState;

        return {
            processFlowDetails: processFlowState.processFlowDetails,
        };
    },
    {
        dispatchGetProcessFlowDetailsByIdThunk: ReceiveProcessFlowDetailsThunk.invoke,
        dispatchRetryProcessFlowThunk: RetryProcessFlowThunk.invoke,
    }
)(ProcessFlowDetailsViewClass);

export const ProcessFlowDetailsView = withReduxForm(withFloatMessages(ProcessFlowDetailsViewConnected),
    {
        reduxFormName: 'ProcessFlow_Details_Form',
        resetOnUnmount: true
    });