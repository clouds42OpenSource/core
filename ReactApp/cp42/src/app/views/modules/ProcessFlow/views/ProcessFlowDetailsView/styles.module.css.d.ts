declare const styles: {
  readonly 'profiletimeline': string;
  readonly 'sl-item': string;
  readonly 'sl-left': string;
  readonly 'sl-right': string;
  readonly 'steamline': string;
  readonly 'rounded-circle': string;
  readonly 'sl-date': string;
  readonly 'sl-number-container': string;
  readonly 'sl-thumbnail': string;
  readonly 'sl-title': string;
};
export = styles;