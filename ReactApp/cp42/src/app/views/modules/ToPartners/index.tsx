import { useTour } from '@reactour/tour';
import { useEffect, useState } from 'react';
import { useHistory } from 'react-router';

import { AgencyAgreementDialog } from 'app/views/modules/_common/shared/AgencyAgreementDialog';
import { REDUX_API } from 'app/api/useReduxApi';
import { useAppDispatch, useAppSelector } from 'app/hooks';
import { withHeader } from 'app/views/components/_hoc/withHeader';
import { TabsControl } from 'app/views/components/controls/TabsControl';
import { THeaders } from 'app/views/components/controls/TabsControl/views/TabView';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { EPartnersSteps, EPartnersTourId, ERefreshId } from 'app/views/Layout/ProjectTour/enums';

import { InfoCardContainer } from './modules';
import { TPartnersHistory } from './config/types';
import { MyClients, MyServices, Requisites, Transactions, Withdrawing } from './views';

export * from '../_common/shared/PartnersPages/vipAccountValue';

const HEADERS: THeaders = [
    {
        title: 'Мои клиенты',
        isVisible: true,
    },
    {
        title: 'Мои сервисы',
        isVisible: true,
        tourId: EPartnersTourId.servicesTab
    },
    {
        title: 'Транзакции',
        isVisible: true,
        tourId: EPartnersTourId.transactionsTab
    },
    {
        title: 'Реквизиты',
        isVisible: true,
        tourId: EPartnersTourId.requisitesTab
    },
    {
        title: 'Вывод',
        isVisible: true,
        tourId: EPartnersTourId.withdrawalTab
    }
];

const ToPartners = () => {
    const { meta, isOpen: isTourOpen, currentStep, setCurrentStep } = useTour();
    const history = useHistory<TPartnersHistory | undefined>();
    const dispatch = useAppDispatch();
    const { data, isLoading } = useAppSelector(state => state.PartnersState.agencyAgreementStatusReducer);

    const [isOpen, setIsOpen] = useState(false);

    useEffect(() => {
        REDUX_API.PARTNERS_REDUX.getAgencyAgreementStatus(dispatch);
    }, [dispatch]);

    useEffect(() => {
        if (data) {
            setIsOpen(data.rawData);
        }
    }, [data]);

    const onActiveTabIndexChangedHandler = (index: number) => {
        const isWithdrawalTab = currentStep === EPartnersSteps.withdrawalTab && index === 4;
        const isServicesTab = currentStep === EPartnersSteps.servicesTab && index === 1;

        if (meta === 'partners' && isTourOpen && (isWithdrawalTab || isServicesTab)) {
            setCurrentStep(prev => prev + 1);
        }
    };

    return (
        <>
            { isLoading && <LoadingBounce /> }
            <AgencyAgreementDialog isOpen={ isOpen } setIsOpen={ setIsOpen } />
            <TabsControl
                onActiveTabIndexChanged={ onActiveTabIndexChangedHandler }
                headers={ HEADERS }
                headerRefreshId={ ERefreshId.partnersCardInfo }
                headerContent={ <InfoCardContainer /> }
                activeTabIndex={ history.location.state?.activeTabIndex }
            >
                <MyClients />
                <MyServices />
                <Transactions />
                <Requisites />
                <Withdrawing />
            </TabsControl>
        </>
    );
};

export const ToPartnersView = withHeader({
    title: 'Партнёрам',
    page: ToPartners
});