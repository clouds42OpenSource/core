import { REDUX_API } from 'app/api/useReduxApi';
import { setCashFormat } from 'app/common/helpers/setCashFormat';

import { useAppDispatch, useAppSelector } from 'app/hooks';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { EPartnersTourId, ERefreshId } from 'app/views/Layout/ProjectTour/enums';
import { useEffect } from 'react';
import { InfoCard } from '../../components/index';
import { getHref } from '../../helpers';

import styles from './style.module.css';

export const InfoCardContainer = withFloatMessages(({ floatMessage: { show } }: FloatMessageProps) => {
    const dispatch = useAppDispatch();
    const { summaryDataReducer: { data, error, isLoading }, agencyAgreementStatusReducer: { data: agencyAgreementStatus } } = useAppSelector(state => state.PartnersState);

    useEffect(() => {
        void REDUX_API.PARTNERS_REDUX.getSummaryData(dispatch);
    }, [dispatch]);

    if (error) {
        show(MessageType.Error, error);
    }

    const { totalEarned, availableToPay, currencySymbol, monthlyCharge, referrerLink } = data?.rawData || {};

    return (
        <div className={ styles.container } data-refreshid={ ERefreshId.partnersCard }>
            <InfoCard
                isLoading={ isLoading }
                // refreshId={ ERefreshId.partnersCard }
                tourId={ EPartnersTourId.referral }
                image={ getHref('businessman') }
                isLink={ true }
                title="Реф. ссылка"
                description={ referrerLink ?? '' }
                isHidden={ agencyAgreementStatus?.rawData ?? true }
            />
            <InfoCard
                isLoading={ isLoading }
                tourId={ EPartnersTourId.currentSum }
                image={ getHref('piggybank') }
                title="Всего заработано"
                description={ `${ setCashFormat((totalEarned ?? 0) + 0.0000001) } ${ currencySymbol }` }
            />
            <InfoCard
                isLoading={ isLoading }
                image={ getHref('growth') }
                title="Уровень дохода"
                description={ `${ monthlyCharge ? '' : '~' }${ setCashFormat(monthlyCharge ?? availableToPay ?? 0) } ${ currencySymbol }/мес` }
            />
            <InfoCard
                isLoading={ isLoading }
                tourId={ EPartnersTourId.cashOut }
                image={ getHref('money') }
                title="Доступно к выплате"
                description={ `${ setCashFormat(availableToPay ?? 0) } ${ currencySymbol }` }
            />
        </div>
    );
});