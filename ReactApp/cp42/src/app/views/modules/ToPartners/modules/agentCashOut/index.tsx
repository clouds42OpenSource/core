import { MenuItem, Select, SelectChangeEvent, TextField } from '@mui/material';
import { contextAccountId, userId } from 'app/api';
import { EAgentRequisiteFileType, EAgentRequisitesType, ERequestStatus } from 'app/api/endpoints/partners/enum';
import { TAgentCashOutCreateRequest } from 'app/api/endpoints/partners/request';
import { TAgentCashOutEditResponse, TAgentRequisite } from 'app/api/endpoints/partners/response';
import { TAgentRequisiteFile } from 'app/api/endpoints/partners/type';
import { FETCH_API } from 'app/api/useFetchApi';
import { REDUX_API } from 'app/api/useReduxApi';
import { AppRoutes } from 'app/AppRoutes';
import { useAppDispatch, useAppSelector } from 'app/hooks';
import { useQuery } from 'app/hooks/useQuery';
import { getFileFromBase64 } from 'app/utils';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';

import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { ContainedButton, OutlinedButton, SuccessButton } from 'app/views/components/controls/Button';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { TextOut } from 'app/views/components/TextOut';
import { ConfigFileUploader } from 'app/views/modules/_common/components/ConfigFileUploader';
import { objectToFormData } from 'app/web/common';
import { WebVerb } from 'core/requestSender/enums';
import React, { useEffect, useMemo, useState } from 'react';
import { useHistory } from 'react-router';
import { TPartnersHistory } from '../../config/types';

import styles from './style.module.css';

type TAgentCashOutComponent = FloatMessageProps & {
    isDisabled?: boolean;
    dataResponse?: TAgentCashOutEditResponse;
    isEditMode?: boolean;
    cashOutRequestId?: string;
};

const { getAgentContracts, getCashOutBalance, createAgentReportFile, createAgentCashOut, downloadAgentReportFile } = FETCH_API.PARTNERS;
const { postCashOutToBalance } = FETCH_API.ARTICLES;
const { sendEmail } = FETCH_API.EMAILS;

export const AgentCashOutComponent = withFloatMessages(({ floatMessage: { show }, isDisabled, dataResponse, isEditMode, cashOutRequestId }: TAgentCashOutComponent) => {
    const history = useHistory<TPartnersHistory>();
    const dispatch = useAppDispatch();
    const query = useQuery();

    const currencySymbol = useAppSelector(state => state.PartnersState.summaryDataReducer.data?.rawData?.currencySymbol);
    const availableSum = useAppSelector(state => state.ArticlesState.getArticlesSummaryInfoReducer.data?.rawData.availableSum);
    const currentUser = useAppSelector(state => state.MainPageState.mainPageReducer);

    const [isLoading, setIsLoading] = useState(false);
    const [report, setReport] = useState<File | null>(null);
    const [requisitesList, setRequisitesList] = useState<TAgentRequisite[]>([]);
    const [sum, setSum] = useState(0);
    const [requisite, setRequisite] = useState<TAgentRequisite>();
    const [reportFileData, setReportFileData] = useState<TAgentRequisiteFile>();

    const forArticles = useMemo(() => query.has('forArticles'), [query]);

    const isNeedToPayTax = requisite && requisite.agentRequisitesType === EAgentRequisitesType.physicalPersonRequisites;

    const onCancelHandler = () => {
        if (forArticles) {
            history.push(AppRoutes.articlesReference, { activeTabIndex: 3 });
        } else {
            history.push(AppRoutes.partners.toPartners, { activeTabIndex: 4 });
        }
    };

    useEffect(() => {
        if (forArticles && availableSum === undefined) {
            void FETCH_API.ARTICLES.getSummaryInfo(dispatch);
        }
    }, [availableSum, dispatch, forArticles]);

    useEffect(() => {
        if (!currencySymbol || currencySymbol.length === 0) {
            void REDUX_API.PARTNERS_REDUX.getSummaryData(dispatch);
        }
    }, [currencySymbol, dispatch]);

    useEffect(() => {
        (async () => {
            setIsLoading(true);

            const { data, error } = await getAgentContracts();

            if (error) {
                show(MessageType.Error, error);
            } else if (data) {
                setRequisitesList(data.rawData ?? []);

                if (!isEditMode && data.rawData) {
                    setRequisite(data.rawData[0]);
                }
            }

            if (dataResponse && isEditMode) {
                setSum(dataResponse.totalSum);
                setRequisite(data?.rawData?.find(req => req.id === dataResponse.agentRequisitesId));

                if (dataResponse.files.length > 0) {
                    const { content, contentType, fileName } = dataResponse.files[0];

                    setReport(getFileFromBase64(content as string ?? '', fileName, contentType));
                    setReportFileData(dataResponse.files[0]);
                }
            }

            setIsLoading(false);
        })();
    }, [dataResponse, isEditMode, show]);

    useEffect(() => {
        (async () => {
            if (!isEditMode) {
                let currentSum: number;

                if (forArticles) {
                    currentSum = availableSum ?? 0;
                } else {
                    const { data: cashOutData, error: cashOutError } = await getCashOutBalance();

                    if (cashOutError) {
                        show(MessageType.Error, cashOutError);
                    }

                    currentSum = cashOutData?.rawData ?? 0;
                }

                setSum(currentSum);
            }
        })();
    }, [availableSum, forArticles, isEditMode, show]);

    const onCreateHandler = async () => {
        setIsLoading(true);
        const needReport = !forArticles && !!report;

        if (requisite) {
            if (!forArticles) {
                if (!report) {
                    return show(MessageType.Warning, 'Прикрепите скан подписанного отчета');
                }

                const requestBody: TAgentCashOutCreateRequest = {
                    accountId: contextAccountId(),
                    agentRequisitesId: requisite.id,
                    id: cashOutRequestId,
                    requestStatus: ERequestStatus.new,
                    totalSum: sum,
                    paySum: isNeedToPayTax ? Math.ceil(sum - sum * 0.13) : sum,
                    isEditMode: true,
                    files: []
                };

                if (needReport) {
                    if (report.size / (1024 ** 2) > 14) {
                        return show(MessageType.Warning, 'Размер загружаемых файлов оказался слишком большим. Общее ограничение размера файлов составляет 14 мегабайт');
                    }

                    requestBody.files.push({
                        fileName: reportFileData?.fileName ?? report.name,
                        contentType: reportFileData?.contentType ?? report.type,
                        agentRequisitesFileType: EAgentRequisiteFileType.scanCopyContract,
                        cloudFileId: reportFileData?.cloudFileId
                    });
                }

                const requestBodyForm = objectToFormData(requestBody);

                if (needReport) {
                    requestBodyForm.append('files[0].content', report);
                }
                const { error } = await createAgentCashOut(requestBodyForm, isEditMode ? WebVerb.PUT : WebVerb.POST);

                if (error && error !== 'Успешно!') {
                    show(MessageType.Error, error);
                } else {
                    show(MessageType.Success, `Заявка на выплату успешно ${ isEditMode ? 'отредактирована' : 'сохранена' }`);

                    onCancelHandler();
                }
            } else {
                const { success, data: applicationNumber, message } = await postCashOutToBalance({
                    accountUserId: userId(),
                    agentRequisitesId: requisite.id,
                    requestStatus: ERequestStatus.new,
                    totalSum: sum,
                    paySum: isNeedToPayTax ? Math.ceil(sum - sum * 0.13) : sum,
                    isEditMode: true
                });

                if (success) {
                    show(MessageType.Success, `Заявка на выплату успешно ${ isEditMode ? 'отредактирована' : 'сохранена' }`);

                    const author = currentUser.data?.login ?? userId();

                    void sendEmail({
                        body: `Номер заявки: ${ applicationNumber }, Автор статьи: ${ author }, Сумма к выплате: ${ sum }`,
                        subject: 'Создана заявка на вывод денежных средств',
                        header: 'Создана заявка на вывод денежных средств',
                        categories: 'articles',
                        reference: import.meta.env.REACT_SUPPORT_EMAIL ?? ''
                    });

                    onCancelHandler();
                } else {
                    show(MessageType.Error, message);
                }
            }
        } else {
            show(MessageType.Warning, 'Выберите реквизиты, по которым будет произведена выплата');
        }

        setIsLoading(false);
    };

    const openReportHandler = async () => {
        setIsLoading(true);

        if (isEditMode && cashOutRequestId && dataResponse) {
            const { responseBody } = await downloadAgentReportFile({
                agentDocumentFileId: dataResponse.files[0].cloudFileId ?? ''
            });

            window.open(URL.createObjectURL(responseBody));
        } else {
            const { responseBody } = await createAgentReportFile({
                accountId: contextAccountId(),
                agentRequisitesId: requisite?.id ?? ''
            });

            window.open(URL.createObjectURL(responseBody));
        }

        setIsLoading(false);
    };

    const reportHandler = (file: File) => {
        setReport(file);
    };

    const deleteReportHandler = () => {
        setReport(null);

        if (reportFileData) {
            setReportFileData(undefined);
        }
    };

    const selectRequisiteHandler = (ev: SelectChangeEvent) => {
        setRequisite(requisitesList.find(req => req.id === ev.target.value));
    };

    const onTypeError = () => {
        show(MessageType.Error, 'Неверный формат файла');
    };

    return (
        <>
            { isLoading && <LoadingBounce /> }
            <div className={ styles.container }>
                <FormAndLabel label="Выберите реквизиты, по которым будет произведена выплата">
                    <Select fullWidth={ true } onChange={ selectRequisiteHandler } value={ requisite?.id ?? '' } disabled={ isDisabled }>
                        { requisitesList.flatMap(requisiteItem => {
                            if (!forArticles || (forArticles && requisiteItem.agentRequisitesType !== EAgentRequisitesType.legalPersonRequisites)) {
                                return (
                                    <MenuItem value={ requisiteItem.id } key={ requisiteItem.id }>
                                        { `${ requisiteItem.agentRequisitesTypeString } №${ requisiteItem.number }` }
                                    </MenuItem>
                                );
                            }

                            return [];
                        }) }
                    </Select>
                </FormAndLabel>
                <FormAndLabel label={ `Сумма к выводу, ${ currencySymbol ?? '' }.` }>
                    <TextField
                        value={ sum.toFixed(2) }
                        disabled={ true }
                        fullWidth={ true }
                    />
                    { isNeedToPayTax &&
                        <TextOut fontSize={ 11 }>
                            Будет удержано НДФЛ (13%): { `${ Math.ceil(sum * 0.13) } ${ currencySymbol ?? '' }` }
                        </TextOut>
                    }
                </FormAndLabel>
                <FormAndLabel label={ `Сумма к выплате, ${ currencySymbol ?? '' }.` }>
                    <TextField
                        value={ isNeedToPayTax ? Math.ceil(sum - sum * 0.13) : sum }
                        disabled={ true }
                        fullWidth={ true }
                    />
                </FormAndLabel>
                { !forArticles && (
                    <>
                        <FormAndLabel label="Распечатайте отчет">
                            <div className={ styles.elementContainer }>
                                <TextOut fontSize={ 11 }>
                                    Необходимо распечатать отчет, подписать его и прикрепить скан копию в поле &quot;Скан подписанного отчета&quot;.
                                </TextOut>
                                <OutlinedButton className={ styles.button } onClick={ openReportHandler } isEnabled={ !isDisabled }>
                                    <i className="fa fa-print" aria-hidden="true" />
                                    Распечатать отчет
                                </OutlinedButton>
                            </div>
                        </FormAndLabel>
                        <FormAndLabel label="Прикрепите скан подписанного отчета в формате .pdf:">
                            <ConfigFileUploader
                                disabled={ isDisabled }
                                limit={ 1 }
                                required={ true }
                                callBack={ reportHandler }
                                openFile={ openReportHandler }
                                deleteUploadFile={ deleteReportHandler }
                                uploadedFiles={ report && [report] }
                                fileTypesArr={ ['pdf'] }
                                onTypeError={ onTypeError }
                            />
                        </FormAndLabel>
                    </>
                ) }
                <TextOut fontSize={ 11 }>
                    Сроки выполнения заявки: до 5 рабочих дней
                </TextOut>
                <div className={ styles.buttonsContainer }>
                    <ContainedButton onClick={ onCancelHandler }>Назад</ContainedButton>
                    { !isDisabled &&
                        <SuccessButton onClick={ onCreateHandler }>{ isEditMode ? 'Сохранить изменения' : 'Создать' }</SuccessButton>
                    }
                </div>
            </div>
        </>
    );
});