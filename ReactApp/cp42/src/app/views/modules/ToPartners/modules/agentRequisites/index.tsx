import { TArticlesProps } from 'app/views/modules/Articles/types/TArticlesHistory';
import React, { useEffect, useState } from 'react';
import { useLocation } from 'react-router';
import { Divider, MenuItem, Select, SelectChangeEvent } from '@mui/material';

import { FETCH_API } from 'app/api/useFetchApi';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { TAgentRequisiteResponse } from 'app/api/endpoints/partners/response';
import { TData } from 'app/api/types';
import { AppRoutes } from 'app/AppRoutes';
import { EAgentRequisitesStatus, EAgentRequisitesType } from 'app/api/endpoints/partners/enum';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { TextOut } from 'app/views/components/TextOut';
import { EntrepreneurRequisiteForm, LegalPersonRequisiteForm, PhysicalPersonRequisiteForm } from '../../components/requisiteForms';

import styles from './style.module.css';

type TAgentRequisitesComponent = FloatMessageProps & {
    sendRequisiteStatus?: React.Dispatch<EAgentRequisitesStatus>;
};

export const AgentRequisitesComponent = withFloatMessages(({ floatMessage: { show }, sendRequisiteStatus }: TAgentRequisitesComponent) => {
    const location = useLocation<TArticlesProps | undefined>();

    const [requisiteType, setRequisiteType] = useState<EAgentRequisitesType>(EAgentRequisitesType.physicalPersonRequisites);
    const [info, setInfo] = useState<TData<TAgentRequisiteResponse | null>>();
    const [isLoading, setIsLoading] = useState(false);

    const isDisabled = info?.rawData?.agentRequisitesStatus === EAgentRequisitesStatus.verified || info?.rawData?.agentRequisitesStatus === EAgentRequisitesStatus.onCheck;
    const agentRequisitesId = new URLSearchParams(location.search).get('agentRequisitesId');
    const isEditMode = !!(agentRequisitesId && window.location.href.includes(AppRoutes.partners.editAgentRequisites));

    const requisiteTypeHandler = (ev: SelectChangeEvent<EAgentRequisitesType>) => {
        setRequisiteType(ev.target.value as EAgentRequisitesType);
    };

    useEffect(() => {
        (async () => {
            if (isEditMode && !info) {
                setIsLoading(true);
                setRequisiteType(EAgentRequisitesType.undefined);
                const { data, error } = await FETCH_API.PARTNERS.getAgentRequisite({ agentRequisitesId });

                if (error) {
                    show(MessageType.Error, error);
                } else if (data) {
                    setInfo(data);
                    setRequisiteType(data.rawData?.agentRequisitesType ?? EAgentRequisitesType.physicalPersonRequisites);

                    if (sendRequisiteStatus) {
                        sendRequisiteStatus(data.rawData?.agentRequisitesStatus ?? EAgentRequisitesStatus.undefined);
                    }
                }

                setIsLoading(false);
            }
        })();
    }, [agentRequisitesId, info, isEditMode, sendRequisiteStatus, show]);

    return (
        <>
            { isLoading && <LoadingBounce /> }
            <div className={ styles.container }>
                <div className={ styles.header }>
                    <div className={ styles.info }>
                        <span className={ styles.text }>Выберите тип лица</span>
                        <TextOut fontSize={ 12 }>
                            Важно! В зависимости от лица, на реквизиты которого будет выплачиваться вознаграждение, будет удержан налог:
                        </TextOut>
                        <TextOut fontSize={ 12 }>
                            - для физ.лица 13% НДФЛ;
                        </TextOut>
                        <TextOut fontSize={ 12 }>
                            - для ИП, для юр.лица 0%;
                        </TextOut>
                    </div>
                    <Select
                        value={ requisiteType }
                        onChange={ requisiteTypeHandler }
                        disabled={ isDisabled }
                    >
                        <MenuItem value={ EAgentRequisitesType.undefined } className={ styles.hidden } />
                        <MenuItem value={ EAgentRequisitesType.physicalPersonRequisites }>Реквизиты физ. лица</MenuItem>
                        <MenuItem value={ EAgentRequisitesType.legalPersonRequisites } disabled={ location.state?.forArticles }>Реквизиты юр. лица</MenuItem>
                        <MenuItem value={ EAgentRequisitesType.soleProprietorRequisites }>Реквизиты (ИП)</MenuItem>
                    </Select>
                    <Divider />
                </div>
                { requisiteType === EAgentRequisitesType.physicalPersonRequisites &&
                    <PhysicalPersonRequisiteForm
                        formData={
                            info?.rawData?.agentRequisitesType === EAgentRequisitesType.physicalPersonRequisites && info.rawData.physicalPersonRequisites
                                ? info?.rawData?.physicalPersonRequisites
                                : undefined
                        }
                        isDisabled={ isDisabled }
                        agentRequisitesId={ agentRequisitesId ?? undefined }
                    />
                }
                { requisiteType === EAgentRequisitesType.legalPersonRequisites &&
                    <LegalPersonRequisiteForm
                        formData={
                            info?.rawData?.agentRequisitesType === EAgentRequisitesType.legalPersonRequisites && info.rawData.legalPersonRequisites
                                ? info?.rawData?.legalPersonRequisites
                                : undefined
                        }
                        isDisabled={ isDisabled }
                        agentRequisitesId={ agentRequisitesId ?? undefined }
                    />
                }
                { requisiteType === EAgentRequisitesType.soleProprietorRequisites &&
                    <EntrepreneurRequisiteForm
                        formData={
                            info?.rawData?.agentRequisitesType === EAgentRequisitesType.soleProprietorRequisites && info.rawData.soleProprietorRequisites
                                ? info?.rawData?.soleProprietorRequisites
                                : undefined
                        }
                        isDisabled={ isDisabled }
                        agentRequisitesId={ agentRequisitesId ?? undefined }
                    />
                }
            </div>
        </>
    );
});