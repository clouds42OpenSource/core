import { Box } from '@mui/material';
import { contextAccountId } from 'app/api';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import dayjs from 'dayjs';
import React, { memo, useState } from 'react';

import { TDatePicker } from 'app/common/types/app-types';
import { Dialog } from 'app/views/components/controls/Dialog';
import { DoubleDateInputForm } from 'app/views/components/controls/forms/DoubleDateInputView';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { FETCH_API } from 'app/api/useFetchApi';
import { EMessageType, useFloatMessages } from 'app/hooks';
import { TextOut } from 'app/views/components/TextOut';

import globalStyles from '../../views/style.module.css';

type TProps = {
    isOpen: boolean;
    setIsOpen: React.Dispatch<React.SetStateAction<boolean>>;
};

const defaultFrom = dayjs().add(-3, 'month');

const { PARTNERS: { getSheetId }, PROFILE: { useGetProfile } } = FETCH_API;

export const GoogleSheetDialog = memo(({ isOpen, setIsOpen }: TProps) => {
    const { show } = useFloatMessages();

    const { data: profileData, error: profileMessage } = useGetProfile();

    const [period, setPeriod] = useState<TDatePicker>({ from: defaultFrom.toDate(), to: dayjs().toDate() });
    const [isLoading, setIsLoading] = useState(false);

    const onCancelClick = () => {
        setIsOpen(false);
    };

    const periodHandler = (formName: string, newValue: Date) => {
        if (formName === 'from') {
            setPeriod({ ...period, from: newValue });
        } else if (formName === 'to') {
            setPeriod({ ...period, to: newValue });
        }
    };

    const openSheetHandler = async () => {
        setIsLoading(true);

        if (profileData) {
            const from = dayjs(period.from ?? defaultFrom).format('YYYY-MM-DD');
            const to = dayjs(period.to).format('YYYY-MM-DD');
            const { data: sheetId, success, message } = await getSheetId({
                periodFrom: from,
                periodTo: to,
                userEmail: profileData.rawData.email,
                pageNumber: 0,
                pageSize: 0,
                accountId: contextAccountId()
            });

            if (success && sheetId) {
                window.open(`https://docs.google.com/spreadsheets/d/${ sheetId }`, '_blank');
            } else {
                show(EMessageType.error, message ?? 'Не удалось получить ссылку на таблицу');
            }
        } else {
            show(EMessageType.error, profileMessage ?? 'Не удалось получить почту пользователя для выдачи доступа к таблице');
        }

        setIsLoading(false);
        onCancelClick();
    };

    return (
        <>
            { isLoading && <LoadingBounce /> }
            <Dialog
                isOpen={ isOpen }
                dialogWidth="xs"
                title="Выгрузка"
                isTitleSmall={ true }
                maxHeight="320px"
                onCancelClick={ onCancelClick }
                buttons={ [
                    {
                        content: 'Закрыть',
                        onClick: onCancelClick,
                        variant: 'contained',
                        kind: 'default'
                    },
                    {
                        content: 'Подтвердить',
                        onClick: openSheetHandler,
                        variant: 'contained',
                        kind: 'success'
                    }
                ] }
            >
                <Box display="flex" flexDirection="column" gap="5px">
                    <FormAndLabel label="Период подключения клиента" className={ globalStyles.formDatePicker }>
                        <DoubleDateInputForm
                            isNeedToStretch={ true }
                            periodFromValue={ period.from }
                            periodToValue={ period.to }
                            periodFromInputName="from"
                            periodToInputName="to"
                            onValueChange={ periodHandler }
                        />
                    </FormAndLabel>
                    <TextOut textAlign="center" fontSize={ 11 }>
                        Выгрузка будет готова в течение нескольких минут после нажатия кнопки &quot;Подтвердить&quot;. Пожалуйста, подождите.
                    </TextOut>
                </Box>
            </Dialog>
        </>
    );
});