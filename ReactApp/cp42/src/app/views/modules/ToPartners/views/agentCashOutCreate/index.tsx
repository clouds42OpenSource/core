import { withHeader } from 'app/views/components/_hoc/withHeader';
import { AgentCashOutComponent } from '../../modules/agentCashOut';

export const AgentCashOutCreate = withHeader({
    page: AgentCashOutComponent,
    title: 'Создание заявки на выплату'
});