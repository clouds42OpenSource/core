import { TArticlesProps } from 'app/views/modules/Articles/types/TArticlesHistory';
import React, { useState } from 'react';
import { useHistory, useLocation } from 'react-router';

import { withHeader } from 'app/views/components/_hoc/withHeader';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { TabsControl } from 'app/views/components/controls/TabsControl';
import { THeaders } from 'app/views/components/controls/TabsControl/views/TabView';
import { useAppSelector } from 'app/hooks';
import { AppRoutes } from 'app/AppRoutes';
import { AccountUserGroup } from 'app/common/enums';
import { FETCH_API } from 'app/api/useFetchApi';
import { EAgentRequisitesStatus } from 'app/api/endpoints/partners/enum';
import { MenuItem, Select, SelectChangeEvent } from '@mui/material';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { ContainedButton, SuccessButton } from 'app/views/components/controls/Button';
import { AgentRequisitesComponent } from '../../modules';
import { TPartnersHistory } from '../../config/types';

import styles from './style.module.css';

export const EditAgentRequisitesComponent = withFloatMessages(({ floatMessage: { show } }) => {
    const history = useHistory<TPartnersHistory & TArticlesProps | undefined>();
    const location = useLocation();
    const { currentUserGroups } = useAppSelector(state => state.Global.getCurrentSessionSettingsReducer.settings.currentContextInfo);

    const [status, setStatus] = useState<EAgentRequisitesStatus>(EAgentRequisitesStatus.undefined);

    const HEADERS: THeaders = [
        {
            title: 'Общее',
            isVisible: true,
        },
        {
            title: 'Управление',
            isVisible: (currentUserGroups.includes(AccountUserGroup.CloudAdmin) || currentUserGroups.includes(AccountUserGroup.AccountSaleManager)) &&
                status !== EAgentRequisitesStatus.undefined
        }
    ];

    const onCancelHandler = () => {
        if (history.location.state?.forArticles) {
            history.push(AppRoutes.articlesReference, { activeTabIndex: 2 });
        } else {
            history.push(AppRoutes.partners.toPartners, { activeTabIndex: 3 });
        }
    };

    const requisiteStatusHandler = (ev: SelectChangeEvent<EAgentRequisitesStatus>) => {
        setStatus(ev.target.value as EAgentRequisitesStatus);
    };

    const saveStatusHandler = async () => {
        const { error } = await FETCH_API.PARTNERS.changeAgentRequisiteStatus(status, new URLSearchParams(location.search).get('agentRequisitesId') ?? '');

        if (error && error !== 'Успешно!') {
            show(MessageType.Error, error);
        } else {
            onCancelHandler();
        }
    };

    return (
        <TabsControl headers={ HEADERS }>
            <AgentRequisitesComponent sendRequisiteStatus={ setStatus } />
            <div className={ styles.container }>
                <Select value={ status } fullWidth={ true } onChange={ requisiteStatusHandler }>
                    <MenuItem value={ EAgentRequisitesStatus.undefined } className={ styles.hidden } />
                    <MenuItem value={ EAgentRequisitesStatus.draft }>Черновик</MenuItem>
                    <MenuItem value={ EAgentRequisitesStatus.onCheck }>На проверке</MenuItem>
                    <MenuItem value={ EAgentRequisitesStatus.verified }>Проверено</MenuItem>
                </Select>
                <div className={ styles.buttons }>
                    <ContainedButton onClick={ onCancelHandler }>Назад</ContainedButton>
                    <SuccessButton onClick={ saveStatusHandler }>Сохранить изменения</SuccessButton>
                </div>
            </div>
        </TabsControl>
    );
});

export const AgentRequisitesEdit = withHeader({
    title: 'Редактирование реквизитов',
    page: EditAgentRequisitesComponent
});