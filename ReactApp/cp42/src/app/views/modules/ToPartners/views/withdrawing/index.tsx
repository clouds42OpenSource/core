import { Chip, MenuItem, Pagination, Select, SelectChangeEvent } from '@mui/material';
import Tooltip from '@mui/material/Tooltip';

import { contextAccountId, userId } from 'app/api';
import { EAgentRequisitesType, ERequestStatus } from 'app/api/endpoints/partners/enum';
import { TAgentCashOutRequest } from 'app/api/endpoints/partners/request';
import { FETCH_API } from 'app/api/useFetchApi';
import { REDUX_API } from 'app/api/useReduxApi';
import { AppRoutes } from 'app/AppRoutes';
import { AccountUserGroup } from 'app/common/enums';
import { setCashFormat } from 'app/common/helpers/setCashFormat';
import { EMessageType, useAppDispatch, useAppSelector, useFloatMessages } from 'app/hooks';
import { OutlinedButton, SuccessButton } from 'app/views/components/controls/Button';
import { DoubleDateInputForm } from 'app/views/components/controls/forms/DoubleDateInputView';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { EPartnersTourId } from 'app/views/Layout/ProjectTour/enums';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { TArticlesProps } from 'app/views/modules/Articles/types/TArticlesHistory';
import dayjs from 'dayjs';
import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import { BalanceTransferDialog } from '../../components';
import { TDatePicker } from '../../config/types';

import globalStyles from '../style.module.css';
import styles from './style.module.css';

export const Withdrawing = ({ isOtherAccount, contextUserId, forArticles }: TArticlesProps) => {
    const dispatch = useAppDispatch();
    const history = useHistory();
    const { show } = useFloatMessages();

    const {
        PartnersState: {
            agentCashOutRequestListReducer: { data, error, isLoading },
            summaryDataReducer: { data: summaryData },
            agencyAgreementStatusReducer: { data: agencyAgreementStatus }
        },
        Global: {
            getCurrentSessionSettingsReducer: {
                settings: { currentContextInfo: { currentUserGroups } }
            }
        },
    } = useAppSelector(state => state);

    const [isOpenDialog, setIsOpenDialog] = useState(false);
    const [agentRequisiteType, setAgentRequisiteType] = useState<EAgentRequisitesType>(EAgentRequisitesType.undefined);
    const [status, setStatus] = useState<ERequestStatus>(ERequestStatus.undefined);
    const [period, setPeriod] = useState<TDatePicker>({ from: null, to: null });
    const [isInnerLoading, setIsInnerLoading] = useState(false);

    useEffect(() => {
        if (error) {
            show(EMessageType.error, error, 1500);
        }
    }, [error, show]);

    useEffect(() => {
        if (!isOtherAccount) {
            REDUX_API.PARTNERS_REDUX.getAgentCashOut({
                accountUserId: forArticles ? userId() : undefined,
                accountId: contextAccountId(),
                pageNumber: 1
            }, dispatch);
        } if (contextUserId) {
            REDUX_API.PARTNERS_REDUX.getAgentCashOut({
                accountUserId: forArticles ? contextUserId : undefined,
                accountId: contextAccountId(),
                pageNumber: 1
            }, dispatch);
        }
    }, [contextUserId, dispatch, forArticles, isOtherAccount]);

    const periodHandler = (formName: string, newValue: Date) => {
        if (formName === 'from') {
            setPeriod({ ...period, from: newValue });
        } else if (formName === 'to') {
            setPeriod({ ...period, to: newValue });
        }
    };

    const checkMoneyOperationPossibility = async () => {
        setIsInnerLoading(true);

        if (forArticles) {
            const { message } = await FETCH_API.ARTICLES.getCashOutToBalance();
            setIsInnerLoading(false);

            return { possibility: !message, text: message };
        }

        REDUX_API.PARTNERS_REDUX.getAgencyAgreementStatus(dispatch);
        const { data: checkMoneyData, error: checkMoneyError } = await FETCH_API.PARTNERS.checkCashOutToBalance();
        setIsInnerLoading(false);

        return { possibility: checkMoneyData?.rawData ?? false, text: checkMoneyError };
    };

    const changeData = (page = 1, isNeedToRefreshSummary = false) => {
        const { from, to } = period;
        const filterParams: TAgentCashOutRequest = {
            accountUserId: forArticles ? userId() : undefined,
            accountId: contextAccountId(),
            pageNumber: page,
            periodFrom: from ? dayjs(from).format('YYYY-MM-DD') : '',
            periodTo: to ? dayjs(to).format('YYYY-MM-DD') : '',
        };

        if (agentRequisiteType !== EAgentRequisitesType.undefined) {
            filterParams.agentRequisitesType = agentRequisiteType;
        }

        if (status !== ERequestStatus.undefined) {
            filterParams.requestStatus = status;
        }

        REDUX_API.PARTNERS_REDUX.getAgentCashOut(filterParams, dispatch);

        if (isNeedToRefreshSummary) {
            if (forArticles) {
                void REDUX_API.ARTICLES_REDUX.getTransactionInfo(dispatch);
                if (!isOtherAccount) {
                    void FETCH_API.ARTICLES.getSummaryInfo(dispatch);
                } if (contextUserId) {
                    void FETCH_API.ARTICLES.getSummaryInfo(dispatch, contextUserId);
                }
            } else {
                void REDUX_API.PARTNERS_REDUX.getSummaryData(dispatch);
                REDUX_API.PARTNERS_REDUX.getAgentPayments({
                    pageNumber: 1,
                    accountId: contextAccountId(),
                }, dispatch);
            }
        }
    };

    const currentPageHandler = (_: React.ChangeEvent<unknown>, page: number) => {
        changeData(page);
    };

    const searchHandler = () => {
        changeData();
    };

    const agentRequisiteTypeHandler = (ev: SelectChangeEvent<EAgentRequisitesType>) => {
        setAgentRequisiteType(ev.target.value as EAgentRequisitesType);
    };

    const requestStatusHandler = (ev: SelectChangeEvent<ERequestStatus>) => {
        setStatus(ev.target.value as ERequestStatus);
    };

    const openDialogHandler = async () => {
        if (!forArticles) {
            REDUX_API.PARTNERS_REDUX.getAgencyAgreementStatus(dispatch);
        }

        const { possibility, text } = await checkMoneyOperationPossibility();

        if (!possibility) {
            return show(EMessageType.warning, text);
        }

        if (forArticles || (agencyAgreementStatus && !agencyAgreementStatus.rawData)) {
            setIsOpenDialog(true);
        }
    };

    const createRequestHandler = async () => {
        const { possibility, text } = await checkMoneyOperationPossibility();

        if (!possibility) {
            return show(EMessageType.warning, text);
        }

        if (summaryData && summaryData.rawData.availableToPay < 1000) {
            return show(EMessageType.warning, `Для создания заявки необходима минимальная сумма 1000 ${ summaryData.rawData.currencySymbol }`);
        }

        if (forArticles || (agencyAgreementStatus && !agencyAgreementStatus.rawData)) {
            history.push(`${ AppRoutes.partners.createAgentCashOut }${ forArticles ? '?forArticles' : '' }`);
        }
    };

    const deleteRequestHandler = async (_: React.MouseEvent<HTMLDivElement, MouseEvent>, id: string) => {
        setIsInnerLoading(true);
        const { error: deleteError } = await FETCH_API.PARTNERS.deleteAgentCashOut({
            agentCashOutRequestId: id
        });
        setIsInnerLoading(false);

        if (deleteError && deleteError !== 'Успешно!') {
            show(EMessageType.error, deleteError);
        } else {
            changeData(data?.rawData.pageNumber);
        }
    };

    const printRequestHandler = async (_: React.MouseEvent<HTMLDivElement, MouseEvent>, id: string) => {
        setIsInnerLoading(true);
        const { responseBody } = await FETCH_API.PARTNERS.getAgentReportFile({
            agentCashOutRequestId: id
        });

        if (responseBody.size === 0) {
            show(EMessageType.error, 'Не удается получить отчет агента');
        } else {
            window.open(URL.createObjectURL(responseBody));
        }

        setIsInnerLoading(false);
    };

    const editRequestHandler = (_: React.MouseEvent<HTMLDivElement, MouseEvent>, id: string) => {
        history.push(`${ AppRoutes.partners.editAgentCashOut }?agentCashOutRequestId=${ id }${ forArticles ? '&forArticles' : '' }`);
    };

    return (
        <>
            { (isLoading || isInnerLoading) && <LoadingBounce /> }
            <div className={ globalStyles.container }>
                <div className={ globalStyles.header }>
                    <div className={ globalStyles.headerFilters }>
                        <FormAndLabel label="Период" className={ globalStyles.formDatePicker }>
                            <DoubleDateInputForm
                                isNeedToStretch={ true }
                                periodFromValue={ period.from }
                                periodToValue={ period.to }
                                periodFromInputName="from"
                                periodToInputName="to"
                                onValueChange={ periodHandler }
                            />
                        </FormAndLabel>
                        <FormAndLabel label="Реквизиты" className={ globalStyles.form }>
                            <Select value={ agentRequisiteType } fullWidth={ true } onChange={ agentRequisiteTypeHandler }>
                                <MenuItem value={ EAgentRequisitesType.undefined }>Все</MenuItem>
                                <MenuItem value={ EAgentRequisitesType.physicalPersonRequisites }>Реквизиты физ. лица</MenuItem>
                                <MenuItem disabled={ forArticles } value={ EAgentRequisitesType.legalPersonRequisites }>Реквизиты юр. лица</MenuItem>
                                <MenuItem value={ EAgentRequisitesType.soleProprietorRequisites }>Реквизиты (ИП)</MenuItem>
                            </Select>
                        </FormAndLabel>
                        <FormAndLabel label="Статус" className={ globalStyles.form }>
                            <Select value={ status } fullWidth={ true } onChange={ requestStatusHandler }>
                                <MenuItem value={ ERequestStatus.undefined }>Все</MenuItem>
                                <MenuItem value={ ERequestStatus.new }>Новая</MenuItem>
                                <MenuItem value={ ERequestStatus.operation }>В работе</MenuItem>
                                <MenuItem value={ ERequestStatus.paid }>Оплачена</MenuItem>
                            </Select>
                        </FormAndLabel>
                    </div>
                    <SuccessButton className={ globalStyles.button } onClick={ searchHandler }>Найти</SuccessButton>
                </div>
                <CommonTableWithFilter
                    isBorderStyled={ true }
                    isResponsiveTable={ true }
                    uniqueContextProviderStateId="WithdrawingList"
                    tableProps={ {
                        dataset: data ? data.rawData.chunkDataOfPagination : [],
                        keyFieldName: 'id',
                        emptyText: 'Заявки на вывод средств не найдены',
                        fieldsView: {
                            requestNumber: {
                                caption: '№ Заявки'
                            },
                            creationDateTime: {
                                caption: 'Дата создания',
                                format: (value: string) =>
                                    dayjs(value).format('DD.MM.YYYY')
                            },
                            agentRequisites: {
                                caption: 'Реквизиты'
                            },
                            requestStatus: {
                                caption: 'Статус',
                                format: (value: ERequestStatus) => {
                                    switch (value) {
                                        case ERequestStatus.operation:
                                            return <Chip color="info" label="В работе" className={ globalStyles.chip } />;
                                        case ERequestStatus.new:
                                            return <Chip color="warning" label="Новая" className={ globalStyles.chip } />;
                                        case ERequestStatus.paid:
                                            return <Chip color="primary" label="Оплачена" className={ globalStyles.chip } />;
                                        default:
                                            return null;
                                    }
                                }
                            },
                            requestedSum: {
                                caption: `Сумма, ${ summaryData?.rawData?.currencySymbol ?? '' }`,
                                format: (value: number) =>
                                    setCashFormat(value)
                            },
                            id: {
                                caption: 'Действия',
                                format: (value: string, info) => (
                                    <div className={ styles.actions }>
                                        { info.requestStatus === ERequestStatus.new &&
                                            <Tooltip
                                                onClick={ ev => deleteRequestHandler(ev, value) }
                                                title="Удалить заявку"
                                                className={ styles.tooltip }
                                            >
                                                <i className="fa fa-trash" aria-hidden="true" />
                                            </Tooltip>
                                        }
                                        { (!forArticles || (forArticles && (currentUserGroups.includes(AccountUserGroup.CloudAdmin) || currentUserGroups.includes(AccountUserGroup.ArticleEditor)))) && (
                                            <Tooltip
                                                onClick={ ev => editRequestHandler(ev, value) }
                                                title="Изменить заявку"
                                                className={ styles.tooltip }
                                            >
                                                <i className={ `fa fa-pencil-square-o ${ styles.editIcon }` } aria-hidden="true" />
                                            </Tooltip>
                                        ) }
                                        { !forArticles && (
                                            <Tooltip
                                                onClick={ ev => printRequestHandler(ev, value) }
                                                title="Распечатать отчет агента"
                                                className={ styles.tooltip }
                                            >
                                                <i className="fa fa-print" aria-hidden="true" />
                                            </Tooltip>
                                        ) }
                                    </div>
                                )
                            }
                        }
                    } }
                />
                <div className={ globalStyles.footer }>
                    <div className={ styles.buttons }>
                        { (currentUserGroups.includes(AccountUserGroup.AccountAdmin) || currentUserGroups.includes(AccountUserGroup.CloudAdmin)) &&
                            <>
                                <SuccessButton onClick={ createRequestHandler } tourId={ EPartnersTourId.withdrawalTabCreate }>Создать заявку</SuccessButton>
                                { !forArticles && (
                                    <OutlinedButton kind="success" onClick={ openDialogHandler } tourId={ EPartnersTourId.withdrawalTabCashOut }>Перевести на баланс</OutlinedButton>
                                ) }
                            </>
                        }
                    </div>
                    { data && data.rawData.totalCount > data.rawData.pageSize &&
                        <Pagination
                            shape="rounded"
                            page={ data.rawData.pageNumber }
                            count={ Math.ceil(data.rawData.totalCount / data.rawData.pageSize) }
                            onChange={ currentPageHandler }
                            className={ globalStyles.pagination }
                        />
                    }
                </div>
                <BalanceTransferDialog isOpen={ isOpenDialog } setIsOpen={ setIsOpenDialog } refreshData={ changeData } />
            </div>
        </>
    );
};