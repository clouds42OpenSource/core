import { Chip, Pagination } from '@mui/material';
import { useTour } from '@reactour/tour';
import dayjs from 'dayjs';
import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import { Link } from 'react-router-dom';

import { contextAccountId } from 'app/api';
import { EBillingServiceStatus } from 'app/api/endpoints/partners/enum';
import { REDUX_API } from 'app/api/useReduxApi';
import { AppRoutes } from 'app/AppRoutes';
import { setCashFormat } from 'app/common/helpers/setCashFormat';
import { useAppDispatch, useAppSelector } from 'app/hooks';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { SuccessButton } from 'app/views/components/controls/Button';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { TextOut } from 'app/views/components/TextOut';
import { EPartnersSteps, EPartnersTourId } from 'app/views/Layout/ProjectTour/enums';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { SERVICES_ROUTES } from 'app/views/modules/Services/constants/RoutesConstants';
import { TPartnersHistory } from 'app/views/modules/ToPartners/config/types';

import globalStyles from '../style.module.css';
import styles from './style.module.css';

export const MyServices = withFloatMessages(({ floatMessage: { show } }) => {
    const { isOpen, currentStep, meta, setCurrentStep } = useTour();
    const dispatch = useAppDispatch();
    const history = useHistory<TPartnersHistory>();

    const { servicesListReducer: { data, error, isLoading }, agencyAgreementStatusReducer: { data: agencyAgreementStatus } } = useAppSelector(state => state.PartnersState);
    const currencySymbol = useAppSelector(state => state.PartnersState.summaryDataReducer.data?.rawData?.currencySymbol);

    const [currentPage, setCurrentPage] = useState(1);

    useEffect(() => {
        REDUX_API.PARTNERS_REDUX.getServices({
            accountId: contextAccountId(),
            pageNumber: currentPage
        }, dispatch);
    }, [currentPage, dispatch]);

    useEffect(() => {
        if (error) {
            show(MessageType.Error, error, 1500);
        }
    }, [error, show]);

    const currentPageHandler = (_: React.ChangeEvent<unknown>, page: number) => {
        setCurrentPage(page);
    };

    const redirectHandler = () => {
        REDUX_API.PARTNERS_REDUX.getAgencyAgreementStatus(dispatch);

        if (agencyAgreementStatus && !agencyAgreementStatus.rawData) {
            history.push(AppRoutes.services.createService);

            if (meta === 'partners' && isOpen && currentStep === EPartnersSteps.servicesTabCreate) {
                setCurrentStep(prev => prev + 1);
            }
        }
    };

    return (
        <>
            { isLoading && <LoadingBounce /> }
            <div className={ globalStyles.container }>
                { data &&
                    <CommonTableWithFilter
                        isBorderStyled={ true }
                        isResponsiveTable={ true }
                        uniqueContextProviderStateId="ServicesList"
                        tableProps={ {
                            dataset: data.rawData.chunkDataOfPagination,
                            keyFieldName: 'id',
                            isIndexToKeyFieldName: true,
                            emptyText: 'Сервисы не найдены',
                            fieldsView: {
                                name: {
                                    caption: 'Название',
                                    format: (value: string, service) =>
                                        <div className={ styles.serviceName }>
                                            <Link className={ styles.link } to={ `${ SERVICES_ROUTES.ServiceCardPageUrl }/${ service.id }` }>
                                                { value }
                                            </Link>
                                            { !service.isActive && <TextOut fontSize={ 10 } className={ styles.warningText }>(Отключен)</TextOut> }
                                        </div>
                                },
                                serviceActivationDate: {
                                    caption: 'Дата активации',
                                    format: (value: string | null) =>
                                        (value ? dayjs(value).format('DD.MM.YYYY') : '---')
                                },
                                countUsers: {
                                    caption: 'Пользователи',
                                    format: (value: number) =>
                                        <><i className="fa fa-user" aria-hidden="true" />{ ' ' }{ value }</>
                                },
                                serviceCost: {
                                    caption: `Вознаграждение, ${ currencySymbol }`,
                                    format: (value: number) =>
                                        setCashFormat(value)
                                },
                                billingServiceStatus: {
                                    caption: 'Статус',
                                    format: (value: EBillingServiceStatus) => {
                                        switch (value) {
                                            case EBillingServiceStatus.draft:
                                                return <Chip color="warning" label="Черновик" className={ globalStyles.chip } />;
                                            case EBillingServiceStatus.moderate:
                                                return <Chip color="info" label="На модерации" className={ globalStyles.chip } />;
                                            case EBillingServiceStatus.active:
                                                return <Chip color="primary" label="Активен" className={ globalStyles.chip } />;
                                            default:
                                                return null;
                                        }
                                    }
                                },
                            }
                        } }
                    />
                }
                <div className={ globalStyles.footer }>
                    <SuccessButton onClick={ redirectHandler } tourId={ EPartnersTourId.servicesTabCreate }>Создать сервис</SuccessButton>
                    { data && data.rawData.totalCount > data.rawData.pageSize &&
                        <Pagination
                            shape="rounded"
                            page={ currentPage }
                            count={ Math.ceil(data.rawData.totalCount / data.rawData.pageSize) }
                            onChange={ currentPageHandler }
                            className={ globalStyles.pagination }
                        />
                    }
                </div>
            </div>
        </>
    );
});