import { Box, MenuItem, OutlinedInput, Pagination, Select, SelectChangeEvent } from '@mui/material';
import { EPartnersTourId } from 'app/views/Layout/ProjectTour/enums';
import { RegisterClientDialog } from 'app/views/modules/ToPartners/components';
import { GoogleSheetDialog } from 'app/views/modules/ToPartners/modules';
import dayjs from 'dayjs';
import React, { useCallback, useEffect, useState } from 'react';

import { contextAccountId } from 'app/api';
import { ETypeOfPartnership } from 'app/api/endpoints/partners/enum';
import { TClientsListRequest } from 'app/api/endpoints/partners/request';
import { REDUX_API } from 'app/api/useReduxApi';
import { TDatePicker } from 'app/common/types/app-types';
import { useAppDispatch, useAppSelector } from 'app/hooks';
import { SuccessButton } from 'app/views/components/controls/Button';
import { DoubleDateInputForm } from 'app/views/components/controls/forms/DoubleDateInputView';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';

import { MyClientsTable } from '../../components/tables';

import globalStyles from '../style.module.css';

type TSelect = '0' | '1' | '2';

const { getClients, getSummaryData, getAgencyAgreementStatus } = REDUX_API.PARTNERS_REDUX;

export const MyClients = () => {
    const dispatch = useAppDispatch();

    const {
        PartnersState: {
            agencyAgreementStatusReducer: { data: agencyAgreementStatus },
            clientListReducer: { data }
        }
    } = useAppSelector(state => state);

    const [company, setCompany] = useState('');
    const [period, setPeriod] = useState<TDatePicker>({ from: null, to: null });
    const [partnership, setPartnership] = useState<TSelect>('0');
    const [activity, setActivity] = useState<TSelect>('0');

    const [isOpenDialog, setIsOpenDialog] = useState(false);
    const [isOpenSheetDialog, setIsOpenShitDialog] = useState(false);

    useEffect(() => {
        getClients({
            accountId: contextAccountId(),
            pageNumber: 1
        }, dispatch);
    }, [dispatch]);

    const changeData = useCallback((page = 1, isNeedToRefreshSummary = false) => {
        const { from, to } = period;
        const filterParams: TClientsListRequest = {
            accountId: contextAccountId(),
            accountData: company,
            pageNumber: page,
            periodFrom: from ? dayjs(from).format('YYYY-MM-DD') : '',
            periodTo: to ? dayjs(to).format('YYYY-MM-DD') : ''
        };

        if (partnership !== '0') {
            filterParams.typeOfPartnership = parseInt(partnership, 10) as ETypeOfPartnership;
        }

        if (activity !== '0') {
            filterParams.serviceIsActiveForClient = activity === '1';
        }

        getClients(filterParams, dispatch);

        if (isNeedToRefreshSummary) {
            void getSummaryData(dispatch);
        }
    }, [activity, company, dispatch, partnership, period]);

    const searchHandler = () => {
        changeData();
    };

    const openDialogHandler = () => {
        getAgencyAgreementStatus(dispatch);

        if (agencyAgreementStatus && !agencyAgreementStatus.rawData) {
            setIsOpenDialog(true);
        }
    };

    const companyHandler = (ev: React.ChangeEvent<HTMLInputElement>) => {
        setCompany(ev.target.value);
    };

    const partnershipHandler = (ev: SelectChangeEvent<TSelect>) => {
        setPartnership(ev.target.value as TSelect);
    };

    const activityHandler = (ev: SelectChangeEvent<TSelect>) => {
        setActivity(ev.target.value as TSelect);
    };

    const currentPageHandler = (_: React.ChangeEvent<unknown>, page: number) => {
        changeData(page);
    };

    const periodHandler = (formName: string, newValue: Date) => {
        if (formName === 'from') {
            setPeriod({ ...period, from: newValue });
        } else if (formName === 'to') {
            setPeriod({ ...period, to: newValue });
        }
    };

    const openSheetDialogHandler = () => {
        setIsOpenShitDialog(true);
    };

    return (
        <>
            <RegisterClientDialog
                isOpen={ isOpenDialog }
                setIsOpen={ setIsOpenDialog }
            />
            <GoogleSheetDialog
                isOpen={ isOpenSheetDialog }
                setIsOpen={ setIsOpenShitDialog }
            />
            <div className={ globalStyles.container }>
                <div className={ globalStyles.header }>
                    <div className={ globalStyles.headerFilters }>
                        <FormAndLabel label="Период" className={ globalStyles.formDatePicker }>
                            <DoubleDateInputForm
                                isNeedToStretch={ true }
                                periodFromValue={ period.from }
                                periodToValue={ period.to }
                                periodFromInputName="from"
                                periodToInputName="to"
                                onValueChange={ periodHandler }
                            />
                        </FormAndLabel>
                        <FormAndLabel label="Компания" className={ globalStyles.form }>
                            <OutlinedInput
                                value={ company }
                                onChange={ companyHandler }
                                placeholder="Введите название, ID аккаунта или ИНН"
                                fullWidth={ true }
                            />
                        </FormAndLabel>
                        <FormAndLabel label="Тип партнерства" className={ globalStyles.form }>
                            <Select value={ partnership } fullWidth={ true } onChange={ partnershipHandler }>
                                <MenuItem value="0">Все</MenuItem>
                                <MenuItem value="1">Привлеченный мной клиент</MenuItem>
                                <MenuItem value="2">Клиент моего сервиса</MenuItem>
                            </Select>
                        </FormAndLabel>
                        <FormAndLabel label="Активность" className={ globalStyles.form }>
                            <Select value={ activity } fullWidth={ true } onChange={ activityHandler }>
                                <MenuItem value="0">Все</MenuItem>
                                <MenuItem value="1">Активные</MenuItem>
                                <MenuItem value="2">Неактивные</MenuItem>
                            </Select>
                        </FormAndLabel>
                    </div>
                    <SuccessButton className={ globalStyles.button } onClick={ searchHandler }>Найти</SuccessButton>
                </div>
                <MyClientsTable />
                <div className={ globalStyles.footer }>
                    <Box display="flex" gap="10px" flexWrap="wrap">
                        <SuccessButton onClick={ openDialogHandler } tourId={ EPartnersTourId.clientsTabRegisterClient }>
                            <i className="fa fa-plus-circle" />&nbsp;Зарегистрировать клиента
                        </SuccessButton>
                        { !!data?.rawData.totalCount && (
                            <SuccessButton onClick={ openSheetDialogHandler }>
                                <i className="fa fa-table" />Выгрузить в Google таблицу
                            </SuccessButton>
                        ) }
                    </Box>
                    { data && data.rawData.totalCount > data.rawData.pageSize &&
                        <Pagination
                            shape="rounded"
                            page={ data.rawData.pageNumber }
                            count={ Math.ceil(data.rawData.totalCount / data.rawData.pageSize) }
                            onChange={ currentPageHandler }
                            className={ globalStyles.pagination }
                        />
                    }
                </div>
            </div>
        </>
    );
};