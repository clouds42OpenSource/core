import { Chip, Pagination } from '@mui/material';
import Tooltip from '@mui/material/Tooltip';
import { contextAccountId } from 'app/api';
import { EAgentRequisitesStatus } from 'app/api/endpoints/partners/enum';
import { FETCH_API } from 'app/api/useFetchApi';
import { REDUX_API } from 'app/api/useReduxApi';
import { AppRoutes } from 'app/AppRoutes';
import { EMessageType, useAppDispatch, useAppSelector, useFloatMessages } from 'app/hooks';

import { SuccessButton } from 'app/views/components/controls/Button';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { TArticlesProps } from 'app/views/modules/Articles/types/TArticlesHistory';
import dayjs from 'dayjs';
import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router';

import globalStyles from '../style.module.css';
import styles from './style.module.css';

export const Requisites = ({ forArticles }: TArticlesProps) => {
    const history = useHistory();
    const dispatch = useAppDispatch();
    const { show } = useFloatMessages();

    const {
        agentRequisitesListReducer: { data, error, isLoading },
        agencyAgreementStatusReducer: { data: agencyAgreementStatus }
    } = useAppSelector(state => state.PartnersState);

    const [currentPage, setCurrentPage] = useState(1);

    useEffect(() => {
        REDUX_API.PARTNERS_REDUX.getAgentRequisites({
            accountId: contextAccountId(),
            pageNumber: currentPage
        }, dispatch);
    }, [dispatch, currentPage]);

    useEffect(() => {
        if (error) {
            show(EMessageType.error, error, 1500);
        }
    }, [error, show]);

    const currentPageHandler = (_: React.ChangeEvent<unknown>, page: number) => {
        setCurrentPage(data?.rawData.pageNumber ?? page);
    };

    const deleteRequisitesHandler = async (id: string) => {
        const { error: deleteError } = await FETCH_API.PARTNERS.deleteAgentRequisite({ agentRequisitesId: id });

        if (deleteError && deleteError !== 'Успешно!') {
            return show(EMessageType.error, deleteError);
        }

        show(EMessageType.success, 'Реквизиты успешно удалены');
        REDUX_API.PARTNERS_REDUX.getAgentRequisites({
            accountId: contextAccountId(),
            pageNumber: currentPage
        }, dispatch);
    };

    const editRequisitesHandler = (id: string) => {
        history.push(`${ AppRoutes.partners.editAgentRequisites }?agentRequisitesId=${ id }`, { forArticles });
    };

    const createRequisitesHandler = () => {
        if (!forArticles) {
            REDUX_API.PARTNERS_REDUX.getAgencyAgreementStatus(dispatch);
        }

        if (forArticles || (agencyAgreementStatus && !agencyAgreementStatus.rawData)) {
            history.push(AppRoutes.partners.createAgentRequisites, { forArticles });
        }
    };

    return (
        <>
            { isLoading && <LoadingBounce /> }
            <div className={ globalStyles.container }>
                <CommonTableWithFilter
                    isBorderStyled={ true }
                    isResponsiveTable={ true }
                    uniqueContextProviderStateId="RequisitesList"
                    tableProps={ {
                        dataset: data ? data.rawData.chunkDataOfPagination : [],
                        emptyText: 'Реквизиты не найдены',
                        keyFieldName: 'id',
                        fieldsView: {
                            partnerAccountCaption: {
                                caption: 'Получатель'
                            },
                            creationDate: {
                                caption: 'Дата создания',
                                format: (value: string) =>
                                    dayjs(value).format('DD.MM.YYYY')
                            },
                            agentRequisitesTypeString: {
                                caption: 'Тип'
                            },
                            agentRequisitesStatus: {
                                caption: 'Статус',
                                format: (value: EAgentRequisitesStatus) => {
                                    switch (value) {
                                        case EAgentRequisitesStatus.draft:
                                            return <Chip className={ globalStyles.chip } label="Черновик" color="warning" />;
                                        case EAgentRequisitesStatus.onCheck:
                                            return <Chip className={ globalStyles.chip } label="На проверке" color="info" />;
                                        case EAgentRequisitesStatus.verified:
                                            return <Chip className={ globalStyles.chip } label="Проверено" color="primary" />;
                                        default:
                                            return null;
                                    }
                                }
                            },
                            id: {
                                caption: 'Действия',
                                format: (_, info) => {
                                    return (
                                        <div className={ styles.actions }>
                                            <Tooltip
                                                onClick={ () => editRequisitesHandler(info.id) }
                                                title="Изменить реквизиты"
                                                className={ styles.tooltip }
                                            >
                                                <i className={ `fa fa-pencil-square-o ${ styles.editIcon }` } aria-hidden="true" />
                                            </Tooltip>
                                            { info.agentRequisitesStatus === EAgentRequisitesStatus.draft &&
                                                <Tooltip
                                                    onClick={ () => deleteRequisitesHandler(info.id) }
                                                    title="Удалить реквизиты"
                                                    className={ styles.tooltip }
                                                >
                                                    <i className="fa fa-trash-o" aria-hidden="true" />
                                                </Tooltip>
                                            }
                                        </div>
                                    );
                                }
                            }
                        }
                    } }
                />
                <div className={ globalStyles.footer }>
                    <SuccessButton onClick={ createRequisitesHandler }>Добавить реквизиты</SuccessButton>
                    { data && data.rawData.totalCount > data.rawData.pageSize &&
                        <Pagination
                            shape="rounded"
                            page={ data.rawData.pageNumber }
                            count={ Math.ceil(data.rawData.totalCount / data.rawData.pageSize) }
                            onChange={ currentPageHandler }
                            className={ globalStyles.pagination }
                        />
                    }
                </div>
            </div>
        </>
    );
};