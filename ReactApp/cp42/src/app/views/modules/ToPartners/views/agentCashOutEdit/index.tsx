import { useQuery } from 'app/hooks/useQuery';
import React, { useEffect, useMemo, useState } from 'react';
import { useHistory, useLocation } from 'react-router';

import { withHeader } from 'app/views/components/_hoc/withHeader';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { useAppSelector } from 'app/hooks';
import { AppRoutes } from 'app/AppRoutes';
import { TabsControl } from 'app/views/components/controls/TabsControl';
import { THeaders } from 'app/views/components/controls/TabsControl/views/TabView';
import { AccountUserGroup } from 'app/common/enums';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { ERequestStatus } from 'app/api/endpoints/partners/enum';
import { MenuItem, Select, SelectChangeEvent } from '@mui/material';
import { ContainedButton, SuccessButton } from 'app/views/components/controls/Button';
import { FETCH_API } from 'app/api/useFetchApi';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { TAgentCashOutEditResponse } from 'app/api/endpoints/partners/response';
import { AgentCashOutComponent } from '../../modules/agentCashOut';
import { TPartnersHistory } from '../../config/types';

import styles from './style.module.css';

type TAgentCashOutEditComponent = FloatMessageProps & {

};

const AgentCashOutEditComponent = withFloatMessages(({ floatMessage: { show } }: TAgentCashOutEditComponent) => {
    const history = useHistory<TPartnersHistory>();
    const location = useLocation();
    const query = useQuery();

    const { currentUserGroups } = useAppSelector(state => state.Global.getCurrentSessionSettingsReducer.settings.currentContextInfo);

    const [status, setStatus] = useState<ERequestStatus>(ERequestStatus.undefined);
    const [requestData, setRequestData] = useState<TAgentCashOutEditResponse>();
    const [isLoading, setIsLoading] = useState(false);

    const cashOutRequestId = new URLSearchParams(location.search).get('agentCashOutRequestId');
    const isEditMode = !!(cashOutRequestId && window.location.href.includes(AppRoutes.partners.editAgentCashOut));

    const forArticles = useMemo(() => query.has('forArticles'), [query]);

    useEffect(() => {
        (async () => {
            setIsLoading(true);
            const { error: getRequestError, data: getRequestData } = await FETCH_API.PARTNERS.getAgentCashOutRequest({ agentCashOutRequestId: cashOutRequestId ?? '' });

            if (getRequestError) {
                show(MessageType.Error, getRequestError);
            } else if (getRequestData) {
                setRequestData(getRequestData.rawData ?? undefined);
                setStatus(getRequestData.rawData?.requestStatus ?? ERequestStatus.undefined);
            }

            setIsLoading(false);
        })();
    }, [cashOutRequestId, show]);

    const onCancelHandler = () => {
        if (forArticles) {
            history.push(AppRoutes.articlesReference, { activeTabIndex: 3 });
        } else {
            history.push(AppRoutes.partners.toPartners, { activeTabIndex: 4 });
        }
    };

    const HEADERS: THeaders = [
        {
            title: 'Общее',
            isVisible: true,
        },
        {
            title: 'Управление',
            isVisible: currentUserGroups.includes(AccountUserGroup.CloudAdmin) || currentUserGroups.includes(AccountUserGroup.AccountSaleManager)
        }
    ];

    const onChangeStatusHandler = async () => {
        if (requestData) {
            const { error } = await FETCH_API.PARTNERS.changeStatusAgentCashOut({
                requestNumber: requestData.requestNumber,
                sum: requestData.totalSum,
                status,
            });

            if (error && error !== 'Успешно!') {
                show(MessageType.Error, error);
            } else {
                onCancelHandler();
            }
        }
    };

    const requestStatusHandler = (ev: SelectChangeEvent<ERequestStatus>) => {
        setStatus(ev.target.value as ERequestStatus);
    };

    if (isLoading) {
        return <LoadingBounce />;
    }

    return (
        <TabsControl headers={ HEADERS }>
            { requestData &&
                <AgentCashOutComponent
                    cashOutRequestId={ cashOutRequestId ?? '' }
                    isEditMode={ isEditMode }
                    dataResponse={ requestData }
                    isDisabled={ requestData?.requestStatus === ERequestStatus.operation || requestData?.requestStatus === ERequestStatus.paid || forArticles }
                />
            }
            <div className={ styles.container }>
                <Select value={ status } fullWidth={ true } onChange={ requestStatusHandler }>
                    <MenuItem value={ ERequestStatus.undefined } className={ styles.hidden } />
                    <MenuItem value={ ERequestStatus.new }>Новая</MenuItem>
                    <MenuItem value={ ERequestStatus.operation }>В работе</MenuItem>
                    <MenuItem value={ ERequestStatus.paid }>Оплачена</MenuItem>
                </Select>
                <div className={ styles.buttons }>
                    <ContainedButton onClick={ onCancelHandler }>Назад</ContainedButton>
                    <SuccessButton onClick={ onChangeStatusHandler }>Сохранить изменения</SuccessButton>
                </div>
            </div>
        </TabsControl>
    );
});

export const AgentCashOutEdit = withHeader({
    page: AgentCashOutEditComponent,
    title: 'Редактирование заявки на выплату'
});