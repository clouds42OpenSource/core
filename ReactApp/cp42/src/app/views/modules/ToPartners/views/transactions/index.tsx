import { OutlinedInput, Pagination } from '@mui/material';
import React, { useEffect, useState } from 'react';
import dayjs from 'dayjs';

import { AccountUserGroup } from 'app/common/enums';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { DoubleDateInputForm } from 'app/views/components/controls/forms/DoubleDateInputView';
import { SuccessButton } from 'app/views/components/controls/Button';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { useAppDispatch, useAppSelector } from 'app/hooks';
import { REDUX_API } from 'app/api/useReduxApi';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { TAgentPaymentListRequest } from 'app/api/endpoints/partners/request';
import { contextAccountId } from 'app/api';
import { formatAccountCaption, formatClientPaymentSum, formatTransactionDate, formatTransactionSum, formatAgentPaymentSourceType, formatVipAccount } from 'app/views/modules/_common/shared/PartnersPages';
import { TDatePicker } from '../../config/types';
import { CreateTransactionDialog } from '../../components';

import globalStyles from '../style.module.css';

export const Transactions = withFloatMessages(({ floatMessage: { show } }) => {
    const {
        PartnersState: {
            agentPaymentListReducer: { data, error, isLoading },
            summaryDataReducer: { data: summaryData }
        },
        Global: {
            getCurrentSessionSettingsReducer: {
                settings: { currentContextInfo: { currentUserGroups } }
            }
        }
    } = useAppSelector(state => state);

    const dispatch = useAppDispatch();
    const currencySymbol = summaryData ? summaryData.rawData.currencySymbol : '';

    const [isOpenDialog, setIsOpenDialog] = useState(false);
    const [company, setCompany] = useState('');
    const [period, setPeriod] = useState<TDatePicker>({ from: null, to: null });

    useEffect(() => {
        REDUX_API.PARTNERS_REDUX.getAgentPayments({
            accountId: contextAccountId(),
            pageNumber: 1
        }, dispatch);
    }, [dispatch]);

    useEffect(() => {
        if (error) {
            show(MessageType.Error, error, 1500);
        }
    }, [error, show]);

    const changeData = (page = 1, isNeedToRefreshSummary = false) => {
        const { from, to } = period;
        const filterParams: TAgentPaymentListRequest = {
            accountId: contextAccountId(),
            pageNumber: page,
            accountData: company,
            periodFrom: from ? dayjs(from).format('YYYY-MM-DD') : '',
            periodTo: from ? dayjs(to).format('YYYY-MM-DD') : '',
        };

        REDUX_API.PARTNERS_REDUX.getAgentPayments(filterParams, dispatch);

        if (isNeedToRefreshSummary) {
            void REDUX_API.PARTNERS_REDUX.getSummaryData(dispatch);
        }
    };

    const periodHandler = (formName: string, newValue: Date) => {
        if (formName === 'from') {
            setPeriod({ ...period, from: newValue });
        } else if (formName === 'to') {
            setPeriod({ ...period, to: newValue });
        }
    };

    const searchHandler = () => {
        changeData();
    };

    const openDialogHandler = () => {
        setIsOpenDialog(true);
    };

    const companyHandler = (ev: React.ChangeEvent<HTMLInputElement>) => {
        setCompany(ev.target.value);
    };

    const currentPageHandler = (_: React.ChangeEvent<unknown>, page: number) => {
        changeData(page);
    };

    return (
        <>
            { isLoading && <LoadingBounce /> }
            <CreateTransactionDialog isOpen={ isOpenDialog } setIsOpen={ setIsOpenDialog } refreshData={ changeData } />
            <div className={ globalStyles.container }>
                <div className={ globalStyles.header }>
                    <div className={ globalStyles.headerFilters }>
                        <FormAndLabel label="Период" className={ globalStyles.formDatePicker }>
                            <DoubleDateInputForm
                                isNeedToStretch={ true }
                                periodFromValue={ period.from }
                                periodToValue={ period.to }
                                periodFromInputName="from"
                                periodToInputName="to"
                                onValueChange={ periodHandler }
                            />
                        </FormAndLabel>
                        <FormAndLabel label="Компания" className={ globalStyles.form }>
                            <OutlinedInput
                                value={ company }
                                onChange={ companyHandler }
                                placeholder="Название компании"
                                fullWidth={ true }
                            />
                        </FormAndLabel>
                    </div>
                    <SuccessButton onClick={ searchHandler } className={ globalStyles.button }>Найти</SuccessButton>
                </div>
                <CommonTableWithFilter
                    isBorderStyled={ true }
                    isResponsiveTable={ true }
                    uniqueContextProviderStateId="ClientList"
                    tableProps={ {
                        dataset: data ? data.rawData.chunkDataOfPagination : [],
                        keyFieldName: 'accountId',
                        isIndexToKeyFieldName: true,
                        emptyText: 'Транзакции не найдены',
                        fieldsView: {
                            transactionDate: {
                                caption: 'Дата',
                                format: formatTransactionDate,
                            },
                            accountCaption: {
                                caption: 'Название компании',
                                format: formatAccountCaption,
                            },
                            operation: {
                                caption: 'Операция',
                                format: formatVipAccount
                            },
                            clientPaymentSum: {
                                caption: `Списание от клиента, ${ currencySymbol }`,
                                format: formatClientPaymentSum
                            },
                            agentPaymentSourceType: {
                                caption: 'Счет',
                                format: formatAgentPaymentSourceType
                            },
                            transactionSum: {
                                caption: `Вознаграждение, ${ currencySymbol }`,
                                format: formatTransactionSum
                            }
                        }
                    } }
                />
                <div className={ globalStyles.footer }>
                    { currentUserGroups.includes(AccountUserGroup.CloudAdmin) &&
                        <SuccessButton onClick={ openDialogHandler }>Создать транзакцию</SuccessButton>
                    }
                    { data && data.rawData.totalCount > data.rawData.pageSize &&
                        <Pagination
                            shape="rounded"
                            page={ data.rawData.pageNumber }
                            count={ Math.ceil(data.rawData.totalCount / data.rawData.pageSize) }
                            onChange={ currentPageHandler }
                            className={ globalStyles.pagination }
                        />
                    }
                </div>
            </div>
        </>
    );
});