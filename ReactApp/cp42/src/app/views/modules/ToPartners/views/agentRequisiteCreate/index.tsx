import { withHeader } from 'app/views/components/_hoc/withHeader';
import { AgentRequisitesComponent } from '../../modules';

export const AgentRequisitesCreate = withHeader({
    title: 'Создание реквизитов',
    page: AgentRequisitesComponent
});