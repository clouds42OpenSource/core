import { MuiTelInputCountry } from 'mui-tel-input';

export const allowedCountries: MuiTelInputCountry[] = ['RU', 'UA', 'KZ'];