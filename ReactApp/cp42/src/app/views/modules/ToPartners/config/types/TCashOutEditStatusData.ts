import { TAgentCashOutChangeStatusRequest } from 'app/api/endpoints/partners/request';

export type TCashOutEditStatusData = Omit<TAgentCashOutChangeStatusRequest, 'status'>;