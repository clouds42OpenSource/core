export const requiredMessage = 'Поле обязательно к заполнению';

export const wrongFullNameMessage = 'Неверный формат ФИО';

export const limitErrorMessage = (n: number) => `Поле должно состоять из ${ n } цифр`;

export const transformValue = (value: string) => value.replaceAll('_', '').replaceAll('-', '');

export const transformPhoneValue = (value: string) => value.replaceAll(' ', '');

export const innMessage = 'Неверный ИНН, должен быть от 4-х до 14-ти цифр';