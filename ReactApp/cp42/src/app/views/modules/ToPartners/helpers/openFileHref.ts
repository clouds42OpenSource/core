import { FETCH_API } from 'app/api/useFetchApi';

export const getFileHref = (fileId: string) => FETCH_API.PARTNERS.getAgentRequisitesFile(fileId).then(file => URL.createObjectURL(file));