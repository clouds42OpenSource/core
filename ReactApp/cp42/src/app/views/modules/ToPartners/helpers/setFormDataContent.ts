export const soleProprietorContent = (arrayNumber?: number) => `soleProprietorRequisites.files[${ arrayNumber ?? 0 }].content`;

export const legalPersonContent = (arrayNumber?: number) => `legalPersonRequisites.files[${ arrayNumber ?? 0 }].content`;

export const physicalPersonContent = (arrayNumber?: number) => `physicalPersonRequisites.files[${ arrayNumber ?? 0 }].content`;