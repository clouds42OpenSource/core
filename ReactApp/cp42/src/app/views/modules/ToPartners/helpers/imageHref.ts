export const getHref = (imageName: string) =>
    `${ import.meta.env.REACT_APP_CP_HOST }/img/to-partners/${ imageName }.png`;