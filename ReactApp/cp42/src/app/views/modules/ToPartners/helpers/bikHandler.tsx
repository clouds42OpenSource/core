import React from 'react';
import { FormikErrors } from 'formik';

import { FETCH_API } from 'app/api/useFetchApi';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { transformValue } from '../helpers/validationSchema';

type TBikHandler<T> = {
    setIsLoading: React.Dispatch<React.SetStateAction<boolean>>;
    show: (type: MessageType, content: React.ReactNode, autoHideDuration?: (number | undefined)) => void;
    ev: React.ChangeEvent<HTMLInputElement>;
    setFieldValue: (field: string, value: string | null, shouldValidate?: boolean) => (Promise<FormikErrors<T>> | Promise<void>);
};

export const bikHandler = async <T extends object>(props: TBikHandler<T>) => {
    const { target: { value } } = props.ev;
    await props.setFieldValue('bik', value, true);

    if (transformValue(value).length === 9) {
        props.setIsLoading(true);
        const { data, error } = await FETCH_API.PARTNERS.getBankDetails({ bik: transformValue(value) });

        if (error) {
            props.show(MessageType.Error, error);
        } else if (data && data.rawData) {
            if (data.rawData.errorMessage) {
                props.show(MessageType.Info, data.rawData.errorMessage);
            } else {
                await props.setFieldValue('bankName', data.rawData.bankName, true);
                await props.setFieldValue('correspondentAccount', data.rawData.correspondentAccount, true);
            }
        }

        props.setIsLoading(false);
    }
};