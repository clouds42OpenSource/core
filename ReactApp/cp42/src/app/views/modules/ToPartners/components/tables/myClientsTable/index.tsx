import dayjs from 'dayjs';
import React, { memo, useEffect } from 'react';
import { Chip } from '@mui/material';

import { ETypeOfPartnership } from 'app/api/endpoints/partners/enum';
import { EMessageType, useAppSelector, useFloatMessages } from 'app/hooks';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { TextOut } from 'app/views/components/TextOut';
import { EPartnersTourId } from 'app/views/Layout/ProjectTour/enums';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';

import globalStyles from '../../../views/style.module.css';
import styles from './style.module.css';

export const MyClientsTable = memo(() => {
    const { show } = useFloatMessages();

    const { clientListReducer: { data, error, isLoading } } = useAppSelector(state => state.PartnersState);
    const currencySymbol = useAppSelector(state => state.PartnersState.summaryDataReducer.data?.rawData?.currencySymbol);

    useEffect(() => {
        if (error) {
            show(EMessageType.error, error, 1500);
        }
    }, [error, show]);

    return (
        <>
            { isLoading && <LoadingBounce /> }
            <CommonTableWithFilter
                tourId={ EPartnersTourId.clientsTabTable }
                isBorderStyled={ true }
                isResponsiveTable={ true }
                uniqueContextProviderStateId="ClientList"
                tableProps={ {
                    dataset: data ? data.rawData.chunkDataOfPagination : [],
                    keyFieldName: 'accountId',
                    isIndexToKeyFieldName: true,
                    emptyText: 'Клиенты не найдены',
                    fieldsView: {
                        accountIndexNumber: {
                            caption: 'ID Аккаунта'
                        },
                        accountCaption: {
                            caption: 'Название компании',
                            format: (value: string, clientData) =>
                                <>
                                    <TextOut>{ value }</TextOut>
                                    { clientData.isVipAccount && <span className={ styles.vipText }>{ ' VIP' }</span> }
                                </>
                        },
                        typeOfPartnership: {
                            caption: 'Тип партнерства',
                            format: (value: ETypeOfPartnership) =>
                                <Chip label={ value === ETypeOfPartnership.agent ? 'Клиент' : 'Клиент моего сервиса' } />
                        },
                        serviceName: {
                            caption: 'Сервис'
                        },
                        clientActivationDate: {
                            caption: 'Дата подключения',
                            format: (value: string) =>
                                dayjs(value).format('DD.MM.YYYY')
                        },
                        serviceIsActiveForClient: {
                            caption: 'Активность',
                            format: (value: boolean) =>
                                <Chip
                                    color={ value ? 'primary' : 'error' }
                                    label={ value ? 'Активен' : 'Неактивен' }
                                    className={ globalStyles.chip }
                                />
                        },
                        isDemoPeriod: {
                            caption: 'Демо период',
                            format: (value: boolean) =>
                                (value ? <span className={ styles.demoText }>Демо</span> : null)
                        },
                        monthlyBonus: {
                            caption: `Ежемесячное вознаграждение, ${ currencySymbol }`
                        }
                    }
                } }
            />
        </>
    );
});