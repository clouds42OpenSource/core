import React, { useState as useStateMock } from 'react';
import { render } from '@testing-library/react';

import { RequisiteFormButtons } from './index';

jest.mock('react', () => ({
    ...jest.requireActual('react'),
    useState: jest.fn(),
}));

describe('<RequisiteFormButtons />', () => {
    const setState = jest.fn();

    beforeEach(() => {
        (useStateMock as jest.Mock).mockImplementation(init => [init, setState]);
    });

    it('shows button with allowed editing', () => {
        const { getAllByRole } = render(<RequisiteFormButtons setStatus={ setState } />);
        expect(getAllByRole('button').length).toEqual(3);
    });

    it('shows button with disallowed editing', () => {
        const { getAllByRole } = render(<RequisiteFormButtons setStatus={ setState } isDisabled={ true } />);
        expect(getAllByRole('button').length).toEqual(1);
    });
});