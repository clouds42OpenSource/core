import InputMask, { Props } from 'react-input-mask';
import { MuiTelInput } from 'mui-tel-input';
import { TextField, TextFieldProps } from '@mui/material';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import dayjs from 'dayjs';
import { useHistory } from 'react-router';
import React, { useEffect, useState } from 'react';

import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { FETCH_API } from 'app/api/useFetchApi';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { ConfigFileUploader } from 'app/views/modules/_common/components/ConfigFileUploader';
import { EAgentRequisiteFileType, EAgentRequisitesStatus, EAgentRequisitesType } from 'app/api/endpoints/partners/enum';
import { contextAccountId } from 'app/api';
import { AppRoutes } from 'app/AppRoutes';
import { getFileFromBase64 } from 'app/utils';
import { CheckBoxForm } from 'app/views/components/controls/forms';
import { WebVerb } from 'core/requestSender/enums';
import { objectToFormData } from 'app/web/common';
import { TPartialAgentRequisiteRequest } from 'app/api/endpoints/partners/request';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { TAgentRequisiteFile, TLegalPersonRequisites } from 'app/api/endpoints/partners/type';
import { useStateCallback } from 'app/hooks';
import { allowedCountries, fileTypesUploader } from '../../../config/constants';
import { TPartnersHistory } from '../../../config/types';
import { bikHandler, getFileHref, legalPersonContent } from '../../../helpers';
import { RequisiteFormButtons } from '../../../components/requisiteFormButtons';
import { limitErrorMessage, requiredMessage, transformPhoneValue, transformValue, wrongFullNameMessage } from '../../../helpers/validationSchema';

import globalStyle from '../globalStyle.module.css';

const partialLegalPersonRequisiteSchema = Yup.object({
    organizationName: Yup.string(),
    headFullName: Yup.string(),
    headPosition: Yup.string(),
    legalAddress: Yup.string(),
    phoneNumber: Yup.string(),
    ogrn: Yup.string(),
    kpp: Yup.string(),
    inn: Yup.string(),
    settlementAccount: Yup.string(),
    bik: Yup.string(),
    bankName: Yup.string(),
    correspondentAccount: Yup.string(),
    addressForSendingDocuments: Yup.string(),
});

const legalPersonRequisiteSchema = Yup.object({
    organizationName: Yup.string().required(requiredMessage),
    headFullName: Yup.string().min(5, wrongFullNameMessage).max(100, wrongFullNameMessage).required(requiredMessage).matches(/([А-ЯЁ][а-яё]+[-\s]?){3,}/, wrongFullNameMessage),
    headPosition: Yup.string().required(requiredMessage),
    legalAddress: Yup.string().required(requiredMessage),
    phoneNumber: Yup.string().transform(transformPhoneValue).required(requiredMessage).min(12, 'Неверный формат номера телефона').max(14, 'Неверный формат номера телефона'),
    ogrn: Yup.string().transform(transformValue).length(13, limitErrorMessage(13)).required(requiredMessage),
    kpp: Yup.string().transform(transformValue).length(9, limitErrorMessage(9)).required(requiredMessage),
    inn: Yup.string().transform(transformValue).length(10, limitErrorMessage(10)).required(requiredMessage),
    settlementAccount: Yup.string().transform(transformValue).length(20, limitErrorMessage(20)).required(requiredMessage),
    bik: Yup.string().transform(transformValue).length(9, limitErrorMessage(9)).required(requiredMessage),
    bankName: Yup.string().required(requiredMessage),
    correspondentAccount: Yup.string().transform(transformValue).length(20, limitErrorMessage(20)).required(requiredMessage),
    addressForSendingDocuments: Yup.string().required(requiredMessage),
});

type TPartialLegalPersonRequisiteSchema = Yup.InferType<typeof partialLegalPersonRequisiteSchema>;

type TLegalPersonRequisiteForm = FloatMessageProps & {
    formData?: TLegalPersonRequisites;
    isDisabled?: boolean;
    agentRequisitesId?: string;
};

export const LegalPersonRequisiteForm = withFloatMessages(({ floatMessage: { show }, formData, isDisabled, agentRequisitesId }: TLegalPersonRequisiteForm) => {
    const history = useHistory<TPartnersHistory>();
    const [isLoading, setIsLoading] = useState(false);
    const [innScan, setInnScan] = useState<File | null>(null);
    const [certificateScan, setCertificateScan] = useState<File | null>(null);
    const [checkboxValue, setCheckboxValue] = useState(false);

    const [innFormDataScan, setInnFormDataScan] = useState<TAgentRequisiteFile>();
    const [certificateFormDataScan, setCertificateFormDataScan] = useState<TAgentRequisiteFile>();

    const [status, setStatus] = useStateCallback<EAgentRequisitesStatus>(EAgentRequisitesStatus.undefined);

    useEffect(() => {
        if (formData) {
            if (formData.addressForSendingDocuments === formData.legalAddress) {
                setCheckboxValue(true);
            }

            const innFile = formData.files.find(file => file.agentRequisitesFileType === EAgentRequisiteFileType.scanCopyInn);
            const certificateFile = formData.files.find(file => file.agentRequisitesFileType === EAgentRequisiteFileType.scanCopyCertificateRegistrationLegalEntity);

            if (innFile) {
                setInnScan(getFileFromBase64(innFile.content as string, innFile.fileName, innFile.contentType));
                setInnFormDataScan(innFile);
            }

            if (certificateFile) {
                setCertificateScan(getFileFromBase64(certificateFile.content as string, certificateFile.fileName, certificateFile.contentType));
                setCertificateFormDataScan(certificateFile);
            }
        }
    }, [formData, show]);

    const setStateHandler = (st: EAgentRequisitesStatus) => {
        setStatus(st, () => {
            void formik.submitForm();
        });
    };

    const schema = Yup.lazy(() => {
        if (status !== EAgentRequisitesStatus.draft && status !== EAgentRequisitesStatus.undefined) {
            return legalPersonRequisiteSchema;
        }

        return partialLegalPersonRequisiteSchema;
    });

    const formik = useFormik<TPartialLegalPersonRequisiteSchema>({
        initialValues: {
            organizationName: formData?.organizationName ?? '',
            headFullName: formData?.headFullName ?? '',
            headPosition: formData?.headPosition ?? '',
            legalAddress: formData?.legalAddress ?? '',
            phoneNumber: formData?.phoneNumber ?? '',
            ogrn: formData?.ogrn ?? '',
            kpp: formData?.kpp ?? '',
            inn: formData?.inn ?? '',
            settlementAccount: formData?.settlementAccount ?? '',
            bik: formData?.bik ?? '',
            bankName: formData?.bankName ?? '',
            correspondentAccount: formData?.correspondentAccount ?? '',
            addressForSendingDocuments: formData?.addressForSendingDocuments ?? ''
        },
        onSubmit: async values => {
            if ((innScan === null || certificateScan === null) && status !== EAgentRequisitesStatus.draft) {
                return show(MessageType.Warning, 'Приложите скан копии ваших документов');
            }

            try {
                const requestBody: TPartialAgentRequisiteRequest = {
                    accountId: contextAccountId(),
                    id: agentRequisitesId,
                    agentRequisitesStatus: status,
                    agentRequisitesType: EAgentRequisitesType.legalPersonRequisites,
                    creationDate: dayjs(new Date()).format('YYYY-MM-DDTHH:mm:ss'),
                    isEditMode: true,
                    soleProprietorRequisites: null,
                    physicalPersonRequisites: null,
                    legalPersonRequisites: {
                        organizationName: values.organizationName,
                        headFullName: values.headFullName,
                        headPosition: values.headPosition,
                        legalAddress: values.legalAddress,
                        phoneNumber: (values.phoneNumber ?? '').replaceAll(' ', ''),
                        ogrn: transformValue(values.ogrn ?? ''),
                        kpp: transformValue(values.kpp ?? ''),
                        inn: transformValue(values.inn ?? ''),
                        bik: transformValue(values.bik ?? ''),
                        settlementAccount: transformValue(values.settlementAccount ?? ''),
                        correspondentAccount: transformValue(values.correspondentAccount ?? ''),
                        bankName: values.bankName,
                        addressForSendingDocuments: values.addressForSendingDocuments,
                        files: []
                    }
                };
                let legalPersonForm = new FormData();
                let filesSize = 0;

                if (innScan) {
                    legalPersonForm.append(legalPersonContent(requestBody.legalPersonRequisites?.files.length), innScan);
                    requestBody.legalPersonRequisites?.files.push({
                        fileName: innScan.name,
                        contentType: innScan.type,
                        agentRequisitesFileType: EAgentRequisiteFileType.scanCopyInn,
                        cloudFileId: innFormDataScan?.cloudFileId
                    });
                    filesSize += innScan.size;
                }

                if (certificateScan) {
                    legalPersonForm.append(legalPersonContent(requestBody.legalPersonRequisites?.files.length), certificateScan);
                    requestBody.legalPersonRequisites?.files.push({
                        fileName: certificateScan.name,
                        contentType: certificateScan.type,
                        agentRequisitesFileType: EAgentRequisiteFileType.scanCopyCertificateRegistrationLegalEntity,
                        cloudFileId: certificateFormDataScan?.cloudFileId
                    });
                    filesSize += certificateScan.size;
                }

                if (filesSize / (1024 ** 2) > 14) {
                    return show(MessageType.Warning, 'Размер загружаемых файлов оказался слишком большим. Общее ограничение размера файлов составляет 14 мегабайт');
                }

                legalPersonForm = objectToFormData(requestBody, legalPersonForm);
                setIsLoading(true);
                const { error } = await FETCH_API.PARTNERS.createAgentRequisite(legalPersonForm, formData ? WebVerb.PUT : WebVerb.POST);
                setIsLoading(false);

                if (error && error !== 'Успешно!') {
                    return show(MessageType.Error, error);
                }

                show(MessageType.Success, 'Реквизиты успешно созданы');
                history.push(AppRoutes.partners.toPartners, { activeTabIndex: 3 });
            } catch (err: unknown) {
                show(MessageType.Error, err);
            }
        },
        validateOnBlur: true,
        validateOnChange: true,
        validationSchema: schema
    });

    useEffect(() => {
        if (!formik.isSubmitting) {
            setStatus(EAgentRequisitesStatus.undefined);
        }
    }, [formik.isSubmitting, formik.submitCount, setStatus]);

    useEffect(() => {
        if (formik.values.addressForSendingDocuments !== formik.values.legalAddress && checkboxValue) {
            setCheckboxValue(false);
        }
    }, [checkboxValue, formik.values.addressForSendingDocuments, formik.values.legalAddress]);

    const innScanHandler = (file: File) => {
        setInnScan(file);

        if (innFormDataScan) {
            setInnFormDataScan(undefined);
        }
    };

    const innScanDeleteHandler = () => {
        setInnScan(null);
    };

    const innScanOpenHandler = async () => {
        window.open(await getFileHref(innFormDataScan?.cloudFileId ?? ''), '_blank');
    };

    const certificateScanHandler = (file: File) => {
        setCertificateScan(file);
    };

    const certificateScanDeleteHandler = () => {
        setCertificateScan(null);

        if (certificateFormDataScan) {
            setCertificateFormDataScan(undefined);
        }
    };

    const certificateScanOpenHandler = async () => {
        window.open(await getFileHref(certificateFormDataScan?.cloudFileId ?? ''), '_blank');
    };

    const phoneNumberHandler = (value: string) => {
        formik.setFieldValue('phoneNumber', value, true);
    };

    const phoneNumberHandlerBlur = () => {
        formik.setFieldTouched('phoneNumber', true, true);
    };

    const innHandler = async (ev: React.ChangeEvent<HTMLInputElement>) => {
        const { target: { value } } = ev;
        await formik.setFieldValue('inn', value, true);

        if (transformValue(value).length === 10) {
            setIsLoading(true);
            const { data, error } = await FETCH_API.PARTNERS.getLegalEntityDetails({ inn: transformValue(value) });

            if (error) {
                show(MessageType.Error, error);
            } else if (data && data.rawData) {
                if (data.rawData.errorMessage) {
                    show(MessageType.Info, data.rawData.errorMessage);
                } else {
                    await formik.setFieldValue('organizationName', data.rawData.companyName, true);
                    await formik.setFieldValue('kpp', data.rawData.kpp, true);
                    await formik.setFieldValue('ogrn', data.rawData.ogrn, true);
                    await formik.setFieldValue('legalAddress', data.rawData.address, true);
                    await formik.setFieldValue('headFullName', data.rawData.headFullName, true);
                    await formik.setFieldValue('headPosition', data.rawData.headPosition, true);
                }
            }

            setIsLoading(false);
        }
    };

    const checkboxHandler = <TValue, >(_: string, newValue: TValue, __: TValue) => {
        setCheckboxValue(newValue as boolean);

        if (newValue) {
            formik.setFieldValue('addressForSendingDocuments', formik.values.legalAddress);
        }
    };

    return (
        <>
            { isLoading && <LoadingBounce /> }
            <form className={ globalStyle.form }>
                <FormAndLabel isSmallFormat={ true } label="ИНН (после ввода ИНН реквизиты компании заполнятся автоматически)">
                    <InputMask
                        disabled={ isDisabled }
                        mask="999-999-999-9"
                        alwaysShowMask={ true }
                        onChange={ innHandler }
                        onBlur={ formik.handleBlur }
                        name="inn"
                        value={ formik.values.inn }
                    >
                        { (inputProps: Props & TextFieldProps) =>
                            <TextField
                                className={ isDisabled ? globalStyle.maskDisabled : '' }
                                { ...inputProps }
                                autoComplete="off"
                                fullWidth={ true }
                                error={ formik.touched.inn && !!formik.errors.inn }
                                helperText={ formik.touched.inn && formik.errors.inn }
                            />
                        }
                    </InputMask>
                </FormAndLabel>
                <FormAndLabel isSmallFormat={ true } label="Название организации">
                    <TextField
                        disabled={ isDisabled }
                        fullWidth={ true }
                        autoComplete="off"
                        placeholder="Например: ООО 'Рога и копыта'"
                        name="organizationName"
                        value={ formik.values.organizationName }
                        onChange={ formik.handleChange }
                        onBlur={ formik.handleBlur }
                        error={ formik.touched.organizationName && !!formik.errors.organizationName }
                        helperText={ formik.touched.organizationName && formik.errors.organizationName }
                    />
                </FormAndLabel>
                <FormAndLabel isSmallFormat={ true } label="ФИО руководителя">
                    <TextField
                        disabled={ isDisabled }
                        fullWidth={ true }
                        autoComplete="off"
                        placeholder="Например: Иванов Иван Иванович"
                        name="headFullName"
                        value={ formik.values.headFullName }
                        onChange={ formik.handleChange }
                        onBlur={ formik.handleBlur }
                        error={ formik.touched.headFullName && !!formik.errors.headFullName }
                        helperText={ formik.touched.headFullName && formik.errors.headFullName }
                    />
                </FormAndLabel>
                <FormAndLabel isSmallFormat={ true } label="Должность руководителя">
                    <TextField
                        disabled={ isDisabled }
                        fullWidth={ true }
                        autoComplete="off"
                        placeholder="Например: Генеральный директор"
                        name="headPosition"
                        value={ formik.values.headPosition }
                        onChange={ formik.handleChange }
                        onBlur={ formik.handleBlur }
                        error={ formik.touched.headPosition && !!formik.errors.headPosition }
                        helperText={ formik.touched.headPosition && formik.errors.headPosition }
                    />
                </FormAndLabel>
                <FormAndLabel isSmallFormat={ true } label="Телефон">
                    <MuiTelInput
                        disabled={ isDisabled }
                        fullWidth={ true }
                        name="phoneNumber"
                        autoComplete="off"
                        onChange={ phoneNumberHandler }
                        onBlur={ phoneNumberHandlerBlur }
                        value={ formik.values.phoneNumber }
                        preferredCountries={ allowedCountries }
                        defaultCountry="RU"
                        langOfCountryName="RU"
                        error={ formik.touched.phoneNumber && !!formik.errors.phoneNumber }
                        helperText={ formik.touched.phoneNumber && formik.errors.phoneNumber }
                    />
                </FormAndLabel>
                <FormAndLabel isSmallFormat={ true } label="ОГРН">
                    <InputMask
                        disabled={ isDisabled }
                        mask="999-999-999-999-9"
                        alwaysShowMask={ true }
                        onChange={ formik.handleChange }
                        onBlur={ formik.handleBlur }
                        name="ogrn"
                        value={ formik.values.ogrn }
                    >
                        { (inputProps: Props & TextFieldProps) =>
                            <TextField
                                className={ isDisabled ? globalStyle.maskDisabled : '' }
                                { ...inputProps }
                                autoComplete="off"
                                fullWidth={ true }
                                error={ formik.touched.ogrn && !!formik.errors.ogrn }
                                helperText={ formik.touched.ogrn && formik.errors.ogrn }
                            />
                        }
                    </InputMask>
                </FormAndLabel>
                <FormAndLabel isSmallFormat={ true } label="КПП">
                    <InputMask
                        disabled={ isDisabled }
                        mask="9999-99-999"
                        alwaysShowMask={ true }
                        onChange={ formik.handleChange }
                        onBlur={ formik.handleBlur }
                        name="kpp"
                        value={ formik.values.kpp }
                    >
                        { (inputProps: Props & TextFieldProps) =>
                            <TextField
                                className={ isDisabled ? globalStyle.maskDisabled : '' }
                                { ...inputProps }
                                autoComplete="off"
                                fullWidth={ true }
                                error={ formik.touched.kpp && !!formik.errors.kpp }
                                helperText={ formik.touched.kpp && formik.errors.kpp }
                            />
                        }
                    </InputMask>
                </FormAndLabel>
                <FormAndLabel isSmallFormat={ true } label="Юридический адрес">
                    <TextField
                        disabled={ isDisabled }
                        fullWidth={ true }
                        autoComplete="off"
                        placeholder="Например: 40404 г. Москва, ул. Набережная 4"
                        name="legalAddress"
                        value={ formik.values.legalAddress }
                        onChange={ formik.handleChange }
                        onBlur={ formik.handleBlur }
                        error={ formik.touched.legalAddress && !!formik.errors.legalAddress }
                        helperText={ formik.touched.legalAddress && formik.errors.legalAddress }
                    />
                </FormAndLabel>
                <FormAndLabel
                    isSmallFormat={ true }
                    label={
                        <div className={ globalStyle.checkboxView }>
                            Адрес для отправки документов
                            <CheckBoxForm
                                isReadOnly={ isDisabled }
                                isSmallFormat={ true }
                                label="Совпадает с юридическим"
                                formName=""
                                className={ globalStyle.checkbox }
                                onValueChange={ checkboxHandler }
                                isChecked={ checkboxValue }
                            />
                        </div>
                    }
                >
                    <TextField
                        disabled={ isDisabled }
                        fullWidth={ true }
                        autoComplete="off"
                        placeholder="Например: 40404 г. Москва, ул. Набережная 4"
                        name="addressForSendingDocuments"
                        value={ formik.values.addressForSendingDocuments }
                        onChange={ formik.handleChange }
                        onBlur={ formik.handleBlur }
                        error={ formik.touched.addressForSendingDocuments && !!formik.errors.addressForSendingDocuments }
                        helperText={ formik.touched.addressForSendingDocuments && formik.errors.addressForSendingDocuments }
                    />
                </FormAndLabel>
                <FormAndLabel isSmallFormat={ true } label="БИК (после ввода БИК поля “Наименование банка” и “Корреспондентский счет” заполнятся автоматически)">
                    <InputMask
                        disabled={ isDisabled }
                        mask="99-99-99-999"
                        alwaysShowMask={ true }
                        onChange={ ev => bikHandler<TPartialLegalPersonRequisiteSchema>({
                            setIsLoading,
                            show,
                            ev,
                            setFieldValue: formik.setFieldValue
                        }) }
                        onBlur={ formik.handleBlur }
                        name="bik"
                        value={ formik.values.bik }
                    >
                        { (inputProps: Props & TextFieldProps) =>
                            <TextField
                                className={ isDisabled ? globalStyle.maskDisabled : '' }
                                { ...inputProps }
                                autoComplete="off"
                                fullWidth={ true }
                                error={ formik.touched.bik && !!formik.errors.bik }
                                helperText={ formik.touched.bik && formik.errors.bik }
                            />
                        }
                    </InputMask>
                </FormAndLabel>
                <FormAndLabel isSmallFormat={ true } label="Наименование банка">
                    <TextField
                        disabled={ isDisabled }
                        fullWidth={ true }
                        autoComplete="off"
                        name="bankName"
                        value={ formik.values.bankName }
                        onChange={ formik.handleChange }
                        onBlur={ formik.handleBlur }
                        error={ formik.touched.bankName && !!formik.errors.bankName }
                        helperText={ formik.touched.bankName && formik.errors.bankName }
                    />
                </FormAndLabel>
                <FormAndLabel isSmallFormat={ true } label="Корреспондентский счет">
                    <InputMask
                        disabled={ isDisabled }
                        mask="9999-9999-9999-9999-9999"
                        alwaysShowMask={ true }
                        onChange={ formik.handleChange }
                        onBlur={ formik.handleBlur }
                        name="correspondentAccount"
                        value={ formik.values.correspondentAccount }
                    >
                        { (inputProps: Props & TextFieldProps) =>
                            <TextField
                                className={ isDisabled ? globalStyle.maskDisabled : '' }
                                { ...inputProps }
                                autoComplete="off"
                                fullWidth={ true }
                                error={ formik.touched.correspondentAccount && !!formik.errors.correspondentAccount }
                                helperText={ formik.touched.correspondentAccount && formik.errors.correspondentAccount }
                            />
                        }
                    </InputMask>
                </FormAndLabel>
                <FormAndLabel isSmallFormat={ true } label="Расчетный счет">
                    <InputMask
                        disabled={ isDisabled }
                        mask="9999-9999-9999-9999-9999"
                        alwaysShowMask={ true }
                        onChange={ formik.handleChange }
                        onBlur={ formik.handleBlur }
                        name="settlementAccount"
                        value={ formik.values.settlementAccount }
                    >
                        { (inputProps: Props & TextFieldProps) =>
                            <TextField
                                className={ isDisabled ? globalStyle.maskDisabled : '' }
                                { ...inputProps }
                                autoComplete="off"
                                fullWidth={ true }
                                error={ formik.touched.settlementAccount && !!formik.errors.settlementAccount }
                                helperText={ formik.touched.settlementAccount && formik.errors.settlementAccount }
                            />
                        }
                    </InputMask>
                </FormAndLabel>
                <FormAndLabel label="Приложите скан копии Ваших документов">
                    <div className={ globalStyle.files }>
                        <FormAndLabel isSmallFormat={ true } label="Скан свидетельства о регистрации Юр. лица (если свидетельство выдавалось)">
                            <ConfigFileUploader
                                disabled={ isDisabled }
                                limit={ 1 }
                                required={ true }
                                uploadedFiles={ certificateScan && [certificateScan] }
                                fileTypesArr={ fileTypesUploader }
                                callBack={ certificateScanHandler }
                                deleteUploadFile={ certificateScanDeleteHandler }
                                openFile={ certificateScanOpenHandler }
                            />
                        </FormAndLabel>
                        <FormAndLabel isSmallFormat={ true } label="Скан ИНН">
                            <ConfigFileUploader
                                disabled={ isDisabled }
                                limit={ 1 }
                                required={ true }
                                uploadedFiles={ innScan && [innScan] }
                                fileTypesArr={ fileTypesUploader }
                                callBack={ innScanHandler }
                                deleteUploadFile={ innScanDeleteHandler }
                                openFile={ innScanOpenHandler }
                            />
                        </FormAndLabel>
                    </div>
                </FormAndLabel>
            </form>
            <RequisiteFormButtons setStatus={ setStateHandler } isDisabled={ isDisabled } />
        </>
    );
});