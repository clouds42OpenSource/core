import { IconButton } from '@mui/material';
import InfoIcon from '@mui/icons-material/Info';
import Tooltip from '@mui/material/Tooltip';
import React from 'react';

import styles from './style.module.css';

type TRequisiteTooltip = {
    text: string;
};

const tooltipText = 'Информацию о ваших банковских реквизитах (БИК, Наименование банка, Корреспондентский счет, Расчетный счет) вы можете получить через онлайн-банкинг, на горячей линии банка или в отделении банка';

export const RequisiteTooltip = ({ text }: TRequisiteTooltip) =>
    <>
        { text }
        <Tooltip
            className={ styles.tooltip }
            title={ tooltipText }
        >
            <IconButton className={ styles.button }>
                <InfoIcon />
            </IconButton>
        </Tooltip>
    </>;