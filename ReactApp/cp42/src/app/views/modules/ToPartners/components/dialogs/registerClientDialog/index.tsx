import { accountId } from 'app/api';
import { registerUrlFrame } from 'app/api/endpoints';
import { RegistrationConfig } from 'app/api/endpoints/auth/response/RegistrationConfig';
import { useAppSelector } from 'app/hooks';
import { SignUpParsedConfig } from 'app/views/modules/SignUp/types/SignUpParsedConfig';
import Cookies from 'js-cookie';
import React, { memo, useCallback } from 'react';
import { DialogView } from 'app/views/components/controls/Dialog/views/DialogView/Index';
import styles from './style.module.css';

type TRegisterClientDialog = {
    isOpen: boolean;
    setIsOpen: React.Dispatch<React.SetStateAction<boolean>>;
};

const RegisterClient = ({ isOpen, setIsOpen }: TRegisterClientDialog) => {
    const { settings: { currentContextInfo: { locale } } } = useAppSelector(state => state.Global.getCurrentSessionSettingsReducer);
    const parseQueryString = useCallback((): SignUpParsedConfig => {
        const parsedData: SignUpParsedConfig = {};
        const registrationConfig: RegistrationConfig = {
            cloudService: '',
            cloudServiceId: '',
            configuration1C: '',
            referralAccountId: accountId(),
            registrationSource: 0
        };

        const searchPattern = /src=([^|]+)/;
        const userSource = Cookies.get('sbjs_first');
        const match = userSource?.match(searchPattern);
        parsedData.userSource = match ? match[1] : 'Прямой переход по ссылке';
        parsedData.registrationConfig = registrationConfig;

        return parsedData;
    }, []);

    const { userSource, registrationConfig } = { ...parseQueryString() };

    const cancelClickHandler = () => {
        setIsOpen(false);
    };

    return (
        <DialogView
            title="Регистрация клиента"
            isTitleSmall={ true }
            isOpen={ isOpen }
            dialogWidth="xs"
            onCancelClick={ cancelClickHandler }
            className={ styles.dialog }
            contentClassName={ styles.content }
        >
            <iframe
                title="registation form"
                src={ `${ registerUrlFrame }?localeName=${ locale }&${
                    new URLSearchParams({ userinfo: JSON.stringify({
                        cloudService: registrationConfig?.cloudService,
                        cloudServiceId: registrationConfig?.cloudServiceId,
                        configuration1C: registrationConfig?.configuration1C,
                        referralAccountId: registrationConfig?.referralAccountId,
                        registrationSource: registrationConfig?.registrationSource,
                        userSource
                    }) }).toString()
                }` }
                width="100%"
                height="520px"
                scrolling="no"
                frameBorder={ 0 }
            >
                Ваш браузер не поддерживает плавающие фреймы!
            </iframe>
        </DialogView>
    );
};

export const RegisterClientDialog = memo(RegisterClient);