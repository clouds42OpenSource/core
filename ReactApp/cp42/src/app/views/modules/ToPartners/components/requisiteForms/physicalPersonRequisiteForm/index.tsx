import { TextField, TextFieldProps } from '@mui/material';
import { DatePicker, LocalizationProvider } from '@mui/x-date-pickers';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { contextAccountId } from 'app/api';
import { EAgentRequisiteFileType, EAgentRequisitesStatus, EAgentRequisitesType } from 'app/api/endpoints/partners/enum';
import { TPartialAgentRequisiteRequest } from 'app/api/endpoints/partners/request';
import { TAgentRequisiteFile, TPhysicalPersonRequisites } from 'app/api/endpoints/partners/type';
import { FETCH_API } from 'app/api/useFetchApi';
import { AppRoutes } from 'app/AppRoutes';
import { useStateCallback } from 'app/hooks';
import { COLORS, getFileFromBase64 } from 'app/utils';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { CheckBoxForm } from 'app/views/components/controls/forms';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { ConfigFileUploader } from 'app/views/modules/_common/components/ConfigFileUploader';
import { objectToFormData } from 'app/web/common';
import { WebVerb } from 'core/requestSender/enums';
import ruLocale from 'date-fns/locale/ru';
import dayjs from 'dayjs';
import { useFormik } from 'formik';
import { useEffect, useState } from 'react';
import InputMask, { Props } from 'react-input-mask';
import { useHistory } from 'react-router';
import * as Yup from 'yup';
import { RequisiteFormButtons } from '../../../components/requisiteFormButtons';
import { RequisiteTooltip } from '../../../components/tooltip';
import { fileTypesUploader } from '../../../config/constants';
import { TPartnersHistory } from '../../../config/types';
import { bikHandler, getFileHref, physicalPersonContent } from '../../../helpers';
import { limitErrorMessage, requiredMessage, transformValue, wrongFullNameMessage } from '../../../helpers/validationSchema';

import globalStyle from '../globalStyle.module.css';

const partialIndividualRequisiteSchema = Yup.object({
    fullName: Yup.string(),
    dateOfBirth: Yup.date().required(requiredMessage),
    passportSeries: Yup.string(),
    passportNumber: Yup.string(),
    whomIssuedPassport: Yup.string(),
    passportDateOfIssue: Yup.date().required(requiredMessage),
    registrationAddress: Yup.string(),
    inn: Yup.string(),
    snils: Yup.string(),
    settlementAccount: Yup.string(),
    bik: Yup.string(),
    bankName: Yup.string(),
    correspondentAccount: Yup.string(),
    addressForSendingDocuments: Yup.string(),
});

const individualRequisiteSchema = Yup.object({
    fullName: Yup.string().min(5, wrongFullNameMessage).max(100, wrongFullNameMessage).required(requiredMessage).matches(/([А-ЯЁ][а-яё]+[-\s]?){3,}/, wrongFullNameMessage),
    dateOfBirth: Yup.date().required(requiredMessage),
    passportSeries: Yup.string().transform(transformValue).length(4, limitErrorMessage(4)).required(requiredMessage),
    passportNumber: Yup.string().transform(transformValue).length(6, limitErrorMessage(6)).required(requiredMessage),
    whomIssuedPassport: Yup.string().required(requiredMessage),
    passportDateOfIssue: Yup.date().required(requiredMessage),
    registrationAddress: Yup.string().required(requiredMessage),
    inn: Yup.string().transform(transformValue).length(12, limitErrorMessage(12)).required(requiredMessage),
    snils: Yup.string().transform(transformValue).length(11, limitErrorMessage(11)).required(requiredMessage),
    settlementAccount: Yup.string().transform(transformValue).length(20, limitErrorMessage(20)).required(requiredMessage),
    bik: Yup.string().transform(transformValue).length(9, limitErrorMessage(9)).required(requiredMessage),
    bankName: Yup.string().required(requiredMessage),
    correspondentAccount: Yup.string().transform(transformValue).length(20, limitErrorMessage(20)).required(requiredMessage),
    addressForSendingDocuments: Yup.string().required(requiredMessage),
});

type TPartialIndividualRequisiteSchema = Yup.InferType<typeof partialIndividualRequisiteSchema>;

type TPhysicalPersonRequisiteForm = FloatMessageProps & {
    formData?: TPhysicalPersonRequisites;
    isDisabled?: boolean;
    agentRequisitesId?: string;
};

export const PhysicalPersonRequisiteForm = withFloatMessages(({ floatMessage: { show }, formData, isDisabled, agentRequisitesId }: TPhysicalPersonRequisiteForm) => {
    const history = useHistory<TPartnersHistory>();
    const [passportScan, setPassportScan] = useState<File[]>([]);
    const [innScan, setInnScan] = useState<File | null>(null);
    const [snilsScan, setSnilsScan] = useState<File | null>(null);
    const [isLoading, setIsLoading] = useState(false);
    const [checkboxValue, setCheckboxValue] = useState(false);

    const [innFormDataScan, setInnFormDataScan] = useState<TAgentRequisiteFile>();
    const [snilsFormDataScan, setSnilsFormDataScan] = useState<TAgentRequisiteFile>();
    const [passportFormDataScan, setPassportFormDataScan] = useState<TAgentRequisiteFile[]>([]);

    const [status, setStatus] = useStateCallback<EAgentRequisitesStatus>(EAgentRequisitesStatus.onCheck);

    useEffect(() => {
        if (formData) {
            if (formData.addressForSendingDocuments === formData.registrationAddress) {
                setCheckboxValue(true);
            }

            const innFile = formData.files.find(file => file.agentRequisitesFileType === EAgentRequisiteFileType.scanCopyInn);
            const snilsFile = formData.files.find(file => file.agentRequisitesFileType === EAgentRequisiteFileType.scanCopySnils);
            const passportFile = formData.files.filter(file => file.agentRequisitesFileType === EAgentRequisiteFileType.scanCopyPassportAndRegistration);

            if (innFile) {
                setInnScan(getFileFromBase64(innFile.content as string, innFile.fileName, innFile.contentType));
                setInnFormDataScan(innFile);
            }

            if (snilsFile) {
                setSnilsScan(getFileFromBase64(snilsFile.content as string, snilsFile.fileName, snilsFile.contentType));
                setSnilsFormDataScan(snilsFile);
            }

            if (passportFile.length > 0) {
                setPassportScan(passportFile.map(file => getFileFromBase64(file.content as string, file.fileName, file.contentType)));
                setPassportFormDataScan(passportFile);
            }
        }
    }, [formData, show]);

    const setStateHandler = (st: EAgentRequisitesStatus) => {
        setStatus(st, () => {
            void formik.submitForm();
        });
    };

    const schema = Yup.lazy(() => {
        if (status !== EAgentRequisitesStatus.draft && status !== EAgentRequisitesStatus.undefined) {
            return individualRequisiteSchema;
        }

        return partialIndividualRequisiteSchema;
    });

    const formik = useFormik<TPartialIndividualRequisiteSchema>({
        initialValues: {
            fullName: formData?.fullName ?? '',
            dateOfBirth: formData?.dateOfBirth ? new Date(formData.dateOfBirth) : new Date(),
            passportSeries: formData?.passportSeries ?? '',
            passportNumber: formData?.passportNumber ?? '',
            whomIssuedPassport: formData?.whomIssuedPassport ?? '',
            passportDateOfIssue: formData?.passportDateOfIssue ? new Date(formData.passportDateOfIssue) : new Date(),
            registrationAddress: formData?.registrationAddress ?? '',
            inn: formData?.inn ?? '',
            snils: formData?.snils ?? '',
            settlementAccount: formData?.settlementAccount ?? '',
            bik: formData?.bik ?? '',
            bankName: formData?.bankName ?? '',
            correspondentAccount: formData?.correspondentAccount ?? '',
            addressForSendingDocuments: formData?.addressForSendingDocuments ?? ''
        },
        onSubmit: async values => {
            if ((innScan === null || snilsScan === null || passportScan.length < 1) && status !== EAgentRequisitesStatus.draft) {
                return show(MessageType.Warning, 'Приложите скан копии ваших документов');
            }

            try {
                const requestBody: TPartialAgentRequisiteRequest = {
                    accountId: contextAccountId(),
                    id: agentRequisitesId,
                    agentRequisitesStatus: status,
                    agentRequisitesType: EAgentRequisitesType.physicalPersonRequisites,
                    creationDate: dayjs(new Date()).format('YYYY-MM-DDTHH:mm:ss'),
                    isEditMode: true,
                    soleProprietorRequisites: null,
                    legalPersonRequisites: null,
                    physicalPersonRequisites: {
                        fullName: values.fullName,
                        dateOfBirth: dayjs(values.dateOfBirth).format('DD.MM.YYYY'),
                        passportSeries: values.passportSeries,
                        passportNumber: values.passportNumber,
                        whomIssuedPassport: values.whomIssuedPassport,
                        passportDateOfIssue: dayjs(values.passportDateOfIssue).format('DD.MM.YYYY'),
                        registrationAddress: values.registrationAddress,
                        inn: transformValue(values.inn ?? ''),
                        snils: transformValue(values.snils ?? ''),
                        settlementAccount: transformValue(values.settlementAccount ?? ''),
                        bik: transformValue(values.bik ?? ''),
                        bankName: values.bankName,
                        correspondentAccount: transformValue(values.correspondentAccount ?? ''),
                        addressForSendingDocuments: values.addressForSendingDocuments,
                        files: []
                    },
                };
                let physicalPersonForm = new FormData();
                let filesSize = 0;

                if (passportScan[0]) {
                    physicalPersonForm.append(physicalPersonContent(requestBody.physicalPersonRequisites?.files.length), passportScan[0]);
                    requestBody.physicalPersonRequisites?.files.push({
                        fileName: passportScan[0].name,
                        contentType: passportScan[0].type,
                        agentRequisitesFileType: EAgentRequisiteFileType.scanCopyPassportAndRegistration,
                        cloudFileId: passportFormDataScan.find(file => file.fileName === passportScan[0].name)?.cloudFileId
                    });
                    filesSize += passportScan[0].size;
                }

                if (passportScan[1]) {
                    physicalPersonForm.append(physicalPersonContent(requestBody.physicalPersonRequisites?.files.length), passportScan[1]);
                    requestBody.physicalPersonRequisites?.files.push({
                        fileName: passportScan[1].name,
                        contentType: passportScan[1].type,
                        agentRequisitesFileType: EAgentRequisiteFileType.scanCopyPassportAndRegistration,
                        cloudFileId: passportFormDataScan.find(file => file.fileName === passportScan[1].name)?.cloudFileId
                    });
                    filesSize += passportScan[1].size;
                }

                if (passportScan[2]) {
                    physicalPersonForm.append(physicalPersonContent(requestBody.physicalPersonRequisites?.files.length), passportScan[2]);
                    requestBody.physicalPersonRequisites?.files.push({
                        fileName: passportScan[2].name,
                        contentType: passportScan[2].type,
                        agentRequisitesFileType: EAgentRequisiteFileType.scanCopyPassportAndRegistration,
                        cloudFileId: passportFormDataScan.find(file => file.fileName === passportScan[2].name)?.cloudFileId
                    });
                    filesSize += passportScan[2].size;
                }

                if (innScan) {
                    physicalPersonForm.append(physicalPersonContent(requestBody.physicalPersonRequisites?.files.length), innScan);
                    requestBody.physicalPersonRequisites?.files.push({
                        fileName: innScan.name,
                        contentType: innScan.type,
                        agentRequisitesFileType: EAgentRequisiteFileType.scanCopyInn,
                        cloudFileId: innFormDataScan?.cloudFileId
                    });
                    filesSize += innScan.size;
                }

                if (snilsScan) {
                    physicalPersonForm.append(physicalPersonContent(requestBody.physicalPersonRequisites?.files.length), snilsScan);
                    requestBody.physicalPersonRequisites?.files.push({
                        fileName: snilsScan.name,
                        contentType: snilsScan.type,
                        agentRequisitesFileType: EAgentRequisiteFileType.scanCopySnils,
                        cloudFileId: snilsFormDataScan?.cloudFileId
                    });
                    filesSize += snilsScan.size;
                }

                if (filesSize / (1024 ** 2) > 14) {
                    return show(MessageType.Warning, 'Размер загружаемых файлов оказался слишком большим. Общее ограничение размера файлов составляет 14 мегабайт');
                }

                physicalPersonForm = objectToFormData(requestBody, physicalPersonForm);
                setIsLoading(true);
                const { error } = await FETCH_API.PARTNERS.createAgentRequisite(physicalPersonForm, formData ? WebVerb.PUT : WebVerb.POST);
                setIsLoading(false);

                if (error && error !== 'Успешно!') {
                    return show(MessageType.Error, error);
                }

                show(MessageType.Success, 'Реквизиты успешно созданы');
                history.push(AppRoutes.partners.toPartners, { activeTabIndex: 3 });
            } catch (err: unknown) {
                show(MessageType.Error, err);
            }
        },
        validateOnBlur: true,
        validateOnChange: true,
        validationSchema: schema
    });

    useEffect(() => {
        if (formik.values.addressForSendingDocuments !== formik.values.registrationAddress && checkboxValue) {
            setCheckboxValue(false);
        }
    }, [checkboxValue, formik.values.addressForSendingDocuments, formik.values.registrationAddress]);

    useEffect(() => {
        if (!formik.isSubmitting) {
            setStatus(EAgentRequisitesStatus.undefined);
        }
    }, [formik.isSubmitting, formik.submitCount, setStatus]);

    const checkboxHandler = <TValue, >(_: string, newValue: TValue, __: TValue) => {
        setCheckboxValue(newValue as boolean);

        if (newValue) {
            formik.setFieldValue('addressForSendingDocuments', formik.values.registrationAddress);
        }
    };

    const innScanHandler = (file: File) => {
        setInnScan(file);
    };

    const innScanDeleteHandler = () => {
        setInnScan(null);

        if (innFormDataScan) {
            setInnFormDataScan(undefined);
        }
    };

    const innScanOpenHandler = async () => {
        window.open(await getFileHref(innFormDataScan?.cloudFileId ?? ''), '_blank');
    };

    const snilsScanHandler = (file: File) => {
        setSnilsScan(file);
    };

    const snilsScanDeleteHandler = () => {
        setSnilsScan(null);

        if (snilsFormDataScan) {
            setSnilsFormDataScan(undefined);
        }
    };

    const snilsScanOpenHandler = async () => {
        window.open(await getFileHref(snilsFormDataScan?.cloudFileId ?? ''), '_blank');
    };

    const passportScanHandler = (file: File) => {
        if (passportScan.find(scan => scan.name === file.name)) {
            show(MessageType.Warning, 'Вы уже загрузили файл с таким имененем');
        } else {
            setPassportScan([...passportScan, file]);
        }
    };

    const passportScanOpenHandler = async (fileName: string, _: number) => {
        window.open(await getFileHref(passportFormDataScan.find(file => file.fileName === fileName)?.cloudFileId ?? ''), '_blank');
    };

    const passportScanDeleteHandler = (fileName: string, _: number) => {
        if (passportScan) {
            setPassportScan(passportScan.filter(scan => scan.name !== fileName));
        }

        if (passportFormDataScan) {
            setPassportFormDataScan(passportFormDataScan.filter(file => file.fileName !== fileName));
        }
    };

    const handleDateChange = (name: string, date: Date | null) => {
        if (date instanceof Date) {
            formik.setFieldValue(name, date);
        }
    };

    return (
        <>
            { isLoading && <LoadingBounce /> }
            <form className={ globalStyle.form }>
                <FormAndLabel isSmallFormat={ true } label="ФИО">
                    <TextField
                        disabled={ isDisabled }
                        fullWidth={ true }
                        autoComplete="off"
                        placeholder="Например: Иванов Иван Иванович"
                        name="fullName"
                        value={ formik.values.fullName }
                        onChange={ formik.handleChange }
                        onBlur={ formik.handleBlur }
                        error={ formik.touched.fullName && !!formik.errors.fullName }
                        helperText={ formik.touched.fullName && formik.errors.fullName }
                    />
                </FormAndLabel>
                <FormAndLabel isSmallFormat={ true } label="Дата рождения">
                    <LocalizationProvider dateAdapter={ AdapterDateFns } adapterLocale={ ruLocale }>
                        <DatePicker
                            disabled={ isDisabled }
                            format="dd.MM.yyyy"
                            className={ globalStyle.date }
                            sx={ { svg: { color: COLORS.main } } }
                            maxDate={ new Date() }
                            onChange={ value => handleDateChange('dateOfBirth', value) }
                            value={ formik.values.dateOfBirth }
                        />
                    </LocalizationProvider>
                </FormAndLabel>
                <FormAndLabel isSmallFormat={ true } label="Серия паспорта">
                    <InputMask
                        disabled={ isDisabled }
                        mask="9999"
                        alwaysShowMask={ true }
                        onChange={ formik.handleChange }
                        onBlur={ formik.handleBlur }
                        name="passportSeries"
                        value={ formik.values.passportSeries }
                    >
                        { (inputProps: Props & TextFieldProps) =>
                            <TextField
                                className={ isDisabled ? globalStyle.maskDisabled : '' }
                                { ...inputProps }
                                autoComplete="off"
                                fullWidth={ true }
                                error={ formik.touched.passportSeries && !!formik.errors.passportSeries }
                                helperText={ formik.touched.passportSeries && formik.errors.passportSeries }
                            />
                        }
                    </InputMask>
                </FormAndLabel>
                <FormAndLabel isSmallFormat={ true } label="Номер паспорта">
                    <InputMask
                        disabled={ isDisabled }
                        mask="999999"
                        name="passportNumber"
                        value={ formik.values.passportNumber }
                        onChange={ formik.handleChange }
                        onBlur={ formik.handleBlur }
                        alwaysShowMask={ true }
                    >
                        { (inputProps: Props & TextFieldProps) =>
                            <TextField
                                className={ isDisabled ? globalStyle.maskDisabled : '' }
                                { ...inputProps }
                                autoComplete="off"
                                fullWidth={ true }
                                error={ formik.touched.passportNumber && !!formik.errors.passportNumber }
                                helperText={ formik.touched.passportNumber && formik.errors.passportNumber }
                            />
                        }
                    </InputMask>
                </FormAndLabel>
                <FormAndLabel isSmallFormat={ true } label="Кем выдан паспорт">
                    <TextField
                        disabled={ isDisabled }
                        fullWidth={ true }
                        autoComplete="off"
                        name="whomIssuedPassport"
                        value={ formik.values.whomIssuedPassport }
                        onChange={ formik.handleChange }
                        onBlur={ formik.handleBlur }
                        error={ formik.touched.whomIssuedPassport && !!formik.errors.whomIssuedPassport }
                        helperText={ formik.touched.whomIssuedPassport && formik.errors.whomIssuedPassport }
                    />
                </FormAndLabel>
                <FormAndLabel isSmallFormat={ true } label="Дата выдачи паспорта">
                    <LocalizationProvider dateAdapter={ AdapterDateFns } adapterLocale={ ruLocale }>
                        <DatePicker
                            disabled={ isDisabled }
                            format="dd.MM.yyyy"
                            className={ globalStyle.date }
                            sx={ { svg: { color: COLORS.main } } }
                            maxDate={ new Date() }
                            onChange={ value => handleDateChange('passportDateOfIssue', value) }
                            value={ formik.values.passportDateOfIssue }
                        />
                    </LocalizationProvider>
                </FormAndLabel>
                <FormAndLabel isSmallFormat={ true } label="Адрес прописки">
                    <TextField
                        disabled={ isDisabled }
                        fullWidth={ true }
                        autoComplete="off"
                        placeholder="Например: 40404 г. Москва, ул. Набережная 4"
                        name="registrationAddress"
                        value={ formik.values.registrationAddress }
                        onChange={ formik.handleChange }
                        onBlur={ formik.handleBlur }
                        error={ formik.touched.registrationAddress && !!formik.errors.registrationAddress }
                        helperText={ formik.touched.registrationAddress && formik.errors.registrationAddress }
                    />
                </FormAndLabel>
                <FormAndLabel
                    isSmallFormat={ true }
                    label={
                        <div className={ globalStyle.checkboxView }>
                            Адрес для отправки документов
                            <CheckBoxForm
                                isReadOnly={ isDisabled }
                                isSmallFormat={ true }
                                label="Совпадает с адресом прописки"
                                formName=""
                                className={ globalStyle.checkbox }
                                onValueChange={ checkboxHandler }
                                isChecked={ checkboxValue }
                            />
                        </div>
                    }
                >
                    <TextField
                        disabled={ isDisabled }
                        fullWidth={ true }
                        autoComplete="off"
                        placeholder="Например: 40404 г. Москва, ул. Набережная 4"
                        name="addressForSendingDocuments"
                        value={ formik.values.addressForSendingDocuments }
                        onChange={ formik.handleChange }
                        onBlur={ formik.handleBlur }
                        error={ formik.touched.addressForSendingDocuments && !!formik.errors.addressForSendingDocuments }
                        helperText={ formik.touched.addressForSendingDocuments && formik.errors.addressForSendingDocuments }
                    />
                </FormAndLabel>
                <FormAndLabel isSmallFormat={ true } label="ИНН">
                    <InputMask
                        disabled={ isDisabled }
                        mask="999-999-999-999"
                        alwaysShowMask={ true }
                        name="inn"
                        value={ formik.values.inn }
                        onChange={ formik.handleChange }
                        onBlur={ formik.handleBlur }
                    >
                        { (inputProps: Props & TextFieldProps) =>
                            <TextField
                                className={ isDisabled ? globalStyle.maskDisabled : '' }
                                { ...inputProps }
                                autoComplete="off"
                                fullWidth={ true }
                                error={ formik.touched.inn && !!formik.errors.inn }
                                helperText={ formik.touched.inn && formik.errors.inn }
                            />
                        }
                    </InputMask>
                </FormAndLabel>
                <FormAndLabel isSmallFormat={ true } label="СНИЛС">
                    <InputMask
                        disabled={ isDisabled }
                        mask="999-999-999-99"
                        alwaysShowMask={ true }
                        name="snils"
                        value={ formik.values.snils }
                        onChange={ formik.handleChange }
                        onBlur={ formik.handleBlur }
                    >
                        { (inputProps: Props & TextFieldProps) =>
                            <TextField
                                className={ isDisabled ? globalStyle.maskDisabled : '' }
                                { ...inputProps }
                                autoComplete="off"
                                fullWidth={ true }
                                error={ formik.touched.snils && !!formik.errors.snils }
                                helperText={ formik.touched.snils && formik.errors.snils }
                            />
                        }
                    </InputMask>
                </FormAndLabel>
                <FormAndLabel isSmallFormat={ true } label={ <RequisiteTooltip text="БИК (после ввода БИК поля “Наименование банка” и “Корреспондентский счет” заполнятся автоматически)" /> }>
                    <InputMask
                        disabled={ isDisabled }
                        mask="99-99-99-999"
                        alwaysShowMask={ true }
                        name="bik"
                        value={ formik.values.bik }
                        onChange={ ev => bikHandler<TPartialIndividualRequisiteSchema>({
                            setIsLoading,
                            show,
                            ev,
                            setFieldValue: formik.setFieldValue
                        }) }
                        onBlur={ formik.handleBlur }
                    >
                        { (inputProps: Props & TextFieldProps) =>
                            <TextField
                                className={ isDisabled ? globalStyle.maskDisabled : '' }
                                { ...inputProps }
                                autoComplete="off"
                                fullWidth={ true }
                                error={ formik.touched.bik && !!formik.errors.bik }
                                helperText={ formik.touched.bik && formik.errors.bik }
                            />
                        }
                    </InputMask>
                </FormAndLabel>
                <FormAndLabel isSmallFormat={ true } label={ <RequisiteTooltip text="Наименование банка" /> }>
                    <TextField
                        disabled={ isDisabled }
                        fullWidth={ true }
                        autoComplete="off"
                        name="bankName"
                        value={ formik.values.bankName }
                        onChange={ formik.handleChange }
                        onBlur={ formik.handleBlur }
                        error={ formik.touched.bankName && !!formik.errors.bankName }
                        helperText={ formik.touched.bankName && formik.errors.bankName }
                    />
                </FormAndLabel>
                <FormAndLabel isSmallFormat={ true } label={ <RequisiteTooltip text="Корреспондентский счет" /> }>
                    <InputMask
                        disabled={ isDisabled }
                        mask="9999-9999-9999-9999-9999"
                        alwaysShowMask={ true }
                        name="correspondentAccount"
                        value={ formik.values.correspondentAccount }
                        onChange={ formik.handleChange }
                        onBlur={ formik.handleBlur }
                    >
                        { (inputProps: Props & TextFieldProps) =>
                            <TextField
                                className={ isDisabled ? globalStyle.maskDisabled : '' }
                                { ...inputProps }
                                autoComplete="off"
                                fullWidth={ true }
                                error={ formik.touched.correspondentAccount && !!formik.errors.correspondentAccount }
                                helperText={ formik.touched.correspondentAccount && formik.errors.correspondentAccount }
                            />
                        }
                    </InputMask>
                </FormAndLabel>
                <FormAndLabel isSmallFormat={ true } label={ <RequisiteTooltip text="Расчетный счет" /> }>
                    <InputMask
                        disabled={ isDisabled }
                        mask="9999-9999-9999-9999-9999"
                        alwaysShowMask={ true }
                        name="settlementAccount"
                        value={ formik.values.settlementAccount }
                        onChange={ formik.handleChange }
                        onBlur={ formik.handleBlur }
                    >
                        { (inputProps: Props & TextFieldProps) =>
                            <TextField
                                className={ isDisabled ? globalStyle.maskDisabled : '' }
                                { ...inputProps }
                                autoComplete="off"
                                fullWidth={ true }
                                error={ formik.touched.settlementAccount && !!formik.errors.settlementAccount }
                                helperText={ formik.touched.settlementAccount && formik.errors.settlementAccount }
                            />
                        }
                    </InputMask>
                </FormAndLabel>
                <FormAndLabel label="Приложите скан копии ваших документов">
                    <div className={ globalStyle.files }>
                        <FormAndLabel isSmallFormat={ true } label="Скан паспорта (1-я страница, 2-я страница, прописка)">
                            <ConfigFileUploader
                                disabled={ isDisabled }
                                limit={ 3 }
                                required={ true }
                                deleteUploadFile={ passportScanDeleteHandler }
                                uploadedFiles={ passportScan }
                                fileTypesArr={ fileTypesUploader }
                                callBack={ passportScanHandler }
                                openFile={ passportScanOpenHandler }
                            />
                        </FormAndLabel>
                        <FormAndLabel isSmallFormat={ true } label="Скан ИНН">
                            <ConfigFileUploader
                                disabled={ isDisabled }
                                limit={ 1 }
                                required={ true }
                                uploadedFiles={ innScan && [innScan] }
                                fileTypesArr={ fileTypesUploader }
                                callBack={ innScanHandler }
                                deleteUploadFile={ innScanDeleteHandler }
                                openFile={ innScanOpenHandler }
                            />
                        </FormAndLabel>
                        <FormAndLabel isSmallFormat={ true } label="Скан СНИЛС">
                            <ConfigFileUploader
                                disabled={ isDisabled }
                                limit={ 1 }
                                required={ true }
                                uploadedFiles={ snilsScan && [snilsScan] }
                                fileTypesArr={ fileTypesUploader }
                                callBack={ snilsScanHandler }
                                deleteUploadFile={ snilsScanDeleteHandler }
                                openFile={ snilsScanOpenHandler }
                            />
                        </FormAndLabel>
                    </div>
                </FormAndLabel>
            </form>
            <RequisiteFormButtons setStatus={ setStateHandler } isDisabled={ isDisabled } />
        </>
    );
});