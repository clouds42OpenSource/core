import { REDUX_API } from 'app/api/useReduxApi';
import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { OutlinedInput } from '@mui/material';

import { ContainedButton, SuccessButton } from 'app/views/components/controls/Button';
import { DialogView } from 'app/views/components/controls/Dialog/views/DialogView/Index';
import { FETCH_API } from 'app/api/useFetchApi';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { AppReduxStoreState } from 'app/redux/types';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { TextOut } from 'app/views/components/TextOut';

import { useAppSelector } from 'app/hooks';
import styles from './style.module.css';
import dialogStyles from '../dialogStyle.module.css';

type TDispatch = {
    dispatchReceiveNavMenuThunk: () => void;
};

type TBalanceTransferDialog = FloatMessageProps & TDispatch & {
    isOpen: boolean;
    setIsOpen: React.Dispatch<React.SetStateAction<boolean>>;
    refreshData: (page: number, isNeedToRefreshSummary: boolean) => void;
};

const { getNavMenu } = REDUX_API.GLOBAL_REDUX;

const BalanceTransferDialogComponent = withFloatMessages(({ isOpen, setIsOpen, floatMessage: { show }, refreshData, dispatchReceiveNavMenuThunk }: TBalanceTransferDialog) => {
    const [sum, setSum] = useState(0);
    const [isLoading, setIsLoading] = useState(false);

    const currencySymbol = useAppSelector(state => state.PartnersState.summaryDataReducer.data?.rawData?.currencySymbol);

    const cancelClickHandler = () => {
        setIsOpen(false);
    };

    useEffect(() => {
        (async () => {
            if (isOpen) {
                setIsLoading(true);
                const { error, data } = await FETCH_API.PARTNERS.getCashOutBalance();

                if (error) {
                    show(MessageType.Error, error);
                } else if (data?.rawData) {
                    setSum(data.rawData);
                } else {
                    show(MessageType.Error, 'Не удалось получить сумму для перевода');
                }

                setIsLoading(false);
            }
        })();
    }, [isOpen, show]);

    const createTransferBalance = async () => {
        if (sum) {
            setIsLoading(true);
            const { error: createTransferError } = await FETCH_API.PARTNERS.createCashOutToBalance(sum);

            if (createTransferError && createTransferError !== 'Успешно!') {
                show(MessageType.Error, createTransferError);
            } else {
                setIsOpen(false);
                show(MessageType.Success, 'Перевод на баланс совершен успешно');
                dispatchReceiveNavMenuThunk();
                refreshData(1, true);
            }

            setIsLoading(false);
        } else {
            show(MessageType.Error, 'Не удалось установить сумму для перевода');
        }
    };

    const renderButtons = () => {
        return (
            <div className={ dialogStyles.buttons }>
                <ContainedButton onClick={ cancelClickHandler }>Отмена</ContainedButton>
                <SuccessButton onClick={ createTransferBalance }>Перевести</SuccessButton>
            </div>
        );
    };

    return (
        <>
            { isLoading && <LoadingBounce /> }
            <DialogView
                title="Перевод на баланс аккаунта"
                isTitleSmall={ true }
                isOpen={ isOpen }
                dialogWidth="xs"
                onCancelClick={ cancelClickHandler }
                className={ styles.dialog }
                buttons={ renderButtons }
            >
                <div className={ dialogStyles.dialogBody }>
                    <FormAndLabel label={ `Сумма к переводу, ${ currencySymbol ?? 'руб' }.` }>
                        <OutlinedInput fullWidth={ true } type="number" disabled={ true } value={ sum } />
                    </FormAndLabel>
                    <TextOut fontSize={ 13 }>
                        Сумма вознаграждения будет мгновенно переведена на баланс вашего аккаунта.
                    </TextOut>
                </div>
            </DialogView>
        </>
    );
});

export const BalanceTransferDialog = connect<object, TDispatch, object, AppReduxStoreState>(
    _ => ({}),
    dispatch => ({
        dispatchReceiveNavMenuThunk: () => getNavMenu(dispatch)
    })
)(BalanceTransferDialogComponent);