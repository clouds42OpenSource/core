import React from 'react';
import { TextField } from '@mui/material';
import { DateTimePicker, LocalizationProvider } from '@mui/x-date-pickers';
import ruLocale from 'date-fns/locale/ru';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';
import timezone from 'dayjs/plugin/timezone';

import { DialogView } from 'app/views/components/controls/Dialog/views/DialogView/Index';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { COLORS } from 'app/utils';
import { TextOut } from 'app/views/components/TextOut';
import { FETCH_API } from 'app/api/useFetchApi';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { ContainedButton, SuccessButton } from 'app/views/components/controls/Button';

import styles from './style.module.css';
import dialogStyles from '../dialogStyle.module.css';

dayjs.extend(utc);
dayjs.extend(timezone);

type TCreateTransactionDialog = FloatMessageProps & {
    isOpen: boolean;
    setIsOpen: React.Dispatch<React.SetStateAction<boolean>>;
    refreshData: (page: number, isNeedToRefreshSummary: boolean) => void;
};

const requiredMessage = 'Это поле должно быть заполнено';
const minMessageNumber = 'Значение поля должно быть больше 0';
const maxMessageNumber = 'Значение поля не должно превышать 1 000 000 000.00';

const transactionSchema = Yup.object({
    sum: Yup.number().min(1.00, minMessageNumber).max(1000000000.00, maxMessageNumber).required(requiredMessage),
    date: Yup.number().max(Date.now(), 'Дата не должна превышать текущую').required(requiredMessage),
    comment: Yup.string().max(100, 'Длина поля не должна превышать 100 символов'),
    accountNumber: Yup.number().min(1, minMessageNumber).required(requiredMessage),
    clientPaymentSum: Yup.number().min(0.01, minMessageNumber).max(1000000000.00, maxMessageNumber).required(requiredMessage),
});

type TTransactionSchema = Yup.InferType<typeof transactionSchema>;

export const CreateTransactionDialog = withFloatMessages(({ isOpen, setIsOpen, refreshData, floatMessage: { show } }: TCreateTransactionDialog) => {
    const formik = useFormik<TTransactionSchema>({
        initialValues: {
            sum: 0,
            date: Date.now() - 60000,
            comment: '',
            accountNumber: 0,
            clientPaymentSum: 0
        },
        onSubmit: async values => {
            const dateToString = dayjs(values.date).format('YYYY-MM-DDTHH:mm:ssZ');
            const { error } = await FETCH_API.PARTNERS.createAgentPayment({
                ...values,
                date: dateToString
            });

            if (error && error !== 'Успешно!') {
                show(MessageType.Error, error);
            } else {
                show(MessageType.Success, 'Транзакция успешно создана');
                refreshData(1, true);
                setIsOpen(false);
            }
        },
        validateOnBlur: true,
        validateOnChange: true,
        validationSchema: transactionSchema
    });

    const cancelClickHandler = () => {
        setIsOpen(false);
    };

    const dateHandler = (value: Date | null) => {
        formik.setFieldTouched('date', true, true);

        if (value) {
            formik.setFieldValue('date', value.getTime());
        }
    };

    const renderButtons = () => {
        return (
            <div className={ dialogStyles.buttons }>
                <ContainedButton onClick={ cancelClickHandler }>Отмена</ContainedButton>
                <SuccessButton onClick={ formik.submitForm }>Создать</SuccessButton>
            </div>
        );
    };

    return (
        <DialogView
            title="Создание транзакции"
            isTitleSmall={ true }
            isOpen={ isOpen }
            dialogWidth="sm"
            onCancelClick={ cancelClickHandler }
            buttons={ renderButtons }
            className={ styles.dialog }
        >
            <div className={ dialogStyles.dialogBody }>
                <FormAndLabel label="Сумма:">
                    <TextField
                        fullWidth={ true }
                        type="number"
                        autoComplete="off"
                        name="sum"
                        value={ formik.values.sum }
                        onChange={ formik.handleChange }
                        onBlur={ formik.handleBlur }
                        error={ formik.touched.sum && !!formik.errors.sum }
                        helperText={ formik.touched.sum && formik.errors.sum }
                    />
                </FormAndLabel>
                <FormAndLabel label="Сумма клиентского платежа:">
                    <TextField
                        fullWidth={ true }
                        type="number"
                        autoComplete="off"
                        name="clientPaymentSum"
                        value={ formik.values.clientPaymentSum }
                        onChange={ formik.handleChange }
                        onBlur={ formik.handleBlur }
                        error={ formik.touched.clientPaymentSum && !!formik.errors.clientPaymentSum }
                        helperText={ formik.touched.clientPaymentSum && formik.errors.clientPaymentSum }
                    />
                </FormAndLabel>
                <FormAndLabel label="Номер аккаунта клиента:">
                    <TextField
                        fullWidth={ true }
                        type="number"
                        autoComplete="off"
                        name="accountNumber"
                        value={ formik.values.accountNumber }
                        onChange={ formik.handleChange }
                        onBlur={ formik.handleBlur }
                        error={ formik.touched.accountNumber && !!formik.errors.accountNumber }
                        helperText={ formik.touched.accountNumber && formik.errors.accountNumber }
                    />
                </FormAndLabel>
                <FormAndLabel label="Дата транзакции:">
                    <LocalizationProvider dateAdapter={ AdapterDateFns } adapterLocale={ ruLocale }>
                        <DateTimePicker
                            format="dd.MM.yyyy hh:mm"
                            onChange={ dateHandler }
                            value={ new Date(formik.values.date) }
                            className={ styles.datePicker }
                            sx={ { svg: { color: COLORS.main } } }
                            maxDateTime={ new Date() }
                        />
                        { formik.touched.date && <TextOut fontSize={ 12 } className={ styles.errorText }>{ formik.errors.date }</TextOut> }
                    </LocalizationProvider>
                </FormAndLabel>
                <FormAndLabel label="Операция:">
                    <TextField
                        placeholder="Описание операции"
                        fullWidth={ true }
                        autoComplete="off"
                        name="comment"
                        value={ formik.values.comment }
                        onChange={ formik.handleChange }
                        onBlur={ formik.handleBlur }
                        error={ formik.touched.comment && !!formik.errors.comment }
                        helperText={ formik.touched.comment && formik.errors.comment }
                    />
                </FormAndLabel>
            </div>
        </DialogView>
    );
});