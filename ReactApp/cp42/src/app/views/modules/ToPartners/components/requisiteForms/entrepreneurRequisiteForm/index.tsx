import InputMask, { Props } from 'react-input-mask';
import { MuiTelInput } from 'mui-tel-input';
import { useFormik } from 'formik';
import { useHistory } from 'react-router';
import * as Yup from 'yup';
import dayjs from 'dayjs';
import React, { useEffect, useState } from 'react';

import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { TextField, TextFieldProps } from '@mui/material';
import { FETCH_API } from 'app/api/useFetchApi';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { ConfigFileUploader } from 'app/views/modules/_common/components/ConfigFileUploader';
import { EAgentRequisiteFileType, EAgentRequisitesStatus, EAgentRequisitesType } from 'app/api/endpoints/partners/enum';
import { AppRoutes } from 'app/AppRoutes';
import { contextAccountId } from 'app/api';
import { getFileFromBase64 } from 'app/utils';
import { CheckBoxForm } from 'app/views/components/controls/forms';
import { WebVerb } from 'core/requestSender/enums';
import { TPartialAgentRequisiteRequest } from 'app/api/endpoints/partners/request';
import { objectToFormData } from 'app/web/common';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { TAgentRequisiteFile, TSoleProprietorRequisites } from 'app/api/endpoints/partners/type';
import { useStateCallback } from 'app/hooks';
import { TPartnersHistory } from '../../../config/types';
import { RequisiteFormButtons } from '../../../components/requisiteFormButtons';
import { allowedCountries, fileTypesUploader } from '../../../config/constants';
import {
    bikHandler, getFileHref, limitErrorMessage, requiredMessage, soleProprietorContent, transformPhoneValue, transformValue, wrongFullNameMessage
} from '../../../helpers';

import globalStyle from '../globalStyle.module.css';

const partialEntrepreneurRequisiteSchema = Yup.object({
    inn: Yup.string(),
    fullName: Yup.string(),
    phoneNumber: Yup.string(),
    ogrn: Yup.string(),
    legalAddress: Yup.string(),
    addressForSendingDocuments: Yup.string(),
    bik: Yup.string(),
    bankName: Yup.string(),
    correspondentAccount: Yup.string(),
    settlementAccount: Yup.string(),
});

const entrepreneurRequisiteSchema = Yup.object({
    inn: Yup.string().transform(transformValue).required(requiredMessage).length(12, limitErrorMessage(12)),
    fullName: Yup.string().min(5, wrongFullNameMessage).max(100, wrongFullNameMessage).required(requiredMessage)
        .matches(/([А-ЯЁ][а-яё]+[-\s]?){3,}/, wrongFullNameMessage),
    phoneNumber: Yup.string().transform(transformPhoneValue).required(requiredMessage).min(12, 'Неверный формат номера телефона')
        .max(14, 'Неверный формат номера телефона'),
    ogrn: Yup.string().transform(transformValue).required(requiredMessage).length(15, limitErrorMessage(15)),
    legalAddress: Yup.string().required(requiredMessage),
    addressForSendingDocuments: Yup.string().required(requiredMessage),
    bik: Yup.string().transform(transformValue).required(requiredMessage).length(9, limitErrorMessage(9)),
    bankName: Yup.string().required(requiredMessage),
    correspondentAccount: Yup.string().transform(transformValue).required(requiredMessage).length(20, limitErrorMessage(20)),
    settlementAccount: Yup.string().transform(transformValue).required(requiredMessage).length(20, limitErrorMessage(20)),
});

type TPartialEntrepreneurRequisiteFormSchema = Yup.InferType<typeof partialEntrepreneurRequisiteSchema>;

type TEntrepreneurRequisiteForm = FloatMessageProps & {
    formData?: TSoleProprietorRequisites;
    isDisabled?: boolean;
    agentRequisitesId?: string;
};

export const EntrepreneurRequisiteForm = withFloatMessages(({
    floatMessage: { show }, formData, isDisabled, agentRequisitesId
}: TEntrepreneurRequisiteForm) => {
    const history = useHistory<TPartnersHistory>();

    const [isLoading, setIsLoading] = useState(false);
    const [passportScan, setPassportScan] = useState<File[]>([]);
    const [innScan, setInnScan] = useState<File | null>(null);
    const [snilsScan, setSnilsScan] = useState<File | null>(null);
    const [certificateScan, setCertificateScan] = useState<File | null>(null);
    const [checkboxValue, setCheckboxValue] = useState(false);

    const [innFormDataScan, setInnFormDataScan] = useState<TAgentRequisiteFile>();
    const [snilsFormDataScan, setSnilsFormDataScan] = useState<TAgentRequisiteFile>();
    const [passportFormDataScan, setPassportFormDataScan] = useState<TAgentRequisiteFile[]>([]);
    const [certificateFormDataScan, setCertificateFormDataScan] = useState<TAgentRequisiteFile>();

    const [status, setStatus] = useStateCallback<EAgentRequisitesStatus>(EAgentRequisitesStatus.undefined);

    const schema = Yup.lazy(() => {
        if (status !== EAgentRequisitesStatus.draft && status !== EAgentRequisitesStatus.undefined) {
            return entrepreneurRequisiteSchema;
        }

        return partialEntrepreneurRequisiteSchema;
    });

    const setStateHandler = (st: EAgentRequisitesStatus) => {
        setStatus(st, () => {
            void formik.submitForm();
        });
    };

    useEffect(() => {
        if (formData) {
            if (formData.addressForSendingDocuments === formData.legalAddress) {
                setCheckboxValue(true);
            }

            if (formData.files) {
                const innFile = formData.files.find(file => file.agentRequisitesFileType === EAgentRequisiteFileType.scanCopyInn);
                const snilsFile = formData.files.find(file => file.agentRequisitesFileType === EAgentRequisiteFileType.scanCopySnils);
                const certificateFile = formData.files.find(file => file.agentRequisitesFileType === EAgentRequisiteFileType.scanCopyCertificateRegistrationSoleProprietor);
                const passportFile = formData.files.filter(file => file.agentRequisitesFileType === EAgentRequisiteFileType.scanCopyPassportAndRegistration);

                if (innFile) {
                    setInnScan(getFileFromBase64(innFile.content as string, innFile.fileName, innFile.contentType));
                    setInnFormDataScan(innFile);
                }

                if (passportFile.length > 0) {
                    setPassportScan(passportFile.map(file => getFileFromBase64(file.content as string, file.fileName, file.contentType)));
                    setPassportFormDataScan(passportFile);
                }

                if (snilsFile) {
                    setSnilsScan(getFileFromBase64(snilsFile.content as string, snilsFile.fileName, snilsFile.contentType));
                    setSnilsFormDataScan(snilsFile);
                }

                if (certificateFile) {
                    setCertificateScan(getFileFromBase64(certificateFile.content as string, certificateFile.fileName, certificateFile.contentType));
                    setCertificateFormDataScan(certificateFile);
                }
            }
        }
    }, [formData, show]);

    const formik = useFormik<TPartialEntrepreneurRequisiteFormSchema>({
        initialValues: {
            inn: formData?.inn ?? '',
            fullName: formData?.fullName ?? '',
            phoneNumber: formData?.phoneNumber ?? '',
            ogrn: formData?.ogrn ?? '',
            legalAddress: formData?.legalAddress ?? '',
            addressForSendingDocuments: formData?.addressForSendingDocuments ?? '',
            bik: formData?.bik ?? '',
            bankName: formData?.bankName ?? '',
            correspondentAccount: formData?.correspondentAccount ?? '',
            settlementAccount: formData?.settlementAccount ?? '',
        },
        onSubmit: async values => {
            if ((innScan === null || snilsScan === null || certificateScan === null || passportScan.length < 1) && status !== EAgentRequisitesStatus.draft) {
                return show(MessageType.Warning, 'Приложите скан копии ваших документов');
            }

            try {
                const requestBody: TPartialAgentRequisiteRequest = {
                    accountId: contextAccountId(),
                    id: agentRequisitesId,
                    agentRequisitesStatus: status,
                    agentRequisitesType: EAgentRequisitesType.soleProprietorRequisites,
                    creationDate: dayjs(new Date()).format('YYYY-MM-DDTHH:mm:ss'),
                    isEditMode: true,
                    legalPersonRequisites: null,
                    physicalPersonRequisites: null,
                    soleProprietorRequisites: {
                        fullName: values.fullName,
                        phoneNumber: (values.phoneNumber ?? '').replaceAll(' ', ''),
                        legalAddress: values.legalAddress,
                        addressForSendingDocuments: values.addressForSendingDocuments,
                        bankName: values.bankName,
                        inn: transformValue(values.inn ?? ''),
                        ogrn: transformValue(values.ogrn ?? ''),
                        bik: transformValue(values.bik ?? ''),
                        correspondentAccount: transformValue(values.correspondentAccount ?? ''),
                        settlementAccount: transformValue(values.settlementAccount ?? ''),
                        files: [],
                    },
                };
                let entrepreneurForm = new FormData();
                let filesSize = 0;

                if (certificateScan) {
                    entrepreneurForm.append(soleProprietorContent(requestBody.soleProprietorRequisites?.files?.length), certificateScan);
                    requestBody.soleProprietorRequisites?.files.push({
                        fileName: certificateScan?.name,
                        contentType: certificateScan?.type,
                        agentRequisitesFileType: EAgentRequisiteFileType.scanCopyCertificateRegistrationSoleProprietor,
                        cloudFileId: certificateFormDataScan?.cloudFileId,
                    });
                    filesSize += certificateScan.size;
                }

                if (passportScan[0]) {
                    entrepreneurForm.append(soleProprietorContent(requestBody.soleProprietorRequisites?.files?.length), passportScan[0]);
                    requestBody.soleProprietorRequisites?.files.push({
                        fileName: passportScan[0]?.name,
                        contentType: passportScan[0]?.type,
                        agentRequisitesFileType: EAgentRequisiteFileType.scanCopyPassportAndRegistration,
                        cloudFileId: passportFormDataScan.find(file => file.fileName === passportScan[0].name)?.cloudFileId,
                    });
                    filesSize += passportScan[0].size;
                }

                if (passportScan[1]) {
                    entrepreneurForm.append(soleProprietorContent(requestBody.soleProprietorRequisites?.files?.length), passportScan[1]);
                    requestBody.soleProprietorRequisites?.files.push({
                        fileName: passportScan[1]?.name,
                        contentType: passportScan[1]?.type,
                        agentRequisitesFileType: EAgentRequisiteFileType.scanCopyPassportAndRegistration,
                        cloudFileId: passportFormDataScan.find(file => file.fileName === passportScan[1].name)?.cloudFileId,
                    });
                    filesSize += passportScan[1].size;
                }

                if (passportScan[2]) {
                    entrepreneurForm.append(soleProprietorContent(requestBody.soleProprietorRequisites?.files?.length), passportScan[2]);
                    requestBody.soleProprietorRequisites?.files.push({
                        fileName: passportScan[2]?.name,
                        contentType: passportScan[2]?.type,
                        agentRequisitesFileType: EAgentRequisiteFileType.scanCopyPassportAndRegistration,
                        cloudFileId: passportFormDataScan.find(file => file.fileName === passportScan[2].name)?.cloudFileId,
                    });
                    filesSize += passportScan[2].size;
                }

                if (innScan) {
                    entrepreneurForm.append(soleProprietorContent(requestBody.soleProprietorRequisites?.files?.length), innScan);
                    requestBody.soleProprietorRequisites?.files.push({
                        fileName: innScan?.name,
                        contentType: innScan?.type,
                        agentRequisitesFileType: EAgentRequisiteFileType.scanCopyInn,
                        cloudFileId: innFormDataScan?.cloudFileId,
                    });
                    filesSize += innScan.size;
                }

                if (snilsScan) {
                    entrepreneurForm.append(soleProprietorContent(requestBody.soleProprietorRequisites?.files?.length), snilsScan);
                    requestBody.soleProprietorRequisites?.files.push({
                        fileName: snilsScan?.name,
                        contentType: snilsScan?.type,
                        agentRequisitesFileType: EAgentRequisiteFileType.scanCopySnils,
                        cloudFileId: snilsFormDataScan?.cloudFileId,
                    });
                    filesSize += snilsScan.size;
                }

                if (filesSize / (1024 ** 2) > 14) {
                    return show(MessageType.Warning, 'Размер загружаемых файлов оказался слишком большим. Общее ограничение размера файлов составляет 14 мегабайт');
                }

                entrepreneurForm = objectToFormData(requestBody, entrepreneurForm);
                setIsLoading(true);
                const { error } = await FETCH_API.PARTNERS.createAgentRequisite(entrepreneurForm, formData ? WebVerb.PUT : WebVerb.POST);
                setIsLoading(false);

                if (error && error !== 'Успешно!') {
                    return show(MessageType.Error, error);
                }

                show(MessageType.Success, 'Реквизиты успешно созданы');
                setStatus(EAgentRequisitesStatus.undefined);
                history.push(AppRoutes.partners.toPartners, { activeTabIndex: 3 });
            } catch (err: unknown) {
                show(MessageType.Error, err);
            }
        },
        validateOnBlur: true,
        validateOnChange: true,
        validationSchema: schema,
    });

    useEffect(() => {
        if (formik.values.addressForSendingDocuments !== formik.values.legalAddress && checkboxValue) {
            setCheckboxValue(false);
        }
    }, [checkboxValue, formik.values.addressForSendingDocuments, formik.values.legalAddress]);

    useEffect(() => {
        if (!formik.isSubmitting) {
            setStatus(EAgentRequisitesStatus.undefined);
        }
    }, [formik.isSubmitting, formik.submitCount, setStatus]);

    const passportScanDeleteHandler = (fileName: string, _: number) => {
        if (passportScan) {
            setPassportScan(passportScan.filter(scan => scan.name !== fileName));
        }

        if (passportFormDataScan) {
            setPassportFormDataScan(passportFormDataScan.filter(file => file.fileName !== fileName));
        }
    };

    const passportScanOpenHandler = async (fileName: string, _: number) => {
        window.open(await getFileHref(passportFormDataScan.find(file => file.fileName === fileName)?.cloudFileId ?? ''), '_blank');
    };

    const passportScanHandler = (file: File) => {
        if (passportScan.find(scan => scan.name === file.name)) {
            show(MessageType.Warning, 'Вы уже загрузили файл с таким имененем');
        } else {
            setPassportScan([...passportScan, file]);
        }
    };

    const certificateScanHandler = (file: File) => {
        setCertificateScan(file);
    };

    const certificateScanOpenHandler = async () => {
        window.open(await getFileHref(certificateFormDataScan?.cloudFileId ?? ''), '_blank');
    };

    const certificateScanDeleteHandler = () => {
        setCertificateScan(null);

        if (certificateFormDataScan) {
            setCertificateFormDataScan(undefined);
        }
    };

    const snilsScanHandler = (file: File) => {
        setSnilsScan(file);
    };

    const snilsScanOpenHandler = async () => {
        window.open(await getFileHref(snilsFormDataScan?.cloudFileId ?? ''), '_blank');
    };

    const snilsScanDeleteHandler = () => {
        setSnilsScan(null);

        if (snilsFormDataScan) {
            setSnilsFormDataScan(undefined);
        }
    };

    const innScanHandler = (file: File) => {
        setInnScan(file);
    };

    const innScanOpenHandler = async () => {
        window.open(await getFileHref(innFormDataScan?.cloudFileId ?? ''), '_blank');
    };

    const innScanDeleteHandler = () => {
        setInnScan(null);

        if (innFormDataScan) {
            setInnFormDataScan(undefined);
        }
    };

    const checkboxHandler = <TValue, >(_: string, newValue: TValue, __: TValue) => {
        setCheckboxValue(newValue as boolean);

        if (newValue) {
            formik.setFieldValue('addressForSendingDocuments', formik.values.legalAddress);
        }
    };

    const innHandler = async (ev: React.ChangeEvent<HTMLInputElement>) => {
        const { target: { value } } = ev;
        await formik.setFieldValue('inn', value, true);

        if (transformValue(value).length === 12) {
            setIsLoading(true);
            const { data, error } = await FETCH_API.PARTNERS.getSoleProprietorDetails({ inn: transformValue(value) });

            if (error) {
                show(MessageType.Error, error);
            } else if (data && data.rawData) {
                if (data.rawData.errorMessage) {
                    show(MessageType.Info, data.rawData.errorMessage);
                } else {
                    await formik.setFieldValue('fullName', data.rawData.fullName, true);
                    await formik.setFieldValue('ogrn', data.rawData.ogrn, true);
                    await formik.setFieldValue('legalAddress', data.rawData.address, true);
                }
            }

            setIsLoading(false);
        }
    };

    const phoneNumberHandler = (value: string) => {
        formik.setFieldValue('phoneNumber', value, true);
    };

    const phoneNumberHandlerBlur = () => {
        formik.setFieldTouched('phoneNumber', true, true);
    };

    return (
        <>
            { isLoading && <LoadingBounce /> }
            <form className={ globalStyle.form }>
                <FormAndLabel label="ИНН (после ввода ИНН реквизиты компании заполнятся автоматически)" isSmallFormat={ true }>
                    <InputMask
                        disabled={ isDisabled }
                        mask="999-999-999-999"
                        alwaysShowMask={ true }
                        onChange={ innHandler }
                        onBlur={ formik.handleBlur }
                        name="inn"
                        value={ formik.values.inn }
                    >
                        { (inputProps: Props & TextFieldProps) =>
                            (<TextField
                                className={ isDisabled ? globalStyle.maskDisabled : '' }
                                { ...inputProps }
                                autoComplete="off"
                                fullWidth={ true }
                                error={ formik.touched.inn && !!formik.errors.inn }
                                helperText={ formik.touched.inn && formik.errors.inn }
                            />)}
                    </InputMask>
                </FormAndLabel>
                <FormAndLabel label="ФИО" isSmallFormat={ true }>
                    <TextField
                        disabled={ isDisabled }
                        fullWidth={ true }
                        autoComplete="off"
                        placeholder="Например: Иванов Иван Иванович"
                        name="fullName"
                        value={ formik.values.fullName }
                        onChange={ formik.handleChange }
                        onBlur={ formik.handleBlur }
                        error={ formik.touched.fullName && !!formik.errors.fullName }
                        helperText={ formik.touched.fullName && formik.errors.fullName }
                    />
                </FormAndLabel>
                <FormAndLabel label="Телефон" isSmallFormat={ true }>
                    <MuiTelInput
                        disabled={ isDisabled }
                        fullWidth={ true }
                        name="phoneNumber"
                        autoComplete="off"
                        onChange={ phoneNumberHandler }
                        onBlur={ phoneNumberHandlerBlur }
                        value={ formik.values.phoneNumber }
                        preferredCountries={ allowedCountries }
                        defaultCountry="RU"
                        langOfCountryName="RU"
                        error={ formik.touched.phoneNumber && !!formik.errors.phoneNumber }
                        helperText={ formik.touched.phoneNumber && formik.errors.phoneNumber }
                    />
                </FormAndLabel>
                <FormAndLabel label="ОГРН" isSmallFormat={ true }>
                    <InputMask
                        disabled={ isDisabled }
                        mask="999-999-999-999-999"
                        alwaysShowMask={ true }
                        onChange={ formik.handleChange }
                        onBlur={ formik.handleBlur }
                        name="ogrn"
                        value={ formik.values.ogrn }
                    >
                        { (inputProps: Props & TextFieldProps) => (
                            <TextField
                                className={ isDisabled ? globalStyle.maskDisabled : '' }
                                { ...inputProps }
                                autoComplete="off"
                                fullWidth={ true }
                                error={ formik.touched.ogrn && !!formik.errors.ogrn }
                                helperText={ formik.touched.ogrn && formik.errors.ogrn }
                            />
                        )}
                    </InputMask>
                </FormAndLabel>
                <FormAndLabel label="Юридический адрес" isSmallFormat={ true }>
                    <TextField
                        disabled={ isDisabled }
                        fullWidth={ true }
                        autoComplete="off"
                        placeholder="Например: 40404 г. Москва, ул. Набережная 4"
                        name="legalAddress"
                        value={ formik.values.legalAddress }
                        onChange={ formik.handleChange }
                        onBlur={ formik.handleBlur }
                        error={ formik.touched.legalAddress && !!formik.errors.legalAddress }
                        helperText={ formik.touched.legalAddress && formik.errors.legalAddress }
                    />
                </FormAndLabel>
                <FormAndLabel
                    isSmallFormat={ true }
                    label={
                        <div className={ globalStyle.checkboxView }>
                            Адрес для отправки документов
                            <CheckBoxForm
                                isReadOnly={ isDisabled }
                                isSmallFormat={ true }
                                label="Совпадает с юридическим"
                                formName=""
                                className={ globalStyle.checkbox }
                                onValueChange={ checkboxHandler }
                                isChecked={ checkboxValue }
                            />
                        </div>
                    }
                >
                    <TextField
                        disabled={ isDisabled }
                        fullWidth={ true }
                        autoComplete="off"
                        placeholder="Например: 40404 г. Москва, ул. Набережная 4"
                        name="addressForSendingDocuments"
                        value={ formik.values.addressForSendingDocuments }
                        onChange={ formik.handleChange }
                        onBlur={ formik.handleBlur }
                        error={ formik.touched.addressForSendingDocuments && !!formik.errors.addressForSendingDocuments }
                        helperText={ formik.touched.addressForSendingDocuments && formik.errors.addressForSendingDocuments }
                    />
                </FormAndLabel>
                <FormAndLabel label="БИК (после ввода БИК поля “Наименование банка” и “Корреспондентский счет” заполнятся автоматически)" isSmallFormat={ true }>
                    <InputMask
                        disabled={ isDisabled }
                        mask="99-99-99-999"
                        alwaysShowMask={ true }
                        onChange={ ev => bikHandler<TPartialEntrepreneurRequisiteFormSchema>({
                            setIsLoading,
                            show,
                            ev,
                            setFieldValue: formik.setFieldValue,
                        }) }
                        onBlur={ formik.handleBlur }
                        name="bik"
                        value={ formik.values.bik }
                    >
                        { (inputProps: Props & TextFieldProps) =>
                            (<TextField
                                className={ isDisabled ? globalStyle.maskDisabled : '' }
                                { ...inputProps }
                                autoComplete="off"
                                fullWidth={ true }
                                error={ formik.touched.bik && !!formik.errors.bik }
                                helperText={ formik.touched.bik && formik.errors.bik }
                            />)}
                    </InputMask>
                </FormAndLabel>
                <FormAndLabel label="Наименование банка" isSmallFormat={ true }>
                    <TextField
                        disabled={ isDisabled }
                        fullWidth={ true }
                        autoComplete="off"
                        name="bankName"
                        value={ formik.values.bankName }
                        onChange={ formik.handleChange }
                        onBlur={ formik.handleBlur }
                        error={ formik.touched.bankName && !!formik.errors.bankName }
                        helperText={ formik.touched.bankName && formik.errors.bankName }
                    />
                </FormAndLabel>
                <FormAndLabel label="Корреспондентский счет" isSmallFormat={ true }>
                    <InputMask
                        disabled={ isDisabled }
                        mask="9999-9999-9999-9999-9999"
                        alwaysShowMask={ true }
                        onChange={ formik.handleChange }
                        onBlur={ formik.handleBlur }
                        name="correspondentAccount"
                        value={ formik.values.correspondentAccount }
                    >
                        { (inputProps: Props & TextFieldProps) =>
                            (<TextField
                                className={ isDisabled ? globalStyle.maskDisabled : '' }
                                { ...inputProps }
                                autoComplete="off"
                                fullWidth={ true }
                                error={ formik.touched.correspondentAccount && !!formik.errors.correspondentAccount }
                                helperText={ formik.touched.correspondentAccount && formik.errors.correspondentAccount }
                            />) }
                    </InputMask>
                </FormAndLabel>
                <FormAndLabel label="Расчетный счет" isSmallFormat={ true }>
                    <InputMask
                        disabled={ isDisabled }
                        mask="9999-9999-9999-9999-9999"
                        alwaysShowMask={ true }
                        onChange={ formik.handleChange }
                        onBlur={ formik.handleBlur }
                        name="settlementAccount"
                        value={ formik.values.settlementAccount }
                    >
                        { (inputProps: Props & TextFieldProps) =>
                            (<TextField
                                className={ isDisabled ? globalStyle.maskDisabled : '' }
                                { ...inputProps }
                                autoComplete="off"
                                fullWidth={ true }
                                error={ formik.touched.settlementAccount && !!formik.errors.settlementAccount }
                                helperText={ formik.touched.settlementAccount && formik.errors.settlementAccount }
                            />)}
                    </InputMask>
                </FormAndLabel>
                <FormAndLabel label="Приложите скан копии Ваших документов ">
                    <div className={ globalStyle.files }>
                        <FormAndLabel isSmallFormat={ true } label="Скан паспорта (1-я страница, 2-я страница, прописка)">
                            <ConfigFileUploader
                                disabled={ isDisabled }
                                limit={ 3 }
                                required={ true }
                                deleteUploadFile={ passportScanDeleteHandler }
                                uploadedFiles={ passportScan }
                                fileTypesArr={ fileTypesUploader }
                                callBack={ passportScanHandler }
                                openFile={ passportScanOpenHandler }
                            />
                        </FormAndLabel>
                        <FormAndLabel isSmallFormat={ true } label="Скан ИНН">
                            <ConfigFileUploader
                                disabled={ isDisabled }
                                limit={ 1 }
                                required={ true }
                                uploadedFiles={ innScan && [innScan] }
                                fileTypesArr={ fileTypesUploader }
                                callBack={ innScanHandler }
                                deleteUploadFile={ innScanDeleteHandler }
                                openFile={ innScanOpenHandler }
                            />
                        </FormAndLabel>
                        <FormAndLabel isSmallFormat={ true } label="Скан СНИЛС">
                            <ConfigFileUploader
                                disabled={ isDisabled }
                                limit={ 1 }
                                required={ true }
                                uploadedFiles={ snilsScan && [snilsScan] }
                                fileTypesArr={ fileTypesUploader }
                                callBack={ snilsScanHandler }
                                deleteUploadFile={ snilsScanDeleteHandler }
                                openFile={ snilsScanOpenHandler }
                            />
                        </FormAndLabel>
                        <FormAndLabel isSmallFormat={ true } label="Скан свидетельства о регистрации ИП (если свидетельство выдавалось)">
                            <ConfigFileUploader
                                disabled={ isDisabled }
                                limit={ 1 }
                                required={ true }
                                uploadedFiles={ certificateScan && [certificateScan] }
                                fileTypesArr={ fileTypesUploader }
                                callBack={ certificateScanHandler }
                                deleteUploadFile={ certificateScanDeleteHandler }
                                openFile={ certificateScanOpenHandler }
                            />
                        </FormAndLabel>
                    </div>
                </FormAndLabel>
            </form>
            <RequisiteFormButtons setStatus={ setStateHandler } isDisabled={ isDisabled } />
        </>
    );
});