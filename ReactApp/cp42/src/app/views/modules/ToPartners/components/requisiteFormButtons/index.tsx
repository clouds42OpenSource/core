import { TArticlesProps } from 'app/views/modules/Articles/types/TArticlesHistory';
import { useHistory } from 'react-router';
import React from 'react';

import { EAgentRequisitesStatus } from 'app/api/endpoints/partners/enum';
import { AppRoutes } from 'app/AppRoutes';
import { ContainedButton, SuccessButton } from 'app/views/components/controls/Button';
import { TPartnersHistory } from '../../config/types';

import styles from './style.module.css';

type TRequisiteFormButtons = {
    setStatus: (status: EAgentRequisitesStatus) => void;
    isDisabled?: boolean;
};

export const RequisiteFormButtons = ({ setStatus, isDisabled }: TRequisiteFormButtons) => {
    const history = useHistory<TPartnersHistory & TArticlesProps | undefined>();

    const onDraftHandler = () => {
        setStatus(EAgentRequisitesStatus.draft);
    };

    const onCheckHandler = () => {
        setStatus(EAgentRequisitesStatus.onCheck);
    };

    const onCancelHandler = () => {
        if (history.location.state?.forArticles) {
            history.push(AppRoutes.articlesReference, { activeTabIndex: 2 });
        } else {
            history.push(AppRoutes.partners.toPartners, { activeTabIndex: 3 });
        }
    };

    return (
        <div className={ styles.footer }>
            <ContainedButton onClick={ onCancelHandler }>Назад</ContainedButton>
            { !isDisabled &&
                <>
                    <ContainedButton onClick={ onDraftHandler }>Сохранить как черновик</ContainedButton>
                    <SuccessButton onClick={ onCheckHandler }>На модерацию</SuccessButton>
                </>
            }
        </div>
    );
};