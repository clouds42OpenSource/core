/* eslint-disable no-underscore-dangle */
import { Box, Button } from '@mui/material';
import { getZabbixItemsResponseDto } from 'app/api/endpoints/zabbix/response';
import { DateUtility } from 'app/utils';
import { TextOut } from 'app/views/components/TextOut';
import { UltraChart } from 'app/views/modules/Ultra/components/UltraChart';
import { UltraChartModal } from 'app/views/modules/Ultra/components/UltraChartModal';
import css from 'app/views/modules/Ultra/styles.module.css';
import { Condition } from 'app/views/modules/Ultra/types';
import { memo, useState } from 'react';

type Props = {
    item: getZabbixItemsResponseDto;
    pattern: Condition;
    title: string;
    noDataText: string;
    criticalText?: string;
    tooltip?: string;
    chartTitle?: string;
    detailsTitle?: string;
};

const SeparateChartView = ({ item, pattern, title, noDataText, criticalText, tooltip, chartTitle, detailsTitle }: Props) => {
    const [isModalOpened, setIsModalOpened] = useState(false);

    if (!item) {
        return <TextOut>{ noDataText }</TextOut>;
    }

    return (
        <div>
            <Box className={ `${ css.commonInfoList } ${ css.separateChart }` }>
                <Box>
                    <Box>
                        <TextOut>
                            Последнее значение
                        </TextOut>
                    </Box>
                    <Box gridArea="1/2/1/6">
                        <TextOut>
                            { chartTitle ?? 'График измерений за сутки' }
                        </TextOut>
                    </Box>
                    <Box>
                        <TextOut>
                            { detailsTitle ?? 'Значения в динамике' }
                        </TextOut>
                    </Box>
                </Box>
                <Box>
                    <Box>
                        { item.lastvalue.slice(0, 8) }
                    </Box>
                    <Box gridArea="1/2/1/6">
                        <UltraChart
                            tooltip={ tooltip }
                            criticalValue={ pattern.criticalValue }
                            defaultStartTime={ DateUtility.getSomeDaysAgoDate(1) }
                            noDatePicker={ true }
                            title={ title }
                            formatting={ { unit: '', conversionFactor: 1 } }
                            items={ [item] }
                            criticalText={ criticalText }
                        />
                    </Box>
                    <Box>
                        <Button onClick={ () => setIsModalOpened(true) }>
                            <TextOut inheritColor={ true }>
                                Графики
                            </TextOut>
                        </Button>
                    </Box>
                </Box>
                <UltraChartModal criticalText={ criticalText } patterns={ [pattern] } modalTitle={ item.name } isModalOpened={ isModalOpened } setIsModalOpened={ setIsModalOpened } items={ [item] } />
            </Box>
        </div>
    );
};

export const SeparateChart = memo(SeparateChartView);