/* eslint-disable no-underscore-dangle */
import { Box, Button, TextField } from '@mui/material';
import { FETCH_API } from 'app/api/useFetchApi';
import { EMessageType, useFloatMessages } from 'app/hooks';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { TextOut } from 'app/views/components/TextOut';
import { SuccessButton } from 'app/views/components/controls/Button';
import { Dialog } from 'app/views/components/controls/Dialog';
import { useFormik } from 'formik';
import { memo, useCallback, useEffect, useState } from 'react';
import * as yup from 'yup';

export type ReviewModalProps = {
    getCorpCloudExtensions?: () => void;
    buttonText?: string;
    placeholder: string;
    subject: string;
    header: string;
    description: string;
    title: string;
};

type TReview = {
    email: string;
    body: string;
}

const reviewSchema = yup.object().shape({
    email: yup
        .string().email('Неверный формат почты'),
    body: yup.string().min(4, 'Ваш отзыв слишком краток')
});

const ReviewModalView = ({ getCorpCloudExtensions, buttonText = '', subject, header, description, title, placeholder }: ReviewModalProps) => {
    const [isModalOpened, setIsModalOpened] = useState(false);
    const { show } = useFloatMessages();
    const { data: profileData, error, isLoading } = FETCH_API.PROFILE.useGetProfile();

    const memoizedGetCorpCloudExtensions = useCallback(() => {
        if (getCorpCloudExtensions) {
            getCorpCloudExtensions();
        }
    }, [getCorpCloudExtensions]);

    const sendEmail = async (data: TReview) => {
        const response = await FETCH_API.EMAILS.sendEmail({
            body: data.body,
            subject,
            header: `${ header } ${ data.email }`,
            categories: 'corp-cloud',
            reference: import.meta.env.REACT_ZABBIX_EMAIL ?? ''
        });
        if (response.success) {
            show(EMessageType.success, 'Успешно отправлено');
            memoizedGetCorpCloudExtensions();
            setIsModalOpened(false);
        } else {
            show(EMessageType.error, 'Произошла ошибка');
        }
    };

    const {
        handleChange,
        values,
        submitForm,
        errors,
        touched
    } = useFormik<TReview>({
        initialValues: {
            email: profileData?.rawData.email ?? '',
            body: '',
        },
        validationSchema: reviewSchema,
        enableReinitialize: true,
        onSubmit: async formValues => sendEmail(formValues),
    });

    useEffect(() => {
        if (isModalOpened && error) {
            show(EMessageType.error, 'Произошла ошибка при получении почты пользователя, введите почту вручную');
        }
    }, [isModalOpened, error, show]);

    return (
        <>
            { isLoading && <LoadingBounce /> }
            { buttonText ? (
                <SuccessButton onClick={ () => setIsModalOpened(true) }>
                    { buttonText }
                </SuccessButton>
            ) : (
                <Button onClick={ () => setIsModalOpened(true) }>
                    Расширенные параметры производительности
                </Button>
            ) }
            <Dialog
                isOpen={ isModalOpened }
                dialogWidth="sm"
                dialogVerticalAlign="top"
                onCancelClick={ () => setIsModalOpened(false) }
                title={ title }
                titleFontSize={ 20 }
                titleTextAlign="left"
                isTitleSmall={ true }
            >
                <TextOut inDiv={ true }>
                    { description }
                </TextOut>
                <Box marginTop="12px" display="flex" flexDirection="column" gap="8px">
                    <TextField
                        placeholder="Введите свою почту"
                        fullWidth={ true }
                        name="email"
                        value={ values.email }
                        onChange={ handleChange }
                        error={ touched.email && !!errors.email }
                        helperText={ touched.email && errors.email }
                    />
                    <TextField
                        placeholder={ placeholder }
                        fullWidth={ true }
                        name="body"
                        minRows="5"
                        value={ values.body }
                        onChange={ handleChange }
                        multiline={ true }
                    />
                </Box>
                <SuccessButton style={ { marginTop: '6px' } } onClick={ submitForm }>
                    Отправить
                </SuccessButton>
            </Dialog>
        </>
    );
};

export const ReviewModal = memo(ReviewModalView);