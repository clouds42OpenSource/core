/* eslint-disable no-underscore-dangle */
import { Box, TextField, RadioGroup, FormControlLabel, Radio, Button } from '@mui/material';
import { FETCH_API } from 'app/api/useFetchApi';
import { EMessageType, useFloatMessages } from 'app/hooks';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { TextOut } from 'app/views/components/TextOut';
import { SuccessButton } from 'app/views/components/controls/Button';
import { Dialog } from 'app/views/components/controls/Dialog';
import dayjs from 'dayjs';
import { useFormik } from 'formik';
import React, { memo, useEffect, useState } from 'react';
import * as yup from 'yup';

export type SuggestImprovementsModalProps = {
  functionalityName: string;
  companyId: string | number;
  buttonText?: string;
  children?: React.ReactNode;
};

type TFeedback = {
  idea: string;
  satisfaction: string;
  easeOfUse: string;
};

const feedbackSchema = yup.object().shape({
    idea: yup.string().required('Пожалуйста, опишите вашу идею').min(4, 'Идея слишком кратка'),
    satisfaction: yup.string().required('Пожалуйста, выберите оценку'),
    easeOfUse: yup.string().required('Пожалуйста, выберите оценку'),
});

const SuggestImprovementsModalView = ({
    functionalityName,
    companyId,
    buttonText = 'Предложить улучшения',
    children,
}: SuggestImprovementsModalProps) => {
    const [isModalOpened, setIsModalOpened] = useState(false);
    const { show } = useFloatMessages();
    const { data: profileData, error, isLoading } = FETCH_API.PROFILE.useGetProfile();

    const sendFeedback = async (data: TFeedback) => {
        const currentDate = dayjs();

        const emailSubject = `Обратная связь по функционалу ${ functionalityName } от компании ${ companyId }`;

        const emailContent = `
            Получена обратная связь!

            Компания: ${ companyId }
            Пользователь: ${ profileData?.rawData.id }, ${ profileData?.rawData.login }, ${ profileData?.rawData.email }
            Дата: ${ currentDate.format('DD.MM.YYYY HH:mm') }

            Опишите вашу идею, что хотелось бы добавить или изменить:
            ${ data.idea }

            Ответьте на дополнительные вопросы:
            Насколько вы довольны функционалом “${ functionalityName }”?: ${ data.satisfaction }

            Насколько легко пользоваться функционалом “${ functionalityName }”?: ${ data.easeOfUse }
    `;

        const response = await FETCH_API.EMAILS.sendEmail({
            body: emailContent,
            subject: emailSubject,
            header: `Обратная связь по функционалу ${ functionalityName } от компании ${ companyId }`,
            categories: 'corp-cloud',
            reference: import.meta.env.REACT_ZABBIX_EMAIL ?? ''
        });

        if (response.success) {
            show(EMessageType.success, 'Спасибо за вашу обратную связь!');
            setIsModalOpened(false);
        } else {
            show(EMessageType.error, 'Произошла ошибка при отправке обратной связи.');
        }
    };

    const { handleChange, values, submitForm, errors, touched } = useFormik<TFeedback>({
        initialValues: {
            idea: '',
            satisfaction: '',
            easeOfUse: '',
        },
        validationSchema: feedbackSchema,
        onSubmit: async formValues => sendFeedback(formValues),
    });

    useEffect(() => {
        if (isModalOpened && error) {
            show(EMessageType.error, 'Произошла ошибка при получении данных пользователя');
        }
    }, [isModalOpened, error, show]);

    return (
        <>
            { isLoading && <LoadingBounce /> }
            <Button variant="outlined" sx={ { textTransform: 'none' } } onClick={ () => setIsModalOpened(true) }>{ buttonText }</Button>
            <Dialog
                isOpen={ isModalOpened }
                dialogWidth="sm"
                dialogVerticalAlign="top"
                onCancelClick={ () => setIsModalOpened(false) }
                title={ `Предложите улучшение для функционала “${ functionalityName }”` }
                titleFontSize={ 20 }
                titleTextAlign="left"
                isTitleSmall={ true }
            >
                { children && <TextOut inDiv={ true }>{ children }</TextOut> }
                <TextOut inDiv={ true } style={ { marginTop: '12px', display: 'flex', flexDirection: 'column', gap: '12px' } }>
                    <TextOut>
                        Опишите вашу идею, что хотелось бы добавить или изменить:
                    </TextOut>
                    <TextField
                        placeholder="Опишите вашу идею"
                        fullWidth={ true }
                        name="idea"
                        minRows={ 4 }
                        value={ values.idea }
                        onChange={ handleChange }
                        multiline={ true }
                        error={ touched.idea && !!errors.idea }
                        helperText={ touched.idea && errors.idea }
                    />
                    <TextOut>
                        Ответьте на дополнительные вопросы:
                    </TextOut>
                    <Box>
                        <Box marginBottom="8px">
                            Насколько вы довольны функционалом “{ functionalityName }”?
                        </Box>
                        <RadioGroup
                            row={ true }
                            name="satisfaction"
                            value={ values.satisfaction }
                            onChange={ handleChange }
                        >
                            { [5, 4, 3, 2, 1].map(value => (
                                <FormControlLabel
                                    key={ value }
                                    value={ String(value) }
                                    control={ <Radio /> }
                                    label={ String(value) }
                                />
                            )) }
                        </RadioGroup>
                        { touched.satisfaction && errors.satisfaction && (
                            <div style={ { color: 'red', fontSize: '0.75rem' } }>{ errors.satisfaction }</div>
                        ) }
                    </Box>
                    <Box>
                        <Box marginBottom="8px">
                            Насколько легко пользоваться функционалом “{ functionalityName }”?
                        </Box>
                        <RadioGroup
                            row={ true }
                            name="easeOfUse"
                            value={ values.easeOfUse }
                            onChange={ handleChange }
                        >
                            { [5, 4, 3, 2, 1].map(value => (
                                <FormControlLabel
                                    key={ value }
                                    value={ String(value) }
                                    control={ <Radio /> }
                                    label={ String(value) }
                                />
                            )) }
                        </RadioGroup>
                        { touched.easeOfUse && errors.easeOfUse && (
                            <div style={ { color: 'red', fontSize: '0.75rem' } }>{ errors.easeOfUse }</div>
                        ) }
                    </Box>
                </TextOut>
                <SuccessButton style={ { marginTop: '12px' } } onClick={ submitForm }>
                    Отправить
                </SuccessButton>
            </Dialog>
        </>
    );
};

export const SuggestImprovementsModal = memo(SuggestImprovementsModalView);