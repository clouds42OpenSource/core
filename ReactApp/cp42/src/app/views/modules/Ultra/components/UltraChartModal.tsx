/* eslint-disable no-underscore-dangle */
import { Box } from '@mui/material';
import { getZabbixItemsResponseDto } from 'app/api/endpoints/zabbix/response';
import { Dialog } from 'app/views/components/controls/Dialog';
import { UltraChart } from 'app/views/modules/Ultra/components/UltraChart';
import { Condition } from 'app/views/modules/Ultra/types';
import { Chart, LineController, LineElement, LinearScale, PointElement, TimeScale } from 'chart.js';
import 'chartjs-adapter-date-fns';

Chart.register(LineController, LineElement, PointElement, LinearScale, TimeScale);

type Props = {
    items: getZabbixItemsResponseDto[];
    isModalOpened: boolean;
    setIsModalOpened: (value: boolean) => void;
    modalTitle: string;
    patterns?: Condition[];
    noDatePicker?: boolean;
    criticalText?: string;

};

const defaultPatterns: Condition[] = [
    {
        title: 'Использование CPU',
        pattern: /system\.cpu\.util/,
        formatting: { unit: '%', conversionFactor: 1 },
        criticalValue: 80,
    },
    {
        title: 'Длина очереди CPU',
        pattern: /perf_counter\[\\2\\44\]/,
        formatting: { unit: 'ед.', conversionFactor: 1 }
    },
    {
        title: 'Задержки записи на диски',
        pattern: /(?=.*perf_counter)(?=.*236)(?=.*210)/,
        formatting: { unit: 'мс', conversionFactor: 1000 },
        criticalValue: 100,
    },
    {
        title: 'Использование RAM',
        pattern: /vm\.memory\.size\[pused\]/,
        formatting: { unit: '%', conversionFactor: 1 }
    },
    {
        title: 'Количество свободной RAM',
        pattern: /vm\.memory\.size\[available\]|vm\.memory\.available\[real\]/,
        formatting: { unit: 'ГБ', conversionFactor: 1 / (2 ** 30) }
    }
];

export const UltraChartModal = ({ items, isModalOpened, setIsModalOpened, modalTitle, patterns = defaultPatterns, noDatePicker, criticalText }: Props) => {
    return (
        <Dialog
            isOpen={ isModalOpened }
            dialogWidth="xl"
            onCancelClick={ () => setIsModalOpened(false) }
            title={ `График ${ modalTitle }` }
            titleFontSize={ 20 }
            titleTextAlign="left"
            isTitleSmall={ true }
        >
            <Box>
                {
                    patterns.map(({ title, pattern, formatting, criticalValue }) => {
                        const filteredItems = pattern ? items.filter(item => pattern.test(item.key_)) : items;
                        return (
                            <UltraChart criticalText={ criticalText } criticalValue={ criticalValue } noDatePicker={ noDatePicker } key={ title } title={ title } formatting={ formatting } items={ filteredItems } />
                        );
                    })
                }
            </Box>
        </Dialog>
    );
};