/* eslint-disable no-underscore-dangle */
/* eslint-disable camelcase */
import { Box, Button } from '@mui/material';
import { DesktopDateTimePicker, LocalizationProvider } from '@mui/x-date-pickers';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { getZabbixHistoryResponseDto, getZabbixItemsResponseDto } from 'app/api/endpoints/zabbix/response';
import { FETCH_API } from 'app/api/useFetchApi';
import { DateUtility } from 'app/utils';
import { ReplaceNullOrUndefinedUtility } from 'app/utils/ReplaceNullOrUndefinedUtility';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { TextOut } from 'app/views/components/TextOut';
import { Dialog } from 'app/views/components/controls/Dialog';
import { RequestsAndLoadsDBMSConst } from 'app/views/modules/Ultra/constants/RequestsAndLoadsDBMSConst';
import { FieldViewType } from 'app/views/modules/Ultra/types';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import ruLocale from 'date-fns/locale/ru';
import { useState, memo } from 'react';

type Props = {
    item: getZabbixItemsResponseDto;
    useNewKeys: boolean;
};

const QueryHistoryModalView = ({ item, useNewKeys }: Props) => {
    const [isModalOpened, setIsModalOpened] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const [startTime, setStartTime] = useState<Date | null>(DateUtility.getSomeDaysAgoDate(1));
    const [data, setData] = useState<getZabbixHistoryResponseDto | null>(null);
    const [isFirstOpen, setIsFirstOpen] = useState(true);
    const correspondingConst = RequestsAndLoadsDBMSConst.find(constant => constant[useNewKeys ? 'newKey' : 'oldKey'] === item.key_);
    const handleGetData = async () => {
        const from = startTime ? Math.floor(startTime.getTime() / 1000) : undefined;
        const till = startTime ? Math.floor(startTime.getTime() / 1000) + 3600 : undefined;
        setIsLoading(true);
        const response = await FETCH_API.ZABBIX.getHistory(item.itemid, item.value_type, from, till);
        if (response && response.result) {
            setData(response.result[0]);
        }
        setIsLoading(false);
        return null;
    };

    const handleOpenModal = () => {
        if (isFirstOpen) {
            void handleGetData();
            setIsFirstOpen(false);
        }
        setIsModalOpened(true);
    };

    return (
        <>
            { isLoading && <LoadingBounce /> }
            <Dialog
                isOpen={ isModalOpened }
                dialogWidth="xl"
                onCancelClick={ () => setIsModalOpened(false) }
                title="История данных"
                titleFontSize={ 20 }
                titleTextAlign="left"
                isTitleSmall={ true }
            >
                <Box>
                    <TextOut>Период отображения данных:</TextOut>
                    <Box display="flex" gap="6px">
                        <LocalizationProvider dateAdapter={ AdapterDateFns } adapterLocale={ ruLocale }>
                            <DesktopDateTimePicker
                                sx={ { color: 'red' } }
                                value={ startTime }
                                ampm={ false }
                                onChange={ (date: Date | null) => setStartTime(date) }
                                format="dd/MM/yyyy"
                            />
                        </LocalizationProvider>
                        <Button onClick={ handleGetData }>Обновить</Button>
                    </Box>
                    <Box>
                        { data && correspondingConst?.fields && <CommonTableWithFilter
                            uniqueContextProviderStateId={ `table-${ item.itemid }` }
                            isResponsiveTable={ true }
                            isBorderStyled={ true }
                            tableProps={ {
                                dataset: ReplaceNullOrUndefinedUtility(JSON.parse(data.value)),
                                keyFieldName: Object.keys(correspondingConst.fields)[0],
                                fieldsView: Object.entries(correspondingConst.fields).reduce((acc: FieldViewType, [key, caption]) => {
                                    acc[key] = {
                                        caption,
                                        fieldWidth: 'auto',
                                        format: value => value
                                    };
                                    return acc;
                                }, {})
                            } }
                        /> }
                    </Box>
                </Box>
            </Dialog>
            <Button onClick={ handleOpenModal }>
                История данных
            </Button>
        </>
    );
};

export const QueryHistoryModal = memo(QueryHistoryModalView);