/* eslint-disable camelcase */
import { HelpOutline as HelpOutlineIcon } from '@mui/icons-material';
import { Box, Button, IconButton, Tooltip as MuiTooltip } from '@mui/material';
import { DesktopDateTimePicker, LocalizationProvider } from '@mui/x-date-pickers';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { getZabbixHistoryResponseDto, getZabbixItemsResponseDto, getZabbixTrendsResponseDto } from 'app/api/endpoints/zabbix/response';
import { FETCH_API } from 'app/api/useFetchApi';
import { DateUtility } from 'app/utils';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { TextOut } from 'app/views/components/TextOut';
import { ChartData, FormattingConfig } from 'app/views/modules/Ultra/types';
import { Chart, Legend, LineController, LineElement, LinearScale, PointElement, TimeScale, Tooltip } from 'chart.js';
import 'chartjs-adapter-date-fns';
import annotationPlugin from 'chartjs-plugin-annotation';
import ruLocale from 'date-fns/locale/ru';
import { useEffect, useState } from 'react';
import { Line } from 'react-chartjs-2';

Chart.register(LineController, LineElement, PointElement, LinearScale, TimeScale, Tooltip, Legend, annotationPlugin);
type Props = {
    items: getZabbixItemsResponseDto[];
    title: string;
    formatting: FormattingConfig;
    noDatePicker?: boolean;
    defaultStartTime?: Date;
    criticalValue?: number;
    criticalText?: string;
    tooltip?: string;
};

const colorPalette = [
    'rgba(54, 162, 235, 1)',
    'rgba(75, 192, 192, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(255, 159, 64, 1)',
    'rgba(255, 205, 86, 1)',
    'rgba(201, 203, 207, 1)',
    'rgba(128, 0, 128, 1)',
    'rgba(0, 128, 128, 1)',
    'rgba(128, 128, 0, 1)',
    'rgba(0, 128, 0, 1)'
];

export const UltraChart = ({ items, title, formatting, noDatePicker = false, defaultStartTime, criticalValue, criticalText, tooltip }: Props) => {
    const [startTime, setStartTime] = useState<Date | null>(defaultStartTime ?? DateUtility.getSomeDaysAgoDate(1));
    const [endTime, setEndTime] = useState<Date | null>(new Date());
    const [data, setData] = useState<ChartData>({ labels: [], datasets: [] });
    const [colorMapping, setColorMapping] = useState<Record<string, string>>({});
    const [isLoading, setIsLoading] = useState(false);

    let colorIndex = 0;

    const getColorForLabel = (label: string) => {
        if (!colorMapping[label]) {
            const newColor = colorPalette[colorIndex % colorPalette.length];
            setColorMapping(prevMapping => ({
                ...prevMapping,
                [label]: newColor,
            }));
            colorIndex += 1;
            return newColor;
        }
        return colorMapping[label];
    };

    const filterData = (fullData: getZabbixHistoryResponseDto[] | getZabbixTrendsResponseDto[]) => {
        const totalPoints = fullData.length;
        if (totalPoints <= 100) {
            return fullData;
        }

        const interval = Math.floor(totalPoints / 100);
        const filteredData = [];

        for (let i = 0; i < totalPoints; i += interval) {
            filteredData.push(fullData[i]);
        }

        return filteredData;
    };

    const filterLabels = (fullLabels: Date[]): Date[] => {
        const totalLabels = fullLabels.length;
        if (totalLabels <= 100) return fullLabels;

        const interval = Math.floor(totalLabels / 100);
        const filteredLabels: Date[] = [];

        for (let i = 0; i < totalLabels; i += interval) {
            filteredLabels.push(fullLabels[i]);
        }

        return filteredLabels;
    };

    const handleGetData = async () => {
        const from = startTime ? Math.floor(startTime.getTime() / 1000) - 100 : undefined;
        const till = endTime ? Math.floor(endTime.getTime() / 1000) : undefined;
        const isPast = endTime ? endTime < new Date() : undefined;
        const duration = till && from ? till - from : 0;
        const threeHours = 3 * 60 * 60;
        const isTrendData = duration > threeHours;
        let labels: Date[] = [];
        setIsLoading(true);

        const datasetsPromises = items.map(async ({ itemid, name, lastvalue, value_type }) => {
            const response = isTrendData
                ? await FETCH_API.ZABBIX.getTrends(itemid, from, till)
                : await FETCH_API.ZABBIX.getHistory(itemid, value_type, from, till);

            if (response && response.result) {
                const sortedTrends = filterData(response.result).sort((a, b) => Number(a.clock) - Number(b.clock));
                const newData = isTrendData
                    ? (sortedTrends as getZabbixTrendsResponseDto[]).map(entry => Number(entry.value_avg) * formatting.conversionFactor)
                    : (sortedTrends as getZabbixHistoryResponseDto[]).map(entry => Number(entry.value) * formatting.conversionFactor);

                const borderColor = getColorForLabel(name);

                if (!labels.length) {
                    labels = sortedTrends
                        ? filterLabels(sortedTrends.map(entry => new Date(Number(entry.clock) * 1000))).sort((a, b) => Number(a) - Number(b))
                        : [];
                }

                return {
                    label: name,
                    data: isPast ? newData : [...newData, Number(lastvalue) * formatting.conversionFactor],
                    fill: false,
                    borderColor,
                };
            }
            return null;
        });

        const datasets = await Promise.all(datasetsPromises);
        const filteredDatasets = datasets.filter(Boolean) as ChartData['datasets'];
        setIsLoading(false);

        setData({
            labels: isPast ? labels : [...labels, new Date()],
            datasets: filteredDatasets
        });
    };

    useEffect(() => {
        void handleGetData();
    }, [items]);

    return (
        <div style={ { marginBottom: '12px', minHeight: '20vw' } }>
            { isLoading && <LoadingBounce /> }
            <Box display="flex" alignItems="center" justifyContent="center" gap="3px">
                <TextOut fontWeight={ 600 } fontSize={ 20 }>{ title }</TextOut>
                { tooltip ? (
                    <MuiTooltip title={ tooltip }>
                        <IconButton aria-label="prompt">
                            <HelpOutlineIcon />
                        </IconButton>
                    </MuiTooltip>
                ) : '' }
            </Box>
            { criticalValue && <TextOut style={ { fontWeight: 'bold', color: '#ff6284' } }> { criticalText || 'Предельное значение' }: { `${ criticalValue } ${ formatting.unit }` }</TextOut> }
            { !noDatePicker && (
                <div>
                    <TextOut>Период отображения данных:</TextOut>
                    <Box display="flex" gap="6px">
                        <LocalizationProvider dateAdapter={ AdapterDateFns } adapterLocale={ ruLocale }>
                            <DesktopDateTimePicker
                                sx={ { color: 'red' } }
                                value={ startTime }
                                ampm={ false }
                                maxDate={ endTime ?? undefined }
                                onChange={ (date: Date | null) => setStartTime(date) }
                                format="dd/MM/yyyy"
                            />
                        </LocalizationProvider>
                        <TextOut style={ { lineHeight: '36px', fontWeight: 'bold' } }>
                            -
                        </TextOut>
                        <LocalizationProvider dateAdapter={ AdapterDateFns } adapterLocale={ ruLocale }>
                            <DesktopDateTimePicker
                                value={ endTime }
                                ampm={ false }
                                minDate={ startTime ?? undefined }
                                onChange={ (date: Date | null) => setEndTime(date) }
                                format="dd/MM/yyyy"
                            />
                        </LocalizationProvider>
                        <Button onClick={ handleGetData }>Обновить</Button>
                    </Box>
                </div>
            )
            }
            <Line
                data={ data }
                options={ {
                    aspectRatio: 3,
                    scales: {
                        x: {
                            type: 'time',
                            position: 'bottom',
                            time: {
                                tooltipFormat: 'dd.MM.yyyy HH:mm',
                                displayFormats: {
                                    millisecond: 'dd.MM.yyyy HH:mm',
                                    second: 'dd.MM.yyyy HH:mm',
                                    minute: 'dd.MM.yyyy HH:mm',
                                    hour: 'dd.MM.yyyy HH:mm',
                                    day: 'dd.MM.yyyy',
                                    week: 'll',
                                    month: 'MM.YYYY',
                                    quarter: 'QQ - YYYY',
                                    year: 'YYYY'
                                }
                            },
                            title: {
                                display: true,
                                text: 'Время'
                            }
                        },
                        y: {
                            title: {
                                display: true,
                                text: 'Значение'
                            },
                            ticks: {
                                callback: value => `${ Number(value) % 1 === 0 ? value : Number(value).toFixed(2) } ${ formatting.unit }`
                            }
                        }
                    },
                    plugins: {
                        tooltip: {
                            enabled: true,
                            callbacks: {
                                label(context) {
                                    return `${ Number(context.raw).toFixed(2) } ${ formatting.unit }`;
                                }
                            }
                        },
                        annotation: {
                            annotations: criticalValue ? {
                                line1: {
                                    type: 'line',
                                    yMin: criticalValue,
                                    yMax: criticalValue,
                                    borderColor: 'rgba(255, 99, 132, 1)',
                                    borderWidth: 2,
                                }
                            } : undefined
                        }
                    }
                } }
            />
        </div>
    );
};