/* eslint-disable no-underscore-dangle */
import { getZabbixItemsResponseDto } from 'app/api/endpoints/zabbix/response';
import { KeyedZabbixConfigItem } from '../types';

export type ZabbixItemExtended = getZabbixItemsResponseDto & {
  head: string;
  description?: string;
  fields?: Record<string, string>;
};

export const buildZabbixDataByKeys = (
    data: getZabbixItemsResponseDto[],
    config: KeyedZabbixConfigItem[],
    useNewKeys: boolean
): ZabbixItemExtended[] => {
    return config.reduce<ZabbixItemExtended[]>((acc, conf) => {
        const keyToFind = useNewKeys ? conf.newKey : conf.oldKey;

        const found = data.find(item => item.key_ === keyToFind);
        if (found) {
            acc.push({
                ...found,
                head: conf.head,
                description: conf.description,
                fields: conf.fields
            });
        }
        return acc;
    }, []);
};