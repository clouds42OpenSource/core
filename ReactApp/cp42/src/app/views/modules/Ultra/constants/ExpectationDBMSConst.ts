export const ExpectationDBMSConst = [
    {
        head: 'SSP_Waits_now',
        oldKey: 'db.odbc.get[TOP WAIT_TYPE,"{$MSSQL.DSN}"]',
        newKey: 'mssql.TOP_WAIT_TYPE["{$MSSQL.URI}","{$MSSQL.USER}","{$MSSQL.PASSWORD}"]',
        description: 'Этот показатель собирает ожидания для сервера MSSQL'
    }
];