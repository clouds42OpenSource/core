import { KeyedZabbixConfigItem } from '../types';

export const RequestsAndLoadsDBMSConst: KeyedZabbixConfigItem[] = [
    {
        head: 'ТОП баз по нагрузке CPU',
        oldKey: 'db.odbc.get[TOP BASE CPU,"{$MSSQL.DSN}"]',
        newKey: 'mssql.TOP_BASE_CPU["{$MSSQL.URI}","{$MSSQL.USER}","{$MSSQL.PASSWORD}"]',
        description: 'Этот показатель собирает данные по нагрузке CPU в разрезе баз',
        fields: {
            row_num: 'Номер строки',
            DatabaseName: 'Имя базы данных',
            CPU_Time_Ms: 'Время процессора в миллисекундах',
            CPUPercent: '% нагрузки по отношению к другим базам'
        }
    },
    {
        head: 'ТОП баз по нагрузке RAM',
        oldKey: 'db.odbc.get[DB USING CACHE,"{$MSSQL.DSN}"]',
        newKey: 'mssql.DB_USING_CACHE["{$MSSQL.URI}","{$MSSQL.USER}","{$MSSQL.PASSWORD}"]',
        description: 'Этот показатель собирает данные по использованию CACHE в разрезе баз',
        fields: {
            DB: 'Наименование',
            MB: 'Размер в MB',
            GB: 'Размер в GB'
        }
    },
    {
        head: 'ТОП баз по нагрузке DISK',
        oldKey: 'db.odbc.get[TOP BASE DISK,"{$MSSQL.DSN}"]',
        newKey: 'mssql.TOP_BASE_DISK["{$MSSQL.URI}","{$MSSQL.USER}","{$MSSQL.PASSWORD}"]',
        description: 'Этот показатель собирает данные по нагрузке на диск в разрезе баз',
        fields: {
            row_num: 'Номер строки',
            DatabaseName: 'Имя базы данных',
            physical_reads: 'Физические чтения',
            Physical_Reads_Percent: '% нагрузки по отношению к другим базам'
        }
    },
];