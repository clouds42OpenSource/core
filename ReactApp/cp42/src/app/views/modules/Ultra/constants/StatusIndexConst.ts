export const StatusIndexConst = [
    {
        head: 'Топ 20 индексов с высокими задержками',
        oldKey: 'db.odbc.get[SELECT_TOP20_Indexes_with_high_usage_costs,"{$MSSQL.DSN}"]',
        newKey: 'mssql.top20.index.with.high.usage["{$MSSQL.URI}","{$MSSQL.USER}","{$MSSQL.PASSWORD}"]',
        description: 'Этот показатель собирает индексы с высокими издержками',
        fields: {
            DatabaseName: 'Наименование базы данных',
            IndexName: 'Наименование индекса',
            Maintenance_cost: 'Стоимость обслуживания',
            Retrieval_usage: 'Применение извлечения',
            TableName: 'Наименование таблицы'
        }
    }
];