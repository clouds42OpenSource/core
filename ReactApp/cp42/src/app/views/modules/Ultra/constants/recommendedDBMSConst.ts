export const recommendedDBMSConst = [
    {
        head: 'MSSQL: Page life expectancy',
        pattern: {
            title: 'MSSQL: Page life expectancy',
            formatting: { unit: '', conversionFactor: 1 },
            criticalValue: 1000
        },
        key: 'mssql.page_life_expectancy',
        recommended: 'Не менее 1000',
        description: `Этот показатель отражает среднее время (в секундах), в течение которого страницы данных остаются в буферном кэше перед их заменой.
            Значения больше 1000 секунд указывают на эффективное использование кэша и минимальную загрузку I/O.`,
        compare: (value: number) => value > 300
    },
    {
        head: 'Buffer cache hit ratio',
        pattern: {
            title: 'Buffer cache hit ratio',
            formatting: { unit: '', conversionFactor: 1 },
            criticalValue: 100
        },
        key: 'mssql.buffer_cache_hit_ratio',
        recommended: 'Как можно ближе к 100',
        description: `Отношение попаданий в буферный кэш показывает, как часто страницы, запрашиваемые сервером, находятся в кэше, 
            а не загружаются с диска. Чем ближе значение к 100, тем лучше, поскольку это указывает на высокую эффективность кэша.`,
        compare: (value: number) => value > 95
    },
    {
        head: 'MSSQL: Cache hit ratio',
        pattern: {
            title: 'MSSQL: Cache hit ratio',
            formatting: { unit: '', conversionFactor: 1 },
            criticalValue: 100
        },
        key: 'mssql.cache_hit_ratio',
        recommended: 'Как можно ближе к 100',
        description: 'Похож на предыдущий показатель, но может относиться к другим типам кэша в MSSQL. Значение, близкое к 100, свидетельствует о высокой производительности кэша',
        compare: (value: number) => value > 70
    },
    {
        head: 'MSSQL: Page writes per second',
        pattern: {
            title: 'MSSQL: Page writes per second',
            formatting: { unit: '', conversionFactor: 1 },
            criticalValue: 90
        },
        key: 'mssql.page_writes_sec.rate',
        recommended: 'Не более 90',
        description: 'Показывает количество операций записи на диск в секунду. Идеально, если это значение меньше 90, что указывает на умеренную нагрузку на I/O.',
        compare: (value: number) => value < 90
    },
    {
        head: 'MSSQL: Lazy writes per second',
        pattern: {
            title: 'MSSQL: Lazy writes per second',
            formatting: { unit: '', conversionFactor: 1 },
            criticalValue: 20
        },
        key: 'mssql.lazy_writes_sec.rate',
        recommended: 'Не более 20',
        description: 'Отражает количество операций "ленивой" записи, когда измененные страницы асинхронно записываются на диск. Значение меньше 20 считается нормальным.',
        compare: (value: number) => value < 20
    },
    {
        head: 'MSSQL: Batch requests per second',
        pattern: {
            title: 'MSSQL: Batch requests per second',
            formatting: { unit: '', conversionFactor: 1 },
            criticalValue: null
        },
        key: 'mssql.batch_requests_sec.rate',
        description: 'Показывает количество пакетных запросов, обрабатываемых сервером в секунду. Важен для оценки общей производительности сервера.',
        recommended: 'Batch requests per second - SQL re-compilations per second + SQL компиляций > 90%',
        compare: (value: number, values: { key_: string, lastvalue: string }[]) => {
            const sqlCompilationsValue = values.find(({ key_ }) => key_ === 'mssql.sql_compilations_sec.rate')?.lastvalue;
            const sqlRecompilationsValue = values.find(({ key_ }) => key_ === 'mssql.sql_recompilations_sec.rate')?.lastvalue;

            const sqlCompilations = sqlCompilationsValue ? parseFloat(sqlCompilationsValue) : 0;
            const sqlRecompilations = sqlRecompilationsValue ? parseFloat(sqlRecompilationsValue) : 0;

            return ((value - (sqlCompilations + sqlRecompilations)) / value) * 100;
        },
    },
    {
        head: 'Average latch wait time',
        pattern: {
            title: 'Average latch wait time',
            formatting: { unit: '', conversionFactor: 1 },
            criticalValue: 10
        },
        key: 'mssql.average_latch_wait_time',
        recommended: 'Не более 10мс в среднем',
        description: 'Показывает среднее время ожидания блокировки (latch). Значение не должно превышать 10 мс в среднем для хорошей производительности.',
        compare: (value: number) => value <= 10
    },
    {
        head: 'Total average wait time',
        pattern: {
            title: 'Total average wait time',
            formatting: { unit: '', conversionFactor: 1 },
            criticalValue: 0
        },
        key: 'mssql.average_wait_time',
        recommended: 'Как можно ближе к 0',
        description: 'Отражает среднее время ожидания обработки запросов. Чем ближе к 0, тем лучше, так как это указывает на меньшие задержки.',
        compare: (value: number) => value < 5
    },
    {
        head: 'MSSQL: Lock wait time',
        pattern: {
            title: 'MSSQL: Lock wait time',
            formatting: { unit: '', conversionFactor: 1 },
            criticalValue: 0
        },
        key: 'mssql.lock_wait_time',
        recommended: '0',
        description: 'Показывает общее время ожидания блокировок. Идеальное значение — 0, что указывает на отсутствие задержек из-за блокировок.',
        compare: (value: number) => value === 0
    },
    {
        head: 'MSSQL: Total lock requests per second that have deadlocks',
        pattern: {
            title: 'MSSQL: Total lock requests per second that have deadlocks',
            formatting: { unit: '', conversionFactor: 1 },
            criticalValue: 0
        },
        key: 'mssql.number_deadlocks_sec.rate',
        recommended: '0',
        description: 'Показывает количество запросов в секунду, приводящих к взаимоблокировкам (deadlocks). Стремление к значению 0 важно для предотвращения проблем с производительностью.',
        compare: (value: number) => value === 0
    },
    {
        head: 'MSSQL: Total transactions number',
        key: 'mssql.transactions',
        recommended: 'Наблюдаем динамику',
        pattern: {
            title: 'MSSQL: Total transactions number',
            formatting: { unit: '', conversionFactor: 1 },
            criticalValue: null,
        },
        separate: true,
        description: 'Показывает общее количество транзакций. Важно наблюдать за динамикой для оценки нагрузки на сервер.',
    },
    {
        head: 'MSSQL: Number users connected',
        key: 'mssql.user_connections',
        recommended: 'Наблюдаем динамику',
        pattern: {
            title: 'MSSQL: Number users connected',
            formatting: { unit: '', conversionFactor: 1 },
            criticalValue: null,
        },
        separate: true,
        description: 'Отображает количество активных пользовательских соединений с сервером. Важно для оценки нагрузки и масштабируемости системы.',
    },
    {
        head: 'MSSQL: Checkpoint pages per second',
        key: 'mssql.checkpoint_pages_sec.rate',
        recommended: 'Наблюдаем динамику',
        pattern: {
            title: 'MSSQL: Checkpoint pages per second',
            formatting: { unit: '', conversionFactor: 1 },
            criticalValue: null,
        },
        separate: true,
        description: 'Этот показатель отображает количество страниц, записываемых в файл данных при каждом контрольном проходе. Важно наблюдать за динамикой этого показателя, чтобы понимать производительность и нагрузку на I/O.',
    },
    {
        head: 'MSSQL: Page reads per second',
        key: 'mssql.page_reads_sec.rate',
        recommended: 'Наблюдаем динамику',
        pattern: {
            title: 'MSSQL: Page reads per second',
            formatting: { unit: '', conversionFactor: 1 },
            criticalValue: null,
        },
        separate: true,
        description: 'Показывает количество чтений диска (загрузок страниц в кэш) в секунду. Высокие значения могут указывать на неэффективное использование кэша. Необходимо следить за динамикой.',
    }
];