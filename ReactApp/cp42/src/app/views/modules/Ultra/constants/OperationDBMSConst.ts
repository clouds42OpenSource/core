export const OperationDBMSConst = [
    {
        head: "MSSQL Job 'RegOps.Обновление статистик': Last run date-time",
        key: 'mssql.job.lastrundatetime["RegOps.Обновление статистик"]',
        group: 'updateStatistics'
    },
    {
        head: "MSSQL Job 'RegOps.Обновление статистик': Next run date-time",
        key: 'mssql.job.nextrundatetime["RegOps.Обновление статистик"]',
        group: 'updateStatistics'
    },
    {
        head: "MSSQL Job 'RegOps.Обновление статистик': Run status",
        key: 'mssql.job.runstatus["RegOps.Обновление статистик"]',
        group: 'updateStatistics'
    },
    {
        head: "MSSQL Job 'RegOps.Дефрагментация индексов': Last run date-time",
        key: 'mssql.job.lastrundatetime["RegOps.Дефрагментация индексов"]',
        group: 'indexDefragmentation'
    },
    {
        head: "MSSQL Job 'RegOps.Дефрагментация индексов': Next run date-time",
        key: 'mssql.job.nextrundatetime["RegOps.Дефрагментация индексов"]',
        group: 'indexDefragmentation'
    },
    {
        head: "MSSQL Job 'RegOps.Дефрагментация индексов': Run status",
        key: 'mssql.job.runstatus["RegOps.Дефрагментация индексов"]',
        group: 'indexDefragmentation'
    },
    {
        head: "MSSQL Job 'RegOps.Реиндексация таблиц базы данных': Last run date-time",
        key: 'mssql.job.lastrundatetime["RegOps.Реиндексация таблиц базы данных"]',
        group: 'tableReindexing'
    },
    {
        head: "MSSQL Job 'RegOps.Реиндексация таблиц базы данных': Next run date-time",
        key: 'mssql.job.nextrundatetime["RegOps.Реиндексация таблиц базы данных"]',
        group: 'tableReindexing'
    },
    {
        head: "MSSQL Job 'RegOps.Реиндексация таблиц базы данных': Run status",
        key: 'mssql.job.runstatus["RegOps.Реиндексация таблиц базы данных"]',
        group: 'tableReindexing'
    }
];