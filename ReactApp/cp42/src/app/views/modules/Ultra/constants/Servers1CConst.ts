export const Servers1CConst = [
    // {
    //     head: '1С лицензии - активные сеансы',
    //     key: '1c.ls.sessions[{$RAS_PORT},{$RAS_TIMEOUT},{$RAS_USER},{$RAS_PASS}]',
    //     pattern: {
    //         title: '',
    //         formatting: { unit: '', conversionFactor: 1 },
    //         criticalValue: null
    //     },
    //     recommended: 'Keep within recommended limits',
    //     description: 'Shows the total memory used by all working processes. Keeping this within recommended limits is essential.',
    //     compare: (value: number) => value >= 0 && value <= 32
    // },
    {
        head: '1С лицензии -  кол-во веб-клиентов',
        key: '1c.ls.license.webclient',
        pattern: {
            title: '',
            formatting: { unit: '', conversionFactor: 1 },
            criticalValue: null
        },
        recommended: 'Keep within recommended limits',
        description: 'Shows the total memory used by all working processes. Keeping this within recommended limits is essential.',
        compare: (value: number) => value >= 0 && value <= 32
    },
    {
        head: '1С лицензии - Кол-во уникальных пользователей',
        key: '1c.ls.license.uniq',
        pattern: {
            title: '',
            formatting: { unit: '', conversionFactor: 1 },
            criticalValue: null
        },
        recommended: 'Keep within recommended limits',
        description: 'Shows the total memory used by all working processes. Keeping this within recommended limits is essential.',
        compare: (value: number) => value >= 0 && value <= 32
    },
    {
        head: '1С лицензии - Общее кол-во сеансов',
        key: '1c.ls.license.total',
        pattern: {
            title: '',
            formatting: { unit: '', conversionFactor: 1 },
            criticalValue: null
        },
        recommended: 'Keep within recommended limits',
        description: 'Shows the total memory used by all working processes. Keeping this within recommended limits is essential.',
        compare: (value: number) => value >= 0 && value <= 32
    },
    {
        head: '1С менеджер кластера - Объем памяти всех процессов ',
        key: '1c.ws.rmngr.memory',
        pattern: {
            title: '',
            formatting: { unit: 'МБ', conversionFactor: 1 / (2 ** 20) },
            criticalValue: null
        },
        recommended: 'Keep within recommended limits',
        description: 'Shows the total memory used by all working processes. Keeping this within recommended limits is essential.',
        compare: (value: number) => value >= 0 && value <= 32
    },
    {
        head: '1С рабочий процесс - Число процессов ',
        key: '1c.ws.rphost.count',
        pattern: {
            title: '',
            formatting: { unit: '', conversionFactor: 1 },
            criticalValue: null
        },
        recommended: 'Keep within recommended limits',
        description: 'Shows the total memory used by all working processes. Keeping this within recommended limits is essential.',
        compare: (value: number) => value >= 0 && value <= 32
    },
    {
        head: '1С рабочий процесс -Объем памяти всех процессов',
        key: '1c.ws.rphost.memory',
        pattern: {
            title: '',
            formatting: { unit: 'МБ', conversionFactor: 1 / (2 ** 20) },
            criticalValue: null
        },
        recommended: 'Keep within recommended limits',
        description: 'Shows the total memory used by all working processes. Keeping this within recommended limits is essential.',
        compare: (value: number) => value >= 0 && value <= 32
    },
    {
        head: '1С Сеансы - количество http-сервисов',
        key: '1c.cs.sessions.http',
        pattern: {
            title: '',
            formatting: { unit: '', conversionFactor: 1 },
            criticalValue: null
        },
        recommended: 'Keep within recommended limits',
        description: 'Shows the total memory used by all working processes. Keeping this within recommended limits is essential.',
        compare: (value: number) => value >= 0 && value <= 32
    },
    {
        head: '1С Сеансы - Количество активных',
        key: '1c.cs.sessions.as',
        pattern: {
            title: '',
            formatting: { unit: '', conversionFactor: 1 },
            criticalValue: null
        },
        recommended: 'Keep within recommended limits',
        description: 'Shows the total memory used by all working processes. Keeping this within recommended limits is essential.',
        compare: (value: number) => value >= 0 && value <= 32
    },
    {
        head: '1С Сеансы - Количество веб-сервисов',
        key: '1c.cs.sessions.ws',
        pattern: {
            title: '',
            formatting: { unit: '', conversionFactor: 1 },
            criticalValue: null
        },
        recommended: 'Keep within recommended limits',
        description: 'Shows the total memory used by all working processes. Keeping this within recommended limits is essential.',
        compare: (value: number) => value >= 0 && value <= 32
    },
    {
        head: '1С Сеансы - Количество спящих',
        key: '1c.cs.sessions.hb',
        pattern: {
            title: '',
            formatting: { unit: '', conversionFactor: 1 },
            criticalValue: null
        },
        recommended: 'Keep within recommended limits',
        description: 'Shows the total memory used by all working processes. Keeping this within recommended limits is essential.',
        compare: (value: number) => value >= 0 && value <= 32
    },
    {
        head: '1С Сеансы - Количество фоновых заданий',
        key: '1c.cs.sessions.bg',
        pattern: {
            title: '',
            formatting: { unit: '', conversionFactor: 1 },
            criticalValue: null
        },
        recommended: 'Keep within recommended limits',
        description: 'Shows the total memory used by all working processes. Keeping this within recommended limits is essential.',
        compare: (value: number) => value >= 0 && value <= 32
    },
    {
        head: '1С Сеансы - Общее количество',
        key: '1c.cs.sessions.total',
        pattern: {
            title: '',
            formatting: { unit: '', conversionFactor: 1 },
            criticalValue: null
        },
        recommended: 'Keep within recommended limits',
        description: 'Shows the total memory used by all working processes. Keeping this within recommended limits is essential.',
        compare: (value: number) => value >= 0 && value <= 32
    },
    {
        head: '1С Сеансы - Текущая длительность вызова http-сервиса',
        key: '1c.cs.sessions.hsd',
        pattern: {
            title: '',
            formatting: { unit: 'с', conversionFactor: 1 },
            criticalValue: null
        },
        recommended: 'Keep within recommended limits',
        description: 'Shows the total memory used by all working processes. Keeping this within recommended limits is essential.',
        compare: (value: number) => value >= 0 && value <= 32
    },
    {
        head: '1С Сеансы - Текущая длительность вызова веб-сервиса',
        key: '1c.cs.sessions.wsd',
        pattern: {
            title: '',
            formatting: { unit: 'с', conversionFactor: 1 },
            criticalValue: null
        },
        recommended: 'Keep within recommended limits',
        description: 'Shows the total memory used by all working processes. Keeping this within recommended limits is essential.',
        compare: (value: number) => value >= 0 && value <= 32
    },
    {
        head: '1С Сеансы - Текущая длительность вызова фонового',
        key: '1c.cs.sessions.bgd',
        pattern: {
            title: '',
            formatting: { unit: 'с', conversionFactor: 1 },
            criticalValue: null
        },
        recommended: 'Keep within recommended limits',
        description: 'Shows the total memory used by all working processes. Keeping this within recommended limits is essential.',
        compare: (value: number) => value >= 0 && value <= 32
    },
    {
        head: '1С Сеансы - Текущая длительность пользовательского ',
        key: '1c.cs.sessions.cld',
        pattern: {
            title: '',
            formatting: { unit: 'с', conversionFactor: 1 },
            criticalValue: null
        },
        recommended: 'Keep within recommended limits',
        description: 'Shows the total memory used by all working processes. Keeping this within recommended limits is essential.',
        compare: (value: number) => value >= 0 && value <= 32
    },
    // {
    //     head: '1С Блокировки - Общее ожидание на блокировках',
    //     key: '1c.ws.locks.wait',
    //     pattern: {
    //         title: '',
    //         formatting: { unit: 'с', conversionFactor: 1 },
    //         criticalValue: null
    //     },
    //     recommended: 'Keep within recommended limits',
    //     description: 'Shows the total memory used by all working processes. Keeping this within recommended limits is essential.',
    //     compare: (value: number) => value >= 0 && value <= 32
    // },
    // {
    //     head: '1С Блокировки - Число блокировок',
    //     key: '1c.ws.locks.lock',
    //     pattern: {
    //         title: '',
    //         formatting: { unit: '', conversionFactor: 1 },
    //         criticalValue: null
    //     },
    //     recommended: 'Keep within recommended limits',
    //     description: 'Shows the total memory used by all working processes. Keeping this within recommended limits is essential.',
    //     compare: (value: number) => value >= 0 && value <= 32
    // },
    // {
    //     head: '1С Блокировки - Число взаимоблокировок',
    //     key: '1c.ws.locks.deadlock',
    //     pattern: {
    //         title: '',
    //         formatting: { unit: '', conversionFactor: 1 },
    //         criticalValue: null
    //     },
    //     recommended: 'Keep within recommended limits',
    //     description: 'Shows the total memory used by all working processes. Keeping this within recommended limits is essential.',
    //     compare: (value: number) => value >= 0 && value <= 32
    // },
    // {
    //     head: '1С Блокировки - Число таймаутов',
    //     key: '1c.ws.locks.timeout',
    //     pattern: {
    //         title: '',
    //         formatting: {
    //             unit: '', conversionFactor: 1 },
    //         criticalValue: null
    //     },
    //     recommended: 'Keep within recommended limits',
    //     description: 'Shows the total memory used by all working processes. Keeping this within recommended limits is essential.',
    //     compare: (value: number) => value >= 0 && value <= 32
    // },
    {
        head: '1С Менеджер кластера - Число процессов',
        key: '1c.ws.rmngr.count',
        pattern: {
            title: '',
            formatting: { unit: '', conversionFactor: 1 },
            criticalValue: null
        },
        recommended: 'Keep within recommended limits',
        description: 'Shows the total memory used by all working processes. Keeping this within recommended limits is essential.',
        compare: (value: number) => value >= 0 && value <= 32
    }
];