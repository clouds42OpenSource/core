export type KeyedZabbixConfigItem = {
    head: string;
    oldKey: string;
    newKey: string;
    description?: string;
    fields?: Record<string, string>;
};