import { getZabbixItemsResponseDto } from 'app/api/endpoints/zabbix/response';

export type ZabbixItemForOperations = {
    head: string;
    key: string;
    group: string;
};

export type ZabbixItemsForOperations = Array<getZabbixItemsResponseDto>;