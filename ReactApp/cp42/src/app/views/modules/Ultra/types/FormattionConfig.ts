export type FormattingConfig = {
    unit: string;
    conversionFactor: number;
};