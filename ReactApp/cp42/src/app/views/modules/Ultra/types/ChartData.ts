export type ChartData = {
    labels: Date[];
    datasets: {
        label: string;
        data: number[];
        fill: boolean;
        borderColor: string;
    }[];
};