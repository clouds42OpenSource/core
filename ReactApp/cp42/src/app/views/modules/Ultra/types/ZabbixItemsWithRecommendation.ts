import { getZabbixItemsResponseDto } from 'app/api/endpoints/zabbix/response';
import { Condition } from 'app/views/modules/Ultra/types/Condition';

export type ZabbixItemWithRecommendation = {
    head: string;
    key: string;
    recommended?: string;
    separate?: boolean;
    description: string;
    pattern: Condition;
    compare?: (value: number, values: { key_: string, lastvalue: string }[]) => boolean;
} & getZabbixItemsResponseDto;

export type ZabbixItemsWithRecommendation = ZabbixItemWithRecommendation[];