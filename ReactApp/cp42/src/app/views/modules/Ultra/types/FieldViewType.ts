import React from 'react';

export type FieldViewType = {
    [key: string]: {
        caption: string;
        fieldWidth: string;
        format: (value: string) => string | React.ReactNode;
    }
};

export type FieldInfo = {
    name: string;
    format?: (value: string) => string;
};