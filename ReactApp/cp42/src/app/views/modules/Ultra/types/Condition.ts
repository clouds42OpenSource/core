import { FormattingConfig } from 'app/views/modules/Ultra/types/FormattionConfig';

export type Condition = {
    title: string;
    pattern?: RegExp;
    formatting: FormattingConfig;
    criticalValue?: number;
};