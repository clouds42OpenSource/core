export enum OperationStatusEnum {
    FAILURE,
    SUCCESS,
    RETRY,
    CANCELLED,
    IN_PROGRESS
}