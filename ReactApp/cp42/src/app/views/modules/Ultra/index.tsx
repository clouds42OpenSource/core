/* eslint-disable no-underscore-dangle */
import { Box, SelectChangeEvent, MenuItem, Select, InputLabel } from '@mui/material';
import { getZabbixHostGroupsResponseDto } from 'app/api/endpoints/zabbix/response';
import { FETCH_API } from 'app/api/useFetchApi';
import { AccountUserGroup } from 'app/common/enums';
import { AppReduxStoreState } from 'app/redux/types';
import { COLORS } from 'app/utils';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { UltraPerformance } from 'app/views/modules/Ultra/tabs/UltraPerformance';
import { UltraServers } from 'app/views/modules/Ultra/tabs/UltraServers';
import { connect } from 'react-redux';
import { useEffect, useState } from 'react';
import { useLocation } from 'react-router';
import { Helmet } from 'react-helmet';
import { TextOut } from 'app/views/components/TextOut';
import { ReceiveMyCompanyThunk } from 'app/modules/myCompany/store/thunks';
import { ReceiveMyCompanyThunkParams } from 'app/modules/myCompany/store/reducers/receiveMyCompanyData/params';
import { useAppSelector } from 'app/hooks';

type StateProps = {
    currentUserGroups: AccountUserGroup[];
    isCompanyLoading: boolean;
};

type DispatchProps = {
    dispatchReceiveMyCompanyThunk: (args: ReceiveMyCompanyThunkParams) => void;
};

const UltraPage = ({ currentUserGroups, isCompanyLoading, dispatchReceiveMyCompanyThunk }: StateProps & DispatchProps) => {
    const [isLoading, setIsLoading] = useState(false);
    const [hostGroup, setHostGroup] = useState<getZabbixHostGroupsResponseDto | null>(null);
    const [hostGroups, setHostGroups] = useState<getZabbixHostGroupsResponseDto[]>([]);
    const myCompany = useAppSelector(state => state.MyCompanyState.myCompany);

    const handleSelect = (event: SelectChangeEvent<{ value: unknown }>) => {
        const findedGroup = hostGroups.find(group => group.groupid === event.target.value as string);
        if (findedGroup) {
            setHostGroup(findedGroup);
        }
    };

    useEffect(() => {
        dispatchReceiveMyCompanyThunk({ force: true });
    }, [dispatchReceiveMyCompanyThunk]);

    useEffect(() => {
        if (myCompany.indexNumber) {
            (async () => {
                setIsLoading(true);
                const response = await FETCH_API.ZABBIX.getHostGroups();
                if (response.result) {
                    setHostGroups(response.result);
                    const targetId = myCompany.indexNumber;
                    const matchingGroup = response.result.find(group => {
                        const parts = group.name.split('&&');
                        return parts[1] === String(targetId);
                    });
                    if (matchingGroup) {
                        setHostGroup(matchingGroup);
                    }
                }
                setIsLoading(false);
            })();
        }
    }, [myCompany]);

    const location = useLocation();

    const getTitle = (pathname: string) => {
        switch (pathname) {
            case '/ultra/performance':
                return 'Анализ производительности 1С';
            case '/ultra/servers':
            default:
                return 'Мониторинг серверов';
        }
    };

    let ContentComponent;
    switch (location.pathname) {
        case '/ultra/performance':
            ContentComponent = UltraPerformance;
            break;
        case '/ultra/servers':
        default:
            ContentComponent = UltraServers;
            break;
    }

    if (isLoading || !isCompanyLoading) {
        return <LoadingBounce />;
    }

    if (!hostGroup) {
        return (
            <Box>
                <Helmet>
                    <title>
                        { getTitle(location.pathname) }
                    </title>
                </Helmet>
                { (currentUserGroups.includes(AccountUserGroup.CloudAdmin) || currentUserGroups.includes(AccountUserGroup.Hotline)) && (
                    <Box marginBottom="8px">
                        <InputLabel id="group-select-label">Выберите группу хостов</InputLabel>
                        <Select
                            style={ { minWidth: '200px' } }
                            labelId="group-select-label"
                            value={ { value: '' } }
                            onChange={ handleSelect }
                            renderValue={ ({ value }) => {
                                if (value === '') {
                                    return 'Группа хостов';
                                }
                                return value;
                            } }
                        >
                            { hostGroups.map(group => (
                                <MenuItem key={ group.groupid } value={ group.groupid }>
                                    { group.name }
                                </MenuItem>
                            )) }
                        </Select>
                    </Box>
                ) }
                <TextOut>
                    Данный сервис недоступен для вас.
                </TextOut>
                <TextOut>
                    Для его подключения и ознакомления необходимо обратиться в нашу
                    <a style={ { color: COLORS.main } } target="_blank" href="https://t.me/clouds42bot" rel="noreferrer"> службу поддержки </a>
                </TextOut>
            </Box>
        );
    }

    return (
        <>
            <Helmet>
                <title>
                    { getTitle(location.pathname) }
                </title>
            </Helmet>
            { (currentUserGroups.includes(AccountUserGroup.CloudAdmin) || currentUserGroups.includes(AccountUserGroup.Hotline)) && (
                <Box marginBottom="8px">
                    <InputLabel id="group-select-label">Выберите группу хостов</InputLabel>
                    <Select
                        labelId="group-select-label"
                        value={ { value: hostGroup.name } }
                        onChange={ handleSelect }
                        renderValue={ ({ value }) => value }
                    >
                        { hostGroups.map(group => (
                            <MenuItem key={ group.groupid } value={ group.groupid }>
                                { group.name }
                            </MenuItem>
                        )) }
                    </Select>
                </Box>
            ) }
            <ContentComponent hostGroup={ hostGroup } />
        </>
    );
};

const UltraPageConnected = connect<StateProps, DispatchProps, object, AppReduxStoreState>(
    state => {
        const { settings, hasSuccessFor } = state.Global.getCurrentSessionSettingsReducer;
        const { hasSettingsReceived } = hasSuccessFor;
        const currentUserGroups = hasSettingsReceived ? settings.currentContextInfo.currentUserGroups : [];

        return {
            currentUserGroups,
            isCompanyLoading: state.MyCompanyState.hasSuccessFor.hasMyCompanyReceived,
        };
    },
    {
        dispatchReceiveMyCompanyThunk: ReceiveMyCompanyThunk.invoke
    }
)(UltraPage);

export const UltraPageView = UltraPageConnected;