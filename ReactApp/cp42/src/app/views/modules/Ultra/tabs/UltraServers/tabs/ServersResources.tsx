import { Box } from '@mui/material';
import { useEffect, useState } from 'react';
import { TextOut } from 'app/views/components/TextOut';
import { getZabbixHostsResponseDto } from 'app/api/endpoints/zabbix/response';
import css from '../../../styles.module.css';
import { UltraCommonInfo } from './components/UltraCommonInfo';

type Props = {
    hosts: getZabbixHostsResponseDto[];
    handleSetIsLoading: (value: boolean) => void;
};

export const ServersResources = ({ hosts, handleSetIsLoading }: Props) => {
    const [totalCPUSize, setTotalCPUSize] = useState(0);
    const [totalMemorySize, setTotalMemorySize] = useState(0);
    const [totalDiskSize, setTotalDiskSize] = useState(0);

    const handleSummarySize = (value: number, type: string) => {
        switch (type) {
            case 'CPU':
                setTotalCPUSize(current => current + value);
                break;
            case 'memory':
                setTotalMemorySize(current => current + value);
                break;
            default:
                setTotalDiskSize(current => current + value);
        }
    };

    useEffect(() => {
        setTotalCPUSize(0);
        setTotalMemorySize(0);
        setTotalDiskSize(0);
    }, [hosts]);

    return (
        <Box>
            <Box className={ css.summaryInfoList }>
                <TextOut inDiv={ true } fontSize={ 14 }>
                    Общее количество ресурсов
                </TextOut>
                <Box>
                    <Box>
                        <Box>
                            <TextOut>
                                Ядра ЦП, единиц
                            </TextOut>
                            <TextOut>
                                { totalCPUSize }
                            </TextOut>
                        </Box>
                        <Box>
                            <TextOut>
                                Оперативная память, ГБ
                            </TextOut>
                            <TextOut>
                                { totalMemorySize.toFixed(0) }
                            </TextOut>
                        </Box>
                        <Box>
                            <TextOut>
                                Дисковое пространство, ГБ
                            </TextOut>
                            <TextOut>
                                { totalDiskSize }
                            </TextOut>
                        </Box>
                    </Box>
                </Box>
            </Box>
            <TextOut fontWeight={ 500 }>
                Перечень серверов
            </TextOut>
            <Box mt={ 2 } className={ `${ css.commonInfoList } ${ css.serversResources }` }>
                <Box>
                    <TextOut inDiv={ true }>
                        Название сервера
                    </TextOut>
                    <TextOut inDiv={ true }>
                        Описание сервера
                    </TextOut>
                    <TextOut inDiv={ true }>
                        Ядра ЦП, единиц
                    </TextOut>
                    <TextOut inDiv={ true }>
                        Оперативная память, ГБ
                    </TextOut>
                    <TextOut inDiv={ true }>
                        Дисковое пространство (имя диска, общая емкость, свободно), ГБ
                    </TextOut>
                </Box>
                { hosts.map(host => <UltraCommonInfo handleSummarySize={ handleSummarySize } setIsLoading={ handleSetIsLoading } key={ host.hostid } host={ host } />) }
            </Box>
        </Box>
    );
};