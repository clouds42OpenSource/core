import React, { memo } from 'react';
import { Box } from '@mui/material';
import { Victims } from 'app/api/endpoints/blockAnalysis/response/getBlockAnalysisLock';
import dayjs from 'dayjs';
import { TextOut } from 'app/views/components/TextOut';
import { useSort } from '../hooks/useSort';
import { sortData } from '../utils/common';
import { SortableColumnHeader } from './subcomponents/SortableColumnHeader';
import styles from '../style.module.css';
import { LockedObjectView } from './subcomponents/LockedObjectView';

type Props = {
    lockList: Victims[];
};

export const VictimsTable = memo(({ lockList }: Props) => {
    const { sortConfig, handleSort } = useSort<Victims>('datetime');

    const getSortedLocks = (): Victims[] => {
        return sortData(lockList, sortConfig.fieldName, sortConfig.sortKind);
    };

    return (
        <Box className={ styles.victimsTable }>
            <Box className={ styles.victimsTableHead }>
                <SortableColumnHeader sortConfig={ sortConfig } fieldName="datetime" handleSort={ handleSort } label="Дата блокировки" />
                <SortableColumnHeader sortConfig={ sortConfig } fieldName="waitTime" handleSort={ handleSort } label="Время ожидания" />
                <TextOut>Детали события</TextOut>
                <TextOut>Заблокированный пользователь</TextOut>
            </Box>
            <Box className={ styles.victimsContent }>
                { lockList.length ? (
                    getSortedLocks().map(record => (
                        <Box key={ `${ record.id }-${ record.datetime }` } className={ styles.victimsTableContent }>
                            <TextOut>{ dayjs(record.datetime).format('DD.MM.YYYY HH:mm:ss') }</TextOut>
                            <TextOut>{ record.waitTime }</TextOut>
                            <LockedObjectView context={ record.context } userName={ record.user } lockedObjects={ record.lockedObjects } />
                            <TextOut>{ record.user }</TextOut>
                        </Box>
                    ))
                ) : (
                    <TextOut>
                        Выберите виновника в таблице слева.
                    </TextOut>
                ) }
            </Box>
        </Box>
    );
});