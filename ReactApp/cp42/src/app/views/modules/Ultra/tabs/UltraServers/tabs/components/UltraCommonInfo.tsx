/* eslint-disable no-underscore-dangle */
import { Box } from '@mui/material';
import { getZabbixHostsResponseDto, getZabbixItemsResponseDto } from 'app/api/endpoints/zabbix/response';
import { FETCH_API } from 'app/api/useFetchApi';
import { TextOut } from 'app/views/components/TextOut';
import { useEffect, useState } from 'react';

type Props = {
    host: getZabbixHostsResponseDto;
    setIsLoading: (value: boolean) => void;
    handleSummarySize: (value: number, type: string) => void;
};

export const UltraCommonInfo = ({ host, setIsLoading, handleSummarySize }: Props) => {
    const [cpuInfo, setCpuInfo] = useState<getZabbixItemsResponseDto | null>(null);
    const [memoryInfo, setMemoryInfo] = useState<getZabbixItemsResponseDto | null>(null);
    const [diskInfo, setDiskInfo] = useState<Record<string, { total?: string; free?: string }>>({});
    const [description, setDescription] = useState<getZabbixItemsResponseDto | null>(null);

    useEffect(() => {
        (async () => {
            setIsLoading(true);
            const response = await FETCH_API.ZABBIX.getItems(host.hostid);
            setIsLoading(false);
            if (response.result) {
                const cpuResult = response.result.find(item => item.key_ === 'system.cpu.num[]') ?? null;
                const memoryResult = response.result.find(item => item.key_ === 'vm.memory.size[total]') ?? null;
                const descriptionResult = response.result.find(item => item.key_ === 'win.description') ?? null;
                let diskResult = 0;
                setCpuInfo(cpuResult);
                setMemoryInfo(memoryResult);
                setDescription(descriptionResult);

                const diskItems = response.result.filter(item => item.key_.startsWith('vfs.fs.size'));

                const diskInfoMap: Record<string, { total?: string; free?: string }> = {};

                diskItems.forEach(item => {
                    if (Number(item.lastvalue)) {
                        const match = item.key_.match(/vfs.fs.size\[(.+?),/);
                        if (match && match[1]) {
                            const path = match[1];
                            if (!diskInfoMap[path]) {
                                diskInfoMap[path] = {};
                            }

                            if (item.key_.includes('total')) {
                                diskInfoMap[path].total = item.lastvalue;
                                if (Number(item.lastvalue)) {
                                    diskResult += Math.ceil(Number(item.lastvalue) / (2 ** 30));
                                }
                            } else if (item.key_.includes('free') && !item.key_.includes('pfree')) {
                                diskInfoMap[path].free = item.lastvalue;
                            }
                        }
                    }
                });
                if (cpuResult?.lastvalue) {
                    handleSummarySize(Number(cpuResult.lastvalue), 'CPU');
                }
                if (memoryResult?.lastvalue) {
                    handleSummarySize((Number(memoryResult.lastvalue) / 2 ** 30), 'memory');
                }
                if (diskResult) {
                    handleSummarySize(diskResult, 'disk');
                }

                setDiskInfo(diskInfoMap);
            }
        })();
    }, [host.hostid]);

    return (
        <div>
            <Box>
                <TextOut>
                    { host.name }
                </TextOut>
            </Box>
            <Box>
                { description && (
                    <TextOut>
                        { description.lastvalue }
                    </TextOut>
                ) }
            </Box>
            <Box>
                { cpuInfo && (
                    <TextOut>
                        { cpuInfo.lastvalue }
                    </TextOut>
                ) }
            </Box>
            <Box>
                { memoryInfo && (
                    <TextOut>
                        { Math.ceil((Number(memoryInfo.lastvalue) / 2 ** 30)).toFixed(0) }
                    </TextOut>
                ) }
            </Box>
            <Box display="flex" gap="3px" flexDirection="column">
                { Object.keys(diskInfo).map(path => (
                    <TextOut key={ path }>
                        { `${ path } ${ diskInfo[path].total ? Math.ceil((Number(diskInfo[path].total) / (2 ** 30))).toFixed(0) : 'N/A' } / 
                        ${ diskInfo[path].free ? (Number(diskInfo[path].free) / (2 ** 30)).toFixed(2) : 'N/A' }` }
                    </TextOut>
                )) }
            </Box>
        </div>
    );
};