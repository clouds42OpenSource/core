import { useState } from 'react';
import { SortConfig } from '../types/common';

export const useSort = <T extends object>(initialField: keyof T) => {
    const [sortConfig, setSortConfig] = useState<SortConfig<T>>({ fieldName: initialField, sortKind: 0 });

    const handleSort = (fieldName: keyof T) => {
        const currentSortKind = sortConfig.fieldName === fieldName ? sortConfig.sortKind : undefined;
        const newSortKind = currentSortKind === 1 ? 0 : 1;

        setSortConfig({ fieldName, sortKind: newSortKind });
    };

    return { sortConfig, handleSort };
};