export type SortConfig<T> = {
    fieldName: keyof T;
    sortKind: number;
};