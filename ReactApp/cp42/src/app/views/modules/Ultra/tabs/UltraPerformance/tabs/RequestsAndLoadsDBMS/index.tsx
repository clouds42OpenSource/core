/* eslint-disable no-underscore-dangle */
import { HelpOutline as HelpOutlineIcon } from '@mui/icons-material';
import { Box, IconButton, Tooltip } from '@mui/material';
import { getZabbixItemsResponseDto } from 'app/api/endpoints/zabbix/response';
import { ReplaceNullOrUndefinedUtility } from 'app/utils/ReplaceNullOrUndefinedUtility';
import { TextOut } from 'app/views/components/TextOut';
import { QueryHistoryModal } from 'app/views/modules/Ultra/components/QueryHistoryModal';
import { RequestsAndLoadsDBMSConst } from 'app/views/modules/Ultra/constants/RequestsAndLoadsDBMSConst';
import { FieldViewType } from 'app/views/modules/Ultra/types';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { memo } from 'react';

type Props = {
    requestsAndLoadsDBMSValues: getZabbixItemsResponseDto[];
    useNewKeys: boolean;
};

const RequestsAndLoadsDBMSView = ({ requestsAndLoadsDBMSValues, useNewKeys = false }: Props) => {
    return (
        <div style={ { display: 'grid', gridTemplateColumns: '100%' } }>
            { RequestsAndLoadsDBMSConst.map(correspondingConst => {
                const data = requestsAndLoadsDBMSValues.find(constant => constant.key_ === correspondingConst[useNewKeys ? 'newKey' : 'oldKey']);
                if (data && correspondingConst.fields) {
                    try {
                        const parsedData = ReplaceNullOrUndefinedUtility(JSON.parse(data.lastvalue));
                        const fieldsView = Object.entries(correspondingConst.fields).reduce((acc: FieldViewType, [key, caption]) => {
                            acc[key] = {
                                caption,
                                fieldWidth: 'auto',
                                format: value => value
                            };
                            return acc;
                        }, {});
                        return (
                            <Box key={ data.itemid } marginTop="20px" padding="0px 12px 4px 12px">
                                <Box display="flex" justifyContent="space-between" alignItems="center">
                                    <TextOut inDiv={ true } style={ { display: 'flex', alignItems: 'center' } } fontSize={ 20 } fontWeight={ 500 }>
                                        { correspondingConst.head }
                                        <Tooltip title={ correspondingConst.description }>
                                            <IconButton aria-label="prompt">
                                                <HelpOutlineIcon />
                                            </IconButton>
                                        </Tooltip>
                                    </TextOut>
                                    <TextOut>
                                        <QueryHistoryModal useNewKeys={ useNewKeys } item={ data } />
                                    </TextOut>
                                </Box>
                                <CommonTableWithFilter
                                    uniqueContextProviderStateId={ `table-${ data.itemid }` }
                                    isResponsiveTable={ true }
                                    isBorderStyled={ true }
                                    tableProps={ {
                                        dataset: parsedData,
                                        keyFieldName: Object.keys(correspondingConst.fields)[0],
                                        fieldsView
                                    } }
                                />
                            </Box>
                        );
                    } catch {
                        return null;
                    }
                }

                return null;
            }) }
        </div>
    );
};

export const RequestsAndLoadsDBMS = memo(RequestsAndLoadsDBMSView);