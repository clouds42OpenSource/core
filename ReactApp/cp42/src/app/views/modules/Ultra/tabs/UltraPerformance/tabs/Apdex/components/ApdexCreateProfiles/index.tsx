/* eslint-disable react/no-unstable-nested-components */
import { Box, TextField, Checkbox, Button, Select, MenuItem, Collapse } from '@mui/material';
import { IdNameRecord, KeyOperation } from 'app/api/endpoints/apdex/common';
import { DatabaseKeyOperationsResponseDto } from 'app/api/endpoints/apdex/response';
import { TextOut } from 'app/views/components/TextOut';
import { SuccessButton, TextButton } from 'app/views/components/controls/Button';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { SelectDataCommonDataModel } from 'app/web/common';
import { ChangeEvent, useCallback, useEffect, useState } from 'react';
import { EMessageType, useDebounce, useFloatMessages } from 'app/hooks';
import { useDispatch } from 'react-redux';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { templateErrorText } from 'app/api';
import { REDUX_API } from 'app/api/useReduxApi';
import { KeyboardArrowDown as KeyboardArrowDownIcon, KeyboardArrowRight as KeyBoardArrowRightIcon } from '@mui/icons-material';
import styles from '../../styles.module.css';

type Props = {
    databaseList: IdNameRecord[];
    hostGroupId: string;
};

const { APDEX_REDUX } = REDUX_API;

export const ApdexCreateProfile = ({ databaseList, hostGroupId }: Props) => {
    const [databaseKeyOperations, setDatabaseKeyOperations] = useState<DatabaseKeyOperationsResponseDto | null>(null);
    const [selectedKeyOperations, setSelectedKeyOperations] = useState<KeyOperation[]>([]);
    const [selectedDatabase, setSelectedDatabase] = useState('');
    const [isLoading, setIsLoading] = useState(false);
    const [profileName, setProfileName] = useState('');
    const [searchString, setSearchString] = useState<string | null>(null);
    const [isOpened, setIsOpened] = useState(false);
    const dispatch = useDispatch();
    const { show } = useFloatMessages();
    const debouncedServicesSearchString = useDebounce(searchString);

    const handleCheckboxChange = (event: ChangeEvent<HTMLInputElement>) => {
        const { value, checked } = event.target;
        setSelectedKeyOperations(prevSelected =>
            checked
                ? [...prevSelected, { id: value }]
                : prevSelected.filter(item => item.id !== value)
        );
    };

    const handleGetDatabaseKeyOperations = useCallback(async (paginationData: { pageNumber: number, pageSize: number }) => {
        const { pageNumber, pageSize = 10 } = paginationData;

        if (selectedDatabase) {
            setIsLoading(true);
            const response = await APDEX_REDUX.getDatabaseKeyOperations({ clientName: hostGroupId, clientId: selectedDatabase, pageNumber, pageSize, query: { keyOperationName: debouncedServicesSearchString } });
            setIsLoading(false);

            if (response.data && response.success) {
                setDatabaseKeyOperations(response.data);
            } else {
                show(EMessageType.error, response.message ?? templateErrorText);
            }
        } else {
            setDatabaseKeyOperations(null);
        }
    }, [selectedDatabase, hostGroupId, show, debouncedServicesSearchString]);

    const handleDataSelect = (value: SelectDataCommonDataModel<void>) => {
        const { pageNumber } = value;
        handleGetDatabaseKeyOperations({ pageNumber, pageSize: databaseKeyOperations?.pagination['page-size'] ?? 10 });
    };

    const handleCreateProfile = async () => {
        if (profileName && selectedKeyOperations.length) {
            const data = {
                database: {
                    id: selectedDatabase
                },
                name: profileName,
                keyOperations: selectedKeyOperations
            };
            setIsLoading(true);
            const response = await APDEX_REDUX.createApdexProfile({ clientName: hostGroupId, data });
            setIsLoading(false);

            if (response.success) {
                show(EMessageType.success, 'Профиль создан. Данные скоро появятся');
                void APDEX_REDUX.getApdexProfiles(dispatch, hostGroupId, selectedDatabase);
            } else {
                show(EMessageType.error, response.message ?? templateErrorText);
            }
        } else {
            show(EMessageType.warning, 'Заполните все поля');
        }
    };

    useEffect(() => {
        handleGetDatabaseKeyOperations({ pageNumber: 1, pageSize: 10 });
        setSelectedKeyOperations([]);
    }, [handleGetDatabaseKeyOperations]);

    return (
        <Box className={ styles.collapseWrapper }>
            { isLoading && <LoadingBounce /> }
            <TextButton onClick={ () => setIsOpened(!isOpened) }>
                { isOpened ? <KeyboardArrowDownIcon /> : <KeyBoardArrowRightIcon /> }
                Добавить профиль
            </TextButton>
            <Collapse in={ isOpened }>
                <Box className={ styles.wrapper } sx={ { padding: '12px' } }>
                    <TextOut fontWeight={ 500 }>
                        Как добавить профиль расширенного мониторинга производительности 1С?
                    </TextOut>
                    <ul style={ { fontSize: 14 } }>
                        <li>
                            <b> Получите список ключевых операций.</b> Выберите базу 1С и получите рекомендации по списку ключевых операций.
                        </li>
                        <li>
                            <b>Создайте профиль мониторинга ключевых операций.</b> Ключевые операции можно добавить в персонализированный мониторинг и отслеживать динамику скорости их выполнения по методике APDEX.
                            Для этого выберите операции из списка, которые хотите отслеживать, отметив их. Далее внизу под списком введите имя создаваемого профиля и нажмите “Создать профиль”.
                            После создания профиля, его можно будет выбрать в разделе “Расширенный мониторинг производительности 1С” для соответствующей базы 1С.
                        </li>
                    </ul>
                    <Box display="flex" gap="6px">
                        <Select
                            value={ selectedDatabase }
                            onChange={ e => setSelectedDatabase(e.target.value) }
                            displayEmpty={ true }
                        >
                            <MenuItem value="">
                                Выберите базу
                            </MenuItem>
                            {
                                databaseList.map(item => (
                                    <MenuItem key={ item.id } value={ item.id }>
                                        { item.name }
                                    </MenuItem>
                                ))
                            }
                        </Select>
                        { databaseKeyOperations && <TextField onChange={ ({ target }) => setSearchString(target.value) } placeholder="Поиск" /> }
                    </Box>
                    { databaseKeyOperations && (
                        <Box className={ styles.wrapper }>
                            <CommonTableWithFilter
                                uniqueContextProviderStateId="expectationDBMS"
                                isResponsiveTable={ true }
                                isBorderStyled={ true }
                                tableProps={ {
                                    pagination: {
                                        totalPages: databaseKeyOperations.pagination['total-pages'],
                                        recordsCount: databaseKeyOperations.pagination['total-items'],
                                        currentPage: databaseKeyOperations.pagination['page-number'],
                                        pageSize: databaseKeyOperations.pagination['page-size'],
                                    },
                                    dataset: databaseKeyOperations.keyOperations,
                                    keyFieldName: 'name',
                                    fieldsView: {
                                        id: {
                                            caption: 'Чекбокс',
                                            fieldWidth: 'auto',
                                            format: value => <Checkbox
                                                value={ value }
                                                checked={ selectedKeyOperations.some(item => item.id === value) }
                                                onChange={ handleCheckboxChange }
                                            />
                                        },
                                        name: {
                                            caption: 'Ключевая операция',
                                            fieldWidth: '50%',
                                        },
                                        measuringCount: {
                                            caption: 'Кол-во замеров',
                                            fieldWidth: 'auto',
                                            format: value => String(value)
                                        }
                                    }
                                } }
                                onDataSelect={ handleDataSelect }
                            />
                            <Box display="flex" justifyContent="space-between">
                                <Button
                                    onClick={ () => handleGetDatabaseKeyOperations({ pageNumber: 1, pageSize: databaseKeyOperations.pagination['page-size'] + 50 }) }
                                    disabled={ databaseKeyOperations.pagination['page-size'] >= databaseKeyOperations.pagination['total-items'] }
                                >
                                    Добавить 50
                                </Button>
                                <Button
                                    onClick={ () => handleGetDatabaseKeyOperations({ pageNumber: 1, pageSize: databaseKeyOperations.pagination['total-items'] }) }
                                    disabled={ databaseKeyOperations.pagination['total-pages'] === 1 }
                                >
                                    Показать все
                                </Button>
                            </Box>
                            <Box display="flex" alignItems="center" gap="6px">
                                <TextOut>
                                    Введите имя создаваемого профиля
                                </TextOut>
                                <TextField value={ profileName } onChange={ e => setProfileName(e.target.value) } />
                                <SuccessButton onClick={ handleCreateProfile }>
                                    Создать профиль
                                </SuccessButton>
                            </Box>
                        </Box>
                    ) }
                </Box>
            </Collapse>
        </Box>
    );
};