import { Box } from '@mui/material';
import { templateErrorText } from 'app/api';
import { DashboardAttributes, SavedObject } from 'app/api/endpoints/kibana/response';
import { getZabbixHostGroupsResponseDto } from 'app/api/endpoints/zabbix/response';
import { FETCH_API } from 'app/api/useFetchApi';
import { EMessageType, useAppSelector, useFloatMessages } from 'app/hooks';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { memo, useEffect, useState } from 'react';
import { TextOut } from 'app/views/components/TextOut';
import { ReviewModal } from 'app/views/modules/Ultra/components/ReviewModal';
import { SuggestImprovementsModal } from 'app/views/modules/Ultra/components/SuggestImprovementModal';
import { ReferenceComponent } from '../components/ReferenceComponent';

type Props = {
    hostGroup: getZabbixHostGroupsResponseDto;
};

const AnalysisMagazineErrorsReference = ({ isDefaultOpen = false, companyId } : { isDefaultOpen?: boolean, companyId: number }) => {
    return (
        <Box marginBottom="16px">
            <ReferenceComponent
                extraButtons={
                    <SuggestImprovementsModal
                        functionalityName="Анализ ошибок 1С"
                        companyId={ companyId }
                    />
                }
                isDefaultOpen={ isDefaultOpen }
            >
                <b>Общая информация</b>
                <span>
                    Используя функционал анализа ошибок 1С, можно проанализировать ошибки системы, отсортированные по категориям.
                </span>
                <b>Сценарий использования:</b>
                <ul>
                    <ol>1. Выберите период времени для анализа ошибок 1С.</ol>
                    <ol>2. Выберите категорию ошибок в соответствующем выпадающем меню слева.</ol>
                    <ol>3. Выберите подкатегорию ошибок в выпадающем меню справа.</ol>
                    <ol>4. После этого в нижней части дашборда будет отсортирована и показана расширенная информация по возникшим в заданный временной интервал ошибкам.
                        По отдельным пунктам информация может быть детализирована по клику на соответствующем пункте и выборе иконки { '<->' }
                        (например, так можно посмотреть подробную информацию по исходной записи техжурнала 1С).
                    </ol>
                </ul>
            </ReferenceComponent>
        </Box>
    );
};

export const AnalysisMagazineErrors = memo(({ hostGroup }: Props) => {
    const [dashboardData, setDashboardData] = useState<SavedObject<DashboardAttributes> | null>(null);
    const [isLoading, setIsLoading] = useState(false);
    const { show } = useFloatMessages();
    const myCompany = useAppSelector(state => state.MyCompanyState.myCompany);

    useEffect(() => {
        setDashboardData(null);
        (async () => {
            setIsLoading(true);
            const response = await FETCH_API.KIBANA.getKibanaDashboards();
            setIsLoading(false);
            if (response.data) {
                const pattern = /\((\d+)\)/;
                const finded = response.data.saved_objects.find(obj => {
                    const match = pattern.exec(obj.attributes.title);
                    const parts = hostGroup.name.split('&&');
                    return (
                        match &&
                        match[1] === parts[1] &&
                        obj.attributes.title.includes('errors')
                    );
                });
                setDashboardData(finded ?? null);
            } else {
                show(EMessageType.error, response.message ?? templateErrorText);
            }
        })();
    }, [hostGroup, show]);

    if (!dashboardData) {
        return (
            <Box>
                { isLoading && <LoadingBounce /> }
                <AnalysisMagazineErrorsReference isDefaultOpen={ true } companyId={ myCompany.indexNumber } />
                <TextOut>
                    Функционал не подключен. Чтобы подключить вывод ошибок 1С, отправьте заявку.
                </TextOut>
                <Box marginTop="6px">
                    <ReviewModal
                        title="Заявка на подключение функционала анализа ошибок 1С"
                        buttonText="Отправить заявку"
                        placeholder="Опишите ожидаемые данные"
                        description="Функционал анализа ошибок 1С позволяет подключить графический вывод информации по ошибкам, которую собирает технологический журнал 1С.
                        Для того, чтобы подключить вывод ошибок, опишите вашу задачу и наименование базы 1С, после чего отправьте заявку. "
                        subject={ `Заявка на подключение Анализа ошибок 1с для компании ${ myCompany.indexNumber }` }
                        header={ `Заявка на подключение Анализа ошибок 1с для компании ${ myCompany.indexNumber }` }
                    />
                </Box>
            </Box>
        );
    }

    return (
        <Box display="flex" flexDirection="column" gap="12px">
            { isLoading && <LoadingBounce /> }
            <Box>
                <Box>
                    <AnalysisMagazineErrorsReference companyId={ myCompany.indexNumber } />
                    {
                        dashboardData && <iframe
                            style={ { width: '100%', height: '115vh', border: 'none' } }
                            title="kibana"
                            src={ `https://elka.esit.info/app/dashboards?auth_provider_hint=anonymous1#/view/${ dashboardData.id
                            }?embed=true&_g=(refreshInterval%3A(pause%3A!t%2Cvalue%3A60000)%2Ctime%3A(from%3Anow-15m%2Cto%3Anow))&show-query-input=true&show-time-filter=true` }
                        />
                    }
                </Box>
            </Box>
        </Box>
    );
});