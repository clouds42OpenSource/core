/* eslint-disable no-underscore-dangle */
import { Box } from '@mui/material';
import { DesktopDatePicker, LocalizationProvider } from '@mui/x-date-pickers';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { DateUtility } from 'app/utils';
import { Dialog } from 'app/views/components/controls/Dialog';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { Chart, LineController, LineElement, LinearScale, PointElement, TimeScale } from 'chart.js';
import 'chartjs-adapter-date-fns';
import { useCallback, useEffect, useState } from 'react';
import ruLocale from 'date-fns/locale/ru';
import { getApdexProfileDataResponseDto, Score } from 'app/api/endpoints/apdex/response';
import dayjs from 'dayjs';
import { TextOut } from 'app/views/components/TextOut';
import { EMessageType, useFloatMessages } from 'app/hooks';
import { templateErrorText } from 'app/api';
import { ChartData } from 'app/views/modules/Ultra/types';
import { REDUX_API } from 'app/api/useReduxApi';
import { ApdexBaseChart } from './ApdexBaseChart';
import { parseDate } from '../../../utils';

Chart.register(LineController, LineElement, PointElement, LinearScale, TimeScale);

type Props = {
    hostGroupId: string;
    databaseId: string;
    profileId: string;
    keyOperationId: string;
    isModalOpened: boolean;
    setIsModalOpened: (value: boolean) => void;
    modalTitle: string;
};

const extractDetailScores = (data: {
    date: string;
    score: number;
    details: Score[];
}[], keyName: keyof Score): number[] => {
    return data.flatMap(item => item.details.map(detail => detail[keyName] as number));
};

export const ApdexProfileDetailsModal = ({ hostGroupId, databaseId, profileId, keyOperationId, isModalOpened, setIsModalOpened, modalTitle }: Props) => {
    const [isLoading, setIsLoading] = useState(false);
    const [startTime, setStartTime] = useState<Date | null>(DateUtility.getSomeDaysAgoDate(7));
    const [endTime, setEndTime] = useState<Date | null>(new Date());

    const [data, setData] = useState<ChartData>({ labels: [], datasets: [] });
    const { show } = useFloatMessages();

    const prepareChartData = (chartData: getApdexProfileDataResponseDto): ChartData => {
        const { scores } = chartData;

        const labels = scores.records.map(({ date }) => {
            return dayjs(parseDate(date)).toDate();
        });

        return {
            labels,
            datasets: [
                {
                    label: 'Оценка Apdex',
                    data: extractDetailScores(scores.records, 'score'),
                    borderColor: 'rgba(54, 162, 235, 1)',
                    fill: false
                },
            ]
        };
    };

    const handleGetData = useCallback(async () => {
        if (profileId && keyOperationId) {
            setIsLoading(true);
            const response = await REDUX_API.APDEX_REDUX.getApdexProfileData({ clientName: hostGroupId,
                query: {
                    databaseId,
                    profileId,
                    keyOperationId,
                    fromDate: startTime ? dayjs(startTime).format('DD.MM.YYYY') : undefined,
                    toDate: endTime ? dayjs(endTime).format('DD.MM.YYYY') : undefined
                } });

            if (response.data && response.success) {
                setData(prepareChartData(response.data));
            } else {
                show(EMessageType.error, response.message ?? templateErrorText);
            }
            setIsLoading(false);
        } else {
            setData({ labels: [], datasets: [] });
        }
    }, [startTime, endTime, profileId, databaseId, hostGroupId, keyOperationId, show]);

    useEffect(() => {
        void handleGetData();
    }, [handleGetData]);

    return (
        <Dialog
            isOpen={ isModalOpened }
            dialogWidth="xl"
            onCancelClick={ () => setIsModalOpened(false) }
            title={ modalTitle }
            titleFontSize={ 20 }
            titleTextAlign="left"
            isTitleSmall={ true }
        >
            { isLoading && <LoadingBounce /> }
            <Box display="flex" alignItems="center" gap="6px">
                <LocalizationProvider dateAdapter={ AdapterDateFns } adapterLocale={ ruLocale }>
                    <DesktopDatePicker
                        sx={ { color: 'red' } }
                        value={ startTime }
                        maxDate={ endTime || undefined }
                        onChange={ (date: Date | null) => setStartTime(date) }
                        format="dd/MM/yyyy"
                    />
                </LocalizationProvider>
                <TextOut>
                    -
                </TextOut>
                <LocalizationProvider dateAdapter={ AdapterDateFns } adapterLocale={ ruLocale }>
                    <DesktopDatePicker
                        sx={ { color: 'red' } }
                        value={ endTime }
                        minDate={ startTime || undefined }
                        maxDate={ new Date() }
                        onChange={ (date: Date | null) => setEndTime(date) }
                        format="dd/MM/yyyy"
                    />
                </LocalizationProvider>
            </Box>
            <ApdexBaseChart chartData={ data } />
        </Dialog>
    );
};