import React, { useState } from 'react';
import Popover from '@mui/material/Popover';
import { TextOut } from 'app/views/components/TextOut';
import { Box, IconButton, Tooltip } from '@mui/material';
import ContentCopyIcon from '@mui/icons-material/ContentCopy';
import { EMessageType, useFloatMessages } from 'app/hooks';
import WysiwygIcon from '@mui/icons-material/Wysiwyg';
import styles from '../../style.module.css';

type Props = {
    title: string;
};

export const LockPopover = ({ title }: Props) => {
    const [anchorEl, setAnchorEl] = useState<HTMLButtonElement | null>(null);
    const { show } = useFloatMessages();

    const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
        event.stopPropagation();
        setAnchorEl(event.currentTarget);
    };

    const handleClose = (event: React.MouseEvent<HTMLButtonElement>) => {
        event.stopPropagation();
        setAnchorEl(null);
    };

    const open = !!anchorEl;
    const id = open ? 'simple-popover' : undefined;

    const handleCopy = async (event: React.MouseEvent<HTMLButtonElement>) => {
        event.stopPropagation();

        try {
            await navigator.clipboard.writeText(title);
            show(EMessageType.success, 'Скопировано');
        } catch (err) {
            show(EMessageType.error, 'Ошибка');
        }
    };

    return (
        <div>
            <IconButton onClick={ handleClick }>
                <WysiwygIcon />
            </IconButton>
            <Popover
                id={ id }
                open={ open }
                anchorEl={ anchorEl }
                onClick={ e => e.stopPropagation() }
                onClose={ handleClose }
                anchorOrigin={ {
                    vertical: 'bottom',
                    horizontal: 'left',
                } }
                transformOrigin={ {
                    vertical: 'top',
                    horizontal: 'right',
                } }
            >
                <Box className={ styles.lockPopover }>
                    <Box borderBottom="1px solid #e7eaec">
                        <Tooltip title="Скопировать">
                            <IconButton onClick={ handleCopy } aria-label="prompt">
                                <ContentCopyIcon />
                            </IconButton>
                        </Tooltip>
                    </Box>
                    <TextOut inDiv={ true }>{ title }</TextOut>
                </Box>
            </Popover>
        </div>
    );
};