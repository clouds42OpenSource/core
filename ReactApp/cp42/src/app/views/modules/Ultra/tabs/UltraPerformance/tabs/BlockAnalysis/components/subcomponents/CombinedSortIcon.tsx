import { Box } from '@mui/material';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import ArrowDropUpIcon from '@mui/icons-material/ArrowDropUp';

const boxStyles = {
    display: 'flex',
    flexDirection: 'column' as const,
    alignItems: 'center',
    height: '25px',
    fontSize: '20px'
};

const arrowDownStyles = {
    marginTop: '-14px'
};

export const CombinedSortIcon = () => (
    <Box sx={ boxStyles }>
        <ArrowDropUpIcon fontSize="inherit" />
        <ArrowDropDownIcon fontSize="inherit" sx={ arrowDownStyles } />
    </Box>
);