import { getZabbixHostGroupsResponseDto } from 'app/api/endpoints/zabbix/response';
import { memo, useEffect, useState } from 'react';
import { Box } from '@mui/material';
import { useAppSelector } from 'app/hooks';
import { BlockWaitAnalysis } from './tabs/BlockWaitAnalysis';
import { BlockAnalysisErrors } from './tabs/BlockAnalysisErrors';

type Props = {
    hostGroup: getZabbixHostGroupsResponseDto;
};

export const BlockAnalysisView = memo(({ hostGroup }: Props) => {
    const [clientId, setClientId] = useState('');
    const myCompany = useAppSelector(state => state.MyCompanyState.myCompany);

    useEffect(() => {
        const parts = hostGroup.name.split('&&');
        setClientId(parts[1]);
    }, [hostGroup]);

    return (
        <Box display="flex" flexDirection="column" gap="6px">
            <BlockAnalysisErrors clientId={ clientId } myCompany={ myCompany } />
            <BlockWaitAnalysis clientId={ clientId } myCompany={ myCompany } />
        </Box>
    );
});