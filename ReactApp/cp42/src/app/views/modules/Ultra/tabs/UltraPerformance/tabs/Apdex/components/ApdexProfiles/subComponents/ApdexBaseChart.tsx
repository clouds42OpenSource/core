/* eslint-disable camelcase */
import 'chartjs-adapter-date-fns';
import { Line } from 'react-chartjs-2';

type Props = {
    chartData: {
        labels: Date[];
        datasets: {
            label: string;
            data: number[];
            fill: boolean;
            borderColor: string;
        }[];
    };
};

export const ApdexBaseChart = ({ chartData }: Props) => {
    return (
        <Line
            data={ chartData }
            options={ {
                aspectRatio: 3,
                scales: {
                    x: {
                        type: 'time',
                        position: 'bottom',
                        time: {
                            unit: 'day',
                            tooltipFormat: 'dd.MM.yyyy',
                            displayFormats: {
                                day: 'dd.MM.yyyy',
                            }
                        },
                        title: {
                            display: true,
                            text: 'Время'
                        }
                    },
                    y: {
                        title: {
                            display: true,
                            text: 'Значение'
                        },
                        ticks: {
                            callback: value => `${ Number(value) % 1 === 0 ? value : Number(value).toFixed(2) }`
                        }
                    }
                },
                plugins: {
                    tooltip: {
                        enabled: true,
                        callbacks: {
                            label(context) {
                                return `${ Number(context.raw).toFixed(2) }`;
                            }
                        }
                    },
                }
            } }
        />
    );
};