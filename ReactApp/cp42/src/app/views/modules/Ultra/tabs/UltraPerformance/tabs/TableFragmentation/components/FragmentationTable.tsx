import { Box } from '@mui/material';
import { SortingKind } from 'app/common/enums';
import { TextOut } from 'app/views/components/TextOut';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { useState } from 'react';
import { SelectDataCommonDataModel } from 'app/web/common/data-models';

type CleanFragmentationData = {
    page_count: number;
    index: string;
    database: string;
    fragmentation: number;
    table: string;
};

type DatabaseTableProps = {
    database: string;
    data: CleanFragmentationData[];
};

const sortData = (data: CleanFragmentationData[], fieldName: keyof CleanFragmentationData, sortKind: SortingKind): CleanFragmentationData[] => {
    const sortedData = [...data];
    sortedData.sort((a, b) => {
        const aValue = a[fieldName];
        const bValue = b[fieldName];

        if (aValue < bValue) {
            return sortKind === SortingKind.Asc ? -1 : 1;
        }

        if (aValue > bValue) {
            return sortKind === SortingKind.Asc ? 1 : -1;
        }

        return 0;
    });

    return sortedData;
};

export const FragmentationTable = ({ database, data }: DatabaseTableProps) => {
    const [sortedData, setSortedData] = useState(data);
    const [sortingData, setSortingData] = useState<{ fieldName: string, sortKind: SortingKind }>({
        fieldName: 'page_count',
        sortKind: SortingKind.Desc
    });

    const handleDataSelect = (value: SelectDataCommonDataModel<void>) => {
        const { sortingData: newSortingData } = value;

        if (newSortingData) {
            setSortingData(newSortingData);
            const newSortedData = sortData(data, newSortingData.fieldName as keyof CleanFragmentationData, newSortingData.sortKind);
            setSortedData(newSortedData);
        }
    };

    return (
        <Box>
            <TextOut fontWeight={ 500 } fontSize={ 16 }>{ database }</TextOut>
            <CommonTableWithFilter
                uniqueContextProviderStateId={ `tableFragmentation-${ database }` }
                isResponsiveTable={ true }
                isBorderStyled={ true }
                tableProps={ {
                    sorting: {
                        sortFieldName: sortingData.fieldName as keyof CleanFragmentationData,
                        sortKind: sortingData.sortKind
                    },
                    dataset: sortedData,
                    keyFieldName: 'page_count',
                    fieldsView: {
                        page_count: {
                            isSortable: true,
                            caption: 'Количество страниц',
                            fieldWidth: '25%',
                        },
                        table: {
                            caption: 'Имя таблицы',
                            fieldWidth: '25%',
                        },
                        index: {
                            caption: 'Имя индекса',
                            fieldWidth: '35%',
                        },
                        fragmentation: {
                            caption: 'Фрагментация',
                            isSortable: true,
                            fieldWidth: '15%',
                            format: (value: number) => value.toFixed(2)
                        }
                    }
                } }
                onDataSelect={ handleDataSelect }
            />
        </Box>
    );
};