import { MenuItem, Select } from '@mui/material';
import { PerformanceProfileData } from 'app/api/endpoints/apdex/response';
import { memo } from 'react';

type TProfileSelect = {
    profiles: PerformanceProfileData[];
    selectedProfile: string;
    setSelectedProfile: (val: string) => void;
};

export const ProfileSelect = memo(({ profiles, selectedProfile, setSelectedProfile }: TProfileSelect) => (
    <Select
        value={ selectedProfile }
        onChange={ e => setSelectedProfile(e.target.value) }
        displayEmpty={ true }
    >
        <MenuItem value="">Выберите профиль</MenuItem>
        { profiles.map(item => (
            <MenuItem key={ item.id } value={ item.id }>
                { item.name }
            </MenuItem>
        )) }
    </Select>
));