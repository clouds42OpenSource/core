/* eslint-disable no-underscore-dangle */
import { COLORS } from 'app/utils';
import { TextOut } from 'app/views/components/TextOut';
import { OperationStatusEnum } from 'app/views/modules/Ultra/enums/OperationStatusEnum';
import { ZabbixItemsForOperations } from 'app/views/modules/Ultra/types/ZabbixItemsForOpetaions';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import dayjs from 'dayjs';
import 'dayjs/locale/ru';
import { ReactElement, memo } from 'react';

dayjs.locale('ru');

type Props = {
    operationDBMSValues: ZabbixItemsForOperations;
};

type operationDataType = {
    lastruntime: string | null;
    nextruntime: string | null;
    runstatus: string | null;
    head: string | null;
};

type GroupedData = {
    [jobName: string]: operationDataType;
};

const getStatusDescription = (statusValue: string): ReactElement => {
    switch (Number(statusValue)) {
        case OperationStatusEnum.SUCCESS:
            return (
                <TextOut style={ { color: 'rgba(30, 212, 19, 0.99)' } }>
                    Успешно
                </TextOut>
            );
        case OperationStatusEnum.IN_PROGRESS:
            return (
                <TextOut>
                    Выполняется
                </TextOut>
            );
        default:
            return (
                <TextOut style={ { color: COLORS.error } }>
                    Ошибка
                </TextOut>
            );
    }
};

const getRegOpsJobName = (key: string): string | null => {
    const match = key.match(/\["RegOps\.(.*?)"\]/);
    return match ? match[1] : null;
};

const getCorrectedData = (data: ZabbixItemsForOperations): GroupedData => {
    return data.reduce<GroupedData>((acc, item) => {
        const jobName = getRegOpsJobName(item.key_);

        if (!jobName) {
            return acc;
        }

        if (!acc[jobName]) {
            acc[jobName] = {
                lastruntime: null,
                nextruntime: null,
                runstatus: null,
                head: jobName
            };
        }

        if (item.key_.includes('lastrundatetime')) {
            acc[jobName].lastruntime = item.lastvalue;
        } else if (item.key_.includes('nextrundatetime')) {
            console.log(item.lastvalue);
            acc[jobName].nextruntime = item.lastvalue;
        } else if (item.key_.includes('runstatus')) {
            acc[jobName].runstatus = item.lastvalue;
        }

        return acc;
    }, {});
};

const parseDate = (value: string | null): string => {
    if (!value) return '-';

    if (/^\d+$/.test(value)) {
        return dayjs.unix(Number(value)).format('D MMMM YYYY г., HH:mm');
    }

    const parsedDate = dayjs(value, ['YYYY-MM-DD HH:mm:ss.SSS', 'YYYY-MM-DD HH:mm:ss'], true);

    return parsedDate.isValid() ? parsedDate.format('D MMMM YYYY г., HH:mm') : '-';
};

const OperationDMBSView = ({ operationDBMSValues }: Props) => {
    const groupedData = getCorrectedData(operationDBMSValues);

    return (
        <CommonTableWithFilter
            uniqueContextProviderStateId="OperationDBMS"
            isResponsiveTable={ true }
            isBorderStyled={ true }
            tableProps={ {
                dataset: Object.values(groupedData) as operationDataType[],
                keyFieldName: 'head',
                fieldsView: {
                    head: {
                        caption: 'Наименование',
                        fieldWidth: '50%',
                    },
                    runstatus: {
                        caption: 'Статус последней операции',
                        fieldWidth: 'auto',
                        format: getStatusDescription
                    },
                    lastruntime: {
                        caption: 'Дата последней операции',
                        fieldWidth: 'auto',
                        format: parseDate
                    },
                    nextruntime: {
                        caption: 'Дата следующей операции',
                        fieldWidth: 'auto',
                        format: parseDate
                    },
                }
            } }
        />
    );
};

export const OperationDMBS = memo(OperationDMBSView);