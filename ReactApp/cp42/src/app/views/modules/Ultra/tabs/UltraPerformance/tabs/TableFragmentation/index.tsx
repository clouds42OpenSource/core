import { Box } from '@mui/material';
import { getZabbixItemsResponseDto } from 'app/api/endpoints/zabbix/response';
import { memo, useEffect, useState } from 'react';
import { TextOut } from 'app/views/components/TextOut';
import { FragmentationTable } from './components/FragmentationTable';

type RawFragmentationData = {
    '{#PAGE_COUNT}': number;
    '{#INDEX}': string;
    '{#DATABASE}': string;
    '{#FRAGMENTATION}': number;
    '{#TABLE}': string;
};

type RawDataContainer = {
    data: RawFragmentationData[];
};

type CleanFragmentationData = {
    page_count: number;
    index: string;
    database: string;
    fragmentation: number;
    table: string;
};

type Props = {
    tableFragmentationValues: getZabbixItemsResponseDto | null;
};

type GroupedByDatabase = Record<string, CleanFragmentationData[]>;

const cleanData = ({ data }: RawDataContainer): CleanFragmentationData[] => {
    return data.map(item => ({
        page_count: item['{#PAGE_COUNT}'],
        index: item['{#INDEX}'],
        database: item['{#DATABASE}'],
        fragmentation: item['{#FRAGMENTATION}'],
        table: item['{#TABLE}']
    }));
};

const getGroupsByDatabase = (data: CleanFragmentationData[]): GroupedByDatabase => {
    return data.reduce((acc: GroupedByDatabase, obj: CleanFragmentationData) => {
        const key = obj.database;
        if (!acc[key]) {
            acc[key] = [];
        }
        acc[key].push(obj);

        return acc;
    }, {});
};

export const TableFragmentation = memo(({ tableFragmentationValues }: Props) => {
    const [parsedTableFragmentationValues, setParsedFragmentationValues] = useState<GroupedByDatabase | null>(null);

    useEffect(() => {
        if (tableFragmentationValues) {
            try {
                const parsedData: RawDataContainer = JSON.parse(tableFragmentationValues.lastvalue);
                const cleanedData = cleanData(parsedData);
                const groupedData = getGroupsByDatabase(cleanedData);
                setParsedFragmentationValues(groupedData);
            } catch {
                setParsedFragmentationValues(null);
            }
        }
    }, [tableFragmentationValues]);

    if (!tableFragmentationValues) {
        return (
            <TextOut inDiv={ true }>
                Функционал анализа фрагментации таблиц не подключен
            </TextOut>
        );
    }

    return (
        <Box display="flex" flexDirection="column" gap="16px">
            { parsedTableFragmentationValues && Object.keys(parsedTableFragmentationValues).length ? Object.keys(parsedTableFragmentationValues).map(database => (
                <FragmentationTable
                    key={ database }
                    database={ database }
                    data={ parsedTableFragmentationValues[database] }
                />
            )) : (
                <TextOut>
                    Нет данных
                </TextOut>
            ) }
        </Box>
    );
});