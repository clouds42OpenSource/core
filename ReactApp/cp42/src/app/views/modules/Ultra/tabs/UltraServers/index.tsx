import { getZabbixHostGroupsResponseDto, getZabbixHostsResponseDto } from 'app/api/endpoints/zabbix/response';
import { FETCH_API } from 'app/api/useFetchApi';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { useEffect, useState } from 'react';
import { useAppSelector } from 'app/hooks';
import { TabsControl } from 'app/views/components/controls/TabsControl';
import { ServersResources } from './tabs/ServersResources';
import { ServersDashboards } from './tabs/ServersDashboards';

type Props = {
    hostGroup: getZabbixHostGroupsResponseDto;
}

export const UltraServers = ({ hostGroup }: Props) => {
    const [isLoading, setIsLoading] = useState(false);
    const [hosts, setHosts] = useState<getZabbixHostsResponseDto[]>([]);
    const myCompany = useAppSelector(state => state.MyCompanyState.myCompany);

    const handleGetHosts = async (groupId: string) => {
        setIsLoading(true);
        const response = await FETCH_API.ZABBIX.getHosts(groupId);
        setIsLoading(false);
        if (response.result) {
            setHosts(response.result);
        }
    };

    const handleSetIsLoading = (value: boolean) => {
        setIsLoading(value);
    };

    useEffect(() => {
        void handleGetHosts(hostGroup.groupid);
    }, [hostGroup]);

    return (
        <div>
            { isLoading && <LoadingBounce /> }
            <TabsControl
                headers={ [
                    {
                        title: 'Мониторинг',
                        isVisible: true
                    },
                    {
                        title: 'Список серверов',
                        isVisible: true,
                    },
                ] }
            >
                <ServersDashboards myCompany={ myCompany } hostGroup={ hostGroup } />
                <ServersResources hosts={ hosts } handleSetIsLoading={ handleSetIsLoading } />
            </TabsControl>
        </div>
    );
};