import React from 'react';
import { Box, Accordion, AccordionSummary, AccordionDetails, Radio } from '@mui/material';
import { Lock, LockRecord } from 'app/api/endpoints/blockAnalysis/response/getBlockAnalysisLock';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import dayjs from 'dayjs';
import { TextOut } from 'app/views/components/TextOut';
import { sortData } from '../utils/common';
import { SortableColumnHeader } from './subcomponents/SortableColumnHeader';
import styles from '../style.module.css';
import { useSort } from '../hooks/useSort';
import { LockedObjectView } from './subcomponents/LockedObjectView';

type LockListProps = {
    locks: Lock[];
    selectedLock: string;
    handleRadioChange: (lockId: string) => void;
    expanded: string | false;
    handleAccordionChange: (panel: string) => (event: React.SyntheticEvent, isExpanded: boolean) => void;
};

export const BlockAnalysisTable = ({ locks, handleAccordionChange, expanded, handleRadioChange, selectedLock }: LockListProps) => {
    const { sortConfig: sortConfigLocks, handleSort: handleLocksSort } = useSort<Lock>('title');
    const { sortConfig: sortConfigRecords, handleSort: handleRecordsSort } = useSort<LockRecord>('datetime');

    const getSortedLocks = (): Lock[] => {
        return sortData(locks, sortConfigLocks.fieldName, sortConfigLocks.sortKind);
    };

    const getSortedRecords = (lock: Lock): LockRecord[] => {
        return sortData(lock.records, sortConfigRecords.fieldName, sortConfigRecords.sortKind);
    };

    return (
        <Box className={ styles.causerTable }>
            <Box className={ styles.contextTableHeads }>
                <SortableColumnHeader sortConfig={ sortConfigLocks } fieldName="title" handleSort={ handleLocksSort } label="Контекст виновника" />
                <SortableColumnHeader sortConfig={ sortConfigLocks } fieldName="victimsCount" handleSort={ handleLocksSort } label="Кол-во жертв" />
                <SortableColumnHeader sortConfig={ sortConfigLocks } fieldName="waitTime" handleSort={ handleLocksSort } label="Общее время блокировки, сек" />
            </Box>
            <Box className={ styles.lockDetailsTableContentWrapper }>
                { locks.length ? getSortedLocks().map(lock => (
                    <Box key={ lock.title }>
                        <Accordion expanded={ expanded === lock.title } onChange={ handleAccordionChange(lock.title) }>
                            <AccordionSummary expandIcon={ <ExpandMoreIcon /> } aria-controls="panel-content" id="panel-header">
                                <Box className={ styles.contextTableContent }>
                                    <TextOut>{ lock.title }</TextOut>
                                    <TextOut>{ lock.victimsCount }</TextOut>
                                    <TextOut>{ lock.waitTime }</TextOut>
                                </Box>
                            </AccordionSummary>
                            <AccordionDetails>
                                <Box>
                                    <Box className={ styles.lockDetailsTableHead }>
                                        <SortableColumnHeader sortConfig={ sortConfigRecords } fieldName="datetime" handleSort={ handleRecordsSort } label="Время возникновения" />
                                        <TextOut>Блокирующий пользователь</TextOut>
                                        <SortableColumnHeader sortConfig={ sortConfigRecords } fieldName="victimsCount" handleSort={ handleRecordsSort } label="Кол-во жертв" />
                                        <SortableColumnHeader sortConfig={ sortConfigRecords } fieldName="waitTime" handleSort={ handleRecordsSort } label="Общее время блокировки, сек" />
                                        <TextOut>Детали события</TextOut>
                                    </Box>
                                    { getSortedRecords(lock).map(record => (
                                        <Box key={ record.id } component="label" className={ styles.lockDetailsTableContent } sx={ { background: selectedLock === record.id ? '#e7eaec' : 'inherit' } }>
                                            <Radio
                                                checked={ selectedLock === record.id }
                                                onChange={ () => handleRadioChange(record.id) }
                                                value={ record.id }
                                                sx={ { display: 'none' } }
                                            />
                                            <TextOut>{ dayjs(record.datetime).format('DD.MM.YYYY HH:mm:ss') }</TextOut>
                                            <TextOut>{ record.user }</TextOut>
                                            <TextOut>{ record.victimsCount }</TextOut>
                                            <TextOut>{ record.waitTime }</TextOut>
                                            <LockedObjectView context={ record.context } userName={ record.user } />
                                        </Box>
                                    )) }
                                </Box>
                            </AccordionDetails>
                        </Accordion>
                    </Box>
                )) : <TextOut>Нет данных</TextOut> }
            </Box>
        </Box>
    );
};