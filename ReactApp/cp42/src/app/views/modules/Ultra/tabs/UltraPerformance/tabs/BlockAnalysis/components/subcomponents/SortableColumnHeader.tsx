import React from 'react';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import ArrowDropUpIcon from '@mui/icons-material/ArrowDropUp';
import { TextOut } from 'app/views/components/TextOut';
import { CombinedSortIcon } from './CombinedSortIcon';

type SortIconProps<T> = {
    sortConfig: { fieldName: keyof T; sortKind: number };
    fieldName: keyof T;
    handleSort: (fieldName: keyof T) => void;
    label: string;
};

export const SortableColumnHeader = <T extends object>({
    sortConfig,
    fieldName,
    handleSort,
    label,
}: SortIconProps<T>) => {
    const renderSortIcon = () => {
        if (sortConfig.fieldName !== fieldName) {
            return <CombinedSortIcon />;
        }

        return sortConfig.sortKind === 0 ? <ArrowDropUpIcon /> : <ArrowDropDownIcon />;
    };

    return (
        <TextOut onClick={ () => handleSort(fieldName) }>
            { label } { renderSortIcon() }
        </TextOut>
    );
};