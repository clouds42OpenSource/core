/* eslint-disable no-underscore-dangle */
import { HelpOutline as HelpOutlineIcon } from '@mui/icons-material';
import { Box, Button, IconButton, Tooltip } from '@mui/material';
import { getZabbixItemsResponseDto } from 'app/api/endpoints/zabbix/response';
import { TextOut } from 'app/views/components/TextOut';
import { StatusIndexConst } from 'app/views/modules/Ultra/constants/StatusIndexConst';
import { FieldViewType } from 'app/views/modules/Ultra/types';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';

type Props = {
    statusIndexValues: getZabbixItemsResponseDto[];
    updateData: () => void;
    useNewKeys: boolean;
};

export const StatusIndex = ({ statusIndexValues, updateData, useNewKeys = false }: Props) => {
    return (
        <div>
            <Box textAlign="end">
                <Button variant="contained" sx={ { boxShadow: 'none', marginBottom: '12px' } } onClick={ updateData }>
                    Обновить данные
                </Button>
            </Box>
            { StatusIndexConst.map(correspondingConst => {
                const data = statusIndexValues.find(constant => constant.key_ === correspondingConst[useNewKeys ? 'newKey' : 'oldKey']);
                if (data) {
                    try {
                        const parsedData = JSON.parse(data.lastvalue);
                        const fieldsView = Object.entries(correspondingConst.fields).reduce((acc: FieldViewType, [key, caption]) => {
                            acc[key] = {
                                caption,
                                fieldWidth: 'auto',
                                format: value => value
                            };
                            return acc;
                        }, {});
                        return (
                            <Box key={ data.itemid } marginTop="20px" padding="0px 12px 4px 12px">
                                <Box display="flex" justifyContent="space-between" alignItems="center">
                                    <TextOut inDiv={ true } style={ { display: 'flex', alignItems: 'center' } } fontSize={ 20 } fontWeight={ 500 }>
                                        { correspondingConst.head }
                                        <Tooltip title={ correspondingConst.description }>
                                            <IconButton aria-label="prompt">
                                                <HelpOutlineIcon />
                                            </IconButton>
                                        </Tooltip>
                                    </TextOut>
                                </Box>
                                <CommonTableWithFilter
                                    uniqueContextProviderStateId={ `table-${ data.itemid }` }
                                    isResponsiveTable={ true }
                                    isBorderStyled={ true }
                                    tableProps={ {
                                        dataset: parsedData,
                                        keyFieldName: Object.keys(correspondingConst.fields)[0],
                                        fieldsView
                                    } }
                                />
                            </Box>
                        );
                    } catch {
                        return (
                            <TextOut
                                inDiv={ true }
                                textAlign="center"
                                fontSize={ 13 }
                            > нет записей
                            </TextOut>
                        );
                    }
                }

                return (
                    <TextOut
                        key={ correspondingConst[useNewKeys ? 'newKey' : 'oldKey'] }
                        inDiv={ true }
                        textAlign="center"
                        fontSize={ 13 }
                    > нет записей
                    </TextOut>
                );
            }) }
        </div>
    );
};