import React, { useEffect, useMemo, useState } from 'react';
import { templateErrorText } from 'app/api';
import { getZabbixHostGroupsResponseDto } from 'app/api/endpoints/zabbix/response';
import { EMessageType, useFloatMessages } from 'app/hooks';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { TextOut } from 'app/views/components/TextOut';
import { useGetGrafanaDashboards, useGetGrafanaToken } from 'app/api/endpoints';
import { MyCompanyItemDataModel } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/receiveMyCompany';
import { ReferenceComponent } from '../../UltraPerformance/tabs/components/ReferenceComponent';
import { SuggestImprovementsModal } from '../../../components/SuggestImprovementModal';

type Props = {
    hostGroup: getZabbixHostGroupsResponseDto;
    myCompany: MyCompanyItemDataModel;
};

export const ServersDashboards = ({ hostGroup, myCompany }: Props) => {
    const { show } = useFloatMessages();

    const [accessToken, setAccessToken] = useState<string | null>(null);

    const splittedId = useMemo(() => {
        const parts = hostGroup?.name?.split('&&');
        return parts?.[1] ?? '';
    }, [hostGroup]);

    const {
        data: dashboardsData,
        error: dashboardsError,
        isLoading: isDashboardsLoading
    } = useGetGrafanaDashboards(splittedId);

    const firstDashboardUid = dashboardsData?.rawData?.[0]?.uid;

    const {
        data: tokenData,
        error: tokenError,
        isLoading: isTokenLoading
    } = useGetGrafanaToken(firstDashboardUid);

    useEffect(() => {
        setAccessToken(null);
    }, [splittedId]);

    useEffect(() => {
        if (tokenData?.rawData?.accessToken) {
            setAccessToken(tokenData.rawData.accessToken);
        }
    }, [tokenData]);

    useEffect(() => {
        if (dashboardsError) {
            show(EMessageType.error, dashboardsError || templateErrorText);
        }
    }, [dashboardsError, show]);

    useEffect(() => {
        if (tokenError) {
            show(EMessageType.error, tokenError || templateErrorText);
        }
    }, [tokenError, show]);

    const isLoading = isDashboardsLoading || isTokenLoading;

    return (
        <>
            { isLoading && <LoadingBounce /> }

            <ReferenceComponent
                extraButtons={
                    <SuggestImprovementsModal
                        functionalityName="Мониторинг"
                        companyId={ myCompany.indexNumber }
                    />
                }
            >
                <b style={ { fontSize: '15px' } }>Общая информация</b>
                <p>
                    Раздел <b>Графики мониторинга</b> представлен наборами панелей графиков по сегментам:
                </p>
                <ul>
                    <li>
                        <b>Обзор</b> – обзорная информация по инфраструктуре, содержащая текущее использование ЦП, ОЗУ,
                        корневого раздела дисков и аптайм серверов.
                    </li>
                    <li>
                        <b>ЦП и ОЗУ</b> – расширенная информация по потреблению процессорного времени и оперативной памяти.
                    </li>
                    <li>
                        <b>Диски и файловая система</b> – расширенная информация по нагрузке на дисковую подсистему.
                    </li>
                    <li>
                        <b>СУБД</b> – информация по метрикам используемого сервера СУБД.
                    </li>
                </ul>
                <p>
                    Раздел <b>Выделенные ресурсы и перечень серверов</b> отображает подключенные к системе серверы,
                    по которым отображаются графики мониторинга.
                </p>

                <b style={ { fontSize: '15px' } }>Описание функционала</b>
                <p>
                    В разделе <b>Графики мониторинга</b> можно управлять диапазоном времени вывода данных,
                    а также включить автообновление.
                </p>

                <b style={ { fontSize: '14px' } }>Рекомендации</b>
                <p>
                    <b>Утилизация ЦП.</b> Как правило, если потребление ЦП свыше 90% в длительный период времени и не снижается,
                    это может свидетельствовать либо о нехватке ресурсов, либо о проблемах со структурой СУБД,
                    если это сервер СУБД и увеличение ресурсов не дает снижения потребления ЦП.
                </p>
                <p>
                    <b>Утилизация ОЗУ.</b> Высокая утилизация ОЗУ для сервера СУБД не является критичной,
                    здесь нужно обратить внимание на настройки сервера СУБД и выделенный лимит ОЗУ, а также использование кэша.
                </p>
            </ReferenceComponent>

            { firstDashboardUid && accessToken ? (
                <iframe
                    style={ { border: 'none' } }
                    src={ `https://grafana.esit.info/public-dashboards/${ accessToken }` }
                    width="100%"
                    height="2000px"
                    title="Grafana Dashboard"
                />
            ) : (
                <TextOut>Функционал не подключен.</TextOut>
            ) }
        </>
    );
};