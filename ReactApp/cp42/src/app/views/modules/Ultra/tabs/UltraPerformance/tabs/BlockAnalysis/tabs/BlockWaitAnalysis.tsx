/* eslint-disable react-hooks/exhaustive-deps */
import { getBlockAnalysisDatabasesResponseDto } from 'app/api/endpoints/blockAnalysis/response/getBlockAnalysisDatabasesResponseDto';
import { FETCH_API } from 'app/api/useFetchApi';
import React, { memo, useCallback, useEffect, useState } from 'react';
import { getBlockAnalysisLockResponseDto, Victims } from 'app/api/endpoints/blockAnalysis/response/getBlockAnalysisLock';
import { Box, Button, Collapse, IconButton, TextField, Tooltip } from '@mui/material';
import { DesktopDateTimePicker, LocalizationProvider } from '@mui/x-date-pickers';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { TextOut } from 'app/views/components/TextOut';
import ruLocale from 'date-fns/locale/ru';
import { DateUtility } from 'app/utils';
import dayjs from 'dayjs';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { EMessageType, useDebounce, useFloatMessages } from 'app/hooks';
import { templateErrorText } from 'app/api';
import { ReviewModal } from 'app/views/modules/Ultra/components/ReviewModal';
import { HelpOutline as HelpOutlineIcon, KeyboardArrowDown as KeyboardArrowDownIcon, KeyboardArrowRight as KeyBoardArrowRightIcon } from '@mui/icons-material';
import { TextButton } from 'app/views/components/controls/Button';
import { MyCompanyItemDataModel } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/receiveMyCompany';
import { SuggestImprovementsModal } from 'app/views/modules/Ultra/components/SuggestImprovementModal';
import PriorityHighIcon from '@mui/icons-material/PriorityHigh';
import { BlockAnalysisTable } from '../components/BlockAnalysisTable';
import { VictimsTable } from '../components/VictimsTable';
import { DatabaseSelect } from '../../components/DatabaseSelect';
import styles from '../style.module.css';

import { ReferenceComponent } from '../../components/ReferenceComponent';

type Props = {
    myCompany: MyCompanyItemDataModel;
    clientId: string;

};

export const BlockWaitAnalysis = memo(({ clientId, myCompany }: Props) => {
    const [blockAnalysisDatabases, setBlockAnalysisDatabases] = useState<getBlockAnalysisDatabasesResponseDto>([]);
    const [selectedDatabase, setSelectedDatabase] = useState('');
    const [blockAnalysisLocks, setBlockAnalysisLocks] = useState<getBlockAnalysisLockResponseDto | null>(null);
    const [selectedLock, setSelectedLock] = useState('');
    const [startTime, setStartTime] = useState<Date | null>(DateUtility.getSomeDaysAgoDate(30));
    const [endTime, setEndTime] = useState<Date | null>(new Date());
    const [lockList, setLockList] = useState<Victims[]>([]);
    const [isLoading, setIsLoading] = useState(false);
    const [expanded, setExpanded] = useState<string | false>(false);
    const [minLockDuration, setMinLockDuration] = useState(10);
    const [isOpened, setIsOpened] = useState(false);
    const [customMinLockDuration, setCustomMinLockDuration] = useState('');
    const { show } = useFloatMessages();
    const debouncedCustomMinLockDuration = useDebounce(customMinLockDuration, 500);

    const handleAccordionChange =
    (panel: string) => (event: React.SyntheticEvent, isExpanded: boolean) => {
        setExpanded(isExpanded ? panel : false);
    };

    const handleRadioChange = (context: string) => {
        setSelectedLock(context);
    };

    const handleGetBlocks = useCallback(async () => {
        if (selectedDatabase) {
            setSelectedLock('');
            setIsLoading(true);
            const response = await FETCH_API.BLOCKANALYSIS.getBlockAnalysisLocks({
                clientId,
                query: {
                    fromDate: startTime ? dayjs(startTime).format('DD.MM.YYYY') : undefined,
                    toDate: endTime ? dayjs(endTime).format('DD.MM.YYYY') : undefined,
                    databaseId: selectedDatabase,
                    minLockDuration
                }
            });
            setIsLoading(false);

            if (response.success && response.data) {
                setBlockAnalysisLocks(response.data);
            } else {
                show(EMessageType.error, response.message ?? templateErrorText);
            }
        } else {
            setBlockAnalysisLocks(null);
            setLockList([]);
        }
    }, [selectedDatabase, clientId, startTime, endTime, minLockDuration, show]);

    useEffect(() => {
        const value = parseInt(debouncedCustomMinLockDuration, 10);
        if (value >= 1 && value <= 99999) {
            setMinLockDuration(value);
        }
    }, [debouncedCustomMinLockDuration]);

    useEffect(() => {
        handleGetBlocks();
    }, [selectedDatabase, minLockDuration]);

    useEffect(() => {
        if (clientId) {
            (async () => {
                setIsLoading(true);
                setSelectedDatabase('');
                setSelectedLock('');
                setBlockAnalysisLocks(null);
                const response = await FETCH_API.BLOCKANALYSIS.getBlockAnalysisDatabasesList(clientId);
                setIsLoading(false);

                if (response.success && response.data) {
                    setBlockAnalysisDatabases(response.data);
                } else {
                    show(EMessageType.error, response.message ?? templateErrorText);
                }
            })();
        }
    }, [clientId, show]);

    useEffect(() => {
        (async () => {
            if (selectedLock) {
                setIsLoading(true);
                const response = await FETCH_API.BLOCKANALYSIS.getBlockAnalysisLocks({
                    clientId,
                    query: {
                        fromDate: startTime ? dayjs(startTime).format('DD.MM.YYYY') : undefined,
                        toDate: endTime ? dayjs(endTime).format('DD.MM.YYYY') : undefined,
                        databaseId: selectedDatabase,
                        lockId: selectedLock,
                        includeVictims: true
                    }
                });
                setIsLoading(false);

                if (response.success && response.data) {
                    setLockList(response.data.Locks[0].records[0].victims);
                } else {
                    show(EMessageType.error, response.message ?? templateErrorText);
                }
            } else {
                setLockList([]);
            }
        })();
    }, [clientId, endTime, selectedDatabase, selectedLock, startTime, show]);

    return (
        <Box display="flex" flexDirection="column" gap="8px" alignItems="flex-start">
            { isLoading && <LoadingBounce /> }
            <Box className={ styles.collapseWrapper }>
                <TextButton onClick={ () => setIsOpened(!isOpened) }>
                    { isOpened ? <KeyboardArrowDownIcon /> : <KeyBoardArrowRightIcon /> }
                    Анализ ожиданий на блокировках 1С
                </TextButton>
                <Collapse in={ isOpened }>
                    <Box padding="6px 14px">
                        <ReferenceComponent
                            mb="0"
                            extraButtons={
                                <SuggestImprovementsModal
                                    functionalityName="Анализ блокировок 1С"
                                    companyId={ myCompany.indexNumber }
                                />
                            }
                        >
                            <b>
                                Общая информация
                            </b>
                            <span>
                                Блокировка в системе 1С:Предприятие представляет собой механизм ограничения доступа к данным или объектам, чтобы предотвратить одновременное изменение этих данных несколькими пользователями.
                                Блокировка используется для обеспечения целостности данных и предотвращения конфликтов, когда несколько пользователей пытаются редактировать одни и те же данные одновременно.
                            </span>
                            <span>
                                При конфликте блокировок в системе 1С происходит ситуация, когда несколько пользователей или процессов одновременно пытаются получить доступ к одному и тому же объекту данных или ресурсу,
                                и один из них уже заблокировал этот объект.
                                Пользователь, который пытается работать с заблокированным объектом получит сообщение об ошибке.
                                Поиск пользователя, который заблокировал ресурс (виновника) можно выполнить с помощью логов технологического журнала.
                            </span>
                            <b>Описание функционала</b>
                            <span>
                                С помощью сервиса анализа блокировок 1С можно проанализировать ожидания на блокировках конкретной базы 1С.
                                Ожидания на блокировках 1С:Предприятие являются только одной из возможных причин колебаний времени выполнения запросов.
                            </span>
                            <span>
                                Функционал анализа ожиданий на блокировках при выборе базы 1С отображается в виде двух таблиц:
                            </span>
                            <ul>
                                <li>
                                    <b>Таблица виновников блокировки (слева).</b>
                                    В этой таблице отображаются виновники, которые группируются по контексту.
                                    Контекст может быть развернут для детализации, которая содержит время возникновения, пользователя (виновника), количество жертв, общее время блокировки, контекст виновника.
                                </li>
                                <li>
                                    <b>Таблица жертв блокировки (справа).</b>
                                    Заполняется при выборе Виновника в левом секторе.
                                    Эта таблица содержит всех жертв выбранного виновника из левой таблицы со следующими данными: дата блокировки, время ожидания, объект метаданных, заблокированный пользователь (жертва), контекст жертвы.
                                </li>
                            </ul>
                            <b>Сценарий использования:</b>
                            <ul>
                                <ol>1. Для начала работы, выберите базу 1С в выпадающем списке.</ol>
                                <ol>2. Далее выберите значение Фильтра времени ожидания или впишите свое (по умолчанию, установлено значение 10 секунд – отображаются блокировки с временем ожидания более 10 секунд).</ol>
                                <ol>3. Выберите временной интервал для анализа ожиданий. </ol>
                                <ol>4. Работа с Таблицей виновников блокировки:</ol>
                                <ol>4.1. После выбора интервала времени, Таблица виновников блокировки будет заполнена найденными значениями и доступна сортировка по контексту виновника, количеству жертв и общему времени блокировки.</ol>
                                <ol>4.2. Выберите нужную строку контекста виновника в Таблице виновников блокировки и разверните его по клику.</ol>
                                <ol>4.3. После раскрытия строки контекста виновника, будут отображены пользователи-виновники, которые инициировали блокировку в 1С с указанием времени возникновения,
                                    название блокирующего пользователя, количества заблокированных жертв, общее время блокировки и детализация контекста виновника.
                                </ol>
                                <ol>4.4. Выберите пользователя-виновника, по которому требуется получить более подробную информацию по жертвам блокировки.</ol>
                                <ol>5. Работа с Таблицей жертв блокировки:</ol>
                                <ol>5.1. После выбора виновника в таблице слева, в Таблице жертв блокировки появится информация с указанием заблокированных пользователей, включая дату блокировки,
                                    время ожидания, объект метаданных и название заблокированного пользователя.
                                </ol>
                                <ol>5.2. При клике на иконку в колонке Объект метаданных, можно получить детализацию по объектам блокировки, включая контекст. </ol>
                            </ul>
                            <TextOut style={ { display: 'flex', gap: '3px' } }>
                                <PriorityHighIcon style={ { color: 'red', fontSize: '14px' } } /> <span><b>Важно:</b> Автоматические блокировки не поддерживаются.</span>
                            </TextOut>
                        </ReferenceComponent>
                    </Box>
                    <Box padding="6px 14px">
                        <Box className={ styles.wrapper }>
                            { blockAnalysisDatabases.length ? (
                                <DatabaseSelect databaseList={ blockAnalysisDatabases } setSelectedDatabase={ setSelectedDatabase } selectedDatabase={ selectedDatabase } />
                            ) : (
                                <TextOut inDiv={ true }>
                                    <Box marginBottom="8px">
                                        Функционал анализа блокировок 1С не подключен, отправьте заявку на подключение.
                                    </Box>
                                    <ReviewModal
                                        title="Заявка на подключение анализа блокировок 1С"
                                        placeholder="..."
                                        buttonText="Отправить заявку"
                                        description="Заполните заявку на подключение функционала анализа блокировок вашей базы 1С."
                                        subject={ `Заявка на подключение анализа блокировок 1С для компании ${ myCompany.indexNumber }` }
                                        header={ `Заявка на подключение анализа блокировок 1С для компании ${ myCompany.indexNumber }` }
                                    />
                                </TextOut>
                            ) }
                            { selectedDatabase && (
                                <TextOut className={ `${ styles.wrapper } ${ styles.filterWrapper }` }>
                                    <TextOut>
                                        Фильтр времени ожидания:
                                        <Tooltip
                                            title="Фильтр времени ожидания позволяет отсечь блокировки, время ожидания которых менее заданного фильтром значения.
                                                    Например, если задать значение 1 секунда – будут показаны только те блокировки, где время ожидания более 1 секунды. По умолчанию, фильтр установлен на 10 секунд."
                                        >
                                            <IconButton aria-label="prompt">
                                                <HelpOutlineIcon />
                                            </IconButton>
                                        </Tooltip>
                                    </TextOut>
                                    { [1, 3, 5, 7, 10].map(value => (
                                        <button
                                            type="button"
                                            key={ value }
                                            className={ minLockDuration === value ? styles.active : '' }
                                            onClick={ () => {
                                                setMinLockDuration(value);
                                                setCustomMinLockDuration('');
                                            } }
                                        >
                                            { value } сек
                                        </button>
                                    )) }
                                    <TextField
                                        placeholder="99 сек"
                                        value={ customMinLockDuration }
                                        onChange={ e => {
                                            const { value } = e.target;
                                            if (/^\d{0,5}$/.test(value)) {
                                                setCustomMinLockDuration(value);
                                            }
                                        } }
                                        inputProps={ { inputMode: 'numeric', pattern: '[0-9]*', maxLength: 5 } }
                                        sx={ { width: '55px' } }
                                    />
                                </TextOut>
                            ) }
                            {
                                selectedDatabase && (
                                    <Box className={ styles.wrapper }>
                                        <LocalizationProvider dateAdapter={ AdapterDateFns } adapterLocale={ ruLocale }>
                                            <DesktopDateTimePicker
                                                sx={ { color: 'red' } }
                                                value={ startTime }
                                                ampm={ false }
                                                maxDate={ endTime ?? undefined }
                                                onChange={ (date: Date | null) => setStartTime(date) }
                                                format="dd/MM/yyyy"
                                            />
                                        </LocalizationProvider>
                                        <TextOut style={ { lineHeight: '36px', fontWeight: 'bold' } }>
                                            -
                                        </TextOut>
                                        <LocalizationProvider dateAdapter={ AdapterDateFns } adapterLocale={ ruLocale }>
                                            <DesktopDateTimePicker
                                                value={ endTime }
                                                ampm={ false }
                                                minDate={ startTime ?? undefined }
                                                onChange={ (date: Date | null) => setEndTime(date) }
                                                format="dd/MM/yyyy"
                                            />
                                        </LocalizationProvider>
                                        <Button onClick={ handleGetBlocks }>
                                            Обновить
                                        </Button>
                                    </Box>
                                )
                            }
                        </Box>
                        <Box display="grid" gridTemplateColumns="3fr 4fr" gap="12px">
                            { blockAnalysisLocks && (
                                <Box>
                                    <TextOut fontSize={ 16 }>
                                        Таблица виновников блокировки
                                    </TextOut>
                                    <BlockAnalysisTable locks={ blockAnalysisLocks.Locks } selectedLock={ selectedLock } handleRadioChange={ handleRadioChange } handleAccordionChange={ handleAccordionChange } expanded={ expanded } />
                                </Box>
                            ) }
                            { blockAnalysisLocks && (
                                <Box sx={ { overflowX: 'auto' } }>
                                    <TextOut fontSize={ 16 }>
                                        Таблица жертв блокировки
                                    </TextOut>
                                    <VictimsTable lockList={ lockList } />
                                </Box>
                            ) }
                        </Box>
                    </Box>
                </Collapse>
            </Box>
        </Box>
    );
});