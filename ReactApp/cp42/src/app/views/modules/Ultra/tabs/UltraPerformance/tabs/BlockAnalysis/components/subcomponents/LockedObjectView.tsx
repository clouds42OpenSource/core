/* eslint-disable react/no-array-index-key */
import React, { useState } from 'react';
import {
    Box,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableRow,
    Paper,
    IconButton,
    TextField,
    Tooltip,
} from '@mui/material';
import { LockedObject } from 'app/api/endpoints/blockAnalysis/response/getBlockAnalysisLock';
import { Dialog } from 'app/views/components/controls/Dialog';
import WysiwygIcon from '@mui/icons-material/Wysiwyg';
import ContentCopyIcon from '@mui/icons-material/ContentCopy';
import { useFloatMessages, EMessageType } from 'app/hooks';
import { TextOut } from 'app/views/components/TextOut';

type Props = {
    context: string;
    userName: string;
    lockedObjects?: LockedObject[];
};

export const LockedObjectView = ({ context, userName, lockedObjects }: Props) => {
    const [isModalOpened, setIsModalOpened] = useState(false);
    const { show } = useFloatMessages();

    const handleClick = () => {
        setIsModalOpened(true);
    };

    const handleCopyContext = async () => {
        try {
            await navigator.clipboard.writeText(context);
            show(EMessageType.success, 'Контекст скопирован в буфер обмена');
        } catch (err) {
            show(EMessageType.error, 'Ошибка при копировании контекста');
        }
    };

    return (
        <Box>
            <IconButton onClick={ handleClick }>
                <WysiwygIcon />
            </IconButton>
            <Dialog
                isOpen={ isModalOpened }
                dialogWidth="md"
                onCancelClick={ () => setIsModalOpened(false) }
                title="Детали события"
                titleFontSize={ 20 }
                titleTextAlign="left"
                isTitleSmall={ true }
            >
                <Box sx={ { padding: '16px' } }>
                    <Box mb={ 2 }>
                        <TextField
                            InputProps={ {
                                readOnly: true,
                            } }
                            label="Пользователь"
                            fullWidth={ true }
                            value={ userName }
                        />
                    </Box>
                    <Box mb={ 2 } position="relative">
                        <TextField
                            InputProps={ {
                                readOnly: true,
                            } }
                            label="Контекст"
                            value={ context }
                            multiline={ true }
                            fullWidth={ true }
                        />
                        <Tooltip title="Скопировать контекст">
                            <IconButton
                                onClick={ handleCopyContext }
                                sx={ {
                                    position: 'absolute',
                                    top: '8px',
                                    right: '8px',
                                } }
                            >
                                <ContentCopyIcon />
                            </IconButton>
                        </Tooltip>
                    </Box>
                    { lockedObjects && (
                        <Box>
                            <TextOut>
                                Объекты блокировки
                            </TextOut>
                            <TableContainer component={ Paper }>
                                <Table>
                                    <TableBody>
                                        { lockedObjects.map(lockedObject => (
                                            <>
                                                <TableRow>
                                                    <TableCell>
                                                        <TextOut fontWeight={ 500 }>
                                                            Объект
                                                        </TextOut>
                                                    </TableCell>
                                                    <TableCell>
                                                        <TextOut fontWeight={ 500 }>
                                                            { lockedObject.tableName } / { lockedObject.tableName1C }
                                                        </TextOut>
                                                    </TableCell>
                                                </TableRow>
                                                { lockedObject.fields.map((field, fieldIndex) => (
                                                    <TableRow key={ fieldIndex }>
                                                        <TableCell>
                                                            <TextOut>
                                                                { field.name }
                                                                { field.name1C && ` / ${ field.name1C }` }
                                                            </TextOut>
                                                        </TableCell>
                                                        <TableCell sx={ { padding: 2, position: 'relative' } }>
                                                            <TextField
                                                                InputProps={ {
                                                                    readOnly: true,
                                                                    sx: { paddingRight: '12px' }
                                                                } }
                                                                value={ field.valueDecrypted || field.value }
                                                                multiline={ true }
                                                                fullWidth={ true }
                                                            />
                                                            <Tooltip title="Скопировать значение">
                                                                <IconButton
                                                                    onClick={ handleCopyContext }
                                                                    sx={ {
                                                                        position: 'absolute',
                                                                        top: '16px',
                                                                        right: '12px',
                                                                    } }
                                                                >
                                                                    <ContentCopyIcon />
                                                                </IconButton>
                                                            </Tooltip>
                                                        </TableCell>
                                                    </TableRow>
                                                )) }
                                            </>
                                        )) }
                                    </TableBody>
                                </Table>
                            </TableContainer>
                        </Box>
                    ) }
                </Box>
            </Dialog>
        </Box>
    );
};