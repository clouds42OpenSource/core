/* eslint-disable react/no-unstable-nested-components */
import { Circle as CircleIcon, HelpOutline as HelpOutlineIcon } from '@mui/icons-material';
import { Box, Button, IconButton, Tooltip } from '@mui/material';
import { TextOut } from 'app/views/components/TextOut';
import { SeparateChart } from 'app/views/modules/Ultra/components/SeparateChart';
import { UltraChartModal } from 'app/views/modules/Ultra/components/UltraChartModal';
import { ZabbixItemWithRecommendation } from 'app/views/modules/Ultra/types';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { memo, useState } from 'react';

type Props = {
    recommendedDBMSValues: ZabbixItemWithRecommendation[];
    updateData: () => void;
}

const RecommendedDBMSView = ({ recommendedDBMSValues, updateData }: Props) => {
    const [modalData, setModalData] = useState<ZabbixItemWithRecommendation | null>(null);
    const [isModalOpened, setIsModalOpened] = useState(false);

    const handleOpenModal = (value: ZabbixItemWithRecommendation) => {
        setModalData(value);
        setIsModalOpened(true);
    };
    return (
        <>
            <Box textAlign="end">
                <Button variant="contained" sx={ { boxShadow: 'none', marginBottom: '12px' } } onClick={ updateData }>
                    Обновить данные
                </Button>
            </Box>
            <CommonTableWithFilter
                uniqueContextProviderStateId="recommendedDBMS"
                isResponsiveTable={ true }
                isBorderStyled={ true }
                tableProps={ {
                    dataset: recommendedDBMSValues.filter(({ separate }) => !separate),
                    keyFieldName: 'name',
                    fieldsView: {
                        head: {
                            caption: 'Наименование',
                            fieldWidth: '50%',
                            format: (value, row) => (
                                <TextOut>
                                    { value }
                                    <Tooltip title={ row.description }>
                                        <IconButton aria-label="prompt">
                                            <HelpOutlineIcon />
                                        </IconButton>
                                    </Tooltip>
                                </TextOut>
                            )
                        },
                        recommended: {
                            caption: 'Рекомендуемое значение',
                            fieldWidth: 'auto'
                        },
                        lastvalue: {
                            caption: 'Последнее значение',
                            fieldWidth: 'auto',
                            format: value => parseFloat(Number(value).toFixed(2)) || value
                        },
                        itemid: {
                            caption: 'История данных',
                            fieldWidth: 'auto',
                            format: (value, row) => (
                                <Button onClick={ () => handleOpenModal(row) }>
                                    График
                                </Button>
                            )
                        },
                        compare: {
                            caption: 'Флажок',
                            fieldWidth: 'auto',
                            format: (value, row) => {
                                return (
                                    <Box display="flex" gap="6px">
                                        { value ? (
                                            <CircleIcon sx={ { color: value(Number(row.lastvalue), recommendedDBMSValues) ? 'rgba(30, 212, 19, 0.99)' : 'red', fontSize: '12px' } } />
                                        ) : '' }
                                    </Box>
                                );
                            },
                        }
                    }
                } }
            />
            { modalData && <UltraChartModal
                criticalText="Предельное значение"
                patterns={ [modalData.pattern] }
                modalTitle={ modalData.name }
                isModalOpened={ isModalOpened }
                setIsModalOpened={ setIsModalOpened }
                items={ [modalData] }
            /> }
            <Box>
                { recommendedDBMSValues.filter(({ separate }) => separate).map(item => {
                    return (
                        <Box key={ item.key }>
                            <SeparateChart item={ item } title={ item.head } pattern={ item.pattern } tooltip={ item.description } noDataText={ `Данные по ${ item.head } не найдены в вашей группе хостов` } />
                        </Box>
                    );
                }) }
            </Box>
        </>
    );
};

export const RecommendedDBMS = memo(RecommendedDBMSView);