import { Box, MenuItem, Select } from '@mui/material';
import { templateErrorText } from 'app/api';
import { DashboardAttributes, SavedObject } from 'app/api/endpoints/kibana/response';
import { getZabbixHostGroupsResponseDto } from 'app/api/endpoints/zabbix/response';
import { FETCH_API } from 'app/api/useFetchApi';
import { EMessageType, useAppSelector, useFloatMessages } from 'app/hooks';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { TextOut } from 'app/views/components/TextOut';
import { SuccessButton } from 'app/views/components/controls/Button';
import { ReviewModal } from 'app/views/modules/Ultra/components/ReviewModal';
import { memo, useEffect, useState } from 'react';
import { SuggestImprovementsModal } from 'app/views/modules/Ultra/components/SuggestImprovementModal';
import { ReferenceComponent } from '../components/ReferenceComponent';

type Props = {
    hostGroup: getZabbixHostGroupsResponseDto;
};

const AnalysisMagazineReference = ({ isDefaultOpen = false, companyId } : { isDefaultOpen?: boolean; companyId: number; }) => {
    return (
        <ReferenceComponent
            isDefaultOpen={ isDefaultOpen }
            extraButtons={
                <SuggestImprovementsModal
                    functionalityName="Анализ запросов 1С"
                    companyId={ companyId }
                />
            }
        >
            <span>
                Используя функционал анализа технологического журнала 1С, можно проанализировать вывод самых частых и долгих запросов с сортировкой по контексту стека вызовов.
            </span>
            <span>
                Диагностировать длительные запросы можно путем анализа технологических журналов по событиям <b>DBMSSQL</b> (или другие в зависимости от используемой СУБД) и SDBL.
            </span>
            <span>
                По умолчанию, вкладка <b>“Анализ техжурнала”</b> выводит статистику по наиболее длительным запросам в удобном виде, есть возможности:
            </span>
            <ul>
                <li>Выбор периода вывода данных.</li>
                <li>Отображение сводки с группировкой запросов с наибольшей длительностью.</li>
                <li>Отображение визуализации времени выполнения запросов.</li>
                <li>Показ детальных записей технологического журнала с сортировкой.</li>
            </ul>
            <span>
                Дашборд выводит информацию по данным технологического журнала за указанный период. В технологический журнал попадают события с отбором:
            </span>
            <ul>
                <li>Событие <b>DBMSSQL</b> (или другие в зависимости от используемой СУБД), <b>SDBL.</b></li>
                <li>Длительность события больше 1 сек (1 000 000 микросекунд).</li>
            </ul>
            <span>
                Блок <b>“Сводка”</b> выводит сгруппированную информацию по запросам с наибольшей длительностью.
                Данные в таблице сортируются по полю “Среднее время выполнения” по убыванию, на первых позициях находятся наиболее длительные события. В блоке содержатся колонки:
            </span>
            <ul>
                <li>Строка вызова – значение поля <b>Context</b> из записи технологического журнала.</li>
                <li>Суммарное время выполнения – длительность события, значение поля <b /> из записи технологического журнала.</li>
                <li>Количество – количество повторений события за указанный период.</li>
                <li>Среднее время выполнения – расчетное значение. Рассчитывается по формуле: Суммарное время выполнения / Количество.</li>
            </ul>
            <span>
                Блок <b>“Визуализация времени выполнения запросов”</b> предоставляет графическую сводку по наиболее длительным запросам.
                По диаграмме можно отследить в какое время был выполнен длительный запрос.
            </span>
            <span>
                Блок <b>“Детальные записи технологического журнала”</b> отображает записи технологического журнала за указанный период в хронологическом порядке. В блоке содержатся колонки:
            </span>
            <ul>
                <li>Timestamp – дата и время возникновения события.</li>
                <li>Имя события – имя события.</li>
                <li>Контекст – контекст события.</li>
                <li>Имя пользователя – имя пользователя, который инициировал событие.</li>
                <li>Имя сервера 1С – наименование сервера 1С, на котором были собраны логи технологического журнала.</li>
                <li>Текст запроса – текст запроса на стороне СУБД.</li>
                <li>Исх. запись тех. журнала – “сырая”, необработанная запись технологического журнала (необходима в том случае, если при анализе требуется получить больше данных).</li>
            </ul>
            <span>Значения группировок (контексты) в блоке <b>“Сводка”</b> являются кликабельными элементами управления.
                При нажатии на контекст будет применен отбор по значению. В таком случае в блоке <b>“Детальные записи технологического журнала”</b> будут отображаться записи только по выбранному контексту.
            </span>
        </ReferenceComponent>
    );
};

export const AnalysisMagazine = memo(({ hostGroup }: Props) => {
    const [dashBoardList, setDashBoardList] = useState<SavedObject<DashboardAttributes>[]>([]);
    const [selectedDashboardId, setSelectedDashboardId] = useState('');
    const [isLoading, setIsLoading] = useState(false);
    const [isEnabled, setisEnabled] = useState(true);
    const { show } = useFloatMessages();
    const myCompany = useAppSelector(state => state.MyCompanyState.myCompany);

    const sendRequest = async () => {
        setisEnabled(false);
        setIsLoading(true);

        const response = await FETCH_API.EMAILS.sendEmail({
            body: `Прошу подключить анализ технологического журнала в личном кабинете для компании: ${ myCompany.accountCaption }, ${ myCompany.indexNumber }`,
            subject: 'Заявка на подключение анализа технологического журнала',
            header: 'Заявка на подключение анализа технологического журнала',
            categories: 'corp-cloud',
            reference: import.meta.env.REACT_ZABBIX_EMAIL ?? ''
        });

        setIsLoading(false);

        if (response.success) {
            show(EMessageType.success, 'Заявка на подключение успешно отправлена');
        } else {
            setisEnabled(true);
            show(EMessageType.error, 'Произошла ошибка');
        }
    };

    const handleDashboardChange = (value: string) => {
        setSelectedDashboardId(value);
    };

    const getDashboardTitle = (title: string) => {
        const match = title.match(/^([^()]+)\s*(?:\(|$)/);
        return match ? match[1].trim() : title;
    };

    useEffect(() => {
        setSelectedDashboardId('');
        (async () => {
            const response = await FETCH_API.KIBANA.getKibanaDashboards();
            if (response.data) {
                const pattern = /\((\d+)\)/;
                const filtered = response.data.saved_objects.filter(obj => {
                    const match = pattern.exec(obj.attributes.title);
                    const parts = hostGroup.name.split('&&');

                    return (
                        match &&
                        match[1] === parts[1] &&
                        !obj.attributes.title.includes('errors')
                    );
                });
                setDashBoardList(filtered);
            } else {
                show(EMessageType.error, response.message ?? templateErrorText);
            }
        })();
    }, [hostGroup, show]);

    if (!dashBoardList.length) {
        return (
            <Box>
                { isLoading && <LoadingBounce /> }
                <AnalysisMagazineReference companyId={ myCompany.indexNumber } isDefaultOpen={ true } />
                <Box marginTop="6px">
                    <SuccessButton isEnabled={ isEnabled } onClick={ () => sendRequest() }>
                        Отправить заявку
                    </SuccessButton>
                </Box>
            </Box>
        );
    }

    return (
        <Box display="flex" flexDirection="column" gap="12px">
            { isLoading && <LoadingBounce /> }
            <Box>
                <Box>
                    <AnalysisMagazineReference companyId={ myCompany.indexNumber } />
                    <Box>
                        <TextOut>
                            Выберите дашборд
                        </TextOut>
                        <Select
                            sx={ { margin: '12px' } }
                            value={ selectedDashboardId }
                            defaultValue=""
                            displayEmpty={ true }
                            onChange={ ({ target: { value } }) => handleDashboardChange(value) }
                        >
                            <MenuItem value="">
                                Не выбрано
                            </MenuItem>
                            { dashBoardList.map(data => (
                                <MenuItem key={ data.id } value={ data.id }>
                                    { getDashboardTitle(data.attributes.title) }
                                </MenuItem>
                            )) }
                        </Select>
                        <ReviewModal
                            title="Заявка на подключение персонализированного дашборда"
                            buttonText="Подключить новый дашборд"
                            placeholder="Опишите ожидаемые данные"
                            description="Функционал анализа техжурнала 1С позволяет подключить графический вывод любой информации, которую собирает технологический журнал 1С.
                            Для того, чтобы подключить вывод нужной вам информации, опишите вашу задачу как можно более подробно, после чего отправьте заявку."
                            subject={ `Заявка на подключение дополнительных дашбордов для анализа технологического журнала для компании ${ myCompany.indexNumber }` }
                            header={ `Заявка на подключение дополнительных дашбордов для анализа технологического журнала для компании ${ myCompany.indexNumber }` }
                        />
                    </Box>
                    {
                        selectedDashboardId && <iframe
                            style={ { width: '100%', height: '250vh', border: 'none' } }
                            title="kibana"
                            src={ `https://elka.esit.info/app/dashboards?auth_provider_hint=anonymous1#/view/${ selectedDashboardId
                            }?embed=true&_g=(refreshInterval%3A(pause%3A!t%2Cvalue%3A60000)%2Ctime%3A(from%3Anow-15m%2Cto%3Anow))&show-query-input=true&show-time-filter=true` }
                        />
                    }
                </Box>
            </Box>
        </Box>
    );
});