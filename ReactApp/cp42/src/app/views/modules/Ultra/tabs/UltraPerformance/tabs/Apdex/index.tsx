/* eslint-disable jsx-a11y/control-has-associated-label */
/* eslint-disable react/no-unstable-nested-components */
/* eslint-disable no-underscore-dangle */
import { Box, Collapse } from '@mui/material';
import { getZabbixHostGroupsResponseDto } from 'app/api/endpoints/zabbix/response';
import { REDUX_API } from 'app/api/useReduxApi';
import { EMessageType, useAppSelector, useFloatMessages } from 'app/hooks';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { TextOut } from 'app/views/components/TextOut';
import { SuccessButton, TextButton } from 'app/views/components/controls/Button';
import { memo, useEffect, useMemo, useState } from 'react';
import { IdNameRecord } from 'app/api/endpoints/apdex/common';
import { KeyboardArrowDown as KeyboardArrowDownIcon, KeyboardArrowRight as KeyBoardArrowRightIcon } from '@mui/icons-material';
import { SuggestImprovementsModal } from 'app/views/modules/Ultra/components/SuggestImprovementModal';
import { FETCH_API } from 'app/api/useFetchApi';
import { useGetApdexDashboards, useGetGrafanaToken } from 'app/api/endpoints';
import styles from './styles.module.css';
import { ReferenceComponent } from '../components/ReferenceComponent';
import { ApdexProfiles } from './components/ApdexProfiles';
import { ApdexCreateProfile } from './components/ApdexCreateProfiles';

type Props = {
  hostGroup: getZabbixHostGroupsResponseDto;
};

const ApdexTabView = ({ hostGroup }: Props) => {
    const { show } = useFloatMessages();
    const myCompany = useAppSelector(state => state.MyCompanyState.myCompany);

    const [databaseList, setDatabaseList] = useState<IdNameRecord[] | null>([]);
    const [hostGroupId, setHostGroupId] = useState<string | null>(null);
    const [isLoadingRequest, setIsLoadingRequest] = useState(false);
    const [isEnabledRequest, setIsEnabledRequest] = useState(true);
    const [isOpened, setIsOpened] = useState(true);

    const clientId = useMemo(() => {
        const match = hostGroup.name.split('&&');
        return match?.[1]?.trim() || '';
    }, [hostGroup]);

    const {
        data: dashboardsData,
        error: dashboardsError,
        isLoading: isDashboardsLoading
    } = useGetApdexDashboards(clientId);

    const [cachedDashboards, setCachedDashboards] = useState(dashboardsData);

    useEffect(() => {
        setCachedDashboards(null);
    }, [clientId]);

    useEffect(() => {
        if (dashboardsData) {
            setCachedDashboards(dashboardsData);
        }
    }, [dashboardsData]);

    const firstUid = cachedDashboards?.rawData?.[0]?.uid;

    const {
        data: tokenData,
        error: tokenError,
        isLoading: isTokenLoading
    } = useGetGrafanaToken(firstUid);

    const [cachedToken, setCachedToken] = useState(tokenData);

    useEffect(() => {
        setCachedToken(null);
    }, [firstUid]);

    useEffect(() => {
        if (tokenData) {
            setCachedToken(tokenData);
        }
    }, [tokenData]);

    useEffect(() => {
        if (dashboardsError) {
            show(EMessageType.error, dashboardsError);
        }
    }, [dashboardsError, show]);

    useEffect(() => {
        if (tokenError) {
            show(EMessageType.error, tokenError);
        }
    }, [tokenError, show]);

    useEffect(() => {
        const match = hostGroup.name.split('&&');
        if (match.length > 1) {
            const id = match[1].trim();
            setHostGroupId(id);
            (async () => {
                const response = await REDUX_API.APDEX_REDUX.getApdexDatabaseList({ clientName: id });
                if (response.data) {
                    setDatabaseList(response.data);
                }
            })();
        }
    }, [hostGroup]);

    const sendRequest = async () => {
        setIsEnabledRequest(false);
        setIsLoadingRequest(true);

        const response = await FETCH_API.EMAILS.sendEmail({
            body: `Прошу подключить тест APDEX в личном кабинете для компании: ${ myCompany.accountCaption }, ${ myCompany.indexNumber }`,
            subject: 'Заявка на подключение APDEX (Корпоративное облако)',
            header: 'Заявка на подключение APDEX (Корпоративное облако)',
            categories: 'corp-cloud',
            reference: import.meta.env.REACT_ZABBIX_EMAIL ?? ''
        });

        setIsLoadingRequest(false);

        if (response.success) {
            show(EMessageType.success, 'Заявка на подключение успешно отправлена');
        } else {
            setIsEnabledRequest(true);
            show(EMessageType.error, 'Произошла ошибка при отправке заявки');
        }
    };

    const isApdexLoading = isDashboardsLoading || isTokenLoading;

    const accessToken = cachedToken?.rawData?.accessToken;

    return (
        <div>
            { (isLoadingRequest || isApdexLoading) && <LoadingBounce /> }

            <ReferenceComponent
                extraButtons={
                    <SuggestImprovementsModal
                        functionalityName="Оценка APDEX"
                        companyId={ myCompany.indexNumber }
                    />
                }
            >
                <TextOut className={ styles.apdexTable }>
                    <b style={ { fontSize: '16px' } }>Общая информация</b><br /><br />
                    С использованием методики APDEX, можно объективно оценить производительность информационной системы и следить за здоровьем 1С.<br /><br />
                </TextOut>
            </ReferenceComponent>

            <Box className={ styles.collapseWrapper }>
                <TextButton onClick={ () => setIsOpened(!isOpened) }>
                    { isOpened ? <KeyboardArrowDownIcon /> : <KeyBoardArrowRightIcon /> }
                    Базовый мониторинг производительности 1С (APDEX)
                </TextButton>

                <Collapse in={ isOpened }>
                    <Box p="12px">
                        { !firstUid ? (
                            <>
                                <TextOut>
                                    Чтобы подключить вывод показателя APDEX для вашей базы, отправьте заявку.
                                </TextOut>
                                <Box marginTop="6px">
                                    <SuccessButton isEnabled={ isEnabledRequest } onClick={ sendRequest }>
                                        Отправить заявку
                                    </SuccessButton>
                                </Box>
                            </>
                        ) : !accessToken ? (
                            <TextOut>Нет доступа к данным APDEX.</TextOut>
                        ) : (
                            <iframe
                                title="APDEX Dashboard"
                                style={ { border: 'none', marginTop: '12px' } }
                                width="100%"
                                height="600px"
                                src={ `https://grafana.esit.info/public-dashboards/${ accessToken }` }
                            />
                        ) }
                    </Box>
                </Collapse>
            </Box>

            <Box className={ styles.wrapper } gap="16px" marginTop="12px">
                <Box className={ styles.wrapper }>
                    { databaseList && hostGroupId && (
                        <ApdexProfiles databaseList={ databaseList } hostGroupId={ hostGroupId } />
                    ) }
                </Box>

                { databaseList && hostGroupId && (
                    <ApdexCreateProfile databaseList={ databaseList } hostGroupId={ hostGroupId } />
                ) }
            </Box>
        </div>
    );
};

export const ApdexTab = memo(ApdexTabView);