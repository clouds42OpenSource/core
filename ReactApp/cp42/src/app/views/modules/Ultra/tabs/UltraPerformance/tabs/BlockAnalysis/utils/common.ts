export const sortData = <T, >(data: T[], fieldName: keyof T, sortKind: number) => {
    return [...data].sort((a, b) => {
        if (sortKind === 0) {
            return a[fieldName] > b[fieldName] ? 1 : -1;
        }
        return a[fieldName] < b[fieldName] ? 1 : -1;
    });
};