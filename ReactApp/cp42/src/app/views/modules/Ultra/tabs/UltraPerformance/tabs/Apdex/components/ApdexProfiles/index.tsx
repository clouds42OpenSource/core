/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react/no-unstable-nested-components */
import { Box, Button, Collapse, IconButton, TextField } from '@mui/material';
import { getApdexProfileDataResponseDto } from 'app/api/endpoints/apdex/response';
import { useCallback, useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { EMessageType, useAppSelector, useFloatMessages } from 'app/hooks';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { IdNameRecord } from 'app/api/endpoints/apdex/common';
import dayjs from 'dayjs';
import { templateErrorText } from 'app/api';
import { TextButton } from 'app/views/components/controls/Button';
import { KeyboardArrowDown as KeyboardArrowDownIcon, KeyboardArrowRight as KeyBoardArrowRightIcon } from '@mui/icons-material';
import { SelectDataCommonDataModel, SortingDataModel } from 'app/web/common';
import { REDUX_API } from 'app/api/useReduxApi';
import EditIcon from '@mui/icons-material/Edit';
import SaveIcon from '@mui/icons-material/Save';
import CancelIcon from '@mui/icons-material/Cancel';
import { DateUtility } from 'app/utils';
import { DesktopDateTimePicker, LocalizationProvider } from '@mui/x-date-pickers';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { TextOut } from 'app/views/components/TextOut';
import ruLocale from 'date-fns/locale/ru';
import styles from '../../styles.module.css';
import { ApdexChart } from './subComponents/ApdexDatabasesChart';
import { ApdexProfileDetailsModal } from './subComponents/ApdexProfileDetailsModal';
import { ProfileSelect } from './subComponents/common';
import { DatabaseSelect } from '../../../components/DatabaseSelect';

type Props = {
    databaseList: IdNameRecord[];
    hostGroupId: string;
};

const { APDEX_REDUX } = REDUX_API;

export const ApdexProfiles = ({ databaseList, hostGroupId }: Props) => {
    const [selectedDatabase, setSelectedDatabase] = useState('');
    const [selectedProfile, setSelectedProfile] = useState('');
    const [profileData, setProfileData] = useState<getApdexProfileDataResponseDto | null>(null);
    const { data, isLoading: isProfileDataLoading } = useAppSelector(state => state.Apdex.getApdexProfilesReducer);
    const [isModalOpened, setIsModalOpened] = useState(false);
    const [selectedKeyOperation, setSelectedKeyOperation] = useState({ name: '', id: '' });
    const [isOpened, setIsOpened] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const [editingRows, setEditingRows] = useState<{ [key: string]: boolean }>({});
    const [editedTargetTimes, setEditedTargetTimes] = useState<{ [key: string]: number }>({});
    const [startTime, setStartTime] = useState<Date | null>(DateUtility.getSomeDaysAgoDate(7));
    const [endTime, setEndTime] = useState<Date | null>(new Date());
    const [tempStartTime, setTempStartTime] = useState<Date | null>(DateUtility.getSomeDaysAgoDate(7));
    const [tempEndTime, setTempEndTime] = useState<Date | null>(new Date());

    const handleUpdate = () => {
        setStartTime(tempStartTime);
        setEndTime(tempEndTime);
    };

    const { show } = useFloatMessages();

    const dispatch = useDispatch();

    const handleGetPerformanceProfiles = useCallback(() => {
        if (selectedDatabase) {
            void APDEX_REDUX.getApdexProfiles(dispatch, hostGroupId, selectedDatabase);
        }
    }, [selectedDatabase, hostGroupId, dispatch]);

    const handleChangeSelectedDatabase = (value: string) => {
        setSelectedDatabase(value);
        if (!value) {
            setSelectedProfile('');
            setProfileData(null);
        }
    };

    const handleGetProfileData = useCallback(async (paginationData: { pageNumber: number, pageSize: number, sortingData?: SortingDataModel }) => {
        const { pageNumber, pageSize = 10, sortingData } = paginationData;
        const orderBy = sortingData && `${ sortingData.fieldName }.${ sortingData.sortKind ? 'desc' : 'asc' }`;
        if (selectedProfile && selectedDatabase) {
            const today = dayjs().format('DD.MM.YYYY');
            setIsLoading(true);
            const response = await APDEX_REDUX.getApdexProfileData({ clientName: hostGroupId,
                query: { databaseId: selectedDatabase,
                    profileId: selectedProfile,
                    fromDate: startTime ? dayjs(startTime).format('DD.MM.YYYY') : today,
                    toDate: endTime ? dayjs(endTime).format('DD.MM.YYYY') : today,
                    page: pageNumber,
                    pageSize,
                    orderBy } });
            setIsLoading(false);

            if (response.data && response.success) {
                setProfileData(response.data);
            } else {
                show(EMessageType.error, response.message ?? templateErrorText);
            }
        }
    }, [selectedProfile, selectedDatabase, show, startTime, endTime]);

    const handleOpenModal = (modalData: { id: string, name: string }) => {
        setSelectedKeyOperation(modalData);
        setIsModalOpened(true);
    };

    const handleDataSelect = (value: SelectDataCommonDataModel<void>) => {
        const { pageNumber, sortingData } = value;
        handleGetProfileData({ pageNumber, pageSize: Math.max(profileData?.scores.pagination['page-size'] || 10, 10), sortingData });
    };

    const handleEditTargetTime = (keyOperationId: string) => {
        setEditingRows(prev => ({
            ...prev,
            [keyOperationId]: true,
        }));

        if (profileData) {
            const currentTargetTime = profileData.scores.records[0].details.find(
                detail => detail.keyOperation.id === keyOperationId
            );

            if (currentTargetTime) {
                setEditedTargetTimes(prev => ({
                    ...prev,
                    [keyOperationId]: currentTargetTime.keyOperation.targetTime,
                }));
            }
        }
    };

    const handleCancelEdit = (keyOperationId: string) => {
        setEditingRows(prev => ({
            ...prev,
            [keyOperationId]: false,
        }));
        setEditedTargetTimes(prev => {
            const { [keyOperationId]: _, ...rest } = prev;
            return rest;
        });
    };

    const handleSaveTargetTime = async (keyOperationId: string) => {
        const newTargetTime = editedTargetTimes[keyOperationId];
        const clientName = hostGroupId;
        const databaseId = selectedDatabase;

        const payload = {
            keyOperations: [
                {
                    id: keyOperationId,
                    targetTime: newTargetTime,
                },
            ],
        };

        setIsLoading(true);
        const response = await APDEX_REDUX.updateTargetTime({ data: payload, clientName, databaseId });
        setIsLoading(false);

        if (response.data && response.success) {
            setProfileData(prevProfileData => {
                if (!prevProfileData) return prevProfileData;

                const newDetails = prevProfileData.scores.records[0].details.map(detail => {
                    if (detail.keyOperation.id === keyOperationId) {
                        return {
                            ...detail,
                            keyOperation: {
                                ...detail.keyOperation,
                                targetTime: newTargetTime,
                                APDEX_RateRelevanceDate: response.data ? response.data.keyOperations[0].APDEX_RateRelevanceDate : '',
                            },
                        };
                    }
                    return detail;
                });

                return {
                    ...prevProfileData,
                    scores: {
                        ...prevProfileData.scores,
                        records: [
                            {
                                ...prevProfileData.scores.records[0],
                                details: newDetails,
                            },
                        ],
                    },
                };
            });

            setEditingRows(prev => {
                const { [keyOperationId]: _, ...rest } = prev;
                return rest;
            });

            setEditedTargetTimes(prev => {
                const { [keyOperationId]: _, ...rest } = prev;
                return rest;
            });

            show(EMessageType.success, 'Целевое время успешно изменено');
        } else {
            show(EMessageType.error, response.message ?? templateErrorText);
        }
    };

    useEffect(() => {
        handleGetProfileData({ pageNumber: 1, pageSize: 10 });
    }, [handleGetProfileData]);

    useEffect(() => {
        handleGetPerformanceProfiles();
    }, [handleGetPerformanceProfiles]);

    useEffect(() => {
        setSelectedDatabase('');
        setSelectedProfile('');
        setProfileData(null);
    }, [hostGroupId]);

    return (
        <Box className={ styles.collapseWrapper }>
            { (isLoading || isProfileDataLoading) && <LoadingBounce /> }
            <TextButton onClick={ () => setIsOpened(!isOpened) }>
                { isOpened ? <KeyboardArrowDownIcon /> : <KeyBoardArrowRightIcon /> }
                Расширенный мониторинг производительности 1С
            </TextButton>
            <Collapse in={ isOpened }>
                <Box className={ styles.wrapper } sx={ { padding: '12px' } }>
                    <Box display="flex" gap="6px">
                        { databaseList && (
                            <DatabaseSelect
                                databaseList={ databaseList }
                                selectedDatabase={ selectedDatabase }
                                setSelectedDatabase={ handleChangeSelectedDatabase }
                            />
                        ) }
                        { selectedDatabase && data?.rawData.performanceProfiles.length ? (
                            <ProfileSelect
                                profiles={ data.rawData.performanceProfiles }
                                selectedProfile={ selectedProfile }
                                setSelectedProfile={ setSelectedProfile }
                            />
                        ) : null }
                    </Box>
                    { selectedProfile && (
                        <Box>
                            <LocalizationProvider dateAdapter={ AdapterDateFns } adapterLocale={ ruLocale }>
                                <DesktopDateTimePicker
                                    sx={ { color: 'red' } }
                                    value={ tempStartTime }
                                    ampm={ false }
                                    maxDate={ endTime ?? undefined }
                                    onChange={ (date: Date | null) => setTempStartTime(date) }
                                    format="dd/MM/yyyy"
                                />
                            </LocalizationProvider>
                            <TextOut style={ { lineHeight: '36px', fontWeight: 'bold' } }>
                                -
                            </TextOut>
                            <LocalizationProvider dateAdapter={ AdapterDateFns } adapterLocale={ ruLocale }>
                                <DesktopDateTimePicker
                                    value={ tempEndTime }
                                    ampm={ false }
                                    minDate={ startTime ?? undefined }
                                    onChange={ (date: Date | null) => setTempEndTime(date) }
                                    format="dd/MM/yyyy"
                                />
                            </LocalizationProvider>
                            <Button onClick={ handleUpdate }>Обновить</Button>

                        </Box>
                    ) }
                    { selectedProfile && (
                        <ApdexChart
                            startTime={ startTime }
                            endTime={ endTime }
                            databaseId={ selectedDatabase }
                            profileId={ selectedProfile }
                            hostGroupId={ hostGroupId }
                        />
                    ) }
                    { profileData && (
                        <CommonTableWithFilter
                            uniqueContextProviderStateId="apdexProfiles"
                            isResponsiveTable={ true }
                            isBorderStyled={ true }
                            tableProps={ {
                                dataset: profileData.scores.records[0].details.map(score => ({ ...score, profileId: profileData.profile.id })),
                                pagination: {
                                    totalPages: profileData.scores.pagination['total-pages'],
                                    recordsCount: profileData.scores.pagination['total-items'],
                                    currentPage: profileData.scores.pagination['page-number'],
                                    pageSize: profileData.scores.pagination['page-size'],
                                },
                                keyFieldName: 'keyOperation',
                                fieldsView: {
                                    keyOperation: {
                                        caption: 'Ключевая операция',
                                        fieldWidth: '50%',
                                        format: ({ name }) => name
                                    },
                                    score: {
                                        caption: 'Оценка',
                                        format: value => String(value),
                                        isSortable: true,
                                    },
                                    apdex_rate: {
                                        caption: 'Время расчета оценки',
                                        isSortable: true,
                                        format: (value, row) => row.keyOperation.APDEX_RateRelevanceDate,
                                    },
                                    averageExecutionTime: {
                                        caption: 'Среднее время, сек',
                                        format: value => String(value),
                                        isSortable: true,
                                    },
                                    measuringCount: {
                                        caption: 'Кол-во замеров',
                                        isSortable: true,
                                        format: value => String(value),
                                    },
                                    temp: {
                                        caption: 'Целевое время, сек',
                                        format: (value, row) => {
                                            const keyOperationId = row.keyOperation.id;
                                            const isEditing = editingRows[keyOperationId];
                                            if (isEditing) {
                                                const editedValue = editedTargetTimes[keyOperationId] !== undefined ? editedTargetTimes[keyOperationId] : row.keyOperation.targetTime;
                                                return (
                                                    <div style={ { display: 'flex', alignItems: 'center' } }>
                                                        <TextField
                                                            type="number"
                                                            value={ editedValue }
                                                            inputProps={ {
                                                                maxLength: 5
                                                            } }
                                                            onChange={ e => {
                                                                const newValue = e.target.value;
                                                                if (/^\d{0,5}$/.test(newValue)) {
                                                                    const numericValue = parseInt(newValue, 10) || 0;
                                                                    setEditedTargetTimes(prev => ({
                                                                        ...prev,
                                                                        [keyOperationId]: numericValue,
                                                                    }));
                                                                }
                                                            } }
                                                            size="small"
                                                            style={ { width: '80px' } }
                                                        />
                                                        <IconButton onClick={ () => handleSaveTargetTime(keyOperationId) } size="small">
                                                            <SaveIcon />
                                                        </IconButton>
                                                        <IconButton onClick={ () => handleCancelEdit(keyOperationId) } size="small">
                                                            <CancelIcon />
                                                        </IconButton>
                                                    </div>
                                                );
                                            }
                                            return (
                                                <div style={ { display: 'flex', alignItems: 'center', justifyContent: 'space-between' } }>
                                                    { row.keyOperation.targetTime }
                                                    <IconButton onClick={ () => handleEditTargetTime(keyOperationId) } size="small">
                                                        <EditIcon />
                                                    </IconButton>
                                                </div>
                                            );
                                        },
                                    },
                                    profileId: {
                                        caption: 'История',
                                        format: (value, row) => (
                                            <Button onClick={ () => handleOpenModal({ name: row.keyOperation.name, id: row.keyOperation.id }) }>
                                                График
                                            </Button>
                                        )
                                    }
                                }
                            } }
                            onDataSelect={ handleDataSelect }
                        />
                    ) }
                    { profileData && (
                        <Box display="flex" justifyContent="space-between">
                            <Button
                                onClick={ () => handleGetProfileData({ pageNumber: 1, pageSize: profileData.scores.pagination['page-size'] + 50 }) }
                                disabled={ profileData.scores.pagination['page-size'] >= profileData.scores.pagination['total-items'] }
                            >
                                Добавить 50
                            </Button>
                            <Button
                                onClick={ () => handleGetProfileData({ pageNumber: 1, pageSize: profileData.scores.pagination['total-items'] }) }
                                disabled={ profileData.scores.pagination['total-pages'] === 1 }
                            >
                                Показать все
                            </Button>
                        </Box>
                    ) }
                    <ApdexProfileDetailsModal
                        hostGroupId={ hostGroupId }
                        databaseId={ selectedDatabase }
                        profileId={ selectedProfile }
                        keyOperationId={ selectedKeyOperation.id }
                        setIsModalOpened={ setIsModalOpened }
                        isModalOpened={ isModalOpened }
                        modalTitle={ selectedKeyOperation.name }
                    />
                </Box>
            </Collapse>
        </Box>
    );
};