import { Box, Collapse } from '@mui/material';
import { TextButton } from 'app/views/components/controls/Button';
import { AddCircle as AddCircleIcon, RemoveCircle as RemoveCircleIcon } from '@mui/icons-material';
import { useState, ReactNode } from 'react';
import { TextOut } from 'app/views/components/TextOut';

type ReferenceComponentProps = {
    isDefaultOpen?: boolean;
    title?: string;
    children: ReactNode;
    extraButtons?: ReactNode;
    mb?: string;
};

export const ReferenceComponent = ({ isDefaultOpen = false, title = 'Справка', children, extraButtons, mb = '12px' }: ReferenceComponentProps) => {
    const [isOpen, setIsOpen] = useState(isDefaultOpen);

    const handleOpenReference = () => {
        setIsOpen(!isOpen);
    };

    return (
        <Box marginBottom={ mb }>
            <TextButton onClick={ handleOpenReference }>
                { isOpen ? <RemoveCircleIcon fontSize="small" /> : <AddCircleIcon fontSize="small" /> }
                <span style={ { marginLeft: '8px' } }>{ title }</span>
            </TextButton>
            { extraButtons }
            <Collapse sx={ { marginTop: '12px' } } in={ isOpen }>
                <TextOut inDiv={ true } style={ { display: 'flex', flexDirection: 'column', gap: '6px' } }>
                    { children }
                </TextOut>
            </Collapse>
        </Box>
    );
};