import { ApdexScoresListResponseDto } from 'app/api/endpoints/apdex/response';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import 'chartjs-adapter-date-fns';
import dayjs from 'dayjs';
import { useCallback, useEffect, useState } from 'react';
import { EMessageType, useFloatMessages } from 'app/hooks';
import { templateErrorText } from 'app/api';
import { ChartData } from 'app/views/modules/Ultra/types';
import { REDUX_API } from 'app/api/useReduxApi';
import { ApdexBaseChart } from './ApdexBaseChart';
import { parseDate } from '../../../utils';

type Props = {
    databaseId: string;
    profileId: string;
    hostGroupId: string;
    startTime: Date | null;
    endTime: Date | null;
};

export const ApdexChart = ({ databaseId, profileId, hostGroupId, startTime, endTime }: Props) => {
    const [isLoading, setIsLoading] = useState(false);

    const [data, setData] = useState<ChartData>({ labels: [], datasets: [] });
    const { show } = useFloatMessages();

    const prepareChartData = (chartData: ApdexScoresListResponseDto): ChartData => {
        const { scores } = chartData.performanceScores[0];

        const labels = scores.map(({ scoreDate }) => dayjs(parseDate(scoreDate)).toDate());
        const datasetData = scores.map(({ score }) => score);

        return {
            labels,
            datasets: [
                {
                    label: chartData.performanceScores[0].database.name,
                    data: datasetData,
                    borderColor: 'rgba(54, 162, 235, 1)',
                    fill: false
                }
            ]
        };
    };

    const handleGetData = useCallback(async () => {
        if (profileId && databaseId) {
            setIsLoading(true);
            const response = await REDUX_API.APDEX_REDUX.getApdexScores({ clientName: hostGroupId,
                query: { databaseId,
                    profileId,
                    fromDate: startTime ? dayjs(startTime).format('DD.MM.YYYY') : undefined,
                    toDate: endTime ? dayjs(endTime).format('DD.MM.YYYY') : undefined
                } });
            setIsLoading(false);
            if (response.data) {
                setData(prepareChartData(response.data));
            } else {
                show(EMessageType.error, response.message ?? templateErrorText);
            }
        }
    }, [profileId, databaseId, hostGroupId, show, startTime, endTime]);

    useEffect(() => {
        void handleGetData();
    }, [handleGetData]);

    return (
        <>
            { isLoading && <LoadingBounce /> }
            <ApdexBaseChart chartData={ data } />
        </>
    );
};