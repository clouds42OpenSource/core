/* eslint-disable react/no-unstable-nested-components */
import { Button } from '@mui/material';
import { UltraChartModal } from 'app/views/modules/Ultra/components/UltraChartModal';
import { ZabbixItemWithRecommendation, ZabbixItemsWithRecommendation } from 'app/views/modules/Ultra/types';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { memo, useState } from 'react';

type Props = {
    servers1CValues: ZabbixItemsWithRecommendation;
}

const Servers1CView = ({ servers1CValues }: Props) => {
    const [modalData, setModalData] = useState<ZabbixItemWithRecommendation | null>(null);
    const [isModalOpened, setIsModalOpened] = useState(false);

    const handleOpenModal = (value: ZabbixItemWithRecommendation) => {
        setModalData(value);
        setIsModalOpened(true);
    };

    return (
        <div>
            <CommonTableWithFilter
                uniqueContextProviderStateId="recommendedDBMS"
                isResponsiveTable={ true }
                isBorderStyled={ true }
                tableProps={ {
                    dataset: servers1CValues,
                    keyFieldName: 'name',
                    fieldsView: {
                        name: {
                            caption: 'Наименование',
                            fieldWidth: '50%',
                        },
                        lastvalue: {
                            caption: 'Последнее значение',
                            fieldWidth: 'auto',
                            format: (value, row) => Number(value)
                                ? `${ (Number(value) * row.pattern.formatting.conversionFactor) % 1 === 0
                                    ? (Number(value) * row.pattern.formatting.conversionFactor)
                                    : (Number(value) * row.pattern.formatting.conversionFactor).toFixed(2) } ${ row.pattern.formatting.unit }`
                                : value
                        },
                        itemid: {
                            caption: 'История данных',
                            fieldWidth: 'auto',
                            format: (value, row) => (
                                <Button onClick={ () => handleOpenModal(row) }>
                                    График
                                </Button>
                            )
                        }
                    }
                }
                }
            />
            { modalData && <UltraChartModal criticalText="test" patterns={ [modalData.pattern] } modalTitle={ modalData.name } isModalOpened={ isModalOpened } setIsModalOpened={ setIsModalOpened } items={ [modalData] } /> }
        </div>
    );
};

export const Servers1C = memo(Servers1CView);