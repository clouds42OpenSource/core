/* eslint-disable react/no-unstable-nested-components */
import { Box, Button } from '@mui/material';
import { templateErrorText } from 'app/api';
import { getExpectationDBMSDescriptionResponseDto } from 'app/api/endpoints/mvpCore/response';
import { getZabbixItemsResponseDto } from 'app/api/endpoints/zabbix/response';
import { FETCH_API } from 'app/api/useFetchApi';
import { EMessageType, useFloatMessages } from 'app/hooks';
import { TextOut } from 'app/views/components/TextOut';
import { Dialog } from 'app/views/components/controls/Dialog';
import { ExpectationDBMSConst } from 'app/views/modules/Ultra/constants/ExpectationDBMSConst';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { memo, useCallback, useEffect, useState } from 'react';

type Props = {
    expectationDBMSValues: getZabbixItemsResponseDto[];
    updateData: () => void;
};

type WaitData = {
    waitType: string;
    waitTimeSec: string;
    waitingPercent: string;
    description: string;
};

const ExpectationDMBSView = ({ expectationDBMSValues, updateData }: Props) => {
    const [parsedExpectationValues, setParsedExpectationValues] = useState<WaitData[]>([]);
    const [modalData, setModalData] = useState<getExpectationDBMSDescriptionResponseDto | null>(null);
    const [expectationDescriptions, setExpectationDescriptions] = useState<getExpectationDBMSDescriptionResponseDto[]>([]);
    const [isModalOpened, setIsModalOpened] = useState(false);
    const { show } = useFloatMessages();

    const handleGetExpectationDescriptions = useCallback(async () => {
        if (parsedExpectationValues.length) {
            const response = await FETCH_API.MVPCORE.getExpectationDBMSDescription(parsedExpectationValues.map(({ waitType }) => waitType));
            if (response.success && response.data) {
                setExpectationDescriptions(response.data);
            } else {
                setExpectationDescriptions([]);
                show(EMessageType.error, response.message ?? templateErrorText);
            }
        }
    }, [parsedExpectationValues, show]);

    useEffect(() => {
        if (expectationDBMSValues.length) {
            ExpectationDBMSConst.forEach((data, index) => {
                try {
                    const parsedData = JSON.parse(expectationDBMSValues[index].lastvalue);
                    const transformedData = parsedData.map((item: Record<string, string>) => ({
                        waitType: item['Wait type'],
                        waitTimeSec: item['Wait time (s)'],
                        waitingPercent: item['% waiting'],
                        description: item['Wait type'],
                    }));

                    if (transformedData.length) {
                        setParsedExpectationValues(transformedData);
                    }
                } catch (error) {
                }
            });
        } else {
            setParsedExpectationValues([]);
        }
    }, [expectationDBMSValues]);

    useEffect(() => {
        handleGetExpectationDescriptions();
    }, [handleGetExpectationDescriptions]);

    const handleOpenModal = (value: getExpectationDBMSDescriptionResponseDto) => {
        setModalData(value);
        setIsModalOpened(true);
    };

    return (
        <div>
            <Box textAlign="end">
                <Button variant="contained" sx={ { boxShadow: 'none', marginBottom: '12px' } } onClick={ updateData }>
                    Обновить данные
                </Button>
            </Box>
            <TextOut inDiv={ true } fontWeight={ 500 } fontSize={ 18 }>
                Определение ожиданий
            </TextOut>
            <TextOut inDiv={ true } style={ { margin: '6px 0' } }>
                Ожидания и статистика ожидания представляют собой метрики, позволяющие выяснить причины, по которым экземпляр SQL Server
                не может обработать запрос немедленно вследствие нехватки одного или нескольких ресурсов. Таким образом, это объяснение, почему SQL Server не удалось выполнить задачу.
                SQL Server не может выполнить запрос из-за ограниченности ресурсов, и вы можете выяснить, где имеет место значимое ограничение.
                Кроме того, сведения о конкретных ограничениях ресурсов могут привести администратора к решениям по настройке производительности, так как вы можете быстро определить, где источник ваших проблем
            </TextOut>
            <TextOut inDiv={ true } fontWeight={ 400 } style={ { textDecoration: 'underline', marginBottom: '8px' } }>
                <a href="https://learn.microsoft.com/ru-ru/sql/relational-databases/system-dynamic-management-views/sys-dm-os-wait-stats-transact-sql?view=sql-server-ver16&redirectedfrom=MSDN" target="_blank" rel="noreferrer">
                    Подробное описание ожиданий
                </a>
            </TextOut>
            <CommonTableWithFilter
                uniqueContextProviderStateId="expectationDBMS"
                isResponsiveTable={ true }
                isBorderStyled={ true }
                tableProps={ {
                    dataset: parsedExpectationValues,
                    keyFieldName: 'waitType',
                    fieldsView: {
                        waitType: {
                            caption: 'Тип ожидания',
                            fieldWidth: '50%',
                        },
                        waitTimeSec: {
                            caption: 'Время ожидания',
                            fieldWidth: 'auto'
                        },
                        waitingPercent: {
                            caption: 'Процент ожидания',
                            fieldWidth: 'auto',
                        },
                        description: {
                            caption: 'Описание',
                            fieldWidth: 'auto',
                            format: value => {
                                const descriptionData = expectationDescriptions.find(({ waitType }) => waitType === value);
                                return !!descriptionData && (
                                    <Button onClick={ () => handleOpenModal(descriptionData) }>
                                        Подробнее
                                    </Button>
                                );
                            }
                        }
                    }
                } }
            />
            { modalData && (
                <Dialog
                    isOpen={ isModalOpened }
                    dialogWidth="md"
                    onCancelClick={ () => setIsModalOpened(false) }
                    title={ modalData.waitType }
                    titleFontSize={ 20 }
                    titleTextAlign="left"
                    isTitleSmall={ true }
                >
                    <TextOut inDiv={ true }>
                        <span>
                            { modalData.description }
                        </span>
                        <Box>
                            <span>
                                Причины:
                            </span>
                            <ul>
                                { modalData.causes.map(value => <li>{ value }</li>) }
                            </ul>
                        </Box>
                        <Box>
                            <span>
                                Решение:
                            </span>
                            <ul>
                                { modalData.recommendations.map(value => <li>{ value }</li>) }
                            </ul>
                        </Box>
                    </TextOut>
                </Dialog>
            ) }
        </div>
    );
};

export const ExpectationDMBS = memo(ExpectationDMBSView);