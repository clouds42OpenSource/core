/* eslint-disable react-hooks/exhaustive-deps */
import { Box, Collapse } from '@mui/material';
import { templateErrorText } from 'app/api';
import { DashboardAttributes, SavedObject } from 'app/api/endpoints/kibana/response';
import { FETCH_API } from 'app/api/useFetchApi';
import { EMessageType, useFloatMessages } from 'app/hooks';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { memo, useEffect, useState } from 'react';
import { TextOut } from 'app/views/components/TextOut';
import { ReviewModal } from 'app/views/modules/Ultra/components/ReviewModal';
import { MyCompanyItemDataModel } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/receiveMyCompany';
import { TextButton } from 'app/views/components/controls/Button';
import { KeyboardArrowDown as KeyboardArrowDownIcon, KeyboardArrowRight as KeyBoardArrowRightIcon } from '@mui/icons-material';
import { SuggestImprovementsModal } from 'app/views/modules/Ultra/components/SuggestImprovementModal';
import styles from '../style.module.css';
import { ReferenceComponent } from '../../components/ReferenceComponent';

type Props = {
    myCompany: MyCompanyItemDataModel;
    clientId: string;
};

export const BlockAnalysisErrors = memo(({ clientId, myCompany }: Props) => {
    const [dashboardData, setDashboardData] = useState<SavedObject<DashboardAttributes> | null>(null);
    const [isLoading, setIsLoading] = useState(false);
    const [isOpened, setIsOpened] = useState(true);

    const { show } = useFloatMessages();

    useEffect(() => {
        setDashboardData(null);
        (async () => {
            setIsLoading(true);
            const response = await FETCH_API.KIBANA.getKibanaDashboards();
            setIsLoading(false);

            if (response.success && response.data) {
                const found = response.data.saved_objects.find(obj => {
                    const title = obj.attributes.title || '';
                    return (
                        title.includes(`(${ clientId })`) &&
                        title.includes('lock-err')
                    );
                });
                setDashboardData(found ?? null);
            } else {
                show(EMessageType.error, response.message ?? templateErrorText);
            }
        })();
    }, [clientId, show]);

    const LockErrorsReference = ({ isDefaultOpen = false }: { isDefaultOpen?: boolean }) => {
        return (
            <Box padding="6px 14px">
                <ReferenceComponent
                    mb="0"
                    isDefaultOpen={ isDefaultOpen }
                    extraButtons={
                        <SuggestImprovementsModal
                            functionalityName="Ошибки блокировок 1С"
                            companyId={ myCompany.indexNumber }
                        />
                    }
                >
                    <b>Мониторинг событий TTIMEOUT и TDEADLOCK</b>
                    <span>
                        Функционал анализа ошибок блокировок 1С позволяет подключить
                        графический вывод информации по событиям <b>TTIMEOUT</b> и <b>TDEADLOCK</b> из логов
                        технологического журнала 1С.
                    </span>
                    <span>
                        Мониторинг событий TTIMEOUT и TDEADLOCK в логах 1С необходим для:
                    </span>
                    <ul>
                        <li>Контроля производительности системы</li>
                        <li>Выявления проблемных участков кода</li>
                        <li>Определения необходимости оптимизации</li>
                        <li>Предупреждения массовых сбоев</li>
                        <li>Принятия решений по масштабированию системы</li>
                    </ul>
                    <span>
                        Частое появление этих ошибок в логах сигнализирует о необходимости
                        оптимизации кода или увеличения производительности системы.
                    </span>
                </ReferenceComponent>
            </Box>
        );
    };

    if (!dashboardData) {
        return (
            <Box className={ styles.collapseWrapper }>
                <TextButton onClick={ () => setIsOpened(!isOpened) }>
                    { isOpened ? <KeyboardArrowDownIcon /> : <KeyBoardArrowRightIcon /> }
                    Ошибки блокировок 1С
                </TextButton>
                <Collapse in={ isOpened }>
                    <LockErrorsReference isDefaultOpen={ false } />
                    <Box padding="6px 14px">
                        { isLoading && <LoadingBounce /> }
                        <TextOut>
                            Функционал не подключен. Чтобы подключить вывод ошибок блокировок 1С, отправьте заявку.
                        </TextOut>
                        <Box marginTop="6px">
                            <ReviewModal
                                title="Заявка на подключение функционала анализа ошибок блокировок 1С"
                                buttonText="Отправить заявку"
                                placeholder="Опишите вашу задачу и наименование базы 1С"
                                description={ `
                                    Функционал анализа ошибок блокировок 1С позволяет подключить
                                    графический вывод информации по событиям TTIMEOUT и TDEADLOCK из логов технологического журнала 1С.
                                    Для того, чтобы подключить вывод информации, опишите вашу задачу и наименование базы 1С, 
                                    после чего отправьте заявку.
                                ` }
                                subject={ `Заявка на подключение анализа ошибок блокировок 1С для компании ${ myCompany.indexNumber }` }
                                header={ `Заявка на подключение анализа ошибок блокировок 1С для компании ${ myCompany.indexNumber }` }
                            />
                        </Box>
                    </Box>
                </Collapse>
            </Box>
        );
    }

    return (
        <Box className={ styles.collapseWrapper }>
            <TextButton onClick={ () => setIsOpened(!isOpened) }>
                { isOpened ? <KeyboardArrowDownIcon /> : <KeyBoardArrowRightIcon /> }
                Ошибки блокировок 1С
            </TextButton>
            <Collapse in={ isOpened }>
                <LockErrorsReference isDefaultOpen={ false } />
                <Box display="flex" flexDirection="column" gap="12px" padding="6px 14px">
                    { isLoading && <LoadingBounce /> }
                    <Box>
                        <iframe
                            style={ { width: '100%', height: '94vh', border: 'none' } }
                            title="kibana-lock-errors"
                            src={ `https://elka.esit.info/app/dashboards?auth_provider_hint=anonymous1#/view/${ dashboardData.id
                            }?embed=true&_g=(refreshInterval%3A(pause%3A!t%2Cvalue%3A60000)%2Ctime%3A(from%3Anow-15m%2Cto%3Anow))&show-query-input=true&show-time-filter=true` }
                        />
                    </Box>
                </Box>
            </Collapse>
        </Box>
    );
});