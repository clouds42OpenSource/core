import { MenuItem, Select } from '@mui/material';
import { IdNameRecord } from 'app/api/endpoints/apdex/common';
import { memo } from 'react';

type TDatabaseSelect = {
    databaseList: IdNameRecord[];
    selectedDatabase: string;
    setSelectedDatabase: (val: string) => void;
};

export const DatabaseSelect = memo(({ databaseList, selectedDatabase, setSelectedDatabase }: TDatabaseSelect) => (
    <Select
        value={ selectedDatabase }
        onChange={ e => setSelectedDatabase(e.target.value) }
        displayEmpty={ true }
    >
        <MenuItem value="">Выберите базу</MenuItem>
        { databaseList.map(item => (
            <MenuItem key={ item.id } value={ item.id }>
                { item.name }
            </MenuItem>
        )) }
    </Select>
));