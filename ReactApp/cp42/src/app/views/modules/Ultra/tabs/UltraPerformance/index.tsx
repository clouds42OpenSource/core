/* eslint-disable no-loop-func */
/* eslint-disable no-underscore-dangle */
import { getZabbixHostGroupsResponseDto, getZabbixHostsResponseDto, getZabbixItemsResponseDto } from 'app/api/endpoints/zabbix/response';
import { FETCH_API } from 'app/api/useFetchApi';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { TabsControl } from 'app/views/components/controls/TabsControl';
import { ExpectationDBMSConst } from 'app/views/modules/Ultra/constants/ExpectationDBMSConst';
import { RequestsAndLoadsDBMSConst } from 'app/views/modules/Ultra/constants/RequestsAndLoadsDBMSConst';
import { Servers1CConst } from 'app/views/modules/Ultra/constants/Servers1CConst';
import { StatusIndexConst } from 'app/views/modules/Ultra/constants/StatusIndexConst';
import { recommendedDBMSConst } from 'app/views/modules/Ultra/constants/recommendedDBMSConst';
import { ApdexTab } from 'app/views/modules/Ultra/tabs/UltraPerformance/tabs/Apdex';
import { AnalysisMagazine } from 'app/views/modules/Ultra/tabs/UltraPerformance/tabs/AnalysisMagazine';
import { ExpectationDMBS } from 'app/views/modules/Ultra/tabs/UltraPerformance/tabs/ExpectationDBMS';
import { OperationDMBS } from 'app/views/modules/Ultra/tabs/UltraPerformance/tabs/OperationDBMS';
import { RecommendedDBMS } from 'app/views/modules/Ultra/tabs/UltraPerformance/tabs/RecommendedDBMS';
import { RequestsAndLoadsDBMS } from 'app/views/modules/Ultra/tabs/UltraPerformance/tabs/RequestsAndLoadsDBMS';
import { Servers1C } from 'app/views/modules/Ultra/tabs/UltraPerformance/tabs/Servers1C';
import { StatusIndex } from 'app/views/modules/Ultra/tabs/UltraPerformance/tabs/StatusIndex';
import { ZabbixItemsWithRecommendation } from 'app/views/modules/Ultra/types';
import { ZabbixItemsForOperations } from 'app/views/modules/Ultra/types/ZabbixItemsForOpetaions';
import { useCallback, useEffect, useState } from 'react';
import { TableFragmentation } from './tabs/TableFragmentation';
import { TableFragmentationConst } from '../../constants/TableFragmentationConst';
import { BlockAnalysisView } from './tabs/BlockAnalysis';
import { AnalysisMagazineErrors } from './tabs/AnalysisErrors';
import { buildZabbixDataByKeys } from '../../utils/buildZabbixData';

type Props = {
    hostGroup: getZabbixHostGroupsResponseDto;
};

export const UltraPerformance = ({ hostGroup }: Props) => {
    const [isLoading, setIsLoading] = useState(false);
    const [hosts, setHosts] = useState<getZabbixHostsResponseDto[]>([]);
    const [expectationDBMSValues, setExpectationDBMSValues] = useState<getZabbixItemsResponseDto[]>([]);
    const [requestsAndLoadsDBMSValues, setRequestsAndLoadsDBMSValues] = useState<getZabbixItemsResponseDto[]>([]);
    const [recommendedDBMSValues, setRecommendedDBMSValues] = useState<ZabbixItemsWithRecommendation>([]);
    const [operationDBMSValues, setOperationDBMSValues] = useState<ZabbixItemsForOperations>([]);
    const [statusIndexValues, setStatusIndexValues] = useState<getZabbixItemsResponseDto[]>([]);
    const [servers1CValues, setServers1CValues] = useState<ZabbixItemsWithRecommendation>([]);
    const [tableFragmentationValues, setTableFragmentationValues] = useState<getZabbixItemsResponseDto | null>(null);
    const [isFirstLoad, setIsFirstLoad] = useState(false);
    const [useNewKeys, setUseNewKeys] = useState(false);

    const handleGetHosts = useCallback(async (groupId: string) => {
        setIsLoading(true);
        const response = await FETCH_API.ZABBIX.getHosts(groupId);
        setIsLoading(false);
        if (response.result) {
            setHosts(response.result);
        }
    }, []);

    const clearData = () => {
        setRecommendedDBMSValues([]);
        setExpectationDBMSValues([]);
        setRequestsAndLoadsDBMSValues([]);
        setStatusIndexValues([]);
        setOperationDBMSValues([]);
        setServers1CValues([]);
        setTableFragmentationValues(null);
    };

    const setExtensionValues = useCallback((data: getZabbixItemsResponseDto[]) => {
        const hasAnyNewKey = data.some(item => item.key_?.startsWith(RequestsAndLoadsDBMSConst[0].newKey));
        setUseNewKeys(hasAnyNewKey);

        const requestsAndLoads = buildZabbixDataByKeys(data, RequestsAndLoadsDBMSConst, hasAnyNewKey);
        setRequestsAndLoadsDBMSValues(requestsAndLoads);

        const expectData = buildZabbixDataByKeys(data, ExpectationDBMSConst, hasAnyNewKey);
        setExpectationDBMSValues(expectData);

        const statusIndexData = buildZabbixDataByKeys(data, StatusIndexConst, hasAnyNewKey);
        setStatusIndexValues(statusIndexData);

        const combinedRecommendedDBMSValues = data.reduce<getZabbixItemsResponseDto[]>((acc, item) => {
            const matchingConstItem = recommendedDBMSConst.find(constItem => constItem.key === item.key_);
            if (matchingConstItem) {
                acc.push({ ...item, ...matchingConstItem });
            }
            return acc;
        }, []);
        setRecommendedDBMSValues(combinedRecommendedDBMSValues as unknown as ZabbixItemsWithRecommendation);

        const combinedOperationDBMSValues = data.reduce<getZabbixItemsResponseDto[]>((acc, item) => {
            const match = item.key_.match(/\["RegOps\.(.*?)"\]/);

            if (match && match[1]) {
                acc.push({ ...item });
            }

            return acc;
        }, []);

        setOperationDBMSValues(combinedOperationDBMSValues as unknown as ZabbixItemsForOperations);

        const combinedSevers1CValues = data.reduce<getZabbixItemsResponseDto[]>((acc, item) => {
            const matchingConstItem = Servers1CConst.find(constItem => constItem.key === item.key_);
            if (matchingConstItem) {
                acc.push({ ...item, ...matchingConstItem });
            }
            return acc;
        }, []);
        setServers1CValues(combinedSevers1CValues as unknown as ZabbixItemsWithRecommendation);
        setTableFragmentationValues(data.find(({ key_ }) => key_ === TableFragmentationConst.name) ?? null);
    }, []);

    const handleUpdateData = useCallback(async () => {
        if (hosts.length) {
            setIsLoading(true);
            const fetchExtentionsGroup = async (host: getZabbixHostsResponseDto) => {
                const response = await FETCH_API.ZABBIX.getItems(host.hostid);
                if (response.result) {
                    const findedMSSql = response.result.find(item => item.key_ === 'net.tcp.service[tcp,,1433]');
                    if (findedMSSql) {
                        setExtensionValues(response.result);
                        return findedMSSql;
                    }
                }

                return null;
            };

            const extentionsGroupResult = await Promise.all(hosts.map(fetchExtentionsGroup));
            const foundMSSQL = extentionsGroupResult.find(result => result !== null);
            if (!foundMSSQL) {
                clearData();
            }

            setIsLoading(false);
            setIsFirstLoad(true);
        } else {
            clearData();
        }
    }, [hosts]);

    useEffect(() => {
        void handleGetHosts(hostGroup.groupid);
    }, [hostGroup, handleGetHosts]);

    useEffect(() => {
        void handleUpdateData();
    }, [handleUpdateData]);

    if (!isFirstLoad) {
        return <LoadingBounce />;
    }

    return (
        <div>
            { (isLoading && isFirstLoad) && <LoadingBounce /> }
            <TabsControl
                headers={ [
                    {
                        title: 'Оценка APDEX',
                        isVisible: true
                    },
                    {
                        title: 'Анализ запросов 1С',
                        isVisible: true,
                    },
                    {
                        title: 'Анализ ошибок 1С',
                        isVisible: true,
                    },
                    {
                        title: 'Анализ блокировок 1С',
                        isVisible: true,
                    },
                    {
                        title: 'Рекомендованные параметры СУБД',
                        isVisible: true
                    },
                    {
                        title: 'Регламентные операции СУБД',
                        isVisible: true
                    },
                    {
                        title: 'Ожидания в СУБД',
                        isVisible: true
                    },
                    {
                        title: 'Запросы и нагрузка на СУБД',
                        isVisible: true
                    },
                    {
                        title: 'Состояние индексов',
                        isVisible: true
                    },
                    {
                        title: 'Сервер 1С',
                        isVisible: true
                    },
                    {
                        title: 'Фрагментация таблиц',
                        isVisible: true
                    }
                ] }
            >
                <ApdexTab hostGroup={ hostGroup } />
                <AnalysisMagazine hostGroup={ hostGroup } />
                <AnalysisMagazineErrors hostGroup={ hostGroup } />
                <BlockAnalysisView hostGroup={ hostGroup } />
                <RecommendedDBMS recommendedDBMSValues={ recommendedDBMSValues } updateData={ handleUpdateData } />
                <OperationDMBS operationDBMSValues={ operationDBMSValues } />
                <ExpectationDMBS expectationDBMSValues={ expectationDBMSValues } updateData={ handleUpdateData } />
                <RequestsAndLoadsDBMS useNewKeys={ useNewKeys } requestsAndLoadsDBMSValues={ requestsAndLoadsDBMSValues } />
                <StatusIndex useNewKeys={ useNewKeys } statusIndexValues={ statusIndexValues } updateData={ handleUpdateData } />
                <Servers1C servers1CValues={ servers1CValues } />
                <TableFragmentation tableFragmentationValues={ tableFragmentationValues } />
            </TabsControl>
        </div>
    );
};