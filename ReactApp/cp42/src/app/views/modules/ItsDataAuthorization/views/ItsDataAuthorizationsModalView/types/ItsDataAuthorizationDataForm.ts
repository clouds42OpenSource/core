/**
 * Модель для редакс формы для создания и редактирования данных авторизации в ИТС
 */
export type ItsDataAuthorizationDataForm = {
    /**
     * Логин
     */
    login: string,

    /**
     * Хэш пароля
     */
    passwordHash: string
}