import { Box } from '@mui/material';
import { PageEnumType } from 'app/common/enums';
import { checkOnValidField, checkValidOfStringField } from 'app/common/functions';
import { IErrorsType } from 'app/common/interfaces';
import { ItsAuthorizationDataProcessId } from 'app/modules/itsAuthorizationData';
import { CreateItsDataAuthorizationThunkParams } from 'app/modules/itsAuthorizationData/store/reducers/createItsDataAuthorizationReducer/params';
import { EditItsDataAuthorizationThunkParams } from 'app/modules/itsAuthorizationData/store/reducers/editItsDataAuthorizationReducer/params';
import { GetItsDataAuthorizationThunkParams } from 'app/modules/itsAuthorizationData/store/reducers/getItsDataAuthorizationReducer/params';
import { CreateItsDataAuthorizationThunk, EditItsDataAuthorizationThunk, GetItsDataAuthorizationThunk } from 'app/modules/itsAuthorizationData/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { Dialog } from 'app/views/components/controls/Dialog';
import { DialogButtonType } from 'app/views/components/controls/Dialog/views/DialogActionsView/Index';
import { TextBoxForm } from 'app/views/components/controls/forms';
import { ItsDataAuthorizationDataForm } from 'app/views/modules/ItsDataAuthorization/views/ItsDataAuthorizationsModalView/types';
import { CommonPasswordFieldView } from 'app/views/modules/_common/components/ComponentForPasswordField';
import { ShowErrorMessage } from 'app/views/modules/_common/components/ShowErrorMessage';
import { WatchReducerProcessActionState } from 'app/views/modules/_common/components/WatchReducerProcessActionState';
import { ItsDataAuthorizationDataModel } from 'app/web/InterlayerApiProxy/ItsAuthorizationDataApiProxy/data-models';
import { IReducerProcessActionInfo } from 'core/redux/interfaces';
import { Component } from 'react';
import { connect } from 'react-redux';

type OwnProps = ReduxFormProps<ItsDataAuthorizationDataForm>;

type ItsDataAuthorizationsModalProps = {
    /**
     * Поле для открытия модального окошка
     */
    isOpen: boolean;

    /**
     * ID модель данных авторизации в ИТС
     */
    itsAuthorizationDataId?: string;

    /**
     * Функция на закрытие модального окошка
     */
    onClickToCloseModal: () => void;
};

type ItsDataAuthorizationsModalState = {
    /**
     * Обьект Ошибок для валидации
     */
    errors: IErrorsType;

    /**
     * Тип страницы для динамического компонента редактирования/создания
     */
    pageType: PageEnumType;
};

type StateProps = {
    /**
     * Модель данных авторизации в ИТС
     */
    itsDataAuthorization: ItsDataAuthorizationDataModel;
};

type DispatchProps = {
    dispatchGetItsDataAuthorizationThunk: (args: GetItsDataAuthorizationThunkParams) => void;
    dispatchCreateItsDataAuthorizationThunk: (args: CreateItsDataAuthorizationThunkParams) => void;
    dispatchEditItsDataAuthorizationThunk: (args: EditItsDataAuthorizationThunkParams) => void;
};

type AllProps = ItsDataAuthorizationsModalProps & OwnProps & StateProps & DispatchProps & FloatMessageProps;

class ItsDataAuthorizationsModalClass extends Component<AllProps, ItsDataAuthorizationsModalState> {
    public constructor(props: AllProps) {
        super(props);

        this.state = {
            errors: {},
            pageType: this.props.itsAuthorizationDataId ? PageEnumType.Edit : PageEnumType.Create
        };

        this.getDialogBody = this.getDialogBody.bind(this);
        this.onValueChange = this.onValueChange.bind(this);
        this.submitChanges = this.submitChanges.bind(this);
        this.createItsDataAuthorization = this.createItsDataAuthorization.bind(this);
        this.editItsDataAuthorization = this.editItsDataAuthorization.bind(this);
        this.isItsDataAuthorizationsFormValid = this.isItsDataAuthorizationsFormValid.bind(this);

        this.onProcessActionStateChanged = this.onProcessActionStateChanged.bind(this);
        this.onProcessGetItsDataAuthorization = this.onProcessGetItsDataAuthorization.bind(this);
        this.successGetItsDataAuthorizationState = this.successGetItsDataAuthorizationState.bind(this);
        this.errorGetItsDataAuthorizationState = this.errorGetItsDataAuthorizationState.bind(this);
        this.fillItsDataAuthorization = this.fillItsDataAuthorization.bind(this);
        this.getDialogActionButtons = this.getDialogActionButtons.bind(this);

        this.props.reduxForm.setInitializeFormDataAction(this.defaultInitDataReduxForm);
    }

    public componentDidMount() {
        if (this.props.itsAuthorizationDataId) {
            this.props.dispatchGetItsDataAuthorizationThunk({
                itsAuthorizationId: this.props.itsAuthorizationDataId,
                force: true
            });
        }
    }

    private onProcessActionStateChanged(processId: string, actionState: IReducerProcessActionInfo) {
        this.onProcessGetItsDataAuthorization(processId, actionState);
    }

    private onProcessGetItsDataAuthorization(processId: string, actionState: IReducerProcessActionInfo) {
        if (processId !== ItsAuthorizationDataProcessId.GetItsDataAuthorization) return;
        this.successGetItsDataAuthorizationState(actionState);
        this.errorGetItsDataAuthorizationState(actionState);
    }

    private onValueChange(formName: string, newValue: string) {
        this.props.reduxForm.updateReduxFormFields({
            [formName]: newValue
        });

        const { errors } = this.state;

        switch (formName) {
            case 'login':
                errors[formName] = checkValidOfStringField(newValue, 'Заполните логин');
                break;
            case 'passwordHash':
                errors[formName] = checkValidOfStringField(newValue, 'Заполните пароль');
                break;
            default:
                break;
        }
        this.setState({ errors });
    }

    private getDialogActionButtons(): DialogButtonType {
        return [
            {
                kind: 'default',
                content: 'Закрыть',
                fontSize: 12,
                variant: 'outlined',
                onClick: this.props.onClickToCloseModal
            },
            {
                kind: 'success',
                content: `${ this.state.pageType === PageEnumType.Edit ? 'Сохранить' : 'Создать' }`,
                fontSize: 12,
                onClick: this.submitChanges
            }
        ];
    }

    private getDialogBody() {
        const formFields = this.props.reduxForm.getReduxFormFields(false);
        const { errors } = this.state;

        return (
            <Box display="flex" flexDirection="column" gap="16px">
                <TextBoxForm
                    formName="login"
                    label="Логин"
                    placeholder="Введите логин"
                    value={ formFields.login }
                    onValueChange={ this.onValueChange }
                />
                <ShowErrorMessage errors={ errors } formName="login" />
                <CommonPasswordFieldView
                    formName="passwordHash"
                    label="Пароль"
                    onValueChange={ this.onValueChange }
                    placeholder="Введите пароль"
                    value={ formFields.passwordHash }
                />
                <ShowErrorMessage errors={ errors } formName="passwordHash" />
            </Box>
        );
    }

    private successGetItsDataAuthorizationState(actionState: IReducerProcessActionInfo) {
        if (!actionState.isInSuccessState) return;
        const { itsDataAuthorization } = this.props;
        this.props.reduxForm.updateReduxFormFields({
            ...itsDataAuthorization
        });
    }

    private errorGetItsDataAuthorizationState(actionState: IReducerProcessActionInfo, error?: Error) {
        if (!actionState.isInErrorState) return;
        this.props.floatMessage.show(MessageType.Error, `Ошибка при получении данных авторизации доступа к ИТС: ${ error?.message }`);
    }

    private submitChanges() {
        if (!this.isItsDataAuthorizationsFormValid()) return;
        this.createItsDataAuthorization();
        this.editItsDataAuthorization();
    }

    private createItsDataAuthorization() {
        if (this.state.pageType !== PageEnumType.Create) return;
        this.props.dispatchCreateItsDataAuthorizationThunk({
            ...this.fillItsDataAuthorization(),
            force: true
        });
    }

    private editItsDataAuthorization() {
        if (this.state.pageType !== PageEnumType.Edit) return;
        this.props.dispatchEditItsDataAuthorizationThunk({
            ...this.fillItsDataAuthorization(),
            id: this.props.itsAuthorizationDataId!,
            force: true
        });
    }

    private fillItsDataAuthorization(): ItsDataAuthorizationDataModel {
        const formFields = this.props.reduxForm.getReduxFormFields(false);
        return {
            id: '',
            login: formFields.login,
            passwordHash: formFields.passwordHash
        };
    }

    private defaultInitDataReduxForm(): ItsDataAuthorizationDataForm | undefined {
        return {
            login: '',
            passwordHash: ''
        };
    }

    private isItsDataAuthorizationsFormValid() {
        const formFields = this.props.reduxForm.getReduxFormFields(false);
        const errors: IErrorsType = {};
        const arrayResults: boolean[] = [];

        arrayResults.push(checkOnValidField(errors, 'login', formFields.login, 'Заполните логин'));
        arrayResults.push(checkOnValidField(errors, 'passwordHash', formFields.passwordHash, 'Заполните пароль'));

        if (arrayResults.some(item => !item)) {
            this.props.floatMessage.show(MessageType.Warning, 'Форма к отправке не готова. Пожалуйста, заполните все поля.');
            this.setState({ errors });
            return false;
        }

        return true;
    }

    public render() {
        const { isOpen } = this.props;
        const isEditTypePage = this.state.pageType === PageEnumType.Edit;
        const titleText = isEditTypePage ? 'Редактирование' : 'Создание';

        return (
            <>
                <Dialog
                    title={ `${ titleText } доступа к ИТС` }
                    isOpen={ isOpen }
                    dialogWidth="sm"
                    isTitleSmall={ true }
                    titleTextAlign="left"
                    titleFontSize={ 14 }
                    dialogVerticalAlign="center"
                    onCancelClick={ this.props.onClickToCloseModal }
                    buttons={ this.getDialogActionButtons() }
                >
                    {
                        this.getDialogBody()
                    }
                </Dialog>
                <WatchReducerProcessActionState
                    watch={ [{
                        reducerStateName: 'ItsAuthorizationDataState',
                        processIds: [
                            ItsAuthorizationDataProcessId.GetItsDataAuthorization
                        ]
                    }] }
                    onProcessActionStateChanged={ this.onProcessActionStateChanged }
                />
            </>
        );
    }
}

const ItsDataAuthorizationsModalConnected = connect<StateProps, DispatchProps, OwnProps, AppReduxStoreState>(
    state => {
        const itsAuthorizationDataState = state.ItsAuthorizationDataState;
        const { itsDataAuthorization } = itsAuthorizationDataState;

        return {
            itsDataAuthorization
        };
    },
    {
        dispatchGetItsDataAuthorizationThunk: GetItsDataAuthorizationThunk.invoke,
        dispatchCreateItsDataAuthorizationThunk: CreateItsDataAuthorizationThunk.invoke,
        dispatchEditItsDataAuthorizationThunk: EditItsDataAuthorizationThunk.invoke
    }
)(ItsDataAuthorizationsModalClass);

export const ItsDataAuthorizationsModalView = withReduxForm(withFloatMessages(ItsDataAuthorizationsModalConnected),
    {
        reduxFormName: 'Its_Data_Authorizations_Modal_Form',
        resetOnUnmount: true
    });