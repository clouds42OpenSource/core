import React, { Component } from 'react';
import { FilterItemWrapper } from 'app/views/components/_hoc/tableFilterWrapper/filterItemWrapper';
import { SearchByFilterButton } from 'app/views/components/controls/Button';
import { TableFilterWrapper } from 'app/views/components/_hoc/tableFilterWrapper';
import { TextBoxForm } from 'app/views/components/controls/forms';

type ItsDataAuthorizationSearchState = {
    /**
     * Логин доступа к ИТС
     */
    login?: string
};

type ItsDataAuthorizationSearchProps = {
    /**
     * Функция для поиска
     */
    onClickToSearch: () => void;
};

export class ItsDataAuthorizationSearchView extends Component<ItsDataAuthorizationSearchProps, ItsDataAuthorizationSearchState> {
    public constructor(props: ItsDataAuthorizationSearchProps) {
        super(props);

        this.state = {
            login: ''
        };

        this.onValueChange = this.onValueChange.bind(this);
    }

    /**
     * Функция на изменения значения в полях поисковика
     * @param formName Название формы
     * @param newValue Новое значение
     */
    private onValueChange(formName: string, newValue: string) {
        this.setState(prevState => ({
            ...prevState,
            [formName]: newValue
        }));
    }

    public render() {
        return (
            <TableFilterWrapper>
                <FilterItemWrapper width="350px">
                    <TextBoxForm
                        formName="login"
                        label="Логин"
                        placeholder="Введите логин"
                        value={ this.state.login }
                        onValueChange={ this.onValueChange }
                    />
                </FilterItemWrapper>
                <SearchByFilterButton
                    onClick={ this.props.onClickToSearch }
                />
            </TableFilterWrapper>
        );
    }
}