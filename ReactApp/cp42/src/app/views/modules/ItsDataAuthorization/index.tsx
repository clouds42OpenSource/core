import ErrorOutlineIcon from '@mui/icons-material/ErrorOutline';
import { Box } from '@mui/material';
import { ItsAuthorizationDataProcessId } from 'app/modules/itsAuthorizationData';
import { DeleteItsDataAuthorizationThunkParams } from 'app/modules/itsAuthorizationData/store/reducers/deleteItsDataAuthorizationReducer/params';
import { GetItsDataAuthorizationsThunkParams } from 'app/modules/itsAuthorizationData/store/reducers/getItsDataAuthorizationsReducer/params';
import { DeleteItsDataAuthorizationThunk, GetItsDataAuthorizationsThunk } from 'app/modules/itsAuthorizationData/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { TextOut } from 'app/views/components/TextOut';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { withHeader } from 'app/views/components/_hoc/withHeader';
import { SuccessButton, TextLinkButton } from 'app/views/components/controls/Button';
import { Dialog } from 'app/views/components/controls/Dialog';
import { ItsDataAuthorizationSearchView } from 'app/views/modules/ItsDataAuthorization/views/ItsDataAuthorizationSearchView';
import { ItsDataAuthorizationsModalView } from 'app/views/modules/ItsDataAuthorization/views/ItsDataAuthorizationsModalView';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { WatchReducerProcessActionState } from 'app/views/modules/_common/components/WatchReducerProcessActionState';
import { ItsAuthorizationDataPaginationDataModel, ItsDataAuthorizationDataModel } from 'app/web/InterlayerApiProxy/ItsAuthorizationDataApiProxy/data-models';
import { CommonSegmentDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import { SelectDataCommonDataModel, SortingDataModel } from 'app/web/common/data-models';
import { IReducerProcessActionInfo } from 'core/redux/interfaces';
import { Component, createRef } from 'react';
import { connect } from 'react-redux';
import css from './styles.module.css';

type ItsDataAuthorizationState = {
    /**
     * Поле для открытия модального окошка
     */
    isOpen: boolean;

    /**
     * ID данных авторизации в ИТС
     */
    itsAuthorizationDataId?: string;
    /**
     * Состояние модального окна на удаление доступа к ИТС
     */
    deleteIsOpen: boolean;
};

type StateProps = {
    /**
     * Массив данных авторизации в ИТС
     */
    itsDataAuthorizations: ItsAuthorizationDataPaginationDataModel;

    /**
     * Если true, то массив данных авторизации в ИТС получены
     */
    hasItsDataAuthorizationsReceived: boolean;
};

type DispatchProps = {
    dispatchGetItsDataAuthorizationsThunk: (args: GetItsDataAuthorizationsThunkParams) => void;
    dispatchDeleteItsDataAuthorizationThunk: (args: DeleteItsDataAuthorizationThunkParams) => void;
};

type AllProps = StateProps & DispatchProps & FloatMessageProps;

class ItsDataAuthorizationClass extends Component<AllProps, ItsDataAuthorizationState> {
    private _searchItsDataAuthorizationsRef = createRef<ItsDataAuthorizationSearchView>();

    private _commonTableRef = createRef<CommonTableWithFilter<ItsDataAuthorizationDataModel, undefined>>();

    public constructor(props: AllProps) {
        super(props);

        this.state = {
            isOpen: false,
            deleteIsOpen: false
        };

        this.getFormatLoginItsDataAuthorization = this.getFormatLoginItsDataAuthorization.bind(this);
        this.getButtonsForActions = this.getButtonsForActions.bind(this);
        this.onDataSelect = this.onDataSelect.bind(this);

        this.onClickToSearch = this.onClickToSearch.bind(this);
        this.onClickToCloseModal = this.onClickToCloseModal.bind(this);
        this.onClickToOpenModal = this.onClickToOpenModal.bind(this);
        this.onClickToCloseDeleteModal = this.onClickToCloseDeleteModal.bind(this);
        this.openModalForDeleteItsDataAuthorization = this.openModalForDeleteItsDataAuthorization.bind(this);
        this.onClickDeleteItsDataAuthorization = this.onClickDeleteItsDataAuthorization.bind(this);

        this.onProcessActionStateChanged = this.onProcessActionStateChanged.bind(this);
        this.onProcessCreateItsDataAuthorization = this.onProcessCreateItsDataAuthorization.bind(this);
        this.onProcessDeleteItsDataAuthorization = this.onProcessDeleteItsDataAuthorization.bind(this);
        this.onProcessEditItsDataAuthorization = this.onProcessEditItsDataAuthorization.bind(this);
        this.notificationInSuccess = this.notificationInSuccess.bind(this);
        this.notificationInError = this.notificationInError.bind(this);
    }

    public componentDidMount() {
        const { hasItsDataAuthorizationsReceived } = this.props;

        if (!hasItsDataAuthorizationsReceived && this._commonTableRef.current) {
            this._commonTableRef.current.ReSelectData();
        }
    }

    private onProcessActionStateChanged(processId: string, actionState: IReducerProcessActionInfo) {
        this.onProcessCreateItsDataAuthorization(processId, actionState);
        this.onProcessDeleteItsDataAuthorization(processId, actionState);
        this.onProcessEditItsDataAuthorization(processId, actionState);
    }

    private onProcessCreateItsDataAuthorization(processId: string, actionState: IReducerProcessActionInfo) {
        if (processId !== ItsAuthorizationDataProcessId.CreateItsDataAuthorization) return;
        this.notificationInSuccess(actionState, 'Доступ к ИТС успешно создана');
        this.notificationInError(actionState);
    }

    private onProcessDeleteItsDataAuthorization(processId: string, actionState: IReducerProcessActionInfo) {
        if (processId !== ItsAuthorizationDataProcessId.DeleteItsDataAuthorization) return;
        this.notificationInSuccess(actionState, 'Доступ к ИТС успешно удалена');
        this.notificationInError(actionState);
    }

    private onProcessEditItsDataAuthorization(processId: string, actionState: IReducerProcessActionInfo) {
        if (processId !== ItsAuthorizationDataProcessId.EditItsDataAuthorization) return;
        this.notificationInSuccess(actionState, 'Доступ к ИТС успешно сохранена');
        this.notificationInError(actionState);
    }

    private onClickToOpenModal(itsAuthorizationDataId?: string) {
        this.setState({
            isOpen: true,
            itsAuthorizationDataId
        });
    }

    private onClickToCloseModal() {
        this.setState({
            isOpen: false
        });
    }

    private onClickToCloseDeleteModal() {
        this.setState({
            deleteIsOpen: false
        });
    }

    private onClickToSearch() {
        const {
            login
        } = this._searchItsDataAuthorizationsRef.current?.state!;

        this.props.dispatchGetItsDataAuthorizationsThunk({
            filter: { searchString: login ?? '' },
            pageNumber: 1,
            force: true
        });
    }

    private onClickDeleteItsDataAuthorization() {
        if (this.state.itsAuthorizationDataId) {
            this.props.dispatchDeleteItsDataAuthorizationThunk({
                itsAuthorizationId: this.state.itsAuthorizationDataId,
                force: true
            });
        }

        this.setState({
            deleteIsOpen: false
        });
    }

    private onDataSelect(args: SelectDataCommonDataModel<undefined>) {
        const {
            login
        } = this._searchItsDataAuthorizationsRef.current?.state!;
        this.props.dispatchGetItsDataAuthorizationsThunk({
            filter: { searchString: login ?? '' },
            pageNumber: args.pageNumber,
            orderBy: args.sortingData ? this.getSortQuery(args.sortingData) : 'login.desc',
            force: true
        });
    }

    private getFormatLoginItsDataAuthorization(name: string, data: CommonSegmentDataModel) {
        return (
            <TextLinkButton onClick={ () => { this.onClickToOpenModal(data.id); } }>
                <TextOut fontSize={ 13 } className="text-link">
                    { name }
                </TextOut>
            </TextLinkButton>
        );
    }

    private getButtonsForActions(Id: string) {
        return (
            <TextLinkButton
                className={ css['delete-link'] }
                onClick={ () => { this.openModalForDeleteItsDataAuthorization(Id); } }
            >
                <i className="fa fa-trash" />
            </TextLinkButton>
        );
    }

    private getSortQuery(args: SortingDataModel) {
        switch (args.fieldName) {
            case 'login':
                return `login.${ args.sortKind ? 'desc' : 'asc' }`;
            default:
                break;
        }
    }

    private openModalForDeleteItsDataAuthorization(itsAuthorizationDataId: string) {
        this.setState({
            deleteIsOpen: true,
            itsAuthorizationDataId
        });
    }

    private notificationInError(actionState: IReducerProcessActionInfo) {
        if (!actionState.isInErrorState) return;
        this.props.floatMessage.show(MessageType.Error, 'Ошибка добавления доступа к ИТС');
    }

    private notificationInSuccess(actionState: IReducerProcessActionInfo, message: string) {
        if (!actionState.isInSuccessState) return;
        this.props.floatMessage.show(MessageType.Success, message);
        this._commonTableRef.current?.ReSelectData();
        this.onClickToCloseModal();
    }

    public render() {
        const { itsDataAuthorizations: { metadata, records } } = this.props;

        return (
            <>
                <ItsDataAuthorizationSearchView
                    ref={ this._searchItsDataAuthorizationsRef }
                    onClickToSearch={ this.onClickToSearch }
                />
                <Box display="flex" flexDirection="column">
                    <div style={ { margin: '16px 0' } }>
                        <SuccessButton
                            onClick={ () => { this.onClickToOpenModal(); } }
                        >
                            <i className="fa fa-plus-circle mr-1" />
                            Добавить доступ к ИТС
                        </SuccessButton>
                    </div>
                    <CommonTableWithFilter
                        ref={ this._commonTableRef }
                        uniqueContextProviderStateId="ItsDataAuthorizationsContextProviderStateId"
                        tableProps={ {
                            dataset: records,
                            keyFieldName: 'id',
                            fieldsView: {
                                login: {
                                    caption: 'Логин',
                                    fieldWidth: 'auto',
                                    fieldContentNoWrap: false,
                                    isSortable: true,
                                    format: this.getFormatLoginItsDataAuthorization
                                },
                                passwordHash: {
                                    caption: 'Хеш пароля',
                                    fieldWidth: 'auto',
                                    fieldContentNoWrap: false,
                                    isSortable: false
                                },
                                id: {
                                    caption: '',
                                    fieldWidth: '20px',
                                    format: this.getButtonsForActions
                                }
                            },
                            pagination: {
                                currentPage: metadata.pageNumber,
                                pageSize: metadata.pageSize,
                                recordsCount: metadata.totalItemCount,
                                totalPages: metadata.pageCount
                            },
                        } }
                        onDataSelect={ this.onDataSelect }
                    />
                </Box>
                {
                    this.state.isOpen
                        ? (
                            <ItsDataAuthorizationsModalView
                                isOpen={ this.state.isOpen }
                                itsAuthorizationDataId={ this.state.itsAuthorizationDataId }
                                onClickToCloseModal={ this.onClickToCloseModal }
                            />
                        )
                        : null
                }
                <WatchReducerProcessActionState
                    watch={ [{
                        reducerStateName: 'ItsAuthorizationDataState',
                        processIds: [
                            ItsAuthorizationDataProcessId.CreateItsDataAuthorization,
                            ItsAuthorizationDataProcessId.DeleteItsDataAuthorization,
                            ItsAuthorizationDataProcessId.EditItsDataAuthorization,
                        ]
                    }] }
                    onProcessActionStateChanged={ this.onProcessActionStateChanged }
                />
                <Dialog
                    isOpen={ this.state.deleteIsOpen }
                    dialogWidth="sm"
                    dialogVerticalAlign="center"
                    onCancelClick={ this.onClickToCloseDeleteModal }
                    buttons={
                        [
                            {
                                content: 'Нет',
                                kind: 'primary',
                                variant: 'text',
                                onClick: this.onClickToCloseDeleteModal
                            },
                            {
                                content: 'Да',
                                kind: 'error',
                                variant: 'contained',
                                onClick: this.onClickDeleteItsDataAuthorization
                            }
                        ]
                    }
                >
                    <Box display="flex" flexDirection="column" alignItems="center" justifyContent="center">
                        <ErrorOutlineIcon color="warning" sx={ { width: '5em', height: '5em' } } />
                        Вы действительно хотите удалить доступ к ИТС?
                    </Box>
                </Dialog>
            </>
        );
    }
}

const ItsDataAuthorizationConnected = connect<StateProps, DispatchProps, {}, AppReduxStoreState>(
    state => {
        const itsAuthorizationDataState = state.ItsAuthorizationDataState;
        const { itsDataAuthorizations } = itsAuthorizationDataState;
        const { hasItsDataAuthorizationsReceived } = itsAuthorizationDataState.hasSuccessFor;

        return {
            itsDataAuthorizations,
            hasItsDataAuthorizationsReceived
        };
    },
    {
        dispatchGetItsDataAuthorizationsThunk: GetItsDataAuthorizationsThunk.invoke,
        dispatchDeleteItsDataAuthorizationThunk: DeleteItsDataAuthorizationThunk.invoke
    }
)(ItsDataAuthorizationClass);

export const ItsDataAuthorizationView = withHeader({
    title: 'Доступ к ИТС',
    page: withFloatMessages(ItsDataAuthorizationConnected)
});