import { TextBoxForm } from 'app/views/components/controls/forms';
import React from 'react';

type OwnProps = {
    label: string;
    autoFocus?: boolean;
    formName: string;
    value: string;
    isInEditMode: boolean;
    onValueChange: <TValue>(fieldName: string, newValue?: TValue, prevValue?: TValue) => boolean | void;
};

export const DbTemplateDelimiterConfigurationReleaseVersion = (props: OwnProps) => {
    return props.isInEditMode
        ? (
            <TextBoxForm
                autoFocus={ props.autoFocus }
                formName={ props.formName }
                label={ props.label }
                value={ props.value }
                onValueChange={ props.onValueChange }
            />
        )
        : (
            <TextBoxForm
                label={ props.label }
                value={ props.value }
                formName={ props.formName }
                isReadOnly={ true }
            />
        );
};