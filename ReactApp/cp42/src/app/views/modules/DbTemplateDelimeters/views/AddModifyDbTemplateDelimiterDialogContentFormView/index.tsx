import { hasComponentChangesFor } from 'app/common/functions';
import { TextBoxForm } from 'app/views/components/controls/forms';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import {
    DbTemplateDelimiterConfigurationId
} from 'app/views/modules/DbTemplateDelimeters/views/AddModifyDbTemplateDelimiterDialogContentFormView/partial-elements/DbTemplateDelimiterConfigurationId';
import {
    DbTemplateDelimiterConfigurationReleaseVersion
} from 'app/views/modules/DbTemplateDelimeters/views/AddModifyDbTemplateDelimiterDialogContentFormView/partial-elements/DbTemplateDelimiterConfigurationReleaseVersion';
import {
    DbTemplateDelimiterMinReleaseVersion
} from 'app/views/modules/DbTemplateDelimeters/views/AddModifyDbTemplateDelimiterDialogContentFormView/partial-elements/DbTemplateDelimiterMinReleaseVersion';
import {
    DbTemplateDelimiterTemplateId
} from 'app/views/modules/DbTemplateDelimeters/views/AddModifyDbTemplateDelimiterDialogContentFormView/partial-elements/DbTemplateDelimiterTemplateId';
import {
    DbTemplateDelimeterItemDataModel
} from 'app/web/InterlayerApiProxy/DbTemplateDelimetersApiProxy/receiveDbTemplateDelimeters';
import React from 'react';
import { Box } from '@mui/material';

type OwnProps = ReduxFormProps<DbTemplateDelimeterItemDataModel> & {
    itemToEdit: DbTemplateDelimeterItemDataModel;
    isNewItem: boolean;
};

class AddModifyDbTemplateDelimiterDialogContentFormViewClass extends React.Component<OwnProps> {
    public constructor(props: OwnProps) {
        super(props);
        this.onValueChange = this.onValueChange.bind(this);
        this.initReduxForm = this.initReduxForm.bind(this);

        this.props.reduxForm.setInitializeFormDataAction(this.initReduxForm);
    }

    public shouldComponentUpdate(nextProps: OwnProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private onValueChange<TValue>(formName: string, newValue: TValue) {
        this.props.reduxForm.updateReduxFormFields({
            [formName]: newValue
        });
    }

    private initReduxForm(): DbTemplateDelimeterItemDataModel | undefined {
        if (!this.props.itemToEdit) {
            return;
        }

        return {
            configurationId: this.props.itemToEdit.configurationId,
            templateId: this.props.itemToEdit.templateId,
            configurationReleaseVersion: this.props.itemToEdit.configurationReleaseVersion,
            demoDatabaseOnDelimitersPublicationAddress: this.props.itemToEdit.demoDatabaseOnDelimitersPublicationAddress,
            minReleaseVersion: this.props.itemToEdit.minReleaseVersion,
            shortName: this.props.itemToEdit.shortName,
            name: this.props.itemToEdit.name
        };
    }

    public render() {
        if (!this.props.itemToEdit) {
            return null;
        }

        const reduxForm = this.props.reduxForm.getReduxFormFields(false);

        return (
            <Box display="flex" flexDirection="column" gap="16px">
                <DbTemplateDelimiterConfigurationId
                    formName="configurationId"
                    label="Код конфигурации"
                    value={ reduxForm.configurationId }
                    autoFocus={ this.props.isNewItem }
                    isInEditMode={ this.props.isNewItem }
                    onValueChange={ this.onValueChange }
                />
                <TextBoxForm
                    formName="name"
                    autoFocus={ !this.props.isNewItem }
                    label="Название конфигурации"
                    value={ reduxForm.name }
                    onValueChange={ this.onValueChange }
                />
                <DbTemplateDelimiterTemplateId
                    label="Имя шаблона"
                    formName="templateId"
                    value={ reduxForm.templateId }
                    onValueChange={ this.onValueChange }
                />
                <TextBoxForm
                    formName="shortName"
                    label="Короткое имя конфигурации(для загрузки zip)"
                    value={ reduxForm.shortName }
                    onValueChange={ this.onValueChange }
                />
                <TextBoxForm
                    formName="demoDatabaseOnDelimitersPublicationAddress"
                    label="Демо публикация"
                    value={ reduxForm.demoDatabaseOnDelimitersPublicationAddress }
                    onValueChange={ this.onValueChange }
                />
                <DbTemplateDelimiterConfigurationReleaseVersion
                    formName="configurationReleaseVersion"
                    label="Текущая версия релиза конфигурации"
                    value={ reduxForm.configurationReleaseVersion }
                    isInEditMode={ this.props.isNewItem }
                    onValueChange={ this.onValueChange }
                />
                <DbTemplateDelimiterMinReleaseVersion
                    formName="minReleaseVersion"
                    label="Минимальная версия релиза конфигурации"
                    value={ reduxForm.minReleaseVersion }
                    isInEditMode={ this.props.isNewItem }
                    onValueChange={ this.onValueChange }
                />
            </Box>
        );
    }
}

export const AddModifyDbTemplateDelimiterDialogContentFormView = withReduxForm(AddModifyDbTemplateDelimiterDialogContentFormViewClass, {
    reduxFormName: ' AddModifyDbTemplateDelimiter_ReduxForm',
    resetOnUnmount: true
});