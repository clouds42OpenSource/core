import { hasComponentChangesFor } from 'app/common/functions';
import { ComboBoxForm } from 'app/views/components/controls/forms';
import { KeyValueDataModel } from 'app/web/common/data-models';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { RequestKind } from 'core/requestSender/enums/RequestKindEnum';
import React from 'react';

type OwnProps = {
    label: string;
    formName: string;
    value: string;
    onValueChange: <TValue>(fieldName: string, newValue?: TValue, prevValue?: TValue) => boolean | void;
};

type OwnState = {
    dbTemplates: KeyValueDataModel<string, string>[];
};

export class DbTemplateDelimiterTemplateId extends React.Component<OwnProps, OwnState> {
    public constructor(props: OwnProps) {
        super(props);

        this.state = {
            dbTemplates: []
        };
    }

    public async componentDidMount() {
        const api = InterlayerApiProxy.getDbTemplateApiProxy();

        try {
            const templates = await api.receiveAllDbTemplatesAsKeyValue(RequestKind.SEND_BY_ROBOT);
            this.setState({
                dbTemplates: [{ key: '', value: '' }, ...templates]
            });
        } catch (ex) {
            console.log(ex);
        }
    }

    public shouldComponentUpdate(nextProps: OwnProps, nextState: OwnState) {
        return hasComponentChangesFor(this.props, nextProps) ||
            hasComponentChangesFor(this.state, nextState);
    }

    public render() {
        if (!this.state.dbTemplates) {
            return null;
        }

        return (
            <ComboBoxForm
                formName={ this.props.formName }
                label={ this.props.label }
                value={ this.props.value }
                items={ this.state.dbTemplates.map(item => {
                    return {
                        value: item.key,
                        text: item.value
                    };
                }) }
                onValueChange={ this.props.onValueChange }
                allowAutoComplete={ true }
            />
        );
    }
}