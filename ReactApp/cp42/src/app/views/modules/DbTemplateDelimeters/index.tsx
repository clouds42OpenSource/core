import { SortingKind } from 'app/common/enums';
import { hasComponentChangesFor } from 'app/common/functions';
import { DbTemplateDelimetersProcessId } from 'app/modules/dbTemplateDelimeters/DbTemplateDelimetersProcessId';
import {
    AddDbTemplateDelimeterThunkParams
} from 'app/modules/dbTemplateDelimeters/reducers/addDbTemplateDelimeterReducer/params';
import {
    DeleteDbTemplateDelimeterThunkParams
} from 'app/modules/dbTemplateDelimeters/reducers/deleteDbTemplateDelimeterReducer/params';
import {
    ReceiveDbTemplateDelimetersThunkParams
} from 'app/modules/dbTemplateDelimeters/reducers/receiveDbTemplateDelimetersReducer/params';
import {
    UpdateDbTemplateDelimeterThunkParams
} from 'app/modules/dbTemplateDelimeters/reducers/updateDbTemplateDelimeterReducer/params';
import {
    AddDbTemplateDelimeterThunk,
    DeleteDbTemplateDelimeterThunk,
    ReceiveDbTemplateDelimetersThunk,
    UpdateDbTemplateDelimeterThunk
} from 'app/modules/dbTemplateDelimeters/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { TextOut } from 'app/views/components/TextOut';
import { withHeader } from 'app/views/components/_hoc/withHeader';
import {
    AddModifyDbTemplateDelimiterDialogContentFormView
} from 'app/views/modules/DbTemplateDelimeters/views/AddModifyDbTemplateDelimiterDialogContentFormView';
import { DbTemplatesFilterDataForm } from 'app/views/modules/DbTemplates/types';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import {
    DbTemplateDelimeterItemDataModel
} from 'app/web/InterlayerApiProxy/DbTemplateDelimetersApiProxy/receiveDbTemplateDelimeters';
import { SelectDataCommonDataModel } from 'app/web/common/data-models';
import React from 'react';
import { connect } from 'react-redux';

type StateProps<TDataRow = DbTemplateDelimeterItemDataModel> = {
    dataset: TDataRow[];
};

type DispatchProps = {
    dispatchReceiveDbTemplateDelimetersThunk: (args: ReceiveDbTemplateDelimetersThunkParams) => void;
    dispatchUpdateDbTemplateDelimeterThunk: (args: UpdateDbTemplateDelimeterThunkParams) => void;
    dispatchAddDbTemplateDelimeterThunk: (args: AddDbTemplateDelimeterThunkParams) => void;
    dispatchDeleteDbTemplateDelimeterThunk: (args: DeleteDbTemplateDelimeterThunkParams) => void;
};

type AllProps = StateProps & DispatchProps;

class DbTemplateDelimetersClass extends React.Component<AllProps> {
    public constructor(props: AllProps) {
        super(props);
        this.onDataSelect = this.onDataSelect.bind(this);
    }

    public shouldComponentUpdate(nextProps: AllProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private onDataSelect(args: SelectDataCommonDataModel<DbTemplatesFilterDataForm>) {
        this.props.dispatchReceiveDbTemplateDelimetersThunk({
            pageNumber: args.pageNumber,
            force: true
        });
    }

    public render() {
        return (
            <>
                <div>
                    <TextOut fontSize={ 13 } fontWeight={ 400 }>
                        На этой странице вы можете указывать какая база является Базой на разделителях.
                    </TextOut>
                </div>
                <div style={ {
                    marginTop: '10px'
                } }
                >
                    <TextOut fontSize={ 12 } fontWeight={ 600 }>
                        Управление Базами на разделителях
                    </TextOut>
                </div>
                <br />
                <CommonTableWithFilter
                    uniqueContextProviderStateId="DbTemplateDelimetersContextProviderStateId"
                    tableEditProps={ {
                        processProps: {
                            watch: {
                                reducerStateName: 'DbTemplateDelimetersState',
                                processIds: {
                                    addNewItemProcessId: DbTemplateDelimetersProcessId.AddDbTemplateDelimeter,
                                    modifyItemProcessId: DbTemplateDelimetersProcessId.UpdateDbTemplateDelimeter,
                                    deleteItemProcessId: DbTemplateDelimetersProcessId.DeleteDbTemplateDelimeter
                                }
                            },
                            processMessages: {
                                getOnSuccessAddNewItemMessage: () => 'Добавление новой базы на разделителях успешно завершено.',
                                getOnSuccessModifyItemMessage: item => `Редактирование базы на разделителях "${ item.configurationId }" успешно завершено.`,
                                getOnSuccessDeleteItemMessage: item => `Удаление базы на разделителях "${ item.configurationId }" успешно завершено.`
                            }
                        },
                        dialogProps: {
                            deleteDialog: {
                                getDialogTitle: () => 'Удаление базы на разделителях',
                                getDialogContentView: itemToDelete => `Вы действительно хотите удалить базу на разделителях"${ itemToDelete.configurationId }"?`,
                                confirmDeleteItemButtonText: 'Удалить',
                                onDeleteItemConfirmed: itemToDelete => {
                                    this.props.dispatchDeleteDbTemplateDelimeterThunk({
                                        configurationId: itemToDelete.configurationId,
                                        force: true
                                    });
                                }
                            },
                            addDialog: {
                                dialogWidth: 'sm',
                                getDialogTitle: () => 'Добавление новой базы на разделителях',
                                getDialogContentFormView: ref =>
                                    <AddModifyDbTemplateDelimiterDialogContentFormView
                                        ref={ ref }
                                        itemToEdit={
                                            {
                                                configurationId: '',
                                                templateId: '',
                                                configurationReleaseVersion: '',
                                                demoDatabaseOnDelimitersPublicationAddress: '',
                                                minReleaseVersion: '',
                                                shortName: '',
                                                name: ''
                                            }
                                        }
                                        isNewItem={ true }
                                    />,
                                confirmAddItemButtonText: 'Добавить',
                                showAddDialogButtonText: 'Добавить базу на разделителях',
                                onAddNewItemConfirmed: itemToAdd => {
                                    this.props.dispatchAddDbTemplateDelimeterThunk({
                                        ...itemToAdd,
                                        force: true
                                    });
                                }
                            },
                            modifyDialog: {
                                getDialogTitle: () => 'Редактирование базы на разделителях',
                                dialogWidth: 'sm',
                                confirmModifyItemButtonText: 'Сохранить',
                                getDialogContentFormView: (ref, item) =>
                                    <AddModifyDbTemplateDelimiterDialogContentFormView ref={ ref } itemToEdit={ item } isNewItem={ false } />,
                                onModifyItemConfirmed: itemToModify => {
                                    this.props.dispatchUpdateDbTemplateDelimeterThunk({
                                        ...itemToModify,
                                        force: true
                                    });
                                }
                            }
                        }
                    } }
                    tableProps={ {
                        dataset: this.props.dataset,
                        keyFieldName: 'configurationId',
                        sorting: {
                            sortFieldName: 'configurationId',
                            sortKind: SortingKind.Asc
                        },
                        fieldsView: {
                            configurationId: {
                                caption: 'Код конфигурации',
                                fieldContentNoWrap: false,
                                isSortable: false,
                                fieldWidth: 'auto'
                            },
                            name: {
                                caption: 'Название конфигурации',
                                fieldContentNoWrap: false,
                                isSortable: false,
                                fieldWidth: 'auto'
                            },
                            demoDatabaseOnDelimitersPublicationAddress: {
                                caption: 'Имя сайта публикации',
                                fieldContentNoWrap: false,
                                isSortable: false,
                                fieldWidth: 'auto'
                            }
                        }
                    } }
                    onDataSelect={ this.onDataSelect }
                />
            </>
        );
    }
}

const DbTemplateDelimetersConnected = connect<StateProps, DispatchProps, {}, AppReduxStoreState>(
    state => {
        const dbTemplateDelimetersState = state.DbTemplateDelimetersState;
        const { dbTemplateDelimeters } = dbTemplateDelimetersState;
        const dataset = dbTemplateDelimeters.records;

        return {
            dataset,
        };
    },
    {
        dispatchReceiveDbTemplateDelimetersThunk: ReceiveDbTemplateDelimetersThunk.invoke,
        dispatchUpdateDbTemplateDelimeterThunk: UpdateDbTemplateDelimeterThunk.invoke,
        dispatchAddDbTemplateDelimeterThunk: AddDbTemplateDelimeterThunk.invoke,
        dispatchDeleteDbTemplateDelimeterThunk: DeleteDbTemplateDelimeterThunk.invoke
    }
)(DbTemplateDelimetersClass);

export const DbTemplateDelimetersView = withHeader({
    title: 'Страница списка баз на разделителях',
    page: DbTemplateDelimetersConnected
});