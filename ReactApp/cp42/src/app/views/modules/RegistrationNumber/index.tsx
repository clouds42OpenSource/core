import { Autocomplete, Box, TextField } from '@mui/material';
import { ELogEvent } from 'app/api/endpoints/global/enum';
import { FETCH_API } from 'app/api/useFetchApi';
import { EMessageType, useAppSelector, useDebounce, useFloatMessages } from 'app/hooks';
import { withHeader } from 'app/views/components/_hoc/withHeader';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { BoxWithPicture } from 'app/views/modules/_common/reusable-modules/BoxWithPicture';
import { CONFIGURATIONS, ItsHelpAdornment } from 'app/views/modules/_common/reusable-modules/BoxWithPicture/constants';
import { registrationNumberInitialValues, registrationNumberSchema } from 'app/views/modules/_common/reusable-modules/BoxWithPicture/schemas';
import { allowedCountries } from 'app/views/modules/ToPartners/config/constants';
import dayjs from 'dayjs';
import { useFormik } from 'formik';
import { MuiTelInput } from 'mui-tel-input';
import React, { useEffect, useState } from 'react';

const { getLegalEntityDetails } = FETCH_API.PARTNERS;
const { sendEmailBySupportPage } = FETCH_API.EMAILS;
const { sendLogEvent } = FETCH_API.GLOBAL;

const RegistrationNumber = () => {
    const { show } = useFloatMessages();

    const { contextAccountIndex } = useAppSelector(state => state.Global.getCurrentSessionSettingsReducer.settings.currentContextInfo);
    const { data } = useAppSelector(state => state.MainPageState.mainPageReducer);
    const { email, login } = data ?? {};

    const [isLoading, setIsLoading] = useState(false);

    const { values, handleChange, handleBlur, touched, errors, setFieldValue, handleSubmit, setFieldTouched, resetForm } = useFormik({
        validationSchema: registrationNumberSchema,
        initialValues: registrationNumberInitialValues,
        onSubmit: async value => {
            setIsLoading(true);

            const { success, message } = await sendEmailBySupportPage(
                `
                    <p>Логин: ${ login }</p>
                    <p>Конфигурация 1С: ${ value.configuration }</p>
                    <p>Организация: ${ value.organization }</p>
                    <p>ИНН: ${ value.inn }</p>
                    <p>КПП: ${ value.kpp }</p>
                    <p>Вид деятельности компании: ${ value.companyActivities }</p>
                    <p>ФИО руководителя: ${ value.supervisor }</p>
                    <p>Почтовый адрес клиента: ${ value.postalAddress }</p>
                    <p>Телефон: ${ value.phone }</p>
                    <p>Почта: ${ value.email }</p>
                    <p>Логин на ИТС: ${ value.itsLogin }</p>
                    <p>Почта на ИТС: ${ value.itsEmail }</p>
                `,
                `Заказ регистрационного номера от ${ contextAccountIndex } ${ dayjs().format('DD.MM.YYYY HH:mm:ss') }`,
                email
            );

            if (success) {
                show(EMessageType.success, 'Спасибо за Вашу заявку! Мы обработаем ее в ближайшее время.');

                resetForm();
                await setFieldValue('configuration', '');
                await setFieldValue('phone', '');

                await sendLogEvent(ELogEvent.requestSent, 'Отправлена заявка на выпуск рег.номера');
            } else {
                show(EMessageType.error, message);
            }

            setIsLoading(false);
        }
    });

    const debounceInn = useDebounce(values.inn, 500);

    useEffect(() => {
        (async () => {
            if (!errors.inn && debounceInn) {
                const { data: legalEntityDetails, error } = await getLegalEntityDetails({ inn: debounceInn });

                if (!error && legalEntityDetails && legalEntityDetails.rawData && legalEntityDetails.rawData.success) {
                    await setFieldValue('kpp', legalEntityDetails.rawData.kpp);
                    await setFieldValue('organization', legalEntityDetails.rawData.companyName);
                    await setFieldValue('supervisor', legalEntityDetails.rawData.headFullName);
                    await setFieldValue('postalAddress', legalEntityDetails.rawData.address);
                }
            }
        })();
    }, [debounceInn, errors.inn, setFieldValue]);

    return (
        <BoxWithPicture handleSubmit={ handleSubmit } isLoading={ isLoading }>
            <Box display="grid" gridTemplateColumns="1fr 1fr" columnGap={ 2 }>
                <FormAndLabel label="Конфигурация 1С" fullWidth={ true }>
                    <Autocomplete
                        noOptionsText="Конфигурация не найдена"
                        fullWidth={ true }
                        renderInput={ params => <TextField
                            { ...params }
                            name="configuration"
                            error={ touched.configuration && !!errors.configuration }
                            helperText={ touched.configuration && errors.configuration }
                        /> }
                        value={ values.configuration }
                        onBlur={ handleBlur }
                        options={ CONFIGURATIONS }
                        onChange={ (_, value) => setFieldValue('configuration', value) }
                        size="small"
                    />
                </FormAndLabel>
                <FormAndLabel label="Почтовый адрес" fullWidth={ true }>
                    <TextField
                        name="postalAddress"
                        value={ values.postalAddress }
                        onChange={ handleChange }
                        onBlur={ handleBlur }
                        error={ touched.postalAddress && !!errors.postalAddress }
                        helperText={ touched.postalAddress && errors.postalAddress }
                        fullWidth={ true }
                    />
                </FormAndLabel>
                <FormAndLabel label="ИНН" fullWidth={ true }>
                    <TextField
                        name="inn"
                        value={ values.inn }
                        onChange={ handleChange }
                        onBlur={ handleBlur }
                        error={ touched.inn && !!errors.inn }
                        helperText={ touched.inn && errors.inn }
                        fullWidth={ true }
                    />
                </FormAndLabel>
                <FormAndLabel label="Номер телефона" fullWidth={ true }>
                    <MuiTelInput
                        fullWidth={ true }
                        name="phone"
                        autoComplete="off"
                        onChange={ e => setFieldValue('phone', e) }
                        onBlur={ () => setFieldTouched('phone', true, true) }
                        value={ values.phone }
                        preferredCountries={ allowedCountries }
                        defaultCountry="RU"
                        langOfCountryName="RU"
                        error={ touched.phone && !!errors.phone }
                        helperText={ touched.phone && errors.phone }
                    />
                </FormAndLabel>
                <FormAndLabel label="КПП" fullWidth={ true }>
                    <TextField
                        name="kpp"
                        value={ values.kpp }
                        onChange={ handleChange }
                        onBlur={ handleBlur }
                        error={ touched.kpp && !!errors.kpp }
                        helperText={ touched.kpp && errors.kpp }
                        fullWidth={ true }
                    />
                </FormAndLabel>
                <FormAndLabel label="Электронная почта" fullWidth={ true }>
                    <TextField
                        name="email"
                        value={ values.email }
                        onChange={ handleChange }
                        onBlur={ handleBlur }
                        error={ touched.email && !!errors.email }
                        helperText={ touched.email && errors.email }
                        fullWidth={ true }
                    />
                </FormAndLabel>
                <FormAndLabel label="Название организации" fullWidth={ true }>
                    <TextField
                        name="organization"
                        value={ values.organization }
                        onChange={ handleChange }
                        onBlur={ handleBlur }
                        error={ touched.organization && !!errors.organization }
                        helperText={ touched.organization && errors.organization }
                        fullWidth={ true }
                    />
                </FormAndLabel>
                <FormAndLabel label="Логин на портале ИТС" fullWidth={ true }>
                    <TextField
                        name="itsLogin"
                        value={ values.itsLogin }
                        onChange={ handleChange }
                        onBlur={ handleBlur }
                        error={ touched.itsLogin && !!errors.itsLogin }
                        helperText={ touched.itsLogin && errors.itsLogin }
                        fullWidth={ true }
                        InputProps={ { endAdornment: <ItsHelpAdornment /> } }
                    />
                </FormAndLabel>
                <FormAndLabel label="Вид деятельности компании" fullWidth={ true }>
                    <TextField
                        name="companyActivities"
                        value={ values.companyActivities }
                        onChange={ handleChange }
                        onBlur={ handleBlur }
                        error={ touched.companyActivities && !!errors.companyActivities }
                        helperText={ touched.companyActivities && errors.companyActivities }
                        fullWidth={ true }
                    />
                </FormAndLabel>
                <FormAndLabel label="Электронная почта используемая на ИТС" fullWidth={ true }>
                    <TextField
                        name="itsEmail"
                        value={ values.itsEmail }
                        onChange={ handleChange }
                        onBlur={ handleBlur }
                        error={ touched.itsEmail && !!errors.itsEmail }
                        helperText={ touched.itsEmail && errors.itsEmail }
                        fullWidth={ true }
                    />
                </FormAndLabel>
                <FormAndLabel label="ФИО руководителя" fullWidth={ true }>
                    <TextField
                        name="supervisor"
                        value={ values.supervisor }
                        onChange={ handleChange }
                        onBlur={ handleBlur }
                        error={ touched.supervisor && !!errors.supervisor }
                        helperText={ touched.supervisor && errors.supervisor }
                        fullWidth={ true }
                    />
                </FormAndLabel>
            </Box>
        </BoxWithPicture>
    );
};

export const RegistrationNumberView = withHeader({
    title: 'Регистрационный номер',
    page: RegistrationNumber
});