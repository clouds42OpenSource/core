import { AccountUserGroup } from 'app/common/enums';
import { useAppSelector } from 'app/hooks';
import { useHistory } from 'react-router';

import { TActiveTabHistory } from 'app/common/types/app-types';
import { withHeader } from 'app/views/components/_hoc/withHeader';
import { TabsControl } from 'app/views/components/controls/TabsControl';
import { THeaders } from 'app/views/components/controls/TabsControl/views/TabView';
import { Clients, Recalculation, Requisites, Services, Transactions, Withdraw } from 'app/views/modules/Partners/views';
import { ServiceModeration } from 'app/views/modules/Services/views/EditServiceCard/ServiceModeration';
import { ArticlesControl } from 'app/views/modules/Partners/views/articlesControl';

const AdministratePartnersComponent = () => {
    const history = useHistory<TActiveTabHistory>();
    const { currentUserGroups } = useAppSelector(state => state.Global.getCurrentSessionSettingsReducer.settings.currentContextInfo);

    const isDefaultTabsVisible = currentUserGroups.includes(AccountUserGroup.Hotline) ||
        currentUserGroups.includes(AccountUserGroup.CloudAdmin) ||
        currentUserGroups.includes(AccountUserGroup.AccountSaleManager);

    const HEADERS: THeaders = [
        {
            title: 'Клиенты',
            isVisible: isDefaultTabsVisible,
        },
        {
            title: 'Сервисы',
            isVisible: isDefaultTabsVisible,
        },
        {
            title: 'Транзакции',
            isVisible: isDefaultTabsVisible,
        },
        {
            title: 'Реквизиты',
            isVisible: isDefaultTabsVisible || currentUserGroups.includes(AccountUserGroup.ArticleEditor),
        },
        {
            title: 'Заявки на модерацию',
            isVisible: isDefaultTabsVisible
        },
        {
            title: 'Вывод',
            isVisible: isDefaultTabsVisible || currentUserGroups.includes(AccountUserGroup.ArticleEditor)
        },
        {
            title: 'АРМ пересчета стоимости сервисов',
            isVisible: currentUserGroups.includes(AccountUserGroup.Hotline) || currentUserGroups.includes(AccountUserGroup.CloudAdmin)
        },
        {
            title: 'Статьи',
            isVisible: currentUserGroups.includes(AccountUserGroup.ArticleEditor) || currentUserGroups.includes(AccountUserGroup.CloudAdmin),
        }
    ];

    return (
        <TabsControl headers={ HEADERS } activeTabIndex={ history.location.state?.activeTabIndex }>
            { isDefaultTabsVisible && <Clients /> }
            { isDefaultTabsVisible && <Services /> }
            { isDefaultTabsVisible && <Transactions /> }
            { (isDefaultTabsVisible || currentUserGroups.includes(AccountUserGroup.ArticleEditor)) && <Requisites /> }
            { isDefaultTabsVisible && <ServiceModeration /> }
            { (isDefaultTabsVisible || currentUserGroups.includes(AccountUserGroup.ArticleEditor)) && <Withdraw /> }
            { (currentUserGroups.includes(AccountUserGroup.Hotline) || currentUserGroups.includes(AccountUserGroup.CloudAdmin)) && <Recalculation /> }
            <ArticlesControl />
        </TabsControl>
    );
};

export const AdministratePartners = withHeader({
    title: 'Партнёры',
    page: AdministratePartnersComponent
});