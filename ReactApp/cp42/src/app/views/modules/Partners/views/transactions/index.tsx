import React, { useEffect, useState } from 'react';
import { OutlinedInput } from '@mui/material';

import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { useAppDispatch, useAppSelector } from 'app/hooks';
import { dateFilterFormat, useGetTableFilters } from 'app/views/modules/Partners/helpers';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { REDUX_API } from 'app/api/useReduxApi';
import { SelectDataCommonDataModel } from 'app/web/common';
import { TDatePicker } from 'app/common/types/app-types';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { DoubleDateInputForm } from 'app/views/components/controls/forms/DoubleDateInputView';
import { SuccessButton } from 'app/views/components/controls/Button';
import { TransactionsTable } from 'app/views/modules/Partners/modules';
import { TAgentPaymentListRequest } from 'app/api/endpoints/partners/request';

import globalStyles from '../style.module.css';

const { getAgentPayments } = REDUX_API.PARTNERS_REDUX;

export const Transactions = withFloatMessages(({ floatMessage: { show } }) => {
    const dispatch = useAppDispatch();
    const { error, isLoading } = useAppSelector(state => state.PartnersState.agentPaymentListReducer);

    const [partnerName, setPartnerName] = useState('');
    const [clientName, setClientName] = useState('');
    const [operationName, setOperationName] = useState('');
    const [period, setPeriod] = useState<TDatePicker>({ from: null, to: null });

    useEffect(() => {
        getAgentPayments({
            pageNumber: 1
        }, dispatch);
    }, [dispatch]);

    useEffect(() => {
        if (error) {
            show(MessageType.Error, error);
        }
    }, [error, show]);

    const periodHandler = (formName: string, newValue: Date) => {
        if (formName === 'from') {
            setPeriod({ ...period, from: newValue });
        } else {
            setPeriod({ ...period, to: newValue });
        }
    };

    const partnerNameHandler = (ev: React.ChangeEvent<HTMLInputElement>) => {
        setPartnerName(ev.target.value);
    };

    const clientNameHandler = (ev: React.ChangeEvent<HTMLInputElement>) => {
        setClientName(ev.target.value);
    };

    const operationNameHandler = (ev: React.ChangeEvent<HTMLInputElement>) => {
        setOperationName(ev.target.value);
    };

    const onDataSelectHandler = (args: SelectDataCommonDataModel<void>) => {
        const requestBody: TAgentPaymentListRequest = {
            pageNumber: args.pageNumber,
            sortType: args.sortingData?.sortKind,
            sortFieldName: args.sortingData?.fieldName,
            partnerAccountData: partnerName,
            accountData: clientName,
            periodFrom: dateFilterFormat(period.from),
            periodTo: dateFilterFormat(period.to),
            operationData: operationName
        };

        getAgentPayments(requestBody, dispatch);
    };

    const { handleTableFilters } = useGetTableFilters({ onDataSelectHandler });

    const onSearchHandler = () => {
        onDataSelectHandler({
            pageNumber: 1
        });
    };

    return (
        <>
            { isLoading && <LoadingBounce /> }
            <div className={ globalStyles.container }>
                <div className={ globalStyles.header }>
                    <FormAndLabel label="Период">
                        <DoubleDateInputForm
                            isNeedToStretch={ true }
                            periodFromValue={ period.from }
                            periodToValue={ period.to }
                            periodFromInputName="from"
                            periodToInputName="to"
                            onValueChange={ periodHandler }
                        />
                    </FormAndLabel>
                    <FormAndLabel label="Операция">
                        <OutlinedInput
                            value={ operationName }
                            onChange={ operationNameHandler }
                            placeholder="Операция"
                            fullWidth={ true }
                        />
                    </FormAndLabel>
                    <FormAndLabel label="От кого (Клиент)">
                        <OutlinedInput
                            value={ clientName }
                            onChange={ clientNameHandler }
                            placeholder="Название компании"
                            fullWidth={ true }
                        />
                    </FormAndLabel>
                    <FormAndLabel label="Кому (Партнер)">
                        <OutlinedInput
                            value={ partnerName }
                            onChange={ partnerNameHandler }
                            placeholder="Название компании"
                            fullWidth={ true }
                        />
                    </FormAndLabel>
                    <SuccessButton onClick={ onSearchHandler }>Найти</SuccessButton>
                </div>
                <TransactionsTable handleFilters={ handleTableFilters } />
            </div>
        </>
    );
});