import { ArticleCoreStatusEnum } from 'app/api/endpoints/articles/response/getCoreArticleResponseDto';

export type TArticlesControlSearch = {
    filter?: {
        status?: ArticleCoreStatusEnum;
        createdFrom?: Date;
        createdTo?: Date
        topic?: string;
        accountUserLogin?: string;
    },
};