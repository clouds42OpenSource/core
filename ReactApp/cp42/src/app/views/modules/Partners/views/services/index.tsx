import React, { useEffect, useState } from 'react';
import { MenuItem, OutlinedInput, Select, SelectChangeEvent } from '@mui/material';

import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { useAppDispatch, useAppSelector } from 'app/hooks';
import { dateFilterFormat, useGetTableFilters } from 'app/views/modules/Partners/helpers';
import { REDUX_API } from 'app/api/useReduxApi';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { EBillingServiceStatus } from 'app/api/endpoints/partners/enum';
import { TServicesListRequest } from 'app/api/endpoints/partners/request';
import { SelectDataCommonDataModel } from 'app/web/common';
import { TDatePicker } from 'app/common/types/app-types';
import { ServicesTable } from 'app/views/modules/Partners/modules';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { DoubleDateInputForm } from 'app/views/components/controls/forms/DoubleDateInputView';
import { SuccessButton } from 'app/views/components/controls/Button';

import globalStyles from '../style.module.css';

type TSelect = '0' | '1' | '2' | '3';

const { getServices } = REDUX_API.PARTNERS_REDUX;

export const Services = withFloatMessages(({ floatMessage: { show } }) => {
    const dispatch = useAppDispatch();
    const { error, isLoading } = useAppSelector(state => state.PartnersState.servicesListReducer);

    const [partnerName, setPartnerName] = useState('');
    const [serviceName, setServiceName] = useState('');
    const [period, setPeriod] = useState<TDatePicker>({ from: null, to: null });
    const [status, setStatus] = useState<TSelect>('0');

    useEffect(() => {
        getServices({
            pageNumber: 1,
        }, dispatch);
    }, [dispatch]);

    useEffect(() => {
        if (error) {
            show(MessageType.Error, error);
        }
    }, [error, show]);

    const onDataSelectHandler = (args: SelectDataCommonDataModel<void>) => {
        const requestBody: TServicesListRequest = {
            pageNumber: args.pageNumber,
            sortType: args.sortingData?.sortKind,
            sortFieldName: args.sortingData?.fieldName,
            name: serviceName,
            partnerAccountData: partnerName,
            periodTo: dateFilterFormat(period.to),
            periodFrom: dateFilterFormat(period.from)
        };

        if (status !== '0') {
            requestBody.billingServiceStatus = parseInt(status, 10) as EBillingServiceStatus;
        }

        getServices(requestBody, dispatch);
    };

    const { handleTableFilters } = useGetTableFilters({ onDataSelectHandler });

    const partnerNameHandler = (ev: React.ChangeEvent<HTMLInputElement>) => {
        setPartnerName(ev.target.value);
    };

    const serviceNameHandler = (ev: React.ChangeEvent<HTMLInputElement>) => {
        setServiceName(ev.target.value);
    };

    const periodHandler = (formName: string, newValue: Date) => {
        if (formName === 'from') {
            setPeriod({ ...period, from: newValue });
        } else {
            setPeriod({ ...period, to: newValue });
        }
    };

    const statusHandler = (ev: SelectChangeEvent<TSelect>) => {
        setStatus(ev.target.value as TSelect);
    };

    const onSearchHandler = () => {
        onDataSelectHandler({
            pageNumber: 1
        });
    };

    return (
        <>
            { isLoading && <LoadingBounce /> }
            <div className={ globalStyles.container }>
                <div className={ globalStyles.header }>
                    <FormAndLabel label="Дата активации">
                        <DoubleDateInputForm
                            isNeedToStretch={ true }
                            periodFromValue={ period.from }
                            periodToValue={ period.to }
                            periodFromInputName="from"
                            periodToInputName="to"
                            onValueChange={ periodHandler }
                        />
                    </FormAndLabel>
                    <FormAndLabel label="Партнер">
                        <OutlinedInput
                            value={ partnerName }
                            onChange={ partnerNameHandler }
                            placeholder="Название, ID аккаунта или ИНН"
                            fullWidth={ true }
                        />
                    </FormAndLabel>
                    <FormAndLabel label="Статус">
                        <Select value={ status } fullWidth={ true } onChange={ statusHandler }>
                            <MenuItem value="0">Все</MenuItem>
                            <MenuItem value="1">Черновик</MenuItem>
                            <MenuItem value="2">На модерации</MenuItem>
                            <MenuItem value="3">Активен</MenuItem>
                        </Select>
                    </FormAndLabel>
                    <FormAndLabel label="Название сервиса">
                        <OutlinedInput
                            value={ serviceName }
                            onChange={ serviceNameHandler }
                            placeholder="Название"
                            fullWidth={ true }
                        />
                    </FormAndLabel>
                    <SuccessButton onClick={ onSearchHandler }>Найти</SuccessButton>
                </div>
                <ServicesTable handleFilters={ handleTableFilters } />
            </div>
        </>
    );
});