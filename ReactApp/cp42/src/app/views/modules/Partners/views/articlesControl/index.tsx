import { Delete as DeleteIcon, Unpublished as UnpublishedIcon } from '@mui/icons-material';
import { getCoreArticlesFilterParams } from 'app/api/endpoints/articles/requests/getCoreArticleRequestDto';
import { Box, Tooltip, Chip, Link as MuiLink } from '@mui/material';
import { ArticleCoreStatusEnum, getCoreArticleResponseDto } from 'app/api/endpoints/articles/response/getCoreArticleResponseDto';
import { FETCH_API } from 'app/api/useFetchApi';
import { AppRoutes } from 'app/AppRoutes';
import { SortingKind } from 'app/common/enums';
import { ArticleStatusDescriptions } from 'app/views/modules/Articles/utils/getArticleStatusName';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import dayjs from 'dayjs';
import React, { useEffect, useState, useRef, useCallback } from 'react';
import { EMessageType, useFloatMessages } from 'app/hooks';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { ArticlesControlSearch } from 'app/views/modules/Partners/views/articlesControl/components/ArticlesControlSearch';
import { SelectDataMetadataResponseDto, SortingDataModel } from 'app/web/common';
import { Link } from 'react-router-dom';

import styles from './style.module.css';

type TGetCoreArticles = {
    pageNumber: number;
    sortingData?: SortingDataModel;
};

export const ArticlesControl = () => {
    const { show } = useFloatMessages();

    const [articles, setArticles] = useState<null | SelectDataMetadataResponseDto<getCoreArticleResponseDto>>(null);
    const [isLoading, setIsLoading] = useState(false);
    const [pageNumber, setPageNumber] = useState(1);
    const [searchData, setSearchData] = useState<getCoreArticlesFilterParams>({
        status: undefined,
        createdFrom: undefined,
        createdTo: undefined,
        accountUserLogin: undefined,
        topic: undefined
    });
    const pageNumberRef = useRef(pageNumber);

    useEffect(() => {
        pageNumberRef.current = pageNumber;
    }, [pageNumber]);

    const handleGetCoreArticles = useCallback(async (value?: TGetCoreArticles) => {
        setIsLoading(true);
        const currentPageNumber = value?.pageNumber || pageNumberRef.current;

        if (value?.pageNumber) {
            setPageNumber(value.pageNumber);
        }

        const response = await FETCH_API.ARTICLES.getCoreArticles({
            pageNumber: currentPageNumber,
            pageSize: 10,
            filter: { ...searchData },
            orderBy: value?.sortingData ? `${ value.sortingData.fieldName }.${ value.sortingData.sortKind === SortingKind.Asc ? 'asc' : 'desc' }` : '',
        });

        if (response.success) {
            setArticles(response.data);
        }

        setIsLoading(false);
    }, [searchData]);

    const deleteArticle = async (data: getCoreArticleResponseDto) => {
        setIsLoading(true);
        const response = await FETCH_API.ARTICLES.deleteCoreArticle({ id: data.id });
        let wpResponse;

        if (data.wpId) {
            wpResponse = await FETCH_API.ARTICLES.deletePost(data.wpId);
        }

        if ((response.success && (wpResponse && wpResponse.success)) || (response.success && !wpResponse)) {
            if (data.topicId) {
                await FETCH_API.ARTICLES.editThemeStatus({
                    id: data.topicId,
                    status: 'publish'
                });
            }
            show(EMessageType.success, 'Статья успешно удалена');
        } else {
            if (response.message) {
                show(EMessageType.error, response.message);
            }
            if (wpResponse && wpResponse.message) {
                show(EMessageType.error, wpResponse.message);
            }
        }

        void handleGetCoreArticles();
        setIsLoading(false);
    };

    const editArticleStatus = async (data: getCoreArticleResponseDto, status: ArticleCoreStatusEnum, description: string, articleStatus = 'publish') => {
        setIsLoading(true);
        const response = await FETCH_API.ARTICLES.editCoreArticleStatus({ id: data.id, status });

        if (response.success && data.wpId) {
            if (status === 1 && data.status === 5) {
                await FETCH_API.ARTICLES.editBlogArticleStatus({ id: data.wpId, status: articleStatus });
            }

            show(EMessageType.success, description);
        } else {
            show(EMessageType.error, response.message);
        }

        void handleGetCoreArticles();
        setIsLoading(false);
    };

    const handleSearch = useCallback((value: getCoreArticlesFilterParams) => {
        setSearchData(value);
    }, []);

    useEffect(() => {
        void handleGetCoreArticles();
    }, [handleGetCoreArticles]);

    const getArticleActions = (data: getCoreArticleResponseDto) => (
        <Box display="flex" alignItems="center" gap="5px" justifyContent="flex-start">
            { data.status === ArticleCoreStatusEnum.Published && (
                <Tooltip title="Сделать черновиком">
                    <UnpublishedIcon className={ styles.action } onClick={ () => editArticleStatus(data, 1, 'Статус статьи изменен на черновик', 'draft') } fontSize="small" />
                </Tooltip>
            ) }
            <Tooltip key="delete" title="Удалить">
                <DeleteIcon className={ styles.action } onClick={ () => deleteArticle(data) } fontSize="small" />
            </Tooltip>
        </Box>
    );

    return (
        <>
            { isLoading && <LoadingBounce /> }
            <ArticlesControlSearch onDataSelect={ handleSearch } />
            { articles && (
                <CommonTableWithFilter
                    isBorderStyled={ true }
                    isResponsiveTable={ true }
                    onDataSelect={ handleGetCoreArticles }
                    uniqueContextProviderStateId="articles"
                    tableProps={ {
                        dataset: articles.records,
                        keyFieldName: 'id',
                        pagination: {
                            totalPages: Math.ceil(articles.metadata.totalItemCount / articles.metadata.pageSize),
                            pageSize: articles.metadata.pageSize,
                            currentPage: articles.metadata.pageNumber,
                            recordsCount: articles.metadata.totalItemCount,
                        },
                        fieldsView: {
                            topic: {
                                caption: 'Наименование',
                                isSortable: true,
                                fieldWidth: '30%',
                                format: (value: string, row) =>
                                    <Link to={ `${ AppRoutes.articles.editArticleForBlogReference }/${ row.id }` } className={ styles.link }>
                                        { value }
                                    </Link>
                            },
                            accountUserLogin: {
                                caption: 'Автор статьи',
                                isSortable: true,
                            },
                            createdOn: {
                                caption: 'Дата создания',
                                isSortable: true,
                                format: (value: string) => dayjs(value).format('DD.MM.YYYY HH:mm')
                            },
                            publicationDate: {
                                caption: 'Дата публикации',
                                isSortable: true,
                                format: (value: string | null) => value ? dayjs(value).format('DD.MM.YYYY HH:mm') : '---',
                            },
                            wpLink: {
                                caption: 'Ссылка',
                                format: (value: string | null) => (value ? <MuiLink href={ value } target="_blank" rel="noreferrer">{ value }</MuiLink> : '---'),
                                isSortable: true,
                            },
                            registrationCount: {
                                caption: 'Кол-во регистраций',
                                isSortable: true,
                                format: (value: number, row) => (
                                    row.status === 5
                                        ? (<><i className="fa fa-user" aria-hidden="true" />{ ' ' }{ value }</>)
                                        : '---'
                                )
                            },
                            status: {
                                caption: 'Статус',
                                isSortable: true,
                                format: (value: number) => {
                                    const statusInfo = ArticleStatusDescriptions[value as ArticleCoreStatusEnum];

                                    return (
                                        <Chip className={ styles.chip } label={ statusInfo.title } color={ statusInfo.type } />
                                    );
                                }
                            },
                            type: {
                                caption: 'Тип',
                                isSortable: true,
                                format: (value: number) => value === 1 ? 'Для сайта' : 'Для блога'
                            },
                            googleCloudLink: {
                                caption: 'Действия',
                                fieldWidth: '10%',
                                format: (_, row) => getArticleActions(row)
                            }
                        }
                    } }
                />
            ) }
        </>
    );
};