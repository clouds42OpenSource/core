import { Box, MenuItem, OutlinedInput, Select, SelectChangeEvent } from '@mui/material';
import { getCoreArticlesFilterParams } from 'app/api/endpoints/articles/requests/getCoreArticleRequestDto';
import { ArticleCoreStatusEnum } from 'app/api/endpoints/articles/response/getCoreArticleResponseDto';
import { SuccessButton } from 'app/views/components/controls/Button';
import { DoubleDateInputForm } from 'app/views/components/controls/forms/DoubleDateInputView';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import dayjs from 'dayjs';
import React, { useState, memo } from 'react';

import globalStyles from '../../style.module.css';

type Props = {
    onDataSelect: (value: getCoreArticlesFilterParams) => void;
}
export const ArticlesControlSearch = memo(({ onDataSelect }: Props) => {
    const [status, setStatus] = useState<number>(0);
    const [createdFrom, setCreatedFrom] = useState<Date>();
    const [createdTo, setCreatedTo] = useState<Date >();
    const [topic, setTopic] = useState('');
    const [accountUserLogin, setAccountUserLogin] = useState('');

    const handle = () => {
        onDataSelect({
            status,
            createdFrom: createdFrom ? dayjs(createdFrom).format('YYYY-MM-DD') : undefined,
            createdTo: createdTo ? dayjs(createdTo).format('YYYY-MM-DD') : undefined,
            accountUserLogin,
            topic
        });
    };

    const handleSelectStatus = (ev: SelectChangeEvent<ArticleCoreStatusEnum>) => {
        if (ev.target.value) {
            setStatus(ev.target.value as ArticleCoreStatusEnum);
        } else {
            setStatus(0);
        }
    };
    const handleChange = (ev: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>, formName: string) => {
        const { value } = ev.target;

        switch (formName) {
            case 'topic':
                setTopic(value);
                break;
            case 'accountUserLogin':
                setAccountUserLogin(value);
                break;
            default:
                break;
        }
    };

    const handleDateChange = (formName: string, value: Date) => {
        switch (formName) {
            case 'createdFrom':
                setCreatedFrom(value);
                break;
            case 'createdTo':
                setCreatedTo(value);
                break;
            default:
        }
    };

    return (
        <Box className={ globalStyles.header }>
            <FormAndLabel label="Дата подключения">
                <DoubleDateInputForm
                    isNeedToStretch={ true }
                    periodFromValue={ createdFrom || null }
                    periodToValue={ createdTo || null }
                    periodFromInputName="createdFrom"
                    periodToInputName="createdTo"
                    onValueChange={ handleDateChange }
                />
            </FormAndLabel>
            <FormAndLabel label="Тема">
                <OutlinedInput
                    value={ topic }
                    onChange={ e => handleChange(e, 'topic') }
                    name="test"
                    placeholder="Тема"
                    fullWidth={ true }
                />
            </FormAndLabel>
            <FormAndLabel label="Автор">
                <OutlinedInput
                    value={ accountUserLogin }
                    onChange={ e => handleChange(e, 'accountUserLogin') }
                    name="test"
                    placeholder="Автор"
                    fullWidth={ true }
                />
            </FormAndLabel>
            <FormAndLabel label="Статус">
                <Select value={ status } fullWidth={ true } onChange={ handleSelectStatus }>
                    <MenuItem value={ 0 }>Все</MenuItem>
                    <MenuItem value={ ArticleCoreStatusEnum.Draft }>Черновик</MenuItem>
                    <MenuItem value={ ArticleCoreStatusEnum.UnderInspection }>На проверке</MenuItem>
                    <MenuItem value={ ArticleCoreStatusEnum.Accepted }>Принята</MenuItem>
                    <MenuItem value={ ArticleCoreStatusEnum.ForRevision }>На доработке</MenuItem>
                    <MenuItem value={ ArticleCoreStatusEnum.Published }>Опубликована</MenuItem>
                </Select>
            </FormAndLabel>
            <SuccessButton onClick={ handle }>Найти</SuccessButton>
        </Box>
    );
});