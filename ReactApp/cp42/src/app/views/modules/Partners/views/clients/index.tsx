import React, { useEffect, useState } from 'react';
import { MenuItem, OutlinedInput, Select, SelectChangeEvent } from '@mui/material';

import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { REDUX_API } from 'app/api/useReduxApi';
import { useAppDispatch, useAppSelector } from 'app/hooks';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { dateFilterFormat, useGetServicesFilter, useGetTableFilters } from 'app/views/modules/Partners/helpers';
import { SelectDataCommonDataModel } from 'app/web/common';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { DoubleDateInputForm } from 'app/views/components/controls/forms/DoubleDateInputView';
import { SuccessButton } from 'app/views/components/controls/Button';
import { TClientsListRequest } from 'app/api/endpoints/partners/request';
import { ETypeOfPartnership } from 'app/api/endpoints/partners/enum';
import { TDatePicker } from 'app/common/types/app-types';
import { AutocompleteComboBoxForm } from 'app/views/components/controls/forms/ComboBox/AutocompleteComboBoxForm';
import { ClientsTable } from 'app/views/modules/Partners/modules';

import globalStyles from '../style.module.css';
import styles from './style.module.css';

type TSelect = '0' | '1' | '2';

const { getClients } = REDUX_API.PARTNERS_REDUX;

export const Clients = withFloatMessages(({ floatMessage: { show } }) => {
    const dispatch = useAppDispatch();
    const { error, isLoading } = useAppSelector(state => state.PartnersState.clientListReducer);

    const {
        servicesSearchResult,
        serviceId,
        isServicesSearchLoading,
        servicesSearchString,
        serviceSearchHandler,
        serviceIdHandler
    } = useGetServicesFilter(show);

    const [clientName, setClientName] = useState('');
    const [partnerName, setPartnerName] = useState('');
    const [partnership, setPartnership] = useState<TSelect>('0');
    const [activity, setActivity] = useState<TSelect>('0');
    const [connectionPeriod, setConnectionPeriod] = useState<TDatePicker>({ from: null, to: null });
    const [expiredPeriod, setExpiredPeriod] = useState<TDatePicker>({ from: null, to: null });

    useEffect(() => {
        getClients({
            pageNumber: 1,
        }, dispatch);
    }, [dispatch]);

    useEffect(() => {
        if (error) {
            show(MessageType.Error, error);
        }
    }, [error, show]);

    const onDataSelectHandler = (args: SelectDataCommonDataModel<void>) => {
        const requestParams: TClientsListRequest = {
            pageNumber: args.pageNumber,
            accountData: clientName,
            partnerAccountData: partnerName,
            sortType: args.sortingData?.sortKind,
            sortFieldName: args.sortingData?.fieldName,
            periodFrom: dateFilterFormat(connectionPeriod.from),
            periodTo: dateFilterFormat(connectionPeriod.to),
            periodServiceExpireDateFrom: dateFilterFormat(expiredPeriod.from),
            periodServiceExpireDateTo: dateFilterFormat(expiredPeriod.to),
            serviceId
        };

        if (partnership !== '0') {
            requestParams.typeOfPartnership = parseInt(partnership, 10) as ETypeOfPartnership;
        }

        if (activity !== '0') {
            requestParams.serviceIsActiveForClient = activity === '1';
        }

        getClients(requestParams, dispatch);
    };

    const { handleTableFilters } = useGetTableFilters({ onDataSelectHandler });

    const clientNameHandler = (ev: React.ChangeEvent<HTMLInputElement>) => {
        setClientName(ev.target.value);
    };

    const partnerNameHandler = (ev: React.ChangeEvent<HTMLInputElement>) => {
        setPartnerName(ev.target.value);
    };

    const partnershipHandler = (ev: SelectChangeEvent<TSelect>) => {
        setPartnership(ev.target.value as TSelect);
    };

    const activityHandler = (ev: SelectChangeEvent<TSelect>) => {
        setActivity(ev.target.value as TSelect);
    };

    const periodHandler = (formName: string, newValue: Date) => {
        switch (formName) {
            case 'connection_from':
                setConnectionPeriod({ ...connectionPeriod, from: newValue });
                break;
            case 'connection_to':
                setConnectionPeriod({ ...connectionPeriod, to: newValue });
                break;
            case 'expired_from':
                setExpiredPeriod({ ...expiredPeriod, from: newValue });
                break;
            case 'expired_to':
                setExpiredPeriod({ ...expiredPeriod, to: newValue });
                break;
            default:
        }
    };

    const onSearchHandler = () => {
        onDataSelectHandler({
            pageNumber: 1
        });
    };

    return (
        <>
            { isLoading && <LoadingBounce /> }
            <div className={ globalStyles.container }>
                <div className={ styles.header }>
                    <FormAndLabel label="Дата подключения">
                        <DoubleDateInputForm
                            isNeedToStretch={ true }
                            periodFromValue={ connectionPeriod.from }
                            periodToValue={ connectionPeriod.to }
                            periodFromInputName="connection_from"
                            periodToInputName="connection_to"
                            onValueChange={ periodHandler }
                        />
                    </FormAndLabel>
                    <FormAndLabel label="Клиент">
                        <OutlinedInput
                            onChange={ clientNameHandler }
                            value={ clientName }
                            placeholder="Название, ID аккаунта или ИНН"
                            fullWidth={ true }
                        />
                    </FormAndLabel>
                    <FormAndLabel label="Тип партнерства">
                        <Select value={ partnership } fullWidth={ true } onChange={ partnershipHandler }>
                            <MenuItem value="0">Все</MenuItem>
                            <MenuItem value="1">Привлеченный мной клиент</MenuItem>
                            <MenuItem value="2">Клиент моего сервиса</MenuItem>
                        </Select>
                    </FormAndLabel>
                    <FormAndLabel label="Активность">
                        <Select value={ activity } fullWidth={ true } onChange={ activityHandler }>
                            <MenuItem value="0">Все</MenuItem>
                            <MenuItem value="1">Активные</MenuItem>
                            <MenuItem value="2">Неактивные</MenuItem>
                        </Select>
                    </FormAndLabel>
                    <FormAndLabel label="Дата окончания сервиса">
                        <DoubleDateInputForm
                            isNeedToStretch={ true }
                            periodFromValue={ expiredPeriod.from }
                            periodToValue={ expiredPeriod.to }
                            periodFromInputName="expired_from"
                            periodToInputName="expired_to"
                            onValueChange={ periodHandler }
                        />
                    </FormAndLabel>
                    <FormAndLabel label="Партнер">
                        <OutlinedInput
                            onChange={ partnerNameHandler }
                            value={ partnerName }
                            placeholder="Название, ID аккаунта или ИНН"
                            fullWidth={ true }
                        />
                    </FormAndLabel>
                    <FormAndLabel label="Сервис">
                        <AutocompleteComboBoxForm
                            placeholder="Все"
                            isClearable={ true }
                            noItemsText="Не найдено"
                            isLoading={ isServicesSearchLoading }
                            items={ servicesSearchResult }
                            formName=""
                            raiseInputValueChangeEvents={ true }
                            inputValue={ servicesSearchString ?? '' }
                            value={ serviceId }
                            onValueChange={ serviceIdHandler }
                            onInputValueChange={ serviceSearchHandler }
                        />
                    </FormAndLabel>
                    <SuccessButton onClick={ onSearchHandler }>Найти</SuccessButton>
                </div>
                <ClientsTable handleFilters={ handleTableFilters } />
            </div>
        </>
    );
});