import React, { useEffect, useState } from 'react';

import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { REDUX_API } from 'app/api/useReduxApi';
import { useAppDispatch, useAppSelector } from 'app/hooks';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { SelectDataCommonDataModel } from 'app/web/common';
import { TAgentCashOutRequest } from 'app/api/endpoints/partners/request';
import { WithdrawTable } from 'app/views/modules/Partners/modules';
import { dateFilterFormat, useGetTableFilters } from 'app/views/modules/Partners/helpers';
import { SuccessButton } from 'app/views/components/controls/Button';
import { TDatePicker } from 'app/common/types/app-types';
import { EAgentRequisitesStatus, EAgentRequisitesType, ERequestStatus } from 'app/api/endpoints/partners/enum';
import { MenuItem, OutlinedInput, Select, SelectChangeEvent } from '@mui/material';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';

import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { DoubleDateInputForm } from 'app/views/components/controls/forms/DoubleDateInputView';
import globalStyles from '../style.module.css';

const { getAgentCashOut } = REDUX_API.PARTNERS_REDUX;

export const Withdraw = withFloatMessages(({ floatMessage: { show } }) => {
    const dispatch = useAppDispatch();
    const { error, isLoading } = useAppSelector(state => state.PartnersState.agentCashOutRequestListReducer);

    const [requestNumber, setRequestNumber] = useState('');
    const [creationDate, setCreationDate] = useState<TDatePicker>({ from: null, to: null });
    const [partnerName, setPartnerName] = useState('');
    const [requisiteType, setRequisiteType] = useState<EAgentRequisitesType>(EAgentRequisitesType.undefined);
    const [requestStatus, setRequestStatus] = useState<ERequestStatus>(ERequestStatus.undefined);

    useEffect(() => {
        getAgentCashOut({
            pageNumber: 1
        }, dispatch);
    }, [dispatch]);

    useEffect(() => {
        if (error) {
            show(MessageType.Error, error);
        }
    }, [error, show]);

    const requestNumberHandler = (ev: React.ChangeEvent<HTMLInputElement>) => {
        setRequestNumber(ev.target.value);
    };

    const requisiteTypeHandler = (ev: SelectChangeEvent<EAgentRequisitesType>) => {
        setRequisiteType(ev.target.value as EAgentRequisitesType);
    };

    const requestStatusHandler = (ev: SelectChangeEvent<ERequestStatus>) => {
        setRequestStatus(ev.target.value as ERequestStatus);
    };

    const partnerNameHandler = (ev: React.ChangeEvent<HTMLInputElement>) => {
        setPartnerName(ev.target.value);
    };

    const creationDateHandler = (formName: string, newValue: Date) => {
        if (formName === 'from') {
            setCreationDate({ ...creationDate, from: newValue });
        } else {
            setCreationDate({ ...creationDate, to: newValue });
        }
    };

    const onDataSelectHandler = (args: SelectDataCommonDataModel<void>) => {
        const requestBody: TAgentCashOutRequest = {
            pageNumber: args.pageNumber,
            sortType: args.sortingData?.sortKind,
            sortFieldName: args.sortingData?.fieldName,
            requestNumber,
            agentRequisitesType: requisiteType,
            periodFrom: dateFilterFormat(creationDate.from),
            periodTo: dateFilterFormat(creationDate.to),
            partnerAccountData: partnerName,
            requestStatus
        };

        getAgentCashOut(requestBody, dispatch);
    };

    const { handleTableFilters } = useGetTableFilters({ onDataSelectHandler });

    const onSearchHandler = () => {
        onDataSelectHandler({
            pageNumber: 1
        });
    };

    return (
        <>
            { isLoading && <LoadingBounce /> }
            <div className={ globalStyles.container }>
                <div className={ globalStyles.header }>
                    <FormAndLabel label="Дата создания">
                        <DoubleDateInputForm
                            isNeedToStretch={ true }
                            periodFromValue={ creationDate.from }
                            periodToValue={ creationDate.to }
                            periodFromInputName="from"
                            periodToInputName="to"
                            onValueChange={ creationDateHandler }
                        />
                    </FormAndLabel>
                    <FormAndLabel label="Партнер">
                        <OutlinedInput
                            value={ partnerName }
                            onChange={ partnerNameHandler }
                            placeholder="Название, ID аккаунта или ИНН"
                            fullWidth={ true }
                        />
                    </FormAndLabel>
                    <FormAndLabel label="Статус">
                        <Select value={ requestStatus } fullWidth={ true } onChange={ requestStatusHandler }>
                            <MenuItem value={ ERequestStatus.undefined }>Все</MenuItem>
                            <MenuItem value={ ERequestStatus.new }>Новая</MenuItem>
                            <MenuItem value={ ERequestStatus.operation }>В работе</MenuItem>
                            <MenuItem value={ ERequestStatus.paid }>Оплачена</MenuItem>
                        </Select>
                    </FormAndLabel>
                    <FormAndLabel label="Номер заявки">
                        <OutlinedInput
                            value={ requestNumber }
                            onChange={ requestNumberHandler }
                            placeholder="Номер заявки"
                            fullWidth={ true }
                        />
                    </FormAndLabel>
                    <FormAndLabel label="Тип реквизитов">
                        <Select value={ requisiteType } fullWidth={ true } onChange={ requisiteTypeHandler }>
                            <MenuItem value={ EAgentRequisitesStatus.undefined }>Все</MenuItem>
                            <MenuItem value={ EAgentRequisitesType.physicalPersonRequisites }>Реквизиты физ. лица</MenuItem>
                            <MenuItem value={ EAgentRequisitesType.legalPersonRequisites }>Реквизиты юр. лица</MenuItem>
                            <MenuItem value={ EAgentRequisitesType.soleProprietorRequisites }>Реквизиты (ИП)</MenuItem>
                        </Select>
                    </FormAndLabel>
                    <SuccessButton onClick={ onSearchHandler }>Найти</SuccessButton>
                </div>
                <WithdrawTable handleFilters={ handleTableFilters } />
            </div>
        </>
    );
});