import { MenuItem, OutlinedInput, Select, SelectChangeEvent } from '@mui/material';
import React, { useEffect, useState } from 'react';

import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { useAppDispatch, useAppSelector } from 'app/hooks';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { REDUX_API } from 'app/api/useReduxApi';
import { SelectDataCommonDataModel } from 'app/web/common';
import { EAgentRequisitesStatus, EAgentRequisitesType } from 'app/api/endpoints/partners/enum';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { DoubleDateInputForm } from 'app/views/components/controls/forms/DoubleDateInputView';
import { SuccessButton } from 'app/views/components/controls/Button';
import { TDatePicker } from 'app/common/types/app-types';
import { TAgentsRequisitesListRequest } from 'app/api/endpoints/partners/request';
import { dateFilterFormat, useGetTableFilters } from 'app/views/modules/Partners/helpers';

import { RequisitesTable } from 'app/views/modules/Partners/modules/tables/requisitesTable';
import globalStyles from '../style.module.css';

const { getAgentRequisites } = REDUX_API.PARTNERS_REDUX;

export const Requisites = withFloatMessages(({ floatMessage: { show } }) => {
    const dispatch = useAppDispatch();
    const { error, isLoading } = useAppSelector(state => state.PartnersState.agentRequisitesListReducer);

    const [period, setPeriod] = useState<TDatePicker>({ from: null, to: null });
    const [partnerName, setPartnerName] = useState('');
    const [status, setStatus] = useState<EAgentRequisitesStatus>(EAgentRequisitesStatus.undefined);
    const [requisiteType, setRequisiteType] = useState<EAgentRequisitesType>(EAgentRequisitesType.undefined);

    useEffect(() => {
        getAgentRequisites({
            pageNumber: 1
        }, dispatch);
    }, [dispatch]);

    useEffect(() => {
        if (error) {
            show(MessageType.Error, error);
        }
    }, [error, show]);

    const periodHandler = (formName: string, newValue: Date) => {
        if (formName === 'from') {
            setPeriod({ ...period, from: newValue });
        } else {
            setPeriod({ ...period, to: newValue });
        }
    };

    const statusHandler = (ev: SelectChangeEvent<EAgentRequisitesStatus>) => {
        setStatus(ev.target.value as EAgentRequisitesStatus);
    };

    const requisiteTypeHandler = (ev: SelectChangeEvent<EAgentRequisitesType>) => {
        setRequisiteType(ev.target.value as EAgentRequisitesType);
    };

    const partnerNameHandler = (ev: React.ChangeEvent<HTMLInputElement>) => {
        setPartnerName(ev.target.value);
    };

    const onDataSelectHandler = (args: SelectDataCommonDataModel<void>) => {
        const requestBody: TAgentsRequisitesListRequest = {
            pageNumber: args.pageNumber,
            sortType: args.sortingData?.sortKind,
            sortFieldName: args.sortingData?.fieldName,
            partnerAccountData: partnerName,
            periodFrom: dateFilterFormat(period.from),
            periodTo: dateFilterFormat(period.to)
        };

        if (status !== EAgentRequisitesStatus.undefined) {
            requestBody.agentRequisitesStatus = status;
        }

        if (requisiteType !== EAgentRequisitesType.undefined) {
            requestBody.agentRequisitesType = requisiteType;
        }

        getAgentRequisites(requestBody, dispatch);
    };

    const { handleTableFilters } = useGetTableFilters({ onDataSelectHandler });

    const onSearchHandler = () => {
        onDataSelectHandler({
            pageNumber: 1
        });
    };

    return (
        <>
            { isLoading && <LoadingBounce /> }
            <div className={ globalStyles.container }>
                <div className={ globalStyles.header }>
                    <FormAndLabel label="Дата создания">
                        <DoubleDateInputForm
                            isNeedToStretch={ true }
                            periodFromValue={ period.from }
                            periodToValue={ period.to }
                            periodFromInputName="from"
                            periodToInputName="to"
                            onValueChange={ periodHandler }
                        />
                    </FormAndLabel>
                    <FormAndLabel label="Партнер">
                        <OutlinedInput
                            value={ partnerName }
                            onChange={ partnerNameHandler }
                            placeholder="Название, ID аккаунта или ИНН"
                            fullWidth={ true }
                        />
                    </FormAndLabel>
                    <FormAndLabel label="Статус">
                        <Select value={ status } fullWidth={ true } onChange={ statusHandler }>
                            <MenuItem value={ EAgentRequisitesStatus.undefined }>Все</MenuItem>
                            <MenuItem value={ EAgentRequisitesStatus.draft }>Черновик</MenuItem>
                            <MenuItem value={ EAgentRequisitesStatus.onCheck }>На проверке</MenuItem>
                            <MenuItem value={ EAgentRequisitesStatus.verified }>Проверено</MenuItem>
                        </Select>
                    </FormAndLabel>
                    <FormAndLabel label="Тип реквизитов">
                        <Select value={ requisiteType } fullWidth={ true } onChange={ requisiteTypeHandler }>
                            <MenuItem value={ EAgentRequisitesStatus.undefined }>Все</MenuItem>
                            <MenuItem value={ EAgentRequisitesType.physicalPersonRequisites }>Реквизиты физ. лица</MenuItem>
                            <MenuItem value={ EAgentRequisitesType.legalPersonRequisites }>Реквизиты юр. лица</MenuItem>
                            <MenuItem value={ EAgentRequisitesType.soleProprietorRequisites }>Реквизиты (ИП)</MenuItem>
                        </Select>
                    </FormAndLabel>
                    <SuccessButton onClick={ onSearchHandler }>Найти</SuccessButton>
                </div>
                <RequisitesTable handleFilters={ handleTableFilters } />
            </div>
        </>
    );
});