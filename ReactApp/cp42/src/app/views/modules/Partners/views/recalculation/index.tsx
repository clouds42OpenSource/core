import React, { useEffect, useState } from 'react';
import { MenuItem, Select, SelectChangeEvent } from '@mui/material';

import { SuccessButton } from 'app/views/components/controls/Button';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { DoubleDateInputForm } from 'app/views/components/controls/forms/DoubleDateInputView';
import { TDatePicker } from 'app/common/types/app-types';
import { dateFilterFormat, useGetServicesFilter, useGetTableFilters } from 'app/views/modules/Partners/helpers';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { AutocompleteComboBoxForm } from 'app/views/components/controls/forms/ComboBox/AutocompleteComboBoxForm';
import { ERecalculationTaskStatus } from 'app/api/endpoints/partners/enum';
import { RecalculationTable } from 'app/views/modules/Partners/modules/tables';
import { SelectDataCommonDataModel } from 'app/web/common';
import { useAppDispatch, useAppSelector } from 'app/hooks';
import { REDUX_API } from 'app/api/useReduxApi';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { TRecalculationServiceCostRequest } from 'app/api/endpoints/partners/request';

import globalStyles from '../style.module.css';

const { getRecalculationServiceCosts } = REDUX_API.PARTNERS_REDUX;

export const Recalculation = withFloatMessages(({ floatMessage: { show } }) => {
    const dispatch = useAppDispatch();
    const { error, isLoading } = useAppSelector(state => state.PartnersState.recalculationServiceCostListReducer);

    const [period, setPeriod] = useState<TDatePicker>({ from: null, to: null });
    const [recalculationPeriod, setRecalculationPeriod] = useState<TDatePicker>({ from: null, to: null });
    const [status, setStatus] = useState<ERecalculationTaskStatus>(ERecalculationTaskStatus.undefined);

    const {
        servicesSearchResult,
        serviceId,
        isServicesSearchLoading,
        servicesSearchString,
        serviceSearchHandler,
        serviceIdHandler
    } = useGetServicesFilter(show);

    useEffect(() => {
        getRecalculationServiceCosts({
            pageNumber: 1
        }, dispatch);
    }, [dispatch]);

    useEffect(() => {
        if (error) {
            show(MessageType.Error, error);
        }
    }, [error, show]);

    const periodHandler = (formName: string, newValue: Date) => {
        if (formName === 'from') {
            setPeriod({ ...period, from: newValue });
        } else {
            setPeriod({ ...period, to: newValue });
        }
    };

    const recalculationPeriodHandler = (formName: string, newValue: Date) => {
        if (formName === 'rec_from') {
            setRecalculationPeriod({ ...period, from: newValue });
        } else {
            setRecalculationPeriod({ ...period, to: newValue });
        }
    };

    const statusHandler = (ev: SelectChangeEvent<ERecalculationTaskStatus>) => {
        setStatus(ev.target.value as ERecalculationTaskStatus);
    };

    const onDataSelectHandler = (args: SelectDataCommonDataModel<void>) => {
        const requestBody: TRecalculationServiceCostRequest = {
            pageNumber: args.pageNumber,
            periodCreationDateFrom: dateFilterFormat(period.from),
            periodCreationDateTo: dateFilterFormat(period.to),
            periodRecalculationDateFrom: dateFilterFormat(recalculationPeriod.from),
            periodRecalculationDateTo: dateFilterFormat(recalculationPeriod.to),
            queueStatus: status,
            sortType: args.sortingData?.sortKind,
            sortFieldName: args.sortingData?.fieldName,
            serviceId
        };

        getRecalculationServiceCosts(requestBody, dispatch);
    };

    const onSearchHandler = () => {
        onDataSelectHandler({
            pageNumber: 1
        });
    };

    const { handleTableFilters } = useGetTableFilters({ onDataSelectHandler });

    return (
        <>
            { isLoading && <LoadingBounce /> }
            <div className={ globalStyles.container }>
                <div className={ globalStyles.header }>
                    <FormAndLabel label="Дата создания">
                        <DoubleDateInputForm
                            isNeedToStretch={ true }
                            periodFromValue={ period.from }
                            periodToValue={ period.to }
                            periodFromInputName="from"
                            periodToInputName="to"
                            onValueChange={ periodHandler }
                        />
                    </FormAndLabel>
                    <FormAndLabel label="Дата пересчета стоимости сервиса">
                        <DoubleDateInputForm
                            isNeedToStretch={ true }
                            periodFromValue={ recalculationPeriod.from }
                            periodToValue={ recalculationPeriod.to }
                            periodFromInputName="rec_from"
                            periodToInputName="rec_to"
                            onValueChange={ recalculationPeriodHandler }
                        />
                    </FormAndLabel>
                    <FormAndLabel label="Сервис">
                        <AutocompleteComboBoxForm
                            placeholder="Все"
                            isClearable={ true }
                            noItemsText="Не найдено"
                            isLoading={ isServicesSearchLoading }
                            items={ servicesSearchResult }
                            formName=""
                            raiseInputValueChangeEvents={ true }
                            inputValue={ servicesSearchString ?? '' }
                            value={ serviceId }
                            onValueChange={ serviceIdHandler }
                            onInputValueChange={ serviceSearchHandler }
                        />
                    </FormAndLabel>
                    <FormAndLabel label="Статус задачи">
                        <Select value={ status } fullWidth={ true } onChange={ statusHandler }>
                            <MenuItem value={ ERecalculationTaskStatus.undefined }>Все</MenuItem>
                            <MenuItem value={ ERecalculationTaskStatus.new }>Новая</MenuItem>
                            <MenuItem value={ ERecalculationTaskStatus.captured }>Захвачена воркером</MenuItem>
                            <MenuItem value={ ERecalculationTaskStatus.processing }>В процессе выполнения</MenuItem>
                            <MenuItem value={ ERecalculationTaskStatus.ready }>Готова</MenuItem>
                            <MenuItem value={ ERecalculationTaskStatus.error }>Ошибка</MenuItem>
                            <MenuItem value={ ERecalculationTaskStatus.needRetry }>Будет перезапущена</MenuItem>
                        </Select>
                    </FormAndLabel>
                    <SuccessButton onClick={ onSearchHandler }>Найти</SuccessButton>
                </div>
                <RecalculationTable handleFilters={ handleTableFilters } />
            </div>
        </>
    );
});