import { TFilterTable } from 'app/views/modules/Partners/config/types/TFilterTable';

export type TTable = {
    handleFilters: (filters: TFilterTable) => void;
};