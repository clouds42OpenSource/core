import { SortingKind } from 'app/common/enums';

export type TFilterTable = {
    pageNumber: number;
    sortType?: SortingKind;
    sortFieldName?: string;
};