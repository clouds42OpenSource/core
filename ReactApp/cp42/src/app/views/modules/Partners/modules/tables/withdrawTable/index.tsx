import React, { memo } from 'react';
import { Chip, Link } from '@mui/material';

import { TTable } from 'app/views/modules/Partners/config';
import { CommonTableWithFilter, SelectDataInitiator } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { setCashFormat, setDateFormat } from 'app/common/helpers';
import { ERequestStatus } from 'app/api/endpoints/partners/enum';
import { AccountCard } from 'app/views/modules/_common/reusable-modules/AccountCard';
import { SelectDataCommonDataModel } from 'app/web/common';
import { useAccountCard, useAppSelector } from 'app/hooks';
import { AppRoutes } from 'app/AppRoutes';

import globalStyles from '../style.module.css';

export const WithdrawTable = memo(({ handleFilters }: TTable) => {
    const { data } = useAppSelector(state => state.PartnersState.agentCashOutRequestListReducer);
    const { closeDialogHandler, openDialogHandler, dialogAccountNumber, isOpen } = useAccountCard();

    const onDataSelectHandler = (args: SelectDataCommonDataModel<void>, _: string | null, initiator: SelectDataInitiator) => {
        handleFilters({
            pageNumber: initiator === 'sorting-changed' ? 1 : args.pageNumber,
            sortType: args.sortingData?.sortKind,
            sortFieldName: args.sortingData?.fieldName
        });
    };

    const openWithdrawRequest = (id: string) => {
        window.open(`${ AppRoutes.partners.editAgentCashOut }?agentCashOutRequestId=${ id }`);
    };

    return (
        <>
            <AccountCard isOpen={ isOpen } onCancelDialogButtonClick={ closeDialogHandler } accountNumber={ dialogAccountNumber } />
            <CommonTableWithFilter
                isBorderStyled={ true }
                isResponsiveTable={ true }
                uniqueContextProviderStateId="WithdrawingList"
                onDataSelect={ onDataSelectHandler }
                tableProps={ {
                    dataset: data ? data.rawData.chunkDataOfPagination : [],
                    keyFieldName: 'id',
                    emptyText: 'Заявки на вывод средств не найдены',
                    pagination: data
                        ? {
                            totalPages: Math.ceil(data.rawData.totalCount / data.rawData.pageSize),
                            pageSize: data.rawData.pageSize,
                            currentPage: data.rawData.pageNumber,
                            recordsCount: data.rawData.totalCount
                        }
                        : undefined,
                    fieldsView: {
                        requestNumber: {
                            caption: '№ Заявки',
                            isSortable: true,
                            format: (value: string, withdrawData) =>
                                <Link onClick={ () => openWithdrawRequest(withdrawData.id) }>
                                    { value }
                                </Link>
                        },
                        creationDateTime: {
                            caption: 'Дата создания',
                            isSortable: true,
                            format: (value: string) =>
                                setDateFormat(value)
                        },
                        partnerAccountIndexNumber: {
                            caption: 'Партнер (ID, Название)',
                            isSortable: true,
                            format: (value: number, withdrawData) =>
                                <Link onClick={ () => openDialogHandler(value) }>
                                    { `${ value }, ${ withdrawData.partnerAccountCaption }` }
                                </Link>
                        },
                        requisiteType: {
                            caption: 'Реквизиты',
                            isSortable: true,
                            format: (_value, item) => item.agentRequisites
                        },
                        requestStatus: {
                            caption: 'Статус',
                            isSortable: true,
                            format: (value: ERequestStatus) => {
                                switch (value) {
                                    case ERequestStatus.operation:
                                        return <Chip color="info" label="В работе" className={ globalStyles.chip } />;
                                    case ERequestStatus.new:
                                        return <Chip color="warning" label="Новая" className={ globalStyles.chip } />;
                                    case ERequestStatus.paid:
                                        return <Chip color="primary" label="Оплачена" className={ globalStyles.chip } />;
                                    default:
                                        break;
                                }
                            }
                        },
                        statusDateTime: {
                            caption: 'Дата статуса',
                            isSortable: true,
                            format: (value: string | null) =>
                                setDateFormat(value)
                        },
                        requestedSum: {
                            caption: 'Сумма',
                            isSortable: true,
                            format: (value: number) =>
                                setCashFormat(value)
                        },
                    }
                } }
            />
        </>
    );
});