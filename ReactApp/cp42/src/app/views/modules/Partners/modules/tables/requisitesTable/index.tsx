import React, { memo } from 'react';
import { Chip, Link as MuiLink } from '@mui/material';

import { TTable } from 'app/views/modules/Partners/config';
import { CommonTableWithFilter, SelectDataInitiator } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { setDateFormat } from 'app/common/helpers';
import { EAgentRequisitesStatus } from 'app/api/endpoints/partners/enum';
import { SelectDataCommonDataModel } from 'app/web/common';
import { AccountCard } from 'app/views/modules/_common/reusable-modules/AccountCard';
import { AppRoutes } from 'app/AppRoutes';
import { useAccountCard, useAppSelector } from 'app/hooks';

import globalStyles from '../style.module.css';

export const RequisitesTable = memo(({ handleFilters }: TTable) => {
    const { data } = useAppSelector(state => state.PartnersState.agentRequisitesListReducer);

    const { closeDialogHandler, openDialogHandler, dialogAccountNumber, isOpen } = useAccountCard();

    const onDataSelectHandler = (args: SelectDataCommonDataModel<void>, _: string | null, initiator: SelectDataInitiator) => {
        handleFilters({
            pageNumber: initiator === 'sorting-changed' ? 1 : args.pageNumber,
            sortType: args.sortingData?.sortKind,
            sortFieldName: args.sortingData?.fieldName
        });
    };

    const openRequisiteRequestHandler = (id: string) => {
        window.open(`${ AppRoutes.partners.editAgentRequisites }?agentRequisitesId=${ id }`, '_blank');
    };

    return (
        <>
            <AccountCard isOpen={ isOpen } onCancelDialogButtonClick={ closeDialogHandler } accountNumber={ dialogAccountNumber } />
            <CommonTableWithFilter
                isBorderStyled={ true }
                isResponsiveTable={ true }
                uniqueContextProviderStateId="RequisitesList"
                onDataSelect={ onDataSelectHandler }
                tableProps={ {
                    dataset: data ? data.rawData.chunkDataOfPagination : [],
                    keyFieldName: 'id',
                    emptyText: 'Реквизиты не найдены',
                    pagination: data
                        ? {
                            totalPages: Math.ceil(data.rawData.totalCount / data.rawData.pageSize),
                            pageSize: data.rawData.pageSize,
                            currentPage: data.rawData.pageNumber,
                            recordsCount: data.rawData.totalCount
                        }
                        : undefined,
                    fieldsView: {
                        partnerAccountCaption: {
                            caption: 'Получатель',
                            isSortable: true,
                            format: (value: string, requisiteData) =>
                                <MuiLink onClick={ () => openRequisiteRequestHandler(requisiteData.id) }>
                                    { value ?? '---' }
                                </MuiLink>
                        },
                        creationDate: {
                            caption: 'Дата создания',
                            isSortable: true,
                            format: (value: string) =>
                                setDateFormat(value)
                        },
                        agentRequisitesType: {
                            caption: 'Реквизиты',
                            isSortable: true,
                            format: (_value, item) => item.agentRequisitesTypeString
                        },
                        partnerAccountIndexNumber: {
                            caption: 'Партнер (ID)',
                            isSortable: true,
                            format: (value: number) =>
                                <MuiLink onClick={ () => openDialogHandler(value) }>
                                    { value }
                                </MuiLink>
                        },
                        agentRequisitesStatus: {
                            caption: 'Статус',
                            isSortable: true,
                            format: (value: EAgentRequisitesStatus) => {
                                switch (value) {
                                    case EAgentRequisitesStatus.draft:
                                        return <Chip className={ globalStyles.chip } label="Черновик" color="warning" />;
                                    case EAgentRequisitesStatus.onCheck:
                                        return <Chip className={ globalStyles.chip } label="На проверке" color="info" />;
                                    case EAgentRequisitesStatus.verified:
                                        return <Chip className={ globalStyles.chip } label="Проверено" color="primary" />;
                                }
                            }
                        },
                        statusDateTime: {
                            caption: 'Дата статуса',
                            isSortable: true,
                            format: (value: string) =>
                                setDateFormat(value)
                        }
                    }
                } }
            />
        </>
    );
});