import React, { memo } from 'react';

import { TTable } from 'app/views/modules/Partners/config';
import { CommonTableWithFilter, SelectDataInitiator } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { formatAccountCaption, formatAgentPaymentSourceType, formatClientPaymentSum, formatTransactionDate, formatTransactionSum, formatVipAccount } from 'app/views/modules/_common/shared/PartnersPages';
import { ETransactionType } from 'app/api/endpoints/partners/enum';
import { AccountCard } from 'app/views/modules/_common/reusable-modules/AccountCard';
import { useAccountCard, useAppSelector } from 'app/hooks';
import { SelectDataCommonDataModel } from 'app/web/common';

export const TransactionsTable = memo(({ handleFilters }: TTable) => {
    const { closeDialogHandler, openDialogHandler, dialogAccountNumber, isOpen } = useAccountCard();
    const { data } = useAppSelector(state => state.PartnersState.agentPaymentListReducer);

    const onDataSelectHandler = (args: SelectDataCommonDataModel<void>, _: string | null, initiator: SelectDataInitiator) => {
        handleFilters({
            pageNumber: initiator === 'sorting-changed' ? 1 : args.pageNumber,
            sortType: args.sortingData?.sortKind,
            sortFieldName: args.sortingData?.fieldName
        });
    };

    return (
        <>
            <AccountCard isOpen={ isOpen } onCancelDialogButtonClick={ closeDialogHandler } accountNumber={ dialogAccountNumber } />
            <CommonTableWithFilter
                isResponsiveTable={ true }
                isBorderStyled={ true }
                uniqueContextProviderStateId="ClientList"
                onDataSelect={ onDataSelectHandler }
                tableProps={ {
                    dataset: data ? data.rawData.chunkDataOfPagination : [],
                    keyFieldName: 'accountId',
                    isIndexToKeyFieldName: true,
                    emptyText: 'Транзакции не найдены',
                    pagination: data
                        ? {
                            totalPages: Math.ceil(data.rawData.totalCount / data.rawData.pageSize),
                            pageSize: data.rawData.pageSize,
                            currentPage: data.rawData.pageNumber,
                            recordsCount: data.rawData.totalCount
                        }
                        : undefined,
                    fieldsView: {
                        transactionDate: {
                            caption: 'Дата и время',
                            isSortable: true,
                            format: formatTransactionDate,
                        },
                        accountIndexNumber: {
                            caption: 'От кого (клиент)',
                            isSortable: true,
                            format: (value: number, transactionData) =>
                                formatAccountCaption(`${ value }, ${ transactionData.accountCaption }`, transactionData, openDialogHandler, value)
                        },
                        partnerAccountIndexNumber: {
                            caption: 'Кому (партнер)',
                            isSortable: true,
                            format: (value: number, transactionData) =>
                                formatAccountCaption(`${ value }, ${ transactionData.partnerAccountCaption }`, transactionData, openDialogHandler, value, true),
                        },
                        operation: {
                            caption: 'Операция',
                            isSortable: true,
                            format: formatVipAccount
                        },
                        clientPaymentSum: {
                            caption: 'Списание от клиента',
                            isSortable: true,
                            format: formatClientPaymentSum
                        },
                        transactionType: {
                            caption: 'Счет',
                            isSortable: true,
                            format: (_: ETransactionType, transactionData) =>
                                formatAgentPaymentSourceType(transactionData.agentPaymentSourceType, transactionData)
                        },
                        transactionSum: {
                            caption: 'Вознаграждение',
                            isSortable: true,
                            format: formatTransactionSum
                        }
                    }
                } }
            />
        </>
    );
});