import React, { useEffect, useState } from 'react';

import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { DialogView } from 'app/views/components/controls/Dialog/views/DialogView/Index';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { ContainedButton, SuccessButton } from 'app/views/components/controls/Button';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { AutocompleteComboBoxForm } from 'app/views/components/controls/forms/ComboBox/AutocompleteComboBoxForm';
import { FETCH_API } from 'app/api/useFetchApi';
import { useAppDispatch, useDebounce } from 'app/hooks';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { ComboboxItemModel } from 'app/views/components/controls/forms/ComboBox/models';

import { REDUX_API } from 'app/api/useReduxApi';
import { TabsControl } from 'app/views/components/controls/TabsControl';
import { THeaders } from 'app/views/components/controls/TabsControl/views/TabView';
import dialogStyles from '../style.module.css';

type TConnectionDialog = FloatMessageProps & {
    isOpen: boolean;
    setIsOpen: React.Dispatch<React.SetStateAction<boolean>>;
};

const { searchAccounts, bindAccounts, unbindAccounts } = FETCH_API.PARTNERS;
const { getClients } = REDUX_API.PARTNERS_REDUX;

const enum ECurrentTab {
    connect,
    disconnect
}

const HEADERS: THeaders = [
    {
        title: 'Подключение',
        isVisible: true
    },
    {
        title: 'Отключение',
        isVisible: true
    }
];

export const ConnectionDialog = withFloatMessages(({ isOpen, setIsOpen, floatMessage: { show } }: TConnectionDialog) => {
    const dispatch = useAppDispatch();

    const [clientId, setClientId] = useState('');
    const [partnerId, setPartnerId] = useState('');
    const [currentTab, setCurrentTab] = useState<ECurrentTab>(ECurrentTab.connect);

    const [clientsSearchString, setClientsSearchString] = useState<string | null>(null);
    const [clientsSearchResult, setClientsSearchResult] = useState<ComboboxItemModel<string>[]>([]);
    const [isClientsSearchLoading, setIsClientsSearchLoading] = useState(false);
    const debouncedClientsSearchString = useDebounce(clientsSearchString);

    const [partnersSearchString, setPartnersSearchString] = useState<string | null>(null);
    const [partnersSearchResult, setPartnersSearchResult] = useState<ComboboxItemModel<string>[]>([]);
    const [isPartnersSearchLoading, setIsPartnersSearchLoading] = useState(false);
    const debouncedPartnersSearchString = useDebounce(partnersSearchString);

    useEffect(() => {
        if (!isOpen) {
            setClientsSearchString(null);
            setClientsSearchResult([]);
            setClientId('');
            setPartnersSearchString(null);
            setPartnersSearchResult([]);
            setPartnerId('');
        }
    }, [isOpen]);

    useEffect(() => {
        if (debouncedPartnersSearchString !== null && isOpen) {
            (async () => {
                setIsPartnersSearchLoading(true);
                const { data, error } = await searchAccounts(debouncedPartnersSearchString);

                if (data && data.rawData) {
                    setPartnersSearchResult(data.rawData.map(partner => ({
                        text: partner.accountCaption,
                        value: partner.accountId
                    })));
                } else {
                    show(MessageType.Error, error);
                }

                setIsPartnersSearchLoading(false);
            })();
        }
    }, [debouncedPartnersSearchString, isOpen, show]);

    useEffect(() => {
        if (debouncedClientsSearchString !== null && isOpen) {
            (async () => {
                const { data, error } = await searchAccounts(debouncedClientsSearchString);

                if (data && data.rawData) {
                    setClientsSearchResult(data.rawData.map(client => ({
                        text: client.accountCaption,
                        value: client.accountId
                    })));
                } else {
                    show(MessageType.Error, error);
                }

                setIsClientsSearchLoading(false);
            })();
        }
    }, [debouncedClientsSearchString, isOpen, show]);

    const cancelClickHandler = () => {
        setIsOpen(false);
    };

    const partnerIdHandler = (_: string, id: string) => {
        setPartnerId(id);
    };

    const clientIdHandler = (_: string, id: string) => {
        setClientId(id);
    };

    const partnersSearchHandler = (_: string, searchLine: string, __: string) => {
        setPartnersSearchString(searchLine);
    };

    const clientsSearchHandler = (_: string, searchLine: string, __: string) => {
        setClientsSearchString(searchLine);
    };

    const onConnectHandler = async () => {
        if (partnerId === clientId) {
            return show(MessageType.Error, 'Аккаунты для привязки должны быть разными');
        }

        const { error } = await bindAccounts({
            partnerAccountId: partnerId,
            clientAccountId: clientId
        });

        if (error && error !== 'Успешно!') {
            show(MessageType.Error, error);
        } else {
            show(MessageType.Success, 'Аккаунт успешно подключен');
            getClients({
                pageNumber: 1
            }, dispatch);
            cancelClickHandler();
        }
    };

    const onDisconnectHandler = async () => {
        const { error } = await unbindAccounts(clientId);

        if (error && error !== 'Успешно!') {
            show(MessageType.Error, error);
        } else {
            show(MessageType.Success, 'Аккаунт успешно отключен');
            getClients({
                pageNumber: 1
            }, dispatch);
            cancelClickHandler();
        }
    };

    const activeTabIndexHandler = (index: number) => {
        setCurrentTab(index);
    };

    const renderButtons = () => {
        return (
            <div className={ dialogStyles.buttons }>
                <ContainedButton onClick={ cancelClickHandler }>Отмена</ContainedButton>
                { currentTab === ECurrentTab.connect &&
                    <SuccessButton onClick={ onConnectHandler }>Подключить</SuccessButton>
                }
                { currentTab === ECurrentTab.disconnect &&
                    <ContainedButton kind="error" onClick={ onDisconnectHandler }>Отключить</ContainedButton>
                }
            </div>
        );
    };

    return (
        <DialogView
            title="Подключение/Отключение аккаунта к партнеру"
            isTitleSmall={ true }
            isOpen={ isOpen }
            dialogWidth="xs"
            onCancelClick={ cancelClickHandler }
            buttons={ renderButtons }
            className={ dialogStyles.dialog }
        >
            <TabsControl headers={ HEADERS } onActiveTabIndexChanged={ activeTabIndexHandler } activeTabIndex={ currentTab }>
                <div className={ dialogStyles.container }>
                    <FormAndLabel label="Партнер">
                        <AutocompleteComboBoxForm
                            placeholder="Введите название или ID аккаунта партнера"
                            isClearable={ true }
                            isLoading={ isPartnersSearchLoading }
                            noItemsText="Не найдено"
                            items={ partnersSearchResult }
                            formName=""
                            value={ partnerId }
                            inputValue={ partnersSearchString ?? '' }
                            raiseInputValueChangeEvents={ true }
                            onValueChange={ partnerIdHandler }
                            onInputValueChange={ partnersSearchHandler }
                        />
                    </FormAndLabel>
                    <FormAndLabel label="Клиент">
                        <AutocompleteComboBoxForm
                            placeholder="Введите название или ID аккаунта клиента"
                            isClearable={ true }
                            isLoading={ isClientsSearchLoading }
                            noItemsText="Не найдено"
                            items={ clientsSearchResult }
                            formName=""
                            value={ clientId }
                            inputValue={ clientsSearchString ?? '' }
                            raiseInputValueChangeEvents={ true }
                            onValueChange={ clientIdHandler }
                            onInputValueChange={ clientsSearchHandler }
                        />
                    </FormAndLabel>
                </div>
                <div className={ dialogStyles.container }>
                    <FormAndLabel label="Клиент">
                        <AutocompleteComboBoxForm
                            placeholder="Введите название или ID аккаунта клиента"
                            isClearable={ true }
                            isLoading={ isClientsSearchLoading }
                            noItemsText="Не найдено"
                            items={ clientsSearchResult }
                            formName=""
                            value={ clientId }
                            inputValue={ clientsSearchString ?? '' }
                            raiseInputValueChangeEvents={ true }
                            onValueChange={ clientIdHandler }
                            onInputValueChange={ clientsSearchHandler }
                        />
                    </FormAndLabel>
                </div>
            </TabsControl>
        </DialogView>
    );
});