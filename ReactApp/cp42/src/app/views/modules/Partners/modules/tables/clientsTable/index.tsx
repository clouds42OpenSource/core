import { AccountUserGroup } from 'app/common/enums';
import React, { memo, useState } from 'react';
import { Chip, Link } from '@mui/material';

import { CommonTableWithFilter, SelectDataInitiator } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { SuccessButton } from 'app/views/components/controls/Button';
import { setCashFormat, setDateFormat } from 'app/common/helpers';
import { useAccountCard, useAppSelector } from 'app/hooks';
import { ConnectionDialog } from 'app/views/modules/Partners/modules';
import { SelectDataCommonDataModel } from 'app/web/common';
import { TTable } from 'app/views/modules/Partners/config';
import { AccountCard } from 'app/views/modules/_common/reusable-modules/AccountCard';

import globalStyles from '../style.module.css';

export const ClientsTable = memo(({ handleFilters }: TTable) => {
    const { data } = useAppSelector(state => state.PartnersState.clientListReducer);
    const { currentUserGroups } = useAppSelector(state => state.Global.getCurrentSessionSettingsReducer.settings.currentContextInfo);
    const { closeDialogHandler, openDialogHandler, dialogAccountNumber, isOpen } = useAccountCard();

    const [isOpenConnectionDialog, setIsOpenConnectionDialog] = useState(false);

    const onDataSelectHandler = (args: SelectDataCommonDataModel<void>, _: string | null, initiator: SelectDataInitiator) => {
        handleFilters({
            pageNumber: initiator === 'sorting-changed' ? 1 : args.pageNumber,
            sortType: args.sortingData?.sortKind,
            sortFieldName: args.sortingData?.fieldName
        });
    };

    const openConnectionDialogHandler = () => {
        setIsOpenConnectionDialog(true);
    };

    return (
        <>
            <AccountCard isOpen={ isOpen } onCancelDialogButtonClick={ closeDialogHandler } accountNumber={ dialogAccountNumber } />
            <ConnectionDialog isOpen={ isOpenConnectionDialog } setIsOpen={ setIsOpenConnectionDialog } />
            <CommonTableWithFilter
                isBorderStyled={ true }
                isResponsiveTable={ true }
                uniqueContextProviderStateId="ClientList"
                onDataSelect={ onDataSelectHandler }
                tableProps={ {
                    dataset: data ? data.rawData.chunkDataOfPagination : [],
                    keyFieldName: 'accountId',
                    isIndexToKeyFieldName: true,
                    emptyText: 'Клиенты не найдены',
                    pagination: data
                        ? {
                            totalPages: Math.ceil(data.rawData.totalCount / data.rawData.pageSize),
                            pageSize: data.rawData.pageSize,
                            currentPage: data.rawData.pageNumber,
                            recordsCount: data.rawData.totalCount,
                            leftAlignContent: (
                                <div>
                                    { currentUserGroups.includes(AccountUserGroup.CloudAdmin) && <SuccessButton onClick={ openConnectionDialogHandler }>Клиент и партнер</SuccessButton> }
                                </div>
                            )
                        }
                        : undefined,
                    fieldsView: {
                        partnerAccountIndexNumber: {
                            caption: 'Партнер (ID, Название)',
                            isSortable: true,
                            format: (value: string, clientData) =>
                                <Link onClick={ () => openDialogHandler(clientData.partnerAccountIndexNumber) }>
                                    { `${ value }, ${ clientData.partnerAccountCaption }` }
                                </Link>
                        },
                        accountIndexNumber: {
                            caption: 'Клиент (ID, Название)',
                            isSortable: true,
                            format: (value: string, clientData) =>
                                <>
                                    <Link onClick={ () => openDialogHandler(clientData.accountIndexNumber) }>
                                        { `${ value }, ${ clientData.accountCaption }` }
                                    </Link>
                                    { clientData.isVipAccount && <span className={ globalStyles.vipText }>{ ' VIP' }</span> }
                                </>
                        },
                        serviceName: {
                            caption: 'Сервис',
                            isSortable: true,
                        },
                        clientActivationDate: {
                            caption: 'Дата подключения',
                            isSortable: true,
                            format: (value: string) =>
                                setDateFormat(value)
                        },
                        serviceExpireDate: {
                            caption: 'Дата окончания сервиса',
                            isSortable: true,
                            format: (value: string) =>
                                setDateFormat(value)
                        },
                        serviceIsActiveForClient: {
                            caption: 'Активность',
                            isSortable: true,
                            format: (value: boolean) =>
                                <Chip
                                    color={ value ? 'primary' : 'error' }
                                    label={ value ? 'Активен' : 'Неактивен' }
                                    className={ globalStyles.chip }
                                />
                        },
                        isDemoPeriod: {
                            caption: 'Демо период',
                            isSortable: true,
                            format: (value: boolean) =>
                                value ? <span className={ globalStyles.demoText }>Демо</span> : null
                        },
                        monthlyBonus: {
                            caption: 'Вознаграждение (мес.)',
                            isSortable: true,
                            format: (value: number) =>
                                setCashFormat(value)
                        }
                    }
                } }
            />
        </>
    );
});