import { AppRoutes } from 'app/AppRoutes';
import React, { memo } from 'react';
import { Chip, Link as MuiLink } from '@mui/material';

import { TTable } from 'app/views/modules/Partners/config';
import { SelectDataCommonDataModel } from 'app/web/common';
import { CommonTableWithFilter, SelectDataInitiator } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { useAppSelector } from 'app/hooks';
import { setCashFormat, setDateFormat } from 'app/common/helpers';
import { ERecalculationTaskStatus } from 'app/api/endpoints/partners/enum';

import globalStyles from '../style.module.css';

export const RecalculationTable = memo(({ handleFilters }: TTable) => {
    const { data } = useAppSelector(state => state.PartnersState.recalculationServiceCostListReducer);

    const onDataSelectHandler = (args: SelectDataCommonDataModel<void>, _: string | null, initiator: SelectDataInitiator) => {
        handleFilters({
            pageNumber: initiator === 'sorting-changed' ? 1 : args.pageNumber,
            sortType: args.sortingData?.sortKind,
            sortFieldName: args.sortingData?.fieldName
        });
    };

    const openServiceCardHandler = (id: string) => {
        window.open(`${ AppRoutes.services.myServices }/${ id }`, '_blank');
    };

    const recalculationStatusFormatter = (status: ERecalculationTaskStatus) => {
        switch (status) {
            case ERecalculationTaskStatus.error:
                return <Chip className={ globalStyles.chip } label="Ошибка" color="error" />;
            case ERecalculationTaskStatus.new:
                return <Chip label="Новая" color="default" />;
            case ERecalculationTaskStatus.processing:
                return <Chip className={ globalStyles.chip } label="В процессе выполнения" color="success" />;
            case ERecalculationTaskStatus.ready:
                return <Chip className={ globalStyles.chip } label="Готова" color="primary" />;
            case ERecalculationTaskStatus.needRetry:
                return <Chip className={ globalStyles.chip } label="Будет перезапущена" color="warning" />;
            case ERecalculationTaskStatus.captured:
                return <Chip className={ globalStyles.chip } label="Захвачена воркером" color="info" />;
            default:
                return null;
        }
    };

    return (
        <CommonTableWithFilter
            isBorderStyled={ true }
            isResponsiveTable={ true }
            uniqueContextProviderStateId="RecalculationServiceCostList"
            onDataSelect={ onDataSelectHandler }
            tableProps={ {
                emptyText: 'Записи о пересчете не найдены',
                dataset: data ? data.rawData.chunkDataOfPagination : [],
                keyFieldName: 'serviceId',
                fieldsView: {
                    creationDate: {
                        caption: 'Дата создания',
                        isSortable: true,
                        format: (value: string) =>
                            setDateFormat(value)
                    },
                    recalculationDate: {
                        caption: 'Дата пересчета стоимости',
                        isSortable: true,
                        format: (value: string) =>
                            setDateFormat(value)
                    },
                    serviceName: {
                        caption: 'Сервис',
                        isSortable: true,
                        format: (value: string, serviceData) =>
                            <MuiLink onClick={ () => openServiceCardHandler(serviceData.serviceId) }>
                                { value }
                            </MuiLink>
                    },
                    oldServiceCost: {
                        caption: 'Старая стоимость',
                        isSortable: true,
                        format: (value: number) =>
                            setCashFormat(value)
                    },
                    newServiceCost: {
                        caption: 'Новая стоимость',
                        isSortable: true,
                        format: (value: number) =>
                            setCashFormat(value)
                    },
                    queueStatus: {
                        caption: 'Статус задачи',
                        isSortable: true,
                        format: (value: ERecalculationTaskStatus) =>
                            recalculationStatusFormatter(value)
                    },
                    comment: {
                        caption: 'Комментарий',
                        isSortable: true
                    }
                }
            } }
        />
    );
});