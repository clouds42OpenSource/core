import { AppRoutes } from 'app/AppRoutes';
import React, { memo } from 'react';
import { Chip, Link as MuiLink } from '@mui/material';

import { TextOut } from 'app/views/components/TextOut';
import { setCashFormat, setDateFormat } from 'app/common/helpers';
import { EBillingServiceStatus } from 'app/api/endpoints/partners/enum';
import { CommonTableWithFilter, SelectDataInitiator } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { TTable } from 'app/views/modules/Partners/config';
import { SelectDataCommonDataModel } from 'app/web/common';
import { AccountCard } from 'app/views/modules/_common/reusable-modules/AccountCard';
import { useAccountCard, useAppSelector } from 'app/hooks';

import globalStyles from '../style.module.css';

export const ServicesTable = memo(({ handleFilters }: TTable) => {
    const { closeDialogHandler, openDialogHandler, dialogAccountNumber, isOpen } = useAccountCard();
    const { data } = useAppSelector(state => state.PartnersState.servicesListReducer);

    const onDataSelectHandler = (args: SelectDataCommonDataModel<void>, _: string | null, initiator: SelectDataInitiator) => {
        handleFilters({
            pageNumber: initiator === 'sorting-changed' ? 1 : args.pageNumber,
            sortType: args.sortingData?.sortKind,
            sortFieldName: args.sortingData?.fieldName
        });
    };

    const openServiceCardHandler = (id: string) => {
        window.open(`${ AppRoutes.services.myServices }/${ id }`, '_blank');
    };

    return (
        <>
            <AccountCard isOpen={ isOpen } onCancelDialogButtonClick={ closeDialogHandler } accountNumber={ dialogAccountNumber } />
            <CommonTableWithFilter
                isBorderStyled={ true }
                isResponsiveTable={ true }
                uniqueContextProviderStateId="ServicesList"
                onDataSelect={ onDataSelectHandler }
                tableProps={ {
                    dataset: data ? data.rawData.chunkDataOfPagination : [],
                    keyFieldName: 'id',
                    isIndexToKeyFieldName: true,
                    emptyText: 'Сервисы не найдены',
                    pagination: data
                        ? {
                            totalPages: Math.ceil(data.rawData.totalCount / data.rawData.pageSize),
                            pageSize: data.rawData.pageSize,
                            currentPage: data.rawData.pageNumber,
                            recordsCount: data.rawData.totalCount
                        }
                        : undefined,
                    fieldsView: {
                        partnerAccountIndexNumber: {
                            caption: 'Партнер (ID, Название)',
                            isSortable: true,
                            format: (value: number, serviceData) =>
                                <MuiLink onClick={ () => openDialogHandler(value) }>
                                    { `${ value }, ${ serviceData.partnerAccountCaption }` }
                                </MuiLink>
                        },
                        name: {
                            caption: 'Название',
                            isSortable: true,
                            format: (value: string, serviceData) =>
                                <div className={ globalStyles.serviceName }>
                                    <MuiLink onClick={ () => openServiceCardHandler(serviceData.id) }>
                                        { value }
                                    </MuiLink>
                                    { !serviceData.isActive && <TextOut fontSize={ 10 } className={ globalStyles.warningText }>(Отключен)</TextOut> }
                                </div>
                        },
                        serviceActivationDate: {
                            caption: 'Дата активации',
                            isSortable: true,
                            format: (value: string | null) =>
                                setDateFormat(value)
                        },
                        countUsers: {
                            caption: 'Пользователи',
                            isSortable: true,
                            format: (value: number) =>
                                <><i className="fa fa-user" aria-hidden="true" />{ ' ' }{ value }</>
                        },
                        billingServiceStatus: {
                            caption: 'Статус',
                            isSortable: true,
                            format: (value: EBillingServiceStatus) => {
                                switch (value) {
                                    case EBillingServiceStatus.draft:
                                        return <Chip color="warning" label="Черновик" className={ globalStyles.chip } />;
                                    case EBillingServiceStatus.moderate:
                                        return <Chip color="info" label="На модерации" className={ globalStyles.chip } />;
                                    case EBillingServiceStatus.active:
                                        return <Chip color="primary" label="Активен" className={ globalStyles.chip } />;
                                    default:
                                        break;
                                }
                            }
                        },
                        statusDateTime: {
                            caption: 'Дата статуса',
                            isSortable: true,
                            format: (value: string) =>
                                setDateFormat(value)
                        },
                        serviceCost: {
                            caption: 'Вознаграждение',
                            isSortable: true,
                            format: (value: number) =>
                                setCashFormat(value)
                        },
                    }
                } }
            />
        </>
    );
});