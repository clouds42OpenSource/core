import dayjs from 'dayjs';

export const dateFilterFormat = (value: Date | null) => value ? dayjs(value).format('YYYY-MM-DD') : '';