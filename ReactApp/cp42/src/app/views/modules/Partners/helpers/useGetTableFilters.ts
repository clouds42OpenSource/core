import { useCallback, useEffect, useState } from 'react';
import { TFilterTable } from 'app/views/modules/Partners/config';
import { SortingKind } from 'app/common/enums';
import { usePrevious } from 'app/hooks';
import { SelectDataCommonDataModel } from 'app/web/common';

type TGetTableFilters = {
    onDataSelectHandler: (args: SelectDataCommonDataModel<void>) => void;
};

export const useGetTableFilters = ({ onDataSelectHandler }: TGetTableFilters) => {
    const [tableFilters, setTableFilters] = useState<TFilterTable>({
        pageNumber: 1,
        sortFieldName: '',
        sortType: SortingKind.Desc
    });

    const previousFilters = usePrevious(tableFilters);

    useEffect(() => {
        const compareSortType = previousFilters?.sortType !== tableFilters.sortType;
        const compareSortFieldName = previousFilters?.sortFieldName !== tableFilters.sortFieldName;
        const comparePageNumber = previousFilters?.pageNumber !== tableFilters.pageNumber;
        const haveTableFilters = !!(tableFilters.sortFieldName && tableFilters.sortType !== undefined);

        if ((compareSortType || compareSortFieldName || comparePageNumber) && previousFilters) {
            onDataSelectHandler({
                pageNumber: tableFilters.pageNumber,
                sortingData: haveTableFilters
                    ? {
                        fieldName: tableFilters.sortFieldName!,
                        sortKind: tableFilters.sortType!
                    }
                    : undefined
            });
        }
    }, [tableFilters, onDataSelectHandler, previousFilters]);

    const handleTableFilters = useCallback((filters: TFilterTable) => {
        setTableFilters(filters);
    }, []);

    return { handleTableFilters };
};