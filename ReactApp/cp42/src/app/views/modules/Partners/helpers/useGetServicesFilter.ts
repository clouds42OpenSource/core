import { ReactNode, useEffect, useState } from 'react';

import { ComboboxItemModel } from 'app/views/components/controls/forms/ComboBox/models';
import { useDebounce } from 'app/hooks';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FETCH_API } from 'app/api/useFetchApi';

const { searchServices } = FETCH_API.PARTNERS;

export const useGetServicesFilter = (show: (type: MessageType, content: ReactNode, autoHideDuration?: number) => void) => {
    const [serviceId, setServiceId] = useState('');
    const [servicesSearchString, setServicesSearchString] = useState<string | null>(null);
    const [servicesSearchResult, setServicesSearchResult] = useState<ComboboxItemModel<string>[]>([]);
    const [isServicesSearchLoading, setIsServicesSearchLoading] = useState(false);
    const debouncedServicesSearchString = useDebounce(servicesSearchString);

    useEffect(() => {
        if (debouncedServicesSearchString !== null) {
            (async () => {
                setIsServicesSearchLoading(true);
                const { data, error: searchError } = await searchServices(debouncedServicesSearchString);

                if (data && data.rawData) {
                    setServicesSearchResult(data.rawData.map(partner => ({
                        text: partner.text,
                        value: partner.value
                    })));
                } else {
                    show(MessageType.Error, searchError);
                }

                setIsServicesSearchLoading(false);
            })();
        }
    }, [debouncedServicesSearchString, show]);

    const serviceIdHandler = (_: string, id: string) => {
        setServiceId(id);
    };

    const serviceSearchHandler = (_: string, searchLine: string, __: string) => {
        setServicesSearchString(searchLine);
    };

    return { serviceId, servicesSearchResult, isServicesSearchLoading, servicesSearchString, serviceIdHandler, serviceSearchHandler };
};