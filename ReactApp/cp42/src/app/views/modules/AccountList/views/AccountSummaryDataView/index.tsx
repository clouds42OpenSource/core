import { hasComponentChangesFor } from 'app/common/functions';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { TextOut } from 'app/views/components/TextOut';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { AccountSummaryDataModel } from 'app/web/InterlayerApiProxy/AccountApiProxy/getAccountSummaryData/data-models';
import cn from 'classnames';
import { RequestKind } from 'core/requestSender/enums';
import React from 'react';
import css from '../styles.module.css';

type OwnState = {
    isInLoadingState: boolean;
    accountSummaryData: AccountSummaryDataModel | null;
};

const SummaryItem = ({ header, children }: {
    header: string;
    children: React.ReactNode;
}) => (
    <li>
        <TextOut fontSize={ 13 } fontWeight={ 400 }>
            { header } { children }
        </TextOut>
    </li>
);

export class AccountSummaryDataView extends React.Component<{}, OwnState> {
    public constructor(props: {}) {
        super(props);
        this.state = {
            isInLoadingState: false,
            accountSummaryData: null
        };
    }

    public async componentDidMount() {
        const api = InterlayerApiProxy.getAccountApi();

        this.setState({
            isInLoadingState: true
        });
        try {
            const summaryData = await api.getAccountSummaryData(RequestKind.SEND_BY_ROBOT);
            this.setState({
                accountSummaryData: summaryData,
                isInLoadingState: false
            });
        } catch {
            //
        }
    }

    public shouldComponentUpdate(nextProps: {}, nextState: OwnState) {
        return hasComponentChangesFor(this.props, nextProps) ||
            hasComponentChangesFor(this.state, nextState);
    }

    public render() {
        if (!this.state.accountSummaryData) {
            return <LoadingBounce />;
        }

        return (
            <>
                <br />
                <ul className={ cn(css['account-summary-list']) }>
                    <SummaryItem header="Зарегистрировано компаний:">
                        { this.state.accountSummaryData.registeredCompanyAmount }
                    </SummaryItem>
                    <SummaryItem header="Последняя регистрация:">
                        { this.state.accountSummaryData.lastRegistration }
                    </SummaryItem>
                    <SummaryItem header="Количество пользователей:">
                        { this.state.accountSummaryData.usersAmount }
                    </SummaryItem>
                    <SummaryItem header="Всего оплата сервисов:">
                        { this.state.accountSummaryData.totalPaymentByServices }
                    </SummaryItem>
                    <SummaryItem header="Сегодня:">
                        { this.state.accountSummaryData.registeredCompanyAmountToday }
                    </SummaryItem>
                    <SummaryItem header="Компания:">
                        { this.state.accountSummaryData.lastRegisteredCompany }
                    </SummaryItem>
                    <SummaryItem header="Количество баз:">
                        { this.state.accountSummaryData.databasesAmount }
                    </SummaryItem>
                    <SummaryItem header="За месяц:">
                        { this.state.accountSummaryData.monthTotalPayment }
                    </SummaryItem>
                </ul>
            </>
        );
    }
}