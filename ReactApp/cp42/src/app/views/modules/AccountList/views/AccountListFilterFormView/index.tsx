import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { AccountListFilterDataForm, AccountListFilterDataFormFieldNamesType } from 'app/views/modules/AccountList/types';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { AccountUserGroup } from 'app/common/enums';
import { CheckBoxForm } from 'app/views/components/controls/forms';
import { DateUtility } from 'app/utils';
import { DoubleDateInputForm } from 'app/views/components/controls/forms/DoubleDateInputView';
import React from 'react';
import { SearchByFilterButton } from 'app/views/components/controls/Button';
import { TextBoxForm } from 'app/views/components/controls/forms/TextBoxForm';
import { hasComponentChangesFor } from 'app/common/functions';
import { Box } from '@mui/material';

import styles from './style.module.css';

type OwnProps = ReduxFormProps<AccountListFilterDataForm> & {
    onFilterChanged: (filter: AccountListFilterDataForm, filterFormName: string) => void;
    isFilterFormDisabled: boolean;
    currentUserGroups: Array<AccountUserGroup>;
};

class AccountListFilterFormViewClass extends React.Component<OwnProps> {
    public constructor(props: OwnProps) {
        super(props);
        this.initReduxForm = this.initReduxForm.bind(this);
        this.handleEnterKey = this.handleEnterKey.bind(this);
        this.onValueChange = this.onValueChange.bind(this);
        this.applyFilter = this.applyFilter.bind(this);
    }

    public componentDidMount() {
        this.props.reduxForm.setInitializeFormDataAction(this.initReduxForm);
        document.body.addEventListener('keydown', this.handleEnterKey);
    }

    public shouldComponentUpdate(nextProps: OwnProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    public componentWillUnmount() {
        document.body.removeEventListener('keydown', this.handleEnterKey);
    }

    private handleEnterKey(event: KeyboardEvent) {
        if (event.key !== 'Enter') {
            return;
        }

        event.preventDefault();
        this.applyFilter();
    }

    private onValueChange<TValue, TForm extends string = AccountListFilterDataFormFieldNamesType>(formName: TForm, newValue: TValue) {
        this.props.reduxForm.updateReduxFormFields({
            [formName]: newValue
        });
    }

    private initReduxForm(): AccountListFilterDataForm | undefined {
        return {
            searchLine: '',
            onlyMine: null,
            registeredFrom: null,
            registeredTo: DateUtility.setDayEndTime(new Date())
        };
    }

    private applyFilter() {
        this.props.onFilterChanged({
            ...this.props.reduxForm.getReduxFormFields(false)
        }, '');
    }

    public render() {
        const formFields = this.props.reduxForm.getReduxFormFields(false);

        return (
            <>
                <Box display="flex" gap="10px" alignItems="flex-end" flexWrap="wrap">
                    <FormAndLabel label="Поиск">
                        <TextBoxForm
                            formName="searchLine"
                            onValueChange={ this.onValueChange }
                            value={ formFields.searchLine }
                            isReadOnly={ this.props.isFilterFormDisabled }
                            placeholder="Введите название, ИНН, телефон, email, имя, логин, счет..."
                        />
                    </FormAndLabel>
                    <FormAndLabel label="Период">
                        <DoubleDateInputForm
                            onValueChange={ this.onValueChange }
                            periodFromInputName="registeredFrom"
                            periodToInputName="registeredTo"
                            periodToValue={ formFields.registeredTo }
                            periodFromValue={ formFields.registeredFrom }
                            isReadOnly={ this.props.isFilterFormDisabled }
                        />
                    </FormAndLabel>
                    <SearchByFilterButton onClick={ this.applyFilter } />
                </Box>
                { this.props.currentUserGroups.indexOf(AccountUserGroup.AccountSaleManager) >= 0 && (
                    <CheckBoxForm
                        className={ styles.checkbox }
                        label="Только мои"
                        captionAsLabel={ true }
                        captionFontSize={ 13 }
                        captionFontWeight={ 600 }
                        formName="onlyMine"
                        onValueChange={ this.onValueChange }
                        isChecked={ formFields.onlyMine === true }
                        isReadOnly={ this.props.isFilterFormDisabled }
                    />
                ) }
            </>
        );
    }
}

export const AccountListFilterFormView = withReduxForm(AccountListFilterFormViewClass, {
    reduxFormName: 'AccountList_Filter_Form',
    resetOnUnmount: true
});