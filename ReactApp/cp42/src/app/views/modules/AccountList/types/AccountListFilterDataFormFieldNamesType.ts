import { AccountListFilterDataForm } from 'app/views/modules/AccountList/types/AccountListFilterDataForm';

export type AccountListFilterDataFormFieldNamesType = keyof AccountListFilterDataForm;