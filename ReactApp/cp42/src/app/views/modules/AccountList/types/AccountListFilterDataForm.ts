/**
 * Модель Redux формы для редактирования фильтра для таблицы списка аккаунтов
 */
export type AccountListFilterDataForm = {
    /**
     * Строка фильтра
     */
    searchLine: string;

    /**
     * Начальная дата регистрации аккаунта
     */
    registeredFrom: Date | null;

    /**
     * Конечная дата регистрации аккаунта
     */
    registeredTo: Date | null;

    /**
     * Индикатор что бы показать только мои аккаунты
     */
    onlyMine: boolean | null;
};