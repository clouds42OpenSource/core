import { Tooltip } from '@mui/material';
import { YandexMetrics, yandexCounter } from 'app/common/_external-services/yandexMetrics';
import { AccountUserGroup, SortingKind } from 'app/common/enums';
import { hasComponentChangesFor } from 'app/common/functions';
import { ProcessIdStateHelper } from 'app/common/helpers/ProcessIdStateHelper';
import { AccountListProcessId } from 'app/modules/accountList/AccountListProcessId';
import { ReceiveAccountListThunkParams } from 'app/modules/accountList/store/reducers/receiveAccountListReducer/params';
import {
    UpdateAccountInListThunkParams
} from 'app/modules/accountList/store/reducers/updateAccountInListReducer/params';
import { ReceiveAccountListThunk, UpdateAccountInListThunk } from 'app/modules/accountList/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { DateUtility } from 'app/utils';
import { TextOut } from 'app/views/components/TextOut';
import { withHeader } from 'app/views/components/_hoc/withHeader';
import { TextButton } from 'app/views/components/controls/Button';
import { TextWithTooltip } from 'app/views/components/controls/forms/Tooltips';
import { TooltipPlacementTypes } from 'app/views/components/controls/forms/Tooltips/types';
import {
    AccountListFilterDataForm,
    AccountListFilterDataFormFieldNamesType
} from 'app/views/modules/AccountList/types';
import { AccountListFilterFormView } from 'app/views/modules/AccountList/views/AccountListFilterFormView';
import { AccountSummaryDataView } from 'app/views/modules/AccountList/views/AccountSummaryDataView';
import { CommonTableWithFilter, SelectDataInitiator } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { TablePagination } from 'app/views/modules/_common/components/CommonTableWithFilter/TableView';
import { AccountCard } from 'app/views/modules/_common/reusable-modules/AccountCard';
import { AccountCardEditAccountDataForm } from 'app/views/modules/_common/reusable-modules/AccountCard/types';
import { AccountListItemDataModel } from 'app/web/InterlayerApiProxy/AccountApiProxy/receiveAccountList/data-models';
import { SelectDataCommonDataModel, SortingDataModel } from 'app/web/common/data-models';
import cn from 'classnames';
import React from 'react';
import { connect } from 'react-redux';
import css from './views/styles.module.css';

type StateProps<TDataRow = AccountListItemDataModel> = {
    dataset: TDataRow[];
    isInLoadingDataProcess: boolean;
    pagination: TablePagination;
    currentUserGroups: Array<AccountUserGroup>;
};

type DispatchProps = {
    dispatchReceiveAccountListThunk: (args: ReceiveAccountListThunkParams) => void;
    dispatchUpdateAccountInListThunk: (args: UpdateAccountInListThunkParams) => void;
};

type AllProps = StateProps & DispatchProps;

type OwnState = {
    isAccountCardVisible: boolean;
    accountNumber: number;
};

const VipStatus = ({ isVip }: { isVip: boolean }) => (isVip ? <span className={ cn(css.vip) }>VIP</span> : null);

class AccountListViewClass extends React.Component<AllProps, OwnState> {
    public constructor(props: AllProps) {
        super(props);
        this.onDataSelect = this.onDataSelect.bind(this);

        this.showAccountCard = this.showAccountCard.bind(this);
        this.hideAccountCard = this.hideAccountCard.bind(this);
        this.getSortQuery = this.getSortQuery.bind(this);
        this.onCommonAccountInfoChange = this.onCommonAccountInfoChange.bind(this);

        this.state = {
            isAccountCardVisible: false,
            accountNumber: 0
        };
    }

    public shouldComponentUpdate(nextProps: AllProps, nextState: OwnState) {
        return hasComponentChangesFor(this.props, nextProps) ||
            hasComponentChangesFor(this.state, nextState);
    }

    private onCommonAccountInfoChange(fields: AccountCardEditAccountDataForm) {
        this.props.dispatchUpdateAccountInListThunk({
            force: true,
            caption: fields.accountCaption,
            isVip: fields.isVip,
            accountId: fields.accountId
        });
    }

    private onDataSelect(args: SelectDataCommonDataModel<AccountListFilterDataForm>, changedFilterFormName: string | null, initiator: SelectDataInitiator) {
        this.props.dispatchReceiveAccountListThunk({
            pageNumber: args.pageNumber,
            filter: args.filter,
            orderBy: args.sortingData ? this.getSortQuery(args.sortingData) : 'registrationDate.asc desc.desc',
            force: true
        });
        if (initiator === 'filter-changed') {
            const formName = changedFilterFormName as AccountListFilterDataFormFieldNamesType;
            if (formName === 'registeredFrom' || formName === 'registeredTo') {
                yandexCounter.getInstance().reachGoal(YandexMetrics.ChoosePeriod);
            }
            yandexCounter.getInstance().reachGoal(YandexMetrics.SearchUser);
        }
    }

    private getSortQuery(args: SortingDataModel) {
        switch (args.fieldName) {
            case 'accountRegistrationDate':
                return `accountRegistrationDate.${ args.sortKind ? 'desc' : 'asc' }`;
            case 'rent1CExpiredDate':
                return `rent1CExpiredDate.${ args.sortKind ? 'desc' : 'asc' }`;
            default:
                return 'accountRegistrationDate.desc';
        }
    }

    private hideAccountCard() {
        this.setState({
            isAccountCardVisible: false,
            accountNumber: 0
        });
    }

    private showAccountCard(accountNumber: number) {
        return () => {
            this.setState({
                isAccountCardVisible: true,
                accountNumber
            });
        };
    }

    public render() {
        return (
            <>
                <CommonTableWithFilter
                    isResponsiveTable={ true }
                    uniqueContextProviderStateId="AccountDatabaseListContextProviderStateId"
                    filterProps={ {
                        getFilterContentView: (ref, onFilterChanged) => <AccountListFilterFormView
                            ref={ ref }
                            currentUserGroups={ this.props.currentUserGroups }
                            isFilterFormDisabled={ this.props.isInLoadingDataProcess }
                            onFilterChanged={ onFilterChanged }
                        />
                    } }
                    tableProps={ {
                        dataset: this.props.dataset,
                        keyFieldName: 'accountId',
                        sorting: {
                            sortFieldName: 'accountRegistrationDate',
                            sortKind: SortingKind.Desc
                        },
                        fieldsView: {
                            indexNumber: {
                                caption: '№',
                                fieldContentNoWrap: false,
                                fieldWidth: '3%',
                            },
                            accountCaption: {
                                caption: 'Аккаунт',
                                fieldContentNoWrap: false,
                                style: { lineHeight: '35px' },
                                fieldWidth: '30%',
                                format: (value, data) => {
                                    return (
                                        <TextButton
                                            onClick={ this.showAccountCard(data.indexNumber) }
                                            canSelectButtonContent={ true }
                                            wrapLongText={ true }
                                        >
                                            <TextOut fontSize={ 13 } className="text-link">
                                                { value }
                                                <VipStatus isVip={ data.isVip } />
                                            </TextOut>
                                        </TextButton>
                                    );
                                }
                            },
                            accountAdminLogin: {
                                caption: 'Логин',
                                fieldContentNoWrap: false,
                                fieldWidth: '20%'
                            },
                            accountAdminEmail: {
                                caption: 'Email',
                                fieldContentNoWrap: false,
                                fieldWidth: '20%'
                            },
                            accountUsersCount: {
                                caption: (
                                    <>
                                        <Tooltip title="Все пользователи" placement="top">
                                            <i className="fa fa-user" />
                                        </Tooltip>/
                                        <Tooltip title="Активные пользователи" placement="top">
                                            <i className="fa fa-user-plus" />
                                        </Tooltip>
                                    </>
                                ),
                                fieldContentNoWrap: true,
                                fieldWidth: '10%',
                                format: (value, data) => {
                                    return <TextOut style={ { wordBreak: 'normal' } }>{ value }/{ data.countOfActiveRent1CUsers }</TextOut>;
                                }
                            },
                            accountRegistrationDate: {
                                caption: 'Регистрация',
                                fieldContentNoWrap: false,
                                isSortable: true,
                                fieldWidth: '10%',
                                format: (value: Date) => {
                                    return (<TextWithTooltip
                                        text={ DateUtility.dateToDayMonthYear(value) }
                                        tooltipPlacement={ TooltipPlacementTypes.top }
                                        tooltipText={ DateUtility.dateToDayMonthYearHourMinuteSeconds(value) }
                                    />);
                                }
                            },
                            rent1CExpiredDate: {
                                caption: 'Аренда',
                                isSortable: true,
                                fieldContentNoWrap: false,
                                fieldWidth: '10%',
                                format: (value: Date) => {
                                    return value
                                        ? (
                                            <TextWithTooltip
                                                text={ DateUtility.dateToDayMonthYear(value) }
                                                tooltipPlacement={ TooltipPlacementTypes.top }
                                                tooltipText={ DateUtility.dateToDayMonthYearHourMinuteSeconds(value) }
                                            />
                                        )
                                        : '----------';
                                }
                            }
                        },
                        pagination: this.props.pagination
                    } }
                    onDataSelect={ this.onDataSelect }
                />
                <AccountSummaryDataView />
                <AccountCard
                    accountNumber={ this.state.accountNumber }
                    isOpen={ this.state.isAccountCardVisible }
                    onCancelDialogButtonClick={ this.hideAccountCard }
                    onCommonAccountInfoChange={ this.onCommonAccountInfoChange }
                />
            </>
        );
    }
}

const AccountListViewConnected = connect<StateProps, DispatchProps, {}, AppReduxStoreState>(
    state => {
        const accountListState = state.AccountListState;
        const dataset = accountListState.accountList.records;

        const currentPage = accountListState.accountList.metadata.pageNumber;
        const totalPages = accountListState.accountList.metadata.pageCount;
        const { pageSize } = accountListState.accountList.metadata;
        const recordsCount = accountListState.accountList.metadata.totalItemCount;

        const isInLoadingDataProcess = ProcessIdStateHelper.isInProgress(accountListState.reducerActions, AccountListProcessId.ReceiveAccountList);

        const hasSessionSettingsReceived = state.Global.getCurrentSessionSettingsReducer.hasSuccessFor.hasSettingsReceived;
        const currentUserGroups = hasSessionSettingsReceived ? state.Global.getCurrentSessionSettingsReducer.settings.currentContextInfo.currentUserGroups : [];

        return {
            dataset,
            isInLoadingDataProcess,
            pagination: {
                currentPage,
                totalPages,
                pageSize,
                recordsCount
            },
            currentUserGroups
        };
    },
    {
        dispatchReceiveAccountListThunk: ReceiveAccountListThunk.invoke,
        dispatchUpdateAccountInListThunk: UpdateAccountInListThunk.invoke
    }
)(AccountListViewClass);

export const AccountListView = withHeader({
    title: 'Аккаунты',
    page: AccountListViewConnected
});