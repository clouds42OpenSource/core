import React, { useEffect, useState } from 'react';

import { withHeader } from 'app/views/components/_hoc/withHeader';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { RequestKind } from 'core/requestSender/enums';
import { ArmChart, ArmDetails, ArmFilters } from './modules';

const { getConfigurationNames } = InterlayerApiProxy.getArmAutoUpdateAccountDatabaseApiProxy();

const ArmManagerComponent = () => {
    const [configurations, setConfigurations] = useState<string[]>([]);
    const [checkedConfigurations, setCheckedConfigurations] = useState<string[]>([]);

    useEffect(() => {
        getConfigurationNames(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY).then(response => {
            setConfigurations(response);
            setCheckedConfigurations(response);
        });
    }, []);

    return (
        <>
            <ArmFilters
                configList={ configurations }
                checkedConfigList={ checkedConfigurations }
            />
            <ArmChart
                checkedConfigList={ checkedConfigurations }
            />
            <ArmDetails />
        </>
    );
};

export const ArmManager = withHeader({
    page: ArmManagerComponent,
    title: 'Контроль ТиИ и автообновлений'
});