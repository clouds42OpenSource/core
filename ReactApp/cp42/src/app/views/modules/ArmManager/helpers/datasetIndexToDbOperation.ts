import { EDatasetIndex } from 'app/views/modules/ArmManager/config/enums';
import { EDbOperation } from 'app/api/endpoints/armManager/enum';

export const datasetIndexToDbOperation = (datasetIndex: number): EDbOperation | undefined => {
    switch (datasetIndex as EDatasetIndex) {
        case EDatasetIndex.autoUpdateFactCount:
            return EDbOperation.autoUpdate;
        case EDatasetIndex.autoUpdatePlanCount:
            return EDbOperation.planAutoUpdate;
        case EDatasetIndex.tehSupportFactCount:
            return EDbOperation.techSupport;
        case EDatasetIndex.tehSupportPlanCount:
            return EDbOperation.planTehSupport;
        case EDatasetIndex.errorsConnectCount:
            return EDbOperation.authToDb;
        case EDatasetIndex.errorsCount:
            return EDbOperation.autoUpdateRequest;
        default:
            return undefined;
    }
};