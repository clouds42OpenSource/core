export const enum EArmPeriod {
    week,
    month,
    quarter,
    half
}

export const enum EArmPeriodValue {
    week = 604800000,
    month = 2629800000,
    quarter = 7889400000,
    half = 15778800000
}