export const enum EArmFilterStatus {
    unchecked,
    confirmed,
    rejected
}