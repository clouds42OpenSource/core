export const CHART_COLORS = {
    errorsCount: {
        border: 'rgba(221,83,83)',
        background: 'rgba(221,83,83,0.5)'
    },
    errorsConnectCount: {
        border: 'rgba(255,0,0)',
        background: 'rgba(255,0,0,0.5)'
    },
    autoUpdateFactCount: {
        border: 'rgba(10,10,255)',
        background: 'rgba(10,10,255,0.5)'
    },
    autoUpdatePlanCount: {
        border: 'rgba(107,153,87)',
        background: 'rgba(107,153,87,0.5)'
    },
    tehSupportFactCount: {
        border: 'rgba(7,255,255)',
        background: 'rgba(7,255,255,0.5)'
    },
    tehSupportPlanCount: {
        border: 'rgba(2,255,2)',
        background: 'rgba(2,255,2,0.5)'
    },
} as const;