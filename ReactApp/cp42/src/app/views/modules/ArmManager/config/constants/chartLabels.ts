export const CHART_LABELS = {
    all: 'Все',
    errorsCount: 'Ошибки при ТиИ и АО',
    errorsConnectCount: 'Ошибки подкл. ТиИ и АО',
    autoUpdateFactCount: 'Автообновление (факт)',
    autoUpdatePlanCount: 'Автообновление (план)',
    tehSupportFactCount: 'ТиИ (факт)',
    tehSupportPlanCount: 'ТиИ (план)'
} as const;