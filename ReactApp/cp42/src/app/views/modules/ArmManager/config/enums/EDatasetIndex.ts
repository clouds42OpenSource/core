export const enum EDatasetIndex {
    tehSupportPlanCount,
    autoUpdatePlanCount,
    tehSupportFactCount,
    autoUpdateFactCount,
    errorsCount,
    errorsConnectCount
}