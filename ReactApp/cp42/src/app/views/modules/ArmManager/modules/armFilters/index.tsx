import { useAppDispatch } from 'app/hooks';
import React, { useEffect, useState } from 'react';
import { MenuItem, Select } from '@mui/material';
import { SelectChangeEvent } from '@mui/material/Select';

import { REDUX_API } from 'app/api/useReduxApi';
import { DoubleDateInputForm } from 'app/views/components/controls/forms/DoubleDateInputView';
import { TDatePicker } from 'app/common/types/app-types';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { SuccessButton } from 'app/views/components/controls/Button';
import { CHART_LABELS } from 'app/views/modules/ArmManager/config/constants';
import { EArmPeriod, EArmPeriodValue } from 'app/views/modules/ArmManager/config/enums';
import { DropDownListWithCheckboxes } from 'app/views/components/controls/forms';
import { EDbOperation } from 'app/api/endpoints/armManager/enum';
import { dateFilterFormat } from 'app/common/helpers';
import { DropDownListWithCheckboxItem } from 'app/views/components/controls/forms/DropDownListWithCheckboxes/types';
import { ArmFilterProps } from 'app/views/modules/ArmManager/modules/armFilters/types';

import styles from './style.module.css';

const { getArmPeriod } = REDUX_API.ARM_MANAGER_REDUX;
const { all, errorsCount, errorsConnectCount, tehSupportPlanCount, tehSupportFactCount, autoUpdatePlanCount, autoUpdateFactCount } = CHART_LABELS;

export const ArmFilters = ({ configList, checkedConfigList }: ArmFilterProps) => {
    const dispatch = useAppDispatch();

    const [armType, setArmType] = useState<EDbOperation>(EDbOperation.undefined);
    const [shortcutPeriod, setShortcutPeriod] = useState<EArmPeriod>(EArmPeriod.week);
    const [period, setPeriod] = useState<TDatePicker>({ from: new Date(Date.now() - EArmPeriodValue.week), to: new Date() });
    const [configurations, setConfigurations] = useState<string[]>([]);
    const [checkedConfigurations, setCheckedConfigurations] = useState<string[]>([]);

    useEffect(() => {
        getArmPeriod(dispatch, {});
    }, [dispatch]);

    useEffect(() => {
        setConfigurations(configList);
        setCheckedConfigurations(checkedConfigList);
    }, [checkedConfigList, configList]);

    useEffect(() => {
        switch (shortcutPeriod) {
            case EArmPeriod.week:
                setPeriod({ to: new Date(), from: new Date(Date.now() - EArmPeriodValue.week) });
                break;
            case EArmPeriod.month:
                setPeriod({ to: new Date(), from: new Date(Date.now() - EArmPeriodValue.month) });
                break;
            case EArmPeriod.quarter:
                setPeriod({ to: new Date(), from: new Date(Date.now() - EArmPeriodValue.quarter) });
                break;
            case EArmPeriod.half:
                setPeriod({ to: new Date(), from: new Date(Date.now() - EArmPeriodValue.half) });
                break;
            default:
                break;
        }
    }, [shortcutPeriod]);

    const periodHandler = (formName: string, newValue: Date) => {
        if (formName === 'from') {
            setPeriod({ ...period, from: newValue });
        } else {
            setPeriod({ ...period, to: newValue });
        }
    };

    const armTypeHandler = (ev: SelectChangeEvent<EDbOperation>) => {
        setArmType(ev.target.value as EDbOperation);
    };

    const shortcutPeriodHandler = (ev: SelectChangeEvent<EArmPeriod>) => {
        setShortcutPeriod(ev.target.value as EArmPeriod);
    };

    const getDetails = () => {
        getArmPeriod(dispatch, {
            filter: {
                startRange: dateFilterFormat(period.from),
                endRange: dateFilterFormat(period.to),
                dbOperation: armType === EDbOperation.undefined ? undefined : armType,
                configurations: checkedConfigurations
            }
        });
    };

    const onConfigurationsChange = (_: string, items: DropDownListWithCheckboxItem[]) => {
        setCheckedConfigurations(items.map(item => item.value));
    };

    return (
        <div className={ styles.container }>
            <FormAndLabel label="Период">
                <Select value={ shortcutPeriod } onChange={ shortcutPeriodHandler } fullWidth={ true }>
                    <MenuItem value={ EArmPeriod.week }>За неделю</MenuItem>
                    <MenuItem value={ EArmPeriod.month }>За месяц</MenuItem>
                    <MenuItem value={ EArmPeriod.quarter }>За три месяца</MenuItem>
                    <MenuItem value={ EArmPeriod.half }>За полгода</MenuItem>
                </Select>
            </FormAndLabel>
            <FormAndLabel label="Дата событий">
                <DoubleDateInputForm
                    isNeedToStretch={ true }
                    periodFromValue={ period.from }
                    periodToValue={ period.to }
                    periodFromInputName="from"
                    periodToInputName="to"
                    onValueChange={ periodHandler }
                />
            </FormAndLabel>
            <FormAndLabel label="Тип АРМ">
                <Select value={ armType } onChange={ armTypeHandler } fullWidth={ true }>
                    <MenuItem value={ EDbOperation.undefined }>{ all }</MenuItem>
                    <MenuItem value={ EDbOperation.planTehSupport }>{ tehSupportPlanCount }</MenuItem>
                    <MenuItem value={ EDbOperation.planAutoUpdate }>{ autoUpdatePlanCount }</MenuItem>
                    <MenuItem value={ EDbOperation.techSupport }>{ tehSupportFactCount }</MenuItem>
                    <MenuItem value={ EDbOperation.autoUpdate }>{ autoUpdateFactCount }</MenuItem>
                    <MenuItem value={ EDbOperation.autoUpdateRequest }>{ errorsCount }</MenuItem>
                    <MenuItem value={ EDbOperation.authToDb }>{ errorsConnectCount }</MenuItem>
                </Select>
            </FormAndLabel>
            <FormAndLabel label="Конфигурация">
                <DropDownListWithCheckboxes
                    label=""
                    formName="configurations"
                    items={ configurations.map(item => {
                        return {
                            text: item,
                            value: item,
                            checked: true
                        };
                    }) }
                    onValueChange={ onConfigurationsChange }
                />
            </FormAndLabel>
            <SuccessButton onClick={ getDetails }>Отфильтровать</SuccessButton>
        </div>
    );
};