import { InformationDatabaseCard } from 'app/views/modules/_common/reusable-modules/InformationDatabaseCard';
import React, { useEffect, useState } from 'react';
import OutlinedInput from '@mui/material/OutlinedInput';

import { useAccountCard, useAppSelector, useDatabaseCard } from 'app/hooks';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { Divider, Link, Pagination } from '@mui/material';
import { TextOut } from 'app/views/components/TextOut';
import { setDateFormat } from 'app/common/helpers';
import { AccountCard } from 'app/views/modules/_common/reusable-modules/AccountCard';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';

import styles from './style.module.css';

const pageSize = 20;

export const ArmDetails = () => {
    const { openDatabaseDialogHandler, closeDatabaseDialogHandler, dialogDatabaseNumber, isOpen: isOpenDatabaseCard } = useDatabaseCard();
    const { closeDialogHandler, openDialogHandler, dialogAccountNumber, isOpen } = useAccountCard();

    const { data } = useAppSelector(state => state.ArmManagerState.armDetailsReducer);

    const [currentPage, setCurrentPage] = useState(1);
    const [databaseNumber, setDatabaseNumber] = useState('');
    const [account, setAccount] = useState('');

    const filteredData = data
        ? data.filter(({ accountDatabaseNumber, accountCaption }) => accountDatabaseNumber.includes(databaseNumber) && accountCaption.includes(account))
        : [];

    const currentPageHandler = (_: React.ChangeEvent<unknown>, page: number) => {
        setCurrentPage(page);
    };

    const databaseNumberHandler = (ev: React.ChangeEvent<HTMLInputElement>) => {
        setDatabaseNumber(ev.target.value);
    };

    const accountHandler = (ev: React.ChangeEvent<HTMLInputElement>) => {
        setAccount(ev.target.value);
    };

    useEffect(() => {
        if (data?.length) {
            setCurrentPage(1);
        }
    }, [data?.length, filteredData.length]);

    return (
        <>
            { data && data.length > 0 &&
                <>
                    <AccountCard
                        isReadonlyAccountInfo={ true }
                        isOpen={ isOpen }
                        onCancelDialogButtonClick={ closeDialogHandler }
                        accountNumber={ dialogAccountNumber }
                    />
                    <InformationDatabaseCard
                        isOpen={ isOpenDatabaseCard }
                        databaseNumber={ dialogDatabaseNumber }
                        onClose={ closeDatabaseDialogHandler }
                    />
                    <div className={ styles.container }>
                        <div className={ styles.header }>
                            <TextOut fontSize={ 20 } fontWeight={ 700 }>Действия</TextOut>
                            <div className={ styles.headerFilters }>
                                <FormAndLabel label="Номер базы">
                                    <OutlinedInput
                                        fullWidth={ true }
                                        placeholder="Введите номер базы"
                                        value={ databaseNumber }
                                        onChange={ databaseNumberHandler }
                                    />
                                </FormAndLabel>
                                <FormAndLabel label="Аккаунт">
                                    <OutlinedInput
                                        fullWidth={ true }
                                        placeholder="Введите наименование аккаунта"
                                        value={ account }
                                        onChange={ accountHandler }
                                    />
                                </FormAndLabel>
                            </div>
                        </div>
                        <Divider />
                        <CommonTableWithFilter
                            isBorderStyled={ true }
                            isResponsiveTable={ true }
                            uniqueContextProviderStateId="armDetailsList"
                            tableProps={ {
                                dataset: filteredData.slice(pageSize * (currentPage - 1), pageSize * currentPage),
                                keyFieldName: 'date',
                                fieldsView: {
                                    accountDatabaseNumber: {
                                        caption: 'Номер',
                                        format: (value: string, rowData) =>
                                            <Link onClick={ () => openDatabaseDialogHandler(rowData.accountDatabaseNumber) }>{ value }</Link>
                                    },
                                    accountDatabaseName: {
                                        caption: 'Название'
                                    },
                                    accountCaption: {
                                        caption: 'Аккаунт',
                                        format: (value: string, rowData) =>
                                            <Link onClick={ () => openDialogHandler(parseInt(rowData.accountNumber, 10)) }>{ value }</Link>
                                    },
                                    date: {
                                        caption: 'Дата',
                                        format: (value: string) =>
                                            setDateFormat(value)
                                    },
                                    previousRelease: {
                                        caption: 'Старый релиз'
                                    },
                                    currentRelease: {
                                        caption: 'Новый релиз'
                                    },
                                    description: {
                                        caption: 'Описание'
                                    },
                                }
                            } }
                        />
                        <Pagination
                            shape="rounded"
                            page={ currentPage }
                            count={ Math.ceil(filteredData.length / pageSize) }
                            onChange={ currentPageHandler }
                            className={ styles.pagination }
                        />
                    </div>
                </>
            }
        </>
    );
};