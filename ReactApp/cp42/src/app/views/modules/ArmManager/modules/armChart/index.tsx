import { ActiveElement, CategoryScale, Chart as ChartJS, ChartData, ChartEvent, ChartOptions, Legend, LegendItem, LinearScale, LineElement, PointElement, Title, Tooltip } from 'chart.js';
import { Line } from 'react-chartjs-2';
import React, { useEffect } from 'react';
import dayjs from 'dayjs';

import { useAppDispatch, useAppSelector } from 'app/hooks';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { CHART_COLORS, CHART_LABELS } from 'app/views/modules/ArmManager/config/constants';
import { datasetIndexToDbOperation } from 'app/views/modules/ArmManager/helpers';
import { EDbOperation } from 'app/api/endpoints/armManager/enum';
import { REDUX_API } from 'app/api/useReduxApi';

import { ArmChartProps } from 'app/views/modules/ArmManager/modules/armChart/types';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import styles from './style.module.css';

ChartJS.register(
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend
);

const { getArmDetails } = REDUX_API.ARM_MANAGER_REDUX;

export const ArmChart = withFloatMessages(({ checkedConfigList, floatMessage: { show } }: ArmChartProps & FloatMessageProps) => {
    const dispatch = useAppDispatch();
    const { isLoading, data, error } = useAppSelector(state => state.ArmManagerState.armPeriodReducer);

    const chartData = data ?? [];
    const labels = chartData.map(({ dateString }) => dateString);

    useEffect(() => {
        if (error) {
            show(MessageType.Error, error);
        }
    }, [show, error]);

    const options: ChartOptions = {
        responsive: true,
        plugins: {
            legend: {
                position: 'right' as const,
                onClick(_: ChartEvent, legendItem: LegendItem) {
                    const length = labels.length > 0 ? labels.length - 1 : 0;

                    const startRange = dayjs(labels[0].split('.').reverse().join('-')).toISOString();
                    const endRange = dayjs(labels[length].split('.').reverse().join('-')).add(1, 'day').toISOString();

                    getArmDetails(dispatch, {
                        filter: {
                            endRange,
                            startRange,
                            dbOperation: datasetIndexToDbOperation(legendItem.datasetIndex ?? EDbOperation.undefined),
                            configurations: checkedConfigList
                        }
                    });
                }
            },
        },
        onClick(_: ChartEvent, elements: ActiveElement[]) {
            if (elements.length > 0) {
                const startRange = dayjs(labels[elements[0].index].split('.').reverse().join('-')).toISOString();
                const endRange = dayjs(startRange).add(1, 'day').toISOString();

                getArmDetails(dispatch, {
                    filter: {
                        endRange,
                        startRange,
                        dbOperation: datasetIndexToDbOperation(elements[0].datasetIndex),
                        configurations: checkedConfigList
                    }
                });
            }
        }
    };

    const chartParams: ChartData<'line', number[], string> = {
        labels,
        datasets: [
            {
                cubicInterpolationMode: 'monotone',
                label: CHART_LABELS.tehSupportPlanCount,
                data: chartData.map(({ tehSupportPlanCount }) => tehSupportPlanCount),
                borderColor: CHART_COLORS.tehSupportPlanCount.border,
                backgroundColor: CHART_COLORS.tehSupportPlanCount.background,
            },
            {
                cubicInterpolationMode: 'monotone',
                label: CHART_LABELS.autoUpdatePlanCount,
                data: chartData.map(({ autoUpdatePlanCount }) => autoUpdatePlanCount),
                borderColor: CHART_COLORS.autoUpdatePlanCount.border,
                backgroundColor: CHART_COLORS.autoUpdatePlanCount.background,
            },
            {
                cubicInterpolationMode: 'monotone',
                label: CHART_LABELS.tehSupportFactCount,
                data: chartData.map(({ tehSupportFactCount }) => tehSupportFactCount),
                borderColor: CHART_COLORS.tehSupportFactCount.border,
                backgroundColor: CHART_COLORS.tehSupportFactCount.background,
            },
            {
                cubicInterpolationMode: 'monotone',
                label: CHART_LABELS.autoUpdateFactCount,
                data: chartData.map(({ autoUpdateFactCount }) => autoUpdateFactCount),
                borderColor: CHART_COLORS.autoUpdateFactCount.border,
                backgroundColor: CHART_COLORS.autoUpdateFactCount.background,
            },
            {
                cubicInterpolationMode: 'monotone',
                label: CHART_LABELS.errorsCount,
                data: chartData.map(({ errorsCount }) => errorsCount),
                borderColor: CHART_COLORS.errorsCount.border,
                backgroundColor: CHART_COLORS.errorsCount.background,
            },
            {
                cubicInterpolationMode: 'monotone',
                label: CHART_LABELS.errorsConnectCount,
                data: chartData.map(({ errorsConnectCount }) => errorsConnectCount),
                borderColor: CHART_COLORS.errorsConnectCount.border,
                backgroundColor: CHART_COLORS.errorsConnectCount.background,
            },
        ],
    };

    return (
        <>
            { isLoading && <LoadingBounce /> }
            <Line className={ styles.chart } options={ options } data={ chartParams } />
        </>
    );
});