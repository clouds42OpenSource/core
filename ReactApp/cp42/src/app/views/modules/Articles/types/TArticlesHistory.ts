export type TArticlesProps = {
    isOtherAccount?: boolean;
    forArticles?: boolean;
    contextUserId?: string;
};