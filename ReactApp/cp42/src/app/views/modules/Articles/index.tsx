import { Autocomplete, TextField } from '@mui/material';
import { accountId, contextAccountId } from 'app/api';
import { REDUX_API } from 'app/api/useReduxApi';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { ArticleTransactions } from 'app/views/modules/Articles/components/ArticleTransactions';
import { useEffect, useState } from 'react';
import { useLocation } from 'react-router';

import { FETCH_API } from 'app/api/useFetchApi';
import { EMessageType, useAppDispatch, useAppSelector, useFloatMessages } from 'app/hooks';
import { withHeader } from 'app/views/components/_hoc/withHeader';
import { THeaders } from 'app/views/components/controls/TabsControl/views/TabView';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { TabsControl } from 'app/views/components/controls/TabsControl';
import { CommonArticlesInfo } from 'app/views/modules/Articles/components/CommonArticlesInfo';
import { Requisites, Withdrawing } from 'app/views/modules/ToPartners/views';
import { TPartnersHistory } from 'app/views/modules/ToPartners/config/types';

import { ArticlesForBlog } from './tabs/ArticlesForBlog';

const HEADERS: THeaders = [
    {
        title: 'Статьи для блога',
        isVisible: true
    },
    {
        title: 'Транзакции',
        isVisible: true
    },
    {
        title: 'Реквизиты',
        isVisible: true,
    },
    {
        title: 'Вывод',
        isVisible: true,
    }
];

type TArticlesLocation = TPartnersHistory;

const isOtherAccount = accountId() !== contextAccountId();
const { getUsers } = REDUX_API.USERS_REDUX;

const Articles = () => {
    const dispatch = useAppDispatch();
    const { show } = useFloatMessages();

    const { data, error, isLoading } = FETCH_API.PROFILE.useGetProfile();
    const { data: userList } = useAppSelector(state => state.Users.usersSlice);
    const location = useLocation<TArticlesLocation | undefined>();

    const [isUserInfoFilled, setIsUserInfoFilled] = useState(false);
    const [contextUserId, setContextUserId] = useState<{ label: string; value: string }>();

    useEffect(() => {
        if (isOtherAccount) {
            void getUsers(dispatch);
        }
    }, [dispatch]);

    useEffect(() => {
        if (!contextUserId && userList && userList.records.length) {
            setContextUserId({ label: `${ userList.records[0].login } ${ userList.records[0].fullName ? `(${ userList.records[0].fullName })` : '' }`, value: userList.records[0].id });
        }
    }, [contextUserId, userList]);

    useEffect(() => {
        if (error) {
            show(EMessageType.error, error);
        }
    }, [error, show]);

    useEffect(() => {
        if (data?.rawData) {
            setIsUserInfoFilled(!!data.rawData.firstName && !!data.rawData.lastName);
        }
    }, [data]);

    return (
        <>
            { isLoading && <LoadingBounce /> }
            {
                isOtherAccount && (
                    <FormAndLabel label="Выберите пользователя">
                        <Autocomplete
                            key={ contextUserId?.value }
                            value={ contextUserId }
                            onChange={ (_event, value: { label: string; value: string }) => {
                                setContextUserId(value);
                            } }
                            disableClearable={ true }
                            options={ (userList?.records ?? []).map(item => ({ label: `${ item.login } ${ item.fullName ? `(${ item.fullName })` : '' }`, value: item.id })) }
                            sx={ { width: 300, marginBottom: '8px' } }
                            renderInput={ params => <TextField { ...params } hiddenLabel={ true } /> }
                        />
                    </FormAndLabel>
                )
            }
            <TabsControl
                activeTabIndex={ location.state?.activeTabIndex }
                headers={ HEADERS }
                headerContent={ <CommonArticlesInfo isOtherAccount={ isOtherAccount } contextUserId={ contextUserId?.value } /> }
            >
                <ArticlesForBlog isAllowCreate={ isUserInfoFilled } isOtherAccount={ isOtherAccount } contextUserId={ contextUserId?.value } />
                <ArticleTransactions isOtherAccount={ isOtherAccount } contextUserId={ contextUserId?.value } />
                <Requisites forArticles={ true } />
                <Withdrawing forArticles={ true } isOtherAccount={ isOtherAccount } contextUserId={ contextUserId?.value } />
            </TabsControl>
        </>
    );
};

export const ArticlesView = withHeader({
    page: Articles,
    title: 'Статьи'
});