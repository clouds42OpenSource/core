import { Delete as DeleteIcon } from '@mui/icons-material';
import { Chip, IconButton, Link as MuiLink, Tooltip } from '@mui/material';

import { userId } from 'app/api';
import { ArticleCoreStatusEnum, getCoreArticleResponseDto } from 'app/api/endpoints/articles/response/getCoreArticleResponseDto';
import { FETCH_API } from 'app/api/useFetchApi';
import { AppRoutes } from 'app/AppRoutes';
import { EMessageType, useFloatMessages } from 'app/hooks';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { SelectThemeModal } from 'app/views/modules/Articles/components/SelectThemeModal';
import { SendToFillInfoModal } from 'app/views/modules/Articles/components/SendToFillInfoModal';
import { ArticleStatusDescriptions } from 'app/views/modules/Articles/utils/getArticleStatusName';
import { SelectDataMetadataResponseDto } from 'app/web/common';
import dayjs from 'dayjs';
import React, { useCallback, useEffect, useRef, useState } from 'react';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';

import styles from '../../style.module.css';

type TProps = {
    isAllowCreate: boolean;
    isOtherAccount: boolean;
    contextUserId?: string;
};

const { ARTICLES: { getCoreArticles, deleteCoreArticle, deletePost, editThemeStatus, getSummaryInfo } } = FETCH_API;

export const ArticlesForBlog = ({ isAllowCreate, isOtherAccount, contextUserId }: TProps) => {
    const [articles, setArticles] = useState<null | SelectDataMetadataResponseDto<getCoreArticleResponseDto>>(null);
    const [isLoading, setIsLoading] = useState(false);
    const [pageNumber, setPageNumber] = useState(1);

    const pageNumberRef = useRef(pageNumber);

    const dispatch = useDispatch();
    const { show } = useFloatMessages();

    useEffect(() => {
        pageNumberRef.current = pageNumber;
    }, [pageNumber]);

    const handleGetCoreArticles = useCallback(async (value?: { pageNumber: number }) => {
        setIsLoading(true);
        const currentPageNumber = value?.pageNumber || pageNumberRef.current;

        if (value?.pageNumber) {
            setPageNumber(value.pageNumber);
        }

        if (!isOtherAccount) {
            const response = await getCoreArticles({ pageNumber: currentPageNumber, pageSize: 10, filter: { type: 2, accountUserId: userId() } });

            if (response.success) {
                setArticles(response.data);
            }
        } else if (contextUserId) {
            const response = await getCoreArticles({ pageNumber: currentPageNumber, pageSize: 10, filter: { type: 2, accountUserId: contextUserId } });

            if (response.success) {
                setArticles(response.data);
            }
        }

        setIsLoading(false);
    }, [contextUserId, isOtherAccount]);

    const deleteArticle = async (data: getCoreArticleResponseDto) => {
        setIsLoading(true);
        const response = await deleteCoreArticle({ id: data.id });
        let wpResponse;

        if (data.wpId) {
            wpResponse = await deletePost(data.wpId);
        }

        if ((response.success && (wpResponse && wpResponse.success)) || (response.success && !wpResponse)) {
            await editThemeStatus({ id: data.topicId, status: 'publish' });

            show(EMessageType.success, 'Статья успешно удалена');
        } else {
            if (response.message) {
                show(EMessageType.error, response.message);
            }
            if (wpResponse && wpResponse.message) {
                show(EMessageType.error, wpResponse.message);
            }
        }

        await handleGetCoreArticles();
        void getSummaryInfo(dispatch);
        setIsLoading(false);
    };

    useEffect(() => {
        void handleGetCoreArticles();
    }, [handleGetCoreArticles]);

    return (
        <>
            { isLoading && <LoadingBounce /> }
            { isAllowCreate
                ? (
                    <SelectThemeModal updateArticles={ () => handleGetCoreArticles() } />
                ) : (
                    <SendToFillInfoModal />
                )
            }
            { articles && (
                <CommonTableWithFilter
                    isBorderStyled={ true }
                    isResponsiveTable={ true }
                    uniqueContextProviderStateId="articles"
                    onDataSelect={ handleGetCoreArticles }
                    tableProps={ {
                        emptyText: 'Статьи для блога не найдены',
                        dataset: articles.records,
                        keyFieldName: 'topic',
                        pagination: {
                            totalPages: Math.ceil(articles.metadata.totalItemCount / articles.metadata.pageSize),
                            pageSize: articles.metadata.pageSize,
                            currentPage: articles.metadata.pageNumber,
                            recordsCount: articles.metadata.totalItemCount,
                        },
                        fieldsView: {
                            wpId: {
                                caption: 'ID Статьи',
                            },
                            topic: {
                                caption: 'Наименование',
                                format: (value: string, row) =>
                                    [ArticleCoreStatusEnum.Draft, ArticleCoreStatusEnum.ForRevision].includes(row.status)
                                        ? (
                                            <Link className={ styles.link } to={ `${ AppRoutes.articles.editArticleForBlogReference }/${ row.id }` }>
                                                { value }
                                            </Link>
                                        )
                                        : value
                            },
                            createdOn: {
                                caption: 'Дата создания',
                                format: (value: string) => dayjs(value).format('DD.MM.YYYY HH:mm')
                            },
                            publicationDate: {
                                caption: 'Дата публикации',
                                format: (value: string | null) => value ? dayjs(value).format('DD.MM.YYYY HH:mm') : '---',
                            },
                            wpLink: {
                                caption: 'Ссылка',
                                format: (value: string | null, row) => (
                                    value && row.status === ArticleCoreStatusEnum.Published
                                        ? <MuiLink href={ value } target="_blank" rel="noreferrer">{ value }</MuiLink>
                                        : '---'
                                )
                            },
                            status: {
                                caption: 'Статус',
                                format: (value: number) => {
                                    const statusInfo = ArticleStatusDescriptions[value as ArticleCoreStatusEnum];

                                    return (
                                        <Chip className={ styles.chip } label={ statusInfo.title } color={ statusInfo.type } />
                                    );
                                }
                            },
                            registrationCount: {
                                caption: 'Кол-во регистраций',
                                format: (value: number, row) => (
                                    row.status === 5
                                        ? (<><i className="fa fa-user" aria-hidden="true" />{ ' ' }{ value }</>)
                                        : '---'
                                )
                            },
                            id: {
                                caption: 'Действия',
                                format: (_, row) => (
                                    <>
                                        {
                                            (
                                                row.status === ArticleCoreStatusEnum.Draft || row.status === ArticleCoreStatusEnum.ForRevision
                                            ) && (
                                                <Tooltip title="Удалить статью">
                                                    <IconButton
                                                        sx={ { padding: 0 } }
                                                        size="small"
                                                        key="delete"
                                                        onClick={ () => deleteArticle(row) }
                                                    >
                                                        <DeleteIcon fontSize="small" />
                                                    </IconButton>
                                                </Tooltip>
                                            )
                                        }
                                    </>
                                )
                            }
                        }
                    } }
                />
            ) }
        </>
    );
};