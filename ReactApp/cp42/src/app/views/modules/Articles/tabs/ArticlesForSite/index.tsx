import { Delete as DeleteIcon, Edit as EditIcon, Send as SendIcon } from '@mui/icons-material';
import { Box, Chip, IconButton, Tooltip } from '@mui/material';
import { AppRoutes } from 'app/AppRoutes';
import { userId } from 'app/api';
import { ArticleCoreStatusEnum, getCoreArticleResponseDto } from 'app/api/endpoints/articles/response/getCoreArticleResponseDto';
import { FETCH_API } from 'app/api/useFetchApi';
import { EMessageType, useFloatMessages } from 'app/hooks';
import { DateUtility } from 'app/utils';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { TextOut } from 'app/views/components/TextOut';
import { SuccessButton } from 'app/views/components/controls/Button';
import { SendToFillInfoModal } from 'app/views/modules/Articles/components/SendToFillInfoModal';
import { ArticleStatusDescriptions } from 'app/views/modules/Articles/utils/getArticleStatusName';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import styles from 'app/views/modules/Partners/views/articlesControl/style.module.css';
import { SelectDataMetadataResponseDto } from 'app/web/common';
import { useCallback, useEffect, useRef, useState } from 'react';
import { Link } from 'react-router-dom';

type Props = {
    isAllowCreate: boolean;
};

const { EMAILS: { sendEmail }, ARTICLES: { getCoreArticles, deleteCoreArticle, deletePost, deleteArticle: articleDelete, editCoreArticleStatus, editArticleStatus } } = FETCH_API;

export const ArticlesForSite = ({ isAllowCreate }: Props) => {
    const [isLoading, setIsLoading] = useState(false);
    const [articles, setArticles] = useState<null | SelectDataMetadataResponseDto<getCoreArticleResponseDto>>(null);
    const [pageNumber, setPageNumber] = useState(1);
    const { show } = useFloatMessages();
    const pageNumberRef = useRef(pageNumber);

    useEffect(() => {
        pageNumberRef.current = pageNumber;
    }, [pageNumber]);

    const handleGetCoreArticles = useCallback(async (value?: { pageNumber: number }) => {
        setIsLoading(true);
        const currentPageNumber = value?.pageNumber ?? pageNumberRef.current;

        if (value?.pageNumber) {
            setPageNumber(value.pageNumber);
        }

        const response = await getCoreArticles({ pageNumber: currentPageNumber, pageSize: 10, filter: { type: 1, accountUserId: userId() } });

        if (response.success) {
            setArticles(response.data);
        }

        setIsLoading(false);
    }, []);

    const deleteArticle = async (data: getCoreArticleResponseDto) => {
        setIsLoading(true);
        const response = await deleteCoreArticle({ id: data.id });
        let wpResponse;

        if (data.wpId) {
            wpResponse = await deletePost(data.wpId);
        }

        if ((response.success && (wpResponse && wpResponse.success)) || (response.success && !wpResponse)) {
            await articleDelete({ id: data.id });
            show(EMessageType.success, 'Статья успешно удалена');
        } else {
            if (response.message) {
                show(EMessageType.error, response.message);
            }
            if (wpResponse && wpResponse.message) {
                show(EMessageType.error, wpResponse.message);
            }
        }

        await handleGetCoreArticles();
    };

    const handleEditCoreArticleStatus = async (data: getCoreArticleResponseDto, status: ArticleCoreStatusEnum) => {
        setIsLoading(true);
        const response = await editCoreArticleStatus({ id: data.id, status });

        if (status === ArticleCoreStatusEnum.Published && response.success && data.wpId) {
            await editArticleStatus({ id: data.wpId, status: 'publish' });
        }

        if (response.success && status === ArticleCoreStatusEnum.UnderInspection) {
            await sendEmail({
                body: `Автор статьи: ${ data.accountUserLogin }, Наименование статьи: ${ data.topic }`,
                subject: 'Статья для сайта отправлена на проверку',
                header: 'Статья для сайта отправлена на проверку',
                categories: 'articles',
                reference: import.meta.env.REACT_SUPPORT_EMAIL ?? ''
            });

            await sendEmail({
                body: `Автор статьи: ${ data.accountUserLogin }, Наименование статьи: ${ data.topic } 
                <a href="https://cp.42clouds.com/administration/partners">Ссылка на модерирование</a>`,
                subject: 'Статья для сайта отправлена на проверку',
                header: 'Статья для сайта отправлена на проверку',
                categories: 'articles',
                reference: import.meta.env.REACT_ARTICLE_EDITOR_EMAIL ?? ''
            });
        }

        if (response.success) {
            if (status === ArticleCoreStatusEnum.Published) {
                show(EMessageType.success, 'Статья успешно опубликована');
            } else {
                show(EMessageType.success, 'Статья успешно отправлена на проверку');
            }
        } else {
            show(EMessageType.error, response.message);
        }

        await handleGetCoreArticles();
        setIsLoading(false);
    };

    useEffect(() => {
        void handleGetCoreArticles();
    }, [handleGetCoreArticles]);

    return (
        <>
            { isLoading && <LoadingBounce /> }
            { isAllowCreate
                ? (
                    <Box textAlign="start" marginBottom="12px">
                        <Link to={ AppRoutes.articles.editArticleForSiteReference }>
                            <SuccessButton>
                                Написать статью
                            </SuccessButton>
                        </Link>
                    </Box>
                )
                : (
                    <SendToFillInfoModal />
                )
            }
            { articles && (
                <CommonTableWithFilter
                    isBorderStyled={ true }
                    isResponsiveTable={ true }
                    uniqueContextProviderStateId="articles"
                    onDataSelect={ handleGetCoreArticles }
                    tableProps={ {
                        dataset: articles.records,
                        emptyText: 'Статьи для сайта не найдены',
                        keyFieldName: 'topic',
                        pagination: {
                            totalPages: Math.ceil(articles.metadata.totalItemCount / articles.metadata.pageSize),
                            pageSize: articles.metadata.pageSize,
                            currentPage: articles.metadata.pageNumber,
                            recordsCount: articles.metadata.totalItemCount,
                        },
                        fieldsView: {
                            wpId: {
                                caption: 'ID Статьи',
                                fieldWidth: 'auto'
                            },
                            topic: {
                                caption: 'Наименование',
                                fieldWidth: '45%',
                            },
                            createdOn: {
                                caption: 'Дата создания',
                                fieldWidth: 'auto',
                                format: value => DateUtility.dateToDayMonthYearHourMinute(DateUtility.convertToDate(value))
                            },
                            status: {
                                caption: 'Статус',
                                fieldWidth: 'auto',
                                format: (value: number) => {
                                    const statusInfo = ArticleStatusDescriptions[value as ArticleCoreStatusEnum];

                                    return (
                                        <Chip className={ styles.chip } label={ statusInfo.title } color={ statusInfo.type } />
                                    );
                                }
                            },
                            registrationCount: {
                                caption: 'Кол-во регистраций',
                                fieldWidth: 'auto',
                                format: (value: number, row) => (
                                    row.status === 5 && (
                                        <TextOut inDiv={ true } style={ { paddingLeft: '30%' } }>
                                            { value }
                                        </TextOut>
                                    )
                                )
                            },
                            id: {
                                caption: 'Действия',
                                fieldWidth: '10%',
                                format: (value: string, row) => (
                                    <Box display="flex" alignItems="center" gap="3px">
                                        <Tooltip title="Редактировать">
                                            <Link to={ `${ AppRoutes.articles.editArticleForSiteReference }/${ value }` }>
                                                <IconButton
                                                    sx={ { padding: 0 } }
                                                    key="edit"
                                                >
                                                    <EditIcon fontSize="small" />
                                                </IconButton>
                                            </Link>
                                        </Tooltip>
                                        {
                                            !(row.status === ArticleCoreStatusEnum.UnderInspection || row.status === ArticleCoreStatusEnum.Published) && (
                                                <Tooltip title="Отправить на проверку">
                                                    <IconButton
                                                        sx={ { padding: 0 } }
                                                        key="sendForReview"
                                                        onClick={ () => handleEditCoreArticleStatus(row, 2) }
                                                    >
                                                        <SendIcon fontSize="small" />
                                                    </IconButton>
                                                </Tooltip>
                                            )
                                        }
                                        <Tooltip title="Удалить статью">
                                            <IconButton
                                                sx={ { padding: 0 } }
                                                size="small"
                                                key="delete"
                                                onClick={ () => deleteArticle(row) }
                                            >
                                                <DeleteIcon fontSize="small" />
                                            </IconButton>
                                        </Tooltip>
                                    </Box>
                                )
                            }
                        }
                    } }
                />
            ) }
        </>
    );
};