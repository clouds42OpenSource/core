import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import { Accordion, AccordionDetails, AccordionSummary, Card, CardActionArea, CardContent, CardMedia, List, ListItem, SelectChangeEvent, TextareaAutosize, TextField, Tooltip, Link } from '@mui/material';
import { Box } from '@mui/system';
import { userId } from 'app/api';
import { editArticleRequest } from 'app/api/endpoints/articles/requests/editArticleRequestDto';
import { getArticleResponseDto } from 'app/api/endpoints/articles/response/getArticleResponseDto';
import { ArticleCoreStatusEnum, getCoreArticleResponseDto } from 'app/api/endpoints/articles/response/getCoreArticleResponseDto';
import { ProfileItemResponseDto } from 'app/api/endpoints/profile/response';
import { TResponseLowerCase } from 'app/api/types';
import { FETCH_API } from 'app/api/useFetchApi';
import { REDUX_API } from 'app/api/useReduxApi';
import { AccountUserGroup } from 'app/common/enums';
import { EMessageType, useAppDispatch, useAppSelector, useFloatMessages } from 'app/hooks';
import { COLORS } from 'app/utils';
import { MultipleChipSelect, TMenuItem } from 'app/views/components/controls';
import { ContainedButton, OutlinedButton, SuccessButton } from 'app/views/components/controls/Button';
import { Dialog } from 'app/views/components/controls/Dialog';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { RichTextEditor } from 'app/views/components/RichTextEditor';
import { ButtonsToolbarListType } from 'app/views/components/RichTextEditor/types';
import { TextOut } from 'app/views/components/TextOut';
import { ArticleStatusEnum } from 'app/views/modules/Articles/types/ArticleStatusEnum';
import cn from 'classnames';
import { WebVerb } from 'core/requestSender/enums';
import { useFormik } from 'formik';
import { ChangeEvent, useEffect, useRef, useState } from 'react';
import { Helmet } from 'react-helmet';
import { useHistory, useLocation, useParams } from 'react-router';
import * as Yup from 'yup';
import styles from './style.module.css';

type RouteParams = {
    id: string;
};

const requiredMessage = 'Поле обязательно к заполнению';

const validationSchema = Yup.object({
    title: Yup.string().required(requiredMessage),
    excerpt: Yup.string().required(requiredMessage),
    featured_media: Yup.number().required('Изображение обязательно для загрузки'),
    content: Yup.string().required(requiredMessage).min(20, 'Длина поля должна быть не менее 20 символов'),
    author: Yup.string(),
    categories: Yup.array().of(Yup.string()).min(1, requiredMessage).required(requiredMessage),
});

const moderateValidationSchema = Yup.object({
    title: Yup.string().required(requiredMessage),
    excerpt: Yup.string(),
    featured_media: Yup.number().required('Изображение обязательно для загрузки'),
    content: Yup.string(),
    author: Yup.string(),
    categories: Yup.array().of(Yup.string()),
});

const buttonList: ButtonsToolbarListType = [
    ['undo', 'redo'],
    ['formatBlock'],
    ['bold', 'underline', 'italic'],
    ['outdent', 'indent'],
    ['align', 'horizontalRule', 'list', 'table'],
    ['link', 'image', 'video'],
    ['codeView'],
    ['math']
];

const {
    EMAILS: {
        sendEmail
    },
    ARTICLES: {
        checkArticleAuthor,
        createArticleAuthor,
        createArticle,
        editArticle,
        createBlogArticle,
        createCoreArticle,
        editCoreArticle,
        editThemeStatus,
        uploadMedia,
        getArticleById,
        getBlogArticleById,
        getCoreArticle,
        editCoreArticleStatus,
        editBlogArticleStatus,
        editBlogArticle,
        getMediaById,
        useGetChat,
        createChat,
        editChat,
        getThemeById
    }
} = FETCH_API;

const { getArticleCategories } = REDUX_API.ARTICLES_REDUX;

export const EditArticleView = () => {
    const { data: userData, error: profileError, isLoading: isProfileLoading } = FETCH_API.PROFILE.useGetProfile();
    const {
        ArticlesState: { getArticleCategoriesReducer: { data, error, isLoading: isCategoriesLoading } },
        Global: { getCurrentSessionSettingsReducer: { settings: { currentContextInfo: { currentUserGroups } } } }
    } = useAppSelector(state => state);

    const fileInputRef = useRef<HTMLInputElement>(null);

    const [articleMainImg, setArticleMainImg] = useState<string | null>(null);
    const [article, setArticle] = useState<null | getArticleResponseDto>(null);
    const [coreArticle, setCoreArticle] = useState<null | getCoreArticleResponseDto>(null);
    const [themes, setThemes] = useState<getArticleResponseDto | null>(null);

    const [isLoading, setIsLoading] = useState(false);
    const [isOpen, setIsOpen] = useState(false);

    const [comment, setComment] = useState('');

    const location = useLocation();
    const dispatch = useAppDispatch();
    const { id } = useParams<RouteParams>();
    const { goBack } = useHistory();
    const { show } = useFloatMessages();

    const { data: chatData } = useGetChat(coreArticle?.wpId ?? '');

    const isBlog = location.pathname.includes('for-blog');
    const isSite = location.pathname.includes('for-site');

    useEffect(() => {
        void getArticleCategories(dispatch);
    }, [dispatch]);

    useEffect(() => {
        if (profileError) {
            show(EMessageType.error, profileError);
            goBack();
        }
    }, [goBack, profileError, show]);

    useEffect(() => {
        if (error) {
            show(EMessageType.error, error);
        }
    }, [error, show]);

    useEffect(() => {
        if (id) {
            (async () => {
                setIsLoading(true);
                const coreArticleResponse = await getCoreArticle({ id });

                if (coreArticleResponse.data) {
                    setCoreArticle(coreArticleResponse.data);
                    let response: TResponseLowerCase<getArticleResponseDto>;

                    const themesResponse = await getThemeById(coreArticleResponse.data.topicId);

                    if (themesResponse.data) {
                        setThemes(themesResponse.data);
                    } else {
                        show(EMessageType.error, themesResponse.message);
                    }

                    if (coreArticleResponse.data.wpId) {
                        if (isSite) {
                            response = await getArticleById({ id: coreArticleResponse.data.wpId });
                        } else {
                            response = await getBlogArticleById({ id: coreArticleResponse.data.wpId });
                        }

                        if (response.data && response.success) {
                            setArticle(response.data);

                            if (response.data.title.rendered === '') {
                                await setFieldValue('title', themesResponse.data?.title.rendered ?? '');
                            }

                            const mediaResponse = await getMediaById(response.data.featured_media);
                            setIsLoading(false);

                            if (mediaResponse.data) {
                                setArticleMainImg(mediaResponse.data.guid.rendered);
                            }
                        } else if (isSite) {
                            show(EMessageType.error, 'Ошибка получения статьи');
                            goBack();
                        }
                    } else if (values.title === '') {
                        await setFieldValue('title', themesResponse.data?.title.rendered ?? '');
                    }
                }

                setIsLoading(false);
            })();
        }
    }, [id, show, isSite, goBack]);

    const getAuthorId = async () => {
        const userInfo = userData?.rawData ?? {} as ProfileItemResponseDto;
        const isAuthorRegistered = await checkArticleAuthor(userInfo.login);
        let authorId: string | undefined;

        if (!isAuthorRegistered.data?.length) {
            const registerAuthorResponse = await createArticleAuthor({
                username: userInfo.login,
                email: userInfo.email,
                password: userInfo.id,
                roles: 'author_articles_ext',
                name: `${ userInfo.firstName } ${ userInfo.lastName }`,
                first_name: userInfo.firstName,
                last_name: userInfo.lastName
            });
            authorId = registerAuthorResponse.data?.id;
        } else {
            authorId = isAuthorRegistered.data[0].id;
        }

        return authorId;
    };

    const handleCreateArticle = async (formValues: editArticleRequest) => {
        setIsLoading(true);
        const authorId = await getAuthorId();
        const response = await createArticle({ ...formValues, author: authorId || '' });

        if (response.data && response.success) {
            const coreResponse = await createCoreArticle({ topic: formValues.title, wpId: response.data.id, type: 1 });

            if (coreResponse.success) {
                show(EMessageType.success, 'Успешно');
            } else {
                show(EMessageType.error, coreResponse.message);
            }
        } else {
            show(EMessageType.error, response.message);
        }

        setIsLoading(false);
        goBack();
    };

    const handleCreateBlogArticle = async (formValues: editArticleRequest & { status?: ArticleStatusEnum }) => {
        setIsLoading(true);
        const authorId = await getAuthorId();
        const response = await createBlogArticle({ ...formValues, author: authorId || '', status: formValues?.status ?? ArticleStatusEnum.Pending });

        if (response.data && response.success) {
            const requestId = Number.parseInt(id, 10);
            const coreArticleResponse = await editCoreArticle({
                id: requestId,
                topic: formValues.title,
                wpId: response.data.id,
                wpLink: response.data.link,
                title: formValues.title,
                text: formValues.content,
            });

            if (coreArticleResponse.message) {
                show(EMessageType.error, coreArticleResponse.message);
            }

            if (formValues.status !== ArticleStatusEnum.Draft) {
                const coreResponse = await editCoreArticleStatus({ id, status: ArticleCoreStatusEnum.UnderInspection });

                if (coreResponse.data && !article) {
                    await editThemeStatus({ id: coreResponse.data?.topicId, method: WebVerb.DELETE });
                }
            }
        } else {
            show(EMessageType.error, response.message);
        }

        setIsLoading(false);
        goBack();
    };

    const handleEditArticle = async (formValues: editArticleRequest) => {
        if (article) {
            setIsLoading(true);
            const response = await editArticle({ ...formValues, id: article.id, status: article.status });

            if (response.data && response.success) {
                const requestId = Number.parseInt(id, 10);
                const coreArticleResponse = await editCoreArticle({
                    id: requestId,
                    topic: formValues.title,
                    wpId: response.data.id,
                    wpLink: response.data.link,
                    title: formValues.title,
                    text: formValues.content
                });

                if (coreArticleResponse.message) {
                    show(EMessageType.error, coreArticleResponse.message);
                }

                await editCoreArticleStatus({ id, status: coreArticle?.status ?? ArticleCoreStatusEnum.Draft });
            }

            if (response.success) {
                show(EMessageType.success, 'Успешно');
            } else {
                show(EMessageType.error, response.message);
            }

            setIsLoading(false);
            goBack();
        }
    };

    const handleEditBlogArticle = async (formValues: editArticleRequest & { status?: ArticleStatusEnum}, back = true) => {
        if (article) {
            setIsLoading(true);
            const response = await editBlogArticle({ ...formValues, id: article.id, status: formValues?.status ?? ArticleStatusEnum.Pending });

            if (response.data && response.success) {
                const requestId = Number.parseInt(id, 10);
                const coreArticleResponse = await editCoreArticle({
                    id: requestId,
                    topic: formValues.title,
                    wpId: response.data.id,
                    wpLink: response.data.link,
                    title: formValues.title,
                    text: formValues.content
                });

                if (coreArticleResponse.message) {
                    show(EMessageType.error, coreArticleResponse.message);
                }

                if (formValues.status !== ArticleStatusEnum.Draft) {
                    const coreResponse = await editCoreArticleStatus({ id, status: ArticleCoreStatusEnum.UnderInspection });

                    if (coreResponse.data) {
                        await editThemeStatus({ id: coreResponse.data?.topicId, status: ArticleStatusEnum.Pending });
                    }
                }
            }

            setIsLoading(false);

            if (response.success) {
                show(EMessageType.success, 'Успешно');
            } else {
                show(EMessageType.error, response.message);
            }

            if (back) {
                goBack();
            }
        }
    };

    const addComment = () => {
        const prefix = currentUserGroups.includes(AccountUserGroup.ArticleEditor) ? 'Редактор: - ' : 'Автор: - ';

        if (chatData?.rawData.length) {
            const { content: { rendered }, id: chatId } = chatData.rawData[0];

            void editChat(chatId, rendered + prefix + comment);
        } else if (coreArticle) {
            void createChat({ title: coreArticle.wpId ?? '', content: prefix + comment });
        }
    };

    const editArticleStatus = async (status: ArticleCoreStatusEnum, articleStatus = 'publish') => {
        if (status === ArticleCoreStatusEnum.ForRevision) {
            addComment();
        }

        if (coreArticle) {
            setIsLoading(true);
            const response = await editCoreArticleStatus({ id: coreArticle.id, status });

            if (response.success) {
                if (coreArticle.wpId) {
                    void editBlogArticleStatus({ id: coreArticle.wpId, status: articleStatus });
                }
            } else {
                show(EMessageType.error, response.message);
            }

            setIsLoading(false);
            goBack();
        }
    };

    const schema = Yup.lazy(() => {
        if (values.status === ArticleStatusEnum.Draft) {
            return moderateValidationSchema;
        }

        return validationSchema;
    });

    const {
        handleChange,
        values,
        submitForm,
        setFieldValue,
        touched,
        errors,
        isValid
    } = useFormik({
        initialValues: {
            title: article?.title.rendered ?? '',
            content: article?.content.rendered ?? '',
            featured_media: article?.featured_media ?? '',
            excerpt: article?.excerpt.rendered.replace(/^<p>|<\/p>\s*$/g, '') ?? '',
            author: article?.author ?? '',
            categories: article?.categories.map(category => category.toString()) ?? [],
            ...(isBlog && { status: ArticleStatusEnum.Pending })
        },
        validationSchema: schema,
        validateOnMount: true,
        validateOnChange: true,
        validateOnBlur: false,
        enableReinitialize: true,
        onSubmit: formValues => {
            if (comment) {
                addComment();
            }

            const value = { ...formValues, rank_math_description: themes?.acf.theme_description ?? '' };

            if (article) {
                if (isSite) {
                    void handleEditArticle(value);
                } else {
                    void handleEditBlogArticle(value);
                }
            } else if (isBlog) {
                void handleCreateBlogArticle(value);
            } else {
                void handleCreateArticle(value);
            }
        },
    });

    const handleImageUploadBefore = (files: File[], info: object, uploadHandler: Function) => {
        (async () => {
            setIsLoading(true);
            const response = await uploadMedia(files);
            setIsLoading(false);

            if (response.data) {
                uploadHandler({ result: [{ url: response.data.guid.rendered, name: files[0].name, size: files[0].size }] });
            } else {
                uploadHandler('Ошибка при загрузке файла');
            }
        })();
    };

    const handleImageChange = (e: ChangeEvent<HTMLInputElement>) => {
        const file = e.target.files?.[0];

        try {
            if (file) {
                const reader = new FileReader();
                reader.readAsDataURL(file);

                reader.onload = ({ target }) => {
                    const image = new Image();

                    if (target && typeof target.result === 'string') {
                        image.src = target.result;

                        image.onload = async () => {
                            if (image.width < 1600 || image.height < 1000 || (image.width / image.height).toFixed(1) !== '1.6') {
                                show(EMessageType.warning, 'Минимальный размер изображения должен быть не менее 1600x1000 с сохранением пропорций');
                            } else {
                                setIsLoading(true);
                                const response = await uploadMedia([file]);
                                setIsLoading(false);

                                if (response.data) {
                                    await setFieldValue('featured_media', response.data.id);
                                    setArticleMainImg(response.data.guid.rendered);
                                }
                            }
                        };
                    }
                };
            }
        } catch (err: unknown) {
            show(EMessageType.error, 'Произошла ошибка при загрузке изображения');
        }
    };

    const commentHandler = (ev: ChangeEvent<HTMLInputElement>) => {
        setComment(ev.target.value);
    };

    const handleRemoveImage = () => {
        setFieldValue('featured_media', null);
        setArticleMainImg(null);
    };

    const triggerFileSelectPopup = () => {
        fileInputRef.current?.click();
    };

    const handleSaveArticleForBlog = () => {
        if (isValid && values.status === ArticleStatusEnum.Draft) {
            void submitForm();
        }
        setFieldValue('status', ArticleStatusEnum.Draft);
    };

    useEffect(() => {
        if (values.status === ArticleStatusEnum.Draft) {
            void submitForm();
        }
    }, [submitForm, values.status]);

    const handleSelectedCategories = ({ target: { value } }: SelectChangeEvent<string[]>) => {
        if (Array.isArray(value)) {
            setFieldValue('categories', value);
        }
    };

    const openDialogHandler = () => {
        setIsOpen(true);
    };

    const closeDialogHandler = () => {
        setIsOpen(false);
    };

    return (
        <>
            { (isLoading || isProfileLoading || isCategoriesLoading) && <LoadingBounce /> }
            <Dialog
                title="Комментарий"
                isTitleSmall={ true }
                isOpen={ isOpen }
                dialogWidth="xs"
                maxHeight="400px"
                onCancelClick={ closeDialogHandler }
                buttons={ [
                    {
                        kind: 'default',
                        content: 'Закрыть',
                        onClick: closeDialogHandler,
                    },
                    {
                        kind: 'success',
                        content: 'Отправить',
                        onClick: () => {
                            if (currentUserGroups.includes(AccountUserGroup.ArticleEditor)) {
                                void editArticleStatus(ArticleCoreStatusEnum.ForRevision, 'draft');
                            } else {
                                void submitForm();
                            }
                        }
                    }
                ] }
            >
                <FormAndLabel label={ <Box display="flex" gap="5px"><TextOut fontWeight={ 700 }>Комменатрий</TextOut><TextOut>(Необязательное поле)</TextOut></Box> }>
                    <TextField
                        fullWidth={ true }
                        multiline={ true }
                        rows={ 6 }
                        value={ comment }
                        onChange={ commentHandler }
                    />
                </FormAndLabel>
            </Dialog>
            <Helmet>
                <title>{ `${ (id && article) ? 'Редактирование' : 'Создание' } cтатьи` }</title>
            </Helmet>
            <Box className={ styles.wrapper }>
                <FormAndLabel label="Заголовок" forId="title">
                    <TextField
                        name="title"
                        fullWidth={ true }
                        variant="standard"
                        placeholder="Введите заголовок"
                        onChange={ handleChange }
                        error={ touched.title && !!errors.title }
                        helperText={ touched.title && <TextOut style={ { color: COLORS.error } }>{ errors.title }</TextOut> }
                        value={ values.title }
                        InputProps={ {
                            disableUnderline: true,
                            inputComponent: TextareaAutosize,
                            inputProps: {
                                className: cn(styles.title, styles['custom-textarea'])
                            }
                        } }
                    />
                </FormAndLabel>
                <FormAndLabel label="Описание" forId="excerpt">
                    <TextField
                        name="excerpt"
                        fullWidth={ true }
                        variant="standard"
                        placeholder="Введите описание"
                        onChange={ handleChange }
                        error={ touched.excerpt && !!errors.excerpt }
                        helperText={ touched.excerpt && <TextOut style={ { color: COLORS.error } }>{ errors.excerpt }</TextOut> }
                        value={ values.excerpt }
                        InputProps={ {
                            disableUnderline: true,
                            inputComponent: TextareaAutosize,
                            inputProps: {
                                className: styles['custom-textarea'],
                                style: { fontSize: '18px' }
                            }
                        } }
                    />
                </FormAndLabel>
                <FormAndLabel label="Изображение страницы">
                    <Tooltip
                        title={
                            <List>
                                Требования к изображению:
                                <ListItem>Изображение должно быть без текста.</ListItem>
                                <ListItem>Убирайте лишнее пустое место (отступы сверху и снизу) на изображении.</ListItem>
                                <ListItem>Минимальный размер изображения ширина - 1600 пикселей, высота - 1000 пикселей (с сохранением пропорций).</ListItem>
                            </List>
                        }
                    >
                        <Card variant="outlined" sx={ { maxWidth: 330 } }>
                            <CardActionArea onClick={ triggerFileSelectPopup }>
                                <CardMedia
                                    style={ { cursor: 'pointer' } }
                                    component="img"
                                    width="330"
                                    height="220"
                                    src={ articleMainImg ?? 'https://via.placeholder.com/1600x1000' }
                                    alt="Изображение страницы"
                                />
                                { !articleMainImg && (
                                    <Box height={ 40 } width={ 330 } display="flex" justifyContent="center" alignItems="center">
                                        <TextOut>
                                            Нажмите на изображение чтобы изменить его
                                        </TextOut>
                                    </Box>
                                ) }
                                <input
                                    ref={ fileInputRef }
                                    type="file"
                                    accept="image/png, image/jpeg, image/bmp, image/svg+xml"
                                    hidden={ true }
                                    onChange={ handleImageChange }
                                />
                            </CardActionArea>
                            { articleMainImg && (
                                <CardContent className={ styles.cardContent }>
                                    <ContainedButton size="small" kind="error" onClick={ handleRemoveImage }>
                                        Удалить
                                    </ContainedButton>
                                </CardContent>
                            ) }
                        </Card>
                    </Tooltip>
                    { !!touched.featured_media && (<TextOut style={ { color: COLORS.error } } inDiv={ true }>{ errors.featured_media }</TextOut>) }
                </FormAndLabel>
                { data && (
                    <FormAndLabel label="Категории">
                        <MultipleChipSelect
                            error={ touched.categories && !!errors.categories }
                            name="categories"
                            items={ data.rawData.reduce((items, { id: categoryId, name }) => {
                                if (name !== 'Без рубрики' && name !== 'Блог') {
                                    items.push({ value: categoryId.toString(), text: name });
                                }

                                return items;
                            }, [] as TMenuItem[]) }
                            selectedItems={ values.categories }
                            handleChange={ handleSelectedCategories }
                        />
                        { !!touched.categories && (<TextOut style={ { color: COLORS.error } } inDiv={ true }>{ errors.categories }</TextOut>) }
                    </FormAndLabel>
                ) }
                <Accordion>
                    <AccordionSummary expandIcon={ <ArrowDropDownIcon /> }>
                        <TextOut fontWeight={ 700 }>Требования к написанию статьи</TextOut>
                    </AccordionSummary>
                    <AccordionDetails>
                        <Box display="flex" flexDirection="column" gap="8px">
                            <FormAndLabel label={ `Минимальный уровень уникальности текста: ${ themes?.acf.theme_unique }` } />
                            <FormAndLabel label="Ключевые слова:">
                                { themes?.acf.theme_keywords }
                            </FormAndLabel>
                            <FormAndLabel label="Мета-описание:">
                                { themes?.acf.theme_description }
                            </FormAndLabel>
                            <FormAndLabel label="Статьи конкурентов:">
                                <ul>
                                    { themes?.acf.theme_resources.split(', ').map(link => <li><Link href={ link } rel="noreferrer" target="_blank">{ link }</Link></li>) }
                                </ul>
                            </FormAndLabel>
                        </Box>
                    </AccordionDetails>
                </Accordion>
                <FormAndLabel forId="content" label="Содержание">
                    <RichTextEditor
                        onImageUploadBefore={ handleImageUploadBefore }
                        value={ values.content }
                        setDefaultStyle="min-height: 30vh; font-family: 'Gilroy', sans-serif; max-width: 950px; padding: 4px; font-size: 18px; color: #192225;"
                        formName="content"
                        heightPercent="100%"
                        toolBarHeight={ 200 }
                        autoFocus={ false }
                        onPaste={ (_, html) => html.replace(/<\/?span[^>]*>/gi, '') }
                        language="ru"
                        toolbarButtonList={ buttonList }
                        onValueChange={ (formName, newValue) => {
                            setFieldValue(formName, newValue);
                        } }
                        showPathLabel={ false }
                        setOptions={ {
                            imageUrlInput: false
                        } }
                    />
                    { !!touched.content && (<TextOut style={ { color: COLORS.error } } inDiv={ true }>{ errors.content }</TextOut>) }
                </FormAndLabel>
                { !!chatData?.rawData.length && (
                    <FormAndLabel label="Комментарии">
                        <div className={ styles.comments } dangerouslySetInnerHTML={ { __html: chatData.rawData[0].content.rendered } } />
                    </FormAndLabel>
                ) }
                <FormAndLabel label="Где я могу проверить свой текст?">
                    <ul>
                        <li><Link href="https://text.ru/" rel="noreferrer" target="_blank">«Текст»</Link> — проверка текста на уникальность</li>
                        <li><Link href="https://glvrd.ru/" rel="noreferrer" target="_blank">«Главред»</Link> — помогает очистить текст от словесного мусора</li>
                        <li><Link href="https://orfogrammka.ru/" rel="noreferrer" target="_blank">«Орфограммка»</Link> — подскажет, где могут быть ошибки</li>
                        <li><Link href="https://turgenev.ashmanov.com/" rel="noreferrer" target="_blank">«Тургенев»</Link> – общая оценка текста</li>
                        <li><Link href="https://miratext.ru/keywords-checker/" rel="noreferrer" target="_blank">«Miratext»</Link> - проверка текста на вхождение ключевых слов</li>
                    </ul>
                </FormAndLabel>
            </Box>
            <Box gridTemplateColumns="1fr 1fr" marginTop="10px" display="grid" gap="10px" justifyContent="space-between">
                { (currentUserGroups.includes(AccountUserGroup.ArticleEditor) || currentUserGroups.includes(AccountUserGroup.CloudAdmin)) && coreArticle?.status === ArticleCoreStatusEnum.UnderInspection &&
                    (
                        <Box display="flex" gap="6px" gridColumn={ 1 }>
                            <OutlinedButton onClick={ () => handleEditBlogArticle({ ...values, rank_math_description: themes?.acf.theme_description ?? '' }, false) } kind="success">Сохранить</OutlinedButton>
                            <OutlinedButton onClick={ () => editArticleStatus(ArticleCoreStatusEnum.Published) } kind="success">Принять</OutlinedButton>
                            <OutlinedButton onClick={ openDialogHandler } kind="error">Отклонить</OutlinedButton>
                        </Box>
                    )
                }
                <Box justifyContent="flex-end" display="flex" gap="6px" gridColumn={ 2 }>
                    <ContainedButton onClick={ goBack }>
                        Закрыть
                    </ContainedButton>
                    { coreArticle?.accountUserId === userId() && (
                        <>
                            { isBlog && coreArticle?.status === ArticleCoreStatusEnum.Draft && (
                                <SuccessButton onClick={ handleSaveArticleForBlog }>
                                    Сохранить как черновик
                                </SuccessButton>
                            ) }
                            { (coreArticle?.status === ArticleCoreStatusEnum.Draft || coreArticle?.status === ArticleCoreStatusEnum.ForRevision) && (
                                <SuccessButton onClick={ (id && article) ? openDialogHandler : submitForm }>
                                    { isBlog ? 'На модерацию' : 'Сохранить' }
                                </SuccessButton>
                            ) }
                        </>
                    ) }
                </Box>
            </Box>
        </>
    );
};