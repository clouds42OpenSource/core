import { setCashFormat } from 'app/common/helpers';
import dayjs from 'dayjs';
import { ReactNode, useCallback, useEffect } from 'react';

import { ETransactionCause, ETransactionType } from 'app/api/endpoints/articles/response/getTransactionInfoResponse';
import { REDUX_API } from 'app/api/useReduxApi';
import { useAppDispatch, useAppSelector } from 'app/hooks';
import { CommonTableWithFilter, SelectDataInitiator } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { TablePagination } from 'app/views/modules/_common/components/CommonTableWithFilter/TableView';
import { SelectDataCommonDataModel } from 'app/web/common';
import { TextOut } from 'app/views/components/TextOut';

import { RowWrapper } from '../shared/RowWrapper';

type TArticleTransactionsProps = {
    isOtherAccount: boolean;
    contextUserId?: string;
};

const { getTransactionInfo } = REDUX_API.ARTICLES_REDUX;

export const ArticleTransactions = ({ isOtherAccount, contextUserId }: TArticleTransactionsProps) => {
    const dispatch = useAppDispatch();

    const { data } = useAppSelector(state => state.ArticlesState.getArticlesTransactionsReducer);

    useEffect(() => {
        if (!isOtherAccount) {
            void getTransactionInfo(dispatch);
        } else if (contextUserId) {
            void getTransactionInfo(dispatch, { filter: { accountUserId: contextUserId } });
        }
    }, [contextUserId, dispatch, isOtherAccount]);

    const onDataSelectHandler = (args: SelectDataCommonDataModel<void>, _: string | null, initiator: SelectDataInitiator) => {
        if (initiator === 'page-changed') {
            void getTransactionInfo(dispatch, { pageNumber: args.pageNumber });
        }
    };

    const getPagination = useCallback((): TablePagination | undefined => {
        if (data?.rawData) {
            const { pageCount, totalItemCount, pageNumber, pageSize } = data.rawData.metadata;

            return {
                totalPages: pageCount,
                recordsCount: totalItemCount,
                currentPage: pageNumber,
                pageSize,
            };
        }

        return undefined;
    }, [data?.rawData]);

    return (
        <CommonTableWithFilter
            uniqueContextProviderStateId="articleTransactions"
            isBorderStyled={ true }
            isResponsiveTable={ true }
            tableProps={ {
                dataset: data?.rawData.records ?? [],
                emptyText: 'Транзакции для блога не найдены',
                keyFieldName: 'id',
                pagination: getPagination(),
                fieldsView: {
                    createdOn: {
                        caption: 'Дата',
                        format: (value: string, row) =>
                            <RowWrapper isColored={ row.transactionType === ETransactionType.outflow }>{ dayjs(value).format('DD.MM.YYYY HH:mm') }</RowWrapper>
                    },
                    transactionCause: {
                        caption: 'Операция',
                        format: (value: ETransactionCause, row) => {
                            const isColored = row.transactionType === ETransactionType.outflow;
                            let text: ReactNode = '';

                            if (isColored) {
                                text = 'Вывод вознаграждения автору';
                            } else {
                                switch (value) {
                                    case ETransactionCause.articlePublication:
                                        text = (
                                            <TextOut>
                                                Публикация статьи: <TextOut fontWeight={ 700 }>{ row.topic }</TextOut>
                                            </TextOut>
                                        );
                                        break;
                                    case ETransactionCause.userRegistration:
                                        text = (
                                            <TextOut>
                                                Регистрация пользователя: <TextOut fontWeight={ 700 }>{ row.topic }</TextOut>
                                            </TextOut>
                                        );
                                        break;
                                    default:
                                        text = 'Не определено';
                                }
                            }

                            return (
                                <RowWrapper isColored={ isColored }>
                                    { text }
                                </RowWrapper>
                            );
                        }
                    },
                    amount: {
                        caption: 'Вознаграждение, руб',
                        format: (value: number, row) => {
                            const isColored = row.transactionType === ETransactionType.outflow;
                            let amount = setCashFormat(value).toString();

                            if (isColored) {
                                amount = `-${ amount }`;
                            }

                            return (
                                <RowWrapper isColored={ isColored }>{ amount }</RowWrapper>
                            );
                        },
                    },
                }
            } }
            onDataSelect={ onDataSelectHandler }
        />
    );
};