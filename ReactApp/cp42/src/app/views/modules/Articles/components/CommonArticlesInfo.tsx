import { FETCH_API } from 'app/api/useFetchApi';
import { setCashFormat } from 'app/common/helpers';
import { useAppDispatch, useAppSelector } from 'app/hooks';
import { InfoCard } from 'app/views/modules/ToPartners/components';
import { getHref } from 'app/views/modules/ToPartners/helpers';
import { useEffect } from 'react';

import styles from './style.module.css';

type TCommonArticlesInfoProps = {
    isOtherAccount: boolean;
    contextUserId?: string;
};

export const CommonArticlesInfo = ({ isOtherAccount, contextUserId }: TCommonArticlesInfoProps) => {
    const dispatch = useAppDispatch();
    const { data, isLoading } = useAppSelector(state => state.ArticlesState.getArticlesSummaryInfoReducer);

    useEffect(() => {
        if (!isOtherAccount) {
            void FETCH_API.ARTICLES.getSummaryInfo(dispatch);
        } if (contextUserId) {
            void FETCH_API.ARTICLES.getSummaryInfo(dispatch, contextUserId);
        }
    }, [contextUserId, dispatch, isOtherAccount]);

    return (
        <div className={ styles.articlesContainer }>
            <InfoCard
                isLoading={ isLoading }
                image={ getHref('businessman') }
                title="Всего статей написано"
                description={ data?.rawData?.count ?? '0' }
            />
            <InfoCard
                isLoading={ isLoading }
                image={ getHref('mans') }
                title="Всего зарегистрировано"
                description={ `${ data?.rawData?.usersCount ?? '0' } клиентов` }
            />
            <InfoCard
                isLoading={ isLoading }
                image={ getHref('piggybank') }
                title="Всего заработано"
                description={ `${ setCashFormat(data?.rawData?.totalEarned ?? 0) } руб` }
            />
            <InfoCard
                isLoading={ isLoading }
                image={ getHref('money') }
                title="Доступно к выплате"
                description={ `${ setCashFormat(data?.rawData?.availableSum ?? 0) } руб` }
            />
        </div>
    );
};