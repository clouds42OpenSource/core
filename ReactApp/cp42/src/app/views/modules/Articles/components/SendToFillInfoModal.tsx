import { TextOut } from 'app/views/components/TextOut';
import { SuccessButton } from 'app/views/components/controls/Button';
import { Dialog } from 'app/views/components/controls/Dialog';
import { useState } from 'react';
import { useHistory } from 'react-router';
import { Box } from '@mui/material';

export const SendToFillInfoModal = () => {
    const [isModalOpened, setIsModalOpened] = useState(false);
    const history = useHistory();

    const handleCloseModal = () => {
        setIsModalOpened(false);
    };

    const handleGoToProfile = () => {
        history.push('/profile');
    };

    return (
        <>
            <SuccessButton onClick={ () => setIsModalOpened(true) } style={ { marginBottom: '12px' } }>
                Написать статью
            </SuccessButton>
            <Dialog
                isTitleSmall={ true }
                maxHeight="300px"
                dialogWidth="xs"
                title="Подтверждение данных профиля"
                isOpen={ isModalOpened }
                onCancelClick={ handleCloseModal }
                buttons={
                    [
                        {
                            content: 'Закрыть',
                            kind: 'default',
                            onClick: () => handleCloseModal()
                        },
                        {
                            content: 'Перейти',
                            kind: 'success',
                            onClick: () => handleGoToProfile()
                        }
                    ] }
            >
                <Box display="flex" flexDirection="column" gap="5px">
                    <TextOut inDiv={ true }>
                        В вашем профиле недостает ключевых данных: Фамилия и Имя. Для нас критически важно, чтобы все материалы на нашем ресурсе были опубликованы от имени их авторов.
                    </TextOut>
                    <TextOut inDiv={ true }>
                        Хотите перейти к профилю и дополнить его этой информацией?
                    </TextOut>
                </Box>
            </Dialog>
        </>
    );
};