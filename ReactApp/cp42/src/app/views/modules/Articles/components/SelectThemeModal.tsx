import { memo, useState } from 'react';
import { Box, MenuItem, Select } from '@mui/material';
import { FETCH_API } from 'app/api/useFetchApi';
import { getArticleThemeResponseDto } from 'app/api/endpoints/articles/response/getArticleResponseDto';
import { Dialog } from 'app/views/components/controls/Dialog';
import { EMessageType, useAppDispatch, useFloatMessages } from 'app/hooks';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { TextOut } from 'app/views/components/TextOut';
import { SuccessButton } from 'app/views/components/controls/Button';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';

type Props = {
    updateArticles?: () => void;
};

const { getArticleThemes, editThemeStatus, getSummaryInfo } = FETCH_API.ARTICLES;

const SelectThemeModalView = ({ updateArticles }: Props) => {
    const [isModalOpened, setIsModalOpened] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const [selectedTheme, setSelectedTheme] = useState<string>('');
    const [articleThemes, setArticleThemes] = useState<getArticleThemeResponseDto[]>([]);

    const { show } = useFloatMessages();
    const dispatch = useAppDispatch();

    const handleGetThemes = async () => {
        setIsLoading(true);

        const response = await getArticleThemes();

        setIsLoading(false);

        if (response.data) {
            setArticleThemes(response.data);
        }
    };

    const handleSelectTheme = (value: string) => {
        setSelectedTheme(value);
    };

    const handleOpenModal = async () => {
        void handleGetThemes();
        setIsModalOpened(true);
    };

    const handleCloseModal = () => {
        setIsModalOpened(false);
    };

    const handleCreateArticle = async () => {
        const theme = articleThemes.find(articleTheme => articleTheme.id === selectedTheme);

        if (theme) {
            setIsLoading(true);
            const response = await FETCH_API.ARTICLES.createCoreArticle({
                topic: theme.title.rendered,
                topicId: theme.id,
                type: 2
            });

            if (response.success) {
                await editThemeStatus({ id: selectedTheme, status: 'draft' });
                void getSummaryInfo(dispatch);
                show(EMessageType.success, 'Тема выбрана успешно');
            } else {
                show(EMessageType.error, 'Ошибка при выборе темы');
            }

            handleCloseModal();

            if (updateArticles) {
                updateArticles();
            }

            void handleGetThemes();
            setSelectedTheme('');
            setIsLoading(false);
        }
    };

    return (
        <Box marginBottom="10px">
            { isLoading && <LoadingBounce /> }
            <Dialog
                isTitleSmall={ true }
                dialogWidth="xs"
                maxHeight="330px"
                title="Выберите тему"
                isOpen={ isModalOpened }
                onCancelClick={ handleCloseModal }
                buttons={
                    [
                        {
                            content: 'Отмена',
                            kind: 'default',
                            onClick: () => handleCloseModal()
                        },
                        {
                            content: 'Выбрать',
                            kind: 'success',
                            onClick: () => handleCreateArticle()
                        }
                    ] }
            >
                <Box>
                    <TextOut inDiv={ true }>
                        Ниже приведен список доступных тем для написания статей. Выберите интересующую вас тему, чтобы начать писать по ней статью.
                    </TextOut>
                    <FormAndLabel label="Темы" forId="select-label">
                        <Select
                            variant="outlined"
                            labelId="select-label"
                            id="select"
                            displayEmpty={ true }
                            value={ selectedTheme }
                            style={ { minWidth: '10vh' } }
                            fullWidth={ true }
                            size="small"
                            onChange={ ({ target: { value } }) => handleSelectTheme(value) }
                        >
                            <MenuItem value="">
                                Выберите тему
                            </MenuItem>
                            {
                                articleThemes.map(({ id, title, acf: { theme_unique } }) => {
                                    return (
                                        <MenuItem value={ id }>
                                            <Box height="20px" display="flex" alignItems="center">{ title.rendered } ({ theme_unique })</Box>
                                        </MenuItem>
                                    );
                                })
                            }
                        </Select>
                    </FormAndLabel>
                </Box>
            </Dialog>
            <SuccessButton onClick={ handleOpenModal }>
                Написать статью
            </SuccessButton>
        </Box>
    );
};

export const SelectThemeModal = memo(SelectThemeModalView);