import { COLORS } from 'app/utils';
import { TextOut } from 'app/views/components/TextOut';
import { ReactNode } from 'react';

type TProps = {
    isColored?: boolean;
    children?: ReactNode;
};

export const RowWrapper = ({ isColored, children }: TProps) => {
    return (
        <TextOut style={ { color: isColored ? COLORS.main : COLORS.font } } fontWeight={ isColored ? 700 : undefined }>
            { children }
        </TextOut>
    );
};