import { ArticleCoreStatusEnum } from 'app/api/endpoints/articles/response/getCoreArticleResponseDto';
import { COLORS } from 'app/utils';

type TArticleStatusDescription = {
    title: string;
    color: string;
    type: 'info' | 'warning' | 'primary' | 'error' | 'success';
};

export const ArticleStatusDescriptions: { [key in ArticleCoreStatusEnum]: TArticleStatusDescription } = {
    [ArticleCoreStatusEnum.Draft]: {
        title: 'Черновик',
        color: '#FFCF34',
        type: 'warning'
    },
    [ArticleCoreStatusEnum.UnderInspection]: {
        title: 'На проверке',
        color: '#52AAEF',
        type: 'info'
    },
    [ArticleCoreStatusEnum.Accepted]: {
        title: 'Принята',
        color: COLORS.main,
        type: 'success'
    },
    [ArticleCoreStatusEnum.ForRevision]: {
        title: 'На доработку',
        color: '#ED5565',
        type: 'error'
    },
    [ArticleCoreStatusEnum.Published]: {
        title: 'Опубликована',
        color: COLORS.main,
        type: 'primary'
    }
};