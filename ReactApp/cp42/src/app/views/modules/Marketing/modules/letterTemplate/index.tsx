import { TGetLetterTemplateDetailsResponse } from 'app/api/endpoints/marketing/response';
import { Divider, OutlinedInput, TextField } from '@mui/material';
import { number, object, ObjectSchema, string } from 'yup';
import React, { useState } from 'react';
import { useFormik } from 'formik';

import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { YUP_SCHEMA_ERROR_MESSAGES } from 'app/views/modules/Marketing/config/constants';
import { ContainedButton, OutlinedButton, SuccessButton } from 'app/views/components/controls/Button';
import { TEditLetterTemplateRequest } from 'app/api/endpoints/marketing/request';
import { useHistory } from 'react-router';

import { FETCH_API } from 'app/api/useFetchApi';
import { AutocompleteComboBoxForm } from 'app/views/components/controls/forms/ComboBox/AutocompleteComboBoxForm';
import { ComboboxItemModel } from 'app/views/components/controls/forms/ComboBox/models';
import { AppRoutes } from 'app/AppRoutes';
import { RichTextEditor } from 'app/views/components/RichTextEditor';
import { ButtonsToolbarListType } from 'app/views/components/RichTextEditor/types';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { LoadingBounce } from 'app/views/components/LoadingBounce';

import { TActiveTabHistory } from 'app/common/types/app-types';
import styles from './style.module.css';

type TLetterTemplate = FloatMessageProps & {
    formData?: TGetLetterTemplateDetailsResponse;
};

type TLetterTemplateSchema = Omit<TEditLetterTemplateRequest, 'id'>;

const { required } = YUP_SCHEMA_ERROR_MESSAGES;
const { sendTestLetter, editLetterTemplate } = FETCH_API.MARKETING;
const buttonList: ButtonsToolbarListType = [
    ['bold', 'underline', 'removeFormat'],
    ['font'],
    ['fontColor'],
    ['list'],
    ['table'],
    ['link', 'image', 'video'],
    ['fullScreen', 'codeView'],
];

const letterTemplateSchema: ObjectSchema<TLetterTemplateSchema> = object({
    body: string().required(required),
    subject: string().required(required),
    header: string().required(required),
    notificationText: string().required(required),
    sendGridTemplateId: string().required(required),
    advertisingBannerTemplateId: number().nullable(),
});

export const LetterTemplate = withFloatMessages(({ formData, floatMessage: { show } }: TLetterTemplate) => {
    const history = useHistory<TActiveTabHistory>();

    const [email, setEmail] = useState('');
    const [isLoading, setIsLoading] = useState(false);

    const {
        body,
        category,
        subject,
        header,
        sendGridTemplateId,
        advertisingBannerTemplateId,
        advertisingBannerTemplatesShortInfo,
        notificationText
    } = formData || {};

    const advertisingBannerItems: ComboboxItemModel<number>[] = (advertisingBannerTemplatesShortInfo ?? [])
        .map(({ id, header: bannerHeader }) => ({ text: bannerHeader, value: id }));

    const onCancelClickHandler = () => {
        history.push(AppRoutes.administration.marketing, { activeTabIndex: 1 });
    };

    const {
        handleChange,
        handleBlur,
        values,
        submitForm,
        touched,
        errors,
        setFieldValue,
        setFieldTouched,
        status,
        setStatus
    } = useFormik<TLetterTemplateSchema>({
        initialValues: {
            body: body ?? '',
            subject: subject ?? '',
            header: header ?? '',
            sendGridTemplateId: sendGridTemplateId ?? '',
            notificationText: notificationText ?? '',
            advertisingBannerTemplateId: advertisingBannerTemplateId ?? null,
        },
        onSubmit: async formValues => {
            if (status.isTestLetterRequest) {
                if (email) {
                    setIsLoading(true);

                    const { error } = await sendTestLetter({
                        email,
                        letterTemplateDataForNotification: {
                            id: formData?.id ?? -1,
                            ...formValues
                        }
                    });

                    if (error) {
                        show(MessageType.Error, error);
                    }
                    setIsLoading(false);
                } else {
                    show(MessageType.Warning, 'Введите E-mail');
                }
            } else {
                setIsLoading(true);

                const { error } = await editLetterTemplate({
                    id: formData?.id ?? -1,
                    ...formValues
                });

                if (error) {
                    show(MessageType.Error, error);
                } else {
                    show(MessageType.Success, 'Шаблон письма успешно отредактирован');
                    onCancelClickHandler();
                }

                setIsLoading(false);
            }
        },
        validateOnBlur: true,
        validateOnChange: true,
        validationSchema: letterTemplateSchema
    });

    const emailHandler = (ev: React.ChangeEvent<HTMLInputElement>) => {
        setEmail(ev.target.value);
    };

    const bodyChangeHandler = (bodyData: string) => {
        setFieldValue('body', bodyData);
        setFieldTouched('body');
    };

    const advertisingBannerIdHandler = (_: string, value: number | null) => {
        setFieldValue('advertisingBannerTemplateId', value);
        setFieldTouched('advertisingBannerTemplateId');
    };

    const sendTestLetterHandler = () => {
        setStatus({ isTestLetterRequest: true });
        void submitForm();
    };

    const saveLetterTemplateHandler = () => {
        setStatus({ isTestLetterRequest: false });
        void submitForm();
    };

    return (
        <>
            { isLoading && <LoadingBounce /> }
            <div className={ styles.container }>
                <FormAndLabel label="Тема письма">
                    <TextField
                        placeholder="Введите тему письма"
                        fullWidth={ true }
                        name="subject"
                        value={ values.subject }
                        onChange={ handleChange }
                        onBlur={ handleBlur }
                        error={ touched.subject && !!errors.subject }
                        helperText={ touched.subject && errors.subject }
                    />
                </FormAndLabel>
                <FormAndLabel label="Заголовок">
                    <TextField
                        placeholder="Введите заголовок"
                        fullWidth={ true }
                        name="header"
                        value={ values.header }
                        onChange={ handleChange }
                        onBlur={ handleBlur }
                        error={ touched.header && !!errors.header }
                        helperText={ touched.header && errors.header }
                    />
                </FormAndLabel>
                <FormAndLabel label="Тело письма">
                    <RichTextEditor
                        value={ values.body }
                        onValueChange={ (_, newValue) => bodyChangeHandler(newValue) }
                        autoFocus={ false }
                        formName="body"
                        heightPercent="200px"
                        language="ru"
                        toolBarHeight={ 35 }
                        toolbarButtonList={ buttonList }
                    />
                </FormAndLabel>
                <FormAndLabel label="Текст уведомления">
                    <TextField
                        placeholder="Введите текст уведомления"
                        fullWidth={ true }
                        name="notificationText"
                        value={ values.notificationText }
                        onChange={ handleChange }
                        onBlur={ handleBlur }
                        error={ touched.notificationText && !!errors.notificationText }
                        helperText={ touched.notificationText && errors.notificationText }
                        multiline={ true }
                        rows={ 4 }
                        maxRows={ 8 }
                    />
                </FormAndLabel>
                <FormAndLabel label="Рекламный баннер">
                    <AutocompleteComboBoxForm
                        formName="advertisingBannerTemplateId"
                        items={ [...advertisingBannerItems, { text: '--Не выбрано--', value: null }] }
                        value={ values.advertisingBannerTemplateId ?? null }
                        onValueChange={ advertisingBannerIdHandler }
                    />
                </FormAndLabel>
                <FormAndLabel label="ID шаблона SendGrid">
                    <TextField
                        placeholder="Введите ID шаблона"
                        fullWidth={ true }
                        name="subject"
                        value={ values.sendGridTemplateId }
                        onChange={ handleChange }
                        onBlur={ handleBlur }
                        error={ touched.sendGridTemplateId && !!errors.sendGridTemplateId }
                        helperText={ touched.sendGridTemplateId && errors.sendGridTemplateId }
                    />
                </FormAndLabel>
                <FormAndLabel label="Категория письма">
                    <OutlinedInput
                        disabled={ true }
                        fullWidth={ true }
                        value={ category }
                    />
                </FormAndLabel>
                <Divider />
                <FormAndLabel label="E-mail для тестовой отправки">
                    <div className={ styles.footerContainer }>
                        <div className={ styles.emailContainer }>
                            <OutlinedInput
                                fullWidth={ true }
                                placeholder="Введите E-mail"
                                value={ email }
                                onChange={ emailHandler }
                            />
                            <OutlinedButton fullWidth={ true } onClick={ sendTestLetterHandler }>Отправить тестовое</OutlinedButton>
                        </div>
                        <div className={ styles.buttonsContainer }>
                            <ContainedButton fullWidth={ true } onClick={ onCancelClickHandler }>Отмена</ContainedButton>
                            <SuccessButton fullWidth={ true } onClick={ saveLetterTemplateHandler }>Сохранить</SuccessButton>
                        </div>
                    </div>
                </FormAndLabel>
            </div>
        </>
    );
});