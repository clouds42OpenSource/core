import { Divider, SelectChangeEvent, TextField } from '@mui/material';
import { object, ObjectSchema, string } from 'yup';
import React, { useEffect, useState } from 'react';
import { useFormik } from 'formik';
import { useHistory } from 'react-router';
import dayjs from 'dayjs';

import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { ImageDropZone } from 'app/views/modules/_common/components/ImageDropZone';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { EAccountType } from 'app/api/endpoints/marketing/enum';
import { DoubleDateInputForm } from 'app/views/components/controls/forms/DoubleDateInputView';
import { ButtonGroup } from 'app/views/components/controls/ButtonGroup';
import { ButtonGroupItem } from 'app/views/components/controls/ButtonGroup/types';
import { TCalculateAudienceResponse, TGetAdvertisingBannerDetailsResponse } from 'app/api/endpoints/marketing/response';
import { ContainedButton, OutlinedButton, SuccessButton } from 'app/views/components/controls/Button';
import { TextOut } from 'app/views/components/TextOut';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { objectToFormData } from 'app/web/common';
import { AppRoutes } from 'app/AppRoutes';
import { getFileFromBase64 } from 'app/utils';
import { TCreateAdvertisingBannerRequest } from 'app/api/endpoints/marketing/request';
import { YUP_SCHEMA_ERROR_MESSAGES } from 'app/views/modules/Marketing/config/constants';
import { FETCH_API } from 'app/api/useFetchApi';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { RequestKind } from 'core/requestSender/enums';
import { ComboboxItemModel } from 'app/views/components/controls/forms/ComboBox/models';
import { MultipleChipSelect } from 'app/views/components/controls';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { TActiveTabHistory } from 'app/common/types/app-types';

import styles from './style.module.css';

type TAdvertisingBanner = FloatMessageProps & {
    formData?: TGetAdvertisingBannerDetailsResponse;
    isEditPage?: boolean;
};

const { createAdvertisingBanner, editAdvertisingBanner, calculateAudience, deleteAdvertisingBanner } = FETCH_API.MARKETING;
const { receiveLocales } = InterlayerApiProxy.getLocalesApiProxy();

const { maxLength, required } = YUP_SCHEMA_ERROR_MESSAGES;

const accountTypeButtons: ButtonGroupItem[] = [
    {
        index: EAccountType.all,
        text: 'Все'
    },
    {
        index: EAccountType.vip,
        text: 'VIP'
    },
    {
        index: EAccountType.notVip,
        text: 'Не VIP'
    }
];

const paymentAvailabilityButtons: ButtonGroupItem[] = [
    {
        index: 0,
        text: 'Все'
    },
    {
        index: 1,
        text: 'C оплатой'
    },
    {
        index: 2,
        text: 'Без оплаты'
    }
];

const serviceStateButtons: ButtonGroupItem[] = [
    {
        index: 0,
        text: 'Все',
    },
    {
        index: 1,
        text: 'Активен'
    },
    {
        index: 2,
        text: 'Заблокирован'
    }
];

type TAdvertisingBannerSchema = Omit<TCreateAdvertisingBannerRequest, 'image' | 'advertisingBannerAudience' >;

const advertisingBannerSchema: ObjectSchema<TAdvertisingBannerSchema> = object({
    header: string().required(required).max(70, `${ maxLength }70`),
    body: string().required(required).max(200, `${ maxLength }200`),
    captionLink: string().required(required).max(30, `${ maxLength }30`),
    link: string().required(required),
    showFrom: string().required(required),
    showTo: string().required(required),
});

export const AdvertisingBanner = withFloatMessages(({ formData, isEditPage, floatMessage: { show } }: TAdvertisingBanner) => {
    const {
        advertisingBannerAudience,
        image,
        header,
        body,
        link,
        captionLink,
        showFrom,
        showTo,
    } = formData ?? {};

    const history = useHistory<TActiveTabHistory>();

    const [bannerImage, setBannerImage] = useState<File | undefined>(image && image.base64 ? getFileFromBase64(image.base64, image.fileName, image.fileName) : undefined);
    const [currentAccountType, setCurrentAccountType] = useState(advertisingBannerAudience?.accountType ?? EAccountType.all);
    const [currentPaymentAvailability, setCurrentPaymentAvailability] = useState(advertisingBannerAudience?.availabilityPayment ?? 0);
    const [currentServiceState, setCurrentServiceState] = useState(advertisingBannerAudience?.rentalServiceState ?? 0);
    const [currentLocales, setCurrentLocales] = useState<string[]>(advertisingBannerAudience?.locales ?? []);
    const [currentAudience, setCurrentAudience] = useState<TCalculateAudienceResponse>({ accountsCount: 0, accountUsersCount: 0 });
    const [fetchedLocales, setFetchedLocales] = useState<ComboboxItemModel<string>[]>([]);

    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        (async () => {
            setIsLoading(true);

            const { records } = await receiveLocales(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, { pageNumber: 1 });
            setFetchedLocales((records ?? []).map(({ country, localeId }) => ({
                text: country,
                value: localeId
            })));

            if (!isEditPage) {
                setCurrentLocales((records ?? []).map(({ localeId }) => localeId));
            }

            setIsLoading(false);
        })();
    }, [isEditPage]);

    useEffect(() => {
        (async () => {
            const { data } = await calculateAudience({
                availabilityPayment: currentPaymentAvailability,
                accountType: currentAccountType,
                rentalServiceState: currentServiceState,
                locales: currentLocales
            });

            if (data) {
                setCurrentAudience(data);
            }
        })();
    }, [currentAccountType, currentLocales, currentPaymentAvailability, currentServiceState]);

    const {
        handleChange,
        handleBlur,
        values,
        submitForm,
        setFieldValue,
        touched,
        errors,
    } = useFormik<TAdvertisingBannerSchema>({
        initialValues: {
            header: header ?? '',
            body: body ?? '',
            captionLink: captionLink ?? '',
            link: link ?? '',
            showFrom: showFrom ?? dayjs(new Date()).toString(),
            showTo: showTo ?? dayjs('2099-12-31').toString()
        },
        onSubmit: async formValues => {
            if (bannerImage && currentLocales.length > 0) {
                setIsLoading(true);
                const requestBody: TCreateAdvertisingBannerRequest = {
                    ...formValues,
                    image: {
                        fileName: bannerImage.name,
                        contentType: bannerImage.type
                    },
                    advertisingBannerAudience: {
                        accountType: currentAccountType,
                        availabilityPayment: currentPaymentAvailability,
                        rentalServiceState: currentServiceState,
                        locales: currentLocales
                    }
                };
                const requestFormData = new FormData();
                requestFormData.append('image.content', bannerImage);

                if (isEditPage && formData) {
                    requestFormData.append('templateId', formData.templateId.toString());
                    const { error } = await editAdvertisingBanner(objectToFormData(requestBody, requestFormData));

                    if (error) {
                        show(MessageType.Error, error);
                    } else {
                        show(MessageType.Success, 'Рекламный баннер успешно отредактирован');
                        history.push(AppRoutes.administration.marketing);
                    }
                } else {
                    const { error } = await createAdvertisingBanner(objectToFormData(requestBody, requestFormData));

                    if (error) {
                        show(MessageType.Error, error);
                    } else {
                        show(MessageType.Success, 'Рекламный баннер успешно создан');
                        history.push(AppRoutes.administration.marketing);
                    }
                }

                setIsLoading(false);
            } else {
                show(MessageType.Warning, 'Заполните все поля');
            }
        },
        validateOnBlur: true,
        validateOnChange: true,
        validationSchema: advertisingBannerSchema
    });

    const onFileUploader = (file: File) => {
        setBannerImage(file);
    };

    const currentAccountTypeHandler = (index: number) => {
        setCurrentAccountType(index);
    };

    const currentPaymentAvailabilityHandler = (index: number) => {
        setCurrentPaymentAvailability(index);
    };

    const currentServiceStateHandler = (index: number) => {
        setCurrentServiceState(index);
    };

    const periodHandler = (formName: string, newValue: Date) => {
        if (formName === 'showFrom') {
            setFieldValue('showFrom', dayjs(newValue).toString());
        } else {
            setFieldValue('showTo', dayjs(newValue).toString());
        }
    };

    const localesHandler = (ev: SelectChangeEvent<string[]>) => {
        if (Array.isArray(ev.target.value)) {
            setCurrentLocales(ev.target.value);
        }
    };

    const onCancelClickHandler = () => {
        history.push(AppRoutes.administration.marketing, { activeTabIndex: 0 });
    };

    const deleteAdvertisingBannerHandler = async () => {
        setIsLoading(true);
        const { error } = await deleteAdvertisingBanner({ templateId: formData?.templateId ?? -1 });

        if (error) {
            show(MessageType.Error, error);
        } else {
            onCancelClickHandler();
        }

        setIsLoading(false);
    };

    return (
        <>
            { isLoading && <LoadingBounce /> }
            <div className={ styles.container }>
                <div className={ styles.gridContainer }>
                    <ImageDropZone
                        defaultFile={ bannerImage }
                        acceptTypes={ ['.png', '.jpg', '.jpeg'] }
                        onUploaded={ onFileUploader }
                    />
                    <div className={ styles.form }>
                        <FormAndLabel label="Заголовок баннера">
                            <TextField
                                placeholder="Введите заголовок баннера"
                                fullWidth={ true }
                                name="header"
                                value={ values.header }
                                onChange={ handleChange }
                                onBlur={ handleBlur }
                                error={ touched.header && !!errors.header }
                                helperText={ touched.header && errors.header }
                            />
                        </FormAndLabel>
                        <FormAndLabel label="Основной текст баннера">
                            <TextField
                                className={ styles.textArea }
                                placeholder="Введите основной текст баннера"
                                fullWidth={ true }
                                multiline={ true }
                                rows={ 5 }
                                name="body"
                                value={ values.body }
                                onChange={ handleChange }
                                onBlur={ handleBlur }
                                error={ touched.body && !!errors.body }
                                helperText={ touched.body && errors.body }
                            />
                        </FormAndLabel>
                        <FormAndLabel label="Текст ссылки">
                            <TextField
                                placeholder="Введите текст ссылки"
                                fullWidth={ true }
                                name="captionLink"
                                value={ values.captionLink }
                                onChange={ handleChange }
                                onBlur={ handleBlur }
                                error={ touched.captionLink && !!errors.captionLink }
                                helperText={ touched.captionLink && errors.captionLink }
                            />
                        </FormAndLabel>
                        <FormAndLabel label="Адрес ссылки">
                            <TextField
                                placeholder="Введите адрес ссылки"
                                fullWidth={ true }
                                name="link"
                                value={ values.link }
                                onChange={ handleChange }
                                onBlur={ handleBlur }
                                error={ touched.link && !!errors.link }
                                helperText={ touched.link && errors.link }
                            />
                        </FormAndLabel>
                        <FormAndLabel label="Период отображения баннера">
                            <DoubleDateInputForm
                                isNeedToStretch={ true }
                                periodFromValue={ dayjs(values.showFrom).toDate() }
                                periodToValue={ dayjs(values.showTo).toDate() }
                                periodFromInputName="showFrom"
                                periodToInputName="showTo"
                                onValueChange={ periodHandler }
                            />
                        </FormAndLabel>
                    </div>
                </div>
                <Divider />
                <div className={ styles.audience }>
                    <FormAndLabel label={ <TextOut fontWeight={ 600 } fontSize={ 26 }>Подбор аудитории</TextOut> }>
                        <div className={ styles.audienceForm }>
                            <FormAndLabel label="Потенциальная аудитория с учетом указанных ниже отборов:">
                                <div className={ styles.users }>
                                    <div>
                                        <i className="fa fa-users" />
                                        <TextOut>{ currentAudience.accountUsersCount }</TextOut>
                                    </div>
                                    <div>
                                        <i className="fa fa-home" />
                                        <TextOut>{ currentAudience.accountsCount }</TextOut>
                                    </div>
                                </div>
                            </FormAndLabel>
                            <FormAndLabel label="Локаль аккаунта">
                                <MultipleChipSelect
                                    items={ fetchedLocales }
                                    selectedItems={ currentLocales }
                                    handleChange={ localesHandler }
                                />
                            </FormAndLabel>
                            <ButtonGroup
                                items={ accountTypeButtons }
                                selectedButtonIndex={ currentAccountType }
                                onClick={ currentAccountTypeHandler }
                                label="Тип аккаунта"
                            />
                            <ButtonGroup
                                items={ paymentAvailabilityButtons }
                                selectedButtonIndex={ currentPaymentAvailability }
                                onClick={ currentPaymentAvailabilityHandler }
                                label="Наличие оплаты"
                            />
                            <ButtonGroup
                                items={ serviceStateButtons }
                                selectedButtonIndex={ currentServiceState }
                                onClick={ currentServiceStateHandler }
                                label="Состояние сервиса Аренда 1С"
                            />
                        </div>
                    </FormAndLabel>
                </div>
                <div className={ styles.buttons }>
                    { isEditPage &&
                        <OutlinedButton kind="error" onClick={ deleteAdvertisingBannerHandler }>Удалить баннер</OutlinedButton>
                    }
                    <div className={ styles.rightAlignButtons }>
                        <ContainedButton onClick={ onCancelClickHandler }>Отмена</ContainedButton>
                        <SuccessButton onClick={ submitForm }>{ isEditPage ? 'Сохранить' : 'Создать' }</SuccessButton>
                    </div>
                </div>
            </div>
        </>
    );
});