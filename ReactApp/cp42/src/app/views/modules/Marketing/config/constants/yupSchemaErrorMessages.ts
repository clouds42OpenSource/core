export const YUP_SCHEMA_ERROR_MESSAGES = {
    required: 'Это поле должно быть заполнено',
    maxLength: 'Максимальное количество символов - '
} as const;