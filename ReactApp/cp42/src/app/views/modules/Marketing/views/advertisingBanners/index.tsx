import { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { AppRoutes } from 'app/AppRoutes';
import { useHistory } from 'react-router';

import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { useAppDispatch, useAppSelector } from 'app/hooks';
import { REDUX_API } from 'app/api/useReduxApi';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { SuccessButton } from 'app/views/components/controls/Button';
import { setDateFormat } from 'app/common/helpers';

import globalStyles from '../style.module.css';

const { getAdvertisingBannersList } = REDUX_API.MARKETING_REDUX;

export const AdvertisingBanners = withFloatMessages(({ floatMessage: { show } }) => {
    const history = useHistory();
    const dispatch = useAppDispatch();
    const { data, isLoading, error } = useAppSelector(state => state.MarketingState.advertisingBannersListReducer);

    useEffect(() => {
        getAdvertisingBannersList(dispatch);
    }, [dispatch]);

    useEffect(() => {
        if (error) {
            show(MessageType.Error, error);
        }
    }, [error, show]);

    const addAdvertisingBannerHandler = () => {
        history.push(AppRoutes.marketing.createAdvertisingBanner);
    };

    return (
        <>
            { isLoading && <LoadingBounce /> }
            <div className={ globalStyles.container }>
                <SuccessButton onClick={ addAdvertisingBannerHandler }><i className="fa fa-plus-circle" aria-hidden="true" />Добавить рекламный баннер</SuccessButton>
                <CommonTableWithFilter
                    isResponsiveTable={ true }
                    isBorderStyled={ true }
                    uniqueContextProviderStateId="AdvertisingBannersList"
                    tableProps={ {
                        dataset: data ?? [],
                        keyFieldName: 'id',
                        emptyText: 'Рекламные баннеры не найдены',
                        fieldsView: {
                            header: {
                                caption: 'Заголовок баннера',
                                format: (value: string, bannerData) =>
                                    <Link to={ `${ AppRoutes.marketing.editAdvertisingBanner }?id=${ bannerData.id }` } className={ globalStyles.link }>{ value }</Link>
                            },
                            body: {
                                caption: 'Тело баннера',
                                fieldWidth: '35%'
                            },
                            captionLink: {
                                caption: 'Описание ссылки'
                            },
                            showFrom: {
                                caption: 'Показывать с',
                                format: (value: string) =>
                                    setDateFormat(value)
                            },
                            showTo: {
                                caption: 'Показывать по',
                                format: (value: string) =>
                                    setDateFormat(value)
                            }
                        }
                    } }
                />
            </div>
        </>
    );
});