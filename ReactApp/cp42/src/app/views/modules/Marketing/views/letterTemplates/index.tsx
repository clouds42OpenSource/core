import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

import { AppRoutes } from 'app/AppRoutes';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { useAppDispatch, useAppSelector } from 'app/hooks';
import { REDUX_API } from 'app/api/useReduxApi';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { ButtonGroupItem } from 'app/views/components/controls/ButtonGroup/types';
import { ButtonGroup } from 'app/views/components/controls/ButtonGroup';

import globalStyles from '../style.module.css';

const { getLetterTemplatesList } = REDUX_API.MARKETING_REDUX;

const letterTypeButtons: ButtonGroupItem[] = [
    {
        index: 0,
        text: 'Клиентские письма'
    },
    {
        index: 1,
        text: 'Системные письма'
    }
];

export const LetterTemplates = withFloatMessages(({ floatMessage: { show } }) => {
    const dispatch = useAppDispatch();
    const { data, isLoading, error } = useAppSelector(state => state.MarketingState.letterTemplatesListReducer);

    const [currentLetterType, setCurrentLetterType] = useState(0);

    useEffect(() => {
        getLetterTemplatesList(dispatch);
    }, [dispatch]);

    useEffect(() => {
        if (error) {
            show(MessageType.Error, error);
        }
    }, [error, show]);

    const currentLetterTypeHandler = (index: number) => {
        setCurrentLetterType(index);
    };

    return (
        <>
            { isLoading && <LoadingBounce /> }
            <div className={ globalStyles.letterTemplateContainer }>
                <ButtonGroup
                    items={ letterTypeButtons }
                    selectedButtonIndex={ currentLetterType }
                    onClick={ currentLetterTypeHandler }
                    label="Тип писем"
                />
                <CommonTableWithFilter
                    isResponsiveTable={ true }
                    isBorderStyled={ true }
                    uniqueContextProviderStateId="LettersList"
                    tableProps={ {
                        dataset: data ? data.filter(({ isSystem }) => (currentLetterType === 0 ? !isSystem : isSystem)) : [],
                        keyFieldName: 'id',
                        emptyText: 'Письма не найдены',
                        fieldsView: {
                            subject: {
                                caption: 'Тема',
                                format: (value: string, letterData) =>
                                    <Link
                                        className={ globalStyles.link }
                                        to={ `${ AppRoutes.marketing.editLetterTemplate }?id=${ letterData.id }` }
                                    >
                                        { value }
                                    </Link>
                            },
                            letterCategory: {
                                caption: 'Категория'
                            },
                            advertisingBannerTemplateName: {
                                caption: 'Рекламный баннер'
                            },
                        }
                    } }
                />
            </div>
        </>
    );
});