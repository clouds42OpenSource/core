import { useEffect, useState } from 'react';
import { useLocation } from 'react-router';

import { LetterTemplate } from 'app/views/modules/Marketing/modules';
import { FETCH_API } from 'app/api/useFetchApi';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { TGetLetterTemplateDetailsResponse } from 'app/api/endpoints/marketing/response';
import { PageHeaderView } from 'app/views/components/_hoc/withHeader/PageHeaderView';

const { getLetterTemplateDetails } = FETCH_API.MARKETING;

const LetterTemplateEditComponent = withFloatMessages(({ floatMessage: { show } }) => {
    const location = useLocation();

    const [isLoading, setIsLoading] = useState(false);
    const [formData, setFormData] = useState<TGetLetterTemplateDetailsResponse>();

    const letterTemplateId = new URLSearchParams(location.search).get('id') ?? '';

    useEffect(() => {
        (async () => {
            setIsLoading(true);
            const id = parseInt(letterTemplateId, 10);

            const { error, data } = await getLetterTemplateDetails({ templateId: id });

            if (error) {
                show(MessageType.Error, error);
            } else {
                setFormData(data ?? undefined);
            }

            setIsLoading(false);
        })();
    }, [letterTemplateId, show]);

    return (
        <>
            <PageHeaderView title={ `Редактирование шаблона письма ${ formData ? `"${ formData?.header }"` : '' }` } />
            { isLoading && <LoadingBounce /> }
            { formData &&
                <LetterTemplate formData={ formData } />
            }
        </>
    );
});

export const LetterTemplateEdit = LetterTemplateEditComponent;