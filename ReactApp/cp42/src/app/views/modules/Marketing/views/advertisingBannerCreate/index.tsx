import { withHeader } from 'app/views/components/_hoc/withHeader';
import { AdvertisingBanner } from 'app/views/modules/Marketing/modules';

export const AdvertisingBannerCreate = withHeader({
    title: 'Добавление нового рекламного баннера',
    page: AdvertisingBanner
});