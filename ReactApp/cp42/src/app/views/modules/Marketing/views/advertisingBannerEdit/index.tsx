import { useEffect, useState } from 'react';

import { withHeader } from 'app/views/components/_hoc/withHeader';
import { AdvertisingBanner } from 'app/views/modules/Marketing/modules';
import { useLocation } from 'react-router';
import { FETCH_API } from 'app/api/useFetchApi';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { TGetAdvertisingBannerDetailsResponse } from 'app/api/endpoints/marketing/response';

const { getAdvertisingBannerDetails } = FETCH_API.MARKETING;

const AdvertisingBannerEditComponent = withFloatMessages(({ floatMessage: { show } }) => {
    const location = useLocation();

    const [isLoading, setIsLoading] = useState(false);
    const [formData, setFormData] = useState<TGetAdvertisingBannerDetailsResponse>();

    const advertisingBannerId = new URLSearchParams(location.search).get('id') ?? '';

    useEffect(() => {
        (async () => {
            setIsLoading(true);
            const id = parseInt(advertisingBannerId, 10);

            const { error, data } = await getAdvertisingBannerDetails({ templateId: id });

            if (error) {
                show(MessageType.Error, error);
            } else {
                setFormData(data ?? undefined);
            }

            setIsLoading(false);
        })();
    }, [advertisingBannerId, show]);

    return (
        <>
            { isLoading && <LoadingBounce /> }
            { formData &&
                <AdvertisingBanner formData={ formData } isEditPage={ true } />
            }
        </>
    );
});

export const AdvertisingBannerEdit = withHeader({
    title: 'Редактирование рекламного баннера',
    page: AdvertisingBannerEditComponent
});