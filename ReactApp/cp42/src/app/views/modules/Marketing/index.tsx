import { THeaders } from 'app/views/components/controls/TabsControl/views/TabView';
import { TabsControl } from 'app/views/components/controls/TabsControl';
import { withHeader } from 'app/views/components/_hoc/withHeader';
import { useHistory } from 'react-router';
import { TActiveTabHistory } from 'app/common/types/app-types';
import { AdvertisingBanners, LetterTemplates } from './views';

export { AdvertisingBannerCreate, AdvertisingBannerEdit, LetterTemplates, LetterTemplateEdit } from './views';

const HEADERS: THeaders = [
    {
        title: 'Рекламные баннеры',
        isVisible: true,
    },
    {
        title: 'Письма',
        isVisible: true,
    },
];

const Marketing = () => {
    const history = useHistory<TActiveTabHistory>();

    return (
        <TabsControl headers={ HEADERS } activeTabIndex={ history.location.state?.activeTabIndex }>
            <AdvertisingBanners />
            <LetterTemplates />
        </TabsControl>
    );
};

export const MarketingView = withHeader({
    title: 'Маркетинг',
    page: Marketing
});