import { Box, Typography } from '@mui/material';
import { AppRoutes } from 'app/AppRoutes';
import { FETCH_API } from 'app/api/useFetchApi';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { withHeader } from 'app/views/components/_hoc/withHeader';
import { SuccessButton } from 'app/views/components/controls/Button';
import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

const UnsubscribePage = () => {
    const params = new URLSearchParams(document.location.search);
    const code = params.get('code');
    const [isLoading, setIsLoading] = useState(false);
    const [isSuccess, setIsSuccess] = useState(false);

    const handleUnsubscribe = async (userCode: string) => {
        if (userCode) {
            setIsLoading(true);
            const response = await FETCH_API.UNSUBSCRIBE.tryUnsubscribe(userCode);
            setIsLoading(false);
            if (!response.error) {
                setIsSuccess(true);
            }
        }
    };

    useEffect(() => {
        if (code) {
            void handleUnsubscribe(code);
        }
    }, [code]);

    if (isLoading) {
        return <LoadingBounce noMargin={ true } />;
    }

    return (
        <Box minHeight="100vh" color="#333333">
            <Box padding="40px">
                <img height="50px" src={ `${ import.meta.env.REACT_APP_API_HOST }/img/registration/logo.png` } alt="42clouds" />
                <Box display="flex" marginTop="40px" flexDirection="column" maxWidth="550px" gap="20px">
                    <Typography variant="h4" fontWeight="bold">
                        {isSuccess ? 'Вы успешно отписаны!' : 'Ошибка отписки от рассылки'}
                    </Typography>
                    <Typography variant="subtitle1">
                        {isSuccess ? 'Подписка на рассылку успешно отменена! Если захотите снова получать от нас письма или отписались по ошибке, напишите на почту '
                            : 'Попробуйте еще раз или обратитесь в тех поддержку '}
                        <a style={ { color: '#1AB39F' } } href="mailto:support@42clouds.com">support@42clouds.com</a>
                    </Typography>
                    <Link to={ AppRoutes.homeRoute }>
                        <SuccessButton>
                            Перейти на главную
                        </SuccessButton>
                    </Link>
                </Box>
            </Box>
        </Box>
    );
};

export const UnsubscribePageView = withHeader({
    title: 'Отписка от рассылки',
    page: withFloatMessages(UnsubscribePage)
});