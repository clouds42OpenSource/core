import { AppRoutes } from 'app/AppRoutes';
import { TextOut } from 'app/views/components/TextOut';
import { Dialog } from 'app/views/components/controls/Dialog';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { RequestKind } from 'core/requestSender/enums';
import React from 'react';

type OwnProps = {
    isOpen: boolean;
    completeStatus: boolean;
    canUsePromisePayment: boolean;
    cost: number;
    currency: string;
    notEnoughMoney: number;
    databaseId: string;
    serviceId: string;
    message: React.ReactNode;
    typeOperation: boolean;
    closeDialog: () => void;
};

export class ArchiveDbTypeDialogMessage extends React.Component<OwnProps, {}> {
    constructor(props: OwnProps) {
        super(props);

        this.archiveBuy = this.archiveBuy.bind(this);
    }

    private archiveBuy(isPromisePayment: boolean) {
        const api = InterlayerApiProxy.getInfoDbCardApi();

        try {
            void api.ArchiveOrBackup(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, {
                cost: this.props.cost,
                currency: this.props.currency,
                databaseId: this.props.databaseId,
                operation: this.props.typeOperation ? 1 : 2,
                serviceId: this.props.serviceId,
                usePromisePayment: isPromisePayment
            });
            this.props.closeDialog();
        } catch (er) {
            console.log(er);
        }
    }

    public render() {
        return (
            <Dialog
                dialogWidth="sm"
                isOpen={ this.props.isOpen }
                onCancelClick={ this.props.closeDialog }
                dialogVerticalAlign="center"
                buttons={ this.props.completeStatus
                    ? [
                        {
                            kind: 'default',
                            variant: 'contained',
                            content: 'Отмена',
                            onClick: this.props.closeDialog
                        },
                        {
                            kind: 'success',
                            variant: 'contained',
                            content: this.props.typeOperation
                                ? `Переместить в архив (${ this.props.cost } ${ this.props.currency })`
                                : `Создать бекап (${ this.props.cost } ${ this.props.currency })`,
                            onClick: () => this.archiveBuy(false)
                        }
                    ]
                    : this.props.canUsePromisePayment
                        ? [
                            {
                                kind: 'default',
                                variant: 'contained',
                                content: 'Отмена',
                                onClick: this.props.closeDialog
                            },
                            {
                                kind: 'primary',
                                variant: 'contained',
                                content: `Пополнить счет на (${ this.props.notEnoughMoney } ${ this.props.currency })`,
                                onClick: () => window.open(
                                    `${ window.location.origin }${ AppRoutes.accountManagement.balanceManagement }/replenish-balance?customPayment.amount=${ this.props.notEnoughMoney }&customPayment.description=Оплата+за+перенос+ИБ+в+архив+`,
                                    '_blank'
                                )
                            },
                            {
                                kind: 'primary',
                                variant: 'contained',
                                content: 'Обещанный платеж',
                                onClick: () => this.archiveBuy(true)
                            }
                        ]
                        : [
                            {
                                kind: 'default',
                                variant: 'contained',
                                content: 'Отмена',
                                onClick: this.props.closeDialog
                            },
                            {
                                kind: 'primary',
                                variant: 'contained',
                                content: `Пополнить счет на (${ this.props.notEnoughMoney } ${ this.props.currency })`,
                                onClick: () => window.open(
                                    `${ window.location.origin }${ AppRoutes.accountManagement.balanceManagement }/replenish-balance?customPayment.amount=${ this.props.notEnoughMoney }&customPayment.description=Оплата+за+перенос+ИБ+в+архив+`,
                                    '_blank'
                                )
                            }
                        ]
                }
            >
                <TextOut fontSize={ 14 } style={ { wordBreak: 'normal' } }>
                    { this.props.message } Стоимость { this.props.cost } { this.props.currency }.
                </TextOut>
            </Dialog>
        );
    }
}