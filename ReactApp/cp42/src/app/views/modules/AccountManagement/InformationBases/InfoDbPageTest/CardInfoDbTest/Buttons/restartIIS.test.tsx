import '@testing-library/jest-dom/extend-expect';
import { render } from '@testing-library/react';
import { DbPublishState } from 'app/common/enums';
import { createAppReduxStore } from 'app/redux/functions';
import { RestartPoolIisButton } from 'app/views/modules/_common/reusable-modules/InfoDbCard/buttons/RestartPoolIisButton';
import { SnackbarProvider } from 'notistack';
import { Provider } from 'react-redux';
import { applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';

describe('publish Button', () => {
    let onChangeField: jest.Mock;
    const store = createAppReduxStore(composeWithDevTools(applyMiddleware(thunk)));

    it('restart renderButton ', async () => {
        const { queryByText } = render(
            <Provider store={ { ...store } }>
                <SnackbarProvider>
                    <RestartPoolIisButton
                        databaseId=""
                        onChangeField={ onChangeField }
                        isInPublishingState={ DbPublishState.Published }
                    />
                </SnackbarProvider>
            </Provider>
        );
        expect(queryByText(/Перезапустить пул IIS/)).toBeInTheDocument();
    });
    it('restart renderButton in proccess', async () => {
        const { queryByText } = render(
            <Provider store={ { ...store } }>
                <SnackbarProvider>
                    <RestartPoolIisButton
                        databaseId=""
                        onChangeField={ onChangeField }
                        isInPublishingState={ DbPublishState.RestartingPool }
                    />
                </SnackbarProvider>
            </Provider>
        );
        expect(queryByText(/Перезапуск пула IIS.../)).toBeInTheDocument();
    });
});