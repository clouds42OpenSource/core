import { contextAccountId, userId } from 'app/api';
import { AccountUserGroup, DatabaseState } from 'app/common/enums';
import { getErrorMessage } from 'app/common/functions/getErrorMessage';
import { localStorageHelper } from 'app/common/helpers/LocalStorageHelper';
import { ProcessIdStateHelper } from 'app/common/helpers/ProcessIdStateHelper';
import { TimerHelper } from 'app/common/helpers/TimerHelper';
import { InfoDbListProcessId } from 'app/modules/infoDbList/InfoDbListProcessId';
import { DeleteInfoDbThunkParams } from 'app/modules/infoDbList/store/reducers/deleteDbTemplateReducer/params';
import { EditInfoDbThunkParams } from 'app/modules/infoDbList/store/reducers/editInfoDbReducer/params';
import { ReceiveInfoDbListThunkParams } from 'app/modules/infoDbList/store/reducers/receiveInfoDbListReducer/params';
import { ReceiveInfoDbListOtherThunk, ReceiveInfoDbListThunk } from 'app/modules/infoDbList/store/thunks';
import { AvailabilityInfoDbListThunk } from 'app/modules/infoDbList/store/thunks/AvailabilityInfoDbListThunk';
import { DeleteInfoDbThunk } from 'app/modules/infoDbList/store/thunks/DeleteDbTemplateThunk';
import { EditDbListThunk } from 'app/modules/infoDbList/store/thunks/EditDbListThunk';
import { StatusInfoDbOnDelimitersListThunk } from 'app/modules/infoDbList/store/thunks/StatusInfoDbListOnDelimitersThunk';
import { StatusInfoDbListThunk } from 'app/modules/infoDbList/store/thunks/StatusInfoDbListThunk';
import { AppReduxStoreState } from 'app/redux/types';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { withHeader } from 'app/views/components/_hoc/withHeader';
import { getFieldInfoDb } from 'app/views/modules/AccountManagement/InformationBases/functions/getFieldInfoDb';
import { addCountAndSizeDb, removeCountAndSizeDb } from 'app/views/modules/AccountManagement/InformationBases/functions/onChangeCountAndSizeDb';
import { AccountInfoDbListFilterDataForm } from 'app/views/modules/AccountManagement/InformationBases/types/AccountInfoDbListFilterDataForm';
import { CollapseContentViews } from 'app/views/modules/AccountManagement/InformationBases/views/CollapseContent';
import { ControlPanelInfoDbViews } from 'app/views/modules/AccountManagement/InformationBases/views/ControlPanelInfoDb';
import { TableDbViews } from 'app/views/modules/AccountManagement/InformationBases/views/TableDb';
import { InfoDbCard } from 'app/views/modules/_common/reusable-modules/InfoDbCard';
import { defineInfoDbCheckState, defineInfoDbOnDelimitersCheckState } from 'app/views/modules/_common/reusable-modules/InfoDbCard/functions/defineInfoDbCheckState';
import { TablePagination } from 'app/views/modules/_common/components/CommonTableWithFilter/TableView';
import { InfoDbListItemDataModel } from 'app/web/InterlayerApiProxy/InfoDbApiProxy/receiveDatabaseList/data-models';
import { InputAccountId } from 'app/web/api/InfoDbProxy/request-dto/InputAccountId';
import { InputIdsArr } from 'app/web/api/InfoDbProxy/request-dto/InputIdsArr';
import { GetAvailabilityInfoDb } from 'app/web/api/InfoDbProxy/responce-dto/GetAvailabilityInfoDb';
import { InfoDbTypeEnum, SelectInfoDbGetQueryParams } from 'app/web/common/data-models/SelectInfoDbGetQueryParams';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';
import React, { createRef } from 'react';
import { connect } from 'react-redux';

type StateProps<TDataRow = InfoDbListItemDataModel> = {
    dataset: TDataRow[];
    datasetOther: TDataRow[];
    pagination: TablePagination;
    paginationOther: TablePagination;
    availability: GetAvailabilityInfoDb;
    AllDatabasesCount: number;
    ServerDatabasesCount: number;
    ArchievedDatabasesCount: number;
    FileDatabasesCount: number;
    DelimeterDatabasesCount: number;
    DeletedDatabases: number;
    currentUserGroups: Array<AccountUserGroup>;
    hasInfoDbListReceived: boolean;
    hasInfoDbListStatusReceived: boolean;
    hasAvailabilityDbListReceived: boolean;
    permissionsAll: string[];
    errorMessage?: string | null;
};

type DispatchProps = {
    dispatchReceiveInfoDbListThunk: (args: ReceiveInfoDbListThunkParams) => void;
    dispatchReceiveInfoDbListOtherThunk: (args: ReceiveInfoDbListThunkParams) => void;
    dispatchDeleteInfoDbThunk: (args: DeleteInfoDbThunkParams) => void;
    dispatchEditInfoDbThunk: (args: EditInfoDbThunkParams) => void;
    dispatchStatusInfoDbThunk: (args: IForceThunkParam & InputIdsArr) => void;
    dispatchStatusInfoDbOnDelimitersThunk: (args: IForceThunkParam & InputIdsArr) => void;
    dispatchAvailabilityInfoDbThunk: (args: IForceThunkParam & InputAccountId) => void;
};

export type OwnStateDbList = {
    pageSize?: number;
    pageSizeOther: number;
    valueCheck: string[];
    allCheck: boolean;
    allSizeInMb: number;
    serverBases: {
        count: number;
        size: number;
    };
    fileBases: {
        count: number;
        size: number;
    };
    isInfoDbCardVisible: boolean;
    databaseName: string;
    item: InfoDbListItemDataModel | null;
    ids: string[];
    idsOnDelimiters: string[];
    changeField: boolean;
    activeTab: number;
    tabTypeBases: number;
    loading: boolean;
    isOther: boolean;
    initState: boolean;
};

type AllProps = StateProps & DispatchProps & FloatMessageProps;

class InformationBasesViewClass extends React.Component<AllProps, OwnStateDbList> {
    private intervalId: NodeJS.Timeout | number;

    private searchViewRef = createRef<ControlPanelInfoDbViews>();

    private accountId = contextAccountId();

    private accountUserId = userId();

    public constructor(props: AllProps) {
        super(props);
        this.state = {
            tabTypeBases: 0,
            pageSize: 10,
            pageSizeOther: 10,
            valueCheck: [],
            allCheck: false,
            allSizeInMb: 0,
            serverBases: {
                count: 0,
                size: 0
            },
            fileBases: {
                count: 0,
                size: 0
            },
            isInfoDbCardVisible: false,
            databaseName: '',
            item: null,
            ids: [],
            idsOnDelimiters: [],
            changeField: false,
            activeTab: 0,
            loading: false,
            isOther: false,
            initState: false
        };

        this.intervalId = setInterval(() => void 0, TimerHelper.waitTimeRefreshDatabaseState);
        this.onDataSelect = this.onDataSelect.bind(this);
        this.onDataSelectOther = this.onDataSelectOther.bind(this);
        this.onValueChange = this.onValueChange.bind(this);
        this.onValueChangeAll = this.onValueChangeAll.bind(this);
        this.onDeleteDb = this.onDeleteDb.bind(this);
        this.showInfoDbCard = this.showInfoDbCard.bind(this);
        this.hideDatabaseCard = this.hideDatabaseCard.bind(this);
        this.onChangeField = this.onChangeField.bind(this);
        this.deleteIdDbStates = this.deleteIdDbStates.bind(this);
        this.clearTabIndex = this.clearTabIndex.bind(this);
        this.onChangeTab = this.onChangeTab.bind(this);
        this.initialRefreshData = this.initialRefreshData.bind(this);
        this.getIdsList = this.getIdsList.bind(this);
    }

    public componentDidMount() {
        window.scrollTo(0, 0);
        clearInterval(this.intervalId);

        this.props.dispatchReceiveInfoDbListThunk({
            search: '',
            type: InfoDbTypeEnum.all,
            pageNumber: 1,
            field: '',
            size: 10,
            affiliation: 0,
            sort: 0,
            accountID: this.accountId || '',
            accountUserId: localStorageHelper.getUserId() || '',
            showLoadingProgress: false,
            force: true
        });

        this.intervalId = setInterval(() => {
            this.props.dispatchStatusInfoDbThunk({
                ids: this.state.ids,
                force: true,
                showLoadingProgress: false
            });
            this.props.dispatchStatusInfoDbOnDelimitersThunk({
                ids: this.state.idsOnDelimiters,
                force: true,
                showLoadingProgress: false
            });
        }, TimerHelper.waitTimeRefreshDatabaseState);

        this.props.dispatchAvailabilityInfoDbThunk({
            accountId: this.accountId,
            accountUserId: this.accountUserId,
            force: true,
        });
    }

    public componentDidUpdate(prevProps: AllProps): void {
        if (!prevProps.hasAvailabilityDbListReceived && this.props.hasAvailabilityDbListReceived) {
            this.props.dispatchReceiveInfoDbListOtherThunk({
                accountID: this.accountId,
                pageNumber: 1,
                search: '',
                type: InfoDbTypeEnum.all,
                force: true,
                size: 10,
                affiliation: 1,
                field: '',
                sort: 0,
                accountUserId: localStorageHelper.getUserId() || '',
                showLoadingProgress: false
            });
            this.props.dispatchReceiveInfoDbListThunk({
                search: '',
                type: InfoDbTypeEnum.all,
                pageNumber: 1,
                field: '',
                size: 10,
                affiliation: 0,
                sort: 0,
                accountID: this.accountId || '',
                accountUserId: localStorageHelper.getUserId() || '',
                showLoadingProgress: false
            });
        }

        if (!prevProps.errorMessage && this.props.errorMessage) {
            this.props.floatMessage.show(MessageType.Error, this.props.errorMessage);
        }

        if (!this.props.dataset.length && prevProps.dataset.length) {
            clearInterval(this.intervalId);
        }

        if (this.props.hasInfoDbListReceived && !prevProps.hasInfoDbListReceived && this.props.dataset.length) {
            this.setState(
                {
                    ids: this.getIdsList(false),
                    idsOnDelimiters: this.getIdsList(true)
                },
                () => { this.initialRefreshData(); }
            );
        }

        if (!this.props.dataset.length && prevProps.dataset.length) {
            clearInterval(this.intervalId);
        }

        if (!this.props.hasInfoDbListStatusReceived && prevProps.hasInfoDbListStatusReceived) {
            this.setState(
                {
                    ids: this.getIdsList(false),
                    idsOnDelimiters: this.getIdsList(true)
                },
                () => { this.initialRefreshData(); }
            );
        }

        if (this.props.hasInfoDbListReceived && this.props.dataset.length && (!this.state.initState || (!prevProps.hasInfoDbListReceived && this.props.hasInfoDbListReceived))) {
            this.setState({
                initState: true,
                ids: this.getIdsList(false),
                idsOnDelimiters: this.getIdsList(true)
            }, () => { this.initialRefreshData(); });

            const ids = this.getIdsList(true);

            if (ids.length) {
                this.props.dispatchStatusInfoDbOnDelimitersThunk({
                    ids,
                    force: true,
                    showLoadingProgress: false
                });
            }
        }

        if (
            this.props.dataset
                .reduce((acc: InfoDbListItemDataModel[], curr) => {
                    if (curr.createStatus && curr.createStatus.state === 'Используется') {
                        acc.push(curr);
                    }

                    return acc;
                }, [])
                .length
        ) {
            this.onDataSelect();
        }
    }

    public componentWillUnmount(): void {
        clearInterval(this.intervalId);
    }

    private getIdsList(isDelimiters: boolean) {
        return this.props.dataset.reduce((prev, curr) => {
            if ((isDelimiters ? defineInfoDbOnDelimitersCheckState(curr.state) : defineInfoDbCheckState(curr.state)) && curr.isDbOnDelimiters === isDelimiters) {
                prev.push(curr.databaseId);
            }

            return prev;
        }, [] as string[]);
    }

    private onChangeField() {
        this.setState({
            changeField: !this.state.changeField
        });
    }

    private onDeleteDb() {
        this.props.dispatchDeleteInfoDbThunk({
            databasesId: this.state.valueCheck,
            force: true,
        });
    }

    private onValueChange(value: string, item: InfoDbListItemDataModel) {
        if (this.state.valueCheck.indexOf(value) === -1) {
            this.setState(
                addCountAndSizeDb(value, item, this.state, this.props.dataset)
            );
        } else {
            this.setState(
                removeCountAndSizeDb(value, item, this.state, this.props.dataset)
            );
        }
    }

    private onValueChangeAll() {
        const availableDb = this.props.dataset.filter(i => i.state !== DatabaseState.NewItem &&
            i.state !== DatabaseState.DetachingToTomb && i.state !== DatabaseState.DelitingToTomb);
        const all = availableDb.map((i: InfoDbListItemDataModel) => i.databaseId);
        const allFilesDb = availableDb.filter((i: InfoDbListItemDataModel) => i.isFile);
        const allServerDb = availableDb.filter((i: InfoDbListItemDataModel) => !i.isFile && !i.isDbOnDelimiters);
        const addAllSizeInMb = availableDb.reduce((a, c) => a + c.sizeInMb, 0);
        this.state.allCheck
            ? this.setState({
                allSizeInMb: 0,
                valueCheck: [],
                serverBases: { count: 0, size: 0 },
                fileBases: { count: 0, size: 0 },
                allCheck: !this.state.allCheck
            })
            : this.setState({
                allSizeInMb: addAllSizeInMb,
                valueCheck: all,
                serverBases: { count: allServerDb.length, size: allServerDb.reduce((a, c) => a + c.sizeInMb, 0) },
                fileBases: { count: allFilesDb.length, size: allFilesDb.reduce((a, c) => a + c.sizeInMb, 0) },
                allCheck: !this.state.allCheck
            });
    }

    private onChangeTab(value: number) {
        this.setState({
            activeTab: value
        });
    }

    private deleteIdDbStates(value: string) {
        this.setState({
            ids: [...this.state.ids.splice(this.state.ids.indexOf(value), 1)]
        });
    }

    private onDataSelectOther(args?: SelectInfoDbGetQueryParams<AccountInfoDbListFilterDataForm>, _filterFormName?: string | null) {
        this.setState(prevState => ({ pageSizeOther: args?.size || prevState.pageSizeOther }), () => {
            this.props.dispatchReceiveInfoDbListOtherThunk({
                accountID: this.accountId,
                pageNumber: args?.pageNumber || 1,
                search: this.searchViewRef.current?.state.searchValue ?? '',
                type: this.searchViewRef.current?.state.type ?? 0,
                force: true,
                size: this.state.pageSizeOther || 10,
                affiliation: 1,
                field: getFieldInfoDb(args),
                sort: args?.sortingData?.sortKind ?? 0,
                accountUserId: this.accountUserId,
            });
        });
    }

    private onDataSelect(args?: SelectInfoDbGetQueryParams<AccountInfoDbListFilterDataForm>, _filterFormName?: string | null) {
        this.setState(prevState => ({ pageSizeOther: args?.size || prevState.pageSizeOther }), () => {
            this.props.dispatchReceiveInfoDbListThunk({
                accountID: this.accountId,
                pageNumber: args?.pageNumber || 1,
                search: this.searchViewRef.current?.state.searchValue ?? '',
                type: this.searchViewRef.current?.state.type ?? 0,
                force: true,
                size: args?.size || this.state.pageSize || 10,
                affiliation: 0,
                field: getFieldInfoDb(args),
                sort: args?.sortingData?.sortKind ?? 0,
                accountUserId: this.accountUserId,
            });
        });
        this.setState({
            tabTypeBases: this.searchViewRef.current?.state.type ?? 0,
            allCheck: false,
            valueCheck: [],
            ids: this.props.dataset.map(i => i.databaseId),
        }, () => {
            this.initialRefreshData();
        });
    }

    private clearTabIndex(id: string) {
        this.setState({
            activeTab: 0
        });

        this.props.dispatchStatusInfoDbThunk({
            ids: [id],
            force: true,
            showLoadingProgress: false
        });
    }

    private initialRefreshData() {
        clearInterval(this.intervalId);
        this.intervalId = setInterval(() => {
            if (this.state.ids.length) {
                this.props.dispatchStatusInfoDbThunk({
                    ids: this.getIdsList(false),
                    force: true,
                    showLoadingProgress: false
                });
            }
            if (this.state.idsOnDelimiters.length) {
                this.props.dispatchStatusInfoDbOnDelimitersThunk({
                    ids: this.getIdsList(true),
                    force: true,
                    showLoadingProgress: false
                });
            }
        }, TimerHelper.waitTimeRefreshDatabaseState);
    }

    private hideDatabaseCard() {
        this.setState({
            isInfoDbCardVisible: false,
            databaseName: ''
        });
    }

    private async showInfoDbCard(databaseName: string, item: InfoDbListItemDataModel, isOther: boolean) {
        this.setState({
            loading: true
        });

        try {
            if (item.state !== DatabaseState.NewItem &&
                item.state !== DatabaseState.Attaching &&
                item.state !== DatabaseState.ErrorCreate &&
                item.state !== DatabaseState.DetachingToTomb
            ) {
                this.setState({
                    isInfoDbCardVisible: true,
                    item,
                    databaseName,
                    loading: false,
                    isOther
                });
            }

            if (item.state === DatabaseState.ErrorCreate) {
                this.props.floatMessage.show(
                    MessageType.Warning,
                    `При создании базы произошла ошибка: ${ item.createAccountDatabaseComment ?? 'Вы можете удалить эту базу самостоятельно или обратитесь в техподдержку' }`
                );
            }

            this.setState({
                loading: false
            });
        } catch (e) {
            this.props.floatMessage.show(MessageType.Error, getErrorMessage(e));
        }
    }

    public render() {
        const countBase = {
            AllDatabasesCount: this.props.AllDatabasesCount,
            ArchievedDatabasesCount: this.props.ArchievedDatabasesCount,
            ServerDatabasesCount: this.props.ServerDatabasesCount,
            FileDatabasesCount: this.props.FileDatabasesCount,
            DelimeterDatabasesCount: this.props.DelimeterDatabasesCount,
            DeletedDatabases: this.props.DeletedDatabases
        };

        return (
            <div>
                { this.props.hasAvailabilityDbListReceived &&
                    <CollapseContentViews availability={ this.props.availability } />
                }
                <TableDbViews
                    hasInfoDbListStatusReceived={ this.props.hasInfoDbListStatusReceived }
                    tabTypeBases={ this.state.tabTypeBases }
                    currentUserGroups={ this.props.currentUserGroups }
                    availability={ this.props.availability }
                    dataset={ this.props.dataset }
                    datasetOther={ this.props.datasetOther }
                    onDataSelectOther={ this.onDataSelectOther }
                    onDataSelect={ this.onDataSelect }
                    searchViewRef={ this.searchViewRef }
                    pagination={ this.props.pagination }
                    paginationOther={ this.props.paginationOther }
                    countBase={ countBase }
                    onValueChange={ this.onValueChange }
                    onValueChangeAll={ this.onValueChangeAll }
                    valueCheck={ this.state.valueCheck }
                    allCheck={ this.state.allCheck }
                    allSizeInMb={ this.state.allSizeInMb }
                    serverBases={ this.state.serverBases }
                    fileBases={ this.state.fileBases }
                    showInfoDbCard={ this.showInfoDbCard }
                    data-testid="TableDbViews"
                    isLoading={ this.state.loading }
                />
                <InfoDbCard
                    onDataSelect={ this.onDataSelect }
                    onDataSelectOther={ this.onDataSelectOther }
                    clearTabIndex={ this.clearTabIndex }
                    changeField={ this.state.changeField }
                    onChangeField={ this.onChangeField }
                    item={ this.state.item }
                    activeTab={ this.state.activeTab }
                    isOpen={ this.state.isInfoDbCardVisible }
                    onCancelDialogButtonClick={ this.hideDatabaseCard }
                    isOther={ this.state.isOther }
                    onChangeTab={ this.onChangeTab }
                    accountGroup={
                        this.props.currentUserGroups.includes(AccountUserGroup.CloudAdmin) ||
                        this.props.currentUserGroups.includes(AccountUserGroup.AccountAdmin) ||
                        this.props.currentUserGroups.includes(AccountUserGroup.Hotline)
                    }
                    accessPermissions={ this.props.currentUserGroups.length === 1 && this.props.currentUserGroups.includes(AccountUserGroup.AccountSaleManager) }
                />
            </div>
        );
    }
}

const InformationBasesViewConnected = connect<StateProps, DispatchProps, NonNullable<unknown>, AppReduxStoreState>(
    state => {
        const infoDbListState = state.InfoDbListState;
        const permissionsAll = state.Global.getUserPermissionsReducer.permissions;
        const infoDbListOtherState = state.InfoDbListOtherState;
        const dataset = infoDbListState.infoDbList.records;
        const datasetOther = infoDbListOtherState.infoDbListOther.records;
        const AllDatabasesCount = infoDbListState.infoDbList.AllDatabasesCount!;
        const ArchievedDatabasesCount = infoDbListState.infoDbList.ArchievedDatabasesCount!;
        const ServerDatabasesCount = infoDbListState.infoDbList.ServerDatabasesCount!;
        const FileDatabasesCount = infoDbListState.infoDbList.FileDatabasesCount!;
        const DelimeterDatabasesCount = infoDbListState.infoDbList.DelimeterDatabasesCount!;
        const DeletedDatabases = infoDbListState.infoDbList.DeletedDatabases!;
        const availability = infoDbListState.AvailabilityInfoDb;
        const { hasInfoDbListReceived } = infoDbListState.hasSuccessFor;
        const { hasAvailabilityDbListReceived } = infoDbListState.hasSuccessFor;
        const { hasInfoDbListStatusReceived } = infoDbListState.hasSuccessFor;
        const totalPages = infoDbListState.infoDbList.pagination.pageCount;
        const { pageSize } = infoDbListState.infoDbList.pagination;
        const recordsCount = infoDbListState.infoDbList.pagination.totalCount;
        const { currentPage } = infoDbListState.infoDbList.pagination;
        const currentPageOther = infoDbListOtherState.infoDbListOther.pagination.currentPage;
        const totalPagesOther = infoDbListOtherState.infoDbListOther.pagination.pageCount;
        const pageSizeOther = infoDbListOtherState.infoDbListOther.pagination.pageSize;
        const recordsCountOther = infoDbListOtherState.infoDbListOther.pagination.totalCount;
        const hasSessionSettingsReceived = state.Global.getCurrentSessionSettingsReducer.hasSuccessFor.hasSettingsReceived;
        const currentUserGroups = hasSessionSettingsReceived ? state.Global.getCurrentSessionSettingsReducer.settings.currentContextInfo.currentUserGroups : [];
        const isInLoadingDataProcess = ProcessIdStateHelper.isInProgress(infoDbListState.reducerActions, InfoDbListProcessId.ReceiveInfoDbList);
        const { errorMessage } = infoDbListState;
        return {
            hasInfoDbListReceived,
            hasAvailabilityDbListReceived,
            hasInfoDbListStatusReceived,
            currentUserGroups,
            availability,
            dataset,
            datasetOther,
            AllDatabasesCount,
            ServerDatabasesCount,
            ArchievedDatabasesCount,
            FileDatabasesCount,
            DelimeterDatabasesCount,
            DeletedDatabases,
            isInLoadingDataProcess,
            pagination: {
                currentPage,
                totalPages,
                pageSize,
                recordsCount,
            },
            paginationOther: {
                currentPage: currentPageOther,
                totalPages: totalPagesOther,
                pageSize: pageSizeOther,
                recordsCount: recordsCountOther,
            },
            permissionsAll,
            errorMessage
        };
    },
    {
        dispatchReceiveInfoDbListThunk: ReceiveInfoDbListThunk.invoke,
        dispatchReceiveInfoDbListOtherThunk: ReceiveInfoDbListOtherThunk.invoke,
        dispatchDeleteInfoDbThunk: DeleteInfoDbThunk.invoke,
        dispatchEditInfoDbThunk: EditDbListThunk.invoke,
        dispatchStatusInfoDbThunk: StatusInfoDbListThunk.invoke,
        dispatchStatusInfoDbOnDelimitersThunk: StatusInfoDbOnDelimitersListThunk.invoke,
        dispatchAvailabilityInfoDbThunk: AvailabilityInfoDbListThunk.invoke
    }
)(InformationBasesViewClass);

export const InformationBasesView = withHeader({
    title: 'Информационные базы',
    page: withFloatMessages(InformationBasesViewConnected)
});