import { Services } from 'app/api/endpoints/managingServiceSubscriptions/request';
import React from 'react';

export type AccessTableProps = {
    setTotalSum: React.Dispatch<React.SetStateAction<number>>;
    setChangedUsers: React.Dispatch<React.SetStateAction<Services[]>>
}