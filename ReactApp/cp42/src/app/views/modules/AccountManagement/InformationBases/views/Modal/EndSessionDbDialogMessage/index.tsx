import { hasComponentChangesFor } from 'app/common/functions';
import { ContainedButton } from 'app/views/components/controls/Button';
import { DialogMessage } from 'app/views/components/controls/DialogMessage';
import cn from 'classnames';
import React from 'react';
import css from './styles.module.css';

type OwnProps = {
    isOpen: boolean;
    databaseName: string;
    onNoClick: () => void;
    onYesClick: () => void;
};

export class EndSessionDbDialogMessage extends React.Component<OwnProps> {
    public shouldComponentUpdate(nextProps: OwnProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    public render() {
        return (
            <DialogMessage
                isOpen={ this.props.isOpen }
                dialogWidth="xs"
                onCancelClick={ this.props.onNoClick }
            >
                <div className={ cn(css['warning-icon-container'], css.pulseWarning) }>
                    <span className={ cn(css['warning-icon-circle'], css.pulseWarningIns) } />
                    <span className={ cn(css['warning-icon-dot'], css.pulseWarningIns) } />
                </div>
                <h2 className={ cn(css['sweet-alert-title']) }>
                    Завершение сеансов
                </h2>
                <p className={ cn(css['sweet-alert-body']) }>
                    Все активные сеансы в базе “{ this.props.databaseName }” будут завершены.Завершить все сеансы?
                </p>
                <div style={ { textAlign: 'center', margin: '26px 0' } }>
                    <ContainedButton
                        kind="default"
                        className={ cn(css['sweet-alert-button']) }
                        onClick={ this.props.onNoClick }
                    >
                        Нет
                    </ContainedButton>
                    <ContainedButton
                        kind="error"
                        className={ cn(css['sweet-alert-button'], css['sweet-alert-button-yes']) }
                        onClick={ this.props.onYesClick }
                    >
                        Да
                    </ContainedButton>
                </div>
            </DialogMessage>
        );
    }
}