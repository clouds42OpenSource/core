import { Box, Fade, OutlinedInput, Popper } from '@mui/material';
import { AppRoutes } from 'app/AppRoutes';
import { AccountUserGroup } from 'app/common/enums';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { TextOut } from 'app/views/components/TextOut';
import { SearchByFilterButton, SuccessButton } from 'app/views/components/controls/Button';
import { ButtonGroup } from 'app/views/components/controls/ButtonGroup';
import { ERefreshId, ETourId } from 'app/views/Layout/ProjectTour/enums';
import { CreatePlatform42Base } from 'app/views/modules/AccountManagement/InformationBases/Permissions/CommonPage';
import { CountBasesDataModel } from 'app/views/modules/AccountManagement/InformationBases/types/CountBasesDataModel';
import { TablePagination } from 'app/views/modules/_common/components/CommonTableWithFilter/TableView';
import { GetAvailabilityInfoDb } from 'app/web/api/InfoDbProxy/responce-dto/GetAvailabilityInfoDb';
import cn from 'classnames';
import React, { ChangeEvent, RefObject } from 'react';
import { Link } from 'react-router-dom';
import css from '../styles.module.css';

type SearchInfoDbProps = {
    countBase?: CountBasesDataModel;
    pagination: TablePagination;
    paginationOther: TablePagination;
    availability: GetAvailabilityInfoDb;
    ref: RefObject<ControlPanelInfoDbViews> | undefined;
    currentUserGroups: Array<AccountUserGroup>;
    tabTypeBases: number;
    onDataSelect(): void;
    onDataSelectOther(): void;
};

type SearchPrintedInfoDbState = {
    searchValue: string;
    type: number;
    anchorEl: HTMLElement | null;
    open: boolean;
};

export class ControlPanelInfoDbViews extends React.Component<SearchInfoDbProps, SearchPrintedInfoDbState> {
    /**
     * Номер клавиши 13
     */
    private readonly enterKeyCode = 13;

    /**
     * Название клавиши 13
     */
    private readonly enterKeyName = 'enter';

    public constructor(props: SearchInfoDbProps) {
        super(props);
        this.state = {
            searchValue: '',
            type: 0,
            anchorEl: null,
            open: false
        };
        this.onValueChange = this.onValueChange.bind(this);
        this.onChangeType = this.onChangeType.bind(this);
        this.onInputKeyDown = this.onInputKeyDown.bind(this);
        this.flipOpen = this.flipOpen.bind(this);
    }

    private onInputKeyDown(ev: React.KeyboardEvent<HTMLInputElement>) {
        if (ev.key.toLowerCase() === this.enterKeyName) {
            this.props.onDataSelectOther();
            this.props.onDataSelect();
        }
    }

    private onChangeType(buttonIndex: number) {
        this.setState({ type: buttonIndex }, () => {
            this.props.onDataSelect();
            this.props.onDataSelectOther();
        });
    }

    private onValueChange(ev: ChangeEvent<HTMLInputElement>) {
        this.setState({ searchValue: ev.target.value });
    }

    private onClickAddBase(event: React.MouseEvent<HTMLElement>) {
        this.setState(prevState => ({ anchorEl: prevState.anchorEl !== null ? null : event.currentTarget }));
        this.flipOpen();
    }

    private flipOpen() {
        this.setState(prevState => ({ open: !prevState.open }));
    }

    public render() {
        return (
            <Box display="flex" gap="10px" flexWrap="wrap" justifyContent="space-between" alignItems="flex-end" marginTop="15px">
                <Box display="flex" gap="10px" alignItems="flex-end" flex="1 1 auto">
                    { this.props.availability.AllowAddingDatabaseInfo.IsAllowedCreateDb &&
                        this.props.availability.PermissionsForWorkingWithDatabase.HasPermissionForCreateAccountDatabase &&
                        this.props.availability.IsMainServiceAllowed &&
                        <Box onMouseLeave={ () => this.setState({ open: false }) } data-refreshid={ ERefreshId.controlPanelInfo }>
                            <div className={ cn(css.root) } data-tourid={ ETourId.createDatabase }>
                                <SuccessButton
                                    aria-describedby="id"
                                    kind="success"
                                    onMouseEnter={ (event: React.MouseEvent<HTMLElement>) => this.onClickAddBase(event) }
                                >
                                    <i className="fa fa-plus-circle mr-1" />
                                    Добавить базу
                                </SuccessButton>
                                <Popper
                                    placement="bottom"
                                    className={ cn(css.dropdown) }
                                    id="id"
                                    open={ this.state.open }
                                    anchorEl={ this.state.anchorEl }
                                    transition={ true }
                                    disablePortal={ true }
                                >
                                    { ({ TransitionProps }) => (
                                        <Fade { ...TransitionProps } timeout={ 0 }>
                                            <Box sx={ { width: '250px' } }>
                                                <Link
                                                    className={ cn(css.linkDb) }
                                                    to={ {
                                                        pathname: AppRoutes.accountManagement.createAccountDatabase.createDb,
                                                        state: {
                                                            isVip: this.props.availability.IsVipAccount,
                                                            isDbOnDelimiters: false
                                                        }
                                                    } }
                                                >
                                                    <TextOut inDiv={ true } fontSize={ 14 } fontWeight={ 400 }>
                                                        Создать новую
                                                    </TextOut>
                                                </Link>
                                                {
                                                    CreatePlatform42Base(this.props.availability.IsVipAccount, this.props.availability.AccountLocaleName) &&
                                                    <Link
                                                        className={ cn(css.linkDb) }
                                                        to={ {
                                                            pathname: AppRoutes.accountManagement.createAccountDatabase.createDb,
                                                            state: {
                                                                isVip: this.props.availability.IsVipAccount,
                                                                isDbOnDelimiters: true
                                                            }
                                                        } }
                                                    >
                                                        <TextOut inDiv={ true } fontSize={ 14 } fontWeight={ 400 }>
                                                            Создать новую (Платформа42)
                                                        </TextOut>
                                                    </Link>
                                                }
                                                <Link
                                                    className={ cn(css.linkDb) }
                                                    to={ AppRoutes.accountManagement.createAccountDatabase.loadingDatabase }
                                                >
                                                    <TextOut inDiv={ true } fontSize={ 14 } fontWeight={ 400 }>
                                                        Загрузить свою базу 1С
                                                    </TextOut>
                                                </Link>
                                            </Box>
                                        </Fade>
                                    ) }
                                </Popper>
                            </div>
                        </Box>
                    }
                    <FormAndLabel label="Поиск" fullWidth={ true }>
                        <OutlinedInput
                            fullWidth={ true }
                            placeholder="Поиск базы по названию, шаблону, номеру"
                            value={ this.state.searchValue }
                            onChange={ this.onValueChange }
                            onKeyDown={ this.onInputKeyDown }
                        />
                    </FormAndLabel>
                </Box>
                <Box display="flex" gap="10px" alignItems="flex-end" flexWrap="wrap">
                    <ButtonGroup
                        items={
                            [
                                {
                                    index: 0,
                                    text: `Все базы (${ this.props.countBase?.AllDatabasesCount || 0 })`
                                },
                                {
                                    index: 1,
                                    text: `Серверные (${ this.props.countBase?.ServerDatabasesCount || 0 })`
                                },
                                {
                                    index: 2,
                                    text: `В архиве (${ this.props.countBase?.ArchievedDatabasesCount || 0 })`
                                },
                                {
                                    index: 3,
                                    text: `Файловые (${ this.props.countBase?.FileDatabasesCount || 0 })`
                                },
                                {
                                    index: 4,
                                    text: `На разделителях (${ this.props.countBase?.DelimeterDatabasesCount || 0 })`
                                },
                                {
                                    index: 5,
                                    text: `Корзина (${ this.props.countBase?.DeletedDatabases || 0 })`
                                },
                            ]
                        }
                        selectedButtonIndex={ this.state.type }
                        label="Быстрые отборы"
                        onClick={ this.onChangeType }
                    />
                    <SearchByFilterButton
                        className={ css['search-button'] }
                        kind="success"
                        onClick={ () => {
                            this.props.onDataSelectOther();
                            this.props.onDataSelect();
                        } }
                    />
                </Box>
            </Box>
        );
    }
}