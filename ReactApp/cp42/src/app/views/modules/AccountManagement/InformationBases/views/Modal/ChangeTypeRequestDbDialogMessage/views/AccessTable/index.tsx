import { Box } from '@mui/material';
import { localStorageHelper } from 'app/common/helpers/LocalStorageHelper';
import { useAppSelector } from 'app/hooks';
import { CheckBoxForm } from 'app/views/components/controls/forms';
import { TextOut } from 'app/views/components/TextOut';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import css from 'app/views/modules/_common/reusable-modules/InfoDbCard/styles.module.css';
import { AccessTableProps } from 'app/views/modules/AccountManagement/InformationBases/views/Modal/ChangeTypeRequestDbDialogMessage/views/AccessTable/types';
import { AccountUserDataModelsList } from 'app/web/api/InfoDbCardProxy/request-dto/InputCalculateAccesses';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { DatabaseAccessesDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/AccessInfoDbDataModel';
import cn from 'classnames';
import { RequestKind } from 'core/requestSender/enums';
import React, { useEffect, useState } from 'react';

const { CalculateAccesses } = InterlayerApiProxy.getInfoDbCardApi();

export const AccessTable = ({ setTotalSum, setChangedUsers }: AccessTableProps) => {
    const {
        InfoDbCardState: {
            commonDetails,
            databaseAccesses,
            rateData
        },
        InfoDbListState: {
            AvailabilityInfoDb
        }
    } = useAppSelector(state => state);

    const [selectedUsers, setSelectedUsers] = useState<AccountUserDataModelsList[]>([]);
    const [cost, setCost] = useState(0);

    const selectAll = () => {
        if (!selectedUsers.length) {
            setSelectedUsers((databaseAccesses as DatabaseAccessesDataModel[]).map(item => {
                return {
                    UserId: item.userId,
                    HasAccess: item.hasAccess
                };
            }));
            setChangedUsers(() => (databaseAccesses as DatabaseAccessesDataModel[]).map(item => {
                return {
                    billingServiceTypeId: commonDetails.myDatabasesServiceTypeId,
                    sponsorship: {
                        i: false,
                        me: false,
                        label: ''
                    },
                    status: true,
                    subject: item.userId
                };
            }));
            setChangedUsers(prevState => prevState.concat((databaseAccesses as DatabaseAccessesDataModel[]).map(item => {
                return {
                    billingServiceTypeId: AvailabilityInfoDb.AccessToServerDatabaseServiceTypeId,
                    sponsorship: {
                        i: false,
                        me: false,
                        label: ''
                    },
                    status: true,
                    subject: item.userId
                };
            })));
        } else {
            setSelectedUsers([]);
            setChangedUsers([]);
        }
    };

    const selectById = (userId: string, hasAccess: boolean) => {
        if (!selectedUsers.find(item => item.UserId === userId)) {
            setSelectedUsers([...selectedUsers, { UserId: userId, HasAccess: hasAccess }]);
            setChangedUsers(prevState => [
                ...prevState,
                {
                    billingServiceTypeId: commonDetails.myDatabasesServiceTypeId,
                    sponsorship: {
                        i: false,
                        me: false,
                        label: ''
                    },
                    status: true,
                    subject: userId
                },
                {
                    billingServiceTypeId: AvailabilityInfoDb.AccessToServerDatabaseServiceTypeId,
                    sponsorship: {
                        i: false,
                        me: false,
                        label: ''
                    },
                    status: true,
                    subject: userId
                }
            ]);
        } else {
            setSelectedUsers(selectedUsers.filter(item => item.UserId !== userId));
            setChangedUsers(prevState => prevState.filter(item => item.subject !== userId));
        }
    };

    useEffect(() => {
        if (commonDetails.isFile) {
            CalculateAccesses(
                RequestKind.SEND_BY_USER_ASYNCHRONOUSLY,
                {
                    serviceTypesIdsList: [rateData.serviceTypeId],
                    accountUserDataModelsList: [{
                        UserId: localStorageHelper.getUserId() ?? '',
                        HasAccess: true
                    }]
                }
            ).then(response => {
                const ksCost = response.reduce((previousValue, currentValue) => previousValue + currentValue.accessCost, 0);
                setCost(ksCost);
                setTotalSum(ksCost);
            });
        }
    }, []);

    useEffect(() => {
        CalculateAccesses(
            RequestKind.SEND_BY_USER_ASYNCHRONOUSLY,
            {
                serviceTypesIdsList: commonDetails.isFile
                    ? [commonDetails.myDatabasesServiceTypeId, AvailabilityInfoDb.AccessToServerDatabaseServiceTypeId]
                    : [commonDetails.myDatabasesServiceTypeId],
                accountUserDataModelsList: selectedUsers
            }
        ).then(response => setTotalSum(response.reduce((previousValue, currentValue) => previousValue + currentValue.accessCost, 0) + cost));
    }, [selectedUsers, cost]);

    return (
        <div>
            <CommonTableWithFilter
                uniqueContextProviderStateId="AccessInfoDbListContextProviderStateId"
                tableProps={ {
                    dataset: databaseAccesses as DatabaseAccessesDataModel[],
                    keyFieldName: 'userId',
                    fieldsView: {
                        userId: {
                            caption: <CheckBoxForm
                                className={ cn(css.check) }
                                formName="isDefault"
                                label=""
                                isChecked={ (databaseAccesses as DatabaseAccessesDataModel[]).length === selectedUsers.length }
                                onValueChange={ selectAll }
                            />,
                            fieldContentNoWrap: false,
                            fieldWidth: '3%',
                            format: (value: string, item) => (
                                <CheckBoxForm
                                    className={ cn(css.check) }
                                    formName={ value }
                                    label=""
                                    isChecked={ !!selectedUsers.find(isCheck => isCheck.UserId === value) }
                                    onValueChange={ () => selectById(value, item.hasAccess) }
                                />
                            )
                        },
                        userLogin: {
                            caption: 'Логин',
                            fieldContentNoWrap: false,
                            fieldWidth: '30%',
                            isSortable: false,
                            format: (value: string) => (
                                <Box>
                                    <TextOut inDiv={ true }>{ value }</TextOut>
                                </Box>
                            )
                        },
                        userFullName: {
                            caption: 'ФИО',
                            fieldContentNoWrap: false,
                            isSortable: false,
                            fieldWidth: '40%',
                            format: (value: string, item: DatabaseAccessesDataModel) => (
                                <TextOut fontSize={ 13 }>
                                    { value || item.userLastName }
                                </TextOut>
                            )
                        },
                        userEmail: {
                            caption: 'Эл. адрес',
                            fieldContentNoWrap: false,
                            isSortable: false,
                            fieldWidth: '30%',
                            format: (value: string, item: DatabaseAccessesDataModel) => (
                                <>
                                    <TextOut fontSize={ 13 }>
                                        { value }
                                    </TextOut>
                                    { Object.prototype.hasOwnProperty.call(item, 'accessCost') || item.isExternalAccess
                                        ? (
                                            <TextOut inDiv={ true } style={ { color: 'red', fontSize: '10px' } }>
                                                { item.accountInfo }
                                            </TextOut>
                                        )
                                        : null
                                    }
                                </>
                            )
                        }
                    }
                } }
            />
        </div>
    );
};