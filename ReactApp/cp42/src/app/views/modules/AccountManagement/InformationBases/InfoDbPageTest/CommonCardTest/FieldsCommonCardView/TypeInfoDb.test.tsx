import '@testing-library/jest-dom/extend-expect';
import { render } from '@testing-library/react';
import { partialFillOf } from 'app/common/functions';
import { createAppReduxStore } from 'app/redux/functions/createAppReduxStore';
import { availabilityTest } from 'app/views/modules/AccountManagement/InformationBases/InfoDbPageTest/mock/availability';
import { InfoDbListTest } from 'app/views/modules/AccountManagement/InformationBases/InfoDbPageTest/mock/infoDbListTest';
import { itemInfoDb } from 'app/views/modules/AccountManagement/InformationBases/InfoDbPageTest/mock/itemInfoDb';
import { InfoDbCardEditDatabaseDataForm } from 'app/views/modules/AccountManagement/InformationBases/types/InfiDbCardEditDatabaseDataForm';
import { FieldsCommonCardView } from 'app/views/modules/_common/reusable-modules/InfoDbCard/infoDbCardContent/tabs/CommonTabView/FieldsCommonCardView';
import { Provider } from 'react-redux';
import { applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';

describe('TypeInfoDbTest', () => {
    const onValueChangeWebServices = jest.fn();
    const onChangeField = jest.fn();
    const onValueChange = jest.fn();
    const onChangeV82Name = jest.fn();
    const reduxForm = partialFillOf<InfoDbCardEditDatabaseDataForm>();
    const store = createAppReduxStore(composeWithDevTools(applyMiddleware(thunk)));

    it('render type InfoDb', async () => {
        const { queryByText } = render(
            <Provider store={ { ...store } }>
                <FieldsCommonCardView
                    changeWebServices={ true }
                    isOther={ true }
                    availability={ availabilityTest }
                    commonDetails={ itemInfoDb()! }
                    onChangeField={ onChangeField }
                    item={ InfoDbListTest[0] }
                    onChangeV82Name={ onChangeV82Name }
                    permissions={ ['AccountDatabases_EditFull'] }
                    reduxForm={ reduxForm }
                    onValueChange={ onValueChange }
                    onValueChangeWebServices={ onValueChangeWebServices }
                    errorV82Name=""
                    currentUserGroups={ [] }
                />
            </Provider>
        );

        const type = itemInfoDb()!.isFile ? 'Файловая' : 'Серверная';
        expect(queryByText(type)).toBeInTheDocument();
    });
});