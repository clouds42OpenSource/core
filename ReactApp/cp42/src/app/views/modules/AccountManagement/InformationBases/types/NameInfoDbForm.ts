/**
 * Модель Redux формы для редактирования имени базы данных
 */
export type NameInfoDbForm = {

  caption: string;
};