import { Box } from '@mui/material';
import { Dialog } from 'app/views/components/controls/Dialog';
import { TextOut } from 'app/views/components/TextOut';
import cn from 'classnames';
import React from 'react';
import css from '../EndSessionDbDialogMessage/styles.module.css';

type TProps = {
    isOpen: boolean;
    databaseName: string;
    onNoClick: () => void;
    onYesClick: () => void;
};

export const UploadDtDialogMessage = ({ isOpen, databaseName, onNoClick, onYesClick }: TProps) => {
    return (
        <Dialog
            isOpen={ isOpen }
            dialogWidth="sm"
            buttons={ [
                {
                    kind: 'default',
                    onClick: onNoClick,
                    content: 'Нет'
                },
                {
                    kind: 'error',
                    onClick: onYesClick,
                    content: 'Да'
                }
            ] }
            title={ `Создание выгрузки DT для базы ${ databaseName }` }
            isTitleSmall={ true }
            maxHeight="450px"
        >
            <Box display="flex" flexDirection="column" gap="5px">
                <div className={ cn(css['warning-icon-container'], css.pulseWarning) }>
                    <span className={ cn(css['warning-icon-circle'], css.pulseWarningIns) } />
                    <span className={ cn(css['warning-icon-dot'], css.pulseWarningIns) } />
                </div>
                <TextOut>
                    Пожалуйста, обратите внимание, что во время формирования файла выгрузки данных текущая база будет временно недоступна до завершения процедуры.
                </TextOut>
                <TextOut>
                    Также хотели бы напомнить, что возможность создания такой выгрузки предоставляется лишь один раз в сутки для каждой базы данных.
                </TextOut>
                <TextOut>
                    Запустить процесс прямо сейчас?
                </TextOut>
            </Box>
        </Dialog>
    );
};