import { AccountUserGroup, DatabaseState } from 'app/common/enums';
import { InfoDbCardPermissionsEnum } from 'app/web/common/enums/InfoDbCardPermissionsEnum';
import { InfoDbListItemDataModel } from 'app/web/InterlayerApiProxy/InfoDbApiProxy/receiveDatabaseList/data-models';
import { InfoDbCardAccountDatabaseInfoDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/InfoDbCardAccountDatabaseInfoDataModel';
import { RightsInfoDbCardDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getRightsInfoDbCard/data-models/InfoDbCardAccountDatabaseInfoDataModel';

/**
 * Пермишен для таба Настройки доступа
 */
export function accessSettingsTab(permissions: RightsInfoDbCardDataModel, common: InfoDbCardAccountDatabaseInfoDataModel, accessPermissions: boolean, status: boolean) {
    return (permissions.includes(InfoDbCardPermissionsEnum.AcDbAccesses_Edit) || accessPermissions) && status && common.databaseState !== 16;
}

/**
 * Пермишен для таба Сервисы
 */
export function servicesTab(permissions: RightsInfoDbCardDataModel, common: InfoDbCardAccountDatabaseInfoDataModel, isSale: boolean) {
    return (permissions.includes(InfoDbCardPermissionsEnum.AccountDatabase_Change) || isSale) &&
        common.isDbOnDelimiters &&
        common.databaseState === DatabaseState.Ready;
}

/**
 * Пермишен для таба Архивные копии
 */
export function archivalCopiesTab(permissions: RightsInfoDbCardDataModel) {
    return permissions.includes(InfoDbCardPermissionsEnum.AccountDatabase_Change);
}
/**
 * Пермишен для таба Авторизация
 */
export function authorizationTab(permissions: RightsInfoDbCardDataModel, common: InfoDbCardAccountDatabaseInfoDataModel | InfoDbListItemDataModel, status: boolean) {
    return permissions.includes(InfoDbCardPermissionsEnum.AccountDatabases_EditSupport) &&
        !common.isDbOnDelimiters && status;
}

/**
 * Пермишен для таба Настройки доступов
 */
export function accessIsOtherTab(currentUserGroups: AccountUserGroup[], isOther: boolean) {
    return (currentUserGroups.includes(AccountUserGroup.AccountAdmin) ||
            currentUserGroups.includes(AccountUserGroup.CloudAdmin) ||
            currentUserGroups.includes(AccountUserGroup.Hotline)) &&
            isOther;
}