import { Box } from '@mui/material';
import { getInfoDbState } from 'app/common/functions/getInfoDbState';
import { COLORS } from 'app/utils';
import { TextOut } from 'app/views/components/TextOut';
import { CountDownTimerViews } from 'app/views/modules/AccountManagement/InformationBases/views/DbStateLine/CountDownTimer';
import { InfoDbListItemDataModel } from 'app/web/InterlayerApiProxy/InfoDbApiProxy/receiveDatabaseList/data-models';
import React from 'react';

type OwnProps = {
    item: InfoDbListItemDataModel;
    hasInfoDbListStatusReceived: boolean
};

export const DbStateLineViews = ({ item, hasInfoDbListStatusReceived }: OwnProps) => {
    const currentDate = new Date();
    const lastActivityDate = new Date(item.lastActivityDate);

    lastActivityDate.setMinutes(lastActivityDate.getHours() + 1);

    return (
        <div>
            {
                item.createStatus
                    ? item.createStatus.errorCode && new Date(lastActivityDate.toISOString()) > currentDate
                        ? (
                            <Box style={ { display: 'flex', lineHeight: '16px', color: COLORS.main } }>
                                <img src="img/database-icons/animated-clock.gif" alt="timer" style={ { width: '14px', height: '14px' } } />&nbsp;
                                <TextOut fontSize={ 10 } inheritColor={ true }>База в процессе создания&nbsp;</TextOut>
                            </Box>
                        )
                        : !item.createStatus.error
                            ? (
                                <Box style={ { display: 'flex', lineHeight: '16px', color: COLORS.main } }>
                                    <img src="img/database-icons/animated-clock.gif" alt="timer" style={ { width: '14px', height: '14px' } } />&nbsp;
                                    <TextOut fontSize={ 10 } inheritColor={ true }>{ item.createStatus?.state }&nbsp;
                                        {
                                            /**
                                             * -1 возможен только при миграции базыл (ответ с МС)
                                             */
                                            item.createStatus.timeout !== -1 && (
                                                <CountDownTimerViews
                                                    hours={ parseInt(new Date(item.createStatus.timeout * 1000).toISOString().slice(11, 13), 10) }
                                                    minutes={ parseInt(new Date(item.createStatus.timeout * 1000).toISOString().slice(14, 16), 10) }
                                                    seconds={ parseInt(new Date(item.createStatus.timeout * 1000).toISOString().slice(17, 19), 10) }
                                                    hasInfoDbListStatusReceived={ hasInfoDbListStatusReceived }
                                                />
                                            )
                                        }
                                    </TextOut>
                                </Box>
                            )
                            : (
                                <TextOut fontSize={ 10 } style={ { color: COLORS.warning } }>
                                    { item.createStatus?.error }
                                </TextOut>
                            )
                    : (
                        <TextOut fontSize={ 10 } style={ { color: COLORS.warning } }>
                            { getInfoDbState(item.state, item.publishState).length ? `(${ getInfoDbState(item.state, item.publishState) })` : null }
                        </TextOut>
                    )
            }
        </div>
    );
};