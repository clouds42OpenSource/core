/**
 * Модель данных связанная с нумерацией
 */
export type CountBasesDataModel = {
  /**
   * Количество всего баз
   */
  AllDatabasesCount: number;
  /**
   * Количество серверных баз
   */
  ServerDatabasesCount: number;
  /**
   * Количество баз в архиве
   */
  ArchievedDatabasesCount: number;
  /**
   * Количество баз на разделителях
   */
  DelimeterDatabasesCount?: number;
  /**
   * Количество файловых баз
   */
  FileDatabasesCount?: number;
  /**
   * Количество баз в корзине
   */
  DeletedDatabases?: number;
};