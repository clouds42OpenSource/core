import '@testing-library/jest-dom/extend-expect';
import { render } from '@testing-library/react';
import { createAppReduxStore } from 'app/redux/functions';
import { ChangeTypeButton } from 'app/views/modules/_common/reusable-modules/InfoDbCard/buttons/ChangeTypeButton';
import { SnackbarProvider } from 'notistack';
import { Provider } from 'react-redux';
import { applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';

describe('publish Button', () => {
    const onChangeField = jest.fn();
    const onUpdateAccessCard = jest.fn();
    const store = createAppReduxStore(composeWithDevTools(applyMiddleware(thunk)));

    it('change type an file renderButton', async () => {
        const { queryByText } = render(
            <Provider store={ { ...store } }>
                <SnackbarProvider>
                    <ChangeTypeButton
                        isFile={ false }
                        databaseId=""
                        onChangeField={ onChangeField }
                        databaseName=""
                        updateAccessCard={ onUpdateAccessCard }
                    />
                </SnackbarProvider>
            </Provider>
        );
        expect(queryByText(/Перевести в файловую/)).toBeInTheDocument();
    });
    it('change type an server renderButton', async () => {
        const { queryByText } = render(
            <Provider store={ { ...store } }>
                <SnackbarProvider>
                    <ChangeTypeButton
                        isFile={ true }
                        databaseId=""
                        onChangeField={ onChangeField }
                        databaseName=""
                        updateAccessCard={ onUpdateAccessCard }
                    />
                </SnackbarProvider>
            </Provider>
        );
        expect(queryByText(/Перевести в серверную/)).toBeInTheDocument();
    });
});