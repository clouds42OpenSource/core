declare const styles: {
  readonly 'warning-icon-container': string;
  readonly 'warning-icon-circle': string;
  readonly 'warning-icon-dot': string;
  readonly 'pulseWarningIns': string;
  readonly 'pulseWarning': string;
  readonly 'sa-warning': string;
  readonly 'sa-icon': string;
  readonly 'sa-body': string;
  readonly 'sa-dot': string;
  readonly 'sweet-alert-title': string;
  readonly 'sweet-alert-body': string;
  readonly 'sweet-alert-button': string;
  readonly 'sweet-alert-button-yes': string;
};
export = styles;