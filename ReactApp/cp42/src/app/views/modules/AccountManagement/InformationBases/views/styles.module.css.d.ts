declare const styles: {
    readonly 'accordion': string;
    readonly 'label1C': string;
    readonly 'labelWindows': string;
    readonly 'label1CGrid': string;
    readonly 'search-button': string;
    readonly 'myinfoDb': string;
    readonly 'check': string;
    readonly 'root': string;
    readonly 'dropdown': string;
    readonly 'linkDb': string;
    readonly 'button-delete-outline': string;
    readonly 'container-delete-btn': string;
    readonly 'isNotActive1C': string;
    readonly 'runDb': string;
    readonly 'header': string;
    readonly 'warning-icon-container': string;
    readonly 'warning-icon-circle': string;
    readonly 'pulseWarningIns': string;
    readonly 'pulseWarning': string;
    readonly 'warning-icon-dot': string;
    readonly 'notHidden': string;
    readonly 'button': string;
    readonly 'info-icon': string;
    readonly 'icon': string;
    readonly 'timer': string;
};

export = styles;