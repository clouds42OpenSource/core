import '@testing-library/jest-dom/extend-expect';
import { render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { partialFillOf } from 'app/common/functions';
import { InfoDbCardConsts } from 'app/modules/infoDbCard/constants';
import { receiveAccessReducer } from 'app/modules/infoDbCard/store/reducers/receiveAccessInfoDbCardReducer';
import { receiveBuckapsReducer } from 'app/modules/infoDbCard/store/reducers/receiveBuckupsReducer';
import { receiveAccessCostReducer } from 'app/modules/infoDbCard/store/reducers/receiveCalculateAccessInfoDbCardReducer';
import { receiveInfoDbCardReducer } from 'app/modules/infoDbCard/store/reducers/receiveDatabaseCardReducer';
import { receiveCommonInfoDbCardReducer } from 'app/modules/infoDbCard/store/reducers/receiveDatabaseCommonCardReducer';
import { receiveHasSupportReducer } from 'app/modules/infoDbCard/store/reducers/receiveHasSupportReducer';
import { receiveRightsInfoDbCardReducer } from 'app/modules/infoDbCard/store/reducers/receiveRightsInfoDbCardReducer';
import { receiveServicesReducer } from 'app/modules/infoDbCard/store/reducers/receiveServicesReducer';
import { receiveStatusServiceReducer } from 'app/modules/infoDbCard/store/reducers/receiveStatusServiceInfoDbCardReducer';
import { AppReduxStoreReducers } from 'app/redux';
import { MockStoreCard } from 'app/views/modules/AccountManagement/InformationBases/InfoDbPageTest/functions/MockStore';
import { accessItem } from 'app/views/modules/AccountManagement/InformationBases/InfoDbPageTest/mock/accessItem';
import { buckupsItem } from 'app/views/modules/AccountManagement/InformationBases/InfoDbPageTest/mock/buckupItem';
import { hasSupportItem } from 'app/views/modules/AccountManagement/InformationBases/InfoDbPageTest/mock/hasSupportItem';
import { serviceTableList1 } from 'app/views/modules/AccountManagement/InformationBases/InfoDbPageTest/mock/serviceTableList';
import { HasSupportView } from 'app/views/modules/_common/reusable-modules/InfoDbCard/infoDbCardContent/tabs/HasSupportTabView';
import { HasSupportInfoDbDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/HasSupportInfoDbDataModel';
import { InfoDbCardAccountDatabaseInfoDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/InfoDbCardAccountDatabaseInfoDataModel';
import { createReducer, createStore, initReducerState } from 'core/redux/functions';
import { SnackbarProvider } from 'notistack';
import { Provider } from 'react-redux';
import { applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';

describe('Support filter InfoDbTest', () => {
    const store = createStore({
        ...AppReduxStoreReducers,
        InfoDbCardState: createReducer(initReducerState<any>(InfoDbCardConsts.reducerName, {
            commonDetails: partialFillOf<InfoDbCardAccountDatabaseInfoDataModel>(),
            hasSupportInfoDb: partialFillOf<HasSupportInfoDbDataModel>(),
            rightsInfoDb: [],
            databaseAccesses: accessItem,
            databaseAccessesCost: [],
            servicesInfoDb: serviceTableList1,
            backupsInfoDb: {
                databaseBackups: buckupsItem().databaseBackups,
                dbBackupsCount: buckupsItem().dbBackupsCount
            },
            hasSuccessFor: {
                hasDatabaseCardReceived: false,
                hasCommonDatabaseCardReceived: false,
                hasHasSupportReceived: false,
                hasServicesReceived: false,
                hasAccessReceived: false,
                hasAccessCostReceived: false,
                hasBackupsReceived: false
            }
        }), [receiveInfoDbCardReducer,
            receiveCommonInfoDbCardReducer,
            receiveRightsInfoDbCardReducer,
            receiveHasSupportReducer,
            receiveAccessReducer,
            receiveAccessCostReducer,
            receiveServicesReducer,
            receiveStatusServiceReducer,
            receiveBuckapsReducer])
    }, composeWithDevTools(applyMiddleware(thunk)));

    it('Support form render', async () => {
        const { getByRole, container } = render(
            <Provider store={ store }>
                <SnackbarProvider>
                    <HasSupportView hasSupportInfoDb={ hasSupportItem } />
                </SnackbarProvider>
            </Provider>
        );
        const user1C = container.querySelector('[name = username]')!;
        const btn = getByRole('button', { name: 'Авторизоваться в базе 1С' });
        const pass = container.querySelector('[name = password]')!;
        expect(user1C).toBeInTheDocument();
        expect(btn).toBeInTheDocument();
        expect(pass).toBeInTheDocument();
    });

    it('Support form change', async () => {
        const { getByRole, container } = render(
            <Provider store={ store }>
                <SnackbarProvider>
                    <HasSupportView hasSupportInfoDb={ hasSupportItem } />
                </SnackbarProvider>
            </Provider>
        );
        const user1C = container.querySelector('[name = username]')!;
        const pass = container.querySelector('[name = password]')!;
        await userEvent.type(user1C, 'Ю');
        await userEvent.type(user1C, 'з');
        await userEvent.type(user1C, 'е');
        await userEvent.type(user1C, 'р');
        expect(user1C).toHaveValue('Юзер');
        await userEvent.type(pass, 'П');
        await userEvent.type(pass, 'а');
        await userEvent.type(pass, 'с');
        expect(pass).toHaveValue('Пас');
    });

    it('Support form with not auth', async () => {
        const { getByText } = render(
            <Provider store={ { ...store, ...MockStoreCard({ hasSupportInfoDb: hasSupportItem }) } }>
                <SnackbarProvider>
                    <HasSupportView hasSupportInfoDb={ hasSupportItem } />
                </SnackbarProvider>
            </Provider>
        );
        const stateDescription = getByText(hasSupportItem.supportStateDescription);
        expect(stateDescription).toBeInTheDocument();
    });
});