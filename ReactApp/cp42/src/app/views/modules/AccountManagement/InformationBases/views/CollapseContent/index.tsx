import { AppRoutes } from 'app/AppRoutes';
import React from 'react';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { Accordion, AccordionDetails, AccordionSummary, Box, Link, Divider } from '@mui/material';

import { TextOut } from 'app/views/components/TextOut';
import { BlockMessageView } from 'app/views/modules/AccountManagement/InformationBases/views/CollapseContent/BlockMessageView';
import { GetAvailabilityInfoDb } from 'app/web/api/InfoDbProxy/responce-dto/GetAvailabilityInfoDb';

import styles from './style.module.css';

type TElement = {
    device: string;
    platform: RegExp;
    icon: string;
    link: string;
};

type TProps = {
    availability: GetAvailabilityInfoDb;
    localeTitle?: string;
    isRent?: boolean;
};

const elements: TElement[] = [
    { device: 'Android', platform: /Android/, icon: 'android', link: 'rent-1c-android.html' },
    { device: 'iOS', platform: /iPhone/, icon: 'apple', link: 'podklyuchenie-k-1s-dlya-polzovateley-iphone-i-ipad.html' },
    { device: 'iPad', platform: /iPad/, icon: 'tablet', link: 'podklyuchenie-k-1s-dlya-polzovateley-iphone-i-ipad.html' },
    { device: 'macOS', platform: /Tablet OS/, icon: 'apple', link: 'rent-1c-mac-os.html' },
    { device: 'Linux', platform: /Linux/, icon: 'linux', link: 'rent-1c-linux.html' },
];

export const CollapseContentViews = ({ availability, isRent, localeTitle }: TProps) => {
    return (
        <Box display="flex" flexDirection="column" gap="10px">
            { !isRent && (
                <TextOut>
                    Вы можете загрузить свою конфигурацию с данными либо создать новую базу на основе одной из типовых конфигураций 1С.
                </TextOut>
            ) }
            <Accordion defaultExpanded={ true } className={ styles.accordion }>
                <AccordionSummary expandIcon={ <ExpandMoreIcon /> }>
                    <TextOut fontWeight={ 600 }>
                        { localeTitle ?? 'Скачайте ярлык подключения к  1С' }
                    </TextOut>
                </AccordionSummary>
                <AccordionDetails>
                    <Box display="flex" flexDirection="column" gap="5px">
                        <Box display="flex" gap="16px">
                            <Link data-testid="winDownload" href={ AppRoutes.link42Downloader } rel="noreferrer" target="_blank" className={ styles.link }>
                                <img height={ 35 } width={ 35 } src="/img/link42.png" alt="link42" />
                                <TextOut className={ styles.linkText } inheritColor={ true } fontWeight={ 500 }>Линк42</TextOut>
                            </Link>
                            <Link href={ AppRoutes.settings.connectionSettings } rel="noreferrer" target="_blank" className={ styles.link }>
                                <img height={ 35 } width={ 35 } src="/img/thinClient.png" alt="connection settings" />
                                <TextOut className={ styles.linkText } inheritColor={ true } fontWeight={ 500 }>Тонкий клиент</TextOut>
                            </Link>
                        </Box>
                        <Divider><TextOut>Инструкции</TextOut></Divider>
                        <Box display="flex" justifyContent="space-between">
                            { elements.map(element => (
                                <Link key={ element.link } className={ styles.link } rel="noreferrer" target="_blank" href={ `https://42clouds.com/ru-ru/manuals/${ element.link }` }>
                                    <i className={ `fa fa-${ element.icon } ${ styles.icon }` } />
                                    <TextOut inheritColor={ true } fontWeight={ 500 }>{ element.device }</TextOut>
                                </Link>
                            )) }
                        </Box>
                    </Box>
                </AccordionDetails>
            </Accordion>
            { !isRent && <BlockMessageView { ...availability } /> }
        </Box>
    );
};