import { TablePagination } from 'app/views/modules/_common/components/CommonTableWithFilter/TableView';

export const tablePaginationTest: TablePagination = {
    totalPages: 1,
    currentPage: 1,
    pageSize: 10,
    recordsCount: 1,
};