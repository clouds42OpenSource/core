import { Box, IconButton } from '@mui/material';
import { ServiceLockReasonType } from 'app/api/endpoints/managingServiceSubscriptions/enum';
import { AccountUserGroup, DatabaseState } from 'app/common/enums';
import { DateUtility, NumberUtility } from 'app/utils';
import { TextButton } from 'app/views/components/controls/Button';
import { CheckBoxForm } from 'app/views/components/controls/forms';
import CustomizedSwitches from 'app/views/components/controls/Switch/views/SwitchViews';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { HomeSvgSelector } from 'app/views/components/svgGenerator/HomeSvgSelector';
import { TextOut } from 'app/views/components/TextOut';
import { ERefreshId, ETourId } from 'app/views/Layout/ProjectTour/enums';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { TablePagination } from 'app/views/modules/_common/components/CommonTableWithFilter/TableView';
import { AutoUpdateDbIsVisible, CheckBoxDeleteDbIsVisible, HasSupportDbIsVisible, WebPathDbIsVisible } from 'app/views/modules/AccountManagement/InformationBases/Permissions/CommonPage';
import { CountBasesDataModel } from 'app/views/modules/AccountManagement/InformationBases/types/CountBasesDataModel';
import { ControlPanelInfoDbViews } from 'app/views/modules/AccountManagement/InformationBases/views/ControlPanelInfoDb';
import { DbStateLineViews } from 'app/views/modules/AccountManagement/InformationBases/views/DbStateLine/DbStateLine';
import { DeletePanelViews } from 'app/views/modules/AccountManagement/InformationBases/views/DeletePanel';
import { GetAvailabilityInfoDb } from 'app/web/api/InfoDbProxy/responce-dto/GetAvailabilityInfoDb';
import { InfoDbListItemDataModel } from 'app/web/InterlayerApiProxy/InfoDbApiProxy/receiveDatabaseList/data-models';
import cn from 'classnames';
import { PureComponent, RefObject } from 'react';
import css from '../styles.module.css';
import SettingsIcon from '@mui/icons-material/Settings';

export type CountBasesType = {
    count: number,
    size: number
};

type OwnProps = {
    currentUserGroups: Array<AccountUserGroup>;
    availability: GetAvailabilityInfoDb;
    dataset: InfoDbListItemDataModel[];
    datasetOther: InfoDbListItemDataModel[];
    searchViewRef: RefObject<ControlPanelInfoDbViews> | undefined;
    pagination: TablePagination;
    paginationOther: TablePagination;
    countBase: CountBasesDataModel;
    valueCheck: string[];
    allCheck: boolean;
    allSizeInMb: number;
    serverBases: CountBasesType;
    fileBases: CountBasesType;
    tabTypeBases: number;
    hasInfoDbListStatusReceived: boolean;
    isLoading: boolean;
    onDataSelect(): void;
    onDataSelectOther(): void;
    onValueChange(value: string, item: InfoDbListItemDataModel): void;
    onValueChangeAll(): void;
    showInfoDbCard(databaseName: string, item: InfoDbListItemDataModel, isOther: boolean): void;
};

export class TableDbViews extends PureComponent<OwnProps> {
    public render() {
        const deleteVisible = this.props.valueCheck.length > 0;
        const IsActive1C = !this.props.availability.MainServiceStatusInfo.ServiceIsLocked;

        return (
            <>
                { this.props.isLoading && <LoadingBounce /> }
                { deleteVisible && this.props.tabTypeBases !== 5
                    ? (
                        <DeletePanelViews
                            valueCheck={ this.props.valueCheck }
                            allSizeInMb={ this.props.allSizeInMb }
                            serverBases={ this.props.serverBases }
                            fileBases={ this.props.fileBases }
                            onDataSelect={ this.props.onDataSelect }
                        />
                    )
                    : (
                        <ControlPanelInfoDbViews
                            tabTypeBases={ this.props.tabTypeBases }
                            availability={ this.props.availability }
                            onDataSelect={ this.props.onDataSelect }
                            onDataSelectOther={ this.props.onDataSelectOther }
                            ref={ this.props.searchViewRef }
                            countBase={ this.props.countBase }
                            pagination={ this.props.pagination }
                            paginationOther={ this.props.paginationOther }
                            currentUserGroups={ this.props.currentUserGroups }
                        />
                    ) }
                <CommonTableWithFilter
                    refreshId={ ERefreshId.myDatabasesTable }
                    tourId={ ETourId.myDatabasesPageTable }
                    data-testid="myInfoDb"
                    uniqueContextProviderStateId="InfoDbListContextProviderStateId"
                    tableProps={ {
                        dataset: this.props.dataset,
                        keyFieldName: 'databaseId',
                        headerTitleTable: {
                            text: 'Мои информационные базы'
                        },
                        fieldsView: {
                            databaseId: {
                                style: {
                                    display: !CheckBoxDeleteDbIsVisible(this.props.availability.PermissionsForWorkingWithDatabase.HasPermissionForMultipleActionsWithDb)
                                        ? 'none'
                                        : ''
                                },
                                mobileCaption: 'Выбрать базу',
                                caption: (
                                    <CheckBoxForm
                                        className={ cn(css.check) }
                                        formName="isDefault"
                                        label=""
                                        isChecked={ this.props.allCheck }
                                        onValueChange={ this.props.onValueChangeAll }
                                        isReadOnly={ this.props.tabTypeBases === 5 }
                                    />
                                ),
                                fieldContentNoWrap: false,
                                fieldWidth: '3%',
                                format: (value: string, item: InfoDbListItemDataModel) => {
                                    return (
                                        <CheckBoxForm
                                            isReadOnly={ item.state === DatabaseState.NewItem ||
                                            item.state === DatabaseState.DetachingToTomb || item.state === DatabaseState.DelitingToTomb || this.props.tabTypeBases === 5 }
                                            className={ cn(css.check) }
                                            formName={ value }
                                            label=""
                                            isChecked={ this.props.valueCheck.includes(value) }
                                            onValueChange={ () => this.props.onValueChange(value, item) }
                                        />
                                    );
                                }
                            },
                            icon: {
                                noNeedForMobile: true,
                                caption: '',
                                fieldContentNoWrap: false,
                                fieldWidth: '3%',
                                isSortable: false,
                                format: (value: string) => {
                                    return <HomeSvgSelector width={ 36 } height={ 36 } id={ value } />;
                                }
                            },
                            name: {
                                caption: 'Название',
                                fieldContentNoWrap: false,
                                fieldWidth: '30%',
                                isSortable: true,
                                format: (value: string, item: InfoDbListItemDataModel) => {
                                    return (
                                        <>
                                            <Box onClick={ () => this.props.showInfoDbCard(value, item, false) } style={ { padding: '4px 5px 0', cursor: 'pointer' } }>
                                                <TextOut inDiv={ true } fontSize={ 13 } className="text-link">
                                                    { value }
                                                </TextOut>
                                            </Box>
                                            {
                                                this.props.tabTypeBases !== 5 && <DbStateLineViews
                                                    hasInfoDbListStatusReceived={ this.props.hasInfoDbListStatusReceived }
                                                    item={ item }
                                                />
                                            }
                                        </>
                                    );
                                }
                            },
                            template: {
                                caption: 'Конфигурация',
                                fieldContentNoWrap: false,
                                isSortable: true,
                                fieldWidth: '20%'
                            },
                            sizeInMb: {
                                caption: 'Размер',
                                fieldContentNoWrap: false,
                                isSortable: true,
                                fieldWidth: '10%',
                                format: (value: number) => {
                                    return (
                                        <TextOut fontSize={ 13 }>
                                            { NumberUtility.megabytesToClassicFormat('-', value) }
                                        </TextOut>
                                    );
                                }
                            },
                            lastActivityDate: {
                                caption: 'Активность',
                                isSortable: true,
                                fieldContentNoWrap: false,
                                wrapCellHeader: true,
                                fieldWidth: '12%',
                                format: (value: Date) => {
                                    return DateUtility.dateToDayMonthYearHourMinute(value);
                                }
                            },
                            webPublishPath: {
                                caption: 'WEB',
                                fieldContentNoWrap: false,
                                isSortable: true,
                                fieldWidth: '8%',
                                format: (value: string, item: InfoDbListItemDataModel) => {
                                    return value &&
                                        WebPathDbIsVisible(this.props.availability.PermissionsForWorkingWithDatabase.HasPermissionForWeb, item) &&
                                        !(this.props.availability.MainServiceStatusInfo.ServiceLockReasonType === ServiceLockReasonType.NoDiskSpace)
                                        ? (
                                            <TextButton onClick={ () => window.open(value, '_blank') } className={ cn(css.runDb) }>
                                                <TextOut style={ { color: '#337AB7', width: '70px' } } inDiv={ true } fontSize={ 13 }>
                                                    Запустить
                                                </TextOut>
                                            </TextButton>
                                        )
                                        : null;
                                }
                            },
                            hasSupport: {
                                caption: 'ТиИ',
                                fieldContentNoWrap: false,
                                fieldWidth: '5%',
                                isSortable: true,
                                format: (value: boolean, item: InfoDbListItemDataModel) => {
                                    return (
                                        HasSupportDbIsVisible(item) && (
                                            <CustomizedSwitches
                                                checked={ value }
                                                onChange={ () => { /*empty*/ } }
                                                disabled={ true }
                                            />
                                        )
                                    );
                                }
                            },
                            hasAutoUpdate: {
                                caption: 'Автообновление',
                                style: {
                                    display: !this.props.availability.PermissionsForWorkingWithDatabase.HasPermissionForDisplayAutoUpdateButton
                                        ? 'none' : ''
                                },
                                fieldContentNoWrap: false,
                                fieldWidth: IsActive1C ? '7%' : '0%',
                                isSortable: IsActive1C,
                                format: (value: boolean, item: InfoDbListItemDataModel) => {
                                    return (
                                        AutoUpdateDbIsVisible(item) && (
                                            <CustomizedSwitches
                                                checked={ value }
                                                onChange={ () => { /*empty*/ } }
                                                disabled={ true }
                                            />
                                        )
                                    );
                                }
                            },
                            templateCaption: {
                                caption: '',
                                format: (_, item: InfoDbListItemDataModel) => {
                                    return (
                                        <IconButton onClick={ () => this.props.showInfoDbCard(item.name, item, false) } sx={ { padding: 0 } }>
                                            <SettingsIcon />
                                        </IconButton>
                                    );
                                }
                            },
                        },
                        pagination: { ...this.props.pagination, selectIsVisible: true, selectCount: ['10', '50', '250'] }
                    } }
                    onDataSelect={ this.props.onDataSelect }
                />
                <CommonTableWithFilter
                    uniqueContextProviderStateId="InfoDbListOtherContextProviderStateId"
                    tableProps={ {
                        headerTitleTable: {
                            text: 'Доступные информационные базы других аккаунтов',
                        },
                        dataset: this.props.datasetOther,
                        keyFieldName: 'databaseId',
                        fieldsView: {
                            databaseId: {
                                caption: '',
                                fieldContentNoWrap: false,
                                fieldWidth: '0%',
                                format: () => null
                            },
                            icon: {
                                caption: '',
                                fieldContentNoWrap: false,
                                fieldWidth: '3%',
                                isSortable: false,
                                format: (value: string) => {
                                    return <HomeSvgSelector width={ 36 } height={ 36 } id={ value } />;
                                }
                            },

                            name: {
                                caption: 'Название',
                                fieldContentNoWrap: false,
                                fieldWidth: '35%',
                                isSortable: true,
                                format: (value: string, item: InfoDbListItemDataModel) => {
                                    return (
                                        <div>
                                            <Box onClick={ () => this.props.showInfoDbCard(value, item, true) } style={ { cursor: 'pointer', padding: 0 } }>
                                                <TextOut inDiv={ true } fontSize={ 13 } className="text-link">
                                                    { value }
                                                </TextOut>
                                            </Box>
                                            <DbStateLineViews
                                                hasInfoDbListStatusReceived={ this.props.hasInfoDbListStatusReceived }
                                                item={ item }
                                            />
                                        </div>
                                    );
                                }
                            },
                            template: {
                                caption: 'Шаблон',
                                fieldContentNoWrap: false,
                                isSortable: true,
                                fieldWidth: '25%'
                            },
                            sizeInMb: {
                                caption: 'Размер',
                                fieldContentNoWrap: false,
                                isSortable: true,
                                fieldWidth: '15%',
                                format: (value: number) => {
                                    return (
                                        <TextOut fontSize={ 13 }>
                                            { NumberUtility.megabytesToClassicFormat('-', value) }
                                        </TextOut>
                                    );
                                }

                            },

                            lastActivityDate: {
                                caption: 'Активность',
                                isSortable: true,
                                fieldContentNoWrap: false,
                                wrapCellHeader: true,
                                fieldWidth: '15%',
                                format: (value: Date) => {
                                    return DateUtility.dateToDayMonthYearHourMinute(value);
                                }
                            },
                            webPublishPath: {
                                caption: 'WEB',
                                fieldContentNoWrap: false,
                                isSortable: true,
                                fieldWidth: '10%',
                                format: (value: string, item: InfoDbListItemDataModel) => {
                                    return value &&
                                        WebPathDbIsVisible(this.props.availability.PermissionsForWorkingWithDatabase.HasPermissionForWeb, item) &&
                                        !(this.props.availability.MainServiceStatusInfo.ServiceLockReasonType === ServiceLockReasonType.NoDiskSpace) &&
                                        <TextButton onClick={ () => window.open(value, '_blank') } className={ cn(css.runDb) }>
                                            <TextOut style={ { color: '#337AB7', width: '70px' } } inDiv={ true } fontSize={ 13 }>
                                                Запустить
                                            </TextOut>
                                        </TextButton>;
                                }
                            },
                        },
                        pagination: { ...this.props.paginationOther, selectIsVisible: true, selectCount: ['10', '50', '250'] }
                    } }
                    onDataSelect={ this.props.onDataSelectOther }
                />
            </>
        );
    }
}