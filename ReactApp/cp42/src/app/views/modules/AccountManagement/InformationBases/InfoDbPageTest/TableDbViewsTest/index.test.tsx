import '@testing-library/jest-dom/extend-expect';
import { render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { partialFillOf } from 'app/common/functions';
import { InfoDbCardConsts } from 'app/modules/infoDbCard/constants';
import { receiveAccessReducer } from 'app/modules/infoDbCard/store/reducers/receiveAccessInfoDbCardReducer';
import { receiveBuckapsReducer } from 'app/modules/infoDbCard/store/reducers/receiveBuckupsReducer';
import { receiveAccessCostReducer } from 'app/modules/infoDbCard/store/reducers/receiveCalculateAccessInfoDbCardReducer';
import { receiveInfoDbCardReducer } from 'app/modules/infoDbCard/store/reducers/receiveDatabaseCardReducer';
import { receiveCommonInfoDbCardReducer } from 'app/modules/infoDbCard/store/reducers/receiveDatabaseCommonCardReducer';
import { receiveHasSupportReducer } from 'app/modules/infoDbCard/store/reducers/receiveHasSupportReducer';
import { receiveRightsInfoDbCardReducer } from 'app/modules/infoDbCard/store/reducers/receiveRightsInfoDbCardReducer';
import { receiveServicesReducer } from 'app/modules/infoDbCard/store/reducers/receiveServicesReducer';
import { receiveStatusServiceReducer } from 'app/modules/infoDbCard/store/reducers/receiveStatusServiceInfoDbCardReducer';
import { AppReduxStoreReducers } from 'app/redux';
import { availabilityTestForTable } from 'app/views/modules/AccountManagement/InformationBases/InfoDbPageTest/mock/availability';
import { countBase } from 'app/views/modules/AccountManagement/InformationBases/InfoDbPageTest/mock/countBase';
import { InfoDbListTest1, InfoDbListTestOther1 } from 'app/views/modules/AccountManagement/InformationBases/InfoDbPageTest/mock/infoDbListTest';
import { itemInfoDb } from 'app/views/modules/AccountManagement/InformationBases/InfoDbPageTest/mock/itemInfoDb';
import { paginationMock } from 'app/views/modules/AccountManagement/InformationBases/InfoDbPageTest/mock/paginationMock';
import { serviceTableList1 } from 'app/views/modules/AccountManagement/InformationBases/InfoDbPageTest/mock/serviceTableList';
import { tablePaginationTest } from 'app/views/modules/AccountManagement/InformationBases/InfoDbPageTest/mock/tablePaginationTest';
import { ControlPanelInfoDbViews } from 'app/views/modules/AccountManagement/InformationBases/views/ControlPanelInfoDb';
import { TableDbViews } from 'app/views/modules/AccountManagement/InformationBases/views/TableDb';
import { HasSupportInfoDbDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/HasSupportInfoDbDataModel';
import { createReducer, createStore, initReducerState } from 'core/redux/functions';
import { createRef } from 'react';
import { Provider } from 'react-redux';
import { applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';

describe('TableDbViewsTest', () => {
    const store = createStore({
        ...AppReduxStoreReducers,
        InfoDbCardState: createReducer(initReducerState<any>(InfoDbCardConsts.reducerName, {
            commonDetails: itemInfoDb(),
            hasSupportInfoDb: partialFillOf<HasSupportInfoDbDataModel>(),
            rightsInfoDb: [],
            databaseAccesses: [],
            databaseAccessesCost: [],
            servicesInfoDb: serviceTableList1,
            backupsInfoDb: {
                databaseBackups: [],
                dbBackupsCount: 0
            },
            hasSuccessFor: {
                hasDatabaseCardReceived: true,
                hasCommonDatabaseCardReceived: true,
                hasHasSupportReceived: true,
                hasServicesReceived: true,
                hasAccessReceived: true,
                hasAccessCostReceived: true,
                hasBackupsReceived: true
            }
        }), [receiveInfoDbCardReducer,
            receiveCommonInfoDbCardReducer,
            receiveRightsInfoDbCardReducer,
            receiveHasSupportReducer,
            receiveAccessReducer,
            receiveAccessCostReducer,
            receiveServicesReducer,
            receiveStatusServiceReducer,
            receiveBuckapsReducer])
    }, composeWithDevTools(applyMiddleware(thunk)));

    const onDataSelect = jest.fn();
    const onDataSelect1 = jest.fn().mockImplementation(() => InfoDbListTest1);
    const ref = createRef<ControlPanelInfoDbViews>();

    it('Test void Table', async () => {
        const { queryAllByText } = render(
            <TableDbViews
                hasInfoDbListStatusReceived={ true }
                isLoading={ true }
                tabTypeBases={ 1 }
                data-testid="TableDbViews"
                currentUserGroups={ [1, 3] }
                availability={ availabilityTestForTable }
                dataset={ [] }
                datasetOther={ [] }
                onDataSelectOther={ onDataSelect }
                onDataSelect={ onDataSelect }
                searchViewRef={ ref }
                pagination={ tablePaginationTest }
                paginationOther={ tablePaginationTest }
                countBase={ countBase }
                onValueChange={ onDataSelect }
                onValueChangeAll={ onDataSelect }
                valueCheck={ [] }
                allCheck={ false }
                allSizeInMb={ 0 }
                serverBases={ { count: 0, size: 0 } }
                fileBases={ { count: 0, size: 0 } }
                showInfoDbCard={ onDataSelect }
            />
        );

        expect(queryAllByText(/нет записей/).length).toBe(2);
    });

    it('Test Table with dataset', async () => {
        const { getByText } = render(
            <Provider store={ { ...store } }>
                <TableDbViews
                    hasInfoDbListStatusReceived={ true }
                    isLoading={ true }
                    tabTypeBases={ 1 }
                    currentUserGroups={ [] }
                    availability={ availabilityTestForTable }
                    dataset={ InfoDbListTest1 }
                    datasetOther={ InfoDbListTestOther1 }
                    onDataSelectOther={ onDataSelect }
                    onDataSelect={ onDataSelect1 }
                    searchViewRef={ ref }
                    pagination={ paginationMock }
                    paginationOther={ paginationMock }
                    countBase={ countBase }
                    onValueChange={ onDataSelect }
                    onValueChangeAll={ onDataSelect }
                    valueCheck={ [] }
                    allCheck={ false }
                    allSizeInMb={ 0 }
                    serverBases={ { count: 0, size: 0 } }
                    fileBases={ { count: 0, size: 0 } }
                    showInfoDbCard={ onDataSelect }
                />

            </Provider>
        );

        await userEvent.click(getByText(/Все базы/));
    });
    it('Test Table with dataset not empty', async () => {
        const { queryAllByText } = render(
            <TableDbViews
                hasInfoDbListStatusReceived={ true }
                isLoading={ true }
                tabTypeBases={ 1 }
                data-testid="myInfoDb"
                currentUserGroups={ [] }
                availability={ availabilityTestForTable }
                dataset={ InfoDbListTest1 }
                datasetOther={ InfoDbListTestOther1 }
                onDataSelectOther={ onDataSelect1 }
                onDataSelect={ onDataSelect }
                searchViewRef={ ref }
                pagination={ paginationMock }
                paginationOther={ paginationMock }
                countBase={ countBase }
                onValueChange={ onDataSelect }
                onValueChangeAll={ onDataSelect }
                valueCheck={ [] }
                allCheck={ false }
                allSizeInMb={ 0 }
                serverBases={ { count: 0, size: 0 } }
                fileBases={ { count: 0, size: 0 } }
                showInfoDbCard={ onDataSelect }
            />
        );

        expect(queryAllByText(/Название/).length).toBe(2);
    });
    it('Test Table with dataset not empty', async () => {
        const { queryAllByText, queryByText } = render(
            <Provider store={ store }>
                <TableDbViews
                    hasInfoDbListStatusReceived={ true }
                    isLoading={ true }
                    tabTypeBases={ 1 }
                    currentUserGroups={ [] }
                    availability={ availabilityTestForTable }
                    dataset={ InfoDbListTest1 }
                    datasetOther={ InfoDbListTestOther1 }
                    onDataSelectOther={ onDataSelect1 }
                    onDataSelect={ onDataSelect }
                    searchViewRef={ ref }
                    pagination={ paginationMock }
                    paginationOther={ paginationMock }
                    countBase={ countBase }
                    onValueChange={ onDataSelect }
                    onValueChangeAll={ onDataSelect }
                    valueCheck={ [] }
                    allCheck={ false }
                    allSizeInMb={ 0 }
                    serverBases={ { count: 0, size: 0 } }
                    fileBases={ { count: 0, size: 0 } }
                    showInfoDbCard={ onDataSelect }
                />
            </Provider>
        );

        const { name } = InfoDbListTest1[0];
        expect(queryAllByText(/Название/).length).toBe(2);
        await userEvent.click(queryByText(name)!);
    });
});