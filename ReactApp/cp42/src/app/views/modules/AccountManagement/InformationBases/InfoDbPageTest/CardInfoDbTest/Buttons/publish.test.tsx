import '@testing-library/jest-dom/extend-expect';
import { render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { DbPublishState } from 'app/common/enums';
import { createAppReduxStore } from 'app/redux/functions';
import { PublishInfoDbButton } from 'app/views/modules/_common/reusable-modules/InfoDbCard/buttons/PublishInfoDbButton';
import { SnackbarProvider } from 'notistack';
import { Provider } from 'react-redux';
import { applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';

describe('publish Button', () => {
    let onChangeField: jest.Mock;

    const store = createAppReduxStore(composeWithDevTools(applyMiddleware(thunk)));

    it('take off publish renderButton ', async () => {
        const { queryByText } = render(
            <Provider store={ { ...store } }>
                <SnackbarProvider>
                    <PublishInfoDbButton
                        usedWebServices={ false }
                        databaseId=""
                        onChangeField={ onChangeField }
                        isInPublishingState={ DbPublishState.Published }
                        canWebPublish={ true }
                    />
                </SnackbarProvider>
            </Provider>
        );
        expect(queryByText(/Снять с публикации/)).toBeInTheDocument();
    });
    it('publish renderButton ', async () => {
        const { queryByText } = render(
            <Provider store={ { ...store } }>
                <SnackbarProvider>
                    <PublishInfoDbButton
                        usedWebServices={ false }
                        databaseId=""
                        onChangeField={ onChangeField }
                        isInPublishingState={ DbPublishState.Unpublished }
                        canWebPublish={ true }
                    />
                </SnackbarProvider>
            </Provider>
        );
        expect(queryByText(/Опубликовать/)).toBeInTheDocument();
    });
    it('publish Base', async () => {
        const { debug, getByText } = render(
            <Provider store={ { ...store } }>
                <SnackbarProvider>
                    <PublishInfoDbButton
                        usedWebServices={ false }
                        databaseId=""
                        onChangeField={ onChangeField }
                        isInPublishingState={ DbPublishState.Unpublished }
                        canWebPublish={ true }
                    />
                </SnackbarProvider>
            </Provider>
        );
        const btn = getByText(/Опубликовать/);
        expect(btn).toBeInTheDocument();
        await userEvent.click(btn);
        debug(undefined, Infinity);
    });
    it('publish Base proccess', async () => {
        const { getByText } = render(
            <Provider store={ { ...store } }>
                <SnackbarProvider>
                    <PublishInfoDbButton
                        usedWebServices={ false }
                        databaseId=""
                        onChangeField={ onChangeField }
                        isInPublishingState={ DbPublishState.PendingPublication }
                        canWebPublish={ true }
                    />
                </SnackbarProvider>
            </Provider>
        );
        const btn = getByText(/База в процессе публикации/);
        expect(btn).toBeInTheDocument();
    });
    it(' take off publish Base proccess', async () => {
        const { getByText } = render(
            <Provider store={ { ...store } }>
                <SnackbarProvider>
                    <PublishInfoDbButton
                        usedWebServices={ false }
                        databaseId=""
                        onChangeField={ onChangeField }
                        isInPublishingState={ DbPublishState.PendingUnpublication }
                        canWebPublish={ true }
                    />
                </SnackbarProvider>
            </Provider>
        );
        const btn = getByText(/База в процессе снятия с публикации/);
        expect(btn).toBeInTheDocument();
    });
});