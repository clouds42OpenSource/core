import '@testing-library/jest-dom/extend-expect';
import { render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { partialFillOf } from 'app/common/functions';
import { createAppReduxStore } from 'app/redux/functions/createAppReduxStore';
import { availabilityTest } from 'app/views/modules/AccountManagement/InformationBases/InfoDbPageTest/mock/availability';
import { InfoDbListTest } from 'app/views/modules/AccountManagement/InformationBases/InfoDbPageTest/mock/infoDbListTest';
import { itemInfoDb } from 'app/views/modules/AccountManagement/InformationBases/InfoDbPageTest/mock/itemInfoDb';
import { InfoDbCardEditDatabaseDataForm } from 'app/views/modules/AccountManagement/InformationBases/types/InfiDbCardEditDatabaseDataForm';
import { FieldsCommonCardView } from 'app/views/modules/_common/reusable-modules/InfoDbCard/infoDbCardContent/tabs/CommonTabView/FieldsCommonCardView';
import { Provider } from 'react-redux';
import { applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';

describe('CardInfoDbTest', () => {
    const onValueChangeWebServices = jest.fn();
    const onChangeField = jest.fn();
    const onValueChange = jest.fn();
    const onChangeV82Name = jest.fn();

    const reduxForm = partialFillOf<InfoDbCardEditDatabaseDataForm>();
    const store = createAppReduxStore(composeWithDevTools(applyMiddleware(thunk)));

    it('templateCard Read Only from permissions', async () => {
        const { getByText, queryByText } = render(
            <Provider store={ { ...store } }>
                <FieldsCommonCardView
                    availability={ availabilityTest }
                    commonDetails={ itemInfoDb()! }
                    onChangeField={ onChangeField }
                    item={ InfoDbListTest[0] }
                    onChangeV82Name={ onChangeV82Name }
                    permissions={ [] }
                    reduxForm={ reduxForm }
                    onValueChange={ onValueChange }
                    onValueChangeWebServices={ onValueChangeWebServices }
                    errorV82Name=""
                    changeWebServices={ true }
                    isOther={ true }
                    currentUserGroups={ [6] }
                />
            </Provider>
        );
        const template = itemInfoDb()!.caption;
        const variableTemplate = itemInfoDb()!.commonDataForWorkingWithDB.AvailableDbTemplates[0].value;

        expect(queryByText(template)).toBeInTheDocument();

        await userEvent.click(getByText(template));

        expect(queryByText(variableTemplate)).not.toBeInTheDocument();
    });

    it('templateCard with change value from permissions', async () => {
        const { getByText, queryByText } = render(
            <Provider store={ { ...store } }>
                <FieldsCommonCardView
                    availability={ availabilityTest }
                    commonDetails={ itemInfoDb()! }
                    onChangeField={ onChangeField }
                    item={ InfoDbListTest[0] }
                    onChangeV82Name={ onChangeV82Name }
                    permissions={ ['AccountDatabases_EditFull'] }
                    reduxForm={ reduxForm }
                    onValueChange={ onValueChange }
                    onValueChangeWebServices={ onValueChangeWebServices }
                    errorV82Name=""
                    changeWebServices={ true }
                    isOther={ true }
                    currentUserGroups={ [6] }
                />
            </Provider>
        );
        const template = itemInfoDb()!.caption;
        const variableTemplate = itemInfoDb()!.commonDataForWorkingWithDB.AvailableDbTemplates[0].value;

        expect(queryByText(template)).toBeInTheDocument();
        await userEvent.click(getByText(template));
        expect(queryByText(variableTemplate)).toBeInTheDocument();
    });

    it('templateCard with change value from isDbOnDelimiters', async () => {
        const { getByText, queryByText } = render(
            <Provider store={ { ...store } }>
                <FieldsCommonCardView
                    availability={ availabilityTest }
                    commonDetails={ { ...itemInfoDb()!, isDbOnDelimiters: false } }
                    onChangeField={ onChangeField }
                    item={ InfoDbListTest[0] }
                    onChangeV82Name={ onChangeV82Name }
                    permissions={ ['AccountDatabases_EditFull'] }
                    reduxForm={ reduxForm }
                    onValueChange={ onValueChange }
                    onValueChangeWebServices={ onValueChangeWebServices }
                    errorV82Name=""
                    changeWebServices={ true }
                    isOther={ true }
                    currentUserGroups={ [6] }
                />
            </Provider>
        );
        const template = itemInfoDb()!.caption;
        const variableTemplate = itemInfoDb()!.commonDataForWorkingWithDB.AvailableDbTemplates[0].value;

        expect(queryByText(template)).toBeInTheDocument();
        await userEvent.click(getByText(template));
        expect(queryByText(variableTemplate)).toBeInTheDocument();
    });

    it('templateCard with Read only value from isDbOnDelimiters', async () => {
        const { getByText, queryByText } = render(
            <Provider store={ { ...store } }>
                <FieldsCommonCardView
                    availability={ availabilityTest }
                    commonDetails={ { ...itemInfoDb()!, isDbOnDelimiters: true } }
                    onChangeField={ onChangeField }
                    item={ InfoDbListTest[0] }
                    onChangeV82Name={ onChangeV82Name }
                    permissions={ ['AccountDatabases_EditFull'] }
                    reduxForm={ reduxForm }
                    onValueChange={ onValueChange }
                    onValueChangeWebServices={ onValueChangeWebServices }
                    errorV82Name=""
                    changeWebServices={ true }
                    isOther={ true }
                    currentUserGroups={ [6] }
                />
            </Provider>
        );
        const template = itemInfoDb()!.caption;
        const variableTemplate = itemInfoDb()!.commonDataForWorkingWithDB.AvailableDbTemplates[0].value;

        expect(queryByText(template)).toBeInTheDocument();
        await userEvent.click(getByText(template));
        expect(queryByText(variableTemplate)).not.toBeInTheDocument();
    });
});