import '@testing-library/jest-dom/extend-expect';
import { render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { createAppReduxStore } from 'app/redux/functions';
import { RestoreInfoDbButton } from 'app/views/modules/_common/reusable-modules/InfoDbCard/buttons/RestoreInfoDbButton';
import { SnackbarProvider } from 'notistack';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import { applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';

describe('publish Button', () => {
    const store = createAppReduxStore(composeWithDevTools(applyMiddleware(thunk)));

    it('restore renderButton ', async () => {
        const { queryByText } = render(
            <Provider store={ { ...store } }>
                <BrowserRouter basename="">
                    <SnackbarProvider>
                        <RestoreInfoDbButton backupId="" />
                    </SnackbarProvider>
                </BrowserRouter>
            </Provider>
        );
        expect(queryByText(/Восстановить/)).toBeInTheDocument();
    });
    it('restore click button ', async () => {
        const { queryByText, getByText } = render(
            <Provider store={ { ...store } }>
                <BrowserRouter basename="">
                    <SnackbarProvider>
                        <RestoreInfoDbButton backupId="" />
                    </SnackbarProvider>
                </BrowserRouter>
            </Provider>
        );
        const btn = getByText(/Восстановить/);
        await userEvent.click(btn);
        expect(queryByText(/Восстановить/)).toBeInTheDocument();
    });
});