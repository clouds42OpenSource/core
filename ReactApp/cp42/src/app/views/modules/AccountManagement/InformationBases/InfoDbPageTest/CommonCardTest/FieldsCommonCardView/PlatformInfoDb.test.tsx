import '@testing-library/jest-dom/extend-expect';
import { render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { PlatformType, PlatformTypeDescription } from 'app/common/enums';
import { partialFillOf } from 'app/common/functions';
import { createAppReduxStore } from 'app/redux/functions/createAppReduxStore';
import { availabilityTest } from 'app/views/modules/AccountManagement/InformationBases/InfoDbPageTest/mock/availability';
import { InfoDbListTest } from 'app/views/modules/AccountManagement/InformationBases/InfoDbPageTest/mock/infoDbListTest';
import { itemInfoDb } from 'app/views/modules/AccountManagement/InformationBases/InfoDbPageTest/mock/itemInfoDb';
import { InfoDbCardEditDatabaseDataForm } from 'app/views/modules/AccountManagement/InformationBases/types/InfiDbCardEditDatabaseDataForm';
import { FieldsCommonCardView } from 'app/views/modules/_common/reusable-modules/InfoDbCard/infoDbCardContent/tabs/CommonTabView/FieldsCommonCardView';
import { Provider } from 'react-redux';
import { applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';

describe('PlatformInfoDbTest', () => {
    const onValueChangeWebServices = jest.fn();
    const onChangeField = jest.fn();
    const onValueChange = jest.fn();
    const onChangeV82Name = jest.fn();
    const reduxForm = partialFillOf<InfoDbCardEditDatabaseDataForm>();

    const store = createAppReduxStore(composeWithDevTools(applyMiddleware(thunk)));

    it('render Read only platform InfoDb', async () => {
        const { queryByText, getByText } = render(
            <Provider store={ { ...store } }>
                <FieldsCommonCardView
                    availability={ availabilityTest }
                    commonDetails={ itemInfoDb()! }
                    onChangeField={ onChangeField }
                    item={ InfoDbListTest[0] }
                    onChangeV82Name={ onChangeV82Name }
                    permissions={ ['AccountDatabases_EditPlatform'] }
                    reduxForm={ reduxForm }
                    onValueChange={ onValueChange }
                    onValueChangeWebServices={ onValueChangeWebServices }
                    errorV82Name=""
                    isOther={ true }
                    changeWebServices={ true }
                    currentUserGroups={ [] }
                />
            </Provider>
        );
        const platform = PlatformTypeDescription[itemInfoDb()!.platformType];
        const secondPlatform = PlatformTypeDescription[[PlatformType.V82, PlatformType.V83].filter(i => i !== itemInfoDb()!.platformType)[0]];
        await userEvent.click(getByText(platform));
        expect(queryByText(platform)).toBeInTheDocument();
        expect(queryByText(secondPlatform)).not.toBeInTheDocument();
    });

    it('render with change platform InfoDb', async () => {
        const { queryByText, getByText } = render(
            <Provider store={ { ...store } }>
                <FieldsCommonCardView
                    availability={ availabilityTest }
                    commonDetails={ { ...itemInfoDb()!, isFile: true, isDbOnDelimiters: false } }
                    onChangeField={ onChangeField }
                    item={ InfoDbListTest[0] }
                    onChangeV82Name={ onChangeV82Name }
                    permissions={ ['AccountDatabases_EditPlatform'] }
                    reduxForm={ reduxForm }
                    onValueChange={ onValueChange }
                    onValueChangeWebServices={ onValueChangeWebServices }
                    errorV82Name=""
                    isOther={ true }
                    changeWebServices={ true }
                    currentUserGroups={ [] }
                />
            </Provider>
        );
        const platform = PlatformTypeDescription[itemInfoDb()!.platformType];
        const secondPlatform = PlatformTypeDescription[[PlatformType.V82, PlatformType.V83].filter(i => i !== itemInfoDb()!.platformType)[0]];
        expect(queryByText(platform)).toBeInTheDocument();
        await userEvent.click(getByText(platform));
        expect(queryByText(secondPlatform)).not.toBeInTheDocument();
    });
});