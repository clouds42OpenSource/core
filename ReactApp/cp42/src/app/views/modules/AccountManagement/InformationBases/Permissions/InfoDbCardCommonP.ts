import { DatabaseState, DbPublishState, PlatformType } from 'app/common/enums';
import { GetAvailabilityInfoDb } from 'app/web/api/InfoDbProxy/responce-dto/GetAvailabilityInfoDb';
import { InfoDbCardPermissionsEnum } from 'app/web/common/enums/InfoDbCardPermissionsEnum';
import { InfoDbCardAccountDatabaseInfoDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/InfoDbCardAccountDatabaseInfoDataModel';
import { RightsInfoDbCardDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getRightsInfoDbCard/data-models/InfoDbCardAccountDatabaseInfoDataModel';

/**
 * Можно ли редактировать Шаблон базы
 */
export function InfoDbTemplatePermissions(isDbOnDelimiters: boolean, permissions: RightsInfoDbCardDataModel) {
    return !isDbOnDelimiters && permissions.includes(InfoDbCardPermissionsEnum.AccountDatabases_EditFull);
}

/**
 * Можно ли редактировать Платформу базы
 */
export function InfoDbPlatformPermissions(common: InfoDbCardAccountDatabaseInfoDataModel, permissions: RightsInfoDbCardDataModel) {
    return !!(permissions.includes(InfoDbCardPermissionsEnum.AccountDatabases_EditPlatform) &&
        common.databaseState === DatabaseState.Ready &&
        common.isFile &&
        !common.isDbOnDelimiters &&
        common.stable82Version);
}

/**
 * Можно ли редактировать Релиз платформы базы
 */
export function InfoDbDestributionPlatformPermissions(common: InfoDbCardAccountDatabaseInfoDataModel, permissions: RightsInfoDbCardDataModel) {
    return !!(permissions.includes(InfoDbCardPermissionsEnum.AccountDatabases_EditPlatform) &&
        common.alpha83Version &&
        common.alpha83Version !== common.stable83Version &&
        common.isFile &&
        common.platformType === PlatformType.V83 &&
        common.databaseState !== DatabaseState.DetachedToTomb);
}

/**
 * Показывать ли веб ссылку
 */
export function InfoDbWebLinkPermissions(common: InfoDbCardAccountDatabaseInfoDataModel, permissions: RightsInfoDbCardDataModel, availability: GetAvailabilityInfoDb, needShowWebLink: boolean) {
    return availability.IsMainServiceAllowed &&
        common.publishState === DbPublishState.Published &&
        availability.PermissionsForWorkingWithDatabase.HasPermissionForWeb &&
        common.databaseState === DatabaseState.Ready &&
        needShowWebLink;
}

/**
 * Показывать ли путь к базе
 */
export function InfoDbPathToBasePermissions(common: InfoDbCardAccountDatabaseInfoDataModel) {
    return common.isFile || common.isDbOnDelimiters;
}