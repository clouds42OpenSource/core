export const availabilityTest = {
    IsVipAccount: false,
    IsMainServiceAllowed: false,
    AccessToServerDatabaseServiceTypeId: '',
    AccountAdminInfo: '',
    AccountId: '',
    AccountLocaleName: '',
    AccountUserId: '',
    MyDatabaseServiceId: '',
    AccountAdminPhoneNumber: '',
    MainServiceStatusInfo: {
        PromisedPaymentExpireDate: null,
        PromisedPaymentIsActive: false,
        PromisedPaymentSum: null,
        ServiceIsLocked: false,
        ServiceLockReason: 'Сервис \'Аренда 1С\' заблокирован.',
        ServiceLockReasonType: null
    },
    AllowAddingDatabaseInfo: {
        ForbiddeningReasonMessage: '',
        IsAllowedCreateDb: false,
        SurchargeForCreation: 0
    },
    PermissionsForWorkingWithDatabase: {
        HasPermissionForCreateAccountDatabase: false,
        HasPermissionForDisplayAutoUpdateButton: false,
        HasPermissionForDisplayPayments: false,
        HasPermissionForDisplaySupportData: false,
        HasPermissionForMultipleActionsWithDb: false,
        HasPermissionForRdp: false,
        HasPermissionForWeb: false,
    }
};
export const availabilityTestForTable = {
    IsVipAccount: false,
    IsMainServiceAllowed: true,
    AccessToServerDatabaseServiceTypeId: '',
    AccountAdminInfo: '',
    AccountId: '',
    AccountLocaleName: 'ru-ru',
    AccountUserId: '',
    MyDatabaseServiceId: '',
    AccountAdminPhoneNumber: '',
    MainServiceStatusInfo: {
        PromisedPaymentExpireDate: null,
        PromisedPaymentIsActive: false,
        PromisedPaymentSum: null,
        ServiceIsLocked: false,
        ServiceLockReason: '',
        ServiceLockReasonType: null
    },
    AllowAddingDatabaseInfo: {
        ForbiddeningReasonMessage: '',
        IsAllowedCreateDb: true,
        SurchargeForCreation: 0
    },
    PermissionsForWorkingWithDatabase: {
        HasPermissionForCreateAccountDatabase: true,
        HasPermissionForDisplayAutoUpdateButton: true,
        HasPermissionForDisplayPayments: true,
        HasPermissionForDisplaySupportData: true,
        HasPermissionForMultipleActionsWithDb: true,
        HasPermissionForRdp: true,
        HasPermissionForWeb: true
    }
};