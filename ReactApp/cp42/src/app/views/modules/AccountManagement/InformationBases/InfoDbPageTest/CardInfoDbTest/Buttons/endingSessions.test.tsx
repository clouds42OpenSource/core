import '@testing-library/jest-dom/extend-expect';
import { render } from '@testing-library/react';
import { createAppReduxStore } from 'app/redux/functions';
import { EndingSessionsButton } from 'app/views/modules/_common/reusable-modules/InfoDbCard/buttons/EndingSessionsButton';
import { SnackbarProvider } from 'notistack';
import { Provider } from 'react-redux';
import { applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';

describe('ending sessions', () => {
    let onChangeField: jest.Mock;

    const store = createAppReduxStore(composeWithDevTools(applyMiddleware(thunk)));

    it('ending sessions render', async () => {
        const { queryByText } = render(
            <Provider store={ { ...store } }>
                <SnackbarProvider>
                    <EndingSessionsButton databaseId="" databaseName="" isInPublishingState={ false } onChangeField={ onChangeField } />
                </SnackbarProvider>
            </Provider>
        );
        expect(queryByText(/Завершить сеансы/)).toBeInTheDocument();
    });
    it('ending sessions render proccess', async () => {
        const { queryByText } = render(
            <Provider store={ { ...store } }>
                <SnackbarProvider>
                    <EndingSessionsButton databaseId="" databaseName="" isInPublishingState={ true } onChangeField={ onChangeField } />
                </SnackbarProvider>
            </Provider>
        );
        expect(queryByText(/Завершение сеансов/)).toBeInTheDocument();
    });
});