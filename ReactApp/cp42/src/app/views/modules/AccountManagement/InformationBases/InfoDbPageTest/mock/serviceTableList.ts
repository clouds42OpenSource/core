import { ServicesInfoDbDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/ServicesInfoDbDataModel';

export const serviceTableList1: ServicesInfoDbDataModel[] = [
    {
        extensionLastActivityDate: new Date(),
        extensionState: 0,
        id: '526d5193-2ecc-4a81-8811-088e412eaa81',
        isInstalled: true,
        isActiveService: true,
        isFrozenService: false,
        name: 'Chrome Test 21 09 2021 Edit',
        shortDescription: 'Chrome any Test '
    }
];