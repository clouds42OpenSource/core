import { OwnStateDbList } from 'app/views/modules/AccountManagement/InformationBases';
import { InfoDbListItemDataModel } from 'app/web/InterlayerApiProxy/InfoDbApiProxy/receiveDatabaseList/data-models';

/**
 * подсчет размера и количества баз при добавлении галочки в удаление
 */
export function addCountAndSizeDb(value: string, item: InfoDbListItemDataModel, state: OwnStateDbList, dataset: InfoDbListItemDataModel[]) {
    return {
        valueCheck: [...state.valueCheck, value],
        allSizeInMb: state.allSizeInMb + item.sizeInMb,
        serverBases: {
            ...state.serverBases,
            size: !item.isDbOnDelimiters && !item.isFile
                ? state.serverBases.size + item.sizeInMb
                : state.serverBases.size,
            count: !item.isDbOnDelimiters && !item.isFile
                ? state.serverBases.count + 1
                : state.serverBases.count
        },
        fileBases: {
            ...state.fileBases,
            size: item.isFile
                ? state.fileBases.size + item.sizeInMb
                : state.fileBases.size,
            count: item.isFile
                ? state.fileBases.count + 1
                : state.fileBases.count
        },
        allCheck: dataset.length === [...state.valueCheck, value].length
    };
}

/**
 * подсчет размера и количества при убирании галочки в удаление
 */
export function removeCountAndSizeDb(value: string, item: InfoDbListItemDataModel, state: OwnStateDbList, dataset: InfoDbListItemDataModel[]) {
    return {
        valueCheck: state.valueCheck.filter((i: string) => i !== value),
        allSizeInMb: state.allSizeInMb - item.sizeInMb,
        serverBases: {
            ...state.serverBases,
            size: !item.isDbOnDelimiters && !item.isFile
                ? state.serverBases.size - item.sizeInMb
                : state.serverBases.size,
            count: !item.isDbOnDelimiters && !item.isFile
                ? state.serverBases.count - 1
                : state.serverBases.count
        },
        fileBases: {
            ...state.fileBases,
            size: item.isFile
                ? state.fileBases.size - item.sizeInMb
                : state.fileBases.size,
            count: item.isFile
                ? state.fileBases.count - 1
                : state.fileBases.count
        },
        allCheck: dataset.length === [...state.valueCheck, value].length
    };
}