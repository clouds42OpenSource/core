import { Link } from '@mui/material';
import { Box } from '@mui/system';
import { ContainedButton } from 'app/views/components/controls/Button';
import { Dialog } from 'app/views/components/controls/Dialog';
import { TextOut } from 'app/views/components/TextOut';
import cn from 'classnames';
import React, { useState } from 'react';
import css from './styles.module.css';

type OwnProps = {
    isFile?: boolean;
    isOpen: boolean;
    databaseName: string;
    onNoClick: () => void;
    onYesClick: () => void;
    onChangeField: () => void;
};

export const ChangeTypeDbDialogMessage = ({ databaseName, isOpen, onNoClick, onYesClick, isFile, onChangeField }: OwnProps) => {
    const [showWarningDialog, setShowWarningDialog] = useState(false);

    return (
        <>
            <Dialog
                isOpen={ isOpen }
                dialogWidth="xs"
                dialogVerticalAlign="center"
                onCancelClick={ onNoClick }
            >
                <div className={ cn(css['warning-icon-container'], css.pulseWarning) }>
                    <span className={ cn(css['warning-icon-circle'], css.pulseWarningIns) } />
                    <span className={ cn(css['warning-icon-dot'], css.pulseWarningIns) } />
                </div>
                <h2 className={ cn(css['sweet-alert-title']) }>
                    Изменение типа базы
                </h2>
                <p className={ cn(css['sweet-alert-body']) }>
                    Вы действительно хотите изменить тип базы { databaseName } на { !isFile ? 'файловую' : 'серверную' }
                </p>
                <div style={ { textAlign: 'center', margin: '26px 0' } }>
                    <ContainedButton
                        kind="default"
                        className={ cn(css['sweet-alert-button']) }
                        onClick={ onNoClick }
                    >
                        Нет
                    </ContainedButton>
                    <ContainedButton
                        kind="error"
                        className={ cn(css['sweet-alert-button'], css['sweet-alert-button-yes']) }
                        onClick={ () => {
                            onYesClick();
                            if (!isFile) {
                                setShowWarningDialog(true);
                            }
                        } }
                    >
                        Да
                    </ContainedButton>
                </div>
            </Dialog>
            <Dialog
                isOpen={ showWarningDialog }
                dialogWidth="sm"
                dialogVerticalAlign="center"
                title="Внимание!"
                hiddenX={ true }
                buttons={ [
                    {
                        onClick: () => {
                            onNoClick();
                            setShowWarningDialog(false);
                            onChangeField();
                        },
                        variant: 'contained',
                        kind: 'success',
                        content: 'Продолжить',
                    }
                ] }
            >
                <Box display="flex" flexDirection="column" gap="16px" alignItems="center">
                    <div className={ cn(css['warning-icon-container'], css.pulseWarning) }>
                        <span className={ cn(css['warning-icon-circle'], css.pulseWarningIns) } />
                        <span className={ cn(css['warning-icon-dot'], css.pulseWarningIns) } />
                    </div>
                    <TextOut>
                        Если у клиента больше не осталось серверных баз, необходимо отключить доступ в разделе&nbsp;
                        <Link
                            href={ `${ window.location.origin }/my-database-services/fba5f480-fbfa-4ad9-b468-9fa7d6c43524` }
                            target="_blank"
                            rel="noreferrer"
                        >
                            &#34;Клиент-серверный режим&#34;
                        </Link>.&nbsp;
                        После перевода базы в файловый вариант, необходимо удалить базу с сервера предприятия и SQL сервера.
                    </TextOut>
                </Box>
            </Dialog>
        </>
    );
};