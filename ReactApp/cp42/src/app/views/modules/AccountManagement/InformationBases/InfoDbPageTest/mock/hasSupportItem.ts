import { HasSupportInfoDbDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/HasSupportInfoDbDataModel';
import { databaseCardSupportDataMapFrom } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/mappings/databaseCardSupportDataMapFrom';

export const hasSupportItem: HasSupportInfoDbDataModel = databaseCardSupportDataMapFrom({
    DatabaseId: '417df583-8dac-4869-b856-0350885a525e',
    IsConnects: true,
    IsAuthorized: false,
    SupportState: 2,
    SupportStateDescription: 'Авторизация успешно завершена!',
    LastTehSupportDate: '',
    Login: '',
    Password: '',
    CompleteSessioin: false,
    TimeOfUpdate: 14,
    HasAcDbSupport: false,
    HasAutoUpdate: false,
    HasSupport: false,
    HasModifications: false,
    AcDbSupportHistories: [
        {
            Date: '2022-11-07T09:32:23.687',
            Operation: 'Планирование АО',
            Description: "Запланировано обновление базы на вечер '07.11.2022'"
        },
        {
            Date: '2022-11-15T18:22:40.023',
            Operation: 'Планирование ТиИ',
            Description: 'Запланировано проведение ТиИ на вечер 15 ноября 2022 г.'
        },
        {
            Date: '2022-11-07T17:36:07.613',
            Operation: 'Авторизация к ИБ',
            Description: 'Авторизация в информационную базу "Чистый шаблон" (base_11599_85) прошла успешно.'
        }
    ]
});