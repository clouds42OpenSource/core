import { AppRoutes } from 'app/AppRoutes';
import { ServiceLockReasonType } from 'app/common/enums/ServiceLockReasonType';
import { TextOut } from 'app/views/components/TextOut';
import { GetAvailabilityInfoDb } from 'app/web/api/InfoDbProxy/responce-dto/GetAvailabilityInfoDb';
import cn from 'classnames';
import { Link } from 'react-router-dom';
import css from '../../styles.module.css';

const reasonBlock = (availability: GetAvailabilityInfoDb) => {
    if (availability.MainServiceStatusInfo.ServiceLockReasonType === ServiceLockReasonType.ServiceNotPaid) {
        return (
            <TextOut inDiv={ true } className={ cn(css.isNotActive1C) }>
                <b>Внимание!</b> { availability.MainServiceStatusInfo.ServiceLockReason } Пожалуйста,
                <Link to={ AppRoutes.accountManagement.balanceManagement }>&nbsp;пополните баланс</Link>.
            </TextOut>
        );
    }

    if (availability.MainServiceStatusInfo.ServiceLockReasonType === ServiceLockReasonType.OverduePromisedPayment) {
        return (
            <TextOut inDiv={ true } className={ cn(css.isNotActive1C) }>
                <b>Внимание!</b>&nbsp;{ availability.MainServiceStatusInfo.ServiceLockReason }Пожалуйста, погасите обещанный платеж.
            </TextOut>
        );
    }

    if (availability.MainServiceStatusInfo.ServiceLockReasonType === ServiceLockReasonType.noDiskSpace) {
        return (
            <TextOut inDiv={ true } className={ cn(css.isNotActive1C) }>
                <b>Внимание!</b>&nbsp;{ availability.MainServiceStatusInfo.ServiceLockReason }
            </TextOut>
        );
    }

    return null;
};

export const BlockMessageView = (availability: GetAvailabilityInfoDb) => {
    const { MainServiceStatusInfo: { ServiceIsLocked }, PermissionsForWorkingWithDatabase: { HasPermissionForDisplayPayments }, AccountAdminInfo } = availability;

    if (ServiceIsLocked) {
        if (HasPermissionForDisplayPayments) {
            return reasonBlock(availability);
        }

        return (
            <TextOut inDiv={ true } className={ cn(css.isNotActive1C) }>
                На балансе недостаточно средств для работы с сервисами 42Clouds.
                Обратитесь, пожалуйста, к менеджеру вашего аккаунта ({ AccountAdminInfo }).
            </TextOut>
        );
    }

    return null;
};