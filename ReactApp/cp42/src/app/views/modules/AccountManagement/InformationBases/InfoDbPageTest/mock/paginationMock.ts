import { TablePagination } from 'app/views/modules/_common/components/CommonTableWithFilter/TableView';

export const paginationMock: TablePagination = {
    selectIsVisible: false,
    selectCount: ['10', '50', '100'],
    onSelectCount: () => { /* empty */ },
    hideRecordsCount: true,
    totalPages: 1,
    currentPage: 2,
    pageSize: 10,
    recordsCount: 1,
};