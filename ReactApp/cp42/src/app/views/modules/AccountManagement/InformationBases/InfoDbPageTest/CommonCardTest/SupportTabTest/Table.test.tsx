import '@testing-library/jest-dom/extend-expect';
import { render } from '@testing-library/react';
import { partialFillOf } from 'app/common/functions';
import { InfoDbCardConsts } from 'app/modules/infoDbCard/constants';
import { receiveAccessReducer } from 'app/modules/infoDbCard/store/reducers/receiveAccessInfoDbCardReducer';
import { receiveBuckapsReducer } from 'app/modules/infoDbCard/store/reducers/receiveBuckupsReducer';
import { receiveAccessCostReducer } from 'app/modules/infoDbCard/store/reducers/receiveCalculateAccessInfoDbCardReducer';
import { receiveInfoDbCardReducer } from 'app/modules/infoDbCard/store/reducers/receiveDatabaseCardReducer';
import { receiveCommonInfoDbCardReducer } from 'app/modules/infoDbCard/store/reducers/receiveDatabaseCommonCardReducer';
import { receiveHasSupportReducer } from 'app/modules/infoDbCard/store/reducers/receiveHasSupportReducer';
import { receiveRightsInfoDbCardReducer } from 'app/modules/infoDbCard/store/reducers/receiveRightsInfoDbCardReducer';
import { receiveServicesReducer } from 'app/modules/infoDbCard/store/reducers/receiveServicesReducer';
import { receiveStatusServiceReducer } from 'app/modules/infoDbCard/store/reducers/receiveStatusServiceInfoDbCardReducer';
import { AppReduxStoreReducers } from 'app/redux';
import { accessItem } from 'app/views/modules/AccountManagement/InformationBases/InfoDbPageTest/mock/accessItem';
import { buckupsItem } from 'app/views/modules/AccountManagement/InformationBases/InfoDbPageTest/mock/buckupItem';
import { hasSupportItem } from 'app/views/modules/AccountManagement/InformationBases/InfoDbPageTest/mock/hasSupportItem';
import { serviceTableList1 } from 'app/views/modules/AccountManagement/InformationBases/InfoDbPageTest/mock/serviceTableList';
import { HasSupportView } from 'app/views/modules/_common/reusable-modules/InfoDbCard/infoDbCardContent/tabs/HasSupportTabView';
import { InfoDbCardAccountDatabaseInfoDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/InfoDbCardAccountDatabaseInfoDataModel';
import { createReducer, createStore, initReducerState } from 'core/redux/functions';
import dayjs from 'dayjs';
import { SnackbarProvider } from 'notistack';
import { Provider } from 'react-redux';
import { applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';

describe('Arhival table InfoDbTest', () => {
    const store = createStore({
        ...AppReduxStoreReducers,
        InfoDbCardState: createReducer(initReducerState<any>(InfoDbCardConsts.reducerName, {
            commonDetails: partialFillOf<InfoDbCardAccountDatabaseInfoDataModel>(),
            hasSupportInfoDb: hasSupportItem,
            rightsInfoDb: [],
            databaseAccesses: accessItem,
            databaseAccessesCost: [],
            servicesInfoDb: serviceTableList1,
            backupsInfoDb: {
                databaseBackups: buckupsItem().databaseBackups,
                dbBackupsCount: buckupsItem().dbBackupsCount
            },
            hasSuccessFor: {
                hasDatabaseCardReceived: false,
                hasCommonDatabaseCardReceived: false,
                hasHasSupportReceived: false,
                hasServicesReceived: false,
                hasAccessReceived: false,
                hasAccessCostReceived: false,
                hasBackupsReceived: false
            }
        }), [receiveInfoDbCardReducer,
            receiveCommonInfoDbCardReducer,
            receiveRightsInfoDbCardReducer,
            receiveHasSupportReducer,
            receiveAccessReducer,
            receiveAccessCostReducer,
            receiveServicesReducer,
            receiveStatusServiceReducer,
            receiveBuckapsReducer])
    }, composeWithDevTools(applyMiddleware(thunk)));

    it('HasSupport table', async () => {
        const { getByText } = render(
            <Provider store={ store }>
                <SnackbarProvider>
                    <HasSupportView hasSupportInfoDb={ hasSupportItem } />
                </SnackbarProvider>
            </Provider>
        );

        const desc = getByText(hasSupportItem.acDbSupportHistories[0].Description);
        const time = getByText(dayjs(new Date(hasSupportItem.acDbSupportHistories[0].Date)).format('DD.MM.YYYY hh:mm'));
        const operation = getByText(hasSupportItem.acDbSupportHistories[0].Operation);
        expect(desc).toBeInTheDocument();
        expect(time).toBeInTheDocument();
        expect(operation).toBeInTheDocument();
    });
});