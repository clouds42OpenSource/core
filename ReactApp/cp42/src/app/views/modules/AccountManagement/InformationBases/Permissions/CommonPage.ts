import { DatabaseState, DbPublishState } from 'app/common/enums';
import { InfoDbCardPermissionsEnum } from 'app/web/common/enums/InfoDbCardPermissionsEnum';
import { InfoDbListItemDataModel } from 'app/web/InterlayerApiProxy/InfoDbApiProxy/receiveDatabaseList/data-models';

/**
 * Показывать ли чекбоксы удаления баз
 */
export function CheckBoxDeleteDbIsVisible(hasPermissionForMultipleActionsWithDb: boolean) {
    return hasPermissionForMultipleActionsWithDb;
}

/**
 * Показывать ли Веб ссылки баз
 */
export function WebPathDbIsVisible(hasPermissionForWeb: boolean, item: InfoDbListItemDataModel) {
    return hasPermissionForWeb &&
        (item.needShowWebLink || (item.isDbOnDelimiters && item.isDemo)) &&
        (!item.isDbOnDelimiters || (item.isDbOnDelimiters && item.state === DatabaseState.Ready)) &&
        item.publishState === DbPublishState.Published;
}
/**
 * Показывать ли Переключатели ТиИ
 */
export function HasSupportDbIsVisible(item: InfoDbListItemDataModel) {
    return (item.state === DatabaseState.ProcessingSupport || item.state === DatabaseState.Ready) &&
        !item.isDbOnDelimiters && item.hasAcDbSupport;
}

/**
 * Показывать ли Переключатели Автобновления
 */
export function AutoUpdateDbIsVisible(item: InfoDbListItemDataModel) {
    return (item.state === DatabaseState.ProcessingSupport || item.state === DatabaseState.Ready) &&
        !item.isDbOnDelimiters && item.hasAcDbSupport;
}

/**
 * Показывать ли кнопку добавления базы Платформы42
 */
export function CreatePlatform42Base(isVipAccount: boolean, AccountLocaleName: string) {
    return isVipAccount && AccountLocaleName === 'ru-ru';
}

/**
 * Показывать ли Опубликованы веб-сервыисы
 */
export function visibleWebServices(isDbOnDelimiters: boolean, permissions: string[]) {
    return permissions.includes(InfoDbCardPermissionsEnum.AccountDatabases_EditUsedWebServices) &&
        !isDbOnDelimiters;
}

/**
 * Показывать ли база содержит доработки
 */
export function visibleImprovements(hasAcDbSupport: boolean | undefined) {
    return hasAcDbSupport;
}