import '@testing-library/jest-dom/extend-expect';
import { render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { partialFillOf } from 'app/common/functions';
import { InfoDbCardConsts } from 'app/modules/infoDbCard/constants';
import { receiveAccessReducer } from 'app/modules/infoDbCard/store/reducers/receiveAccessInfoDbCardReducer';
import { receiveBuckapsReducer } from 'app/modules/infoDbCard/store/reducers/receiveBuckupsReducer';
import { receiveAccessCostReducer } from 'app/modules/infoDbCard/store/reducers/receiveCalculateAccessInfoDbCardReducer';
import { receiveInfoDbCardReducer } from 'app/modules/infoDbCard/store/reducers/receiveDatabaseCardReducer';
import { receiveCommonInfoDbCardReducer } from 'app/modules/infoDbCard/store/reducers/receiveDatabaseCommonCardReducer';
import { receiveHasSupportReducer } from 'app/modules/infoDbCard/store/reducers/receiveHasSupportReducer';
import { receiveRightsInfoDbCardReducer } from 'app/modules/infoDbCard/store/reducers/receiveRightsInfoDbCardReducer';
import { receiveServicesReducer } from 'app/modules/infoDbCard/store/reducers/receiveServicesReducer';
import { receiveStatusServiceReducer } from 'app/modules/infoDbCard/store/reducers/receiveStatusServiceInfoDbCardReducer';
import { AppReduxStoreReducers } from 'app/redux';
import { createAppReduxStore } from 'app/redux/functions';
import { MockStoreCard } from 'app/views/modules/AccountManagement/InformationBases/InfoDbPageTest/functions/MockStore';
import { accessItem } from 'app/views/modules/AccountManagement/InformationBases/InfoDbPageTest/mock/accessItem';
import { serviceTableList1 } from 'app/views/modules/AccountManagement/InformationBases/InfoDbPageTest/mock/serviceTableList';
import { ServicesTabView } from 'app/views/modules/_common/reusable-modules/InfoDbCard/infoDbCardContent/tabs/ServicesTavView';
import { HasSupportInfoDbDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/HasSupportInfoDbDataModel';
import { InfoDbCardAccountDatabaseInfoDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/InfoDbCardAccountDatabaseInfoDataModel';
import { createReducer, createStore, initReducerState } from 'core/redux/functions';
import { SnackbarProvider } from 'notistack';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import { applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';

describe('ServicesInfoDbTest', () => {
    const storeVoid = createAppReduxStore(composeWithDevTools(applyMiddleware(thunk)));
    const store = createStore({
        ...AppReduxStoreReducers,
        InfoDbCardState: createReducer(initReducerState<any>(InfoDbCardConsts.reducerName, {
            commonDetails: partialFillOf<InfoDbCardAccountDatabaseInfoDataModel>(),
            hasSupportInfoDb: partialFillOf<HasSupportInfoDbDataModel>(),
            rightsInfoDb: [],
            databaseAccesses: accessItem,
            databaseAccessesCost: [],
            servicesInfoDb: serviceTableList1,
            backupsInfoDb: {
                databaseBackups: [],
                dbBackupsCount: 0
            },
            hasSuccessFor: {
                hasDatabaseCardReceived: false,
                hasCommonDatabaseCardReceived: false,
                hasHasSupportReceived: false,
                hasServicesReceived: false,
                hasAccessReceived: false,
                hasAccessCostReceived: false,
                hasBackupsReceived: false
            }
        }), [receiveInfoDbCardReducer,
            receiveCommonInfoDbCardReducer,
            receiveRightsInfoDbCardReducer,
            receiveHasSupportReducer,
            receiveAccessReducer,
            receiveAccessCostReducer,
            receiveServicesReducer,
            receiveStatusServiceReducer,
            receiveBuckapsReducer])
    }, composeWithDevTools(applyMiddleware(thunk)));

    it('Services card render', () => {
        render(
            <Provider store={ store }>
                <SnackbarProvider>
                    <BrowserRouter>
                        <ServicesTabView isOpen={ true } />
                    </BrowserRouter>
                </SnackbarProvider>
            </Provider>
        );
    });
    it('Services card void render', async () => {
        const { queryByText } = render(
            <Provider store={ storeVoid }>
                <SnackbarProvider>
                    <BrowserRouter>
                        <ServicesTabView isOpen={ true } />
                    </BrowserRouter>
                </SnackbarProvider>
            </Provider>
        );
        const voidTable = /нет записей/;
        expect(queryByText(voidTable)).toBeInTheDocument();
    });

    it('Services card Table', async () => {
        const { queryByText } = render(
            <SnackbarProvider>
                <Provider store={ { ...store, ...MockStoreCard({}) } }>
                    <BrowserRouter>
                        <ServicesTabView isOpen={ true } />
                    </BrowserRouter>
                </Provider>
            </SnackbarProvider>
        );
        const name = queryByText(serviceTableList1[0].name);
        expect(name).toBeInTheDocument();
        expect(queryByText(/Краткое описание/)).toBeInTheDocument();
    });
    it('Activate services card Table', async () => {
        const { getByTestId } = render(
            <Provider store={ { ...store, ...MockStoreCard({}) } }>
                <SnackbarProvider>
                    <BrowserRouter>
                        <ServicesTabView isOpen={ true } />
                    </BrowserRouter>
                </SnackbarProvider>
            </Provider>
        );
        const switchC = getByTestId('CustomizedSwitches');
        await userEvent.click(switchC);
        expect(switchC).not.toBeChecked();
    });
});