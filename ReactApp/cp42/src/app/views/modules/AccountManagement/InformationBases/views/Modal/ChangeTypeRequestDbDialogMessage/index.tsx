import { Visibility, VisibilityOff } from '@mui/icons-material';
import { Checkbox, FormControlLabel, IconButton, InputAdornment, Link, OutlinedInput, TextField } from '@mui/material';
import { Box } from '@mui/system';
import { contextAccountId, userId } from 'app/api';
import { Services } from 'app/api/endpoints/managingServiceSubscriptions/request';
import { FETCH_API } from 'app/api/useFetchApi';
import { EMessageType, useAppSelector, useFloatMessages, useGetCurrency } from 'app/hooks';
import { useQuery } from 'app/hooks/useQuery';
import { Dialog } from 'app/views/components/controls/Dialog';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { TextOut } from 'app/views/components/TextOut';
import { stylePassword } from 'app/views/modules/ResetPassword/const/stylePassword';
import { allowedCountries } from 'app/views/modules/ToPartners/config/constants';
import { useFormik } from 'formik';
import { MuiTelInput, MuiTelInputInfo } from 'mui-tel-input';
import React, { useState } from 'react';
import * as yup from 'yup';
import { AccessTable } from './views/AccessTable';

type OwnProps = {
    databaseId: string;
    isFile?: boolean;
    isOpen: boolean;
    databaseName: string;
    onNoClick: () => void;
    setIsEnabled: React.Dispatch<React.SetStateAction<boolean>>;
};

const changeTypeDbSchema = yup.object().shape({
    phone: yup.string().min(6, 'Неверный формат номера телефона').max(18, 'Неверный формат номера телефона').required(),
    login: yup.string(),
    password: yup.string(),
    localeName: yup.string().required(),
    isCopy: yup.boolean(),
});

const getDefaultCountry = (countryCode: string) => {
    switch (countryCode) {
        case 'ru-kz':
            return 'KZ';
        case 'ru-ua':
            return 'UA';
        default:
            return 'RU';
    }
};

const { fromFileToCluster } = FETCH_API.ACCOUNT_DATABASES;
const { putServicesApply } = FETCH_API.MANAGING_SERVICE_SUBSCRIPTIONS;

export const ChangeTypeDbDialogMessage = ({ databaseId, isOpen, onNoClick, isFile, databaseName, setIsEnabled }: OwnProps) => {
    const { AccountAdminPhoneNumber } = useAppSelector(state => state.InfoDbListState.AvailabilityInfoDb);
    const { clientServerAccessCost, rateData: { costPerOneLicense } } = useAppSelector(state => state.InfoDbCardState);
    const { AvailabilityInfoDb } = useAppSelector(state => state.InfoDbListState);

    const currency = useGetCurrency();
    const params = useQuery();
    const { show } = useFloatMessages();

    const [isLoading, setIsLoading] = useState(false);

    const [showPassword, setShowPassword] = useState(false);
    const [showTable, setShowTable] = useState(false);
    const [showWarningDialog, setShowWarningDialog] = useState(false);

    const [notLoginAndPassword, setNotLoginAndPassword] = useState(false);
    const [totalSum, setTotalSum] = useState(0);
    const [changedUsers, setChangedUsers] = useState<Services[]>([]);

    const setInitialValue = {
        phone: AccountAdminPhoneNumber,
        login: '',
        password: '',
        localeName: new URLSearchParams(window.location.search).get('LocaleName') || (window.location.origin.split('.').pop() === 'pro' ? 'ru-ua' : 'ru-ru'),
        isCopy: false,
    };

    const formik = useFormik({
        initialValues: setInitialValue,
        validationSchema: changeTypeDbSchema,
        onSubmit: () => showTableOrApply(),
        validateOnBlur: true,
        validateOnChange: true,
    });

    const setFormToInitialState = () => {
        setShowTable(false);
        setIsLoading(false);
        setNotLoginAndPassword(false);
        formik.setValues(setInitialValue);
        formik.setErrors({
            password: undefined,
            phone: undefined,
            login: undefined,
            localeName: undefined
        });
    };

    const changeTypeInfoDb = async () => {
        setIsLoading(true);
        setIsEnabled(false);

        const { success, message } = await fromFileToCluster({
            accountDatabaseId: databaseId,
            accountUserId: userId(),
            accountId: contextAccountId(),
            login: formik.values.login,
            password: formik.values.password,
            phoneNumber: formik.values.phone,
            isCopy: formik.values.isCopy,
        });

        if (success) {
            onNoClick();
            const { data } = await putServicesApply({
                accountId: contextAccountId(),
                changedUsers,
                billingServiceId: AvailabilityInfoDb.MyDatabaseServiceId,
                usePromisePayment: false
            });

            if (data?.isComplete) {
                show(EMessageType.success, `Заявка на перевод базы "${ databaseName }" в ${ !isFile ? 'файловый' : 'серверный' } режим успешно оставлена`);
                setFormToInitialState();

                if (!isFile) {
                    setShowWarningDialog(true);
                }
            } else {
                show(EMessageType.error, `Не удалось произвести платеж. На балансе недостаточно средств в размере - ${ data?.notEnoughMoney } ${ data?.currency }`);
                setIsLoading(false);
            }
        } else {
            setIsLoading(false);
            setShowTable(false);
            show(EMessageType.error, `Произошла ошибка при оформлении заявка на перевод базы "${ databaseName }" в ${ !isFile ? 'файловый' : 'серверный' } режим: ${ message }`);
        }
    };

    const showTableOrApply = () => {
        if (isFile && !showTable) {
            setShowTable(true);
        } else {
            void changeTypeInfoDb();
        }
    };

    const phoneNumberHandlerBlur = () => {
        formik.setFieldTouched('phoneNumber', true, true);
    };

    const phoneNumberHandler = (value: string, { countryCode }: MuiTelInputInfo) => {
        let validCountryCode;

        switch (countryCode) {
            case 'KZ':
                validCountryCode = 'ru-kz';
                break;
            case 'UA':
                validCountryCode = 'ru-ua';
                break;
            default:
                validCountryCode = 'ru-ru';
                break;
        }

        formik.setFieldValue('localeName', validCountryCode);
        formik.setFieldValue('phone', value, true);
    };

    return (
        <>
            { isLoading && <LoadingBounce /> }
            <Dialog
                isOpen={ isOpen }
                dialogWidth={ showTable ? 'md' : 'sm' }
                dialogVerticalAlign="center"
                onCancelClick={ () => {
                    setFormToInitialState();
                    onNoClick();
                } }
                title={ `Перевод базы в ${ !isFile ? 'файловый' : 'серверный' } режим` }
                buttons={ [
                    {
                        onClick: () => {
                            setFormToInitialState();
                            onNoClick();
                        },
                        variant: 'text',
                        kind: 'default',
                        content: 'Отмена'
                    },
                    {
                        onClick: formik.submitForm,
                        variant: 'contained',
                        kind: 'success',
                        content: showTable ? `Продолжить${ totalSum > 0 ? ` (${ totalSum } ${ currency }.)` : '' }` : 'Продолжить',
                        isEnabled: (formik.values.login !== '' || notLoginAndPassword) &&
                            (formik.values.password !== '' || notLoginAndPassword) &&
                            formik.values.phone !== ''
                    }
                ] }
            >
                {
                    !showTable
                        ? (
                            <Box display="flex" flexDirection="column" gap="5px">
                                { isFile && (
                                    <>
                                        <TextOut>
                                            Клиент-серверный режим рекомендуется для повышения производительности для баз с объемом свыше 5 Гб или количеством пользователей 5 и более.
                                        </TextOut>
                                        <TextOut>
                                            - Стоимость размещения 1 серверной базы - <b>{ costPerOneLicense } { currency }/мес</b>
                                        </TextOut>
                                        <TextOut>
                                            - Стоимость лицензии на 1 пользователя - <b>{ clientServerAccessCost } { currency }/мес</b> (дополнительно к Web или Стандарт)
                                        </TextOut>
                                    </>
                                ) }
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            id="isCopy"
                                            checked={ formik.values.isCopy }
                                            onChange={ ev => formik.setFieldValue('isCopy', ev.target.checked) }
                                            size="small"
                                            sx={ { padding: 0 } }
                                        />
                                    }
                                    label={ <TextOut>Копия существующей базы</TextOut> }
                                    sx={ { width: 'fit-content', display: 'flex', gap: '10px', ml: 0 } }
                                />
                                <FormAndLabel label="Номер телефона">
                                    <MuiTelInput
                                        fullWidth={ true }
                                        name="phone"
                                        autoComplete="off"
                                        onChange={ (val, details) => phoneNumberHandler(val, details) }
                                        onBlur={ phoneNumberHandlerBlur }
                                        value={ formik.values.phone }
                                        preferredCountries={ allowedCountries }
                                        defaultCountry={ getDefaultCountry(params.get('LocaleName') ?? '') }
                                        langOfCountryName="RU"
                                        error={ formik.touched.phone && !!formik.errors.phone }
                                    />
                                </FormAndLabel>
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            checked={ notLoginAndPassword }
                                            onChange={ event => setNotLoginAndPassword(event.target.checked) }
                                            size="small"
                                            sx={ { padding: 0 } }
                                        />
                                    }
                                    label={ <TextOut>Нет пользователя</TextOut> }
                                    sx={ { width: 'fit-content', display: 'flex', gap: '10px', ml: 0, mt: '5px' } }
                                />
                                { !notLoginAndPassword && (
                                    <>
                                        <FormAndLabel label="Логин">
                                            <TextField
                                                fullWidth={ true }
                                                id="login"
                                                value={ formik.values.login }
                                                onChange={ formik.handleChange }
                                                error={ formik.touched.login && !!formik.errors.login }
                                                InputLabelProps={ { shrink: true } }
                                            />
                                        </FormAndLabel>
                                        <FormAndLabel label="Пароль">
                                            <OutlinedInput
                                                id="password"
                                                value={ formik.values.password }
                                                onChange={ formik.handleChange }
                                                autoComplete="off"
                                                required={ true }
                                                notched={ true }
                                                fullWidth={ true }
                                                endAdornment={
                                                    <InputAdornment position="end">
                                                        <IconButton
                                                            aria-label="toggle password visibility"
                                                            onClick={ () => setShowPassword(showIcon => !showIcon) }
                                                            onMouseDown={ (event: React.MouseEvent<HTMLButtonElement>) => event.preventDefault() }
                                                            edge="end"
                                                        >
                                                            { showPassword ? <VisibilityOff /> : <Visibility /> }
                                                        </IconButton>
                                                    </InputAdornment>
                                                }
                                                sx={ !showPassword ? stylePassword : undefined }
                                                error={ formik.touched.password && !!formik.errors.password }

                                            />
                                        </FormAndLabel>
                                        <TextOut>
                                            Для перевода в клиент-серверный режим необходимо предоставить данные пользователя базы &quot;<b>{ databaseName }</b>&quot; с правами администратора базы 1С.&nbsp;
                                            <Link target="_blank" rel="noreferrer" sx={ { fontWeight: 700 } } href="https://42clouds.com/ru-ru/manuals/rent-1c-users.html">
                                                Инструкция
                                            </Link>
                                        </TextOut>
                                    </>
                                ) }
                            </Box>
                        )
                        : (
                            <AccessTable
                                setTotalSum={ setTotalSum }
                                setChangedUsers={ setChangedUsers }
                            />
                        )
                }
            </Dialog>
            <Dialog
                isOpen={ showWarningDialog }
                dialogWidth="sm"
                dialogVerticalAlign="center"
                title="Внимание!"
                hiddenX={ true }
                buttons={ [
                    {
                        onClick: () => setShowWarningDialog(false),
                        variant: 'contained',
                        kind: 'success',
                        content: 'Продолжить',
                    }
                ] }
            >
                <Box display="flex" flexDirection="column" gap="16px" alignItems="center">
                    <TextOut>
                        В связи с переводом базы данных с серверной на файловую версию, <b>важно</b> перепроверить настройки доступа в разделе&nbsp;
                        <Link
                            href={ `${ window.location.origin }/my-database-services/fba5f480-fbfa-4ad9-b468-9fa7d6c43524` }
                            target="_blank"
                            rel="noreferrer"
                        >
                            &#34;Клиент-серверный режим&#34;
                        </Link>.&nbsp;
                        В случае отсутствия необходимости в таком режиме, <b>рекомендуется его отключить</b>, чтобы избежать <b>дальнейших платежей</b> за использование клиент-серверных возможностей.
                    </TextOut>
                </Box>
            </Dialog>
        </>
    );
};