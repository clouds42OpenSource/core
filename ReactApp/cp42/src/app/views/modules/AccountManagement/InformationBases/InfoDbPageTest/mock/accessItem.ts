import { databaseCardAccessDataMapFrom } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/mappings/databaseCardAccessDataMapFrom';

export const accessItem = databaseCardAccessDataMapFrom({
    Currency: '',
    ClientServerAccessCost: 0,
    RateData: {
        SystemServiceType: 0,
        VolumeInQuantity: 0,
        ServiceTypeName: '',
        ServiceTypeId: '',
        ServiceTypeDescription: '',
        ServiceName: '',
        LimitOnFreeLicenses: 0,
        IsActiveService: false,
        CostPerOneLicense: 0,
        Clouds42ServiceType: 0,
        BillingType: 0,
        ServiceId: '',
        TotalAmount: 0
    },
    DatabaseAccesses: [
        {
            IsExternalAccess: false,
            State: 0,
            DelayReason: null,
            UserId: '3278e599-928e-42a3-82b6-16af11d23011',
            UserLogin: 'Vlad2.dev',
            UserLastName: null,
            UserFirstName: null,
            UserMiddleName: 'testMiddleName',
            UserEmail: 'vladess97@mail.ru',
            AccountIndexNumber: 11599,
            AccountCaption: 'Core42 Develop',
            UserFullName: 'testUserFullName',
            AccountInfo: 'аккаунт 11599 “Core42 Develop”',
            HasAccess: true
        }
    ]
}).databaseAccesses;