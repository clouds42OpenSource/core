import React from 'react';
import style from '../styles.module.css';

type OwnProps = {
    hours: number;
    minutes: number;
    seconds: number;
    hasInfoDbListStatusReceived: boolean;
};

type OwnState = {
    h: number,
    m: number,
    s: number,
    over: boolean
};

export class CountDownTimerViews extends React.Component<OwnProps, OwnState> {
    private readonly _timerID: () => NodeJS.Timeout | number;

    public constructor(props: OwnProps) {
        super(props);
        this.state = {
            h: 0,
            m: 0,
            s: 0,
            over: false
        };
        this._timerID = () => setInterval(() => this.tick(), 1000);
    }

    public componentDidMount() {
        this.setState(
            {
                h: this.props.hours,
                m: this.props.minutes,
                s: this.props.seconds,
                over: false
            },
            () => {
                this._timerID();
            }
        );
    }

    public componentDidUpdate(prevProps: Readonly<OwnProps>): void {
        if (this.props.hasInfoDbListStatusReceived && !prevProps.hasInfoDbListStatusReceived) {
            this.setState({
                h: this.props.hours,
                m: this.props.minutes,
                s: this.props.seconds,
                over: false
            });
        }
    }

    public componentWillUnmount(): void {
        clearInterval(this._timerID());
    }

    private setTime(h: number, m: number, s: number) {
        this.setState(prevState => ({
            ...prevState,
            h,
            m,
            s,
        }));
    }

    private tick() {
        if (this.state.over) {
            clearInterval(this._timerID());
            return;
        }
        if (this.state.h === 0 && this.state.m === 0 && this.state.s === 0) {
            this.setState({ over: true });
        } else if (this.state.m === 0 && this.state.s === 0) {
            this.setTime(this.state.h - 1, 59, 59);
        } else if (this.state.s === 0) {
            this.setTime(this.state.h, this.state.m - 1, 59);
        } else {
            this.setTime(this.state.h, this.state.m, this.state.s - 1);
        }
    }

    public render() {
        return (
            <span className={ style.timer }>
                {
                    `${ this.state.h.toString().padStart(2, '0') }:
                  ${ this.state.m.toString().padStart(2, '0') }:
                  ${ this.state.s.toString().padStart(2, '0') }`
                }
            </span>
        );
    }
}