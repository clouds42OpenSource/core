import { InfoDbCardAccountDatabaseInfoDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/InfoDbCardAccountDatabaseInfoDataModel';
import { infoDbCardModelMapFrom } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/mappings/databaseCardAccountDatabaseInfoDataModelMapFrom';

export const itemInfoDb = (): InfoDbCardAccountDatabaseInfoDataModel | null => {
    return infoDbCardModelMapFrom({
        Id: '89a8d776-0989-4d68-a988-6ea2b8f96f01',
        V82Name: '42_11599_84',
        Caption: 'Комплексная автоматизация 2.4',
        DbNumber: 84,
        DatabaseState: 2,
        IsFile: false,
        DbTemplate: '76c5b4f9-f8f8-4c24-bb00-9c0e5eba1c1c',
        TemplateName: 'Комплексная автоматизация 2.4',
        ConfigurationName: '',
        CanEditDatabase: false,
        TemplatePlatform: 2,
        DistributionType: 0,
        PlatformType: 2,
        Stable82Version: '8.2.19.130',
        Alpha83Version: '8.3.17.1549',
        Stable83Version: '8.3.20.1674',
        V82Server: '1c-linux',
        SqlServer: 'adl-msdb-ha',
        FilePath: '',
        CanWebPublish: true,
        ConfigurationCode: '',
        PublishState: 0,
        UsedWebServices: false,
        FileStorageId: '',
        RestoreModelType: 0,
        CanChangeRestoreModel: false,
        FileStorageName: '',
        Version: 0,
        IsDbOnDelimiters: false,
        IsDemoDelimiters: false,
        ZoneNumber: 25949,
        DatabaseConnectionPath: 'Srvr=\'1c-linux\';Ref=\'42_11599_84\'',
        AvailableFileStorages: [
            {
                key: '2d378f71-0786-4f76-83e7-02e574f3657d',
                value: '01 Первое хранилище'
            },
            {
                key: 'c7c54b64-9c0c-4a97-ab2e-ce9dde8c6f71',
                value: '02 Второе хранилище'
            },
            {
                key: '2e98cb68-940a-448b-b247-a9e69f45159a',
                value: '04 Четвертое хранилище'
            },
            {
                key: '7a888f96-4682-48dd-af21-6bcd37a83089',
                value: '03 Третье хранилище'
            }
        ],
        CreationDate: '2022-11-01T15:49:47.6011651',
        MyDatabasesServiceTypeId: '006cbd71-cb4e-414e-bbc8-a277b683fffb',
        ActualDatabaseBackupId: '',
        IsExistTerminatingSessionsProcessInDatabase: false,
        IsStatusErrorCreated: false,
        SizeInMb: 0,
        CloudStorageWebLink: '',
        WebPublishPath: 'https://beta-ka.42clouds.com/ka_base1/25949',
        AccountCaption: 'Core42 Develop',
        LastEditedDateTime: '2022-11-01T15:52:14.5378384',
        BackupDate: '',
        CalculateSizeDateTime: '',
        ArchivePath: '',
        DatabaseOperations: [
            {
                Cost: 50,
                Currency: 'руб',
                Id: '89a8d776-0989-4d68-a988-6ea2b8f96f01',
                Name: 'Создание архивной базы'
            },
            {
                Cost: 50,
                Currency: 'руб',
                Id: '89a8d776-0989-4d68-a988-6ea2b8f96f01',
                Name: 'Создание архивной базы'
            }
        ],
        CommonDataForWorkingWithDB: {
            AvailableDatabaseStatuses: [
                {
                    Key: 1,
                    Value: 'База в процессе создания'
                },

            ],
            AvailablePlatformTypes: [
                {
                    Key: 1,
                    Value: 'Платформа 8.2'
                },
                {
                    Key: 2,
                    Value: 'Платформа 8.3'
                }
            ],
            AvailableAccountDatabaseRestoreModelTypes: [
                {
                    Key: 0,
                    Value: 'Простая'
                },
                {
                    Key: 1,
                    Value: 'Полная'
                }
            ],
            AvailableDbTemplates: [
                {
                    Key: '76c5b4f9',
                    Value: '1C:Документооборот 8 ПРОФ'
                },
                {
                    Key: '76c5b4f9-f8f8-4c24-bb00-9c0e5eba1c1c',
                    Value: 'Комплексная автоматизация 2.4'
                },

            ],
            ServiceExtensionsForDatabaseTypes: [
                {
                    Key: 0,
                    Value: 'Все сервисы'
                },
                {
                    Key: 1,
                    Value: 'Подключенные'
                },
                {
                    Key: 2,
                    Value: 'Отключенные'
                },
                {
                    Key: 3,
                    Value: 'Доступные'
                }
            ]
        }
    });
};