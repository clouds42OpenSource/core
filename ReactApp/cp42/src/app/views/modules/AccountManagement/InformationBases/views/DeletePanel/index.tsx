import { getErrorMessage } from 'app/common/functions/getErrorMessage';
import { NumberUtility } from 'app/utils';
import { TextOut } from 'app/views/components/TextOut';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { ContainedButton, OutlinedButton } from 'app/views/components/controls/Button';
import { Dialog } from 'app/views/components/controls/Dialog';
import { CountBasesType } from 'app/views/modules/AccountManagement/InformationBases/views/TableDb';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import cn from 'classnames';
import { RequestKind } from 'core/requestSender/enums';
import React from 'react';
import css from '../styles.module.css';

type OwnProps = FloatMessageProps & {
    onDataSelect: () => void;
    valueCheck: string[];
    allSizeInMb: number;
    serverBases: CountBasesType;
    fileBases: CountBasesType;
};

type OwnState = {
    showDeleteDbModal: boolean;
};

export class DeletePanelClass extends React.Component<OwnProps, OwnState> {
    public constructor(props: OwnProps) {
        super(props);
        this.state = {
            showDeleteDbModal: false,
        };
        this.renderDeleteInfoDbView = this.renderDeleteInfoDbView.bind(this);
        this.toggleDeleteDbModal = this.toggleDeleteDbModal.bind(this);
    }

    private toggleDeleteDbModal() {
        this.setState(prevState => {
            return {
                showDeleteDbModal: !prevState.showDeleteDbModal
            };
        });
    }

    private renderDeleteInfoDbView() {
        return (
            <Dialog
                title="Удаление баз"
                dialogVerticalAlign="center"
                isOpen={ this.state.showDeleteDbModal }
                isTitleSmall={ true }
                dialogWidth="xs"
                onCancelClick={ this.toggleDeleteDbModal }
            >
                <div className={ cn(css['warning-icon-container'], css.pulseWarning) }>
                    <span className={ cn(css['warning-icon-circle'], css.pulseWarningIns) } />
                    <span className={ cn(css['warning-icon-dot'], css.pulseWarningIns) } />
                </div>
                <p style={ { textAlign: 'center' } }>Выбранные базы будут перемещены в корзину и недоступны для работы. Скачать резервную копию возможно в течение 30 дней.</p>
                <div style={ { textAlign: 'center', display: 'flex', justifyContent: 'center', flexDirection: 'row', gap: '5px' } }>
                    <ContainedButton kind="default" onClick={ this.toggleDeleteDbModal }>Нет</ContainedButton>
                    <ContainedButton
                        kind="error"
                        onClick={ async () => {
                            this.toggleDeleteDbModal();
                            const api = InterlayerApiProxy.getInfoDbApi();
                            try {
                                await api.deleteInfoDbList(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, { databasesId: this.props.valueCheck });
                                this.props.onDataSelect();
                            } catch (e) {
                                this.props.floatMessage.show(MessageType.Error, getErrorMessage(e));
                            }
                        } }
                    >Да
                    </ContainedButton>
                </div>
            </Dialog>
        );
    }

    public render() {
        return (
            <>
                <TextOut style={ { marginRight: '40px' } } inDiv={ false } fontSize={ 13 } fontWeight={ 600 }>
                    Выбрано баз: { this.props.valueCheck.length }, размер: { NumberUtility.megabytesToClassicFormat('0', this.props.allSizeInMb) }
                </TextOut>
                <TextOut style={ { marginRight: '40px' } } inDiv={ false } fontSize={ 13 } fontWeight={ 600 }>
                    Серверные: { this.props.serverBases.count }, размер: { NumberUtility.megabytesToClassicFormat('0', this.props.serverBases.size) }
                </TextOut>
                <TextOut inDiv={ false } fontSize={ 13 } fontWeight={ 600 }>
                    Файловые: { this.props.fileBases.count }, размер: { NumberUtility.megabytesToClassicFormat('0', this.props.fileBases.size) }
                </TextOut>
                <div className={ cn(css['container-delete-btn']) }>
                    <OutlinedButton
                        className={ cn(css['button-delete-outline']) }
                        onClick={ this.toggleDeleteDbModal }
                        style={ { borderRadius: '4px 0px 0px 4px' } }
                        kind="error"
                    >
                        Удалить
                    </OutlinedButton>
                </div>
                { this.renderDeleteInfoDbView() }
            </>
        );
    }
}

export const DeletePanelViews = withFloatMessages(DeletePanelClass);