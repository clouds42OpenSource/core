/**
 * Модель Redux формы для поиска во вкладке Настройки доступа
 */
export type InfoDbAccessFilterDataForm = {
    /**
     * Строка поиска пользвователя
     */
    searchLineUser: string;
    /**
     * Строка поиска внешнего пользвователя
     */
    searchUser: string;
};