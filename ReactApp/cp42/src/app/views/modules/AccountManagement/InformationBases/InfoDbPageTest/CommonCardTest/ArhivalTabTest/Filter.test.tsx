import '@testing-library/jest-dom/extend-expect';
import { render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { partialFillOf } from 'app/common/functions';
import { InfoDbCardConsts } from 'app/modules/infoDbCard/constants';
import { receiveAccessReducer } from 'app/modules/infoDbCard/store/reducers/receiveAccessInfoDbCardReducer';
import { receiveBuckapsReducer } from 'app/modules/infoDbCard/store/reducers/receiveBuckupsReducer';
import { receiveAccessCostReducer } from 'app/modules/infoDbCard/store/reducers/receiveCalculateAccessInfoDbCardReducer';
import { receiveInfoDbCardReducer } from 'app/modules/infoDbCard/store/reducers/receiveDatabaseCardReducer';
import { receiveCommonInfoDbCardReducer } from 'app/modules/infoDbCard/store/reducers/receiveDatabaseCommonCardReducer';
import { receiveHasSupportReducer } from 'app/modules/infoDbCard/store/reducers/receiveHasSupportReducer';
import { receiveRightsInfoDbCardReducer } from 'app/modules/infoDbCard/store/reducers/receiveRightsInfoDbCardReducer';
import { receiveServicesReducer } from 'app/modules/infoDbCard/store/reducers/receiveServicesReducer';
import { receiveStatusServiceReducer } from 'app/modules/infoDbCard/store/reducers/receiveStatusServiceInfoDbCardReducer';
import { AppReduxStoreReducers } from 'app/redux';
import { accessItem } from 'app/views/modules/AccountManagement/InformationBases/InfoDbPageTest/mock/accessItem';
import { buckupsItem } from 'app/views/modules/AccountManagement/InformationBases/InfoDbPageTest/mock/buckupItem';
import { itemInfoDb } from 'app/views/modules/AccountManagement/InformationBases/InfoDbPageTest/mock/itemInfoDb';
import { serviceTableList1 } from 'app/views/modules/AccountManagement/InformationBases/InfoDbPageTest/mock/serviceTableList';
import { ArchivalCopiesView } from 'app/views/modules/_common/reusable-modules/InfoDbCard/infoDbCardContent/tabs/ArchivalСopiesTabView';
import { HasSupportInfoDbDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/HasSupportInfoDbDataModel';
import { createReducer, createStore, initReducerState } from 'core/redux/functions';
import { SnackbarProvider } from 'notistack';
import { Provider } from 'react-redux';
import { applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';

describe('Archival filter InfoDbTest', () => {
    const store = createStore({
        ...AppReduxStoreReducers,
        InfoDbCardState: createReducer(initReducerState<any>(InfoDbCardConsts.reducerName, {
            commonDetails: itemInfoDb(),
            hasSupportInfoDb: partialFillOf<HasSupportInfoDbDataModel>(),
            rightsInfoDb: [],
            databaseAccesses: accessItem,
            databaseAccessesCost: [],
            servicesInfoDb: serviceTableList1,
            backupsInfoDb: {
                databaseBackups: buckupsItem().databaseBackups,
                dbBackupsCount: buckupsItem().dbBackupsCount
            },
            hasSuccessFor: {
                hasDatabaseCardReceived: true,
                hasCommonDatabaseCardReceived: true,
                hasHasSupportReceived: true,
                hasServicesReceived: true,
                hasAccessReceived: true,
                hasAccessCostReceived: true,
                hasBackupsReceived: true
            }
        }), [receiveInfoDbCardReducer,
            receiveCommonInfoDbCardReducer,
            receiveRightsInfoDbCardReducer,
            receiveHasSupportReducer,
            receiveAccessReducer,
            receiveAccessCostReducer,
            receiveServicesReducer,
            receiveStatusServiceReducer,
            receiveBuckapsReducer])
    }, composeWithDevTools(applyMiddleware(thunk)));

    it('Archival filter render', async () => {
        const { getAllByText } = render(
            <Provider store={ store }>
                <SnackbarProvider>
                    <ArchivalCopiesView applicationId="" status={ true } accountGroup={ false } state={ 2 } />
                </SnackbarProvider>
            </Provider>
        );
        const timeSearch = getAllByText(/Период поиска/)[0];
        expect(timeSearch).toBeInTheDocument();
    });

    it('Archival filter date', async () => {
        const { getAllByRole, getAllByText } = render(
            <Provider store={ store }>
                <SnackbarProvider>
                    <ArchivalCopiesView applicationId="" status={ true } accountGroup={ false } state={ 2 } />
                </SnackbarProvider>
            </Provider>
        );
        const chooseDate = getAllByRole('button', { name: 'Choose date' });
        const timeSearch = getAllByText(/Период поиска/)[0];
        await userEvent.click(timeSearch!);
        expect(getAllByRole('button', { name: 'Choose date' }));
        chooseDate.forEach(i => {
            expect(i).toBeInTheDocument();
        });
    });
});