import { AppRoutes } from 'app/AppRoutes';
import { Dialog } from 'app/views/components/controls/Dialog';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import cn from 'classnames';
import React from 'react';
import { withRouter } from 'react-router';
import { RouteComponentProps } from 'react-router-dom';
import css from './styles.module.css';

type OwnProps = RouteComponentProps & {
    isOpen: boolean;
    onNoClick: () => void;
    onYesClick: () => void;
    notEnoughMoney: number;
    usePromise: boolean;
    canUsePromisePayment: boolean;
    loading: boolean
};

class BuyAccessDbDialogMessageClass extends React.Component<OwnProps> {
    public constructor(props: OwnProps) {
        super(props);

        this.redirectToPay = this.redirectToPay.bind(this);
    }

    private redirectToPay() {
        this.props.history.push(`${ AppRoutes.accountManagement.replenishBalance }?customPayment.amount=${ this.props.notEnoughMoney }&customPayment.description=Оплата+за+предоставление+доступов`);
    }

    public render() {
        return (
            <>
                { this.props.loading && <LoadingBounce /> }
                <Dialog
                    isOpen={ this.props.isOpen }
                    dialogWidth="sm"
                    dialogVerticalAlign="center"
                    onCancelClick={ this.props.onNoClick }
                    buttons={ [
                        {
                            onClick: this.props.onNoClick,
                            variant: 'contained',
                            kind: 'default',
                            content: 'Отмена',
                        },
                        {
                            onClick: this.props.onYesClick,
                            variant: 'contained',
                            kind: 'primary',
                            content: 'Взять обещанный платёж',
                            hiddenButton: !this.props.canUsePromisePayment
                        },
                        {
                            onClick: this.redirectToPay,
                            variant: 'contained',
                            kind: 'success',
                            content: 'Оплатить сейчас',
                        }
                    ] }
                >
                    <div className={ cn(css['warning-icon-container'], css.pulseWarning) }>
                        <span className={ cn(css['warning-icon-circle'], css.pulseWarningIns) } />
                        <span className={ cn(css['warning-icon-dot'], css.pulseWarningIns) } />
                    </div>
                    <h2 className={ cn(css['sweet-alert-title']) }>
                        Внимание
                    </h2>
                    {
                        this.props.usePromise
                            ? (
                                <p className={ cn(css['sweet-alert-body']) }>
                                    Уважаемый пользователь, для оплаты доступов у Вас
                                    недостаточно средств в размере <b>{ this.props.notEnoughMoney.toFixed(2) }</b> руб. {
                                        this.props.canUsePromisePayment
                                            ? (
                                                <p>Вы можете
                                                    воспользоваться услугой <b>&quot;Обещанный платеж&quot;</b>, Вы
                                                    должны будете погасить задолжность в течение 7 дней.
                                                </p>
                                            )
                                            : ''
                                    }
                                </p>
                            )
                            : (
                                <p className={ cn(css['sweet-alert-body']) }>
                                    Уважаемый пользователь, для оплаты доступов у Вас
                                    недостаточно средств в размере { this.props.notEnoughMoney.toFixed(2) } руб. Пожалуйста, пополните баланс
                                </p>
                            )
                    }
                </Dialog>
            </>
        );
    }
}

export const BuyAccessDbDialogMessage = withRouter(BuyAccessDbDialogMessageClass);