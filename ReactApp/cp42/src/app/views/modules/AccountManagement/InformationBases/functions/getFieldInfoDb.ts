import { AccountInfoDbListFilterDataForm } from 'app/views/modules/AccountManagement/InformationBases/types';
import { SelectInfoDbGetQueryParams } from 'app/web/common';

const compareField = {
    template: {
        key: 'template',
        value: 'TemplateCaption'
    },
    web: {
        key: 'webpublishpath',
        value: 'DatabaseLaunchLink'
    }
};

/**
 * Получить поле по которому сортируется таблица
 */
export function getFieldInfoDb(args?: SelectInfoDbGetQueryParams<AccountInfoDbListFilterDataForm>) {
    if (args?.sortingData?.fieldName) {
        switch (args.sortingData.fieldName.toLowerCase()) {
            case compareField.template.key:
                return compareField.template.value;
            case compareField.web.key:
                return compareField.web.value;
            default:
                return args.sortingData.fieldName[0].toUpperCase() + args.sortingData.fieldName.slice(1);
        }
    }

    return '';
}