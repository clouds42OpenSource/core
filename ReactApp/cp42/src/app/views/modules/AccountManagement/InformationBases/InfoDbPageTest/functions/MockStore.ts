import '@testing-library/jest-dom/extend-expect';
import { getInitialPagination, partialFillOf } from 'app/common/functions';
import { InfoDbCardConsts } from 'app/modules/infoDbCard/constants';
import { receiveAccessReducer } from 'app/modules/infoDbCard/store/reducers/receiveAccessInfoDbCardReducer';
import { receiveBuckapsReducer } from 'app/modules/infoDbCard/store/reducers/receiveBuckupsReducer';
import { receiveAccessCostReducer } from 'app/modules/infoDbCard/store/reducers/receiveCalculateAccessInfoDbCardReducer';
import { receiveInfoDbCardReducer } from 'app/modules/infoDbCard/store/reducers/receiveDatabaseCardReducer';
import { receiveCommonInfoDbCardReducer } from 'app/modules/infoDbCard/store/reducers/receiveDatabaseCommonCardReducer';
import { receiveHasSupportReducer } from 'app/modules/infoDbCard/store/reducers/receiveHasSupportReducer';
import { receiveRightsInfoDbCardReducer } from 'app/modules/infoDbCard/store/reducers/receiveRightsInfoDbCardReducer';
import { receiveServicesReducer } from 'app/modules/infoDbCard/store/reducers/receiveServicesReducer';
import { receiveStatusServiceReducer } from 'app/modules/infoDbCard/store/reducers/receiveStatusServiceInfoDbCardReducer';
import { InfoDbListConsts } from 'app/modules/infoDbList/constants';
import { availabilityInfoDbListReducer } from 'app/modules/infoDbList/store/reducers/availabilityInfoDbListReducer';
import { deleteInfoDbReducer } from 'app/modules/infoDbList/store/reducers/deleteDbTemplateReducer';
import { editInfoDbReducer } from 'app/modules/infoDbList/store/reducers/editInfoDbReducer';
import { receiveInfoDbListReducer } from 'app/modules/infoDbList/store/reducers/receiveInfoDbListReducer';
import { statusInfoDbListReducer } from 'app/modules/infoDbList/store/reducers/receiveStatusesInfoDbListReducer';
import { AppReduxStoreReducers } from 'app/redux';
import { accessItem } from 'app/views/modules/AccountManagement/InformationBases/InfoDbPageTest/mock/accessItem';
import { InfoDbListTest1 } from 'app/views/modules/AccountManagement/InformationBases/InfoDbPageTest/mock/infoDbListTest';
import { serviceTableList1 } from 'app/views/modules/AccountManagement/InformationBases/InfoDbPageTest/mock/serviceTableList';
import { HasSupportInfoDbDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/HasSupportInfoDbDataModel';
import { InfoDbCardAccountDatabaseInfoDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/InfoDbCardAccountDatabaseInfoDataModel';
import { createReducer, createStore, initReducerState } from 'core/redux/functions';
import { applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';

export function MockStoreCard(object: object) {
    return createStore({
        ...AppReduxStoreReducers,
        InfoDbCardState: createReducer(initReducerState<any>(InfoDbCardConsts.reducerName, {
            commonDetails: partialFillOf<InfoDbCardAccountDatabaseInfoDataModel>(),
            hasSupportInfoDb: partialFillOf<HasSupportInfoDbDataModel>(),
            rightsInfoDb: [],
            databaseAccesses: accessItem,
            databaseAccessesCost: [],
            servicesInfoDb: serviceTableList1,
            backupsInfoDb: {
                databaseBackups: [],
                dbBackupsCount: 0
            },
            hasSuccessFor: {
                hasDatabaseCardReceived: true,
                hasCommonDatabaseCardReceived: true,
                hasHasSupportReceived: true,
                hasServicesReceived: true,
                hasAccessReceived: true,
                hasAccessCostReceived: true,
                hasBackupsReceived: true
            },
            ...object
        }), [receiveInfoDbCardReducer,
            receiveCommonInfoDbCardReducer,
            receiveRightsInfoDbCardReducer,
            receiveHasSupportReducer,
            receiveAccessReducer,
            receiveAccessCostReducer,
            receiveServicesReducer,
            receiveStatusServiceReducer,
            receiveBuckapsReducer])
    }, composeWithDevTools(applyMiddleware(thunk)));
}
export function MockStoreList(object: object) {
    return createStore({
        ...AppReduxStoreReducers,
        InfoDbListState: createReducer(initReducerState<any>(
            InfoDbListConsts.reducerName,
            {
                statusInfoDbList: [],
                AvailabilityInfoDb: {
                    IsVipAccount: false,
                    IsMainServiceAllowed: false,
                    AccessToServerDatabaseServiceTypeId: '',
                    AccountAdminInfo: '',
                    AccountId: '',
                    AccountLocaleName: '',
                    AccountUserId: '',
                    MyDatabaseServiceId: '',
                    MainServiceStatusInfo: {
                        PromisedPaymentExpireDate: null,
                        PromisedPaymentIsActive: false,
                        PromisedPaymentSum: null,
                        ServiceIsLocked: false,
                        ServiceLockReason: null,
                        ServiceLockReasonType: null
                    },
                    AllowAddingDatabaseInfo: {
                        ForbiddeningReasonMessage: '',
                        IsAllowedCreateDb: false,
                        SurchargeForCreation: 0
                    },
                    PermissionsForWorkingWithDatabase: {
                        HasPermissionForCreateAccountDatabase: true,
                        HasPermissionForDisplayAutoUpdateButton: true,
                        HasPermissionForDisplayPayments: true,
                        HasPermissionForDisplaySupportData: true,
                        HasPermissionForMultipleActionsWithDb: true,
                        HasPermissionForRdp: true,
                        HasPermissionForWeb: true,
                    }
                },
                infoDbList: {
                    records: InfoDbListTest1,
                    pagination: getInitialPagination(),
                    AllDatabasesCount: 0,
                    ServerDatabasesCount: 0,
                    ArchievedDatabasesCount: 0,

                },
                databasesId: [],
                databaseId: '',
                HasSupport: false,
                HasAutoUpdate: false,
                caption: '',
                errorMessage: null,
                hasSuccessFor: {
                    hasInfoDbListReceived: true,
                    hasAvailabilityDbListReceived: true,
                },
                ...object
            }
        ), [
            receiveInfoDbListReducer,
            deleteInfoDbReducer,
            editInfoDbReducer,
            statusInfoDbListReducer,
            availabilityInfoDbListReducer
        ])
    }, composeWithDevTools(applyMiddleware(thunk)));
}