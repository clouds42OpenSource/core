/**
 * Модель Redux формы для редактирования фильтра для таблицы поиска во вкладке Сервисы
 */
export type InfoDbServicesFilterDataForm = {
    /**
     * Строка фильтра
     */
    searchServices: string;

};