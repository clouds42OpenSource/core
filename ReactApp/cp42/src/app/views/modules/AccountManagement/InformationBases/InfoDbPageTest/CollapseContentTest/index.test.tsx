import '@testing-library/jest-dom/extend-expect';
import { fireEvent, render, screen } from '@testing-library/react';
import { AppRoutes } from 'app/AppRoutes';
import { availabilityTest } from 'app/views/modules/AccountManagement/InformationBases/InfoDbPageTest/mock/availability';
import { CollapseContentViews } from 'app/views/modules/AccountManagement/InformationBases/views/CollapseContent';
import { BrowserRouter } from 'react-router-dom';

describe('CollapseContentTest', () => {
    const textAccondeon = /Вы можете загрузить свою конфигурацию с данными либо создать новую базу на основе одной из типовых конфигураций 1С./;
    const block1CText = /Сервис 'Аренда 1С' заблокирован./;
    const blockReason0 = /пополните баланс/;
    const blockPromisePay = /Пожалуйста, погасите обещанный платеж./;
    const blockSayManager = /Обратитесь, пожалуйста, к менеджеру вашего аккаунта/;
    const RdpText = /Скачайте ярлык подключения к 1С/;

    it('the presence of an accordion with IsAllowedCreateDb', () => {
        const { getByText } = render(
            <CollapseContentViews
                availability={ {
                    ...availabilityTest,
                    AllowAddingDatabaseInfo: {
                        ...availabilityTest.AllowAddingDatabaseInfo, IsAllowedCreateDb: true
                    }
                } }
            />
        );
        expect(getByText(textAccondeon)).toBeInTheDocument();
    });

    it('the presence of an accordion with hasPermissionForRdp', () => {
        const { queryByText } = render(
            <BrowserRouter>
                <CollapseContentViews
                    availability={ {
                        ...availabilityTest,
                        PermissionsForWorkingWithDatabase: {
                            ...availabilityTest.PermissionsForWorkingWithDatabase,
                            HasPermissionForRdp: true
                        }
                    } }
                />
            </BrowserRouter>
        );
        expect(queryByText(RdpText)).toBeInTheDocument();
    });
    it('the presence of an accordion with NOT active aredna 1С block reason 0', () => {
        const { getByText, queryByText, } = render(
            <BrowserRouter>
                <CollapseContentViews
                    availability={ {
                        ...availabilityTest,
                        MainServiceStatusInfo: {
                            ...availabilityTest.MainServiceStatusInfo,
                            ServiceIsLocked: true,
                            ServiceLockReasonType: 0
                        },
                        PermissionsForWorkingWithDatabase: {

                            ...availabilityTest.PermissionsForWorkingWithDatabase,

                            HasPermissionForDisplayPayments: true
                        }
                    } }
                />
            </BrowserRouter>
        );
        expect(getByText(block1CText)).toBeInTheDocument();
        expect(getByText(blockReason0)).toBeInTheDocument();
        expect(queryByText(blockPromisePay)).toBeNull();
        expect(queryByText(blockSayManager)).toBeNull();
    });
    it('the presence of an accordion with NOT active aredna 1С block reason 1', () => {
        const { getByText, queryByText, } = render(
            <BrowserRouter>
                <CollapseContentViews
                    availability={ {
                        ...availabilityTest,
                        MainServiceStatusInfo: {
                            ...availabilityTest.MainServiceStatusInfo,
                            ServiceIsLocked: true,
                            ServiceLockReasonType: 1
                        },
                        PermissionsForWorkingWithDatabase: {

                            ...availabilityTest.PermissionsForWorkingWithDatabase,

                            HasPermissionForDisplayPayments: true
                        }
                    } }
                />
            </BrowserRouter>
        );
        expect(getByText(block1CText)).toBeInTheDocument();
        expect(getByText(blockPromisePay)).toBeInTheDocument();
        expect(queryByText(blockReason0)).toBeNull();
        expect(queryByText(blockSayManager)).toBeNull();
    });
    it('the presence of an accordion with NOT active aredna 1С with not right', () => {
        const { queryByText, } = render(
            <BrowserRouter>
                <CollapseContentViews
                    availability={ {
                        ...availabilityTest,
                        MainServiceStatusInfo: {
                            ...availabilityTest.MainServiceStatusInfo,
                            ServiceIsLocked: true,
                            ServiceLockReasonType: 1
                        },
                        PermissionsForWorkingWithDatabase: {
                            ...availabilityTest.PermissionsForWorkingWithDatabase,
                            HasPermissionForDisplayPayments: false
                        }
                    } }
                />
            </BrowserRouter>
        );
        expect(queryByText(block1CText)).toBeNull();
        expect(queryByText(blockPromisePay)).toBeNull();
        expect(queryByText(blockReason0)).toBeNull();
        expect(queryByText(blockSayManager)).toBeInTheDocument();
    });

    it('Click on the download button', async () => {
        render(
            <BrowserRouter>
                <CollapseContentViews
                    availability={ {
                        ...availabilityTest,
                        PermissionsForWorkingWithDatabase: {
                            ...availabilityTest.PermissionsForWorkingWithDatabase,
                            HasPermissionForRdp: true
                        }
                    } }
                />
            </BrowserRouter>
        );

        const windowsDownload = screen.getByTestId('winDownload');
        expect(windowsDownload).toBeInTheDocument();
        expect(windowsDownload.getAttribute('href')).toEqual(AppRoutes.link42Downloader);
        fireEvent.click(windowsDownload);
    });
});