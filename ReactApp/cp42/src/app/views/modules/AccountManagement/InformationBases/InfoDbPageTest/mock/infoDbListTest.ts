import { DatabaseState } from 'app/common/enums';
import { InfoDbListItemDataModel } from 'app/web/InterlayerApiProxy/InfoDbApiProxy/receiveDatabaseList/data-models';
import { infoDbListItemDataModelMapFrom } from 'app/web/InterlayerApiProxy/InfoDbApiProxy/receiveDatabaseList/mappings/infoDbListItemDataModelMapFrom';

export const infoDbList = (): InfoDbListItemDataModel[] => {
    return [infoDbListItemDataModelMapFrom(
        {
            CreateAccountDatabaseComment: null,
            Id: '5ba4f2ef-3760-49ef-8ed5-9a9f48a11226',
            Name: 'Управление нашей фирмой 1.6( (base_11599_80)',
            TemplateCaption: 'Управление нашей фирмой 1.6',
            TemplateImageName: 'unf15',
            SizeInMb: 1968,
            LastActivityDate: '2022-11-01T15:12:48.5042414',
            DatabaseLaunchLink: 'path',
            IsFile: true,
            State: 1,
            PublishState: 3,
            NeedShowWebLink: false,
            IsExistSessionTerminationProcess: false,
            IsDemo: false,
            IsDbOnDelimiters: false,
            ConfigurationName: 'Управление нашей фирмой 1.6',
            HasSupport: false,
            HasAcDbSupport: false,
            HasAutoUpdate: false
        }
    )];
};

export const InfoDbListTest = infoDbList();

export const InfoDbListTest1: InfoDbListItemDataModel[] = [
    {
        createAccountDatabaseComment: null,
        databaseId: '5ba4f2ef-3760-49ef-8ed5-9a9f48a11226',
        name: 'Управление нашей фирмой 2.0( (base_11599_81)',
        templateCaption: 'Управление нашей фирмой 1.6',
        templateImageName: 'unf15',
        sizeInMb: 1968,
        lastActivityDate: new Date(),
        webPublishPath: '',
        icon: '',
        template: '',
        isFile: true,
        isDbOnDelimiters: false,
        createStatus: {
            id: '',
            errorCode: '',
            timeout: 0,
            state: '',
            error: ''
        },
        state: DatabaseState.Ready,
        publishState: 3,
        needShowWebLink: false,
        isExistSessionTerminationProcess: false,
        isDemo: false,
        configurationName: 'Управление нашей фирмой 1.6',
        hasSupport: false,
        hasAcDbSupport: false,
        hasAutoUpdate: false
    }
];

export const InfoDbListTestOther1: InfoDbListItemDataModel[] = [
    {
        createAccountDatabaseComment: null,
        databaseId: '5ba4f2ef-3760-49ef-8ed5-9a9f48a11225',
        name: 'Управление нашей фирмой 1.5( (base_11599_8`)',
        templateCaption: 'Управление нашей фирмой 1.6',
        templateImageName: 'unf15',
        sizeInMb: 1968,
        lastActivityDate: new Date(),
        webPublishPath: '',
        icon: '',
        template: '',
        isFile: true,
        isDbOnDelimiters: false,
        createStatus: {
            id: '',
            errorCode: '',
            timeout: 0,
            state: '',
            error: ''
        },
        state: 1,
        publishState: 3,
        needShowWebLink: false,
        isExistSessionTerminationProcess: false,
        isDemo: false,
        configurationName: 'Управление нашей фирмой 1.6',
        hasSupport: false,
        hasAcDbSupport: false,
        hasAutoUpdate: false
    }
];