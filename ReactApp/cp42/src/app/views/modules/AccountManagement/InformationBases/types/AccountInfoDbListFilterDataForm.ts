/**
 * Модель Redux формы для редактирования фильтра для таблицы списка информационных баз
 */
export type AccountInfoDbListFilterDataForm = {
    /**
     * Тип базы
     */
    type: string;
    /**
     * Строка фильтра
     */
    searchLine: string;
    /**
     * Номер страницы
     */
    pageNumber: number,
    /**
     * Размер страницы
     */
    pageSize?: number,
    /**
     * Размер страницы
     */
    size: number,
    /**
     * своя/доступная база
     */
    affiliation: number,
    /**
     * Поле сортировки
     */
    field: string,
    /**
     * Тип сортировки
     */
    sort: number,
};