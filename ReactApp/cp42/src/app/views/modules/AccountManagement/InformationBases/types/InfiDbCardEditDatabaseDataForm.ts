import { DatabaseState, DistributionType, PlatformType } from 'app/common/enums';

/**
 * Модель Redux формы для редактирования данных базы данных
 */
export type InfoDbCardEditDatabaseDataForm = {
    /**
     * ID базы
     */
    databaseId: string;
    /**
     * Номер базы
     */
    v82Name: string;
    /**
     * Наименование базы
     */
    databaseCaption: string;
    /**
     * Шаблон базы
     */
    databaseTemplateId: string;
    /**
     * Тип платформы
     */
    platformType: PlatformType;
    /**
     * Дистрибуция, версия
     */
    distributionType: DistributionType;
    /**
     * Маркер о том что опубликованы веб сервисы
     */
    usedWebServices: boolean;
    /**
     * Состояние базы
     */
    databaseState: DatabaseState;
    /**
     * Модель восстановления
     */
    restoreModel: number
};