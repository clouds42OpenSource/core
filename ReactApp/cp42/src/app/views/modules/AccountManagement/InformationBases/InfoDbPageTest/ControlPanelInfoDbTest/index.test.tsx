import '@testing-library/jest-dom/extend-expect';
import { render } from '@testing-library/react';
import { availabilityTest } from 'app/views/modules/AccountManagement/InformationBases/InfoDbPageTest/mock/availability';
import { countBase } from 'app/views/modules/AccountManagement/InformationBases/InfoDbPageTest/mock/countBase';
import { paginationMock } from 'app/views/modules/AccountManagement/InformationBases/InfoDbPageTest/mock/paginationMock';
import { ControlPanelInfoDbViews } from 'app/views/modules/AccountManagement/InformationBases/views/ControlPanelInfoDb';

describe('ControlPanelInfoDbTest', () => {
    let onDataSelect: jest.Mock;

    beforeEach(() => {
        onDataSelect = jest.fn();
    });

    it('Create Db button visible, IsAllowedCreateDb = false', () => {
        const { queryByText, } = render(
            <ControlPanelInfoDbViews
                availability={ availabilityTest }
                onDataSelect={ onDataSelect }
                onDataSelectOther={ onDataSelect }
                ref={ undefined }
                countBase={ countBase }
                pagination={ paginationMock }
                paginationOther={ paginationMock }
                currentUserGroups={ [2, 6] }
                tabTypeBases={ 0 }
            />
        );
        expect(queryByText(/Добавить базу/)).toBeNull();
    });

    it('Create Db button visible', () => {
        const { queryByText, } = render(
            <ControlPanelInfoDbViews
                availability={ {
                    ...availabilityTest,
                    IsMainServiceAllowed: true,

                    AllowAddingDatabaseInfo: {

                        ForbiddeningReasonMessage: '',

                        IsAllowedCreateDb: true,

                        SurchargeForCreation: 0
                    },
                    PermissionsForWorkingWithDatabase: {

                        ...availabilityTest.PermissionsForWorkingWithDatabase,

                        HasPermissionForCreateAccountDatabase: true
                    }
                } }
                onDataSelect={ onDataSelect }
                onDataSelectOther={ onDataSelect }
                ref={ undefined }
                countBase={ countBase }
                pagination={ paginationMock }
                paginationOther={ paginationMock }
                currentUserGroups={ [2, 6] }
                tabTypeBases={ 0 }
            />
        );
        expect(queryByText(/Добавить базу/)).toBeInTheDocument();
    });

    it('Create Db button hidden', () => {
        const { queryByText, } = render(
            <ControlPanelInfoDbViews
                availability={ {
                    ...availabilityTest,
                    AllowAddingDatabaseInfo: {
                        ForbiddeningReasonMessage: '',
                        IsAllowedCreateDb: false,
                        SurchargeForCreation: 0
                    },
                    PermissionsForWorkingWithDatabase: {
                        ...availabilityTest.PermissionsForWorkingWithDatabase,
                        HasPermissionForCreateAccountDatabase: false
                    }
                } }
                onDataSelect={ onDataSelect }
                onDataSelectOther={ onDataSelect }
                ref={ undefined }
                countBase={ countBase }
                pagination={ paginationMock }
                paginationOther={ paginationMock }
                currentUserGroups={ [2, 6] }
                tabTypeBases={ 0 }
            />
        );
        expect(queryByText(/Добавить базу/)).toBeNull();
    });

    it('Test count base in Button', () => {
        const { getByText } = render(
            <ControlPanelInfoDbViews
                availability={ {
                    ...availabilityTest,
                    AllowAddingDatabaseInfo: {
                        ForbiddeningReasonMessage: '',

                        IsAllowedCreateDb: true,

                        SurchargeForCreation: 0
                    }
                } }
                onDataSelect={ onDataSelect }
                onDataSelectOther={ onDataSelect }
                ref={ undefined }
                countBase={ countBase }
                pagination={ paginationMock }
                paginationOther={ paginationMock }
                currentUserGroups={ [2, 6] }
                tabTypeBases={ 0 }
            />
        );
        const AllBases = getByText(/Все базы/);
        const ServerBases = getByText(/Серверные/);
        const ArhivalBases = getByText(/В архиве/);

        expect(AllBases).toHaveTextContent(countBase.AllDatabasesCount.toString());
        expect(ServerBases).toHaveTextContent(countBase.ServerDatabasesCount.toString());
        expect(ArhivalBases).toHaveTextContent(countBase.ArchievedDatabasesCount.toString());
    });

    it('Visible Search Form', () => {
        const { getByRole, getByPlaceholderText } = render(
            <ControlPanelInfoDbViews
                availability={ {
                    ...availabilityTest,
                    AllowAddingDatabaseInfo: {
                        ForbiddeningReasonMessage: '',

                        IsAllowedCreateDb: true,

                        SurchargeForCreation: 0
                    }
                } }
                onDataSelect={ onDataSelect }
                onDataSelectOther={ onDataSelect }
                ref={ undefined }
                countBase={ countBase }
                pagination={ paginationMock }
                paginationOther={ paginationMock }
                currentUserGroups={ [2, 6] }
                tabTypeBases={ 0 }
            />
        );
        const input = getByPlaceholderText(/Поиск базы по названию, шаблону, номеру/);
        const searchBtn = getByRole('button', { name: 'Поиск' });
        expect(input).toBeInTheDocument();
        expect(searchBtn).toBeInTheDocument();
    });
});