import '@testing-library/jest-dom/extend-expect';
import { render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { partialFillOf } from 'app/common/functions';
import { createAppReduxStore } from 'app/redux/functions/createAppReduxStore';
import { availabilityTest } from 'app/views/modules/AccountManagement/InformationBases/InfoDbPageTest/mock/availability';
import { InfoDbListTest1 } from 'app/views/modules/AccountManagement/InformationBases/InfoDbPageTest/mock/infoDbListTest';
import { itemInfoDb } from 'app/views/modules/AccountManagement/InformationBases/InfoDbPageTest/mock/itemInfoDb';
import { InfoDbCardEditDatabaseDataForm } from 'app/views/modules/AccountManagement/InformationBases/types/InfiDbCardEditDatabaseDataForm';
import { FieldsCommonCardView } from 'app/views/modules/_common/reusable-modules/InfoDbCard/infoDbCardContent/tabs/CommonTabView/FieldsCommonCardView';
import { Provider } from 'react-redux';
import { applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';

describe('PlatformDestributionInfoDbTest', () => {
    const onValueChangeWebServices: jest.Mock = jest.fn();

    const onChangeField: jest.Mock = jest.fn();

    const onValueChange: jest.Mock = jest.fn();

    const onChangeV82Name: jest.Mock = jest.fn();

    const reduxForm = partialFillOf<InfoDbCardEditDatabaseDataForm>();

    const store = createAppReduxStore(composeWithDevTools(applyMiddleware(thunk)));

    it('render readonly distribution type InfoDb', async () => {
        const { queryByText } = render(
            <Provider store={ { ...store } }>
                <FieldsCommonCardView
                    availability={ availabilityTest }
                    commonDetails={ itemInfoDb()! }
                    onChangeField={ onChangeField }
                    item={ InfoDbListTest1[0] }
                    onChangeV82Name={ onChangeV82Name }
                    permissions={ ['AccountDatabases_EditPlatform'] }
                    reduxForm={ reduxForm }
                    onValueChange={ onValueChange }
                    onValueChangeWebServices={ onValueChangeWebServices }
                    errorV82Name=""
                    changeWebServices={ true }
                    isOther={ true }
                    currentUserGroups={ [] }
                />
            </Provider>
        );
        const platformDestribution = itemInfoDb()!.stable83Version;
        expect(queryByText(platformDestribution)).toBeInTheDocument();
    });
    it('render readonly distribution type InfoDb', async () => {
        const { queryByText, getByText } = render(
            <Provider store={ { ...store } }>
                <FieldsCommonCardView
                    availability={ availabilityTest }
                    commonDetails={ itemInfoDb()! }
                    onChangeField={ onChangeField }
                    item={ InfoDbListTest1[0] }
                    onChangeV82Name={ onChangeV82Name }
                    permissions={ ['AccountDatabases_EditPlatform'] }
                    reduxForm={ reduxForm }
                    onValueChange={ onValueChange }
                    onValueChangeWebServices={ onValueChangeWebServices }
                    errorV82Name=""
                    changeWebServices={ true }
                    isOther={ true }
                    currentUserGroups={ [] }
                />
            </Provider>
        );
        const platformDestribution = itemInfoDb()!.stable83Version;
        const platformDestributionAlfa = itemInfoDb()!.alpha83Version;
        await userEvent.click(getByText(platformDestribution));
        expect(queryByText(platformDestribution)).toBeInTheDocument();
        expect(queryByText(platformDestributionAlfa)).not.toBeInTheDocument();
    });

    it('render with change distribution type InfoDb', async () => {
        const { queryByText, getByText } = render(
            <Provider store={ { ...store } }>
                <FieldsCommonCardView
                    availability={ availabilityTest }
                    commonDetails={ { ...itemInfoDb()!, isFile: true } }
                    onChangeField={ onChangeField }
                    item={ InfoDbListTest1[0] }
                    onChangeV82Name={ onChangeV82Name }
                    permissions={ ['AccountDatabases_EditPlatform'] }
                    reduxForm={ reduxForm }
                    onValueChange={ onValueChange }
                    onValueChangeWebServices={ onValueChangeWebServices }
                    errorV82Name=""
                    changeWebServices={ true }
                    isOther={ true }
                    currentUserGroups={ [] }
                />
            </Provider>
        );
        const platformDestribution = itemInfoDb()!.stable83Version;
        const platformDestributionAlfa = itemInfoDb()!.alpha83Version;
        await userEvent.click(getByText(platformDestribution));
        expect(queryByText(platformDestributionAlfa)).not.toBeInTheDocument();
    });
});