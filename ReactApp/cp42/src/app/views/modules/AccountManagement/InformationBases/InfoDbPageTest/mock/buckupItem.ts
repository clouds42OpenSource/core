import { BuckupInfoDbDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/BuckupInfoDbDataModel';
import { databaseBuckupsInfoDbDataMapFrom } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/mappings/databaseBuckupsInfoDbDataMapFrom';

export const buckupsItem = (): BuckupInfoDbDataModel => {
    return databaseBuckupsInfoDbDataMapFrom({
        DatabaseBackups: [{
            Id: '180ccdd9-4b99-4641-b4f6-6686b8e9f778',
            Initiator: 'Platform42',
            CreationDate: '2022-06-25T02:19:54',
            BackupPath: '\\\\u290893.your-storagebox.de\\backup\\Beta\\Realms\\25615\\20220625\\v8_E9ED_6ad.zip',
            EventTrigger: 6,
            EventTriggerDescription: 'Регламентное резервное копирование',
            IsDbOnDelimiters: true,
            SourceType: 0
        }],
        DbBackupsCount: 1
    });
};