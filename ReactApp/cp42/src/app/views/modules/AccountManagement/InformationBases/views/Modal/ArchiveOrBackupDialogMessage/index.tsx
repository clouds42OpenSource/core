import { Box } from '@mui/material';
import { DeleteType, RequestType } from 'app/api/endpoints/eternalBackup/request';
import { FETCH_API } from 'app/api/useFetchApi';
import { AppRoutes } from 'app/AppRoutes';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { Dialog } from 'app/views/components/controls/Dialog';
import CustomizedSwitches from 'app/views/components/controls/Switch/views/SwitchViews';
import { TextOut } from 'app/views/components/TextOut';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { RequestKind } from 'core/requestSender/enums';
import React from 'react';
import { withRouter } from 'react-router';
import { RouteComponentProps } from 'react-router-dom';

type OwnProps = {
    isOpen: boolean;
    completeStatus: boolean;
    canUsePromisePayment: boolean;
    canIncreasePromisePayment: boolean;
    cost: number;
    currency: string;
    notEnoughMoney: number;
    databaseId: string;
    serviceId: string;
    isDelimiters: boolean;
    closeDialog: () => void;
    switchType: (type: number) => void;
    enableButton: () => void;
} & FloatMessageProps & RouteComponentProps;

type OwnState = {
    archiveStatus: boolean;
    backupStatus: boolean;
};

class ArchiveOrBackupDialogMessageClass extends React.Component<OwnProps, OwnState> {
    constructor(props: OwnProps) {
        super(props);

        this.state = {
            archiveStatus: false,
            backupStatus: true
        };

        this.onValueChangeArch = this.onValueChangeArch.bind(this);
        this.onValueChangeBackup = this.onValueChangeBackup.bind(this);
        this.createArchive = this.createArchive.bind(this);
        this.createBackup = this.createBackup.bind(this);
        this.archiveBuy = this.archiveBuy.bind(this);
        this.confirmationClick = this.confirmationClick.bind(this);
        this.redirectToBilling = this.redirectToBilling.bind(this);
    }

    public componentDidUpdate(prevProps: Readonly<OwnProps>): void {
        if (prevProps.isOpen !== this.props.isOpen) {
            this.setState({
                archiveStatus: false,
                backupStatus: true
            });
        }
    }

    private onValueChangeArch() {
        this.setState(prevState => ({
            archiveStatus: !prevState.archiveStatus,
            backupStatus: false
        }));
        this.props.switchType(0);

        if (this.state.archiveStatus) {
            this.setState({ backupStatus: true });
            this.props.switchType(1);
        }
    }

    private onValueChangeBackup() {
        this.setState(prevState => ({
            archiveStatus: false,
            backupStatus: !prevState.backupStatus
        }));
        this.props.switchType(1);

        if (this.state.backupStatus) {
            this.setState({ archiveStatus: true });
            this.props.switchType(0);
        }
    }

    private redirectToBilling() {
        this.props.history.push(`${ AppRoutes.accountManagement.replenishBalance }?customPayment.amount=${ this.props.notEnoughMoney }&customPayment.description=Оплата+за+перенос+ИБ+в+архив`);
    }

    private async createBackup(isPromise: boolean) {
        const responsePay = await FETCH_API.ETERNAL_BACKUPS.putPay({
            usePromisePayment: isPromise,
            paymentSum: this.props.cost,
            serviceId: this.props.serviceId
        });

        if (responsePay.success && responsePay.data?.complete) {
            FETCH_API.ETERNAL_BACKUPS.postBackups({
                databaseId: this.props.databaseId,
                force: true,
                type: RequestType.eternal,
                payment: {
                    serviseId: this.props.serviceId,
                    sum: this.props.cost,
                    usePromise: isPromise
                }
            }).then(response => {
                return response.message && this.props.floatMessage.show(MessageType.Error, response.message);
            });
        } else {
            return this.props.floatMessage.show(MessageType.Error, responsePay.message);
        }

        this.props.closeDialog();
        this.props.enableButton();
    }

    private async createArchive(isPromise: boolean) {
        const responsePay = await FETCH_API.ETERNAL_BACKUPS.putPay({
            usePromisePayment: isPromise,
            paymentSum: this.props.cost,
            serviceId: this.props.serviceId
        });

        if (responsePay.success && responsePay.data?.complete) {
            FETCH_API.ETERNAL_BACKUPS.deleteApplication({
                databaseId: this.props.databaseId,
                type: DeleteType.tomb,
                paymentServiceId: this.props.serviceId,
                paymentSum: this.props.cost,
                paymentUsePromise: isPromise
            }).then(response => {
                return response.message && this.props.floatMessage.show(MessageType.Error, response.message);
            });
        } else {
            return this.props.floatMessage.show(MessageType.Error, responsePay.message);
        }

        this.props.closeDialog();
        this.props.enableButton();
    }

    private archiveBuy(isPromisePayment: boolean) {
        const api = InterlayerApiProxy.getInfoDbCardApi();

        try {
            void api.ArchiveOrBackup(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, {
                cost: this.props.cost,
                currency: this.props.currency,
                databaseId: this.props.databaseId,
                operation: this.state.archiveStatus ? 1 : 2,
                serviceId: this.props.serviceId,
                usePromisePayment: isPromisePayment
            });
            this.props.closeDialog();
            this.props.enableButton();
        } catch (er: unknown) {
            this.props.floatMessage.show(MessageType.Error, `При создании ${ this.state.archiveStatus ? 'архива' : 'бэкапа' } произошла ошибка`);
        }
    }

    private confirmationClick(isPromise: boolean) {
        if (this.props.isDelimiters) {
            return this.state.archiveStatus ? this.createArchive(isPromise) : this.createBackup(isPromise);
        }
        return this.archiveBuy(isPromise);
    }

    public render() {
        return (
            <Dialog
                dialogWidth="md"
                isOpen={ this.props.isOpen }
                onCancelClick={ this.props.closeDialog }
                dialogVerticalAlign="center"
                buttons={
                    this.props.completeStatus
                        ? [
                            {
                                kind: 'default',
                                variant: 'contained',
                                content: 'Отмена',
                                onClick: this.props.closeDialog
                            },
                            {
                                kind: 'success',
                                variant: 'contained',
                                content: `Применить (${ this.props.cost } ${ this.props.currency })`,
                                onClick: () => this.confirmationClick(false)
                            }
                        ]
                        : this.props.canUsePromisePayment
                            ? [
                                {
                                    kind: 'default',
                                    variant: 'contained',
                                    content: 'Отмена',
                                    onClick: this.props.closeDialog
                                },
                                {
                                    kind: 'primary',
                                    variant: 'contained',
                                    content: `Пополнить счет на (${ this.props.notEnoughMoney } ${ this.props.currency })`,
                                    onClick: this.redirectToBilling
                                },
                                {
                                    kind: 'primary',
                                    variant: 'contained',
                                    content: 'Обещанный платеж',
                                    onClick: () => this.confirmationClick(true)
                                }
                            ]
                            : this.props.canIncreasePromisePayment
                                ? [
                                    {
                                        kind: 'default',
                                        variant: 'contained',
                                        content: 'Отмена',
                                        onClick: this.props.closeDialog
                                    },
                                    {
                                        kind: 'primary',
                                        variant: 'contained',
                                        content: `Пополнить счет на (${ this.props.notEnoughMoney } ${ this.props.currency })`,
                                        onClick: this.redirectToBilling
                                    },
                                    {
                                        kind: 'primary',
                                        variant: 'contained',
                                        content: 'Увеличить обещанный платеж',
                                        onClick: () => this.props.history.push(`${ AppRoutes.accountManagement.balanceManagement }?prepayment=true`)
                                    }
                                ]
                                : [
                                    {
                                        kind: 'default',
                                        variant: 'contained',
                                        content: 'Отмена',
                                        onClick: this.props.closeDialog
                                    },
                                    {
                                        kind: 'primary',
                                        variant: 'contained',
                                        content: `Пополнить счет на (${ this.props.notEnoughMoney } ${ this.props.currency })`,
                                        onClick: this.redirectToBilling
                                    }
                                ]
                }
            >
                <Box display="flex" flexDirection="column" gap="16px">
                    <Box display="flex" flexDirection="row" alignItems="center">
                        <TextOut>
                            Система произведет <b>резервное копирование базы</b> и данных в течение <TextOut fontWeight={ 700 }>30 минут</TextOut>.
                            После создания копии она отобразится на вкладке архивные копии.
                            Вы сможете в любое время восстановить базу к состоянию на момент создания копии.
                            Создание копии - платная услуга.
                        </TextOut>
                        <CustomizedSwitches
                            checked={ this.state.backupStatus }
                            onChange={ this.onValueChangeBackup }
                        />
                    </Box>
                    <Box display="flex" flexDirection="row" alignItems="center">
                        <TextOut>
                            С целью экономии занимаемого дискового пространства, Вы можете перенести информационную базу в архив.
                            <b> После переноса базы в архив она будет удалена</b>, исключена из биллинга занимаемого дискового пространства и из биллинга лицензирования количества информационных баз.
                            Вы можете в любое время восстановить базу из архива.
                            Время восстановления базы зависит от размера базы.
                            Архивация неиспользуемой базы - платная услуга.
                        </TextOut>
                        <CustomizedSwitches
                            checked={ this.state.archiveStatus }
                            onChange={ this.onValueChangeArch }
                        />
                    </Box>
                </Box>
            </Dialog>
        );
    }
}

export const ArchiveOrBackupDialogMessage = withFloatMessages(withRouter(ArchiveOrBackupDialogMessageClass));