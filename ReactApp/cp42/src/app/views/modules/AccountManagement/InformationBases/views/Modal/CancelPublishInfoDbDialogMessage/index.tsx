import { hasComponentChangesFor } from 'app/common/functions';
import { ContainedButton } from 'app/views/components/controls/Button';
import { DialogMessage } from 'app/views/components/controls/DialogMessage';
import cn from 'classnames';
import React from 'react';
import css from './styles.module.css';

type OwnProps = {
    isOpen: boolean;
    onNoClick: () => void;
    onYesClick: () => void;
};

export class CancelPublishInfoDbDialogMessage extends React.Component<OwnProps> {
    public shouldComponentUpdate(nextProps: OwnProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    public render() {
        return (
            <DialogMessage
                isOpen={ this.props.isOpen }
                dialogWidth="sm"
                onCancelClick={ this.props.onNoClick }
            >
                <div className={ cn(css['warning-icon-container'], css.pulseWarning) }>
                    <span className={ cn(css['warning-icon-circle'], css.pulseWarningIns) } />
                    <span className={ cn(css['warning-icon-dot'], css.pulseWarningIns) } />
                </div>
                <h2 className={ cn(css['sweet-alert-title']) }>
                    Подтверждение отмены публикации
                </h2>

                <p className={ cn(css['sweet-alert-body']) }>
                    Уважаемый пользователь.<br />
                    В информационной базе опубликованы веб сервисы.
                    После отмены публикации они будут не доступны.
                    Для повторной публикации веб сервисов обратитесь в техподдержку.
                </p>
                <div style={ { textAlign: 'center', margin: '26px 0' } }>
                    <ContainedButton
                        kind="default"
                        className={ cn(css['sweet-alert-button']) }
                        onClick={ this.props.onNoClick }
                    >
                        Нет
                    </ContainedButton>
                    <ContainedButton
                        kind="error"
                        className={ cn(css['sweet-alert-button'], css['sweet-alert-button-yes']) }
                        onClick={ this.props.onYesClick }
                    >
                        Да
                    </ContainedButton>
                </div>
            </DialogMessage>
        );
    }
}