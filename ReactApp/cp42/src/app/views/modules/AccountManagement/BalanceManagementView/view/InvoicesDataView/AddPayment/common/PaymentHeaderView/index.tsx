import { FETCH_API } from 'app/api/useFetchApi';
import { GetInvoiceCalculatorParams } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/GetInvoiceCalculatorReducer/params';
import { GetInvoiceCalculatorThunk } from 'app/modules/AccountManagement/BalanceManagement/store/thunks/GetInvoiceCalculatorThunk';
import { AppReduxStoreState } from 'app/redux/types';
import { COLORS, DateUtility } from 'app/utils';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { TextOut } from 'app/views/components/TextOut';
import { InovoiceCalculatorDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/getInvoiceCalculator';
import React, { useEffect, useMemo } from 'react';
import { connect } from 'react-redux';
import { Box, MenuItem, Select, SelectChangeEvent } from '@mui/material';

type DispatchProps = {
    getInvoiceCalculator: (args: GetInvoiceCalculatorParams) => void;
};

type StateProps = {
    invoiceCalculator: InovoiceCalculatorDataModel
};

type TProps = StateProps & DispatchProps & {
    setAdditionalCompanyId?: (id: string) => void;
    additionalCompanyId?: string;
    needShowAdditionalCompanies?: boolean;
};

const { useGetAdditionalCompanies } = FETCH_API.ACCOUNTS;

export const AddPaymentFormHeaderViewClass = ({ invoiceCalculator: { buyerName, supplierName }, getInvoiceCalculator, setAdditionalCompanyId, additionalCompanyId, needShowAdditionalCompanies }: TProps) => {
    const { data } = useGetAdditionalCompanies();

    useEffect(() => {
        getInvoiceCalculator({ force: true });
    }, [getInvoiceCalculator]);

    const additionalCompanyHandler = ({ target: { value } }: SelectChangeEvent) => {
        if (setAdditionalCompanyId) {
            setAdditionalCompanyId(value);
        }
    };

    return (
        <Box display="flex" flexDirection="column" gap="20px">
            <Box display="flex" justifyContent="center">
                <TextOut fontWeight={ 700 } fontSize={ 32 }>
                    Счет на оплату № 42________ от { DateUtility.getDateWithMonthName(new Date()) }
                </TextOut>
            </Box>
            <Box display="flex" flexDirection="column" gap="10px" width="fit-content">
                <FormAndLabel fullWidth={ needShowAdditionalCompanies } label="Поставщик">
                    <TextOut>{ supplierName }</TextOut>
                </FormAndLabel>
                <FormAndLabel label="Покупатель">
                    { needShowAdditionalCompanies
                        ? (
                            <Select
                                onChange={ additionalCompanyHandler }
                                fullWidth={ true }
                                variant="outlined"
                                value={ additionalCompanyId }
                            >
                                <MenuItem value="default">{ buyerName }</MenuItem>
                                { data?.rawData.map(({ key, value }) => (
                                    <MenuItem key={ key } value={ key }>{ value }</MenuItem>
                                )) }
                            </Select>
                        )
                        : (
                            <TextOut>{ buyerName }</TextOut>
                        )
                    }
                </FormAndLabel>
            </Box>
        </Box>
    );
};

export const AddPaymentFormHeaderView = connect<StateProps, DispatchProps, object, AppReduxStoreState>(
    ({ BalanceManagementState: { invoiceCalculatorFasta } }) => ({
        invoiceCalculator: invoiceCalculatorFasta
    }),
    {
        getInvoiceCalculator: GetInvoiceCalculatorThunk.invoke,
    }
)(AddPaymentFormHeaderViewClass);