// @ts-nocheck

import { AppRoutes } from 'app/AppRoutes';
import { OutlinedButton } from 'app/views/components/controls/Button';
import { useEffect } from 'react';
import { Link } from 'react-router-dom';

const message = 'Платеж находится в обработке';
const cancelMessage = 'Платеж отменен';

export const YookassaWidget = ({ token }: { token: string }) => {
    useEffect(() => {
        checkout.render('payment-form');
    }, []);

    const checkout = new window.YooMoneyCheckoutWidget({
        confirmation_token: token,
        return_url: `${ window.origin }${ AppRoutes.accountManagement.balanceManagement }?message=${ message }`,
        customization: {
            colors: {
                controlPrimary: '#FACF1E',
                background: '#fffff'
            }
        },
        error_callback(error: unknown) {
            console.log(error);
        }
    });

    return (
        <>
            <div id="payment-form" />
            <div style={ { textAlign: 'center' } }>
                <OutlinedButton>
                    <Link to={ `${ AppRoutes.accountManagement.balanceManagement }?message=${ cancelMessage }` }>
                        Вернуться на страницу баланса
                    </Link>
                </OutlinedButton>
            </div>
        </>
    );
};