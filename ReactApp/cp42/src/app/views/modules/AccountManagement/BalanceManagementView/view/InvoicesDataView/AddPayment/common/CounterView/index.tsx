import { ChangedValues } from 'app/views/modules/AccountManagement/BalanceManagementView/view/InvoicesDataView/AddPayment/common/Functions';
import React from 'react';
import { Services } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/getInvoiceCalculator';
import { TextField } from '@mui/material';
import cn from 'classnames';
import { hasComponentChangesFor } from 'app/common/functions';
import css from '../styles.module.css';

type OwnProps = {
    product: Services;
    quantity: string;
    onChange?: (args: ChangedValues) => void
};

type OwnState = {
    quantity: number;
};

export class CounterView extends React.Component<OwnProps, OwnState> {
    private readonly maxCount: number;

    private readonly minCount: number;

    private waitTimerId = 0;

    private readonly beforeValueAppliedTimeoutInMilliseconds = 700;

    constructor(props: OwnProps) {
        super(props);
        const quantity = Number(this.props.quantity);
        this.waitForTimeBeforeValueAppliedEvent = this.waitForTimeBeforeValueAppliedEvent.bind(this);
        this.onChange = this.onChange.bind(this);
        this.increment = this.increment.bind(this);
        this.decrement = this.decrement.bind(this);
        this.state = { quantity };
        const { Settings, Type } = this.props.product.QuantityControl;
        const settingMax = Type !== 'PredefinedList' ? Settings.find(item => item.Key === 'MaxValue') : Settings[Settings.length - 1];
        const settingMin = Type !== 'PredefinedList' ? Settings.find(item => item.Key === 'MinValue') : Settings[0];

        this.maxCount = settingMax ? Number(settingMax.Value) : 9999;
        this.minCount = settingMin ? Number(settingMin.Value) : 0;
    }

    public shouldComponentUpdate(nextProps: OwnProps, nextState: OwnState) {
        return hasComponentChangesFor(this.props, nextProps) || hasComponentChangesFor(this.state, nextState);
    }

    private onChange(num: number) {
        if (num > this.maxCount) {
            this.setState({ quantity: this.maxCount });

            return true;
        }

        this.setState({ quantity: num });
        return true;
    }

    private increment() {
        const QuantytySettings = this.props.product.QuantityControl.Settings;

        if (this.state.quantity >= this.maxCount) {
            return;
        }

        if (this.props.product.QuantityControl.Type === 'PredefinedList') {
            const index = QuantytySettings.findIndex(item => item.Value === this.state.quantity.toString());
            const res = QuantytySettings.length - 1 < index + 1 ? QuantytySettings[QuantytySettings.length - 1].Value : QuantytySettings[index + 1].Value;

            if (Number(res) === this.state.quantity) {
                return;
            }

            this.setState({ quantity: Number(res) });

            if (this.props.onChange) {
                this.props.onChange({ Quantity: Number(res), Identifier: this.props.product.identifier });
            }

            return;
        }

        this.setState(prevState => ({
            quantity: prevState.quantity + 1
        }));

        if (this.props.onChange) {
            this.props.onChange({ Quantity: this.state.quantity + 1, Identifier: this.props.product.identifier });
        }
    }

    private decrement() {
        const QuantytySettings = this.props.product.QuantityControl.Settings;

        if (this.state.quantity === this.minCount) {
            return;
        }

        if (this.props.product.QuantityControl.Type === 'PredefinedList') {
            const index = QuantytySettings.findIndex(item => item.Value === this.state.quantity.toString());
            const res = index === 0 ? QuantytySettings[0].Value : QuantytySettings[index - 1].Value;
            this.setState({ quantity: Number(res) });
            if (this.props.onChange) {
                this.props.onChange({ Quantity: Number(res), Identifier: this.props.product.identifier });
            }

            return;
        }

        this.setState(prevState => ({
            quantity: prevState.quantity - 1
        }));

        if (this.props.onChange) {
            this.props.onChange({ Quantity: this.state.quantity - 1, Identifier: this.props.product.identifier });
        }
    }

    private waitForTimeBeforeValueAppliedEvent(e: React.ChangeEvent<HTMLInputElement>) {
        const newValue = Number(e.target.value);
        const isSuccess = this.onChange(newValue) ?? true;

        if (this.waitTimerId) {
            clearTimeout(this.waitTimerId);
        }

        if (!this.props.onChange || !isSuccess) {
            return;
        }

        this.waitTimerId = setTimeout(this.props.onChange, this.beforeValueAppliedTimeoutInMilliseconds, { Quantity: newValue > this.maxCount ? this.maxCount : newValue, Identifier: this.props.product.identifier });
    }

    public render() {
        const { product } = this.props;
        const constantType = product.QuantityControl.Type === 'Constant';
        const inActive = !product.isActive;
        const predefinedList = product.QuantityControl.Type === 'PredefinedList';
        const readOnly = predefinedList || inActive || constantType || product.name === 'Fasta';

        return (
            <div className={ css.counter }>
                { (!constantType && this.minCount !== this.state.quantity)
                    ? (
                        <i
                            className={ cn({
                                [css.icon]: true,
                                'fa fa-minus': true
                            }) }
                            onClick={ this.decrement }
                        />
                    )
                    : <span className={ cn(css['empty-icon']) } />
                }
                <TextField
                    disabled={ readOnly }
                    value={ Number(this.state.quantity).toString() }
                    label={ null }
                    onChange={ this.waitForTimeBeforeValueAppliedEvent }
                    InputLabelProps={ { shrink: false } }
                    onKeyDown={ e => ['e', 'E', '+', '-'].includes(e.key) && e.preventDefault() }
                    InputProps={ { readOnly } }
                    inputProps={ {
                        style: { padding: 0 },
                        min: '0',
                        type: 'number',
                        'aria-readonly': readOnly,
                        className: cn(css['counter-input']),
                    } }
                />
                { (!constantType && this.maxCount !== this.state.quantity)
                    ? (
                        <i
                            className={ cn({
                                [css.icon]: true,
                                'fa fa-plus': true
                            }) }
                            onClick={ this.increment }
                        />
                    )
                    : <span className={ cn(css['empty-icon']) } />
                }
            </div>
        );
    }
}