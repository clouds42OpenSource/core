import { ComboBoxForm, TextBoxForm } from 'app/views/components/controls/forms';
import { PaymentType, TransactionsFilterStatusExtensions, TransactionType } from 'app/common/enums/BalanceManagement';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { getComboBoxFormOptionsFromKeyValueDataModel } from 'app/common/functions';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { AddTransactionDataForm } from 'app/views/modules/AccountManagement/BalanceManagementView/types/AddTransactionDataForm';
import { AddTransactionDataFormFieldNamesType } from 'app/views/modules/AccountManagement/BalanceManagementView/types/TransactionsFilterDataFormFieldNamesType';
import { Dialog } from 'app/views/components/controls/Dialog';
import { MuiDateInputForm } from 'app/views/components/controls/forms/MuiDateInputForm';
import React from 'react';
import { Box } from '@mui/material';

type OwnProps = ReduxFormProps<AddTransactionDataForm> & {
    changeTransaction: () => void;
    isVisible: boolean;
    onAddTransaction: (args: AddTransactionDataForm) => void;
};

type OwnState = {
    showErrorText: boolean;
};

const initReduxForm = (): AddTransactionDataForm | undefined => ({
    Date: new Date(),
    Description: '',
    Type: PaymentType.Incomming,
    Total: '0.00',
    TransactionType: TransactionType.Money
});

export class AddTransactionFormClass extends React.Component<OwnProps, OwnState> {
    constructor(props: OwnProps) {
        super(props);
        this.onValueChange = this.onValueChange.bind(this);
        this.addTransaction = this.addTransaction.bind(this);
        this.state = {
            showErrorText: false
        };
    }

    public componentDidMount() {
        this.props.reduxForm.setInitializeFormDataAction(initReduxForm);
    }

    private onValueChange<TValue, TForm extends string = AddTransactionDataFormFieldNamesType>(formName: TForm, newValue: TValue) {
        if (formName === 'Total') {
            const str = newValue as unknown as string;
            const newNum = Number(str.replace(/,/g, '.'));

            this.setState({ showErrorText: !newNum || newNum <= 0 });
        }

        this.props.reduxForm.updateReduxFormFields({ [formName]: newValue });
    }

    private get checkValidTotal() {
        const { showErrorText } = this.state;
        const formFields = this.props.reduxForm.getReduxFormFields(false);

        if (formFields.Total !== undefined) {
            return !showErrorText && Number(formFields.Total.replace(/,/g, '.')) > 0;
        }

        return !showErrorText;
    }

    private addTransaction() {
        this.props.onAddTransaction(this.props.reduxForm.getReduxFormFields(false));
    }

    public render() {
        const { changeTransaction, isVisible, reduxForm } = this.props;
        const formFields = reduxForm.getReduxFormFields(false);

        return (
            <Dialog
                title="Создание фин. транзакции"
                dialogVerticalAlign="center"
                isOpen={ isVisible }
                isTitleSmall={ true }
                dialogWidth="sm"
                onCancelClick={ changeTransaction }
                buttons={ [
                    {
                        content: 'Отмена',
                        onClick: changeTransaction,
                        kind: 'default',
                    },
                    {
                        content: 'Создать',
                        onClick: this.addTransaction,
                        isEnabled: this.checkValidTotal,
                        kind: 'success'
                    }
                ] }
            >
                <Box display="flex" flexDirection="column" gap="10px">
                    <FormAndLabel label="Сумма">
                        <TextBoxForm
                            showErrorText={ this.state.showErrorText }
                            formName="Total"
                            onValueChange={ this.onValueChange }
                            value={ formFields.Total }
                            placeholder="0.00"
                        />
                    </FormAndLabel>
                    <FormAndLabel label="Тип платежа">
                        <ComboBoxForm
                            formName="Type"
                            onValueChange={ this.onValueChange }
                            value={ formFields.Type || null }
                            items={ getComboBoxFormOptionsFromKeyValueDataModel(TransactionsFilterStatusExtensions.getPaymentType()) }
                        />
                    </FormAndLabel>
                    <FormAndLabel label="Тип транзакции">
                        <ComboBoxForm
                            formName="TransactionType"
                            onValueChange={ this.onValueChange }
                            value={ formFields.TransactionType || null }
                            items={ getComboBoxFormOptionsFromKeyValueDataModel(TransactionsFilterStatusExtensions.getTransactionType()) }
                        />
                    </FormAndLabel>
                    <FormAndLabel label="Дата">
                        <MuiDateInputForm
                            formName="Date"
                            placeholder="Дата"
                            value={ formFields.Date }
                            onValueChange={ this.onValueChange }
                            includeTime={ true }
                        />
                    </FormAndLabel>
                    <FormAndLabel label="Комментарий">
                        <TextBoxForm
                            formName="Description"
                            onValueChange={ this.onValueChange }
                            value={ formFields.Description }
                            placeholder=""
                        />
                    </FormAndLabel>
                </Box>
            </Dialog>
        );
    }
}

export const AddTransactionView = withReduxForm(AddTransactionFormClass, {
    reduxFormName: 'AddTransaction',
    resetOnUnmount: true
});