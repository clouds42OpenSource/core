import { AddTransactionDataForm } from './AddTransactionDataForm';
import { TransactionsFilterDataForm } from './TransactionsFilterDataForm';

export type TransactionsFilterDataFormFieldNamesType = keyof TransactionsFilterDataForm;
export type AddTransactionDataFormFieldNamesType = keyof AddTransactionDataForm;