import { hasComponentChangesFor } from 'app/common/functions';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { withHeader } from 'app/views/components/_hoc/withHeader';
import { TabsControl } from 'app/views/components/controls/TabsControl';
import { ERefreshId } from 'app/views/Layout/ProjectTour/enums';
import { AddPaymentFormHeaderView } from 'app/views/modules/AccountManagement/BalanceManagementView/view/InvoicesDataView/AddPayment/common/PaymentHeaderView';
import { AdditionalServicesView } from 'app/views/modules/AccountManagement/BalanceManagementView/view/InvoicesDataView/AddPayment/Tabs/AdditionalServicesView';
import { ArbitiraryAmountView } from 'app/views/modules/AccountManagement/BalanceManagementView/view/InvoicesDataView/AddPayment/Tabs/ArbitraryAmountView';
import { UserPaymentsView } from 'app/views/modules/AccountManagement/BalanceManagementView/view/InvoicesDataView/AddPayment/Tabs/UserPaymentView';
import React from 'react';
import { withRouter } from 'react-router';
import { RouteComponentProps } from 'react-router-dom';

type OwnProps = RouteComponentProps & FloatMessageProps;

type OwnState = {
    activeTabIndex: number;
    additionalCompanyId: string;
};

type AllProps = OwnProps;

const headers = [
    {
        title: 'Абонентская плата',
        isVisible: true,
    },
    {
        title: 'Доп. сервисы и услуги',
        isVisible: true
    },
    {
        title: 'Произвольная сумма',
        isVisible: true
    }
];

export class AddPaymentViewClass extends React.Component<AllProps, OwnState> {
    private readonly isPromisePaymentIncrease = new URLSearchParams(window.location.search).get('isPromisePaymentIncrease')?.trim().toLowerCase() === 'true';

    public constructor(props: AllProps) {
        super(props);
        this.onActiveTabIndexChanged = this.onActiveTabIndexChanged.bind(this);
        this.additionalCompanyIdHandler = this.additionalCompanyIdHandler.bind(this);
        this.state = {
            activeTabIndex: new URLSearchParams(window.location.search).get('customPayment.amount') !== null ? 2 : 0,
            additionalCompanyId: 'default'
        };
    }

    public shouldComponentUpdate(prevProps: AllProps, prevState: OwnState) {
        return hasComponentChangesFor(prevProps, this.props) || hasComponentChangesFor(this.state, prevState);
    }

    private onActiveTabIndexChanged(tabIndex: number) {
        this.setState({ activeTabIndex: tabIndex });
    }

    private additionalCompanyIdHandler(id: string) {
        this.setState({ additionalCompanyId: id });
    }

    public render() {
        const { additionalCompanyId } = this.state;

        if (this.isPromisePaymentIncrease) {
            return (
                <>
                    <AddPaymentFormHeaderView />
                    <ArbitiraryAmountView />
                </>
            );
        }

        return (
            <TabsControl
                headerRefreshId={ ERefreshId.userPaymentViewTable }
                headers={ headers }
                activeTabIndex={ this.state.activeTabIndex }
                onActiveTabIndexChanged={ this.onActiveTabIndexChanged }
                headerContent={
                    <AddPaymentFormHeaderView
                        needShowAdditionalCompanies={ this.state.activeTabIndex === 0 }
                        additionalCompanyId={ additionalCompanyId }
                        setAdditionalCompanyId={ this.additionalCompanyIdHandler }
                    />
                }
            >
                <UserPaymentsView additionalCompanyId={ additionalCompanyId === 'default' ? '' : additionalCompanyId } />
                <AdditionalServicesView />
                <ArbitiraryAmountView />
            </TabsControl>
        );
    }
}

const title = () => {
    const query = new URLSearchParams(window.location.search);

    if (query.get('isPromisePayment')?.trim().toLowerCase() === 'true') {
        return 'Активация обещанного платежа';
    }

    if (query.get('isPromisePaymentIncrease')?.trim().toLowerCase() === 'true') {
        return 'Увеличение обещанного платежа';
    }

    return 'Создание счета';
};

export const AddPaymentView = withHeader({
    page: withRouter(withFloatMessages(AddPaymentViewClass)),
    title: title()
});