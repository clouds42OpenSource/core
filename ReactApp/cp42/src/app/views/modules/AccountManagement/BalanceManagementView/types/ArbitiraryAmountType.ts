export type ArbitiraryAmountType = {
    service: string;
    descService: string;
    sum: number;
};