import { Box, Checkbox, FormControlLabel } from '@mui/material';
import { AppRoutes } from 'app/AppRoutes';
import { hasComponentChangesFor } from 'app/common/functions';
import { AppReduxStoreState } from 'app/redux/types';
import { COLORS, NumberUtility } from 'app/utils';
import { TableFilterWrapper } from 'app/views/components/_hoc/tableFilterWrapper';
import { FilterItemWrapper } from 'app/views/components/_hoc/tableFilterWrapper/filterItemWrapper';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { DefaultButton, SuccessButton } from 'app/views/components/controls/Button';
import { ComboBoxForm } from 'app/views/components/controls/forms';
import { ETourId } from 'app/views/Layout/ProjectTour/enums';
import cn from 'classnames';
import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps, withRouter } from 'react-router-dom';
import css from '../styles.module.css';

enum EPaymentType {
    default,
    promise,
    increase
}

type StateProps = {
    error?: Error;
    id: string;
};

type OwnProps = {
    TotalBeforeVAT?: number;
    VAT?: number;
    TotalAfterVAT?: number;
    TotalBonus?: number;
    emails: { text: string, value: number }[];
    currentEmail?: number;
    onEmailChange: (formName: string, newValue?: number | null) => void;
    onCreateInvoice?: () => void;
    onActivatePromisePayment?: (paymentType?: EPaymentType) => void | boolean;
    type: string;
    description?: string;
};

type OwnState = {
    disabled: boolean;
};

type AllProps = RouteComponentProps & FloatMessageProps & OwnProps & StateProps;

export class PaymentFooter extends React.Component<AllProps, OwnState> {
    constructor(props: AllProps) {
        super(props);

        this.state = {
            disabled: true,
        };
        this.onChangeCheckBox = this.onChangeCheckBox.bind(this);
        this.createInvoice = this.createInvoice.bind(this);
        this.activatePromisePayment = this.activatePromisePayment.bind(this);
    }

    public shouldComponentUpdate(nextProps: OwnProps, nextState: OwnState) {
        return hasComponentChangesFor(this.props, nextProps) || hasComponentChangesFor(this.state, nextState);
    }

    public componentDidUpdate(prevProps: Readonly<AllProps>) {
        if (this.props.error && prevProps.error !== this.props.error) {
            this.props.floatMessage.show(MessageType.Error, this.props.error.message);
        } else if (prevProps.id !== this.props.id) {
            this.props.history.push(AppRoutes.accountManagement.balanceManagement);
        }
    }

    private get isPromisePayment() {
        const query = new URLSearchParams(window.location.search);

        if (query.get('isPromisePayment')?.trim().toLowerCase() === 'true') {
            return EPaymentType.promise;
        }

        if (query.get('isPromisePaymentIncrease')?.trim().toLowerCase() === 'true') {
            return EPaymentType.increase;
        }

        return EPaymentType.default;
    }

    private createInvoice() {
        if (this.props.onCreateInvoice) {
            this.props.onCreateInvoice();
        }
    }

    private onChangeCheckBox() {
        this.setState({
            disabled: !this.state.disabled
        });
        if (this.state.disabled) {
            this.props.onEmailChange('', undefined);
        } else {
            this.props.onEmailChange('', 0);
        }
    }

    private activatePromisePayment() {
        if (this.props.onActivatePromisePayment) {
            this.props.onActivatePromisePayment(this.isPromisePayment);
        }
    }

    public render() {
        return (
            <Box display="flex" flexWrap="wrap" marginTop="10px">
                <Box display="flex" flexDirection="column" flex="1 1 auto">
                    <TableFilterWrapper>
                        <FormControlLabel
                            sx={ { margin: '0', padding: '0' } }
                            control={ <Checkbox
                                size="small"
                                checked={ this.state.disabled }
                                onChange={ () => this.onChangeCheckBox() }
                            /> }
                            label={ <span style={ { fontSize: '13px' } }>Отправить на эл.адрес:</span> }
                        />
                        <ComboBoxForm
                            formName="needSendEmail"
                            items={ !this.props.emails.length ? [{ value: 0, text: 'Нет почты для отправки' }] : this.props.emails }
                            value={ this.props.currentEmail }
                            label="Почта"
                            isReadOnly={ !this.state.disabled }
                            onValueChange={ this.props.onEmailChange }
                        />
                    </TableFilterWrapper>
                </Box>
                <Box display="flex" flexDirection="column" flex="1 1 auto">
                    <div className={ cn(css['summary-block']) }>
                        <span>Итого:</span><p /> <span>{ NumberUtility.numberToMoneyFormat(this.props.TotalBeforeVAT ?? 0) }</span>
                    </div>
                    <div className={ cn(css['summary-block']) }>
                        <span>В том числе НДС:</span><p /><span>{ this.props.VAT === 0 ? 'Без НДС' : NumberUtility.numberToMoneyFormat(this.props.VAT ?? 0) }</span>
                    </div>
                    <div className={ cn(css['summary-block'], css['summary-block-total']) }>
                        <strong>Всего к оплате:</strong><p /><strong>{ NumberUtility.numberToMoneyFormat(this.props.TotalAfterVAT ?? 0) }</strong>
                    </div>
                    { !!this.props.TotalBonus &&
                        <div className={ cn(css['summary-block']) }>
                            <span>Будет начислено бонусов:</span><p /><strong style={ { color: COLORS.main } }>{ NumberUtility.numberToMoneyFormat(this.props.TotalBonus ?? 0) }</strong>
                        </div>
                    }
                    <FilterItemWrapper width="100%" style={ { justifyContent: 'flex-end', display: 'flex', flexWrap: 'wrap', gap: '8px' } }>
                        <DefaultButton>
                            <Link to={ AppRoutes.accountManagement.balanceManagement }>
                                Отмена
                            </Link>
                        </DefaultButton>
                        { this.isPromisePayment !== EPaymentType.default
                            ? (
                                <SuccessButton
                                    isEnabled={ !!(this.props.TotalAfterVAT && this.props.TotalAfterVAT > 0) }
                                    onClick={ this.props.onActivatePromisePayment ? this.activatePromisePayment : undefined }
                                >
                                    { this.isPromisePayment === EPaymentType.promise ? 'Активировать' : 'Увеличить' } обещанный платеж
                                </SuccessButton>
                            )
                            : (
                                <>
                                    <DefaultButton
                                        tourId={ ETourId.replenishBalancePageGenerateInvoiceButton }
                                        isEnabled={ !!(this.props.TotalAfterVAT && this.props.TotalAfterVAT > 0) }
                                        onClick={ this.createInvoice }
                                    >
                                        Сформировать счёт
                                    </DefaultButton>
                                    <Link to={ `${ AppRoutes.accountManagement.balanceManagement }/confirm-payment?&total-sum=${ this.props.TotalAfterVAT }&type=${ this.props.type }&email-id=${ this.props.currentEmail }${ this.props.description ? `&description=${ this.props.description }` : '' }` }>
                                        <SuccessButton isEnabled={ !!(this.props.TotalAfterVAT && this.props.TotalAfterVAT > 0) }>Оплатить онлайн</SuccessButton>
                                    </Link>
                                </>
                            )
                        }
                    </FilterItemWrapper>
                </Box>
            </Box>
        );
    }
}

export const PaymentFooterView = connect<StateProps, {}, OwnProps, AppReduxStoreState>(
    state => ({
        error: state.BalanceManagementState.process.error,
        id: state.BalanceManagementState.invoiceId
    }), {}
)(withRouter(withFloatMessages(PaymentFooter)));