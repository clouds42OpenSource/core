import { AccountUserGroup } from 'app/common/enums';
import { DateUtility, NumberUtility } from 'app/utils';
import { CommonTableWithFilter, SelectDataInitiator } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { TablePagination } from 'app/views/modules/_common/components/CommonTableWithFilter/TableView';
import { TransactionsFilterDataForm } from 'app/views/modules/AccountManagement/BalanceManagementView/types/TransactionsFilterDataForm';
import { TransactionsInvoiceType } from 'app/views/modules/AccountManagement/BalanceManagementView/types/TransactionsInvoiceType';
import { TransactionsListFilterFormView } from 'app/views/modules/AccountManagement/BalanceManagementView/view/TransactionsDataView/view/TransactionsListFilterFormClass';
import { TruncateText } from 'app/views/modules/AccountManagement/BalanceManagementView/view/TruncateText/TruncateTextView';
import { SelectDataCommonDataModel } from 'app/web/common';
import { AccountPermissionsEnum } from 'app/web/common/enums/AccountPermissionsEnum';
import { TransactionsItemDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/recieveTransactions';
import React from 'react';

type StateProps = {
    isMobileSized: boolean;
    currency: string;
    hasPermission: string[];
    dataset: {
        data: TransactionsItemDataModel[];
        pagination: TablePagination;
    };
    accountUserGroup: AccountUserGroup[];
};

type DispatchProps = {
    onDataSelect: (args: SelectDataCommonDataModel<TransactionsFilterDataForm>, filterFormName: string | null, initiator: SelectDataInitiator) => void
    changeTransaction: () => void
};

type AllProps = StateProps & DispatchProps;

export class TransactionsListDataView extends React.Component<AllProps> {
    public shouldComponentUpdate(nextProps: AllProps) {
        return nextProps.isMobileSized !== this.props.isMobileSized ||
            this.props.dataset !== nextProps.dataset ||
            this.props.currency !== nextProps.currency;
    }

    public render() {
        const transactions = this.props.dataset;

        return (
            <CommonTableWithFilter
                isResponsiveTable={ true }
                isBorderStyled={ true }
                filterProps={ {
                    getFilterContentView: (ref, onFilterChanged) =>
                        <TransactionsListFilterFormView
                            ref={ ref }
                            onFilterChanged={ onFilterChanged }
                            onAddTransaction={ this.props.changeTransaction }
                            canAddTransaction={ this.props.hasPermission.includes(AccountPermissionsEnum.AddTransaction) }
                        />
                } }
                uniqueContextProviderStateId="TransactionsListContextProviderStateId"
                tableProps={ {
                    emptyText: 'Вы не совершали еще никаких операций',
                    dataset: transactions.data,
                    keyFieldName: 'Id',
                    pagination: transactions.pagination,
                    fieldsView: {
                        Title: {
                            caption: 'Операция',
                            fieldContentNoWrap: false,
                            fieldWidth: '40%',
                            format: (value: string) => {
                                return <TruncateText text={ value } isMobileSized={ this.props.isMobileSized } />;
                            }
                        },
                        Date: {
                            caption: 'Дата',
                            fieldContentNoWrap: false,
                            fieldWidth: '15%',
                            format: (value: string) => {
                                return value
                                    ? <span>{ DateUtility.dateToDayMonthYear(DateUtility.convertToDate(value)) }</span>
                                    : '----------';
                            }
                        },
                        TransactionType: {
                            caption: 'Счет',
                            fieldContentNoWrap: false,
                            fieldWidth: '15%',
                            format: (value: boolean) => {
                                return value ? 'Бонусный' : 'Основной';
                            }
                        },
                        PaySum: {
                            caption: `Сумма,${ this.props.currency }`,
                            fieldContentNoWrap: false,
                            style: { textAlign: 'start' },
                            fieldWidth: '15%',
                            format: (value: number, data) => {
                                return value !== null
                                    ? (
                                        <>
                                            <span>{ data.Type === TransactionsInvoiceType.outgoing ? <i className="fa fa-plus" /> : <i className="fa fa-minus" /> }</span>
                                            <span> { NumberUtility.numberToMoneyFormat(value) }</span>
                                        </>
                                    )
                                    : '----------';
                            }
                        },
                        ...((this.props.accountUserGroup.includes(AccountUserGroup.AccountSaleManager) || this.props.accountUserGroup.includes(AccountUserGroup.CloudAdmin)) && {
                            Remains: {
                                caption: 'Остаток на балансе',
                                fieldWidth: '15%',
                                format: (value: number) => {
                                    return value
                                        ? <span>{ NumberUtility.numberToMoneyFormat(value) }</span>
                                        : '-';
                                }
                            }
                        }),
                        Status: {
                            caption: 'Статус',
                            fieldContentNoWrap: false,
                            style: { textAlign: 'start', height: '35px' },
                            fieldWidth: '15%',
                            format: (value: number) => {
                                return value === 1 ? 'Отменен' : value === 2 ? 'Ожидает' : 'Успешно';
                            }
                        }
                    }
                } }
                onDataSelect={ this.props.onDataSelect }
            />
        );
    }
}