import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { CommonTableBasic } from 'app/views/modules/_common/components/CommonTableBasic';
import { memo } from 'react';

export const ElectronicDocumentManagement = memo(() => {
    return (
        <FormAndLabel label="Идентификаторы ЭДО">
            <CommonTableBasic
                dataset={ [
                    {
                        caption: 'Организация',
                        value: 'ЭР СОФТ, ООО, Москва ИНН 7716509296',
                        visible: true,
                    },
                    {
                        caption: 'Калуга (АО «Калуга Астрал»)',
                        value: '2AE094A24C6-3D36-472A-B628-C26A9946FE46',
                        visible: true,
                    },
                    {
                        caption: 'Такском (ООО «Такском»)',
                        value: '2AL-37D027BC-7D35-4AE9-9C1C-35EBEEF40757-00001',
                        visible: true,
                    },
                    {
                        caption: 'Диадок (АО «ПФ «СКБ КОНТУР»)',
                        value: '2BM-7716509296-2013091712090776150840000000000',
                        visible: true,
                    }
                ] }
            />
        </FormAndLabel>
    );
});