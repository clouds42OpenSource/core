import React from 'react';
import { hasComponentChangesFor } from 'app/common/functions';
import { truncate } from 'app/views/components/controls/Table/functions/getStringTruncate';

type OwnProps = {
    text: string
    isMobileSized: boolean
};

type OwnState = {
    show: boolean
};

export class TruncateText extends React.Component<OwnProps, OwnState> {
    constructor(props: OwnProps) {
        super(props);
        this.state = {
            show: !this.props.isMobileSized
        };
        this.setShowState = this.setShowState.bind(this);
        this.truncatedText = this.truncatedText.bind(this);
    }

    public componentDidMount() {
        this.truncatedText();
    }

    public shouldComponentUpdate(nextProps: OwnProps, nextState: OwnState) {
        return hasComponentChangesFor(this.props.isMobileSized, nextProps.isMobileSized) || hasComponentChangesFor(this.state.show, nextState.show);
    }

    public setShowState() {
        if (!this.props.isMobileSized) {
            return;
        }

        this.setState(prevState => ({ show: !prevState.show }));
    }

    public truncatedText() {
        if (this.props.text.length < 29) {
            return this.props.text;
        }

        return (
            <>
                <span>{ truncate(this.props.text, 30) }</span>
                <span style={ { color: '#337ab7', fontSize: '11px', borderBottom: '1px dotted #337ab7' } }> ещё</span>
            </>
        );
    }

    public render() {
        return (
            <span onClick={ this.setShowState }>
                { this.state.show ? this.props.text : this.truncatedText() }
            </span>
        );
    }
}