import { Box } from '@mui/material';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { SearchByFilterButton, SuccessButton } from 'app/views/components/controls/Button';
import { TransactionsFilterStatusExtensions, TransactionsFiltertStatus } from 'app/common/enums/BalanceManagement';
import { getComboBoxFormOptionsFromKeyValueDataModel } from 'app/common/functions';
import { ComboBoxForm } from 'app/views/components/controls/forms';
import { DateUtility } from 'app/utils/DateUtility';
import { DoubleDateInputForm } from 'app/views/components/controls/forms/DoubleDateInputView';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import React from 'react';
import { TableFilterWrapper } from 'app/views/components/_hoc/tableFilterWrapper';
import { TransactionsFilterDataForm } from 'app/views/modules/AccountManagement/BalanceManagementView/types/TransactionsFilterDataForm';
import { TransactionsFilterDataFormFieldNamesType } from 'app/views/modules/AccountManagement/BalanceManagementView/types/TransactionsFilterDataFormFieldNamesType';

const initReduxForm = () => ({
    start: DateUtility.getSomeYearAgoDate(1),
    end: new Date(),
    type: TransactionsFiltertStatus.AllServices
});

/**
 * Свойства для компонента фильтра
 */
type OwnProps = ReduxFormProps<TransactionsFilterDataForm> & {
    onFilterChanged: (filter: TransactionsFilterDataForm, filterFormName: string) => void;
    onAddTransaction: () => void;
    canAddTransaction: boolean;
};

class TransactionsListFilterFormClass extends React.Component<OwnProps> {
    public constructor(props: OwnProps) {
        super(props);
        this.onValueChange = this.onValueChange.bind(this);
        this.applyFilter = this.applyFilter.bind(this);
        this.handleEnterKey = this.handleEnterKey.bind(this);
    }

    public componentDidMount() {
        this.props.reduxForm.setInitializeFormDataAction(initReduxForm);
        document.body.addEventListener('keydown', this.handleEnterKey);
        this.props.reduxForm.updateReduxFormFields({
            start: DateUtility.getSomeMonthAgoDate(6),
            end: new Date(),
            type: TransactionsFiltertStatus.AllServices
        });
    }

    public componentWillUnmount() {
        document.body.removeEventListener('keydown', this.handleEnterKey);
    }

    private handleEnterKey(event: KeyboardEvent) {
        if (event.key !== 'Enter') {
            return;
        }

        event.preventDefault();
        this.applyFilter();
    }

    private onValueChange<TValue, TForm extends string = TransactionsFilterDataFormFieldNamesType>(formName: TForm, newValue: TValue) {
        this.props.reduxForm.updateReduxFormFields({
            [formName]: newValue
        });
    }

    private applyFilter() {
        this.props.onFilterChanged({
            ...this.props.reduxForm.getReduxFormFields(false)
        }, '');
    }

    public render() {
        const formFields = this.props.reduxForm.getReduxFormFields(false);

        if (formFields.end || formFields.start) {
            return (
                <Box display="flex" gap="10px" alignItems="flex-end">
                    <FormAndLabel label="Период поиска">
                        <DoubleDateInputForm
                            periodFromValue={ formFields.start }
                            periodToValue={ formFields.end }
                            periodFromInputName="start"
                            periodToInputName="end"
                            onValueChange={ this.onValueChange }
                            maxDateFrom={ new Date() }
                        />
                    </FormAndLabel>
                    <FormAndLabel label="Тип транзакции">
                        <ComboBoxForm
                            formName="type"
                            onValueChange={ this.onValueChange }
                            value={ formFields.type ?? -1 }
                            items={ getComboBoxFormOptionsFromKeyValueDataModel(TransactionsFilterStatusExtensions.getAllDescriptions()) }
                        />
                    </FormAndLabel>
                    <SearchByFilterButton onClick={ this.applyFilter } />
                    { this.props.canAddTransaction &&
                        <SuccessButton onClick={ this.props.onAddTransaction }>
                            Создать фин. транзакцию
                        </SuccessButton>
                    }
                </Box>
            );
        }

        return null;
    }
}

export const TransactionsListFilterFormView = withReduxForm(TransactionsListFilterFormClass, {
    reduxFormName: 'TransactionsList',
    resetOnUnmount: true
});