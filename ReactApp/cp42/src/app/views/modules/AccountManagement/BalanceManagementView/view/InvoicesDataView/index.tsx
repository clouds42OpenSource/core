import ContactPageIcon from '@mui/icons-material/ContactPage';
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import FileDownloadOutlinedIcon from '@mui/icons-material/FileDownloadOutlined';
import FileUploadOutlinedIcon from '@mui/icons-material/FileUploadOutlined';
import FindInPageIcon from '@mui/icons-material/FindInPage';
import PrintOutlinedIcon from '@mui/icons-material/PrintOutlined';
import TaskIcon from '@mui/icons-material/Task';
import { Box, Chip, IconButton, Link as MuiLink, Tooltip } from '@mui/material';
import { apiHost } from 'app/api';
import { GetCurrentSessionsSettingsResponse } from 'app/api/endpoints/global/response';
import { FETCH_API } from 'app/api/useFetchApi';
import { AppRoutes } from 'app/AppRoutes';
import { AccountUserGroup } from 'app/common/enums';
import { ReceiveInvoiceListThunkParams } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/receiveInvoiceListReducer/params';
import { ReceiveInvoiceListThunk } from 'app/modules/AccountManagement/BalanceManagement/store/thunks';
import { PayOnlineThunk, PayOnlineThunkParams } from 'app/modules/AccountManagement/BalanceManagement/store/thunks/PayOnlineThunk';
import { AppReduxStoreState } from 'app/redux/types';
import { COLORS, NumberUtility } from 'app/utils';
import { DateUtility } from 'app/utils/DateUtility';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { ContainedButton } from 'app/views/components/controls/Button';
import { Dialog } from 'app/views/components/controls/Dialog';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { TextOut } from 'app/views/components/TextOut';
import { ERefreshId, ETourId } from 'app/views/Layout/ProjectTour/enums';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { TablePagination } from 'app/views/modules/_common/components/CommonTableWithFilter/TableView';
import { SelectDataCommonDataModel } from 'app/web/common';
import { GetSavedPaymentMethods } from 'app/web/InterlayerApiProxy/AutoPaymentApiProxy/getSavedPaymentMethods';
import { InvoiceItemDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/receiveInvoices';
import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { DialogContentView } from './DialogContent';
import { ElectronicDocumentManagement } from './ElectronicDocumentManagement';

import styles from './style.module.css';

type StateProps<TDataRow = InvoiceItemDataModel> = {
    currency: string;
    dataset: TDataRow[];
    pagination: TablePagination;
    paymentMethods: GetSavedPaymentMethods[];
    currentSettings: GetCurrentSessionsSettingsResponse;
};

type OwnProps = {
    locales: string;
    isDemo: boolean;
};

type DispatchProps = {
    dispatchReceiveInvoicesListThunk: (args: ReceiveInvoiceListThunkParams) => void;
    payOnline: (args: PayOnlineThunkParams) => void;
};

type AllProps = StateProps & DispatchProps & OwnProps & FloatMessageProps;

type OwnState = {
    isOpenAddDefaultPaymentMethod: boolean;
    isLoading: boolean;
    pageNumber: number;
};

const { deleteInvoice, postClosingDocuments } = FETCH_API.INVOICE;
const { getClosingDocumentFile } = FETCH_API.CLOSING_DOCUMENTS;

export class InvoicesDataViewClass extends React.Component<AllProps, OwnState> {
    public constructor(props: AllProps) {
        super(props);

        this.state = {
            isOpenAddDefaultPaymentMethod: false,
            isLoading: false,
            pageNumber: 1
        };

        this.onDataSelect = this.onDataSelect.bind(this);
        this.openAddDefaultPaymentMethod = this.openAddDefaultPaymentMethod.bind(this);
        this.closeAddDefaultPaymentMethod = this.closeAddDefaultPaymentMethod.bind(this);
        this.addDefaultPaymentMethod = this.addDefaultPaymentMethod.bind(this);
        this.deleteInvoice = this.deleteInvoice.bind(this);
        this.closingDocuments = this.closingDocuments.bind(this);
        this.renderSignedDocument = this.renderSignedDocument.bind(this);
        this.downloadAct = this.downloadAct.bind(this);
        this.uploadAct = this.uploadAct.bind(this);
    }

    public componentDidMount() {
        this.props.dispatchReceiveInvoicesListThunk({ pageNumber: 1, force: true, showLoadingProgress: true });
    }

    private onDataSelect({ pageNumber }: SelectDataCommonDataModel<object>) {
        this.props.dispatchReceiveInvoicesListThunk({ pageNumber, force: true });
    }

    private openAddDefaultPaymentMethod() {
        this.setState({ isOpenAddDefaultPaymentMethod: true });
    }

    private closeAddDefaultPaymentMethod() {
        this.setState({ isOpenAddDefaultPaymentMethod: false });
    }

    private addDefaultPaymentMethod() {
        this.props.payOnline({ isAutoPay: true, isSbp: false });
    }

    private async deleteInvoice(invoiceId: string, invoiceNumber: string) {
        const { error } = await deleteInvoice(invoiceId);
        const { floatMessage: { show }, dispatchReceiveInvoicesListThunk, pagination } = this.props;

        if (error) {
            show(MessageType.Error, error);
        } else {
            show(MessageType.Success, `Счет ${ invoiceNumber } успешно удален`);
            dispatchReceiveInvoicesListThunk({ pageNumber: pagination.currentPage, force: true });
        }
    }

    private async closingDocuments(invoiceId: string, actId: string, file?: File) {
        this.setState({ isLoading: true });

        if (file) {
            const { success, message } = await postClosingDocuments({ invoiceId, actId, file });

            if (success) {
                this.props.floatMessage.show(MessageType.Success, 'Документ успешно отправлен!');

                this.props.dispatchReceiveInvoicesListThunk({ pageNumber: this.state.pageNumber, force: true, showLoadingProgress: true });
            } else {
                this.props.floatMessage.show(MessageType.Error, message);
            }
        }

        this.setState({ isLoading: false });
    }

    private async uploadAct(id: string) {
        document.getElementById(`closingDocument${ id }`)?.click();
    }

    private async downloadAct(actId: string) {
        this.setState({ isLoading: true });

        await getClosingDocumentFile(actId);

        this.setState({ isLoading: false });
    }

    private renderSignedDocument(data: InvoiceItemDataModel) {
        if ((data.RequiredSignature && data.Status !== 3 && data.Status !== 5) || (data.RequiredSignature && data.Status === 0)) {
            return (
                <Tooltip title="Загрузить подписанный документ">
                    <IconButton
                        onClick={ () => this.uploadAct(data.Id) }
                        sx={ { padding: 0, height: '100%' } }
                    >
                        <FileUploadOutlinedIcon fontSize="small" style={ { color: COLORS.link } } />
                        <input
                            id={ `closingDocument${ data.Id }` }
                            type="file"
                            hidden={ true }
                            onChange={ ({ target: { files } }) => this.closingDocuments(data.Id, data.ActId, files ? files[0] : undefined) }
                        />
                    </IconButton>
                </Tooltip>
            );
        }

        if (data.Status === 3) {
            return (
                <Tooltip title="Проверен менеджером">
                    <IconButton
                        onClick={ () => this.downloadAct(data.ActId) }
                        sx={ { padding: 0, height: '100%' } }
                    >
                        <ContactPageIcon fontSize="small" style={ { color: COLORS.link } } />
                    </IconButton>
                </Tooltip>
            );
        }

        if (data.Status === 4) {
            return (
                <Tooltip title="На проверке">
                    <IconButton
                        onClick={ () => this.downloadAct(data.ActId) }
                        sx={ { padding: 0, height: '100%' } }
                    >
                        <FindInPageIcon fontSize="small" style={ { color: COLORS.link } } />
                    </IconButton>
                </Tooltip>
            );
        }

        if (data.Status === 5) {
            return (
                <Tooltip title="Документ подписан">
                    <TaskIcon fontSize="small" style={ { color: COLORS.link } } />
                </Tooltip>
            );
        }

        return null;
    }

    public render() {
        const {
            dataset,
            pagination,
            currentSettings: { currentContextInfo: { currentUserGroups, userSource } },
            isDemo,
            locales,
            paymentMethods,
            currency
        } = this.props;

        return (
            <>
                { this.state.isLoading && <LoadingBounce /> }
                <CommonTableWithFilter
                    refreshId={ ERefreshId.balanceButton }
                    isResponsiveTable={ true }
                    isBorderStyled={ true }
                    uniqueContextProviderStateId="InvoiceListContextProviderStateId"
                    tableProps={ {
                        emptyText: 'Вы еще не выставляли счет на оплату',
                        dataset,
                        keyFieldName: 'Id',
                        pagination: {
                            ...pagination,
                            hideRecordsCount: true,
                            leftAlignContent: (
                                <Box display="flex" flexDirection="row" gap="10px">
                                    { userSource !== 'Cloud' && (
                                        <ContainedButton tourId={ ETourId.billingPageReplenishBalanceButton } kind="success">
                                            <Link to={ AppRoutes.accountManagement.replenishBalance }>
                                                Пополнить баланс
                                            </Link>
                                        </ContainedButton>
                                    ) }
                                    { /*{ !this.props.currentSettings.currentContextInfo.currentUserGroups.includes(AccountUserGroup.AccountAdmin) ||*/ }
                                    { /*    (!!this.props.currentSettings.currentContextInfo.isEmailVerified && !!this.props.currentSettings.currentContextInfo.isPhoneVerified)*/ }
                                    { /*    ? (*/ }
                                    { /*        <ContainedButton tourId={ ETourId.billingPageReplenishBalanceButton } kind="success">*/ }
                                    { /*            <Link to={ AppRoutes.accountManagement.replenishBalance }>*/ }
                                    { /*                Пополнить баланс*/ }
                                    { /*            </Link>*/ }
                                    { /*        </ContainedButton>*/ }
                                    { /*    )*/ }
                                    { /*    : (*/ }
                                    { /*        <ContainedButton*/ }
                                    { /*            tourId={ ETourId.billingPageReplenishBalanceButton }*/ }
                                    { /*            kind="success"*/ }
                                    { /*            onClick={ () => this.props.floatMessage.show(MessageType.Warning, 'Для пополнения счета верифицируйте номер телефона и почту') }*/ }
                                    { /*        >*/ }
                                    { /*            Пополнить баланс*/ }
                                    { /*        </ContainedButton>*/ }
                                    { /*    )*/ }
                                    { /*}*/ }
                                    { paymentMethods.filter(item => item.DefaultPaymentMethod).length === 0 &&
                                        locales === 'ru-ru' &&
                                        !isDemo &&
                                        userSource !== 'Cloud' && (
                                        <ContainedButton kind="success" onClick={ this.openAddDefaultPaymentMethod }>
                                            Добавить автоплатеж
                                        </ContainedButton>
                                    ) }
                                </Box>
                            )
                        },
                        fieldsView: {
                            Uniq: {
                                caption: '№ Счета',
                                fieldContentNoWrap: false,
                                format: (value: string, data) => (
                                    <Box display="flex" gap="10px" alignItems="center">
                                        <TextOut>{ value }</TextOut>
                                        <Tooltip title="Скачать">
                                            <MuiLink href={ apiHost(`billing-accounts/invoice-pdf/${ data.Id }?download=true`) } rel="noreferrer" target="_blank">
                                                <FileDownloadOutlinedIcon fontSize="small" style={ { color: COLORS.link } } />
                                            </MuiLink>
                                        </Tooltip>
                                        <Tooltip title="Распечатать">
                                            <Box display="flex" gap="10px" alignItems="center">
                                                <MuiLink style={ { lineHeight: 1 } } href={ apiHost(`billing-accounts/invoice-pdf/${ data.Id }`) } rel="noreferrer" target="_blank">
                                                    <PrintOutlinedIcon fontSize="small" style={ { color: COLORS.link } } />
                                                </MuiLink>
                                                { data.IsNewInvoice && <TextOut style={ { color: COLORS.main } }>!new</TextOut> }
                                            </Box>
                                        </Tooltip>
                                    </Box>
                                ),
                            },
                            InvoiceDate: {
                                caption: 'Дата создания',
                                fieldContentNoWrap: false,
                                format: (value: string) =>
                                    value ? <TextOut>{ DateUtility.dateToDayMonthYear(DateUtility.convertToDate(value)) }</TextOut> : '---'
                            },
                            InvoiceSum: {
                                caption: `Сумма (${ currency })`,
                                fieldContentNoWrap: false,
                                format: (value: number | null) =>
                                    value !== null ? NumberUtility.numberToMoneyFormat(value) : '---'
                            },
                            State: {
                                caption: 'Статус',
                                fieldContentNoWrap: false,
                                format: (value: number) =>
                                    <Chip color={ value === 1 ? 'primary' : 'info' } label={ value === 1 ? 'Oплачен' : 'Ожидает оплаты' } />
                            },
                            ActDescription: {
                                caption: 'Акты об оказании услуг',
                                fieldContentNoWrap: false,
                                format: (value: string, data) => (
                                    <>
                                        { data.ActId && (
                                            <Box display="flex" gap="10px">
                                                <TextOut>Акт: { value }</TextOut>
                                                <Tooltip title="Скачать">
                                                    <MuiLink href={ apiHost(`billing-accounts/download-act-pdf/${ data.ActId }`) } rel="noreferrer" target="_blank">
                                                        <FileDownloadOutlinedIcon fontSize="small" style={ { color: COLORS.link } } />
                                                    </MuiLink>
                                                </Tooltip>
                                                <Tooltip title="Распечатать">
                                                    <MuiLink href={ apiHost(`billing-accounts/open-act-pdf/${ data.ActId }`) } rel="noreferrer" target="_blank">
                                                        <PrintOutlinedIcon fontSize="small" style={ { color: COLORS.link } } />
                                                    </MuiLink>
                                                </Tooltip>
                                                { this.renderSignedDocument(data) }
                                            </Box>
                                        ) }
                                        { data.ReceiptFiscalNumber && (
                                            <Box display="flex" gap="10px">
                                                <TextOut>Чек № { data.ReceiptFiscalNumber }</TextOut>
                                                <Tooltip title="Скачать фискальный чек">
                                                    <MuiLink href={ apiHost(`billing-accounts/download-fiscal-receipt-pdf/${ data.Id }`) } rel="noreferrer" target="_blank">
                                                        <FileDownloadOutlinedIcon fontSize="small" style={ { color: COLORS.link } } />
                                                    </MuiLink>
                                                </Tooltip>
                                                <Tooltip title="Распечатать фискальный чек">
                                                    <MuiLink href={ apiHost(`billing-accounts/open-fiscal-receipt-pdf/${ data.Id }`) } rel="noreferrer" target="_blank">
                                                        <PrintOutlinedIcon fontSize="small" style={ { color: COLORS.link } } />
                                                    </MuiLink>
                                                </Tooltip>
                                            </Box>
                                        ) }
                                    </>
                                )
                            },
                            Id: {
                                caption: 'Действия',
                                format: (value: string, rowData) => {
                                    const hasEnoughRights = currentUserGroups.some(userGroup => [AccountUserGroup.AccountAdmin, AccountUserGroup.AccountSaleManager, AccountUserGroup.CloudAdmin].includes(userGroup));

                                    if (rowData.State !== 1 && hasEnoughRights) {
                                        return (
                                            <Box display="flex" alignItems="center">
                                                <Tooltip title="Удалить" onClick={ () => this.deleteInvoice(value, rowData.Uniq) }>
                                                    <DeleteOutlineIcon className={ styles.delete } fontSize="small" />
                                                </Tooltip>
                                            </Box>
                                        );
                                    }

                                    return null;
                                }
                            }
                        }
                    } }
                    onDataSelect={ this.onDataSelect }
                />
                { this.props.currentSettings.currentContextInfo.locale === 'ru-ru' && <ElectronicDocumentManagement /> }
                <Dialog
                    isOpen={ this.state.isOpenAddDefaultPaymentMethod }
                    isTitleSmall={ true }
                    title="Добавление платежного метода"
                    titleTextAlign="center"
                    dialogWidth="xs"
                    dialogVerticalAlign="center"
                    onCancelClick={ this.closeAddDefaultPaymentMethod }
                    buttons={
                        [
                            {
                                content: 'Отмена',
                                kind: 'default',
                                variant: 'contained',
                                onClick: this.closeAddDefaultPaymentMethod

                            },
                            {
                                content: 'Продолжить',
                                kind: 'success',
                                variant: 'contained',
                                onClick: this.addDefaultPaymentMethod
                            }
                        ]
                    }
                >
                    <DialogContentView />
                </Dialog>
            </>
        );
    }
}

export const ConnectedInvoicesDataViewClass = connect<StateProps, DispatchProps, {}, AppReduxStoreState>(
    ({
        BalanceManagementState: { invoiceList: { pagination: { currentPage, pageCount: totalPages, pageSize, totalCount: recordsCount }, records }, billingAccount },
        AutoPaymentState,
        Global: { getCurrentSessionSettingsReducer: { settings } }
    }) => ({
        dataset: records,
        currency: billingAccount.locale.currency,
        pagination: {
            currentPage,
            totalPages,
            pageSize,
            recordsCount
        },
        paymentMethods: AutoPaymentState.savedPaymentMethods,
        currentSettings: settings
    }), {
        dispatchReceiveInvoicesListThunk: ReceiveInvoiceListThunk.invoke,
        payOnline: PayOnlineThunk.invoke,
    }
)(withFloatMessages(InvoicesDataViewClass));