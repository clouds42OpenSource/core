import { COLORS } from 'app/utils';
import React from 'react';
import { hasComponentChangesFor } from 'app/common/functions';

type OwnProps = {
    show: boolean;
    onChange: (arg: boolean) => void
};

export class ShowServicesButton extends React.Component<OwnProps> {
    public shouldComponentUpdate(nextProps: OwnProps) {
        return hasComponentChangesFor(nextProps, this.props);
    }

    public render() {
        if (this.props.show) {
            return (
                <div style={ { fontSize: '13px', fontWeight: 'bold', color: COLORS.link, cursor: 'pointer', marginTop: '5px' } }>
                    <span onClick={ () => this.props.onChange(true) }>
                        Показать неактивные сервисы
                    </span>
                    <i className="fa fa-caret-down" />
                </div>
            );
        }

        return (
            <div style={ { fontSize: '13px', fontWeight: 'bold', color: COLORS.font, cursor: 'pointer', marginTop: '5px' } }>
                <span onClick={ () => this.props.onChange(false) }>
                    Свернуть неактивные сервисы
                </span>
                <i className="fa fa-caret-up" />
            </div>
        );
    }
}