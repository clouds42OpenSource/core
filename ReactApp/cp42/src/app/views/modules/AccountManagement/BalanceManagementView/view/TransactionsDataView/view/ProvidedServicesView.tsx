import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { DateUtility } from 'app/utils';
import { ProvidedServicesItemDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/recieveProvidedServices';
import React, { memo } from 'react';
import { SelectDataCommonDataModel } from 'app/web/common';
import { TablePagination } from 'app/views/modules/_common/components/CommonTableWithFilter/TableView';
import { TruncateText } from 'app/views/modules/AccountManagement/BalanceManagementView/view/TruncateText/TruncateTextView';

type TDataset = {
    data: ProvidedServicesItemDataModel[];
    pagination:TablePagination;
};

type TProps = {
    isMobileSized: boolean;
    dataset: TDataset;
    onDataSelect: (args: SelectDataCommonDataModel<object>) => void;
};

export const ProvidedServicesView = memo(({ dataset: { data, pagination }, onDataSelect, isMobileSized }: TProps) => {
    return (
        <CommonTableWithFilter
            isResponsiveTable={ true }
            isBorderStyled={ true }
            uniqueContextProviderStateId="ProvidedServicesContextProviderStateId"
            tableProps={ {
                dataset: data,
                keyFieldName: 'Rate',
                pagination,
                headerTitleTable: { text: 'Оказанные услуги' },
                isHistoricalRow: 'IsHistoricalRow',
                fieldsView: {
                    ServiceDescription: {
                        caption: 'Услуга',
                        fieldContentNoWrap: false,
                        fieldWidth: '20%',
                        format: (value: string) => {
                            return <TruncateText text={ value } isMobileSized={ isMobileSized } />;
                        }
                    },
                    ActiveFrom: {
                        caption: 'Активность с',
                        fieldContentNoWrap: false,
                        fieldWidth: '15%',
                        format: (value: string) => {
                            return value
                                ? <span>{ DateUtility.dateToDayMonthYear(DateUtility.convertToDate(value)) }</span>
                                : '----------';
                        }
                    },
                    ActiveTo: {
                        caption: 'Активность по',
                        fieldContentNoWrap: false,
                        fieldWidth: '15%',
                        format: (value: string) => {
                            return value
                                ? <span>{ DateUtility.dateToDayMonthYear(DateUtility.convertToDate(value)) }</span>
                                : '----------';
                        }
                    },
                    Count: {
                        caption: 'Кол-во',
                        style: { textAlign: 'start' },
                        fieldContentNoWrap: false,
                        fieldWidth: '10%'
                    },
                    Rate: {
                        caption: 'Тариф',
                        style: { textAlign: 'start' },
                        fieldContentNoWrap: false,
                        fieldWidth: '10%',
                        format: (value: boolean) => {
                            return value ? '1' : '0';
                        }
                    },
                    Source: {
                        caption: 'Источник',
                        style: { textAlign: 'start' },
                        fieldContentNoWrap: false,
                        fieldWidth: '10%',
                        format: (value: boolean) => {
                            return value ? 'Корп' : 'Ядро';
                        }
                    },
                    SposoredAccountName: {
                        caption: 'Спонсируемый аккаунт',
                        fieldContentNoWrap: false,
                        style: { textAlign: 'start', height: '35px' },
                        fieldWidth: '20%',
                    }
                }
            } }
            onDataSelect={ onDataSelect }
        />
    );
});