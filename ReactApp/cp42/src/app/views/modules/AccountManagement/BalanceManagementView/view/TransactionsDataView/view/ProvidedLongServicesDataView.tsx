import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { DateUtility } from 'app/utils';
import { ProvidedLongServicesItemDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/recieveProvidedLongServices';
import React, { memo } from 'react';
import { SelectDataCommonDataModel } from 'app/web/common';
import { TablePagination } from 'app/views/modules/_common/components/CommonTableWithFilter/TableView';
import { TruncateText } from 'app/views/modules/AccountManagement/BalanceManagementView/view/TruncateText/TruncateTextView';

type TDataset = {
    data: ProvidedLongServicesItemDataModel[];
    pagination:TablePagination;
};

type TProps = {
    isMobileSized: boolean;
    dataset: TDataset;
    onDataSelect: (args: SelectDataCommonDataModel<{}>) => void;
};

export const ProvidedLongServicesView = memo(({ isMobileSized, dataset: { data, pagination }, onDataSelect }: TProps) => {
    return (
        <CommonTableWithFilter
            isResponsiveTable={ true }
            isBorderStyled={ true }
            uniqueContextProviderStateId="ProvidedLongServicesContextProviderStateId"
            tableProps={ {
                headerTitleTable: { text: 'Длительные услуги' },
                dataset: data ?? [],
                keyFieldName: 'ServiceDescription',
                pagination,
                isHistoricalRow: 'IsHistoricalRow',
                fieldsView: {
                    ServiceDescription: {
                        caption: 'Услуга',
                        fieldWidth: '50%',
                        format: (value: string) =>
                            <TruncateText text={ value } isMobileSized={ isMobileSized } />
                    },
                    ActiveFrom: {
                        caption: 'Активность с',
                        fieldWidth: '20%',
                        format: (value: string) =>
                            value ? <span>{ DateUtility.dateToDayMonthYear(DateUtility.convertToDate(value)) }</span> : '----------'
                    },
                    ActiveTo: {
                        caption: 'Активность по',
                        fieldWidth: '20%',
                        format: (value: string) =>
                            value ? <span>{ DateUtility.dateToDayMonthYear(DateUtility.convertToDate(value)) }</span> : '----------'
                    },
                    Rate: {
                        caption: 'Тариф',
                        fieldWidth: '10%',
                        format: (value: boolean) =>
                            value ? '1' : '0'
                    },
                }
            } }
            onDataSelect={ onDataSelect }
        />
    );
});