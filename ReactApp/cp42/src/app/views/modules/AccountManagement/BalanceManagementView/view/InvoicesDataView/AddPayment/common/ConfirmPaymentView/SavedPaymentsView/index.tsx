import { Paper } from '@mui/material';
import { FETCH_API } from 'app/api/useFetchApi';
import { QuickPayThunkParams } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/QuickPayReducer/params';
import { QuickPayThunk } from 'app/modules/AccountManagement/BalanceManagement/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { TextOut } from 'app/views/components/TextOut';
import { PaymentItemView } from 'app/views/modules/AccountManagement/BalanceManagementView/view/InvoicesDataView/AddPayment/common/ConfirmPaymentView/SavedPaymentsView/PaymentItemView';
import { GetSavedPaymentMethods } from 'app/web/InterlayerApiProxy/AutoPaymentApiProxy/getSavedPaymentMethods';
import React from 'react';
import { connect } from 'react-redux';
import { NewPaymentButton } from './PaymentItemView/NewPaymentButton';

type OwnState = {
    paymentTypeID: string;
    savedPaymentMethod: GetSavedPaymentMethods[];
};

type OwnProps = {
    onNewPaymentClick: (isSbp?: boolean) => void
};

type StateProps = {
    data: GetSavedPaymentMethods[]
};

type DispatchProps = {
    dispatchQuickPay: (args: QuickPayThunkParams) => void
};

type AllProps = OwnProps & StateProps & DispatchProps & FloatMessageProps;

const { postQrPay } = FETCH_API.QR_PAY;

class SavedPaymentsViewClass extends React.Component<AllProps, OwnState> {
    constructor(props: AllProps) {
        super(props);
        this.state = {
            paymentTypeID: '',
            savedPaymentMethod: this.props.data
        };
        this.onClickPaymentType = this.onClickPaymentType.bind(this);
        this.onConfirmPayment = this.onConfirmPayment.bind(this);
        this.QuickPay = this.QuickPay.bind(this);
        this.deletePaymentMethod = this.deletePaymentMethod.bind(this);
    }

    private onClickPaymentType(id: string) {
        this.setState({ paymentTypeID: id });
    }

    private onConfirmPayment() {
        const query = new URLSearchParams(window.location.search).get('total-sum');

        if (this.state.paymentTypeID === 'new') {
            return this.props.onNewPaymentClick();
        }

        if (this.state.paymentTypeID === 'sbp') {
            return postQrPay({ totalSum: parseInt(query ?? '0', 10) }).then(response => {
                if (response.success && response.data) {
                    window.location.href = response.data.redirectUrl;
                } else {
                    this.props.floatMessage.show(MessageType.Error, response.message);
                }
            });
        }

        return this.QuickPay();
    }

    private deletePaymentMethod(paymentId: string) {
        this.setState(prevState => ({
            savedPaymentMethod: prevState.savedPaymentMethod.filter(item => item.Id !== paymentId)
        }));
    }

    private QuickPay() {
        const query = new URLSearchParams(window.location.search).get('total-sum');
        const isAuto = new URLSearchParams(window.location.search).get('is-auto');

        return this.props.dispatchQuickPay({
            paymentMethodId: this.state.paymentTypeID,
            totalSum: parseInt(query!, 10),
            isAutoPay: isAuto === 'true'
        });
    }

    public render() {
        return (
            <Paper
                sx={ { boxShadow: 3 } }
                style={ {
                    minWidth: '280px',
                    maxWidth: '460px',
                    margin: '0 auto',
                    padding: '32px 16px',
                    marginBottom: '16px'
                } }
            >
                <TextOut fontWeight={ 700 } fontSize={ 14 } textAlign="center">Выберите способ оплаты</TextOut>
                { this.state.savedPaymentMethod.map(item => (
                    <PaymentItemView
                        onClickDelete={ this.deletePaymentMethod }
                        { ...item }
                        key={ item.Id }
                        onClickPayment={ this.onClickPaymentType }
                        paymentID={ this.state.paymentTypeID }
                    />
                )) }
                <NewPaymentButton
                    onClickPayment={ this.onClickPaymentType }
                    paymentID={ this.state.paymentTypeID }
                    onConfirmPayment={ this.onConfirmPayment }
                />
            </Paper>
        );
    }
}

export const SavedPaymentsView = connect<StateProps, DispatchProps, OwnProps, AppReduxStoreState>(
    state => {
        return {
            data: state.AutoPaymentState.savedPaymentMethods
        };
    },
    {
        dispatchQuickPay: QuickPayThunk.invoke
    }
)(withFloatMessages(SavedPaymentsViewClass));