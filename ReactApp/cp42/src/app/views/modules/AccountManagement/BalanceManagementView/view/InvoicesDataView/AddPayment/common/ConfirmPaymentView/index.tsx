import ArrowBackOutlinedIcon from '@mui/icons-material/ArrowBackOutlined';
import { Box, IconButton } from '@mui/material';
import { AppRoutes } from 'app/AppRoutes';
import { hasComponentChangesFor } from 'app/common/functions';
import { GetSavedPaymentMethodsParams } from 'app/modules/AccountManagement/AutoPayment/store/reducers/getSavedPaymentMethods/params';
import { GetSavedPaymentMethodsThunk } from 'app/modules/AccountManagement/AutoPayment/store/thunks';
import { invoiceTypeEnum } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/createInvoiceReducer/params/CreateCustomInvoiceDataModelInputParams';
import { PayOnlineThunk, PayOnlineThunkParams } from 'app/modules/AccountManagement/BalanceManagement/store/thunks/PayOnlineThunk';
import { AppReduxStoreState } from 'app/redux/types';
import { withHeader } from 'app/views/components/_hoc/withHeader';
import { SavedPaymentsView } from 'app/views/modules/AccountManagement/BalanceManagementView/view/InvoicesDataView/AddPayment/common/ConfirmPaymentView/SavedPaymentsView';
import { YookassaWidget } from 'app/views/modules/AccountManagement/BalanceManagementView/view/InvoicesDataView/AddPayment/common/ConfirmPaymentView/YookassaWidget';
import { getParamsForCreateInvoice } from 'app/views/modules/AccountManagement/BalanceManagementView/view/InvoicesDataView/AddPayment/common/Functions';
import { GetSavedPaymentMethods } from 'app/web/InterlayerApiProxy/AutoPaymentApiProxy/getSavedPaymentMethods';
import { AccountUsersResponseDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/getAccountUsers';
import { InovoiceCalculatorDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/getInvoiceCalculator';
import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { RouteComponentProps } from 'react-router-dom';

type StateProps = {
    data: GetSavedPaymentMethods[];
    hasSavedPaymentMethods: boolean;
    InvoiceCalculatorStandard: InovoiceCalculatorDataModel;
    InvoiceCalculatorFasta: InovoiceCalculatorDataModel;
    users: AccountUsersResponseDataModel;
    locale: string;
};

type DispatchProps = {
    getSavedPaymentMethods: (args: GetSavedPaymentMethodsParams) => void;
    payOnline: (args: PayOnlineThunkParams) => void;
};

type AllProps = StateProps & DispatchProps & RouteComponentProps;

type OwnState = {
    isNewPayment: boolean;
};

class ConfirmPaymentViewClass extends React.Component<AllProps, OwnState> {
    public confirmationToken: string;

    public emailId: number;

    constructor(props: AllProps) {
        super(props);
        const query = new URLSearchParams(window.location.search).get('confirmation-token');

        this.emailId = Number(new URLSearchParams(window.location.search).get('email-id')?.trim()) ?? 0;

        this.confirmationToken = query ?? '';

        this.onClickNewPayment = this.onClickNewPayment.bind(this);
        this.onPayOnline = this.onPayOnline.bind(this);

        this.state = {
            isNewPayment: false,
        };
    }

    public componentDidMount() {
        if (!this.props.users.records[0] && !this.confirmationToken) {
            this.props.history.push(AppRoutes.accountManagement.balanceManagement);
        } else if (this.props.locale === 'ru-ru') {
            this.props.getSavedPaymentMethods({});
        } else {
            this.onPayOnline();
        }
    }

    public componentDidUpdate() {
        if (!this.props.hasSavedPaymentMethods) this.onPayOnline();
    }

    public shouldComponentUpdate(prevProps: AllProps, prevState: OwnState) {
        return hasComponentChangesFor(prevProps, this.props) || hasComponentChangesFor(this.state, prevState);
    }

    private onClickNewPayment(isSbp?: boolean) {
        this.onPayOnline(isSbp);
    }

    private onPayOnline(isSbp?: boolean) {
        const { InvoiceCalculatorStandard: invoiceStandard, InvoiceCalculatorFasta: invoiceFasta } = this.props;
        const { users } = this.props;
        const type = new URLSearchParams(window.location.search).get('type');

        switch (type) {
            case 'user':
                const invoiceStandardProducts = getParamsForCreateInvoice(invoiceStandard.products);
                this.props.payOnline({
                    createInvoiceParams: {
                        AccountId: invoiceStandard.accountId,
                        NotifyAccountUserId: users.records[this.emailId].id,
                        PayPeriod: invoiceStandard.payPeriod,
                        TotalAfterVAT: invoiceStandard.totalAfterVAT,
                        TotalBeforeVAT: invoiceStandard.totalBeforeVAT,
                        TotalBonus: invoiceStandard.totalBonus,
                        VAT: invoiceStandard.VAT,
                        Products: invoiceStandardProducts,
                        type: invoiceTypeEnum.subscriptionServices
                    },
                    isAutoPay: false,
                    isSbp: isSbp ?? false
                });
                break;
            case 'additional':
                const invoiceFastaProducts = getParamsForCreateInvoice(invoiceFasta.products);
                this.props.payOnline({
                    createInvoiceParams: {
                        AccountId: invoiceFasta.accountId,
                        NotifyAccountUserId: users.records[this.emailId].id,
                        PayPeriod: invoiceFasta.payPeriod,
                        TotalAfterVAT: invoiceFasta.totalAfterVAT,
                        TotalBeforeVAT: invoiceFasta.totalBeforeVAT,
                        TotalBonus: invoiceFasta.totalBonus,
                        VAT: invoiceFasta.VAT,
                        Products: invoiceFastaProducts,
                        type: invoiceTypeEnum.fasta
                    },
                    isAutoPay: false,
                    isSbp: isSbp ?? false
                });
                break;
            case 'custom':
                const description = new URLSearchParams(window.location.search).get('description');
                const total = new URLSearchParams(window.location.search).get('total-sum');

                this.props.payOnline({
                    createCustomInvoiceParams: {
                        Amount: Number(total),
                        Description: description || '',
                        NotifyAccountUserId: users.records[this.emailId].id
                    },
                    isAutoPay: false,
                    isSbp: isSbp ?? false
                });
                break;
            default:
                break;
        }
    }

    public render() {
        return (
            <Box padding="32px 0">
                { this.confirmationToken
                    ? (
                        <>
                            { !!this.props.data.length &&
                            <IconButton style={ { outline: 'none' } } onClick={ () => this.onClickNewPayment() }>
                                <ArrowBackOutlinedIcon />
                            </IconButton> }
                            <YookassaWidget token={ this.confirmationToken } />
                        </>
                    )
                    : <SavedPaymentsView onNewPaymentClick={ this.onClickNewPayment } />
                }
            </Box>
        );
    }
}

const ConnectedConfirmPaymentViewClass = connect<StateProps, DispatchProps, {}, AppReduxStoreState>(
    state => {
        const store = state;
        return {
            locale: store.Global.getCurrentSessionSettingsReducer.settings.currentContextInfo.locale,
            InvoiceCalculatorStandard: store.BalanceManagementState.invoiceCalculatorStandard,
            InvoiceCalculatorFasta: store.BalanceManagementState.invoiceCalculatorFasta,
            data: store.AutoPaymentState.savedPaymentMethods,
            hasSavedPaymentMethods: store.AutoPaymentState.hasSuccessFor.hasSavedPaymentMethods,
            users: store.BalanceManagementState.users
        };
    },
    {
        payOnline: PayOnlineThunk.invoke,
        getSavedPaymentMethods: GetSavedPaymentMethodsThunk.invoke,
    }
)(withRouter(ConfirmPaymentViewClass));

export const ConfirmPaymentComponent = withHeader({
    page: ConnectedConfirmPaymentViewClass,
    title: 'Подтверждение оплаты'
});