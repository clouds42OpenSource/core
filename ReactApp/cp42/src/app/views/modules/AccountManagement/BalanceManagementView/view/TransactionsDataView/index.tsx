import { AccountUserGroup } from 'app/common/enums';
import { AddTransactionParams } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/AddTransactionReducer/params';
import { ReceiveProvidedLongServicesThunkParams } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/recieveProvidedLongServicesReducer/params';
import { ReceiveProvidedServicesThunkParams } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/recieveProvidedServicesReducer/params';
import { ReceiveTransactionsThunkParams } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/recieveTransactionsReducer/params';
import { ReceiveTransactionsThunk } from 'app/modules/AccountManagement/BalanceManagement/store/thunks';
import { AddTransactionThunk } from 'app/modules/AccountManagement/BalanceManagement/store/thunks/AddTransactionThunk';
import { ProvidedLongServicesThunk } from 'app/modules/AccountManagement/BalanceManagement/store/thunks/ProvidedLongServicesThunk';
import { ProvidedServicesThunk } from 'app/modules/AccountManagement/BalanceManagement/store/thunks/ProvidedServicesThunk';
import { AppReduxStoreState } from 'app/redux/types';
import withWindowDimensions, { IWithWindowDimensionsProps } from 'app/views/components/_hoc/withWindowDimensions';
import { TransactionsFilterDataForm } from 'app/views/modules/AccountManagement/BalanceManagementView/types/TransactionsFilterDataForm';
import { AddTransactionView } from 'app/views/modules/AccountManagement/BalanceManagementView/view/TransactionsDataView/view/AddTransactionFormClass';
import { ProvidedLongServicesView } from 'app/views/modules/AccountManagement/BalanceManagementView/view/TransactionsDataView/view/ProvidedLongServicesDataView';
import { ProvidedServicesView } from 'app/views/modules/AccountManagement/BalanceManagementView/view/TransactionsDataView/view/ProvidedServicesView';
import { TransactionsListDataView } from 'app/views/modules/AccountManagement/BalanceManagementView/view/TransactionsDataView/view/TransactionsListDataView';
import { SelectDataInitiator } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { TablePagination } from 'app/views/modules/_common/components/CommonTableWithFilter/TableView';
import { ProvidedLongServicesItemDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/recieveProvidedLongServices';
import { ProvidedServicesItemDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/recieveProvidedServices';
import { TransactionsItemDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/recieveTransactions';
import { SelectDataCommonDataModel } from 'app/web/common';
import { AccountPermissionsEnum } from 'app/web/common/enums/AccountPermissionsEnum';
import React from 'react';
import { connect } from 'react-redux';

type StateProps = {
    permissions: string[];
    currency: string;
    currentUserGroups: Array<AccountUserGroup>;
    dataset: {
        has: boolean;
        transactions: {
            data: TransactionsItemDataModel[];
            pagination: TablePagination;
        };
        providedServices: {
            data: ProvidedServicesItemDataModel[];
            pagination: TablePagination;
        };
        providedLongServices: {
            data: ProvidedLongServicesItemDataModel[];
            pagination: TablePagination;
        };
    };
};

type DispatchProps = {
    dispatchReceiveTransactionsThunk: (args?: ReceiveTransactionsThunkParams) => void;
    dispatchReceiveProvidedServicesThunk: (args?: ReceiveProvidedServicesThunkParams) => void;
    dispatchProvidedLongServicesThunk: (args?: ReceiveProvidedLongServicesThunkParams) => void;
    dispatchAddTransaction: (args: AddTransactionParams) => void;
};

type OwnProps = IWithWindowDimensionsProps;

type AllProps = OwnProps & StateProps & DispatchProps;

type OwnState = {
    isOpenTransaction: boolean;
};

class TransactionsDataViewClass extends React.Component<AllProps, OwnState> {
    public constructor(props: AllProps) {
        super(props);
        this.onDataSelect = this.onDataSelect.bind(this);
        this.state = {
            isOpenTransaction: false
        };
        this.changeTransaction = this.changeTransaction.bind(this);
        this.onServicesPaginationChange = this.onServicesPaginationChange.bind(this);
        this.onLongServicesPaginationChange = this.onLongServicesPaginationChange.bind(this);
    }

    public componentDidMount() {
        const { dispatchReceiveTransactionsThunk, dispatchReceiveProvidedServicesThunk, dispatchProvidedLongServicesThunk, currentUserGroups } = this.props;
        dispatchReceiveTransactionsThunk({ pageNumber: 1, showLoadingProgress: true, force: true });

        if (currentUserGroups.some(userGroup => [AccountUserGroup.AccountSaleManager, AccountUserGroup.CloudAdmin, AccountUserGroup.Hotline].includes(userGroup))) {
            dispatchReceiveProvidedServicesThunk({ pageNumber: 1, showLoadingProgress: false, force: true });
            dispatchProvidedLongServicesThunk({ pageNumber: 1, showLoadingProgress: false, force: true });
        }
    }

    private onServicesPaginationChange({ pageNumber }: SelectDataCommonDataModel<{}>) {
        this.props.dispatchReceiveProvidedServicesThunk({ pageNumber, force: true });
    }

    private onLongServicesPaginationChange({ pageNumber }: SelectDataCommonDataModel<{}>) {
        this.props.dispatchReceiveProvidedServicesThunk({ pageNumber, force: true });
    }

    private onDataSelect({ pageNumber, filter }: SelectDataCommonDataModel<TransactionsFilterDataForm>, _: string | null, initiator: SelectDataInitiator) {
        const { dispatchReceiveTransactionsThunk, dispatchReceiveProvidedServicesThunk } = this.props;

        if (initiator === 'page-changed') {
            return dispatchReceiveTransactionsThunk({ pageNumber, filter, force: true });
        }

        dispatchReceiveProvidedServicesThunk({ showLoadingProgress: false, pageNumber: 1, filter, force: true });
        dispatchReceiveTransactionsThunk({ pageNumber, filter, force: true });
    }

    private changeTransaction() {
        this.setState(prevState => ({ isOpenTransaction: !prevState.isOpenTransaction }));
    }

    public render() {
        const { permissions, currency, dataset: { transactions, providedServices, providedLongServices }, windowDimensions, dispatchAddTransaction } = this.props;

        return (
            <>
                <TransactionsListDataView
                    hasPermission={ permissions }
                    currency={ currency }
                    dataset={ transactions }
                    onDataSelect={ this.onDataSelect }
                    changeTransaction={ this.changeTransaction }
                    isMobileSized={ windowDimensions.isMobileSized }
                    accountUserGroup={ this.props.currentUserGroups }
                />
                { permissions.includes(AccountPermissionsEnum.ProvidedServicesView) && (
                    <ProvidedServicesView
                        onDataSelect={ this.onServicesPaginationChange }
                        dataset={ providedServices }
                        isMobileSized={ windowDimensions.isMobileSized }
                    />
                ) }
                <ProvidedLongServicesView
                    dataset={ providedLongServices }
                    isMobileSized={ windowDimensions.isMobileSized }
                    onDataSelect={ this.onLongServicesPaginationChange }
                />
                <AddTransactionView
                    changeTransaction={ this.changeTransaction }
                    isVisible={ this.state.isOpenTransaction }
                    onAddTransaction={ dispatchAddTransaction }
                />
            </>
        );
    }
}

const ConnectedTransactionsDataViewClass = connect<StateProps, DispatchProps, {}, AppReduxStoreState>(
    ({ BalanceManagementState: { billingAccount, providedServicesList, transactionsList, hasSuccessFor, providedLongServicesList }, Global }) => ({
        permissions: Global.getUserPermissionsReducer.permissions,
        currency: billingAccount.locale.currency,
        currentUserGroups: Global.getCurrentSessionSettingsReducer.settings.currentContextInfo.currentUserGroups,
        dataset: {
            has: hasSuccessFor.hasTransactionsListRecieved,
            transactions: {
                data: transactionsList.records,
                pagination: {
                    currentPage: transactionsList.pagination.currentPage,
                    totalPages: transactionsList.pagination.pageCount,
                    pageSize: transactionsList.pagination.pageSize,
                    recordsCount: transactionsList.pagination.totalCount
                },
            },
            providedServices: {
                data: providedServicesList.records,
                pagination: {
                    currentPage: providedServicesList.pagination.currentPage,
                    totalPages: providedServicesList.pagination.pageCount,
                    pageSize: providedServicesList.pagination.pageSize,
                    recordsCount: providedServicesList.pagination.totalCount
                },
            },
            providedLongServices: {
                data: providedLongServicesList.records,
                pagination: {
                    currentPage: providedLongServicesList.pagination.currentPage,
                    totalPages: providedLongServicesList.pagination.pageCount,
                    pageSize: providedLongServicesList.pagination.pageSize,
                    recordsCount: providedLongServicesList.pagination.totalCount
                },
            },
        }
    }), {
        dispatchReceiveTransactionsThunk: ReceiveTransactionsThunk.invoke,
        dispatchReceiveProvidedServicesThunk: ProvidedServicesThunk.invoke,
        dispatchProvidedLongServicesThunk: ProvidedLongServicesThunk.invoke,
        dispatchAddTransaction: AddTransactionThunk.invoke,
    }
)(TransactionsDataViewClass);

export const TransactionsView = withWindowDimensions(ConnectedTransactionsDataViewClass);