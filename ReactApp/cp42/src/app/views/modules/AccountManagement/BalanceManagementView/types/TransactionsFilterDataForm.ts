import { TransactionsFiltertStatus } from 'app/common/enums/BalanceManagement';

export type TransactionsFilterDataForm = {
    start: Date;
    end: Date;
    type: TransactionsFiltertStatus;
};