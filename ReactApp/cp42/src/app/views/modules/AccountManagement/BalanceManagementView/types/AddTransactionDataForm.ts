export type AddTransactionDataForm = {
    /**
    * Сумма
    */
    Total: string;
    /**
    * Тип платежа
    */
    Type: number;
    /**
    * Тип транзакции
    */
    TransactionType: number;
    /**
    * Дата
    */
    Date: Date;
    /**
    * Описание
    */
    Description: string;
};