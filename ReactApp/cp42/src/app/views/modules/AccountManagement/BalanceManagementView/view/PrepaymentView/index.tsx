import { Box } from '@mui/material';
import { TextOut } from 'app/views/components/TextOut';
import { BillingAccountDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/getBilingAccountData';
import { ContainedButton } from 'app/views/components/controls/Button';
import { Dialog } from 'app/views/components/controls/Dialog';
import { Component } from 'react';
import { Link } from 'react-router-dom';
import { AppRoutes } from 'app/AppRoutes';

type TProps = {
   data: BillingAccountDataModel;
   onProlong: () => void;
   onRepay: () => void;
};

type TState = {
    isOpenRePayPromisePayment: boolean;
};

export class PrepaymentView extends Component<TProps, TState> {
    public constructor(props: TProps) {
        super(props);
        this.openDialog = this.openDialog.bind(this);
        this.closeDialog = this.closeDialog.bind(this);
        this.state = {
            isOpenRePayPromisePayment: false
        };
    }

    private closeDialog() {
        this.setState({ isOpenRePayPromisePayment: false });
    }

    private openDialog() {
        this.setState({ isOpenRePayPromisePayment: true });
    }

    public render() {
        const { data } = this.props;

        if (data.promisePayment.canTakePromisePayment) {
            return (
                <Box display="flex" flexDirection="column" gap="5px">
                    <TextOut>Обещанный платеж позволит мгновенно зачислить деньги на баланс аккаунта.</TextOut>
                    <TextOut>В течение 7 дней необходимо будет погасить задолженность во избежание блокировки сервиса.</TextOut>
                    <ContainedButton style={ { width: 'fit-content' } } kind="success">
                        <Link to={ `${ AppRoutes.accountManagement.replenishBalance }?isPromisePayment=true` }>
                            Активировать обещанный платеж
                        </Link>
                    </ContainedButton>
                </Box>
            );
        }

        return (
            <Box display="flex" flexDirection="column" gap="5px" flexWrap="wrap">
                <TextOut>{ data.promisePayment.blockReason }</TextOut>
                <Box display="flex" gap="10px" marginTop="10px">
                    { data.canEarlyPromisePaymentRepay &&
                        <ContainedButton kind="default" onClick={ this.openDialog }>
                            Погасить обещанный платеж
                        </ContainedButton>
                    }
                    { data.canProlongPromisePayment &&
                        <ContainedButton kind="default" onClick={ () => this.props.onProlong() }>
                            Продлить обещанный платеж
                        </ContainedButton>
                    }
                    { data.promisePayment.canIncreasePromisePayment &&
                        <ContainedButton kind="default">
                            <Link to={ `${ AppRoutes.accountManagement.replenishBalance }?isPromisePaymentIncrease=true` }>
                                Увеличить обещанный платеж
                            </Link>
                        </ContainedButton>
                    }
                </Box>
                <Dialog
                    dialogVerticalAlign="center"
                    isOpen={ this.state.isOpenRePayPromisePayment }
                    dialogWidth="xs"
                    title="Подтвердите погашение «Обещанного платежа»"
                    isTitleSmall={ true }
                    onCancelClick={ this.closeDialog }
                    buttons={ [
                        {
                            kind: 'default',
                            variant: 'contained',
                            onClick: this.closeDialog,
                            content: 'Отмена'
                        },
                        {
                            kind: 'success',
                            variant: 'contained',
                            onClick: () => {
                                this.props.onRepay();
                                setTimeout(() => {
                                    window.location.reload();
                                }, 1000);
                            },
                            content: 'Ок'
                        }
                    ] }
                >
                    <Box display="flex" justifyContent="center">
                        <TextOut textAlign="center">Вы действительно хотите досрочно погасить обещанный платеж?</TextOut>
                    </Box>
                </Dialog>
            </Box>
        );
    }
}