import { Box, Checkbox } from '@mui/material';
import { TextOut } from 'app/views/components/TextOut';
import { GetSavedPaymentMethods } from 'app/web/InterlayerApiProxy/AutoPaymentApiProxy/getSavedPaymentMethods';
import { ImageUtility } from 'app/utils/ImageUtility';
import RadioButtonCheckedIcon from '@mui/icons-material/RadioButtonChecked';
import RadioButtonUncheckedIcon from '@mui/icons-material/RadioButtonUnchecked';
import DeleteIcon from '@mui/icons-material/Delete';
import React from 'react';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { RequestKind } from 'core/requestSender/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { Dialog } from 'app/views/components/controls/Dialog';
import { COLORS } from 'app/utils';

type OwnProps = GetSavedPaymentMethods & {
    paymentID: string;
    onClickPayment: (id: string) => void;
    onClickDelete: (id: string) => void;
};

type OwnState = {
    confirmDeletePaymentMethod: boolean;
};

class PaymentItemClass extends React.Component<OwnProps & FloatMessageProps, OwnState> {
    constructor(props: OwnProps & FloatMessageProps) {
        super(props);
        this.state = {
            confirmDeletePaymentMethod: false
        };

        this.deleteSavedPaymentMethod = this.deleteSavedPaymentMethod.bind(this);
        this.openConfirmDialog = this.openConfirmDialog.bind(this);
        this.closeConfirmDialog = this.closeConfirmDialog.bind(this);
    }

    private openConfirmDialog() {
        this.setState({
            confirmDeletePaymentMethod: true
        });
    }

    private closeConfirmDialog() {
        this.setState({
            confirmDeletePaymentMethod: false
        });
    }

    private deleteSavedPaymentMethod(id: string) {
        const api = InterlayerApiProxy.getAutoPaymentApiProxy();

        try {
            void api.deletePaymentMethods(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, { id });
            this.setState({ confirmDeletePaymentMethod: false });
            this.props.onClickDelete(id);
        } catch (er: unknown) {
            this.props.floatMessage.show(MessageType.Error, 'При удаление сохраненного метода платежа произошла ошибка');
        }
    }

    public render() {
        return (
            <>
                <Box
                    alignItems="center"
                    display="flex"
                    width="95%"
                    gap="10px"
                    border={ `1px solid ${ COLORS.border }` }
                    padding="8px"
                    marginBottom="8px"
                    borderRadius="4px"
                    style={ { cursor: 'pointer' } }
                    onClick={ () => { this.props.onClickPayment(this.props.Id); } }
                >
                    <img style={ { width: '35px' } } src={ ImageUtility.getImageSrc(this.props) } alt="" />
                    { this.props.BankCardDetails
                        ? (
                            <Box display="flex" flexDirection="column">
                                <TextOut fontWeight={ 700 }>**** **** **** { this.props.BankCardDetails.LastFourDigits }</TextOut>
                                <TextOut fontSize={ 12 }>
                                    Действует до: { this.props.BankCardDetails.ExpiryMonth }/{ this.props.BankCardDetails.ExpiryYear.toString().split('').map((item, index) => index > 1 ? item : undefined).join('') }
                                </TextOut>
                            </Box>
                        )
                        : (
                            <Box display="flex" flexDirection="column">
                                <TextOut fontWeight={ 700 }>{ this.props.Title }</TextOut>
                                <TextOut fontSize={ 12 }>{ this.props.Type }</TextOut>
                            </Box>
                        )
                    }
                    <Box display="flex" gap="5px" marginLeft="auto" alignItems="center">
                        <DeleteIcon color="error" onClick={ this.openConfirmDialog } />
                        <Checkbox
                            checked={ this.props.Id === this.props.paymentID }
                            checkedIcon={ <RadioButtonCheckedIcon color="primary" /> }
                            icon={ <RadioButtonUncheckedIcon /> }
                            onClick={ () => { this.props.onClickPayment(this.props.Id); } }
                        />
                    </Box>
                </Box>
                <Dialog
                    isTitleSmall={ true }
                    title="Подтверждение удаления метода оплаты"
                    isOpen={ this.state.confirmDeletePaymentMethod }
                    dialogWidth="xs"
                    dialogVerticalAlign="center"
                    onCancelClick={ this.closeConfirmDialog }
                    buttons={ [
                        {
                            content: 'Отмена',
                            kind: 'default',
                            onClick: this.closeConfirmDialog,
                            variant: 'text'
                        },
                        {
                            content: 'Удалить',
                            kind: 'error',
                            onClick: () => this.deleteSavedPaymentMethod(this.props.paymentID),
                            variant: 'contained'
                        }
                    ] }
                >
                    <Box display="flex" flexDirection="column" gap="5px">
                        <TextOut>Вы действительно хотите удалить сохраненную карту?</TextOut>
                        <TextOut fontWeight={ 700 }>После удаления подключить данную карту на автоплатеж невозможно!</TextOut>
                    </Box>
                </Dialog>
            </>
        );
    }
}

export const PaymentItemView = withFloatMessages(PaymentItemClass);