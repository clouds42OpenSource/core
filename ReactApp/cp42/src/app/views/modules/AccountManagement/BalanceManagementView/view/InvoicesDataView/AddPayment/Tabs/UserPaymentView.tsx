import { Checkbox, FormControlLabel } from '@mui/material';
import { hasComponentChangesFor } from 'app/common/functions';
import { Nullable } from 'app/common/types';
import { ActivatePromisePaymentParams } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/ActivatePromisePaymentReducer/params';
import { CreateInvoiceInputParams } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/createInvoiceReducer/params';
import { RecalculateInvoiceStandardParams } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/RecalculateInvoiceStandardReducer/params';
import { ActivatePromisePaymentThunk } from 'app/modules/AccountManagement/BalanceManagement/store/thunks/ActivatePromisePaymentThunk';
import { CreateInvoiceThunk } from 'app/modules/AccountManagement/BalanceManagement/store/thunks/CreateInvoiceThunk';
import { RecalculateInvoiceStandardThunk } from 'app/modules/AccountManagement/BalanceManagement/store/thunks/RecalculateInvoiceStandardThunk';
import { AppReduxStoreState } from 'app/redux/types';
import { NumberUtility } from 'app/utils';
import { FilterItemWrapper } from 'app/views/components/_hoc/tableFilterWrapper/filterItemWrapper';
import { ComboBoxForm } from 'app/views/components/controls/forms';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { ETourId } from 'app/views/Layout/ProjectTour/enums';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { CounterView } from 'app/views/modules/AccountManagement/BalanceManagementView/view/InvoicesDataView/AddPayment/common/CounterView';
import { ChangedValues, getAccountEmails, getMonthList, getParamsForCreateInvoice, getParamsForRecalculateProducts } from 'app/views/modules/AccountManagement/BalanceManagementView/view/InvoicesDataView/AddPayment/common/Functions';
import { PaymentFooterView } from 'app/views/modules/AccountManagement/BalanceManagementView/view/InvoicesDataView/AddPayment/common/PaymentFooterView';
import { ShowServicesButton } from 'app/views/modules/AccountManagement/BalanceManagementView/view/InvoicesDataView/AddPayment/common/ShowServicesButton';
import { AccountUsersResponseDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/getAccountUsers';
import { InovoiceCalculatorDataModel, Services } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/getInvoiceCalculator';
import React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps, withRouter } from 'react-router-dom';

type DispatchProps = {
    recalculateInvoiceStandard: (args: RecalculateInvoiceStandardParams) => void;
    createInvoice: (args: CreateInvoiceInputParams & { type: string }) => void;
    activatePromisePayment: (args: ActivatePromisePaymentParams) => void;
};

type StateProps = {
    invoiceCalculator: InovoiceCalculatorDataModel,
    users: AccountUsersResponseDataModel;
};

type OwnProps = {
    additionalCompanyId: string;
};

type OwnState = {
    isShownServices: boolean;
    emailToSendNotification: {
        id: Nullable<string>
        index: number;
    };

};
type AllProps = StateProps & DispatchProps & RouteComponentProps & OwnProps;

class UserPaymentsViewClass extends React.Component<AllProps, OwnState> {
    constructor(props: AllProps) {
        super(props);
        this.onEmailChange = this.onEmailChange.bind(this);
        this.onServiceShow = this.onServiceShow.bind(this);
        this.createInvoice = this.createInvoice.bind(this);
        this.onRecalculate = this.onRecalculate.bind(this);
        this.activatePromisePayment = this.activatePromisePayment.bind(this);
        this.state = {
            isShownServices: false,
            emailToSendNotification: {
                id: null,
                index: 0,
            },
        };
    }

    public shouldComponentUpdate(nextProps: AllProps, nexState: OwnState) {
        return hasComponentChangesFor(nextProps, this.props) || hasComponentChangesFor(this.state, nexState);
    }

    public componentDidUpdate(prevProps: AllProps, _: OwnState) {
        if (hasComponentChangesFor(prevProps.users.records, this.props.users.records)) {
            const { records } = this.props.users;
            this.setState({
                emailToSendNotification: {
                    id: records[0].id,
                    index: 0
                }
            });
        }
    }

    private onEmailChange(_: string, newValue?: number | null) {
        if (!newValue && newValue !== 0) {
            this.setState({
                emailToSendNotification: {
                    id: null,
                    index: 0
                }
            });
            return;
        }
        const { records } = this.props.users;
        const userID = records[newValue].id;
        this.setState({
            emailToSendNotification: {
                id: userID,
                index: newValue
            }
        });
    }

    private onServiceShow(val: boolean) {
        this.setState({ isShownServices: val });
    }

    private onRecalculate(args?: ChangedValues) {
        const { accountId, products, payPeriod } = this.props.invoiceCalculator;
        this.props.recalculateInvoiceStandard({
            AccountId: accountId,
            PayPeriod: args?.Period ?? payPeriod,
            Products: getParamsForRecalculateProducts(products, { ...args })
        });
    }

    private createInvoice() {
        const invoice = this.props.invoiceCalculator;
        const { id } = this.state.emailToSendNotification;
        const products = getParamsForCreateInvoice(invoice.products);
        this.props.createInvoice({
            showLoadingProgress: true,
            AccountId: invoice.accountId,
            NotifyAccountUserId: id,
            PayPeriod: invoice.payPeriod,
            TotalAfterVAT: invoice.totalAfterVAT,
            TotalBeforeVAT: invoice.totalBeforeVAT,
            TotalBonus: invoice.totalBonus,
            VAT: invoice.VAT,
            Products: products,
            type: 'subscription-services',
            AccountAdditionalCompanyId: this.props.additionalCompanyId
        });
    }

    private activatePromisePayment() {
        const invoice = this.props.invoiceCalculator;
        const { id } = this.state.emailToSendNotification;
        const products = getParamsForCreateInvoice(invoice.products);
        this.props.activatePromisePayment({
            common: {
                AccountId: invoice.accountId,
                NotifyAccountUserId: id,
                PayPeriod: invoice.payPeriod,
                TotalAfterVAT: invoice.totalAfterVAT,
                TotalBeforeVAT: invoice.totalBeforeVAT,
                TotalBonus: invoice.totalBonus,
                VAT: invoice.VAT,
                Products: products,
                type: 'subscription-services'
            }
        });
    }

    public render() {
        const { products, payPeriod, PayPeriodControl, currency, totalBonus } = this.props.invoiceCalculator;
        const periodControl = getMonthList(PayPeriodControl.Settings);
        const Products: Services[] = this.state.isShownServices ? products.sort((a, b) => a.isInitialActive - b.isInitialActive) : products.filter(item => item.isInitialActive === 1);

        return (
            <div>
                <CommonTableWithFilter
                    filterProps={ {
                        getFilterContentView: () =>
                            <FilterItemWrapper width="370px">
                                <div data-tourid={ ETourId.replenishBalancePagePaymentPeriod }>
                                    <FormAndLabel label="Период оплаты">
                                        <ComboBoxForm
                                            formName="periodControl"
                                            items={ periodControl }
                                            value={ payPeriod }
                                            onValueChange={ (_, val) => this.onRecalculate({ Period: val }) }
                                        />
                                    </FormAndLabel>
                                </div>
                            </FilterItemWrapper>
                    } }
                    isResponsiveTable={ true }
                    isBorderStyled={ true }
                    uniqueContextProviderStateId="PaymentServicesTableView"
                    tableProps={ {
                        dataset: Products,
                        keyFieldName: 'description',
                        fieldsView: {
                            name: {
                                caption: 'Сервис',
                                fieldContentNoWrap: false,
                                fieldWidth: '20%',
                                format: (value: string, data: Services) => {
                                    return (<FormControlLabel
                                        sx={ { margin: '0', padding: '0' } }
                                        control={ <Checkbox
                                            size="small"
                                            checked={ data.isActive }
                                            onChange={ (e, checked) => this.onRecalculate({ IsActive: checked, Identifier: data.identifier }) }
                                        /> }
                                        label={ <span style={ { fontSize: '13px' } }>{ (this.props.invoiceCalculator.supplierName.includes('ООО "ЭР Софт"') && value === 'Аренда 1С') ? 'Лицензионное ПО' : value }</span> }
                                    />);
                                }
                            },
                            description: {
                                caption: 'Содержание услуги',
                                fieldContentNoWrap: false,
                                fieldWidth: '20%'
                            },
                            quantity: {
                                caption: 'Количество',
                                fieldContentNoWrap: false,
                                fieldWidth: '13%',
                                format: (value: string, data: Services) => {
                                    return <CounterView product={ data } quantity={ value } onChange={ this.onRecalculate } />;
                                },
                                style: { textAlign: 'center' }
                            },
                            payPeriod: {
                                caption: 'Период, мес',
                                fieldContentNoWrap: false,
                                fieldWidth: '13%',
                                format: (value: string) => {
                                    return (<span key={ value }>{ value }</span>);
                                },
                                style: { textAlign: 'center' }
                            },
                            rate: {
                                caption: 'Цена за ед.',
                                fieldContentNoWrap: false,
                                fieldWidth: '13%',
                                style: { textAlign: 'end' },
                                format: (value: string) => {
                                    return (<span>{ NumberUtility.numberToMoneyFormat(Number(value)) }</span>);
                                }
                            },
                            totalPrice: {
                                caption: `Сумма, ${ currency.Currency }`,
                                fieldContentNoWrap: false,
                                fieldWidth: totalBonus ? '13%' : '30%',
                                format: (val: number) => {
                                    return <span style={ { textAlign: 'center' } }>{ NumberUtility.numberToMoneyFormat(val) }</span>;
                                },
                                style: { textAlign: 'end' }
                            },
                            totalBonus: {
                                caption: totalBonus ? `Бонус, ${ currency.Currency }` : '',
                                fieldContentNoWrap: false,
                                fieldWidth: '13%',
                                style: { textAlign: 'end' },
                                format: (value: string) => {
                                    return (
                                        value
                                            ? <span>{ NumberUtility.numberToMoneyFormat(Number(value)) }</span>
                                            : <span />
                                    );
                                }
                            },
                        }
                    } }
                />
                <ShowServicesButton show={ !this.state.isShownServices } onChange={ this.onServiceShow } />
                <PaymentFooterView
                    type="user"
                    TotalAfterVAT={ this.props.invoiceCalculator.totalAfterVAT }
                    TotalBeforeVAT={ this.props.invoiceCalculator.totalBeforeVAT }
                    TotalBonus={ this.props.invoiceCalculator.totalBonus }
                    VAT={ this.props.invoiceCalculator.VAT }
                    onCreateInvoice={ this.createInvoice }
                    currentEmail={ this.state.emailToSendNotification.index }
                    emails={ getAccountEmails(this.props.users.records) }
                    onEmailChange={ this.onEmailChange }
                    onActivatePromisePayment={ this.activatePromisePayment }
                />
            </div>
        );
    }
}

export const UserPaymentsView = connect<StateProps, DispatchProps, OwnProps, AppReduxStoreState>(
    state => {
        const store = state.BalanceManagementState;
        return {
            invoiceCalculator: store.invoiceCalculatorStandard,
            users: store.users
        };
    },
    {
        recalculateInvoiceStandard: RecalculateInvoiceStandardThunk.invoke,
        createInvoice: CreateInvoiceThunk.invoke,
        activatePromisePayment: ActivatePromisePaymentThunk.invoke
    }
)(withRouter(UserPaymentsViewClass));