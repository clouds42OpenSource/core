declare const styles: {
  readonly 'footer-wrapper': string;
  readonly 'summary-block': string;
  readonly 'summary-block-total': string;
  readonly 'button-wrapper': string;
  readonly 'link': string;
  readonly 'input-number': string;
  readonly 'counter-input': string;
  readonly 'icon': string;
  readonly 'empty-icon': string;
  readonly 'counter': string;
};

export = styles;