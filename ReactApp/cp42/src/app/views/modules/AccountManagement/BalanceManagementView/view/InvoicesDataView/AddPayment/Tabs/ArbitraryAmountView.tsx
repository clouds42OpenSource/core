import { TextField } from '@mui/material';
import { AppConsts } from 'app/common/constants';
import { hasComponentChangesFor } from 'app/common/functions';
import { Nullable } from 'app/common/types';
import { ActivatePromisePaymentParams } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/ActivatePromisePaymentReducer/params';
import { CreateCustomInvoiceDataModelInputParams } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/createInvoiceReducer/params/CreateCustomInvoiceDataModelInputParams';
import { GetAccountUsersParams } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/GetAccountUsersReducer/params';
import { ActivatePromisePaymentThunk } from 'app/modules/AccountManagement/BalanceManagement/store/thunks/ActivatePromisePaymentThunk';
import { CreateCustomInvoiceThunk } from 'app/modules/AccountManagement/BalanceManagement/store/thunks/CreateCustomInvoiceThunk';
import { GetAccountUsersThunk } from 'app/modules/AccountManagement/BalanceManagement/store/thunks/GetAccountUsersThunk';
import { AppReduxStoreState } from 'app/redux/types';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { getAccountEmails, getArbitraryPaymentFields } from 'app/views/modules/AccountManagement/BalanceManagementView/view/InvoicesDataView/AddPayment/common/Functions';
import { PaymentFooterView } from 'app/views/modules/AccountManagement/BalanceManagementView/view/InvoicesDataView/AddPayment/common/PaymentFooterView';
import { AccountPermissionsEnum } from 'app/web/common/enums/AccountPermissionsEnum';
import { AccountUsersResponseDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/getAccountUsers';
import { InovoiceCalculatorDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/getInvoiceCalculator';
import cn from 'classnames';
import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { RouteComponentProps } from 'react-router-dom';
import css from '../common/styles.module.css';

type DispatchProps = {
    onCreateInvoice: (args: CreateCustomInvoiceDataModelInputParams) => void;
    activatePromisePayment: (args: ActivatePromisePaymentParams) => void;
    getAccountUsers: (args: GetAccountUsersParams) => void;
};

type StateProps = {
    permissions: string[];
    invoiceCalculator: InovoiceCalculatorDataModel;
    users: AccountUsersResponseDataModel;
};

type OwnState = {
    TotalPay: string;
    Description: string;
    emailToSendNotification: {
        id: Nullable<string>
        index: number;
    };
};

type AllProps = StateProps & DispatchProps & FloatMessageProps & RouteComponentProps;

class ArbitiraryAmountViewClass extends React.Component<AllProps, OwnState> {
    private totalPayRef = React.createRef<HTMLInputElement>();

    private descriptionRef = React.createRef<HTMLInputElement>();

    constructor(props: AllProps) {
        super(props);
        this.onEmailChange = this.onEmailChange.bind(this);
        this.onChangeTotalOrDescription = this.onChangeTotalOrDescription.bind(this);
        this.createInvoice = this.createInvoice.bind(this);
        this.activatePromisePayment = this.activatePromisePayment.bind(this);
        const amount = new URLSearchParams(window.location.search).get('customPayment.amount');
        const description = new URLSearchParams(window.location.search).get('customPayment.description');
        this.state = {
            TotalPay: amount ?? '',
            Description: description ?? '',
            emailToSendNotification: {
                id: null,
                index: 0,
            },
        };
    }

    public componentDidMount() {
        this.props.getAccountUsers({
            pageNumber: 1,
            orderBy: 'login.asc',
            showLoadingProgress: true,
            force: true
        });
        this.totalPayRef.current?.focus();
        this.descriptionRef.current?.focus();
    }

    public shouldComponentUpdate(nextProps: AllProps, nextState: OwnState) {
        return hasComponentChangesFor(this.props, nextProps) || hasComponentChangesFor(this.state, nextState);
    }

    public componentDidUpdate(prevProps: AllProps, prevState: OwnState) {
        if (hasComponentChangesFor(prevState.TotalPay, this.state.TotalPay)) {
            this.totalPayRef.current?.focus();
        }

        if (hasComponentChangesFor(prevState.Description, this.state.Description)) {
            this.descriptionRef.current?.focus();
        }

        if (hasComponentChangesFor(prevProps.users.records, this.props.users.records)) {
            const { records } = this.props.users;
            this.setState({
                emailToSendNotification: {
                    id: records[0].id,
                    index: 0
                }
            });
        }
    }

    private onChangeTotalOrDescription(value: string, name: string) {
        if (name === 'Total') {
            if (Number(value) < 0 || Number(value) > AppConsts.maxSummToPay) {
                return;
            }

            this.setState({ TotalPay: Number(value).toString() });
        }

        if (name === 'Description') this.setState({ Description: value });
    }

    private onEmailChange(formName: string, newValue?: number | null) {
        if (!newValue && newValue !== 0) {
            this.setState({
                emailToSendNotification: {
                    id: null,
                    index: 0
                }
            });

            return;
        }

        const { records } = this.props.users;
        const userID = records[newValue].id;
        this.setState({
            emailToSendNotification: {
                id: userID,
                index: newValue
            }
        });
    }

    private createInvoice() {
        const { Description, TotalPay, emailToSendNotification } = this.state;
        this.props.onCreateInvoice({
            showLoadingProgress: true,
            Amount: Number(TotalPay),
            Description,
            NotifyAccountUserId: emailToSendNotification.id
        });
    }

    private activatePromisePayment(type?: number) {
        const { TotalPay, Description, emailToSendNotification } = this.state;
        const payload = {
            Amount: Number(TotalPay),
            Description,
            NotifyAccountUserId: emailToSendNotification.id
        };

        if (type !== 2) {
            this.props.activatePromisePayment({
                custom: payload
            });
        } else {
            this.props.activatePromisePayment({
                increase: payload
            });
        }
    }

    public render() {
        const { Currency } = this.props.invoiceCalculator.currency;
        const readOnly = !this.props.permissions.includes(AccountPermissionsEnum.ChangeDescription);

        let vat = 0;

        if (Currency === 'грн') {
            vat = Number(this.state.TotalPay) * 0.2;
        } else if (Currency === 'тг') {
            vat = (Number(this.state.TotalPay) / 1.12) * 0.12;
        }

        return (
            <div style={ { paddingTop: '16px' } }>
                <CommonTableWithFilter
                    isBorderStyled={ true }
                    isResponsiveTable={ true }
                    uniqueContextProviderStateId="ArbitraryAmountView"
                    tableProps={ {
                        dataset: getArbitraryPaymentFields(this.props.invoiceCalculator.supplierName.includes('УчетОнлайн')),
                        keyFieldName: 'descService',
                        isResponsiveTable: true,
                        fieldsView: {
                            service: {
                                caption: 'Услуга',
                                fieldWidth: '30%',
                                fieldContentNoWrap: false,
                            },
                            descService: {
                                caption: 'Содержание услуги',
                                fieldWidth: '50%',
                                fieldContentNoWrap: false,
                                format: (_: string) =>
                                    <TextField
                                        size="small"
                                        fullWidth={ true }
                                        type="text"
                                        style={ { padding: '8px 0' } }
                                        inputRef={ this.descriptionRef }
                                        value={ this.state.Description }
                                        disabled={ readOnly }
                                        onChange={ e => this.onChangeTotalOrDescription(e.target.value, 'Description') }
                                        inputProps={ { style: { padding: '8px', fontSize: '13px', backgroundColor: readOnly ? '#ebebeb' : '#fffff' } } }
                                        InputLabelProps={ { shrink: false } }
                                        label={ null }
                                    />
                            },
                            sum: {
                                caption: `Сумма, ${ Currency }`,
                                fieldWidth: '20%',
                                fieldContentNoWrap: false,
                                format: (_: string) =>
                                    <TextField
                                        fullWidth={ true }
                                        inputRef={ this.totalPayRef }
                                        type="number"
                                        style={ { padding: '8px 0' } }
                                        size="small"
                                        onKeyDown={ e => ['e', 'E', '+', '-'].includes(e.key) && e.preventDefault() }
                                        value={ this.state.TotalPay }
                                        onChange={ e => this.onChangeTotalOrDescription(e.target.value, 'Total') }
                                        inputProps={ { min: 0, className: cn(css['input-number']) } }
                                        InputLabelProps={ { shrink: false } }
                                        label={ null }
                                    />
                            }
                        }
                    } }
                />
                <PaymentFooterView
                    TotalBeforeVAT={ Number(this.state.TotalPay) }
                    TotalAfterVAT={ Number(this.state.TotalPay) + vat }
                    VAT={ vat }
                    onCreateInvoice={ this.createInvoice }
                    description={ this.state.Description }
                    type="custom"
                    emails={ getAccountEmails(this.props.users.records) }
                    currentEmail={ this.state.emailToSendNotification.index }
                    onEmailChange={ this.onEmailChange }
                    onActivatePromisePayment={ this.activatePromisePayment }
                />
            </div>
        );
    }
}

export const ArbitiraryAmountView = connect<StateProps, DispatchProps, object, AppReduxStoreState>(
    state => {
        const store = state.BalanceManagementState;
        return {
            permissions: state.Global.getUserPermissionsReducer.permissions,
            invoiceCalculator: store.invoiceCalculatorStandard,
            users: store.users
        };
    },
    {
        onCreateInvoice: CreateCustomInvoiceThunk.invoke,
        getAccountUsers: GetAccountUsersThunk.invoke,
        activatePromisePayment: ActivatePromisePaymentThunk.invoke
    }
)(withFloatMessages(withRouter(ArbitiraryAmountViewClass)));