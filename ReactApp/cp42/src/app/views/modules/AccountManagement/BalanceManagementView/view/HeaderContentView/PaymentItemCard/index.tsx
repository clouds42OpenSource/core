import { Box, Paper } from '@mui/material';
import ClearOutlinedIcon from '@mui/icons-material/ClearOutlined';
import { COLORS } from 'app/utils';
import { Dialog } from 'app/views/components/controls/Dialog';
import { TextOut } from 'app/views/components/TextOut';
import { GetSavedPaymentMethods } from 'app/web/InterlayerApiProxy/AutoPaymentApiProxy/getSavedPaymentMethods';
import { ImageUtility } from 'app/utils/ImageUtility';
import React from 'react';
import ReportOutlinedIcon from '@mui/icons-material/ReportOutlined';
import { hasComponentChangesFor } from 'app/common/functions';

type OwnProps = GetSavedPaymentMethods & {
    onDelete: (id: string) => void;
};

type OwnState = {
    deletePayment: boolean;
};

export class PaymentItemCard extends React.Component<OwnProps, OwnState> {
    constructor(props: OwnProps) {
        super(props);

        this.openDialog = this.openDialog.bind(this);
        this.closeDialog = this.closeDialog.bind(this);

        this.state = {
            deletePayment: false
        };
    }

    public shouldComponentUpdate(prevProps: OwnProps, prevState: OwnState) {
        return hasComponentChangesFor(prevProps, this.props) || hasComponentChangesFor(prevState, this.state);
    }

    private closeDialog() {
        this.setState({
            deletePayment: false
        });
    }

    private openDialog() {
        this.setState({
            deletePayment: true
        });
    }

    public render() {
        return (
            <Paper
                style={ {
                    width: '100%',
                    padding: '8px',
                    borderRadius: '3px',
                    border: `1px solid ${ COLORS.border }`,
                    display: 'flex',
                    boxShadow: 'none',
                    gap: '10px'
                } }
            >
                <img style={ { width: '30px' } } src={ ImageUtility.getImageSrc(this.props) } alt="" />
                { this.props.BankCardDetails
                    ? (
                        <Box display="flex" flexDirection="column">
                            <TextOut fontWeight={ 700 }>**** **** **** { this.props.BankCardDetails.LastFourDigits }</TextOut>
                            <TextOut fontSize={ 12 }>
                                Действует до: { this.props.BankCardDetails.ExpiryMonth }/{ this.props.BankCardDetails.ExpiryYear.toString().split('').map((item, index) => index > 1 ? item : undefined).join('') }
                            </TextOut>
                        </Box>
                    )
                    : (
                        <Box display="flex" flexDirection="column">
                            <TextOut fontWeight={ 700 }>{ this.props.Title }</TextOut>
                            <TextOut fontSize={ 12 }>{ this.props.Type }</TextOut>
                        </Box>
                    )
                }
                <ClearOutlinedIcon onClick={ this.openDialog } style={ { marginLeft: 'auto' } } />
                <Dialog
                    dialogWidth="xs"
                    isTitleSmall={ true }
                    title="Удаление способа оплаты"
                    isOpen={ this.state.deletePayment }
                    onCancelClick={ this.closeDialog }
                    dialogVerticalAlign="center"
                    buttons={ [
                        {
                            variant: 'contained',
                            kind: 'default',
                            onClick: this.closeDialog,
                            content: 'Отмена'
                        },
                        {
                            variant: 'outlined',
                            kind: 'error',
                            onClick: () => {
                                this.closeDialog();
                                this.props.onDelete(this.props.Id);
                            },
                            content: 'Удалить'
                        }
                    ] }
                >
                    <Box display="flex" flexDirection="column" alignItems="center" gap="10px">
                        <ReportOutlinedIcon style={ { color: COLORS.warning, fontSize: '100px' } } />
                        <TextOut>
                            { this.props.BankCardDetails
                                ? `Карта (**** **** **** ${ this.props.BankCardDetails.LastFourDigits })`
                                : `Способ оплаты(${ this.props.Type }) будет удалена. Что бы снова привязать, вам нужно будет повторно провести оплату и указать галочку "Разрешить автоплатеж"`
                            }
                        </TextOut>
                    </Box>
                </Dialog>
            </Paper>
        );
    }
}