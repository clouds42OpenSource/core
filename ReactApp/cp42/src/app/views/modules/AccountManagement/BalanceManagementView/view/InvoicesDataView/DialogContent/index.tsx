import { Box } from '@mui/material';
import React from 'react';
import { TextOut } from 'app/views/components/TextOut';

export class DialogContentView extends React.PureComponent {
    public render() {
        return (
            <Box display="flex" flexDirection="column">
                <TextOut>
                    Ежемесячный платеж будет автоматически списываться с вашего счета за день до окончания текущего периода.
                </TextOut>
                <TextOut>
                    По-своему усмотрению, вы сможете удалить платежный метод на странице &quot;Баланс&quot;.
                </TextOut>
            </Box>
        );
    }
}