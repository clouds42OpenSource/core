import { Box, Checkbox, Divider } from '@mui/material';
import { useAppSelector } from 'app/hooks';
import { OutlinedButton, SuccessButton } from 'app/views/components/controls/Button';
import AddOutlinedIcon from '@mui/icons-material/AddOutlined';
import { COLORS, NumberUtility } from 'app/utils';
import RadioButtonCheckedIcon from '@mui/icons-material/RadioButtonChecked';
import RadioButtonUncheckedIcon from '@mui/icons-material/RadioButtonUnchecked';
import { TextOut } from 'app/views/components/TextOut';
import React from 'react';
import { useHistory } from 'react-router';
import { AppRoutes } from 'app/AppRoutes';
import QrCode2Icon from '@mui/icons-material/QrCode2';

type OwnProps = {
    paymentID: string;
    onClickPayment: (id: string) => void;
    onConfirmPayment: () => void;
};

export const NewPaymentButton = ({ onConfirmPayment, paymentID, onClickPayment }: OwnProps) => {
    const { locale } = useAppSelector(state => state.Global.getCurrentSessionSettingsReducer.settings.currentContextInfo);

    const history = useHistory();

    const query = new URLSearchParams(window.location.search).get('total-sum');
    const message = 'Платеж отменен';

    return (
        <>
            <Box
                alignItems="center"
                display="flex"
                width="95%"
                gap="10px"
                border={ `1px solid ${ COLORS.border }` }
                padding="8px"
                marginBottom="8px"
                borderRadius="4px"
                style={ { cursor: 'pointer' } }
                onClick={ () => onClickPayment('new') }
            >
                <AddOutlinedIcon color="primary" />
                <Box display="flex" flexDirection="column" alignItems="center">
                    <TextOut fontWeight={ 700 }>Новый способ оплаты</TextOut>
                    { locale === 'ru-ru' && <TextOut fontSize={ 12 }>Новая карта, СберКлик и др.</TextOut> }
                </Box>
                <Checkbox
                    style={ { marginLeft: 'auto' } }
                    onClick={ () => onClickPayment('new') }
                    checked={ paymentID === 'new' }
                    checkedIcon={ <RadioButtonCheckedIcon color="primary" /> }
                    icon={ <RadioButtonUncheckedIcon /> }
                />
            </Box>
            {
                locale === 'ru-ru' && (
                    <Box
                        alignItems="center"
                        display="flex"
                        width="95%"
                        gap="10px"
                        border={ `1px solid ${ COLORS.border }` }
                        padding="8px"
                        marginBottom="8px"
                        borderRadius="4px"
                        style={ { cursor: 'pointer' } }
                        onClick={ () => onClickPayment('sbp') }
                    >
                        <QrCode2Icon />
                        <Box display="flex" flexDirection="column" alignItems="center">
                            <TextOut fontWeight={ 700 }>Оплатить через QR код</TextOut>
                        </Box>
                        <Checkbox
                            style={ { marginLeft: 'auto' } }
                            onClick={ () => onClickPayment('sbp') }
                            checked={ paymentID === 'sbp' }
                            checkedIcon={ <RadioButtonCheckedIcon color="primary" /> }
                            icon={ <RadioButtonUncheckedIcon /> }
                        />
                    </Box>
                )
            }
            <Divider />
            <Box display="flex" justifyContent="flex-end" gap="8px" marginTop="8px">
                <OutlinedButton onClick={ () => history.push(`${ AppRoutes.accountManagement.balanceManagement }?message=${ message }`) }>
                    Отмена
                </OutlinedButton>
                <SuccessButton isEnabled={ paymentID !== '' } onClick={ () => onConfirmPayment() }>
                    Оплатить { NumberUtility.numberToMoneyFormat(Number(query)) }
                </SuccessButton>
            </Box>
        </>
    );
};