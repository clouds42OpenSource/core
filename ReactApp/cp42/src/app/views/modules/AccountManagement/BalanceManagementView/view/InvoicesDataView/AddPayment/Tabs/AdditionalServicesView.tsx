import { hasComponentChangesFor } from 'app/common/functions';
import { Nullable } from 'app/common/types';
import { ActivatePromisePaymentParams } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/ActivatePromisePaymentReducer/params';
import { CreateInvoiceInputParams } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/createInvoiceReducer/params';
import { RecalculateInvoiceFastaParams } from 'app/modules/AccountManagement/BalanceManagement/store/reducers/recalculateInvoiceFastaReducer/params';
import { ActivatePromisePaymentThunk } from 'app/modules/AccountManagement/BalanceManagement/store/thunks/ActivatePromisePaymentThunk';
import { CreateInvoiceThunk } from 'app/modules/AccountManagement/BalanceManagement/store/thunks/CreateInvoiceThunk';
import { RecalculateInvoiceFastaThunk } from 'app/modules/AccountManagement/BalanceManagement/store/thunks/RecalculateInvoiceFastaThunk';
import { AppReduxStoreState } from 'app/redux/types';
import { NumberUtility } from 'app/utils';
import { CounterView } from 'app/views/modules/AccountManagement/BalanceManagementView/view/InvoicesDataView/AddPayment/common/CounterView';
import { ChangedValues, getAccountEmails, getParamsForCreateInvoice, getParamsForRecalculateProducts } from 'app/views/modules/AccountManagement/BalanceManagementView/view/InvoicesDataView/AddPayment/common/Functions';
import { PaymentFooterView } from 'app/views/modules/AccountManagement/BalanceManagementView/view/InvoicesDataView/AddPayment/common/PaymentFooterView';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { AccountUsersResponseDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/getAccountUsers';
import { InovoiceCalculatorDataModel, Services } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/getInvoiceCalculator';
import React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps, withRouter } from 'react-router-dom';

type StateProps = {
    InvoiceCalculator: InovoiceCalculatorDataModel;
    users: AccountUsersResponseDataModel;
};

type DispatchProps = {
    recalculateInvoiceFasta: (args: RecalculateInvoiceFastaParams) => void;
    createInvoice: (args: CreateInvoiceInputParams & { type: string }) => void;
    activatePromisePayment: (args: ActivatePromisePaymentParams) => void;
};

type AllProps = StateProps & DispatchProps & RouteComponentProps;

type OwnState = {
    emailToSendNotification: {
        id: Nullable<string>
        index: number;
    };
};

export class AdditionalServicesViewClass extends React.Component<AllProps, OwnState> {
    constructor(props: AllProps) {
        super(props);
        this.onEmailChange = this.onEmailChange.bind(this);
        this.onValueChange = this.onValueChange.bind(this);
        this.createInvoice = this.createInvoice.bind(this);
        this.activatePromisePayment = this.activatePromisePayment.bind(this);
        this.state = {
            emailToSendNotification: {
                id: null,
                index: 0,
            },
        };
    }

    public shouldComponentUpdate(nextProps: AllProps, nextState: OwnState) {
        return hasComponentChangesFor(nextProps, this.props) || hasComponentChangesFor(this.state, nextState);
    }

    private onValueChange(args?: ChangedValues) {
        const { accountId, products } = this.props.InvoiceCalculator;
        this.props.recalculateInvoiceFasta({
            AccountId: accountId,
            PayPeriod: 1,
            Products: getParamsForRecalculateProducts(products, { ...args })
        });
    }

    private onEmailChange(_: string, newValue?: number | null) {
        if (!newValue && newValue !== 0) {
            this.setState({
                emailToSendNotification: {
                    id: null,
                    index: 0
                }
            });
            return;
        }
        const { records } = this.props.users;
        const userID = records[newValue].id;
        this.setState({
            emailToSendNotification: {
                id: userID,
                index: newValue
            }
        });
    }

    private createInvoice() {
        const invoice = this.props.InvoiceCalculator;
        const { id } = this.state.emailToSendNotification;
        const products = getParamsForCreateInvoice(invoice.products);
        this.props.createInvoice({
            AccountId: invoice.accountId,
            NotifyAccountUserId: id,
            PayPeriod: invoice.payPeriod,
            TotalAfterVAT: invoice.totalAfterVAT,
            TotalBeforeVAT: invoice.totalBeforeVAT,
            TotalBonus: invoice.totalBonus,
            VAT: invoice.VAT,
            Products: products,
            type: 'fasta',
        });
    }

    private activatePromisePayment() {
        const invoice = this.props.InvoiceCalculator;
        const { id } = this.state.emailToSendNotification;
        const products = getParamsForCreateInvoice(invoice.products);
        this.props.activatePromisePayment({
            showLoadingProgress: true,
            common: {
                AccountId: invoice.accountId,
                NotifyAccountUserId: id,
                PayPeriod: invoice.payPeriod,
                TotalAfterVAT: invoice.totalAfterVAT,
                TotalBeforeVAT: invoice.totalBeforeVAT,
                TotalBonus: invoice.totalBonus,
                VAT: invoice.VAT,
                Products: products,
                type: 'fasta'
            }
        });
    }

    public render() {
        const { products, currency } = this.props.InvoiceCalculator;

        return (
            <div style={ { paddingTop: '16px' } }>
                <CommonTableWithFilter
                    uniqueContextProviderStateId="AdditionalServices"
                    isResponsiveTable={ true }
                    isBorderStyled={ true }
                    tableProps={ {
                        dataset: products,
                        keyFieldName: 'description',
                        fieldsView: {
                            name: {
                                caption: 'Сервис',
                                fieldWidth: '20%',
                                fieldContentNoWrap: false
                            },
                            description: {
                                caption: 'Содержание услуги',
                                fieldWidth: '20%',
                                fieldContentNoWrap: false
                            },
                            quantity: {
                                caption: 'Количество',
                                fieldWidth: '20%',
                                fieldContentNoWrap: false,
                                style: { textAlign: 'center' },
                                format: (value: string, data: Services) =>
                                    <CounterView product={ data } quantity={ value } onChange={ this.onValueChange } />
                            },
                            rate: {
                                caption: 'Цена за ед.',
                                fieldWidth: '20%',
                                fieldContentNoWrap: false,
                                style: { textAlign: 'end' },
                                format: (value: number) =>
                                    <span>{ NumberUtility.numberToMoneyFormat(value) }</span>
                            },
                            totalPrice: {
                                caption: `Сумма, ${ currency.Currency }`,
                                fieldWidth: '20%',
                                fieldContentNoWrap: false,
                                style: { textAlign: 'end' },
                                format: (value: number) =>
                                    <span>{ NumberUtility.numberToMoneyFormat(value) }</span>
                            }
                        }
                    } }
                />
                <PaymentFooterView
                    type="additional"
                    TotalAfterVAT={ this.props.InvoiceCalculator.totalAfterVAT }
                    TotalBeforeVAT={ this.props.InvoiceCalculator.totalBeforeVAT }
                    TotalBonus={ this.props.InvoiceCalculator.totalBonus }
                    VAT={ this.props.InvoiceCalculator.VAT }
                    onCreateInvoice={ this.createInvoice }
                    emails={ getAccountEmails(this.props.users.records) }
                    currentEmail={ this.state.emailToSendNotification.index }
                    onEmailChange={ this.onEmailChange }
                    onActivatePromisePayment={ this.activatePromisePayment }
                />
            </div>
        );
    }
}

export const AdditionalServicesView = connect<StateProps, DispatchProps, NonNullable<unknown>, AppReduxStoreState>(
    state => {
        const store = state.BalanceManagementState;
        return {
            InvoiceCalculator: store.invoiceCalculatorFasta,
            users: store.users
        };
    },
    {
        recalculateInvoiceFasta: RecalculateInvoiceFastaThunk.invoke,
        createInvoice: CreateInvoiceThunk.invoke,
        activatePromisePayment: ActivatePromisePaymentThunk.invoke
    }
)(withRouter(AdditionalServicesViewClass));