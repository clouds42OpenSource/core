import { TextOut } from 'app/views/components/TextOut';
import React from 'react';
import { connect } from 'react-redux';
import { Box, Tooltip } from '@mui/material';
import { hasComponentChangesFor } from 'app/common/functions';
import { DeletePaymentMethodInputParams } from 'app/modules/AccountManagement/AutoPayment/store/reducers/deletePaymentMethod/params';
import { DeletePaymentMethodThunk } from 'app/modules/AccountManagement/AutoPayment/store/thunks/DeletePaymentMethodThunk';
import { AppReduxStoreState } from 'app/redux/types';
import { InfoCard } from 'app/views/modules/_common/shared/Cards/infoCard';
import { PaymentItemCard } from 'app/views/modules/AccountManagement/BalanceManagementView/view/HeaderContentView/PaymentItemCard';
import { GetSavedPaymentMethods } from 'app/web/InterlayerApiProxy/AutoPaymentApiProxy/getSavedPaymentMethods';
import { BillingAccountDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/getBilingAccountData';

import styles from './style.module.css';

type StateProps = {
    accountData: BillingAccountDataModel;
    paymentMethods: GetSavedPaymentMethods[];
    isLoading: boolean;
};

type DispatchProps = {
    deletePaymentMethod: (args: DeletePaymentMethodInputParams) => void;
};

type AllProps = StateProps & DispatchProps;

class HeaderContentViewClass extends React.Component<AllProps> {
    public constructor(props: AllProps) {
        super(props);

        this.onDeleteMethod = this.onDeleteMethod.bind(this);
    }

    public shouldComponentUpdate(prevProps: AllProps) {
        return hasComponentChangesFor(prevProps, this.props);
    }

    private onDeleteMethod(id: string) {
        this.props.deletePaymentMethod({ id });
    }

    public render() {
        const { isLoading, accountData } = this.props;
        const { currentBalance, currentBonusBalance, regularPayment, locale: { currency } } = accountData;
        const defaultPaymentMethod = this.props.paymentMethods.filter(item => item.DefaultPaymentMethod);

        return (
            <>
                <Box display="flex" gap="10px" flexWrap="wrap" className={ styles.cards } alignItems="center">
                    <InfoCard title="Баланс:" description={ `${ currentBalance } ${ currency }` } isLoading={ isLoading } />
                    <InfoCard
                        isLoading={ isLoading }
                        title={
                            <Box display="flex" gap="2px">
                                <TextOut fontWeight={ 600 } fontSize={ 12 }>Бонусы: </TextOut>
                                <Tooltip title="Бонусы начисляются за оплату сервисов Аренда 1С и Мой Диск на длительный период. Списание с бонусного счета происходит по окончанию средств основного счета">
                                    <i className="fa fa-info-circle" style={ { fontSize: '11px', marginTop: '3px' } } />
                                </Tooltip>
                            </Box>
                        }
                        description={ `${ currentBonusBalance } ${ currency }` }
                    />
                    <InfoCard title="Ежемесячный платеж:" description={ `${ regularPayment } ${ currency }` } isLoading={ isLoading } />
                </Box>
                <Box marginY="10px" display="flex">
                    { defaultPaymentMethod.map(item => <PaymentItemCard { ...item } key={ item.Id } onDelete={ this.onDeleteMethod } />) }
                </Box>
            </>
        );
    }
}

export const HeaderContentView = connect<StateProps, DispatchProps, object, AppReduxStoreState>(
    state => ({
        accountData: state.BalanceManagementState.billingAccount,
        paymentMethods: state.AutoPaymentState.savedPaymentMethods,
        isLoading: !state.BalanceManagementState.hasSuccessFor.hasBillingAccountRecieved
    }),
    {
        deletePaymentMethod: DeletePaymentMethodThunk.invoke
    }
)(HeaderContentViewClass);