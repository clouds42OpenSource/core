import { ComboboxItemModel } from 'app/views/components/controls/forms/ComboBox/models';
import { ArbitiraryAmountType } from 'app/views/modules/AccountManagement/BalanceManagementView/types/ArbitiraryAmountType';
import { CreateInvoiceProduct } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/createInvoice/input-params/CreateInvoiceProduct';
import { AccountUsersItemDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/getAccountUsers';
import { Services, Setting } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/getInvoiceCalculator';
import { RecalculateProductType } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/recalculateInvoice';
import { AccountUserGroupEnum } from 'app/web/common/enums/AccountUserGroupEnum';

export type ChangedValues = {
    /** Идентификатор продукта */
    Identifier?: string;
    /** Новое значение количества продукта */
    Quantity?: number;
    /** Активность продукта true|false */
    IsActive?: boolean;
    /** Смена длительности периода */
    Period?: number;
};

/**
 * Функция для получения массива сервисов для калькуляции
 * @param products массив всех сервисов для филтрации и получения активных
 * @param changedValues поля с изменениями || если есть изменения то Identifier обязателен
 * @returns возвращает массив типа RecalculateProductType с новыми данными из параметров changedValues
 */
export function getParamsForRecalculateProducts(products: Services[], changedValues?: ChangedValues): RecalculateProductType[] {
    const id = changedValues ? changedValues.Identifier : '';
    const activeProducts = products.filter(item => item.isActive && item.identifier !== id).map(item => mapProductFor(item)!);
    if (changedValues && !changedValues.Period) {
        const changedProduct = products.find(item => item.identifier === changedValues!.Identifier);
        return [{
            Identifier: changedProduct!.identifier,
            IsActive: changedValues!.IsActive ?? changedProduct!.isActive,
            Quantity: changedValues!.Quantity ?? changedProduct!.quantity
        }, ...activeProducts];
    }
    return [...activeProducts];
}

/**
 * Функция для маппинга сервисов
 * @param value сервис для преобразования в тип RecalculateProductType
 * @returns только поля для типа RecalculateProductType
 */
function mapProductFor(value: Services | undefined): RecalculateProductType | undefined {
    if (value === undefined) {
        return;
    }
    return {
        Identifier: value.identifier,
        IsActive: value.isActive,
        Quantity: value.quantity
    };
}

/**
 * Функция для преобразования списка периода для ComboboxForm
 * @param monthList массив данных в типе Key: string, Value: string
 * @returns массив списка модели ComboboxItemModel<number>
 */
export function getMonthList(monthList: Setting[]): ComboboxItemModel<number>[] {
    return monthList.map(item => mapMonthsFor(item));
}

/**
 * Функция для маппинга элементов monthList
 * @param month элемент модели Setting
 * @returns элемент модели ComboboxItemModel<number>
 */
function mapMonthsFor(month: Setting): ComboboxItemModel<number> {
    const textString = () => {
        if (Number(month.Value) === 1) {
            return `Оплата за ${month.Value} месяц`;
        }
        if (Number(month.Value) > 1 && Number(month.Value) < 4) {
            return `Оплата за ${month.Value} месяца`;
        }
        return `Оплата за ${month.Value} месяцев`;
    };

    return {
        text: textString(),
        value: Number(month.Value)
    };
}

/**
 * Функция получения модели продукта для создания счета на оплату
 * @param products Список сервисов для приведения в тип CreateInvoiceProduct
 * @returns массив продуктов для создания счета на оплату
 */
export function getParamsForCreateInvoice(products: Services[]): CreateInvoiceProduct[] {
    return products.filter(item => item.isActive).map(item => mapProductForCreate(item)!);
}
/**
 * Функция для маппинга списка продуктов
 * @param item элемент списка сервисов
 * @returns модель для продукта создания счета на оплату
 */
function mapProductForCreate(item: Services): CreateInvoiceProduct {
    return {
        Identifier: item.identifier,
        IsActive: item.isActive,
        PayPeriod: item.payPeriod,
        Quantity: item.quantity,
        Rate: item.rate,
        TotalBonus: item.totalBonus,
        TotalPrice: item.totalPrice
    };
}
/**
 * Функция для генерации списка Email
 * @param users Список пользователей
 * @returns Массив обьектов с Email в типе ComboboxItemModel
 */
export function getAccountEmails(users: AccountUsersItemDataModel[]): ComboboxItemModel<number>[] {
    const sortedUsersByRole = users.sort((a, b) => {
        const x = a.accountRoles.includes(AccountUserGroupEnum.AccountAdmin);
        const y = b.accountRoles.includes(AccountUserGroupEnum.AccountAdmin);
        return (x === y) ? 0 : x ? -1 : 1;
    });
    return sortedUsersByRole.map((item, index) => mapUsersForEmail(item, index));
}

/**
 * Функция для маппинга списка пользователей
 * @param user пользователь аккаунта
 * @param index индекс в массиве
 * @returns данные для списка text: string value: number
 */
export function mapUsersForEmail(user: AccountUsersItemDataModel, index: number): ComboboxItemModel<number> {
    return {
        text: user.email,
        value: index
    };
}

/**
 * Функция для генерирования данных для таблицы в вкладке Произвольная сумма
 * @returns Поля для таблицы
 */
export function getArbitraryPaymentFields(sup: boolean): ArbitiraryAmountType[] {
    return [
        {
            descService: '',
            service: sup ? 'Предоставление доступа к сервису 42облака' : 'Предоставление права для использования лицензионного обеспечения',
            sum: 0
        }
    ];
}
