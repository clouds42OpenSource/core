import { ERefreshId, ETourId } from 'app/views/Layout/ProjectTour/enums';
import React from 'react';
import { connect } from 'react-redux';

import { GetSavedPaymentMethodsParams } from 'app/modules/AccountManagement/AutoPayment/store/reducers/getSavedPaymentMethods/params';
import { GetSavedPaymentMethodsThunk } from 'app/modules/AccountManagement/AutoPayment/store/thunks';
import { GetBalanceManagementDataThunk } from 'app/modules/AccountManagement/BalanceManagement/store/thunks/GetBalanceManagementDataThunk';
import { ProlongPromisePaymentThunk } from 'app/modules/AccountManagement/BalanceManagement/store/thunks/ProlongPromisePaymentThunk';
import { RePayPromisePaymentThunk } from 'app/modules/AccountManagement/BalanceManagement/store/thunks/RePayPromisePaymentThunk';
import { AppReduxStoreState } from 'app/redux/types';
import { TabsControl } from 'app/views/components/controls/TabsControl';
import { withHeader } from 'app/views/components/_hoc/withHeader';
import { HeaderContentView } from 'app/views/modules/AccountManagement/BalanceManagementView/view/HeaderContentView';
import { ConnectedInvoicesDataViewClass } from 'app/views/modules/AccountManagement/BalanceManagementView/view/InvoicesDataView';
import { PrepaymentView } from 'app/views/modules/AccountManagement/BalanceManagementView/view/PrepaymentView';
import { TransactionsView } from 'app/views/modules/AccountManagement/BalanceManagementView/view/TransactionsDataView';
import { BillingAccountDataModel, getBillingAccountParams } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/getBilingAccountData';

type OwnState = {
    activeTabIndex: number;
};

type StateProps = {
    billingAccount: BillingAccountDataModel;
};

type DispatchProps = {
    dispatchGetBillingAccountData: (args?: getBillingAccountParams) => void;
    dispatchProlongPromisePayment: () => void;
    dispatchRePayPromisePayment: () => void;
    dispatchGetSavedPaymentMethods: (args: GetSavedPaymentMethodsParams) => void;
};

type AllProps = StateProps & DispatchProps;

class BalanceManagementViewClass extends React.Component<AllProps, OwnState> {
    private query = new URLSearchParams(window.location.search);

    private readonly headers = [
        {
            title: 'Счета',
            isVisible: true,
        },
        {
            title: 'Транзакции',
            isVisible: true,
            tourId: ETourId.billingPageTransactionTab
        },
        {
            title: 'Обещанный платёж',
            isVisible: true,
            tourId: ETourId.billingPagePromisePaymentTab
        },
    ];

    public constructor(props: AllProps) {
        super(props);
        this.onActiveTabIndexChanged = this.onActiveTabIndexChanged.bind(this);
        this.state = {
            activeTabIndex: this.query.get('prepayment') ? 2 : 0,
        };
    }

    public componentDidMount() {
        this.props.dispatchGetBillingAccountData({ showLoadingProgress: false, force: true });
        this.props.dispatchGetSavedPaymentMethods({ showLoadingProgress: false, force: true });
    }

    private onActiveTabIndexChanged(activeTabIndex: number) {
        this.setState({ activeTabIndex });
    }

    public render() {
        const { dispatchProlongPromisePayment, dispatchRePayPromisePayment, billingAccount } = this.props;

        return (
            <TabsControl
                headerRefreshId={ ERefreshId.replenishBalanceTable }
                headers={ this.headers }
                onActiveTabIndexChanged={ this.onActiveTabIndexChanged }
                headerContent={ <HeaderContentView /> }
                activeTabIndex={ this.state.activeTabIndex }
            >
                <ConnectedInvoicesDataViewClass locales={ billingAccount.locale.name } isDemo={ billingAccount.accountIsDemo } />
                <TransactionsView />
                <PrepaymentView data={ billingAccount } onProlong={ dispatchProlongPromisePayment } onRepay={ dispatchRePayPromisePayment } />
            </TabsControl>
        );
    }
}

const ConnectedBalanceManagementViewClass = connect<StateProps, DispatchProps, void, AppReduxStoreState>(
    ({ BalanceManagementState: { billingAccount } }) => ({
        billingAccount,
    }),
    {
        dispatchGetBillingAccountData: GetBalanceManagementDataThunk.invoke,
        dispatchProlongPromisePayment: ProlongPromisePaymentThunk.invoke,
        dispatchRePayPromisePayment: RePayPromisePaymentThunk.invoke,
        dispatchGetSavedPaymentMethods: GetSavedPaymentMethodsThunk.invoke,
    }
)(BalanceManagementViewClass);

export const BalanceManagementView = withHeader({
    title: 'Баланс',
    page: ConnectedBalanceManagementViewClass,
});