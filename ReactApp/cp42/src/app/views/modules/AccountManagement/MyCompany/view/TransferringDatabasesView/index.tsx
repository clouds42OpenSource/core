import { Box, Chip } from '@mui/material';
import { SortingKind } from 'app/common/enums';
import { ReceiveDatabaseMigrationThunkParams } from 'app/modules/myCompany/store/reducers/receiveDatabaseMigration/params';
import { ReceiveDatabaseMigrationThunk } from 'app/modules/myCompany/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { COLORS } from 'app/utils';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { TextOut } from 'app/views/components/TextOut';
import { BaseButton } from 'app/views/components/controls/Button/BaseButton';
import { TransferringDatabaseFilterDataFrom } from 'app/views/modules/AccountManagement/MyCompany/types/TransferringDatabaseFilterDataFrom';
import { TransferringDatabaseComboBoxView } from 'app/views/modules/AccountManagement/MyCompany/view/TransferringDatabasesView/view/TransferringDatabaseComboBox';
import { TransferringDatabasesFilterFormView } from 'app/views/modules/AccountManagement/MyCompany/view/TransferringDatabasesView/view/TransferringDatabasesFilterFromView';
import css from 'app/views/modules/AccountManagement/MyCompany/view/TransferringDatabasesView/view/style.module.css';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { CopyAccountDatabasePathButton } from 'app/views/modules/_common/reusable-modules/AccountDatabasesForMigrations/views/_buttons/CopyAccountDatabasePathButton';
import { DatabaseMigrationDataModel } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/receiveDatabaseMigration';
import { SelectDataCommonDataModel, SortingDataModel } from 'app/web/common/data-models';
import cn from 'classnames';
import React from 'react';
import { connect } from 'react-redux';

type StateProps = {
    dataset: DatabaseMigrationDataModel;
    isInLoadingDataProcess: boolean;
};

type DispatchProps = {
    dispatchReceiveDatabaseMigrationThunk: (args: ReceiveDatabaseMigrationThunkParams) => void;
};

type OwnProps = {
    onDatabaseMigration: boolean;
};

type AllProps = StateProps & DispatchProps & OwnProps;

type OwnState = {
    databaseMigration: string[];
};

class TransferringDatabases extends React.Component<AllProps, OwnState> {
    private commonTableWithFilterViewRef = React.createRef<CommonTableWithFilter<any, any>>();

    public constructor(props: AllProps) {
        super(props);

        this.onDataSelect = this.onDataSelect.bind(this);
        this.onDatabaseMigration = this.onDatabaseMigration.bind(this);
        this.deleteOnDatabaseMigrationItem = this.deleteOnDatabaseMigrationItem.bind(this);
        this.cleanOffDatabaseMigration = this.cleanOffDatabaseMigration.bind(this);

        this.state = {
            databaseMigration: [],
        };
    }

    public componentDidMount() {
        if (this.props.onDatabaseMigration) {
            this.props.dispatchReceiveDatabaseMigrationThunk({
                accountIndexNumber: null,
                pageNumber: 1,
                orderBy: 'v82name.desc'
            });
        }
    }

    private onDataSelect(args: SelectDataCommonDataModel<TransferringDatabaseFilterDataFrom>) {
        if (this.props.onDatabaseMigration) {
            this.props.dispatchReceiveDatabaseMigrationThunk({
                accountIndexNumber: null,
                searchLine: args.filter?.searchLine,
                pageNumber: args.pageNumber,
                orderBy: args.sortingData ? this.getSortQuery(args.sortingData) : '',
                force: true
            });
        }
    }

    private onDatabaseMigration(v82Name: string) {
        if (!this.state.databaseMigration.includes(v82Name)) {
            this.setState(prev => ({ databaseMigration: [...prev.databaseMigration, v82Name] }));
        }
    }

    private getSortQuery(args: SortingDataModel) {
        return `${ args.fieldName }.${ args.sortKind ? 'desc' : 'asc' }`;
    }

    private deleteOnDatabaseMigrationItem(v82Name: string) {
        this.setState(prev => ({ databaseMigration: prev.databaseMigration.filter(item => item !== v82Name) }));
    }

    private cleanOffDatabaseMigration() {
        this.setState({ databaseMigration: [] });
    }

    public render() {
        return (
            <>
                <CommonTableWithFilter
                    isBorderStyled={ true }
                    isResponsiveTable={ true }
                    ref={ this.commonTableWithFilterViewRef }
                    uniqueContextProviderStateId="DatabaseMigrationContextProviderStateId"
                    filterProps={ {
                        getFilterContentView: (ref, onFilterChanged) => <TransferringDatabasesFilterFormView
                            ref={ ref }
                            onFilterChange={ onFilterChanged }
                            isFilterFormatDisable={ !this.props.isInLoadingDataProcess }
                        />
                    } }
                    tableProps={ {
                        dataset: this.props.dataset.databases.records,
                        keyFieldName: 'accountDatabaseId',
                        sorting: {
                            sortFieldName: 'v82Name',
                            sortKind: SortingKind.Asc
                        },
                        fieldsView: {
                            v82Name: {
                                caption: 'Номер',
                                isSortable: true,
                            },
                            path: {
                                caption: 'Путь',
                                fieldWidth: '40%',
                                isSortable: false,
                                format: (value: string, data) => !!value && (
                                    <Box display="flex" gap="10px">
                                        <div className={ css['copy-accountdatabase-path-button-container'] }>
                                            <CopyAccountDatabasePathButton inputElementId={ `input${ data.accountDatabaseId }` } />
                                        </div>
                                        <div className={ css['accountdatabase-path-container'] }>
                                            <input style={ { color: COLORS.font } } className={ css.accountDatabasePathContainer } type="text" id={ `input${ data.accountDatabaseId }` } readOnly={ true } defaultValue={ value } />
                                        </div>
                                    </Box>
                                )
                            },
                            isFile: {
                                caption: 'Тип',
                                isSortable: false,
                                format: (value: boolean) =>
                                    <Chip label={ value ? 'Файловая' : 'Серверная' } />
                            },
                            size: {
                                caption: 'Размер, МБ',
                                isSortable: false,
                            },
                            isPublishDatabase: {
                                caption: 'Web базы',
                                isSortable: false,
                                format: (value: boolean) => value ? '+' : ''
                            },
                            isPublishServices: {
                                caption: 'Web сервис',
                                isSortable: false,
                                format: (value: boolean) => value ? '+' : ''
                            },
                            migrationAction: {
                                caption: '',
                                format: (_, row) => row.isFile &&
                                    <BaseButton variant="text" onClick={ () => this.onDatabaseMigration(row.v82Name) }>
                                        <i className="fa fa-arrow-right" />
                                    </BaseButton>
                            }
                        },
                        pagination: {
                            currentPage: this.props.dataset.databases.metadata.pageNumber,
                            recordsCount: this.props.dataset.databases.metadata.totalItemCount,
                            totalPages: this.props.dataset.databases.metadata.pageCount,
                            pageSize: this.props.dataset.databases.metadata.pageSize
                        }
                    } }
                    onDataSelect={ this.onDataSelect }
                />
                { !!this.state.databaseMigration.length && (
                    <Box display="flex" flexDirection="column" gap="10px">
                        <FormAndLabel label="Базы для переноса">
                            <Box
                                display="flex"
                                paddingInline="3px"
                                paddingBlock="6px"
                                gap="5px"
                                flexWrap="wrap"
                                borderRadius="3px"
                                sx={ { border: `1px solid ${ COLORS.border }` } }
                            >
                                { this.state.databaseMigration.map(item =>
                                    <Chip
                                        key={ item }
                                        label={ item }
                                        onClick={ () => this.deleteOnDatabaseMigrationItem(item) }
                                    />
                                ) }
                            </Box>
                        </FormAndLabel>
                        <TransferringDatabaseComboBoxView
                            dataset={ this.props.dataset.avalableFileStorages }
                            selectDatabases={ this.state.databaseMigration }
                            clearDatabasesForMigrate={ this.cleanOffDatabaseMigration }
                        />
                    </Box>
                ) }
            </>
        );
    }
}

export const DatabaseMigrationPage = connect<StateProps, DispatchProps, {}, AppReduxStoreState>(
    state => ({
        dataset: state.MyCompanyState.databaseMigration,
        isInLoadingDataProcess: state.MyCompanyState.hasSuccessFor.hasDatabaseMigrationReceived
    }),
    {
        dispatchReceiveDatabaseMigrationThunk: ReceiveDatabaseMigrationThunk.invoke,
    }
)(TransferringDatabases);