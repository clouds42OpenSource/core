import { Box, TextField } from '@mui/material';
import { contextAccountId } from 'app/api';
import { FETCH_API } from 'app/api/useFetchApi';
import { EMessageType, useFloatMessages } from 'app/hooks';
import { ReceiveMyCompanyThunkParams } from 'app/modules/myCompany/store/reducers/receiveMyCompanyData/params';
import { Dialog } from 'app/views/components/controls/Dialog';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { innMessage, limitErrorMessage, requiredMessage, transformValue } from 'app/views/modules/ToPartners/helpers';
import { TAdditionalCompany } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/receiveMyCompany';
import { useFormik } from 'formik';
import React, { useEffect } from 'react';
import { object, ObjectSchema, string } from 'yup';

type TProps = {
    isOpen: boolean;
    onClose: () => void;
    data?: TAdditionalCompany;
    getCompanyData: (args: ReceiveMyCompanyThunkParams) => void;
};

type TSchema = Omit<TAdditionalCompany, 'id'>;

const validationSchema: ObjectSchema<TSchema> = object({
    name: string().required(requiredMessage),
    inn: string().min(4, innMessage).max(14, innMessage).required(requiredMessage),
    description: string(),
    email: string().email('Неверный формат почты'),
});

const { editAdditionalCompany, createAdditionalCompany } = FETCH_API.ACCOUNTS;

export const OrganizationDialog = ({ isOpen, onClose, data, getCompanyData }: TProps) => {
    const { show } = useFloatMessages();

    const { submitForm, errors, values, handleChange, handleBlur, touched, dirty, resetForm } = useFormik<TSchema>({
        initialValues: {
            name: data?.name ?? '',
            inn: data?.inn ?? '',
            description: data?.description ?? '',
            email: data?.email ?? '',
        },
        onSubmit: async formValues => {
            let result = { success: false, message: '' };

            if (data) {
                const { message, success } = await editAdditionalCompany({
                    ...formValues,
                    id: data.id,
                    accountId: contextAccountId()
                });
                result = { message: message ?? '', success };
            } else {
                const { message, success } = await createAdditionalCompany({
                    ...formValues,
                    accountId: contextAccountId(),
                });
                result = { message: message ?? '', success };
            }

            show(result.success ? EMessageType.success : EMessageType.error, result.message);

            if (result.success) {
                onClose();
                getCompanyData({ showLoadingProgress: true, force: true });
            }
        },
        validationSchema,
        validateOnBlur: true,
        validateOnChange: true,
        enableReinitialize: true,
    });

    useEffect(() => {
        if (!isOpen && dirty && !data) {
            resetForm();
        }
    }, [data, dirty, isOpen, resetForm]);

    return (
        <Dialog
            isOpen={ isOpen }
            dialogWidth="sm"
            onCancelClick={ onClose }
            title={ `${ data ? 'Редактировать' : 'Добавить' } организацию` }
            isTitleSmall={ true }
            dialogVerticalAlign="top"
            buttons={ [
                {
                    kind: 'default',
                    onClick: onClose,
                    content: 'Закрыть'
                },
                {
                    kind: 'success',
                    onClick: submitForm,
                    content: 'Сохранить'
                }
            ] }
        >
            <Box display="flex" flexDirection="column" gap="10px">
                <FormAndLabel label="Название">
                    <TextField
                        onChange={ handleChange }
                        onBlur={ handleBlur }
                        name="name"
                        value={ values.name }
                        fullWidth={ true }
                        error={ touched.name && !!errors.name }
                        helperText={ touched.name && errors.name }
                    />
                </FormAndLabel>
                <FormAndLabel label="ИНН">
                    <TextField
                        onChange={ handleChange }
                        onBlur={ handleBlur }
                        name="inn"
                        value={ values.inn }
                        autoComplete="off"
                        fullWidth={ true }
                        error={ touched.inn && !!errors.inn }
                        helperText={ touched.inn && errors.inn }
                    />
                </FormAndLabel>
                <FormAndLabel label="Описание">
                    <TextField
                        onChange={ handleChange }
                        onBlur={ handleBlur }
                        name="description"
                        value={ values.description }
                        fullWidth={ true }
                        error={ touched.description && !!errors.description }
                        helperText={ touched.description && errors.description }
                        multiline={ true }
                        rows={ 5 }
                    />
                </FormAndLabel>
                <FormAndLabel label="E-Mail для отправки писем">
                    <TextField
                        onChange={ handleChange }
                        onBlur={ handleBlur }
                        name="email"
                        value={ values.email }
                        fullWidth={ true }
                        error={ touched.email && !!errors.email }
                        helperText={ touched.email && errors.email }
                    />
                </FormAndLabel>
            </Box>
        </Dialog>
    );
};