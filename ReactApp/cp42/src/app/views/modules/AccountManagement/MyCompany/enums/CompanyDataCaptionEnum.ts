export enum CompanyDataCaptionEnum {
    caption = 'Название компании',
    indexNumber = 'Номер компании',
    accountInn = 'ИНН',
    isVip = 'VIP',
    createClusterDatabase = 'Создание серверных баз по умолчанию',
    accountSaleManagerCaption = 'Менеджер компании',
    segmentName = 'Сегмент',
    accountDescription = 'Описание',
    emails = 'Email для отправки писем',
    departments = 'Отделы компании',
    kazakhstanIin = 'ИИН'
}