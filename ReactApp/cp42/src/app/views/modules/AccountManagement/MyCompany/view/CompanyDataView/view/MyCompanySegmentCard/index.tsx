import { localStorageHelper } from 'app/common/helpers/LocalStorageHelper';
import { ChangeMyCompanySegmentThunkParams } from 'app/modules/myCompany/store/reducers/changeMyCompanySegmentData/params';
import { ReceiveMyCompanySegmentThunkParams } from 'app/modules/myCompany/store/reducers/receiveMyCompanySegmentData/params';
import { ChangeMyCompanySegmentThunk, ReceiveMyCompanySegmentThunk } from 'app/modules/myCompany/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { Dialog } from 'app/views/components/controls/Dialog';
import { ComboBoxForm } from 'app/views/components/controls/forms';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { FloatMessageEnum } from 'app/views/modules/AccountManagement/MyCompany/enums/FloatMessageEnum';
import { getDialogButtons } from 'app/views/modules/Segments/common/functions';
import { MyCompanySegmentDataModel } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/receiveMyCompanySegment';
import React from 'react';
import { connect } from 'react-redux';

type StateProps = {
    dataset: MyCompanySegmentDataModel[];
};

type DispatchProps = {
    dispatchReceiveMyCompanySegmentThunk: (args: ReceiveMyCompanySegmentThunkParams) => void;
    dispatchChangeMyCompanySegmentThunk: (args: ChangeMyCompanySegmentThunkParams) => void;
};

type OwnProps = FloatMessageProps & {
    isOpen: boolean;
    onCancelDialogButtonClick: () => void;
    activeSegmentId: string;
    onChangeStatus: boolean;
};

type AllProps = OwnProps & StateProps & DispatchProps;

type OwnState = {
    segmentId: string;
};

class MyCompanySegmentCard extends React.Component<AllProps, OwnState> {
    public constructor(props: AllProps) {
        super(props);

        this.onSaveChangeSegment = this.onSaveChangeSegment.bind(this);

        this.state = {
            segmentId: this.props.activeSegmentId
        };
    }

    public componentDidMount() {
        if (this.props.onChangeStatus) {
            this.props.dispatchReceiveMyCompanySegmentThunk({ force: true })
        }
    }

    public onSaveChangeSegment() {
        this.props.dispatchChangeMyCompanySegmentThunk({
            accountId: localStorageHelper.getContextAccountId() ?? '',
            segmentId: this.state.segmentId,
            force: true
        });

        this.props.onCancelDialogButtonClick();
        this.props.floatMessage.show(MessageType.Default, FloatMessageEnum.ChangeSegment, 1500);
    }

    public render() {
        return (
            <Dialog
                title="Смена сегмента"
                isTitleSmall={ true }
                dialogWidth="xs"
                titleTextAlign="left"
                dialogVerticalAlign="top"
                isOpen={ this.props.isOpen }
                onCancelClick={ this.props.onCancelDialogButtonClick }
                buttons={ this.props.onChangeStatus
                    ? getDialogButtons({
                        pageType: 0,
                        onClickToCloseModal: this.props.onCancelDialogButtonClick,
                        submitChanges: this.onSaveChangeSegment
                    })
                    : []
                }
            >
                <FormAndLabel label="Выберите сегмент для миграции">
                    <ComboBoxForm
                        formName="segmentId"
                        items={ this.props.dataset.map(item => ({ value: item.id, text: item.name })) }
                        value={ this.state.segmentId }
                        onValueChange={ (_, n) => { this.setState({ segmentId: n }); } }
                    />
                </FormAndLabel>
            </Dialog>
        );
    }
}

const MyCompanyCardConnect = connect<StateProps, DispatchProps, NonNullable<unknown>, AppReduxStoreState>(
    ({ MyCompanyState: { segment, hasSuccessFor } }) => ({
        dataset: segment,
        isInLoadingDataProcess: hasSuccessFor.hasMyCompanySegmentReceived,
    }),
    {
        dispatchReceiveMyCompanySegmentThunk: ReceiveMyCompanySegmentThunk.invoke,
        dispatchChangeMyCompanySegmentThunk: ChangeMyCompanySegmentThunk.invoke
    }
)(MyCompanySegmentCard);

export const MyCompanyCard = withFloatMessages(MyCompanyCardConnect);