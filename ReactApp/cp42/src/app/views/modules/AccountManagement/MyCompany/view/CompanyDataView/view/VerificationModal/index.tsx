import { contextAccountId } from 'app/api';
import { FETCH_API } from 'app/api/useFetchApi';
import { EMessageType, useFloatMessages } from 'app/hooks';
import { ReceiveMyCompanyThunkParams } from 'app/modules/myCompany/store/reducers/receiveMyCompanyData/params';
import { Dialog } from 'app/views/components/controls/Dialog';
import { ConfigFileUploader } from 'app/views/modules/_common/components/ConfigFileUploader';
import { useState } from 'react';

type TProps = {
    isOpen: boolean;
    onClose: () => void;
    getCompanyData: (args: ReceiveMyCompanyThunkParams) => void;
};

const { sendCertificate } = FETCH_API.ACCOUNTS;

export const VerificationModal = ({ isOpen, onClose, getCompanyData }: TProps) => {
    const { show } = useFloatMessages();

    const [certificate, setCertificate] = useState<File>();

    const uploadCertificateHandler = (file: File) => {
        setCertificate(file);
    };

    const deleteCertificateHandler = () => {
        setCertificate(undefined);
    };

    const submit = async () => {
        if (certificate) {
            const formData = new FormData();
            formData.append('file', certificate);
            formData.append('accountId', contextAccountId());
            formData.append('type', '0');

            const { message, success } = await sendCertificate(formData);

            show(success ? EMessageType.success : EMessageType.error, message);

            if (success) {
                getCompanyData({ force: true });
                onClose();
            }
        }
    };

    return (
        <Dialog
            isOpen={ isOpen }
            onCancelClick={ onClose }
            dialogWidth="sm"
            isTitleSmall={ true }
            dialogVerticalAlign="top"
            title="Верификация компании"
            buttons={ [
                {
                    kind: 'default',
                    onClick: onClose,
                    content: 'Закрыть'
                },
                {
                    kind: 'success',
                    onClick: submit,
                    content: 'Отправить',
                    isEnabled: !!certificate
                }
            ] }
        >
            <ConfigFileUploader
                uploadedFiles={ certificate && [certificate] }
                deleteUploadFile={ deleteCertificateHandler }
                callBack={ uploadCertificateHandler }
                limit={ 1 }
                text="Свидетельство о постановке на учёт в налоговый орган"
                fileTypesArr={ ['pdf', 'doc', 'docx'] }
                uploadFileStyle={ { maxWidth: '300px' } }
            />
        </Dialog>
    );
};