export enum FloatMessageEnum {
    EditMyCompany = 'Данные успешно сохранены',
    ChangeSegment = 'Добавлена задача на смену сегмента',
    TransferringDatabase = 'Добавлена задача на перемещение баз'
}