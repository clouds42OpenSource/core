import { Box, OutlinedInput } from '@mui/material';
import Stack from '@mui/material/Stack';
import { EVerificationStatus } from 'app/api/endpoints/accounts/enum';
import { AccountUserGroup } from 'app/common/enums';
import { checkOnValidField, checkValidOfStringField } from 'app/common/functions';
import { IErrorsType } from 'app/common/interfaces';
import { createEmailValidator } from 'app/common/validators/createEmailValidator';
import { createInnValidator } from 'app/common/validators/createInnValidator';
import { ReceiveMyCompanyThunkParams } from 'app/modules/myCompany/store/reducers/receiveMyCompanyData/params';
import { ReceiveMyCompanyThunk } from 'app/modules/myCompany/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { IReduxForm, ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { BaseButton } from 'app/views/components/controls/Button/BaseButton';
import { CheckBoxForm, TagInputForm, TextBoxForm } from 'app/views/components/controls/forms';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { ShowErrorMessage } from 'app/views/modules/_common/components/ShowErrorMessage';
import { FloatMessageEnum } from 'app/views/modules/AccountManagement/MyCompany/enums/FloatMessageEnum';
import { SelectLocalesView } from 'app/views/modules/AccountManagement/MyCompany/view/CompanyDataView/view/SelectLocales';
import { AccountPermissionsEnum } from 'app/web/common/enums/AccountPermissionsEnum';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { MyCompanyDataModel, TAccountDepartment } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/receiveMyCompany';
import { RequestKind } from 'core/requestSender/enums';
import React from 'react';
import { connect } from 'react-redux';

import styles from './styles.module.css';

type StateProps = {
    dataset: MyCompanyDataModel;
    permissionsReceived: boolean;
    permissions: string[];
    currentUserGroups: AccountUserGroup[];
};

type DispatchProps = {
    dispatchReceiveMyCompanyThunk: (args: ReceiveMyCompanyThunkParams) => void;
};

type OwnProps = ReduxFormProps<MyCompanyDataModel> & FloatMessageProps & {
    itemToEdit: MyCompanyDataModel;
    cancelButton: () => void;
};

type AllProps = StateProps & DispatchProps & OwnProps;

type OwnState = {
    errors: IErrorsType;
    departments: TAccountDepartment[];
};

export class EditMyCompanyFormClass extends React.Component<AllProps, OwnState> {
    private selectLocaleRef = React.createRef<IReduxForm<{ locales: string }>>();

    public constructor(props: AllProps) {
        super(props);
        this.onValueChange = this.onValueChange.bind(this);
        this.initReduxForm = this.initReduxForm.bind(this);
        this.editMyCompanyDataChange = this.editMyCompanyDataChange.bind(this);
        this.checkPermission = this.checkPermission.bind(this);
        this.onDepartmentChange = this.onDepartmentChange.bind(this);

        this.props.reduxForm.setInitializeFormDataAction(this.initReduxForm);

        this.state = {
            errors: {},
            departments: [],
        };
    }

    private onDepartmentChange(_: string, current: string[], prev: string[]) {
        const { reduxForm: { updateReduxFormFields, getReduxFormFields }, dataset } = this.props;
        const { departments } = getReduxFormFields(false);

        if (prev.length < current.length) {
            updateReduxFormFields({ departments: [...departments, { name: current[current.length - 1], departmentId: '', actionType: 1 }] });
        } else if (prev.length > current.length) {
            const difference = dataset.departments.find(item => item.name === prev.filter(elem => !current.includes(elem))[0]);

            if (difference) {
                this.setState(state => ({ departments: [...state.departments, { ...difference, actionType: 3 }] }));
            }

            const value = current.flatMap((item: string) => {
                return departments.find(department => department.name === item) ?? [];
            });

            updateReduxFormFields({ departments: value });
        }
    }

    private onValueChange<TValue>(formName: string, newValue: TValue) {
        if (formName === 'isVip') {
            if (typeof newValue === 'boolean' && newValue === true) {
                this.props.reduxForm.updateReduxFormFields({ isVip: true, createClusterDatabase: true });
            } else {
                this.props.reduxForm.updateReduxFormFields({ isVip: false, createClusterDatabase: false });
            }
        } else {
            this.props.reduxForm.updateReduxFormFields({ [formName]: newValue });
        }

        const { errors } = this.state;

        if (typeof newValue === 'string') {
            switch (formName) {
                case 'accountCaption':
                    errors[formName] = checkValidOfStringField(newValue, 'Обязательное поле');
                    break;
                default:
                    break;
            }

            this.setState({ errors });
        }
    }

    private get isMyCompanyFormValid() {
        const formField = this.props.reduxForm.getReduxFormFields(false);
        const errors: IErrorsType = {};
        const arrayResult: boolean[] = [];

        arrayResult.push(checkOnValidField(errors, 'accountCaption', formField.accountCaption, 'Обязательное поле'));

        if (arrayResult.some(item => !item)) {
            this.props.floatMessage.show(MessageType.Warning, 'Форма к отправке не готова. Пожалуйста, проверьте корректность полей');
            this.setState({ errors });

            return false;
        }

        return true;
    }

    private initReduxForm(): MyCompanyDataModel | undefined {
        return this.props.itemToEdit;
    }

    private checkPermission(permission: string) {
        return this.props.permissionsReceived && this.props.permissions.includes(permission);
    }

    private async editMyCompanyDataChange() {
        if (this.isMyCompanyFormValid) {
            const { reduxForm: { getReduxFormFields }, cancelButton, floatMessage: { show }, dispatchReceiveMyCompanyThunk } = this.props;
            const { accountId, accountDescription, accountCaption, accountInn, isVip, emails, createClusterDatabase, departments } = getReduxFormFields(false);

            const myCompanyProxy = InterlayerApiProxy.getMyCompanyApiProxy();

            const { Success, Message } = await myCompanyProxy.editMyCompany(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, {
                accountId,
                accountDescription,
                accountCaption,
                accountInn,
                isVip,
                emails,
                createClusterDatabase,
                departments: [...departments, ...this.state.departments],
                localeId: this.selectLocaleRef.current?.getReduxForm().getReduxFormFields(false).locales ?? this.props.itemToEdit.localeId,
            });

            if (Success) {
                dispatchReceiveMyCompanyThunk({ force: true });
                cancelButton();
                show(MessageType.Success, FloatMessageEnum.EditMyCompany, 1500);
            } else {
                show(MessageType.Error, Message);
            }
        }
    }

    public render() {
        const { itemToEdit, reduxForm: { getReduxFormFields }, dataset, cancelButton, currentUserGroups } = this.props;

        if (!itemToEdit) {
            return null;
        }

        const { accountCaption, accountInn, indexNumber, isVip, accountSaleManagerCaption, canChangeLocale, accountDescription, emails, createClusterDatabase, departments } = getReduxFormFields(false);
        const { errors } = this.state;
        const isReadOnly = dataset.verificationStatus === EVerificationStatus.verified || dataset.verificationStatus === EVerificationStatus.underReview
            ? !currentUserGroups.includes(AccountUserGroup.CloudAdmin)
            : !currentUserGroups.includes(AccountUserGroup.CloudAdmin) && !currentUserGroups.includes(AccountUserGroup.AccountAdmin);
        const isInnValid = accountInn ? /^\d{10,12}$/.test(accountInn) : true;

        return (
            <Box display="flex" flexDirection="column" gap="10px">
                <FormAndLabel label="Название компании">
                    <TextBoxForm
                        formName="accountCaption"
                        value={ accountCaption }
                        onValueChange={ this.onValueChange }
                        disabled={ isReadOnly }
                    />
                    <ShowErrorMessage errors={ errors } formName="accountCaption" />
                </FormAndLabel>
                <FormAndLabel label="ИНН">
                    <TextBoxForm
                        formName="accountInn"
                        value={ accountInn }
                        onValueChange={ this.onValueChange }
                        validator={ createInnValidator() }
                        disabled={ isReadOnly }
                    />
                </FormAndLabel>
                <FormAndLabel label="Номер компании">
                    <OutlinedInput
                        fullWidth={ true }
                        disabled={ true }
                        name="indexNumber"
                        value={ indexNumber }
                    />
                </FormAndLabel>
                { this.checkPermission(AccountPermissionsEnum.AccountsViewFullInfo) && (
                    <>
                        <CheckBoxForm
                            className={ styles.checkbox }
                            formName="isVip"
                            label="VIP"
                            isChecked={ isVip }
                            onValueChange={ this.onValueChange }
                        />
                        <CheckBoxForm
                            className={ styles.checkbox }
                            formName="createClusterDatabase"
                            label="Создание серверных баз по умолчанию"
                            isChecked={ createClusterDatabase }
                            onValueChange={ this.onValueChange }
                            isReadOnly={ !isVip }
                        />
                    </>
                ) }
                { this.checkPermission(AccountPermissionsEnum.ControlPanelSetSalemanager) && (
                    <FormAndLabel label="Менеджер компании">
                        <TextBoxForm
                            formName="accountSaleManagerCaption"
                            value={ accountSaleManagerCaption }
                            disabled={ true }
                            onValueChange={ this.onValueChange }
                        />
                    </FormAndLabel>
                ) }
                { this.checkPermission(AccountPermissionsEnum.ControlPanelSetLocale) && (
                    <SelectLocalesView
                        dataset={ dataset }
                        isReadOnly={ !canChangeLocale || !dataset.locales }
                        ref={ this.selectLocaleRef }
                    />
                ) }
                <FormAndLabel label="Описание компании">
                    <TextBoxForm
                        formName="accountDescription"
                        type="textarea"
                        value={ accountDescription }
                        onValueChange={ this.onValueChange }
                    />
                </FormAndLabel>
                <TagInputForm
                    validator={ createEmailValidator() }
                    onTagsChanged={ this.onValueChange }
                    tags={ emails }
                    placeholder="Для ввода почты нажмите Enter"
                    formName="emails"
                    label="Email для отправки писем"
                />
                <TagInputForm
                    validator={ {
                        onChange: (value: string) => {
                            if (departments.find(item => item.name === value)) {
                                return 'Данный отдел уже был создан';
                            }
                        },
                        onBlur: (value: string) => {
                            if (departments.find(item => item.name === value)) {
                                return 'Данный отдел уже был создан';
                            }
                        },
                    } }
                    onTagsChanged={ this.onDepartmentChange }
                    tags={ departments.map(department => department.name) }
                    placeholder="Для ввода отдела нажмите Enter"
                    formName="departments"
                    label="Отделы компании"
                />
                <Stack
                    direction="row"
                    spacing={ 2 }
                    justifyContent="flex-end"
                >
                    <BaseButton variant="contained" onClick={ cancelButton }>
                        <i className="fa fa-ban" />&nbsp;Отмена
                    </BaseButton>
                    <BaseButton variant="contained" kind="turquoise" isEnabled={ isInnValid } onClick={ this.editMyCompanyDataChange }>
                        <i className="fa fa-save" />&nbsp;Сохранить
                    </BaseButton>
                </Stack>
            </Box>
        );
    }
}

const EditMyCompanyFormConnect = connect<StateProps, DispatchProps, NonNullable<unknown>, AppReduxStoreState>(
    ({ Global: { getUserPermissionsReducer: { permissions, hasSuccessFor }, getCurrentSessionSettingsReducer }, MyCompanyState }) => ({
        dataset: MyCompanyState.myCompany,
        permissionsReceived: hasSuccessFor.permissionsReceived,
        permissions,
        currentUserGroups: getCurrentSessionSettingsReducer.settings.currentContextInfo.currentUserGroups
    }),
    {
        dispatchReceiveMyCompanyThunk: ReceiveMyCompanyThunk.invoke
    }
)(EditMyCompanyFormClass);

const EditMyCompanyFormReduxForm = withReduxForm(EditMyCompanyFormConnect, {
    reduxFormName: 'EditMyCompany_ReduxForm',
    resetOnUnmount: true
});

export const EditMyCompanyView = withFloatMessages(EditMyCompanyFormReduxForm);