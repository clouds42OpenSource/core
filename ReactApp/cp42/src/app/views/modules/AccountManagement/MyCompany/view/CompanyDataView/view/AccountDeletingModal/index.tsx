import { ErrorOutline as ErrorOutlineIcon } from '@mui/icons-material';
import { Box, TextField } from '@mui/material';
import { FETCH_API } from 'app/api/useFetchApi';
import { AppRoutes } from 'app/AppRoutes';
import { EMessageType, useFloatMessages } from 'app/hooks';
import { COLORS } from 'app/utils';
import { Dialog } from 'app/views/components/controls/Dialog';
import { TextOut } from 'app/views/components/TextOut';
import { useState } from 'react';
import { useHistory } from 'react-router';

type TAccountDeletingModal = {
    isOpen: boolean;
    onCancelClick: () => void;
};

const { postDeleteConfirmation, deleteConfirmDeletion } = FETCH_API.ACCOUNTS;

export const AccountDeletingModal = ({ isOpen, onCancelClick }: TAccountDeletingModal) => {
    const history = useHistory();
    const { show } = useFloatMessages();

    const [showDeleting, setShowDeleting] = useState(false);
    const [code, setCode] = useState('');

    const openDeletingDialog = () => {
        setShowDeleting(true);
    };

    const closeDeletingDialog = () => {
        setShowDeleting(false);
    };

    const sendAccountDelete = async () => {
        const confirmation = await postDeleteConfirmation();

        onCancelClick();
        if (confirmation.success) {
            show(EMessageType.success, 'Уведомление отправлено на почту');
            openDeletingDialog();
        } else if (confirmation.message?.includes('За последние')) {
            show(EMessageType.warning, confirmation.message);
            openDeletingDialog();
        } else {
            show(EMessageType.error, confirmation.message);
        }
    };

    const deleteAccount = async () => {
        const deleteInfo = await deleteConfirmDeletion(code);

        if (deleteInfo.success) {
            history.push(AppRoutes.signInRoute);
            show(EMessageType.success, 'Аккаунт успешно удален');
        } else {
            closeDeletingDialog();
            show(EMessageType.error, `Произошла ошибка: ${ deleteInfo.message }`);
        }
    };

    return (
        <>
            <Dialog
                title="Вы точно хотите удалить аккаунт?"
                isTitleSmall={ true }
                isOpen={ isOpen }
                onCancelClick={ onCancelClick }
                buttons={ [
                    {
                        content: 'Отмена',
                        kind: 'default',
                        variant: 'contained',
                        onClick: onCancelClick
                    },
                    {
                        content: 'УДАЛИТЬ',
                        kind: 'error',
                        variant: 'contained',
                        onClick: sendAccountDelete
                    }
                ] }
                dialogWidth="xs"
                dialogVerticalAlign="center"
            >
                <Box display="flex" flexDirection="column" alignItems="center" gap="16px">
                    <ErrorOutlineIcon sx={ { fontSize: '88px', color: COLORS.error } } />
                    <TextOut>После удаления акканта, все пользователи будут отключены, персональные данные и базы 1С будут так же <TextOut style={ { color: COLORS.error } } fontWeight={ 700 }>удалены</TextOut>.</TextOut>
                </Box>
            </Dialog>
            <Dialog
                isOpen={ showDeleting }
                dialogWidth="xs"
                dialogVerticalAlign="center"
                onCancelClick={ closeDeletingDialog }
                buttons={ [
                    {
                        content: 'Отмена',
                        kind: 'default',
                        variant: 'contained',
                        onClick: () => closeDeletingDialog()
                    },
                    {
                        content: 'УДАЛИТЬ',
                        kind: 'error',
                        variant: 'contained',
                        onClick: () => deleteAccount()
                    }
                ] }
            >
                <Box display="flex" flexDirection="column" alignItems="center" gap="16px">
                    <ErrorOutlineIcon sx={ { fontSize: '88px', color: COLORS.error } } />
                    <TextOut>Введите код подтверждения <TextOut fontWeight={ 700 }>УДАЛЕНИЯ</TextOut> аккаунта</TextOut>
                    <TextField value={ code } onChange={ e => setCode(e.target.value) } hiddenLabel={ true } fullWidth={ true } />
                </Box>
            </Dialog>
        </>
    );
};