import { TGetConfirmationFilesRequest } from 'app/api/endpoints/accounts/request';
import { FETCH_API } from 'app/api/useFetchApi';
import { SelectDataCommonDataModel } from 'app/web/common';
import React, { useCallback, memo } from 'react';
import cn from 'classnames';
import { Box, Chip, Link, Tooltip } from '@mui/material';

import { EMessageType, useAccountCard, useAppSelector, useFloatMessages } from 'app/hooks';
import { TablePagination } from 'app/views/modules/_common/components/CommonTableWithFilter/TableView';
import { AccountCard } from 'app/views/modules/_common/reusable-modules/AccountCard';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { EVerificationStatus } from 'app/api/endpoints/accounts/enum';

import styles from '../style.module.css';

type TProps = {
    getData: (args?: SelectDataCommonDataModel<TGetConfirmationFilesRequest>) => void;
};

const { editCertificateStatus, getConfirmationFile, deleteConfirmationFile } = FETCH_API.ACCOUNTS;

export const AdministrationTable = memo(({ getData }: TProps) => {
    const { data } = useAppSelector(({ Accounts }) => Accounts.getConfirmationFilesReducer);

    const { openDialogHandler, closeDialogHandler, isOpen, dialogAccountNumber } = useAccountCard();
    const { show } = useFloatMessages();

    const getPagination = useCallback((): TablePagination | undefined => {
        if (data?.rawData?.metadata) {
            const { pageCount, pageNumber, pageSize, totalItemCount } = data.rawData.metadata;

            return {
                totalPages: pageCount,
                currentPage: pageNumber,
                pageSize,
                recordsCount: totalItemCount,
            };
        }

        return undefined;
    }, [data?.rawData.metadata]);

    const changeFileStatus = (id: string, status: EVerificationStatus) => async () => {
        const { success, message } = await editCertificateStatus({ id, status });

        if (success) {
            void getData();
        }

        show(success ? EMessageType.success : EMessageType.error, message);
    };

    const openFileHandler = (accountId: string, id: string) => async () => {
        const fileData = await getConfirmationFile(accountId, id);

        window.open(URL.createObjectURL(fileData), '_blank');
    };

    const deleteFileHandler = (accountId: string, id: string) => async () => {
        const { success, message } = await deleteConfirmationFile(accountId, id);

        if (success) {
            void getData();
        }

        show(success ? EMessageType.success : EMessageType.error, message);
    };
    return (
        <>
            <AccountCard isOpen={ isOpen } onCancelDialogButtonClick={ closeDialogHandler } accountNumber={ dialogAccountNumber } />
            <CommonTableWithFilter
                isBorderStyled={ true }
                isResponsiveTable={ true }
                uniqueContextProviderStateId="company-administation"
                onDataSelect={ getData }
                tableProps={ {
                    dataset: data?.rawData?.records ?? [],
                    keyFieldName: 'id',
                    pagination: getPagination(),
                    fieldsView: {
                        accountIndexNumber: {
                            caption: 'Номер аккаунта'
                        },
                        accountCaption: {
                            caption: 'Название аккаунта',
                            format: (value: string, row) =>
                                <Link onClick={ () => openDialogHandler(row.accountIndexNumber) }>{ value }</Link>
                        },
                        status: {
                            caption: 'Статус',
                            format: (value: EVerificationStatus | null) => {
                                switch (value) {
                                    case EVerificationStatus.underReview:
                                        return (
                                            <Chip label="На проверке" color="info" />
                                        );
                                    case EVerificationStatus.verified:
                                        return (
                                            <Chip label="Проверено" color="primary" />
                                        );
                                    case EVerificationStatus.canceled:
                                        return (
                                            <Chip label="Аннулировано" color="error" />
                                        );
                                    default:
                                        return (
                                            <Chip label="Не верифицирован" />
                                        );
                                }
                            }
                        },
                        id: {
                            caption: 'Действия',
                            fieldWidth: '7%',
                            format: (_, row) =>
                                <Box display="flex" gap="10px">
                                    <Tooltip title="Открыть">
                                        <i onClick={ openFileHandler(row.accountId, row.cloudFileId) } className={ cn('fa', 'fa-folder-open', styles.icon, styles.open) } aria-hidden="true" />
                                    </Tooltip>
                                    { row.status === EVerificationStatus.canceled && (
                                        <Tooltip title="Удалить">
                                            <i onClick={ deleteFileHandler(row.accountId, row.id) } className={ cn('fa', 'fa-trash', styles.icon, styles.cancel) } aria-hidden="true" />
                                        </Tooltip>
                                    ) }
                                    { ![EVerificationStatus.verified, EVerificationStatus.canceled].includes(row.status) && (
                                        <>
                                            <Tooltip title="Аннулировать" placement="top">
                                                <i onClick={ changeFileStatus(row.id, EVerificationStatus.canceled) } className={ cn('fa', 'fa-ban', styles.icon, styles.cancel) } aria-hidden="true" />
                                            </Tooltip>
                                            <Tooltip title="Одобрить" placement="top">
                                                <i onClick={ changeFileStatus(row.id, EVerificationStatus.verified) } className={ cn('fa', 'fa-check-circle-o', styles.icon, styles.approve) } aria-hidden="true" />
                                            </Tooltip>
                                        </>
                                    ) }
                                </Box>
                        }
                    }
                } }
            />
        </>
    );
});