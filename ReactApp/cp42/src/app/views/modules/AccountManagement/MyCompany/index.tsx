import React from 'react';
import { connect } from 'react-redux';

import { AccountUserGroup } from 'app/common/enums';
import { hasComponentChangesFor } from 'app/common/functions';
import { AppReduxStoreState } from 'app/redux/types';
import { TabsControl } from 'app/views/components/controls/TabsControl';
import { withHeader } from 'app/views/components/_hoc/withHeader';
import { AccountPermissionsEnum } from 'app/web/common/enums/AccountPermissionsEnum';

import { AdministrationView } from './view/AdministrationView';
import { MyCompanyPage } from './view/CompanyDataView';
import { Organizations } from './view/OrganizationsView';
import { DatabaseMigrationPage } from './view/TransferringDatabasesView';

type StateProps = {
    permissions: string[];
    currentUserGroups: AccountUserGroup[];
};

type OwnState = {
    activeTabIndex: number;
};

class MyCompanyViewClass extends React.Component<StateProps, OwnState> {
    private readonly canAdministrate = this.props.currentUserGroups.includes(AccountUserGroup.AccountSaleManager) || this.props.currentUserGroups.includes(AccountUserGroup.CloudAdmin);

    public constructor(props: StateProps) {
        super(props);

        this.state = {
            activeTabIndex: 0
        };

        this.onActiveTabIndexChanged = this.onActiveTabIndexChanged.bind(this);
        this.getTabsHeaders = this.getTabsHeaders.bind(this);
    }

    public shouldComponentUpdate(nextProps: StateProps, nextState: OwnState) {
        return hasComponentChangesFor(this.props, nextProps) || hasComponentChangesFor(this.state, nextState);
    }

    private onActiveTabIndexChanged(tabIndex: number) {
        this.setState({ activeTabIndex: tabIndex });
    }

    private getTabsHeaders() {
        return [
            {
                title: 'Данные о компании',
                isVisible: true,
            },
            {
                title: 'Дополнительные организации',
                isVisible: true,
            },
            {
                title: 'Перенос баз между хранилищами',
                isVisible: this.props.permissions.includes(AccountPermissionsEnum.AccountsMoveInfoBase)
            },
            {
                title: 'Управление',
                isVisible: this.canAdministrate
            },
        ];
    }

    public render() {
        return (
            <TabsControl
                activeTabIndex={ this.state.activeTabIndex }
                onActiveTabIndexChanged={ this.onActiveTabIndexChanged }
                headers={ this.getTabsHeaders() }
            >
                <MyCompanyPage />
                <Organizations />
                { this.props.permissions.includes(AccountPermissionsEnum.AccountsMoveInfoBase) &&
                    <DatabaseMigrationPage onDatabaseMigration={ this.props.permissions.includes(AccountPermissionsEnum.AccountsMoveInfoBase) } />
                }
                { this.canAdministrate && <AdministrationView /> }
            </TabsControl>
        );
    }
}

const MyCompanyPageViewConnect = connect<StateProps, NonNullable<unknown>, NonNullable<unknown>, AppReduxStoreState>(
    ({ Global: { getCurrentSessionSettingsReducer, getUserPermissionsReducer } }) => ({
        permissions: getUserPermissionsReducer.permissions,
        currentUserGroups: getCurrentSessionSettingsReducer.settings.currentContextInfo.currentUserGroups
    }),
    {}
)(MyCompanyViewClass);

export const MyCompanyPageView = withHeader({
    page: MyCompanyPageViewConnect,
    title: 'Данные о компании'
});