import { Box, Tooltip } from '@mui/material';
import { FETCH_API } from 'app/api/useFetchApi';
import { EMessageType, useFloatMessages, useIsOpen } from 'app/hooks';
import { ReceiveMyCompanyThunkParams } from 'app/modules/myCompany/store/reducers/receiveMyCompanyData/params';
import { ReceiveMyCompanyThunk } from 'app/modules/myCompany/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { SuccessButton } from 'app/views/components/controls/Button';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { TAdditionalCompany } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/receiveMyCompany';
import cn from 'classnames';
import { MouseEvent } from 'react';
import { connect } from 'react-redux';
import styles from './style.module.css';
import { OrganizationDialog } from './widgets/OrganizationDialog';

type TStateProps = {
    additionalCompanies: TAdditionalCompany[];
};

type TDispatchProps = {
    getCompanyData: (args: ReceiveMyCompanyThunkParams) => void;
};

type TProps = TStateProps & TDispatchProps;

const { deleteAdditionalCompany } = FETCH_API.ACCOUNTS;

const OrganizationsComponent = ({ additionalCompanies, getCompanyData }: TProps) => {
    const { state: { isOpen, data }, setOpen, setClose } = useIsOpen<TAdditionalCompany>();
    const { show } = useFloatMessages();

    const deletionHandler = (id: string) => async () => {
        const { message, success } = await deleteAdditionalCompany(id);

        show(success ? EMessageType.success : EMessageType.error, message);

        if (success) {
            getCompanyData({ force: true, showLoadingProgress: true });
        }
    };

    const editionHandler = (companyData: TAdditionalCompany) => (event: MouseEvent) => {
        setOpen(event, companyData);
    };

    return (
        <>
            <OrganizationDialog isOpen={ isOpen } onClose={ setClose } data={ data } getCompanyData={ getCompanyData } />
            <Box display="flex" flexDirection="column" gap="20px">
                <CommonTableWithFilter
                    uniqueContextProviderStateId="organizations"
                    isResponsiveTable={ true }
                    isBorderStyled={ true }
                    tableProps={ {
                        dataset: additionalCompanies ?? [],
                        keyFieldName: 'id',
                        fieldsView: {
                            name: {
                                caption: 'Наименование'
                            },
                            inn: {
                                caption: 'ИНН'
                            },
                            description: {
                                caption: 'Описание'
                            },
                            email: {
                                caption: 'E-Mail для отправки писем'
                            },
                            id: {
                                caption: 'Действия',
                                fieldWidth: '5%',
                                format: (value: string, row) => (
                                    <Box display="flex" gap="8px">
                                        <Tooltip title="Редактировать">
                                            <i className={ cn('fa fa-pencil', styles.action) } aria-hidden="true" onClick={ editionHandler(row) } />
                                        </Tooltip>
                                        <Tooltip title="Удалить">
                                            <i className={ cn('fa fa-trash-o', styles.action) } aria-hidden="true" onClick={ deletionHandler(value) } />
                                        </Tooltip>
                                    </Box>
                                )
                            }
                        }
                    } }
                />
                <SuccessButton onClick={ setOpen } className={ styles.button }>
                    Добавить организацию
                </SuccessButton>
            </Box>
        </>
    );
};

export const Organizations = connect<TStateProps, TDispatchProps, {}, AppReduxStoreState>(
    state => ({
        additionalCompanies: state.MyCompanyState.myCompany.additionalCompanies,
    }),
    {
        getCompanyData: ReceiveMyCompanyThunk.invoke
    }
)(OrganizationsComponent);