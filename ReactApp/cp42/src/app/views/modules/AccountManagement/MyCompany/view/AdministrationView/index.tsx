import { Box, MenuItem, OutlinedInput, Select, SelectChangeEvent } from '@mui/material';
import { EVerificationStatus } from 'app/api/endpoints/accounts/enum';
import { TGetConfirmationFilesRequest } from 'app/api/endpoints/accounts/request';

import { REDUX_API } from 'app/api/useReduxApi';
import { EMessageType, useAppDispatch, useAppSelector, useFloatMessages } from 'app/hooks';
import { SuccessButton } from 'app/views/components/controls/Button';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { SelectDataCommonDataModel } from 'app/web/common';
import React, { ChangeEvent, useCallback, useEffect, useRef, useState } from 'react';

import styles from './style.module.css';

import { AdministrationTable } from './widgets/AdministrationTable';

const { getConfirmationFiles } = REDUX_API.ACCOUNTS_REDUX;

export const AdministrationView = () => {
    const ref = useRef({ accountNumber: '', search: '', verificationStatus: EVerificationStatus.undefined });

    const dispatch = useAppDispatch();
    const { show } = useFloatMessages();

    const [accountIndexNumber, setAccountIndexNumber] = useState('');
    const [searchLine, setSearchLine] = useState('');
    const [status, setStatus] = useState(EVerificationStatus.undefined);

    const { error, isLoading } = useAppSelector(({ Accounts }) => Accounts.getConfirmationFilesReducer);

    useEffect(() => {
        void getConfirmationFiles(dispatch, {});
    }, [dispatch]);

    useEffect(() => {
        if (error) {
            show(EMessageType.error, error);
        }
    }, [show, error]);

    const getData = useCallback((args?: SelectDataCommonDataModel<TGetConfirmationFilesRequest>) => {
        const { verificationStatus, accountNumber, search } = ref.current;

        const parsedAccountNumber = parseInt(accountNumber, 10);

        void getConfirmationFiles(dispatch, {
            pageNumber: args?.pageNumber,
            searchLine: search,
            orderBy: args?.orderBy,
            filter: {
                accountIndexNumber: Number.isNaN(parsedAccountNumber) ? undefined : parsedAccountNumber,
                status: verificationStatus === EVerificationStatus.undefined ? undefined : verificationStatus,
            },
        });
    }, [dispatch]);

    const searchLineHandler = (ev: ChangeEvent<HTMLInputElement>) => {
        setSearchLine(ev.target.value);
        ref.current.search = ev.target.value;
    };

    const accountIndexNumberHandler = (ev: ChangeEvent<HTMLInputElement>) => {
        setAccountIndexNumber(ev.target.value);
        ref.current.accountNumber = ev.target.value;
    };

    const statusHandler = ({ target: { value } }: SelectChangeEvent<EVerificationStatus>) => {
        if (typeof value !== 'string') {
            setStatus(value);
            ref.current.verificationStatus = value;
        }
    };

    return (
        <>
            { isLoading && <LoadingBounce /> }
            <Box display="flex" flexDirection="column" gap="20px">
                <Box className={ styles.container }>
                    <FormAndLabel label="Название аккаунта">
                        <OutlinedInput
                            onChange={ searchLineHandler }
                            value={ searchLine }
                            fullWidth={ true }
                        />
                    </FormAndLabel>
                    <FormAndLabel label="Номер аккаунта">
                        <OutlinedInput
                            onChange={ accountIndexNumberHandler }
                            value={ accountIndexNumber ?? '' }
                            fullWidth={ true }
                            error={ /[^0-9]$/.test(accountIndexNumber) }
                        />
                    </FormAndLabel>
                    <FormAndLabel label="Статус">
                        <Select
                            fullWidth={ true }
                            value={ status }
                            variant="outlined"
                            onChange={ statusHandler }
                        >
                            <MenuItem value={ EVerificationStatus.undefined }>Все</MenuItem>
                            <MenuItem value={ EVerificationStatus.underReview }>На проверке</MenuItem>
                            <MenuItem value={ EVerificationStatus.canceled }>Аннулировано</MenuItem>
                            <MenuItem value={ EVerificationStatus.verified }>Проверено</MenuItem>
                        </Select>
                    </FormAndLabel>
                    <SuccessButton onClick={ () => getData() }>Найти</SuccessButton>
                </Box>
                <AdministrationTable getData={ getData } />
            </Box>
        </>
    );
};