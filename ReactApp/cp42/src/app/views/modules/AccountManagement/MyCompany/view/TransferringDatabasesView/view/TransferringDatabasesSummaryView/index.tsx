import React from 'react';
import { AppReduxStoreState } from 'app/redux/types';
import { Table } from 'app/views/components/controls/Table';
import { DatabaseMigrationDataModel } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/receiveDatabaseMigration/data-model';
import { connect } from 'react-redux';

type TProps = {
    dataset: DatabaseMigrationDataModel;
    hasDatabases: boolean;
};

class TransferringDatabasesSummaryViewClass extends React.PureComponent<TProps> {
    public render() {
        if (!this.props.hasDatabases) {
            return null;
        }

        const total = {
            fileStorage: 'Всего',
            totatlSizeOfDatabases: this.props.dataset.fileStorageAccountDatabasesSummary.map(item => item.totatlSizeOfDatabases).reduce((prev, curr) => prev + curr, 0),
            databaseCount: this.props.dataset.fileStorageAccountDatabasesSummary.map(item => item.databaseCount).reduce((prev, curr) => prev + curr, 0)
        };

        return (
            <Table
                isResponsiveTable={ true }
                isBorderStyled={ true }
                dataset={ [...this.props.dataset.fileStorageAccountDatabasesSummary, total] }
                keyFieldName="fileStorage"
                fieldsView={ {
                    fileStorage: {
                        caption: 'Хранилище',
                        isSortable: false,
                    },
                    totatlSizeOfDatabases: {
                        caption: 'Размер, МБ',
                        isSortable: false,
                    },
                    databaseCount: {
                        caption: 'Количество',
                        isSortable: false,
                    }
                } }
            />
        );
    }
}

export const TransferringDatabasesSummaryView = connect<TProps, {}, {}, AppReduxStoreState>(
    state => {
        const transferringDatabaseState = state.MyCompanyState;
        const dataset = transferringDatabaseState.databaseMigration;
        const hasDatabases = transferringDatabaseState.databaseMigration.fileStorageAccountDatabasesSummary.length > 0;

        return {
            dataset,
            hasDatabases
        };
    }
)(TransferringDatabasesSummaryViewClass);