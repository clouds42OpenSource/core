import { Box } from '@mui/material';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import React from 'react';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { TransferringDatabaseFilterDataFrom } from 'app/views/modules/AccountManagement/MyCompany/types/TransferringDatabaseFilterDataFrom';
import { hasComponentChangesFor } from 'app/common/functions';
import { TextBoxForm } from 'app/views/components/controls/forms/TextBoxForm';
import { TransferringDatabasesSummaryView } from 'app/views/modules/AccountManagement/MyCompany/view/TransferringDatabasesView/view/TransferringDatabasesSummaryView';
import { TransferringDatabaseFilterDataFromType } from 'app/views/modules/AccountManagement/MyCompany/types/TransferringDatabaseFilterDataFromType';

type OwnProps = ReduxFormProps<TransferringDatabaseFilterDataFrom> & {
    isFilterFormatDisable: boolean;
    onFilterChange: (filter: TransferringDatabaseFilterDataFrom, filterForName: string) => void;
};

class TransferringDatabasesFilterFormViewClass extends React.Component<OwnProps> {
    public constructor(props: OwnProps) {
        super(props);

        this.initReduxForm = this.initReduxForm.bind(this);
        this.onValueChange = this.onValueChange.bind(this);
        this.onValueApplied = this.onValueApplied.bind(this);
    }

    public componentDidMount() {
        this.props.reduxForm.setInitializeFormDataAction(this.initReduxForm);
    }

    public shouldComponentUpdate(nextProps: OwnProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private onValueChange<TValue, TForm extends string = TransferringDatabaseFilterDataFromType>(formName: TForm, newValue: TValue) {
        this.props.reduxForm.updateReduxFormFields({
            [formName]: newValue
        });
    }

    private onValueApplied<TValue>(formName: string, value: TValue) {
        this.props.onFilterChange({
            ...this.props.reduxForm.getReduxFormFields(false),
            [formName]: value
        }, formName);
    }

    private initReduxForm(): TransferringDatabaseFilterDataFrom | undefined {
        return {
            searchLine: ''
        };
    }

    public render() {
        const formField = this.props.reduxForm.getReduxFormFields(false);

        return (
            <Box display="flex" flexDirection="column" gap="10px">
                <TransferringDatabasesSummaryView />
                <FormAndLabel label="Поиск">
                    <TextBoxForm
                        autoFocus={ false }
                        placeholder="Введите номер, размер или тип"
                        formName="searchLine"
                        onValueChange={ this.onValueChange }
                        onValueApplied={ this.onValueApplied }
                        value={ formField.searchLine }
                        isReadOnly={ this.props.isFilterFormatDisable }
                    />
                </FormAndLabel>
            </Box>
        );
    }
}

export const TransferringDatabasesFilterFormView = withReduxForm(TransferringDatabasesFilterFormViewClass, {
    reduxFormName: 'TransferringDatabase_Filter_Form',
    resetOnUnmount: true
});