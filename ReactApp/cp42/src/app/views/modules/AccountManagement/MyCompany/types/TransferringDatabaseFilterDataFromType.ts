import { TransferringDatabaseFilterDataFrom } from 'app/views/modules/AccountManagement/MyCompany/types/TransferringDatabaseFilterDataFrom';

export type TransferringDatabaseFilterDataFromType = keyof TransferringDatabaseFilterDataFrom;