import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import React from 'react';
import { hasComponentChangesFor } from 'app/common/functions';
import { ComboBoxForm } from 'app/views/components/controls/forms';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { BaseButton } from 'app/views/components/controls/Button/BaseButton';
import { ChangeDatabaseMigrationThunkParams } from 'app/modules/myCompany/store/reducers/changeDatabaseMigration/params';
import { connect } from 'react-redux';
import { AppReduxStoreState } from 'app/redux/types';
import { ChangeDatabaseMigrationThunk } from 'app/modules/myCompany/store/thunks';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { FloatMessageEnum } from 'app/views/modules/AccountManagement/MyCompany/enums/FloatMessageEnum';
import { AvalableFileStoragesItem } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/receiveDatabaseMigration/data-model/DatabaseMigrationItemDataModel';

type StateProps = {
    dataset: AvalableFileStoragesItem[],
    selectDatabases: string[];
    clearDatabasesForMigrate: () => void;
} & ReduxFormProps<{
    fileStorageId: string;
}>;

type DispatchProps = {
    dispatchChangeDatabaseMigrationThunk: (args: ChangeDatabaseMigrationThunkParams) => void;
};

type OwnProps = FloatMessageProps;

type AllProps = StateProps & DispatchProps & OwnProps;

class TransferringDatabaseComboBox extends React.Component<AllProps> {
    public constructor(props: AllProps) {
        super(props);

        this.onValueChange = this.onValueChange.bind(this);
        this.migrateDatabases = this.migrateDatabases.bind(this);
        this.props.reduxForm.setInitializeFormDataAction(() => ({
            fileStorageId: this.props.dataset[0].key
        }));
    }

    public shouldComponentUpdate(nextProps: AllProps) {
        return hasComponentChangesFor(this.props, nextProps);
    }

    private onValueChange(formName: string, value: string) {
        this.props.reduxForm.updateReduxFormFields({
            [formName]: value
        });
    }

    private migrateDatabases() {
        const reduxForm = this.props.reduxForm.getReduxFormFields(false);
        this.props.dispatchChangeDatabaseMigrationThunk({
            databases: this.props.selectDatabases,
            storage: reduxForm.fileStorageId,
            force: true
        });
        this.props.clearDatabasesForMigrate();
        this.props.floatMessage.show(MessageType.Default, FloatMessageEnum.TransferringDatabase, 1500);
    }

    public render() {
        const reduxForm = this.props.reduxForm.getReduxFormFields(false);

        return (
            <>
                <FormAndLabel label="Доступные хранилища">
                    <ComboBoxForm
                        formName="fileStorageId"
                        items={ this.props.dataset.map(item => ({ value: item.key, text: item.value })) }
                        value={ reduxForm.fileStorageId }
                        onValueChange={ this.onValueChange }
                    />
                </FormAndLabel>
                <BaseButton style={ { width: 'fit-content' } } kind="primary" onClick={ this.migrateDatabases }>Перенести</BaseButton>
            </>
        );
    }
}

const TransferringDatabaseComboBoxConnect = connect<{}, DispatchProps, {}, AppReduxStoreState>(
    null,
    { dispatchChangeDatabaseMigrationThunk: ChangeDatabaseMigrationThunk.invoke }
)(TransferringDatabaseComboBox);

export const TransferringDatabaseComboBoxReduxForm = withReduxForm(TransferringDatabaseComboBoxConnect, { reduxFormName: 'TransferringDataset_ReduxForm' });

export const TransferringDatabaseComboBoxView = withFloatMessages(TransferringDatabaseComboBoxReduxForm);