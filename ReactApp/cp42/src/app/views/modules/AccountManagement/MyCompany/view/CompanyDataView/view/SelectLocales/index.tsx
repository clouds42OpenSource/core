import React from 'react';

import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { ComboBoxForm } from 'app/views/components/controls/forms';
import { MyCompanyDataModel } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/receiveMyCompany';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';

type StateProps = {
    dataset: MyCompanyDataModel;
    isReadOnly: boolean;
};

type OwnProps = ReduxFormProps<{ locales: string }>;

type AllProps = StateProps & OwnProps;

class SelectLocalesClass extends React.Component<AllProps> {
    constructor(props: AllProps) {
        super(props);

        this.onValueChange = this.onValueChange.bind(this);
        this.props.reduxForm.setInitializeFormDataAction(() => ({ locales: this.props.dataset.localeId }));
    }

    private onValueChange(formName: string, newValue: string) {
        this.props.reduxForm.updateReduxFormFields({ [formName]: newValue });
    }

    public render() {
        const reduxForm = this.props.reduxForm.getReduxFormFields(false);

        return (
            <FormAndLabel label="Местоположение">
                <ComboBoxForm
                    formName="locales"
                    items={ this.props.dataset.locales?.map(item => ({ value: item.id, text: item.value })) ?? [] }
                    value={ reduxForm.locales }
                    onValueChange={ this.onValueChange }
                    isReadOnly={ this.props.isReadOnly }
                />
            </FormAndLabel>
        );
    }
}

export const SelectLocalesView = withReduxForm(SelectLocalesClass, { reduxFormName: 'SelectLocalesClass_ReduxForm' });