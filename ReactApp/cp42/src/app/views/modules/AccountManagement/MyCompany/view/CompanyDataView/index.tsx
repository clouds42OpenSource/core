import { Box } from '@mui/material';
import { EVerificationStatus } from 'app/api/endpoints/accounts/enum';
import { AccountUserGroup } from 'app/common/enums';
import { ReceiveMyCompanyThunkParams } from 'app/modules/myCompany/store/reducers/receiveMyCompanyData/params';
import { ReceiveMyCompanyThunk } from 'app/modules/myCompany/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { COLORS } from 'app/utils';
import { ContainedButton, OutlinedButton } from 'app/views/components/controls/Button';
import { BaseButton } from 'app/views/components/controls/Button/BaseButton';
import { TextOut } from 'app/views/components/TextOut';
import { CommonTableBasic } from 'app/views/modules/_common/components/CommonTableBasic';
import { CompanyDataCaptionEnum } from 'app/views/modules/AccountManagement/MyCompany/enums/CompanyDataCaptionEnum';
import { AccountDeletingModal } from 'app/views/modules/AccountManagement/MyCompany/view/CompanyDataView/view/AccountDeletingModal';
import { EditMyCompanyView } from 'app/views/modules/AccountManagement/MyCompany/view/CompanyDataView/view/EditMyCompanyForm';
import { MyCompanyCard } from 'app/views/modules/AccountManagement/MyCompany/view/CompanyDataView/view/MyCompanySegmentCard';
import { VerificationModal } from 'app/views/modules/AccountManagement/MyCompany/view/CompanyDataView/view/VerificationModal';
import { AccountPermissionsEnum } from 'app/web/common/enums/AccountPermissionsEnum';
import { MyCompanyDataModel } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/receiveMyCompany';
import React from 'react';
import { connect } from 'react-redux';

type StateProps = {
    dataset: MyCompanyDataModel;
    isInLoadingDataProcess: boolean;
    permissionsReceived: boolean;
    permissions: string[];
    currentUserGroups: AccountUserGroup[];
};

type DispatchProps = {
    dispatchReceiveMyCompanyThunk: (args: ReceiveMyCompanyThunkParams) => void;
};

type OwnState = {
    isEditMyCompanyData: boolean;
    isMyCompanyCardVisible: boolean;
    isOpenAccountDeletingModal: boolean;
    isOpenVerificationModal: boolean;
};

type AllProps = StateProps & DispatchProps;

class CompanyData extends React.Component<AllProps, OwnState> {
    public constructor(props: AllProps) {
        super(props);

        this.hideMyCompanySegmentCard = this.hideMyCompanySegmentCard.bind(this);
        this.isEditMyCompanyDataChange = this.isEditMyCompanyDataChange.bind(this);
        this.showMyCompanySegmentCard = this.showMyCompanySegmentCard.bind(this);
        this.openAccountDeletingModal = this.openAccountDeletingModal.bind(this);
        this.closeAccountDeletingModal = this.closeAccountDeletingModal.bind(this);
        this.openVerificationModal = this.openVerificationModal.bind(this);
        this.closeVerificationModal = this.closeVerificationModal.bind(this);

        this.state = {
            isEditMyCompanyData: true,
            isMyCompanyCardVisible: false,
            isOpenAccountDeletingModal: false,
            isOpenVerificationModal: false,
        };
    }

    public componentDidMount() {
        this.props.dispatchReceiveMyCompanyThunk({ force: true });
    }

    private get tableData() {
        const { departments, accountCaption, indexNumber, accountInn, isVip, accountSaleManagerCaption, segmentName, accountDescription, emails, localeCurrency, createClusterDatabase } = this.props.dataset;
        const isNeedFullInfo = this.props.permissions.includes(AccountPermissionsEnum.AccountsViewFullInfo);

        return [
            {
                caption: CompanyDataCaptionEnum.caption,
                value: accountCaption,
                visible: true
            },
            {
                caption: CompanyDataCaptionEnum.indexNumber,
                value: indexNumber,
                visible: true
            },
            {
                caption: localeCurrency !== 'тг' ? CompanyDataCaptionEnum.accountInn : CompanyDataCaptionEnum.kazakhstanIin,
                value: accountInn ?? '',
                visible: true
            },
            {
                caption: CompanyDataCaptionEnum.isVip,
                value: isVip,
                visible: isNeedFullInfo
            },
            {
                caption: CompanyDataCaptionEnum.createClusterDatabase,
                value: createClusterDatabase,
                visible: isNeedFullInfo
            },
            {
                caption: CompanyDataCaptionEnum.accountSaleManagerCaption,
                value: accountSaleManagerCaption,
                visible: isNeedFullInfo
            },
            {
                caption: CompanyDataCaptionEnum.segmentName,
                value: segmentName,
                visible: this.props.permissions.includes(AccountPermissionsEnum.AccountsViewFullInfoWithSegments)
            },
            {
                caption: CompanyDataCaptionEnum.accountDescription,
                value: accountDescription,
                visible: true
            },
            {
                caption: CompanyDataCaptionEnum.emails,
                value: emails.join(' '),
                visible: true
            },
            {
                caption: CompanyDataCaptionEnum.departments,
                value: departments.map(department => department.name).join(' '),
                visible: true
            }
        ];
    }

    private get verification() {
        const { dataset: { verificationStatus } } = this.props;

        switch (verificationStatus) {
            case EVerificationStatus.underReview:
                return (
                    <TextOut style={ { color: COLORS.info } } fontSize={ 14 } fontWeight={ 700 }>Документы на проверке</TextOut>
                );
            case EVerificationStatus.verified:
                return (
                    <TextOut style={ { color: COLORS.main } } fontSize={ 14 } fontWeight={ 700 }>Документы верифицированы</TextOut>
                );
            case EVerificationStatus.canceled:
                return (
                    <Box display="flex" flexDirection="column" gap="5px">
                        <OutlinedButton kind="warning" onClick={ this.openVerificationModal }>
                            Верифицировать
                        </OutlinedButton>
                        <TextOut fontSize={ 10 } style={ { color: COLORS.warning } }>Документы не были приняты</TextOut>
                    </Box>
                );
            default:
                return (
                    <OutlinedButton kind="success" onClick={ this.openVerificationModal }>
                        Верифицировать
                    </OutlinedButton>
                );
        }
    }

    private hideMyCompanySegmentCard() {
        this.setState({ isMyCompanyCardVisible: false });
    }

    private showMyCompanySegmentCard() {
        this.setState({ isMyCompanyCardVisible: true });
    }

    private openAccountDeletingModal() {
        this.setState({ isOpenAccountDeletingModal: true });
    }

    private closeAccountDeletingModal() {
        this.setState({ isOpenAccountDeletingModal: false });
    }

    private openVerificationModal() {
        this.setState({ isOpenVerificationModal: true });
    }

    private closeVerificationModal() {
        this.setState({ isOpenVerificationModal: false });
    }

    private isEditMyCompanyDataChange() {
        this.setState(prev => ({ isEditMyCompanyData: !prev.isEditMyCompanyData }));
    }

    public render() {
        const { dataset, currentUserGroups, permissionsReceived, permissions, isInLoadingDataProcess, dispatchReceiveMyCompanyThunk } = this.props;
        const { isOpenAccountDeletingModal, isMyCompanyCardVisible, isEditMyCompanyData, isOpenVerificationModal } = this.state;

        return (
            <>
                { isEditMyCompanyData
                    ? (
                        <>
                            <CommonTableBasic dataset={ this.tableData } />
                            <Box marginLeft="12px" display="flex" justifyContent="space-between" flexWrap="wrap" gap="10px" alignItems="center">
                                { this.verification }
                                <Box display="flex" gap="10px" flexWrap="wrap">
                                    { (currentUserGroups.includes(AccountUserGroup.AccountAdmin) || currentUserGroups.includes(AccountUserGroup.CloudAdmin)) && (
                                        <ContainedButton
                                            kind="error"
                                            onClick={ this.openAccountDeletingModal }
                                        >
                                            Удалить аккаунт
                                        </ContainedButton>
                                    ) }
                                    { permissionsReceived && permissions.includes(AccountPermissionsEnum.SegmentView) && (
                                        <BaseButton variant="contained" kind="success" onClick={ this.showMyCompanySegmentCard }>
                                            <i className="fa fa-bookmark" />&nbsp;Сменить сегмент
                                        </BaseButton>
                                    ) }
                                    { permissionsReceived && permissions.includes(AccountPermissionsEnum.AccountsEdit) && (
                                        <BaseButton variant="contained" kind="turquoise" onClick={ this.isEditMyCompanyDataChange }>
                                            <i className="fa fa-edit" />&nbsp;Редактировать
                                        </BaseButton>
                                    ) }
                                </Box>
                            </Box>
                        </>
                    )
                    : (
                        <EditMyCompanyView
                            itemToEdit={ { ...dataset, accountInn: dataset.accountInn ?? '' } }
                            cancelButton={ this.isEditMyCompanyDataChange }
                        />
                    )
                }
                { isInLoadingDataProcess && (
                    <MyCompanyCard
                        isOpen={ isMyCompanyCardVisible }
                        onCancelDialogButtonClick={ this.hideMyCompanySegmentCard }
                        activeSegmentId={ dataset.segmentId }
                        onChangeStatus={ permissions.includes(AccountPermissionsEnum.AccountsEditFullInfoWithSegments) }
                    />
                ) }
                <AccountDeletingModal
                    isOpen={ isOpenAccountDeletingModal }
                    onCancelClick={ this.closeAccountDeletingModal }
                />
                <VerificationModal
                    getCompanyData={ dispatchReceiveMyCompanyThunk }
                    isOpen={ isOpenVerificationModal }
                    onClose={ this.closeVerificationModal }
                />
            </>
        );
    }
}

export const MyCompanyPage = connect<StateProps, DispatchProps, NonNullable<unknown>, AppReduxStoreState>(
    ({ Global: { getCurrentSessionSettingsReducer, getUserPermissionsReducer }, MyCompanyState: { myCompany, hasSuccessFor } }) => ({
        currentUserGroups: getCurrentSessionSettingsReducer.settings.currentContextInfo.currentUserGroups,
        dataset: myCompany,
        isInLoadingDataProcess: hasSuccessFor.hasMyCompanyReceived,
        permissionsReceived: getUserPermissionsReducer.hasSuccessFor.permissionsReceived,
        permissions: getUserPermissionsReducer.permissions
    }),
    {
        dispatchReceiveMyCompanyThunk: ReceiveMyCompanyThunk.invoke
    }
)(CompanyData);