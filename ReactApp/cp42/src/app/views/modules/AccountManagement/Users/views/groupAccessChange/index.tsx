import { Box, FormControlLabel, MenuItem, OutlinedInput, Pagination, Select } from '@mui/material';
import Checkbox from '@mui/material/Checkbox';
import { contextAccountId } from 'app/api';
import { TChangeDatabaseAccessRequest } from 'app/api/endpoints/accountUsers/request';
import { TAccountProfile, TDatabaseAccesses } from 'app/api/endpoints/accountUsers/response';
import { FETCH_API } from 'app/api/useFetchApi';
import { REDUX_API } from 'app/api/useReduxApi';
import { configurationProfilesToRole } from 'app/common/functions';
import { EMessageType, useAppDispatch, useAppSelector, useFloatMessages } from 'app/hooks';
import { getDatabaseAccessUsersListSlice } from 'app/modules/accountUsers';
import { Dialog } from 'app/views/components/controls/Dialog';
import { CheckBoxForm, DropDownListWithCheckboxes } from 'app/views/components/controls/forms';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { HomeSvgSelector } from 'app/views/components/svgGenerator/HomeSvgSelector';
import { TextOut } from 'app/views/components/TextOut';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { PaymentDialog } from 'app/views/modules/_common/dialogs';
import { InputCalculateAccesses } from 'app/web/api/InfoDbCardProxy/request-dto/InputCalculateAccesses';
import { SortingDataModel } from 'app/web/common';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { CalculateAccessInfoDbDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/CalculateAccessInfoDbDataModel';
import { RequestKind } from 'core/requestSender/enums';
import React, { useCallback, useEffect, useMemo, useState } from 'react';

import style from './style.module.css';

type TGroupAccessChange = {
    isOpen: boolean;
    onCancelClick: () => void;
    userId: string;
    userName: string;
    isExternalUser?: boolean;
};

const { getDatabaseAccessUsersList, getGivenAccountProfilesList } = REDUX_API.ACCOUNT_USERS_REDUX;
const { changeDatabaseAccessOnDelimiters, putDatabaseAccess } = FETCH_API.ACCOUNT_USERS;
const { putServicesApply } = FETCH_API.MANAGING_SERVICE_SUBSCRIPTIONS;
const { CalculateAccesses } = InterlayerApiProxy.getInfoDbCardApi();

const pageSize = 10;

const { changeStatus } = getDatabaseAccessUsersListSlice.actions;

type SortingField = 'caption' | 'configurationName' | 'v82Name';

export const GroupAccessChange = ({ isOpen, onCancelClick, userId, userName, isExternalUser }: TGroupAccessChange) => {
    const dispatch = useAppDispatch();
    const { show } = useFloatMessages();
    const {
        databaseAccessUsersListReducer: { data, isLoading, error },
        accountProfilesListReducer: { data: profileData, isLoading: isProfileLoading, error: profileError },
        givenAccountProfilesListReducer: { data: givenProfileData, isLoading: isGivenProfileLoading, error: givenProfileError }
    } = useAppSelector(state => state.AccountUsersState);
    const { locale } = useAppSelector(state => state.Global.getCurrentSessionSettingsReducer.settings.currentContextInfo);

    const [isInLoading, setIsInLoading] = useState(false);
    const [currentPage, setCurrentPage] = useState(1);
    const [searchString, setSearchString] = useState('');
    const [sortingData, setSortingData] = useState<SortingDataModel>();
    const [paymentDialogParams, setPaymentDialogParams] = useState({ isOpen: false, isPromisePayment: false, sum: 0 });

    const [changedDatabaseStatusOnDelimiters, setChangedDatabaseStatusOnDelimiters] = useState<string[]>([]);
    const [changedDatabaseStatus, setChangedDatabaseStatus] = useState<string[]>([]);
    const [hasAccessDatabase, setHasAccessDatabase] = useState(false);

    const [lastCalculateCostResult, setLastCalculateCostResult] = useState<CalculateAccessInfoDbDataModel>([]);
    const [lastCalculateCostResponse, setLastCalculateCostResponse] = useState<InputCalculateAccesses>({
        serviceTypesIdsList: [],
        accountUserDataModelsList: [{
            UserId: userId,
            HasAccess: true
        }]
    });

    const [isFile, setIsFile] = useState(false);
    const [selectConfiguration, setSelectConfiguration] = useState('');
    const [selectProfiles, setSelectProfiles] = useState<Omit<TAccountProfile, 'databases'>[]>([]);

    const [isFirstShowWarning, setIsFirstShowWarning] = useState(true);

    useEffect(() => {
        if (locale === 'ru-ua') {
            setIsFile(true);
        }
    }, [locale]);

    const getCurrentDataset = useCallback(() => {
        const currentDataset = data?.databaseAccesses ?? [];
        const filterString = searchString.toLowerCase();

        return currentDataset.filter(dbInfo => {
            const searchStringFilter = (
                dbInfo.caption.toLowerCase().includes(filterString) ||
                (dbInfo.configurationName ? dbInfo.configurationName.toLowerCase() : '').includes(filterString) ||
                dbInfo.v82Name.toLowerCase().includes(filterString)
            );

            const typeDatabase = isFile
                ? !dbInfo.isDbOnDelimiters
                : selectConfiguration ? dbInfo.configurationCode === selectConfiguration : true;

            return dbInfo && searchStringFilter && typeDatabase;
        }).sort((a, b) => {
            if (sortingData) {
                const { fieldName, sortKind } = sortingData;
                const firstElement = a[fieldName as SortingField];
                const secondElement = b[fieldName as SortingField];

                if (firstElement && secondElement) {
                    if (sortKind) {
                        if (firstElement > secondElement) {
                            return 1;
                        }

                        if (firstElement < secondElement) {
                            return -1;
                        }

                        return 0;
                    }

                    if (firstElement > secondElement) {
                        return -1;
                    }

                    if (firstElement < secondElement) {
                        return 1;
                    }

                    return 0;
                }
            }

            return 0;
        });
    }, [data?.databaseAccesses, isFile, searchString, selectConfiguration, sortingData]);

    const dataset = useMemo(getCurrentDataset, [getCurrentDataset]);

    const currentPageHandler = (_: React.ChangeEvent<unknown>, page: number) => {
        setCurrentPage(page);
    };

    const costSum = useMemo(() => lastCalculateCostResult.reduce((accum, costResultElem) => accum + costResultElem.accessCost, 0), [lastCalculateCostResult]);

    useEffect(() => {
        if (error) {
            show(EMessageType.error, error);
        }
    }, [error, show]);

    useEffect(() => {
        if (profileError) {
            show(EMessageType.error, profileError);
        }
    }, [profileError, show]);

    useEffect(() => {
        if (givenProfileError) {
            show(EMessageType.error, givenProfileError);
        }
    }, [givenProfileError, show]);

    const changeAccessToDatabase = (databaseId: string) => {
        setChangedDatabaseStatus(prevStatus => {
            const databaseIndex = prevStatus.indexOf(databaseId);

            if (databaseIndex !== -1) {
                prevStatus.splice(databaseIndex, 1);
            } else {
                prevStatus.push(databaseId);
            }

            return prevStatus;
        });
    };

    const giveAccessToDatabaseOnDelimiters = (databaseId: string) => {
        const databaseIsExist = changedDatabaseStatusOnDelimiters.find(database => database === databaseId);

        setChangedDatabaseStatusOnDelimiters(prevState => {
            if (!databaseIsExist) {
                prevState.push(databaseId);
            } else {
                prevState = prevState.flatMap(database => {
                    if (database === databaseId) {
                        return [];
                    }

                    return database;
                });
            }

            return [...prevState];
        });
    };

    const removeAccessFromDatabaseOnDelimiters = (databaseId: string) => {
        if (givenProfileData) {
            setChangedDatabaseStatusOnDelimiters(prevState => prevState.filter(database => database !== databaseId));
        }
    };

    const checkboxHandler = (__: string, value: boolean, ___: boolean, { myDatabasesServiceTypeId, databaseId }: TDatabaseAccesses, isNeedToCountImmediate = true) => {
        if (data) {
            dispatch(changeStatus(databaseId));

            setLastCalculateCostResponse(prevState => {
                if (value) {
                    prevState.serviceTypesIdsList.push(myDatabasesServiceTypeId);

                    if (!isFile) {
                        giveAccessToDatabaseOnDelimiters(databaseId);
                    }
                } else {
                    const index = prevState.serviceTypesIdsList.indexOf(myDatabasesServiceTypeId);

                    if (index !== -1) {
                        prevState.serviceTypesIdsList.splice(index, 1);
                    }

                    if (!isFile) {
                        removeAccessFromDatabaseOnDelimiters(databaseId);
                    }
                }

                if (isFile) {
                    changeAccessToDatabase(databaseId);
                }

                if (isNeedToCountImmediate) {
                    CalculateAccesses(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, prevState).then(result => {
                        setLastCalculateCostResult(result);
                    });
                }

                return prevState;
            });
        }
    };

    const checkboxHeaderHandler = (formName: string, value: boolean, prevValue: boolean, databaseAccesses: TDatabaseAccesses[]) => {
        if (isFile) {
            if (value) {
                databaseAccesses.forEach(databaseAccess => {
                    if (!changedDatabaseStatus.includes(databaseAccess.databaseId)) {
                        checkboxHandler(formName, value, prevValue, databaseAccess, false);
                    }
                });
            } else {
                databaseAccesses.forEach(databaseAccess => {
                    if (changedDatabaseStatus.includes(databaseAccess.databaseId)) {
                        checkboxHandler(formName, value, prevValue, databaseAccess, false);
                    }
                });
            }
        } else if (value) {
            databaseAccesses.forEach(databaseAccess => {
                if (!changedDatabaseStatusOnDelimiters.includes(databaseAccess.databaseId)) {
                    checkboxHandler(formName, value, prevValue, databaseAccess, false);
                }
            });
        } else {
            databaseAccesses.forEach(databaseAccess => {
                if (changedDatabaseStatusOnDelimiters.includes(databaseAccess.databaseId)) {
                    checkboxHandler(formName, value, prevValue, databaseAccess, false);
                }
            });
        }

        CalculateAccesses(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, lastCalculateCostResponse).then(result => {
            setLastCalculateCostResult(result);
        });
    };

    const closeGroupAccessChangeDialog = () => {
        onCancelClick();
        setIsFile(false);
        setChangedDatabaseStatus([]);
        setSelectProfiles([]);
        setSelectConfiguration('');
        setSearchString('');
        setIsInLoading(false);
        setCurrentPage(1);
        getDatabaseAccessUsersList(dispatch, userId, true, isExternalUser);
        getGivenAccountProfilesList(dispatch, userId);
        setLastCalculateCostResult([]);
        setLastCalculateCostResponse({
            serviceTypesIdsList: [],
            accountUserDataModelsList: [{
                UserId: userId,
                HasAccess: true
            }]
        });
        setChangedDatabaseStatusOnDelimiters([]);
    };

    const putServicesApplyHandler = async () => {
        setIsInLoading(true);
        let isSuccess = true;

        if (!isFile) {
            const body: TChangeDatabaseAccessRequest = {
                permissions: []
            };

            for (const databaseId of changedDatabaseStatusOnDelimiters) {
                body.permissions.push({
                    'application-id': databaseId,
                    'user-id': userId,
                    profiles: selectProfiles,
                    'allow-backup': false
                });
            }

            const { data: accessData } = await changeDatabaseAccessOnDelimiters(
                body,
                body.permissions.map(permission => {
                    const params = data?.databaseAccesses.find(access => access.databaseId === permission['application-id']);

                    return {
                        isExternal: false,
                        v82Name: params?.v82Name ?? '',
                        databaseName: params?.caption ?? '',
                        databaseId: params?.databaseId ?? '',
                        userId,
                        userName,
                    };
                })
            );

            if (accessData && accessData.message) {
                show(EMessageType.error, accessData.message);
                isSuccess = false;
            }

            if (accessData && accessData.results) {
                accessData.results.forEach(statusResult => {
                    if (statusResult.status === 'failure') {
                        show(EMessageType.warning, statusResult.error ?? `Не удалось отредактировать права в базу ${ statusResult['application-id'] }`);
                    }
                });
            }
        } else {
            const { data: accessData } = await putDatabaseAccess({
                usersId: userId,
                giveAccess: hasAccessDatabase,
                databaseIds: changedDatabaseStatus
            });

            if (accessData) {
                accessData.forEach(({ success, message, databasesName }) => {
                    if (!success) {
                        show(EMessageType.warning, `${ message } ${ databasesName ? `(${ databasesName })` : '' }`);
                    }
                });
            }
        }

        if (isSuccess) {
            show(EMessageType.success, 'Изменение доступов будет выполнено в течение 1 минуты. Пожалуйста, подождите.');
        }
        closeGroupAccessChangeDialog();
    };

    const closeDialogHandler = () => {
        setPaymentDialogParams({ isOpen: false, isPromisePayment: false, sum: 0 });
        closeGroupAccessChangeDialog();
    };

    const openDialogHandler = (isPromisePayment: boolean, sum: number) => {
        setPaymentDialogParams({ isOpen: true, isPromisePayment, sum });
    };

    const costApplyHandler = async (usePromisePayment?: boolean) => {
        setIsInLoading(true);
        const uniqueServiceTypeIdsList = [...new Set(lastCalculateCostResponse.serviceTypesIdsList)];
        const { data: responseData, message } = await putServicesApply({
            accountId: contextAccountId(),
            changedUsers: uniqueServiceTypeIdsList.map(uniqueId => ({
                billingServiceTypeId: uniqueId ?? '',
                status: true,
                subject: userId,
                sponsorship: {
                    i: false,
                    me: false,
                    label: ''
                }
            })),
            billingServiceId: data?.myDatabaseBillingServiceId ?? '',
            usePromisePayment
        });
        setIsInLoading(false);

        if (message) {
            show(EMessageType.error, message);
        } else if (responseData && responseData.isComplete) {
            void putServicesApplyHandler();

            if (paymentDialogParams.isOpen) {
                closeDialogHandler();
            }
        } else if (responseData && !responseData.isComplete) {
            openDialogHandler(responseData.canUsePromisePayment, responseData.notEnoughMoney ?? 0);
        }
    };

    return (
        <>
            { (isLoading || isProfileLoading || isGivenProfileLoading || isInLoading) && <LoadingBounce /> }
            <PaymentDialog
                apply={ costApplyHandler }
                isOpen={ paymentDialogParams.isOpen }
                onCancelClick={ closeDialogHandler }
                isPromisePaymentAvailable={ paymentDialogParams.isPromisePayment }
                sum={ paymentDialogParams.sum }
                currency={ data?.currency }
            />
            <Dialog
                isOpen={ isOpen }
                onCancelClick={ closeGroupAccessChangeDialog }
                dialogVerticalAlign="center"
                buttons={ [
                    {
                        content: 'Отмена',
                        kind: 'default',
                        variant: 'contained',
                        onClick: closeGroupAccessChangeDialog
                    },
                    {
                        content: costSum === 0 ? 'Применить' : `Купить (${ costSum } ${ data?.currency })`,
                        kind: 'success',
                        variant: 'contained',
                        hiddenButton: isFile ? !changedDatabaseStatus.length : !selectConfiguration || !changedDatabaseStatusOnDelimiters.length,
                        onClick: costSum === 0 ? putServicesApplyHandler : costApplyHandler
                    }
                ] }
                dialogWidth="md"
            >
                <Box display="flex" flexDirection="column" gap="8px">
                    {
                        locale !== 'ru-ua' && (
                            <>
                                <Box display="flex" gap="16px">
                                    <FormControlLabel
                                        control={
                                            <Checkbox
                                                checked={ isFile }
                                                onChange={ event => {
                                                    setIsFile(event.target.checked);
                                                    if (selectProfiles.length || selectConfiguration) {
                                                        setSelectConfiguration('');
                                                        setSelectProfiles([]);
                                                    }
                                                } }
                                                size="small"
                                            />
                                        }
                                        label={
                                            <TextOut fontWeight={ 700 }>Файловые/серверные базы</TextOut>
                                        }
                                        className={ style.configuration }
                                    />
                                    {
                                        isFile && (
                                            <FormAndLabel label="Выберите профили доступа" fullWidth={ true }>
                                                <DropDownListWithCheckboxes
                                                    noUpdateValueChangeHandler={ true }
                                                    deepCompare={ true }
                                                    hiddenAdditionalElement={ true }
                                                    renderValueIsChip={ true }
                                                    hiddenSearchLine={ true }
                                                    items={ [
                                                        {
                                                            text: 'Доступ к базе',
                                                            value: `${ hasAccessDatabase }`,
                                                            checked: hasAccessDatabase
                                                        }
                                                    ] }
                                                    label=""
                                                    formName="profiles"
                                                    onValueChange={ () => setHasAccessDatabase(prev => !prev) }
                                                />
                                            </FormAndLabel>
                                        )
                                    }
                                </Box>
                                {
                                    !isFile && (
                                        <Box display="flex" gap="16px">
                                            <FormAndLabel label="Выберите конфигурацию базы" fullWidth={ true } className={ style.configuration }>
                                                <Select
                                                    value={ selectConfiguration }
                                                    onChange={ event => setSelectConfiguration(event.target.value) }
                                                    fullWidth={ true }
                                                >
                                                    {
                                                        profileData?.map(profile => (
                                                            <MenuItem value={ profile.code } key={ profile.code }>{ profile.name }</MenuItem>
                                                        ))
                                                    }
                                                </Select>
                                            </FormAndLabel>
                                            {
                                                selectConfiguration && (
                                                    <FormAndLabel label="Выберите профили доступа" fullWidth={ true }>
                                                        <DropDownListWithCheckboxes
                                                            noUpdateValueChangeHandler={ true }
                                                            deepCompare={ true }
                                                            items={ (profileData ?? []).flatMap(profile => {
                                                                if (profile.code === selectConfiguration) {
                                                                    return profile.profiles.map(pr => ({
                                                                        text: pr.name,
                                                                        value: pr.id,
                                                                        checked: !!selectProfiles.find(item => item.id === pr.id)
                                                                    }));
                                                                }

                                                                return [];
                                                            }) }
                                                            label=""
                                                            hiddenSelectAll={ true }
                                                            renderValueIsChip={ true }
                                                            hiddenAdditionalElement={ true }
                                                            formName="profiles"
                                                            onValueChange={ (_, items) => setSelectProfiles(prevProfiles => (profileData ?? []).flatMap(profile => {
                                                                if (profile.code === selectConfiguration) {
                                                                    const { adminRole, userRole } = configurationProfilesToRole(profile.profiles, items);

                                                                    if (items.length > 1 && isFirstShowWarning && adminRole.length && userRole.length) {
                                                                        show(EMessageType.info, 'Установка профиля доступа "Администратор" одновременно с другими профилями через личный кабинет не поддерживается');
                                                                        setIsFirstShowWarning(false);
                                                                    }

                                                                    if (prevProfiles && prevProfiles.some(p => p.admin)) {
                                                                        return (userRole.length ? userRole : adminRole).filter(pr => items.find(item => item.value === pr.id));
                                                                    }

                                                                    return (adminRole.length ? adminRole : userRole).filter(pr => items.find(item => item.value === pr.id));
                                                                }

                                                                return [];
                                                            })) }
                                                        />
                                                    </FormAndLabel>
                                                )
                                            }
                                        </Box>
                                    )
                                }
                            </>
                        )
                    }
                    <FormAndLabel label="Поиск">
                        <OutlinedInput fullWidth={ true } placeholder="Поиск базы по названию или конфигурации" onChange={ event => setSearchString(event.target.value) } value={ searchString } />
                    </FormAndLabel>
                    <CommonTableWithFilter
                        uniqueContextProviderStateId="AccountUserGroupAccessChange"
                        tableProps={ {
                            emptyText: searchString ? `По запросу "${ searchString }" не удалось найти совпадений среди ваших информационных баз` : 'Информационные базы не найдены',
                            dataset: dataset.slice(pageSize * (currentPage - 1), pageSize * currentPage),
                            keyFieldName: 'databaseId',
                            fieldsView: {
                                databaseId: {
                                    fieldWidth: '4%',
                                    mobileCaption: 'Выдача доступа',
                                    caption: <CheckBoxForm
                                        className={ style.checkbox }
                                        label=""
                                        formName=""
                                        isReadOnly={ isFile ? !isFile : !selectConfiguration }
                                        isChecked={ isFile ? dataset.length === changedDatabaseStatus.length : dataset.length === changedDatabaseStatusOnDelimiters.length }
                                        onValueChange={ (formName, newValue, prevValue) => checkboxHeaderHandler(formName, newValue, prevValue, dataset) }
                                    />,
                                    format: (value, rowData) => {
                                        return (
                                            <CheckBoxForm
                                                className={ style.checkbox }
                                                label=""
                                                formName=""
                                                isReadOnly={ isFile ? !isFile : !selectConfiguration }
                                                isChecked={ isFile ? changedDatabaseStatus.includes(value) : changedDatabaseStatusOnDelimiters.includes(value) }
                                                onValueChange={ (formName, newValue, prevValue) => checkboxHandler(formName, newValue, prevValue, rowData) }
                                            />
                                        );
                                    }
                                },
                                templateImageName: {
                                    caption: '',
                                    noNeedForMobile: true,
                                    fieldWidth: '5%',
                                    format: (value: string) => <HomeSvgSelector width={ 36 } height={ 36 } id={ value } />
                                },
                                caption: {
                                    isSortable: true,
                                    caption: 'Название',
                                    fieldWidth: '35%',
                                    format: (value: string, { v82Name }) =>
                                        <Box display="flex" flexDirection="column" gap="5px">
                                            <TextOut>{ `${ value } (${ v82Name })` }</TextOut>
                                        </Box>
                                },
                                configurationName: {
                                    isSortable: true,
                                    fieldWidth: '35%',
                                    caption: 'Конфигурация',
                                },
                                v82Name: {
                                    isSortable: true,
                                    caption: 'Тип',
                                    format: (_, rowData) => {
                                        if (rowData.isFile) {
                                            return 'Файловая';
                                        }

                                        if (rowData.isDbOnDelimiters) {
                                            return 'Разделители';
                                        }

                                        return 'Серверная';
                                    }
                                }
                            }
                        } }
                        onDataSelect={ args => setSortingData(args.sortingData) }
                    />
                    <Pagination page={ currentPage } count={ Math.ceil(dataset.length / pageSize) } onChange={ currentPageHandler } />
                </Box>
            </Dialog>
        </>
    );
};