import { Box, Chip, Link, OutlinedInput, Pagination, Tooltip } from '@mui/material';
import { contextAccountId } from 'app/api';
import { EAccountDatabaseAccessState } from 'app/api/endpoints/accountUsers/enum';
import { TChangeDatabaseAccessRequest } from 'app/api/endpoints/accountUsers/request';
import { TAccountProfile, TDatabaseAccesses } from 'app/api/endpoints/accountUsers/response';
import { FETCH_API } from 'app/api/useFetchApi';
import { REDUX_API } from 'app/api/useReduxApi';
import { AppRoutes } from 'app/AppRoutes';
import { configurationProfilesToRole } from 'app/common/functions';
import { getEmptyUuid } from 'app/common/helpers';
import { TimerHelper } from 'app/common/helpers/TimerHelper';
import { EMessageType, useAppDispatch, useAppSelector, useFloatMessages } from 'app/hooks';
import { getAccountProfilesListSlice, getDatabaseAccessUsersListSlice } from 'app/modules/accountUsers';
import { COLORS } from 'app/utils';
import { ClockLoading } from 'app/views/components/controls';
import { SuccessButton } from 'app/views/components/controls/Button';
import { ButtonGroup } from 'app/views/components/controls/ButtonGroup';
import { ButtonGroupItem } from 'app/views/components/controls/ButtonGroup/types';
import { Dialog } from 'app/views/components/controls/Dialog';
import { DropDownListWithCheckboxes } from 'app/views/components/controls/forms';
import { DropDownListWithCheckboxItem } from 'app/views/components/controls/forms/DropDownListWithCheckboxes/types';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import CustomizedSwitches from 'app/views/components/controls/Switch/views/SwitchViews';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { HomeSvgSelector } from 'app/views/components/svgGenerator/HomeSvgSelector';
import { TextOut } from 'app/views/components/TextOut';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { PaymentDialog } from 'app/views/modules/_common/dialogs';
import { GroupAccessChange } from 'app/views/modules/AccountManagement/Users/views/groupAccessChange';
import { InputCalculateAccesses } from 'app/web/api/InfoDbCardProxy/request-dto/InputCalculateAccesses';
import { SortingDataModel } from 'app/web/common';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { CalculateAccessInfoDbDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/CalculateAccessInfoDbDataModel';
import { RequestKind } from 'core/requestSender/enums';
import React, { memo, useCallback, useEffect, useMemo, useState } from 'react';
import { useHistory } from 'react-router';

import styles from '../style.module.css';

type TDatabaseAccessSettings = {
    userId: string;
    userName: string;
    isRentActive?: boolean;
    isExternalUser?: boolean;
    hiddenFilter?: boolean;
    skipButton?: React.ReactNode;
    handleSkip?: () => void;
};

type TChangedDatabaseStatus = {
    give: string[];
    remove: string[];
};

type TChangedDatabaseStatusOnDelimiters = {
    databaseId: string;
    profiles: Omit<TAccountProfile, 'databases'>[];
    allowBackup?: boolean;
};

const { getDatabaseAccessUsersList, getAccountProfilesList, getGivenAccountProfilesList } = REDUX_API.ACCOUNT_USERS_REDUX;
const { putDatabaseAccess, changeDatabaseAccessOnDelimiters } = FETCH_API.ACCOUNT_USERS;
const { putServicesApply } = FETCH_API.MANAGING_SERVICE_SUBSCRIPTIONS;
const { CalculateAccesses } = InterlayerApiProxy.getInfoDbCardApi();

const buttonGroupElements: ButtonGroupItem[] = [
    {
        index: 0,
        text: 'Все базы'
    },
    {
        index: 1,
        text: 'C доступом'
    },
    {
        index: 2,
        text: 'Без доступа'
    }
];

const pageSize = 10;

const { changeStatus } = getDatabaseAccessUsersListSlice.actions;
const { refresh } = getAccountProfilesListSlice.actions;

const EMPTY_UUID = getEmptyUuid();

export const DatabaseAccessSettings = memo(({ userId, isRentActive, isExternalUser, userName, hiddenFilter, skipButton, handleSkip }: TDatabaseAccessSettings) => {
    const dispatch = useAppDispatch();
    const history = useHistory();
    const { show } = useFloatMessages();
    const {
        databaseAccessUsersListReducer: { data, isLoading, error },
        accountProfilesListReducer: { data: profileData, isLoading: isProfileLoading, error: profileError },
        givenAccountProfilesListReducer: { data: givenProfileData, isLoading: isGivenProfileLoading, error: givenProfileError }
    } = useAppSelector(state => state.AccountUsersState);
    const { locale } = useAppSelector(state => state.Global.getCurrentSessionSettingsReducer.settings.currentContextInfo);

    const [isInLoading, setIsInLoading] = useState(false);
    const [currentPage, setCurrentPage] = useState(1);
    const [selectedFilter, setSelectedFilter] = useState(0);
    const [searchString, setSearchString] = useState('');
    const [paymentDialogParams, setPaymentDialogParams] = useState({ isOpen: false, isPromisePayment: false, sum: 0 });

    const [changedDatabaseStatusOnDelimiters, setChangedDatabaseStatusOnDelimiters] = useState<TChangedDatabaseStatusOnDelimiters[]>([]);
    const [changedDatabaseStatus, setChangedDatabaseStatus] = useState<TChangedDatabaseStatus>({ give: [], remove: [] });
    const [lastCalculateCostResult, setLastCalculateCostResult] = useState<CalculateAccessInfoDbDataModel>([]);
    const [lastCalculateCostResponse, setLastCalculateCostResponse] = useState<InputCalculateAccesses>({
        serviceTypesIdsList: [],
        accountUserDataModelsList: [{
            UserId: userId,
            HasAccess: true
        }]
    });
    const [sorting, setSorting] = useState<SortingDataModel | undefined>();

    const [showCostDialog, setShowCostDialog] = useState({
        isOpen: false,
        databaseName: ''
    });
    const [showGroupAccessChangeDialog, setShowGroupAccessChangeDialog] = useState(false);
    const [isFirstShow, setIsFirstShow] = useState(true);

    const getCurrentDataset = useCallback(() => {
        const currentDataset = data?.databaseAccesses ?? [];
        const filterString = searchString.toLowerCase();

        return currentDataset.filter(dbInfo => {
            const searchStringFilter = dbInfo.caption.toLowerCase().includes(filterString) ||
                (dbInfo.configurationName ? dbInfo.configurationName.toLowerCase() : '').includes(filterString) ||
                dbInfo.v82Name.toLowerCase().includes(filterString);

            if (data && selectedFilter === 1) {
                return data.hasAccessArray.includes(dbInfo.databaseId) && searchStringFilter;
            }

            if (data && selectedFilter === 2) {
                return !data.hasAccessArray.includes(dbInfo.databaseId) && searchStringFilter;
            }

            return dbInfo && searchStringFilter;
        }).sort((a, b) => {
            if (sorting) {
                const { fieldName, sortKind } = sorting;
                const firstElement = a[fieldName as 'configurationName' || 'v82Name'];
                const secondElement = b[fieldName as 'configurationName' || 'v82Name'];

                if (firstElement && secondElement) {
                    if (sortKind) {
                        if (firstElement > secondElement) {
                            return 1;
                        }

                        if (firstElement < secondElement) {
                            return -1;
                        }

                        return 0;
                    }

                    if (firstElement > secondElement) {
                        return -1;
                    }

                    if (firstElement < secondElement) {
                        return 1;
                    }

                    return 0;
                }
            }

            return 0;
        });
    }, [data, searchString, selectedFilter, sorting]);

    const dataset = useMemo(getCurrentDataset, [getCurrentDataset]);
    const costSum = useMemo(() => lastCalculateCostResult.reduce((accum, costResultElem) => accum + costResultElem.accessCost, 0), [lastCalculateCostResult]);

    useEffect(() => {
        if (isRentActive) {
            getAccountProfilesList(dispatch, locale);
            getDatabaseAccessUsersList(dispatch, userId, true, isExternalUser);
            getGivenAccountProfilesList(dispatch, userId);
        }
    }, [userId, dispatch, isRentActive, isExternalUser, locale]);

    useEffect(() => {
        if (
            data?.databaseAccesses.find(database => database.state === EAccountDatabaseAccessState.processingDelete || database.state === EAccountDatabaseAccessState.processingGrant) ||
            givenProfileData?.permissions.find(givenProfile => givenProfile.state === 'Processing')
        ) {
            const interval = setTimeout(() => {
                getDatabaseAccessUsersList(dispatch, userId, false, isExternalUser);
                getGivenAccountProfilesList(dispatch, userId, '', false);
            }, TimerHelper.waitTimeRefreshDatabaseState);

            return () => {
                clearTimeout(interval);
            };
        }

        if (isRentActive) {
            dispatch(refresh());
        }
    }, [data, dispatch, givenProfileData?.permissions, isExternalUser, isRentActive, userId]);

    useEffect(() => {
        if (profileError) {
            show(EMessageType.error, profileError);
        }
    }, [profileError, show]);

    useEffect(() => {
        if (error) {
            show(EMessageType.error, error);
        }
    }, [error, show]);

    useEffect(() => {
        if (givenProfileError) {
            show(EMessageType.error, givenProfileError);
        }
    }, [givenProfileError, show]);

    const giveAccessToDatabase = (databaseId: string) => {
        setChangedDatabaseStatus(prevStatus => {
            const databaseIndex = prevStatus.remove.indexOf(databaseId);

            if (databaseIndex !== -1) {
                prevStatus.remove.splice(databaseIndex, 1);
            } else {
                prevStatus.give.push(databaseId);
            }

            return prevStatus;
        });
    };

    const removeAccessFromDatabase = (databaseId: string) => {
        setChangedDatabaseStatus(prevStatus => {
            const databaseIndex = prevStatus.give.indexOf(databaseId);

            if (databaseIndex !== -1) {
                prevStatus.give.splice(databaseIndex, 1);
            } else {
                prevStatus.remove.push(databaseId);
            }

            return prevStatus;
        });
    };

    const giveAccessToDatabaseOnDelimiters = (databaseId: string) => {
        const databaseIsExist = changedDatabaseStatusOnDelimiters.find(database => database.databaseId === databaseId);
        const alreadyHasAccess = data && data.hasAccessArray.includes(databaseId);

        setChangedDatabaseStatusOnDelimiters(prevState => {
            if (!databaseIsExist && !alreadyHasAccess) {
                prevState.push({
                    databaseId,
                    profiles: []
                });
            } else {
                prevState = prevState.flatMap(database => {
                    if (database.databaseId === databaseId) {
                        return [];
                    }

                    return database;
                });
            }

            return [...prevState];
        });
    };

    const removeAccessFromDatabaseOnDelimiters = (databaseId: string) => {
        if (givenProfileData) {
            const databaseIsExist = changedDatabaseStatusOnDelimiters.find(database => database.databaseId === databaseId);
            const alreadyHasAccess = data && data.hasAccessArray.includes(databaseId);

            setChangedDatabaseStatusOnDelimiters(prevState => {
                if (databaseIsExist) {
                    return prevState.flatMap(database => {
                        if (!alreadyHasAccess) {
                            return [];
                        }

                        if (database.databaseId === databaseId) {
                            database.profiles = [];
                        }

                        return database;
                    });
                }

                prevState.push({
                    databaseId,
                    profiles: []
                });

                return [...prevState];
            });
        }
    };

    const allowBackupHandler = (databaseId: string, isChecked: boolean) => {
        setChangedDatabaseStatusOnDelimiters(prevState => {
            if (prevState.find(prevElem => prevElem.databaseId === databaseId)) {
                return prevState.map(prevElem => {
                    if (prevElem.databaseId === databaseId) {
                        prevElem.allowBackup = !isChecked;
                    }

                    return prevElem;
                });
            }

            prevState.push({
                databaseId,
                allowBackup: !isChecked,
                profiles: givenProfileData?.permissions.find(profile => profile.id === databaseId)?.profiles ?? []
            });

            return [...prevState];
        });
    };

    const checkboxHandler = (value: boolean, { myDatabasesServiceTypeId, isFile, isDbOnDelimiters, databaseId }: TDatabaseAccesses, isNeedToCountImmediate = true) => {
        if (data) {
            dispatch(changeStatus(databaseId));

            setLastCalculateCostResponse(prevState => {
                if (value) {
                    prevState.serviceTypesIdsList.push(myDatabasesServiceTypeId);

                    if (!isFile && !isDbOnDelimiters) {
                        prevState.serviceTypesIdsList.push(data.accessToServerDatabaseServiceTypeId);
                    }

                    if (!isDbOnDelimiters) {
                        giveAccessToDatabase(databaseId);
                    } else {
                        giveAccessToDatabaseOnDelimiters(databaseId);
                    }
                } else {
                    if (!isFile && !isDbOnDelimiters) {
                        const index = prevState.serviceTypesIdsList.indexOf(data.accessToServerDatabaseServiceTypeId);

                        if (index !== -1) {
                            prevState.serviceTypesIdsList.splice(index, 1);
                        }
                    }

                    const index = prevState.serviceTypesIdsList.indexOf(myDatabasesServiceTypeId);

                    if (index !== -1) {
                        prevState.serviceTypesIdsList.splice(index, 1);
                    }

                    if (!isDbOnDelimiters) {
                        removeAccessFromDatabase(databaseId);
                    } else {
                        removeAccessFromDatabaseOnDelimiters(databaseId);
                    }
                }

                if (isNeedToCountImmediate) {
                    CalculateAccesses(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, prevState).then(result => {
                        setLastCalculateCostResult(result);
                    });
                }

                return prevState;
            });
        }
    };

    const putServicesApplyHandler = async () => {
        setIsInLoading(true);
        const isNeedToRemoveAccess = changedDatabaseStatus.remove.length > 0;
        const isNeedToGiveAccess = changedDatabaseStatus.give.length > 0;
        const isNeedToChangeAccessForDelimiters = changedDatabaseStatusOnDelimiters.length > 0;
        let isSuccess = true;
        let isNeedChangePermission = true;

        if (data && isNeedToChangeAccessForDelimiters) {
            isNeedChangePermission = (!data.hasAccessArray.includes(changedDatabaseStatusOnDelimiters[0].databaseId) && changedDatabaseStatusOnDelimiters[0].profiles.length > 0) ||
                data.hasAccessArray.includes(changedDatabaseStatusOnDelimiters[0].databaseId);
        }

        const body: TChangeDatabaseAccessRequest = {
            permissions: []
        };

        for (const status of changedDatabaseStatusOnDelimiters) {
            body.permissions.push({
                'application-id': status.databaseId,
                'user-id': userId,
                profiles: status.profiles,
                'allow-backup': status.allowBackup
            });
        }

        const isNeedRetry = (isNeedToGiveAccess || isNeedToRemoveAccess || isNeedToChangeAccessForDelimiters) && body.permissions.length;

        if (isNeedToChangeAccessForDelimiters) {
            if (isNeedChangePermission) {
                const { data: accessData } = await changeDatabaseAccessOnDelimiters(
                    body,
                    body.permissions.map(permission => {
                        const params = data?.databaseAccesses.find(access => access.databaseId === permission['application-id']);

                        return {
                            isExternal: isExternalUser,
                            v82Name: params?.v82Name ?? '',
                            databaseName: params?.caption ?? '',
                            databaseId: params?.databaseId ?? '',
                            userId,
                            userName,
                        };
                    })
                );

                if (accessData && accessData.message) {
                    show(EMessageType.error, accessData.message);
                    isSuccess = false;
                }

                if (accessData && accessData.results) {
                    accessData.results.forEach(statusResult => {
                        if (statusResult.status === 'failure') {
                            show(EMessageType.warning, statusResult.error ?? `Не удалось отредактировать права в базу ${ statusResult['application-id'] }`);
                        }
                    });
                }
            }
        }

        if (isNeedToGiveAccess) {
            const { data: accessData } = await putDatabaseAccess({
                usersId: userId,
                giveAccess: true,
                databaseIds: changedDatabaseStatus.give
            });

            if (accessData) {
                accessData.forEach(({ success, message, databasesName }) => {
                    if (!success) {
                        show(EMessageType.warning, `${ message } ${ databasesName ? `(${ databasesName })` : '' }`);
                    }
                });
            }
        }

        if (isNeedToRemoveAccess) {
            const { data: accessData } = await putDatabaseAccess({
                usersId: userId,
                giveAccess: false,
                databaseIds: changedDatabaseStatus.remove
            });

            if (accessData) {
                accessData.forEach(({ success, message, databasesName }) => {
                    if (!success) {
                        show(EMessageType.warning, `${ message } ${ databasesName ? `(${ databasesName })` : '' }`);
                    }
                });
            }
        }

        setIsInLoading(false);

        if (isNeedRetry && isNeedChangePermission) {
            setCurrentPage(1);
            getDatabaseAccessUsersList(dispatch, userId, true, isExternalUser);
            getGivenAccountProfilesList(dispatch, userId);
        }

        setLastCalculateCostResult([]);
        setChangedDatabaseStatus(prevStatus => {
            prevStatus.give = [];
            prevStatus.remove = [];

            return prevStatus;
        });
        setLastCalculateCostResponse({
            serviceTypesIdsList: [],
            accountUserDataModelsList: [{
                UserId: userId,
                HasAccess: true
            }]
        });
        setChangedDatabaseStatusOnDelimiters([]);

        if (isSuccess && isNeedRetry) {
            show(EMessageType.success, 'Изменение доступов будет выполнено в течение 1 минуты. Пожалуйста, подождите.');
        }

        if (handleSkip) {
            handleSkip();
        }
    };

    const valueProfilesInfoHandler = (_: string, items: DropDownListWithCheckboxItem[], { databaseId, configurationCode }: TDatabaseAccesses) => {
        if (profileData) {
            const configurationProfiles = profileData.find(({ code }) => configurationCode === code);

            if (configurationProfiles) {
                const { adminRole, userRole } = configurationProfilesToRole(configurationProfiles.profiles, items);

                setChangedDatabaseStatusOnDelimiters(prevState => {
                    const currentDatabase = prevState.find(prevElem => prevElem.databaseId === databaseId);

                    const prevProfile = (currentDatabase ?? givenProfileData?.permissions.find(el => el.id === databaseId))?.profiles;
                    let foundedProfiles: Omit<TAccountProfile, 'databases'>[] = [];

                    if (items.length > 1 && isFirstShow && adminRole.length && userRole.length) {
                        show(EMessageType.info, 'Установка профиля доступа "Администратор" одновременно с другими профилями через личный кабинет не поддерживается');
                        setIsFirstShow(false);
                    }

                    if (prevProfile && prevProfile.some(profile => profile.admin)) {
                        foundedProfiles = userRole.length ? userRole : adminRole;
                    } else {
                        foundedProfiles = adminRole.length ? adminRole : userRole;
                    }

                    if (currentDatabase) {
                        prevState = prevState.map(database => {
                            if (database.databaseId === databaseId) {
                                database.profiles = foundedProfiles;

                                if (!foundedProfiles.find(profile => profile.admin)) {
                                    database.allowBackup = false;
                                }
                            }

                            return database;
                        });
                    } else {
                        prevState.push({
                            databaseId,
                            profiles: foundedProfiles,
                            allowBackup: !foundedProfiles.find(profile => profile.admin) ? false : givenProfileData?.permissions.find(permission => permission.id === databaseId)?.allowBackup
                        });
                    }

                    return [...prevState];
                });
            }
        }
    };

    const closeDialogHandler = () => {
        setPaymentDialogParams({ isOpen: false, isPromisePayment: false, sum: 0 });
    };

    const openDialogHandler = (isPromisePayment: boolean, sum: number) => {
        setPaymentDialogParams({ isOpen: true, isPromisePayment, sum });
    };

    const openCostDialogHandler = (databaseName: string) => {
        setShowCostDialog({ isOpen: true, databaseName });
    };

    const closeCostDialogHandler = () => {
        setShowCostDialog({ isOpen: false, databaseName: '' });
    };

    const costApplyHandler = async (usePromisePayment?: boolean) => {
        const isNeedToRemoveAccess = changedDatabaseStatus.remove.length > 0;
        const isNeedToGiveAccess = changedDatabaseStatus.give.length > 0;
        const isNeedToChangeAccessForDelimiters = changedDatabaseStatusOnDelimiters.length > 0;
        let isNeedChangePermission = true;

        if (data && isNeedToChangeAccessForDelimiters) {
            isNeedChangePermission = (!data.hasAccessArray.includes(changedDatabaseStatusOnDelimiters[0].databaseId) && changedDatabaseStatusOnDelimiters[0].profiles.length > 0) ||
                data.hasAccessArray.includes(changedDatabaseStatusOnDelimiters[0].databaseId);
        }

        if (isNeedToRemoveAccess || isNeedToGiveAccess || (isNeedToChangeAccessForDelimiters && isNeedChangePermission)) {
            closeCostDialogHandler();
            setIsInLoading(true);
            const uniqueServiceTypeIdsList = [...new Set(lastCalculateCostResponse.serviceTypesIdsList)];
            const { data: responseData, message } = await putServicesApply({
                accountId: contextAccountId(),
                changedUsers: uniqueServiceTypeIdsList.map(uniqueId => ({
                    billingServiceTypeId: uniqueId ?? '',
                    status: true,
                    subject: userId,
                    sponsorship: {
                        i: false,
                        me: false,
                        label: ''
                    }
                })),
                billingServiceId: data?.myDatabaseBillingServiceId ?? '',
                usePromisePayment
            });
            setIsInLoading(false);

            if (message) {
                show(EMessageType.error, message);
            } else if (responseData && responseData.isComplete) {
                void putServicesApplyHandler();
            } else if (responseData && !responseData.isComplete) {
                openDialogHandler(responseData.canUsePromisePayment, responseData.notEnoughMoney ?? 0);
            }
        }

        if (handleSkip) {
            handleSkip();
        }
    };

    const buttonFilterHandler = (buttonIndex: number) => {
        setSelectedFilter(buttonIndex);
        setCurrentPage(1);
    };

    const currentPageHandler = (_: React.ChangeEvent<unknown>, page: number) => {
        setCurrentPage(page);
    };

    const searchHandler = (ev: React.ChangeEvent<HTMLInputElement>) => {
        setSearchString(ev.target.value);
        setCurrentPage(1);
    };

    const redirectRentHandler = () => {
        history.push(AppRoutes.services.rentReference);
    };

    const getProfile = (rowData: TDatabaseAccesses) => {
        if (rowData.isDbOnDelimiters && givenProfileData && profileData) {
            const isInProcessing = !!givenProfileData.permissions.find(givenProfile => givenProfile.id === rowData.databaseId && givenProfile.state === 'Processing');
            const isInError = !!givenProfileData.permissions.find(givenProfile => givenProfile.id === rowData.databaseId && givenProfile.state === 'Error');
            const isNeedToWarn = rowData.hasAccess && rowData.state !== EAccountDatabaseAccessState.processingDelete && rowData.state !== EAccountDatabaseAccessState.processingGrant;
            const changeDatabaseStatusInfo = changedDatabaseStatusOnDelimiters.find(database => database.databaseId === rowData.databaseId);
            const givenProfileInfo = givenProfileData.permissions.find(givenProfile => givenProfile.id === rowData.databaseId);
            const isSwitchChecked = !!(changeDatabaseStatusInfo?.allowBackup ?? givenProfileInfo?.allowBackup);
            const currentProfiles = profileData.flatMap(profile => {
                if (profile.code === rowData.configurationCode) {
                    return profile.profiles.flatMap(pr => {
                        const currentProfile = {
                            text: pr.name,
                            value: pr.id,
                            checked: changeDatabaseStatusInfo
                                ? !!changeDatabaseStatusInfo.profiles
                                    .find(profileInfo => profileInfo.id === EMPTY_UUID ? profileInfo.name === pr.name : profileInfo.id === pr.id)
                                : !!givenProfileData.permissions
                                    .find(givenProfile => givenProfile.id === rowData.databaseId)?.profiles
                                    .find(profileInfo => profileInfo.id === EMPTY_UUID ? profileInfo.name === pr.name : profileInfo.id === pr.id)
                        };

                        if (!pr.predefined) {
                            if (pr.databases.includes(rowData.databaseId)) {
                                return currentProfile;
                            }

                            return [];
                        }

                        return currentProfile;
                    });
                }

                return [];
            });

            return (
                <Box display="flex" flexDirection="column" gap="5px" marginY={ isInProcessing || isNeedToWarn || isInError ? '5px' : '0px' }>
                    <DropDownListWithCheckboxes
                        noUpdateValueChangeHandler={ true }
                        deepCompare={ true }
                        hiddenAdditionalElement={ true }
                        renderValue={ selected => (
                            <Box sx={ { display: 'flex', flexWrap: 'wrap', gap: 0.5 } }>
                                {
                                    selected.map(({ value, text }) => {
                                        const stateProfile = givenProfileInfo?.profiles.find(profile => profile.id === value);

                                        return !stateProfile?.error
                                            ? <Chip key={ value } label={ text } />
                                            : (
                                                <Tooltip key={ value } placement="top" title={ stateProfile.description }>
                                                    <Chip label={ text } sx={ { color: COLORS.error } } />
                                                </Tooltip>
                                            );
                                    })
                                }
                            </Box>
                        ) }
                        hiddenSelectAll={ true }
                        disabled={ isInProcessing }
                        handleOpenSelect={ () => {
                            if (currentProfiles.every(profile => !profile.checked)) {
                                checkboxHandler(true, rowData);
                            }
                        } }
                        handleCloseSelect={ costSum === 0 ? () => costApplyHandler(false) : () => openCostDialogHandler(rowData.v82Name) }
                        additionalElement={
                            <Tooltip title="Разрешить выгрузку информационной базы" placement="top">
                                <CustomizedSwitches
                                    onChange={ () => allowBackupHandler(rowData.databaseId, isSwitchChecked) }
                                    checked={ isSwitchChecked }
                                    disabled={
                                        isInError ||
                                        !(changeDatabaseStatusInfo ? changeDatabaseStatusInfo?.profiles.find(profile => profile.admin) : givenProfileInfo?.profiles.find(profile => profile.admin)) ||
                                        isInProcessing
                                    }
                                />
                            </Tooltip>
                        }
                        items={ currentProfiles }
                        label=""
                        formName="profiles"
                        onValueChange={ (formName, items) => valueProfilesInfoHandler(formName, items, rowData) }
                    />
                    { isInProcessing &&
                        <Box display="flex" gap="5px">
                            <ClockLoading />
                            <TextOut fontSize={ 10 } style={ { color: COLORS.warning } }>В процессе редактирования</TextOut>
                        </Box>
                    }
                    { isInError &&
                        <TextOut fontSize={ 10 } style={ { color: COLORS.error } }>Ошибка редактирования</TextOut>
                    }
                </Box>
            );
        }

        return (
            <Box display="flex" flexDirection="column" gap="5px">
                <DropDownListWithCheckboxes
                    noUpdateValueChangeHandler={ true }
                    deepCompare={ true }
                    hiddenAdditionalElement={ true }
                    renderValueIsChip={ true }
                    hiddenSearchLine={ true }
                    handleCloseSelect={ costSum === 0 ? () => costApplyHandler(false) : () => openCostDialogHandler(rowData.v82Name) }
                    items={ [
                        {
                            text: 'Доступ к базе',
                            value: rowData.databaseId,
                            checked: rowData.hasAccess
                        }
                    ] }
                    label=""
                    formName="profiles"
                    onValueChange={ () => checkboxHandler(!rowData.hasAccess, rowData, !rowData.hasAccess) }
                />
            </Box>
        );
    };

    const closeGroupAccessChangeDialog = () => {
        setShowGroupAccessChangeDialog(false);
    };

    if (!isRentActive) {
        return (
            <Box className={ styles.error }>
                <TextOut inheritColor={ true }>
                    Для настройки доступа подключите пользователя к сервису <Link onClick={ redirectRentHandler }>Аренда 1С</Link>
                </TextOut>
            </Box>
        );
    }

    return (
        <>
            { (isLoading || isProfileLoading || isGivenProfileLoading || isInLoading) && <LoadingBounce /> }
            <PaymentDialog
                apply={ costApplyHandler }
                isOpen={ paymentDialogParams.isOpen }
                onCancelClick={ closeDialogHandler }
                isPromisePaymentAvailable={ paymentDialogParams.isPromisePayment }
                sum={ paymentDialogParams.sum }
                currency={ data?.currency }
            />
            <Dialog
                isOpen={ showCostDialog.isOpen }
                onCancelClick={ closeCostDialogHandler }
                buttons={ [
                    {
                        content: 'Отмена',
                        kind: 'default',
                        variant: 'contained',
                        onClick: closeCostDialogHandler
                    },
                    {
                        content: `Купить (${ costSum } ${ data?.currency })`,
                        kind: 'success',
                        variant: 'contained',
                        onClick: () => costApplyHandler()
                    }
                ] }
                dialogWidth="sm"
                dialogVerticalAlign="center"
            >
                Для предоставления пользователю { userName } доступа в базу &quot;{ showCostDialog.databaseName }&quot; необходимо оплатить { costSum } { data?.currency }
            </Dialog>
            <GroupAccessChange
                isOpen={ showGroupAccessChangeDialog }
                onCancelClick={ closeGroupAccessChangeDialog }
                userId={ userId }
                userName={ userName }
                isExternalUser={ isExternalUser }
            />
            <div className={ styles.container }>
                <div className={ styles.header }>
                    <FormAndLabel label="Поиск">
                        <OutlinedInput fullWidth={ true } placeholder="Поиск базы по названию или конфигурации" onChange={ searchHandler } value={ searchString } />
                    </FormAndLabel>
                    <div className={ styles.quickSort }>
                        {
                            !hiddenFilter && <ButtonGroup items={ buttonGroupElements } onClick={ buttonFilterHandler } selectedButtonIndex={ selectedFilter } label="Быстрые отборы" fullWidth={ true } />
                        }
                        <SuccessButton style={ { minWidth: 'auto' } } onClick={ () => setShowGroupAccessChangeDialog(true) }>Групповое изменение профилей</SuccessButton>
                    </div>
                </div>
                <CommonTableWithFilter
                    uniqueContextProviderStateId="DatabaseAccessSettingsList"
                    tableProps={ {
                        emptyText: searchString ? `По запросу "${ searchString }" не удалось найти совпадений среди ваших информационных баз` : 'Информационные базы не найдены',
                        dataset: dataset.slice(pageSize * (currentPage - 1), pageSize * currentPage),
                        keyFieldName: 'databaseId',
                        fieldsView: {
                            templateImageName: {
                                caption: '',
                                noNeedForMobile: true,
                                fieldWidth: '5%',
                                format: (value: string) => <HomeSvgSelector width={ 36 } height={ 36 } id={ value } />
                            },
                            caption: {
                                caption: 'Название',
                                fieldWidth: '15%',
                                format: (value: string, { v82Name, state }) =>
                                    <Box display="flex" flexDirection="column" gap="5px">
                                        <TextOut>{ `${ value } (${ v82Name })` }</TextOut>
                                        { state === EAccountDatabaseAccessState.processingDelete &&
                                            <Box display="flex" gap="5px">
                                                <ClockLoading />
                                                <TextOut fontSize={ 10 } style={ { color: COLORS.error } }>В процессе удаления</TextOut>
                                            </Box>
                                        }
                                        { state === EAccountDatabaseAccessState.processingGrant &&
                                            <Box display="flex" gap="5px">
                                                <ClockLoading />
                                                <TextOut fontSize={ 10 } style={ { color: COLORS.main } }>В процессе выдачи</TextOut>
                                            </Box>
                                        }
                                    </Box>
                            },
                            myDatabasesServiceTypeId: {
                                caption: 'Профиль доступа',
                                fieldWidth: '45%',
                                format: (_: string, rowData) => getProfile(rowData)
                            },
                            configurationName: {
                                caption: 'Конфигурация',
                                fieldWidth: '20%',
                                isSortable: true
                            },
                            v82Name: {
                                caption: 'Тип',
                                format: (_: string, rowData) => {
                                    if (rowData.isFile) {
                                        return 'Файловая';
                                    }

                                    if (rowData.isDbOnDelimiters) {
                                        return 'Разделители';
                                    }

                                    return 'Серверная';
                                },
                                isSortable: true
                            }
                        }
                    } }
                    onDataSelect={ ({ sortingData }) => setSorting(sortingData) }
                />
                <div className={ styles.footer }>
                    <Pagination page={ currentPage } count={ Math.ceil(dataset.length / pageSize) } onChange={ currentPageHandler } />
                    { skipButton }
                </div>
            </div>
        </>
    );
});