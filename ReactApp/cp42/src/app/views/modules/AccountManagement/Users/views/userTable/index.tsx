import { Chip, Link, Box } from '@mui/material';
import { TourProps } from '@reactour/tour';
import { REDUX_API } from 'app/api/useReduxApi';
import { SortingKind } from 'app/common/enums';
import { EMessageType, useAppDispatch, useAppSelector, useFloatMessages } from 'app/hooks';
import { useQuery } from 'app/hooks/useQuery';
import { Get1CStatusForUserThunkParams } from 'app/modules/AccountManagement/UserManagement/store/reducers/GetUserWithRent1CReducer/params';
import { Reset1CStatusForUserThunkParams } from 'app/modules/AccountManagement/UserManagement/store/reducers/ResetUserWithRent1CReducer/params';
import { GetUserWithRent1CThunk, ResetUserWithRent1CThunk } from 'app/modules/AccountManagement/UserManagement/store/thunks';
import { usersFilter } from 'app/modules/users';
import { userCard } from 'app/modules/users/reducers/userCard';
import { AppReduxStoreState } from 'app/redux/types';
import { COLORS, DateUtility } from 'app/utils';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { TextOut } from 'app/views/components/TextOut';
import { ERefreshId, ETourId } from 'app/views/Layout/ProjectTour/enums';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { AccountUserRolesEnum } from 'app/views/modules/AccountManagement/Users/views/types/AccountUserRolesEnum';
import { UserCard } from 'app/views/modules/AccountManagement/Users/views/userCard';
import { SelectDataCommonDataModel, SortingDataModel } from 'app/web/common';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { RequestKind } from 'core/requestSender/enums';
import React, { useCallback, useEffect, useState } from 'react';
import { connect } from 'react-redux';

type MapDispatchToProps = {
    dispatchRent1C: (args: Get1CStatusForUserThunkParams) => void;
    dispatchResetRent1C: (args: Reset1CStatusForUserThunkParams) => void;
};

const { getUsers } = REDUX_API.USERS_REDUX;
const { isAccountLastAdmin } = InterlayerApiProxy.getBalanceManagementApiProxy();

const { onChangeFilter, onChangePage } = usersFilter.actions;
const { changeStateUserCard, toInitialState } = userCard.actions;

const UserTableFunction = ({ dispatchRent1C, dispatchResetRent1C, ...tourProps }: TourProps & MapDispatchToProps) => {
    const { show } = useFloatMessages();
    const dispatch = useAppDispatch();
    const query = useQuery();

    const { usersSlice: { data, error, isLoading }, usersFilter: filter, userCard: userCardState } = useAppSelector(state => state.Users);

    const [isLastAdmin, setIsLastAdmin] = useState(false);

    useEffect(() => {
        if (error) {
            show(EMessageType.error, error);
        }
    }, [show, error]);

    const getSortQuery = ({ fieldName, sortKind }: SortingDataModel) => {
        const sort = sortKind ? 'desc' : 'asc';

        switch (fieldName) {
            case 'fullName':
                return `lastName.${ sort },firstName.${ sort }`;
            default:
                return `${ fieldName }.${ sort }`;
        }
    };

    const openUserCard = useCallback(async (id: string) => {
        dispatchRent1C({ userId: id, force: true });
        setIsLastAdmin(id ? await isAccountLastAdmin(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, { accountId: id }) : false);

        dispatch(changeStateUserCard({ id, showUserCard: true }));

        if (tourProps.isOpen) {
            setTimeout(() => {
                tourProps.setCurrentStep(prev => prev + 1);
            }, 500);
        }
    }, [dispatch, dispatchRent1C, tourProps]);

    const closeUserCard = (isEdited?: boolean) => {
        dispatchResetRent1C({ force: true });
        dispatch(toInitialState());

        if (isEdited) {
            const { searchLine, orderBy, pageNumber, group } = filter;
            void getUsers(dispatch, { filter: { searchLine }, orderBy, pageNumber, group });
        }
    };

    const onDataSelectHandler = ({ pageNumber, sortingData }: SelectDataCommonDataModel<{}>) => {
        const { orderBy, searchLine, group } = filter;

        if (data?.metadata?.pageNumber !== pageNumber) {
            dispatch(onChangePage(pageNumber));
        }

        if (sortingData && orderBy !== getSortQuery(sortingData)) {
            dispatch(onChangeFilter({ formName: 'orderBy', value: getSortQuery(sortingData) }));
        }

        if (sortingData) {
            void getUsers(dispatch, { filter: { searchLine }, orderBy: getSortQuery(sortingData), pageNumber, group });
        }
    };

    const getPagination = () => {
        if (data?.metadata) {
            const { pageNumber, pageCount, pageSize, totalItemCount } = data.metadata;

            return {
                currentPage: pageNumber,
                totalPages: pageCount,
                pageSize,
                recordsCount: totalItemCount,
            };
        }
    };

    useEffect(() => {
        const id = query.get('id');

        if (id && !userCardState.showUserCard) {
            void openUserCard(id);
            query.delete('id');
        }
    }, [openUserCard, query, userCardState.showUserCard]);

    return (
        <>
            { isLoading && <LoadingBounce /> }
            <CommonTableWithFilter
                refreshId={ ERefreshId.usersPage }
                tourId={ ETourId.usersPageTable }
                uniqueContextProviderStateId="UsersListContextProviderStateId"
                isResponsiveTable={ true }
                isBorderStyled={ true }
                tableProps={ {
                    emptyText: filter.searchLine ? `По запросу "${ filter.searchLine }" не удалось найти совпадений среди пользователей` : 'Пользователи не найдены',
                    dataset: data?.records ?? [],
                    keyFieldName: 'id',
                    sorting: {
                        sortKind: SortingKind.Asc,
                        sortFieldName: 'login'
                    },
                    fieldsView: {
                        login: {
                            caption: 'Логин',
                            fieldWidth: '20%',
                            isSortable: true,
                            format: (value: string, item) => {
                                return (
                                    <Box alignItems="center" display="flex" gap="3px">
                                        <Link onClick={ () => openUserCard(item.id) }>{ value }</Link>
                                        { item.accountRoles.includes(AccountUserRolesEnum.AccountAdmin) && (
                                            <TextOut fontSize={ 10 } style={ { color: COLORS.error } }>админ</TextOut>
                                        ) }
                                    </Box>
                                );
                            }
                        },
                        fullName: {
                            caption: 'Пользователь',
                            fieldWidth: '20%',
                            isSortable: true,
                            format: (_, item) => <TextOut>{ item.lastName } { item.firstName } { item.middleName }</TextOut>
                        },
                        phoneNumber: {
                            caption: 'Телефон',
                            fieldWidth: '15%',
                            isSortable: true
                        },
                        email: {
                            caption: 'E-mail',
                            fieldWidth: '20%',
                            isSortable: true
                        },
                        activated: {
                            caption: 'Статус',
                            fieldWidth: '15%',
                            isSortable: true,
                            format: (value: boolean) => <Chip color={ value ? 'primary' : 'error' } label={ value ? 'Активен' : 'Заблокирован' } />
                        },
                        creationDate: {
                            caption: 'Дата создания',
                            fieldWidth: '10%',
                            format: (value: Date) => value ? DateUtility.dateToDayMonthYear(new Date(value?.toString())) : '---',
                            isSortable: true
                        }
                    },
                    pagination: getPagination()
                } }
                onDataSelect={ onDataSelectHandler }
            />
            <UserCard
                id={ userCardState.id }
                isOpen={ userCardState.showUserCard }
                onCancelClick={ closeUserCard }
                isLastAdmin={ isLastAdmin }
            />
        </>
    );
};

export const UserTable = connect<object, MapDispatchToProps, object, AppReduxStoreState>(
    null,
    {
        dispatchRent1C: GetUserWithRent1CThunk.invoke,
        dispatchResetRent1C: ResetUserWithRent1CThunk.invoke
    }
)(UserTableFunction);