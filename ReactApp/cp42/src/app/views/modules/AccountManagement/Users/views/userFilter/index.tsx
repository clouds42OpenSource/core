import { REDUX_API } from 'app/api/useReduxApi';
import { useAppDispatch, useAppSelector } from 'app/hooks';
import { usersFilter } from 'app/modules/users';
import React, { memo, useEffect, useState } from 'react';
import { Box, TextField } from '@mui/material';
import Checkbox from '@mui/material/Checkbox';
import { OutlinedButton } from 'app/views/components/controls/Button';
import { TextOut } from 'app/views/components/TextOut';
import style from 'app/views/modules/AccountManagement/Users/style.module.css';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';

const { getUsers } = REDUX_API.USERS_REDUX;

const { onChangeFilter, onChangePage, resetFilters } = usersFilter.actions;

const ENTER_KEY_CODE = 13;
const ENTER_KEY_NAME = 'enter';

export const UserFilter = memo(() => {
    const dispatch = useAppDispatch();

    const { group, orderBy } = useAppSelector(state => state.Users.usersFilter);
    const [search, setSearch] = useState('');

    useEffect(() => {
        return () => {
            dispatch(resetFilters());
        };
    }, [dispatch]);

    const changeSearchLine = () => {
        dispatch(onChangeFilter({ formName: 'searchLine', value: search }));
    };

    const searchUser = () => {
        changeSearchLine();
        void getUsers(dispatch, {
            filter: {
                searchLine: search,
            },
            orderBy,
            pageNumber: 1,
            group
        });
    };

    const onKeyDownHandler = (ev: React.KeyboardEvent<HTMLInputElement>) => {
        if (ev.keyCode === ENTER_KEY_CODE || ev.key.toLowerCase() === ENTER_KEY_NAME) {
            searchUser();
        }
    };

    const onGroupChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const groupValue = e.target.checked ? 'AccountAdmin' : undefined;

        dispatch(onChangeFilter({ formName: 'group', value: groupValue }));
        dispatch(onChangePage(1));

        void getUsers(dispatch, { pageNumber: 1, group: groupValue, orderBy, filter: { searchLine: search } });
    };

    const onSearchLineChange = async (e: React.ChangeEvent<HTMLInputElement>) => {
        const { value } = e.target;

        setSearch(value);
    };

    return (
        <Box display="flex" gap="10px" alignItems="flex-end" flexWrap="wrap">
            <Box alignItems="center" display="flex" width="170px">
                <Checkbox
                    checked={ !!group }
                    onChange={ onGroupChange }
                    size="small"
                />
                <TextOut fontWeight={ 600 }>Показывать только администраторов</TextOut>
            </Box>
            <FormAndLabel label="Поиск">
                <TextField
                    className={ style.control }
                    value={ search }
                    onChange={ onSearchLineChange }
                    onBlur={ changeSearchLine }
                    placeholder="телефон, email, имя, логин"
                    fullWidth={ true }
                    onKeyDown={ onKeyDownHandler }
                />
            </FormAndLabel>
            <OutlinedButton className={ style.control } kind="success" onClick={ searchUser }>Поиск</OutlinedButton>
        </Box>
    );
});