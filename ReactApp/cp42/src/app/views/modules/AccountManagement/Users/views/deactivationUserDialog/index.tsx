import { FETCH_API } from 'app/api/useFetchApi';
import { REDUX_API } from 'app/api/useReduxApi';
import { EMessageType, useAppDispatch, useAppSelector, useFloatMessages } from 'app/hooks';
import { Dialog } from 'app/views/components/controls/Dialog';
import { TextOut } from 'app/views/components/TextOut';
import React from 'react';

type TDeactivationUserDialogProps = {
    isOpen: boolean;
    onCancelClick: () => void;
    userId?: string
};

const { postActivateUser } = FETCH_API.USERS;
const { getUsers } = REDUX_API.USERS_REDUX;

export const DeactivationUserDialog = ({ isOpen, onCancelClick, userId }: TDeactivationUserDialogProps) => {
    const dispatch = useAppDispatch();
    const { show } = useFloatMessages();

    const filter = useAppSelector(state => state.Users.usersFilter);

    const deactivationUser = async () => {
        const { success, message } = await postActivateUser({ accountUserID: userId, isActivate: false });

        if (!success) {
            show(EMessageType.error, message);
        } else {
            void getUsers(dispatch, {
                filter: {
                    searchLine: filter.searchLine,
                },
                orderBy: filter.orderBy,
                pageNumber: filter.pageNumber,
                group: filter.group
            });
        }

        onCancelClick();
    };

    return (
        <Dialog
            title="Отключение пользователя"
            dialogVerticalAlign="center"
            isOpen={ isOpen }
            isTitleSmall={ true }
            dialogWidth="sm"
            onCancelClick={ onCancelClick }
            buttons={ [
                {
                    variant: 'contained',
                    kind: 'default',
                    content: 'Нет',
                    onClick: () => onCancelClick()
                },
                {
                    variant: 'contained',
                    kind: 'error',
                    content: 'Да',
                    onClick: () => deactivationUser()
                }
            ] }
        >
            <TextOut>Все доступы в базы будут отключены!</TextOut>
        </Dialog>
    );
};