import { Verified as VerifiedIcon, Visibility, VisibilityOff } from '@mui/icons-material';
import { Box, Divider, IconButton, InputAdornment, MenuItem, OutlinedInput, Select, TextField, Tooltip } from '@mui/material';
import { userId } from 'app/api';
import { TPostNewUser, TPutEditUser } from 'app/api/endpoints/users/request';
import { FETCH_API } from 'app/api/useFetchApi';
import { REDUX_API } from 'app/api/useReduxApi';
import { AccountUserGroup } from 'app/common/enums';
import { EMessageType, useAppDispatch, useAppSelector, useFloatMessages } from 'app/hooks';
import { ReceiveMyCompanyThunkParams } from 'app/modules/myCompany/store/reducers/receiveMyCompanyData/params';
import { ReceiveMyCompanyThunk } from 'app/modules/myCompany/store/thunks';
import { COLORS } from 'app/utils';
import { ContainedButton, SuccessButton } from 'app/views/components/controls/Button';
import { DropDownListWithCheckboxes } from 'app/views/components/controls/forms';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { TextOut } from 'app/views/components/TextOut';
import { ETourId } from 'app/views/Layout/ProjectTour/enums';
import { getUserForm } from 'app/views/modules/AccountManagement/Users/constants';
import { TCommonUserInfoViewProps } from 'app/views/modules/AccountManagement/Users/tabs/types';
import { DeactivationUserDialog } from 'app/views/modules/AccountManagement/Users/views/deactivationUserDialog';
import { DeleteUserDialog } from 'app/views/modules/AccountManagement/Users/views/deleteUserDialog';
import { UserRolesEnumStatusExtensions } from 'app/views/modules/AccountManagement/Users/views/types/AccountUserRolesEnum';
import { stylePassword } from 'app/views/modules/ResetPassword/const/stylePassword';
import { AccountPermissionsEnum } from 'app/web/common/enums/AccountPermissionsEnum';
import { EDepartmentAction } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/receiveMyCompany';
import { useFormik } from 'formik';
import debounce from 'lodash.debounce';
import React, { memo, useEffect, useMemo, useRef, useState } from 'react';
import { connect } from 'react-redux';

const { postCreateNewUser, getUsers } = REDUX_API.USERS_REDUX;
const { putEditUser, postActivateUser } = FETCH_API.USERS;

type TShowPassword = {
    password: boolean;
    confirmPassword: boolean;
};

type TDispatchProps = {
    dispatchReceiveMyCompanyThunk: (args: ReceiveMyCompanyThunkParams) => void;
};

const UserInfo = ({ user, isLastAdmin, setShowRentDialog, setIsEdited, onCancelClick, dispatchReceiveMyCompanyThunk }: TCommonUserInfoViewProps & TDispatchProps) => {
    const dispatch = useAppDispatch();
    const { show } = useFloatMessages();

    const {
        Global: {
            getUserPermissionsReducer: { permissions },
            getCurrentSessionSettingsReducer: { settings: { currentContextInfo: { currentUserGroups } } }
        },
        Users: {
            usersFilter
        },
        MyCompanyState: {
            myCompany, hasSuccessFor: { hasMyCompanyReceived }
        },
    } = useAppSelector(state => state);

    const { userFormSchema, initialValueUserForm } = useMemo(() => getUserForm(user), [user]);
    const valueRef = useRef(initialValueUserForm);

    const [isLoading, setIsLoading] = useState(false);
    const [showDeleteUserModal, setShowDeleteUserModal] = useState(false);
    const [showDeactivationUserDialog, setShowDeactivationUserDialog] = useState(false);
    const [showPassword, setShowPassword] = useState<TShowPassword>({ password: false, confirmPassword: false });

    useEffect(() => {
        if (!hasMyCompanyReceived) {
            dispatchReceiveMyCompanyThunk({ force: true });
        }
    }, [dispatch, dispatchReceiveMyCompanyThunk, hasMyCompanyReceived]);

    const userForm = useFormik<TPutEditUser>({
        initialValues: initialValueUserForm as TPutEditUser,
        validationSchema: userFormSchema,
        onSubmit: async () => {
            setIsLoading(true);
            setIsEdited(true);
            await createOrSaveUser();
            onCancelClick(true);
        },
        validateOnChange: false,
        validateOnBlur: false,
        enableReinitialize: true,
    });

    const debounceValidate = useMemo(() => debounce(userForm.validateField, 750), [userForm.validateField]);

    useEffect(() => {
        Object.entries(userForm.values).forEach(([key, value]) => {
            const field = key as keyof TPutEditUser;

            if (valueRef.current[field] !== value) {
                if ((field === 'phoneNumber' || field === 'email') && value === '') {
                    delete userForm.errors[field];
                } else {
                    void debounceValidate(field);
                }
            }
        });

        valueRef.current = userForm.values;
    }, [debounceValidate, user, userForm.errors, userForm.values]);

    const isCurrentUser = useMemo(() => user && userId() === user.id, [user]);
    const canEditRoles = useMemo(() => permissions.includes(AccountPermissionsEnum.EditUserRoles), [permissions]);
    const canEditUser = useMemo(() => permissions.includes(AccountPermissionsEnum.EditUserFields), [permissions]);
    const isAdminInfoShow = useMemo(() =>
        currentUserGroups.includes(AccountUserGroup.CloudAdmin) ||
        currentUserGroups.includes(AccountUserGroup.Hotline) ||
        currentUserGroups.includes(AccountUserGroup.AccountSaleManager),
    [currentUserGroups]);

    const openDeleteUserDialog = () => {
        setShowDeleteUserModal(true);
    };

    const closeDeleteUserDialog = () => {
        setShowDeleteUserModal(false);
    };

    const closeDeactivationUserDialog = () => {
        setShowDeactivationUserDialog(false);
    };

    const changeShowPassword = (field: keyof TShowPassword) => {
        setShowPassword(prev => ({
            ...prev,
            [field]: !prev[field]
        }));
    };

    const onEditUser = async (args: TPutEditUser) => {
        const { success, message } = await putEditUser(args);

        if (!success) {
            show(EMessageType.error, message);
        } else {
            show(EMessageType.success, 'Пользователь успешно отредактирован');
        }
    };

    const onAddNewUser = async (args: TPostNewUser) => {
        const { success, message } = await postCreateNewUser(dispatch, args);

        if (success) {
            setShowRentDialog(true);
        } else {
            show(EMessageType.error, message);
        }
    };

    const createOrSaveUser = async () => {
        const formValues = userForm.values;

        if (formValues.departmentRequest && formValues.departmentRequest.actionType === undefined) {
            formValues.departmentRequest = null;
        }

        if (user) {
            await onEditUser(formValues);
            if (formValues.email !== '' && formValues.email !== user?.email) {
                show(EMessageType.info, 'Смена почты произойдет после верификации новой почты. Проверьте письмо на почте.');
            }
        } else {
            await onAddNewUser(formValues);
            if (formValues.email !== '') {
                show(EMessageType.info, 'Подтверждение почты произойдет после верификации. Проверьте письмо на почте.');
            }
        }

        setIsLoading(false);
    };

    const activateUser = async () => {
        const { success, message } = await postActivateUser({ accountUserID: user?.id, isActivate: true });

        if (!success) {
            show(EMessageType.error, message);
        } else {
            void getUsers(dispatch, {
                filter: {
                    searchLine: usersFilter.searchLine,
                },
                orderBy: usersFilter.orderBy,
                pageNumber: usersFilter.pageNumber,
                group: usersFilter.group
            });
        }
    };

    const onFocusHandler = (field: keyof TPutEditUser) => () => {
        if (!userForm.touched[field]) {
            userForm.setFieldTouched(field, true);
        }
    };

    return (
        <>
            { isLoading && <LoadingBounce /> }
            {
                user && !isCurrentUser && canEditUser && (
                    <Box display="flex" gap="10px" flexWrap="wrap" marginBottom="16px" data-tourid={ ETourId.usersPageDisableButton } width="fit-content">
                        <ContainedButton kind="error" onClick={ openDeleteUserDialog }>
                            Удалить пользователя
                        </ContainedButton>
                        {
                            user.activated
                                // ? (
                                //     <ContainedButton kind="warning" onClick={ openDeactivationUserDialog }>
                                //         Отключить пользователя
                                //     </ContainedButton>
                                // )
                                ? null
                                : (
                                    <ContainedButton
                                        kind="primary"
                                        onClick={ activateUser }
                                    >
                                        Включить пользователя
                                    </ContainedButton>
                                )
                        }
                    </Box>
                )
            }
            <Box display="grid" gridTemplateColumns="1fr 1fr" gap="10px">
                <Box display="flex" flexDirection="column" gap="5px" pt="4px">
                    <FormAndLabel
                        label={ (
                            <Box display="flex" gap="5px" justifyContent="space-between" alignItems="baseline">
                                <TextOut fontWeight={ 700 }>Логин</TextOut>
                                <TextOut style={ { color: COLORS.info } } fontSize={ 10 }>
                                    { user?.login ? 'Смена логина невозможна. Рекомендуем удалить пользователя и создать нового' : 'В дальнейшем изменить логин пользователя будет невозможно' }
                                </TextOut>
                            </Box>
                        ) }
                    >
                        <div>
                            <TextField
                                name="login"
                                onFocus={ onFocusHandler('login') }
                                onChange={ userForm.handleChange }
                                type="text"
                                value={ userForm.values.login.trim() }
                                disabled={ !!user?.login }
                                error={ userForm.touched.login && !!userForm.errors.login }
                                fullWidth={ true }
                            />
                            <TextOut style={ { color: COLORS.error } } inDiv={ true }>{ userForm.touched.login && userForm.errors.login }</TextOut>
                        </div>
                    </FormAndLabel>
                    <FormAndLabel label="Телефон">
                        <div>
                            <OutlinedInput
                                name="phoneNumber"
                                onChange={ userForm.handleChange }
                                value={ userForm.values.phoneNumber.trim() }
                                error={ !!userForm.errors.phoneNumber }
                                onBlur={ userForm.handleBlur }
                                fullWidth={ true }
                                endAdornment={
                                    isAdminInfoShow && userForm.values.phoneNumber.trim() !== '' && (
                                        <Tooltip title={ `Телефон ${ !user?.isPhoneVerified ? 'не' : '' } подтвержден` } placement="top">
                                            <VerifiedIcon sx={ { color: user?.isPhoneVerified ? COLORS.main : undefined } } />
                                        </Tooltip>
                                    )
                                }
                            />
                            <TextOut style={ { color: COLORS.error } }>{ userForm.errors.phoneNumber }</TextOut>
                        </div>
                    </FormAndLabel>
                    <FormAndLabel label="Электронная почта">
                        <div>
                            <OutlinedInput
                                name="email"
                                onChange={ userForm.handleChange }
                                value={ userForm.values.email.trim() }
                                error={ !!userForm.errors.email }
                                onBlur={ userForm.handleBlur }
                                fullWidth={ true }
                                endAdornment={
                                    isAdminInfoShow && (
                                        <Tooltip title={ `Почта ${ !user?.isEmailVerified ? 'не' : '' } подтверждена` } placement="top">
                                            <VerifiedIcon sx={ { color: user?.isEmailVerified ? COLORS.main : undefined } } />
                                        </Tooltip>
                                    )
                                }
                            />
                            <TextOut style={ { color: COLORS.error } }>{ userForm.errors.email }</TextOut>
                        </div>
                    </FormAndLabel>
                    <FormAndLabel label="Фамилия">
                        <TextField
                            name="lastName"
                            onChange={ userForm.handleChange }
                            value={ userForm.values.lastName }
                            error={ !!userForm.errors.lastName }
                            helperText={ userForm.errors.lastName }
                            fullWidth={ true }
                        />
                    </FormAndLabel>
                    <FormAndLabel label="Имя">
                        <TextField
                            name="firstName"
                            onChange={ userForm.handleChange }
                            value={ userForm.values.firstName }
                            error={ !!userForm.errors.firstName }
                            helperText={ userForm.errors.firstName }
                            fullWidth={ true }
                        />
                    </FormAndLabel>
                    <FormAndLabel label="Отчество">
                        <TextField
                            name="middleName"
                            onChange={ userForm.handleChange }
                            value={ userForm.values.middleName }
                            error={ !!userForm.errors.middleName }
                            helperText={ userForm.errors.middleName }
                            fullWidth={ true }
                        />
                    </FormAndLabel>
                </Box>
                <Box display="flex" flexDirection="column" gap="5px">
                    <FormAndLabel label="Новый пароль">
                        <OutlinedInput
                            fullWidth={ true }
                            id="password"
                            value={ userForm.values.password }
                            onChange={ userForm.handleChange }
                            onFocus={ onFocusHandler('password') }
                            autoComplete="off"
                            error={ userForm.touched.password && !!userForm.errors.password }
                            endAdornment={
                                <InputAdornment position="end">
                                    <IconButton
                                        aria-label="toggle password visibility"
                                        onClick={ () => changeShowPassword('password') }
                                        onMouseDown={ (event: React.MouseEvent<HTMLButtonElement>) => event.preventDefault() }
                                        edge="end"
                                    >
                                        { showPassword.password ? <VisibilityOff /> : <Visibility /> }
                                    </IconButton>
                                </InputAdornment>
                            }
                            sx={ !showPassword.password ? stylePassword : undefined }
                        />
                        <TextOut style={ { color: COLORS.error } }>{ userForm.touched.password && userForm.errors.password }</TextOut>
                    </FormAndLabel>
                    <FormAndLabel label="Подтверждение пароля">
                        <OutlinedInput
                            fullWidth={ true }
                            id="confirmPassword"
                            value={ userForm.values.confirmPassword }
                            onChange={ userForm.handleChange }
                            onFocus={ onFocusHandler('confirmPassword') }
                            autoComplete="off"
                            error={ userForm.touched.confirmPassword && !!userForm.errors.confirmPassword }
                            endAdornment={
                                <InputAdornment position="end">
                                    <IconButton
                                        aria-label="toggle password visibility"
                                        onClick={ () => changeShowPassword('confirmPassword') }
                                        onMouseDown={ (event: React.MouseEvent<HTMLButtonElement>) => event.preventDefault() }
                                        edge="end"
                                    >
                                        { showPassword.confirmPassword ? <VisibilityOff /> : <Visibility /> }
                                    </IconButton>
                                </InputAdornment>
                            }
                            sx={ !showPassword.confirmPassword ? stylePassword : undefined }
                        />
                        <TextOut style={ { color: COLORS.error } }>{ userForm.touched.confirmPassword && userForm.errors.confirmPassword }</TextOut>
                    </FormAndLabel>
                    <FormAndLabel label="Отдел">
                        <Select
                            value={
                                userForm.values.departmentRequest?.actionType === EDepartmentAction.remove ? 'none' : userForm.values.departmentRequest?.departmentId ?? 'none'
                            }
                            variant="outlined"
                            id="departmentRequest"
                            name="departmentRequest"
                            fullWidth={ true }
                            onChange={ ev => {
                                if (ev.target.value !== 'none') {
                                    userForm.setFieldValue('departmentRequest', {
                                        ...myCompany.departments.find(department => department.departmentId === ev.target.value),
                                        actionType: user?.department ? EDepartmentAction.update : EDepartmentAction.create
                                    });
                                } else if (user && user?.department !== null) {
                                    userForm.setFieldValue('departmentRequest', {
                                        ...user.department,
                                        actionType: EDepartmentAction.remove
                                    });
                                } else {
                                    userForm.setFieldValue('departmentRequest', null);
                                }
                            } }
                        >
                            { myCompany.departments.map(({ departmentId, name }) => (
                                <MenuItem key={ departmentId } value={ departmentId }>{ name }</MenuItem>
                            )) }
                            <MenuItem sx={ { '&:hover': { color: COLORS.errorLight } } } value="none">Не выбран</MenuItem>
                        </Select>
                    </FormAndLabel>
                    {
                        canEditRoles && !isCurrentUser && (
                            <FormAndLabel label="Роли">
                                <DropDownListWithCheckboxes
                                    formName="roles"
                                    deepCompare={ true }
                                    items={
                                        currentUserGroups.includes(5) && currentUserGroups.length === 1
                                            ? UserRolesEnumStatusExtensions.getHotlineDescription(user ? { isLastAdmin, user: user.accountRoles } : undefined)
                                            : UserRolesEnumStatusExtensions.getAllDescriptions(user ? { isLastAdmin, user: user.accountRoles } : undefined)
                                    }
                                    onValueChange={ (formName, value) => userForm.setFieldValue(formName, value.map(item => item.value)) }
                                />
                            </FormAndLabel>
                        )
                    }
                </Box>
            </Box>
            <Divider style={ { margin: '8px 0' } } />
            <div style={ { textAlign: 'end' } }>
                {
                    canEditUser || isCurrentUser
                        ? <SuccessButton isEnabled={ userForm.isValid } onClick={ userForm.submitForm }>Сохранить</SuccessButton>
                        : <TextOut>Нет прав для редактирования</TextOut>
                }
            </div>
            <DeleteUserDialog
                isOpen={ showDeleteUserModal }
                onCancelClick={ closeDeleteUserDialog }
                closeUserCard={ onCancelClick }
                login={ user?.login }
                userId={ user?.id }
            />
            <DeactivationUserDialog
                isOpen={ showDeactivationUserDialog }
                onCancelClick={ closeDeactivationUserDialog }
                userId={ user?.id }
            />
        </>
    );
};

export const CommonUserInfoView = memo((connect(null, { dispatchReceiveMyCompanyThunk: ReceiveMyCompanyThunk.invoke })(UserInfo)));