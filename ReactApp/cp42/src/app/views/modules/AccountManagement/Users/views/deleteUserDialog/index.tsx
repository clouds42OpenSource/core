import { FETCH_API } from 'app/api/useFetchApi';
import { EMessageType, useFloatMessages } from 'app/hooks';
import { Dialog } from 'app/views/components/controls/Dialog';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { TextOut } from 'app/views/components/TextOut';
import React, { useState } from 'react';

type TDeleteUserDialogProps = {
    isOpen: boolean;
    onCancelClick: () => void;
    closeUserCard: (isEdited: boolean) => void;
    userId?: string;
    login?: string;
};

const { postDeleteUser } = FETCH_API.USERS;

export const DeleteUserDialog = ({ isOpen, onCancelClick, login, userId, closeUserCard }: TDeleteUserDialogProps) => {
    const { show } = useFloatMessages();

    const [isLoading, setIsLoading] = useState(false);

    const deleteUser = async () => {
        onCancelClick();
        setIsLoading(true);
        const { success, message } = await postDeleteUser(userId);

        if (!success) {
            show(EMessageType.error, message);
        }

        closeUserCard(true);

        setIsLoading(false);
    };

    return (
        <>
            { isLoading && <LoadingBounce /> }
            <Dialog
                title="Удаление пользователя"
                dialogVerticalAlign="center"
                isOpen={ isOpen }
                isTitleSmall={ true }
                dialogWidth="sm"
                onCancelClick={ onCancelClick }
                buttons={ [
                    {
                        variant: 'contained',
                        kind: 'default',
                        content: 'Нет',
                        onClick: () => onCancelClick()
                    },
                    {
                        variant: 'contained',
                        kind: 'error',
                        content: 'Да',
                        onClick: () => deleteUser()
                    }
                ] }
            >
                <TextOut>Вы действительно хотите удалить пользователя { login ?? '' }?</TextOut>
            </Dialog>
        </>
    );
};