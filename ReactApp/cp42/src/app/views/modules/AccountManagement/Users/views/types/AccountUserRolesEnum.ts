import { SortingKind } from 'app/common/enums';
import { generateReadOnlyKeyValueDataModelArrayFrom, makeReadOnlyKeyValueDataModelArrayByValueComparator } from 'app/common/functions';
import { DropDownListWithCheckboxItem } from 'app/views/components/controls/forms/DropDownListWithCheckboxes/types';

export enum AccountUserRolesEnum {
    /** Пользователь аккаунта */
    AccountUser = 'AccountUser',
    /** Менеджер аккаунта */
    AccountAdmin = 'AccountAdmin',
    /** Сейлс менеджер */
    AccountSaleManager = 'AccountSaleManager',
    /** Менеджер облака */
    CloudAdmin = 'CloudAdmin',
    /** Служба */
    Cloud42Service = 'Cloud42Service',
    /** Хотлайн */
    Hotline = 'Hotline',
    /** Инженер облака */
    CloudSE = 'CloudSE',
    /** Оператор процес центра */
    ProcOperator = 'ProcOperator',
    /** Внешняя служба */
    ExternalService = 'ExternalService',
    /** Редактор статей */
    ArticleEditor = 'ArticleEditor'
}

/**
 * Описание перечисления ролей пользователя
 */
const UserRolesStatusStatusDescription = generateReadOnlyKeyValueDataModelArrayFrom(AccountUserRolesEnum, enumValue => {
    switch (enumValue) {
        case AccountUserRolesEnum.AccountAdmin:
            return 'Менеджер аккаунта';

        case AccountUserRolesEnum.AccountSaleManager:
            return 'Сейлс менеджер';

        case AccountUserRolesEnum.AccountUser:
            return 'Пользователь аккаунта';

        case AccountUserRolesEnum.CloudAdmin:
            return 'Менеджер облака';

        case AccountUserRolesEnum.Cloud42Service:
            return 'Служба';

        case AccountUserRolesEnum.Hotline:
            return 'Хотлайн';

        case AccountUserRolesEnum.CloudSE:
            return 'Инженер облака';

        case AccountUserRolesEnum.ProcOperator:
            return 'Оператор процес центра';

        case AccountUserRolesEnum.ExternalService:
            return 'Внешняя служба';

        case AccountUserRolesEnum.ArticleEditor:
            return 'Редактор статей';

        default:
            return '';
    }
}).sort(makeReadOnlyKeyValueDataModelArrayByValueComparator(SortingKind.Asc, AccountUserRolesEnum.CloudAdmin));

export const UserRolesEnumStatusExtensions = {
    getAllDescriptions: (args?: {
        isLastAdmin: boolean | undefined, user: string[]
    }): DropDownListWithCheckboxItem[] => {
        return UserRolesStatusStatusDescription.map(item => {
            return {
                checked: args ? args.user.includes(item.key) : false,
                text: item.value,
                value: item.key,
                disabled: args?.user.includes(item.key) && item.key === 'AccountAdmin' && args?.isLastAdmin,
                visible: true
            };
        });
    },
    getHotlineDescription: (args?: {
        isLastAdmin: boolean | undefined, user: string[]
    }): DropDownListWithCheckboxItem[] => {
        return UserRolesStatusStatusDescription.map(item => {
            return {
                checked: args ? args.user.includes(item.key) : false,
                text: item.value,
                value: item.key,
                disabled: args?.user.includes(item.key) && item.key === 'AccountAdmin' && args?.isLastAdmin,
                visible: item.key === 'AccountUser' || item.key === 'AccountSaleManager' || item.key === 'AccountAdmin'
            };
        });
    }
};