import { DatabaseAccessSettings } from 'app/views/modules/AccountManagement/Users/tabs/DatabaseAccessSettings';
import { AccountRelatedUsersDataModel, AccountRelatedUsersItemDataModel } from 'app/web/InterlayerApiProxy/UserManagementApiProxy/getUsersWithAccess/data-models';
import { AppReduxStoreState } from 'app/redux/types';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { Dialog } from 'app/views/components/controls/Dialog';
import { Divider } from '@mui/material';
import { GetAccountRelatedUsersThunk } from 'app/modules/AccountManagement/UserManagement/store/thunks/GetAccountRelatedUsersThunk';
import { GetAccountRelatedUsersThunkParams } from 'app/modules/AccountManagement/UserManagement/store/reducers/GetAccountRelatedUsersReducer/params';
import React from 'react';
import { SelectDataCommonDataModel } from 'app/web/common';
import { TextButton } from 'app/views/components/controls/Button';
import { TextOut } from 'app/views/components/TextOut';
import { connect } from 'react-redux';
import { hasComponentChangesFor } from 'app/common/functions';
import { Get1CStatusForUserThunkParams } from 'app/modules/AccountManagement/UserManagement/store/reducers/GetUserWithRent1CReducer/params';
import { GetUserWithRent1CThunk } from 'app/modules/AccountManagement/UserManagement/store/thunks/GetUserWithRent1CThunk';
import { Reset1CStatusForUserThunkParams } from 'app/modules/AccountManagement/UserManagement/store/reducers/ResetUserWithRent1CReducer/params';
import { ResetUserWithRent1CThunk } from 'app/modules/AccountManagement/UserManagement/store/thunks/ResetUserWithRent1CThunk';
import { AppRoutes } from 'app/AppRoutes';

type DispatchProps = {
    dispatchGetRelatedUsers: (args: GetAccountRelatedUsersThunkParams) => void;
    dispatchRent1C: (args: Get1CStatusForUserThunkParams) => void;
    dispatchResetRent1C: (args: Reset1CStatusForUserThunkParams) => void;
};

type StateProps = {
    data: AccountRelatedUsersDataModel
    hasSuccessRent1C: boolean;
};

type OwnState = {
    showDBList: boolean;
    userId: string;
    pagination: {
        currentPage: number,
        pageSize: number,
        recordsCount: number,
        totalPages: number
    }
};

type AllProps = StateProps & DispatchProps;

class RelatedUsersViewClass extends React.Component<AllProps, OwnState> {
    constructor(props: AllProps) {
        super(props);
        this.onUserClick = this.onUserClick.bind(this);
        this.onDataSelect = this.onDataSelect.bind(this);
        this.getTitle = this.getTitle.bind(this);
        this.getUserById = this.getUserById.bind(this);

        this.state = {
            showDBList: false,
            userId: '',
            pagination: {
                currentPage: 1,
                pageSize: 10,
                recordsCount: this.props.data.recordsCount,
                totalPages: this.props.data.pages
            }
        };
    }

    public componentDidMount() {
        this.onDataSelect();
    }

    public shouldComponentUpdate(nextProps: AllProps, nextState: OwnState) {
        if (this.props.data.recordsCount !== nextProps.data.recordsCount) {
            this.setState(prevState => ({
                pagination: {
                    ...prevState.pagination,
                    recordsCount: nextProps.data.recordsCount,
                    totalPages: nextProps.data.pages
                }
            }));
        }
        return hasComponentChangesFor(this.props, nextProps) || hasComponentChangesFor(this.state, nextState);
    }

    private onDataSelect(args?: SelectDataCommonDataModel<object>) {
        if (args) {
            this.setState(prevState => ({
                pagination: {
                    ...prevState.pagination,
                    currentPage: args.pageNumber,
                }
            }));
            this.props.dispatchGetRelatedUsers({
                'page-size': this.state.pagination.pageSize,
                page: args.pageNumber,
                'account-related': false,
                force: true
            });
            return;
        }
        this.props.dispatchGetRelatedUsers({
            'page-size': this.state.pagination.pageSize,
            page: this.state.pagination.currentPage,
            'account-related': false,
            force: true
        });
    }

    private onUserClick(userId: string, show: boolean) {
        this.setState({ showDBList: show, userId });

        if (window.location.search !== '') {
            window.history.pushState(null, '', AppRoutes.accountManagement.userManagement);
            this.props.dispatchResetRent1C({ force: true });
        }
    }

    private getTitle() {
        if (this.state.userId) {
            return `Права пользователя: ${ this.getUserById(this.state.userId)?.name }`;
        }

        return 'Создание пользователя';
    }

    private getUserById(id?: string): AccountRelatedUsersItemDataModel | undefined {
        if (!id) {
            return undefined;
        }

        const index = this.props.data.users.findIndex(item => item.id === id);
        return this.props.data.users[index];
    }

    public render() {
        return (
            <>
                {
                    !!this.props.data.users.length && (
                        <CommonTableWithFilter
                            isBorderStyled={ true }
                            onDataSelect={ this.onDataSelect }
                            uniqueContextProviderStateId="accountRelatedUsersContextProviderStateId"
                            isResponsiveTable={ true }
                            tableProps={ {
                                headerTitleTable: {
                                    text: 'Сторонние пользователи с доступом к базам этого аккаунта',
                                },
                                pagination: {
                                    currentPage: this.state.pagination.currentPage,
                                    pageSize: this.state.pagination.pageSize,
                                    recordsCount: this.state.pagination.recordsCount,
                                    totalPages: this.state.pagination.totalPages,
                                },
                                dataset: this.props.data.users,
                                keyFieldName: 'id',
                                fieldsView: {
                                    name: {
                                        fieldWidth: '100%',
                                        caption: 'Пользователь',
                                        format: (value: string, data) =>
                                            <TextButton
                                                onClick={ () => {
                                                    this.props.dispatchRent1C({ userId: data.id, force: true });
                                                    this.onUserClick(data.id, true);
                                                } }
                                                canSelectButtonContent={ true }
                                                wrapLongText={ true }
                                            >
                                                <TextOut fontSize={ 13 } className="text-link">
                                                    { value }
                                                </TextOut>
                                            </TextButton>
                                    }
                                }
                            } }
                        />
                    )
                }
                { this.props.hasSuccessRent1C && this.state.userId !== '' &&
                    <Dialog
                        title={ this.getTitle() }
                        dialogVerticalAlign="top"
                        isOpen={ this.state.showDBList }
                        isTitleSmall={ true }
                        dialogWidth="lg"
                        onCancelClick={ () => this.onUserClick('', false) }
                    >
                        <DatabaseAccessSettings
                            userId={ this.state.userId }
                            isRentActive={ true }
                            isExternalUser={ true }
                            userName={ this.props.data.users.find(user => user.id === this.state.userId)?.name ?? '' }
                        />
                    </Dialog>
                }
                <Divider style={ { height: '36px' } } />
            </>
        );
    }
}

export const RelatedUsersView = connect<StateProps, DispatchProps, object, AppReduxStoreState>(
    state => {
        return {
            data: state.UserManagementState.accountRelatedUsers,
            hasSuccessRent1C: state.UserManagementState.hasSuccessFor.hasRent1CReceived
        };
    },
    {
        dispatchGetRelatedUsers: GetAccountRelatedUsersThunk.invoke,
        dispatchRent1C: GetUserWithRent1CThunk.invoke,
        dispatchResetRent1C: ResetUserWithRent1CThunk.invoke
    }
)(RelatedUsersViewClass);