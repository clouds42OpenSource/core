import { Box } from '@mui/material';
import { FETCH_API } from 'app/api/useFetchApi';
import { EMessageType, useAppDispatch, useFloatMessages } from 'app/hooks';
import { userCard } from 'app/modules/users/reducers/userCard';
import { OutlinedButton } from 'app/views/components/controls/Button';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { ETourId } from 'app/views/Layout/ProjectTour/enums';
import style from 'app/views/modules/AccountManagement/Users/style.module.css';
import { DisableSessionsDialog } from 'app/views/modules/AccountManagement/Users/views/disableSessionsDialog';
import { TRowData } from 'app/views/modules/AccountManagement/Users/views/types';
import React, { useState } from 'react';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';

const { getAccountUserActiveSessions } = FETCH_API.USERS;

const { createNewUser } = userCard.actions;

export const UserManagement = () => {
    const dispatch = useAppDispatch();
    const { show } = useFloatMessages();

    const [isLoading, setIsLoading] = useState(false);
    const [rows, setRows] = useState<TRowData[]>([]);
    const [showDisableSessionsDialog, setShowDisableSessionsDialog] = useState(false);

    const createUser = () => {
        dispatch(createNewUser());
    };

    const handleGetActiveSessions = async () => {
        setIsLoading(true);

        const { data, success, message } = await getAccountUserActiveSessions();

        if (success) {
            setRows((data?.userTerminateList ?? []).map(item => ({
                id: item.id,
                fullName: item.fullName ?? `${ item.lastName ?? '' } ${ item.firstName ?? '' } ${ item.middleName ?? '' }` ?? '',
                login: item.login
            })));
            setShowDisableSessionsDialog(true);
        } else {
            show(EMessageType.error, message);
        }

        setIsLoading(false);
    };

    const closeDisableSessionsDialog = () => {
        setShowDisableSessionsDialog(false);
        setRows([]);
    };

    return (
        <>
            { isLoading && <LoadingBounce /> }
            <FormAndLabel label="Управление пользователями">
                <Box display="flex" gap="8px" flexWrap="wrap">
                    <OutlinedButton
                        tourId={ ETourId.usersPageAddUser }
                        className={ style.control }
                        kind="success"
                        onClick={ createUser }
                    >
                        <i className="fa fa-plus-circle" />&nbsp;
                        Добавить пользователя
                    </OutlinedButton>
                    <OutlinedButton
                        className={ style.control }
                        kind="error"
                        onClick={ handleGetActiveSessions }
                    >
                        <i className="fa fa-times-circle" />&nbsp;
                        Завершение сеансов на сервере
                    </OutlinedButton>
                </Box>
            </FormAndLabel>
            <DisableSessionsDialog
                isOpen={ showDisableSessionsDialog }
                onCancelClick={ closeDisableSessionsDialog }
                rows={ rows }
            />
        </>
    );
};