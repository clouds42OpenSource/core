import { Box } from '@mui/material';
import { FETCH_API } from 'app/api/useFetchApi';

import { AppRoutes } from 'app/AppRoutes';
import { EMessageType, useAppSelector, useFloatMessages } from 'app/hooks';
import { Dialog } from 'app/views/components/controls/Dialog';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import CustomizedSwitches from 'app/views/components/controls/Switch/views/SwitchViews';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { TextOut } from 'app/views/components/TextOut';
import { PaymentDialog } from 'app/views/modules/_common/dialogs';
import React, { useEffect, useMemo, useState } from 'react';
import { Link } from 'react-router-dom';

import styles from './style.module.css';

type TUserRentDialog = {
    isOpen: boolean;
    onCancelClick: () => void;
    onSkipClick: () => void;
};

type TPaymentDialogProps = {
    notEnoughSum?: number;
    canUsePromisePayment?: boolean;
};

const { RENT: { tryPayAccess, useGetRent, tryPromisePayment } } = FETCH_API;

export const UserRentDialog = ({ isOpen, onCancelClick, onSkipClick }: TUserRentDialog) => {
    const { show } = useFloatMessages();
    const { lastCreateUser } = useAppSelector(state => state.Users.userCard);
    const { lastCreatedUser: { message, data: createdUserData } } = useAppSelector(state => state.UserManagementState) || {};

    const { data: rentData, error: rentError, isLoading } = useGetRent(!isOpen);

    const [isWeb, setIsWeb] = useState(false);
    const [isPaymentDialogOpen, setIsPaymentDialogOpen] = useState(false);
    const [paymentDialogProps, setPaymentDialogProps] = useState<TPaymentDialogProps>({});
    const [isManualLoading, setIsManualLoading] = useState(false);

    const rentUserData = useMemo(() => rentData?.rawData.userList.find(user => user.id === (createdUserData?.id ?? lastCreateUser?.data?.id)), [createdUserData?.id, lastCreateUser?.data?.id, rentData?.rawData.userList]);

    useEffect(() => {
        if (rentError) {
            show(EMessageType.error, rentError);
        }
    }, [rentError, show]);

    useEffect(() => {
        if (message || (lastCreateUser && lastCreateUser.message)) {
            show(EMessageType.error, message ?? lastCreateUser?.message);
        }
    }, [lastCreateUser, message, onCancelClick, show]);

    useEffect(() => {
        if (rentData?.rawData.serviceStatus === null) {
            onCancelClick();
        }
    }, [onCancelClick, rentData?.rawData.serviceStatus]);

    const acceptRent = async () => {
        if ((createdUserData ?? lastCreateUser?.data) && rentData) {
            setIsManualLoading(true);
            const { freeWebResources, freeRdpResources } = rentData.rawData;

            const { message: payMessage, data: payData } = await tryPayAccess([{
                webResource: isWeb,
                standartResource: !isWeb,
                accountUserId: createdUserData?.id ?? lastCreateUser?.data?.id ?? '',
                webResourceId: freeWebResources.length > 0 ? freeWebResources[0] : null,
                rdpResourceId: !isWeb && freeRdpResources.length > 0 ? freeRdpResources[0] : null
            }]);

            if (!payData) {
                show(EMessageType.error, payMessage);
            } else if (!payData.complete) {
                const { canGetPromisePayment, enoughMoney, errorMessage } = payData;
                setPaymentDialogProps({
                    canUsePromisePayment: canGetPromisePayment,
                    notEnoughSum: enoughMoney
                });

                if (errorMessage) {
                    show(EMessageType.warning, errorMessage);
                }

                setIsPaymentDialogOpen(true);
            } else {
                show(EMessageType.success, 'Аренда 1С успешно подключена');
                onCancelClick();
            }

            setIsManualLoading(false);
        }
    };

    const changeTariff = () => {
        setIsWeb(prev => !prev);
    };

    const getAcceptRentContent = () => {
        if (rentData && (createdUserData ?? lastCreateUser?.data) && rentUserData) {
            const { freeWebResources, freeRdpResources, locale: { currency } } = rentData.rawData;
            const freeWebResourcesLength = freeWebResources.length;
            const freeRdpResourcesLength = freeRdpResources.length;

            if (freeWebResourcesLength > 0 && (freeRdpResourcesLength > 0 || isWeb)) {
                return 'Применить';
            }

            if (isWeb || (freeWebResourcesLength === 0 && freeRdpResourcesLength > 0)) {
                return `Купить (${ rentUserData.partialUserCostWeb } ${ currency }.)`;
            }

            if (freeWebResourcesLength > 0 && freeRdpResourcesLength === 0) {
                return `Купить (${ rentUserData.partialUserCostRdp } ${ currency }.)`;
            }

            return `Купить (${ rentUserData.partialUserCostStandart } ${ currency }.)`;
        }

        return false;
    };

    const onCancelClickPaymentDialogHandler = () => {
        setIsPaymentDialogOpen(false);
    };

    const applyPaymentHandler = async () => {
        const { freeWebResources, freeRdpResources } = rentData?.rawData || {};
        setIsManualLoading(true);

        const { message: applyMessage } = await tryPromisePayment([{
            accountUserId: createdUserData?.id ?? lastCreateUser?.data?.id ?? '',
            webResource: isWeb,
            standartResource: !isWeb,
            rdpResourceId: !isWeb && freeRdpResources && freeRdpResources.length > 0 ? freeRdpResources[0] : null,
            webResourceId: freeWebResources && freeWebResources.length > 0 ? freeWebResources[0] : null
        }]);

        if (applyMessage) {
            show(EMessageType.error, applyMessage);
        } else {
            show(EMessageType.success, 'Обещанный платеж был успешно взят');
        }

        setIsManualLoading(false);
    };

    return (
        <>
            { (isLoading || isManualLoading) && <LoadingBounce /> }
            <PaymentDialog
                isOpen={ isPaymentDialogOpen }
                onCancelClick={ () => {
                    onCancelClickPaymentDialogHandler();
                    onCancelClick();
                } }
                apply={ applyPaymentHandler }
                currency={ rentData?.rawData.locale.currency }
                sum={ paymentDialogProps.notEnoughSum }
                isPromisePaymentAvailable={ paymentDialogProps.canUsePromisePayment }
            />
            <Dialog
                buttons={ [
                    {
                        content: 'Пропустить',
                        kind: 'default',
                        onClick: () => {
                            onCancelClick();
                            onSkipClick();
                        }
                    },
                    {
                        isEnabled: !!(createdUserData ?? lastCreateUser?.data) && !!rentData && !rentData.rawData.serviceStatus?.serviceIsLocked,
                        content: getAcceptRentContent(),
                        kind: 'success',
                        onClick: acceptRent
                    }
                ] }
                title="Выберите тип аренды"
                isTitleSmall={ true }
                isOpen={ isOpen }
                dialogWidth="xs"
                onCancelClick={ onCancelClick }
                maxHeight="310px"
                dialogVerticalAlign="bottom"
            >
                { rentData?.rawData.serviceStatus?.serviceIsLocked
                    ? (
                        <Box className={ styles.error }>
                            <TextOut inheritColor={ true } textAlign="center">
                                { rentData.rawData.serviceStatus.serviceLockReason }
                            </TextOut>
                        </Box>
                    )
                    : (
                        <>
                            <TextOut>
                                Вы можете активировать тариф пользователю {
                                    createdUserData?.login ?? lastCreateUser?.data?.login ?? ''
                                } сейчас или на странице <Link className={ styles.link } to={ AppRoutes.services.rentReference }>Аренда 1С</Link>.
                            </TextOut>
                            <Box display="flex" gap="10px" justifyContent="space-around">
                                <FormAndLabel label="WEB" className={ styles.formAndLabel }>
                                    <CustomizedSwitches disabled={ !(createdUserData ?? lastCreateUser?.data) && !rentData } onChange={ changeTariff } checked={ isWeb } />
                                </FormAndLabel>
                                <FormAndLabel label="Стандарт" className={ styles.formAndLabel }>
                                    <CustomizedSwitches disabled={ !(createdUserData ?? lastCreateUser?.data) && !rentData } onChange={ changeTariff } checked={ !isWeb } />
                                </FormAndLabel>
                            </Box>
                        </>
                    )
                }
            </Dialog>
        </>
    );
};