import { TPutEditUser } from 'app/api/endpoints/users/request';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { AccountUsersItemDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/getAccountUsers';
import { RequestKind } from 'core/requestSender/enums';
import * as Yup from 'yup';

const { IsUserLoginAviable, IsEmailAvailable, IsPhoneAvailable } = InterlayerApiProxy.getUserManagementApiProxy();

const passwordRegEx = /^([A-Za-z0-9!@#$^&*_+\-=\[\];':""\\|,.<>\/?])+$/;

export const getUserForm = (user?: AccountUsersItemDataModel) => {
    const loginTest = (): Yup.TestConfig<string | undefined, Yup.AnyObject> => ({
        name: 'checkLogin',
        message: '',
        skipAbsent: true,
        test: async (value, context) => {
            const { message } = await IsUserLoginAviable(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, { Login: value ?? '', userId: user?.id ?? '' });

            if (message) {
                return context.createError({ message });
            }

            return true;
        }
    });

    const emailTest = (): Yup.TestConfig<string | undefined, Yup.AnyObject> => ({
        name: 'checkEmail',
        message: '',
        skipAbsent: true,
        test: async (value, context) => {
            if (value === user?.email) {
                return true;
            }

            const { message } = await IsEmailAvailable(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, { Email: value ?? '', userId: user?.id ?? '' });

            if (message) {
                return context.createError({ message });
            }

            return true;
        }
    });

    const phoneNumberTest = (): Yup.TestConfig<string | undefined, Yup.AnyObject> => ({
        name: 'checkPhoneNumber',
        message: '',
        skipAbsent: true,
        test: async (value, context) => {
            if (value === user?.phoneNumber) {
                return true;
            }

            const { message } = await IsPhoneAvailable(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, { PhoneNumber: value ?? '', userId: '' });

            if (message) {
                return context.createError({ message });
            }

            return true;
        }
    });

    const namesTest = (): Yup.TestConfig<string | undefined, Yup.AnyObject> => ({
        name: '',
        message: '',
        skipAbsent: true,
        test: async (value, context) => {
            if (value && !/^[а-яА-ЯІіЇїЄєҐґёЁa-zA-Z]+$/.test(value)) {
                return context.createError({ message: 'Неверный формат данных' });
            }

            return true;
        },
    });

    let userFormSchema = Yup.object({
        login: Yup.string(),
        phoneNumber: Yup.string()
            .test(phoneNumberTest()),
        email: Yup.string()
            .test(emailTest()),
        lastName: Yup.string().test(namesTest()),
        firstName: Yup.string().test(namesTest()),
        middleName: Yup.string().test(namesTest()),
        password: Yup.string()
            .min(8, 'Пароль должен быть длиной от 8 до 25 символов')
            .max(25, 'Пароль должен быть длиной от 8 до 25 символов')
            .matches(passwordRegEx, 'Пароль должен содержать только латинские буквы, числа и спецзнаки'),
        confirmPassword: Yup.string()
            .oneOf([Yup.ref('password'), undefined], 'Пароль и его подтверждение совпадают')
            .when('password', {
                is: (val: string) => !!val,
                then: (schema: Yup.StringSchema) => schema.required('Подтвердите пароль'),
                otherwise: (schema: Yup.StringSchema) => schema.notRequired(),
            }),
        roles: Yup.array().of(Yup.string()),
        departmentRequest: Yup.object({
            name: Yup.string(),
            departmentId: Yup.string(),
            actionType: Yup.number(),
        }).nullable()
    });

    if (!user) {
        userFormSchema = Yup.object({
            login: Yup.string()
                .test(loginTest())
                .required('Логин не может быть пустым'),
            phoneNumber: Yup.string()
                .test(phoneNumberTest()),
            email: Yup.string()
                .test(emailTest()),
            lastName: Yup.string().test(namesTest()),
            firstName: Yup.string().test(namesTest()),
            middleName: Yup.string().test(namesTest()),
            password: Yup.string()
                .min(8, 'Пароль должен быть длиной от 8 до 25 символов')
                .max(25, 'Пароль должен быть длиной от 8 до 25 символов')
                .matches(passwordRegEx, 'Пароль должен содержать только латинские буквы, числа и спецзнаки')
                .required('Пароль должен быть длиной от 8 до 25 символов и содержать только латинские буквы, числа и спецзнаки'),
            confirmPassword: Yup.string()
                .oneOf([Yup.ref('password'), undefined], 'Пароль и его подтверждение должны совпадать')
                .required('Пароль и его подтверждение должны совпадать'),
            roles: Yup.array().of(Yup.string()),
            departmentRequest: Yup.object({
                name: Yup.string(),
                departmentId: Yup.string(),
                actionType: Yup.number(),
            }).nullable()
        });
    }

    const initialValueUserForm: TPutEditUser = {
        id: user?.id ?? '',
        login: user?.login ?? '',
        phoneNumber: user?.phoneNumber ?? '',
        email: user?.email ?? '',
        lastName: user?.lastName ?? '',
        firstName: user?.firstName ?? '',
        middleName: user?.middleName ?? '',
        password: '',
        confirmPassword: '',
        roles: user?.accountRoles ?? [],
        departmentRequest: user?.department ?? null
    };

    return { userFormSchema: Yup.lazy(() => userFormSchema), initialValueUserForm };
};