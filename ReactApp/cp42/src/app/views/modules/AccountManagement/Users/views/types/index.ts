export type TRowData = {
    id: string;
    fullName: string;
    login: string;
}