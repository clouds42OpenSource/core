import { Box } from '@mui/material';
import { TourProps, withTour } from '@reactour/tour';
import { withHeader } from 'app/views/components/_hoc/withHeader';
import { RelatedUsersView } from 'app/views/modules/AccountManagement/Users/views/RelatedUsersView';
import { UserFilter } from 'app/views/modules/AccountManagement/Users/views/userFilter';
import { UserManagement } from 'app/views/modules/AccountManagement/Users/views/userManagement';
import { UserTable } from 'app/views/modules/AccountManagement/Users/views/userTable';
import React from 'react';

const Users = ({ ...tourProps }: TourProps) => {
    return (
        <Box display="flex" flexDirection="column" gap="16px">
            <Box display="flex" justifyContent="space-between" alignItems="flex-end" flexWrap="wrap" gap="8px">
                <UserManagement />
                <UserFilter />
            </Box>
            <UserTable { ...tourProps } />
            <RelatedUsersView />
        </Box>
    );
};

export const UsersView = withHeader({
    title: 'Пользователи',
    page: withTour(Users)
});