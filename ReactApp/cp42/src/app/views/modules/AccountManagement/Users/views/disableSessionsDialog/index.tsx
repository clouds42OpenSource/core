import { DataGrid, GridColDef } from '@mui/x-data-grid';
import { FETCH_API } from 'app/api/useFetchApi';
import { EMessageType, useFloatMessages } from 'app/hooks';
import { Dialog } from 'app/views/components/controls/Dialog';
import { TextOut } from 'app/views/components/TextOut';
import { TRowData } from 'app/views/modules/AccountManagement/Users/views/types';
import React, { useState } from 'react';

const columnsDisableSessions: GridColDef[] = [
    { field: 'id', headerName: 'ID', width: 50, hide: true },
    { field: 'login', headerName: 'Логин', minWidth: 200, sortable: false, disableColumnMenu: true },
    { field: 'fullName', headerName: 'ФИО', minWidth: 250, sortable: false, disableColumnMenu: true },
];

type TDisableSessionsDialogProps = {
    rows: TRowData[];
    isOpen: boolean;
    onCancelClick: () => void;
}

const { postAccountUserActiveSessions } = FETCH_API.USERS;

export const DisableSessionsDialog = ({ isOpen, rows, onCancelClick }: TDisableSessionsDialogProps) => {
    const { show } = useFloatMessages();

    const [userId, setUserId] = useState<React.ReactText[]>([]);

    const endSessions = async () => {
        const { success, message } = await postAccountUserActiveSessions(userId);

        if (!success) {
            show(EMessageType.error, message);
        }

        onCancelClick();
    };

    return (
        <Dialog
            title="Завершение сеансов"
            isOpen={ isOpen }
            onCancelClick={ onCancelClick }
            dialogWidth="sm"
            dialogVerticalAlign="center"
            buttons={ [
                {
                    content: 'Завершить сеанс(ы)',
                    kind: 'error',
                    onClick: () => endSessions(),
                    hiddenButton: userId.length === 0
                },
                {
                    content: 'Закрыть',
                    kind: 'default',
                    onClick: () => onCancelClick()
                }
            ] }
        >
            <>
                {
                    rows.length
                        ? (
                            <div style={ { height: 300, minHeight: 300, width: '100%' } }>
                                <DataGrid
                                    rows={ rows }
                                    columns={ columnsDisableSessions }
                                    checkboxSelection={ true }
                                    showColumnRightBorder={ false }
                                    showCellRightBorder={ false }
                                    hideFooter={ true }
                                    onSelectionModelChange={ item => setUserId(prev => [...prev, ...item]) }
                                />
                            </div>
                        )
                        : <TextOut textAlign="center">Нет активных сессий.</TextOut>
                }
            </>
        </Dialog>
    );
};