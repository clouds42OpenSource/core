import { TUserItem } from 'app/api/endpoints/users/response';
import { FETCH_API } from 'app/api/useFetchApi';
import { useQuery } from 'app/hooks/useQuery';
import React, { useEffect, useMemo, useState } from 'react';

import { AccountUserGroup } from 'app/common/enums';
import { EMessageType, useAppSelector, useFloatMessages } from 'app/hooks';
import { DefaultButton } from 'app/views/components/controls/Button';
import { Dialog } from 'app/views/components/controls/Dialog';
import { TabsControl } from 'app/views/components/controls/TabsControl';
import { THeaders } from 'app/views/components/controls/TabsControl/views/TabView';
import { ETourId } from 'app/views/Layout/ProjectTour/enums';
import { CommonUserInfoView } from 'app/views/modules/AccountManagement/Users/tabs/CommonUserInfoView';
import { DatabaseAccessSettings } from 'app/views/modules/AccountManagement/Users/tabs/DatabaseAccessSettings';
import { UserRentDialog } from 'app/views/modules/AccountManagement/Users/views/userRentDialog';

export type TProps = {
    isOpen: boolean;
    onCancelClick: (isEdited?: boolean) => void;
    id?: string;
    isLastAdmin?: boolean;
};

const { getUser } = FETCH_API.USERS;

export const UserCard = ({ isOpen, onCancelClick, id, isLastAdmin }: TProps) => {
    const { currentUserGroups } = useAppSelector(state => state.Global.getCurrentSessionSettingsReducer.settings.currentContextInfo);
    const { lastCreateUser } = useAppSelector(state => state.Users.userCard);
    const { rent1C } = useAppSelector(state => state.UserManagementState);

    const { show } = useFloatMessages();
    const query = useQuery();

    const [showRentDialog, setShowRentDialog] = useState(false);
    const [showAccessDialog, setShowAccessDialog] = useState(false);
    const [isSkip, setIsSkip] = useState(false);
    const [isEdited, setIsEdited] = useState(false);
    const [user, setUser] = useState<TUserItem>();

    const isSettingsVisible = useMemo(() =>
        !!id && !(currentUserGroups.length === 1 && currentUserGroups.includes(AccountUserGroup.AccountUser)),
    [currentUserGroups, id]);

    const headers: THeaders = [
        {
            title: 'Общие',
            isVisible: true,
            id: 'info'
        },
        {
            tourId: ETourId.usersPageAccessTab,
            title: 'Настройки доступа',
            isVisible: isSettingsVisible,
            id: 'settings'
        }
    ];

    const getActiveTabIndex = () => {
        const tabIndex = Number.parseInt(query.get('tabIndex') ?? '', 10);

        return !Number.isNaN(tabIndex) && tabIndex >= 0 && tabIndex < headers.length && isSettingsVisible ? tabIndex : 0;
    };

    useEffect(() => {
        if (id) {
            (async () => {
                const { message, data } = await getUser(id);

                if (message) {
                    show(EMessageType.error, message);
                }

                if (data) {
                    setUser(data);
                }
            })();
        }
    }, [id, show]);

    const handleSkipClick = () => {
        setIsSkip(true);
        onCancelClick(true);
    };

    const closeRentDialog = () => {
        setShowRentDialog(false);
        setShowAccessDialog(true);
    };

    const closeAccessDialog = () => {
        setShowAccessDialog(false);
        handleSkipClick();
    };

    const closeUserDialog = (isEditable?: boolean) => {
        onCancelClick(isEditable);
        setUser(undefined);
    };

    return (
        <>
            <UserRentDialog
                isOpen={ showRentDialog && !!lastCreateUser && lastCreateUser.success }
                onCancelClick={ closeRentDialog }
                onSkipClick={ handleSkipClick }
            />
            {
                lastCreateUser && lastCreateUser.success && lastCreateUser.data && (
                    <Dialog
                        isOpen={ showAccessDialog && !isSkip }
                        dialogWidth="lg"
                        title="Настройки доступа"
                        isTitleSmall={ true }
                        onCancelClick={ closeAccessDialog }
                    >
                        <DatabaseAccessSettings
                            isRentActive={ true }
                            userId={ lastCreateUser.data.id }
                            userName={ lastCreateUser.data.login }
                            hiddenFilter={ true }
                            skipButton={ <DefaultButton onClick={ handleSkipClick }>Пропустить</DefaultButton> }
                            handleSkip={ handleSkipClick }
                        />
                    </Dialog>
                )
            }
            <Dialog
                title={ user ? `Карточка пользователя: ${ user.login }` : 'Создание пользователя' }
                dialogVerticalAlign="top"
                isOpen={ isOpen }
                isTitleSmall={ true }
                dialogWidth="lg"
                onCancelClick={ () => closeUserDialog(isEdited) }
            >
                <TabsControl
                    variant="standard"
                    activeTabIndex={ getActiveTabIndex() }
                    headers={ headers }
                >
                    <CommonUserInfoView
                        user={ user }
                        isLastAdmin={ isLastAdmin }
                        setShowRentDialog={ setShowRentDialog }
                        setIsEdited={ setIsEdited }
                        onCancelClick={ closeUserDialog }
                    />
                    { !!user && !(currentUserGroups.length === 1 && currentUserGroups.includes(AccountUserGroup.AccountUser)) && (
                        <DatabaseAccessSettings userId={ user.id } isRentActive={ rent1C.success } userName={ user.login } />
                    ) }
                </TabsControl>
            </Dialog>
        </>
    );
};