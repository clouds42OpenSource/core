import { AccountUsersItemDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/getAccountUsers';
import React from 'react';

export type TCommonUserInfoViewProps = {
    setShowRentDialog: React.Dispatch<React.SetStateAction<boolean>>;
    setIsEdited: React.Dispatch<React.SetStateAction<boolean>>;
    onCancelClick: (isEdited: boolean) => void;
    user?: AccountUsersItemDataModel;
    isLastAdmin?: boolean;
};