import ErrorOutlineIcon from '@mui/icons-material/ErrorOutline';
import { Box } from '@mui/system';
import { FETCH_API } from 'app/api/useFetchApi';
import { REDUX_API } from 'app/api/useReduxApi';
import { EMessageType, useAppDispatch, useAppSelector, useFloatMessages } from 'app/hooks';
import { informationBasesList } from 'app/modules/informationBases/reducers/informationBasesList';
import { NumberUtility } from 'app/utils';
import { ContainedButton } from 'app/views/components/controls/Button';
import { Dialog } from 'app/views/components/controls/Dialog';
import { TextOut } from 'app/views/components/TextOut';
import React, { useMemo, useState } from 'react';
import style from './style.module.css';

type TSelectDatabaseInfoItem = {
    count: number;
    size: number;
};

type TSelectDatabaseInfo = {
    all: TSelectDatabaseInfoItem;
    server: TSelectDatabaseInfoItem;
    file: TSelectDatabaseInfoItem;
};

const { deleteDatabases } = FETCH_API.INFORMATION_BASES;
const { getInformationBases } = REDUX_API.INFORMATION_BASES_REDUX;
const { setLoading, setSelectedDatabases } = informationBasesList.actions;

export const DeleteDatabase = () => {
    const { show } = useFloatMessages();

    const dispatch = useAppDispatch();

    const { selectedDatabases, params } = useAppSelector(state => state.InformationBases.informationBasesList);

    const [isOpenDeleteDialog, setIsOpenDeleteDialog] = useState(false);

    const selectDatabaseInfo = useMemo<TSelectDatabaseInfo>(() => {
        const serverDatabase = selectedDatabases.filter(database => !database.isFile && !database.isDbOnDelimiters);
        const fileDatabase = selectedDatabases.filter(database => database.isFile);

        return {
            all: {
                count: selectedDatabases.length,
                size: selectedDatabases.reduce((databaseSize, item) => databaseSize + item.sizeInMb, 0)
            },
            server: {
                count: serverDatabase.length,
                size: serverDatabase.reduce((databaseSize, item) => databaseSize + item.sizeInMb, 0)
            },
            file: {
                count: fileDatabase.length,
                size: fileDatabase.reduce((databaseSize, item) => databaseSize + item.sizeInMb, 0)
            }
        };
    }, [selectedDatabases]);

    const openDeleteDialog = () => {
        setIsOpenDeleteDialog(true);
    };

    const closeDeleteDialog = () => {
        setIsOpenDeleteDialog(false);
    };

    const deleteSelectedDatabases = async () => {
        dispatch(setLoading(true));

        const { success, message } = await deleteDatabases(selectedDatabases.map(database => database.id));

        if (success) {
            dispatch(setSelectedDatabases([]));
            void getInformationBases(dispatch, params);
        } else {
            show(EMessageType.error, message);
        }

        closeDeleteDialog();
        dispatch(setLoading(true));
    };

    return (
        <>
            <Box display="flex" flexDirection="column" gap="8px" mt={ 2 }>
                <Box display="flex" gap="16px">
                    <TextOut fontSize={ 13 } fontWeight={ 600 }>
                        Выбрано баз: { selectDatabaseInfo.all.count }, размер: { NumberUtility.megabytesToClassicFormat('0', selectDatabaseInfo.all.size) }
                    </TextOut>
                    <TextOut fontSize={ 13 } fontWeight={ 600 }>
                        Серверные: { selectDatabaseInfo.server.count }, размер: { NumberUtility.megabytesToClassicFormat('0', selectDatabaseInfo.server.size) }
                    </TextOut>
                    <TextOut fontSize={ 13 } fontWeight={ 600 }>
                        Файловые: { selectDatabaseInfo.file.count }, размер: { NumberUtility.megabytesToClassicFormat('0', selectDatabaseInfo.file.size) }
                    </TextOut>
                </Box>
                <ContainedButton kind="error" className={ style.delete } onClick={ openDeleteDialog }>Удалить</ContainedButton>
            </Box>
            <Dialog
                isOpen={ isOpenDeleteDialog }
                onCancelClick={ closeDeleteDialog }
                dialogVerticalAlign="center"
                dialogWidth="xs"
                title="Удаление информационных баз"
                isTitleSmall={ true }
                buttons={
                    [
                        {
                            content: 'Отмена',
                            kind: 'default',
                            variant: 'text',
                            onClick: closeDeleteDialog
                        },
                        {
                            content: 'Удалить',
                            kind: 'error',
                            variant: 'contained',
                            onClick: deleteSelectedDatabases
                        }
                    ]
                }
            >
                <Box display="flex" flexDirection="column" alignItems="center" justifyContent="center" gap="8px">
                    <ErrorOutlineIcon color="warning" sx={ { width: '4em', height: '4em' } } />
                    <TextOut>
                        Выбранные базы будут перемещены в корзину и недоступны для работы. Скачать резервную копию возможно в течение 30 дней.
                    </TextOut>
                </Box>
            </Dialog>
        </>
    );
};