import { TGetInformationBasesRequest } from 'app/api/endpoints/informationBases/request';
import { TGetAvailability, TInformationBaseItem } from 'app/api/endpoints/informationBases/response';
import { EDatabaseState, EInformationBasesType, EServiceLockReasonType, EPublishState } from 'app/api/endpoints/informationBases/enum';

export const checkStateDatabase = (database: TInformationBaseItem, params?: TGetInformationBasesRequest) =>
    database.state === EDatabaseState.NewItem ||
    database.state === EDatabaseState.DetachingToTomb ||
    database.state === EDatabaseState.DeletingToTomb ||
    (params && params.type === EInformationBasesType.trash);

export const showWebPublishPath = (availability: TGetAvailability, informationBaseItem: TInformationBaseItem) =>
    !!(informationBaseItem.databaseLaunchLink &&
        availability.permissionsForWorkingWithDatabase.hasPermissionForWeb &&
        !(availability.mainServiceStatusInfo.serviceLockReasonType === EServiceLockReasonType.NoDiskSpace) &&
        (informationBaseItem.needShowWebLink || (informationBaseItem.isDbOnDelimiters && informationBaseItem.isDemo)) &&
        (!informationBaseItem.isDbOnDelimiters || (informationBaseItem.isDbOnDelimiters && informationBaseItem.state === EDatabaseState.Ready)) &&
        informationBaseItem.publishState === EPublishState.Published);

export const showHasSupport = (informationBaseItem: TInformationBaseItem) =>
    (informationBaseItem.state === EDatabaseState.ProcessingSupport || informationBaseItem.state === EDatabaseState.Ready) &&
    !informationBaseItem.isDbOnDelimiters &&
    informationBaseItem.hasAcDbSupport;

export const showHasAutoUpdate = (informationBaseItem: TInformationBaseItem) =>
    (informationBaseItem.state === EDatabaseState.ProcessingSupport || informationBaseItem.state === EDatabaseState.Ready) &&
    !informationBaseItem.isDbOnDelimiters &&
    informationBaseItem.hasAcDbSupport;