import React, { useState, useEffect, useCallback } from 'react';

import style from '../style.module.css';

type CountDownTimerProps = {
    hours: number;
    minutes: number;
    seconds: number;
};

export const CountDownTimer = ({ hours, minutes, seconds }: CountDownTimerProps) => {
    const [time, setTime] = useState({ h: hours, m: minutes, s: seconds, over: false });

    const tick = useCallback(() => {
        const { h, m, s } = time;
        if (h === 0 && m === 0 && s === 0) {
            setTime({ ...time, over: true });
        } else if (m === 0 && s === 0) {
            setTime({ h: h - 1, m: 59, s: 59, over: false });
        } else if (s === 0) {
            setTime({ h, m: m - 1, s: 59, over: false });
        } else {
            setTime({ h, m, s: s - 1, over: false });
        }
    }, [time]);

    useEffect(() => {
        if (time.over) return;

        const timerID = setInterval(() => tick(), 1000);
        return () => clearInterval(timerID);
    }, [tick, time]);

    return (
        <span className={ style.timer }>
            { `${ time.h.toString().padStart(2, '0') }:${ time.m.toString().padStart(2, '0') }:${ time.s.toString().padStart(2, '0') }` }
        </span>
    );
};