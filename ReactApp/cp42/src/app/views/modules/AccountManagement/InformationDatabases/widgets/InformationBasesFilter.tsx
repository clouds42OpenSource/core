import { Box, OutlinedInput } from '@mui/material';
import { useAppDispatch, useAppSelector } from 'app/hooks';
import { informationBasesList } from 'app/modules/informationBases/reducers/informationBasesList';
import { SearchByFilterButton } from 'app/views/components/controls/Button';
import { ButtonGroup } from 'app/views/components/controls/ButtonGroup';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import React from 'react';

import { DeleteDatabase, AddBase } from '../feature';
import style from '../style.module.css';

const { setType, setSearch } = informationBasesList.actions;

export const InformationBasesFilter = () => {
    const dispatch = useAppDispatch();

    const {
        informationBasesList: { allDatabasesCount, archievedDatabasesCount, delimeterDatabasesCount, fileDatabasesCount, serverDatabasesCount, deletedDatabases },
        params,
        selectedDatabases
    } = useAppSelector(state => state.InformationBases.informationBasesList);

    const [searchValue, setSearchValue] = React.useState('');

    const onSearchValueChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setSearchValue(e.target.value);
    };

    const onSearch = () => {
        dispatch(setSearch(searchValue));
    };

    const onInputKeyDown = (ev: React.KeyboardEvent<HTMLInputElement>) => {
        if (ev.key.toLowerCase() === 'enter') {
            onSearch();
        }
    };

    const changeType = (type: number) => {
        dispatch(setType(type));
    };

    if (selectedDatabases.length) {
        return (
            <DeleteDatabase />
        );
    }

    return (
        <Box display="flex" gap="10px" flexWrap="wrap" justifyContent="space-between" alignItems="flex-end" marginTop="15px">
            <Box display="flex" gap="10px" alignItems="flex-end" flex="1 1 auto">
                <AddBase />
                <FormAndLabel label="Поиск" fullWidth={ true }>
                    <OutlinedInput
                        fullWidth={ true }
                        placeholder="Поиск базы по названию, шаблону, номеру"
                        value={ searchValue }
                        onChange={ onSearchValueChange }
                        onKeyDown={ onInputKeyDown }
                    />
                </FormAndLabel>
            </Box>
            <Box display="flex" gap="10px" alignItems="flex-end" flexWrap="wrap">
                <ButtonGroup
                    items={
                        [
                            {
                                index: 0,
                                text: `Все базы (${ allDatabasesCount ?? 0 })`
                            },
                            {
                                index: 1,
                                text: `Серверные (${ serverDatabasesCount ?? 0 })`
                            },
                            {
                                index: 2,
                                text: `В архиве (${ archievedDatabasesCount ?? 0 })`
                            },
                            {
                                index: 3,
                                text: `Файловые (${ fileDatabasesCount ?? 0 })`
                            },
                            {
                                index: 4,
                                text: `На разделителях (${ delimeterDatabasesCount ?? 0 })`
                            },
                            {
                                index: 5,
                                text: `Корзина (${ deletedDatabases ?? 0 })`
                            },
                        ]
                    }
                    selectedButtonIndex={ params?.type ?? 0 }
                    onClick={ changeType }
                    label="Быстрые отборы"
                />
                <SearchByFilterButton onClick={ onSearch } className={ style.search }>Поиск</SearchByFilterButton>
            </Box>
        </Box>
    );
};