import { Link } from 'react-router-dom';

import { EServiceLockReasonType } from 'app/api/endpoints/informationBases/enum';
import { TGetAvailability } from 'app/api/endpoints/informationBases/response';
import { AppRoutes } from 'app/AppRoutes';
import { TextOut } from 'app/views/components/TextOut';

import style from '../style.module.css';

const ReasonBlock = ({ mainServiceStatusInfo: { serviceLockReasonType, serviceLockReason } }: TGetAvailability) => {
    if (serviceLockReasonType === EServiceLockReasonType.ServiceNotPaid) {
        return (
            <TextOut inDiv={ true } className={ style.isNotActive1C }>
                <b>Внимание!</b> { serviceLockReason } Пожалуйста,
                <Link to={ AppRoutes.accountManagement.balanceManagement }>&nbsp;пополните баланс</Link>.
            </TextOut>
        );
    }

    if (serviceLockReasonType === EServiceLockReasonType.OverduePromisedPayment) {
        return (
            <TextOut inDiv={ true } className={ style.isNotActive1C }>
                <b>Внимание!</b>&nbsp;{ serviceLockReason }Пожалуйста, погасите обещанный платеж.
            </TextOut>
        );
    }

    if (serviceLockReasonType === EServiceLockReasonType.NoDiskSpace) {
        return (
            <TextOut inDiv={ true } className={ style.isNotActive1C }>
                <b>Внимание!</b>&nbsp;{ serviceLockReason }
            </TextOut>
        );
    }

    return null;
};

export const BlockMessage = (availability: TGetAvailability) => {
    const { mainServiceStatusInfo: { serviceIsLocked }, permissionsForWorkingWithDatabase: { hasPermissionForDisplayPayments }, accountAdminInfo } = availability;

    if (serviceIsLocked) {
        if (hasPermissionForDisplayPayments) {
            return (
                <ReasonBlock { ...availability } />
            );
        }

        return (
            <TextOut inDiv={ true } className={ style.isNotActive1C }>
                На балансе недостаточно средств для работы с сервисами 42Clouds.
                Обратитесь, пожалуйста, к менеджеру вашего аккаунта ({ accountAdminInfo }).
            </TextOut>
        );
    }

    return null;
};