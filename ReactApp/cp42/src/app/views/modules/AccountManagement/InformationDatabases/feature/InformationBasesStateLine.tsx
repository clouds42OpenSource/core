import { useAppSelector } from 'app/hooks';
import React from 'react';

import { Box } from '@mui/material';
import { COLORS } from 'app/utils';
import { TextOut } from 'app/views/components/TextOut';
import { getInfoDbState, CountDownTimer } from '../shared';

type TInformationBasesStateLineProps = {
    id: string;
};

const getTimer = (timeout: number) => {
    const time = new Date(timeout * 1000).toISOString();
    const hours = parseInt(time.slice(11, 13), 10);
    const minutes = parseInt(time.slice(14, 16), 10);
    const seconds = parseInt(time.slice(17, 19), 10);

    return (
        <CountDownTimer
            hours={ hours }
            minutes={ minutes }
            seconds={ seconds }
        />
    );
};

export const InformationBasesStateLine = ({ id }: TInformationBasesStateLineProps) => {
    const { records } = useAppSelector(state => state.InformationBases.informationBasesList.informationBasesList);
    const item = records.find(database => database.id === id);

    if (item) {
        const currentDate = new Date();
        const lastActivityDate = new Date(item.lastActivityDate);
        lastActivityDate.setMinutes(lastActivityDate.getHours() + 1);

        if (item.createStatus) {
            if (item.createStatus.errorCode && lastActivityDate > currentDate) {
                return (
                    <Box style={ { display: 'flex', lineHeight: '16px', color: COLORS.main } }>
                        <img src="img/database-icons/animated-clock.gif" alt="timer" style={ { width: '14px', height: '14px' } } />
                        &nbsp;
                        <TextOut fontSize={ 10 } inheritColor={ true }>База в процессе создания&nbsp;</TextOut>
                    </Box>
                );
            }

            if (!item.createStatus.error) {
                return (
                    <Box style={ { display: 'flex', lineHeight: '16px', color: COLORS.main } }>
                        <img src="img/database-icons/animated-clock.gif" alt="timer" style={ { width: '14px', height: '14px' } } />
                        &nbsp;
                        <TextOut fontSize={ 10 } inheritColor={ true }>
                            { item.createStatus?.state }&nbsp;
                            { item.createStatus.timeout !== -1 && getTimer(item.createStatus.timeout) }
                        </TextOut>
                    </Box>
                );
            }

            return (
                <TextOut fontSize={ 10 } style={ { color: COLORS.warning } } inDiv={ true }>
                    { item.createStatus?.error }
                </TextOut>
            );
        }

        return (
            <TextOut fontSize={ 10 } style={ { color: COLORS.warning } } inDiv={ true }>
                {
                    getInfoDbState(item.state, item.publishState)?.length
                        ? `(${ getInfoDbState(item.state, item.publishState) })`
                        : null
                }
            </TextOut>
        );
    }

    return null;
};