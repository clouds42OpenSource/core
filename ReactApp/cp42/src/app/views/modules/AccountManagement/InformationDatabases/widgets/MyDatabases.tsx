import SettingsIcon from '@mui/icons-material/Settings';
import { Checkbox, IconButton, Link } from '@mui/material';
import { EDatabaseState, EInformationBasesType, EPublishState } from 'app/api/endpoints/informationBases/enum';
import { TInformationBaseItem } from 'app/api/endpoints/informationBases/response';
import { REDUX_API } from 'app/api/useReduxApi';
import { TimerHelper } from 'app/common/helpers/TimerHelper';
import { EMessageType, useAppDispatch, useAppSelector, useDatabaseCard, useFloatMessages } from 'app/hooks';
import { informationBasesList } from 'app/modules/informationBases/reducers/informationBasesList';
import { DateUtility, NumberUtility } from 'app/utils';
import CustomizedSwitches from 'app/views/components/controls/Switch/views/SwitchViews';
import { HomeSvgSelector } from 'app/views/components/svgGenerator/HomeSvgSelector';
import { TextOut } from 'app/views/components/TextOut';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { InformationDatabaseCard } from 'app/views/modules/_common/reusable-modules/InformationDatabaseCard';
import { SelectDataCommonDataModel } from 'app/web/common';
import { useCallback, useEffect, useMemo, useRef } from 'react';

import { InformationBasesStateLine } from '../feature';
import { checkStateDatabase, showHasAutoUpdate, showHasSupport, showWebPublishPath } from '../shared';

const { getInformationBases, getStates, getStatesOnDelimiters } = REDUX_API.INFORMATION_BASES_REDUX;
const { setParams, setSelectedDatabases } = informationBasesList.actions;

export const MyDatabases = () => {
    const { show } = useFloatMessages();

    const dispatch = useAppDispatch();
    const { openDatabaseDialogHandler, closeDatabaseDialogHandler, dialogDatabaseNumber, isOpen } = useDatabaseCard();

    const {
        informationBasesList: { records, pagination },
        availability,
        params,
        selectedDatabases
    } = useAppSelector(state => state.InformationBases.informationBasesList);

    const { permissionsForWorkingWithDatabase, mainServiceStatusInfo } = availability || {};

    const intervalDatabasesRef = useRef<NodeJS.Timeout | number>(0);
    const intervalDatabasesOnDelimitersRef = useRef<NodeJS.Timeout | number>(0);

    useEffect(() => {
        void getInformationBases(dispatch, params);
    }, [dispatch, params]);

    const updateDatabaseState = useCallback(() => {
        const updateList = records.reduce<string[]>((databasesIds, { state, publishState, isDbOnDelimiters, id }) => {
            const isRefreshState = state === EDatabaseState.NewItem || state === EDatabaseState.DeletingToTomb;
            const isRefreshPublishState = publishState === EPublishState.PendingPublication || publishState === EPublishState.PendingUnpublication;

            if ((isRefreshState || isRefreshPublishState) && !isDbOnDelimiters) {
                databasesIds.push(id);
            }

            return databasesIds;
        }, []);

        if (updateList.length) {
            void getStates(dispatch, updateList);
        }
    }, [dispatch, records]);

    useEffect(() => {
        if (records.length) {
            if (!intervalDatabasesRef.current) {
                updateDatabaseState();
            }

            intervalDatabasesRef.current = setInterval(() => {
                updateDatabaseState();
            }, TimerHelper.waitTimeRefreshDatabaseState);
        }

        return () => clearInterval(intervalDatabasesRef.current);
    }, [records.length, updateDatabaseState]);

    const updateDatabaseOnDelimitersState = useCallback(() => {
        const updateList = records.reduce<string[]>((databasesIds, { isDbOnDelimiters, state, id }) => {
            if (state === EDatabaseState.NewItem && isDbOnDelimiters) {
                databasesIds.push(id);
            }

            return databasesIds;
        }, []);

        if (updateList.length) {
            void getStatesOnDelimiters(dispatch, updateList);
        }
    }, [dispatch, records]);

    useEffect(() => {
        if (records.length) {
            if (!intervalDatabasesOnDelimitersRef.current) {
                updateDatabaseOnDelimitersState();
            }

            intervalDatabasesOnDelimitersRef.current = setInterval(() => {
                updateDatabaseOnDelimitersState();
            }, TimerHelper.waitTimeRefreshDatabaseState);
        }

        return () => clearInterval(intervalDatabasesOnDelimitersRef.current);
    }, [records.length, updateDatabaseOnDelimitersState]);

    const selectDatabase = (item: TInformationBaseItem) => {
        if (selectedDatabases.find(database => database.id === item.id)) {
            dispatch(setSelectedDatabases(selectedDatabases.filter(database => database.id !== item.id)));
        } else {
            dispatch(setSelectedDatabases([...selectedDatabases, item]));
        }
    };

    const readyDatabaseList = useMemo(() => records.filter(item => !checkStateDatabase(item, params)), [params, records]);

    const selectAllDatabases = () => {
        if (selectedDatabases.length !== readyDatabaseList.length) {
            dispatch(setSelectedDatabases(records.filter(database => !checkStateDatabase(database, params))));
        } else {
            dispatch(setSelectedDatabases([]));
        }
    };

    const onDataSelect = (args: SelectDataCommonDataModel<unknown>) => {
        clearInterval(intervalDatabasesRef.current);
        clearInterval(intervalDatabasesOnDelimitersRef.current);

        intervalDatabasesRef.current = 0;
        intervalDatabasesOnDelimitersRef.current = 0;

        dispatch(setParams({ field: 'params', value: { ...args, field: args.sortingData?.fieldName, sort: args.sortingData?.sortKind, page: args.pageNumber } }));
    };

    const openDatabase = (item: TInformationBaseItem) => {
        if (item.state !== EDatabaseState.NewItem &&
            item.state !== EDatabaseState.Attaching &&
            item.state !== EDatabaseState.ErrorCreate &&
            item.state !== EDatabaseState.DetachingToTomb
        ) {
            openDatabaseDialogHandler(item.v82Name);
        }

        if (item.state === EDatabaseState.ErrorCreate) {
            show(EMessageType.warning, `При создании базы произошла ошибка: ${ item.createAccountDatabaseComment ?? 'Вы можете удалить эту базу самостоятельно или обратитесь в техподдержку' }`);
        }
    };

    return (
        <>
            <InformationDatabaseCard
                isOpen={ isOpen }
                databaseNumber={ dialogDatabaseNumber }
                onClose={ closeDatabaseDialogHandler }
            />
            <CommonTableWithFilter
                uniqueContextProviderStateId="myDatabasesContextProvider"
                tableProps={ {
                    dataset: [...records],
                    keyFieldName: 'id',
                    headerTitleTable: {
                        text: 'Мои информационные базы'
                    },
                    fieldsView: {
                        id: {
                            style: {
                                display: permissionsForWorkingWithDatabase?.hasPermissionForMultipleActionsWithDb ? '' : 'none',
                            },
                            caption: (
                                <Checkbox
                                    size="small"
                                    checked={ selectedDatabases.length !== 0 && selectedDatabases.length === readyDatabaseList.length }
                                    onChange={ selectAllDatabases }
                                    disabled={ params && params.type === EInformationBasesType.trash }
                                />
                            ),
                            format: (id: string, item) => (
                                <Checkbox
                                    size="small"
                                    checked={ !!selectedDatabases.find(database => database.id === id) }
                                    onChange={ () => selectDatabase(item) }
                                    disabled={ checkStateDatabase(item, params) }
                                />
                            )
                        },
                        templateImageName: {
                            noNeedForMobile: true,
                            caption: '',
                            fieldContentNoWrap: false,
                            fieldWidth: '3%',
                            format: (value: string) => (
                                <HomeSvgSelector width={ 36 } height={ 36 } id={ value } />
                            )
                        },
                        name: {
                            caption: 'Название',
                            fieldContentNoWrap: false,
                            fieldWidth: '30%',
                            isSortable: true,
                            format: (value: string, item) => (
                                <>
                                    <Link onClick={ () => openDatabase(item) }>
                                        { value }
                                    </Link>
                                    { (params && params.type !== EInformationBasesType.trash) && <InformationBasesStateLine id={ item.id } /> }
                                </>
                            )
                        },
                        templateCaption: {
                            caption: 'Конфигурация',
                            fieldContentNoWrap: false,
                            isSortable: true,
                            fieldWidth: '20%'
                        },
                        sizeInMb: {
                            caption: 'Размер',
                            fieldContentNoWrap: false,
                            isSortable: true,
                            fieldWidth: '10%',
                            format: (value: number) => (
                                <TextOut fontSize={ 13 }>{ NumberUtility.megabytesToClassicFormat('-', value) }</TextOut>
                            )
                        },
                        lastActivityDate: {
                            caption: 'Активность',
                            isSortable: true,
                            fieldContentNoWrap: false,
                            wrapCellHeader: true,
                            fieldWidth: '12%',
                            format: (value: Date) => DateUtility.dateToDayMonthYearHourMinute(new Date(value))
                        },
                        databaseLaunchLink: {
                            caption: 'WEB',
                            fieldContentNoWrap: false,
                            isSortable: true,
                            fieldWidth: '8%',
                            format: (value: string, item: TInformationBaseItem) => availability && showWebPublishPath(availability, item) && (
                                <Link onClick={ () => window.open(value, '_blank') }>
                                    Запустить
                                </Link>
                            )
                        },
                        hasSupport: {
                            caption: 'ТиИ',
                            fieldContentNoWrap: false,
                            fieldWidth: '5%',
                            isSortable: true,
                            format: (value: boolean, item: TInformationBaseItem) => showHasSupport(item) && (
                                <CustomizedSwitches
                                    checked={ value }
                                    onChange={ () => { /*empty*/ } }
                                    disabled={ true }
                                />
                            )
                        },
                        hasAutoUpdate: {
                            caption: 'Автообновление',
                            style: {
                                display: permissionsForWorkingWithDatabase?.hasPermissionForDisplayAutoUpdateButton ? '' : 'none'
                            },
                            fieldContentNoWrap: false,
                            fieldWidth: mainServiceStatusInfo?.serviceIsLocked ? '0%' : '7%',
                            isSortable: !mainServiceStatusInfo?.serviceIsLocked,
                            format: (value: boolean, item: TInformationBaseItem) => showHasAutoUpdate(item) && (
                                <CustomizedSwitches
                                    checked={ value }
                                    onChange={ () => { /*empty*/ } }
                                    disabled={ true }
                                />
                            )
                        },
                        v82Name: {
                            caption: '',
                            format: (_, item) => {
                                return (
                                    <IconButton onClick={ () => openDatabase(item) } sx={ { padding: 0 } }>
                                        <SettingsIcon />
                                    </IconButton>
                                );
                            }
                        }
                    },
                    pagination: {
                        ...pagination,
                        totalPages: pagination.pageCount,
                        recordsCount: pagination.totalCount,
                        selectIsVisible: true,
                        selectCount: ['10', '50', '250']
                    },
                } }
                onDataSelect={ onDataSelect }
            />
        </>
    );
};