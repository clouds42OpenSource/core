import { Box } from '@mui/material';
import { AppRoutes } from 'app/AppRoutes';
import { useAppSelector } from 'app/hooks';
import { TooltipPlacementTypes } from 'app/views/components/controls/forms/Tooltips/types';
import { ERefreshId, ETourId } from 'app/views/Layout/ProjectTour/enums';
import { PopperButton } from 'app/views/modules/_common/components/PopperButton';
import React from 'react';
import { Link } from 'react-router-dom';

import style from './style.module.css';

export const AddBase = () => {
    const { availability } = useAppSelector(state => state.InformationBases.informationBasesList);

    if (availability?.allowAddingDatabaseInfo.isAllowedCreateDb && availability.permissionsForWorkingWithDatabase.hasPermissionForCreateAccountDatabase && availability.isMainServiceAllowed) {
        return (
            <Box data-refreshid={ ERefreshId.controlPanelInfo }>
                <div data-tourid={ ETourId.createDatabase }>
                    <PopperButton
                        buttonText={
                            <>
                                <i className="fa fa-plus-circle mr-1" />
                                Добавить базу
                            </>
                        }
                        classNameButton={ style.addButton }
                        isHoverOpen={ true }
                        placement={ TooltipPlacementTypes.bottomStart }
                    >
                        <Box sx={ { width: '250px' } }>
                            <Link
                                className={ style.linkDb }
                                to={ {
                                    pathname: AppRoutes.accountManagement.createAccountDatabase.createDb,
                                    state: {
                                        isVip: availability?.isVipAccount,
                                        isDbOnDelimiters: false
                                    }
                                } }
                            >
                                Создать новую
                            </Link>
                            { availability?.isVipAccount && availability?.accountLocaleName === 'ru-ru' && (
                                <Link
                                    className={ style.linkDb }
                                    to={ {
                                        pathname: AppRoutes.accountManagement.createAccountDatabase.createDb,
                                        state: {
                                            isVip: availability?.isVipAccount,
                                            isDbOnDelimiters: true
                                        }
                                    } }
                                >
                                    Создать новую (Платформа42)
                                </Link>
                            ) }
                            <Link className={ style.linkDb } to={ AppRoutes.accountManagement.createAccountDatabase.loadingDatabase }>
                                Загрузить свою базу 1С
                            </Link>
                        </Box>
                    </PopperButton>
                </div>
            </Box>
        );
    }

    return null;
};