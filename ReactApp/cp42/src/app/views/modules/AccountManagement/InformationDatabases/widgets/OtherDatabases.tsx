import { EDatabaseState } from 'app/api/endpoints/informationBases/enum';
import { InformationDatabaseCard } from 'app/views/modules/_common/reusable-modules/InformationDatabaseCard';
import { useEffect } from 'react';
import { Link } from '@mui/material';
import { TInformationBaseItem } from 'app/api/endpoints/informationBases/response';
import { REDUX_API } from 'app/api/useReduxApi';
import { EMessageType, useAppDispatch, useAppSelector, useDatabaseCard, useFloatMessages } from 'app/hooks';
import { informationBasesList } from 'app/modules/informationBases/reducers/informationBasesList';
import { DateUtility, NumberUtility } from 'app/utils';
import { HomeSvgSelector } from 'app/views/components/svgGenerator/HomeSvgSelector';
import { TextOut } from 'app/views/components/TextOut';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { showWebPublishPath } from '../shared';
import { InformationBasesStateLine } from '../feature';

const { getInformationBases } = REDUX_API.INFORMATION_BASES_REDUX;
const { setParams } = informationBasesList.actions;

export const OtherDatabases = () => {
    const { openDatabaseDialogHandler, closeDatabaseDialogHandler, dialogDatabaseNumber, isOpen } = useDatabaseCard();
    const { show } = useFloatMessages();

    const dispatch = useAppDispatch();

    const {
        otherInformationBasesList: { records: otherRecords, pagination: otherPagination },
        availability,
        otherParams
    } = useAppSelector(state => state.InformationBases.informationBasesList);

    useEffect(() => {
        void getInformationBases(dispatch, otherParams, false);
    }, [dispatch, otherParams]);

    const openDatabase = (item: TInformationBaseItem) => {
        if (item.state !== EDatabaseState.NewItem &&
            item.state !== EDatabaseState.Attaching &&
            item.state !== EDatabaseState.ErrorCreate &&
            item.state !== EDatabaseState.DeletedToTomb
        ) {
            openDatabaseDialogHandler(item.v82Name, true);
        }

        if (item.state === EDatabaseState.ErrorCreate) {
            show(EMessageType.warning, `При создании базы произошла ошибка: ${ item.createAccountDatabaseComment ?? 'Вы можете удалить эту базу самостоятельно или обратитесь в техподдержку' }`);
        }
    };

    return (
        <>
            <InformationDatabaseCard
                isOpen={ isOpen }
                databaseNumber={ dialogDatabaseNumber }
                onClose={ closeDatabaseDialogHandler }
            />
            <CommonTableWithFilter
                uniqueContextProviderStateId="otherDatabasesContextProvider"
                tableProps={ {
                    headerTitleTable: {
                        text: 'Доступные информационные базы других аккаунтов',
                    },
                    dataset: otherRecords,
                    keyFieldName: 'id',
                    fieldsView: {
                        templateImageName: {
                            caption: '',
                            fieldContentNoWrap: false,
                            fieldWidth: '3%',
                            isSortable: false,
                            format: (value: string) => {
                                return <HomeSvgSelector width={ 36 } height={ 36 } id={ value } />;
                            }
                        },
                        name: {
                            caption: 'Название',
                            fieldContentNoWrap: false,
                            fieldWidth: '35%',
                            isSortable: true,
                            format: (value: string, item) => (
                                <>
                                    <Link onClick={ () => openDatabase(item) }>
                                        { value }
                                    </Link>
                                    <InformationBasesStateLine id={ item.id } />
                                </>
                            )
                        },
                        templateCaption: {
                            caption: 'Шаблон',
                            fieldContentNoWrap: false,
                            isSortable: true,
                            fieldWidth: '25%'
                        },
                        sizeInMb: {
                            caption: 'Размер',
                            fieldContentNoWrap: false,
                            isSortable: true,
                            fieldWidth: '15%',
                            format: (value: number) => <TextOut fontSize={ 13 }>{ NumberUtility.megabytesToClassicFormat('-', value) }</TextOut>

                        },

                        lastActivityDate: {
                            caption: 'Активность',
                            isSortable: true,
                            fieldContentNoWrap: false,
                            wrapCellHeader: true,
                            fieldWidth: '15%',
                            format: (value: Date) => DateUtility.dateToDayMonthYearHourMinute(new Date(value))
                        },
                        databaseLaunchLink: {
                            caption: 'WEB',
                            fieldContentNoWrap: false,
                            isSortable: true,
                            fieldWidth: '10%',
                            format: (value: string, item: TInformationBaseItem) => availability && showWebPublishPath(availability, item) && (
                                <Link onClick={ () => window.open(value, '_blank') }>
                                    Запустить
                                </Link>
                            )
                        },
                    },
                    pagination: {
                        ...otherPagination,
                        totalPages: otherPagination.pageCount,
                        recordsCount: otherPagination.totalCount,
                        selectIsVisible: true,
                        selectCount: ['10', '50', '250']
                    }
                } }
                onDataSelect={ args => dispatch(setParams({ field: 'otherParams', value: { ...args, field: args.sortingData?.fieldName, sort: args.sortingData?.sortKind, page: args.pageNumber } })) }
            />
        </>
    );
};