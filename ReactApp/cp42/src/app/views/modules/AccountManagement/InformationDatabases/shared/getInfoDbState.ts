import { EDatabaseState, EPublishState } from 'app/api/endpoints/informationBases/enum';

export const getInfoDbState = (value: EDatabaseState, publish?: EPublishState) => {
    if (publish === EPublishState.PendingPublication && value === EDatabaseState.Ready) {
        return 'Обрабатывается публикация';
    }

    if (publish === EPublishState.PendingUnpublication && value === EDatabaseState.Ready) {
        return 'Обрабатывается снятие публикации';
    }

    switch (value) {
        case EDatabaseState.Undefined:
            return 'База в неизвестном состоянии';
        case EDatabaseState.NewItem:
            return 'База в процессе создания';
        case EDatabaseState.Ready:
            return '';
        case EDatabaseState.Unknown:
            return 'Статус базы не определен';
        case EDatabaseState.Detached:
            return 'База отправлена в хранилище';
        case EDatabaseState.Attaching:
            return 'База в процессе восстановления из архива';
        case EDatabaseState.ErrorNotEnoughSpace:
            return 'База не создана, недостаточно места на диске';
        case EDatabaseState.ErrorCreate:
            return 'База не создана, ошибка создания';
        case EDatabaseState.ErrorDtFormat:
            return 'База не создана, ошибка загрузки из dt';
        case EDatabaseState.ProcessingSupport:
            return 'Идет выполнение регламентных операций.  Это может занять некоторое время.';
        case EDatabaseState.TransferDb:
            return 'База в состоянии переноса';
        case EDatabaseState.TransferArchive:
            return 'Архив базы в состоянии переноса';
        case EDatabaseState.RestoringFromTomb:
            return 'База в очереди на восстановление';
        case EDatabaseState.DeletingToTomb:
            return 'База в очереди на удаление в склеп';
        case EDatabaseState.DetachingToTomb:
            return 'База в очереди на архивацию';
        case EDatabaseState.DeletedToTomb:
            return 'База удалена в склеп';
        case EDatabaseState.DetachedToTomb:
            return 'База в архиве';
        case EDatabaseState.DeletedFromCloud:
            return 'База удалена с облака';
        default:
            return '';
    }
};