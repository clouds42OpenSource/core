import { useEffect } from 'react';

import { REDUX_API } from 'app/api/useReduxApi';
import { EMessageType, useAppDispatch, useAppSelector, useFloatMessages } from 'app/hooks';
import { withHeader } from 'app/views/components/_hoc/withHeader';
import { LoadingBounce } from 'app/views/components/LoadingBounce';

import { ConnectionShortcuts } from './feature';
import { InformationBasesFilter, MyDatabases, OtherDatabases } from './widgets';

const { getAvailabilityInformationBases } = REDUX_API.INFORMATION_BASES_REDUX;

export const InformationDatabases = () => {
    const dispatch = useAppDispatch();
    const { show } = useFloatMessages();

    const {
        loading,
        availability,
        error
    } = useAppSelector(state => state.InformationBases.informationBasesList);

    useEffect(() => {
        if (error) {
            show(EMessageType.error, error);
        }
    }, [show, error]);

    useEffect(() => {
        void getAvailabilityInformationBases(dispatch);
    }, [dispatch]);

    return (
        <>
            { loading && <LoadingBounce /> }
            { availability && <ConnectionShortcuts availability={ availability } /> }
            <InformationBasesFilter />
            <MyDatabases />
            <OtherDatabases />
        </>
    );
};

export const InformationDatabasesPage = withHeader({
    title: 'Информационные базы',
    page: InformationDatabases
});