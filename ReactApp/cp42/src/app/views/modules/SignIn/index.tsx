import { msProvider, toQueryParams } from 'app/api';
import { localStorageHelper } from 'app/common/helpers/LocalStorageHelper';
import { useQuery } from 'app/hooks/useQuery';
import { useEffect } from 'react';

type TAuthorizeOpenId = {
    'openid.return_to': string;
    'openid.mode': string;
    return_to: string;
};

export const SignInView = () => {
    const query = useQuery();

    useEffect(() => {
        localStorageHelper.removeAllUserItems();

        const params: TAuthorizeOpenId = {
            'openid.mode': 'checkid_setup',
            'openid.return_to': `${ window.location.origin }/_`,
            return_to: query.get('return_to') ?? '',
        };

        window.location.href = msProvider(`oid/hs/oid2op${ toQueryParams(params) }`);
    }, []);

    return null;
};