import { Box, Card, Link } from '@mui/material';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import { FETCH_API } from 'app/api/useFetchApi';
import { withHeader } from 'app/views/components/_hoc/withHeader';
import { OutlinedButton } from 'app/views/components/controls/Button';
import { TextOut } from 'app/views/components/TextOut';
import React from 'react';

import css from './style.module.css';

const { useGetActivationStatus, useGetServiceMs, getServicesActivation } = FETCH_API.MANAGING_SERVICE_SUBSCRIPTIONS;
const { REACT_NEURO_SERVICE_ID } = import.meta.env;

const ADVANTAGES = [
    'Полный доступ ко всем ИИ - Ассистентам Нейро42, включая Ассистента 1С',
    'Неограниченный доступ к диалогам на каждого пользователя',
    'Расширенный доступ к загрузке файлов, анализу данных и созданию изображений',
    'Управление доступами сотрудников',
    'Сохранение и учитывание контекста запросов вашего бизнеса'
];

const NeuroPage = () => {
    const { data: msData, refreshData: refreshMsData } = useGetServiceMs(REACT_NEURO_SERVICE_ID);
    const { data: activationStatus, refreshData: refreshStatus } = useGetActivationStatus(REACT_NEURO_SERVICE_ID);

    const msInfo = msData?.rawData.result;

    const onActivate = async () => {
        const { success } = await getServicesActivation({ id: REACT_NEURO_SERVICE_ID });

        if (success && refreshMsData && refreshStatus) {
            refreshMsData();
            refreshStatus();
        }
    };

    const onRedirect = () => {
        window.open('https://neuro.42clouds.com', '_blank');
    };

    if (activationStatus?.rawData === false) {
        return (
            <Box display="flex" flexDirection="column">
                <Box display="flex" gap="10px">
                    <img height={ 120 } style={ { borderRadius: '50%' } } src={ msInfo?.serviceIcon } alt="icon service" loading="lazy" />
                    <Box display="flex" flexDirection="column" gap="10px" justifyContent="space-between">
                        <TextOut>{ msInfo?.serviceShortDescription }</TextOut>
                        <Box display="flex" flexDirection="column" gap="10px">
                            <Link target="_blank" href="https://efsol.ru/solutions/neuro42-ai-assistants-business/">Подробнее</Link>
                            <OutlinedButton onClick={ onActivate } style={ { width: 'fit-content' } } kind="success">Попробовать бесплатно</OutlinedButton>
                        </Box>
                    </Box>
                </Box>
            </Box>
        );
    }

    return (
        <>
            <Box display="flex" gap="10px">
                <img height={ 120 } style={ { borderRadius: '50%' } } src={ msInfo?.serviceIcon } alt="icon service" loading="lazy" />
                { msInfo?.serviceDescription && (
                    <div className={ css.description } dangerouslySetInnerHTML={ { __html: msInfo.serviceDescription } } />

                ) }
            </Box>
            <Box display="flex" flexDirection="column" gap="10px" alignItems="center">
                <Card variant="outlined" sx={ { paddingBlock: '10px', paddingInline: '20px', width: 'fit-content' } }>
                    <Box alignItems="center" display="flex" flexDirection="column" gap="5px">
                        <TextOut fontSize={ 20 } fontWeight={ 700 }>Тариф «Стандарт»</TextOut>
                        <TextOut fontSize={ 18 } fontWeight={ 700 }>Месячная подписка: 1999₽</TextOut>
                    </Box>
                    <Box display="flex" flexDirection="column" gap="15px" marginTop="20px">
                        { ADVANTAGES.map(advantage => (
                            <Box display="flex" gap="5px" alignItems="flex-end">
                                <CheckCircleIcon fontSize="small" color="primary" />
                                <TextOut fontSize={ 14 }>{ advantage }</TextOut>
                            </Box>
                        )) }
                    </Box>
                </Card>
                <OutlinedButton onClick={ onRedirect } style={ { width: 'fit-content' } } kind="success">Перейти на сайт Нейро42</OutlinedButton>
            </Box>
        </>
    );
};

export const Neuro = withHeader({
    title: 'Нейро42',
    page: NeuroPage
});