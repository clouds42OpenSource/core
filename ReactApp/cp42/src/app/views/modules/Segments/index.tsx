import { withHeader } from 'app/views/components/_hoc/withHeader';
import { TabsControl } from 'app/views/components/controls/TabsControl';
import { THeaders } from 'app/views/components/controls/TabsControl/views/TabView';
import { SegmentsTabEnumType } from 'app/views/modules/Segments/types';
import {
    ArrServersTabView,
    BackupStorageTabView,
    ContentServersTabView,
    EnterpriseServersTabView,
    FileStoragesTabView,
    GatewayTerminalTabView,
    HostingTabView,
    PlatformVersionTabView,
    PublishNodesTabView,
    SegmentsTabView,
    SqlServersTabView,
    TerminalFarmsTabView,
    TerminalServerTabView,
    Link42View
} from 'app/views/modules/Segments/views';
import { Component } from 'react';
import './common/styles.module.css';

type SegmentsState = {
    /**
     * Index активного таба
     */
    activeTabIndex: SegmentsTabEnumType;
};

class SegmentsClass extends Component<NonNullable<unknown>, SegmentsState> {
    private readonly headers: THeaders = [
        {
            title: 'Сегменты',
            isVisible: true
        },
        {
            title: 'Фермы ТС',
            isVisible: true
        },
        {
            title: 'Сервера 1С',
            isVisible: true
        },
        {
            title: 'Ноды публикаций',
            isVisible: true
        },
        {
            title: 'Сайты публикаций',
            isVisible: true
        },
        {
            title: 'Файл. хранилища',
            isVisible: true
        },
        {
            title: 'SQL',
            isVisible: true
        },
        {
            title: 'Хранилище бекапов',
            isVisible: true
        },
        {
            title: 'Терм. шлюзы',
            isVisible: true
        },
        {
            title: 'Терм. сервера',
            isVisible: true
        },
        {
            title: 'Платформа 1С',
            isVisible: true
        },
        {
            title: 'Хостинг',
            isVisible: true
        },
        {
            title: 'Ноды АО',
            isVisible: true
        },
        {
            title: 'Линк42',
            isVisible: true,
        }
    ];

    public constructor(props: NonNullable<unknown>) {
        super(props);

        this.state = {
            activeTabIndex: SegmentsTabEnumType.Segments
        };

        this.onActiveTabIndexChanged = this.onActiveTabIndexChanged.bind(this);
    }

    private onActiveTabIndexChanged(tabIndex: SegmentsTabEnumType) {
        this.setState({ activeTabIndex: tabIndex });
    }

    public render() {
        const { activeTabIndex } = this.state;

        return (
            <TabsControl
                noUnderline={ true }
                sxProps={ {
                    bgcolor: 'background.paper',
                    '@media (min-width: 780px)': {
                        '& .MuiTabs-flexContainer': {
                            flexWrap: 'wrap'
                        }
                    }
                } }
                activeTabIndex={ activeTabIndex }
                onActiveTabIndexChanged={ this.onActiveTabIndexChanged }
                headers={ this.headers }
            >
                <SegmentsTabView />
                <TerminalFarmsTabView isCanLoad={ activeTabIndex === SegmentsTabEnumType.TsFerms } />
                <EnterpriseServersTabView isCanLoad={ activeTabIndex === SegmentsTabEnumType.Servers1C } />
                <PublishNodesTabView isCanLoad={ activeTabIndex === SegmentsTabEnumType.PublishNodes } />
                <ContentServersTabView isCanLoad={ activeTabIndex === SegmentsTabEnumType.PublishSites } />
                <FileStoragesTabView isCanLoad={ activeTabIndex === SegmentsTabEnumType.FileStorage } />
                <SqlServersTabView isCanLoad={ activeTabIndex === SegmentsTabEnumType.Sql } />
                <BackupStorageTabView isCanLoad={ activeTabIndex === SegmentsTabEnumType.BackupStorage } />
                <GatewayTerminalTabView isCanLoad={ activeTabIndex === SegmentsTabEnumType.TerminalGateway } />
                <TerminalServerTabView isCanLoad={ activeTabIndex === SegmentsTabEnumType.TerminalServer } />
                <PlatformVersionTabView isCanLoad={ activeTabIndex === SegmentsTabEnumType.Platform1C } />
                <HostingTabView isCanLoad={ activeTabIndex === SegmentsTabEnumType.Hosting } />
                <ArrServersTabView isCanLoad={ activeTabIndex === SegmentsTabEnumType.ServersARR } />
                { activeTabIndex === SegmentsTabEnumType.Link42 && <Link42View /> }
            </TabsControl>
        );
    }
}

export const SegmentsView = withHeader({
    page: SegmentsClass,
    title: 'Сегменты'
});