import { Box } from '@mui/material';
import { OutlinedButton } from 'app/views/components/controls/Button';
import { TextBoxForm } from 'app/views/components/controls/forms';
import { Component } from 'react';
import commonCss from '../../styles.module.css';

type CommonSearchSegmentState = {
    connectionAddress?: string;
    description?: string;
    name?: string;
};

type CommonSearchSegmentProps = {
    isNameVisible?: boolean;
    isDescriptionVisible?: boolean;
    isConnectionAddressVisible?: boolean;
    connectionAddressLabel?: string;
    onClickToSearch: () => void;
};

export class CommonSearchSegmentView extends Component<CommonSearchSegmentProps, CommonSearchSegmentState> {
    public constructor(props: CommonSearchSegmentProps) {
        super(props);

        this.state = {
            connectionAddress: '',
            description: '',
            name: ''
        };

        this.onValueChange = this.onValueChange.bind(this);
    }

    private onValueChange(formName: string, newValue: string) {
        this.setState(prevState => ({
            ...prevState,
            [formName]: newValue
        }));
    }

    public render() {
        const { isConnectionAddressVisible, isDescriptionVisible, isNameVisible, connectionAddressLabel } = this.props;

        return (
            <Box display="grid" gridTemplateColumns="repeat(auto-fit, minmax(250px, 1fr))" gap="12px" marginBottom="16px">
                {
                    isNameVisible || isNameVisible === undefined
                        ? (
                            <TextBoxForm
                                formName="name"
                                label="Название"
                                placeholder="Введите название"
                                value={ this.state.name }
                                onValueChange={ this.onValueChange }
                            />
                        )
                        : null
                }
                {
                    isDescriptionVisible || isDescriptionVisible === undefined
                        ? (
                            <TextBoxForm
                                formName="description"
                                label="Описание"
                                placeholder="Введите описание"
                                value={ this.state.description }
                                onValueChange={ this.onValueChange }
                            />
                        )
                        : null
                }
                {
                    isConnectionAddressVisible || isConnectionAddressVisible === undefined
                        ? (
                            <TextBoxForm
                                formName="connectionAddress"
                                label={ connectionAddressLabel || 'Адрес подключения' }
                                placeholder="Введите адрес"
                                value={ this.state.connectionAddress }
                                onValueChange={ this.onValueChange }
                            />
                        )
                        : null
                }
                <OutlinedButton
                    kind="success"
                    className={ commonCss['search-button'] }
                    onClick={ this.props.onClickToSearch }
                >
                    <i className="fa fa-search mr-1" />
                    Поиск
                </OutlinedButton>
            </Box>
        );
    }
}