import { TabsControl } from 'app/views/components/controls/TabsControl';
import { TabProps } from 'app/views/components/controls/TabsControl/views/TabView';
import React from 'react';

/**
 * Функция на добавление табов
 * @param elements Массив JSX элементов
 * @param tabProps Свойства компонента таба
 * @returns JSX элементы с табом
 */
export function setTabsControl(elements: React.JSX.Element[], tabProps: TabProps) {
    return (
        <TabsControl { ...tabProps }>
            { elements }
        </TabsControl>
    );
}