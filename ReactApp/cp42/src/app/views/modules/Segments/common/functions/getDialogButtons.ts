import { PageEnumType } from 'app/common/enums';
import { DialogButtonType } from 'app/views/components/controls/Dialog/views/DialogActionsView/Index';

type GetDialogButtonsParams = {
    /**
     * Тип страницы
     */
    pageType: PageEnumType;

    /**
     * Флаг для добавления кнопки удаления
     */
    isAddDeleteButton?: boolean;

    /**
     * Функция на удаление записи
     */
    onClickToDelete?: () => void;

    /**
     * Функция на закрытие модального окошка
     */
    onClickToCloseModal: () => void;

    /**
     * Функция на сохранение изменений или создания записи
     */
    submitChanges: () => void;
};

/**
 * Получить массив обьектов кнопок для диалогового окошка
 * @param params Параметры функции для получения массива обьектов кнопок
 * @returns Массив обьектов кнопок
 */
export function getDialogButtons(params: GetDialogButtonsParams): DialogButtonType {
    const { onClickToCloseModal, submitChanges, pageType, isAddDeleteButton, onClickToDelete } = params;
    const buttons = [
        {
            kind: 'default',
            content: 'Закрыть',
            fontSize: 12,
            variant: 'outlined',
            onClick: onClickToCloseModal
        },
        {
            kind: 'success',
            content: `${ pageType === PageEnumType.Edit ? 'Сохранить' : 'Создать' }`,
            fontSize: 12,
            onClick: submitChanges
        }
    ];

    if (isAddDeleteButton && onClickToDelete && pageType === PageEnumType.Edit) {
        buttons.splice(1, 0, {
            kind: 'error',
            content: 'Удалить',
            fontSize: 12,
            onClick: onClickToDelete
        });
    }

    return buttons as DialogButtonType;
}