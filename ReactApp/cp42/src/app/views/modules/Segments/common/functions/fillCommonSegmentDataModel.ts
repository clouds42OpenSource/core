import { CommonSegmentDataForm } from 'app/views/modules/Segments/types';
import { CommonSegmentDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';

/**
 * Функция заполнения общей модели сегмента
 * @param formFields Модель Redux формы для общей модели сегмента
 * @returns Заполненный обьект общей модели сегмента
 */
export function fillCommonSegmentDataModel(formFields: CommonSegmentDataForm): CommonSegmentDataModel {
    return {
        usingOutdatedWindows: formFields.usingOutdatedWindows,
        connectionAddress: formFields.connectionAddress,
        description: formFields.description,
        name: formFields.name,
        id: ''
    };
}