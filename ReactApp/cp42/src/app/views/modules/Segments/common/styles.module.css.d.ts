declare const styles: {
  readonly 'search-button': string;
  readonly 'delete-link': string;
  readonly 'search-button-margin': string;
  readonly 'custom-table': string;
  readonly 'button-text': string;
  readonly 'checkbox': string;
};

export = styles;