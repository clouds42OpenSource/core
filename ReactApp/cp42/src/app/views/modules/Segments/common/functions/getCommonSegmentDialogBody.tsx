import { Box } from '@mui/material';
import { IErrorsType } from 'app/common/interfaces';
import { TextBoxForm, CheckBoxForm } from 'app/views/components/controls/forms';
import { CommonSegmentDataForm } from 'app/views/modules/Segments/types';
import { ShowErrorMessage } from 'app/views/modules/_common/components/ShowErrorMessage';

import styles from '../styles.module.css';
/**
 * Получить общий JSX элемент для создания/редактирования общей модели сегмента
 * @param formFields Модель Redux формы для общей модели сегмента
 * @param errors Интерфейс для валидации ошибок
 * @param onValueChange Функция на изменение в полях редакс формы
 * @returns JSX Element
 */
export function getCommonSegmentDialogBody(formFields: CommonSegmentDataForm, errors: IErrorsType, onValueChange: (formName: string, newValue: string | boolean) => void) {
    return (
        <Box display="flex" flexDirection="column" gap="12px">
            <Box>
                <TextBoxForm
                    formName="name"
                    label="Название"
                    placeholder="Введите название"
                    value={ formFields.name }
                    onValueChange={ onValueChange }
                />
                <ShowErrorMessage errors={ errors } formName="name" />
            </Box>
            <Box>
                <TextBoxForm
                    formName="connectionAddress"
                    label="Адрес подключения"
                    placeholder="Введите адрес"
                    value={ formFields.connectionAddress }
                    onValueChange={ onValueChange }
                />
                <ShowErrorMessage errors={ errors } formName="connectionAddress" />
            </Box>
            <Box>
                <TextBoxForm
                    type="textarea"
                    formName="description"
                    label="Описание"
                    placeholder="Введите описание"
                    value={ formFields.description }
                    onValueChange={ onValueChange }
                />
                <ShowErrorMessage errors={ errors } formName="description" />
            </Box>
            { formFields.usingOutdatedWindows !== undefined && (
                <CheckBoxForm
                    label="Используется версия ниже Windows Server 2016"
                    formName="usingOutdatedWindows"
                    isChecked={ formFields.usingOutdatedWindows }
                    className={ styles.checkbox }
                    onValueChange={ (formName, newValue) => onValueChange(formName, newValue) }
                />
            ) }
        </Box>
    );
}