export enum SegmentsTabEnumType {
    Segments = 0,
    TsFerms,
    Servers1C,
    PublishNodes,
    PublishSites,
    FileStorage,
    Sql,
    BackupStorage,
    TerminalGateway,
    TerminalServer,
    Platform1C,
    Hosting,
    ServersARR,
    Link42
}