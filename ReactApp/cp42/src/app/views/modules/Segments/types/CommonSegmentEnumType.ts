/**
 * Enum тип для общего значения
 */
export enum CommonSegmentEnumType {
    /**
     * Размер подгрузки элементов по умолчанию на страницу
     */
    DefaultPageSize = 15
}