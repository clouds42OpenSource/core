/**
 * Enum максимальной длинны полей общей модели сегмента
 */
export enum CommonSegmentLengthEnumType {
    /**
     * Максимальная длина названия
     */
    Name = 50,

    /**
     * Максимальная длина описания
     */
    Description = 250,

    /**
     * Максимальная длина адреса соединения
     */
    ConnectionAddress = 250
}