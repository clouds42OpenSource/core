export type CommonSegmentDataForm = {
    connectionAddress: string;
    description: string;
    name: string;
    usingOutdatedWindows?: boolean;
};