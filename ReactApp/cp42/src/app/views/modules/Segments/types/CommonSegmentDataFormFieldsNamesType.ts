import { CommonSegmentDataForm } from 'app/views/modules/Segments/types';

export type CommonSegmentDataFormFieldsNamesType = keyof CommonSegmentDataForm;