import { PageEnumType } from 'app/common/enums';
import { checkOnValidField, checkValidOfStringField } from 'app/common/functions';
import { IErrorsType } from 'app/common/interfaces';
import { TabTerminalServerProcessId } from 'app/modules/segments/TabTerminalServer';
import { CreateCloudTerminalServerThunkParams } from 'app/modules/segments/TabTerminalServer/store/reducer/createCloudTerminalServerReducer/params';
import { EditCloudTerminalServerThunkParams } from 'app/modules/segments/TabTerminalServer/store/reducer/editCloudTerminalServerReducer/params';
import { GetCloudTerminalServerThunkParams } from 'app/modules/segments/TabTerminalServer/store/reducer/getCloudTerminalServerReducer/params';
import { CreateCloudTerminalServerThunk, EditCloudTerminalServerThunk, GetCloudTerminalServerThunk } from 'app/modules/segments/TabTerminalServer/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { Dialog } from 'app/views/components/controls/Dialog';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { fillCommonSegmentDataModel, getCommonSegmentDialogBody, getDialogButtons } from 'app/views/modules/Segments/common/functions';
import { CommonSegmentDataForm, CommonSegmentDataFormFieldsNamesType, CommonSegmentLengthEnumType } from 'app/views/modules/Segments/types';
import { WatchReducerProcessActionState } from 'app/views/modules/_common/components/WatchReducerProcessActionState';
import { CommonSegmentDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import { IReducerProcessActionInfo } from 'core/redux/interfaces';
import React, { Component } from 'react';
import { connect } from 'react-redux';

type OwnProps = ReduxFormProps<CommonSegmentDataForm>;

type TerminalServerModalProps = {
    /**
     * Поле для открытия модального окошка терминальной фирмы ТС
     */
    isOpen: boolean;

    /**
     * ID терминального сервера
     */
    terminalServerId?: string;

    /**
     * Функция на закрытие модального окошка
     */
    onClickToCloseModal: () => void;
};

type TerminalServerModalState = {
    /**
     * Обьект Ошибок для валидации
     */
    errors: IErrorsType;

    /**
     * Тип страницы для динамического компонента редактирования/создания
     */
    pageType: PageEnumType;
};

type StateProps = {
    /**
     * Модель терминального сервера
     */
    cloudTerminalServer: CommonSegmentDataModel;
};

type DispatchProps = {
    dispatchCreateCloudTerminalServerThunk: (args: CreateCloudTerminalServerThunkParams) => void;
    dispatchGetCloudTerminalServerThunk: (args: GetCloudTerminalServerThunkParams) => void;
    dispatchEditCloudTerminalServerThunk: (args: EditCloudTerminalServerThunkParams) => void;
};

type AllProps = TerminalServerModalProps & OwnProps & StateProps & DispatchProps & FloatMessageProps;

class TerminalServerModalClass extends Component<AllProps, TerminalServerModalState> {
    public constructor(props: AllProps) {
        super(props);

        this.state = {
            errors: {},
            pageType: this.props.terminalServerId ? PageEnumType.Edit : PageEnumType.Create
        };

        this.getDialogBody = this.getDialogBody.bind(this);
        this.onValueChange = this.onValueChange.bind(this);
        this.submitChanges = this.submitChanges.bind(this);
        this.createTerminalServer = this.createTerminalServer.bind(this);
        this.editTerminalServer = this.editTerminalServer.bind(this);
        this.isTerminalServerFormValid = this.isTerminalServerFormValid.bind(this);

        this.onProcessActionStateChanged = this.onProcessActionStateChanged.bind(this);
        this.onProcessGetTerminalServer = this.onProcessGetTerminalServer.bind(this);
        this.successGetTerminalServerState = this.successGetTerminalServerState.bind(this);
        this.errorGetTerminalServerState = this.errorGetTerminalServerState.bind(this);

        this.props.reduxForm.setInitializeFormDataAction(this.defaultInitDataReduxForm);
    }

    public componentDidMount() {
        if (this.props.terminalServerId) {
            this.props.dispatchGetCloudTerminalServerThunk({
                terminalServerId: this.props.terminalServerId,
                force: true
            });
        }
    }

    private onProcessActionStateChanged(processId: string, actionState: IReducerProcessActionInfo) {
        this.onProcessGetTerminalServer(processId, actionState);
    }

    private onProcessGetTerminalServer(processId: string, actionState: IReducerProcessActionInfo) {
        if (processId !== TabTerminalServerProcessId.GetTerminalServer) return;
        this.successGetTerminalServerState(actionState);
        this.errorGetTerminalServerState(actionState);
    }

    private onValueChange<TValue, TForm extends string = CommonSegmentDataFormFieldsNamesType>(formName: TForm, newValue: TValue) {
        this.props.reduxForm.updateReduxFormFields({
            [formName]: newValue
        });

        const { errors } = this.state;

        if (typeof newValue === 'string') {
            switch (formName) {
                case 'name':
                    errors[formName] = checkValidOfStringField(newValue, 'Заполните название', CommonSegmentLengthEnumType.Name);
                    break;
                case 'description':
                    errors[formName] = checkValidOfStringField(newValue, undefined, CommonSegmentLengthEnumType.Description);
                    break;
                case 'connectionAddress':
                    errors[formName] = checkValidOfStringField(newValue, 'Заполните адрес', CommonSegmentLengthEnumType.ConnectionAddress);
                    break;
                default:
                    break;
            }

            this.setState({ errors });
        }
    }

    private getDialogBody() {
        const formFields = this.props.reduxForm.getReduxFormFields(false);
        return getCommonSegmentDialogBody(formFields, this.state.errors, this.onValueChange);
    }

    private successGetTerminalServerState(actionState: IReducerProcessActionInfo) {
        if (!actionState.isInSuccessState) return;
        this.props.reduxForm.updateReduxFormFields({
            ...this.props.cloudTerminalServer
        });
    }

    private errorGetTerminalServerState(actionState: IReducerProcessActionInfo, error?: Error) {
        if (!actionState.isInErrorState) return;
        this.props.floatMessage.show(MessageType.Error, `Ошибка при получении терминального сервера: ${ error?.message }`);
    }

    private submitChanges() {
        if (!this.isTerminalServerFormValid()) return;
        this.createTerminalServer();
        this.editTerminalServer();
    }

    private createTerminalServer() {
        if (this.state.pageType !== PageEnumType.Create) return;
        this.props.dispatchCreateCloudTerminalServerThunk({
            ...fillCommonSegmentDataModel(this.props.reduxForm.getReduxFormFields(false)),
            force: true
        });
    }

    private editTerminalServer() {
        if (this.state.pageType !== PageEnumType.Edit) return;
        this.props.dispatchEditCloudTerminalServerThunk({
            ...fillCommonSegmentDataModel(this.props.reduxForm.getReduxFormFields(false)),
            id: this.props.terminalServerId!,
            force: true
        });
    }

    private defaultInitDataReduxForm(): CommonSegmentDataForm | undefined {
        return {
            connectionAddress: '',
            description: '',
            name: ''
        };
    }

    private isTerminalServerFormValid() {
        const formFields = this.props.reduxForm.getReduxFormFields(false);
        const errors: IErrorsType = {};
        const arrayResults: boolean[] = [];

        arrayResults.push(checkOnValidField(errors, 'name', formFields.name, 'Заполните название', CommonSegmentLengthEnumType.Name));
        arrayResults.push(checkOnValidField(errors, 'description', formFields.description, undefined, CommonSegmentLengthEnumType.Description));
        arrayResults.push(checkOnValidField(errors, 'connectionAddress', formFields.connectionAddress, 'Заполните адрес', CommonSegmentLengthEnumType.ConnectionAddress));

        if (arrayResults.some(item => !item)) {
            this.props.floatMessage.show(MessageType.Warning, 'Форма к отправке не готова. Пожалуйста, заполните все поля.');
            this.setState({ errors });
            return false;
        }

        return true;
    }

    public render() {
        const { isOpen } = this.props;
        const isEditTypePage = this.state.pageType === PageEnumType.Edit;
        const titleText = isEditTypePage ? 'Редактирование' : 'Создание';

        return (
            <>
                <Dialog
                    disableBackdropClick={ true }
                    title={ `${ titleText } терминального сервера` }
                    isOpen={ isOpen }
                    dialogWidth="sm"
                    isTitleSmall={ true }
                    titleTextAlign="left"
                    titleFontSize={ 14 }
                    dialogVerticalAlign="center"
                    onCancelClick={ this.props.onClickToCloseModal }
                    buttons={ getDialogButtons({
                        pageType: this.state.pageType,
                        onClickToCloseModal: this.props.onClickToCloseModal,
                        submitChanges: this.submitChanges
                    }) }
                >
                    {
                        this.getDialogBody()
                    }
                </Dialog>
                <WatchReducerProcessActionState
                    watch={ [{
                        reducerStateName: 'TabTerminalServerState',
                        processIds: [
                            TabTerminalServerProcessId.GetTerminalServer
                        ]
                    }] }
                    onProcessActionStateChanged={ this.onProcessActionStateChanged }
                />
            </>
        );
    }
}

const TerminalServerModalConnected = connect<StateProps, DispatchProps, OwnProps, AppReduxStoreState>(
    state => {
        const tabTerminalServerState = state.TabTerminalServerState;
        const { cloudTerminalServer } = tabTerminalServerState;

        return {
            cloudTerminalServer
        };
    },
    {
        dispatchCreateCloudTerminalServerThunk: CreateCloudTerminalServerThunk.invoke,
        dispatchGetCloudTerminalServerThunk: GetCloudTerminalServerThunk.invoke,
        dispatchEditCloudTerminalServerThunk: EditCloudTerminalServerThunk.invoke
    }
)(TerminalServerModalClass);

export const TerminalServerModalView = withReduxForm(withFloatMessages(TerminalServerModalConnected),
    {
        reduxFormName: 'Terminal_Server_Modal_Form',
        resetOnUnmount: true
    });