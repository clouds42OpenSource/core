/**
 * Enum максимальной длинны полей sql сервера
 */
export enum SqlServersTabLengthEnumType {
    /**
     * Максимальная длина названия sql сервера
     */
    Name = 50,

    /**
     * Максимальная длина описания sql сервера
     */
    Description = 250,

    /**
     * Максимальная длина адреса соединения sql сервера
     */
    ConnectionAddress = 250
}