import { DualListBoxForm } from 'app/views/components/controls/forms/DualListBoxForm';
import { DualListItem, DualListStateListTypes } from 'app/views/components/controls/forms/DualListBoxForm/views/types';
import { SegmentModalDataForm } from 'app/views/modules/Segments/views/SegmentsTabView/types';
import { MigrationSegmentDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabSegmentsApiProxy';
import { Component } from 'react';

type AvailableForMigrationTabProps = {
    /**
     * Модель Redux формы для карточки сегмента
     */
    formFields: SegmentModalDataForm;

    /**
     * Функция на обновление Redux form'ы
     */
    updateReduxFormFields: (payload: Partial<SegmentModalDataForm>) => void;
};

/**
 * Компонент вкладки "Сегменты доступные для миграции"
 */
export class AvailableForMigrationTabView extends Component<AvailableForMigrationTabProps> {
    public constructor(props: AvailableForMigrationTabProps) {
        super(props);

        this.onValueChange = this.onValueChange.bind(this);
    }

    /**
     * Функция на изменение DualListBoxForm
     * @param _fieldName Название формы
     * @param newLeftValue Новые значения левой колонки
     * @param newRightValue Новые значения правой колонки
     * @returns Логическое значение для стейта DualListBoxForm
     */
    private onValueChange(_fieldName: string, newLeftValue: DualListItem<string>[], newRightValue: DualListItem<string>[]) {
        this.props.updateReduxFormFields({
            availableMigrationSegment: newRightValue.map<MigrationSegmentDataModel>(item => ({
                key: item.key,
                value: item.label,
                isAvailable: true,
                isSimilar: item.isSimilar!
            })),
            segmentsForMigration: newLeftValue.map<MigrationSegmentDataModel>(item => ({
                key: item.key,
                value: item.label,
                isAvailable: true,
                isSimilar: item.isSimilar!
            }))
        });

        return true;
    }

    /**
     * Получить элементы двойного выпадающего списка
     * @param items список элементов модели
     */
    private getDualListItems(items: MigrationSegmentDataModel[]): Array<DualListItem<string>> {
        if (items) {
            return items.map(item => ({
                label: item.value,
                key: item.key,
                isSimilar: item.isSimilar
            }));
        }

        return [];
    }

    public render() {
        const { formFields: { availableMigrationSegment, segmentsForMigration } } = this.props;

        return (<DualListBoxForm
            allowSelectAll={ true }
            mainList={ DualListStateListTypes.rightList }
            leftListLabel="Похожие сегменты для выбора:"
            rightListLabel="Доступные сегменты:"
            formName="availableForMigration"
            leftListItems={ this.getDualListItems(segmentsForMigration) }
            rightListItems={ this.getDualListItems(availableMigrationSegment) }
            isFormDisabled={ false }
            onValueChange={ this.onValueChange }
        />);
    }
}