import ErrorOutlineIcon from '@mui/icons-material/ErrorOutline';
import { Box } from '@mui/material';
import { SortingKind } from 'app/common/enums';
import { TabHostingProcessId } from 'app/modules/segments/TabHosting';
import { DeleteCoreHostingThunkParams } from 'app/modules/segments/TabHosting/store/reducer/deleteCoreHostingReducer/params';
import { GetCoreHostingsThunkParams } from 'app/modules/segments/TabHosting/store/reducer/getCoreHostingsReducer/params';
import { DeleteCoreHostingThunk, GetCoreHostingsThunk } from 'app/modules/segments/TabHosting/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { SuccessButton, TextLinkButton } from 'app/views/components/controls/Button';
import { Dialog } from 'app/views/components/controls/Dialog';
import { TextOut } from 'app/views/components/TextOut';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { WatchReducerProcessActionState } from 'app/views/modules/_common/components/WatchReducerProcessActionState';
import { CommonSegmentEnumType } from 'app/views/modules/Segments/types';
import { HostingModalView } from 'app/views/modules/Segments/views/HostingTabView/views/HostingModalView';
import { SearchHostingsView } from 'app/views/modules/Segments/views/HostingTabView/views/SearchHostingsView';
import { SelectDataMetadataResponseDto, SortingDataModel } from 'app/web/common';
import { SelectDataCommonDataModel } from 'app/web/common/data-models';
import { CoreHostingDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabHostinApiProxy';
import { IReducerProcessActionInfo } from 'core/redux/interfaces';
import { Component, createRef } from 'react';
import { connect } from 'react-redux';
import commonCss from '../../common/styles.module.css';

type HostingTabState = {
    /**
     * Поле обозначающая что первая подгрузка данных была сделана
     */
    isFirstTimeLoaded: boolean;

    /**
     * Поле для открытия модального окошка
     */
    isOpen: boolean;

    /**
     * ID хостинга облака
     */
    coreHostingId?: string;
    /**
     * Состояние модального окна на удаление хостингов
     */
    deleteIsOpen: boolean;
    orderBy?: string;
};

type HostingTabProps = {
    /**
     * Поле для подгрузки данных
     */
    isCanLoad: boolean;
};

type StateProps = {
    /**
     * Массив хостинга
     */
    cloudHostings: SelectDataMetadataResponseDto<CoreHostingDataModel>;

    /**
     * Если true, то массив хостинга получены
     */
    hasCloudHostingsReceived: boolean;
};

type DispatchProps = {
    dispatchGetCoreHostingsThunk: (args: GetCoreHostingsThunkParams) => void;
    dispatchDeleteCoreHostingThunk: (args: DeleteCoreHostingThunkParams) => void;
};

type AllProps = HostingTabProps & StateProps & DispatchProps & FloatMessageProps;

class HostingTabClass extends Component<AllProps, HostingTabState> {
    private _commonTableRef = createRef<CommonTableWithFilter<CoreHostingDataModel, undefined>>();

    private _searchHostingsRef = createRef<SearchHostingsView>();

    public constructor(props: AllProps) {
        super(props);

        this.state = {
            isOpen: false,
            isFirstTimeLoaded: false,
            deleteIsOpen: false,
            orderBy: ''
        };

        this.onClickToOpenModal = this.onClickToOpenModal.bind(this);
        this.onClickToCloseModal = this.onClickToCloseModal.bind(this);
        this.onClickToSearch = this.onClickToSearch.bind(this);
        this.onDataSelect = this.onDataSelect.bind(this);
        this.getFormatHosting = this.getFormatHosting.bind(this);
        this.notificationInSuccess = this.notificationInSuccess.bind(this);
        this.notificationInError = this.notificationInError.bind(this);
        this.notificationCreateInError = this.notificationCreateInError.bind(this);
        this.openModalForDeleteHosting = this.openModalForDeleteHosting.bind(this);
        this.getButtonsForActions = this.getButtonsForActions.bind(this);

        this.onProcessActionStateChanged = this.onProcessActionStateChanged.bind(this);
        this.onProcessCreateHosting = this.onProcessCreateHosting.bind(this);
        this.onProcessEditHosting = this.onProcessEditHosting.bind(this);
        this.onProcessDeleteHosting = this.onProcessDeleteHosting.bind(this);
        this.onClickDeleteHosting = this.onClickDeleteHosting.bind(this);
        this.closeModalForDeleteHosting = this.closeModalForDeleteHosting.bind(this);
    }

    public componentDidUpdate() {
        const { isCanLoad, hasCloudHostingsReceived } = this.props;
        const { isFirstTimeLoaded } = this.state;

        if (isCanLoad && !isFirstTimeLoaded && !hasCloudHostingsReceived && this._commonTableRef.current) {
            this._commonTableRef.current.ReSelectData();
            this.setState({
                isFirstTimeLoaded: true
            });
        }
    }

    private onClickDeleteHosting() {
        if (this.state.coreHostingId) {
            this.props.dispatchDeleteCoreHostingThunk({
                coreHostingId: this.state.coreHostingId,
                force: true
            });
            this.setState({
                deleteIsOpen: false
            });
        }
    }

    private onProcessActionStateChanged(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        this.onProcessCreateHosting(processId, actionState, error);
        this.onProcessEditHosting(processId, actionState, error);
        this.onProcessDeleteHosting(processId, actionState, error);
    }

    private onProcessCreateHosting(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId !== TabHostingProcessId.CreateHosting) return;
        this.notificationInSuccess(actionState, 'Хостинг успешно создан');
        this.notificationCreateInError(actionState, error?.message);
    }

    private onProcessEditHosting(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId !== TabHostingProcessId.EditHosting) return;
        this.notificationInSuccess(actionState, 'Хостинг успешно сохранен');
        this.notificationInError(actionState, error?.message);
    }

    private onProcessDeleteHosting(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId !== TabHostingProcessId.DeleteHosting) return;
        this.notificationInSuccess(actionState, 'Хостинг успешно удален');
        this.notificationInError(actionState, error?.message);
    }

    private onClickToOpenModal(coreHostingId?: string) {
        this.setState({
            isOpen: true,
            coreHostingId
        });
    }

    private onClickToCloseModal() {
        this.setState({
            isOpen: false
        });
    }

    private onClickToSearch() {
        const {
            name,
            uploadApiUrl,
            uploadFilesPath
        } = this._searchHostingsRef.current?.state!;

        this.props.dispatchGetCoreHostingsThunk({
            filter: {
                name,
                uploadApiUrl,
                uploadFilesPath,
            },
            pageNumber: 1,
            orderBy: this.state.orderBy,
            pageSize: CommonSegmentEnumType.DefaultPageSize,
            force: true
        });
    }

    private onDataSelect(args: SelectDataCommonDataModel<undefined>) {
        const {
            name,
            uploadApiUrl,
            uploadFilesPath
        } = this._searchHostingsRef.current?.state!;

        const orderBy = args.sortingData ? this.getSortQuery(args.sortingData) : 'name.asc';

        this.setState({
            orderBy
        });

        this.props.dispatchGetCoreHostingsThunk({
            filter: {
                name,
                uploadApiUrl,
                uploadFilesPath,
            },
            pageNumber: args.pageNumber,
            orderBy,
            pageSize: CommonSegmentEnumType.DefaultPageSize,
            force: true
        });
    }

    private getSortQuery(args: SortingDataModel) {
        return `${ args.fieldName }.${ args.sortKind ? 'desc' : 'asc' }`;
    }

    private getButtonsForActions(coreHostingId: string) {
        return (
            <TextLinkButton
                className={ commonCss['delete-link'] }
                onClick={ () => { this.openModalForDeleteHosting(coreHostingId); } }
            >
                <i className="fa fa-trash" />
            </TextLinkButton>
        );
    }

    private getFormatHosting(name: string, data: CoreHostingDataModel) {
        return (
            <TextLinkButton onClick={ () => { this.onClickToOpenModal(data.id); } }>
                <TextOut fontSize={ 13 } className={ `text-link ${ commonCss['button-text'] }` }>
                    { name }
                </TextOut>
            </TextLinkButton>
        );
    }

    private notificationInError(actionState: IReducerProcessActionInfo, message?: string) {
        if (!actionState.isInErrorState) return;
        this.props.floatMessage.show(MessageType.Error, message);
    }

    private notificationCreateInError(actionState: IReducerProcessActionInfo, message?: string) {
        if (!actionState.isInErrorState) return;
        this.props.floatMessage.show(MessageType.Warning, message);
    }

    private notificationInSuccess(actionState: IReducerProcessActionInfo, message: string) {
        if (!actionState.isInSuccessState) return;
        this.props.floatMessage.show(MessageType.Success, message);
        this._commonTableRef.current?.ReSelectData();
        this.onClickToCloseModal();
    }

    private closeModalForDeleteHosting() {
        this.setState({
            deleteIsOpen: false
        });
    }

    private openModalForDeleteHosting(coreHostingId: string) {
        this.setState({
            coreHostingId,
            deleteIsOpen: true
        });
    }

    public render() {
        const { cloudHostings: { metadata, records } } = this.props;

        return (
            <>
                <SearchHostingsView
                    ref={ this._searchHostingsRef }
                    onClickToSearch={ this.onClickToSearch }
                />
                <CommonTableWithFilter
                    className={ commonCss['custom-table'] }
                    ref={ this._commonTableRef }
                    isResponsiveTable={ true }
                    isBorderStyled={ true }
                    uniqueContextProviderStateId="TabHostingsContextProviderStateId"
                    tableProps={ {
                        dataset: records,
                        keyFieldName: 'id',
                        sorting: {
                            sortKind: SortingKind.Asc,
                            sortFieldName: 'name'
                        },
                        fieldsView: {
                            name: {
                                caption: 'Название',
                                fieldWidth: 'auto',
                                fieldContentNoWrap: false,
                                isSortable: true,
                                format: this.getFormatHosting
                            },
                            uploadFilesPath: {
                                caption: 'Путь для загружаемых файлов',
                                fieldWidth: 'auto',
                                fieldContentNoWrap: false,
                                isSortable: true,
                            },
                            uploadApiUrl: {
                                caption: 'Адрес API для загрузки файлов',
                                fieldWidth: 'auto',
                                fieldContentNoWrap: false,
                                isSortable: true,
                            },
                            id: {
                                caption: '',
                                fieldWidth: 'auto',
                                fieldContentNoWrap: false,
                                format: this.getButtonsForActions
                            }
                        },
                        pagination: {
                            currentPage: metadata.pageNumber,
                            pageSize: metadata.pageSize,
                            recordsCount: metadata.totalItemCount,
                            totalPages: metadata.pageCount
                        },
                    } }
                    onDataSelect={ this.onDataSelect }
                />
                <SuccessButton
                    onClick={ () => { this.onClickToOpenModal(); } }
                >
                    <i className="fa fa-plus-circle mr-1" />
                    Создать хостинг
                </SuccessButton>
                {
                    this.state.isOpen
                        ? (
                            <HostingModalView
                                isOpen={ this.state.isOpen }
                                coreHostingId={ this.state.coreHostingId }
                                onClickToCloseModal={ this.onClickToCloseModal }
                            />
                        )
                        : null
                }
                <WatchReducerProcessActionState
                    watch={ [{
                        reducerStateName: 'TabHostingState',
                        processIds: [
                            TabHostingProcessId.CreateHosting,
                            TabHostingProcessId.EditHosting,
                            TabHostingProcessId.DeleteHosting
                        ]
                    }] }
                    onProcessActionStateChanged={ this.onProcessActionStateChanged }
                />
                <Dialog
                    isOpen={ this.state.deleteIsOpen }
                    dialogWidth="sm"
                    dialogVerticalAlign="center"
                    onCancelClick={ this.closeModalForDeleteHosting }
                    buttons={
                        [
                            {
                                content: 'Нет',
                                kind: 'primary',
                                variant: 'text',
                                onClick: this.closeModalForDeleteHosting
                            },
                            {
                                content: 'Да',
                                kind: 'error',
                                variant: 'contained',
                                onClick: this.onClickDeleteHosting
                            }
                        ]
                    }
                >
                    <Box display="flex" flexDirection="column" alignItems="center" justifyContent="center">
                        <ErrorOutlineIcon color="warning" sx={ { width: '5em', height: '5em' } } />
                        Вы действительно хотите удалить хостинг?
                    </Box>
                </Dialog>
            </>
        );
    }
}

const HostingTabConnected = connect<StateProps, DispatchProps, {}, AppReduxStoreState>(
    state => {
        const tabHostingState = state.TabHostingState;
        const { cloudHostings } = tabHostingState;
        const { hasCloudHostingsReceived } = tabHostingState.hasSuccessFor;

        return {
            cloudHostings,
            hasCloudHostingsReceived
        };
    },
    {
        dispatchGetCoreHostingsThunk: GetCoreHostingsThunk.invoke,
        dispatchDeleteCoreHostingThunk: DeleteCoreHostingThunk.invoke
    }
)(HostingTabClass);

export const HostingTabView = withFloatMessages(HostingTabConnected);