/**
 * Модель Redux формы для хостинга облака
 */
export type HostingTabDataForm = {
    /**
     * Название хостинга
     */
    name: string,

    /**
     * Путь для загружаемых файлов
     */
    uploadFilesPath: string,

    /**
     * Адрес API для загрузки файлов
     */
    uploadApiUrl: string
};