import { ServerFarmPublishNodeDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabServerFarmsApiProxy';

/**
 * Модель Redux формы для фермы сервера
 */
export type ServerFarmsTabDataForm = {
    /**
     * Название фермы
     */
    name: string,

    /**
     * Описание
     */
    description: string,

    /**
     * Сервера
     */
    servers: Array<ServerFarmPublishNodeDataModel>,

    /**
     * Доступные сервера для выбора
     */
    availableServers: Array<ServerFarmPublishNodeDataModel>
};