import { Box } from '@mui/material';
import { OutlinedButton } from 'app/views/components/controls/Button';
import { TextBoxForm } from 'app/views/components/controls/forms';
import { Component } from 'react';
import commonCss from '../../../../common/styles.module.css';

type SearchPlatformVersionsState = {
    /**
     * Версия платформы
     */
    version: string,

    /**
     * Путь к платформе
     */
    pathToPlatform: string,

    /**
     * Путь к платформе x64
     */
    pathToPlatformX64: string
};

type SearchPlatformVersionsProps = {
    /**
     * Функция для поиска
     */
    onClickToSearch: () => void;
};

export class SearchPlatformVersionsView extends Component<SearchPlatformVersionsProps, SearchPlatformVersionsState> {
    public constructor(props: SearchPlatformVersionsProps) {
        super(props);

        this.state = {
            version: '',
            pathToPlatformX64: '',
            pathToPlatform: ''
        };

        this.onValueChange = this.onValueChange.bind(this);
    }

    private onValueChange(formName: string, newValue: string) {
        this.setState(prevState => ({
            ...prevState,
            [formName]: newValue
        }));
    }

    public render() {
        return (
            <Box display="grid" gridTemplateColumns="repeat(auto-fit, minmax(250px, 1fr))" gap="12px" marginBottom="16px">
                <TextBoxForm
                    formName="version"
                    label="Версия 1С"
                    placeholder="Введите версию 1С"
                    value={ this.state.version }
                    onValueChange={ this.onValueChange }
                />
                <TextBoxForm
                    formName="pathToPlatform"
                    label="Путь к платформе"
                    placeholder="Введите путь"
                    value={ this.state.pathToPlatform }
                    onValueChange={ this.onValueChange }
                />
                <TextBoxForm
                    formName="pathToPlatformX64"
                    label="Путь к платформе x64"
                    placeholder="Введите путь"
                    value={ this.state.pathToPlatformX64 }
                    onValueChange={ this.onValueChange }
                />
                <OutlinedButton
                    kind="success"
                    className={ commonCss['search-button'] }
                    onClick={ this.props.onClickToSearch }
                >
                    <i className="fa fa-search mr-1" />
                    Поиск
                </OutlinedButton>
            </Box>
        );
    }
}