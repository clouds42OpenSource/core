import { CheckNodeAvailabilityThunkParams } from 'app/modules/segments/TabContentServers/store/reducer/checkNodeAvailabilityReducer/params';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { DualListBoxForm } from 'app/views/components/controls/forms/DualListBoxForm';
import { DualListItem, DualListStateListTypes } from 'app/views/components/controls/forms/DualListBoxForm/views/types';
import { ContentServersTabDataForm } from 'app/views/modules/Segments/views/ContentServersTabView/common/types';
import { PublishNodeDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import React, { Component } from 'react';

type CloudServerPublishNodesTabProps = {
    formFields: ContentServersTabDataForm;
    showNotification: (type: MessageType, content: React.ReactNode, autoHideDuration?: number) => void;
    updateReduxFormFields: (payload: Partial<ContentServersTabDataForm>) => void;
    dispatchCheckNodeAvailabilityThunk: (args: CheckNodeAvailabilityThunkParams) => void;
};

/**
 * Компонент таба "Ноды публикаций"
 */
export class CloudServerPublishNodesTabView extends Component<CloudServerPublishNodesTabProps> {
    public constructor(props: CloudServerPublishNodesTabProps) {
        super(props);

        this.onValueChange = this.onValueChange.bind(this);
        this.checkNodeAvailability = this.checkNodeAvailability.bind(this);
    }

    /**
     * Функция на изменение DualListBoxForm
     * @param _fieldName Название формы
     * @param newLeftValue Новые значения левой колонки
     * @param newRightValue Новые значения правой колонки
     * @param _oldLeftValue Старое значения левой колонки
     * @param oldRightValue Старое значения правой колонки
     * @returns Логическое значение для стейта DualListBoxForm
     */
    private onValueChange(_fieldName: string, newLeftValue: DualListItem<string>[], newRightValue: DualListItem<string>[],
        _oldLeftValue: DualListItem<string>[], oldRightValue: DualListItem<string>[]) {
        if (this.checkLastNode(newRightValue)) return false;
        this.checkNodeAvailability(newRightValue, oldRightValue);
        this.props.updateReduxFormFields({
            nodesData: {
                available: newLeftValue.map(item => ({
                    address: item.label,
                    description: '',
                    id: item.key
                })),
                selected: newRightValue.map(item => ({
                    address: item.label,
                    description: '',
                    id: item.key
                }))
            }
        });

        return true;
    }

    /**
     * Получить элементы двойного выпадающего списка
     * @param items список элементов модели
     */
    private getDualListItems(items: PublishNodeDataModel[]): Array<DualListItem<string>> {
        if (items) {
            return items.map(item => ({
                label: `${ item.address } ${ item.description }`,
                key: item.id
            }));
        }

        return [];
    }

    /**
     * Функция для проверки последнего нода
     * @param newRightValue Новое значение правой колонки
     * @returns Флаг указывающий что это последняя нода
     */
    private checkLastNode = (newRightValue: DualListItem<string>[]) => {
        const isRemovedLastNode = newRightValue.length === 0;
        if (isRemovedLastNode) this.props.showNotification(MessageType.Warning, 'Не возможно удалить единственную ноду сайта публикации');
        return isRemovedLastNode;
    };

    /**
     * Функция на проверку доступности нода
     * @param newRightValue Новые значения правой колонки
     * @param oldRightValue Старое значения правой колонки
     */
    private checkNodeAvailability(newRightValue: DualListItem<string>[], oldRightValue: DualListItem<string>[]) {
        const pusblishNewNodeId = newRightValue.slice(-1)[0]?.key;
        const isInArray = oldRightValue.some(item => item.key === pusblishNewNodeId);

        if (pusblishNewNodeId && !isInArray && newRightValue !== oldRightValue) {
            this.props.dispatchCheckNodeAvailabilityThunk({
                publishNodeId: pusblishNewNodeId,
                force: true
            });
        }
    }

    public render() {
        const { formFields: { nodesData } } = this.props;

        return (<DualListBoxForm
            allowSelectAll={ true }
            mainList={ DualListStateListTypes.rightList }
            leftListLabel="Доступные ноды для выбора"
            rightListLabel="Выбранные ноды"
            formName="contentServers"
            leftListItems={ this.getDualListItems(nodesData.available) }
            rightListItems={ this.getDualListItems(nodesData.selected) }
            isFormDisabled={ false }
            onValueChange={ this.onValueChange }
        />);
    }
}