import { SortingKind } from 'app/common/enums';
import { SegmentsProcessId } from 'app/modules/segments/TabSegments';
import { GetCloudSegmentsThunkParams } from 'app/modules/segments/TabSegments/store/reducers/getCloudSegmentsReducer/params';
import { getSegmentsDropdownListThunkParams } from 'app/modules/segments/TabSegments/store/reducers/getSegmentDropdownListReducer/params';
import { GetCloudSegmentsThunk, GetSegmentDropdownListThunk } from 'app/modules/segments/TabSegments/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { SuccessButton, TextLinkButton } from 'app/views/components/controls/Button';
import { TextOut } from 'app/views/components/TextOut';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { WatchReducerProcessActionState } from 'app/views/modules/_common/components/WatchReducerProcessActionState';
import { CommonSegmentEnumType } from 'app/views/modules/Segments/types';
import { SearchSegmentsView } from 'app/views/modules/Segments/views/SegmentsTabView/views/SearchSegmentsView';
import { SegmentModalView } from 'app/views/modules/Segments/views/SegmentsTabView/views/SegmentModalView';
import { SegmentDropdownListResponseDto } from 'app/web/api/SegmentsProxy';
import { SelectDataCommonDataModel, SelectDataResultMetadataModel, SortingDataModel } from 'app/web/common/data-models';
import { CloudSegmentDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabSegmentsApiProxy';
import { IReducerProcessActionInfo } from 'core/redux/interfaces';
import { Component, createRef } from 'react';
import { connect } from 'react-redux';

type SegmentsTabState = {
    /**
     * Поле для открытия модального окошка
     */
    isOpen: boolean;

    /**
     * ID сегмента
     */
    segmentId?: string;
};

type StateProps = {
    /**
     * Список сегментов
     */
    segments: SelectDataResultMetadataModel<CloudSegmentDataModel>;
    segmentDropdownList: SegmentDropdownListResponseDto
};

type DispatchProps = {
    dispatchGetCloudSegmentsThunk: (args: GetCloudSegmentsThunkParams) => void;
    dispatchGetSegmentDropdownListThunk: (args?: getSegmentsDropdownListThunkParams) => void;

};

type AllProps = StateProps & DispatchProps & FloatMessageProps;

class SegmentsTabClass extends Component<AllProps, SegmentsTabState> {
    private _searchSegmentRef = createRef<SearchSegmentsView>();

    private _commonTableRef = createRef<CommonTableWithFilter<CloudSegmentDataModel, undefined>>();

    public constructor(props: AllProps) {
        super(props);

        this.state = {
            isOpen: false
        };

        this.notificationInSuccess = this.notificationInSuccess.bind(this);
        this.notificationInError = this.notificationInError.bind(this);

        this.getFormatNameSegment = this.getFormatNameSegment.bind(this);
        this.onDataSelect = this.onDataSelect.bind(this);
        this.onClickSearch = this.onClickSearch.bind(this);
        this.onClickToOpenModal = this.onClickToOpenModal.bind(this);
        this.onClickToCloseModal = this.onClickToCloseModal.bind(this);
        this.onProcessActionStateChanged = this.onProcessActionStateChanged.bind(this);
        this.onProcessCreateSegment = this.onProcessCreateSegment.bind(this);
        this.onProcessDeleteSegment = this.onProcessDeleteSegment.bind(this);
        this.onProcessEditSegment = this.onProcessEditSegment.bind(this);
    }

    public componentDidMount() {
        this._commonTableRef.current?.ReSelectData();
        this.props.dispatchGetSegmentDropdownListThunk({ force: true });
    }

    public componentDidUpdate(prevProps: Readonly<AllProps>, prevState: Readonly<SegmentsTabState>) {
        if (this.state.isOpen && !prevState.isOpen) {
            this.props.dispatchGetSegmentDropdownListThunk({ force: true });
        }
    }

    private onProcessActionStateChanged(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        this.onProcessCreateSegment(processId, actionState, error);
        this.onProcessDeleteSegment(processId, actionState, error);
        this.onProcessEditSegment(processId, actionState, error);
    }

    private onProcessCreateSegment(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId !== SegmentsProcessId.CreateSegment) return;
        this.notificationInSuccess(actionState, 'Сегмент успешно создан');
        this.notificationInError(actionState, error?.message);
    }

    private onProcessDeleteSegment(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId !== SegmentsProcessId.DeleteSegment) return;
        this.notificationInSuccess(actionState, 'Сегмент успешно удален');
        this.notificationInError(actionState, error?.message);
    }

    private onProcessEditSegment(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId !== SegmentsProcessId.EditSegment) return;
        this.notificationInSuccess(actionState, 'Сегмент успешно сохранен');
        this.notificationInError(actionState, error?.message);
    }

    private onClickToOpenModal(segmentId?: string) {
        this.setState({
            isOpen: true,
            segmentId
        });
    }

    private onClickToCloseModal() {
        this.setState({
            isOpen: false
        });
    }

    private onClickSearch() {
        const {
            contentServersIds, enterpriseServersIds,
            fileStorageServersIds, gatewayTerminalsIds,
            pathToUserFiles, segmentDescription,
            segmentName, sqlServersIds,
            terminalFarmsIds
        } = this._searchSegmentRef.current?.state!;

        this.props.dispatchGetCloudSegmentsThunk({
            filter: {
                contentServerId: contentServersIds,
                customFileStoragePath: pathToUserFiles,
                description: segmentDescription,
                enterpriseServerId: enterpriseServersIds,
                fileStorageServerId: fileStorageServersIds,
                gatewayTerminalId: gatewayTerminalsIds,
                name: segmentName,
                sqlServerId: sqlServersIds,
                terminalFarmId: terminalFarmsIds,
            },
            pageNumber: 1,
            pageSize: CommonSegmentEnumType.DefaultPageSize,
            force: true
        });
    }

    private onDataSelect(args: SelectDataCommonDataModel<undefined>) {
        const {
            contentServersIds, enterpriseServersIds,
            fileStorageServersIds, gatewayTerminalsIds,
            pathToUserFiles, segmentDescription,
            segmentName, sqlServersIds,
            terminalFarmsIds
        } = this._searchSegmentRef.current?.state!;

        this.props.dispatchGetCloudSegmentsThunk({
            filter: {
                contentServerId: contentServersIds,
                customFileStoragePath: pathToUserFiles,
                description: segmentDescription,
                enterpriseServerId: enterpriseServersIds,
                fileStorageServerId: fileStorageServersIds,
                gatewayTerminalId: gatewayTerminalsIds,
                name: segmentName,
                sqlServerId: sqlServersIds,
                terminalFarmId: terminalFarmsIds,
            },
            orderBy: args.sortingData ? this.getSortQuery(args.sortingData) : 'name.asc',
            pageNumber: args.pageNumber,
            pageSize: CommonSegmentEnumType.DefaultPageSize,
            force: true
        });
    }

    private getFormatNameSegment(value: string, data: CloudSegmentDataModel) {
        return (
            <TextLinkButton onClick={ () => { this.onClickToOpenModal(data.id); } }>
                <TextOut fontSize={ 13 } className="text-link">
                    { value }
                </TextOut>
            </TextLinkButton>
        );
    }

    private getSortQuery(args: SortingDataModel) {
        return `${ args.fieldName }.${ args.sortKind ? 'desc' : 'asc' }`;
    }

    private notificationInError(actionState: IReducerProcessActionInfo, message?: string) {
        if (!actionState.isInErrorState) return;
        this.props.floatMessage.show(MessageType.Error, message);
    }

    private notificationInSuccess(actionState: IReducerProcessActionInfo, message: string) {
        if (!actionState.isInSuccessState) return;
        this.props.floatMessage.show(MessageType.Success, message);
        this._commonTableRef.current?.ReSelectData();
        this.onClickToCloseModal();
    }

    public render() {
        const { segments: { metadata, records } } = this.props;
        return (
            <>
                <SearchSegmentsView
                    ref={ this._searchSegmentRef }
                    onClickSearch={ this.onClickSearch }
                    segmentDropdownList={ this.props.segmentDropdownList }
                />
                <CommonTableWithFilter
                    ref={ this._commonTableRef }
                    isResponsiveTable={ true }
                    isBorderStyled={ true }
                    uniqueContextProviderStateId="SegmentsContextProviderStateId"
                    tableProps={ {
                        dataset: records,
                        keyFieldName: 'id',
                        sorting: {
                            sortKind: SortingKind.Asc,
                            sortFieldName: 'name'
                        },
                        fieldsView: {
                            name: {
                                caption: 'Название',
                                fieldWidth: '12%',
                                fieldContentNoWrap: false,
                                isSortable: true,
                                format: this.getFormatNameSegment
                            },
                            terminalFarmName: {
                                caption: 'Ферма ТС',
                                fieldWidth: '8%',
                                isSortable: true,
                                fieldContentNoWrap: false
                            },
                            enterpriseServer83Name: {
                                caption: 'Сервер 1С 8.3',
                                fieldWidth: '9%',
                                isSortable: true,
                                fieldContentNoWrap: false
                            },
                            defaultFileStorageName: {
                                caption: 'Файловое хранилище по умолчанию',
                                fieldWidth: '13%',
                                isSortable: true,
                                fieldContentNoWrap: false
                            },
                            contentServerName: {
                                caption: 'Сервер публикации',
                                fieldWidth: '10%',
                                isSortable: true,
                                fieldContentNoWrap: false
                            },
                            fileStorageServerName: {
                                caption: 'Хранилище клиентских файлов',
                                fieldWidth: '13%',
                                isSortable: true,
                                fieldContentNoWrap: false
                            },
                            sqlServerName: {
                                caption: 'Sql сервер',
                                fieldWidth: '7%',
                                isSortable: true,
                                fieldContentNoWrap: false
                            },
                            gatewayTerminalName: {
                                caption: 'Шлюз',
                                fieldWidth: '5%',
                                isSortable: true,
                                fieldContentNoWrap: false
                            },
                            customFileStoragePath: {
                                caption: 'Путь к файлам',
                                fieldWidth: '9%',
                                isSortable: true,
                                fieldContentNoWrap: false
                            },
                            description: {
                                caption: 'Описание',
                                fieldWidth: '9%',
                                isSortable: true,
                                fieldContentNoWrap: false
                            },
                            stable83Version: {
                                caption: 'Версия',
                                fieldWidth: '6%',
                                isSortable: true,
                                fieldContentNoWrap: false
                            }
                        },
                        pagination: {
                            currentPage: metadata.pageNumber,
                            pageSize: metadata.pageSize,
                            recordsCount: metadata.totalItemCount,
                            totalPages: metadata.pageCount
                        },
                    } }
                    onDataSelect={ this.onDataSelect }
                />
                <SuccessButton
                    onClick={ () => { this.onClickToOpenModal(); } }
                >
                    <i className="fa fa-plus-circle mr-1" />
                    Добавить сегмент
                </SuccessButton>
                {
                    this.state.isOpen
                        ? (
                            <SegmentModalView
                                isOpen={ this.state.isOpen }
                                segmentId={ this.state.segmentId }
                                onClickToCloseModal={ this.onClickToCloseModal }
                            />
                        )
                        : null
                }
                <WatchReducerProcessActionState
                    watch={ [{
                        reducerStateName: 'SegmentsState',
                        processIds: [
                            SegmentsProcessId.CreateSegment,
                            SegmentsProcessId.DeleteSegment,
                            SegmentsProcessId.EditSegment
                        ]
                    }] }
                    onProcessActionStateChanged={ this.onProcessActionStateChanged }
                />
            </>
        );
    }
}

const SegmentsTabConnected = connect<StateProps, DispatchProps, {}, AppReduxStoreState>(
    state => {
        const segmentsState = state.SegmentsState;
        const { segments } = segmentsState;
        const { segmentDropdownList } = segmentsState;

        return {
            segments,
            segmentDropdownList
        };
    },
    {
        dispatchGetCloudSegmentsThunk: GetCloudSegmentsThunk.invoke,
        dispatchGetSegmentDropdownListThunk: GetSegmentDropdownListThunk.invoke
    }
)(SegmentsTabClass);

export const SegmentsTabView = withFloatMessages(SegmentsTabConnected);