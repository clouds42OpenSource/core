/**
 * Enum максимальной длинны полей нода публикации
 */
export enum TabPublishNodeLengthEnumType {
    /**
     * Максимальная длина адреса подключения нода публикации
     */
    Address = 250
}