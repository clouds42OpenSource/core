import ErrorOutlineIcon from '@mui/icons-material/ErrorOutline';
import { Box } from '@mui/material';
import { PageEnumType } from 'app/common/enums';
import { checkOnValidField, checkValidOfStringField } from 'app/common/functions';
import { getEmptyUuid } from 'app/common/helpers';
import { IErrorsType } from 'app/common/interfaces';
import { SegmentsProcessId } from 'app/modules/segments/TabSegments';
import { CreateSegmentThunkParams } from 'app/modules/segments/TabSegments/store/reducers/createSegmentReducer/params';
import { DeleteSegmentThunkParams } from 'app/modules/segments/TabSegments/store/reducers/deleteSegmentReducer/params';
import { EditSegmentThunkParams } from 'app/modules/segments/TabSegments/store/reducers/editSegmentReducer/params';
import { GetSegmentAccountsThunkParams } from 'app/modules/segments/TabSegments/store/reducers/getSegmentAccountsReducer/params';
import { GetSegmentThunkParams } from 'app/modules/segments/TabSegments/store/reducers/getSegmentReducer/params';
import { CreateSegmentThunk, DeleteSegmentThunk, EditSegmentThunk, GetSegmentAccountsThunk, GetSegmentThunk } from 'app/modules/segments/TabSegments/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { Dialog } from 'app/views/components/controls/Dialog';
import { getDialogButtons, setTabsControl } from 'app/views/modules/Segments/common/functions';
import { CommonSegmentEnumType, CommonSegmentLengthEnumType } from 'app/views/modules/Segments/types';
import { SegmentModalDataForm, SegmentModalDataFormFieldsNamesType } from 'app/views/modules/Segments/views/SegmentsTabView/types';
import { AvailableForMigrationTabView } from 'app/views/modules/Segments/views/SegmentsTabView/views/SegmentModalView/partial-components/AvailableForMigrationTabView';
import { CommonTabView } from 'app/views/modules/Segments/views/SegmentsTabView/views/SegmentModalView/partial-components/CommonTabView';
import { FileStorageTabView } from 'app/views/modules/Segments/views/SegmentsTabView/views/SegmentModalView/partial-components/FileStorageTabView';
import { SegmentAccountsTabView } from 'app/views/modules/Segments/views/SegmentsTabView/views/SegmentModalView/partial-components/SegmentAccountsTabView';
import { TerminalServersTabView } from 'app/views/modules/Segments/views/SegmentsTabView/views/SegmentModalView/partial-components/TerminalServersTabView';
import { WatchReducerProcessActionState } from 'app/views/modules/_common/components/WatchReducerProcessActionState';
import { SegmentAccountDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabSegmentsApiProxy';
import { CloudServicesSegmentDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabSegmentsApiProxy/getSegment/data-models/CloudServicesSegmentDataModel';
import { CloudServicesSegmentBaseDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import { SegmentDropdownListResponseDto } from 'app/web/api/SegmentsProxy';
import { SelectDataMetadataResponseDto } from 'app/web/common';
import { IReducerProcessActionInfo } from 'core/redux/interfaces';
import { Component } from 'react';
import { connect } from 'react-redux';

type OwnProps = ReduxFormProps<SegmentModalDataForm>;

type SegmentModalProps = {
    /**
     * Поле для открытия модального окошка сегмента
     */
    isOpen: boolean;

    /**
     * ID сегмента
     */
    segmentId?: string;

    /**
     * Функция на закрытие модального окошка
     */
    onClickToCloseModal: () => void;
};

type SegmentModalState = {
    /**
     * Обьект Ошибок для валидации
     */
    errors: IErrorsType;

    /**
     * Тип страницы для динамического компонента редактирования/создания
     */
    pageType: PageEnumType;
    /**
     * Состояние модального окна на удаление сегмента
     */
    deleteIsOpen: boolean;
};

type StateProps = {
    /**
     * Модель сегмента
     */
    segment: CloudServicesSegmentDataModel;

    /**
     * Массив аккаунта сегмента
     */
    segmentAccounts: SelectDataMetadataResponseDto<SegmentAccountDataModel>;
    segmentDropdownList: SegmentDropdownListResponseDto
};

type DispatchProps = {
    dispatchCreateSegmentThunk: (args: CreateSegmentThunkParams) => void;
    dispatchGetSegmentThunk: (args: GetSegmentThunkParams) => void;
    dispatchGetSegmentAccountsThunk: (args: GetSegmentAccountsThunkParams) => void;
    dispatchDeleteSegmentThunk: (args: DeleteSegmentThunkParams) => void;
    dispatchEditSegmentThunk: (args: EditSegmentThunkParams) => void;
};

type AllProps = SegmentModalProps & OwnProps & StateProps & DispatchProps & FloatMessageProps;

class SegmentModalClass extends Component<AllProps, SegmentModalState> {
    public constructor(props: AllProps) {
        super(props);

        this.state = {
            errors: {},
            pageType: this.props.segmentId ? PageEnumType.Edit : PageEnumType.Create,
            deleteIsOpen: false
        };

        this.getTabCommon = this.getTabCommon.bind(this);
        this.getTabFileStorage = this.getTabFileStorage.bind(this);
        this.getTabAvailableForMigration = this.getTabAvailableForMigration.bind(this);
        this.getTabSegmentAccounts = this.getTabSegmentAccounts.bind(this);
        this.getTabTerminalServers = this.getTabTerminalServers.bind(this);
        this.onValueChange = this.onValueChange.bind(this);
        this.submitChanges = this.submitChanges.bind(this);
        this.isSegmentFormValid = this.isSegmentFormValid.bind(this);
        this.openModalForDeleteSegment = this.openModalForDeleteSegment.bind(this);
        this.onClickDeleteSegment = this.onClickDeleteSegment.bind(this);
        this.fillBaseSegmentModel = this.fillBaseSegmentModel.bind(this);
        this.createSegment = this.createSegment.bind(this);
        this.editSegment = this.editSegment.bind(this);

        this.onProcessActionStateChanged = this.onProcessActionStateChanged.bind(this);
        this.onProcessGetSegment = this.onProcessGetSegment.bind(this);
        this.successGetSegmentState = this.successGetSegmentState.bind(this);
        this.errorGetSegmentState = this.errorGetSegmentState.bind(this);
        this.closeModalForDeleteSegment = this.closeModalForDeleteSegment.bind(this);

        this.props.reduxForm.setInitializeFormDataAction(this.defaultInitDataReduxForm);
    }

    public componentDidMount() {
        if (this.props.segmentId) {
            this.props.dispatchGetSegmentThunk({
                segmentId: this.props.segmentId,
                force: true
            });
        }
    }

    private onProcessActionStateChanged(processId: string, actionState: IReducerProcessActionInfo) {
        void this.onProcessGetSegment(processId, actionState);
    }

    private async onProcessGetSegment(processId: string, actionState: IReducerProcessActionInfo) {
        if (processId !== SegmentsProcessId.GetSegment) return;
        await this.successGetSegmentState(actionState);
        this.errorGetSegmentState(actionState);
    }

    private onClickDeleteSegment() {
        const formFields = this.props.reduxForm.getReduxFormFields(false);

        if (formFields.segmentId) {
            this.props.dispatchDeleteSegmentThunk({
                segmentId: formFields.segmentId,
                force: true
            });
        }
    }

    private onValueChange<TValue, TForm extends string = SegmentModalDataFormFieldsNamesType>(formName: TForm, newValue: TValue) {
        this.props.reduxForm.updateReduxFormFields({
            [formName]: newValue
        });

        const { errors } = this.state;

        if (typeof newValue === 'string') {
            switch (formName) {
                case 'name':
                    errors[formName] = checkValidOfStringField(newValue, 'Заполните наименование', CommonSegmentLengthEnumType.Name);
                    break;
                case 'description':
                case 'pathToFile':
                    errors[formName] = checkValidOfStringField(newValue, undefined, CommonSegmentLengthEnumType.Description);
                    break;
                case 'backupStorageId':
                case 'fileStorageServerId':
                case 'enterprise82ServerId':
                case 'enterprise83ServerId':
                case 'contentServerId':
                case 'sqlServerId':
                case 'terminalFarmId':
                case 'stable82Version':
                case 'stable83Version':
                case 'coreHostingListId':
                    errors[formName] = checkValidOfStringField(newValue, 'Выберите значение из списка');
                    break;
                default:
                    break;
            }

            this.setState({ errors });
        }
    }

    private getTabCommon() {
        const { errors, pageType } = this.state;
        const formFields = this.props.reduxForm.getReduxFormFields(false);
        const { segmentDropdownList } = this.props;

        return (<CommonTabView
            key="commonTab"
            errors={ errors }
            formFields={ formFields }
            onValueChange={ this.onValueChange }
            pageType={ pageType }
            segmentDropdownList={ segmentDropdownList }
        />);
    }

    private getTabAvailableForMigration() {
        const formFields = this.props.reduxForm.getReduxFormFields(false);

        return (<AvailableForMigrationTabView
            key="availableForMigrationTab"
            formFields={ formFields }
            updateReduxFormFields={ this.props.reduxForm.updateReduxFormFields }
        />);
    }

    private getTabSegmentAccounts() {
        const formFields = this.props.reduxForm.getReduxFormFields(false);

        return (<SegmentAccountsTabView
            key="segmentAccountsTab"
            segmentAccounts={ this.props.segmentAccounts }
            formFields={ formFields }
            onValueChange={ this.onValueChange }
            dispatchGetSegmentAccountsThunk={ this.props.dispatchGetSegmentAccountsThunk }
        />);
    }

    private getTabFileStorage() {
        const formFields = this.props.reduxForm.getReduxFormFields(false);

        return (<FileStorageTabView
            key="fileStorageTab"
            formFields={ formFields }
            floatMessageShow={ this.props.floatMessage.show }
            onValueChange={ this.onValueChange }
            updateReduxFormFields={ this.props.reduxForm.updateReduxFormFields }
        />);
    }

    private getHeadersArray() {
        return [
            {
                title: 'Общее',
                isVisible: true
            },
            {
                title: 'Хранилища файловых баз',
                isVisible: true
            },
            {
                title: 'Сегменты доступные для миграции',
                isVisible: true
            },
            {
                title: 'Аккаунты сегмента',
                isVisible: true
            },
            {
                title: 'Терминальные сервера',
                isVisible: true
            }
        ];
    }

    private getTabTerminalServers() {
        const formFields = this.props.reduxForm.getReduxFormFields(false);

        return (<TerminalServersTabView
            key="terminalServersTab"
            formFields={ formFields }
            updateReduxFormFields={ this.props.reduxForm.updateReduxFormFields }
        />);
    }

    private fillBaseSegmentModel(formFields: SegmentModalDataForm): CloudServicesSegmentBaseDataModel {
        return {
            alpha83VersionId: formFields.alpha83VersionId,
            backupStorageId: formFields.backupStorageId,
            contentServerId: formFields.contentServerId,
            customFileStoragePath: formFields.customFileStoragePath,
            defaultSegmentStorageId: formFields.defaultSegmentStorageId,
            delimiterDatabaseMustUseWebService: formFields.delimiterDatabaseMustUseWebService,
            description: formFields.description,
            enterpriseServer82Id: formFields.enterpriseServer82Id,
            enterpriseServer83Id: formFields.enterpriseServer83Id,
            fileStorageServersId: formFields.fileStorageServersId,
            gatewayTerminalsId: formFields.gatewayTerminalsId,
            hostingName: '',
            name: formFields.name,
            servicesTerminalFarmId: formFields.servicesTerminalFarmId,
            sqlServerId: formFields.sqlServerId,
            stable82VersionId: formFields.stable82VersionId,
            stable83VersionId: formFields.stable83VersionId,
            autoUpdateNodeId: formFields.autoUpdateNodeId,
            notMountDiskR: formFields.notMountDiskR
        };
    }

    private createSegment() {
        if (this.state.pageType !== PageEnumType.Create) return;
        const formFields = this.props.reduxForm.getReduxFormFields(false);
        const baseSegmentModel = this.fillBaseSegmentModel(formFields);

        this.props.dispatchCreateSegmentThunk({
            ...baseSegmentModel,
            coreHostingId: formFields.coreHostingId,
            isDefault: false,
            autoUpdateNodeId: formFields.autoUpdateNodeId ? formFields.autoUpdateNodeId : null,
            notMountDiskR: formFields.notMountDiskR,
            force: true
        });
    }

    private closeModalForDeleteSegment() {
        this.setState({
            deleteIsOpen: false
        });
    }

    private openModalForDeleteSegment() {
        this.setState({
            deleteIsOpen: true
        });
    }

    private editSegment() {
        if (this.state.pageType !== PageEnumType.Edit) return;
        const formFields = this.props.reduxForm.getReduxFormFields(false);
        const baseSegmentModel = this.fillBaseSegmentModel(formFields);
        const { segment: { baseData, id } } = this.props;

        this.props.dispatchEditSegmentThunk({
            ...baseSegmentModel,
            id,
            hostingName: baseData.hostingName,
            availableMigrationSegment: formFields.availableMigrationSegment.map(({ key, value }) => ({ id: key, name: value })),
            segmentFileStorageServers: formFields.segmentFileStorageServers.map(({ key, value }) => ({ id: key, name: value })),
            segmentTerminalServers: formFields.segmentTerminalServers,
            autoUpdateNodeId: formFields.autoUpdateNodeId ? formFields.autoUpdateNodeId : null,
            notMountDiskR: formFields.notMountDiskR,
            force: true
        });
    }

    private submitChanges() {
        if (!this.isSegmentFormValid()) return;
        this.createSegment();
        this.editSegment();
    }

    private errorGetSegmentState(actionState: IReducerProcessActionInfo, error?: Error) {
        if (!actionState.isInErrorState) return;
        this.props.floatMessage.show(MessageType.Error, `Ошибка при получении сегмента: ${ error?.message }`);
    }

    private async successGetSegmentState(actionState: IReducerProcessActionInfo) {
        if (!actionState.isInSuccessState) return;
        const { segment } = this.props;
        const baseSegment = segment.baseData;

        this.props.reduxForm.updateReduxFormFields({
            alpha83VersionId: baseSegment.alpha83VersionId,
            backupStorageId: baseSegment.backupStorageId,
            contentServerId: baseSegment.contentServerId,
            coreHostingId: baseSegment.hostingName,
            delimiterDatabaseMustUseWebService: baseSegment.delimiterDatabaseMustUseWebService,
            description: baseSegment.description,
            enterpriseServer82Id: baseSegment.enterpriseServer82Id,
            enterpriseServer83Id: baseSegment.enterpriseServer83Id,
            fileStorageServersId: baseSegment.fileStorageServersId,
            gatewayTerminalsId: baseSegment.gatewayTerminalsId,
            name: baseSegment.name,
            customFileStoragePath: baseSegment.customFileStoragePath,
            sqlServerId: baseSegment.sqlServerId,
            stable82VersionId: baseSegment.stable82VersionId,
            stable83VersionId: baseSegment.stable83VersionId,
            servicesTerminalFarmId: baseSegment.servicesTerminalFarmId,
            availableSegmentFileStorageServers: segment.fileStoragesData.available,
            segmentFileStorageServers: segment.fileStoragesData.selected,
            defaultSegmentStorageId: baseSegment.defaultSegmentStorageId,
            availableMigrationSegment: segment.migrationData.available,
            segmentsForMigration: segment.migrationData.selected,
            segmentId: segment.id,
            availableSegmentTerminalServers: segment.terminalServersData.available,
            segmentTerminalServers: segment.terminalServersData.selected,
            autoUpdateNodeId: segment.baseData.autoUpdateNodeId ?? '',
            notMountDiskR: baseSegment.notMountDiskR,
        });

        this.props.dispatchGetSegmentAccountsThunk({
            pageNumber: 1,
            segmentId: segment.id,
            pageSize: CommonSegmentEnumType.DefaultPageSize,
            force: true
        });
    }

    private isSegmentFormValid() {
        const formFields = this.props.reduxForm.getReduxFormFields(false);
        const errors: IErrorsType = {};
        const arrayResults: boolean[] = [];

        arrayResults.push(checkOnValidField(errors, 'name', formFields.name, 'Заполните наименование', CommonSegmentLengthEnumType.Name));
        arrayResults.push(checkOnValidField(errors, 'description', formFields.description, undefined, CommonSegmentLengthEnumType.Description));
        arrayResults.push(checkOnValidField(errors, 'pathToFile', formFields.customFileStoragePath, undefined, CommonSegmentLengthEnumType.Description));
        arrayResults.push(checkOnValidField(errors, 'backupStorageId', formFields.backupStorageId, 'Выберите значение из списка'));
        arrayResults.push(checkOnValidField(errors, 'fileStorageServerId', formFields.fileStorageServersId, 'Выберите значение из списка'));
        arrayResults.push(checkOnValidField(errors, 'enterprise82ServerId', formFields.enterpriseServer82Id, 'Выберите значение из списка'));
        arrayResults.push(checkOnValidField(errors, 'enterprise83ServerId', formFields.enterpriseServer83Id, 'Выберите значение из списка'));
        arrayResults.push(checkOnValidField(errors, 'contentServerId', formFields.contentServerId, 'Выберите значение из списка'));
        arrayResults.push(checkOnValidField(errors, 'sqlServerId', formFields.sqlServerId, 'Выберите значение из списка'));
        arrayResults.push(checkOnValidField(errors, 'terminalFarmId', formFields.servicesTerminalFarmId, 'Выберите значение из списка'));
        arrayResults.push(checkOnValidField(errors, 'stable82Version', formFields.stable82VersionId, 'Выберите значение из списка'));
        arrayResults.push(checkOnValidField(errors, 'stable83Version', formFields.stable83VersionId, 'Выберите значение из списка'));
        arrayResults.push(checkOnValidField(errors, 'coreHostingListId', formFields.coreHostingId, 'Выберите значение из списка'));

        if (arrayResults.some(item => !item)) {
            this.props.floatMessage.show(MessageType.Warning, 'Форма к отправке не готова. Пожалуйста, заполните все поля.');
            this.setState({ errors });
            return false;
        }

        return true;
    }

    private defaultInitDataReduxForm(): SegmentModalDataForm | undefined {
        return {
            alpha83VersionId: '',
            backupStorageId: '',
            contentServerId: '',
            coreHostingId: '',
            description: '',
            enterpriseServer82Id: '',
            enterpriseServer83Id: '',
            fileStorageServersId: '',
            gatewayTerminalsId: '',
            name: '',
            customFileStoragePath: '',
            sqlServerId: '',
            stable82VersionId: '',
            stable83VersionId: '',
            servicesTerminalFarmId: '',
            delimiterDatabaseMustUseWebService: false,
            availableSegmentFileStorageServers: [],
            defaultSegmentStorageId: getEmptyUuid(),
            segmentFileStorageServers: [],
            availableMigrationSegment: [],
            segmentsForMigration: [],
            accountName: '',
            accountNumber: '',
            segmentId: '',
            availableSegmentTerminalServers: [],
            segmentTerminalServers: [],
            autoUpdateNodeId: '',
            notMountDiskR: false,
        };
    }

    public render() {
        const { isOpen } = this.props;
        const isEditTypePage = this.state.pageType === PageEnumType.Edit;
        const titleText = isEditTypePage ? 'Карточка' : 'Создание';
        const formFields = this.props.reduxForm.getReduxFormFields(false);

        return (
            <>
                <Dialog
                    disableBackdropClick={ true }
                    title={ `${ titleText } сегмента ${ formFields.name && isEditTypePage ? `"${ formFields.name }"` : '' }` }
                    isOpen={ isOpen }
                    dialogWidth="lg"
                    isTitleSmall={ true }
                    titleTextAlign="left"
                    titleFontSize={ 14 }
                    dialogVerticalAlign="top"
                    onCancelClick={ this.props.onClickToCloseModal }
                    buttons={ getDialogButtons({
                        pageType: this.state.pageType,
                        onClickToCloseModal: this.props.onClickToCloseModal,
                        submitChanges: this.submitChanges,
                        isAddDeleteButton: true,
                        onClickToDelete: this.openModalForDeleteSegment
                    }) }
                >
                    {
                        this.state.pageType === PageEnumType.Edit
                            ? setTabsControl(
                                [
                                    this.getTabCommon(),
                                    this.getTabFileStorage(),
                                    this.getTabAvailableForMigration(),
                                    this.getTabSegmentAccounts(),
                                    this.getTabTerminalServers()
                                ],
                                { headers: this.getHeadersArray() })
                            : this.getTabCommon()
                    }
                </Dialog>
                <WatchReducerProcessActionState
                    watch={ [{
                        reducerStateName: 'SegmentsState',
                        processIds: [
                            SegmentsProcessId.GetSegment,
                            SegmentsProcessId.CheckAbilityToDeleteFileStorage
                        ]
                    }] }
                    onProcessActionStateChanged={ this.onProcessActionStateChanged }
                />
                <Dialog
                    isOpen={ this.state.deleteIsOpen }
                    dialogWidth="sm"
                    dialogVerticalAlign="center"
                    onCancelClick={ this.closeModalForDeleteSegment }
                    buttons={
                        [
                            {
                                content: 'Нет',
                                kind: 'primary',
                                variant: 'text',
                                onClick: this.closeModalForDeleteSegment
                            },
                            {
                                content: 'Да',
                                kind: 'error',
                                variant: 'contained',
                                onClick: this.onClickDeleteSegment
                            }
                        ]
                    }
                >
                    <Box display="flex" flexDirection="column" alignItems="center" justifyContent="center">
                        <ErrorOutlineIcon color="warning" sx={ { width: '5em', height: '5em' } } />
                        Вы действительно хотите удалить сегмент?
                    </Box>
                </Dialog>
            </>
        );
    }
}

const SegmentModalConnected = connect<StateProps, DispatchProps, OwnProps, AppReduxStoreState>(
    state => {
        const segmentsState = state.SegmentsState;
        let { segmentElements } = segmentsState;
        const { segment } = segmentsState;
        const { segmentAccounts } = segmentsState;
        const { isCheckAbilityToDeleteFileStorage } = segmentsState;
        const { segmentDropdownList } = segmentsState;

        if (segment.additionalData) segmentElements = segment.additionalData;

        return {
            segmentElements,
            segmentDropdownList,
            segment,
            segmentAccounts,
            isCheckAbilityToDeleteFileStorage
        };
    },
    {
        dispatchCreateSegmentThunk: CreateSegmentThunk.invoke,
        dispatchGetSegmentThunk: GetSegmentThunk.invoke,
        dispatchGetSegmentAccountsThunk: GetSegmentAccountsThunk.invoke,
        dispatchDeleteSegmentThunk: DeleteSegmentThunk.invoke,
        dispatchEditSegmentThunk: EditSegmentThunk.invoke
    }
)(SegmentModalClass);

export const SegmentModalView = withReduxForm(withFloatMessages(SegmentModalConnected),
    {
        reduxFormName: 'Segment_Modal_Form',
        resetOnUnmount: true
    });