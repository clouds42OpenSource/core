import { SegmentElementDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import { MigrationSegmentDataModel, SegmentTerminalServerDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabSegmentsApiProxy';

/**
 * Модель Redux формы для карточки сегмента
 */
export type SegmentModalDataForm = {
    /**
     * Название сегмента
     */
    name: string;

    /**
     * Описание сегмента
     */
    description: string;

    /**
     * ID сервера бекапов
     */
    backupStorageId: string;

    /**
     * ID Хранилище клиентских файлов
     */
    fileStorageServersId: string;

    /**
     * ID Предприятие 8.2
     */
    enterpriseServer82Id: string;

    /**
     * ID Предприятие 8.3
     */
    enterpriseServer83Id: string;

    /**
     * ID Cервер публикации
     */
    contentServerId: string;

    /**
     * ID SQL сервер
     */
    sqlServerId: string;

    /**
     * ID Ферма ТС
     */
    servicesTerminalFarmId: string;

    /**
     * ID Шлюз
     */
    gatewayTerminalsId: string;

    /**
     * Cтабильная версий 8.2
     */
    stable82VersionId: string;

    /**
     * Cтабильная версий 8.3
     */
    stable83VersionId: string;

    /**
     * Альфа версия 8.3
     */
    alpha83VersionId: string | null;

    /**
     * Путь к файлам пользователя
     */
    customFileStoragePath: string;

    /**
     * ID Хостинг
     */
    coreHostingId: string;

    /**
     * Открывать базы в тонком клиенте по http ссылке
     */
    delimiterDatabaseMustUseWebService: boolean;

    /**
     * Хранилище по умолчанию
     */
    defaultSegmentStorageId: string;

    /**
     * Выбранные хранилища
     */
    segmentFileStorageServers: SegmentElementDataModel[];

    /**
     * Доступные хранилища для выбора
     */
    availableSegmentFileStorageServers: SegmentElementDataModel[];

    /**
     * Доступные сегменты для миграции
     */
    availableMigrationSegment: MigrationSegmentDataModel[];

    /**
     * Сегменты для миграции
     */
    segmentsForMigration: MigrationSegmentDataModel[];

    /**
     * Название аккаунта
     */
    accountName: string;

    /**
     * Номер аккаунта
     */
    accountNumber: string;

    /**
     * ID сегмента
     */
    segmentId: string;

    /**
     * Терминальные сервера сегмента
     */
    segmentTerminalServers: SegmentTerminalServerDataModel[];

    /**
     * Доступные терминальные сервера для сегмента
     */
    availableSegmentTerminalServers: SegmentTerminalServerDataModel[];
    autoUpdateNodeId: string | null;
    notMountDiskR?: boolean;
};