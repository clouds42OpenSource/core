import ErrorOutlineIcon from '@mui/icons-material/ErrorOutline';
import { Box } from '@mui/material';
import { SortingKind } from 'app/common/enums';
import { TabFileStoragesProcessId } from 'app/modules/segments/TabFileStorages';
import { DeleteCloudFileStorageServerThunkParams } from 'app/modules/segments/TabFileStorages/store/reducer/deleteCloudFileStorageServerReducer/params';
import { GetCloudFileStorageServersThunkParams } from 'app/modules/segments/TabFileStorages/store/reducer/getCloudFileStorageServersReducer/params';
import { DeleteCloudFileStorageServerThunk, GetCloudFileStorageServersThunk } from 'app/modules/segments/TabFileStorages/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { SuccessButton, TextLinkButton } from 'app/views/components/controls/Button';
import { Dialog } from 'app/views/components/controls/Dialog';
import { TextOut } from 'app/views/components/TextOut';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { WatchReducerProcessActionState } from 'app/views/modules/_common/components/WatchReducerProcessActionState';
import { CommonSearchSegmentView } from 'app/views/modules/Segments/common/components';
import { CommonSegmentEnumType } from 'app/views/modules/Segments/types';
import { FileStorageModalView } from 'app/views/modules/Segments/views/FileStoragesTabView/views/FileStorageModalView';
import { SelectDataMetadataResponseDto, SortingDataModel } from 'app/web/common';
import { SelectDataCommonDataModel } from 'app/web/common/data-models';
import { CloudFileStorageServerDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabFileStorageApiProxy';
import { IReducerProcessActionInfo } from 'core/redux/interfaces';
import { Component, createRef } from 'react';
import { connect } from 'react-redux';
import commonCss from '../../common/styles.module.css';
import css from './styles.module.css';

type FileStoragesTabState = {
    isFirstTimeLoaded: boolean;
    isOpen: boolean;
    fileStorageId?: string;
    deleteIsOpen: boolean;
    orderBy?: string;
};

type FileStoragesTabProps = {
    isCanLoad: boolean;
};

type StateProps = {
    cloudFileStorageServers: SelectDataMetadataResponseDto<CloudFileStorageServerDataModel>;

    hasCloudFileStorageServersReceived: boolean;
};

type DispatchProps = {
    dispatchGetCloudFileStorageServersThunk: (args: GetCloudFileStorageServersThunkParams) => void;
    dispatchDeleteCloudFileStorageServerThunk: (args: DeleteCloudFileStorageServerThunkParams) => void;
};

type AllProps = FileStoragesTabProps & StateProps & DispatchProps & FloatMessageProps;

class FileStoragesTabClass extends Component<AllProps, FileStoragesTabState> {
    private _commonTableRef = createRef<CommonTableWithFilter<CloudFileStorageServerDataModel, undefined>>();

    private _searchCloudFileStorageServerRef = createRef<CommonSearchSegmentView>();

    public constructor(props: AllProps) {
        super(props);

        this.state = {
            isOpen: false,
            isFirstTimeLoaded: false,
            deleteIsOpen: false,
            orderBy: ''
        };

        this.onClickToOpenModal = this.onClickToOpenModal.bind(this);
        this.onClickToCloseModal = this.onClickToCloseModal.bind(this);
        this.onClickToSearch = this.onClickToSearch.bind(this);
        this.onDataSelect = this.onDataSelect.bind(this);
        this.getFormatNameFileStorage = this.getFormatNameFileStorage.bind(this);
        this.notificationInSuccess = this.notificationInSuccess.bind(this);
        this.notificationInError = this.notificationInError.bind(this);
        this.openModalForDeleteFileStorage = this.openModalForDeleteFileStorage.bind(this);
        this.getButtonsForActions = this.getButtonsForActions.bind(this);

        this.onProcessActionStateChanged = this.onProcessActionStateChanged.bind(this);
        this.onProcessCreateFileStorage = this.onProcessCreateFileStorage.bind(this);
        this.onProcessEditFileStorage = this.onProcessEditFileStorage.bind(this);
        this.onProcessDeleteFileStorage = this.onProcessDeleteFileStorage.bind(this);
        this.onClickDeleteFileStorage = this.onClickDeleteFileStorage.bind(this);
        this.closeModalForDeleteFileStorage = this.closeModalForDeleteFileStorage.bind(this);
        this.getSortQuery = this.getSortQuery.bind(this);
    }

    public componentDidUpdate() {
        const { isCanLoad, hasCloudFileStorageServersReceived } = this.props;
        const { isFirstTimeLoaded } = this.state;

        if (isCanLoad && !isFirstTimeLoaded && !hasCloudFileStorageServersReceived && this._commonTableRef.current) {
            this._commonTableRef.current.ReSelectData();
            this.setState({
                isFirstTimeLoaded: true
            });
        }
    }

    private onClickDeleteFileStorage() {
        if (this.state.fileStorageId) {
            this.props.dispatchDeleteCloudFileStorageServerThunk({
                fileStorageId: this.state.fileStorageId,
                force: true
            });
            this.setState({
                deleteIsOpen: false
            });
        }
    }

    private onProcessActionStateChanged(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        this.onProcessCreateFileStorage(processId, actionState, error);
        this.onProcessEditFileStorage(processId, actionState, error);
        this.onProcessDeleteFileStorage(processId, actionState, error);
    }

    private onProcessCreateFileStorage(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId !== TabFileStoragesProcessId.CreateCloudFileStorageServer) return;
        this.notificationInSuccess(actionState, 'Файловое хранилище успешно создано');
        this.notificationInError(actionState, error?.message);
    }

    private onProcessEditFileStorage(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId !== TabFileStoragesProcessId.EditCloudFileStorageServer) return;
        this.notificationInSuccess(actionState, 'Файловое хранилище успешно сохранено');
        this.notificationInError(actionState, error?.message);
    }

    private onProcessDeleteFileStorage(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId !== TabFileStoragesProcessId.DeleteCloudFileStorageServer) return;
        this.notificationInSuccess(actionState, 'Файловое хранилище успешно удалено');
        this.notificationInError(actionState, error?.message);
    }

    private onClickToOpenModal(fileStorageId?: string) {
        this.setState({
            isOpen: true,
            fileStorageId
        });
    }

    private onClickToCloseModal() {
        this.setState({
            isOpen: false
        });
    }

    private onClickToSearch() {
        const {
            description,
            connectionAddress,
            name
        } = this._searchCloudFileStorageServerRef.current?.state!;

        this.props.dispatchGetCloudFileStorageServersThunk({
            filter: {
                description,
                connectionAddress,
                name,
            },
            pageNumber: 1,
            orderBy: this.state.orderBy,
            pageSize: CommonSegmentEnumType.DefaultPageSize,
            force: true
        });
    }

    private onDataSelect(args: SelectDataCommonDataModel<undefined>) {
        const {
            description,
            connectionAddress,
            name
        } = this._searchCloudFileStorageServerRef.current?.state!;

        const orderBy = args.sortingData ? this.getSortQuery(args.sortingData) : 'name.asc';

        this.setState({
            orderBy
        });

        this.props.dispatchGetCloudFileStorageServersThunk({
            filter: {
                description,
                connectionAddress,
                name,
            },
            pageNumber: args.pageNumber,
            orderBy,
            pageSize: CommonSegmentEnumType.DefaultPageSize,
            force: true
        });
    }

    private getButtonsForActions(Id: string) {
        return (
            <TextLinkButton
                className={ commonCss['delete-link'] }
                onClick={ () => { this.openModalForDeleteFileStorage(Id); } }
            >
                <i className="fa fa-trash" />
            </TextLinkButton>
        );
    }

    private getSortQuery(args: SortingDataModel) {
        return `${ args.fieldName }.${ args.sortKind ? 'desc' : 'asc' }`;
    }

    private getFormatNameFileStorage(name: string, data: CloudFileStorageServerDataModel) {
        return (
            <TextLinkButton onClick={ () => { this.onClickToOpenModal(data.id); } }>
                <TextOut fontSize={ 13 } className="text-link">
                    { name }
                </TextOut>
            </TextLinkButton>
        );
    }

    private notificationInError(actionState: IReducerProcessActionInfo, message?: string) {
        if (!actionState.isInErrorState) return;
        this.props.floatMessage.show(MessageType.Error, message);
    }

    private notificationInSuccess(actionState: IReducerProcessActionInfo, message: string) {
        if (!actionState.isInSuccessState) return;
        this.props.floatMessage.show(MessageType.Success, message);
        this._commonTableRef.current?.ReSelectData();
        this.onClickToCloseModal();
    }

    private closeModalForDeleteFileStorage() {
        this.setState({
            deleteIsOpen: false
        });
    }

    private openModalForDeleteFileStorage(fileStorageId: string) {
        this.setState({
            fileStorageId,
            deleteIsOpen: true
        });
    }

    public render() {
        const { cloudFileStorageServers: { metadata, records } } = this.props;

        return (
            <>
                <CommonSearchSegmentView
                    ref={ this._searchCloudFileStorageServerRef }
                    onClickToSearch={ this.onClickToSearch }
                />
                <CommonTableWithFilter
                    className={ css['custom-table'] }
                    ref={ this._commonTableRef }
                    isResponsiveTable={ true }
                    isBorderStyled={ true }
                    uniqueContextProviderStateId="TabFileStorageServerContextProviderStateId"
                    tableProps={ {
                        dataset: records,
                        keyFieldName: 'id',
                        sorting: {
                            sortKind: SortingKind.Asc,
                            sortFieldName: 'name'
                        },
                        fieldsView: {
                            name: {
                                caption: 'Название',
                                fieldContentNoWrap: false,
                                fieldWidth: 'auto',
                                isSortable: true,
                                format: this.getFormatNameFileStorage
                            },
                            description: {
                                caption: 'Описание',
                                fieldContentNoWrap: false,
                                fieldWidth: 'auto',
                                isSortable: true
                            },
                            connectionAddress: {
                                caption: 'Адрес подключения',
                                fieldContentNoWrap: false,
                                fieldWidth: 'auto',
                                isSortable: true
                            },
                            dnsName: {
                                caption: 'DNS имя хранилища',
                                fieldContentNoWrap: false,
                                fieldWidth: 'auto',
                                isSortable: true
                            },
                            id: {
                                caption: '',
                                fieldWidth: 'auto',
                                fieldContentNoWrap: false,
                                format: this.getButtonsForActions
                            }
                        },
                        pagination: {
                            currentPage: metadata.pageNumber,
                            pageSize: metadata.pageSize,
                            recordsCount: metadata.totalItemCount,
                            totalPages: metadata.pageCount
                        },
                    } }
                    onDataSelect={ this.onDataSelect }
                />
                <SuccessButton
                    onClick={ () => { this.onClickToOpenModal(); } }
                >
                    <i className="fa fa-plus-circle mr-1" />
                    Создать файловое хранилище
                </SuccessButton>
                {
                    this.state.isOpen
                        ? (
                            <FileStorageModalView
                                isOpen={ this.state.isOpen }
                                fileStorageId={ this.state.fileStorageId }
                                onClickToCloseModal={ this.onClickToCloseModal }
                            />
                        )
                        : null
                }
                <WatchReducerProcessActionState
                    watch={ [{
                        reducerStateName: 'TabFileStoragesState',
                        processIds: [
                            TabFileStoragesProcessId.CreateCloudFileStorageServer,
                            TabFileStoragesProcessId.EditCloudFileStorageServer,
                            TabFileStoragesProcessId.DeleteCloudFileStorageServer,
                        ]
                    }] }
                    onProcessActionStateChanged={ this.onProcessActionStateChanged }
                />
                <Dialog
                    isOpen={ this.state.deleteIsOpen }
                    dialogWidth="sm"
                    dialogVerticalAlign="center"
                    onCancelClick={ this.closeModalForDeleteFileStorage }
                    buttons={
                        [
                            {
                                content: 'Нет',
                                kind: 'primary',
                                variant: 'text',
                                onClick: this.closeModalForDeleteFileStorage
                            },
                            {
                                content: 'Да',
                                kind: 'error',
                                variant: 'contained',
                                onClick: this.onClickDeleteFileStorage
                            }
                        ]
                    }
                >
                    <Box display="flex" flexDirection="column" alignItems="center" justifyContent="center">
                        <ErrorOutlineIcon color="warning" sx={ { width: '5em', height: '5em' } } />
                        Вы действительно хотите удалить файловое хранилище?
                    </Box>
                </Dialog>
            </>
        );
    }
}

const FileStoragesTabConnected = connect<StateProps, DispatchProps, NonNullable<unknown>, AppReduxStoreState>(
    state => {
        const TabFileStorageServerState = state.TabFileStoragesState;
        const { cloudFileStorageServers } = TabFileStorageServerState;
        const hasCloudFileStorageServersReceived = TabFileStorageServerState.hasSuccessFor.hasCloudFileStorageServerReceived;

        return {
            cloudFileStorageServers,
            hasCloudFileStorageServersReceived
        };
    },
    {
        dispatchGetCloudFileStorageServersThunk: GetCloudFileStorageServersThunk.invoke,
        dispatchDeleteCloudFileStorageServerThunk: DeleteCloudFileStorageServerThunk.invoke
    }
)(FileStoragesTabClass);

export const FileStoragesTabView = withFloatMessages(FileStoragesTabConnected);