declare const styles: {
  readonly 'search-button': string;
  readonly 'checkbox-container': string;
};
export = styles;