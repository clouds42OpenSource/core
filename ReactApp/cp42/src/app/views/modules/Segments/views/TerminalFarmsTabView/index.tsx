import ErrorOutlineIcon from '@mui/icons-material/ErrorOutline';
import { Box } from '@mui/material';
import { SortingKind } from 'app/common/enums';
import { TabTerminalFarmsProcessId } from 'app/modules/segments/TabTerminalFarms';
import { DeleteTerminalFarmThunkParams } from 'app/modules/segments/TabTerminalFarms/store/reducers/deleteTerminalFarmReducer/params';
import { GetTerminalFarmsThunkParams } from 'app/modules/segments/TabTerminalFarms/store/reducers/getTerminalFarmsReducer/params';
import { DeleteTerminalFarmThunk, GetTerminalFarmsThunk } from 'app/modules/segments/TabTerminalFarms/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { SuccessButton, TextLinkButton } from 'app/views/components/controls/Button';
import { Dialog } from 'app/views/components/controls/Dialog';
import { TextOut } from 'app/views/components/TextOut';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { WatchReducerProcessActionState } from 'app/views/modules/_common/components/WatchReducerProcessActionState';
import { CommonSearchSegmentView } from 'app/views/modules/Segments/common/components';
import { CommonSegmentEnumType } from 'app/views/modules/Segments/types';
import { TerminalFarmsModalView } from 'app/views/modules/Segments/views/TerminalFarmsTabView/views/TerminalFarmsModalView';
import { SelectDataMetadataResponseDto, SortingDataModel } from 'app/web/common';
import { SelectDataCommonDataModel } from 'app/web/common/data-models';
import { CommonSegmentDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import { IReducerProcessActionInfo } from 'core/redux/interfaces';
import { Component, createRef } from 'react';
import { connect } from 'react-redux';
import commonCss from '../../common/styles.module.css';

type TerminalFarmsTabState = {
    /**
     * Поле обозначающая что первая подгрузка данных была сделана
     */
    isFirstTimeLoaded: boolean;

    /**
     * Поле для открытия модального окошка сегмента
     */
    isOpen: boolean;

    /**
     * ID терминальные фермы ТС
     */
    terminalFarmId?: string;
    /**
     * Состояние модального окна на удаление фермы ТС
     */
    deleteIsOpen: boolean;
    orderBy?: string;
};

type TerminalFarmsTabProps = {
    /**
     * Поле для подгрузки данных
     */
    isCanLoad: boolean;
};

type StateProps = {
    /**
     * Массив терминальных ферм тс с пагинацией
     */
    terminalFarms: SelectDataMetadataResponseDto<CommonSegmentDataModel>;

    /**
     * Если true, значит список терминальных ферм тс были получены
     */
    hasTerminalFarmsReceived: boolean;
};

type DispatchProps = {
    dispatchGetTerminalFarmsThunk: (args: GetTerminalFarmsThunkParams) => void;
    dispatchDeleteTerminalFarmThunk: (args: DeleteTerminalFarmThunkParams) => void;
};

type AllProps = TerminalFarmsTabProps & StateProps & DispatchProps & FloatMessageProps;

class TerminalFarmsTabClass extends Component<AllProps, TerminalFarmsTabState> {
    private _searchTerminalFarmsRef = createRef<CommonSearchSegmentView>();

    private _commonTableRef = createRef<CommonTableWithFilter<CommonSegmentDataModel, undefined>>();

    public constructor(props: AllProps) {
        super(props);

        this.state = {
            isFirstTimeLoaded: false,
            isOpen: false,
            deleteIsOpen: false,
            orderBy: ''
        };

        this.getFormatNameTerminalFarm = this.getFormatNameTerminalFarm.bind(this);
        this.getButtonsForActions = this.getButtonsForActions.bind(this);
        this.onDataSelect = this.onDataSelect.bind(this);

        this.onClickToSearch = this.onClickToSearch.bind(this);
        this.onClickToCloseModal = this.onClickToCloseModal.bind(this);
        this.onClickToOpenModal = this.onClickToOpenModal.bind(this);
        this.openModalForDeleteTerminalFarm = this.openModalForDeleteTerminalFarm.bind(this);
        this.onClickDeleteTerminalFarm = this.onClickDeleteTerminalFarm.bind(this);

        this.onProcessActionStateChanged = this.onProcessActionStateChanged.bind(this);
        this.onProcessCreateTerminalFarm = this.onProcessCreateTerminalFarm.bind(this);
        this.onProcessDeleteTerminalFarm = this.onProcessDeleteTerminalFarm.bind(this);
        this.onProcessEditTerminalFarm = this.onProcessEditTerminalFarm.bind(this);
        this.notificationInSuccess = this.notificationInSuccess.bind(this);
        this.notificationInError = this.notificationInError.bind(this);
        this.closeModalForDeleteTerminalFarm = this.closeModalForDeleteTerminalFarm.bind(this);
    }

    public componentDidUpdate() {
        const { isCanLoad, hasTerminalFarmsReceived } = this.props;
        const { isFirstTimeLoaded } = this.state;

        if (isCanLoad && !isFirstTimeLoaded && !hasTerminalFarmsReceived && this._commonTableRef.current) {
            this._commonTableRef.current.ReSelectData();
            this.setState({
                isFirstTimeLoaded: true
            });
        }
    }

    private onProcessActionStateChanged(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        this.onProcessCreateTerminalFarm(processId, actionState, error);
        this.onProcessDeleteTerminalFarm(processId, actionState, error);
        this.onProcessEditTerminalFarm(processId, actionState, error);
    }

    private onProcessCreateTerminalFarm(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId !== TabTerminalFarmsProcessId.CreateTerminalFarm) return;
        this.notificationInSuccess(actionState, 'Ферма ТС успешно создана');
        this.notificationInError(actionState, error?.message);
    }

    private onProcessDeleteTerminalFarm(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId !== TabTerminalFarmsProcessId.DeleteTerminalFarm) return;
        this.notificationInSuccess(actionState, 'Ферма ТС успешно удалена');
        this.notificationInError(actionState, error?.message);
    }

    private onProcessEditTerminalFarm(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId !== TabTerminalFarmsProcessId.EditTerminalFarm) return;
        this.notificationInSuccess(actionState, 'Ферма ТС успешно сохранена');
        this.notificationInError(actionState, error?.message);
    }

    private onClickToOpenModal(terminalFarmId?: string) {
        this.setState({
            isOpen: true,
            terminalFarmId
        });
    }

    private onClickToCloseModal() {
        this.setState({
            isOpen: false
        });
    }

    private onClickToSearch() {
        const {
            connectionAddress,
            description,
            name,

        } = this._searchTerminalFarmsRef.current?.state!;

        this.props.dispatchGetTerminalFarmsThunk({
            filter: {
                connectionAddress,
                description,
                name,
            },
            orderBy: this.state.orderBy,
            pageNumber: 1,
            pageSize: CommonSegmentEnumType.DefaultPageSize,
            force: true
        });
    }

    private onClickDeleteTerminalFarm() {
        if (this.state.terminalFarmId) {
            this.props.dispatchDeleteTerminalFarmThunk({
                terminalFarmId: this.state.terminalFarmId,
                force: true
            });
            this.setState({
                deleteIsOpen: false
            });
        }
    }

    private onDataSelect(args: SelectDataCommonDataModel<undefined>) {
        const {
            connectionAddress,
            description,
            name
        } = this._searchTerminalFarmsRef.current?.state!;

        const orderBy = args.sortingData ? this.getSortQuery(args.sortingData) : 'name.asc';

        this.setState({
            orderBy
        });

        this.props.dispatchGetTerminalFarmsThunk({
            filter: {
                connectionAddress,
                description,
                name,
            },
            pageNumber: args.pageNumber,
            orderBy,
            pageSize: CommonSegmentEnumType.DefaultPageSize,
            force: true
        });
    }

    private getButtonsForActions(Id: string) {
        return (
            <TextLinkButton
                className={ commonCss['delete-link'] }
                onClick={ () => { this.openModalForDeleteTerminalFarm(Id); } }
            >
                <i className="fa fa-trash" />
            </TextLinkButton>
        );
    }

    private getFormatNameTerminalFarm(name: string, data: CommonSegmentDataModel) {
        return (
            <TextLinkButton onClick={ () => { this.onClickToOpenModal(data.id); } }>
                <TextOut fontSize={ 13 } className="text-link">
                    { name }
                </TextOut>
            </TextLinkButton>
        );
    }

    private getSortQuery(args: SortingDataModel) {
        return `${ args.fieldName }.${ args.sortKind ? 'desc' : 'asc' }`;
    }

    private notificationInError(actionState: IReducerProcessActionInfo, message?: string) {
        if (!actionState.isInErrorState) return;
        this.props.floatMessage.show(MessageType.Error, message);
    }

    private notificationInSuccess(actionState: IReducerProcessActionInfo, message: string) {
        if (!actionState.isInSuccessState) return;
        this.props.floatMessage.show(MessageType.Success, message);
        this._commonTableRef.current?.ReSelectData();
        this.onClickToCloseModal();
    }

    private openModalForDeleteTerminalFarm(terminalFarm: string) {
        this.setState({
            terminalFarmId: terminalFarm,
            deleteIsOpen: true
        });
    }

    private closeModalForDeleteTerminalFarm() {
        this.setState({
            deleteIsOpen: false
        });
    }

    public render() {
        const { terminalFarms: { metadata, records } } = this.props;

        return (
            <>
                <CommonSearchSegmentView
                    ref={ this._searchTerminalFarmsRef }
                    onClickToSearch={ this.onClickToSearch }
                />
                <CommonTableWithFilter
                    className={ commonCss['custom-table'] }
                    ref={ this._commonTableRef }
                    isResponsiveTable={ true }
                    isBorderStyled={ true }
                    uniqueContextProviderStateId="TabTerminalFarmsContextProviderStateId"
                    tableProps={ {
                        dataset: records,
                        keyFieldName: 'id',
                        sorting: {
                            sortFieldName: 'name',
                            sortKind: SortingKind.Asc
                        },
                        fieldsView: {
                            name: {
                                caption: 'Название',
                                fieldWidth: 'auto',
                                fieldContentNoWrap: false,
                                isSortable: true,
                                format: this.getFormatNameTerminalFarm
                            },
                            description: {
                                caption: 'Описание',
                                fieldWidth: 'auto',
                                fieldContentNoWrap: false,
                                isSortable: true
                            },
                            connectionAddress: {
                                caption: 'Адрес подключения',
                                fieldWidth: 'auto',
                                fieldContentNoWrap: false,
                                isSortable: true
                            },
                            id: {
                                caption: '',
                                fieldWidth: 'auto',
                                format: this.getButtonsForActions
                            }
                        },
                        pagination: {
                            currentPage: metadata.pageNumber,
                            pageSize: metadata.pageSize,
                            recordsCount: metadata.totalItemCount,
                            totalPages: metadata.pageCount
                        },
                    } }
                    onDataSelect={ this.onDataSelect }
                />
                <SuccessButton
                    onClick={ () => { this.onClickToOpenModal(); } }
                >
                    Создать ферму ТС
                </SuccessButton>
                {
                    this.state.isOpen
                        ? (
                            <TerminalFarmsModalView
                                isOpen={ this.state.isOpen }
                                terminalFarmId={ this.state.terminalFarmId }
                                onClickToCloseModal={ this.onClickToCloseModal }
                            />
                        )
                        : null
                }
                <WatchReducerProcessActionState
                    watch={ [{
                        reducerStateName: 'TabTerminalFarmsState',
                        processIds: [
                            TabTerminalFarmsProcessId.CreateTerminalFarm,
                            TabTerminalFarmsProcessId.DeleteTerminalFarm,
                            TabTerminalFarmsProcessId.EditTerminalFarm,
                        ]
                    }] }
                    onProcessActionStateChanged={ this.onProcessActionStateChanged }
                />
                <Dialog
                    isOpen={ this.state.deleteIsOpen }
                    dialogWidth="sm"
                    dialogVerticalAlign="center"
                    onCancelClick={ this.closeModalForDeleteTerminalFarm }
                    buttons={
                        [
                            {
                                content: 'Нет',
                                kind: 'primary',
                                variant: 'text',
                                onClick: this.closeModalForDeleteTerminalFarm
                            },
                            {
                                content: 'Да',
                                kind: 'error',
                                variant: 'contained',
                                onClick: this.onClickDeleteTerminalFarm
                            }
                        ]
                    }
                >
                    <Box display="flex" flexDirection="column" alignItems="center" justifyContent="center">
                        <ErrorOutlineIcon color="warning" sx={ { width: '5em', height: '5em' } } />
                        Вы действительно хотите удалить ферму ТС?
                    </Box>
                </Dialog>
            </>
        );
    }
}

const TerminalFarmsTabConnected = connect<StateProps, DispatchProps, {}, AppReduxStoreState>(
    state => {
        const tabTerminalFarmsState = state.TabTerminalFarmsState;
        const { terminalFarms } = tabTerminalFarmsState;
        const { hasTerminalFarmsReceived } = tabTerminalFarmsState.hasSuccessFor;

        return {
            terminalFarms,
            hasTerminalFarmsReceived
        };
    },
    {
        dispatchGetTerminalFarmsThunk: GetTerminalFarmsThunk.invoke,
        dispatchDeleteTerminalFarmThunk: DeleteTerminalFarmThunk.invoke
    }
)(TerminalFarmsTabClass);

export const TerminalFarmsTabView = withFloatMessages(TerminalFarmsTabConnected);