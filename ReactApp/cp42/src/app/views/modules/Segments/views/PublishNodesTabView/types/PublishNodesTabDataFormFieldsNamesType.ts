import { PublishNodesTabDataForm } from 'app/views/modules/Segments/views/PublishNodesTabView/types';

export type PublishNodesTabDataFormFieldsNamesType = keyof PublishNodesTabDataForm;