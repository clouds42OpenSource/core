import { Box, Chip, Tooltip } from '@mui/material';
import cn from 'classnames';
import dayjs from 'dayjs';
import React, { memo, useEffect, useMemo, useState } from 'react';

import { FETCH_API } from 'app/api/useFetchApi';
import { REDUX_API } from 'app/api/useReduxApi';
import { SortingKind } from 'app/common/enums';
import { EMessageType, useAppDispatch, useAppSelector, useFloatMessages } from 'app/hooks';
import { SuccessButton } from 'app/views/components/controls/Button';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { SelectDataCommonDataModel } from 'app/web/common';
import { TInstallLimitsFilters } from 'app/api/endpoints/link42/request';
import { TInstallLimits } from 'app/api/endpoints/link42/response';

import styles from './style.module.css';

import { LinkVersionDialog, LinkVersionSearch } from './widgets';

const { getInstallLimitsList } = REDUX_API.LINK42_REDUX;
const { deleteLinkConfiguration } = FETCH_API.LINK42;

export const Link42View = memo(() => {
    const { data, error, params } = useAppSelector(state => state.Link42State.installLimitsListReducer);
    const dispatch = useAppDispatch();
    const { show } = useFloatMessages();

    const [dialogData, setDialogData] = useState<[boolean, TInstallLimits | undefined]>([false, undefined]);

    const isSortable = useMemo(() => !!data?.records.length && data.records.length > 1, [data?.records.length]);

    useEffect(() => {
        getInstallLimitsList(dispatch);
    }, [dispatch]);

    useEffect(() => {
        if (error) {
            show(EMessageType.error, error);
        }
    }, [show, error]);

    const deleteVersionHandler = (id: string) => async () => {
        const { message } = await deleteLinkConfiguration(id);

        if (message) {
            show(EMessageType.error, message);
        } else {
            getInstallLimitsList(dispatch, params);
        }
    };

    const editVersionHandler = (versionData: TInstallLimits) => () => {
        setDialogData([true, versionData]);
    };

    const createVersionHandler = () => {
        setDialogData([true, undefined]);
    };

    const onCancelClickHandler = () => {
        setDialogData([false, undefined]);
    };

    const onDataSelectHandler = (args: SelectDataCommonDataModel<TInstallLimitsFilters>) => {
        getInstallLimitsList(dispatch, {
            pageNumber: args.pageNumber,
            orderBy: args.sortingData ? `${ args.sortingData.fieldName }.${ args.sortingData.sortKind === SortingKind.Asc ? 'asc' : 'desc' }` : undefined,
            filter: params.filter
        });
    };

    return (
        <>
            <LinkVersionDialog isOpen={ dialogData[0] } data={ dialogData[1] } onCancelClick={ onCancelClickHandler } />
            <Box display="flex" flexDirection="column" gap="15px">
                <LinkVersionSearch />
                <CommonTableWithFilter
                    onDataSelect={ onDataSelectHandler }
                    isResponsiveTable={ true }
                    isBorderStyled={ true }
                    uniqueContextProviderStateId="installLimitsList"
                    tableProps={ {
                        emptyText: 'Зарегистированных версий не найдено',
                        pagination: {
                            totalPages: data?.metadata.pageCount ?? 1,
                            currentPage: data?.metadata.pageNumber ?? 1,
                            pageSize: data?.metadata.pageSize ?? 10,
                            recordsCount: data?.metadata.totalItemCount ?? 0,
                        },
                        dataset: data?.records ?? [],
                        keyFieldName: 'id',
                        fieldsView: {
                            version: {
                                caption: 'Версия',
                                isSortable,
                                format: (value: string, row) =>
                                    <Chip label={ value } className={ styles.chip } color={ row.isCurrentVersion ? 'primary' : 'default' } />
                            },
                            createdOn: {
                                caption: 'Создано',
                                isSortable,
                                format: (value: string) =>
                                    dayjs(value).format('DD.MM.YYYY HH:mm')
                            },
                            updatedOn: {
                                caption: 'Отредактировано',
                                isSortable,
                                format: (value: string | null) =>
                                    value ? dayjs(value).format('DD.MM.YYYY HH:mm') : '---'
                            },
                            downloadsCount: {
                                caption: 'Количество скачиваний',
                                isSortable,
                                format: (value: number) =>
                                    <><i className="fa fa-user" aria-hidden="true" />{ ' ' }{ value }</>
                            },
                            downloadsLimit: {
                                caption: 'Лимит скачиваний',
                                isSortable,
                                format: (value: number) =>
                                    <><i className="fa fa-user" aria-hidden="true" />{ ' ' }{ value }</>
                            },
                            id: {
                                caption: 'Действия',
                                format: (value: string, row) => (
                                    <Box className={ styles.actions } display="flex" gap="10px">
                                        <Tooltip title="Удалить">
                                            <i onClick={ deleteVersionHandler(value) } className={ cn('fa fa-trash-o', styles.delete) } aria-hidden="true" />
                                        </Tooltip>
                                        <Tooltip title="Редактировать">
                                            <i onClick={ editVersionHandler(row) } className={ cn('fa fa-pencil-square-o', styles.edit) } aria-hidden="true" />
                                        </Tooltip>
                                    </Box>
                                )
                            }
                        }
                    } }
                />
                <SuccessButton className={ styles.button } onClick={ createVersionHandler }>
                    <i className="fa fa-plus-circle mr-1" />
                    Создать версию
                </SuccessButton>
            </Box>
        </>
    );
});