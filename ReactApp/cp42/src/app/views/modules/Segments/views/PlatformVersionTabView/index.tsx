import ErrorOutlineIcon from '@mui/icons-material/ErrorOutline';
import { Box } from '@mui/material';
import { SortingKind } from 'app/common/enums';
import { TabPlatformVersionProcessId } from 'app/modules/segments/TabPlatformVersion';
import { DeletePlatformVersionThunkParams } from 'app/modules/segments/TabPlatformVersion/store/reducer/deletePlatformVersionReducer/params';
import { GetPlatformVersionsThunkParams } from 'app/modules/segments/TabPlatformVersion/store/reducer/getPlatformVersionsReducer/params';
import { DeletePlatformVersionThunk, GetPlatformVersionsThunk } from 'app/modules/segments/TabPlatformVersion/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { SuccessButton, TextLinkButton } from 'app/views/components/controls/Button';
import { Dialog } from 'app/views/components/controls/Dialog';
import { TextOut } from 'app/views/components/TextOut';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { WatchReducerProcessActionState } from 'app/views/modules/_common/components/WatchReducerProcessActionState';
import { CommonSegmentEnumType } from 'app/views/modules/Segments/types';
import { PlatformVersionModalView } from 'app/views/modules/Segments/views/PlatformVersionTabView/views/PlatformVersionModalView';
import { SearchPlatformVersionsView } from 'app/views/modules/Segments/views/PlatformVersionTabView/views/SearchPlatformVersionsView';
import { SelectDataMetadataResponseDto, SortingDataModel } from 'app/web/common';
import { SelectDataCommonDataModel } from 'app/web/common/data-models';
import { PlatformVersion1CDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabPlatformVersionApiProxy';
import { IReducerProcessActionInfo } from 'core/redux/interfaces';
import { Component, createRef } from 'react';
import { connect } from 'react-redux';
import commonCss from '../../common/styles.module.css';

type PlatformVersionTabState = {
    /**
     * Поле обозначающая что первая подгрузка данных была сделана
     */
    isFirstTimeLoaded: boolean;

    /**
     * Поле для открытия модального окошка
     */
    isOpen: boolean;

    /**
     * Версия платформы
     */
    platformVersion?: string;
    /**
     * Состояние модального окна для удаления версии платформы
     */
    deleteIsOpen: boolean;
    orderBy?: string;
};

type PlatformVersionTabProps = {
    /**
     * Поле для подгрузки данных
     */
    isCanLoad: boolean;
};

type StateProps = {
    /**
     * Список данных списка версии платформы 1С
     */
    platformVersions: SelectDataMetadataResponseDto<PlatformVersion1CDataModel>;

    /**
     * Если true, значит список версии платформы 1С получены
     */
    hasPlatformVersionsReceived: boolean;
};

type DispatchProps = {
    dispatchGetPlatformVersionsThunk: (args: GetPlatformVersionsThunkParams) => void;
    dispatchDeletePlatformVersionThunk: (args: DeletePlatformVersionThunkParams) => void;
};

type AllProps = PlatformVersionTabProps & StateProps & DispatchProps & FloatMessageProps;

class PlatformVersionTabClass extends Component<AllProps, PlatformVersionTabState> {
    private _commonTableRef = createRef<CommonTableWithFilter<PlatformVersion1CDataModel, undefined>>();

    private _searchPlatformVersionsRef = createRef<SearchPlatformVersionsView>();

    public constructor(props: AllProps) {
        super(props);

        this.state = {
            isOpen: false,
            isFirstTimeLoaded: false,
            deleteIsOpen: false,
            orderBy: ''
        };

        this.onClickToOpenModal = this.onClickToOpenModal.bind(this);
        this.onClickToCloseModal = this.onClickToCloseModal.bind(this);
        this.onClickToSearch = this.onClickToSearch.bind(this);
        this.onDataSelect = this.onDataSelect.bind(this);
        this.getFormatPlatformVersion = this.getFormatPlatformVersion.bind(this);
        this.notificationInSuccess = this.notificationInSuccess.bind(this);
        this.notificationInError = this.notificationInError.bind(this);
        this.notificationCreateInError = this.notificationCreateInError.bind(this);
        this.openModalForDeletePlatformVersion = this.openModalForDeletePlatformVersion.bind(this);
        this.getButtonsForActions = this.getButtonsForActions.bind(this);

        this.onProcessActionStateChanged = this.onProcessActionStateChanged.bind(this);
        this.onProcessCreatePlatformVersion = this.onProcessCreatePlatformVersion.bind(this);
        this.onProcessEditPlatformVersion = this.onProcessEditPlatformVersion.bind(this);
        this.onProcessDeletePlatformVersion = this.onProcessDeletePlatformVersion.bind(this);
        this.onClickDeletePlatformVersion = this.onClickDeletePlatformVersion.bind(this);
        this.closeModalForDeletePlatformVersion = this.closeModalForDeletePlatformVersion.bind(this);
    }

    public componentDidUpdate() {
        const { isCanLoad, hasPlatformVersionsReceived } = this.props;
        const { isFirstTimeLoaded } = this.state;

        if (isCanLoad && !isFirstTimeLoaded && !hasPlatformVersionsReceived && this._commonTableRef.current) {
            this._commonTableRef.current.ReSelectData();
            this.setState({
                isFirstTimeLoaded: true
            });
        }
    }

    private onClickDeletePlatformVersion() {
        if (this.state.platformVersion) {
            this.props.dispatchDeletePlatformVersionThunk({
                platformVersion: this.state.platformVersion,
                force: true
            });
            this.setState({
                deleteIsOpen: false
            });
        }
    }

    private onProcessActionStateChanged(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        this.onProcessCreatePlatformVersion(processId, actionState, error);
        this.onProcessEditPlatformVersion(processId, actionState, error);
        this.onProcessDeletePlatformVersion(processId, actionState, error);
    }

    private onProcessCreatePlatformVersion(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId !== TabPlatformVersionProcessId.CreatePlatformVersion) return;
        this.notificationInSuccess(actionState, 'Версия платформы успешно создана');
        this.notificationCreateInError(actionState, error?.message);
    }

    private onProcessEditPlatformVersion(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId !== TabPlatformVersionProcessId.EditPlatformVersion) return;
        this.notificationInSuccess(actionState, 'Версия платформы успешно сохранена');
        this.notificationInError(actionState, error?.message);
    }

    private onProcessDeletePlatformVersion(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId !== TabPlatformVersionProcessId.DeletePlatformVersion) return;
        this.notificationInSuccess(actionState, 'Версия платформы успешно удалена');
        this.notificationInError(actionState, error?.message);
    }

    private onClickToOpenModal(platformVersion?: string) {
        this.setState({
            isOpen: true,
            platformVersion
        });
    }

    private onClickToCloseModal() {
        this.setState({
            isOpen: false
        });
    }

    private onClickToSearch() {
        const {
            version,
            pathToPlatform,
            pathToPlatformX64
        } = this._searchPlatformVersionsRef.current?.state!;

        this.props.dispatchGetPlatformVersionsThunk({
            filter: {
                pathToPlatform,
                pathToPlatformX64,
                version,
            },
            pageNumber: 1,
            orderBy: this.state.orderBy,
            pageSize: CommonSegmentEnumType.DefaultPageSize,
            force: true
        });
    }

    private onDataSelect(args: SelectDataCommonDataModel<undefined>) {
        const {
            version,
            pathToPlatform,
            pathToPlatformX64
        } = this._searchPlatformVersionsRef.current?.state!;

        const orderBy = args.sortingData ? this.getSortQuery(args.sortingData) : 'version.asc';

        this.setState({
            orderBy
        });

        this.props.dispatchGetPlatformVersionsThunk({
            filter: {
                pathToPlatform,
                pathToPlatformX64,
                version,
            },
            pageNumber: args.pageNumber,
            orderBy,
            pageSize: CommonSegmentEnumType.DefaultPageSize,
            force: true
        });
    }

    private getSortQuery(args: SortingDataModel) {
        return `${ args.fieldName }.${ args.sortKind ? 'desc' : 'asc' }`;
    }

    private getButtonsForActions(_value: string, data: PlatformVersion1CDataModel) {
        return (
            <TextLinkButton
                className={ commonCss['delete-link'] }
                onClick={ () => { this.openModalForDeletePlatformVersion(data.version); } }
            >
                <i className="fa fa-trash" />
            </TextLinkButton>
        );
    }

    private getFormatPlatformVersion(version: string) {
        return (
            <TextLinkButton onClick={ () => { this.onClickToOpenModal(version); } }>
                <TextOut fontSize={ 13 } className="text-link">
                    { version }
                </TextOut>
            </TextLinkButton>
        );
    }

    private openModalForDeletePlatformVersion(platformVersion: string) {
        this.setState({
            platformVersion,
            deleteIsOpen: true
        });
    }

    private notificationInSuccess(actionState: IReducerProcessActionInfo, message: string) {
        if (!actionState.isInSuccessState) return;
        this.props.floatMessage.show(MessageType.Success, message);
        this._commonTableRef.current?.ReSelectData();
        this.onClickToCloseModal();
    }

    private notificationInError(actionState: IReducerProcessActionInfo, message?: string) {
        if (!actionState.isInErrorState) return;
        this.props.floatMessage.show(MessageType.Error, message);
    }

    private notificationCreateInError(actionState: IReducerProcessActionInfo, message?: string) {
        if (!actionState.isInErrorState) return;
        this.props.floatMessage.show(MessageType.Warning, message);
    }

    private closeModalForDeletePlatformVersion() {
        this.setState({
            deleteIsOpen: false
        });
    }

    public render() {
        const { platformVersions: { metadata, records } } = this.props;

        return (
            <>
                <SearchPlatformVersionsView
                    ref={ this._searchPlatformVersionsRef }
                    onClickToSearch={ this.onClickToSearch }
                />
                <CommonTableWithFilter
                    ref={ this._commonTableRef }
                    isResponsiveTable={ true }
                    className={ commonCss['custom-table'] }
                    isBorderStyled={ true }
                    uniqueContextProviderStateId="TabPlatformVersionsContextProviderStateId"
                    tableProps={ {
                        dataset: records,
                        keyFieldName: 'version',
                        sorting: {
                            sortFieldName: 'version',
                            sortKind: SortingKind.Asc
                        },
                        fieldsView: {
                            version: {
                                caption: 'Версия 1С',
                                fieldWidth: 'auto',
                                fieldContentNoWrap: false,
                                isSortable: true,
                                format: this.getFormatPlatformVersion
                            },
                            pathToPlatform: {
                                caption: 'Путь к платформе',
                                fieldWidth: 'auto',
                                fieldContentNoWrap: false,
                                isSortable: true,
                            },
                            pathToPlatformX64: {
                                caption: 'Путь к платформе x64',
                                fieldWidth: 'auto',
                                fieldContentNoWrap: false,
                                isSortable: true,
                            },
                            macOsThinClientDownloadLink: {
                                caption: '',
                                fieldWidth: 'auto',
                                fieldContentNoWrap: false,
                                format: this.getButtonsForActions
                            }
                        },
                        pagination: {
                            currentPage: metadata.pageNumber,
                            pageSize: metadata.pageSize,
                            recordsCount: metadata.totalItemCount,
                            totalPages: metadata.pageCount
                        },
                    } }
                    onDataSelect={ this.onDataSelect }
                />
                <SuccessButton
                    onClick={ () => { this.onClickToOpenModal(); } }
                >
                    <i className="fa fa-plus-circle mr-1" />
                    Создать версию платформы
                </SuccessButton>
                {
                    this.state.isOpen
                        ? (
                            <PlatformVersionModalView
                                isOpen={ this.state.isOpen }
                                version={ this.state.platformVersion }
                                onClickToCloseModal={ this.onClickToCloseModal }
                            />
                        )
                        : null
                }
                <WatchReducerProcessActionState
                    watch={ [{
                        reducerStateName: 'TabPlatformVersionState',
                        processIds: [
                            TabPlatformVersionProcessId.CreatePlatformVersion,
                            TabPlatformVersionProcessId.EditPlatformVersion,
                            TabPlatformVersionProcessId.DeletePlatformVersion
                        ]
                    }] }
                    onProcessActionStateChanged={ this.onProcessActionStateChanged }
                />
                <Dialog
                    isOpen={ this.state.deleteIsOpen }
                    dialogWidth="sm"
                    dialogVerticalAlign="center"
                    onCancelClick={ this.closeModalForDeletePlatformVersion }
                    buttons={
                        [
                            {
                                content: 'Нет',
                                kind: 'primary',
                                variant: 'text',
                                onClick: this.closeModalForDeletePlatformVersion
                            },
                            {
                                content: 'Да',
                                kind: 'error',
                                variant: 'contained',
                                onClick: this.onClickDeletePlatformVersion
                            }
                        ]
                    }
                >
                    <Box display="flex" flexDirection="column" alignItems="center" justifyContent="center">
                        <ErrorOutlineIcon color="warning" sx={ { width: '5em', height: '5em' } } />
                        Вы действительно хотите удалить версию платформы?
                    </Box>
                </Dialog>
            </>
        );
    }
}

const PlatformVersionTabConnected = connect<StateProps, DispatchProps, NonNullable<unknown>, AppReduxStoreState>(
    state => {
        const tabPlatformVersionState = state.TabPlatformVersionState;
        const { platformVersions } = tabPlatformVersionState;
        const { hasPlatformVersionsReceived } = tabPlatformVersionState.hasSuccessFor;

        return {
            platformVersions,
            hasPlatformVersionsReceived
        };
    },
    {
        dispatchGetPlatformVersionsThunk: GetPlatformVersionsThunk.invoke,
        dispatchDeletePlatformVersionThunk: DeletePlatformVersionThunk.invoke
    }
)(PlatformVersionTabClass);

export const PlatformVersionTabView = withFloatMessages(PlatformVersionTabConnected);