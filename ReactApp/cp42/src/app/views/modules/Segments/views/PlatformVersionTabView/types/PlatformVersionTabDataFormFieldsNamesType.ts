import { PlatformVersionTabDataForm } from 'app/views/modules/Segments/views/PlatformVersionTabView/types';

export type PlatformVersionTabDataFormFieldsNamesType = keyof PlatformVersionTabDataForm;