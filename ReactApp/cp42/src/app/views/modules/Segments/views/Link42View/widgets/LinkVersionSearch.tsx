import { OutlinedInput } from '@mui/material';
import { REDUX_API } from 'app/api/useReduxApi';
import { useAppDispatch } from 'app/hooks';
import { SearchByFilterButton } from 'app/views/components/controls/Button';

import { CheckBoxForm } from 'app/views/components/controls/forms';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import cn from 'classnames';
import { ChangeEvent, memo, useState } from 'react';

import styles from '../style.module.css';

const { getInstallLimitsList } = REDUX_API.LINK42_REDUX;

export const LinkVersionSearch = memo(() => {
    const dispatch = useAppDispatch();

    const [isCurrentVersion, setIsCurrentVersion] = useState(false);
    const [version, setVersion] = useState('');
    const [downloadsCount, setDownloadsCount] = useState<number>();
    const [downloadsLimit, setDownloadsLimit] = useState<number>();

    const versionHandler = (ev: ChangeEvent<HTMLInputElement>) => {
        setVersion(ev.target.value);
    };

    const downloadsCountHandler = (ev: ChangeEvent<HTMLInputElement>) => {
        setDownloadsCount(Number.parseInt(ev.target.value, 10));
    };

    const downloadsLimitHandler = (ev: ChangeEvent<HTMLInputElement>) => {
        setDownloadsLimit(Number.parseInt(ev.target.value, 10));
    };

    const currentVersionHandler = (_: string, value: boolean) => {
        setIsCurrentVersion(value);

        getInstallLimitsList(dispatch, { pageNumber: 1, filter: { isCurrentVersion: value } });
    };

    const searchByFilterHandler = () => {
        getInstallLimitsList(dispatch, {
            pageNumber: 1,
            filter: {
                downloadsCount,
                downloadsLimit,
                version
            }
        });
    };

    return (
        <>
            <CheckBoxForm
                className={ styles.checkbox }
                isChecked={ isCurrentVersion }
                onValueChange={ currentVersionHandler }
                label="Только текущая версия"
                formName="isCurrentVersion"
            />
            <div
                className={ cn({
                    [styles.header]: !isCurrentVersion,
                    [styles.hiddenHeader]: isCurrentVersion
                }) }
            >
                <FormAndLabel label="Версия">
                    <OutlinedInput placeholder="Введите версию" value={ version } onChange={ versionHandler } fullWidth={ true } />
                </FormAndLabel>
                <FormAndLabel label="Количество скачиваний">
                    <OutlinedInput placeholder="Введите количество скачиваний" type="number" value={ downloadsCount } onChange={ downloadsCountHandler } fullWidth={ true } />
                </FormAndLabel>
                <FormAndLabel label="Лимит скачиваний">
                    <OutlinedInput placeholder="Введите лимит скачиваний" type="number" value={ downloadsLimit } onChange={ downloadsLimitHandler } fullWidth={ true } />
                </FormAndLabel>
                <SearchByFilterButton onClick={ searchByFilterHandler } />
            </div>
        </>
    );
});