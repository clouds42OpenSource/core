export const requiredMessage = 'Поле обязательно к заполнению';

export const accordionSummarySx = {
    '.MuiAccordionSummary-content': { margin: 0 },
    '.MuiAccordionSummary-content.Mui-expanded': { margin: 0 },
    minHeight: '35px',
    '&.Mui-expanded': { minHeight: '35px' }
};

export const accordionSx = {
    '&.Mui-expanded': { margin: 0 },
    minHeight: '35px'
};