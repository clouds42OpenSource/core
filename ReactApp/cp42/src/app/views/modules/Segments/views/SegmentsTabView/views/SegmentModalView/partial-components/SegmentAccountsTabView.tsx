import { Box } from '@mui/material';
import { GetSegmentAccountsThunkParams } from 'app/modules/segments/TabSegments/store/reducers/getSegmentAccountsReducer/params';
import { OutlinedButton } from 'app/views/components/controls/Button';
import { TextBoxForm } from 'app/views/components/controls/forms';
import { CommonSegmentEnumType } from 'app/views/modules/Segments/types';
import { SegmentModalDataForm, SegmentModalDataFormFieldsNamesType } from 'app/views/modules/Segments/views/SegmentsTabView/types';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { SegmentAccountDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabSegmentsApiProxy';
import { SelectDataMetadataResponseDto } from 'app/web/common';
import { SelectDataCommonDataModel, SortingDataModel } from 'app/web/common/data-models';
import cn from 'classnames';
import { Component } from 'react';
import css from '../styles.module.css';

type SegmentAccountsTabProps = {
    formFields: SegmentModalDataForm;
    segmentAccounts: SelectDataMetadataResponseDto<SegmentAccountDataModel>;
    dispatchGetSegmentAccountsThunk: (args: GetSegmentAccountsThunkParams) => void;
    onValueChange<TValue, TForm extends string = SegmentModalDataFormFieldsNamesType>(formName: TForm, newValue: TValue): void;
};

export class SegmentAccountsTabView extends Component<SegmentAccountsTabProps> {
    public constructor(props: SegmentAccountsTabProps) {
        super(props);

        this.onDataSelect = this.onDataSelect.bind(this);
        this.onClickSearch = this.onClickSearch.bind(this);
    }

    private onClickSearch() {
        const { accountName, accountNumber, segmentId } = this.props.formFields;

        this.props.dispatchGetSegmentAccountsThunk({
            segmentId,
            accountCaption: accountName,
            accountIndexNumber: accountNumber && !Number.isNaN(+accountNumber) ? Number(accountNumber) : undefined,
            pageNumber: 1,
            pageSize: CommonSegmentEnumType.DefaultPageSize,
            force: true
        });
    }

    private onDataSelect(args: SelectDataCommonDataModel<undefined>) {
        const { accountName, accountNumber, segmentId } = this.props.formFields;

        this.props.dispatchGetSegmentAccountsThunk({
            segmentId,
            accountCaption: accountName,
            accountIndexNumber: accountNumber && !Number.isNaN(+accountNumber) ? Number(accountNumber) : undefined,
            pageNumber: args.pageNumber,
            pageSize: CommonSegmentEnumType.DefaultPageSize,
            orderBy: args.sortingData ? this.getSortQuery(args.sortingData) : 'accountCaption.asc',
            force: true
        });
    }

    private getSortQuery(args: SortingDataModel) {
        return `${ args.fieldName }.${ args.sortKind ? 'desc' : 'asc' }`;
    }

    public render() {
        const { accountName, accountNumber } = this.props.formFields;
        const { metadata, records } = this.props.segmentAccounts;

        return (
            <Box display="flex" flexDirection="column" gap="12px">
                <TextBoxForm
                    formName="accountName"
                    label="Название аккаунта"
                    placeholder="Введите название"
                    value={ accountName }
                    onValueChange={ this.props.onValueChange }
                />
                <TextBoxForm
                    formName="accountNumber"
                    label="Номер аккаунта"
                    placeholder="Введите номер"
                    value={ accountNumber }
                    onValueChange={ this.props.onValueChange }
                />
                <OutlinedButton
                    kind="success"
                    className={ cn(css['search-button']) }
                    onClick={ this.onClickSearch }
                >
                    <i className="fa fa-search mr-1" />
                    Поиск
                </OutlinedButton>
                <CommonTableWithFilter
                    uniqueContextProviderStateId="SegmentAccountsTabContextProviderStateId"
                    tableProps={ {
                        dataset: records,
                        keyFieldName: 'indexNumber',
                        fieldsView: {
                            indexNumber: {
                                caption: '№',
                                fieldWidth: '8%',
                                isSortable: true,
                                fieldContentNoWrap: false
                            },
                            accountCaption: {
                                caption: 'Аккаунт',
                                fieldWidth: '32%',
                                isSortable: true,
                                fieldContentNoWrap: false
                            },
                            sizeOfFileAccountDatabases: {
                                caption: 'Размер файловых баз(мб)',
                                fieldWidth: '20%',
                                isSortable: true,
                                fieldContentNoWrap: false,
                                format: value => {
                                    return value ?? 0;
                                }
                            },
                            sizeOfServerAccountDatabases: {
                                caption: 'Размер серверных баз(мб)',
                                fieldWidth: '20%',
                                isSortable: true,
                                fieldContentNoWrap: false,
                                format: value => {
                                    return value ?? 0;
                                }
                            },
                            sizeOfClientFiles: {
                                caption: 'Размер клиентских файлов(мб)',
                                fieldWidth: '20%',
                                isSortable: true,
                                fieldContentNoWrap: false,
                                format: value => {
                                    return value ?? 0;
                                }
                            }
                        },
                        pagination: {
                            currentPage: metadata.pageNumber,
                            pageSize: metadata.pageSize,
                            recordsCount: metadata.totalItemCount,
                            totalPages: metadata.pageCount
                        },
                    } }
                    onDataSelect={ this.onDataSelect }
                />
            </Box>
        );
    }
}