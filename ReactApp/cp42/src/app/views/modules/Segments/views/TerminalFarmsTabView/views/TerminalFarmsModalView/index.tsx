import { PageEnumType } from 'app/common/enums';
import { checkOnValidField, checkValidOfStringField } from 'app/common/functions';
import { IErrorsType } from 'app/common/interfaces';
import { TabTerminalFarmsProcessId } from 'app/modules/segments/TabTerminalFarms';
import { CreateTerminalFarmThunkParams } from 'app/modules/segments/TabTerminalFarms/store/reducers/createTerminalFarmReducer/params';
import { EditTerminalFarmThunkParams } from 'app/modules/segments/TabTerminalFarms/store/reducers/editTerminalFarmReducer/params';
import { GetTerminalFarmThunkParams } from 'app/modules/segments/TabTerminalFarms/store/reducers/getTerminalFarmReducer/params';
import { CreateTerminalFarmThunk, EditTerminalFarmThunk, GetTerminalFarmThunk } from 'app/modules/segments/TabTerminalFarms/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { Dialog } from 'app/views/components/controls/Dialog';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { fillCommonSegmentDataModel, getCommonSegmentDialogBody, getDialogButtons } from 'app/views/modules/Segments/common/functions';
import { CommonSegmentDataForm, CommonSegmentDataFormFieldsNamesType, CommonSegmentLengthEnumType } from 'app/views/modules/Segments/types';
import { WatchReducerProcessActionState } from 'app/views/modules/_common/components/WatchReducerProcessActionState';
import { CommonSegmentDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import { IReducerProcessActionInfo } from 'core/redux/interfaces';
import React, { Component } from 'react';
import { connect } from 'react-redux';

type OwnProps = ReduxFormProps<CommonSegmentDataForm>;

type TerminalFarmsModalProps = {
    /**
     * Поле для открытия модального окошка терминальной фирмы ТС
     */
    isOpen: boolean;

    /**
     * ID терминальной фирмы ТС
     */
    terminalFarmId?: string;

    /**
     * Функция на закрытие модального окошка
     */
    onClickToCloseModal: () => void;
};

type TerminalFarmsModalState = {
    /**
     * Обьект Ошибок для валидации
     */
    errors: IErrorsType;

    /**
     * Тип страницы для динамического компонента редактирования/создания
     */
    pageType: PageEnumType;
};

type StateProps = {
    /**
     * Модель фермы ТС
     */
    terminalFarm: CommonSegmentDataModel;
};

type DispatchProps = {
    dispatchCreateTerminalFarmThunk: (args: CreateTerminalFarmThunkParams) => void;
    dispatchGetTerminalFarmThunk: (args: GetTerminalFarmThunkParams) => void;
    dispatchEditTerminalFarmThunk: (args: EditTerminalFarmThunkParams) => void;
};

type AllProps = TerminalFarmsModalProps & OwnProps & StateProps & DispatchProps & FloatMessageProps;

class TerminalFarmsModalClass extends Component<AllProps, TerminalFarmsModalState> {
    public constructor(props: AllProps) {
        super(props);

        this.state = {
            errors: {},
            pageType: this.props.terminalFarmId ? PageEnumType.Edit : PageEnumType.Create
        };

        this.getDialogBody = this.getDialogBody.bind(this);
        this.onValueChange = this.onValueChange.bind(this);
        this.submitChanges = this.submitChanges.bind(this);
        this.createTerminalFarm = this.createTerminalFarm.bind(this);
        this.editTerminalFarm = this.editTerminalFarm.bind(this);
        this.isTerminalFarmsFormValid = this.isTerminalFarmsFormValid.bind(this);

        this.onProcessActionStateChanged = this.onProcessActionStateChanged.bind(this);
        this.onProcessGetTerminalFarm = this.onProcessGetTerminalFarm.bind(this);
        this.successGetTerminalFarmState = this.successGetTerminalFarmState.bind(this);
        this.errorGetTerminalFarmState = this.errorGetTerminalFarmState.bind(this);

        this.props.reduxForm.setInitializeFormDataAction(this.defaultInitDataReduxForm);
    }

    public componentDidMount() {
        if (this.props.terminalFarmId) {
            this.props.dispatchGetTerminalFarmThunk({
                terminalFarmId: this.props.terminalFarmId,
                force: true
            });
        }
    }

    private onProcessActionStateChanged(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        this.onProcessGetTerminalFarm(processId, actionState, error);
    }

    private onProcessGetTerminalFarm(processId: string, actionState: IReducerProcessActionInfo, _?: Error) {
        if (processId !== TabTerminalFarmsProcessId.GetTerminalFarm) {
            return;
        }

        this.successGetTerminalFarmState(actionState);
        this.errorGetTerminalFarmState(actionState);
    }

    private onValueChange<TValue, TForm extends string = CommonSegmentDataFormFieldsNamesType>(formName: TForm, newValue: TValue) {
        this.props.reduxForm.updateReduxFormFields({
            [formName]: newValue
        });

        const { errors } = this.state;

        if (typeof newValue === 'string') {
            switch (formName) {
                case 'name':
                    errors[formName] = checkValidOfStringField(newValue, 'Заполните название', CommonSegmentLengthEnumType.Name);
                    break;
                case 'description':
                    errors[formName] = checkValidOfStringField(newValue, undefined, CommonSegmentLengthEnumType.Description);
                    break;
                case 'connectionAddress':
                    errors[formName] = checkValidOfStringField(newValue, 'Заполните адрес', CommonSegmentLengthEnumType.ConnectionAddress);
                    break;
                case 'usingOutdatedWindows':
                    break;
                default:
                    break;
            }

            this.setState({ errors });
        }
    }

    private getDialogBody() {
        const formFields = this.props.reduxForm.getReduxFormFields(false);

        return getCommonSegmentDialogBody(formFields, this.state.errors, this.onValueChange);
    }

    private successGetTerminalFarmState(actionState: IReducerProcessActionInfo) {
        if (!actionState.isInSuccessState) {
            return;
        }

        this.props.reduxForm.updateReduxFormFields({
            ...this.props.terminalFarm
        });
    }

    private errorGetTerminalFarmState(actionState: IReducerProcessActionInfo, error?: Error) {
        if (!actionState.isInErrorState) return;
        this.props.floatMessage.show(MessageType.Error, `Ошибка при получении терминальной фермы: ${ error?.message }`);
    }

    private submitChanges() {
        if (!this.isTerminalFarmsFormValid()) {
            return;
        }

        this.createTerminalFarm();
        this.editTerminalFarm();
    }

    private createTerminalFarm() {
        if (this.state.pageType !== PageEnumType.Create) return;
        this.props.dispatchCreateTerminalFarmThunk({
            ...fillCommonSegmentDataModel(this.props.reduxForm.getReduxFormFields(false)),
            force: true
        });
    }

    private editTerminalFarm() {
        if (this.state.pageType !== PageEnumType.Edit) {
            return;
        }

        this.props.dispatchEditTerminalFarmThunk({
            ...fillCommonSegmentDataModel(this.props.reduxForm.getReduxFormFields(false)),
            id: this.props.terminalFarmId!,
            force: true
        });
    }

    private defaultInitDataReduxForm(): CommonSegmentDataForm | undefined {
        return {
            connectionAddress: '',
            description: '',
            name: '',
            usingOutdatedWindows: false,
        };
    }

    private isTerminalFarmsFormValid() {
        const formFields = this.props.reduxForm.getReduxFormFields(false);
        const errors: IErrorsType = {};
        const arrayResults: boolean[] = [];

        arrayResults.push(checkOnValidField(errors, 'name', formFields.name, 'Заполните название', CommonSegmentLengthEnumType.Name));
        arrayResults.push(checkOnValidField(errors, 'description', formFields.description, undefined, CommonSegmentLengthEnumType.Description));
        arrayResults.push(checkOnValidField(errors, 'connectionAddress', formFields.connectionAddress, 'Заполните адрес', CommonSegmentLengthEnumType.ConnectionAddress));

        if (arrayResults.some(item => !item)) {
            this.props.floatMessage.show(MessageType.Warning, 'Форма к отправке не готова. Пожалуйста, заполните все поля.');
            this.setState({ errors });
            return false;
        }

        return true;
    }

    public render() {
        const { isOpen } = this.props;
        const isEditTypePage = this.state.pageType === PageEnumType.Edit;
        const titleText = isEditTypePage ? 'Редактировать' : 'Создать';

        return (
            <>
                <Dialog
                    disableBackdropClick={ true }
                    title={ `${ titleText } ферму ТС` }
                    isOpen={ isOpen }
                    dialogWidth="sm"
                    isTitleSmall={ true }
                    titleTextAlign="left"
                    titleFontSize={ 14 }
                    dialogVerticalAlign="center"
                    onCancelClick={ this.props.onClickToCloseModal }
                    buttons={ getDialogButtons({
                        pageType: this.state.pageType,
                        onClickToCloseModal: this.props.onClickToCloseModal,
                        submitChanges: this.submitChanges
                    }) }
                >
                    { this.getDialogBody() }
                </Dialog>
                <WatchReducerProcessActionState
                    watch={ [{
                        reducerStateName: 'TabTerminalFarmsState',
                        processIds: [TabTerminalFarmsProcessId.GetTerminalFarm]
                    }] }
                    onProcessActionStateChanged={ this.onProcessActionStateChanged }
                />
            </>
        );
    }
}

const TerminalFarmsModalConnected = connect<StateProps, DispatchProps, OwnProps, AppReduxStoreState>(
    state => {
        const tabTerminalFarmsState = state.TabTerminalFarmsState;
        const { terminalFarm } = tabTerminalFarmsState;

        return {
            terminalFarm
        };
    },
    {
        dispatchCreateTerminalFarmThunk: CreateTerminalFarmThunk.invoke,
        dispatchGetTerminalFarmThunk: GetTerminalFarmThunk.invoke,
        dispatchEditTerminalFarmThunk: EditTerminalFarmThunk.invoke
    }
)(TerminalFarmsModalClass);

export const TerminalFarmsModalView = withReduxForm(withFloatMessages(TerminalFarmsModalConnected),
    {
        reduxFormName: 'Terminal_Farm_Modal_Form',
        resetOnUnmount: true
    });