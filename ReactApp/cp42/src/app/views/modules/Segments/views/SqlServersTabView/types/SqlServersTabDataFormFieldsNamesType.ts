import { SqlServersTabDataForm } from 'app/views/modules/Segments/views/SqlServersTabView/types';

export type SqlServersTabDataFormFieldsNamesType = keyof SqlServersTabDataForm;