import { PlatformEnumType } from 'app/common/enums';

/**
 * Получить индекс enum'а платформы
 * @param enumName Название платформы
 * @returns Индекс enum платформы
 */
export function getVersionEnumKeyByName(enumName: string) {
    const versionsTextArray = Object.keys(PlatformEnumType).filter(key => Number.isNaN(+key));
    return versionsTextArray.indexOf(enumName);
}