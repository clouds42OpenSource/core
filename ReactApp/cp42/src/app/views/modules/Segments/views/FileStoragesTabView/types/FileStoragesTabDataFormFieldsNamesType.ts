import { FileStoragesTabDataForm } from 'app/views/modules/Segments/views/FileStoragesTabView/types';

export type FileStoragesTabDataFormFieldsNamesType = keyof FileStoragesTabDataForm;