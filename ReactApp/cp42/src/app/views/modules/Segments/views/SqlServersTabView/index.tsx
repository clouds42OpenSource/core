import ErrorOutlineIcon from '@mui/icons-material/ErrorOutline';
import { Box, Chip } from '@mui/material';
import { AccountDatabaseRestoreModelType, SortingKind } from 'app/common/enums';
import { TabSqlServersProcessId } from 'app/modules/segments/TabSqlServers';
import { DeleteCloudSqlServerThunkParams } from 'app/modules/segments/TabSqlServers/store/reducer/deleteCloudSqlServerReducer/params';
import { GetCloudSqlServersThunkParams } from 'app/modules/segments/TabSqlServers/store/reducer/getCloudSqlServersReducer/params';
import { DeleteCloudSqlServerThunk, GetCloudSqlServersThunk } from 'app/modules/segments/TabSqlServers/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { SuccessButton, TextLinkButton } from 'app/views/components/controls/Button';
import { Dialog } from 'app/views/components/controls/Dialog';
import { StatusLabelTypes } from 'app/views/components/controls/Labels/views/StatusLabelView/types';
import { TextOut } from 'app/views/components/TextOut';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { WatchReducerProcessActionState } from 'app/views/modules/_common/components/WatchReducerProcessActionState';
import { CommonSegmentEnumType } from 'app/views/modules/Segments/types';
import { SearchCloudSqlServerView } from 'app/views/modules/Segments/views/SqlServersTabView/views/SearchCloudSqlServerView';
import { SqlServerModalView } from 'app/views/modules/Segments/views/SqlServersTabView/views/SqlServerModalView';
import { SelectDataMetadataResponseDto, SortingDataModel } from 'app/web/common';
import { SelectDataCommonDataModel } from 'app/web/common/data-models';
import { CloudSqlServerDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabSqlServersApiProxy';
import { IReducerProcessActionInfo } from 'core/redux/interfaces';
import { Component, createRef } from 'react';
import { connect } from 'react-redux';
import commonCss from '../../common/styles.module.css';
import css from './styles.module.css';

type SqlServersTabState = {
    /**
     * Поле обозначающая что первая подгрузка данных была сделана
     */
    isFirstTimeLoaded: boolean;

    /**
     * Поле для открытия модального окошка
     */
    isOpen: boolean;

    /**
     * ID sql сервера
     */
    sqlServerId?: string;
    /**
     * Состояние модального окна на удаление SQL сервера
     */
    deleteIsOpen: boolean;
    orderBy?: string;
};

type SqlServersTabProps = {
    /**
     * Поле для подгрузки данных
     */
    isCanLoad: boolean;
};

type StateProps = {
    /**
     * Список данных списка sql сервера
     */
    cloudSqlServers: SelectDataMetadataResponseDto<CloudSqlServerDataModel>;

    /**
     * Если true, значит список sql сервера
     */
    hasCloudSqlServersReceived: boolean;
};

type DispatchProps = {
    dispatchGetCloudSqlServersThunk: (args: GetCloudSqlServersThunkParams) => void;
    dispatchDeleteCloudSqlServerThunk: (args: DeleteCloudSqlServerThunkParams) => void;
};

type AllProps = SqlServersTabProps & StateProps & DispatchProps & FloatMessageProps;

class SqlServersTabClass extends Component<AllProps, SqlServersTabState> {
    private _commonTableRef = createRef<CommonTableWithFilter<CloudSqlServerDataModel, undefined>>();

    private _searchCloudSqlServerRef = createRef<SearchCloudSqlServerView>();

    public constructor(props: AllProps) {
        super(props);

        this.state = {
            isOpen: false,
            isFirstTimeLoaded: false,
            deleteIsOpen: false,
            orderBy: ''
        };

        this.onClickToOpenModal = this.onClickToOpenModal.bind(this);
        this.onClickToCloseModal = this.onClickToCloseModal.bind(this);
        this.onClickToSearch = this.onClickToSearch.bind(this);
        this.onDataSelect = this.onDataSelect.bind(this);
        this.getFormatNameSqlServer = this.getFormatNameSqlServer.bind(this);
        this.getFormatRestoreModelType = this.getFormatRestoreModelType.bind(this);
        this.notificationInSuccess = this.notificationInSuccess.bind(this);
        this.notificationInError = this.notificationInError.bind(this);
        this.openModalForDeleteSqlServer = this.openModalForDeleteSqlServer.bind(this);
        this.getButtonsForActions = this.getButtonsForActions.bind(this);

        this.onProcessActionStateChanged = this.onProcessActionStateChanged.bind(this);
        this.onProcessCreateSqlServer = this.onProcessCreateSqlServer.bind(this);
        this.onProcessEditSqlServer = this.onProcessEditSqlServer.bind(this);
        this.onProcessDeleteSqlServer = this.onProcessDeleteSqlServer.bind(this);
        this.onClickDeleteSqlServer = this.onClickDeleteSqlServer.bind(this);
        this.closeModalForDeleteSqlServer = this.closeModalForDeleteSqlServer.bind(this);
    }

    public componentDidUpdate() {
        const { isCanLoad, hasCloudSqlServersReceived } = this.props;
        const { isFirstTimeLoaded } = this.state;

        if (isCanLoad && !isFirstTimeLoaded && !hasCloudSqlServersReceived && this._commonTableRef.current) {
            this._commonTableRef.current.ReSelectData();
            this.setState({
                isFirstTimeLoaded: true
            });
        }
    }

    private onClickDeleteSqlServer() {
        if (this.state.sqlServerId) {
            this.props.dispatchDeleteCloudSqlServerThunk({
                sqlServerId: this.state.sqlServerId,
                force: true
            });
            this.setState({
                deleteIsOpen: false
            });
        }
    }

    private onProcessActionStateChanged(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        this.onProcessCreateSqlServer(processId, actionState, error);
        this.onProcessEditSqlServer(processId, actionState, error);
        this.onProcessDeleteSqlServer(processId, actionState, error);
    }

    private onProcessCreateSqlServer(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId !== TabSqlServersProcessId.CreateCloudSqlServer) return;
        this.notificationInSuccess(actionState, 'SQL сервер успешно создан');
        this.notificationInError(actionState, error?.message);
    }

    private onProcessEditSqlServer(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId !== TabSqlServersProcessId.EditCloudSqlServer) return;
        this.notificationInSuccess(actionState, 'SQL сервер успешно сохранен');
        this.notificationInError(actionState, error?.message);
    }

    private onProcessDeleteSqlServer(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId !== TabSqlServersProcessId.DeleteCloudSqlServer) return;
        this.notificationInSuccess(actionState, 'SQL сервер успешно удален');
        this.notificationInError(actionState, error?.message);
    }

    private onClickToOpenModal(sqlServerId?: string) {
        this.setState({
            isOpen: true,
            sqlServerId
        });
    }

    private onClickToCloseModal() {
        this.setState({
            isOpen: false
        });
    }

    private onClickToSearch() {
        const {
            description,
            connectionAddress,
            name,
            restoreModelType
        } = this._searchCloudSqlServerRef.current?.state!;

        this.props.dispatchGetCloudSqlServersThunk({
            filter: {
                connectionAddress,
                description,
                name,
                restoreModelType: restoreModelType ? Number(restoreModelType) : undefined,
            },
            pageNumber: 1,
            orderBy: this.state.orderBy,
            pageSize: CommonSegmentEnumType.DefaultPageSize,
            force: true
        });
    }

    private onDataSelect(args: SelectDataCommonDataModel<undefined>) {
        const {
            description,
            connectionAddress,
            name,
            restoreModelType
        } = this._searchCloudSqlServerRef.current?.state!;

        const orderBy = args.sortingData ? this.getSortQuery(args.sortingData) : 'name.asc';

        this.setState({
            orderBy
        });

        this.props.dispatchGetCloudSqlServersThunk({
            filter: {
                description,
                connectionAddress,
                name,
                restoreModelType: restoreModelType ? Number(restoreModelType) : undefined,
            },
            pageNumber: args.pageNumber,
            orderBy,
            pageSize: CommonSegmentEnumType.DefaultPageSize,
            force: true
        });
    }

    private getFormatRestoreModelType(restoreModelType: AccountDatabaseRestoreModelType) {
        let typeLabel: StatusLabelTypes = 'secondary';
        let textLabel = '';

        switch (restoreModelType) {
            case AccountDatabaseRestoreModelType.Simple:
                typeLabel = 'primary';
                textLabel = 'Простая';
                break;
            case AccountDatabaseRestoreModelType.Full:
                typeLabel = 'success';
                textLabel = 'Полная';
                break;
            case AccountDatabaseRestoreModelType.Mixed:
                typeLabel = 'error';
                textLabel = 'Смешанная';
                break;
        }

        return (
            <Chip
                color={ typeLabel }
                label={ textLabel }
            />
        );
    }

    private getButtonsForActions(Id: string) {
        return (
            <TextLinkButton
                className={ commonCss['delete-link'] }
                onClick={ () => { this.openModalForDeleteSqlServer(Id); } }
            >
                <i className="fa fa-trash" />
            </TextLinkButton>
        );
    }

    private getSortQuery(args: SortingDataModel) {
        return `${ args.fieldName }.${ args.sortKind ? 'desc' : 'asc' }`;
    }

    private getFormatNameSqlServer(name: string, data: CloudSqlServerDataModel) {
        return (
            <TextLinkButton onClick={ () => { this.onClickToOpenModal(data.id); } }>
                <TextOut fontSize={ 13 } className="text-link">
                    { name }
                </TextOut>
            </TextLinkButton>
        );
    }

    private notificationInError(actionState: IReducerProcessActionInfo, message?: string) {
        if (!actionState.isInErrorState) return;
        this.props.floatMessage.show(MessageType.Error, message);
    }

    private notificationInSuccess(actionState: IReducerProcessActionInfo, message: string) {
        if (!actionState.isInSuccessState) return;
        this.props.floatMessage.show(MessageType.Success, message);
        this._commonTableRef.current?.ReSelectData();
        this.onClickToCloseModal();
    }

    private openModalForDeleteSqlServer(sqlServerId: string) {
        this.setState({
            sqlServerId,
            deleteIsOpen: true
        });
    }

    private closeModalForDeleteSqlServer() {
        this.setState({
            deleteIsOpen: false
        });
    }

    public render() {
        const { cloudSqlServers: { metadata, records } } = this.props;

        return (
            <>
                <SearchCloudSqlServerView
                    ref={ this._searchCloudSqlServerRef }
                    onClickToSearch={ this.onClickToSearch }
                />
                <CommonTableWithFilter
                    className={ css['custom-table'] }
                    ref={ this._commonTableRef }
                    isResponsiveTable={ true }
                    isBorderStyled={ true }
                    uniqueContextProviderStateId="TabSqlServersContextProviderStateId"
                    tableProps={ {
                        dataset: records,
                        keyFieldName: 'id',
                        sorting: {
                            sortFieldName: 'name',
                            sortKind: SortingKind.Asc
                        },
                        fieldsView: {
                            name: {
                                caption: 'Название',
                                fieldContentNoWrap: false,
                                fieldWidth: 'auto',
                                isSortable: true,
                                format: this.getFormatNameSqlServer
                            },
                            description: {
                                caption: 'Описание',
                                fieldContentNoWrap: false,
                                fieldWidth: 'auto',
                                isSortable: true
                            },
                            connectionAddress: {
                                caption: 'Адрес подключения',
                                fieldContentNoWrap: false,
                                fieldWidth: 'auto',
                                isSortable: true
                            },
                            restoreModelType: {
                                caption: 'Модель восстановления',
                                fieldContentNoWrap: false,
                                fieldWidth: 'auto',
                                isSortable: true,
                                format: this.getFormatRestoreModelType
                            },
                            id: {
                                caption: '',
                                fieldWidth: 'auto',
                                fieldContentNoWrap: false,
                                format: this.getButtonsForActions
                            }
                        },
                        pagination: {
                            currentPage: metadata.pageNumber,
                            pageSize: metadata.pageSize,
                            recordsCount: metadata.totalItemCount,
                            totalPages: metadata.pageCount
                        },
                    } }
                    onDataSelect={ this.onDataSelect }
                />
                <SuccessButton
                    onClick={ () => { this.onClickToOpenModal(); } }
                >
                    <i className="fa fa-plus-circle mr-1" />
                    Создать SQL сервер
                </SuccessButton>
                {
                    this.state.isOpen
                        ? (
                            <SqlServerModalView
                                isOpen={ this.state.isOpen }
                                sqlServerId={ this.state.sqlServerId }
                                onClickToCloseModal={ this.onClickToCloseModal }
                            />
                        )
                        : null
                }
                <WatchReducerProcessActionState
                    watch={ [{
                        reducerStateName: 'TabSqlServersState',
                        processIds: [
                            TabSqlServersProcessId.CreateCloudSqlServer,
                            TabSqlServersProcessId.EditCloudSqlServer,
                            TabSqlServersProcessId.DeleteCloudSqlServer,
                        ]
                    }] }
                    onProcessActionStateChanged={ this.onProcessActionStateChanged }
                />
                <Dialog
                    isOpen={ this.state.deleteIsOpen }
                    dialogWidth="sm"
                    dialogVerticalAlign="center"
                    onCancelClick={ this.closeModalForDeleteSqlServer }
                    buttons={
                        [
                            {
                                content: 'Нет',
                                kind: 'primary',
                                variant: 'text',
                                onClick: this.closeModalForDeleteSqlServer
                            },
                            {
                                content: 'Да',
                                kind: 'error',
                                variant: 'contained',
                                onClick: this.onClickDeleteSqlServer
                            }
                        ]
                    }
                >
                    <Box display="flex" flexDirection="column" alignItems="center" justifyContent="center">
                        <ErrorOutlineIcon color="warning" sx={ { width: '5em', height: '5em' } } />
                        Вы действительно хотите удалить SQL сервер?
                    </Box>
                </Dialog>
            </>
        );
    }
}

const SqlServersTabConnected = connect<StateProps, DispatchProps, NonNullable<unknown>, AppReduxStoreState>(
    state => {
        const tabSqlServersState = state.TabSqlServersState;
        const { cloudSqlServers } = tabSqlServersState;
        const hasCloudSqlServersReceived = tabSqlServersState.hasSuccessFor.hasCloudSqlServerReceived;

        return {
            cloudSqlServers,
            hasCloudSqlServersReceived
        };
    },
    {
        dispatchGetCloudSqlServersThunk: GetCloudSqlServersThunk.invoke,
        dispatchDeleteCloudSqlServerThunk: DeleteCloudSqlServerThunk.invoke
    }
)(SqlServersTabClass);

export const SqlServersTabView = withFloatMessages(SqlServersTabConnected);