import ErrorOutlineIcon from '@mui/icons-material/ErrorOutline';
import { Box } from '@mui/material';
import { SortingKind } from 'app/common/enums';
import { TabArrServersProcessId } from 'app/modules/segments/TabArrServers';
import { DeleteArrServerThunkParams } from 'app/modules/segments/TabArrServers/store/reducer/deleteArrServerReducer/params';
import { GetArrServersThunkParams } from 'app/modules/segments/TabArrServers/store/reducer/getArrServersReducer/params';
import { DeleteArrServerThunk, GetArrServersThunk } from 'app/modules/segments/TabArrServers/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { COLORS } from 'app/utils';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { SuccessButton, TextLinkButton } from 'app/views/components/controls/Button';
import { Dialog } from 'app/views/components/controls/Dialog';
import { TextOut } from 'app/views/components/TextOut';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { WatchReducerProcessActionState } from 'app/views/modules/_common/components/WatchReducerProcessActionState';
import { CommonSegmentEnumType } from 'app/views/modules/Segments/types';
import { ArrServerModalView } from 'app/views/modules/Segments/views/ArrServersTabView/views/ArrServerModalView';
import { ArrServerDataModel } from 'app/web/api/SegmentsProxy/TabArrServers/response-dto';
import { SelectDataMetadataResponseDto, SortingDataModel } from 'app/web/common';
import { SelectDataCommonDataModel } from 'app/web/common/data-models';
import { IReducerProcessActionInfo } from 'core/redux/interfaces';
import React, { Component, createRef } from 'react';
import { connect } from 'react-redux';
import commonCss from '../../common/styles.module.css';

type ArrServersTabState = {
    isFirstTimeLoaded: boolean;
    isOpen: boolean;
    arrServerId?: string;
    deleteIsOpen: boolean;
};

type ArrServersTabProps = {
    isCanLoad: boolean;
};

type StateProps = {
    arrServers: SelectDataMetadataResponseDto<ArrServerDataModel>;
    hasArrServersReceived: boolean;
};

type DispatchProps = {
    dispatchGetArrServersThunk: (args: GetArrServersThunkParams) => void;
    dispatchDeleteArrServerThunk: (args: DeleteArrServerThunkParams) => void;
};

type AllProps = ArrServersTabProps & StateProps & DispatchProps & FloatMessageProps;

class ArrServersTabClass extends Component<AllProps, ArrServersTabState> {
    private _commonTableRef = createRef<CommonTableWithFilter<ArrServerDataModel, undefined>>();

    public constructor(props: AllProps) {
        super(props);

        this.state = {
            isOpen: false,
            isFirstTimeLoaded: false,
            deleteIsOpen: false
        };

        this.onClickToOpenModal = this.onClickToOpenModal.bind(this);
        this.onClickToCloseModal = this.onClickToCloseModal.bind(this);
        this.onDataSelect = this.onDataSelect.bind(this);
        this.getFormatNameArrServer = this.getFormatNameArrServer.bind(this);
        this.notificationInSuccess = this.notificationInSuccess.bind(this);
        this.notificationInError = this.notificationInError.bind(this);
        this.openModalForDeleteArrServer = this.openModalForDeleteArrServer.bind(this);
        this.getButtonsForActions = this.getButtonsForActions.bind(this);

        this.onProcessActionStateChanged = this.onProcessActionStateChanged.bind(this);
        this.onProcessCreateArrServer = this.onProcessCreateArrServer.bind(this);
        this.onProcessEditArrServer = this.onProcessEditArrServer.bind(this);
        this.onProcessDeleteArrServer = this.onProcessDeleteArrServer.bind(this);
        this.onClickDeleteArrServer = this.onClickDeleteArrServer.bind(this);
        this.closeModalForDeleteArrServer = this.closeModalForDeleteArrServer.bind(this);
    }

    public componentDidUpdate() {
        const { isCanLoad, hasArrServersReceived } = this.props;
        const { isFirstTimeLoaded } = this.state;

        if (isCanLoad && !isFirstTimeLoaded && !hasArrServersReceived && this._commonTableRef.current) {
            this._commonTableRef.current.ReSelectData();
            this.setState({
                isFirstTimeLoaded: true
            });
        }
    }

    private onClickDeleteArrServer() {
        if (this.state.arrServerId) {
            this.props.dispatchDeleteArrServerThunk({
                arrServerId: this.state.arrServerId,
                force: true
            });
            this.setState({
                deleteIsOpen: false
            });
        }
    }

    private onProcessActionStateChanged(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        this.onProcessCreateArrServer(processId, actionState, error);
        this.onProcessEditArrServer(processId, actionState, error);
        this.onProcessDeleteArrServer(processId, actionState, error);
    }

    private onProcessCreateArrServer(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId !== TabArrServersProcessId.CreateArrServer) return;
        this.notificationInSuccess(actionState, 'Нода АО успешно создана');
        this.notificationInError(actionState, error?.message);
    }

    private onProcessEditArrServer(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId !== TabArrServersProcessId.EditArrServer) return;
        this.notificationInSuccess(actionState, 'Нода АО успешно сохранена');
        this.notificationInError(actionState, error?.message);
    }

    private onProcessDeleteArrServer(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId !== TabArrServersProcessId.DeleteArrServer) return;
        this.notificationInSuccess(actionState, 'Нода АО успешно удалена');
        this.notificationInError(actionState, error?.message);
    }

    private onClickToOpenModal(arrServerId?: string) {
        this.setState({
            isOpen: true,
            arrServerId
        });
    }

    private onClickToCloseModal() {
        this.setState({
            isOpen: false
        });
    }

    private onDataSelect(args: SelectDataCommonDataModel<undefined>) {
        this.props.dispatchGetArrServersThunk({
            filter: {},
            pageNumber: args.pageNumber,
            orderBy: args.sortingData ? this.getSortQuery(args.sortingData) : 'nodeAddress.asc',
            pageSize: CommonSegmentEnumType.DefaultPageSize,
            force: true
        });
    }

    private getButtonsForActions(Id: string) {
        return (
            <TextLinkButton
                className={ commonCss['delete-link'] }
                onClick={ () => { this.openModalForDeleteArrServer(Id); } }
            >
                <i className="fa fa-trash" />
            </TextLinkButton>
        );
    }

    private getSortQuery(args: SortingDataModel) {
        return `${ args.fieldName }.${ args.sortKind ? 'desc' : 'asc' }`;
    }

    private getFormatNameArrServer(name: string, data: ArrServerDataModel) {
        return (
            <TextLinkButton onClick={ () => { this.onClickToOpenModal(data.id); } }>
                <TextOut fontSize={ 13 } className="text-link">
                    { name }
                </TextOut>
            </TextLinkButton>
        );
    }

    private notificationInError(actionState: IReducerProcessActionInfo, message?: string) {
        if (!actionState.isInErrorState) return;
        this.props.floatMessage.show(MessageType.Error, message);
    }

    private notificationInSuccess(actionState: IReducerProcessActionInfo, message: string) {
        if (!actionState.isInSuccessState) return;
        this.props.floatMessage.show(MessageType.Success, message);
        this._commonTableRef.current?.ReSelectData();
        this.onClickToCloseModal();
    }

    private closeModalForDeleteArrServer() {
        this.setState({
            deleteIsOpen: false
        });
    }

    private openModalForDeleteArrServer(arrServerId: string) {
        this.setState({
            arrServerId,
            deleteIsOpen: true
        });
    }

    public render() {
        const { arrServers: { metadata, records } } = this.props;

        return (
            <>
                <CommonTableWithFilter
                    className={ commonCss['custom-table'] }
                    ref={ this._commonTableRef }
                    isResponsiveTable={ true }
                    isBorderStyled={ true }
                    uniqueContextProviderStateId="TabArrServerContextProviderStateId"
                    tableProps={ {
                        dataset: records,
                        keyFieldName: 'id',
                        sorting: {
                            sortKind: SortingKind.Asc,
                            sortFieldName: 'nodeAddress'
                        },
                        fieldsView: {
                            nodeAddress: {
                                caption: 'Название ноды АО',
                                fieldContentNoWrap: false,
                                fieldWidth: 'auto',
                                isSortable: true,
                                format: this.getFormatNameArrServer
                            },
                            autoUpdateObnovlyatorPath: {
                                caption: 'Адрес регулярного обновлятора',
                                fieldContentNoWrap: false,
                                fieldWidth: 'auto',
                                isSortable: true
                            },
                            manualUpdateObnovlyatorPath: {
                                caption: 'Адрес ручного обновлятора',
                                fieldContentNoWrap: false,
                                fieldWidth: 'auto',
                                isSortable: true
                            },
                            isBusy: {
                                caption: '',
                                format: value => (
                                    <TextOut
                                        fontWeight={ 700 }
                                        style={ { color: value ? COLORS.error : COLORS.main } }
                                    >
                                        { value ? 'Нода занята' : 'Нода свободна' }
                                    </TextOut>
                                )
                            },
                            id: {
                                caption: '',
                                fieldWidth: 'auto',
                                fieldContentNoWrap: false,
                                format: this.getButtonsForActions
                            }
                        },
                        pagination: {
                            currentPage: metadata.pageNumber,
                            pageSize: metadata.pageSize,
                            recordsCount: metadata.totalItemCount,
                            totalPages: metadata.pageCount
                        },
                    } }
                    onDataSelect={ this.onDataSelect }
                />
                <SuccessButton
                    onClick={ () => { this.onClickToOpenModal(); } }
                >
                    <i className="fa fa-plus-circle mr-1" />
                    Добавить ноду АО
                </SuccessButton>
                {
                    this.state.isOpen
                        ? (
                            <ArrServerModalView
                                isOpen={ this.state.isOpen }
                                arrServerId={ this.state.arrServerId }
                                onClickToCloseModal={ this.onClickToCloseModal }
                            />
                        )
                        : null
                }
                <WatchReducerProcessActionState
                    watch={ [{
                        reducerStateName: 'TabArrServersState',
                        processIds: [
                            TabArrServersProcessId.CreateArrServer,
                            TabArrServersProcessId.EditArrServer,
                            TabArrServersProcessId.DeleteArrServer,
                            TabArrServersProcessId.CheckArrServerAvailability
                        ]
                    }] }
                    onProcessActionStateChanged={ this.onProcessActionStateChanged }
                />
                <Dialog
                    isOpen={ this.state.deleteIsOpen }
                    dialogWidth="sm"
                    dialogVerticalAlign="center"
                    onCancelClick={ this.closeModalForDeleteArrServer }
                    buttons={
                        [
                            {
                                content: 'Нет',
                                kind: 'primary',
                                variant: 'text',
                                onClick: this.closeModalForDeleteArrServer
                            },
                            {
                                content: 'Да',
                                kind: 'error',
                                variant: 'contained',
                                onClick: this.onClickDeleteArrServer
                            }
                        ]
                    }
                >
                    <Box display="flex" flexDirection="column" alignItems="center" justifyContent="center">
                        <ErrorOutlineIcon color="warning" sx={ { width: '5em', height: '5em' } } />
                        Вы действительно хотите удалить ноду АО?
                    </Box>
                </Dialog>
            </>
        );
    }
}

const ArrServersTabConnected = connect<StateProps, DispatchProps, NonNullable<unknown>, AppReduxStoreState>(
    state => {
        const tabArrServersState = state.TabArrServersState;
        const { arrServers } = tabArrServersState;
        const { hasArrServersReceived } = tabArrServersState.hasSuccessFor;

        return {
            arrServers,
            hasArrServersReceived
        };
    },
    {
        dispatchGetArrServersThunk: GetArrServersThunk.invoke,
        dispatchDeleteArrServerThunk: DeleteArrServerThunk.invoke
    }
)(ArrServersTabClass);

export const ArrServersTabView = withFloatMessages(ArrServersTabConnected);