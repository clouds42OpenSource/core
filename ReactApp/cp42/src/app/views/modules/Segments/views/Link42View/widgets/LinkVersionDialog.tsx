import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { Accordion, AccordionDetails, AccordionSummary, Box, MenuItem, OutlinedInput, Select, TextField } from '@mui/material';

import { EBitDepth, ELinkAppType, ESystemType } from 'app/api/endpoints/link42/enum';
import { TCreateLinkConfiguration, TEditLinkConfiguration } from 'app/api/endpoints/link42/request';
import { TBitDepths, TInstallLimits } from 'app/api/endpoints/link42/response';
import { FETCH_API } from 'app/api/useFetchApi';
import { REDUX_API } from 'app/api/useReduxApi';
import { EMessageType, useAppDispatch, useAppSelector, useFloatMessages } from 'app/hooks';
import { ContainedButton, SuccessButton } from 'app/views/components/controls/Button';
import { Dialog } from 'app/views/components/controls/Dialog';
import { CheckBoxForm } from 'app/views/components/controls/forms';
import { FormAndLabel } from 'app/views/components/controls/forms/FormAndLabel';
import { TextOut } from 'app/views/components/TextOut';
import cn from 'classnames';
import dayjs from 'dayjs';
import { FormikErrors, useFormik } from 'formik';
import React, { memo } from 'react';
import * as Yup from 'yup';

import { accordionSummarySx, accordionSx, requiredMessage } from '../shared';
import styles from '../style.module.css';

type TProps = {
    isOpen: boolean;
    data?: TInstallLimits;
    onCancelClick: () => void;
};

const linkConfigurationSchema = Yup.object({
    version: Yup.string().required(requiredMessage),
    isCurrentVersion: Yup.boolean(),
    downloadsLimit: Yup.number().min(1, 'Укажите положительное число').required(requiredMessage),
    bitDepths: Yup.array().of(Yup.object({
        linkAppType: Yup.number().min(ELinkAppType.cloudApp).max(ELinkAppType.cmdApp).required(requiredMessage),
        bitDepth: Yup.number().min(EBitDepth.x86).max(EBitDepth.arm).required(requiredMessage),
        systemType: Yup.number().min(ESystemType.windows).max(ESystemType.linux).required(requiredMessage),
        downloadLink: Yup.string().required(requiredMessage)
    }))
});

const { editLinkConfiguration, createLinkConfiguration } = FETCH_API.LINK42;
const { getInstallLimitsList } = REDUX_API.LINK42_REDUX;

export const LinkVersionDialog = memo(({ isOpen, data, onCancelClick }: TProps) => {
    const { params } = useAppSelector(state => state.Link42State.installLimitsListReducer);

    const dispatch = useAppDispatch();
    const { show } = useFloatMessages();

    const { values, errors, touched, handleBlur, handleChange, setFieldValue, submitForm } = useFormik<TCreateLinkConfiguration>({
        initialValues: {
            version: data?.version ?? '',
            isCurrentVersion: data?.isCurrentVersion ?? true,
            downloadsLimit: data?.downloadsLimit ?? 50,
            bitDepths: data?.bitDepths ?? []
        },
        onSubmit: async submitValues => {
            if (!submitValues.bitDepths.length) {
                show(EMessageType.error, 'Конфигурация должна содержать хотя бы одну сборку');

                return;
            }

            let errorMessage: string | null = null;

            if (data) {
                const body: TEditLinkConfiguration = {
                    ...submitValues,
                    id: data.id
                };
                const { message } = await editLinkConfiguration(body);

                errorMessage = message;
            } else {
                const { message } = await createLinkConfiguration(submitValues);

                errorMessage = message;
            }

            if (errorMessage) {
                show(EMessageType.error, errorMessage);
            } else {
                onCancelClick();
                getInstallLimitsList(dispatch, data ? { pageNumber: 1 } : params);
            }
        },
        validateOnBlur: true,
        validateOnChange: true,
        enableReinitialize: true,
        validationSchema: linkConfigurationSchema,
    });

    const currentVersionHandler = (_: string, value: boolean) => {
        setFieldValue('isCurrentVersion', value);
    };

    const addInstallation = () => {
        setFieldValue('bitDepths', [...values.bitDepths, {
            linkAppType: ELinkAppType.clientApp,
            bitDepth: EBitDepth.x64,
            systemType: ESystemType.windows,
            downloadLink: '',
        }], true);
    };

    const removeInstallation = (index: number) => () => {
        setFieldValue('bitDepths', values.bitDepths.filter((_, i) => i !== index), true);
    };

    return (
        <Dialog
            onCancelClick={ onCancelClick }
            isTitleSmall={ true }
            isOpen={ isOpen }
            dialogWidth="sm"
            title={ `${ data ? 'Редактирование' : 'Создание' }` }
            buttons={ [
                {
                    content: 'Закрыть',
                    kind: 'default',
                    onClick: onCancelClick
                },
                {
                    content: 'Сохранить',
                    kind: 'success',
                    onClick: submitForm
                }
            ] }
        >
            <Box display="flex" flexDirection="column" gap="10px">
                <Box display="flex" gap="10px" justifyContent="space-between">
                    <CheckBoxForm
                        onValueChange={ currentVersionHandler }
                        className={ styles.formAndLabel }
                        isChecked={ values.isCurrentVersion }
                        label="Текущая версия"
                        formName="isCurrentVersion"
                    />
                    { data && (
                        <Box display="flex" gap="15px">
                            <FormAndLabel className={ styles.formAndLabel } label="Создано пользователем">
                                { data.createdBy }
                            </FormAndLabel>
                            <FormAndLabel className={ styles.formAndLabel } label="Количество скачиваний">
                                <i className="fa fa-user" aria-hidden="true" />{ ' ' }{ data.downloadsCount }
                            </FormAndLabel>
                        </Box>
                    ) }
                </Box>
                <FormAndLabel label="Версия приложения">
                    <TextField
                        fullWidth={ true }
                        disabled={ !!data }
                        name="version"
                        value={ values.version }
                        onChange={ handleChange }
                        onBlur={ handleBlur }
                        error={ touched.version && !!errors.version }
                        helperText={ touched.version && errors.version }
                    />
                </FormAndLabel>
                <FormAndLabel label="Лимит скачиваний">
                    <TextField
                        type="number"
                        fullWidth={ true }
                        name="downloadsLimit"
                        value={ values.downloadsLimit }
                        onChange={ handleChange }
                        onBlur={ handleBlur }
                        error={ touched.downloadsLimit && !!errors.downloadsLimit }
                        helperText={ touched.downloadsLimit && errors.downloadsLimit }
                    />
                </FormAndLabel>
                { data && (
                    <>
                        <FormAndLabel label="Идентификатор">
                            <OutlinedInput fullWidth={ true } disabled={ true } value={ data.id } />
                        </FormAndLabel>
                        <FormAndLabel label="Создано">
                            <OutlinedInput fullWidth={ true } disabled={ true } value={ dayjs(data.createdOn).format('DD.MM.YYYY HH:mm') } />
                        </FormAndLabel>
                        { data.updatedOn && (
                            <FormAndLabel label="Отредактировано">
                                <OutlinedInput fullWidth={ true } disabled={ true } value={ dayjs(data.updatedOn).format('DD.MM.YYYY HH:mm') } />
                            </FormAndLabel>
                        ) }
                    </>
                ) }
                { !!values.bitDepths.length && (
                    <FormAndLabel label="Сборки">
                        <Box display="flex" flexDirection="column" gap="15px">
                            { values.bitDepths.map((_, index) => {
                                const { bitDepth, downloadLink, systemType, linkAppType, id } = values.bitDepths[index];

                                const bitDepthHasError = touched.bitDepths && !!errors.bitDepths && typeof errors.bitDepths !== 'string' && !!errors.bitDepths[index] && typeof errors.bitDepths[index] !== 'string';
                                const downloadLinkError = bitDepthHasError ? (errors.bitDepths as FormikErrors<TBitDepths>[])[index].downloadLink : '';
                                const bitDepthError = bitDepthHasError ? (errors.bitDepths as FormikErrors<TBitDepths>[])[index].bitDepth : '';
                                const systemTypeError = bitDepthHasError ? (errors.bitDepths as FormikErrors<TBitDepths>[])[index].systemType : '';
                                const linkAppTypeError = bitDepthHasError ? (errors.bitDepths as FormikErrors<TBitDepths>[])[index].linkAppType : '';

                                return (
                                    <Accordion key={ id } sx={ accordionSx } className={ cn(styles.accordion, { [styles.accordionError]: bitDepthHasError }) }>
                                        <AccordionSummary sx={ accordionSummarySx } className={ styles.accordionSummary } expandIcon={ <ExpandMoreIcon /> }>
                                            <TextOut fontWeight={ 600 }>
                                                { linkAppType === ELinkAppType.clientApp ? 'Клиентский' : 'Терминальный' }
                                            </TextOut>
                                            &nbsp;
                                            <TextOut fontWeight={ 600 }>
                                                { systemType === ESystemType.linux ? 'Linux' : systemType === ESystemType.macOS ? 'Mac OS' : 'Windows' }
                                            </TextOut>
                                            &nbsp;
                                            <TextOut fontWeight={ 600 }>
                                                { bitDepth === EBitDepth.x64 ? 'x64' : bitDepth === EBitDepth.x86 ? 'x86' : 'ARM' }
                                            </TextOut>
                                        </AccordionSummary>
                                        <AccordionDetails>
                                            <Box display="flex" flexDirection="column" gap="10px">
                                                <FormAndLabel label="Ссылка для скачивания">
                                                    <TextField
                                                        fullWidth={ true }
                                                        name={ `bitDepths[${ index }].downloadLink` }
                                                        value={ downloadLink }
                                                        onChange={ handleChange }
                                                        onBlur={ handleBlur }
                                                        error={ !!downloadLinkError }
                                                        helperText={ downloadLinkError }
                                                    />
                                                </FormAndLabel>
                                                <FormAndLabel label="Архитектура">
                                                    <Select
                                                        variant="outlined"
                                                        onChange={ handleChange }
                                                        onBlur={ handleBlur }
                                                        error={ !!bitDepthError }
                                                        value={ bitDepth }
                                                        fullWidth={ true }
                                                        name={ `bitDepths[${ index }].bitDepth` }
                                                    >
                                                        <MenuItem value={ EBitDepth.x64 }>x64</MenuItem>
                                                        <MenuItem value={ EBitDepth.x86 }>x86</MenuItem>
                                                        <MenuItem value={ EBitDepth.arm }>ARM</MenuItem>
                                                    </Select>
                                                    { !!bitDepthError && (<TextOut className={ styles.error }>{ bitDepthError }</TextOut>) }
                                                </FormAndLabel>
                                                <FormAndLabel label="Операционная система">
                                                    <Select
                                                        variant="outlined"
                                                        onChange={ handleChange }
                                                        onBlur={ handleBlur }
                                                        error={ !!systemTypeError }
                                                        value={ systemType }
                                                        fullWidth={ true }
                                                        name={ `bitDepths[${ index }].systemType` }
                                                    >
                                                        <MenuItem value={ ESystemType.windows }>Windows</MenuItem>
                                                        <MenuItem value={ ESystemType.macOS }>Mac OS</MenuItem>
                                                        <MenuItem value={ ESystemType.linux }>Linux</MenuItem>
                                                    </Select>
                                                    { !!systemTypeError && (<TextOut className={ styles.error }>{ systemTypeError }</TextOut>) }
                                                </FormAndLabel>
                                                <FormAndLabel label="Тип">
                                                    <Select
                                                        variant="outlined"
                                                        onChange={ handleChange }
                                                        onBlur={ handleBlur }
                                                        error={ !!linkAppTypeError }
                                                        value={ linkAppType }
                                                        fullWidth={ true }
                                                        name={ `bitDepths[${ index }].linkAppType` }
                                                    >
                                                        <MenuItem value={ ELinkAppType.clientApp }>Клиентский</MenuItem>
                                                        <MenuItem value={ ELinkAppType.cloudApp }>Терминальный</MenuItem>
                                                        <MenuItem value={ ELinkAppType.cmdApp }>Консольный</MenuItem>
                                                    </Select>
                                                    { !!linkAppTypeError && (<TextOut className={ styles.error }>{ linkAppTypeError }</TextOut>) }
                                                </FormAndLabel>
                                                <ContainedButton className={ styles.button } onClick={ removeInstallation(index) } kind="error">Удалить</ContainedButton>
                                            </Box>
                                        </AccordionDetails>
                                    </Accordion>
                                );
                            }) }
                        </Box>
                    </FormAndLabel>
                ) }
                <SuccessButton onClick={ addInstallation } className={ styles.button }>
                    <i className="fa fa-plus-circle mr-1" />
                    Добавить сборку
                </SuccessButton>
            </Box>
        </Dialog>
    );
});