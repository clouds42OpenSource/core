/**
 * Модель Redux формы для файла хранилища
 */
export type FileStoragesTabDataForm = {
    /**
     * Адрес подключения хранилища
     */
    connectionAddress: string;

    /**
     * Описание хранилища
     */
    description: string;

    /**
     * Название хранилища
     */
    name: string;

    /**
     * DNS имя хранилища
     */
    dnsName: string;
};