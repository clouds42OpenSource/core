import { Box, FormControlLabel } from '@mui/material';
import Checkbox from '@mui/material/Checkbox';
import { FETCH_API } from 'app/api/useFetchApi';
import { PageEnumType } from 'app/common/enums';
import { checkOnValidField, checkValidOfStringField } from 'app/common/functions';
import { IErrorsType } from 'app/common/interfaces';
import { TabArrServersProcessId } from 'app/modules/segments/TabArrServers';
import { CreateArrServerThunkParams } from 'app/modules/segments/TabArrServers/store/reducer/createArrServerReducer/params';
import { EditArrServerThunkParams } from 'app/modules/segments/TabArrServers/store/reducer/editArrServerReducer/params';
import { GetArrServerThunkParams } from 'app/modules/segments/TabArrServers/store/reducer/getArrServerReducer/params';
import { GetArrServersThunkParams } from 'app/modules/segments/TabArrServers/store/reducer/getArrServersReducer/params';
import { CreateArrServerThunk, EditArrServerThunk, GetArrServersThunk, GetArrServerThunk } from 'app/modules/segments/TabArrServers/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { OutlinedButton } from 'app/views/components/controls/Button';
import { Dialog } from 'app/views/components/controls/Dialog';
import { ComboBoxForm, TextBoxForm } from 'app/views/components/controls/forms';
import { ComboboxItemModel } from 'app/views/components/controls/forms/ComboBox/models';
import { TextOut } from 'app/views/components/TextOut';
import { ShowErrorMessage } from 'app/views/modules/_common/components/ShowErrorMessage';
import { WatchReducerProcessActionState } from 'app/views/modules/_common/components/WatchReducerProcessActionState';
import { getDialogButtons } from 'app/views/modules/Segments/common/functions';
import { CommonSegmentDataFormFieldsNamesType, CommonSegmentEnumType, CommonSegmentLengthEnumType } from 'app/views/modules/Segments/types';
import { ArrServerDataModel, EAutoUpdateNodeType } from 'app/web/api/SegmentsProxy/TabArrServers/response-dto';
import { CommonSegmentDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import { IReducerProcessActionInfo } from 'core/redux/interfaces';
import React, { Component } from 'react';
import { connect } from 'react-redux';

type OwnProps = ReduxFormProps<ArrServerDataModel>;

type ArrServerModalProps = {
    isOpen: boolean;
    arrServerId?: string;
    onClickToCloseModal: () => void;
};

type ArrServerModalState = {
    errors: IErrorsType;
    pageType: PageEnumType;
};

type StateProps = {
    arrServer: CommonSegmentDataModel;
};

type DispatchProps = {
    dispatchCreateArrServerThunk: (args: CreateArrServerThunkParams) => void;
    dispatchGetArrServerThunk: (args: GetArrServerThunkParams) => void;
    dispatchEditArrServerThunk: (args: EditArrServerThunkParams) => void;
    dispatchGetArrServersThunk: (args: GetArrServersThunkParams) => void;
};

type AllProps = ArrServerModalProps & OwnProps & StateProps & DispatchProps & FloatMessageProps;

const { putAutoUpdateNodes } = FETCH_API.AUTO_UPDATE_NODES;

class ArrServerModalClass extends Component<AllProps, ArrServerModalState> {
    private _IsEditTypePage = false;

    public constructor(props: AllProps) {
        super(props);

        this.state = {
            errors: {},
            pageType: this.props.arrServerId ? PageEnumType.Edit : PageEnumType.Create
        };

        this._IsEditTypePage = this.state.pageType === PageEnumType.Edit;

        this.getDialogBody = this.getDialogBody.bind(this);
        this.onValueChange = this.onValueChange.bind(this);
        this.submitChanges = this.submitChanges.bind(this);
        this.createArrServer = this.createArrServer.bind(this);
        this.editArrServer = this.editArrServer.bind(this);
        this.isArrServerFormValid = this.isArrServerFormValid.bind(this);

        this.onProcessActionStateChanged = this.onProcessActionStateChanged.bind(this);
        this.onProcessGetArrServer = this.onProcessGetArrServer.bind(this);
        this.successGetArrServerState = this.successGetArrServerState.bind(this);
        this.errorGetArrServerState = this.errorGetArrServerState.bind(this);
        this.releaseNode = this.releaseNode.bind(this);

        this.props.reduxForm.setInitializeFormDataAction(this.defaultInitDataReduxForm);
    }

    public componentDidMount() {
        if (this.props.arrServerId) {
            this.props.dispatchGetArrServerThunk({
                arrServerId: this.props.arrServerId,
                force: true
            });
        }
    }

    private onProcessActionStateChanged(processId: string, actionState: IReducerProcessActionInfo) {
        this.onProcessGetArrServer(processId, actionState);
    }

    private onProcessGetArrServer(processId: string, actionState: IReducerProcessActionInfo) {
        if (processId !== TabArrServersProcessId.GetArrServer) return;
        this.successGetArrServerState(actionState);
        this.errorGetArrServerState(actionState);
    }

    private onValueChange<TValue, TForm extends string = CommonSegmentDataFormFieldsNamesType>(formName: TForm, newValue: TValue) {
        this.props.reduxForm.updateReduxFormFields({
            [formName]: newValue
        });

        const { errors } = this.state;

        if (typeof newValue === 'string') {
            switch (formName) {
                case 'name':
                    errors[formName] = checkValidOfStringField(newValue, 'Заполните название', CommonSegmentLengthEnumType.Name);
                    break;
                case 'description':
                    errors[formName] = checkValidOfStringField(newValue, undefined, CommonSegmentLengthEnumType.Description);
                    break;
                case 'connectionAddress':
                    errors[formName] = checkValidOfStringField(newValue, 'Заполните адрес', CommonSegmentLengthEnumType.ConnectionAddress);
                    break;
                default:
                    break;
            }

            this.setState({ errors });
        }
    }

    private getDialogBody() {
        const formFields = this.props.reduxForm.getReduxFormFields(false);
        const { errors } = this.state;

        return (
            <Box display="flex" flexDirection="column" gap="12px">
                <div>
                    <TextBoxForm
                        formName="nodeAddress"
                        label="Название ноды АО"
                        placeholder="Введите название "
                        value={ formFields.nodeAddress }
                        onValueChange={ this.onValueChange }
                    />
                    <ShowErrorMessage errors={ errors } formName="nodeAddress" />
                </div>
                <div>
                    <TextBoxForm
                        formName="autoUpdateObnovlyatorPath"
                        label="Адрес регулярного обновлятора"
                        placeholder="Введите адрес"
                        value={ formFields.autoUpdateObnovlyatorPath }
                        onValueChange={ this.onValueChange }
                        autoComplete="doNotAutoComplete"
                    />
                    <ShowErrorMessage errors={ errors } formName="reportFolderPath" />
                </div>
                <div>
                    <TextBoxForm
                        type="textarea"
                        formName="manualUpdateObnovlyatorPath"
                        label="Адрес ручного обновлятора"
                        placeholder="Введите адрес"
                        value={ formFields.manualUpdateObnovlyatorPath }
                        onValueChange={ this.onValueChange }
                        autoComplete="doNotAutoComplete"
                    />
                    <ShowErrorMessage errors={ errors } formName="manualUpdateObnovlyatorPath" />
                </div>
                <ComboBoxForm
                    label="Тип автообновления"
                    formName="autoUpdateNodeType"
                    items={
                        Object
                            .keys(EAutoUpdateNodeType)
                            .reduce((prev: ComboboxItemModel<number>[], curr, index) => {
                                if (Number.isNaN(Number(curr))) {
                                    /**
                                     * - 3 изза пар ключей (первые 3 ключа не нужны)
                                     */
                                    prev.push({ text: curr, value: index - 3 });
                                }

                                return prev;
                            }, [])
                    }
                    onValueChange={ this.onValueChange }
                    value={ formFields.autoUpdateNodeType }
                />
                <FormControlLabel
                    control={
                        <Checkbox
                            checked={ formFields.dontRunInGlobalAU }
                            onChange={ event => this.onValueChange('dontRunInGlobalAU', event.target.checked) }
                            size="small"
                        />
                    }
                    label={
                        <TextOut>Не учавствовать в общем автообновлении</TextOut>
                    }
                />
                {
                    formFields.isBusy && (
                        <OutlinedButton
                            onClick={ this.releaseNode }
                        >
                            Освободить ноду
                        </OutlinedButton>
                    )
                }
            </Box>
        );
    }

    private async releaseNode() {
        const { success, message } = await putAutoUpdateNodes(this.props.reduxForm.getReduxFormFields(false));

        if (!success) {
            this.props.floatMessage.show(MessageType.Error, message);
        } else {
            this.props.dispatchGetArrServersThunk({
                filter: {},
                pageNumber: 1,
                orderBy: 'nodeAddress.asc',
                pageSize: CommonSegmentEnumType.DefaultPageSize,
                force: true
            });
        }

        this.props.onClickToCloseModal();
    }

    private successGetArrServerState(actionState: IReducerProcessActionInfo) {
        if (!actionState.isInSuccessState) return;
        this.props.reduxForm.updateReduxFormFields({
            ...this.props.arrServer
        });
    }

    private errorGetArrServerState(actionState: IReducerProcessActionInfo, error?: Error) {
        if (!actionState.isInErrorState) return;
        this.props.floatMessage.show(MessageType.Error, `Ошибка при получении сервера ARR: ${ error?.message }`);
    }

    private submitChanges() {
        if (!this.isArrServerFormValid()) return;
        this.createArrServer();
        this.editArrServer();
    }

    private createArrServer() {
        if (this.state.pageType !== PageEnumType.Create) return;
        this.props.dispatchCreateArrServerThunk({
            ...this.props.reduxForm.getReduxFormFields(false),
            force: true
        });
    }

    private editArrServer() {
        if (this.state.pageType !== PageEnumType.Edit) return;
        this.props.dispatchEditArrServerThunk({
            ...this.props.reduxForm.getReduxFormFields(false),
            id: this.props.arrServerId!,
            force: true
        });
    }

    private defaultInitDataReduxForm(): ArrServerDataModel | undefined {
        return {
            id: '',
            nodeAddress: '',
            manualUpdateObnovlyatorPath: '',
            autoUpdateObnovlyatorPath: '',
            dontRunInGlobalAU: false,
            autoUpdateNodeType: 0,
            isBusy: false
        };
    }

    private isArrServerFormValid() {
        const formFields = this.props.reduxForm.getReduxFormFields(false);
        const errors: IErrorsType = {};
        const arrayResults: boolean[] = [];

        arrayResults.push(checkOnValidField(errors, 'name', formFields.nodeAddress, 'Заполните название'));
        arrayResults.push(checkOnValidField(errors, 'description', formFields.manualUpdateObnovlyatorPath, undefined));
        arrayResults.push(checkOnValidField(errors, 'connectionAddress', formFields.autoUpdateObnovlyatorPath, 'Заполните адрес'));

        if (arrayResults.some(item => !item)) {
            this.props.floatMessage.show(MessageType.Warning, 'Форма к отправке не готова. Пожалуйста, заполните все поля.');
            this.setState({ errors });
            return false;
        }

        return true;
    }

    public render() {
        const { isOpen } = this.props;
        const titleText = this._IsEditTypePage ? 'Редактирование' : 'Создание';

        return (
            <>
                <Dialog
                    disableBackdropClick={ true }
                    title={ `${ titleText } Ноды АО` }
                    isOpen={ isOpen }
                    dialogWidth="sm"
                    isTitleSmall={ true }
                    titleTextAlign="left"
                    titleFontSize={ 14 }
                    dialogVerticalAlign="center"
                    onCancelClick={ this.props.onClickToCloseModal }
                    buttons={ getDialogButtons({
                        pageType: this.state.pageType,
                        onClickToCloseModal: this.props.onClickToCloseModal,
                        submitChanges: this.submitChanges
                    }) }
                >
                    {
                        this.getDialogBody()
                    }
                </Dialog>
                <WatchReducerProcessActionState
                    watch={ [{
                        reducerStateName: 'TabArrServersState',
                        processIds: [
                            TabArrServersProcessId.GetArrServer
                        ]
                    }] }
                    onProcessActionStateChanged={ this.onProcessActionStateChanged }
                />
            </>
        );
    }
}

const ArrServerModalConnected = connect<StateProps, DispatchProps, OwnProps, AppReduxStoreState>(
    state => {
        const tabArrServerState = state.TabArrServersState;
        const { arrServer } = tabArrServerState;

        return {
            arrServer
        };
    },
    {
        dispatchGetArrServersThunk: GetArrServersThunk.invoke,
        dispatchCreateArrServerThunk: CreateArrServerThunk.invoke,
        dispatchGetArrServerThunk: GetArrServerThunk.invoke,
        dispatchEditArrServerThunk: EditArrServerThunk.invoke
    }
)(ArrServerModalClass);

export const ArrServerModalView = withReduxForm(withFloatMessages(ArrServerModalConnected),
    {
        reduxFormName: 'Arr_Server_Modal_Form',
        resetOnUnmount: true
    }
);