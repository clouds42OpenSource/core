import { PublishNodeDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';

/**
 * Модель Redux формы для сайта публикации
 */
export type ContentServersTabDataForm = {
    /**
     * Название
     */
    name: string;

    /**
     * Описание
     */
    description: string;

    /**
     * Сайт публикации
     */
    publishSiteName: string;

    /**
     * Сгрупирован по аккаунту
     */
    groupByAccount: boolean;

    /**
     * Выбранные ноды публикации
     */
    nodesData: {
        selected: PublishNodeDataModel[];
        /**
         * Доступные ноды публикации
         */
        available: PublishNodeDataModel[];
    }
};