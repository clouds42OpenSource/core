import { Box } from '@mui/material';
import { AccountDatabaseRestoreModelType, PageEnumType } from 'app/common/enums';
import { checkOnValidField, checkValidOfStringField } from 'app/common/functions';
import { IErrorsType } from 'app/common/interfaces';
import { TabSqlServersProcessId } from 'app/modules/segments/TabSqlServers';
import { CreateCloudSqlServerThunkParams } from 'app/modules/segments/TabSqlServers/store/reducer/createCloudSqlServerReducer/params';
import { EditCloudSqlServerThunkParams } from 'app/modules/segments/TabSqlServers/store/reducer/editCloudSqlServerReducer/params';
import { GetCloudSqlServerThunkParams } from 'app/modules/segments/TabSqlServers/store/reducer/getCloudSqlServerReducer/params';
import { CreateCloudSqlServerThunk, EditCloudSqlServerThunk, GetCloudSqlServerThunk } from 'app/modules/segments/TabSqlServers/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { Dialog } from 'app/views/components/controls/Dialog';
import { ComboBoxForm, TextBoxForm } from 'app/views/components/controls/forms';
import { getDialogButtons } from 'app/views/modules/Segments/common/functions';
import { getRestoreModelTypes } from 'app/views/modules/Segments/views/SqlServersTabView/functions';
import { SqlServersTabDataForm, SqlServersTabDataFormFieldsNamesType, SqlServersTabLengthEnumType } from 'app/views/modules/Segments/views/SqlServersTabView/types';
import { ShowErrorMessage } from 'app/views/modules/_common/components/ShowErrorMessage';
import { WatchReducerProcessActionState } from 'app/views/modules/_common/components/WatchReducerProcessActionState';
import { CloudSqlServerDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabSqlServersApiProxy';
import { IReducerProcessActionInfo } from 'core/redux/interfaces';
import { Component } from 'react';
import { connect } from 'react-redux';

type OwnProps = ReduxFormProps<SqlServersTabDataForm>;

type SqlServerModalProps = {
    /**
     * Поле для открытия модального окошка
     */
    isOpen: boolean;

    /**
     * ID sql сервера
     */
    sqlServerId?: string;

    /**
     * Функция на закрытие модального окошка
     */
    onClickToCloseModal: () => void;
};

type SqlServerModalState = {
    /**
     * Обьект Ошибок для валидации
     */
    errors: IErrorsType;

    /**
     * Тип страницы для динамического компонента редактирования/создания
     */
    pageType: PageEnumType;
};

type StateProps = {
    /**
     * Модель Sql сервера
     */
    cloudSqlServer: CloudSqlServerDataModel;
};

type DispatchProps = {
    dispatchCreateCloudSqlServerThunk: (args: CreateCloudSqlServerThunkParams) => void;
    dispatchGetCloudSqlServerThunk: (args: GetCloudSqlServerThunkParams) => void;
    dispatchEditCloudSqlServerThunk: (args: EditCloudSqlServerThunkParams) => void;
};

type AllProps = SqlServerModalProps & OwnProps & StateProps & DispatchProps & FloatMessageProps;

class SqlServerModalClass extends Component<AllProps, SqlServerModalState> {
    public constructor(props: AllProps) {
        super(props);

        this.state = {
            errors: {},
            pageType: this.props.sqlServerId ? PageEnumType.Edit : PageEnumType.Create
        };

        this.getDialogBody = this.getDialogBody.bind(this);
        this.onValueChange = this.onValueChange.bind(this);
        this.submitChanges = this.submitChanges.bind(this);
        this.createSqlServer = this.createSqlServer.bind(this);
        this.editSqlServer = this.editSqlServer.bind(this);
        this.isSqlServerFormValid = this.isSqlServerFormValid.bind(this);

        this.onProcessActionStateChanged = this.onProcessActionStateChanged.bind(this);
        this.onProcessGetCloudSqlServerServer = this.onProcessGetCloudSqlServerServer.bind(this);
        this.successGetCloudSqlServerServerState = this.successGetCloudSqlServerServerState.bind(this);
        this.errorGetCloudSqlServerServerState = this.errorGetCloudSqlServerServerState.bind(this);
        this.fillSqlServer = this.fillSqlServer.bind(this);

        this.props.reduxForm.setInitializeFormDataAction(this.defaultInitDataReduxForm);
    }

    public componentDidMount() {
        if (this.props.sqlServerId) {
            this.props.dispatchGetCloudSqlServerThunk({
                sqlServerId: this.props.sqlServerId,
                force: true
            });
        }
    }

    private onProcessActionStateChanged(processId: string, actionState: IReducerProcessActionInfo) {
        this.onProcessGetCloudSqlServerServer(processId, actionState);
    }

    private onProcessGetCloudSqlServerServer(processId: string, actionState: IReducerProcessActionInfo) {
        if (processId !== TabSqlServersProcessId.GetCloudSqlServer) return;
        this.successGetCloudSqlServerServerState(actionState);
        this.errorGetCloudSqlServerServerState(actionState);
    }

    private onValueChange<TValue, TForm extends string = SqlServersTabDataFormFieldsNamesType>(formName: TForm, newValue: TValue) {
        this.props.reduxForm.updateReduxFormFields({
            [formName]: newValue
        });

        const { errors } = this.state;

        if (typeof newValue === 'string') {
            switch (formName) {
                case 'connectionAddress':
                    errors[formName] = checkValidOfStringField(newValue, 'Заполните адрес подключения', SqlServersTabLengthEnumType.ConnectionAddress);
                    break;
                case 'description':
                    errors[formName] = checkValidOfStringField(newValue, undefined, SqlServersTabLengthEnumType.Description);
                    break;
                case 'name':
                    errors[formName] = checkValidOfStringField(newValue, 'Заполните название', SqlServersTabLengthEnumType.Name);
                    break;
                default:
                    break;
            }

            this.setState({ errors });
        }
    }

    private getDialogBody() {
        const formFields = this.props.reduxForm.getReduxFormFields(false);
        const { errors } = this.state;

        return (
            <Box display="flex" flexDirection="column" gap="12px">
                <div>
                    <TextBoxForm
                        formName="name"
                        label="Название сервера"
                        placeholder="Введите название"
                        value={ formFields.name }
                        onValueChange={ this.onValueChange }
                    />
                    <ShowErrorMessage errors={ errors } formName="name" />
                </div>
                <div>
                    <TextBoxForm
                        formName="connectionAddress"
                        label="Адрес подключения"
                        placeholder="Введите адрес"
                        value={ formFields.connectionAddress }
                        onValueChange={ this.onValueChange }
                    />
                    <ShowErrorMessage errors={ errors } formName="connectionAddress" />
                </div>
                <div>
                    <ComboBoxForm
                        items={ getRestoreModelTypes() }
                        formName="restoreModelType"
                        label="Модель восстановления"
                        value={ formFields.restoreModelType }
                        onValueChange={ this.onValueChange }
                    />
                </div>
                <div>
                    <TextBoxForm
                        type="textarea"
                        formName="description"
                        label="Описание"
                        placeholder="Введите описание"
                        value={ formFields.description }
                        onValueChange={ this.onValueChange }
                    />
                    <ShowErrorMessage errors={ errors } formName="description" />
                </div>
            </Box>
        );
    }

    private successGetCloudSqlServerServerState(actionState: IReducerProcessActionInfo) {
        if (!actionState.isInSuccessState) return;
        this.props.reduxForm.updateReduxFormFields({
            ...this.props.cloudSqlServer,
            restoreModelType: this.props.cloudSqlServer.restoreModelType?.toString()
        });
    }

    private errorGetCloudSqlServerServerState(actionState: IReducerProcessActionInfo, error?: Error) {
        if (!actionState.isInErrorState) return;
        this.props.floatMessage.show(MessageType.Error, `Ошибка при получении sql сервера: ${ error?.message }`);
    }

    private submitChanges() {
        if (!this.isSqlServerFormValid()) return;
        this.createSqlServer();
        this.editSqlServer();
    }

    private createSqlServer() {
        if (this.state.pageType !== PageEnumType.Create) return;
        this.props.dispatchCreateCloudSqlServerThunk({
            ...this.fillSqlServer(),
            force: true
        });
    }

    private editSqlServer() {
        if (this.state.pageType !== PageEnumType.Edit) return;
        this.props.dispatchEditCloudSqlServerThunk({
            ...this.fillSqlServer(),
            id: this.props.sqlServerId ?? '',
            force: true
        });
    }

    private fillSqlServer(): CloudSqlServerDataModel {
        const formFields = this.props.reduxForm.getReduxFormFields(false);
        return {
            connectionAddress: formFields.connectionAddress,
            description: formFields.description,
            name: formFields.name,
            restoreModelType: Number(formFields.restoreModelType),
            id: ''
        };
    }

    private defaultInitDataReduxForm(): SqlServersTabDataForm | undefined {
        return {
            connectionAddress: '',
            description: '',
            name: '',
            restoreModelType: AccountDatabaseRestoreModelType[AccountDatabaseRestoreModelType.Simple]
        };
    }

    private isSqlServerFormValid() {
        const formFields = this.props.reduxForm.getReduxFormFields(false);
        const errors: IErrorsType = {};
        const arrayResults: boolean[] = [];

        arrayResults.push(checkOnValidField(errors, 'description', formFields.description, undefined, SqlServersTabLengthEnumType.Description));
        arrayResults.push(checkOnValidField(errors, 'connectionAddress', formFields.connectionAddress, 'Заполните адрес подключения', SqlServersTabLengthEnumType.ConnectionAddress));
        arrayResults.push(checkOnValidField(errors, 'name', formFields.name, 'Заполните название', SqlServersTabLengthEnumType.Name));

        if (arrayResults.some(item => !item)) {
            this.props.floatMessage.show(MessageType.Warning, 'Форма к отправке не готова. Пожалуйста, заполните все поля.');
            this.setState({ errors });
            return false;
        }

        return true;
    }

    public render() {
        const { isOpen } = this.props;
        const isEditTypePage = this.state.pageType === PageEnumType.Edit;
        const titleText = isEditTypePage ? 'Редактирование' : 'Создание';

        return (
            <>
                <Dialog
                    disableBackdropClick={ true }
                    title={ `${ titleText } SQL сервера` }
                    isOpen={ isOpen }
                    dialogWidth="sm"
                    isTitleSmall={ true }
                    titleTextAlign="left"
                    titleFontSize={ 14 }
                    dialogVerticalAlign="center"
                    onCancelClick={ this.props.onClickToCloseModal }
                    buttons={ getDialogButtons({
                        pageType: this.state.pageType,
                        onClickToCloseModal: this.props.onClickToCloseModal,
                        submitChanges: this.submitChanges
                    }) }
                >
                    {
                        this.getDialogBody()
                    }
                </Dialog>
                <WatchReducerProcessActionState
                    watch={ [{
                        reducerStateName: 'TabSqlServersState',
                        processIds: [
                            TabSqlServersProcessId.GetCloudSqlServer
                        ]
                    }] }
                    onProcessActionStateChanged={ this.onProcessActionStateChanged }
                />
            </>
        );
    }
}

const SqlServerModalConnected = connect<StateProps, DispatchProps, OwnProps, AppReduxStoreState>(
    state => {
        const tabSqlServersState = state.TabSqlServersState;
        const { cloudSqlServer } = tabSqlServersState;

        return {
            cloudSqlServer
        };
    },
    {
        dispatchCreateCloudSqlServerThunk: CreateCloudSqlServerThunk.invoke,
        dispatchGetCloudSqlServerThunk: GetCloudSqlServerThunk.invoke,
        dispatchEditCloudSqlServerThunk: EditCloudSqlServerThunk.invoke
    }
)(SqlServerModalClass);

export const SqlServerModalView = withReduxForm(withFloatMessages(SqlServerModalConnected),
    {
        reduxFormName: 'Sql_Server_Modal_Form',
        resetOnUnmount: true
    });