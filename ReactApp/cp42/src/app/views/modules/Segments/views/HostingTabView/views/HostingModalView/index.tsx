import { Box } from '@mui/material';
import { PageEnumType } from 'app/common/enums';
import { checkOnValidField, checkValidOfStringField } from 'app/common/functions';
import { IErrorsType } from 'app/common/interfaces';
import { TabHostingProcessId } from 'app/modules/segments/TabHosting';
import { CreateCoreHostingThunkParams } from 'app/modules/segments/TabHosting/store/reducer/createCoreHostingReducer/params';
import { EditCoreHostingThunkParams } from 'app/modules/segments/TabHosting/store/reducer/editCoreHostingReducer/params';
import { GetCoreHostingThunkParams } from 'app/modules/segments/TabHosting/store/reducer/getCoreHostingReducer/params';
import { CreateCoreHostingThunk, EditCoreHostingThunk, GetCoreHostingThunk } from 'app/modules/segments/TabHosting/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { Dialog } from 'app/views/components/controls/Dialog';
import { TextBoxForm } from 'app/views/components/controls/forms';
import { getDialogButtons } from 'app/views/modules/Segments/common/functions';
import { HostingTabDataForm, HostingTabDataFormFieldsNamesType, HostingTabLengthEnumType } from 'app/views/modules/Segments/views/HostingTabView/types';
import { ShowErrorMessage } from 'app/views/modules/_common/components/ShowErrorMessage';
import { WatchReducerProcessActionState } from 'app/views/modules/_common/components/WatchReducerProcessActionState';
import { CoreHostingDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabHostinApiProxy';
import { IReducerProcessActionInfo } from 'core/redux/interfaces';
import { Component } from 'react';
import { connect } from 'react-redux';

type OwnProps = ReduxFormProps<HostingTabDataForm>;

type HostingModalProps = {
    /**
     * Поле для открытия модального окошка
     */
    isOpen: boolean;

    /**
     * ID хостинга облака
     */
    coreHostingId?: string;

    /**
     * Функция на закрытие модального окошка
     */
    onClickToCloseModal: () => void;
};

type HostingModalState = {
    /**
     * Обьект Ошибок для валидации
     */
    errors: IErrorsType;

    /**
     * Тип страницы для динамического компонента редактирования/создания
     */
    pageType: PageEnumType;
};

type StateProps = {
    /**
     * Модель хостинга
     */
    cloudHosting: CoreHostingDataModel;
};

type DispatchProps = {
    dispatchGetCoreHostingThunk: (args: GetCoreHostingThunkParams) => void;
    dispatchCreateCoreHostingThunk: (args: CreateCoreHostingThunkParams) => void;
    dispatchEditCoreHostingThunk: (args: EditCoreHostingThunkParams) => void;
};

type AllProps = HostingModalProps & OwnProps & StateProps & DispatchProps & FloatMessageProps;

class HostingModalClass extends Component<AllProps, HostingModalState> {
    public constructor(props: AllProps) {
        super(props);

        this.state = {
            errors: {},
            pageType: this.props.coreHostingId ? PageEnumType.Edit : PageEnumType.Create
        };

        this.getDialogBody = this.getDialogBody.bind(this);
        this.onValueChange = this.onValueChange.bind(this);
        this.submitChanges = this.submitChanges.bind(this);
        this.createHosting = this.createHosting.bind(this);
        this.editHosting = this.editHosting.bind(this);
        this.isHostingFormValid = this.isHostingFormValid.bind(this);

        this.onProcessActionStateChanged = this.onProcessActionStateChanged.bind(this);
        this.onProcessGetHosting = this.onProcessGetHosting.bind(this);
        this.successGetHostingState = this.successGetHostingState.bind(this);
        this.errorGetHostingState = this.errorGetHostingState.bind(this);
        this.fillHosting = this.fillHosting.bind(this);

        this.props.reduxForm.setInitializeFormDataAction(this.defaultInitDataReduxForm);
    }

    public componentDidMount() {
        if (this.props.coreHostingId) {
            this.props.dispatchGetCoreHostingThunk({
                coreHostingId: this.props.coreHostingId,
                force: true
            });
        }
    }

    private onProcessActionStateChanged(processId: string, actionState: IReducerProcessActionInfo) {
        this.onProcessGetHosting(processId, actionState);
    }

    private onProcessGetHosting(processId: string, actionState: IReducerProcessActionInfo) {
        if (processId !== TabHostingProcessId.GetHosting) return;
        this.successGetHostingState(actionState);
        this.errorGetHostingState(actionState);
    }

    private onValueChange<TValue, TForm extends string = HostingTabDataFormFieldsNamesType>(formName: TForm, newValue: TValue) {
        this.props.reduxForm.updateReduxFormFields({
            [formName]: newValue
        });

        const { errors } = this.state;

        if (typeof newValue === 'string') {
            switch (formName) {
                case 'name':
                    errors[formName] = checkValidOfStringField(newValue, 'Заполните название', HostingTabLengthEnumType.Name);
                    break;
                case 'uploadFilesPath':
                    errors[formName] = checkValidOfStringField(newValue, 'Заполните путь', HostingTabLengthEnumType.UploadFilesPath);
                    break;
                case 'uploadApiUrl':
                    errors[formName] = checkValidOfStringField(newValue, 'Заполните адрес', HostingTabLengthEnumType.UploadApiUrl);
                    break;
                default:
                    break;
            }

            this.setState({ errors });
        }
    }

    private getDialogBody() {
        const formFields = this.props.reduxForm.getReduxFormFields(false);
        const { errors } = this.state;

        return (
            <Box display="flex" flexDirection="column" gap="12px">
                <div>
                    <TextBoxForm
                        formName="name"
                        label="Название хостинга"
                        placeholder="Введите название"
                        value={ formFields.name }
                        onValueChange={ this.onValueChange }
                    />
                    <ShowErrorMessage errors={ errors } formName="name" />
                </div>
                <div>
                    <TextBoxForm
                        formName="uploadFilesPath"
                        label="Путь для загружаемых файлов"
                        placeholder="Введите путь"
                        value={ formFields.uploadFilesPath }
                        onValueChange={ this.onValueChange }
                    />
                    <ShowErrorMessage errors={ errors } formName="uploadFilesPath" />
                </div>
                <div>
                    <TextBoxForm
                        formName="uploadApiUrl"
                        label="Адрес API для загрузки файлов"
                        placeholder="Введите адрес"
                        value={ formFields.uploadApiUrl }
                        onValueChange={ this.onValueChange }
                    />
                    <ShowErrorMessage errors={ errors } formName="uploadApiUrl" />
                </div>
            </Box>
        );
    }

    private successGetHostingState(actionState: IReducerProcessActionInfo) {
        if (!actionState.isInSuccessState) return;
        this.props.reduxForm.updateReduxFormFields({
            ...this.props.cloudHosting
        });
    }

    private errorGetHostingState(actionState: IReducerProcessActionInfo, error?: Error) {
        if (!actionState.isInErrorState) return;
        this.props.floatMessage.show(MessageType.Error, `Ошибка при получении хостинга облака: ${ error?.message }`);
    }

    private submitChanges() {
        if (!this.isHostingFormValid()) return;
        this.createHosting();
        this.editHosting();
    }

    private createHosting() {
        if (this.state.pageType !== PageEnumType.Create) return;

        const rest = this.fillHosting();
        this.props.dispatchCreateCoreHostingThunk({
            ...rest,
            force: true
        });
    }

    private editHosting() {
        if (this.state.pageType !== PageEnumType.Edit) return;
        this.props.dispatchEditCoreHostingThunk({
            ...this.fillHosting(),
            id: this.props.coreHostingId!,
            force: true
        });
    }

    private fillHosting(): CoreHostingDataModel {
        const formFields = this.props.reduxForm.getReduxFormFields(false);
        return {
            id: '',
            name: formFields.name,
            uploadApiUrl: formFields.uploadApiUrl,
            uploadFilesPath: formFields.uploadFilesPath
        };
    }

    private defaultInitDataReduxForm(): HostingTabDataForm | undefined {
        return {
            name: '',
            uploadApiUrl: '',
            uploadFilesPath: ''
        };
    }

    private isHostingFormValid() {
        const formFields = this.props.reduxForm.getReduxFormFields(false);
        const errors: IErrorsType = {};
        const arrayResults: boolean[] = [];

        arrayResults.push(checkOnValidField(errors, 'name', formFields.name, 'Заполните название', HostingTabLengthEnumType.Name));
        arrayResults.push(checkOnValidField(errors, 'uploadFilesPath', formFields.uploadFilesPath, 'Заполните путь', HostingTabLengthEnumType.UploadFilesPath));
        arrayResults.push(checkOnValidField(errors, 'uploadApiUrl', formFields.uploadApiUrl, 'Заполните адрес', HostingTabLengthEnumType.UploadApiUrl));

        if (arrayResults.some(item => !item)) {
            this.props.floatMessage.show(MessageType.Warning, 'Форма к отправке не готова. Пожалуйста, заполните все поля.');
            this.setState({ errors });
            return false;
        }

        return true;
    }

    public render() {
        const { isOpen } = this.props;
        const isEditTypePage = this.state.pageType === PageEnumType.Edit;
        const titleText = isEditTypePage ? 'Редактирование' : 'Создание';

        return (
            <>
                <Dialog
                    disableBackdropClick={ true }
                    title={ `${ titleText } хостинга` }
                    isOpen={ isOpen }
                    dialogWidth="sm"
                    isTitleSmall={ true }
                    titleTextAlign="left"
                    titleFontSize={ 14 }
                    dialogVerticalAlign="center"
                    onCancelClick={ this.props.onClickToCloseModal }
                    buttons={ getDialogButtons({
                        pageType: this.state.pageType,
                        onClickToCloseModal: this.props.onClickToCloseModal,
                        submitChanges: this.submitChanges
                    }) }
                >
                    {
                        this.getDialogBody()
                    }
                </Dialog>
                <WatchReducerProcessActionState
                    watch={ [{
                        reducerStateName: 'TabHostingState',
                        processIds: [
                            TabHostingProcessId.GetHosting
                        ]
                    }] }
                    onProcessActionStateChanged={ this.onProcessActionStateChanged }
                />
            </>
        );
    }
}

const HostingModalConnected = connect<StateProps, DispatchProps, OwnProps, AppReduxStoreState>(
    state => {
        const tabHostingState = state.TabHostingState;
        const { cloudHosting } = tabHostingState;

        return {
            cloudHosting
        };
    },
    {
        dispatchGetCoreHostingThunk: GetCoreHostingThunk.invoke,
        dispatchCreateCoreHostingThunk: CreateCoreHostingThunk.invoke,
        dispatchEditCoreHostingThunk: EditCoreHostingThunk.invoke
    }
)(HostingModalClass);

export const HostingModalView = withReduxForm(withFloatMessages(HostingModalConnected),
    {
        reduxFormName: 'Hosting_Modal_Form',
        resetOnUnmount: true
    });