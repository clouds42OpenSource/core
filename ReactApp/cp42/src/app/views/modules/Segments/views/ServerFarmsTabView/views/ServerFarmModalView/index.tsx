import { Box } from '@mui/material';
import { PageEnumType } from 'app/common/enums';
import { checkOnValidField, checkValidOfStringField } from 'app/common/functions';
import { IErrorsType } from 'app/common/interfaces';
import { TabServerFarmsProcessId } from 'app/modules/segments/TabServerFarms';
import { CreateServerFarmThunkParams } from 'app/modules/segments/TabServerFarms/store/reducer/createServerFarmReducer/params';
import { EditServerFarmThunkParams } from 'app/modules/segments/TabServerFarms/store/reducer/editServerFarmReducer/params';
import { GetServerFarmThunkParams } from 'app/modules/segments/TabServerFarms/store/reducer/getServerFarmReducer/params';
import { CreateServerFarmThunk, EditServerFarmThunk, GetAllPublishNodesThunk, GetServerFarmThunk } from 'app/modules/segments/TabServerFarms/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { Dialog } from 'app/views/components/controls/Dialog';
import { TextBoxForm } from 'app/views/components/controls/forms';
import { DualListBoxForm } from 'app/views/components/controls/forms/DualListBoxForm';
import { DualListItem, DualListStateListTypes } from 'app/views/components/controls/forms/DualListBoxForm/views/types';
import { getDialogButtons } from 'app/views/modules/Segments/common/functions';
import { ServerFarmsTabDataForm, ServerFarmsTabDataFormFieldsNamesType, ServerFarmsTabLengthEnumType } from 'app/views/modules/Segments/views/ServerFarmsTabView/types';
import { ShowErrorMessage } from 'app/views/modules/_common/components/ShowErrorMessage';
import { WatchReducerProcessActionState } from 'app/views/modules/_common/components/WatchReducerProcessActionState';
import { ServerFarmModel, ServerFarmParams, ServerFarmPublishNodeDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabServerFarmsApiProxy';
import { IReducerProcessActionInfo } from 'core/redux/interfaces';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';
import { Component } from 'react';
import { connect } from 'react-redux';

type OwnProps = ReduxFormProps<ServerFarmsTabDataForm>;

type ServerFarmModalProps = {
    /**
     * Поле для открытия модального окошка
     */
    isOpen: boolean;

    /**
     * ID фермы сервера
     */
    serverFarmId?: string;

    /**
     * Функция на закрытие модального окошка
     */
    onClickToCloseModal: () => void;
};

type ServerFarmModalState = {
    /**
     * Обьект Ошибок для валидации
     */
    errors: IErrorsType;

    /**
     * Тип страницы для динамического компонента редактирования/создания
     */
    pageType: PageEnumType;
};

type StateProps = {
    /**
     * Модель фермы серверов
     */
    serverFarm: ServerFarmModel;

    /**
     * Массив нода публикаций
     */
    availableNodes: ServerFarmPublishNodeDataModel[];
};

type DispatchProps = {
    dispatchGetServerFarmThunk: (args: GetServerFarmThunkParams) => void;
    dispatchGetAllPublishNodesThunk: (args: IForceThunkParam) => void;
    dispatchCreateServerFarmThunk: (args: CreateServerFarmThunkParams) => void;
    dispatchEditServerFarmThunk: (args: EditServerFarmThunkParams) => void;
};

type AllProps = ServerFarmModalProps & OwnProps & StateProps & DispatchProps & FloatMessageProps;

class ServerFarmModalClass extends Component<AllProps, ServerFarmModalState> {
    public constructor(props: AllProps) {
        super(props);

        this.state = {
            errors: {},
            pageType: this.props.serverFarmId ? PageEnumType.Edit : PageEnumType.Create
        };

        this.getDialogBody = this.getDialogBody.bind(this);
        this.onValueChange = this.onValueChange.bind(this);
        this.submitChanges = this.submitChanges.bind(this);
        this.createServerFarm = this.createServerFarm.bind(this);
        this.editServerFarm = this.editServerFarm.bind(this);
        this.isServerFarmFormValid = this.isServerFarmFormValid.bind(this);

        this.onProcessActionStateChanged = this.onProcessActionStateChanged.bind(this);
        this.onProcessGetServerFarm = this.onProcessGetServerFarm.bind(this);
        this.onProcessGetAllPublishNodes = this.onProcessGetAllPublishNodes.bind(this);
        this.successGetServerFarmState = this.successGetServerFarmState.bind(this);
        this.successGetAllPublishNodesState = this.successGetAllPublishNodesState.bind(this);
        this.errorNotificationState = this.errorNotificationState.bind(this);
        this.fillServerFarm = this.fillServerFarm.bind(this);
        this.onValueChangeDualList = this.onValueChangeDualList.bind(this);
        this.checkSelectedPublishNodes = this.checkSelectedPublishNodes.bind(this);

        this.props.reduxForm.setInitializeFormDataAction(this.defaultInitDataReduxForm);
    }

    public componentDidMount() {
        this.props.serverFarmId
            ? this.props.dispatchGetServerFarmThunk({
                serverFarmId: this.props.serverFarmId,
                force: true
            })
            : this.props.dispatchGetAllPublishNodesThunk({
                force: true
            });
    }

    private onProcessActionStateChanged(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        this.onProcessGetServerFarm(processId, actionState, error);
        this.onProcessGetAllPublishNodes(processId, actionState, error);
    }

    private onProcessGetServerFarm(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId !== TabServerFarmsProcessId.GetServerFarm) return;
        this.successGetServerFarmState(actionState);
        this.errorNotificationState(actionState, `Ошибка при получении фермы сервера: ${ error?.message }`);
    }

    private onProcessGetAllPublishNodes(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId !== TabServerFarmsProcessId.GetAllPublishNodes) return;
        this.successGetAllPublishNodesState(actionState);
        this.errorNotificationState(actionState, `Ошибка при получении всех доступных нодов: ${ error?.message }`);
    }

    private onValueChangeDualList(_fieldName: string, newLeftValue: DualListItem<string>[], newRightValue: DualListItem<string>[]) {
        this.props.reduxForm.updateReduxFormFields({
            servers: newRightValue.map<ServerFarmPublishNodeDataModel>(item => ({
                id: item.key,
                address: item.label,
                description: '',
                isAvailable: true
            })),
            availableServers: newLeftValue.map<ServerFarmPublishNodeDataModel>(item => ({
                id: item.key,
                address: item.label,
                description: '',
                isAvailable: true
            }))
        });

        return true;
    }

    private onValueChange<TValue, TForm extends string = ServerFarmsTabDataFormFieldsNamesType>(formName: TForm, newValue: TValue) {
        this.props.reduxForm.updateReduxFormFields({
            [formName]: newValue
        });

        const { errors } = this.state;

        if (typeof newValue === 'string') {
            switch (formName) {
                case 'name':
                    errors[formName] = checkValidOfStringField(newValue, 'Заполните название', ServerFarmsTabLengthEnumType.Name);
                    break;
                case 'description':
                    errors[formName] = checkValidOfStringField(newValue, undefined, ServerFarmsTabLengthEnumType.Description);
                    break;
                default:
                    break;
            }

            this.setState({ errors });
        }
    }

    private getDialogBody() {
        const formFields = this.props.reduxForm.getReduxFormFields(false);
        const { errors } = this.state;

        return (
            <Box display="flex" flexDirection="column" gap="12px">
                <div>
                    <TextBoxForm
                        formName="name"
                        label="Название фермы"
                        placeholder="Введите название"
                        value={ formFields.name }
                        onValueChange={ this.onValueChange }
                        isReadOnly={ this.state.pageType === PageEnumType.Edit }
                    />
                    <ShowErrorMessage errors={ errors } formName="name" />
                </div>
                <div>
                    <TextBoxForm
                        formName="description"
                        label="Описание фермы"
                        placeholder="Введите описание"
                        value={ formFields.description }
                        onValueChange={ this.onValueChange }
                    />
                    <ShowErrorMessage errors={ errors } formName="description" />
                </div>
                <div>
                    <DualListBoxForm
                        allowSelectAll={ true }
                        mainList={ DualListStateListTypes.leftList }
                        leftListLabel="Доступные ноды для выбора:"
                        rightListLabel="Выбранные ноды:"
                        formName="availableNodes"
                        leftListItems={ this.getDualListItems(formFields.availableServers) }
                        rightListItems={ this.getDualListItems(formFields.servers) }
                        isFormDisabled={ false }
                        onValueChange={ this.onValueChangeDualList }
                    />
                </div>
            </Box>
        );
    }

    private getDualListItems(items: ServerFarmPublishNodeDataModel[]): Array<DualListItem<string>> {
        if (items) {
            return items.map(item => ({
                label: item.address,
                key: item.id
            }));
        }

        return [];
    }

    private successGetAllPublishNodesState(actionState: IReducerProcessActionInfo) {
        if (!actionState.isInSuccessState) return;
        this.props.reduxForm.updateReduxFormFields({
            availableServers: this.props.availableNodes
        });
    }

    private successGetServerFarmState(actionState: IReducerProcessActionInfo) {
        if (!actionState.isInSuccessState) return;
        const { nodesData, ...rest } = this.props.serverFarm;
        this.props.reduxForm.updateReduxFormFields({
            availableServers: nodesData.available,
            servers: nodesData.selected,
            ...rest
        });
    }

    private errorNotificationState(actionState: IReducerProcessActionInfo, errorMessage: string) {
        if (!actionState.isInErrorState) return;
        this.props.floatMessage.show(MessageType.Error, errorMessage);
    }

    private submitChanges() {
        if (!this.isServerFarmFormValid()) return;
        this.createServerFarm();
        this.editServerFarm();
    }

    private createServerFarm() {
        if (this.state.pageType !== PageEnumType.Create) return;
        this.props.dispatchCreateServerFarmThunk({
            ...this.fillServerFarm(),
            force: true
        });
    }

    private editServerFarm() {
        if (this.state.pageType !== PageEnumType.Edit) return;
        this.props.dispatchEditServerFarmThunk({
            ...this.fillServerFarm(),
            id: this.props.serverFarmId ?? '',
            force: true
        });
    }

    private fillServerFarm(): ServerFarmParams {
        const formFields = this.props.reduxForm.getReduxFormFields(false);
        return {
            id: '',
            name: formFields.name,
            description: formFields.description,
            servers: formFields.servers
        };
    }

    private defaultInitDataReduxForm(): ServerFarmsTabDataForm | undefined {
        return {
            name: '',
            description: '',
            servers: [],
            availableServers: []
        };
    }

    private isServerFarmFormValid() {
        const formFields = this.props.reduxForm.getReduxFormFields(false);
        const errors: IErrorsType = {};
        const arrayResults: boolean[] = [];

        arrayResults.push(checkOnValidField(errors, 'name', formFields.name, 'Заполните название', ServerFarmsTabLengthEnumType.Name));
        arrayResults.push(checkOnValidField(errors, 'description', formFields.description, undefined, ServerFarmsTabLengthEnumType.Description));

        if (arrayResults.some(item => !item)) {
            this.props.floatMessage.show(MessageType.Warning, 'Форма к отправке не готова. Пожалуйста, заполните все поля.');
            this.setState({ errors });
            return false;
        }

        return this.checkSelectedPublishNodes();
    }

    private checkSelectedPublishNodes() {
        const formFields = this.props.reduxForm.getReduxFormFields(false);
        if (formFields.servers.length > 0) return true;
        this.props.floatMessage.show(MessageType.Warning, 'Форма к отправке не готова. Пожалуйста, выберите ноду публикации.');
        return false;
    }

    public render() {
        const { isOpen } = this.props;
        const isEditTypePage = this.state.pageType === PageEnumType.Edit;
        const titleText = isEditTypePage ? 'Редактирование' : 'Создание';

        return (
            <>
                <Dialog
                    disableBackdropClick={ true }
                    title={ `${ titleText } фермы` }
                    isOpen={ isOpen }
                    dialogWidth="md"
                    isTitleSmall={ true }
                    titleTextAlign="left"
                    titleFontSize={ 14 }
                    dialogVerticalAlign="center"
                    onCancelClick={ this.props.onClickToCloseModal }
                    buttons={ getDialogButtons({
                        pageType: this.state.pageType,
                        onClickToCloseModal: this.props.onClickToCloseModal,
                        submitChanges: this.submitChanges
                    }) }
                >
                    {
                        this.getDialogBody()
                    }
                </Dialog>
                <WatchReducerProcessActionState
                    watch={ [{
                        reducerStateName: 'TabServerFarmState',
                        processIds: [
                            TabServerFarmsProcessId.GetServerFarm,
                            TabServerFarmsProcessId.GetAllPublishNodes
                        ]
                    }] }
                    onProcessActionStateChanged={ this.onProcessActionStateChanged }
                />
            </>
        );
    }
}

const ServerFarmModalConnected = connect<StateProps, DispatchProps, OwnProps, AppReduxStoreState>(
    state => {
        const tabServerFarmState = state.TabServerFarmState;
        const { serverFarm } = tabServerFarmState;
        const availableNodes = tabServerFarmState.availableNodes.map<ServerFarmPublishNodeDataModel>(item => ({
            address: item.address,
            description: item.description,
            id: item.id,
            isAvailable: true
        }));

        return {
            serverFarm,
            availableNodes
        };
    },
    {
        dispatchGetServerFarmThunk: GetServerFarmThunk.invoke,
        dispatchGetAllPublishNodesThunk: GetAllPublishNodesThunk.invoke,
        dispatchCreateServerFarmThunk: CreateServerFarmThunk.invoke,
        dispatchEditServerFarmThunk: EditServerFarmThunk.invoke
    }
)(ServerFarmModalClass);

export const ServerFarmModalView = withReduxForm(withFloatMessages(ServerFarmModalConnected),
    {
        reduxFormName: 'Server_Farm_Modal_Form',
        resetOnUnmount: true
    });