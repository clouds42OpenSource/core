import { Grid } from '@mui/material';
import { OutlinedButton, SuccessButton } from 'app/views/components/controls/Button';
import { ComboBoxForm, TextBoxForm } from 'app/views/components/controls/forms';
import { ComboboxItemModel } from 'app/views/components/controls/forms/ComboBox/models';
import style from 'app/views/modules/Segments/views/SegmentsTabView/style.module.css';
import { UpdatePlatformsDialog } from 'app/views/modules/Segments/views/SegmentsTabView/views/UpdatePlatformsDialog';
import { SegmentDropdownListResponseDto } from 'app/web/api/SegmentsProxy';
import { SegmentDropdownItemResponseDto } from 'app/web/api/SegmentsProxy/common';
import { Component } from 'react';
import css from '../../../../common/styles.module.css';

type SearchSegmentsState = {
    segmentName?: string,
    segmentDescription?: string,
    pathToUserFiles?: string,
    terminalFarmsIds?: string,
    enterpriseServersIds?: string,
    contentServersIds?: string,
    fileStorageServersIds?: string,
    sqlServersIds?: string,
    gatewayTerminalsIds?: string
    isOpenUpdatePlatformDialog: boolean;
};

type SearchSegmentsProps = {
    onClickSearch: () => void;
    segmentDropdownList: SegmentDropdownListResponseDto;
};

export class SearchSegmentsView extends Component<SearchSegmentsProps, SearchSegmentsState> {
    public constructor(props: SearchSegmentsProps) {
        super(props);

        this.state = {
            segmentName: '',
            segmentDescription: '',
            pathToUserFiles: '',
            isOpenUpdatePlatformDialog: false
        };

        this.onValueChange = this.onValueChange.bind(this);
        this.onValueChangeSearch = this.onValueChangeSearch.bind(this);
        this.searchTerminalFarms = this.searchTerminalFarms.bind(this);
        this.searchCloudEnterpriseServers = this.searchCloudEnterpriseServers.bind(this);
        this.searchCloudContentServers = this.searchCloudContentServers.bind(this);
        this.searchCloudFileStorageServers = this.searchCloudFileStorageServers.bind(this);
        this.searchCloudSqlServers = this.searchCloudSqlServers.bind(this);
        this.searchCloudGatewayTerminals = this.searchCloudGatewayTerminals.bind(this);
        this.openUpdatePlatformDialog = this.openUpdatePlatformDialog.bind(this);
        this.closeUpdatePlatformDialog = this.closeUpdatePlatformDialog.bind(this);
    }

    private onValueChange(formName: string, newValue: string) {
        this.setState(prevState => ({
            ...prevState,
            [formName]: newValue
        }));
    }

    private onValueChangeSearch(formName: string, newValue: string, items?: ComboboxItemModel<string>[]) {
        let array: string[] = [];

        items ? array = items.map(item => item.value) : array.push(newValue);

        this.setState(prevState => ({
            ...prevState,
            [formName]: array
        }));
    }

    private openUpdatePlatformDialog() {
        this.setState({
            isOpenUpdatePlatformDialog: true
        });
    }

    private closeUpdatePlatformDialog() {
        this.setState({
            isOpenUpdatePlatformDialog: false
        });
    }

    private searchTerminalFarms(searchValue: string, setItems: (items: SegmentDropdownItemResponseDto[]) => void) {
        setItems(this.props.segmentDropdownList.terminalFarms);
    }

    private searchCloudEnterpriseServers(searchValue: string, setItems: (items: SegmentDropdownItemResponseDto[]) => void) {
        setItems(this.props.segmentDropdownList.enterprise82Servers);
    }

    private searchCloudContentServers(searchValue: string, setItems: (items: SegmentDropdownItemResponseDto[]) => void) {
        setItems(this.props.segmentDropdownList.contentServers);
    }

    private searchCloudFileStorageServers(searchValue: string, setItems: (items: SegmentDropdownItemResponseDto[]) => void) {
        setItems(this.props.segmentDropdownList.fileStorageServers);
    }

    private searchCloudSqlServers(searchValue: string, setItems: (items: SegmentDropdownItemResponseDto[]) => void) {
        setItems(this.props.segmentDropdownList.sqlServers);
    }

    private searchCloudGatewayTerminals(searchValue: string, setItems: (items: SegmentDropdownItemResponseDto[]) => void) {
        setItems(this.props.segmentDropdownList.gatewayTerminals);
    }

    public render() {
        return (
            <>
                <Grid container={ true } spacing={ 2 } sx={ { marginBottom: '16px' } }>
                    <Grid item={ true } xs={ 12 } sm={ 6 } md={ 6 } lg={ 4 }>
                        <TextBoxForm
                            formName="segmentName"
                            label="Название"
                            placeholder="Введите название"
                            value={ this.state.segmentName }
                            onValueChange={ this.onValueChange }
                        />
                    </Grid>
                    <Grid item={ true } xs={ 12 } sm={ 6 } md={ 6 } lg={ 4 }>
                        <ComboBoxForm
                            label="Терминальная ферма"
                            formName="terminalFarmsIds"
                            items={ [{ value: '', text: 'Все' }, ...this.props.segmentDropdownList.terminalFarms.map(item => ({ value: item.key, text: item.value }))] }
                            value={ this.state.terminalFarmsIds ?? '' }
                            onValueChange={ this.onValueChange }
                            allowAutoComplete={ true }
                        />
                    </Grid>
                    <Grid item={ true } xs={ 12 } sm={ 6 } md={ 6 } lg={ 4 }>
                        <ComboBoxForm
                            items={ [{ value: '', text: 'Все' }, ...this.props.segmentDropdownList.enterprise82Servers.map(item => ({
                                value: item.key,
                                text: item.value
                            })), ...this.props.segmentDropdownList.enterprise83Servers.map(item => ({ value: item.key, text: item.value }))] }
                            formName="enterpriseServersIds"
                            label="Сервер предприятия"
                            value={ this.state.enterpriseServersIds ?? '' }
                            onValueChange={ this.onValueChange }
                            allowAutoComplete={ true }
                        />
                    </Grid>
                    <Grid item={ true } xs={ 12 } sm={ 6 } md={ 6 } lg={ 4 }>
                        <ComboBoxForm
                            items={ [{ value: '', text: 'Все' }, ...this.props.segmentDropdownList.contentServers.map(item => ({ value: item.key, text: item.value }))] }
                            formName="contentServersIds"
                            label="Сервер публикации"
                            value={ this.state.contentServersIds ?? '' }
                            onValueChange={ this.onValueChange }
                            allowAutoComplete={ true }
                        />
                    </Grid>
                    <Grid item={ true } xs={ 12 } sm={ 6 } md={ 6 } lg={ 4 }>
                        <ComboBoxForm
                            items={ [{ value: '', text: 'Все' }, ...this.props.segmentDropdownList.fileStorageServers.map(item => ({ value: item.key, text: item.value }))] }
                            formName="fileStorageServersIds"
                            label="Файловое хранилище"
                            value={ this.state.fileStorageServersIds ?? '' }
                            onValueChange={ this.onValueChange }
                            allowAutoComplete={ true }
                        />
                    </Grid>
                    <Grid item={ true } xs={ 12 } sm={ 6 } md={ 6 } lg={ 4 }>
                        <ComboBoxForm
                            items={ [{ value: '', text: 'Все' }, ...this.props.segmentDropdownList.sqlServers.map(item => ({ value: item.key, text: item.value }))] }
                            formName="sqlServersIds"
                            label="SQL сервер"
                            value={ this.state.sqlServersIds ?? '' }
                            onValueChange={ this.onValueChange }
                            allowAutoComplete={ true }
                        />
                    </Grid>
                    <Grid item={ true } xs={ 12 } sm={ 6 } md={ 6 } lg={ 4 }>
                        <ComboBoxForm
                            items={ [{ value: '', text: 'Все' }, ...this.props.segmentDropdownList.gatewayTerminals.map(item => ({ value: item.key, text: item.value }))] }
                            formName="gatewayTerminalsIds"
                            label="Терминальный шлюз"
                            value={ this.state.gatewayTerminalsIds ?? '' }
                            onValueChange={ this.onValueChange }
                            allowAutoComplete={ true }
                        />
                    </Grid>
                    <Grid item={ true } xs={ 12 } sm={ 6 } md={ 6 } lg={ 4 }>
                        <TextBoxForm
                            formName="pathToUserFiles"
                            label="Путь к файлам пользователей"
                            placeholder="Введите путь"
                            value={ this.state.pathToUserFiles }
                            onValueChange={ this.onValueChange }
                        />
                    </Grid>
                    <Grid item={ true } xs={ 12 } sm={ 6 } md={ 6 } lg={ 4 }>
                        <TextBoxForm
                            formName="segmentDescription"
                            label="Описание"
                            placeholder="Введите описание"
                            value={ this.state.segmentDescription }
                            onValueChange={ this.onValueChange }
                        />
                    </Grid>
                    <Grid item={ true } xs={ 12 } sm={ 6 } md={ 6 } lg={ 4 }>
                        <OutlinedButton
                            style={ { width: '100%' } }
                            kind="success"
                            className={ css['search-button'] }
                            onClick={ this.props.onClickSearch }
                        >
                            <i className="fa fa-search mr-1" />
                            Поиск
                        </OutlinedButton>
                    </Grid>
                    <Grid item={ true } xs={ 12 } sm={ 6 } md={ 6 } lg={ 4 } />
                    <Grid item={ true } xs={ 12 } sm={ 6 } md={ 6 } lg={ 4 }>
                        <SuccessButton onClick={ this.openUpdatePlatformDialog } fullWidth={ true } className={ style.searchButton }>
                            Обновить платформу 1С
                        </SuccessButton>
                    </Grid>
                </Grid>
                {
                    this.state.isOpenUpdatePlatformDialog && <UpdatePlatformsDialog isOpen={ this.state.isOpenUpdatePlatformDialog } onCancelClick={ this.closeUpdatePlatformDialog } />
                }
            </>
        );
    }
}