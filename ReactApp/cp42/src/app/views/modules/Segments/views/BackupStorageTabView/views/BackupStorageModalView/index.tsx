import { Box } from '@mui/material';
import { PageEnumType } from 'app/common/enums';
import { checkOnValidField, checkValidOfStringField } from 'app/common/functions';
import { IErrorsType } from 'app/common/interfaces';
import { TabBackupStorageProcessId } from 'app/modules/segments/TabBackupStorage';
import { CreateCloudBackupStorageThunkParams } from 'app/modules/segments/TabBackupStorage/store/reducer/createCloudBackupStorageReducer/params';
import { EditCloudBackupStorageThunkParams } from 'app/modules/segments/TabBackupStorage/store/reducer/editCloudBackupStorageReducer/params';
import { GetCloudBackupStorageThunkParams } from 'app/modules/segments/TabBackupStorage/store/reducer/getCloudBackupStorageReducer/params';
import { CreateCloudBackupStorageThunk, EditCloudBackupStorageThunk, GetCloudBackupStorageThunk } from 'app/modules/segments/TabBackupStorage/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { Dialog } from 'app/views/components/controls/Dialog';
import { TextBoxForm } from 'app/views/components/controls/forms';
import { getDialogButtons } from 'app/views/modules/Segments/common/functions';
import { BackupStorageTabDataForm, BackupStorageTabDataFormFieldsNamesType, BackupStorageTabLengthEnumType } from 'app/views/modules/Segments/views/BackupStorageTabView/types';
import { ShowErrorMessage } from 'app/views/modules/_common/components/ShowErrorMessage';
import { WatchReducerProcessActionState } from 'app/views/modules/_common/components/WatchReducerProcessActionState';
import { CloudBackupStorageDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabBackupStorageApiProxy';
import { IReducerProcessActionInfo } from 'core/redux/interfaces';
import { Component } from 'react';
import { connect } from 'react-redux';

type OwnProps = ReduxFormProps<BackupStorageTabDataForm>;

type BackupStorageModalProps = {
    /**
     * Поле для открытия модального окошка
     */
    isOpen: boolean;

    /**
     * ID хранилища бэкапов
     */
    backupStorageId?: string;

    /**
     * Функция на закрытие модального окошка
     */
    onClickToCloseModal: () => void;
};

type BackupStorageModalState = {
    /**
     * Обьект Ошибок для валидации
     */
    errors: IErrorsType;

    /**
     * Тип страницы для динамического компонента редактирования/создания
     */
    pageType: PageEnumType;
};

type StateProps = {
    /**
     * Модель хранилища бэкапов
     */
    cloudBackupStorage: CloudBackupStorageDataModel;
};

type DispatchProps = {
    dispatchGetCloudBackupStorageThunk: (args: GetCloudBackupStorageThunkParams) => void;
    dispatchCreateCloudBackupStorageThunk: (args: CreateCloudBackupStorageThunkParams) => void;
    dispatchEditCloudBackupStorageThunk: (args: EditCloudBackupStorageThunkParams) => void;
};

type AllProps = BackupStorageModalProps & OwnProps & StateProps & DispatchProps & FloatMessageProps;

class BackupStorageModalClass extends Component<AllProps, BackupStorageModalState> {
    public constructor(props: AllProps) {
        super(props);

        this.state = {
            errors: {},
            pageType: this.props.backupStorageId ? PageEnumType.Edit : PageEnumType.Create
        };

        this.getDialogBody = this.getDialogBody.bind(this);
        this.onValueChange = this.onValueChange.bind(this);
        this.submitChanges = this.submitChanges.bind(this);
        this.createBackupStorage = this.createBackupStorage.bind(this);
        this.editBackupStorage = this.editBackupStorage.bind(this);
        this.isBackupStorageFormValid = this.isBackupStorageFormValid.bind(this);

        this.onProcessActionStateChanged = this.onProcessActionStateChanged.bind(this);
        this.onProcessGetCloudBackupStorage = this.onProcessGetCloudBackupStorage.bind(this);
        this.successGetCloudBackupStorageState = this.successGetCloudBackupStorageState.bind(this);
        this.errorGetCloudBackupStorageState = this.errorGetCloudBackupStorageState.bind(this);
        this.fillBackupStorage = this.fillBackupStorage.bind(this);

        this.props.reduxForm.setInitializeFormDataAction(this.defaultInitDataReduxForm);
    }

    public componentDidMount() {
        if (this.props.backupStorageId) {
            this.props.dispatchGetCloudBackupStorageThunk({
                backupStorageId: this.props.backupStorageId,
                force: true
            });
        }
    }

    private onProcessActionStateChanged(processId: string, actionState: IReducerProcessActionInfo) {
        this.onProcessGetCloudBackupStorage(processId, actionState);
    }

    private onProcessGetCloudBackupStorage(processId: string, actionState: IReducerProcessActionInfo) {
        if (processId !== TabBackupStorageProcessId.GetCloudBackupStorage) return;
        this.successGetCloudBackupStorageState(actionState);
        this.errorGetCloudBackupStorageState(actionState);
    }

    private onValueChange<TValue, TForm extends string = BackupStorageTabDataFormFieldsNamesType>(formName: TForm, newValue: TValue) {
        this.props.reduxForm.updateReduxFormFields({
            [formName]: newValue
        });

        const { errors } = this.state;

        if (typeof newValue === 'string') {
            switch (formName) {
                case 'connectionAddress':
                    errors[formName] = checkValidOfStringField(newValue, 'Заполните адрес подключения', BackupStorageTabLengthEnumType.ConnectionAddress);
                    break;
                case 'description':
                    errors[formName] = checkValidOfStringField(newValue, undefined, BackupStorageTabLengthEnumType.Description);
                    break;
                case 'name':
                    errors[formName] = checkValidOfStringField(newValue, 'Заполните название', BackupStorageTabLengthEnumType.Name);
                    break;
                default:
                    break;
            }

            this.setState({ errors });
        }
    }

    private getDialogBody() {
        const formFields = this.props.reduxForm.getReduxFormFields(false);
        const { errors } = this.state;

        return (
            <Box display="flex" flexDirection="column" gap="12px">
                <div>
                    <TextBoxForm
                        formName="name"
                        label="Название хранилища"
                        placeholder="Введите название"
                        value={ formFields.name }
                        onValueChange={ this.onValueChange }
                    />
                    <ShowErrorMessage errors={ errors } formName="name" />
                </div>
                <div>
                    <TextBoxForm
                        formName="connectionAddress"
                        label="Адрес подключения"
                        placeholder="Введите адрес"
                        value={ formFields.connectionAddress }
                        onValueChange={ this.onValueChange }
                    />
                    <ShowErrorMessage errors={ errors } formName="connectionAddress" />
                </div>
                <div>
                    <TextBoxForm
                        type="textarea"
                        formName="description"
                        label="Описание"
                        placeholder="Введите описание"
                        value={ formFields.description }
                        onValueChange={ this.onValueChange }
                    />
                    <ShowErrorMessage errors={ errors } formName="description" />
                </div>
            </Box>
        );
    }

    private successGetCloudBackupStorageState(actionState: IReducerProcessActionInfo) {
        if (!actionState.isInSuccessState) return;
        this.props.reduxForm.updateReduxFormFields({
            ...this.props.cloudBackupStorage
        });
    }

    private errorGetCloudBackupStorageState(actionState: IReducerProcessActionInfo, error?: Error) {
        if (!actionState.isInErrorState) return;
        this.props.floatMessage.show(MessageType.Error, `Ошибка при получении хранилища бекапов: ${ error?.message }`);
    }

    private submitChanges() {
        if (!this.isBackupStorageFormValid()) return;
        this.createBackupStorage();
        this.editBackupStorage();
    }

    private createBackupStorage() {
        if (this.state.pageType !== PageEnumType.Create) return;
        this.props.dispatchCreateCloudBackupStorageThunk({
            ...this.fillBackupStorage(),
            force: true
        });
    }

    private editBackupStorage() {
        if (this.state.pageType !== PageEnumType.Edit) return;
        this.props.dispatchEditCloudBackupStorageThunk({
            ...this.fillBackupStorage(),
            id: this.props.backupStorageId!,
            force: true
        });
    }

    private fillBackupStorage(): CloudBackupStorageDataModel {
        const formFields = this.props.reduxForm.getReduxFormFields(false);
        return {
            connectionAddress: formFields.connectionAddress,
            description: formFields.description,
            name: formFields.name,
            physicalPath: '',
            id: ''
        };
    }

    private defaultInitDataReduxForm(): BackupStorageTabDataForm | undefined {
        return {
            connectionAddress: '',
            description: '',
            name: ''
        };
    }

    private isBackupStorageFormValid() {
        const formFields = this.props.reduxForm.getReduxFormFields(false);
        const errors: IErrorsType = {};
        const arrayResults: boolean[] = [];

        arrayResults.push(checkOnValidField(errors, 'description', formFields.description, undefined, BackupStorageTabLengthEnumType.Description));
        arrayResults.push(checkOnValidField(errors, 'connectionAddress', formFields.connectionAddress, 'Заполните адрес подключения', BackupStorageTabLengthEnumType.ConnectionAddress));
        arrayResults.push(checkOnValidField(errors, 'name', formFields.name, 'Заполните название', BackupStorageTabLengthEnumType.Name));

        if (arrayResults.some(item => !item)) {
            this.props.floatMessage.show(MessageType.Warning, 'Форма к отправке не готова. Пожалуйста, заполните все поля.');
            this.setState({ errors });
            return false;
        }

        return true;
    }

    public render() {
        const { isOpen } = this.props;
        const isEditTypePage = this.state.pageType === PageEnumType.Edit;
        const titleText = isEditTypePage ? 'Редактирование' : 'Создание';

        return (
            <>
                <Dialog
                    disableBackdropClick={ true }
                    title={ `${ titleText } хранилище бекапов` }
                    isOpen={ isOpen }
                    dialogWidth="sm"
                    isTitleSmall={ true }
                    titleTextAlign="left"
                    titleFontSize={ 14 }
                    dialogVerticalAlign="center"
                    onCancelClick={ this.props.onClickToCloseModal }
                    buttons={ getDialogButtons({
                        pageType: this.state.pageType,
                        onClickToCloseModal: this.props.onClickToCloseModal,
                        submitChanges: this.submitChanges
                    }) }
                >
                    {
                        this.getDialogBody()
                    }
                </Dialog>
                <WatchReducerProcessActionState
                    watch={ [{
                        reducerStateName: 'TabBackupStorageState',
                        processIds: [
                            TabBackupStorageProcessId.GetCloudBackupStorage
                        ]
                    }] }
                    onProcessActionStateChanged={ this.onProcessActionStateChanged }
                />
            </>
        );
    }
}

const BackupStorageModalConnected = connect<StateProps, DispatchProps, OwnProps, AppReduxStoreState>(
    state => {
        const tabBackupStorageState = state.TabBackupStorageState;
        const { cloudBackupStorage } = tabBackupStorageState;

        return {
            cloudBackupStorage
        };
    },
    {
        dispatchGetCloudBackupStorageThunk: GetCloudBackupStorageThunk.invoke,
        dispatchCreateCloudBackupStorageThunk: CreateCloudBackupStorageThunk.invoke,
        dispatchEditCloudBackupStorageThunk: EditCloudBackupStorageThunk.invoke
    }
)(BackupStorageModalClass);

export const BackupStorageModalView = withReduxForm(withFloatMessages(BackupStorageModalConnected),
    {
        reduxFormName: 'Backup_Storage_Modal_Form',
        resetOnUnmount: true
    });