import ErrorOutlineIcon from '@mui/icons-material/ErrorOutline';
import { Box } from '@mui/material';
import { SortingKind } from 'app/common/enums';
import { TabEnterpriseServersProcessId } from 'app/modules/segments/TabEnterpriseServers';
import { DeleteEnterpriseServerThunkParams } from 'app/modules/segments/TabEnterpriseServers/store/reducer/deleteEnterpriseServerReducer/params';
import { GetEnterpriseServersThunkParams } from 'app/modules/segments/TabEnterpriseServers/store/reducer/getEnterpriseServersReducer/params';
import { DeleteEnterpriseServerThunk, GetEnterpriseServersThunk } from 'app/modules/segments/TabEnterpriseServers/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { SuccessButton, TextLinkButton } from 'app/views/components/controls/Button';
import { Dialog } from 'app/views/components/controls/Dialog';
import { TextOut } from 'app/views/components/TextOut';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { WatchReducerProcessActionState } from 'app/views/modules/_common/components/WatchReducerProcessActionState';
import { CommonSegmentEnumType } from 'app/views/modules/Segments/types';
import { EnterpriseServerModalView } from 'app/views/modules/Segments/views/EnterpriseServersTabView/views/EnterpriseServerModalView';
import { SearchEnterpriseServersView } from 'app/views/modules/Segments/views/EnterpriseServersTabView/views/SearchEnterpriseServersView';
import { SelectDataMetadataResponseDto, SortingDataModel } from 'app/web/common';
import { SelectDataCommonDataModel } from 'app/web/common/data-models';
import { EnterpriseServerDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabEnterpriseServersApiProxy';
import { IReducerProcessActionInfo } from 'core/redux/interfaces';
import { Component, createRef } from 'react';
import { connect } from 'react-redux';
import commonCss from '../../common/styles.module.css';
import css from './styles.module.css';

type EnterpriseServersTabState = {
    /**
     * Поле обозначающая что первая подгрузка данных была сделана
     */
    isFirstTimeLoaded: boolean;

    /**
     * Поле для открытия модального окошка
     */
    isOpen: boolean;

    /**
     * ID сервера 1С:Предприятие
     */
    enterpriseServerId?: string;
    /**
     * Состояние модального окна на удаление сервера 1С
     */
    deleteIsOpen: boolean;
    orderBy?: string;
};

type EnterpriseServersTabProps = {
    /**
     * Поле для подгрузки данных
     */
    isCanLoad: boolean;
};

type StateProps = {
    /**
     * Список данных списка сервера 1С:Предприятие
     */
    enterpriseServers: SelectDataMetadataResponseDto<EnterpriseServerDataModel>;

    /**
     * Если true, значит список сервера 1С:Предприятие
     */
    hasEnterpriseServersReceived: boolean;
};

type DispatchProps = {
    dispatchGetEnterpriseServersThunk: (args: GetEnterpriseServersThunkParams) => void;
    dispatchDeleteEnterpriseServerThunk: (args: DeleteEnterpriseServerThunkParams) => void;
};

type AllProps = EnterpriseServersTabProps & StateProps & DispatchProps & FloatMessageProps;

class EnterpriseServersTabClass extends Component<AllProps, EnterpriseServersTabState> {
    private _commonTableRef = createRef<CommonTableWithFilter<EnterpriseServerDataModel, undefined>>();

    private _searchEnterpriseServersRef = createRef<SearchEnterpriseServersView>();

    public constructor(props: AllProps) {
        super(props);

        this.state = {
            isOpen: false,
            isFirstTimeLoaded: false,
            deleteIsOpen: false,
            orderBy: ''
        };

        this.onClickToOpenModal = this.onClickToOpenModal.bind(this);
        this.onClickToCloseModal = this.onClickToCloseModal.bind(this);
        this.onClickToSearch = this.onClickToSearch.bind(this);
        this.onDataSelect = this.onDataSelect.bind(this);
        this.getFormatNameEnterpriseServer = this.getFormatNameEnterpriseServer.bind(this);
        this.notificationInSuccess = this.notificationInSuccess.bind(this);
        this.notificationInError = this.notificationInError.bind(this);
        this.openModalForDeleteEnterpriseServer = this.openModalForDeleteEnterpriseServer.bind(this);
        this.getButtonsForActions = this.getButtonsForActions.bind(this);

        this.onProcessActionStateChanged = this.onProcessActionStateChanged.bind(this);
        this.onProcessCreateEnterpriseServer = this.onProcessCreateEnterpriseServer.bind(this);
        this.onProcessEditEnterpriseServer = this.onProcessEditEnterpriseServer.bind(this);
        this.onProcessDeleteEnterpriseServer = this.onProcessDeleteEnterpriseServer.bind(this);
        this.onClickDeleteEnterpriseServer = this.onClickDeleteEnterpriseServer.bind(this);
        this.closeModalForDeleteEnterpriseServer = this.closeModalForDeleteEnterpriseServer.bind(this);
    }

    public componentDidUpdate() {
        const { isCanLoad, hasEnterpriseServersReceived } = this.props;
        const { isFirstTimeLoaded } = this.state;

        if (isCanLoad && !isFirstTimeLoaded && !hasEnterpriseServersReceived && this._commonTableRef.current) {
            this._commonTableRef.current.ReSelectData();
            this.setState({
                isFirstTimeLoaded: true
            });
        }
    }

    private onClickDeleteEnterpriseServer() {
        if (this.state.enterpriseServerId) {
            this.props.dispatchDeleteEnterpriseServerThunk({
                enterpriseServerId: this.state.enterpriseServerId,
                force: true
            });
            this.setState({
                deleteIsOpen: false
            });
        }
    }

    private onProcessActionStateChanged(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        this.onProcessCreateEnterpriseServer(processId, actionState, error);
        this.onProcessEditEnterpriseServer(processId, actionState, error);
        this.onProcessDeleteEnterpriseServer(processId, actionState, error);
    }

    private onProcessCreateEnterpriseServer(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId !== TabEnterpriseServersProcessId.CreateEnterpriseServer) return;
        this.notificationInSuccess(actionState, 'Сервер 1С успешно создан');
        this.notificationInError(actionState, error?.message);
    }

    private onProcessEditEnterpriseServer(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId !== TabEnterpriseServersProcessId.EditEnterpriseServer) return;
        this.notificationInSuccess(actionState, 'Сервер 1С успешно сохранен');
        this.notificationInError(actionState, error?.message);
    }

    private onProcessDeleteEnterpriseServer(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId !== TabEnterpriseServersProcessId.DeleteEnterpriseServer) return;
        this.notificationInSuccess(actionState, 'Сервер 1С успешно удален');
        this.notificationInError(actionState, error?.message);
    }

    private onClickToOpenModal(enterpriseServerId?: string) {
        this.setState({
            isOpen: true,
            enterpriseServerId
        });
    }

    private onClickToCloseModal() {
        this.setState({
            isOpen: false
        });
    }

    private onClickToSearch() {
        const {
            connectionAddress,
            description,
            name,
            version
        } = this._searchEnterpriseServersRef.current?.state!;

        this.props.dispatchGetEnterpriseServersThunk({
            filter: {
                connectionAddress,
                description,
                name,
                version,
            },
            orderBy: this.state.orderBy,
            pageNumber: 1,
            pageSize: CommonSegmentEnumType.DefaultPageSize,
            force: true
        });
    }

    private onDataSelect(args: SelectDataCommonDataModel<undefined>) {
        const {
            connectionAddress,
            description,
            name,
            version
        } = this._searchEnterpriseServersRef.current?.state!;

        const orderBy = args.sortingData ? this.getSortQuery(args.sortingData) : 'name.asc';

        this.setState({
            orderBy
        });

        this.props.dispatchGetEnterpriseServersThunk({
            filter: {
                connectionAddress,
                description,
                name,
                version,
            },
            pageNumber: args.pageNumber,
            orderBy,
            pageSize: CommonSegmentEnumType.DefaultPageSize,
            force: true
        });
    }

    private getButtonsForActions(Id: string) {
        return (
            <TextLinkButton
                className={ commonCss['delete-link'] }
                onClick={ () => { this.openModalForDeleteEnterpriseServer(Id); } }
            >
                <i className="fa fa-trash" />
            </TextLinkButton>
        );
    }

    private getSortQuery(args: SortingDataModel) {
        return `${ args.fieldName }.${ args.sortKind ? 'desc' : 'asc' }`;
    }

    private getFormatNameEnterpriseServer(name: string, data: EnterpriseServerDataModel) {
        return (
            <TextLinkButton onClick={ () => { this.onClickToOpenModal(data.id); } }>
                <TextOut fontSize={ 13 } className="text-link">
                    { name }
                </TextOut>
            </TextLinkButton>
        );
    }

    private openModalForDeleteEnterpriseServer(enterpriseServerId: string) {
        this.setState({
            enterpriseServerId,
            deleteIsOpen: true
        });
    }

    private closeModalForDeleteEnterpriseServer() {
        this.setState({
            deleteIsOpen: false
        });
    }

    private notificationInSuccess(actionState: IReducerProcessActionInfo, message: string) {
        if (!actionState.isInSuccessState) return;
        this.props.floatMessage.show(MessageType.Success, message);
        this._commonTableRef.current?.ReSelectData();
        this.onClickToCloseModal();
    }

    private notificationInError(actionState: IReducerProcessActionInfo, message?: string) {
        if (!actionState.isInErrorState) return;
        this.props.floatMessage.show(MessageType.Error, message);
    }

    public render() {
        const { enterpriseServers: { metadata, records } } = this.props;

        return (
            <>
                <SearchEnterpriseServersView
                    ref={ this._searchEnterpriseServersRef }
                    onClickToSearch={ this.onClickToSearch }
                />
                <CommonTableWithFilter
                    className={ css['custom-table'] }
                    ref={ this._commonTableRef }
                    isResponsiveTable={ true }
                    isBorderStyled={ true }
                    uniqueContextProviderStateId="TabEnterpriseServersContextProviderStateId"
                    tableProps={ {
                        dataset: records,
                        keyFieldName: 'id',
                        sorting: {
                            sortFieldName: 'name',
                            sortKind: SortingKind.Asc
                        },
                        fieldsView: {
                            name: {
                                caption: 'Название',
                                fieldWidth: 'auto',
                                fieldContentNoWrap: false,
                                isSortable: true,
                                format: this.getFormatNameEnterpriseServer
                            },
                            description: {
                                caption: 'Описание',
                                fieldWidth: 'auto',
                                fieldContentNoWrap: false,
                                isSortable: true
                            },
                            connectionAddress: {
                                caption: 'Адрес подключения',
                                fieldWidth: 'auto',
                                fieldContentNoWrap: false,
                                isSortable: true
                            },
                            version: {
                                caption: 'Версия',
                                fieldWidth: 'auto',
                                fieldContentNoWrap: false,
                                isSortable: true
                            },
                            id: {
                                caption: '',
                                fieldWidth: 'auto',
                                fieldContentNoWrap: false,
                                format: this.getButtonsForActions
                            }
                        },
                        pagination: {
                            currentPage: metadata.pageNumber,
                            pageSize: metadata.pageSize,
                            recordsCount: metadata.totalItemCount,
                            totalPages: metadata.pageCount
                        },
                    } }
                    onDataSelect={ this.onDataSelect }
                />
                <SuccessButton onClick={ () => this.onClickToOpenModal() }>
                    <i className="fa fa-plus-circle mr-1" />
                    Создать сервер 1С
                </SuccessButton>
                { this.state.isOpen &&
                    <EnterpriseServerModalView
                        isOpen={ this.state.isOpen }
                        enterpriseServerId={ this.state.enterpriseServerId }
                        onClickToCloseModal={ this.onClickToCloseModal }
                    />
                }
                <WatchReducerProcessActionState
                    watch={ [{
                        reducerStateName: 'TabEnterpriseServersState',
                        processIds: [
                            TabEnterpriseServersProcessId.CreateEnterpriseServer,
                            TabEnterpriseServersProcessId.EditEnterpriseServer,
                            TabEnterpriseServersProcessId.DeleteEnterpriseServer,
                        ]
                    }] }
                    onProcessActionStateChanged={ this.onProcessActionStateChanged }
                />
                <Dialog
                    isOpen={ this.state.deleteIsOpen }
                    dialogWidth="xs"
                    dialogVerticalAlign="center"
                    onCancelClick={ this.closeModalForDeleteEnterpriseServer }
                    buttons={
                        [
                            {
                                content: 'Нет',
                                kind: 'default',
                                variant: 'contained',
                                onClick: this.closeModalForDeleteEnterpriseServer
                            },
                            {
                                content: 'Да',
                                kind: 'error',
                                variant: 'contained',
                                onClick: this.onClickDeleteEnterpriseServer
                            }
                        ]
                    }
                >
                    <Box display="flex" flexDirection="column" alignItems="center" justifyContent="center">
                        <ErrorOutlineIcon color="warning" sx={ { width: '5em', height: '5em' } } />
                        <TextOut>Вы действительно хотите удалить Сервер 1С?</TextOut>
                    </Box>
                </Dialog>
            </>
        );
    }
}

const EnterpriseServersTabConnected = connect<StateProps, DispatchProps, NonNullable<unknown>, AppReduxStoreState>(
    state => {
        const tabEnterpriseServersState = state.TabEnterpriseServersState;
        const { enterpriseServers } = tabEnterpriseServersState;
        const { hasEnterpriseServersReceived } = tabEnterpriseServersState.hasSuccessFor;

        return {
            enterpriseServers,
            hasEnterpriseServersReceived
        };
    },
    {
        dispatchGetEnterpriseServersThunk: GetEnterpriseServersThunk.invoke,
        dispatchDeleteEnterpriseServerThunk: DeleteEnterpriseServerThunk.invoke
    }
)(EnterpriseServersTabClass);

export const EnterpriseServersTabView = withFloatMessages(EnterpriseServersTabConnected);