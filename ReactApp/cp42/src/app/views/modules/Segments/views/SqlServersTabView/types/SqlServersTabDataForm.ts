/**
 * Модель Redux формы для sql сервера
 */
export type SqlServersTabDataForm = {
    /**
     * Адрес подключения sql сервера
     */
    connectionAddress: string;

    /**
     * Описание sql сервера
     */
    description: string;

    /**
     * Название sql сервера
     */
    name: string;

    /**
     * Enum типа восстановления инф. базы
     */
    restoreModelType: string;
};