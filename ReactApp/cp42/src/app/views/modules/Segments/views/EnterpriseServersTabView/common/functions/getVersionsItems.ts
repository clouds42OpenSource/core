import { PlatformEnumType } from 'app/common/enums';
import { ComboboxItemModel } from 'app/views/components/controls/forms/ComboBox/models';

/**
 * Получить список версий платформы
 * @returns Тип объекта списка для сервера 1С для ComboBox
 */
export function getVersionsItems() {
    const versionsTypeText = ['Не определено', 'Платформа 8.2', 'Платформа 8.3'];
    return Object.keys(PlatformEnumType).filter(key => !Number.isNaN(+key)).map<ComboboxItemModel<string>>(key => ({
        text: versionsTypeText[Number(key)],
        value: PlatformEnumType[Number(key)]
    }));
}