/**
 * Модель Redux формы для платформы 1С
 */
export type PlatformVersionTabDataForm = {
    /**
     * Версия платформы
     */
    version: string,

    /**
     * Путь к платформе
     */
    pathToPlatform: string,

    /**
     * Путь к платформе x64
     */
    pathToPlatformX64?: string,

    /**
     * Ссылка на скачивание тонкого клиента для MacOs
     */
    macOsThinClientDownloadLink?: string,

    /**
     * Ссылка на скачивание тонкого клиента для Windows
     */
    windowsThinClientDownloadLink?: string
};