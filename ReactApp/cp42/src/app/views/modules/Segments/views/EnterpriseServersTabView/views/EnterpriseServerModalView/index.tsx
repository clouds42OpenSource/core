import { Box } from '@mui/material';
import { PageEnumType, PlatformEnumType } from 'app/common/enums';
import { checkOnValidField, checkValidOfStringField } from 'app/common/functions';
import { IErrorsType } from 'app/common/interfaces';
import { TabEnterpriseServersProcessId } from 'app/modules/segments/TabEnterpriseServers';
import { CreateEnterpriseServerThunkParams } from 'app/modules/segments/TabEnterpriseServers/store/reducer/createEnterpriseServerReducer/params';
import { EditEnterpriseServerThunkParams } from 'app/modules/segments/TabEnterpriseServers/store/reducer/editEnterpriseServerReducer/params';
import { GetEnterpriseServerThunkParams } from 'app/modules/segments/TabEnterpriseServers/store/reducer/getEnterpriseServerReducer/params';
import { CreateEnterpriseServerThunk, EditEnterpriseServerThunk, GetEnterpriseServerThunk } from 'app/modules/segments/TabEnterpriseServers/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { Dialog } from 'app/views/components/controls/Dialog';
import { ComboBoxForm, TextBoxForm } from 'app/views/components/controls/forms';
import { getDialogButtons } from 'app/views/modules/Segments/common/functions';
import { EnterpriseServersTabDataForm, EnterpriseServersTabDataFormFieldsNamesType, TabEnterpriseServerLengthEnumType, getVersionEnumKeyByName, getVersionsItems } from 'app/views/modules/Segments/views/EnterpriseServersTabView/common';
import { ShowErrorMessage } from 'app/views/modules/_common/components/ShowErrorMessage';
import { WatchReducerProcessActionState } from 'app/views/modules/_common/components/WatchReducerProcessActionState';
import { EnterpriseServerDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabEnterpriseServersApiProxy';
import { IReducerProcessActionInfo } from 'core/redux/interfaces';
import { Component } from 'react';
import { connect } from 'react-redux';

type OwnProps = ReduxFormProps<EnterpriseServersTabDataForm>;

type EnterpriseServerModalProps = {
    /**
     * Поле для открытия модального окошка сервера 1С
     */
    isOpen: boolean;

    /**
     * ID сервера 1С
     */
    enterpriseServerId?: string;

    /**
     * Функция на закрытие модального окошка
     */
    onClickToCloseModal: () => void;
};

type EnterpriseServerModalState = {
    /**
     * Обьект Ошибок для валидации
     */
    errors: IErrorsType;

    /**
     * Тип страницы для динамического компонента редактирования/создания
     */
    pageType: PageEnumType;
};

type StateProps = {
    /**
     * Сервер 1С:Предприятие
     */
    enterpriseServer: EnterpriseServerDataModel;
};

type DispatchProps = {
    dispatchCreateEnterpriseServerThunk: (args: CreateEnterpriseServerThunkParams) => void;
    dispatchGetEnterpriseServerThunk: (args: GetEnterpriseServerThunkParams) => void;
    dispatchEditEnterpriseServerThunk: (args: EditEnterpriseServerThunkParams) => void;
};

type AllProps = EnterpriseServerModalProps & OwnProps & StateProps & DispatchProps & FloatMessageProps;

class EnterpriseServerModalClass extends Component<AllProps, EnterpriseServerModalState> {
    public constructor(props: AllProps) {
        super(props);

        this.state = {
            errors: {},
            pageType: this.props.enterpriseServerId ? PageEnumType.Edit : PageEnumType.Create
        };

        this.getDialogBody = this.getDialogBody.bind(this);
        this.onValueChange = this.onValueChange.bind(this);
        this.submitChanges = this.submitChanges.bind(this);
        this.createEnterpriseServer = this.createEnterpriseServer.bind(this);
        this.editEnterpriseServer = this.editEnterpriseServer.bind(this);
        this.isEnterpriseServerFormValid = this.isEnterpriseServerFormValid.bind(this);

        this.onProcessActionStateChanged = this.onProcessActionStateChanged.bind(this);
        this.onProcessGetEnterpriseServer = this.onProcessGetEnterpriseServer.bind(this);
        this.successGetEnterpriseServerState = this.successGetEnterpriseServerState.bind(this);
        this.errorGetEnterpriseServerState = this.errorGetEnterpriseServerState.bind(this);
        this.checkVersionPlatform = this.checkVersionPlatform.bind(this);
        this.fillEnterpriseServer = this.fillEnterpriseServer.bind(this);

        this.props.reduxForm.setInitializeFormDataAction(this.defaultInitDataReduxForm);
    }

    public componentDidMount() {
        if (this.props.enterpriseServerId) {
            this.props.dispatchGetEnterpriseServerThunk({
                enterpriseServerId: this.props.enterpriseServerId,
                force: true
            });
        }
    }

    private onProcessActionStateChanged(processId: string, actionState: IReducerProcessActionInfo) {
        this.onProcessGetEnterpriseServer(processId, actionState);
    }

    private onProcessGetEnterpriseServer(processId: string, actionState: IReducerProcessActionInfo) {
        if (processId !== TabEnterpriseServersProcessId.GetEnterpriseServer) return;
        this.successGetEnterpriseServerState(actionState);
        this.errorGetEnterpriseServerState(actionState);
    }

    private onValueChange<TValue, TForm extends string = EnterpriseServersTabDataFormFieldsNamesType>(formName: TForm, newValue: TValue) {
        this.props.reduxForm.updateReduxFormFields({
            [formName]: newValue
        });

        const { errors } = this.state;

        if (typeof newValue === 'string') {
            switch (formName) {
                case 'name':
                    errors[formName] = checkValidOfStringField(newValue, 'Заполните название', TabEnterpriseServerLengthEnumType.Name);
                    break;
                case 'description':
                    errors[formName] = checkValidOfStringField(newValue, undefined, TabEnterpriseServerLengthEnumType.Description);
                    break;
                case 'adminName':
                    errors[formName] = checkValidOfStringField(newValue, undefined, TabEnterpriseServerLengthEnumType.AdminName);
                    break;
                case 'adminPassword':
                    errors[formName] = checkValidOfStringField(newValue, undefined, TabEnterpriseServerLengthEnumType.AdminPassword);
                    break;
                case 'clusterSettingsPath':
                    errors[formName] = checkValidOfStringField(newValue, 'Заполните путь к настройкам кластера', TabEnterpriseServerLengthEnumType.ConnectionAddress);
                    break;
                case 'connectionAddress':
                    errors[formName] = checkValidOfStringField(newValue, 'Заполните адрес подключения', TabEnterpriseServerLengthEnumType.ConnectionAddress);
                    break;
                case 'version':
                    errors[formName] = getVersionEnumKeyByName(newValue) === PlatformEnumType.Undefined ? 'Укажите версию платформы' : '';
                    break;
                default:
                    break;
            }

            this.setState({ errors });
        }
    }

    private getDialogBody() {
        const formFields = this.props.reduxForm.getReduxFormFields(false);
        const { errors } = this.state;

        return (
            <Box display="flex" flexDirection="column" gap="10px">
                <div>
                    <TextBoxForm
                        formName="name"
                        label="Название"
                        placeholder="Введите название"
                        value={ formFields.name }
                        onValueChange={ this.onValueChange }
                    />
                    <ShowErrorMessage errors={ errors } formName="name" />
                </div>
                <div>
                    <TextBoxForm
                        formName="connectionAddress"
                        label="Адрес подключения"
                        placeholder="Введите адрес"
                        value={ formFields.connectionAddress }
                        onValueChange={ this.onValueChange }
                    />
                    <ShowErrorMessage errors={ errors } formName="connectionAddress" />
                </div>
                <div>
                    <ComboBoxForm
                        items={ getVersionsItems() }
                        formName="version"
                        label="Версия платформы"
                        value={ formFields.version }
                        onValueChange={ this.onValueChange }
                    />
                    <ShowErrorMessage errors={ errors } formName="version" />
                </div>
                <div>
                    <TextBoxForm
                        formName="adminName"
                        label="Администратор кластера"
                        placeholder="Введите админа кластера"
                        value={ formFields.adminName }
                        onValueChange={ this.onValueChange }
                    />
                    <ShowErrorMessage errors={ errors } formName="adminName" />
                </div>
                <div>
                    <TextBoxForm
                        formName="adminPassword"
                        label="Пароль администратора"
                        placeholder="Введите пароль"
                        value={ formFields.adminPassword }
                        onValueChange={ this.onValueChange }
                    />
                    <ShowErrorMessage errors={ errors } formName="adminPassword" />
                </div>
                <div>
                    <TextBoxForm
                        formName="clusterSettingsPath"
                        label="Путь к настройкам кластера"
                        placeholder="Введите Путь к настройкам кластера"
                        value={ formFields.clusterSettingsPath }
                        onValueChange={ this.onValueChange }
                    />
                    <ShowErrorMessage errors={ errors } formName="clusterSettingsPath" />
                </div>
                <div>
                    <TextBoxForm
                        type="textarea"
                        formName="description"
                        label="Описание"
                        placeholder="Введите описание"
                        value={ formFields.description }
                        onValueChange={ this.onValueChange }
                    />
                    <ShowErrorMessage errors={ errors } formName="description" />
                </div>
            </Box>
        );
    }

    private successGetEnterpriseServerState(actionState: IReducerProcessActionInfo) {
        if (!actionState.isInSuccessState) return;
        this.props.reduxForm.updateReduxFormFields({
            ...this.props.enterpriseServer
        });
    }

    private errorGetEnterpriseServerState(actionState: IReducerProcessActionInfo, error?: Error) {
        if (!actionState.isInErrorState) return;
        this.props.floatMessage.show(MessageType.Error, `Ошибка при получении сервера 1С:Предприятия: ${ error?.message }`);
    }

    private submitChanges() {
        if (!this.isEnterpriseServerFormValid()) {
            return;
        }

        this.createEnterpriseServer();
        this.editEnterpriseServer();
    }

    private createEnterpriseServer() {
        if (this.state.pageType !== PageEnumType.Create) return;
        this.props.dispatchCreateEnterpriseServerThunk({
            ...this.fillEnterpriseServer(),
            force: true
        });
    }

    private editEnterpriseServer() {
        if (this.state.pageType !== PageEnumType.Edit) return;
        this.props.dispatchEditEnterpriseServerThunk({
            ...this.fillEnterpriseServer(),
            id: this.props.enterpriseServerId!,
            force: true
        });
    }

    private fillEnterpriseServer(): EnterpriseServerDataModel {
        const formFields = this.props.reduxForm.getReduxFormFields(false);
        return {
            adminName: formFields.adminName,
            adminPassword: formFields.adminPassword,
            connectionAddress: formFields.connectionAddress,
            description: formFields.description,
            clusterSettingsPath: formFields.clusterSettingsPath,
            id: '',
            name: formFields.name,
            version: formFields.version,
            versionEnum: getVersionEnumKeyByName(formFields.version)
        };
    }

    private defaultInitDataReduxForm(): EnterpriseServersTabDataForm | undefined {
        return {
            adminName: '',
            adminPassword: '',
            connectionAddress: '',
            description: '',
            clusterSettingsPath: '',
            name: '',
            version: PlatformEnumType[PlatformEnumType.Undefined]
        };
    }

    private isEnterpriseServerFormValid() {
        const formFields = this.props.reduxForm.getReduxFormFields(false);
        const errors: IErrorsType = {};
        const arrayResults: boolean[] = [];

        arrayResults.push(checkOnValidField(errors, 'name', formFields.name, 'Заполните название', TabEnterpriseServerLengthEnumType.Name));
        arrayResults.push(checkOnValidField(errors, 'description', formFields.description, undefined, TabEnterpriseServerLengthEnumType.Description));
        arrayResults.push(checkOnValidField(errors, 'adminName', formFields.adminName, undefined, TabEnterpriseServerLengthEnumType.AdminName));
        arrayResults.push(checkOnValidField(errors, 'adminPassword', formFields.adminPassword, undefined, TabEnterpriseServerLengthEnumType.AdminPassword));
        arrayResults.push(checkOnValidField(errors, 'clusterSettingsPath', formFields.clusterSettingsPath, undefined, TabEnterpriseServerLengthEnumType.ConnectionAddress));
        arrayResults.push(checkOnValidField(errors, 'connectionAddress', formFields.connectionAddress, 'Заполните адрес', TabEnterpriseServerLengthEnumType.ConnectionAddress));
        arrayResults.push(this.checkVersionPlatform(errors, formFields.version));

        if (arrayResults.some(item => !item)) {
            this.props.floatMessage.show(MessageType.Warning, 'Форма к отправке не готова. Пожалуйста, заполните все поля.');
            this.setState({ errors });
            return false;
        }

        return true;
    }

    private checkVersionPlatform(errors: IErrorsType, version: string) {
        if (getVersionEnumKeyByName(version) !== PlatformEnumType.Undefined) {
            return true;
        }

        errors.version = 'Укажите версию платформы';

        return false;
    }

    public render() {
        const { isOpen } = this.props;
        const isEditTypePage = this.state.pageType === PageEnumType.Edit;
        const titleText = isEditTypePage ? 'Редактирование' : 'Создание';

        return (
            <>
                <Dialog
                    disableBackdropClick={ true }
                    title={ `${ titleText } сервера 1С:Предприятие` }
                    isOpen={ isOpen }
                    dialogWidth="sm"
                    isTitleSmall={ true }
                    titleTextAlign="left"
                    titleFontSize={ 14 }
                    dialogVerticalAlign="center"
                    onCancelClick={ this.props.onClickToCloseModal }
                    buttons={ getDialogButtons({
                        pageType: this.state.pageType,
                        onClickToCloseModal: this.props.onClickToCloseModal,
                        submitChanges: this.submitChanges
                    }) }
                >
                    { this.getDialogBody() }
                </Dialog>
                <WatchReducerProcessActionState
                    watch={ [{
                        reducerStateName: 'TabEnterpriseServersState',
                        processIds: [
                            TabEnterpriseServersProcessId.GetEnterpriseServer
                        ]
                    }] }
                    onProcessActionStateChanged={ this.onProcessActionStateChanged }
                />
            </>
        );
    }
}

const EnterpriseServerModalConnected = connect<StateProps, DispatchProps, OwnProps, AppReduxStoreState>(
    state => ({
        enterpriseServer: state.TabEnterpriseServersState.enterpriseServer
    }),
    {
        dispatchCreateEnterpriseServerThunk: CreateEnterpriseServerThunk.invoke,
        dispatchGetEnterpriseServerThunk: GetEnterpriseServerThunk.invoke,
        dispatchEditEnterpriseServerThunk: EditEnterpriseServerThunk.invoke
    }
)(EnterpriseServerModalClass);

export const EnterpriseServerModalView = withReduxForm(withFloatMessages(EnterpriseServerModalConnected),
    {
        reduxFormName: 'Enterprise_Server_Modal_Form',
        resetOnUnmount: true
    });