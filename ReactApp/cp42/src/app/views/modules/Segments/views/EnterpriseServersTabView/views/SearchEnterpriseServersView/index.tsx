import { Box } from '@mui/material';
import { OutlinedButton } from 'app/views/components/controls/Button';
import { ComboBoxForm, TextBoxForm } from 'app/views/components/controls/forms';
import { getVersionsItems } from 'app/views/modules/Segments/views/EnterpriseServersTabView/common/functions/getVersionsItems';
import cn from 'classnames';
import { Component } from 'react';
import commonCss from '../../../../common/styles.module.css';

type SearchEnterpriseServersState = {
    connectionAddress: string,
    name: string,
    version: string,
    description: string
};

type SearchEnterpriseServersProps = {
    onClickToSearch: () => void;
};

export class SearchEnterpriseServersView extends Component<SearchEnterpriseServersProps, SearchEnterpriseServersState> {
    public constructor(props: SearchEnterpriseServersProps) {
        super(props);

        this.state = {
            connectionAddress: '',
            description: '',
            name: '',
            version: ''
        };

        this.onValueChange = this.onValueChange.bind(this);
    }

    private onValueChange(formName: string, newValue: string) {
        this.setState(prevState => ({
            ...prevState,
            [formName]: newValue
        }));
    }

    public render() {
        return (
            <Box marginBottom="16px">
                <Box display="grid" gridTemplateColumns="repeat(auto-fit, minmax(250px, 1fr))" gap="12px">
                    <TextBoxForm
                        formName="name"
                        label="Название"
                        placeholder="Введите название"
                        value={ this.state.name }
                        onValueChange={ this.onValueChange }
                    />
                    <TextBoxForm
                        formName="description"
                        label="Описание"
                        placeholder="Введите описание"
                        value={ this.state.description }
                        onValueChange={ this.onValueChange }
                    />
                    <TextBoxForm
                        formName="connectionAddress"
                        label="Адрес подключения"
                        placeholder="Введите адрес подключения"
                        value={ this.state.connectionAddress }
                        onValueChange={ this.onValueChange }
                    />
                    <ComboBoxForm
                        items={ [{ text: 'Все', value: '' }].concat(getVersionsItems()) }
                        formName="version"
                        label="Версия платформы"
                        value={ this.state.version }
                        onValueChange={ this.onValueChange }
                    />
                    <OutlinedButton
                        kind="success"
                        style={ { width: '100%' } }
                        className={ cn(commonCss['search-button']) }
                        onClick={ this.props.onClickToSearch }
                    >
                        <i className="fa fa-search mr-1" />
                        Поиск
                    </OutlinedButton>
                </Box>
            </Box>
        );
    }
}