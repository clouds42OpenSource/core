import { Box } from '@mui/material';
import { PageEnumType } from 'app/common/enums';
import { checkOnValidField, checkValidOfStringField } from 'app/common/functions';
import { IErrorsType } from 'app/common/interfaces';
import { TabPlatformVersionProcessId } from 'app/modules/segments/TabPlatformVersion';
import { CreatePlatformVersionThunkParams } from 'app/modules/segments/TabPlatformVersion/store/reducer/createPlatformVersionReducer/params';
import { EditPlatformVersionThunkParams } from 'app/modules/segments/TabPlatformVersion/store/reducer/editPlatformVersionReducer/params';
import { GetPlatformVersionThunkParams } from 'app/modules/segments/TabPlatformVersion/store/reducer/getPlatformVersionReducer/params';
import { CreatePlatformVersionThunk, EditPlatformVersionThunk, GetPlatformVersionThunk } from 'app/modules/segments/TabPlatformVersion/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { Dialog } from 'app/views/components/controls/Dialog';
import { TextBoxForm } from 'app/views/components/controls/forms';
import { getDialogButtons } from 'app/views/modules/Segments/common/functions';
import { PlatformVersionTabDataForm, PlatformVersionTabDataFormFieldsNamesType, PlatformVersionTabLenghEnumType } from 'app/views/modules/Segments/views/PlatformVersionTabView/types';
import { ShowErrorMessage } from 'app/views/modules/_common/components/ShowErrorMessage';
import { WatchReducerProcessActionState } from 'app/views/modules/_common/components/WatchReducerProcessActionState';
import { PlatformVersion1CDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabPlatformVersionApiProxy';
import { IReducerProcessActionInfo } from 'core/redux/interfaces';
import { Component } from 'react';
import { connect } from 'react-redux';

type OwnProps = ReduxFormProps<PlatformVersionTabDataForm>;

type PlatformVersionModalProps = {
    /**
     * Поле для открытия модального окошка
     */
    isOpen: boolean;

    /**
     * Версия платформы
     */
    version?: string;

    /**
     * Функция на закрытие модального окошка
     */
    onClickToCloseModal: () => void;
};

type PlatformVersionModalState = {
    /**
     * Обьект Ошибок для валидации
     */
    errors: IErrorsType;

    /**
     * Тип страницы для динамического компонента редактирования/создания
     */
    pageType: PageEnumType;
};

type StateProps = {
    /**
     * Модель версии платформы 1С
     */
    platformVersion: PlatformVersion1CDataModel;
};

type DispatchProps = {
    dispatchGetPlatformVersionThunk: (args: GetPlatformVersionThunkParams) => void;
    dispatchCreatePlatformVersionThunk: (args: CreatePlatformVersionThunkParams) => void;
    dispatchEditPlatformVersionThunk: (args: EditPlatformVersionThunkParams) => void;
};

type AllProps = PlatformVersionModalProps & OwnProps & StateProps & DispatchProps & FloatMessageProps;

class PlatformVersionModalClass extends Component<AllProps, PlatformVersionModalState> {
    public constructor(props: AllProps) {
        super(props);

        this.state = {
            errors: {},
            pageType: this.props.version ? PageEnumType.Edit : PageEnumType.Create
        };

        this.getDialogBody = this.getDialogBody.bind(this);
        this.onValueChange = this.onValueChange.bind(this);
        this.submitChanges = this.submitChanges.bind(this);
        this.createPlatformVersion = this.createPlatformVersion.bind(this);
        this.editPlatformVersion = this.editPlatformVersion.bind(this);
        this.isPlatformVersionFormValid = this.isPlatformVersionFormValid.bind(this);

        this.onProcessActionStateChanged = this.onProcessActionStateChanged.bind(this);
        this.onProcessGetPlatformVersion = this.onProcessGetPlatformVersion.bind(this);
        this.successGetPlatformVersionState = this.successGetPlatformVersionState.bind(this);
        this.errorGetPlatformVersionState = this.errorGetPlatformVersionState.bind(this);
        this.fillPlatformVersion = this.fillPlatformVersion.bind(this);

        this.props.reduxForm.setInitializeFormDataAction(this.defaultInitDataReduxForm);
    }

    public componentDidMount() {
        if (this.props.version) {
            this.props.dispatchGetPlatformVersionThunk({
                platformVersion: this.props.version,
                force: true
            });
        }
    }

    private onProcessActionStateChanged(processId: string, actionState: IReducerProcessActionInfo) {
        this.onProcessGetPlatformVersion(processId, actionState);
    }

    private onProcessGetPlatformVersion(processId: string, actionState: IReducerProcessActionInfo) {
        if (processId !== TabPlatformVersionProcessId.GetPlatformVersion) return;
        this.successGetPlatformVersionState(actionState);
        this.errorGetPlatformVersionState(actionState);
    }

    private onValueChange<TValue, TForm extends string = PlatformVersionTabDataFormFieldsNamesType>(formName: TForm, newValue: TValue) {
        this.props.reduxForm.updateReduxFormFields({
            [formName]: newValue
        });

        const { errors } = this.state;

        if (typeof newValue === 'string') {
            switch (formName) {
                case 'version':
                    errors[formName] = checkValidOfStringField(newValue, 'Заполните версию');
                    break;
                case 'pathToPlatform':
                    errors[formName] = checkValidOfStringField(newValue, 'Заполните путь', PlatformVersionTabLenghEnumType.PathToPlatform);
                    break;
            }

            this.setState({ errors });
        }
    }

    private getDialogBody() {
        const formFields = this.props.reduxForm.getReduxFormFields(false);
        const { errors } = this.state;

        return (
            <Box display="flex" flexDirection="column" gap="12px">
                <div>
                    <TextBoxForm
                        formName="version"
                        label="Версия 1С"
                        placeholder="Введите версию"
                        value={ formFields.version }
                        onValueChange={ this.onValueChange }
                        isReadOnly={ this.state.pageType === PageEnumType.Edit }
                    />
                    <ShowErrorMessage errors={ errors } formName="version" />
                </div>
                <div>
                    <TextBoxForm
                        formName="pathToPlatform"
                        label="Путь к платформе"
                        placeholder="Введите путь"
                        value={ formFields.pathToPlatform }
                        onValueChange={ this.onValueChange }
                    />
                    <ShowErrorMessage errors={ errors } formName="pathToPlatform" />
                </div>
                <div>
                    <TextBoxForm
                        formName="pathToPlatformX64"
                        label="Путь к платформе x64"
                        placeholder="Введите путь"
                        value={ formFields.pathToPlatformX64 }
                        onValueChange={ this.onValueChange }
                    />
                </div>
                <div>
                    <TextBoxForm
                        formName="windowsThinClientDownloadLink"
                        label="Ссылка на тонкий клиент для Windows"
                        placeholder="Введите ссылку"
                        value={ formFields.windowsThinClientDownloadLink }
                        onValueChange={ this.onValueChange }
                    />
                </div>
                <div>
                    <TextBoxForm
                        formName="macOsThinClientDownloadLink"
                        label="Ссылка на тонкий клиент для MacOS"
                        placeholder="Введите ссылку"
                        value={ formFields.macOsThinClientDownloadLink }
                        onValueChange={ this.onValueChange }
                    />
                </div>
            </Box>
        );
    }

    private successGetPlatformVersionState(actionState: IReducerProcessActionInfo) {
        if (!actionState.isInSuccessState) return;
        this.props.reduxForm.updateReduxFormFields({
            ...this.props.platformVersion
        });
    }

    private errorGetPlatformVersionState(actionState: IReducerProcessActionInfo, error?: Error) {
        if (!actionState.isInErrorState) return;
        this.props.floatMessage.show(MessageType.Error, `Ошибка при получении версии платформы 1С: ${ error?.message }`);
    }

    private submitChanges() {
        if (!this.isPlatformVersionFormValid()) return;
        this.createPlatformVersion();
        this.editPlatformVersion();
    }

    private createPlatformVersion() {
        if (this.state.pageType !== PageEnumType.Create) return;
        this.props.dispatchCreatePlatformVersionThunk({
            ...this.fillPlatformVersion(),
            force: true
        });
    }

    private editPlatformVersion() {
        if (this.state.pageType !== PageEnumType.Edit) return;
        this.props.dispatchEditPlatformVersionThunk({
            ...this.fillPlatformVersion(),
            version: this.props.version!,
            force: true
        });
    }

    private fillPlatformVersion(): PlatformVersion1CDataModel {
        const formFields = this.props.reduxForm.getReduxFormFields(false);
        return {
            macOsThinClientDownloadLink: formFields.macOsThinClientDownloadLink,
            pathToPlatform: formFields.pathToPlatform,
            pathToPlatformX64: formFields.pathToPlatformX64,
            version: formFields.version,
            windowsThinClientDownloadLink: formFields.windowsThinClientDownloadLink
        };
    }

    private defaultInitDataReduxForm(): PlatformVersionTabDataForm | undefined {
        return {
            pathToPlatform: '',
            version: '',
            macOsThinClientDownloadLink: '',
            pathToPlatformX64: '',
            windowsThinClientDownloadLink: ''
        };
    }

    private isPlatformVersionFormValid() {
        const formFields = this.props.reduxForm.getReduxFormFields(false);
        const errors: IErrorsType = {};
        const arrayResults: boolean[] = [];

        arrayResults.push(checkOnValidField(errors, 'version', formFields.version, 'Заполните версию'));
        arrayResults.push(checkOnValidField(errors, 'pathToPlatform', formFields.pathToPlatform, 'Заполните путь', PlatformVersionTabLenghEnumType.PathToPlatform));

        if (arrayResults.some(item => !item)) {
            this.props.floatMessage.show(MessageType.Warning, 'Форма к отправке не готова. Пожалуйста, заполните все поля.');
            this.setState({ errors });
            return false;
        }

        return true;
    }

    public render() {
        const { isOpen } = this.props;
        const isEditTypePage = this.state.pageType === PageEnumType.Edit;
        const titleText = isEditTypePage ? 'Редактирование' : 'Создание';

        return (
            <>
                <Dialog
                    disableBackdropClick={ true }
                    title={ `${ titleText } версии платформы` }
                    isOpen={ isOpen }
                    dialogWidth="sm"
                    isTitleSmall={ true }
                    titleTextAlign="left"
                    titleFontSize={ 14 }
                    dialogVerticalAlign="center"
                    onCancelClick={ this.props.onClickToCloseModal }
                    buttons={ getDialogButtons({
                        pageType: this.state.pageType,
                        onClickToCloseModal: this.props.onClickToCloseModal,
                        submitChanges: this.submitChanges
                    }) }
                >
                    {
                        this.getDialogBody()
                    }
                </Dialog>
                <WatchReducerProcessActionState
                    watch={ [{
                        reducerStateName: 'TabPlatformVersionState',
                        processIds: [
                            TabPlatformVersionProcessId.GetPlatformVersion
                        ]
                    }] }
                    onProcessActionStateChanged={ this.onProcessActionStateChanged }
                />
            </>
        );
    }
}

const PlatformVersionModalConnected = connect<StateProps, DispatchProps, OwnProps, AppReduxStoreState>(
    state => {
        const tabPlatformVersionState = state.TabPlatformVersionState;
        const { platformVersion } = tabPlatformVersionState;

        return {
            platformVersion
        };
    },
    {
        dispatchGetPlatformVersionThunk: GetPlatformVersionThunk.invoke,
        dispatchCreatePlatformVersionThunk: CreatePlatformVersionThunk.invoke,
        dispatchEditPlatformVersionThunk: EditPlatformVersionThunk.invoke
    }
)(PlatformVersionModalClass);

export const PlatformVersionModalView = withReduxForm(withFloatMessages(PlatformVersionModalConnected),
    {
        reduxFormName: 'Platform_Version_Modal_Form',
        resetOnUnmount: true
    });