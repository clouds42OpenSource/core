import ErrorOutlineIcon from '@mui/icons-material/ErrorOutline';
import { Box } from '@mui/material';
import { SortingKind } from 'app/common/enums';
import { TabPublishNodesProcessId } from 'app/modules/segments/TabPublishNodes';
import { DeletePublishNodeThunkParams } from 'app/modules/segments/TabPublishNodes/store/reducer/deletePublishNodeReducer/params';
import { GetPublishNodesThunkParams } from 'app/modules/segments/TabPublishNodes/store/reducer/getPublishNodesReducer/params';
import { DeletePublishNodeThunk, GetPublishNodesThunk } from 'app/modules/segments/TabPublishNodes/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { SuccessButton, TextLinkButton } from 'app/views/components/controls/Button';
import { Dialog } from 'app/views/components/controls/Dialog';
import { TextOut } from 'app/views/components/TextOut';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { WatchReducerProcessActionState } from 'app/views/modules/_common/components/WatchReducerProcessActionState';
import { CommonSearchSegmentView } from 'app/views/modules/Segments/common/components';
import { CommonSegmentEnumType } from 'app/views/modules/Segments/types';
import { PublishNodeModalView } from 'app/views/modules/Segments/views/PublishNodesTabView/views/PublishNodeModalView';
import { SelectDataCommonDataModel, SelectDataResultMetadataModel, SortingDataModel } from 'app/web/common/data-models';
import { PublishNodeDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import { IReducerProcessActionInfo } from 'core/redux/interfaces';
import { Component, createRef } from 'react';
import { connect } from 'react-redux';
import commonCss from '../../common/styles.module.css';

type PublishNodesTabState = {
    isFirstTimeLoaded: boolean;
    isOpen: boolean;
    publishNodeId?: string;
    deleteIsOpen: boolean;
    orderBy?: string;
};

type PublishNodesTabProps = {
    isCanLoad: boolean;
};

type StateProps = {
    publishNodes: SelectDataResultMetadataModel<PublishNodeDataModel>;
    hasPublishNodesReceived: boolean;
};

type DispatchProps = {
    dispatchGetPublishNodesThunk: (args: GetPublishNodesThunkParams) => void;
    dispatchDeletePublishNodeThunk: (args: DeletePublishNodeThunkParams) => void;
};

type AllProps = PublishNodesTabProps & StateProps & DispatchProps & FloatMessageProps;

class PublishNodesTabClass extends Component<AllProps, PublishNodesTabState> {
    private _commonTableRef = createRef<CommonTableWithFilter<PublishNodeDataModel, undefined>>();

    private _searchPublishNodesRef = createRef<CommonSearchSegmentView>();

    public constructor(props: AllProps) {
        super(props);

        this.state = {
            isOpen: false,
            isFirstTimeLoaded: false,
            deleteIsOpen: false,
            orderBy: ''
        };

        this.onClickToOpenModal = this.onClickToOpenModal.bind(this);
        this.onClickToCloseModal = this.onClickToCloseModal.bind(this);
        this.onClickToSearch = this.onClickToSearch.bind(this);
        this.onDataSelect = this.onDataSelect.bind(this);
        this.getFormatNamePublishNode = this.getFormatNamePublishNode.bind(this);
        this.notificationInSuccess = this.notificationInSuccess.bind(this);
        this.notificationInError = this.notificationInError.bind(this);
        this.openModalForDeletePublishNode = this.openModalForDeletePublishNode.bind(this);
        this.getButtonsForActions = this.getButtonsForActions.bind(this);

        this.onProcessActionStateChanged = this.onProcessActionStateChanged.bind(this);
        this.onProcessCreatePublishNode = this.onProcessCreatePublishNode.bind(this);
        this.onProcessEditPublishNode = this.onProcessEditPublishNode.bind(this);
        this.onProcessDeletePublishNode = this.onProcessDeletePublishNode.bind(this);
        this.onClickDeletePublishNode = this.onClickDeletePublishNode.bind(this);
        this.closeModalForDeletePublishNode = this.closeModalForDeletePublishNode.bind(this);
    }

    public componentDidUpdate() {
        const { isCanLoad, hasPublishNodesReceived } = this.props;
        const { isFirstTimeLoaded } = this.state;

        if (isCanLoad && !isFirstTimeLoaded && !hasPublishNodesReceived && this._commonTableRef.current) {
            this._commonTableRef.current.ReSelectData();
            this.setState({
                isFirstTimeLoaded: true
            });
        }
    }

    private onClickDeletePublishNode() {
        if (this.state.publishNodeId) {
            this.props.dispatchDeletePublishNodeThunk({
                publishNodeId: this.state.publishNodeId,
                force: true
            });
            this.setState({
                deleteIsOpen: false
            });
        }
    }

    private onProcessActionStateChanged(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        this.onProcessCreatePublishNode(processId, actionState, error);
        this.onProcessEditPublishNode(processId, actionState, error);
        this.onProcessDeletePublishNode(processId, actionState, error);
    }

    private onProcessCreatePublishNode(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId !== TabPublishNodesProcessId.CreatePublishNode) return;
        this.notificationInSuccess(actionState, 'Нода публикации успешно создана');
        this.notificationInError(actionState, error?.message);
    }

    private onProcessEditPublishNode(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId !== TabPublishNodesProcessId.EditPublishNode) return;
        this.notificationInSuccess(actionState, 'Нода публикации успешно сохранена');
        this.notificationInError(actionState, error?.message);
    }

    private onProcessDeletePublishNode(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId !== TabPublishNodesProcessId.DeletePublishNode) return;
        this.notificationInSuccess(actionState, 'Нода публикации успешно удалена');
        this.notificationInError(actionState, error?.message);
    }

    private onClickToOpenModal(publishNodeId?: string) {
        this.setState({
            isOpen: true,
            publishNodeId
        });
    }

    private onClickToCloseModal() {
        this.setState({
            isOpen: false
        });
    }

    private onClickToSearch() {
        const {
            description,
            connectionAddress
        } = this._searchPublishNodesRef.current?.state!;

        this.props.dispatchGetPublishNodesThunk({
            filter: {
                address: connectionAddress,
                description,
            },
            pageNumber: 1,
            orderBy: this.state.orderBy,
            pageSize: CommonSegmentEnumType.DefaultPageSize,
            force: true
        });
    }

    private onDataSelect(args: SelectDataCommonDataModel<undefined>) {
        const {
            description,
            connectionAddress
        } = this._searchPublishNodesRef.current?.state!;

        const orderBy = args.sortingData ? this.getSortQuery(args.sortingData) : 'address.asc';

        this.setState({
            orderBy
        });

        this.props.dispatchGetPublishNodesThunk({
            filter: {
                description,
                address: connectionAddress,
            },
            pageNumber: args.pageNumber,
            orderBy,
            pageSize: CommonSegmentEnumType.DefaultPageSize,
            force: true
        });
    }

    private getButtonsForActions(Id: string) {
        return (
            <TextLinkButton
                className={ commonCss['delete-link'] }
                onClick={ () => { this.openModalForDeletePublishNode(Id); } }
            >
                <i className="fa fa-trash" />
            </TextLinkButton>
        );
    }

    private getFormatNamePublishNode(name: string, data: PublishNodeDataModel) {
        return (
            <TextLinkButton onClick={ () => { this.onClickToOpenModal(data.id); } }>
                <TextOut fontSize={ 13 } className="text-link">
                    { name }
                </TextOut>
            </TextLinkButton>
        );
    }

    private getSortQuery(args: SortingDataModel) {
        return `${ args.fieldName }.${ args.sortKind ? 'desc' : 'asc' }`;
    }

    private notificationInError(actionState: IReducerProcessActionInfo, message?: string) {
        if (!actionState.isInErrorState) return;
        this.props.floatMessage.show(MessageType.Error, message);
    }

    private notificationInSuccess(actionState: IReducerProcessActionInfo, message: string) {
        if (!actionState.isInSuccessState) return;
        this.props.floatMessage.show(MessageType.Success, message);
        this._commonTableRef.current?.ReSelectData();
        this.onClickToCloseModal();
    }

    private closeModalForDeletePublishNode() {
        this.setState({
            deleteIsOpen: false
        });
    }

    private openModalForDeletePublishNode(publishNodeId: string) {
        this.setState({
            publishNodeId,
            deleteIsOpen: true
        });
    }

    public render() {
        const { publishNodes: { metadata, records } } = this.props;

        return (
            <>
                <CommonSearchSegmentView
                    ref={ this._searchPublishNodesRef }
                    onClickToSearch={ this.onClickToSearch }
                    isNameVisible={ false }
                />
                <CommonTableWithFilter
                    className={ commonCss['custom-table'] }
                    ref={ this._commonTableRef }
                    isResponsiveTable={ true }
                    isBorderStyled={ true }
                    uniqueContextProviderStateId="TabPublishNodesContextProviderStateId"
                    tableProps={ {
                        dataset: records,
                        keyFieldName: 'id',
                        sorting: {
                            sortKind: SortingKind.Asc,
                            sortFieldName: 'address'
                        },
                        fieldsView: {
                            description: {
                                caption: 'Описание',
                                fieldWidth: 'auto',
                                fieldContentNoWrap: false,
                                isSortable: true
                            },
                            address: {
                                caption: 'Адрес подключения',
                                fieldContentNoWrap: false,
                                fieldWidth: 'auto',
                                isSortable: true,
                                format: this.getFormatNamePublishNode
                            },
                            id: {
                                caption: '',
                                fieldWidth: 'auto',
                                fieldContentNoWrap: false,
                                format: this.getButtonsForActions
                            }
                        },
                        pagination: {
                            currentPage: metadata.pageNumber,
                            pageSize: metadata.pageSize,
                            recordsCount: metadata.totalItemCount,
                            totalPages: metadata.pageCount
                        },
                    } }
                    onDataSelect={ this.onDataSelect }
                />
                <SuccessButton
                    onClick={ () => { this.onClickToOpenModal(); } }
                >
                    <i className="fa fa-plus-circle mr-1" />
                    Создать ноду публикации
                </SuccessButton>
                {
                    this.state.isOpen
                        ? (
                            <PublishNodeModalView
                                isOpen={ this.state.isOpen }
                                publishNodeId={ this.state.publishNodeId }
                                onClickToCloseModal={ this.onClickToCloseModal }
                            />
                        )
                        : null
                }
                <WatchReducerProcessActionState
                    watch={ [{
                        reducerStateName: 'TabPublishNodesState',
                        processIds: [
                            TabPublishNodesProcessId.CreatePublishNode,
                            TabPublishNodesProcessId.EditPublishNode,
                            TabPublishNodesProcessId.DeletePublishNode,
                        ]
                    }] }
                    onProcessActionStateChanged={ this.onProcessActionStateChanged }
                />
                <Dialog
                    isOpen={ this.state.deleteIsOpen }
                    dialogWidth="sm"
                    dialogVerticalAlign="center"
                    onCancelClick={ this.closeModalForDeletePublishNode }
                    buttons={
                        [
                            {
                                content: 'Нет',
                                kind: 'primary',
                                variant: 'text',
                                onClick: this.closeModalForDeletePublishNode
                            },
                            {
                                content: 'Да',
                                kind: 'error',
                                variant: 'contained',
                                onClick: this.onClickDeletePublishNode
                            }
                        ]
                    }
                >
                    <Box display="flex" flexDirection="column" alignItems="center" justifyContent="center">
                        <ErrorOutlineIcon color="warning" sx={ { width: '5em', height: '5em' } } />
                        Вы действительно хотите удалить ноду публикации?
                    </Box>
                </Dialog>
            </>
        );
    }
}

const PublishNodesTabConnected = connect<StateProps, DispatchProps, NonNullable<unknown>, AppReduxStoreState>(
    state => {
        const tabPublishNodesState = state.TabPublishNodesState;
        const { publishNodes } = tabPublishNodesState;
        const { hasPublishNodesReceived } = tabPublishNodesState.hasSuccessFor;

        return {
            publishNodes,
            hasPublishNodesReceived
        };
    },
    {
        dispatchGetPublishNodesThunk: GetPublishNodesThunk.invoke,
        dispatchDeletePublishNodeThunk: DeletePublishNodeThunk.invoke
    }
)(PublishNodesTabClass);

export const PublishNodesTabView = withFloatMessages(PublishNodesTabConnected);