import { Box } from '@mui/material';
import { PageEnumType } from 'app/common/enums';
import { checkOnValidField, checkValidOfStringField } from 'app/common/functions';
import { IErrorsType } from 'app/common/interfaces';
import { TabPublishNodesProcessId } from 'app/modules/segments/TabPublishNodes';
import { CreatePublishNodeThunkParams } from 'app/modules/segments/TabPublishNodes/store/reducer/createPublishNodeReducer/params';
import { EditPublishNodeThunkParams } from 'app/modules/segments/TabPublishNodes/store/reducer/editPublishNodeReducer/params';
import { GetPublishNodeThunkParams } from 'app/modules/segments/TabPublishNodes/store/reducer/getPublishNodeReducer/params';
import { CreatePublishNodeThunk, EditPublishNodeThunk, GetPublishNodeThunk } from 'app/modules/segments/TabPublishNodes/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { Dialog } from 'app/views/components/controls/Dialog';
import { TextBoxForm } from 'app/views/components/controls/forms';
import { getDialogButtons } from 'app/views/modules/Segments/common/functions';
import { PublishNodesTabDataForm } from 'app/views/modules/Segments/views/PublishNodesTabView/types';
import { PublishNodesTabDataFormFieldsNamesType } from 'app/views/modules/Segments/views/PublishNodesTabView/types/PublishNodesTabDataFormFieldsNamesType';
import { TabPublishNodeLengthEnumType } from 'app/views/modules/Segments/views/PublishNodesTabView/types/TabPublishNodeLengthEnumType';
import { ShowErrorMessage } from 'app/views/modules/_common/components/ShowErrorMessage';
import { WatchReducerProcessActionState } from 'app/views/modules/_common/components/WatchReducerProcessActionState';
import { PublishNodeDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import { IReducerProcessActionInfo } from 'core/redux/interfaces';
import { Component } from 'react';
import { connect } from 'react-redux';

type OwnProps = ReduxFormProps<PublishNodesTabDataForm>;

type PublishNodeModalProps = {
    isOpen: boolean;
    publishNodeId?: string;
    onClickToCloseModal: () => void;
};

type PublishNodeModalState = {
    errors: IErrorsType;
    pageType: PageEnumType;
};

type StateProps = {
    publishNode: PublishNodeDataModel;
};

type DispatchProps = {
    dispatchGetPublishNodeThunk: (args: GetPublishNodeThunkParams) => void;
    dispatchCreatePublishNodeThunk: (args: CreatePublishNodeThunkParams) => void;
    dispatchEditPublishNodeThunk: (args: EditPublishNodeThunkParams) => void;
};

type AllProps = PublishNodeModalProps & OwnProps & StateProps & DispatchProps & FloatMessageProps;

class PublishNodeModalClass extends Component<AllProps, PublishNodeModalState> {
    public constructor(props: AllProps) {
        super(props);

        this.state = {
            errors: {},
            pageType: this.props.publishNodeId ? PageEnumType.Edit : PageEnumType.Create
        };

        this.getDialogBody = this.getDialogBody.bind(this);
        this.onValueChange = this.onValueChange.bind(this);
        this.submitChanges = this.submitChanges.bind(this);
        this.createPublishNode = this.createPublishNode.bind(this);
        this.editPublishNode = this.editPublishNode.bind(this);
        this.isPublishNodeFormValid = this.isPublishNodeFormValid.bind(this);

        this.onProcessActionStateChanged = this.onProcessActionStateChanged.bind(this);
        this.onProcessGetPublishNode = this.onProcessGetPublishNode.bind(this);
        this.successGetPublishNodeState = this.successGetPublishNodeState.bind(this);
        this.errorGetPublishNodeState = this.errorGetPublishNodeState.bind(this);
        this.fillPublishNode = this.fillPublishNode.bind(this);

        this.props.reduxForm.setInitializeFormDataAction(this.defaultInitDataReduxForm);
    }

    public componentDidMount() {
        if (this.props.publishNodeId) {
            this.props.dispatchGetPublishNodeThunk({
                publishNodeId: this.props.publishNodeId,
                force: true
            });
        }
    }

    private onProcessActionStateChanged(processId: string, actionState: IReducerProcessActionInfo) {
        this.onProcessGetPublishNode(processId, actionState);
    }

    private onProcessGetPublishNode(processId: string, actionState: IReducerProcessActionInfo) {
        if (processId !== TabPublishNodesProcessId.GetPublishNode) return;
        this.successGetPublishNodeState(actionState);
        this.errorGetPublishNodeState(actionState);
    }

    private onValueChange<TValue, TForm extends string = PublishNodesTabDataFormFieldsNamesType>(formName: TForm, newValue: TValue) {
        this.props.reduxForm.updateReduxFormFields({
            [formName]: newValue
        });

        const { errors } = this.state;

        if (typeof newValue === 'string') {
            switch (formName) {
                case 'address':
                    errors[formName] = checkValidOfStringField(newValue, 'Заполните адрес', TabPublishNodeLengthEnumType.Address);
                    break;
                case 'description':
                    errors[formName] = checkValidOfStringField(newValue, 'Заполните описание');
                    break;
                default:
                    break;
            }

            this.setState({ errors });
        }
    }

    private getDialogBody() {
        const formFields = this.props.reduxForm.getReduxFormFields(false);
        const { errors } = this.state;

        return (
            <Box display="flex" flexDirection="column" gap="12px">
                <div>
                    <TextBoxForm
                        formName="address"
                        label="Адрес подключения"
                        placeholder="Введите адрес"
                        value={ formFields.address }
                        onValueChange={ this.onValueChange }
                    />
                    <ShowErrorMessage errors={ errors } formName="address" />
                </div>
                <div>
                    <TextBoxForm
                        type="textarea"
                        formName="description"
                        label="Описание"
                        placeholder="Введите описание"
                        value={ formFields.description }
                        onValueChange={ this.onValueChange }
                    />
                    <ShowErrorMessage errors={ errors } formName="description" />
                </div>
            </Box>
        );
    }

    private successGetPublishNodeState(actionState: IReducerProcessActionInfo) {
        if (!actionState.isInSuccessState) return;
        this.props.reduxForm.updateReduxFormFields({
            ...this.props.publishNode
        });
    }

    private errorGetPublishNodeState(actionState: IReducerProcessActionInfo, error?: Error) {
        if (!actionState.isInErrorState) return;
        this.props.floatMessage.show(MessageType.Error, `Ошибка при получении нода публикации: ${ error?.message }`);
    }

    private submitChanges() {
        if (!this.isPublishNodeFormValid()) return;
        this.createPublishNode();
        this.editPublishNode();
    }

    private createPublishNode() {
        if (this.state.pageType !== PageEnumType.Create) return;
        this.props.dispatchCreatePublishNodeThunk({
            ...this.fillPublishNode(),
            force: true
        });
    }

    private editPublishNode() {
        if (this.state.pageType !== PageEnumType.Edit) return;
        this.props.dispatchEditPublishNodeThunk({
            ...this.fillPublishNode(),
            id: this.props.publishNodeId ?? '',
            force: true
        });
    }

    private fillPublishNode(): PublishNodeDataModel {
        const formFields = this.props.reduxForm.getReduxFormFields(false);
        return {
            address: formFields.address,
            description: formFields.description,
            id: ''
        };
    }

    private defaultInitDataReduxForm(): PublishNodesTabDataForm | undefined {
        return {
            address: '',
            description: ''
        };
    }

    private isPublishNodeFormValid() {
        const formFields = this.props.reduxForm.getReduxFormFields(false);
        const errors: IErrorsType = {};
        const arrayResults: boolean[] = [];

        arrayResults.push(checkOnValidField(errors, 'description', formFields.description, 'Заполните описание'));
        arrayResults.push(checkOnValidField(errors, 'address', formFields.address, 'Заполните адрес', TabPublishNodeLengthEnumType.Address));

        if (arrayResults.some(item => !item)) {
            this.props.floatMessage.show(MessageType.Warning, 'Форма к отправке не готова. Пожалуйста, заполните все поля.');
            this.setState({ errors });
            return false;
        }

        return true;
    }

    public render() {
        const { isOpen } = this.props;
        const isEditTypePage = this.state.pageType === PageEnumType.Edit;
        const titleText = isEditTypePage ? 'Редактирование' : 'Создание';

        return (
            <>
                <Dialog
                    disableBackdropClick={ true }
                    title={ `${ titleText } ноды публикации` }
                    isOpen={ isOpen }
                    dialogWidth="sm"
                    isTitleSmall={ true }
                    titleTextAlign="left"
                    titleFontSize={ 14 }
                    dialogVerticalAlign="center"
                    onCancelClick={ this.props.onClickToCloseModal }
                    buttons={ getDialogButtons({
                        pageType: this.state.pageType,
                        onClickToCloseModal: this.props.onClickToCloseModal,
                        submitChanges: this.submitChanges
                    }) }
                >
                    {
                        this.getDialogBody()
                    }
                </Dialog>
                <WatchReducerProcessActionState
                    watch={ [{
                        reducerStateName: 'TabPublishNodesState',
                        processIds: [
                            TabPublishNodesProcessId.GetPublishNode
                        ]
                    }] }
                    onProcessActionStateChanged={ this.onProcessActionStateChanged }
                />
            </>
        );
    }
}

const PublishNodeModalConnected = connect<StateProps, DispatchProps, OwnProps, AppReduxStoreState>(
    state => {
        const tabPublishNodesState = state.TabPublishNodesState;
        const { publishNode } = tabPublishNodesState;

        return {
            publishNode
        };
    },
    {
        dispatchGetPublishNodeThunk: GetPublishNodeThunk.invoke,
        dispatchCreatePublishNodeThunk: CreatePublishNodeThunk.invoke,
        dispatchEditPublishNodeThunk: EditPublishNodeThunk.invoke
    }
)(PublishNodeModalClass);

export const PublishNodeModalView = withReduxForm(withFloatMessages(PublishNodeModalConnected),
    {
        reduxFormName: 'Publish_Node_Modal_Form',
        resetOnUnmount: true
    });