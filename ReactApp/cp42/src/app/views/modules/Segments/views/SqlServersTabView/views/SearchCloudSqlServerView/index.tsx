import { Box } from '@mui/material';
import { OutlinedButton } from 'app/views/components/controls/Button';
import { ComboBoxForm, TextBoxForm } from 'app/views/components/controls/forms';
import { getRestoreModelTypes } from 'app/views/modules/Segments/views/SqlServersTabView/functions';
import { Component } from 'react';
import commonCss from '../../../../common/styles.module.css';

type SearchCloudSqlServerState = {
    connectionAddress: string;
    description: string;
    name: string;
    restoreModelType?: string;
};

type SearchCloudSqlServerProps = {
    onClickToSearch: () => void;
};

export class SearchCloudSqlServerView extends Component<SearchCloudSqlServerProps, SearchCloudSqlServerState> {
    public constructor(props: SearchCloudSqlServerProps) {
        super(props);

        this.state = {
            connectionAddress: '',
            description: '',
            name: ''
        };

        this.onValueChange = this.onValueChange.bind(this);
    }

    private onValueChange(formName: string, newValue: string) {
        this.setState(prevState => ({
            ...prevState,
            [formName]: newValue
        }));
    }

    public render() {
        return (
            <Box display="grid" gridTemplateColumns="repeat(auto-fit, minmax(250px, 1fr))" gap="12px" marginBottom="16px">
                <TextBoxForm
                    formName="name"
                    label="Название"
                    placeholder="Введите название"
                    value={ this.state.name }
                    onValueChange={ this.onValueChange }
                />
                <TextBoxForm
                    formName="description"
                    label="Описание"
                    placeholder="Введите описание"
                    value={ this.state.description }
                    onValueChange={ this.onValueChange }
                />
                <TextBoxForm
                    formName="connectionAddress"
                    label="Адрес подключения"
                    placeholder="Введите адрес подключения"
                    value={ this.state.connectionAddress }
                    onValueChange={ this.onValueChange }
                />
                <ComboBoxForm
                    items={ [{ text: 'Все', value: '' }].concat(getRestoreModelTypes()) }
                    formName="restoreModelType"
                    label="Модель восстановления"
                    value={ this.state.restoreModelType ?? '' }
                    onValueChange={ this.onValueChange }
                />
                <OutlinedButton
                    kind="success"
                    className={ commonCss['search-button'] }
                    onClick={ this.props.onClickToSearch }
                >
                    <i className="fa fa-search mr-1" />
                    Поиск
                </OutlinedButton>
            </Box>
        );
    }
}