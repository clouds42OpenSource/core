import { AccountDatabaseRestoreModelType } from 'app/common/enums';
import { ComboboxItemModel } from 'app/views/components/controls/forms/ComboBox/models';

/**
 * Получить список типа модели восстановления инф. базы
 * @returns Тип объекта списка для ComboBox
 */
export function getRestoreModelTypes() {
    const restoreModelTypesText = ['Простая', 'Полная', 'Смешанная'];
    return Object.keys(AccountDatabaseRestoreModelType).filter(key => !Number.isNaN(+key)).map<ComboboxItemModel<string>>(key => ({
        text: restoreModelTypesText[Number(key)],
        value: key
    }));
}