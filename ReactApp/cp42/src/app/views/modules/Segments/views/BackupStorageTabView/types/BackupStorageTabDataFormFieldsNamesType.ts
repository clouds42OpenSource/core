import { BackupStorageTabDataForm } from 'app/views/modules/Segments/views/BackupStorageTabView/types/BackupStorageTabDataForm';

export type BackupStorageTabDataFormFieldsNamesType = keyof BackupStorageTabDataForm;