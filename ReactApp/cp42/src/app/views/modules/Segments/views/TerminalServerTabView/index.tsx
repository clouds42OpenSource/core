import ErrorOutlineIcon from '@mui/icons-material/ErrorOutline';
import { Box } from '@mui/material';
import { SortingKind } from 'app/common/enums';
import { TabTerminalServerProcessId } from 'app/modules/segments/TabTerminalServer';
import { DeleteCloudTerminalServerThunkParams } from 'app/modules/segments/TabTerminalServer/store/reducer/deleteCloudTerminalServerReducer/params';
import { GetCloudTerminalServersThunkParams } from 'app/modules/segments/TabTerminalServer/store/reducer/getCloudTerminalServersReducer/params';
import { DeleteCloudTerminalServerThunk, GetCloudTerminalServersThunk } from 'app/modules/segments/TabTerminalServer/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { SuccessButton, TextLinkButton } from 'app/views/components/controls/Button';
import { Dialog } from 'app/views/components/controls/Dialog';
import { TextOut } from 'app/views/components/TextOut';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { WatchReducerProcessActionState } from 'app/views/modules/_common/components/WatchReducerProcessActionState';
import { CommonSearchSegmentView } from 'app/views/modules/Segments/common/components';
import { CommonSegmentEnumType } from 'app/views/modules/Segments/types';
import { TerminalServerModalView } from 'app/views/modules/Segments/views/TerminalServerTabView/views/TerminalServerModalView';
import { SelectDataMetadataResponseDto } from 'app/web/common';
import { SelectDataCommonDataModel, SortingDataModel } from 'app/web/common/data-models';
import { CommonSegmentDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import { IReducerProcessActionInfo } from 'core/redux/interfaces';
import { Component, createRef } from 'react';
import { connect } from 'react-redux';
import commonCss from '../../common/styles.module.css';

type TerminalServerTabState = {
    /**
     * Поле обозначающая что первая подгрузка данных была сделана
     */
    isFirstTimeLoaded: boolean;

    /**
     * Поле для открытия модального окошка
     */
    isOpen: boolean;

    /**
     * ID терминального сервера
     */
    terminalServerId?: string;
    /**
     * Состояние модального окна для удаления терминального сервера
     */
    deleteIsOpen: boolean;
    orderBy?: string;
};

type TerminalServerTabProps = {
    /**
     * Поле для подгрузки данных
     */
    isCanLoad: boolean;
};

type StateProps = {
    /**
     * Список данных списка терминального сервера
     */
    cloudTerminalServers: SelectDataMetadataResponseDto<CommonSegmentDataModel>;

    /**
     * Если true, значит список терминального сервера получен
     */
    hasCloudTerminalServersReceived: boolean;
};

type DispatchProps = {
    dispatchGetCloudTerminalServersThunk: (args: GetCloudTerminalServersThunkParams) => void;
    dispatchDeleteCloudTerminalServerThunk: (args: DeleteCloudTerminalServerThunkParams) => void;
};

type AllProps = TerminalServerTabProps & StateProps & DispatchProps & FloatMessageProps;

class TerminalServerTabClass extends Component<AllProps, TerminalServerTabState> {
    private _commonTableRef = createRef<CommonTableWithFilter<CommonSegmentDataModel, undefined>>();

    private _searchCloudServerTerminalRef = createRef<CommonSearchSegmentView>();

    public constructor(props: AllProps) {
        super(props);

        this.state = {
            isOpen: false,
            isFirstTimeLoaded: false,
            deleteIsOpen: false,
            orderBy: ''
        };

        this.onClickToOpenModal = this.onClickToOpenModal.bind(this);
        this.onClickToCloseModal = this.onClickToCloseModal.bind(this);
        this.onClickToSearch = this.onClickToSearch.bind(this);
        this.onDataSelect = this.onDataSelect.bind(this);
        this.getFormatNameServerTerminal = this.getFormatNameServerTerminal.bind(this);
        this.notificationInSuccess = this.notificationInSuccess.bind(this);
        this.notificationInError = this.notificationInError.bind(this);
        this.openModalForDeleteServerTerminal = this.openModalForDeleteServerTerminal.bind(this);
        this.getButtonsForActions = this.getButtonsForActions.bind(this);

        this.onProcessActionStateChanged = this.onProcessActionStateChanged.bind(this);
        this.onProcessCreateServerTerminal = this.onProcessCreateServerTerminal.bind(this);
        this.onProcessEditServerTerminal = this.onProcessEditServerTerminal.bind(this);
        this.onProcessDeleteServerTerminal = this.onProcessDeleteServerTerminal.bind(this);
        this.onClickDeleteServerTerminal = this.onClickDeleteServerTerminal.bind(this);
        this.closeModalForDeleteServerTerminal = this.closeModalForDeleteServerTerminal.bind(this);
        this.getSortQuery = this.getSortQuery.bind(this);
    }

    public componentDidUpdate() {
        const { isCanLoad, hasCloudTerminalServersReceived } = this.props;
        const { isFirstTimeLoaded } = this.state;

        if (isCanLoad && !isFirstTimeLoaded && !hasCloudTerminalServersReceived && this._commonTableRef.current) {
            this._commonTableRef.current.ReSelectData();
            this.setState({
                isFirstTimeLoaded: true
            });
        }
    }

    private onClickDeleteServerTerminal() {
        if (this.state.terminalServerId) {
            this.props.dispatchDeleteCloudTerminalServerThunk({
                terminalServerId: this.state.terminalServerId,
                force: true
            });
            this.setState({
                deleteIsOpen: false
            });
        }
    }

    private onProcessActionStateChanged(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        this.onProcessCreateServerTerminal(processId, actionState, error);
        this.onProcessEditServerTerminal(processId, actionState, error);
        this.onProcessDeleteServerTerminal(processId, actionState, error);
    }

    private onProcessCreateServerTerminal(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId !== TabTerminalServerProcessId.CreateTerminalServer) return;
        this.notificationInSuccess(actionState, 'Терминальный сервер успешно создан');
        this.notificationInError(actionState, error?.message);
    }

    private onProcessEditServerTerminal(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId !== TabTerminalServerProcessId.EditTerminalServer) return;
        this.notificationInSuccess(actionState, 'Терминальный сервер успешно сохранен');
        this.notificationInError(actionState, error?.message);
    }

    private onClickToCloseModal() {
        this.setState({
            isOpen: false
        });
    }

    private onClickToSearch() {
        const {
            description,
            connectionAddress,
            name
        } = this._searchCloudServerTerminalRef.current?.state!;

        this.props.dispatchGetCloudTerminalServersThunk({
            filter: {
                connectionAddress,
                description,
                name,
            },
            pageNumber: 1,
            orderBy: this.state.orderBy,
            pageSize: CommonSegmentEnumType.DefaultPageSize,
            force: true
        });
    }

    private onDataSelect(args: SelectDataCommonDataModel<undefined>) {
        const {
            description,
            connectionAddress,
            name
        } = this._searchCloudServerTerminalRef.current?.state!;

        const orderBy = args.sortingData ? this.getSortQuery(args.sortingData) : 'name.asc';

        this.setState({
            orderBy
        });

        this.props.dispatchGetCloudTerminalServersThunk({
            filter: {
                description,
                connectionAddress,
                name,
            },
            orderBy,
            pageNumber: args.pageNumber,
            showLoadingProgress: !this.state.isFirstTimeLoaded,
            pageSize: CommonSegmentEnumType.DefaultPageSize,
            force: true
        });
    }

    private onProcessDeleteServerTerminal(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId !== TabTerminalServerProcessId.DeleteTerminalServer) return;
        this.notificationInSuccess(actionState, 'Терминальный сервер успешно удален');
        this.notificationInError(actionState, error?.message);
    }

    private onClickToOpenModal(terminalServerId?: string) {
        this.setState({
            isOpen: true,
            terminalServerId
        });
    }

    private getFormatNameServerTerminal(name: string, data: CommonSegmentDataModel) {
        return (
            <TextLinkButton onClick={ () => { this.onClickToOpenModal(data.id); } }>
                <TextOut fontSize={ 13 } className="text-link">
                    { name }
                </TextOut>
            </TextLinkButton>
        );
    }

    private getButtonsForActions(Id: string) {
        return (
            <TextLinkButton
                className={ commonCss['delete-link'] }
                onClick={ () => { this.openModalForDeleteServerTerminal(Id); } }
            >
                <i className="fa fa-trash" />
            </TextLinkButton>
        );
    }

    private getSortQuery(args: SortingDataModel) {
        return `${ args.fieldName }.${ args.sortKind ? 'desc' : 'asc' }`;
    }

    private notificationInSuccess(actionState: IReducerProcessActionInfo, message: string) {
        if (!actionState.isInSuccessState) return;
        this.props.floatMessage.show(MessageType.Success, message);
        this._commonTableRef.current?.ReSelectData();
        this.onClickToCloseModal();
    }

    private notificationInError(actionState: IReducerProcessActionInfo, message?: string) {
        if (!actionState.isInErrorState) return;
        this.props.floatMessage.show(MessageType.Error, message);
    }

    private closeModalForDeleteServerTerminal() {
        this.setState({
            deleteIsOpen: false
        });
    }

    private openModalForDeleteServerTerminal(terminalServerId: string) {
        this.setState({
            terminalServerId,
            deleteIsOpen: true
        });
    }

    public render() {
        const { cloudTerminalServers: { metadata, records } } = this.props;

        return (
            <>
                <CommonSearchSegmentView
                    ref={ this._searchCloudServerTerminalRef }
                    onClickToSearch={ this.onClickToSearch }
                />
                <CommonTableWithFilter
                    className={ commonCss['custom-table'] }
                    ref={ this._commonTableRef }
                    isResponsiveTable={ true }
                    isBorderStyled={ true }
                    uniqueContextProviderStateId="TabServerTerminalContextProviderStateId"
                    tableProps={ {
                        dataset: records,
                        keyFieldName: 'id',
                        sorting: {
                            sortKind: SortingKind.Asc,
                            sortFieldName: 'name'
                        },
                        fieldsView: {
                            name: {
                                caption: 'Название',
                                fieldContentNoWrap: false,
                                fieldWidth: 'auto',
                                isSortable: true,
                                format: this.getFormatNameServerTerminal
                            },
                            connectionAddress: {
                                caption: 'Адрес подключения',
                                fieldContentNoWrap: false,
                                fieldWidth: 'auto',
                                isSortable: true
                            },
                            description: {
                                caption: 'Описание',
                                fieldContentNoWrap: false,
                                fieldWidth: 'auto',
                                isSortable: true
                            },
                            id: {
                                caption: '',
                                fieldWidth: 'auto',
                                fieldContentNoWrap: false,
                                format: this.getButtonsForActions
                            }
                        },
                        pagination: {
                            currentPage: metadata.pageNumber,
                            pageSize: metadata.pageSize,
                            recordsCount: metadata.totalItemCount,
                            totalPages: metadata.pageCount
                        },
                    } }
                    onDataSelect={ this.onDataSelect }
                />
                <SuccessButton
                    onClick={ () => { this.onClickToOpenModal(); } }
                >
                    <i className="fa fa-plus-circle mr-1" />
                    Создать терминальный сервер
                </SuccessButton>
                {
                    this.state.isOpen
                        ? (
                            <TerminalServerModalView
                                isOpen={ this.state.isOpen }
                                terminalServerId={ this.state.terminalServerId }
                                onClickToCloseModal={ this.onClickToCloseModal }
                            />
                        )
                        : null
                }
                <WatchReducerProcessActionState
                    watch={ [{
                        reducerStateName: 'TabTerminalServerState',
                        processIds: [
                            TabTerminalServerProcessId.CreateTerminalServer,
                            TabTerminalServerProcessId.EditTerminalServer,
                            TabTerminalServerProcessId.DeleteTerminalServer,
                        ]
                    }] }
                    onProcessActionStateChanged={ this.onProcessActionStateChanged }
                />
                <Dialog
                    isOpen={ this.state.deleteIsOpen }
                    dialogWidth="sm"
                    dialogVerticalAlign="center"
                    onCancelClick={ this.closeModalForDeleteServerTerminal }
                    buttons={
                        [
                            {
                                content: 'Нет',
                                kind: 'primary',
                                variant: 'text',
                                onClick: this.closeModalForDeleteServerTerminal
                            },
                            {
                                content: 'Да',
                                kind: 'error',
                                variant: 'contained',
                                onClick: this.onClickDeleteServerTerminal
                            }
                        ]
                    }
                >
                    <Box display="flex" flexDirection="column" alignItems="center" justifyContent="center">
                        <ErrorOutlineIcon color="warning" sx={ { width: '5em', height: '5em' } } />
                        Вы действительно хотите удалить терминальный сервер?
                    </Box>
                </Dialog>
            </>
        );
    }
}

const TerminalServerTabConnected = connect<StateProps, DispatchProps, NonNullable<unknown>, AppReduxStoreState>(
    state => {
        const { TabTerminalServerState } = state;
        const { cloudTerminalServers } = TabTerminalServerState;
        const { hasCloudTerminalServersReceived } = TabTerminalServerState.hasSuccessFor;

        return {
            cloudTerminalServers,
            hasCloudTerminalServersReceived
        };
    },
    {
        dispatchGetCloudTerminalServersThunk: GetCloudTerminalServersThunk.invoke,
        dispatchDeleteCloudTerminalServerThunk: DeleteCloudTerminalServerThunk.invoke
    }
)(TerminalServerTabClass);

export const TerminalServerTabView = withFloatMessages(TerminalServerTabConnected);