import { Box } from '@mui/material';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { ComboBoxForm } from 'app/views/components/controls/forms';
import { DualListBoxForm } from 'app/views/components/controls/forms/DualListBoxForm';
import { DualListItem, DualListStateListTypes } from 'app/views/components/controls/forms/DualListBoxForm/views/types';
import { SegmentModalDataForm, SegmentModalDataFormFieldsNamesType } from 'app/views/modules/Segments/views/SegmentsTabView/types';
import { SegmentElementDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import React, { Component } from 'react';

type FileStorageTabProps = {
    /**
     * Модель Redux формы для карточки сегмента
     */
    formFields: SegmentModalDataForm;

    /**
     * Функция для всплывающего сообщения
     */
    floatMessageShow: (type: MessageType, content: React.ReactNode, autoHideDuration?: number) => void;

    /**
     * Функция на обновление Redux form'ы
     */
    updateReduxFormFields: (payload: Partial<SegmentModalDataForm>) => void;

    /**
     * Функция на изменение в полях редакс формы
     * @param formName Название формы
     * @param newValue Новое значение
     */
    onValueChange<TValue, TForm extends string = SegmentModalDataFormFieldsNamesType>(formName: TForm, newValue: TValue): void;
};

/**
 * Компонент вкладки "Хранилища файловых баз"
 */
export class FileStorageTabView extends Component<FileStorageTabProps> {
    public constructor(props: FileStorageTabProps) {
        super(props);

        this.onValueChange = this.onValueChange.bind(this);
        this.checkAbilityToDeleteFileStorage = this.checkAbilityToDeleteFileStorage.bind(this);
    }

    /**
     * Функция на изменение DualListBoxForm
     * @param _fieldName Название формы
     * @param newLeftValue Новые значения левой колонки
     * @param newRightValue Новые значения правой колонки
     * @returns Логическое значение для стейта DualListBoxForm
     */
    private onValueChange(_fieldName: string, newLeftValue: DualListItem<string>[], newRightValue: DualListItem<string>[]) {
        const isCanChange = this.checkAbilityToDeleteFileStorage(newRightValue);

        isCanChange
            ? this.props.updateReduxFormFields({
                availableSegmentFileStorageServers: newLeftValue.map(item => ({ key: item.key, value: item.label })),
                segmentFileStorageServers: newRightValue.map(item => ({ key: item.key, value: item.label }))
            })
            : this.props.floatMessageShow(MessageType.Warning, 'Удаление не возможно. Это хранилище по умолчанию');

        return isCanChange;
    }

    /**
     * Получить элементы двойного выпадающего списка
     * @param items список элементов модели
     */
    private getDualListItems(items: SegmentElementDataModel[]): Array<DualListItem<string>> {
        if (items) {
            return items.map(item => ({
                label: item.value,
                key: item.key
            }));
        }

        return [];
    }

    /**
     * Функция для проверки возможности удаления файлового хранилища из доступных
     * @param newRightValue Новые значения правой колонки
     * @returns True доступно для удалени, False не доступно
     */
    private checkAbilityToDeleteFileStorage(newRightValue: DualListItem<string>[]): boolean {
        if (newRightValue.length === 0) return false;
        return newRightValue.some(item => item.key === this.props.formFields.defaultSegmentStorageId);
    }

    public render() {
        const { formFields: { availableSegmentFileStorageServers, segmentFileStorageServers, defaultSegmentStorageId } } = this.props;

        return (
            <Box display="grid">
                <ComboBoxForm
                    label="Хранилище по умолчанию:"
                    formName="defaultSegmentStorageId"
                    items={ segmentFileStorageServers ? segmentFileStorageServers.map(item => ({ text: item.value, value: item.key })) : [] }
                    value={ defaultSegmentStorageId ?? '' }
                    onValueChange={ this.props.onValueChange }
                />
                <DualListBoxForm
                    allowSelectAll={ true }
                    mainList={ DualListStateListTypes.rightList }
                    leftListLabel="Доступные хранилища для выбора:"
                    rightListLabel="Выбранные хранилища:"
                    formName="fileStorages"
                    leftListItems={ this.getDualListItems(availableSegmentFileStorageServers) }
                    rightListItems={ this.getDualListItems(segmentFileStorageServers) }
                    isFormDisabled={ false }
                    onValueChange={ this.onValueChange }
                />
            </Box>
        );
    }
}