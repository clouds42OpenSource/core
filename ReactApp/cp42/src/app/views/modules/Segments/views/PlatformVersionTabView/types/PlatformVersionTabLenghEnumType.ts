/**
 * Enum максимальной длинны полей версии платформы 1С
 */
export enum PlatformVersionTabLenghEnumType {
    /**
     * Максимальная длина пути к платформе 1С
     */
    PathToPlatform = 250
}