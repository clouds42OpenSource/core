import ErrorOutlineIcon from '@mui/icons-material/ErrorOutline';
import { Box } from '@mui/material';
import { SortingKind } from 'app/common/enums';
import { TabTerminalGatewayProcessId } from 'app/modules/segments/TabTerminalGateway';
import { DeleteCloudGatewayTerminalThunkParams } from 'app/modules/segments/TabTerminalGateway/store/reducer/deleteCloudGatewayTerminalReducer/params';
import { GetCloudGatewayTerminalsThunkParams } from 'app/modules/segments/TabTerminalGateway/store/reducer/getCloudGatewayTerminalsReducer/params';
import { DeleteCloudGatewayTerminalThunk, GetCloudGatewayTerminalsThunk } from 'app/modules/segments/TabTerminalGateway/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { SuccessButton, TextLinkButton } from 'app/views/components/controls/Button';
import { Dialog } from 'app/views/components/controls/Dialog';
import { TextOut } from 'app/views/components/TextOut';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { WatchReducerProcessActionState } from 'app/views/modules/_common/components/WatchReducerProcessActionState';
import { CommonSearchSegmentView } from 'app/views/modules/Segments/common/components';
import { CommonSegmentEnumType } from 'app/views/modules/Segments/types';
import { GatewayTerminalModalView } from 'app/views/modules/Segments/views/GatewayTerminalTabView/views/GatewayTerminalModalView';
import { SelectDataMetadataResponseDto, SortingDataModel } from 'app/web/common';
import { SelectDataCommonDataModel } from 'app/web/common/data-models';
import { CommonSegmentDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import { IReducerProcessActionInfo } from 'core/redux/interfaces';
import { Component, createRef } from 'react';
import { connect } from 'react-redux';
import commonCss from '../../common/styles.module.css';

type GatewayTerminalTabState = {
    /**
     * Поле обозначающая что первая подгрузка данных была сделана
     */
    isFirstTimeLoaded: boolean;

    /**
     * Поле для открытия модального окошка
     */
    isOpen: boolean;

    /**
     * ID терминального шлюза
     */
    cloudGatewayTerminalId?: string;
    /**
     * Состояние модального окна на удаление терминального шлюза
     */
    deleteIsOpen: boolean;
    orderBy?: string;
};

type GatewayTerminalTabProps = {
    /**
     * Поле для подгрузки данных
     */
    isCanLoad: boolean;
};

type StateProps = {
    /**
     * Список данных списка терминального шлюза
     */
    cloudGatewayTerminals: SelectDataMetadataResponseDto<CommonSegmentDataModel>;

    /**
     * Если true, значит список терминального шлюза получен
     */
    hasCloudGatewayTerminalsReceived: boolean;
};

type DispatchProps = {
    dispatchGetCloudGatewayTerminalsThunk: (args: GetCloudGatewayTerminalsThunkParams) => void;
    dispatchDeleteCloudGatewayTerminalThunk: (args: DeleteCloudGatewayTerminalThunkParams) => void;
};

type AllProps = GatewayTerminalTabProps & StateProps & DispatchProps & FloatMessageProps;

class GatewayTerminalTabClass extends Component<AllProps, GatewayTerminalTabState> {
    private _commonTableRef = createRef<CommonTableWithFilter<CommonSegmentDataModel, undefined>>();

    private _searchCloudGatewayTerminalRef = createRef<CommonSearchSegmentView>();

    public constructor(props: AllProps) {
        super(props);

        this.state = {
            isOpen: false,
            isFirstTimeLoaded: false,
            deleteIsOpen: false,
            orderBy: ''
        };

        this.onClickToOpenModal = this.onClickToOpenModal.bind(this);
        this.onClickToCloseModal = this.onClickToCloseModal.bind(this);
        this.onClickToSearch = this.onClickToSearch.bind(this);
        this.onDataSelect = this.onDataSelect.bind(this);
        this.getFormatNameGatewayTerminal = this.getFormatNameGatewayTerminal.bind(this);
        this.notificationInSuccess = this.notificationInSuccess.bind(this);
        this.notificationInError = this.notificationInError.bind(this);
        this.openModalForDeleteGatewayTerminal = this.openModalForDeleteGatewayTerminal.bind(this);
        this.getButtonsForActions = this.getButtonsForActions.bind(this);

        this.onProcessActionStateChanged = this.onProcessActionStateChanged.bind(this);
        this.onProcessCreateGatewayTerminal = this.onProcessCreateGatewayTerminal.bind(this);
        this.onProcessEditGatewayTerminal = this.onProcessEditGatewayTerminal.bind(this);
        this.onProcessDeleteGatewayTerminal = this.onProcessDeleteGatewayTerminal.bind(this);
        this.onClickDeleteGatewayTerminal = this.onClickDeleteGatewayTerminal.bind(this);
        this.closeModalForDeleteGatewayTerminal = this.closeModalForDeleteGatewayTerminal.bind(this);
    }

    public componentDidUpdate() {
        const { isCanLoad, hasCloudGatewayTerminalsReceived } = this.props;
        const { isFirstTimeLoaded } = this.state;

        if (isCanLoad && !isFirstTimeLoaded && !hasCloudGatewayTerminalsReceived && this._commonTableRef.current) {
            this._commonTableRef.current.ReSelectData();
            this.setState({
                isFirstTimeLoaded: true
            });
        }
    }

    private onClickDeleteGatewayTerminal() {
        if (this.state.cloudGatewayTerminalId) {
            this.props.dispatchDeleteCloudGatewayTerminalThunk({
                cloudGatewayTerminalId: this.state.cloudGatewayTerminalId,
                force: true
            });
            this.setState({
                deleteIsOpen: false
            });
        }
    }

    private onProcessActionStateChanged(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        this.onProcessCreateGatewayTerminal(processId, actionState, error);
        this.onProcessEditGatewayTerminal(processId, actionState, error);
        this.onProcessDeleteGatewayTerminal(processId, actionState, error);
    }

    private onProcessCreateGatewayTerminal(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId !== TabTerminalGatewayProcessId.CreateTerminalGateway) return;
        this.notificationInSuccess(actionState, 'Терминальный шлюз успешно создан');
        this.notificationInError(actionState, error?.message);
    }

    private onProcessEditGatewayTerminal(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId !== TabTerminalGatewayProcessId.EditTerminalGateway) return;
        this.notificationInSuccess(actionState, 'Терминальный шлюз успешно сохранен');
        this.notificationInError(actionState, error?.message);
    }

    private onProcessDeleteGatewayTerminal(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId !== TabTerminalGatewayProcessId.DeleteTerminalGateway) return;
        this.notificationInSuccess(actionState, 'Терминальный шлюз успешно удален');
        this.notificationInError(actionState, error?.message);
    }

    private onClickToOpenModal(cloudGatewayTerminalId?: string) {
        this.setState({
            isOpen: true,
            cloudGatewayTerminalId
        });
    }

    private onClickToCloseModal() {
        this.setState({
            isOpen: false
        });
    }

    private onClickToSearch() {
        const {
            description,
            connectionAddress,
            name
        } = this._searchCloudGatewayTerminalRef.current?.state!;

        this.props.dispatchGetCloudGatewayTerminalsThunk({
            filter: {
                connectionAddress,
                description,
                name,
            },
            pageNumber: 1,
            orderBy: this.state.orderBy,
            pageSize: CommonSegmentEnumType.DefaultPageSize,
            force: true
        });
    }

    private onDataSelect(args: SelectDataCommonDataModel<undefined>) {
        const {
            description,
            connectionAddress,
            name
        } = this._searchCloudGatewayTerminalRef.current?.state!;

        const orderBy = args.sortingData ? this.getSortQuery(args.sortingData) : 'name.asc';

        this.setState({
            orderBy
        });

        this.props.dispatchGetCloudGatewayTerminalsThunk({
            filter: {
                description,
                connectionAddress,
                name,
            },
            pageNumber: args.pageNumber,
            orderBy,
            pageSize: CommonSegmentEnumType.DefaultPageSize,
            force: true
        });
    }

    private getSortQuery(args: SortingDataModel) {
        return `${ args.fieldName }.${ args.sortKind ? 'desc' : 'asc' }`;
    }

    private getButtonsForActions(Id: string) {
        return (
            <TextLinkButton
                className={ commonCss['delete-link'] }
                onClick={ () => { this.openModalForDeleteGatewayTerminal(Id); } }
            >
                <i className="fa fa-trash" />
            </TextLinkButton>
        );
    }

    private getFormatNameGatewayTerminal(name: string, data: CommonSegmentDataModel) {
        return (
            <TextLinkButton onClick={ () => { this.onClickToOpenModal(data.id); } }>
                <TextOut fontSize={ 13 } className="text-link">
                    { name }
                </TextOut>
            </TextLinkButton>
        );
    }

    private notificationInSuccess(actionState: IReducerProcessActionInfo, message: string) {
        if (!actionState.isInSuccessState) return;
        this.props.floatMessage.show(MessageType.Success, message);
        this._commonTableRef.current?.ReSelectData();
        this.onClickToCloseModal();
    }

    private notificationInError(actionState: IReducerProcessActionInfo, message?: string) {
        if (!actionState.isInErrorState) return;
        this.props.floatMessage.show(MessageType.Error, message);
    }

    private closeModalForDeleteGatewayTerminal() {
        this.setState({
            deleteIsOpen: false
        });
    }

    private openModalForDeleteGatewayTerminal(cloudGatewayTerminalId: string) {
        this.setState({
            cloudGatewayTerminalId,
            deleteIsOpen: true
        });
    }

    public render() {
        const { cloudGatewayTerminals: { metadata, records } } = this.props;

        return (
            <>
                <CommonSearchSegmentView
                    ref={ this._searchCloudGatewayTerminalRef }
                    onClickToSearch={ this.onClickToSearch }
                />
                <CommonTableWithFilter
                    className={ commonCss['custom-table'] }
                    ref={ this._commonTableRef }
                    isResponsiveTable={ true }
                    isBorderStyled={ true }
                    uniqueContextProviderStateId="TabGatewayTerminalContextProviderStateId"
                    tableProps={ {
                        dataset: records,
                        keyFieldName: 'id',
                        sorting: {
                            sortFieldName: 'name',
                            sortKind: SortingKind.Asc
                        },
                        fieldsView: {
                            name: {
                                caption: 'Название',
                                fieldContentNoWrap: false,
                                fieldWidth: 'auto',
                                isSortable: true,
                                format: this.getFormatNameGatewayTerminal
                            },
                            description: {
                                caption: 'Описание',
                                fieldContentNoWrap: false,
                                fieldWidth: 'auto',
                                isSortable: true
                            },
                            connectionAddress: {
                                caption: 'Адрес подключения',
                                fieldContentNoWrap: false,
                                fieldWidth: 'auto',
                                isSortable: true
                            },
                            id: {
                                caption: '',
                                fieldWidth: 'auto',
                                fieldContentNoWrap: false,
                                format: this.getButtonsForActions
                            }
                        },
                        pagination: {
                            currentPage: metadata.pageNumber,
                            pageSize: metadata.pageSize,
                            recordsCount: metadata.totalItemCount,
                            totalPages: metadata.pageCount
                        },
                    } }
                    onDataSelect={ this.onDataSelect }
                />
                <SuccessButton
                    onClick={ () => { this.onClickToOpenModal(); } }
                >
                    <i className="fa fa-plus-circle mr-1" />
                    Создать терминальный шлюз
                </SuccessButton>
                {
                    this.state.isOpen
                        ? (
                            <GatewayTerminalModalView
                                isOpen={ this.state.isOpen }
                                cloudGatewayTerminalId={ this.state.cloudGatewayTerminalId }
                                onClickToCloseModal={ this.onClickToCloseModal }
                            />
                        )
                        : null
                }
                <WatchReducerProcessActionState
                    watch={ [{
                        reducerStateName: 'TabTerminalGatewayState',
                        processIds: [
                            TabTerminalGatewayProcessId.CreateTerminalGateway,
                            TabTerminalGatewayProcessId.EditTerminalGateway,
                            TabTerminalGatewayProcessId.DeleteTerminalGateway,
                        ]
                    }] }
                    onProcessActionStateChanged={ this.onProcessActionStateChanged }
                />
                <Dialog
                    isOpen={ this.state.deleteIsOpen }
                    dialogWidth="sm"
                    dialogVerticalAlign="center"
                    onCancelClick={ this.closeModalForDeleteGatewayTerminal }
                    buttons={
                        [
                            {
                                content: 'Нет',
                                kind: 'primary',
                                variant: 'text',
                                onClick: this.closeModalForDeleteGatewayTerminal
                            },
                            {
                                content: 'Да',
                                kind: 'error',
                                variant: 'contained',
                                onClick: this.onClickDeleteGatewayTerminal
                            }
                        ]
                    }
                >
                    <Box display="flex" flexDirection="column" alignItems="center" justifyContent="center">
                        <ErrorOutlineIcon color="warning" sx={ { width: '5em', height: '5em' } } />
                        Вы действительно хотите удалить терминальный шлюз?
                    </Box>
                </Dialog>
            </>
        );
    }
}

const GatewayTerminalTabConnected = connect<StateProps, DispatchProps, NonNullable<unknown>, AppReduxStoreState>(
    state => {
        const tabTerminalGatewayState = state.TabTerminalGatewayState;
        const { cloudGatewayTerminals } = tabTerminalGatewayState;
        const { hasCloudGatewayTerminalsReceived } = tabTerminalGatewayState.hasSuccessFor;

        return {
            cloudGatewayTerminals,
            hasCloudGatewayTerminalsReceived
        };
    },
    {
        dispatchGetCloudGatewayTerminalsThunk: GetCloudGatewayTerminalsThunk.invoke,
        dispatchDeleteCloudGatewayTerminalThunk: DeleteCloudGatewayTerminalThunk.invoke
    }
)(GatewayTerminalTabClass);

export const GatewayTerminalTabView = withFloatMessages(GatewayTerminalTabConnected);