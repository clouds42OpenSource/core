import { PageEnumType } from 'app/common/enums';
import { checkOnValidField, checkValidOfStringField } from 'app/common/functions';
import { IErrorsType } from 'app/common/interfaces';
import { TabContentServersProcessId } from 'app/modules/segments/TabContentServers';
import { CheckNodeAvailabilityThunkParams } from 'app/modules/segments/TabContentServers/store/reducer/checkNodeAvailabilityReducer/params';
import { CreateCloudContentServerThunkParams } from 'app/modules/segments/TabContentServers/store/reducer/createCloudContentServerReducer/params';
import { EditCloudContentServerThunkParams } from 'app/modules/segments/TabContentServers/store/reducer/editCloudContentServerReducer/params';
import { GetCloudContentServerThunkParams } from 'app/modules/segments/TabContentServers/store/reducer/getCloudContentServerReducer/params';
import { CheckNodeAvailabilityThunk, CreateCloudContentServerThunk, EditCloudContentServerThunk, GetAvailablePublishNodesThunk, GetCloudContentServerThunk } from 'app/modules/segments/TabContentServers/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { Dialog } from 'app/views/components/controls/Dialog';
import { TabsControl } from 'app/views/components/controls/TabsControl';
import { getDialogButtons } from 'app/views/modules/Segments/common/functions';
import { ContentServersTabDataForm, ContentServersTabDataFormFieldsNamesType, TabContentServerLengthEnumType } from 'app/views/modules/Segments/views/ContentServersTabView/common/types';
import { CloudServerCommonTabView } from 'app/views/modules/Segments/views/ContentServersTabView/views/ContentServerModalView/partial-components/CloudServerCommonTabView';
import { CloudServerPublishNodesTabView } from 'app/views/modules/Segments/views/ContentServersTabView/views/ContentServerModalView/partial-components/CloudServerPublishNodesTabView';
import { WatchReducerProcessActionState } from 'app/views/modules/_common/components/WatchReducerProcessActionState';
import { CreateCloudContentServerParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabContentServersApiProxy';
import { PublishNodeDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import { CloudContentServerResponseDto } from 'app/web/api/SegmentsProxy/TabContentServers/response-dto';
import { IReducerProcessActionInfo } from 'core/redux/interfaces';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';
import { Component } from 'react';
import { connect } from 'react-redux';

type OwnProps = ReduxFormProps<ContentServersTabDataForm>;

type ContentServerModalProps = {
    /**
     * Поле для открытия модального окошка сайта публикации
     */
    isOpen: boolean;

    /**
     * ID сайта публикации
     */
    contentServerId?: string;

    /**
     * Функция на закрытие модального окошка
     */
    onClickToCloseModal: () => void;
};

type ContentServerModalState = {
    /**
     * Обьект Ошибок для валидации
     */
    errors: IErrorsType;

    /**
     * Тип страницы для динамического компонента редактирования/создания
     */
    pageType: PageEnumType;
};

type StateProps = {
    /**
     *  Модель сервера публикации
     */
    contentServer: CloudContentServerResponseDto;

    /**
     * Массив доступных нодов публикаций
     */
    availablePublishNodes: PublishNodeDataModel[];

    /**
     * Флаг на доступность ноды
     */
    isNodeAvailable: boolean;
};

type DispatchProps = {
    dispatchGetAvailablePublishNodesThunk: (args: IForceThunkParam) => void;
    dispatchGetCloudContentServerThunk: (args: GetCloudContentServerThunkParams) => void;
    dispatchCreateCloudContentServerThunk: (args: CreateCloudContentServerThunkParams) => void;
    dispatchEditCloudContentServerThunk: (args: EditCloudContentServerThunkParams) => void;
    dispatchCheckNodeAvailabilityThunk: (args: CheckNodeAvailabilityThunkParams) => void;
};

type AllProps = ContentServerModalProps & OwnProps & StateProps & DispatchProps & FloatMessageProps;

class ContentServerModalClass extends Component<AllProps, ContentServerModalState> {
    public constructor(props: AllProps) {
        super(props);

        this.state = {
            errors: {},
            pageType: this.props.contentServerId ? PageEnumType.Edit : PageEnumType.Create
        };

        this.getCloudServerCommonTab = this.getCloudServerCommonTab.bind(this);
        this.getCloudServerPublishNodesTab = this.getCloudServerPublishNodesTab.bind(this);
        this.onValueChange = this.onValueChange.bind(this);
        this.submitChanges = this.submitChanges.bind(this);
        this.createContentServer = this.createContentServer.bind(this);
        this.editContentServer = this.editContentServer.bind(this);
        this.isContentServerFormValid = this.isContentServerFormValid.bind(this);

        this.onProcessActionStateChanged = this.onProcessActionStateChanged.bind(this);
        this.onProcessGetContentServer = this.onProcessGetContentServer.bind(this);
        this.onProcessGetAvailablePublishNodes = this.onProcessGetAvailablePublishNodes.bind(this);
        this.onProcessCheckNodeAvailability = this.onProcessCheckNodeAvailability.bind(this);
        this.successGetContentServerState = this.successGetContentServerState.bind(this);
        this.successGetAvailablePublishNodesState = this.successGetAvailablePublishNodesState.bind(this);
        this.successCheckNodeAvailabilityState = this.successCheckNodeAvailabilityState.bind(this);
        this.errorState = this.errorState.bind(this);
        this.fillContentServer = this.fillContentServer.bind(this);
        this.onCreatePageInit = this.onCreatePageInit.bind(this);
        this.onEditPageInit = this.onEditPageInit.bind(this);

        this.props.reduxForm.setInitializeFormDataAction(this.defaultInitDataReduxForm);
    }

    public componentDidMount() {
        this.onCreatePageInit();
        this.onEditPageInit();
    }

    private onCreatePageInit() {
        if (this.state.pageType !== PageEnumType.Create) return;
        this.props.dispatchGetAvailablePublishNodesThunk({
            force: true
        });
    }

    private onEditPageInit() {
        if (this.state.pageType !== PageEnumType.Edit) return;
        this.props.dispatchGetCloudContentServerThunk({
            contentServerId: this.props.contentServerId!,
            force: true
        });
    }

    private onProcessActionStateChanged(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        this.onProcessGetContentServer(processId, actionState, error);
        this.onProcessGetAvailablePublishNodes(processId, actionState, error);
        this.onProcessCheckNodeAvailability(processId, actionState, error);
    }

    private onProcessGetContentServer(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId !== TabContentServersProcessId.GetContentServer) return;
        this.successGetContentServerState(actionState);
        this.errorState(actionState, `Ошибка при получении сайта публикации: ${ error?.message }`);
    }

    private onProcessGetAvailablePublishNodes(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId !== TabContentServersProcessId.GetAvailablePublishNodes) return;
        this.successGetAvailablePublishNodesState(actionState);
        this.errorState(actionState, `Ошибка при получении нодов публикации: ${ error?.message }`);
    }

    private onProcessCheckNodeAvailability(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId !== TabContentServersProcessId.CheckNodeAvailability) return;
        this.successCheckNodeAvailabilityState(actionState);
        this.errorState(actionState, error?.message ?? '');
    }

    private onValueChange<TValue, TForm extends string = ContentServersTabDataFormFieldsNamesType>(formName: TForm, newValue: TValue) {
        this.props.reduxForm.updateReduxFormFields({
            [formName]: newValue
        });

        const { errors } = this.state;

        if (typeof newValue === 'string') {
            switch (formName) {
                case 'name':
                    errors[formName] = checkValidOfStringField(newValue, 'Заполните название', TabContentServerLengthEnumType.Name);
                    break;
                case 'description':
                    errors[formName] = checkValidOfStringField(newValue, undefined, TabContentServerLengthEnumType.Description);
                    break;
                case 'publishSiteName':
                    errors[formName] = checkValidOfStringField(newValue, 'Заполните адрес публикации', TabContentServerLengthEnumType.PublishSiteName);
                    break;
                default:
                    break;
            }

            this.setState({ errors });
        }
    }

    private getCloudServerCommonTab() {
        const formFields = this.props.reduxForm.getReduxFormFields(false);
        const { errors } = this.state;

        return (<CloudServerCommonTabView
            onValueChange={ this.onValueChange }
            errors={ errors }
            formFields={ formFields }
        />);
    }

    private getCloudServerPublishNodesTab() {
        const formFields = this.props.reduxForm.getReduxFormFields(false);

        return (
            <CloudServerPublishNodesTabView
                showNotification={ this.props.floatMessage.show }
                formFields={ formFields }
                updateReduxFormFields={ this.props.reduxForm.updateReduxFormFields }
                dispatchCheckNodeAvailabilityThunk={ this.props.dispatchCheckNodeAvailabilityThunk }
            />
        );
    }

    private successGetContentServerState(actionState: IReducerProcessActionInfo) {
        if (!actionState.isInSuccessState) return;
        this.props.reduxForm.updateReduxFormFields({
            ...this.props.contentServer
        });
    }

    private successCheckNodeAvailabilityState(actionState: IReducerProcessActionInfo) {
        if (!actionState.isInSuccessState) return;
        if (!this.props.isNodeAvailable) this.props.floatMessage.show(MessageType.Warning, 'Обратите внимание, добавленная нода не доступна в данный момент!');
    }

    private successGetAvailablePublishNodesState(actionState: IReducerProcessActionInfo) {
        if (!actionState.isInSuccessState) return;
        this.props.reduxForm.updateReduxFormFields({
            nodesData: {
                selected: [],
                available: this.props.availablePublishNodes
            }
        });
    }

    private errorState(actionState: IReducerProcessActionInfo, message: string) {
        if (!actionState.isInErrorState) return;
        this.props.floatMessage.show(MessageType.Error, message);
    }

    private submitChanges() {
        if (!this.isContentServerFormValid()) return;
        this.createContentServer();
        this.editContentServer();
    }

    private createContentServer() {
        if (this.state.pageType !== PageEnumType.Create) return;
        this.props.dispatchCreateCloudContentServerThunk({
            ...this.fillContentServer(),
            force: true
        });
    }

    private editContentServer() {
        if (this.state.pageType !== PageEnumType.Edit) return;
        const formFields = this.props.reduxForm.getReduxFormFields(false);
        this.props.dispatchEditCloudContentServerThunk({
            ...this.fillContentServer(),
            id: this.props.contentServerId!,
            availablePublishNodes: formFields.nodesData.available,
            force: true
        });
    }

    private fillContentServer(): CreateCloudContentServerParams {
        const formFields = this.props.reduxForm.getReduxFormFields(false);
        return {
            description: formFields.description,
            name: formFields.name,
            groupByAccount: formFields.groupByAccount,
            publishNodes: formFields.nodesData.selected,
            publishSiteName: formFields.publishSiteName
        };
    }

    private defaultInitDataReduxForm(): ContentServersTabDataForm | undefined {
        return {
            description: '',
            name: '',
            groupByAccount: false,
            nodesData: {
                available: [],
                selected: [],
            },
            publishSiteName: ''
        };
    }

    private isContentServerFormValid() {
        const formFields = this.props.reduxForm.getReduxFormFields(false);
        const errors: IErrorsType = {};
        const arrayResults: boolean[] = [];

        arrayResults.push(checkOnValidField(errors, 'name', formFields.name, 'Заполните название', TabContentServerLengthEnumType.Name));
        arrayResults.push(checkOnValidField(errors, 'description', formFields.description, undefined, TabContentServerLengthEnumType.Description));
        arrayResults.push(checkOnValidField(errors, 'publishSiteName', formFields.publishSiteName, 'Заполните адрес публикации', TabContentServerLengthEnumType.PublishSiteName));

        if (arrayResults.some(item => !item)) {
            this.props.floatMessage.show(MessageType.Warning, 'Форма к отправке не готова. Пожалуйста, заполните все поля.');
            this.setState({ errors });
            return false;
        }

        return true;
    }

    public render() {
        const { isOpen } = this.props;
        const isEditTypePage = this.state.pageType === PageEnumType.Edit;
        const titleText = isEditTypePage ? 'Редактирование' : 'Создание';

        return (
            <>
                <Dialog
                    disableBackdropClick={ true }
                    title={ `${ titleText } сайта публикации` }
                    isOpen={ isOpen }
                    dialogWidth="sm"
                    isTitleSmall={ true }
                    titleTextAlign="left"
                    titleFontSize={ 14 }
                    dialogVerticalAlign="center"
                    onCancelClick={ this.props.onClickToCloseModal }
                    buttons={ getDialogButtons({
                        pageType: this.state.pageType,
                        onClickToCloseModal: this.props.onClickToCloseModal,
                        submitChanges: this.submitChanges
                    }) }
                >
                    <TabsControl
                        headers={ [
                            {
                                title: 'Общее',
                                isVisible: true
                            },
                            {
                                title: 'Ноды публикаций',
                                isVisible: true
                            }
                        ] }
                    >
                        { this.getCloudServerCommonTab() }
                        { this.getCloudServerPublishNodesTab() }
                    </TabsControl>
                </Dialog>
                <WatchReducerProcessActionState
                    watch={ [{
                        reducerStateName: 'TabContentServersState',
                        processIds: [
                            TabContentServersProcessId.GetContentServer,
                            TabContentServersProcessId.GetAvailablePublishNodes,
                            TabContentServersProcessId.CheckNodeAvailability
                        ]
                    }] }
                    onProcessActionStateChanged={ this.onProcessActionStateChanged }
                />
            </>
        );
    }
}

const ContentServerModalConnected = connect<StateProps, DispatchProps, OwnProps, AppReduxStoreState>(
    state => {
        const tabContentServersState = state.TabContentServersState;
        const { contentServer } = tabContentServersState;
        const { availablePublishNodes } = tabContentServersState;
        const { isNodeAvailable } = tabContentServersState;

        return {
            contentServer,
            availablePublishNodes,
            isNodeAvailable
        };
    },
    {
        dispatchGetAvailablePublishNodesThunk: GetAvailablePublishNodesThunk.invoke,
        dispatchGetCloudContentServerThunk: GetCloudContentServerThunk.invoke,
        dispatchCreateCloudContentServerThunk: CreateCloudContentServerThunk.invoke,
        dispatchEditCloudContentServerThunk: EditCloudContentServerThunk.invoke,
        dispatchCheckNodeAvailabilityThunk: CheckNodeAvailabilityThunk.invoke
    }
)(ContentServerModalClass);

export const ContentServerModalView = withReduxForm(withFloatMessages(ContentServerModalConnected),
    {
        reduxFormName: 'Content_Server_Modal_Form',
        resetOnUnmount: true
    });