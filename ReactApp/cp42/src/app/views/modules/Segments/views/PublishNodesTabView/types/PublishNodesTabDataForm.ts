/**
 * Модель Redux формы для нода публикации
 */
export type PublishNodesTabDataForm = {
    /**
     * Адрес подключения
     */
    address: string;

    /**
     * Описание
     */
    description: string;
};