import { Box } from '@mui/material';
import { OutlinedButton } from 'app/views/components/controls/Button';
import { TextBoxForm } from 'app/views/components/controls/forms';
import { Component } from 'react';
import commonCss from '../../../../common/styles.module.css';

type SearchHostingsState = {
    /**
     * Название хостинга
     */
    name: string,

    /**
     * Путь для загружаемых файлов
     */
    uploadFilesPath: string,

    /**
     * Адрес API для загрузки файлов
     */
    uploadApiUrl: string
};

type SearchHostingsProps = {
    /**
     * Функция для поиска
     */
    onClickToSearch: () => void;
};

export class SearchHostingsView extends Component<SearchHostingsProps, SearchHostingsState> {
    public constructor(props: SearchHostingsProps) {
        super(props);

        this.state = {
            name: '',
            uploadApiUrl: '',
            uploadFilesPath: ''
        };

        this.onValueChange = this.onValueChange.bind(this);
    }

    private onValueChange(formName: string, newValue: string) {
        this.setState(prevState => ({
            ...prevState,
            [formName]: newValue
        }));
    }

    public render() {
        return (
            <Box display="grid" gridTemplateColumns="repeat(auto-fit, minmax(250px, 1fr))" gap="12px" marginBottom="16px">
                <TextBoxForm
                    formName="name"
                    label="Название"
                    placeholder="Введите название"
                    value={ this.state.name }
                    onValueChange={ this.onValueChange }
                />
                <TextBoxForm
                    formName="uploadFilesPath"
                    label="Путь для загружаемых файлов"
                    placeholder="Введите путь"
                    value={ this.state.uploadFilesPath }
                    onValueChange={ this.onValueChange }
                />
                <TextBoxForm
                    formName="uploadApiUrl"
                    label="Адрес API для загрузки файлов"
                    placeholder="Введите адрес"
                    value={ this.state.uploadApiUrl }
                    onValueChange={ this.onValueChange }
                />
                <OutlinedButton
                    kind="success"
                    className={ commonCss['search-button'] }
                    onClick={ this.props.onClickToSearch }
                >
                    <i className="fa fa-search mr-1" />
                    Поиск
                </OutlinedButton>
            </Box>
        );
    }
}