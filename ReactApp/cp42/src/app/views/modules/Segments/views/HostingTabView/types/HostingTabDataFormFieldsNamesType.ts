import { HostingTabDataForm } from 'app/views/modules/Segments/views/HostingTabView/types';

export type HostingTabDataFormFieldsNamesType = keyof HostingTabDataForm;