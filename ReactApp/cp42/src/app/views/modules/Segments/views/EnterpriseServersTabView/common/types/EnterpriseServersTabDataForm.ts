/**
 * Модель Redux формы для сервера 1С:Предприятия
 */
export type EnterpriseServersTabDataForm = {
    /**
     * Адрес подключения
     */
    connectionAddress: string;

    /**
     * Название
     */
    name: string;

    /**
     * Описание
     */
    description: string;
    clusterSettingsPath: string;

    /**
     * Версия платформы
     */
    version: string;

    /**
     * Логин администратора
     */
    adminName: string;

    /**
     * Пароль администратора
     */
    adminPassword: string;
};