import { EnterpriseServersTabDataForm } from 'app/views/modules/Segments/views/EnterpriseServersTabView/common';

export type EnterpriseServersTabDataFormFieldsNamesType = keyof EnterpriseServersTabDataForm;