import { DualListBoxForm } from 'app/views/components/controls/forms/DualListBoxForm';
import { DualListItem, DualListStateListTypes } from 'app/views/components/controls/forms/DualListBoxForm/views/types';
import { SegmentModalDataForm } from 'app/views/modules/Segments/views/SegmentsTabView/types';
import { SegmentTerminalServerDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabSegmentsApiProxy';
import { Component } from 'react';

type TerminalServersTabProps = {
    formFields: SegmentModalDataForm;
    updateReduxFormFields: (payload: Partial<SegmentModalDataForm>) => void;
};

export class TerminalServersTabView extends Component<TerminalServersTabProps> {
    public constructor(props: TerminalServersTabProps) {
        super(props);

        this.onValueChange = this.onValueChange.bind(this);
    }

    private onValueChange(_fieldName: string, newLeftValue: DualListItem<string>[], newRightValue: DualListItem<string>[]) {
        this.props.updateReduxFormFields({
            availableSegmentTerminalServers: newLeftValue.map(item => ({
                key: item.key,
                value: item.label,
                available: true,
                connectionAddress: ''
            })),
            segmentTerminalServers: newRightValue.map(item => ({
                key: item.key,
                value: item.label,
                available: false,
                connectionAddress: ''
            }))
        });

        return true;
    }

    private getDualListItems(items: SegmentTerminalServerDataModel[]): Array<DualListItem<string>> {
        if (items) {
            return items.map(item => ({
                label: item.value,
                key: item.key
            }));
        }

        return [];
    }

    public render() {
        const { formFields: { availableSegmentTerminalServers, segmentTerminalServers } } = this.props;

        return (
            <DualListBoxForm
                allowSelectAll={ true }
                mainList={ DualListStateListTypes.rightList }
                leftListLabel="Доступные терминальные сервера для выбора:"
                rightListLabel="Выбранные терминальные сервера:"
                formName="segmentTerminalServers"
                leftListItems={ this.getDualListItems(availableSegmentTerminalServers) }
                rightListItems={ this.getDualListItems(segmentTerminalServers) }
                isFormDisabled={ false }
                onValueChange={ this.onValueChange }
            />
        );
    }
}