/**
 * Enum максимальной длинны полей фермы сервера
 */
export enum ServerFarmsTabLengthEnumType {
    /**
     * Максимальная длина названия
     */
    Name = 50,

    /**
     * Максимальная длина описания
     */
    Description = 250
}