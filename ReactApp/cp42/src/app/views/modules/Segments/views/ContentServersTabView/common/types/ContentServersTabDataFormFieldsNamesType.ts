import { ContentServersTabDataForm } from 'app/views/modules/Segments/views/ContentServersTabView/common/types';

export type ContentServersTabDataFormFieldsNamesType = keyof ContentServersTabDataForm;