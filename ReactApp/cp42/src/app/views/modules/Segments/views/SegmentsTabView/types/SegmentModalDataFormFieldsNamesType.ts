import { SegmentModalDataForm } from 'app/views/modules/Segments/views/SegmentsTabView/types';

export type SegmentModalDataFormFieldsNamesType = keyof SegmentModalDataForm;