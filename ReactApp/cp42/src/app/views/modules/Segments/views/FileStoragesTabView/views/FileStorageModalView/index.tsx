import { Box } from '@mui/material';
import { PageEnumType } from 'app/common/enums';
import { checkOnValidField, checkValidOfStringField } from 'app/common/functions';
import { IErrorsType } from 'app/common/interfaces';
import { TabFileStoragesProcessId } from 'app/modules/segments/TabFileStorages';
import { CreateCloudFileStorageServerThunkParams } from 'app/modules/segments/TabFileStorages/store/reducer/createCloudFileStorageServerReducer/params';
import { EditCloudFileStorageServerThunkParams } from 'app/modules/segments/TabFileStorages/store/reducer/editCloudFileStorageServerReducer/params';
import { GetCloudFileStorageServerThunkParams } from 'app/modules/segments/TabFileStorages/store/reducer/getCloudFileStorageServerReducer/params';
import { CreateCloudFileStorageServerThunk, EditCloudFileStorageServerThunk, GetCloudFileStorageServerThunk } from 'app/modules/segments/TabFileStorages/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { Dialog } from 'app/views/components/controls/Dialog';
import { TextBoxForm } from 'app/views/components/controls/forms';
import { getDialogButtons } from 'app/views/modules/Segments/common/functions';
import { FileStoragesTabDataForm, FileStoragesTabDataFormFieldsNamesType, FileStoragesTabLengthEnumType } from 'app/views/modules/Segments/views/FileStoragesTabView/types';
import { ShowErrorMessage } from 'app/views/modules/_common/components/ShowErrorMessage';
import { WatchReducerProcessActionState } from 'app/views/modules/_common/components/WatchReducerProcessActionState';
import { CloudFileStorageServerDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabFileStorageApiProxy';
import { IReducerProcessActionInfo } from 'core/redux/interfaces';
import { Component } from 'react';
import { connect } from 'react-redux';

type OwnProps = ReduxFormProps<FileStoragesTabDataForm>;

type FileStorageModalProps = {
    isOpen: boolean;
    fileStorageId?: string;
    onClickToCloseModal: () => void;
};

type FileStorageModalState = {
    errors: IErrorsType;
    pageType: PageEnumType;
};

type StateProps = {
    cloudFileStorageServer: CloudFileStorageServerDataModel;
};

type DispatchProps = {
    dispatchGetCloudFileStorageServerThunk: (args: GetCloudFileStorageServerThunkParams) => void;
    dispatchCreateCloudFileStorageServerThunk: (args: CreateCloudFileStorageServerThunkParams) => void;
    dispatchEditCloudFileStorageServerThunk: (args: EditCloudFileStorageServerThunkParams) => void;
};

type AllProps = FileStorageModalProps & OwnProps & StateProps & DispatchProps & FloatMessageProps;

class FileStorageModalClass extends Component<AllProps, FileStorageModalState> {
    public constructor(props: AllProps) {
        super(props);

        this.state = {
            errors: {},
            pageType: this.props.fileStorageId ? PageEnumType.Edit : PageEnumType.Create
        };

        this.getDialogBody = this.getDialogBody.bind(this);
        this.onValueChange = this.onValueChange.bind(this);
        this.submitChanges = this.submitChanges.bind(this);
        this.createFileStorage = this.createFileStorage.bind(this);
        this.editFileStorage = this.editFileStorage.bind(this);
        this.isFileStorageFormValid = this.isFileStorageFormValid.bind(this);

        this.onProcessActionStateChanged = this.onProcessActionStateChanged.bind(this);
        this.onProcessGetCloudFileStorageServer = this.onProcessGetCloudFileStorageServer.bind(this);
        this.successGetCloudFileStorageServerState = this.successGetCloudFileStorageServerState.bind(this);
        this.errorGetCloudFileStorageServerState = this.errorGetCloudFileStorageServerState.bind(this);
        this.fillFileStorage = this.fillFileStorage.bind(this);

        this.props.reduxForm.setInitializeFormDataAction(this.defaultInitDataReduxForm);
    }

    public componentDidMount() {
        if (this.props.fileStorageId) {
            this.props.dispatchGetCloudFileStorageServerThunk({
                fileStorageId: this.props.fileStorageId,
                force: true
            });
        }
    }

    private onProcessActionStateChanged(processId: string, actionState: IReducerProcessActionInfo) {
        this.onProcessGetCloudFileStorageServer(processId, actionState);
    }

    private onProcessGetCloudFileStorageServer(processId: string, actionState: IReducerProcessActionInfo) {
        if (processId !== TabFileStoragesProcessId.GetCloudFileStorageServer) return;
        this.successGetCloudFileStorageServerState(actionState);
        this.errorGetCloudFileStorageServerState(actionState);
    }

    private onValueChange<TValue, TForm extends string = FileStoragesTabDataFormFieldsNamesType>(formName: TForm, newValue: TValue) {
        this.props.reduxForm.updateReduxFormFields({
            [formName]: newValue
        });

        const { errors } = this.state;

        if (typeof newValue === 'string') {
            switch (formName) {
                case 'connectionAddress':
                    errors[formName] = checkValidOfStringField(newValue, 'Заполните адрес подключения', FileStoragesTabLengthEnumType.ConnectionAddress);
                    break;
                case 'description':
                    errors[formName] = checkValidOfStringField(newValue, undefined, FileStoragesTabLengthEnumType.Description);
                    break;
                case 'name':
                    errors[formName] = checkValidOfStringField(newValue, 'Заполните название', FileStoragesTabLengthEnumType.Name);
                    break;
                case 'dnsName':
                    errors[formName] = checkValidOfStringField(newValue, 'Заполните DNS имя', FileStoragesTabLengthEnumType.DnsName);
                    break;
                default:
                    break;
            }

            this.setState({ errors });
        }
    }

    private getDialogBody() {
        const formFields = this.props.reduxForm.getReduxFormFields(false);
        const { errors } = this.state;

        return (
            <Box display="flex" flexDirection="column" gap="12px">
                <div>
                    <TextBoxForm
                        formName="name"
                        label="Название хранилища"
                        placeholder="Введите название"
                        value={ formFields.name }
                        onValueChange={ this.onValueChange }
                    />
                    <ShowErrorMessage errors={ errors } formName="name" />
                </div>
                <div>
                    <TextBoxForm
                        formName="connectionAddress"
                        label="Адрес подключения"
                        placeholder="Введите адрес"
                        value={ formFields.connectionAddress }
                        onValueChange={ this.onValueChange }
                    />
                    <ShowErrorMessage errors={ errors } formName="connectionAddress" />
                </div>
                <div>
                    <TextBoxForm
                        formName="dnsName"
                        label="DNS имя хранилища"
                        placeholder="Введите DNS имя"
                        value={ formFields.dnsName }
                        onValueChange={ this.onValueChange }
                    />
                    <ShowErrorMessage errors={ errors } formName="dnsName" />
                </div>
                <div>
                    <TextBoxForm
                        type="textarea"
                        formName="description"
                        label="Описание"
                        placeholder="Введите описание"
                        value={ formFields.description }
                        onValueChange={ this.onValueChange }
                    />
                    <ShowErrorMessage errors={ errors } formName="description" />
                </div>
            </Box>
        );
    }

    private successGetCloudFileStorageServerState(actionState: IReducerProcessActionInfo) {
        if (!actionState.isInSuccessState) return;
        this.props.reduxForm.updateReduxFormFields({
            ...this.props.cloudFileStorageServer
        });
    }

    private errorGetCloudFileStorageServerState(actionState: IReducerProcessActionInfo, error?: Error) {
        if (!actionState.isInErrorState) return;
        this.props.floatMessage.show(MessageType.Error, `Ошибка при получении файлового хранилища: ${ error?.message }`);
    }

    private submitChanges() {
        if (!this.isFileStorageFormValid()) return;
        this.createFileStorage();
        this.editFileStorage();
    }

    private createFileStorage() {
        if (this.state.pageType !== PageEnumType.Create) return;
        this.props.dispatchCreateCloudFileStorageServerThunk({
            ...this.fillFileStorage(),
            force: true
        });
    }

    private editFileStorage() {
        if (this.state.pageType !== PageEnumType.Edit) return;
        this.props.dispatchEditCloudFileStorageServerThunk({
            ...this.fillFileStorage(),
            id: this.props.fileStorageId ?? '',
            force: true
        });
    }

    private fillFileStorage(): CloudFileStorageServerDataModel {
        const formFields = this.props.reduxForm.getReduxFormFields(false);
        return {
            connectionAddress: formFields.connectionAddress,
            description: formFields.description,
            dnsName: formFields.dnsName,
            name: formFields.name,
            id: ''
        };
    }

    private defaultInitDataReduxForm(): FileStoragesTabDataForm | undefined {
        return {
            connectionAddress: '',
            description: '',
            dnsName: '',
            name: ''
        };
    }

    private isFileStorageFormValid() {
        const formFields = this.props.reduxForm.getReduxFormFields(false);
        const errors: IErrorsType = {};
        const arrayResults: boolean[] = [];

        arrayResults.push(checkOnValidField(errors, 'description', formFields.description, undefined, FileStoragesTabLengthEnumType.Description));
        arrayResults.push(checkOnValidField(errors, 'connectionAddress', formFields.connectionAddress, 'Заполните адрес подключения', FileStoragesTabLengthEnumType.ConnectionAddress));
        arrayResults.push(checkOnValidField(errors, 'name', formFields.name, 'Заполните название', FileStoragesTabLengthEnumType.Name));
        arrayResults.push(checkOnValidField(errors, 'dnsName', formFields.dnsName, 'Заполните DNS имя', FileStoragesTabLengthEnumType.DnsName));

        if (arrayResults.some(item => !item)) {
            this.props.floatMessage.show(MessageType.Warning, 'Форма к отправке не готова. Пожалуйста, заполните все поля.');
            this.setState({ errors });
            return false;
        }

        return true;
    }

    public render() {
        const { isOpen } = this.props;
        const isEditTypePage = this.state.pageType === PageEnumType.Edit;
        const titleText = isEditTypePage ? 'Редактирование' : 'Создание';

        return (
            <>
                <Dialog
                    disableBackdropClick={ true }
                    title={ `${ titleText } файлового хранилища` }
                    isOpen={ isOpen }
                    dialogWidth="sm"
                    isTitleSmall={ true }
                    titleTextAlign="left"
                    titleFontSize={ 14 }
                    dialogVerticalAlign="center"
                    onCancelClick={ this.props.onClickToCloseModal }
                    buttons={ getDialogButtons({
                        pageType: this.state.pageType,
                        onClickToCloseModal: this.props.onClickToCloseModal,
                        submitChanges: this.submitChanges
                    }) }
                >
                    {
                        this.getDialogBody()
                    }
                </Dialog>
                <WatchReducerProcessActionState
                    watch={ [{
                        reducerStateName: 'TabFileStoragesState',
                        processIds: [
                            TabFileStoragesProcessId.GetCloudFileStorageServer
                        ]
                    }] }
                    onProcessActionStateChanged={ this.onProcessActionStateChanged }
                />
            </>
        );
    }
}

const FileStorageModalConnected = connect<StateProps, DispatchProps, OwnProps, AppReduxStoreState>(
    state => {
        const tabFileStoragesState = state.TabFileStoragesState;
        const { cloudFileStorageServer } = tabFileStoragesState;

        return {
            cloudFileStorageServer
        };
    },
    {
        dispatchGetCloudFileStorageServerThunk: GetCloudFileStorageServerThunk.invoke,
        dispatchCreateCloudFileStorageServerThunk: CreateCloudFileStorageServerThunk.invoke,
        dispatchEditCloudFileStorageServerThunk: EditCloudFileStorageServerThunk.invoke
    }
)(FileStorageModalClass);

export const FileStorageModalView = withReduxForm(withFloatMessages(FileStorageModalConnected),
    {
        reduxFormName: 'File_Storage_Modal_Form',
        resetOnUnmount: true
    });