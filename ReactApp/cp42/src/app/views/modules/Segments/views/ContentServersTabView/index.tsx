import ErrorOutlineIcon from '@mui/icons-material/ErrorOutline';
import { Box, Chip } from '@mui/material';
import { SortingKind } from 'app/common/enums';
import { TabContentServersProcessId } from 'app/modules/segments/TabContentServers';
import { DeleteCloudContentServerThunkParams } from 'app/modules/segments/TabContentServers/store/reducer/deleteCloudContentServerReducer/params';
import { GetCloudContentServersThunkParams } from 'app/modules/segments/TabContentServers/store/reducer/getCloudContentServersReducer/params';
import { DeleteCloudContentServerThunk, GetCloudContentServersThunk } from 'app/modules/segments/TabContentServers/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { SuccessButton, TextLinkButton } from 'app/views/components/controls/Button';
import { Dialog } from 'app/views/components/controls/Dialog';
import { TextOut } from 'app/views/components/TextOut';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { WatchReducerProcessActionState } from 'app/views/modules/_common/components/WatchReducerProcessActionState';
import { CommonSearchSegmentView } from 'app/views/modules/Segments/common/components';
import { CommonSegmentEnumType } from 'app/views/modules/Segments/types';
import { ContentServerModalView } from 'app/views/modules/Segments/views/ContentServersTabView/views/ContentServerModalView';
import { CloudContentServerResponseDto } from 'app/web/api/SegmentsProxy/TabContentServers/response-dto';
import { SelectDataMetadataResponseDto, SortingDataModel } from 'app/web/common';
import { SelectDataCommonDataModel } from 'app/web/common/data-models';
import { IReducerProcessActionInfo } from 'core/redux/interfaces';
import { Component, createRef } from 'react';
import { connect } from 'react-redux';
import commonCss from '../../common/styles.module.css';

type ContentServersTabState = {
    /**
     * Поле обозначающая что первая подгрузка данных была сделана
     */
    isFirstTimeLoaded: boolean;

    /**
     * Поле для открытия модального окошка
     */
    isOpen: boolean;

    /**
     * ID сайта публикации
     */
    contentServerId?: string;
    /**
     * Состояние модалнього окна на удаление сервера публикации
     */
    deleteIsOpen: boolean;
    orderBy?: string;
};

type ContentServersTabProps = {
    /**
     * Поле для подгрузки данных
     */
    isCanLoad: boolean;
};

type StateProps = {
    /**
     * Список данных списка сайта публикации
     */
    contentServers: SelectDataMetadataResponseDto<CloudContentServerResponseDto>;

    /**
     * Если true, значит список сайта публикации
     */
    hasContentServersReceived: boolean;
};

type DispatchProps = {
    dispatchGetCloudContentServersThunk: (args: GetCloudContentServersThunkParams) => void;
    dispatchDeleteCloudContentServerThunk: (args: DeleteCloudContentServerThunkParams) => void;
};

type AllProps = ContentServersTabProps & StateProps & DispatchProps & FloatMessageProps;

class ContentServersTabClass extends Component<AllProps, ContentServersTabState> {
    private _commonTableRef = createRef<CommonTableWithFilter<CloudContentServerResponseDto, undefined>>();

    private _searchContentServersRef = createRef<CommonSearchSegmentView>();

    public constructor(props: AllProps) {
        super(props);

        this.state = {
            isOpen: false,
            isFirstTimeLoaded: false,
            deleteIsOpen: false,
            orderBy: ''
        };

        this.onClickToOpenModal = this.onClickToOpenModal.bind(this);
        this.onClickToCloseModal = this.onClickToCloseModal.bind(this);
        this.onClickToSearch = this.onClickToSearch.bind(this);
        this.onDataSelect = this.onDataSelect.bind(this);
        this.getFormatNameContentServer = this.getFormatNameContentServer.bind(this);
        this.notificationInSuccess = this.notificationInSuccess.bind(this);
        this.notificationInError = this.notificationInError.bind(this);
        this.openModalForDeleteContentServer = this.openModalForDeleteContentServer.bind(this);
        this.getButtonsForActions = this.getButtonsForActions.bind(this);
        this.getFormatGroupByAccount = this.getFormatGroupByAccount.bind(this);

        this.onProcessActionStateChanged = this.onProcessActionStateChanged.bind(this);
        this.onProcessCreateContentServer = this.onProcessCreateContentServer.bind(this);
        this.onProcessEditContentServer = this.onProcessEditContentServer.bind(this);
        this.onProcessDeleteContentServer = this.onProcessDeleteContentServer.bind(this);
        this.onClickDeleteContentServer = this.onClickDeleteContentServer.bind(this);
        this.closeModalForDeleteContentServer = this.closeModalForDeleteContentServer.bind(this);
        this.getSortQuery = this.getSortQuery.bind(this);
    }

    public componentDidUpdate() {
        const { isCanLoad, hasContentServersReceived } = this.props;
        const { isFirstTimeLoaded } = this.state;
        if (isCanLoad && !isFirstTimeLoaded && !hasContentServersReceived && this._commonTableRef.current) {
            this._commonTableRef.current.ReSelectData();
            this.setState({
                isFirstTimeLoaded: true
            });
        }
    }

    private onProcessActionStateChanged(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        this.onProcessCreateContentServer(processId, actionState, error);
        this.onProcessEditContentServer(processId, actionState, error);
        this.onProcessDeleteContentServer(processId, actionState, error);
    }

    private onProcessCreateContentServer(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId !== TabContentServersProcessId.CreateContentServer) return;
        this.notificationInSuccess(actionState, 'Сайт публикации успешно создан');
        this.notificationInError(actionState, error?.message);
    }

    private onProcessEditContentServer(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId !== TabContentServersProcessId.EditContentServer) return;
        this.notificationInSuccess(actionState, 'Сайт публикации успешно сохранен');
        this.notificationInError(actionState, error?.message);
    }

    private onProcessDeleteContentServer(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId !== TabContentServersProcessId.DeleteContentServer) return;
        this.notificationInSuccess(actionState, 'Сайт публикации успешно удален');
        this.notificationInError(actionState, error?.message);
    }

    private onClickDeleteContentServer() {
        if (this.state.contentServerId) {
            this.props.dispatchDeleteCloudContentServerThunk({
                contentServerId: this.state.contentServerId,
                force: true
            });
            this.setState({
                deleteIsOpen: false
            });
        }
    }

    private onClickToOpenModal(contentServerId?: string) {
        this.setState({
            isOpen: true,
            contentServerId
        });
    }

    private onClickToCloseModal() {
        this.setState({
            isOpen: false
        });
    }

    private onClickToSearch() {
        const {
            description,
            name,
            connectionAddress
        } = this._searchContentServersRef.current?.state!;

        this.props.dispatchGetCloudContentServersThunk({
            filter: {
                publishSiteName: connectionAddress,
                description,
                name,
            },
            pageNumber: 1,
            orderBy: this.state.orderBy,
            pageSize: CommonSegmentEnumType.DefaultPageSize,
            force: true
        });
    }

    private onDataSelect(args: SelectDataCommonDataModel<undefined>) {
        const {
            description,
            name,
            connectionAddress
        } = this._searchContentServersRef.current?.state!;

        const orderBy = args.sortingData ? this.getSortQuery(args.sortingData) : 'name.asc';

        this.setState({
            orderBy
        });

        this.props.dispatchGetCloudContentServersThunk({
            filter: {
                description,
                name,
                publishSiteName: connectionAddress,
            },
            pageNumber: args.pageNumber,
            orderBy,
            pageSize: CommonSegmentEnumType.DefaultPageSize,
            force: true
        });
    }

    private getSortQuery(args: SortingDataModel) {
        return `${ args.fieldName }.${ args.sortKind ? 'desc' : 'asc' }`;
    }

    private getFormatGroupByAccount(isGroup: boolean) {
        return (
            <Chip
                color={ isGroup ? 'success' : 'primary' }
                label={ isGroup ? 'Сгруппирован' : 'Не сгруппирован' }
            />
        );
    }

    private getButtonsForActions(Id: string) {
        return (
            <TextLinkButton
                className={ commonCss['delete-link'] }
                onClick={ () => { this.openModalForDeleteContentServer(Id); } }
            >
                <i className="fa fa-trash" />
            </TextLinkButton>
        );
    }

    private getFormatNameContentServer(name: string, data: CloudContentServerResponseDto) {
        return (
            <TextLinkButton onClick={ () => { this.onClickToOpenModal(data.id); } }>
                <TextOut fontSize={ 13 } className="text-link">
                    { name }
                </TextOut>
            </TextLinkButton>
        );
    }

    private notificationInError(actionState: IReducerProcessActionInfo, message?: string) {
        if (!actionState.isInErrorState) return;
        this.props.floatMessage.show(MessageType.Error, message);
    }

    private notificationInSuccess(actionState: IReducerProcessActionInfo, message: string) {
        if (!actionState.isInSuccessState) return;
        this.props.floatMessage.show(MessageType.Success, message);
        this._commonTableRef.current?.ReSelectData();
        this.onClickToCloseModal();
    }

    private closeModalForDeleteContentServer() {
        this.setState({
            deleteIsOpen: false
        });
    }

    private openModalForDeleteContentServer(contentServerId: string) {
        this.setState({
            contentServerId,
            deleteIsOpen: true
        });
    }

    public render() {
        const { contentServers: { metadata, records } } = this.props;
        return (
            <>
                <CommonSearchSegmentView
                    ref={ this._searchContentServersRef }
                    onClickToSearch={ this.onClickToSearch }
                    connectionAddressLabel="Адрес публикации"
                />
                <CommonTableWithFilter
                    className={ commonCss['custom-table'] }
                    ref={ this._commonTableRef }
                    isResponsiveTable={ true }
                    isBorderStyled={ true }
                    uniqueContextProviderStateId="TabContentServersContextProviderStateId"
                    tableProps={ {
                        dataset: records,
                        keyFieldName: 'id',
                        sorting: {
                            sortFieldName: 'name',
                            sortKind: SortingKind.Asc
                        },
                        fieldsView: {
                            name: {
                                caption: 'Название',
                                fieldWidth: 'auto',
                                fieldContentNoWrap: false,
                                isSortable: true,
                                format: this.getFormatNameContentServer
                            },
                            description: {
                                caption: 'Описание',
                                fieldWidth: 'auto',
                                fieldContentNoWrap: false,
                                isSortable: true
                            },
                            publishSiteName: {
                                caption: 'Адрес публикации',
                                fieldWidth: 'auto',
                                fieldContentNoWrap: false,
                                isSortable: true
                            },
                            groupByAccount: {
                                caption: 'Сгруппирован по аккаунту',
                                fieldWidth: 'auto',
                                fieldContentNoWrap: false,
                                isSortable: true,
                                format: this.getFormatGroupByAccount
                            },
                            id: {
                                caption: '',
                                fieldWidth: 'auto',
                                fieldContentNoWrap: false,
                                format: this.getButtonsForActions
                            }
                        },
                        pagination: {
                            currentPage: metadata.pageNumber,
                            pageSize: metadata.pageSize,
                            recordsCount: metadata.totalItemCount,
                            totalPages: metadata.pageCount
                        },
                    } }
                    onDataSelect={ this.onDataSelect }
                />
                <SuccessButton
                    onClick={ () => { this.onClickToOpenModal(); } }
                >
                    <i className="fa fa-plus-circle mr-1" />
                    Создать сайт публикации
                </SuccessButton>
                {
                    this.state.isOpen
                        ? (
                            <ContentServerModalView
                                isOpen={ this.state.isOpen }
                                contentServerId={ this.state.contentServerId }
                                onClickToCloseModal={ this.onClickToCloseModal }
                            />
                        )
                        : null
                }
                <WatchReducerProcessActionState
                    watch={ [{
                        reducerStateName: 'TabContentServersState',
                        processIds: [
                            TabContentServersProcessId.CreateContentServer,
                            TabContentServersProcessId.EditContentServer,
                            TabContentServersProcessId.DeleteContentServer,
                        ]
                    }] }
                    onProcessActionStateChanged={ this.onProcessActionStateChanged }
                />
                <Dialog
                    isOpen={ this.state.deleteIsOpen }
                    dialogWidth="sm"
                    dialogVerticalAlign="center"
                    onCancelClick={ this.closeModalForDeleteContentServer }
                    buttons={
                        [
                            {
                                content: 'Нет',
                                kind: 'primary',
                                variant: 'text',
                                onClick: this.closeModalForDeleteContentServer
                            },
                            {
                                content: 'Да',
                                kind: 'error',
                                variant: 'contained',
                                onClick: this.onClickDeleteContentServer
                            }
                        ]
                    }
                >
                    <Box display="flex" flexDirection="column" alignItems="center" justifyContent="center">
                        <ErrorOutlineIcon color="warning" sx={ { width: '5em', height: '5em' } } />
                        Вы действительно хотите удалить сайт публикации?
                    </Box>
                </Dialog>
            </>
        );
    }
}

const ContentServersTabConnected = connect<StateProps, DispatchProps, NonNullable<unknown>, AppReduxStoreState>(
    state => {
        const tabContentServersState = state.TabContentServersState;
        const { contentServers } = tabContentServersState;
        const { hasContentServersReceived } = tabContentServersState.hasSuccessFor;

        return {
            contentServers,
            hasContentServersReceived
        };
    },
    {
        dispatchGetCloudContentServersThunk: GetCloudContentServersThunk.invoke,
        dispatchDeleteCloudContentServerThunk: DeleteCloudContentServerThunk.invoke
    }
)(ContentServersTabClass);

export const ContentServersTabView = withFloatMessages(ContentServersTabConnected);