import ErrorOutlineIcon from '@mui/icons-material/ErrorOutline';
import { Box } from '@mui/material';
import { SortingKind } from 'app/common/enums';
import { TabServerFarmsProcessId } from 'app/modules/segments/TabServerFarms';
import { DeleteServerFarmThunkParams } from 'app/modules/segments/TabServerFarms/store/reducer/deleteServerFarmReducer/params';
import { GetServerFarmsThunkParams } from 'app/modules/segments/TabServerFarms/store/reducer/getServerFarmsReducer/params';
import { DeleteServerFarmThunk, GetServerFarmsThunk } from 'app/modules/segments/TabServerFarms/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { SuccessButton, TextLinkButton } from 'app/views/components/controls/Button';
import { Dialog } from 'app/views/components/controls/Dialog';
import { TextOut } from 'app/views/components/TextOut';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { WatchReducerProcessActionState } from 'app/views/modules/_common/components/WatchReducerProcessActionState';
import { CommonSearchSegmentView } from 'app/views/modules/Segments/common/components';
import { CommonSegmentEnumType } from 'app/views/modules/Segments/types';
import { ServerFarmModalView } from 'app/views/modules/Segments/views/ServerFarmsTabView/views/ServerFarmModalView';
import { SelectDataMetadataResponseDto, SortingDataModel } from 'app/web/common';
import { SelectDataCommonDataModel } from 'app/web/common/data-models';
import { ServerFarmDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabServerFarmsApiProxy';
import { IReducerProcessActionInfo } from 'core/redux/interfaces';
import { Component, createRef } from 'react';
import { connect } from 'react-redux';
import commonCss from '../../common/styles.module.css';

type ServerFarmsTabState = {
    isFirstTimeLoaded: boolean;
    isOpen: boolean;
    serverFarmId?: string;
    deleteIsOpen: boolean;
    orderBy?: string;
};

type ServerFarmsTabProps = {
    isCanLoad: boolean;
};

type StateProps = {
    serverFarms: SelectDataMetadataResponseDto<ServerFarmDataModel>;
    hasServerFarmsReceived: boolean;
};

type DispatchProps = {
    dispatchGetServerFarmsThunk: (args: GetServerFarmsThunkParams) => void;
    dispatchDeleteServerFarmThunk: (args: DeleteServerFarmThunkParams) => void;
};

type AllProps = ServerFarmsTabProps & StateProps & DispatchProps & FloatMessageProps;

class ServerFarmsTabClass extends Component<AllProps, ServerFarmsTabState> {
    private _commonTableRef = createRef<CommonTableWithFilter<ServerFarmDataModel, undefined>>();

    private _searchServerFarmsRef = createRef<CommonSearchSegmentView>();

    public constructor(props: AllProps) {
        super(props);

        this.state = {
            isOpen: false,
            isFirstTimeLoaded: false,
            deleteIsOpen: false,
            orderBy: ''
        };

        this.onClickToOpenModal = this.onClickToOpenModal.bind(this);
        this.onClickToCloseModal = this.onClickToCloseModal.bind(this);
        this.onClickToSearch = this.onClickToSearch.bind(this);
        this.onDataSelect = this.onDataSelect.bind(this);
        this.getFormatServerFarm = this.getFormatServerFarm.bind(this);
        this.notificationInSuccess = this.notificationInSuccess.bind(this);
        this.notificationInError = this.notificationInError.bind(this);
        this.notificationCreateInError = this.notificationCreateInError.bind(this);
        this.openModalForDeleteServerFarm = this.openModalForDeleteServerFarm.bind(this);
        this.getButtonsForActions = this.getButtonsForActions.bind(this);

        this.onProcessActionStateChanged = this.onProcessActionStateChanged.bind(this);
        this.onProcessCreateServerFarm = this.onProcessCreateServerFarm.bind(this);
        this.onProcessEditServerFarm = this.onProcessEditServerFarm.bind(this);
        this.onProcessDeleteServerFarm = this.onProcessDeleteServerFarm.bind(this);
        this.onClickDeleteServerFarm = this.onClickDeleteServerFarm.bind(this);
        this.closeModalForDeleteServerFarm = this.closeModalForDeleteServerFarm.bind(this);
    }

    public componentDidUpdate() {
        const { isCanLoad, hasServerFarmsReceived } = this.props;
        const { isFirstTimeLoaded } = this.state;

        if (isCanLoad && !isFirstTimeLoaded && !hasServerFarmsReceived && this._commonTableRef.current) {
            this._commonTableRef.current.ReSelectData();
            this.setState({
                isFirstTimeLoaded: true
            });
        }
    }

    private onClickDeleteServerFarm() {
        if (this.state.serverFarmId) {
            this.props.dispatchDeleteServerFarmThunk({
                serverFarmId: this.state.serverFarmId,
                force: true
            });
            this.setState({
                deleteIsOpen: false
            });
        }
    }

    private onProcessActionStateChanged(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        this.onProcessCreateServerFarm(processId, actionState, error);
        this.onProcessEditServerFarm(processId, actionState, error);
        this.onProcessDeleteServerFarm(processId, actionState, error);
    }

    private onProcessCreateServerFarm(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId !== TabServerFarmsProcessId.CreateServerFarm) return;
        this.notificationInSuccess(actionState, 'Ферма сервера успешно создана');
        this.notificationCreateInError(actionState, error?.message);
    }

    private onProcessEditServerFarm(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId !== TabServerFarmsProcessId.EditServerFarm) return;
        this.notificationInSuccess(actionState, 'Ферма сервера успешно сохранена');
        this.notificationInError(actionState, error?.message);
    }

    private onProcessDeleteServerFarm(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId !== TabServerFarmsProcessId.DeleteServerFarm) return;
        this.notificationInSuccess(actionState, 'Ферма сервера успешно удалена');
        this.notificationInError(actionState, error?.message);
    }

    private onClickToOpenModal(serverFarmId?: string) {
        this.setState({
            isOpen: true,
            serverFarmId
        });
    }

    private onClickToCloseModal() {
        this.setState({
            isOpen: false
        });
    }

    private onClickToSearch() {
        const {
            name,
            description
        } = this._searchServerFarmsRef.current?.state!;

        this.props.dispatchGetServerFarmsThunk({
            filter: {
                description,
                name,
            },
            pageNumber: 1,
            orderBy: this.state.orderBy,
            pageSize: CommonSegmentEnumType.DefaultPageSize,
            force: true
        });
    }

    private onDataSelect(args: SelectDataCommonDataModel<undefined>) {
        const {
            name,
            description
        } = this._searchServerFarmsRef.current?.state!;

        const orderBy = args.sortingData ? this.getSortQuery(args.sortingData) : 'name.asc';

        this.setState({
            orderBy
        });

        this.props.dispatchGetServerFarmsThunk({
            filter: {
                description,
                name,
            },
            orderBy,
            pageNumber: args.pageNumber,
            pageSize: CommonSegmentEnumType.DefaultPageSize,
            force: true
        });
    }

    private getSortQuery(args: SortingDataModel) {
        return `${ args.fieldName }.${ args.sortKind ? 'desc' : 'asc' }`;
    }

    private getButtonsForActions(serverFarmId: string) {
        return (
            <TextLinkButton
                className={ commonCss['delete-link'] }
                onClick={ () => { this.openModalForDeleteServerFarm(serverFarmId); } }
            >
                <i className="fa fa-trash" />
            </TextLinkButton>
        );
    }

    private getFormatServerFarm(name: string, data: ServerFarmDataModel) {
        return (
            <TextLinkButton onClick={ () => { this.onClickToOpenModal(data.id); } }>
                <TextOut fontSize={ 13 } className={ `text-link ${ commonCss['button-text'] }` }>
                    { name }
                </TextOut>
            </TextLinkButton>
        );
    }

    private notificationCreateInError(actionState: IReducerProcessActionInfo, message?: string) {
        if (!actionState.isInErrorState) return;
        this.props.floatMessage.show(MessageType.Warning, message);
    }

    private notificationInError(actionState: IReducerProcessActionInfo, message?: string) {
        if (!actionState.isInErrorState) return;
        this.props.floatMessage.show(MessageType.Error, message);
    }

    private notificationInSuccess(actionState: IReducerProcessActionInfo, message: string) {
        if (!actionState.isInSuccessState) return;
        this.props.floatMessage.show(MessageType.Success, message);
        this._commonTableRef.current?.ReSelectData();
        this.onClickToCloseModal();
    }

    private closeModalForDeleteServerFarm() {
        this.setState({
            deleteIsOpen: false
        });
    }

    private openModalForDeleteServerFarm(serverFarmId: string) {
        this.setState({
            serverFarmId,
            deleteIsOpen: true
        });
    }

    public render() {
        const { serverFarms: { metadata, records } } = this.props;

        return (
            <>
                <CommonSearchSegmentView
                    ref={ this._searchServerFarmsRef }
                    onClickToSearch={ this.onClickToSearch }
                    isConnectionAddressVisible={ false }
                />
                <CommonTableWithFilter
                    className={ commonCss['custom-table'] }
                    ref={ this._commonTableRef }
                    isResponsiveTable={ true }
                    isBorderStyled={ true }
                    uniqueContextProviderStateId="TabServerFarmsContextProviderStateId"
                    tableProps={ {
                        dataset: records,
                        keyFieldName: 'id',
                        sorting: {
                            sortFieldName: 'name',
                            sortKind: SortingKind.Asc
                        },
                        fieldsView: {
                            name: {
                                caption: 'Название',
                                fieldWidth: 'auto',
                                fieldContentNoWrap: false,
                                isSortable: true,
                                format: this.getFormatServerFarm
                            },
                            description: {
                                caption: 'Описание',
                                fieldWidth: 'auto',
                                fieldContentNoWrap: false,
                                isSortable: true,
                            },
                            id: {
                                caption: '',
                                fieldWidth: 'auto',
                                fieldContentNoWrap: false,
                                format: this.getButtonsForActions
                            }
                        },
                        pagination: {
                            currentPage: metadata.pageNumber,
                            pageSize: metadata.pageSize,
                            recordsCount: metadata.totalItemCount,
                            totalPages: metadata.pageCount
                        },
                    } }
                    onDataSelect={ this.onDataSelect }
                />
                <SuccessButton
                    onClick={ () => { this.onClickToOpenModal(); } }
                >
                    <i className="fa fa-plus-circle mr-1" />
                    Создать ферму сервера
                </SuccessButton>
                {
                    this.state.isOpen
                        ? (
                            <ServerFarmModalView
                                isOpen={ this.state.isOpen }
                                serverFarmId={ this.state.serverFarmId }
                                onClickToCloseModal={ this.onClickToCloseModal }
                            />
                        )
                        : null
                }
                <WatchReducerProcessActionState
                    watch={ [{
                        reducerStateName: 'TabServerFarmState',
                        processIds: [
                            TabServerFarmsProcessId.CreateServerFarm,
                            TabServerFarmsProcessId.EditServerFarm,
                            TabServerFarmsProcessId.DeleteServerFarm
                        ]
                    }] }
                    onProcessActionStateChanged={ this.onProcessActionStateChanged }
                />
                <Dialog
                    isOpen={ this.state.deleteIsOpen }
                    dialogWidth="sm"
                    dialogVerticalAlign="center"
                    onCancelClick={ this.closeModalForDeleteServerFarm }
                    buttons={
                        [
                            {
                                content: 'Нет',
                                kind: 'primary',
                                variant: 'text',
                                onClick: this.closeModalForDeleteServerFarm
                            },
                            {
                                content: 'Да',
                                kind: 'error',
                                variant: 'contained',
                                onClick: this.onClickDeleteServerFarm
                            }
                        ]
                    }
                >
                    <Box display="flex" flexDirection="column" alignItems="center" justifyContent="center">
                        <ErrorOutlineIcon color="warning" sx={ { width: '5em', height: '5em' } } />
                        Вы действительно хотите удалить ферму сервера?
                    </Box>
                </Dialog>
            </>
        );
    }
}

const ServerFarmsTabConnected = connect<StateProps, DispatchProps, NonNullable<unknown>, AppReduxStoreState>(
    state => {
        const tabServerFarmState = state.TabServerFarmState;
        const { serverFarms } = tabServerFarmState;
        const { hasServerFarmsReceived } = tabServerFarmState.hasSuccessFor;

        return {
            serverFarms,
            hasServerFarmsReceived
        };
    },
    {
        dispatchGetServerFarmsThunk: GetServerFarmsThunk.invoke,
        dispatchDeleteServerFarmThunk: DeleteServerFarmThunk.invoke
    }
)(ServerFarmsTabClass);

export const ServerFarmsTabView = withFloatMessages(ServerFarmsTabConnected);