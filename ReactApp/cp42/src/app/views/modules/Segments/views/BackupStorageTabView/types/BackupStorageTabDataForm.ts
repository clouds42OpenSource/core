/**
 * Модель Redux формы для хранилища бекапов
 */
export type BackupStorageTabDataForm = {
    /**
     * Адрес подключения хранилища
     */
    connectionAddress: string;

    /**
     * Описание хранилища
     */
    description: string;

    /**
     * Название хранилища
     */
    name: string;
};