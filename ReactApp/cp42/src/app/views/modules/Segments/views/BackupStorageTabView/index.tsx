import ErrorOutlineIcon from '@mui/icons-material/ErrorOutline';
import { Box } from '@mui/material';
import { SortingKind } from 'app/common/enums';
import { TabBackupStorageProcessId } from 'app/modules/segments/TabBackupStorage';
import { DeleteCloudBackupStorageThunkParams } from 'app/modules/segments/TabBackupStorage/store/reducer/deleteCloudBackupStorageReducer/params';
import { GetCloudBackupStoragesThunkParams } from 'app/modules/segments/TabBackupStorage/store/reducer/getCloudBackupStoragesReducer/params';
import { DeleteCloudBackupStorageThunk, GetCloudBackupStoragesThunk } from 'app/modules/segments/TabBackupStorage/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { SuccessButton, TextLinkButton } from 'app/views/components/controls/Button';
import { Dialog } from 'app/views/components/controls/Dialog';
import { TextOut } from 'app/views/components/TextOut';
import { CommonTableWithFilter } from 'app/views/modules/_common/components/CommonTableWithFilter';
import { WatchReducerProcessActionState } from 'app/views/modules/_common/components/WatchReducerProcessActionState';
import { CommonSearchSegmentView } from 'app/views/modules/Segments/common/components';
import { CommonSegmentEnumType } from 'app/views/modules/Segments/types';
import { BackupStorageModalView } from 'app/views/modules/Segments/views/BackupStorageTabView/views/BackupStorageModalView';
import { SelectDataMetadataResponseDto, SortingDataModel } from 'app/web/common';
import { SelectDataCommonDataModel } from 'app/web/common/data-models';
import { CloudBackupStorageDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabBackupStorageApiProxy';
import { IReducerProcessActionInfo } from 'core/redux/interfaces';
import { Component, createRef } from 'react';
import { connect } from 'react-redux';
import commonCss from '../../common/styles.module.css';

type BackupStorageTabState = {
    isFirstTimeLoaded: boolean;
    isOpen: boolean;
    backupStorageId?: string;
    deleteIsOpen: boolean;
    orderBy?: string;
};

type BackupStorageTabProps = {
    isCanLoad: boolean;
};

type StateProps = {
    cloudBackupStorages: SelectDataMetadataResponseDto<CloudBackupStorageDataModel>;
    hasCloudBackupStoragesReceived: boolean;
};

type DispatchProps = {
    dispatchGetCloudBackupStoragesThunk: (args: GetCloudBackupStoragesThunkParams) => void;
    dispatchDeleteCloudBackupStorageThunk: (args: DeleteCloudBackupStorageThunkParams) => void;
};

type AllProps = BackupStorageTabProps & StateProps & DispatchProps & FloatMessageProps;

class BackupStorageTabClass extends Component<AllProps, BackupStorageTabState> {
    private _commonTableRef = createRef<CommonTableWithFilter<CloudBackupStorageDataModel, undefined>>();

    private _searchCloudBackupStorageRef = createRef<CommonSearchSegmentView>();

    public constructor(props: AllProps) {
        super(props);

        this.state = {
            isOpen: false,
            isFirstTimeLoaded: false,
            deleteIsOpen: false,
            orderBy: ''
        };

        this.onClickToOpenModal = this.onClickToOpenModal.bind(this);
        this.onClickToCloseModal = this.onClickToCloseModal.bind(this);
        this.onClickToSearch = this.onClickToSearch.bind(this);
        this.onDataSelect = this.onDataSelect.bind(this);
        this.getFormatNameBackupStorage = this.getFormatNameBackupStorage.bind(this);
        this.notificationInSuccess = this.notificationInSuccess.bind(this);
        this.notificationInError = this.notificationInError.bind(this);
        this.openModalForDeleteBackupStorage = this.openModalForDeleteBackupStorage.bind(this);
        this.getButtonsForActions = this.getButtonsForActions.bind(this);

        this.onProcessActionStateChanged = this.onProcessActionStateChanged.bind(this);
        this.onProcessCreateBackupStorage = this.onProcessCreateBackupStorage.bind(this);
        this.onProcessEditBackupStorage = this.onProcessEditBackupStorage.bind(this);
        this.onProcessDeleteBackupStorage = this.onProcessDeleteBackupStorage.bind(this);
        this.onClickDeleteBackupStorage = this.onClickDeleteBackupStorage.bind(this);
        this.closeModalForDeleteBackupStorage = this.closeModalForDeleteBackupStorage.bind(this);
    }

    public componentDidUpdate() {
        const { isCanLoad, hasCloudBackupStoragesReceived } = this.props;
        const { isFirstTimeLoaded } = this.state;

        if (isCanLoad && !isFirstTimeLoaded && !hasCloudBackupStoragesReceived && this._commonTableRef.current) {
            this._commonTableRef.current.ReSelectData();
            this.setState({
                isFirstTimeLoaded: true
            });
        }
    }

    private onClickDeleteBackupStorage() {
        if (this.state.backupStorageId) {
            this.props.dispatchDeleteCloudBackupStorageThunk({
                backupStorageId: this.state.backupStorageId,
                force: true
            });
        }
        this.setState({
            deleteIsOpen: false
        });
    }

    private onProcessActionStateChanged(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        this.onProcessCreateBackupStorage(processId, actionState, error);
        this.onProcessEditBackupStorage(processId, actionState, error);
        this.onProcessDeleteBackupStorage(processId, actionState, error);
    }

    private onProcessCreateBackupStorage(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId !== TabBackupStorageProcessId.CreateCloudBackupStorage) return;
        this.notificationInSuccess(actionState, 'Хранилище бекапов успешно создано');
        this.notificationInError(actionState, error?.message);
    }

    private onProcessEditBackupStorage(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId !== TabBackupStorageProcessId.EditCloudBackupStorage) return;
        this.notificationInSuccess(actionState, 'Хранилище бекапов успешно сохранено');
        this.notificationInError(actionState, error?.message);
    }

    private onProcessDeleteBackupStorage(processId: string, actionState: IReducerProcessActionInfo, error?: Error) {
        if (processId !== TabBackupStorageProcessId.DeleteCloudBackupStorage) return;
        this.notificationInSuccess(actionState, 'Хранилище бекапов успешно удалено');
        this.notificationInError(actionState, error?.message);
    }

    private onClickToOpenModal(backupStorageId?: string) {
        this.setState({
            isOpen: true,
            backupStorageId
        });
    }

    private onClickToCloseModal() {
        this.setState({
            isOpen: false
        });
    }

    private onClickToSearch() {
        const {
            description,
            connectionAddress,
            name
        } = this._searchCloudBackupStorageRef.current?.state!;

        this.props.dispatchGetCloudBackupStoragesThunk({
            filter: {
                connectionAddress,
                description,
                name,
            },
            pageNumber: 1,
            orderBy: this.state.orderBy,
            pageSize: CommonSegmentEnumType.DefaultPageSize,
            force: true
        });
    }

    private onDataSelect(args: SelectDataCommonDataModel<undefined>) {
        const {
            description,
            connectionAddress,
            name
        } = this._searchCloudBackupStorageRef.current?.state!;

        const orderBy = args.sortingData ? this.getSortQuery(args.sortingData) : 'name.asc';

        this.setState({
            orderBy
        });

        this.props.dispatchGetCloudBackupStoragesThunk({
            filter: {
                description,
                connectionAddress,
                name,
            },
            pageNumber: args.pageNumber,
            orderBy,
            pageSize: CommonSegmentEnumType.DefaultPageSize,
            force: true
        });
    }

    private getSortQuery(args: SortingDataModel) {
        return `${ args.fieldName }.${ args.sortKind ? 'desc' : 'asc' }`;
    }

    private getButtonsForActions(Id: string) {
        return (
            <TextLinkButton
                className={ commonCss['delete-link'] }
                onClick={ () => { this.openModalForDeleteBackupStorage(Id); } }
            >
                <i className="fa fa-trash" />
            </TextLinkButton>
        );
    }

    private getFormatNameBackupStorage(name: string, data: CloudBackupStorageDataModel) {
        return (
            <TextLinkButton onClick={ () => { this.onClickToOpenModal(data.id); } }>
                <TextOut fontSize={ 13 } className="text-link">
                    { name }
                </TextOut>
            </TextLinkButton>
        );
    }

    private notificationInError(actionState: IReducerProcessActionInfo, message?: string) {
        if (!actionState.isInErrorState) return;
        this.props.floatMessage.show(MessageType.Error, message);
    }

    private notificationInSuccess(actionState: IReducerProcessActionInfo, message: string) {
        if (!actionState.isInSuccessState) return;
        this.props.floatMessage.show(MessageType.Success, message);
        this._commonTableRef.current?.ReSelectData();
        this.onClickToCloseModal();
    }

    private closeModalForDeleteBackupStorage() {
        this.setState({
            deleteIsOpen: false
        });
    }

    private openModalForDeleteBackupStorage(backupStorageId: string) {
        this.setState({
            backupStorageId,
            deleteIsOpen: true
        });
    }

    public render() {
        const { cloudBackupStorages: { metadata, records } } = this.props;

        return (
            <>
                <CommonSearchSegmentView
                    ref={ this._searchCloudBackupStorageRef }
                    onClickToSearch={ this.onClickToSearch }
                />
                <CommonTableWithFilter
                    className={ commonCss['custom-table'] }
                    ref={ this._commonTableRef }
                    isResponsiveTable={ true }
                    isBorderStyled={ true }
                    uniqueContextProviderStateId="TabBackupStorageContextProviderStateId"
                    tableProps={ {
                        dataset: records,
                        keyFieldName: 'id',
                        sorting: {
                            sortKind: SortingKind.Asc,
                            sortFieldName: 'name'
                        },
                        fieldsView: {
                            name: {
                                caption: 'Название',
                                fieldContentNoWrap: false,
                                fieldWidth: 'auto',
                                isSortable: true,
                                format: this.getFormatNameBackupStorage
                            },
                            description: {
                                caption: 'Описание',
                                fieldContentNoWrap: false,
                                fieldWidth: 'auto',
                                isSortable: true
                            },
                            connectionAddress: {
                                caption: 'Адрес подключения',
                                fieldContentNoWrap: false,
                                fieldWidth: 'auto',
                                isSortable: true
                            },
                            id: {
                                caption: '',
                                fieldWidth: 'auto',
                                fieldContentNoWrap: false,
                                format: this.getButtonsForActions
                            }
                        },
                        pagination: {
                            currentPage: metadata.pageNumber,
                            pageSize: metadata.pageSize,
                            recordsCount: metadata.totalItemCount,
                            totalPages: metadata.pageCount
                        },
                    } }
                    onDataSelect={ this.onDataSelect }
                />
                <SuccessButton
                    onClick={ () => { this.onClickToOpenModal(); } }
                >
                    <i className="fa fa-plus-circle mr-1" />
                    Создать хранилище бекапов
                </SuccessButton>
                {
                    this.state.isOpen
                        ? (
                            <BackupStorageModalView
                                isOpen={ this.state.isOpen }
                                backupStorageId={ this.state.backupStorageId }
                                onClickToCloseModal={ this.onClickToCloseModal }
                            />
                        )
                        : null
                }
                <WatchReducerProcessActionState
                    watch={ [{
                        reducerStateName: 'TabBackupStorageState',
                        processIds: [
                            TabBackupStorageProcessId.CreateCloudBackupStorage,
                            TabBackupStorageProcessId.EditCloudBackupStorage,
                            TabBackupStorageProcessId.DeleteCloudBackupStorage,
                        ]
                    }] }
                    onProcessActionStateChanged={ this.onProcessActionStateChanged }
                />
                <Dialog
                    isOpen={ this.state.deleteIsOpen }
                    dialogWidth="sm"
                    dialogVerticalAlign="center"
                    onCancelClick={ this.closeModalForDeleteBackupStorage }
                    buttons={
                        [
                            {
                                content: 'Нет',
                                kind: 'primary',
                                variant: 'text',
                                onClick: this.closeModalForDeleteBackupStorage
                            },
                            {
                                content: 'Да',
                                kind: 'error',
                                variant: 'contained',
                                onClick: this.onClickDeleteBackupStorage
                            }
                        ]
                    }
                >
                    <Box display="flex" flexDirection="column" alignItems="center" justifyContent="center">
                        <ErrorOutlineIcon color="warning" sx={ { width: '5em', height: '5em' } } />
                        Вы действительно хотите удалить хранилище бекапов?
                    </Box>
                </Dialog>
            </>
        );
    }
}

const BackupStorageTabConnected = connect<StateProps, DispatchProps, NonNullable<unknown>, AppReduxStoreState>(
    state => {
        const tabBackupStorageState = state.TabBackupStorageState;
        const { cloudBackupStorages } = tabBackupStorageState;
        const { hasCloudBackupStoragesReceived } = tabBackupStorageState.hasSuccessFor;

        return {
            cloudBackupStorages,
            hasCloudBackupStoragesReceived
        };
    },
    {
        dispatchGetCloudBackupStoragesThunk: GetCloudBackupStoragesThunk.invoke,
        dispatchDeleteCloudBackupStorageThunk: DeleteCloudBackupStorageThunk.invoke
    }
)(BackupStorageTabClass);

export const BackupStorageTabView = withFloatMessages(BackupStorageTabConnected);