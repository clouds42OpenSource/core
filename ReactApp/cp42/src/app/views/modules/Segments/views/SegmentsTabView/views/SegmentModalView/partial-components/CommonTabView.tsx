import { Box } from '@mui/material';
import { PageEnumType } from 'app/common/enums';
import { IErrorsType } from 'app/common/interfaces';
import { CheckBoxForm, ComboBoxForm, TextBoxForm } from 'app/views/components/controls/forms';
import { ComboboxItemModel } from 'app/views/components/controls/forms/ComboBox/models';
import { SegmentModalDataForm, SegmentModalDataFormFieldsNamesType } from 'app/views/modules/Segments/views/SegmentsTabView/types';
import { ShowErrorMessage } from 'app/views/modules/_common/components/ShowErrorMessage';
import { SegmentDropdownListResponseDto } from 'app/web/api/SegmentsProxy';
import { Component } from 'react';
import css from '../styles.module.css';

type CommonTabProps = {
    /**
     * Модель Redux формы для карточки сегмента
     */
    formFields: SegmentModalDataForm;

    /**
     * Интерфейс для валидации ошибок
     */
    errors: IErrorsType;

    /**
     * Элементы сегмента
     */
    segmentDropdownList: SegmentDropdownListResponseDto;

    /**
     * Тип страницы для динамического компонента редактирования/создания
     */
    pageType: PageEnumType;

    /**
     * Функция на изменение в полях редакс формы
     * @param formName Название формы
     * @param newValue Новое значение
     */
    onValueChange<TValue, TForm extends string = SegmentModalDataFormFieldsNamesType>(formName: TForm, newValue: TValue): void;
};

/**
 * Компонент вкладки "Общее"
 */
export class CommonTabView extends Component<CommonTabProps> {
    /**
     * Функция для добавления пустого значения в список элементов для ComboBox
     * @param items Массив объекта списка элементов для ComboBox компонента
     * @returns Массив объекта списка элементов с первым пустым значением
     */
    private addEmptyValueToComboboxItemArray = (items: ComboboxItemModel<string>[]) => {
        const arrayWithEmptyValue: ComboboxItemModel<string>[] = [{ text: '--Не выбрано--', value: '' }];
        return arrayWithEmptyValue.concat(items);
    };

    public render() {
        const {
            segmentDropdownList: {
                alpha83Versions, backupStorages,
                contentServers, coreHostingList,
                enterprise82Servers, enterprise83Servers,
                fileStorageServers, gatewayTerminals,
                sqlServers, stable82Versions,
                stable83Versions, terminalFarms,
                autoUpdateNodeList
            },
            formFields, errors,
            pageType
        } = this.props;

        return (
            <Box display="grid" gridTemplateColumns="1fr 1fr" gap="16px">
                <Box display="flex" flexDirection="column" gap="5px">
                    <TextBoxForm
                        label="Наименование:"
                        formName="name"
                        onValueChange={ this.props.onValueChange }
                        value={ formFields.name }
                        isReadOnly={ false }
                        placeholder="Введите наименование"
                    />
                    <ShowErrorMessage errors={ errors } formName="name" />
                </Box>
                <Box display="flex" flexDirection="column" gap="5px">
                    <TextBoxForm
                        label="Описание:"
                        formName="description"
                        onValueChange={ this.props.onValueChange }
                        value={ formFields.description }
                        isReadOnly={ false }
                        placeholder="Введите описание"
                    />
                    <ShowErrorMessage errors={ errors } formName="description" />
                </Box>
                <Box display="flex" flexDirection="column" gap="5px">
                    <ComboBoxForm
                        label="Сервер бэкапов:"
                        formName="backupStorageId"
                        items={ this.addEmptyValueToComboboxItemArray(backupStorages.map(item => ({ text: item.value, value: item.key }))) }
                        value={ formFields.backupStorageId }
                        onValueChange={ this.props.onValueChange }
                    />
                    <ShowErrorMessage errors={ errors } formName="backupStorageId" />
                </Box>
                <Box display="flex" flexDirection="column" gap="5px">
                    <ComboBoxForm
                        label="Хранилище клиентских файлов:"
                        formName="fileStorageServersId"
                        items={ this.addEmptyValueToComboboxItemArray(fileStorageServers.map(item => ({ text: item.value, value: item.key }))) }
                        value={ formFields.fileStorageServersId }
                        onValueChange={ this.props.onValueChange }
                    />
                    <ShowErrorMessage errors={ errors } formName="fileStorageServersId" />
                </Box>
                <Box display="flex" flexDirection="column" gap="5px">
                    <ComboBoxForm
                        label="Предприятие 8.2:"
                        formName="enterpriseServer82Id"
                        items={ this.addEmptyValueToComboboxItemArray(enterprise82Servers.map(item => ({ text: item.value, value: item.key }))) }
                        value={ formFields.enterpriseServer82Id }
                        onValueChange={ this.props.onValueChange }
                    />
                    <ShowErrorMessage errors={ errors } formName="enterpriseServer82Id" />
                </Box>
                <Box display="flex" flexDirection="column" gap="5px">
                    <ComboBoxForm
                        label="Предприятие 8.3:"
                        formName="enterpriseServer83Id"
                        items={ this.addEmptyValueToComboboxItemArray(enterprise83Servers.map(item => ({ text: item.value, value: item.key }))) }
                        value={ formFields.enterpriseServer83Id }
                        onValueChange={ this.props.onValueChange }
                    />
                    <ShowErrorMessage errors={ errors } formName="enterpriseServer83Id" />
                </Box>
                <Box display="flex" flexDirection="column" gap="5px">
                    <ComboBoxForm
                        label="Cервер публикации:"
                        formName="contentServerId"
                        items={ this.addEmptyValueToComboboxItemArray(contentServers.map(item => ({ text: item.value, value: item.key }))) }
                        value={ formFields.contentServerId }
                        onValueChange={ this.props.onValueChange }
                    />
                    <ShowErrorMessage errors={ errors } formName="contentServerId" />
                </Box>
                <Box display="flex" flexDirection="column" gap="5px">
                    <ComboBoxForm
                        label="SQL сервер:"
                        formName="sqlServerId"
                        items={ this.addEmptyValueToComboboxItemArray(sqlServers.map(item => ({ text: item.value, value: item.key }))) }
                        value={ formFields.sqlServerId }
                        onValueChange={ this.props.onValueChange }
                    />
                    <ShowErrorMessage errors={ errors } formName="sqlServerId" />
                </Box>
                <Box display="flex" flexDirection="column" gap="5px">
                    <ComboBoxForm
                        label="Ферма ТС:"
                        formName="servicesTerminalFarmId"
                        items={ this.addEmptyValueToComboboxItemArray(terminalFarms.map(item => ({ text: item.value, value: item.key }))) }
                        value={ formFields.servicesTerminalFarmId }
                        onValueChange={ this.props.onValueChange }
                    />
                    <ShowErrorMessage errors={ errors } formName="servicesTerminalFarmId" />
                </Box>
                <ComboBoxForm
                    label="Шлюз:"
                    formName="gatewayTerminalsId"
                    items={ this.addEmptyValueToComboboxItemArray(gatewayTerminals.map(item => ({ text: item.value, value: item.key }))) }
                    value={ formFields.gatewayTerminalsId }
                    onValueChange={ this.props.onValueChange }
                />
                <Box display="flex" flexDirection="column" gap="5px">
                    <ComboBoxForm
                        label="Cтабильная версий 8.2:"
                        formName="stable82VersionId"
                        items={ this.addEmptyValueToComboboxItemArray(stable82Versions.map(item => ({ text: item, value: item }))) }
                        value={ formFields.stable82VersionId }
                        onValueChange={ this.props.onValueChange }
                    />
                    <ShowErrorMessage errors={ errors } formName="stable82VersionId" />
                </Box>
                <Box display="flex" flexDirection="column" gap="5px">
                    <ComboBoxForm
                        label="Cтабильная версий 8.3:"
                        formName="stable83VersionId"
                        items={ this.addEmptyValueToComboboxItemArray(stable83Versions.map(item => ({ text: item, value: item }))) }
                        value={ formFields.stable83VersionId }
                        onValueChange={ this.props.onValueChange }
                    />
                    <ShowErrorMessage errors={ errors } formName="stable83VersionId" />
                </Box>
                <ComboBoxForm
                    label="Альфа версия 8.3:"
                    formName="alpha83VersionId"
                    items={ this.addEmptyValueToComboboxItemArray(alpha83Versions.map(item => ({ text: item, value: item }))) }
                    value={ formFields.alpha83VersionId }
                    onValueChange={ this.props.onValueChange }
                />
                <Box display="flex" flexDirection="column" gap="5px">
                    <TextBoxForm
                        label="Путь к файлам пользователей:"
                        formName="customFileStoragePath"
                        onValueChange={ this.props.onValueChange }
                        value={ formFields.customFileStoragePath }
                        placeholder="Введите путь"
                    />
                    <ShowErrorMessage errors={ errors } formName="customFileStoragePath" />
                </Box>
                <Box display="flex" flexDirection="column" gap="5px">
                    { pageType === PageEnumType.Edit
                        ? (
                            <TextBoxForm
                                label="Хостинг:"
                                formName="coreHostingId"
                                value={ formFields.coreHostingId }
                                placeholder="Введите хостинг"
                                isReadOnly={ true }
                            />
                        )
                        : (
                            <ComboBoxForm
                                label="Хостинг:"
                                formName="coreHostingId"
                                items={ this.addEmptyValueToComboboxItemArray(coreHostingList.map(item => ({ text: item.value, value: item.key }))) }
                                value={ formFields.coreHostingId }
                                onValueChange={ this.props.onValueChange }
                            />
                        )
                    }
                    <ShowErrorMessage errors={ errors } formName="coreHostingId" />
                </Box>
                <ComboBoxForm
                    label="Нода автообновления:"
                    formName="autoUpdateNodeId"
                    items={ this.addEmptyValueToComboboxItemArray(autoUpdateNodeList.map(item => ({ text: item.value, value: item.key }))) }
                    value={ formFields.autoUpdateNodeId }
                    onValueChange={ this.props.onValueChange }
                />
                <Box display="flex" flexDirection="column" gap="10px">
                    <CheckBoxForm
                        className={ css['checkbox-container'] }
                        formName="delimiterDatabaseMustUseWebService"
                        label="Открывать базы в тонком клиенте по http ссылке:"
                        isChecked={ formFields.delimiterDatabaseMustUseWebService }
                        onValueChange={ this.props.onValueChange }
                    />
                    <CheckBoxForm
                        className={ css['checkbox-container'] }
                        formName="notMountDiskR"
                        label="Не указывать путь до диска R:"
                        isChecked={ formFields.notMountDiskR }
                        onValueChange={ this.props.onValueChange }
                    />
                </Box>
            </Box>
        );
    }
}