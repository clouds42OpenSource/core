import { Box } from '@mui/material';
import { IErrorsType } from 'app/common/interfaces';
import { CheckBoxForm, TextBoxForm } from 'app/views/components/controls/forms';
import { ContentServersTabDataForm, ContentServersTabDataFormFieldsNamesType } from 'app/views/modules/Segments/views/ContentServersTabView/common/types';
import { ShowErrorMessage } from 'app/views/modules/_common/components/ShowErrorMessage';
import { Component } from 'react';

type CloudServerCommonTabProps = {
    /**
     * Модель Redux формы для сайта публикации
     */
    formFields: ContentServersTabDataForm;

    /**
     * Интерфейс для валидации ошибок
     */
    errors: IErrorsType;

    /**
     * Функция на изменение в полях редакс формы
     * @param formName Название формы
     * @param newValue Новое значение
     */
    onValueChange<TValue, TForm extends string = ContentServersTabDataFormFieldsNamesType>(formName: TForm, newValue: TValue): void;
};

/**
 * Таб "Общий"
 */
export class CloudServerCommonTabView extends Component<CloudServerCommonTabProps> {
    public render() {
        const { errors, formFields, onValueChange } = this.props;

        return (
            <Box display="flex" flexDirection="column" gap="12px">
                <div>
                    <TextBoxForm
                        formName="name"
                        label="Название сервера"
                        placeholder="Введите название"
                        value={ formFields.name }
                        onValueChange={ onValueChange }
                    />
                    <ShowErrorMessage errors={ errors } formName="name" />
                </div>
                <div>
                    <TextBoxForm
                        formName="publishSiteName"
                        label="Адрес публикации"
                        placeholder="Введите адрес"
                        value={ formFields.publishSiteName }
                        onValueChange={ onValueChange }
                    />
                    <ShowErrorMessage errors={ errors } formName="publishSiteName" />
                </div>
                <div>
                    <TextBoxForm
                        type="textarea"
                        formName="description"
                        label="Описание"
                        placeholder="Введите описание"
                        value={ formFields.description }
                        onValueChange={ onValueChange }
                    />
                    <ShowErrorMessage errors={ errors } formName="description" />
                </div>
                <div>
                    <CheckBoxForm
                        formName="groupByAccount"
                        label="Сгруппирован по аккаунту"
                        isChecked={ formFields.groupByAccount }
                        onValueChange={ onValueChange }
                    />
                </div>
            </Box>
        );
    }
}