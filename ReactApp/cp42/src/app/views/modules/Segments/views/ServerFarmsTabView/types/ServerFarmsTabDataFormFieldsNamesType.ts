import { ServerFarmsTabDataForm } from 'app/views/modules/Segments/views/ServerFarmsTabView/types';

export type ServerFarmsTabDataFormFieldsNamesType = keyof ServerFarmsTabDataForm;