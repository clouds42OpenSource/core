import { PageEnumType } from 'app/common/enums';
import { checkOnValidField, checkValidOfStringField } from 'app/common/functions';
import { IErrorsType } from 'app/common/interfaces';
import { TabTerminalGatewayProcessId } from 'app/modules/segments/TabTerminalGateway';
import { CreateCloudGatewayTerminalThunkParams } from 'app/modules/segments/TabTerminalGateway/store/reducer/createCloudGatewayTerminalReducer/params';
import { EditCloudGatewayTerminalThunkParams } from 'app/modules/segments/TabTerminalGateway/store/reducer/editCloudGatewayTerminalReducer/params';
import { GetCloudGatewayTerminalThunkParams } from 'app/modules/segments/TabTerminalGateway/store/reducer/getCloudGatewayTerminalReducer/params';
import { CreateCloudGatewayTerminalThunk, EditCloudGatewayTerminalThunk, GetCloudGatewayTerminalThunk } from 'app/modules/segments/TabTerminalGateway/store/thunks';
import { AppReduxStoreState } from 'app/redux/types';
import { Dialog } from 'app/views/components/controls/Dialog';
import { withFloatMessages } from 'app/views/components/_hoc/withFloatMessages';
import { MessageType } from 'app/views/components/_hoc/withFloatMessages/enums';
import { FloatMessageProps } from 'app/views/components/_hoc/withFloatMessages/interfaces';
import { ReduxFormProps, withReduxForm } from 'app/views/components/_hoc/withReduxForm';
import { fillCommonSegmentDataModel, getCommonSegmentDialogBody, getDialogButtons } from 'app/views/modules/Segments/common/functions';
import { CommonSegmentDataForm, CommonSegmentDataFormFieldsNamesType, CommonSegmentLengthEnumType } from 'app/views/modules/Segments/types';
import { WatchReducerProcessActionState } from 'app/views/modules/_common/components/WatchReducerProcessActionState';
import { CommonSegmentDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import { IReducerProcessActionInfo } from 'core/redux/interfaces';
import React, { Component } from 'react';
import { connect } from 'react-redux';

type OwnProps = ReduxFormProps<CommonSegmentDataForm>;

type GatewayTerminalModalProps = {
    /**
     * Поле для открытия модального окошка
     */
    isOpen: boolean;

    /**
     * ID терминального шлюза
     */
    cloudGatewayTerminalId?: string;

    /**
     * Функция на закрытие модального окошка
     */
    onClickToCloseModal: () => void;
};

type GatewayTerminalModalState = {
    /**
     * Обьект Ошибок для валидации
     */
    errors: IErrorsType;

    /**
     * Тип страницы для динамического компонента редактирования/создания
     */
    pageType: PageEnumType;
};

type StateProps = {
    /**
     * Модель терминального шлюза
     */
    cloudGatewayTerminal: CommonSegmentDataModel;
};

type DispatchProps = {
    dispatchGetCloudGatewayTerminalThunk: (args: GetCloudGatewayTerminalThunkParams) => void;
    dispatchCreateCloudGatewayTerminalThunk: (args: CreateCloudGatewayTerminalThunkParams) => void;
    dispatchEditCloudGatewayTerminalThunk: (args: EditCloudGatewayTerminalThunkParams) => void;
};

type AllProps = GatewayTerminalModalProps & OwnProps & StateProps & DispatchProps & FloatMessageProps;

class GatewayTerminalModalClass extends Component<AllProps, GatewayTerminalModalState> {
    public constructor(props: AllProps) {
        super(props);

        this.state = {
            errors: {},
            pageType: this.props.cloudGatewayTerminalId ? PageEnumType.Edit : PageEnumType.Create
        };

        this.getDialogBody = this.getDialogBody.bind(this);
        this.onValueChange = this.onValueChange.bind(this);
        this.submitChanges = this.submitChanges.bind(this);
        this.createGatewayTerminal = this.createGatewayTerminal.bind(this);
        this.editGatewayTerminal = this.editGatewayTerminal.bind(this);
        this.isGatewayTerminalFormValid = this.isGatewayTerminalFormValid.bind(this);

        this.onProcessActionStateChanged = this.onProcessActionStateChanged.bind(this);
        this.onProcessGetCloudGatewayTerminal = this.onProcessGetCloudGatewayTerminal.bind(this);
        this.successGetCloudGatewayTerminalState = this.successGetCloudGatewayTerminalState.bind(this);
        this.errorGetCloudGatewayTerminalState = this.errorGetCloudGatewayTerminalState.bind(this);

        this.props.reduxForm.setInitializeFormDataAction(this.defaultInitDataReduxForm);
    }

    public componentDidMount() {
        if (this.props.cloudGatewayTerminalId) {
            this.props.dispatchGetCloudGatewayTerminalThunk({
                cloudGatewayTerminalId: this.props.cloudGatewayTerminalId,
                force: true
            });
        }
    }

    private onProcessActionStateChanged(processId: string, actionState: IReducerProcessActionInfo) {
        this.onProcessGetCloudGatewayTerminal(processId, actionState);
    }

    private onProcessGetCloudGatewayTerminal(processId: string, actionState: IReducerProcessActionInfo) {
        if (processId !== TabTerminalGatewayProcessId.GetTerminalGateway) return;
        this.successGetCloudGatewayTerminalState(actionState);
        this.errorGetCloudGatewayTerminalState(actionState);
    }

    private onValueChange<TValue, TForm extends string = CommonSegmentDataFormFieldsNamesType>(formName: TForm, newValue: TValue) {
        this.props.reduxForm.updateReduxFormFields({
            [formName]: newValue
        });

        const { errors } = this.state;

        if (typeof newValue === 'string') {
            switch (formName) {
                case 'connectionAddress':
                    errors[formName] = checkValidOfStringField(newValue, 'Заполните адрес подключения', CommonSegmentLengthEnumType.ConnectionAddress);
                    break;
                case 'description':
                    errors[formName] = checkValidOfStringField(newValue, undefined, CommonSegmentLengthEnumType.Description);
                    break;
                case 'name':
                    errors[formName] = checkValidOfStringField(newValue, 'Заполните название', CommonSegmentLengthEnumType.Name);
                    break;
                default:
                    break;
            }

            this.setState({ errors });
        }
    }

    private getDialogBody() {
        const formFields = this.props.reduxForm.getReduxFormFields(false);
        return getCommonSegmentDialogBody(formFields, this.state.errors, this.onValueChange);
    }

    private successGetCloudGatewayTerminalState(actionState: IReducerProcessActionInfo) {
        if (!actionState.isInSuccessState) return;
        this.props.reduxForm.updateReduxFormFields({
            ...this.props.cloudGatewayTerminal
        });
    }

    private errorGetCloudGatewayTerminalState(actionState: IReducerProcessActionInfo, error?: Error) {
        if (!actionState.isInErrorState) return;
        this.props.floatMessage.show(MessageType.Error, `Ошибка при получении терминального шлюза: ${ error?.message }`);
    }

    private submitChanges() {
        if (!this.isGatewayTerminalFormValid()) return;
        this.createGatewayTerminal();
        this.editGatewayTerminal();
    }

    private createGatewayTerminal() {
        if (this.state.pageType !== PageEnumType.Create) return;
        this.props.dispatchCreateCloudGatewayTerminalThunk({
            ...fillCommonSegmentDataModel(this.props.reduxForm.getReduxFormFields(false)),
            force: true
        });
    }

    private editGatewayTerminal() {
        if (this.state.pageType !== PageEnumType.Edit) return;
        this.props.dispatchEditCloudGatewayTerminalThunk({
            ...fillCommonSegmentDataModel(this.props.reduxForm.getReduxFormFields(false)),
            id: this.props.cloudGatewayTerminalId!,
            force: true
        });
    }

    private defaultInitDataReduxForm(): CommonSegmentDataForm | undefined {
        return {
            connectionAddress: '',
            description: '',
            name: ''
        };
    }

    private isGatewayTerminalFormValid() {
        const formFields = this.props.reduxForm.getReduxFormFields(false);
        const errors: IErrorsType = {};
        const arrayResults: boolean[] = [];

        arrayResults.push(checkOnValidField(errors, 'description', formFields.description, undefined, CommonSegmentLengthEnumType.Description));
        arrayResults.push(checkOnValidField(errors, 'connectionAddress', formFields.connectionAddress, 'Заполните адрес подключения', CommonSegmentLengthEnumType.ConnectionAddress));
        arrayResults.push(checkOnValidField(errors, 'name', formFields.name, 'Заполните название', CommonSegmentLengthEnumType.Name));

        if (arrayResults.some(item => !item)) {
            this.props.floatMessage.show(MessageType.Warning, 'Форма к отправке не готова. Пожалуйста, заполните все поля.');
            this.setState({ errors });
            return false;
        }

        return true;
    }

    public render() {
        const { isOpen } = this.props;
        const isEditTypePage = this.state.pageType === PageEnumType.Edit;
        const titleText = isEditTypePage ? 'Редактирование' : 'Создание';

        return (
            <>
                <Dialog
                    disableBackdropClick={ true }
                    title={ `${ titleText } терминального шлюза` }
                    isOpen={ isOpen }
                    dialogWidth="sm"
                    isTitleSmall={ true }
                    titleTextAlign="left"
                    titleFontSize={ 14 }
                    dialogVerticalAlign="center"
                    onCancelClick={ this.props.onClickToCloseModal }
                    buttons={ getDialogButtons({
                        pageType: this.state.pageType,
                        onClickToCloseModal: this.props.onClickToCloseModal,
                        submitChanges: this.submitChanges
                    }) }
                >
                    {
                        this.getDialogBody()
                    }
                </Dialog>
                <WatchReducerProcessActionState
                    watch={ [{
                        reducerStateName: 'TabTerminalGatewayState',
                        processIds: [
                            TabTerminalGatewayProcessId.GetTerminalGateway
                        ]
                    }] }
                    onProcessActionStateChanged={ this.onProcessActionStateChanged }
                />
            </>
        );
    }
}

const GatewayTerminalModalConnected = connect<StateProps, DispatchProps, OwnProps, AppReduxStoreState>(
    state => {
        const tabTerminalGatewayState = state.TabTerminalGatewayState;
        const { cloudGatewayTerminal } = tabTerminalGatewayState;

        return {
            cloudGatewayTerminal
        };
    },
    {
        dispatchGetCloudGatewayTerminalThunk: GetCloudGatewayTerminalThunk.invoke,
        dispatchCreateCloudGatewayTerminalThunk: CreateCloudGatewayTerminalThunk.invoke,
        dispatchEditCloudGatewayTerminalThunk: EditCloudGatewayTerminalThunk.invoke
    }
)(GatewayTerminalModalClass);

export const GatewayTerminalModalView = withReduxForm(withFloatMessages(GatewayTerminalModalConnected),
    {
        reduxFormName: 'Gateway_Terminal_Modal_Form',
        resetOnUnmount: true
    });