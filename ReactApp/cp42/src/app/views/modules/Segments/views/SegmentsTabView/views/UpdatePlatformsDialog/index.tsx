import { Box, Divider, TextField } from '@mui/material';
import { GetAllSegmentsResponse } from 'app/api/endpoints/segments/response';
import { FETCH_API } from 'app/api/useFetchApi';
import { EMessageType, useAppSelector, useFloatMessages } from 'app/hooks';
import { OutlinedButton } from 'app/views/components/controls/Button';
import { Dialog } from 'app/views/components/controls/Dialog';
import { CheckBoxForm, ComboBoxForm } from 'app/views/components/controls/forms';
import { memo, useEffect, useState } from 'react';
import style from './style.module.css';

type TUpdatePlatformsDialogProps = {
    isOpen: boolean;
    onCancelClick: () => void;
};

const { getAllSegments, putPlatforms } = FETCH_API.SEGMENTS;

export const UpdatePlatformsDialog = memo(({ isOpen, onCancelClick }: TUpdatePlatformsDialogProps) => {
    const {
        segmentDropdownList: { stable83Versions },
    } = useAppSelector(state => state.SegmentsState);
    const { show } = useFloatMessages();

    const [segments, setSegments] = useState<GetAllSegmentsResponse[]>([]);
    const [searchSegments, setSearchSegments] = useState('');
    const [selectSegments, setSelectSegments] = useState<string[]>([]);
    const [selectPlatform, setSelectPlatform] = useState('');

    useEffect(() => {
        (async () => {
            const allSegmentsResponse = await getAllSegments();

            if (allSegmentsResponse.success && allSegmentsResponse.data) {
                setSegments(allSegmentsResponse.data);
            } else {
                show(EMessageType.error, allSegmentsResponse.message);
            }
        })();
    }, [show]);

    const selectSegmentsChange = (id: string) => () => {
        if (selectSegments.includes(id)) {
            setSelectSegments(prev => prev.filter(select => select !== id));
        } else {
            setSelectSegments(prev => [...prev, id]);
        }
    };

    const selectAllSegments = () => {
        if (selectSegments.length === segments.length) {
            setSelectSegments([]);
        } else {
            const selectedSegments = segments.reduce((prev: string[], curr) => {
                if (curr.name.toLowerCase().includes(searchSegments.toLowerCase())) {
                    prev.push(curr.id);
                }

                return prev;
            }, []);

            setSelectSegments([...new Set([...selectSegments, ...selectedSegments])]);
        }
    };

    const changePlatforms = async () => {
        const changeResponse = await putPlatforms({ segmentIds: selectSegments, stable83VersionId: selectPlatform });

        if (changeResponse.success) {
            show(EMessageType.success, 'Задачи на смену платформы для сегментов запущены');
            onCancelClick();
        } else {
            show(EMessageType.error, changeResponse.message);
        }
    };

    return (
        <Dialog
            title="Обновление платформ"
            isOpen={ isOpen }
            onCancelClick={ onCancelClick }
            dialogWidth="sm"
            dialogVerticalAlign="center"
            buttons={ [
                {
                    content: 'Отмена',
                    onClick: onCancelClick,
                    kind: 'default',
                },
                {
                    content: 'Изменить',
                    onClick: changePlatforms,
                    kind: 'success',
                    isEnabled: !!selectPlatform && !!selectSegments.length
                }
            ] }
        >
            <Box display="flex" justifyContent="space-between" gap="16px" mb="16px">
                <TextField
                    label="Название сегмента"
                    value={ searchSegments }
                    onChange={ e => setSearchSegments(e.target.value) }
                    className={ style.search }
                    InputLabelProps={ { shrink: true } }
                />
                <ComboBoxForm
                    label="Платформы"
                    formName="plarforms"
                    items={
                        stable83Versions.map(platform => ({
                            value: platform,
                            text: platform
                        }))
                    }
                    value={ selectPlatform }
                    onValueChange={ (_, platform) => setSelectPlatform(platform) }
                />
            </Box>
            <OutlinedButton onClick={ selectAllSegments } kind="success">Выбрать все в выбранном списке</OutlinedButton>
            <Divider sx={ { margin: '8px 0' } } />
            <Box display="flex" flexDirection="column" height="200px" minWidth="50%" overflow="auto" gap="8px">
                {
                    segments.map(segment => {
                        if (segment.name.toLowerCase().includes(searchSegments.toLowerCase())) {
                            return (
                                <CheckBoxForm
                                    key={ segment.id }
                                    formName={ segment.name }
                                    label={ segment.name }
                                    isChecked={ selectSegments.includes(segment.id) }
                                    onValueChange={ selectSegmentsChange(segment.id) }
                                    className={ style.checkbox }
                                />
                            );
                        }

                        return null;
                    })
                }
            </Box>
        </Dialog>
    );
});