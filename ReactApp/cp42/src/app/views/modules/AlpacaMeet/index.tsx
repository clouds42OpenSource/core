import { FETCH_API } from 'app/api/useFetchApi';
import { EMessageType, useFloatMessages } from 'app/hooks';
import { withHeader } from 'app/views/components/_hoc/withHeader';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import { ActiveServices } from 'app/views/modules/ManagingServiceSubscriptions/views/ActiveServices';
import React, { useCallback, useEffect, useState } from 'react';

const id = import.meta.env.REACT_ALPACA_MEET_ID as string;

const api = FETCH_API.MANAGING_SERVICE_SUBSCRIPTIONS;

const AlpacaMeet = () => {
    const { show } = useFloatMessages();

    const [isLoading, setIsLoading] = useState(false);
    const [isActive, setIsActive] = useState(false);

    const getActivationStatus = useCallback(async () => {
        setIsLoading(true);

        const { success, message, data } = await api.getActivationStatus({ id });

        if (success) {
            setIsActive(!!data);
        } else {
            show(EMessageType.error, message);
        }

        setIsLoading(false);
    }, [show]);

    useEffect(() => {
        void getActivationStatus();
    }, [getActivationStatus]);

    return (
        <>
            { isLoading && <LoadingBounce /> }
            <ActiveServices
                id={ id }
                setIsLoading={ setIsLoading }
                isActive={ isActive }
                isAlpaca={ true }
            />
        </>
    );
};

export const AlpacaMeetView = withHeader({
    title: 'AlpacaMeet',
    page: AlpacaMeet
});