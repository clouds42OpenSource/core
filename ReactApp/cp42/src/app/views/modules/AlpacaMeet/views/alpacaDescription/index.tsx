import { TextOut } from 'app/views/components/TextOut';

import style from './style.module.css';

export const AlpacaDescription = () => {
    return (
        <div>
            <TextOut fontSize={ 20 } fontWeight={ 700 } inDiv={ true }>Как бот поможет вашему бизнесу?</TextOut>
            <TextOut inDiv={ true } fontSize={ 14 } className={ style.description }>
                <TextOut fontWeight={ 700 }>AlpacaMeet</TextOut> сделает прозрачными процессы совещаний.&nbsp;
                Акцентирует внимание только на важных итогах. Сервис может работать параллельно в нескольких группах,&nbsp;
                без участия владельца бота. Итоги будут подведены для участников групп.
            </TextOut>
        </div>
    );
};