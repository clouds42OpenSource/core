import { Card, CardContent, List, ListItem, ListItemIcon } from '@mui/material';
import ListItemText from '@mui/material/ListItemText';
import { Box } from '@mui/system';
import { COLORS } from 'app/utils';
import { SuccessButton } from 'app/views/components/controls/Button';
import { TextOut } from 'app/views/components/TextOut';
import { tariffs } from 'app/views/modules/AlpacaMeet/views/alpacaTariffs/consts';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import AccessTimeIcon from '@mui/icons-material/AccessTime';
import AttachMoneyIcon from '@mui/icons-material/AttachMoney';

import style from './style.module.css';

export const AlpacaTariffs = () => {
    const selectTariff = () => {
        window.open('https://t.me/AlpacaMeet_bot', '_blank');
    };

    return (
        <Box display="flex" flexDirection="column" alignItems="center" gap="16px">
            <Box display="flex" flexDirection={ { xs: 'column', md: 'row' } } gap={ { xs: '16px', lg: '32px' } }>
                {
                    tariffs.map(tariff => (
                        <Card sx={ { maxWidth: 345, margin: 'auto', boxShadow: 3 } } key={ tariff.name }>
                            <CardContent sx={ { paddingLeft: { xs: '4px', lg: '16px' }, paddingRight: { xs: '4px', lg: '16px' } } }>
                                <TextOut inDiv={ true } textAlign="center" fontWeight={ 700 } fontSize={ 20 } className={ style.title }>{ tariff.name }</TextOut>
                                <TextOut inDiv={ true } textAlign="center" fontSize={ 14 }>{ tariff.fullPrice }</TextOut>
                                <List>
                                    <ListItem className={ style.listItem }>
                                        <ListItemIcon className={ style.icon }>
                                            <AccessTimeIcon />
                                        </ListItemIcon>
                                        <ListItemText primary={ <TextOut fontSize={ 14 }>Количество часов распознавания - { tariff.hours }</TextOut> } />
                                    </ListItem>
                                    <ListItem className={ style.listItem }>
                                        <ListItemIcon className={ style.icon }>
                                            <AttachMoneyIcon />
                                        </ListItemIcon>
                                        <ListItemText primary={ <TextOut fontSize={ 14 }>Стоимость часа &quot;<TextOut fontWeight={ 700 }>{ tariff.hoursPrice }</TextOut>&quot;</TextOut> } />
                                    </ListItem>
                                    {
                                        tariff.advantages.map(advantage => (
                                            <ListItem key={ advantage } className={ style.listItem }>
                                                <ListItemIcon className={ style.icon }>
                                                    <CheckCircleIcon sx={ { color: COLORS.main } } />
                                                </ListItemIcon>
                                                <ListItemText primary={ <TextOut fontSize={ 14 }>{ advantage }</TextOut> } />
                                            </ListItem>
                                        ))
                                    }
                                </List>
                            </CardContent>
                        </Card>
                    ))
                }
            </Box>
            <TextOut className={ style.addition }>*Каждый месяц <TextOut fontWeight={ 700 } fontSize={ 14 }>3 часа</TextOut> распознавания встреч</TextOut>
            <SuccessButton onClick={ selectTariff } fullWidth={ true } className={ style.selectTariffButton }>Попробовать бесплатно</SuccessButton>
        </Box>
    );
};