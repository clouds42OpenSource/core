type TTariffs = {
    name: string;
    fullPrice: string;
    hours: string;
    hoursPrice: string;
    advantages: string[];
}

export const tariffs: TTariffs[] = [
    {
        name: '*Free',
        fullPrice: 'Бесплатно',
        hours: '3',
        hoursPrice: 'Бесплатно',
        advantages: [
            'Автоматическое формирование итогов встреч',
            'Формирование транскрипта разговора',
            'Поддержка 24/7'
        ]
    },
    {
        name: 'Для себя',
        fullPrice: '7000 руб',
        hours: '10',
        hoursPrice: '700 руб',
        advantages: [
            'Автоматическое формирование итогов встреч',
            'Формирование транскрипта разговора',
            'Поддержка 24/7'
        ]
    },
    {
        name: 'Для команды',
        fullPrice: '24000 руб',
        hours: '40',
        hoursPrice: '600 руб',
        advantages: [
            'Автоматическое формирование итогов встреч',
            'Формирование транскрипта разговора',
            'Поддержка 24/7'
        ]
    }
];