import { AppReduxStoreState } from 'app/redux/types';
import { LoadingBounce } from 'app/views/components/LoadingBounce';
import React from 'react';
import { connect } from 'react-redux';

type StateProps = {
    showLoadingProgress: boolean;
};

type AllProps = StateProps;

class LoadingViewClass extends React.Component<AllProps> {
    public shouldComponentUpdate(nextProps: AllProps, _nextState: any) {
        return this.props.showLoadingProgress !== nextProps.showLoadingProgress;
    }

    public render() {
        return (
            <>
                { this.props.showLoadingProgress ? <LoadingBounce /> : null }
            </>
        );
    }
}

export const LoadingView = connect<StateProps, object, object, AppReduxStoreState>(
    state => {
        const reducerStateKeys = Object.keys(state);

        for (const reducerStateName1 of reducerStateKeys) {
            const reducerStateName = reducerStateName1 as keyof AppReduxStoreState;

            if (reducerStateName === 'ReduxForms' || reducerStateName === 'Global') continue;
            if (!state[reducerStateName].reducerActions) continue;

            const reducerActionkeys = Object.keys(state[reducerStateName].reducerActions);

            for (const reducerActionkey of reducerActionkeys) {
                const reducerAction = (state[reducerStateName].reducerActions as any)[reducerActionkey];

                if (reducerAction.process.showLoadingProgress && reducerAction.hasProcessActionStateChanged && reducerAction.processActionState.isInProgressState) {
                    return { showLoadingProgress: true };
                }
            }
        }

        return { showLoadingProgress: false };
    }
)(LoadingViewClass);