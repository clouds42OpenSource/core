import { useAppDispatch } from 'app/hooks/useRedux';
import { informationBasesList } from 'app/modules/informationBases';
import { useState } from 'react';

const { closeInformationBase, setIsOther } = informationBasesList.actions;

export const useDatabaseCard = () => {
    const dispatch = useAppDispatch();

    const [isOpen, setIsOpen] = useState(false);
    const [dialogDatabaseNumber, setDialogDatabaseNumber] = useState('');

    const closeDatabaseDialogHandler = () => {
        setIsOpen(false);
        setDialogDatabaseNumber('');
        dispatch(closeInformationBase());
    };

    const openDatabaseDialogHandler = (databaseNumber: string, isOther = false) => {
        setIsOpen(true);
        setDialogDatabaseNumber(databaseNumber);
        dispatch(setIsOther(isOther));
    };

    return { openDatabaseDialogHandler, closeDatabaseDialogHandler, isOpen, dialogDatabaseNumber };
};