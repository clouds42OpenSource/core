import { useAppSelector } from 'app/hooks/useRedux';

export const useGetCurrency = () => {
    const { locale } = useAppSelector(state => state.Global.getCurrentSessionSettingsReducer.settings.currentContextInfo);

    switch (locale) {
        case 'ru-kz':
            return 'тенге';
        case 'ru-ua':
            return 'грн';
        case 'ru-ru':
        default:
            return 'руб';
    }
};