import { useState } from 'react';

export const useAccountCard = () => {
    const [isOpen, setIsOpen] = useState(false);
    const [dialogAccountNumber, setDialogAccountNumber] = useState(-1);

    const closeDialogHandler = () => {
        setIsOpen(false);
        setDialogAccountNumber(-1);
    };

    const openDialogHandler = (accountNumber: number) => {
        setIsOpen(true);
        setDialogAccountNumber(accountNumber);
    };

    return { openDialogHandler, closeDialogHandler, isOpen, dialogAccountNumber };
};