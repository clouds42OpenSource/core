import { useAppSelector } from 'app/hooks/useRedux';

export const useGetCurrentUserGroups = () => {
    return useAppSelector(state => state.Global.getCurrentSessionSettingsReducer.settings.currentContextInfo.currentUserGroups);
};