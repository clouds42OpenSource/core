import { useState, MouseEvent } from 'react';

type TState<T> = {
    isOpen: boolean;
    data?: T;
};

export const useIsOpen = <T = undefined>() => {
    const [state, setState] = useState<TState<T>>({ isOpen: false, data: undefined });

    const setOpen = (_: MouseEvent, data?: T) => {
        setState({ isOpen: true, data });
    };

    const setClose = () => {
        setState({ isOpen: false, data: undefined });
    };

    return { state, setOpen, setClose };
};