declare const styles: {
    readonly success: string;
    readonly warning: string;
    readonly info: string;
    readonly error: string;
    readonly default: string;
};

export = styles;