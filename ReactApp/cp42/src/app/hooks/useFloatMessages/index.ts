import { SnackbarMessage, useSnackbar } from 'notistack';
import { useCallback } from 'react';

import styles from './style.module.css';

export enum EMessageType {
    default = 'default',
    error = 'error',
    success = 'success',
    warning = 'warning',
    info = 'info'
}

export const useFloatMessages = () => {
    const { enqueueSnackbar, closeSnackbar } = useSnackbar();

    return {
        show: useCallback((variant: EMessageType, message: SnackbarMessage, autoHideDuration?: number) => {
            const key = enqueueSnackbar(message, {
                variant,
                className: styles[variant],
                autoHideDuration: autoHideDuration ?? 5000,
                preventDuplicate: true,
                anchorOrigin: {
                    vertical: 'top',
                    horizontal: 'center',
                },
                onClick: () => {
                    closeSnackbar(key);
                }
            });
        }, [closeSnackbar, enqueueSnackbar])
    };
};