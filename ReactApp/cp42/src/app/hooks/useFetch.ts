import { getFetchedData, templateErrorText } from 'app/api';
import { TData, TDataReturn, TFetchParams } from 'app/api/types';
import { useEffect, useState } from 'react';

type TUseFetchParams<T> = TFetchParams<T> & {
    isRequestBlocked?: boolean;
};

export const useFetch = <R extends object | string | boolean | number, T extends object | void | string | boolean | number>
    ({ url, headers: requestHeaders, method, hasStandardResponseType = true, body, isRequestBlocked, noAuthorizationToken, noCredentials, isFormData }: TUseFetchParams<T>): TDataReturn<R> => {
    const [data, setData] = useState<TData<R> | null>(null);
    const [error, setError] = useState<string | null>(null);
    const [isLoading, setIsLoading] = useState(false);
    const [refreshFlag, setRefreshFlag] = useState(false);

    useEffect(() => {
        (async () => {
            if (!isRequestBlocked) {
                try {
                    setIsLoading(true);
                    const { status, headers, responseBody } = await getFetchedData<R, T>({ url, headers: requestHeaders, method, body, noAuthorizationToken, noCredentials, isFormData });

                    if (hasStandardResponseType && !responseBody.success) {
                        setError(responseBody.message ?? templateErrorText);
                    } else {
                        setData(() => {
                            const returnedData: TData<R> = {
                                rawData: hasStandardResponseType && responseBody.data !== undefined && responseBody.data !== null ? responseBody.data : responseBody as R,
                                status,
                                headers,
                            };

                            return returnedData;
                        });
                    }
                } catch (err: unknown) {
                    setError(templateErrorText);
                } finally {
                    setIsLoading(false);
                }
            }
        })();
    }, [body, hasStandardResponseType, method, isRequestBlocked, refreshFlag, requestHeaders, url, noAuthorizationToken, noCredentials, isFormData]);

    return { data, error, isLoading, refreshData: () => setRefreshFlag(!refreshFlag) } as const;
};