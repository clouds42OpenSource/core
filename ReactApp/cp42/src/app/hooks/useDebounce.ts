import { useCallback, useEffect, useRef, useState } from 'react';

export const useDebounce = <T>(value: T, delay = 250): T => {
    const [debouncedValue, setDebouncedValue] = useState<T>(value);

    useEffect(() => {
        const timer = setTimeout(() => setDebouncedValue(value), delay);

        return () => {
            clearTimeout(timer);
        };
    }, [value, delay]);

    return debouncedValue;
};

export const useDebounceCallback = (callback: (...args: any[]) => void, delay = 300) => {
    const timeoutRef = useRef<NodeJS.Timeout | null>(null);

    return useCallback(
        (...args) => {
            if (timeoutRef.current) {
                clearTimeout(timeoutRef.current);
            }
            timeoutRef.current = setTimeout(() => callback(...args), delay);
        },
        [callback, delay]
    );
};