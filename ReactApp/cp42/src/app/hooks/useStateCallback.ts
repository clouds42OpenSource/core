import { useRef, useState, useEffect, useCallback } from 'react';

type TCallback<T> = (state: T) => void;

export const useStateCallback = <T>(initialState: T): [T, (state: T, cb?: TCallback<T>) => void] => {
    const [state, setState] = useState(initialState);
    const callbackRef = useRef<TCallback<T>>();

    const setStateCallback = useCallback((st: T, cb?: (state: T) => void) => {
        callbackRef.current = cb;
        setState(st);
    }, []);

    useEffect(() => {
        if (callbackRef.current) {
            callbackRef.current(state);
            callbackRef.current = undefined;
        }
    }, [state]);

    return [state, setStateCallback];
};