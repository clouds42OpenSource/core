import * as Sentry from '@sentry/react';
import 'font-awesome/css/font-awesome.min.css';
import ReactDOM from 'react-dom';

import App from './App';
import './index.css';

Sentry.init({
    dsn: import.meta.env.REACT_APP_SENTRY_DSN,
    enabled: import.meta.env.MODE === 'production' || import.meta.env.MODE === 'beta',
    environment: import.meta.env.MODE,
    integrations: [Sentry.browserTracingIntegration(), Sentry.replayIntegration(), Sentry.contextLinesIntegration()],
    tracesSampleRate: 0.1,
    tracePropagationTargets: [import.meta.env.REACT_APP_API_HOST],
    replaysSessionSampleRate: 0.1,
    replaysOnErrorSampleRate: 1.0,
    denyUrls: [/gtm\.js/, /.*\/externalServices\/.*/],
    ignoreErrors: ['Can\'t find variable: $', 'ReferenceError: $ is not defined'],
    beforeSend(event) {
        if (event.exception && event.exception.values && event.exception.values.some(e => e.type === 'ReferenceError' && e.value?.includes('S is not defined'))) {
            return null;
        }

        return event;
    }
});

ReactDOM.render(<App />, document.getElementById('root'));