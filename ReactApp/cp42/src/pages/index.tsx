import { CreateEditPrintedHtmlFormView } from 'app/views/modules/PrintedHtmlFormsReference/views/CreateEditPrintedHtmlFormView';
import { ProcessFlowDetailsView } from 'app/views/modules/ProcessFlow/views/ProcessFlowDetailsView';
import { RentEditResourceView } from 'app/views/modules/Rent/tabs/RentControl/views/RentEditResource';
import { CreateEditSupplierView } from 'app/views/modules/Suppliers/views/CreateEditSupplierView';

import { asyncComponent } from 'app/views/components/_hoc/asyncComponent';

/**
 * Прокси страница (редирект после регистрации и авторизации для получения всех кредитов)
 */
export const ProxyPage = asyncComponent(() => import('app/views/modules/ProxyPage').then(({ ProxyPage: ProxyPageView }) => ProxyPageView));
/**
 * Главная страница
 */
export const MainPage = asyncComponent(() => import('app/views/modules/Main').then(({ MainView }) => MainView));

/**
 * Страница авторизации
 */
export const SignInPage = asyncComponent(() => import('app/views/modules/SignIn').then(({ SignInView }) => SignInView));

/**
 * Страница Регистрации
 */
export const SignUpPage = asyncComponent(() => import('app/views/modules/SignUp').then(({ SignUpView }) => SignUpView));

/**
 * Страница Восстановление пароля
 */
export const ResetPasswordPage = asyncComponent(() => import('app/views/modules/ResetPassword').then(({ ResetPasswordView }) => ResetPasswordView));

/**
 * Страница ошибки 404
 */
export const Error404Page = asyncComponent(() => import('app/views/modules/Errors').then(({ NotFoundError }) => NotFoundError));

/**
 * Страница ошибки 500
 */
export const Error500Page = asyncComponent(() => import('app/views/modules/Errors').then(({ ServerError }) => ServerError));

/**
 * Страница ошибки прав доступа
 */
export const ErrorAccessPage = asyncComponent(() => import('app/views/modules/Errors').then(({ AccessError }) => AccessError));

/**
 * Страница для скачивания актуальной версии Линк42
 */
export const Link42DownloaderPage = asyncComponent(() => import('app/views/modules/Link42Downloader').then(({ Link42Downloader }) => Link42Downloader));

/**
 * Страница логирования
 */
export const LoggingPageAsync = asyncComponent(() => import('app/views/modules/Logging').then(({ LoggingView }) => LoggingView));

/**
 * Страница задач
 */
export const TasksPageAsync = asyncComponent(() => import('app/views/modules/CoreWorkerTasks').then(({ CoreWorkerTasksView }) => CoreWorkerTasksView));

/**
 * Страница справочника CloudServices
 */
export const CloudServicesPageAsync = asyncComponent(() => import('app/views/modules/CloudServices').then(({ CloudServicesPageView }) => CloudServicesPageView));

/**
 * Страница Рабочие процесы
 */
export const ProcessFlowPageAsync = asyncComponent(() => import('app/views/modules/ProcessFlow').then(({ ProcessFlowView }) => ProcessFlowView));

/**
 * Страница деталей рабочего процесса
 */
export const ProcessFlowDetailsPage = ProcessFlowDetailsView;

/**
 * Страница материнских баз разделителей
 */
export const DelimiterSourceAccountDatabasesPageAsync = asyncComponent(() => import('app/views/modules/DelimiterSourceAccountDatabases').then(({ DelimiterSourceAccountDatabases }) => DelimiterSourceAccountDatabases));

/**
 * Страница служебные аккаунты
 */
export const ServiceAccountsPageAsync = asyncComponent(() => import('app/views/modules/ServiceAccounts').then(({ ServiceAccountsView }) => ServiceAccountsView));

/**
 * Страница конфигураций 1С
 */
export const Configurations1CPageAsync = asyncComponent(() => import('app/views/modules/Configurations1C').then(({ Configurations1CView }) => Configurations1CView));

/**
 * Страница отраслей
 */
export const IndustryPageAsync = asyncComponent(() => import('app/views/modules/Industry').then(({ IndustryView }) => IndustryView));

/**
 * Страница шаблонов
 */
export const DbTemplatesPageAsync = asyncComponent(() => import('app/views/modules/DbTemplates').then(({ DbTemplatesView }) => DbTemplatesView));

/**
 * Страница базы на разделителях
 */
export const DbTemplateDelimetersPageAsync = asyncComponent(() => import('app/views/modules/DbTemplateDelimeters').then(({ DbTemplateDelimetersView }) => DbTemplateDelimetersView));

/**
 * Страница настройки облака
 */
export const CloudCorePageAsync = asyncComponent(() => import('app/views/modules/CloudCore').then(({ CloudCoreView }) => CloudCoreView));

/**
 * Страница агентских соглашений
 */
export const AgencyAgreementPageAsync = asyncComponent(() => import('app/views/modules/AgencyAgreement').then(({ AgencyAgreementView }) => AgencyAgreementView));

/**
 * Страница списка баз данных
 */
export const DatabaseListPageAsync = asyncComponent(() => import('app/views/modules/DatabaseList').then(({ DatabaseListView }) => DatabaseListView));

/**
 * Страница Маркетинг
 */
export const MarketingPage = asyncComponent(() => import('app/views/modules/Marketing').then(({ MarketingView }) => MarketingView));

/**
 * Модуль страницы "Маркетинг" для создания рекламного баннера
 */
export const AdvertisingBannerCreatePage = asyncComponent(() => import('app/views/modules/Marketing').then(({ AdvertisingBannerCreate }) => AdvertisingBannerCreate));

/**
 * Модуль страницы "Маркетинг" для редактирования рекламного баннера
 */
export const AdvertisingBannerEditPage = asyncComponent(() => import('app/views/modules/Marketing').then(({ AdvertisingBannerEdit }) => AdvertisingBannerEdit));

/**
 * Модуль страницы "Маркетинг" для редактирования шаблона письма
 */
export const LetterTemplateEditPage = asyncComponent(() => import('app/views/modules/Marketing').then(({ LetterTemplateEdit }) => LetterTemplateEdit));

/**
 * Страница списка аккаунтов
 */
export const AccountListPageAsync = asyncComponent(() => import('app/views/modules/AccountList').then(({ AccountListView }) => AccountListView));

/**
 * Страница ARM бэкапирования
 */
export const AccountDatabaseBackupArmPageAsync = asyncComponent(() => import('app/views/modules/AccountDatabaseBackupArm').then(({ AccountDatabaseBackupArmView }) => AccountDatabaseBackupArmView));

/**
 * Страница Обновления шаблона базы
 */
export const DbTemplateUpdatesPageAsync = asyncComponent(() => import('app/views/modules/DbTemplateUpdates').then(({ DbTemplateUpdatesView }) => DbTemplateUpdatesView));

/**
 * Страница справочника поставщиков
 */
export const SuppliersPageAsync = asyncComponent(() => import('app/views/modules/Suppliers').then(({ SupplierView }) => SupplierView));

/**
 * Страница редактирования/создания поставщиков
 */
export const CreateEditSupplierPage = CreateEditSupplierView;

/**
 * Страница реферальных аккаунтов поставщика
 */
export const SupplierReferalAccountsPage = asyncComponent(() => import('app/views/modules/Suppliers/views/ReferalAccountsView').then(({ ReferalAccountsView }) => ReferalAccountsView));

/**
 * Страница локалей
 */
export const LocalesPage = asyncComponent(() => import('app/views/modules/Locales').then(({ LocalesPageView }) => LocalesPageView));

/**
 * Страница печатных форм HTML
 */
export const PrintedHtmlFormsReferencePage = asyncComponent(() => import('app/views/modules/PrintedHtmlFormsReference').then(({ PrintedHtmlFormsReferenceView }) => PrintedHtmlFormsReferenceView));

/**
 * Страница редактирования/создания печатных форм HTML
 */
export const CreateEditPrintedHtmlFormPage = CreateEditPrintedHtmlFormView;

/**
 * Страница Сегментов
 */
export const SegmentsPage = asyncComponent(() => import('app/views/modules/Segments').then(({ SegmentsView }) => SegmentsView));

/**
 * Страница Справочник -> Доступ к ИТС
 */
export const ItsDataAuthorizationPage = asyncComponent(() => import('app/views/modules/ItsDataAuthorization').then(({ ItsDataAuthorizationView }) => ItsDataAuthorizationView));

/**
 * Страница ТиИ/АО -> АРМ Автообновления
 */
export const ArmAutoUpdateAccountDatabasePage = asyncComponent(() => import('app/views/modules/ArmAutoUpdateAccountDatabaseView').then(({ ArmAutoUpdateAccountDatabaseView }) => ArmAutoUpdateAccountDatabaseView));

/**
 * Страница Управление Аккаунтом -> Баланс
 */
export const BalanceManagementPage = asyncComponent(() => import('app/views/modules/AccountManagement/BalanceManagementView').then(({ BalanceManagementView }) => BalanceManagementView));
/**
 * Страница ТиИ/АО -> АРМ Автообновления
 */
export const ReplenishBalancePage = asyncComponent(() => import('app/views/modules/AccountManagement/BalanceManagementView/view/InvoicesDataView/AddPayment').then(({ AddPaymentView }) => AddPaymentView));
/**
 * Страница подтверждения оплаты через Юкасса
 */
export const ConfirmPaymentView = asyncComponent(() => import('app/views/modules/AccountManagement/BalanceManagementView/view/InvoicesDataView/AddPayment/common/ConfirmPaymentView')
    .then(({ ConfirmPaymentComponent }) => ConfirmPaymentComponent));
/**
 * Страница Управление Аккаунтом -> Баланс -> Пополнить баланс
 */
// export const ReplanishBalancePage = asyncComponent(() => import('app/views/modules/AccountManagement/BalanceManagementView/view/InvoicesDataView/AddPayment').then(({ AddPaymentView }) => AddPaymentView));

/**
 * Страница Управление Аккаунтом -> Пользователи
 */
export const UsersManagementPage = asyncComponent(() => import('app/views/modules/AccountManagement/Users/index').then(({ UsersView }) => UsersView));

/**
 * Страница Страница Управление Аккаунтом -> Информационные базы
 */
export const InformationBasesAsync = asyncComponent(() => import('app/views/modules/AccountManagement/InformationBases/index').then(({ InformationBasesView }) => InformationBasesView));
/**
 * Страница Управление Аккаунтом -> Данные О Компании
 */
export const MyCompanyPage = asyncComponent(() => import('app/views/modules/AccountManagement/MyCompany/index').then(({ MyCompanyPageView }) => MyCompanyPageView));

/**
 * Страница Сервисы -> Дополнительные сеансы
 */
export const AdditionalSessionsPage = asyncComponent(() => import('app/views/modules/AdditionalSessions/index').then(({ AdditionalSessionsView }) => AdditionalSessionsView));

/**
 * Страница Используемые сервисы -> Маркет42
 */
export const M42Page = asyncComponent(() => import('app/views/modules/Services/views/MyServices/m42').then(({ M42PageView }) => M42PageView));

/**
 * Страница Используемые сервисы -> Расширения и обработки
 */
export const ExtensionsAndProcessingPage = asyncComponent(() => import('app/views/modules/Services/views/MyServices/extensionsAndProcessing').then(({ ExtensionsAndProcessingPageView }) => ExtensionsAndProcessingPageView));

/**
 * Страница Создания Сервиса
 */
export const CreateServicePage = asyncComponent(() => import('app/views/modules/Services/views/AddServiceCard').then(({ AddServicePageView }) => AddServicePageView));

/**
 * Странница Карточки Сервиса
 */
export const ServiceCardPage = asyncComponent(() => import('app/views/modules/Services/views/ServiceCard/').then(({ ServiceCardPageView }) => ServiceCardPageView));

/**
 * Странница Редактирования Сервиса
 */
export const EditServicePage = asyncComponent(() => import('app/views/modules/Services/views/EditServiceCard').then(({ EditServicePageView }) => EditServicePageView));

/**
 * Странца Управление аккаунтом -> Инфомрационные базы -> Загрузить свою базу 1С
 */
export const LoadingDatabase = asyncComponent(() => import('app/views/modules/CreateDatabase/LoadingDatabase/index').then(({ CreateCustomDatabaseView }) => CreateCustomDatabaseView));

/**
 * Страница Настройки -> Профиль
 */
export const ProfilePage = asyncComponent(() => import('app/views/modules/Profile/index').then(({ ProfileView }) => ProfileView));

/**
 * Страница Админ панель -> Партнеры
 */
export const Partners = asyncComponent(() => import('app/views/modules/Partners').then(({ AdministratePartners }) => AdministratePartners));

/**
 * Страница Партнерам
 */
export const ToPartners = asyncComponent(() => import('app/views/modules/ToPartners').then(({ ToPartnersView }) => ToPartnersView));

/**
 * Модуль страницы "Партнерам" для создания реквизитов
 */
export const CreateAgentRequisites = asyncComponent(() => import('app/views/modules/ToPartners/views/agentRequisiteCreate').then(({ AgentRequisitesCreate }) => AgentRequisitesCreate));

/**
 * Модуль страницы "Партнерам" для редактирования реквизитов
 */
export const EditAgentRequisites = asyncComponent(() => import('app/views/modules/ToPartners/views/agentRequisitesEdit').then(({ AgentRequisitesEdit }) => AgentRequisitesEdit));

/**
 * Модуль страницы "Партнерам" для создания заявки на выплату
 */
export const CreateAgentCashOut = asyncComponent(() => import('app/views/modules/ToPartners/views/agentCashOutCreate').then(({ AgentCashOutCreate }) => AgentCashOutCreate));

/**
 * Модуль страницы "Партнерам" для редактирования заявки на выплату
 */
export const EditAgentCashOut = asyncComponent(() => import('app/views/modules/ToPartners/views/agentCashOutEdit').then(({ AgentCashOutEdit }) => AgentCashOutEdit));

export const RentViewPage = asyncComponent(() => import('app/views/modules/Rent/index').then(({ RentViewComponent }) => RentViewComponent));

export const FastaViewPage = asyncComponent(() => import('app/views/modules/Fasta/index').then(({ FastaViewComponent }) => FastaViewComponent));

export const RentEditResourcePage = RentEditResourceView;

/**
 * Страницы Настройки -> Настройки подключения
 */
export const ConnectionSettingsPage = asyncComponent(() => import('app/views/modules/ConnectionSettings').then(({ ConnectionSettingsView }) => ConnectionSettingsView));

export const MyDiskPage = asyncComponent(() => import('app/views/modules/MyDisk').then(({ MyDiskViewPage }) => MyDiskViewPage));

/**
 * Страница Информационные базы -> Добавить базу -> Создать новую
 */
export const CreateDbPage = asyncComponent(() => import('app/views/modules/CreateDatabase/CreateDb').then(({ CreateDbView }) => CreateDbView));

/**
 * Сторонние сервисы (саюри, тардис, ...)
 */
export const ManagingServiceSubscriptions = asyncComponent(() => import('app/views/modules/ManagingServiceSubscriptions').then(({ ManagingServiceSubscriptionsView }) => ManagingServiceSubscriptionsView));

/**
 * Сервисы информационных баз (Клиент серверный режим, Комплексная автоматизация, ...)
 */
export const ManagingServicesMyDatabaseSubscription =
asyncComponent(() => import('app/views/modules/ManagingServicesMyDatabaseSubscription').then(({ ManagingServicesMyDatabaseSubscriptionView }) => ManagingServicesMyDatabaseSubscriptionView));

export const Neuro = asyncComponent(() => import('app/views/modules/Neuro').then(({ Neuro }) => Neuro));

export const ArmManagerPage = asyncComponent(() => import('app/views/modules/ArmManager').then(({ ArmManager }) => ArmManager));

export const UnsubscribePage = asyncComponent(() => import('app/views/modules/UnsubscribePage').then(({ UnsubscribePageView }) => UnsubscribePageView));

export const UltraPage = asyncComponent(() => import('app/views/modules/Ultra').then(({ UltraPageView }) => UltraPageView));

export const UltraDevops = asyncComponent(() => import('app/views/modules/UltraDevops').then(({ UltraDevopsForOneC }) => UltraDevopsForOneC));

export const EmailVerify = asyncComponent(() => import('app/views/modules/EmailVerify').then(({ EmailVerifyView }) => EmailVerifyView));
/**
 * Страница Админ панель -> Рассылка уведомлений
 */
export const SendingNotificationsPage = asyncComponent(() => import('app/views/modules/SendingNotifications').then(({ SendingNotificationsView }) => SendingNotificationsView));

/**
 * Страница Статьи
 */
export const ArticlesPage = asyncComponent(() => import('app/views/modules/Articles/index').then(({ ArticlesView }) => ArticlesView));
/**
 * Страница Информационные базы -> Редактирование статьи
 */
export const EditArticlePage = asyncComponent(() => import('app/views/modules/Articles/components/EditArticle').then(({ EditArticleView }) => EditArticleView));

/**
 * Страница подтверждение удаления аккаунта (переход из ссылки в письме)
 */
export const ConfirmationOfAccountDeletionPage = asyncComponent(() => import('app/views/modules/ConfirmationOfAccountDeletion/index').then(({ ConfirmationOfAccountDeletionView }) => ConfirmationOfAccountDeletionView));

/**
 * Страница подтверждения акта (переход из ссылки в письме)
 */
export const ConfirmationActPage = asyncComponent(() => import('app/views/modules/ConfirmationAct/index').then(({ ConfirmationActView }) => ConfirmationActView));

/**
 * Страница альпаки Используемые сервисы -> AlpacaMeet
 */
export const AlpacaMeetPage = asyncComponent(() => import('app/views/modules/AlpacaMeet').then(({ AlpacaMeetView }) => AlpacaMeetView));

/**
 * Страница Настройки -> Консультант 1С
 */
export const Consultations1CPage = asyncComponent(() => import('app/views/modules/Consultations1C').then(({ Consultations1CView }) => Consultations1CView));

/**
 * Страница Настройки -> Регистрационный номер
 */
export const RegistrationNumberPage = asyncComponent(() => import('app/views/modules/RegistrationNumber').then(({ RegistrationNumberView }) => RegistrationNumberView));

/**
 * Страница Настройки -> 1С Отчетность
 */
export const ReportingPage = asyncComponent(() => import('app/views/modules/Reporting').then(({ ReportingView }) => ReportingView));

/**
 * Страница Настройки -> 1С ЭДО
 */
export const EdoPage = asyncComponent(() => import('app/views/modules/Edo').then(({ EdoView }) => EdoView));

/**
 * Страница Настройки -> Акт всерки
 */
export const ReconciliationStatementPage = asyncComponent(() => import('app/views/modules/ReconciliationStatement').then(({ ReconciliationStatementView }) => ReconciliationStatementView));

export const InformationDatabases = asyncComponent(() => import('app/views/modules/AccountManagement/InformationDatabases').then(({ InformationDatabasesPage }) => InformationDatabasesPage));
export const InformationDatabasesLegacy = asyncComponent(() => import('app/views/modules/AccountManagement/InformationBases').then(({ InformationBasesView }) => InformationBasesView));